/**
 * 
 */
package au.com.target.tgtauspost.deliveryclub.client;

import org.springframework.http.HttpHeaders;


/**
 * @author rmcalave
 *
 */
public class ShipsterResponseHelper {

    private static final String RATE_LIMIT_HOUR_HEADER = "X-RateLimit-Limit-hour";
    private static final String RATE_LIMIT_HOUR_REMAINING_HEADER = "X-RateLimit-Remaining-hour";
    private static final String RATE_LIMIT_DAY_HEADER = "X-RateLimit-Limit-day";
    private static final String RATE_LIMIT_DAY_REMAINING_HEADER = "X-RateLimit-Remaining-day";
    private static final String RATE_LIMIT_MINUTE_HEADER = "X-RateLimit-Limit-minute";
    private static final String RATE_LIMIT_MINUTE_REMAINING_HEADER = "X-RateLimit-Remaining-minute";
    private static final String RATE_LIMIT_SECOND_HEADER = "X-RateLimit-Limit-second";
    private static final String RATE_LIMIT_SECOND_REMAINING_HEADER = "X-RateLimit-Remaining-second";

    private static final String LOG_RATE_LIMITS_MESSAGE = "Limits=[Day=[limit=%s, remaining=%s], Hour=[limit=%s, remaining=%s], Minute=[limit=%s, remaining=%s], Second=[limit=%s, remaining=%s]]";

    public String getTooManyRequestsLogMessage(final HttpHeaders responseHeaders) {
        final String rateLimitDayLimit = responseHeaders.getFirst(RATE_LIMIT_DAY_HEADER);
        final String rateLimitDayRemaining = responseHeaders.getFirst(RATE_LIMIT_DAY_REMAINING_HEADER);
        final String rateLimitHourLimit = responseHeaders.getFirst(RATE_LIMIT_HOUR_HEADER);
        final String rateLimitHourRemaining = responseHeaders.getFirst(RATE_LIMIT_HOUR_REMAINING_HEADER);
        final String rateLimitMinuteLimit = responseHeaders.getFirst(RATE_LIMIT_MINUTE_HEADER);
        final String rateLimitMinuteRemaining = responseHeaders.getFirst(RATE_LIMIT_MINUTE_REMAINING_HEADER);
        final String rateLimitSecondLimit = responseHeaders.getFirst(RATE_LIMIT_SECOND_HEADER);
        final String rateLimitSecondRemaining = responseHeaders.getFirst(RATE_LIMIT_SECOND_REMAINING_HEADER);

        return String.format(LOG_RATE_LIMITS_MESSAGE, rateLimitDayLimit,
                rateLimitDayRemaining, rateLimitHourLimit, rateLimitHourRemaining, rateLimitMinuteLimit,
                rateLimitMinuteRemaining, rateLimitSecondLimit, rateLimitSecondRemaining);


    }
}

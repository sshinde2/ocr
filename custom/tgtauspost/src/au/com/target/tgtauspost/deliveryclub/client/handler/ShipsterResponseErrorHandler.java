/**
 * 
 */
package au.com.target.tgtauspost.deliveryclub.client.handler;

import java.io.IOException;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;


/**
 * Custom error handler to deal with Shipster HTTP status codes.
 * 
 * Shipster use HTTP status codes to indicate error states, but also include details in the response body that we need
 * to log. This error handler causes HTTP status codes that are included in Shipster API documentation to not be treated
 * as an error, so that the client can handle the error itself.
 * 
 * @author rmcalave
 *
 */
public class ShipsterResponseErrorHandler implements ResponseErrorHandler {

    private Set<HttpStatus> shipsterKnownErrorCodes;

    private ResponseErrorHandler fallbackResponseErrorHandler;

    /* (non-Javadoc)
     * @see org.springframework.web.client.DefaultResponseErrorHandler#hasError(org.springframework.http.client.ClientHttpResponse)
     */
    @Override
    public boolean hasError(final ClientHttpResponse response) throws IOException {
        if (CollectionUtils.isNotEmpty(shipsterKnownErrorCodes)) {
            try {
                if (shipsterKnownErrorCodes.contains(response.getStatusCode())) {
                    return false;
                }
            }
            catch (final IOException ex) {
                // Do nothing - fallback handler will handle it.
            }
        }

        return fallbackResponseErrorHandler.hasError(response);
    }

    /* (non-Javadoc)
     * @see org.springframework.web.client.ResponseErrorHandler#handleError(org.springframework.http.client.ClientHttpResponse)
     */
    @Override
    public void handleError(final ClientHttpResponse paramClientHttpResponse) throws IOException {
        fallbackResponseErrorHandler.handleError(paramClientHttpResponse);
    }

    /**
     * @param shipsterKnownErrorCodes
     *            the shipsterKnownErrorCodes to set
     */
    public void setShipsterKnownErrorCodes(final Set<HttpStatus> shipsterKnownErrorCodes) {
        this.shipsterKnownErrorCodes = shipsterKnownErrorCodes;
    }

    /**
     * @param fallbackResponseErrorHandler
     *            the fallbackResponseErrorHandler to set
     */
    @Required
    public void setFallbackResponseErrorHandler(final ResponseErrorHandler fallbackResponseErrorHandler) {
        this.fallbackResponseErrorHandler = fallbackResponseErrorHandler;
    }

}

/**
 * 
 */
package au.com.target.tgtauspost.deliveryclub.dto;

/**
 * @author gsing236
 *
 */
public class ShipsterVerifyEmailResponseDTO extends AbstractShipsterResponseDTO {

    private boolean verified;

    /**
     * @return the verified
     */
    public boolean isVerified() {
        return verified;
    }

    /**
     * @param verified
     *            the verified to set
     */
    public void setVerified(final boolean verified) {
        this.verified = verified;
    }

}

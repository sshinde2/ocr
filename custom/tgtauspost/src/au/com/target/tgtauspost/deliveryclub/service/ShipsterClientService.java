/**
 * 
 */
package au.com.target.tgtauspost.deliveryclub.service;

import de.hybris.platform.core.model.order.OrderModel;

import au.com.target.tgtauspost.exception.AuspostShipsterClientException;
import au.com.target.tgtauspost.integration.dto.ShipsterConfirmOrderResponseDTO;


/**
 * @author gsing236
 *
 */
public interface ShipsterClientService {

    /**
     * verify if the provided email is registered for Australia Post Delivery Club or not
     * 
     * @param email
     *            - email provided
     * @return - boolean, whether member of Australia Post Delivery Club or not
     * @throws AuspostShipsterClientException
     */
    boolean verifyEmail(final String email) throws AuspostShipsterClientException;

    /**
     * 
     * @param orderModel
     * @return Order confirmation response object
     * @throws AuspostShipsterClientException
     */
    ShipsterConfirmOrderResponseDTO confirmOrder(
            final OrderModel orderModel)
                    throws AuspostShipsterClientException;

}

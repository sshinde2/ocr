/**
 * 
 */
package au.com.target.tgtauspost.dispatchlabel.client.impl;

import au.com.target.tgtauspost.dispatchlabel.client.DispatchLabelClient;
import au.com.target.tgtauspost.integration.dto.AuspostRequestDTO;
import au.com.target.tgtfulfilment.integration.dto.LabelResponseDTO;


/**
 * Default implmentation of DispatchLabelClient
 * 
 * @author sbryan6
 *
 */
public class DefaultDispatchLabelClientImpl implements DispatchLabelClient {

    @Override
    public LabelResponseDTO retrieveDispatchLabel(final AuspostRequestDTO request) {

        return null;
    }

}

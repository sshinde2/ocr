/**
 * 
 */
package au.com.target.tgtauspost.exception;

/**
 * custom exception for Auspost delivery club.
 * 
 * @author gsing236
 *
 */
public class AuspostShipsterClientException extends RuntimeException {

    /**
     * @param message
     *            Custom message
     */
    public AuspostShipsterClientException(final String message) {
        super(message);
    }

    /**
     * @param e
     *            Exception
     */
    public AuspostShipsterClientException(final Throwable e) {

        super(e);
    }

    /**
     * @param message
     *            Custom message
     * @param e
     *            Exception
     */
    public AuspostShipsterClientException(final String message, final Throwable e) {
        super(message, e);
    }

}

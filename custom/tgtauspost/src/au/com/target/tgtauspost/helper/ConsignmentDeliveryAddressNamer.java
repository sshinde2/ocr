/**
 * 
 */
package au.com.target.tgtauspost.helper;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtauspost.exception.AuspostIncompleteDataException;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.util.EscapeSpecialCharactersUtil;


/**
 * ConsignmentDeliveryAddressNamer Utility
 * 
 * @author jjayawa1
 *
 */
public class ConsignmentDeliveryAddressNamer {
    private static final Logger LOG = Logger.getLogger(ConsignmentDeliveryAddressNamer.class);

    private String companyName;
    private TargetPointOfServiceService pointOfServiceService;

    public String getDeliveryName(final TargetConsignmentModel consignmentModel) {
        Assert.notNull(consignmentModel, "Consignment cannot be null");

        String deliveryName = null;
        final OfcOrderType orderType = consignmentModel.getOfcOrderType();

        if (OfcOrderType.INTERSTORE_DELIVERY.equals(orderType)) {
            deliveryName = makeStoreName(consignmentModel);
        }
        else {
            if (consignmentModel.getShippingAddress() != null) {
                deliveryName = makeName(consignmentModel.getShippingAddress());
            }
            else {
                throw new AuspostIncompleteDataException(
                        "ConsignmentDeliveryAddressNamer : shipping address cannot be null");
            }
        }
        return deliveryName;
    }

    public String getCompanyName(final TargetConsignmentModel consignmentModel) {
        final OfcOrderType orderType = consignmentModel.getOfcOrderType();

        if (OfcOrderType.INTERSTORE_DELIVERY.equals(orderType)) {
            return EscapeSpecialCharactersUtil.escapeSpecialXMLCharacters(getCompanyName());
        }

        return StringUtils.EMPTY;
    }

    private String makeName(final AddressModel address) {
        String name = null;

        if (StringUtils.isNotEmpty(address.getFirstname())) {
            name = address.getFirstname();
        }

        if (StringUtils.isNotEmpty(name)) {
            if (StringUtils.isNotEmpty(address.getLastname())) {
                name = name + " " + address.getLastname();
            }
        }

        return name;
    }

    private String makeStoreName(final TargetConsignmentModel consignment) {
        String name = null;

        final OrderModel orderModel = (OrderModel)consignment.getOrder();
        final Integer storeNumber = orderModel.getCncStoreNumber();

        if (storeNumber != null) {
            final TargetPointOfServiceModel store;
            try {
                store = pointOfServiceService.getPOSByStoreNumber(storeNumber);
                if (store != null) {
                    name = "Target - ";
                    if (StringUtils.isNotEmpty(store.getName())) {
                        name += store.getName();
                    }
                }
            }
            catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
                LOG.error("ConsignmentDeliveryAddressNamer : "
                        + "FAILED to get store info for StoreNumber=" + storeNumber, e);
            }
        }
        else {
            throw new AuspostIncompleteDataException("ConsignmentDeliveryAddressNamer : storeNumber is cannot be null");
        }

        return name;
    }

    /**
     * @return the companyName
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * @param companyName
     *            the companyName to set
     */
    @Required
    public void setCompanyName(final String companyName) {
        this.companyName = companyName;
    }


    /**
     * @param pointOfServiceService
     *            the pointOfServiceService to set
     */
    @Required
    public void setPointOfServiceService(final TargetPointOfServiceService pointOfServiceService) {
        this.pointOfServiceService = pointOfServiceService;
    }

}

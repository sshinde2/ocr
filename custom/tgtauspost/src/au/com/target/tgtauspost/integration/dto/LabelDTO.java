/**
 * 
 */
package au.com.target.tgtauspost.integration.dto;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * DTO to hold label data.
 * 
 * @author jjayawa1
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class LabelDTO {
    private String layout;
    private String branding;

    /**
     * @return the layout
     */
    public String getLayout() {
        return layout;
    }

    /**
     * @param layout
     *            the layout to set
     */
    public void setLayout(final String layout) {
        this.layout = layout;
    }

    /**
     * @return the branding
     */
    public String getBranding() {
        return branding;
    }

    /**
     * @param branding
     *            the branding to set
     */
    public void setBranding(final String branding) {
        this.branding = branding;
    }
}

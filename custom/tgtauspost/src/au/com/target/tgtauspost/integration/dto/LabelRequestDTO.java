/**
 * 
 */
package au.com.target.tgtauspost.integration.dto;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * DTO to populate with details for manifest request.
 * 
 * @author jjayawa1
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class LabelRequestDTO {
    private StoreDTO store;

    /**
     * @return the store
     */
    public StoreDTO getStore() {
        return store;
    }

    /**
     * @param store
     *            the store to set
     */
    public void setStore(final StoreDTO store) {
        this.store = store;
    }

}

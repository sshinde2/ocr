/**
 * 
 */
package au.com.target.tgtauspost.integration.dto;

/**
 * java object to pass data to AusPost Shipster API for confirming order.
 * 
 * @author gsing236
 *
 */
public class ShipsterConfirmOrderRequestDTO {

    private String email;
    private String orderReference;
    private Number orderTotal;
    private Number shippingFee;
    private Number shippingFeePaid;
    private String shippingMethod;
    private ShipsterDeliveryAddressDTO deliveryAddress;

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * @return the orderReference
     */
    public String getOrderReference() {
        return orderReference;
    }

    /**
     * @param orderReference
     *            the orderReference to set
     */
    public void setOrderReference(final String orderReference) {
        this.orderReference = orderReference;
    }

    /**
     * @return the orderTotal
     */
    public Number getOrderTotal() {
        return orderTotal;
    }

    /**
     * @param orderTotal
     *            the orderTotal to set
     */
    public void setOrderTotal(final Number orderTotal) {
        this.orderTotal = orderTotal;
    }

    /**
     * @return the shippingFee
     */
    public Number getShippingFee() {
        return shippingFee;
    }

    /**
     * @param shippingFee
     *            the shippingFee to set
     */
    public void setShippingFee(final Number shippingFee) {
        this.shippingFee = shippingFee;
    }

    /**
     * @return the shippingMethod
     */
    public String getShippingMethod() {
        return shippingMethod;
    }

    /**
     * @param shippingMethod
     *            the shippingMethod to set
     */
    public void setShippingMethod(final String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    /**
     * @return the deliveryAddress
     */
    public ShipsterDeliveryAddressDTO getDeliveryAddress() {
        return deliveryAddress;
    }

    /**
     * @param deliveryAddress
     *            the deliveryAddress to set
     */
    public void setDeliveryAddress(final ShipsterDeliveryAddressDTO deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    /**
     * @return the shippingFeePaid
     */
    public Number getShippingFeePaid() {
        return shippingFeePaid;
    }

    /**
     * @param shippingFeePaid
     *            the shippingFeePaid to set
     */
    public void setShippingFeePaid(final Number shippingFeePaid) {
        this.shippingFeePaid = shippingFeePaid;
    }


}

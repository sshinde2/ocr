/**
 * 
 */
package au.com.target.tgtauspost.integration.dto;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * DTO to populate store details.
 * 
 * @author jjayawa1
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class StoreDTO {
    private String name;
    private String chargeToAccount;
    private String merchantLocationId;
    private String chargeCode;
    private String productCode;
    private String serviceCode;
    private String username;
    private AddressDTO address;
    private LabelDTO label;
    private TargetManifestDTO manifest;
    private ConsignmentDetailsDTO consignment;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the chargeToAccount
     */
    public String getChargeToAccount() {
        return chargeToAccount;
    }

    /**
     * @param chargeToAccount
     *            the chargeToAccount to set
     */
    public void setChargeToAccount(final String chargeToAccount) {
        this.chargeToAccount = chargeToAccount;
    }

    /**
     * @return the merchantLocationId
     */
    public String getMerchantLocationId() {
        return merchantLocationId;
    }

    /**
     * @param merchantLocationId
     *            the merchantLocationId to set
     */
    public void setMerchantLocationId(final String merchantLocationId) {
        this.merchantLocationId = merchantLocationId;
    }

    /**
     * @return the chargeCode
     */
    public String getChargeCode() {
        return chargeCode;
    }

    /**
     * @param chargeCode
     *            the chargeCode to set
     */
    public void setChargeCode(final String chargeCode) {
        this.chargeCode = chargeCode;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the serviceCode
     */
    public String getServiceCode() {
        return serviceCode;
    }

    /**
     * @param serviceCode
     *            the serviceCode to set
     */
    public void setServiceCode(final String serviceCode) {
        this.serviceCode = serviceCode;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     *            the username to set
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * @return the address
     */
    public AddressDTO getAddress() {
        return address;
    }

    /**
     * @param address
     *            the address to set
     */
    public void setAddress(final AddressDTO address) {
        this.address = address;
    }

    /**
     * @return the label
     */
    public LabelDTO getLabel() {
        return label;
    }

    /**
     * @param label
     *            the label to set
     */
    public void setLabel(final LabelDTO label) {
        this.label = label;
    }

    /**
     * @return the manifest
     */
    public TargetManifestDTO getManifest() {
        return manifest;
    }

    /**
     * @param manifest
     *            the manifest to set
     */
    public void setManifest(final TargetManifestDTO manifest) {
        this.manifest = manifest;
    }

    /**
     * @return the consignment
     */
    public ConsignmentDetailsDTO getConsignment() {
        return consignment;
    }

    /**
     * @param consignment
     *            the consignment to set
     */
    public void setConsignment(final ConsignmentDetailsDTO consignment) {
        this.consignment = consignment;
    }

}

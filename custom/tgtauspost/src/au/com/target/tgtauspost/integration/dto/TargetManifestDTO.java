/**
 * 
 */
package au.com.target.tgtauspost.integration.dto;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * DTO to populate manifest data.
 * 
 * @author jjayawa1
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class TargetManifestDTO {
    private String transactionDateTime;
    private String manifestNumber;
    private List<ConsignmentDTO> consignments;

    /**
     * @return the transactionDateTime
     */
    public String getTransactionDateTime() {
        return transactionDateTime;
    }

    /**
     * @param transactionDateTime
     *            the transactionDateTime to set
     */
    public void setTransactionDateTime(final String transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    /**
     * @return the manifestNumber
     */
    public String getManifestNumber() {
        return manifestNumber;
    }

    /**
     * @param manifestNumber
     *            the manifestNumber to set
     */
    public void setManifestNumber(final String manifestNumber) {
        this.manifestNumber = manifestNumber;
    }

    /**
     * @return the consignments
     */
    public List<ConsignmentDTO> getConsignments() {
        return consignments;
    }

    /**
     * @param consignments
     *            the consignments to set
     */
    public void setConsignments(final List<ConsignmentDTO> consignments) {
        this.consignments = consignments;
    }

}

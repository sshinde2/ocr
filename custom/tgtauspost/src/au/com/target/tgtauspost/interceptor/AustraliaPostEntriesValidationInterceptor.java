package au.com.target.tgtauspost.interceptor;

import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;


/**
 * 
 * @author smishra1
 */
public class AustraliaPostEntriesValidationInterceptor implements ValidateInterceptor {
    private static final String INTER_STORE_DELIVERY_ALLOWED = "Inter Store Delivery Enabled Store";
    private static final String HOME_DELIVERY_ENABLED_STORE = "Customer Delivery Enabled Store";

    /**
     * Method to validate the mandatory Parameters required for Auspost
     * 
     * @throws InterceptorException
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx)
            throws InterceptorException {
        if (model instanceof StoreFulfilmentCapabilitiesModel) {
            final StoreFulfilmentCapabilitiesModel storeFulfilmentModel = (StoreFulfilmentCapabilitiesModel)model;

            if (BooleanUtils.isTrue(storeFulfilmentModel.getAllowDeliveryToAnotherStore())) {
                validateNotNullAusPostEntries(storeFulfilmentModel, INTER_STORE_DELIVERY_ALLOWED);
            }
            else {
                final Set<TargetZoneDeliveryModeModel> deliveryModelList = storeFulfilmentModel
                        .getDeliveryModes();
                if (CollectionUtils.isNotEmpty(deliveryModelList)) {
                    for (final TargetZoneDeliveryModeModel deliveryType : deliveryModelList) {
                        if (BooleanUtils.isFalse(deliveryType.getIsDeliveryToStore())) {
                            validateNotNullAusPostEntries(storeFulfilmentModel, HOME_DELIVERY_ENABLED_STORE);
                        }
                    }
                }
            }
        }
    }

    /**
     * Method to validate the mandatory parameters.
     * 
     * @param storeFulfilmentModel
     *            the StoreFulfilmentCapabilitiesModel
     * 
     * @param type
     *            The type of delivery
     * 
     * @throws InterceptorException
     *             interceptorException
     */
    private void validateNotNullAusPostEntries(
            final StoreFulfilmentCapabilitiesModel storeFulfilmentModel,
            final String type) throws InterceptorException {
        if (StringUtils.isEmpty(storeFulfilmentModel.getMerchantLocationId())) {
            throw new InterceptorException("Merchant Location ID (MLID) can not be null for : " + type);
        }
        else if (StringUtils.isEmpty(storeFulfilmentModel.getChargeCode())) {
            throw new InterceptorException("Charge Code can not be null for : " + type);
        }
        else if (null == storeFulfilmentModel.getChargeAccountNumber()) {
            throw new InterceptorException("Charge Account Number can not be null for : " + type);
        }
        else if (StringUtils.isEmpty(storeFulfilmentModel.getUsername())) {
            throw new InterceptorException("User Name can not be null for : " + type);
        }
        else if (null == storeFulfilmentModel.getProductCode()) {
            throw new InterceptorException("Product Code can not be null for : " + type);
        }
        else if (null == storeFulfilmentModel.getServiceCode()) {
            throw new InterceptorException("Service Code can not be null for : " + type);
        }
        else if (null == storeFulfilmentModel.getMerchantId()) {
            throw new InterceptorException("Merchant ID can not be null for : " + type);
        }
        else if (null == storeFulfilmentModel.getLodgementFacility()) {
            throw new InterceptorException("Lodgement Facility can not be null for : " + type);
        }
    }

}

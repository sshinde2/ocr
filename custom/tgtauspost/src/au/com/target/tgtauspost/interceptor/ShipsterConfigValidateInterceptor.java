/**
 * 
 */
package au.com.target.tgtauspost.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.lang.BooleanUtils;

import au.com.target.tgtauspost.model.ShipsterConfigModel;


/**
 * validate the entries in {@link ShipsterConfigModel} configuration
 * 
 * @author gsing236
 *
 */
public class ShipsterConfigValidateInterceptor implements ValidateInterceptor<ShipsterConfigModel> {

    private static final String MAX_SHIPPING_THRESHOLD_ERROR = "Max Shipping Threshold is mandatory.";
    private static final String MIN_ORDER_VALUE_ERROR = "Min Order Value is mandatory.";

    @Override
    public void onValidate(final ShipsterConfigModel model, final InterceptorContext paramInterceptorContext)
            throws InterceptorException {
        if (model != null && BooleanUtils.isTrue(model.getActive())) {

            if (model.getMaxShippingThresholdValue() == null) {
                throw new InterceptorException(MAX_SHIPPING_THRESHOLD_ERROR);
            }

            if (model.getMinOrderTotalValue() == null) {
                throw new InterceptorException(MIN_ORDER_VALUE_ERROR);
            }
        }

    }

}

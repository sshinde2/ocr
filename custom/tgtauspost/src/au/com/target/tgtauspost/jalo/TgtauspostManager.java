package au.com.target.tgtauspost.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;

import au.com.target.tgtauspost.constants.TgtauspostConstants;


public class TgtauspostManager extends GeneratedTgtauspostManager {

    /**
     * Manager class for TgtAuspost
     * 
     * @return instance of TgtauspostManager
     */
    public static final TgtauspostManager getInstance() {
        final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
        return (TgtauspostManager)em.getExtension(TgtauspostConstants.EXTENSIONNAME);
    }

}

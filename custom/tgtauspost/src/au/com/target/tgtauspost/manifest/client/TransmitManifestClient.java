/**
 * 
 */
package au.com.target.tgtauspost.manifest.client;

import au.com.target.tgtauspost.integration.dto.AuspostRequestDTO;
import au.com.target.tgtfulfilment.integration.dto.ManifestResponseDTO;


/**
 * Interface to define supported transmit manifest operations.
 * 
 * @author jjayawa1
 *
 */
public interface TransmitManifestClient {
    /**
     * Middleware call to transmit manifest to carrier.
     * 
     * @param request
     * @return ManifestResponseDTO
     */
    ManifestResponseDTO transmitManifest(AuspostRequestDTO request);

}

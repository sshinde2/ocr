/**
 * 
 */
package au.com.target.tgtauspost.manifest.client.impl;

import au.com.target.tgtauspost.integration.dto.AuspostRequestDTO;
import au.com.target.tgtauspost.manifest.client.TransmitManifestClient;
import au.com.target.tgtfulfilment.integration.dto.ManifestResponseDTO;


/**
 * Default implementation for TransmitManifestClient
 * 
 * @author jjayawa1
 *
 */
public class DefaultTransmitManifestClientImpl implements TransmitManifestClient {

    /* (non-Javadoc)
     * @see au.com.target.tgtauspost.manifest.client.TransmitManifestClient#transmitManifest(au.com.target.tgtfulfilment.manifest.dto.ManifestRequestDTO)
     */
    @Override
    public ManifestResponseDTO transmitManifest(final AuspostRequestDTO request) {
        return new ManifestResponseDTO();
    }

}

/**
 * 
 */
package au.com.target.tgtauspost.manifest.converter;

/**
 * @author jjayawa1
 *
 */
public interface AuspostPopulatingConverter<TSOURCE, TTARGET> {

    TTARGET convert(TSOURCE SOURCE, TTARGET TARGET);
}

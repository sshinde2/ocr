/**
 * 
 */
package au.com.target.tgtauspost.manifest.converter.impl;

import de.hybris.platform.core.model.user.AddressModel;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import au.com.target.tgtauspost.integration.dto.AddressDTO;
import au.com.target.tgtauspost.manifest.converter.AuspostPopulatingConverter;
import au.com.target.tgtcore.converters.TargetConverter;


/**
 * @author jjayawa1
 *
 */
public class AuspostAddressConverter implements TargetConverter<AddressModel, AddressDTO>,
        AuspostPopulatingConverter<AddressModel, AddressDTO> {
    private static final String DEFAULT_COUNTRY = "Australia";

    @Override
    public AddressDTO convert(final AddressModel address) {
        Assert.notNull(address, "Address cannot be null");
        final AddressDTO addressDTO = new AddressDTO();
        return convert(address, addressDTO);
    }

    @Override
    public AddressDTO convert(final AddressModel address, final AddressDTO addressDTO) {
        Assert.notNull(address, "Address cannot be null");
        Assert.notNull(addressDTO, "Address DTO cannot be null");
        addressDTO.setAddressLine1(address.getStreetname());

        //Hybris populates StreetNumber with overflow from StreetName. To avoid data loss have to set address line 2 with StreetNumber
        if (address.getStreetnumber() != null) {
            addressDTO.setAddressLine2(address.getStreetnumber());
        }
        else {
            addressDTO.setAddressLine2(StringUtils.EMPTY);
        }

        addressDTO.setSuburb(address.getTown());
        addressDTO.setState(address.getDistrict());

        if (address.getPostalcode() != null) {
            addressDTO.setPostcode(address.getPostalcode());
        }

        if (address.getCountry() != null) {
            addressDTO.setCountry(address.getCountry().getName());
        }
        else {
            addressDTO.setCountry(DEFAULT_COUNTRY);
        }

        return addressDTO;
    }

}

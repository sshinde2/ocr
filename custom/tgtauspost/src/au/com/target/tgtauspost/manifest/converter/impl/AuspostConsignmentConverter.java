/**
 * 
 */
package au.com.target.tgtauspost.manifest.converter.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtauspost.integration.dto.ConsignmentDetailsDTO;
import au.com.target.tgtauspost.integration.dto.DeliveryAddressDTO;
import au.com.target.tgtauspost.integration.dto.ParcelDetailsDTO;
import au.com.target.tgtcore.converters.TargetConverter;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.model.ConsignmentParcelModel;


/**
 * @author jjayawa1
 *
 */
public class AuspostConsignmentConverter implements
        TargetConverter<TargetConsignmentModel, ConsignmentDetailsDTO> {

    private AuspostDeliveryAddressConverter auspostDeliveryAddressConverter;
    private AuspostParcelConverter auspostParcelConverter;

    @Override
    public ConsignmentDetailsDTO convert(final TargetConsignmentModel consignment) {
        Assert.notNull(consignment, "Consignment cannot be null");

        final ConsignmentDetailsDTO consignmentDetailsDTO = new ConsignmentDetailsDTO();
        consignmentDetailsDTO.setTrackingId(consignment.getTrackingID());
        final DeliveryAddressDTO deliveryAddressDTO = auspostDeliveryAddressConverter.convert(
                consignment);
        consignmentDetailsDTO.setDeliveryAddress(deliveryAddressDTO);

        final List<ParcelDetailsDTO> parcelDTOs = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(consignment.getParcelsDetails())) {
            for (final ConsignmentParcelModel parcel : consignment.getParcelsDetails()) {
                parcelDTOs.add(auspostParcelConverter.convert(parcel));
            }
        }

        consignmentDetailsDTO.setParcels(parcelDTOs);
        return consignmentDetailsDTO;
    }

    /**
     * @param auspostDeliveryAddressConverter
     *            the auspostDeliveryAddressConverter to set
     */
    @Required
    public void setAuspostDeliveryAddressConverter(
            final AuspostDeliveryAddressConverter auspostDeliveryAddressConverter) {
        this.auspostDeliveryAddressConverter = auspostDeliveryAddressConverter;
    }

    /**
     * @param auspostParcelConverter
     *            the auspostParcelConverter to set
     */
    @Required
    public void setAuspostParcelConverter(final AuspostParcelConverter auspostParcelConverter) {
        this.auspostParcelConverter = auspostParcelConverter;
    }


}

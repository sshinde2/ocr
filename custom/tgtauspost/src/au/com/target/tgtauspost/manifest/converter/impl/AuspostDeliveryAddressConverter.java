/**
 * 
 */
package au.com.target.tgtauspost.manifest.converter.impl;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtauspost.helper.ConsignmentDeliveryAddressNamer;
import au.com.target.tgtauspost.integration.dto.DeliveryAddressDTO;
import au.com.target.tgtcore.converters.TargetConverter;
import au.com.target.tgtcore.model.TargetConsignmentModel;


/**
 * @author jjayawa1
 *
 */
public class AuspostDeliveryAddressConverter implements
        TargetConverter<TargetConsignmentModel, DeliveryAddressDTO> {
    private ConsignmentDeliveryAddressNamer consignmentDeliveryAddressNamer;
    private AuspostAddressConverter auspostAddressConverter;

    @Override
    public DeliveryAddressDTO convert(final TargetConsignmentModel consignment) {
        Assert.notNull(consignment, "Consignment cannot be null");

        final DeliveryAddressDTO deliveryAddressDTO = (DeliveryAddressDTO)auspostAddressConverter.convert(
                consignment.getShippingAddress(), new DeliveryAddressDTO());
        deliveryAddressDTO.setName(consignmentDeliveryAddressNamer.getDeliveryName(consignment));
        deliveryAddressDTO.setCompanyName(consignmentDeliveryAddressNamer.getCompanyName(consignment));

        return deliveryAddressDTO;
    }

    /**
     * @param consignmentDeliveryAddressNamer
     *            the consignmentDeliveryAddressNamer to set
     */
    @Required
    public void setConsignmentDeliveryAddressNamer(
            final ConsignmentDeliveryAddressNamer consignmentDeliveryAddressNamer) {
        this.consignmentDeliveryAddressNamer = consignmentDeliveryAddressNamer;
    }

    /**
     * @param auspostAddressConverter
     *            the auspostAddressConverter to set
     */
    @Required
    public void setAuspostAddressConverter(final AuspostAddressConverter auspostAddressConverter) {
        this.auspostAddressConverter = auspostAddressConverter;
    }

}

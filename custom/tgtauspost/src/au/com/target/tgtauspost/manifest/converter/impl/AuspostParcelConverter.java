/**
 * 
 */
package au.com.target.tgtauspost.manifest.converter.impl;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import org.springframework.util.Assert;

import au.com.target.tgtauspost.integration.dto.ParcelDetailsDTO;
import au.com.target.tgtcore.converters.TargetConverter;
import au.com.target.tgtfulfilment.dto.ConsignmentParcelDTO;
import au.com.target.tgtfulfilment.model.ConsignmentParcelModel;


/**
 * @author rsamuel3
 *
 */
public class AuspostParcelConverter implements
        TargetConverter<ConsignmentParcelModel, ParcelDetailsDTO> {

    @Override
    public ParcelDetailsDTO convert(final ConsignmentParcelModel parcel) throws ConversionException {
        Assert.notNull(parcel, "Parcels cannot be null");

        final DecimalFormat format = new DecimalFormat("#.##");
        format.setRoundingMode(RoundingMode.HALF_UP);

        final ParcelDetailsDTO parcelRequestDto = new ParcelDetailsDTO();
        final ConsignmentParcelDTO consignmentParcel = new ConsignmentParcelDTO();
        final Double height = parcel.getHeight();
        if (height != null) {
            consignmentParcel.setHeight(String.valueOf(height.intValue()));
        }
        final Double length = parcel.getLength();
        if (length != null) {
            consignmentParcel.setLength(String.valueOf(length.intValue()));
        }
        final Double width = parcel.getWidth();
        if (width != null) {
            consignmentParcel.setWidth(String.valueOf(width.intValue()));
        }
        final Double actualWeight = parcel.getActualWeight();
        if (actualWeight != null) {
            consignmentParcel.setWeight(format.format(actualWeight));
        }
        parcelRequestDto.setParcelDetails(consignmentParcel);

        return parcelRequestDto;
    }

}

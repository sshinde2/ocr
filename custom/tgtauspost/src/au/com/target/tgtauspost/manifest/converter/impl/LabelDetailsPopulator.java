/**
 * 
 */
package au.com.target.tgtauspost.manifest.converter.impl;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtauspost.integration.dto.AuspostRequestDTO;
import au.com.target.tgtauspost.integration.dto.LabelDTO;
import au.com.target.tgtauspost.integration.dto.StoreDTO;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.integration.dto.LabelLayout;


/**
 * Populator for LabelDetails.
 * 
 * @author jjayawa1
 *
 */
public class LabelDetailsPopulator {
    private StoreDetailsConverter storeDetailsConverter;
    private AuspostAddressConverter auspostAddressConverter;
    private AuspostConsignmentConverter auspostConsignmentConverter;

    public AuspostRequestDTO populateLabelDetails(final TargetPointOfServiceModel pos,
            final TargetConsignmentModel consignment, final LabelLayout layout, final boolean branding) {
        Assert.notNull(pos, "Point of Service cannot be null");
        Assert.notNull(consignment, "Consignment cannot be null");
        Assert.notNull(layout, "LabelLayout cannot be null");

        final AuspostRequestDTO request = new AuspostRequestDTO();
        final StoreDTO storeDTO = storeDetailsConverter.convert(pos);
        storeDTO.setAddress(auspostAddressConverter.convert(pos.getAddress()));
        storeDTO.setLabel(getLabelData(layout, branding));
        storeDTO.setConsignment(auspostConsignmentConverter.convert(consignment));
        request.setStore(storeDTO);
        return request;
    }

    /**
     * @param storeDetailsConverter
     *            the storeDetailsConverter to set
     */
    @Required
    public void setStoreDetailsConverter(final StoreDetailsConverter storeDetailsConverter) {
        this.storeDetailsConverter = storeDetailsConverter;
    }

    /**
     * @param auspostConsignmentConverter
     *            the auspostConsignmentConverter to set
     */
    @Required
    public void setAuspostConsignmentConverter(final AuspostConsignmentConverter auspostConsignmentConverter) {
        this.auspostConsignmentConverter = auspostConsignmentConverter;
    }

    /**
     * @param auspostAddressConverter
     *            the auspostAddressConverter to set
     */
    @Required
    public void setAuspostAddressConverter(final AuspostAddressConverter auspostAddressConverter) {
        this.auspostAddressConverter = auspostAddressConverter;
    }

    private LabelDTO getLabelData(final LabelLayout layout, final boolean branding) {
        final LabelDTO labelDTO = new LabelDTO();

        if (layout.getCode() != null) {
            labelDTO.setLayout(layout.getCode());
        }
        else {
            labelDTO.setLayout(LabelLayout.getDefaultLayout().getCode());
        }
        labelDTO.setBranding(Boolean.toString(branding));
        return labelDTO;
    }
}
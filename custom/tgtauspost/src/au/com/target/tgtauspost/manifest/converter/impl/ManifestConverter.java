/**
 * 
 */
package au.com.target.tgtauspost.manifest.converter.impl;

import de.hybris.platform.core.model.user.AddressModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtauspost.integration.dto.ConsignmentDTO;
import au.com.target.tgtauspost.integration.dto.ConsignmentDetailsDTO;
import au.com.target.tgtauspost.integration.dto.DeliveryAddressDTO;
import au.com.target.tgtauspost.integration.dto.TargetManifestDTO;
import au.com.target.tgtcore.converters.TargetConverter;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.model.TargetManifestModel;
import au.com.target.tgtutility.util.TargetDateUtil;


/**
 * @author jjayawa1
 *
 */
public class ManifestConverter implements TargetConverter<TargetManifestModel, TargetManifestDTO> {

    private static final Logger LOG = Logger.getLogger(ManifestConverter.class);

    protected static final String YYYY_MM_DD = "yyyyMMdd";

    private AuspostConsignmentConverter auspostConsignmentConverter;

    @Override
    public TargetManifestDTO convert(final TargetManifestModel manifest) {
        Assert.notNull(manifest, "Manifest cannot be null");
        Assert.notNull(manifest.getDate(), "Transaction date for manifest cannot be null");
        final TargetManifestDTO manifestDTO = new TargetManifestDTO();
        manifestDTO.setTransactionDateTime(TargetDateUtil.getDateAsString(manifest.getDate(), YYYY_MM_DD));
        manifestDTO.setManifestNumber(manifest.getCode());

        final List<ConsignmentDTO> consignments = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(manifest.getConsignments())) {
            for (final TargetConsignmentModel consignment : manifest.getConsignments()) {
                final ConsignmentDetailsDTO consignmentDetails = auspostConsignmentConverter.convert(consignment);
                // Cannot do it in consignment coverter as it is commonly used for manifest and label generator
                // and this data is only required for manifest generation
                final DeliveryAddressDTO deliveryAddressDTO = consignmentDetails.getDeliveryAddress();
                populateAuspostNotificationDetails(consignment, deliveryAddressDTO);
                final ConsignmentDTO consignmentDTO = new ConsignmentDTO();
                consignmentDTO.setConsignment(consignmentDetails);
                consignments.add(consignmentDTO);
            }
        }

        manifestDTO.setConsignments(consignments);
        return manifestDTO;
    }

    /**
     * @param consignment
     * @param deliveryAddressDTO
     */
    private void populateAuspostNotificationDetails(final TargetConsignmentModel consignment,
            final DeliveryAddressDTO deliveryAddressDTO) {
        LOG.info("populateAuspostNotificationDetails :: orderNumber=" + consignment.getOrder().getCode() +
                ", consignmentNumber=" + consignment.getCode());

        // don't send the notification details for inter-store delivery
        if (!OfcOrderType.INTERSTORE_DELIVERY.equals(consignment.getOfcOrderType())) {
            LOG.info("populateAuspostNotificationDetails :: Setting sendNotification to 'Y' for consignmentCode="
                    + consignment.getCode());
            deliveryAddressDTO.setSendNotifications("Y");
            final TargetCustomerModel user = (TargetCustomerModel)consignment.getOrder().getUser();
            deliveryAddressDTO.setEmail(user.getContactEmail());

            final AddressModel shippingAddress = consignment.getShippingAddress();
            deliveryAddressDTO.setPhone(shippingAddress.getPhone1());
        }
        else {
            // for interstore delivery
            LOG.info("populateAuspostNotificationDetails :: Setting sendNotification to 'N' for consignmentCode="
                    + consignment.getCode());
            deliveryAddressDTO.setSendNotifications("N");
        }
    }

    /**
     * @param auspostConsignmentConverter
     *            the auspostConsignmentConverter to set
     */
    @Required
    public void setAuspostConsignmentConverter(final AuspostConsignmentConverter auspostConsignmentConverter) {
        this.auspostConsignmentConverter = auspostConsignmentConverter;
    }

}

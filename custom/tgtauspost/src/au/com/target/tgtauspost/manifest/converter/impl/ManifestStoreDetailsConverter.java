/**
 * 
 */
package au.com.target.tgtauspost.manifest.converter.impl;

import org.springframework.util.Assert;

import au.com.target.tgtauspost.integration.dto.StoreDTO;
import au.com.target.tgtauspost.manifest.converter.AuspostPopulatingConverter;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;


/**
 * @author jjayawa1
 *
 */
public class ManifestStoreDetailsConverter extends StoreDetailsConverter implements
        AuspostPopulatingConverter<TargetPointOfServiceModel, StoreDTO> {

    @Override
    public StoreDTO convert(final TargetPointOfServiceModel pos, final StoreDTO storeDTO) {
        Assert.notNull(pos, "Target Point of Service cannot be null");
        Assert.notNull(storeDTO, "Store DTO cannot be null");
        Assert.notNull(pos.getFulfilmentCapability(), "StoreFulfilment Capabilities cannot be null");
        final StoreFulfilmentCapabilitiesModel storeFulfilmentCapabilities = pos.getFulfilmentCapability();
        storeDTO.setUsername(storeFulfilmentCapabilities.getUsername());
        return storeDTO;
    }

    @Override
    public StoreDTO convert(final TargetPointOfServiceModel pos) {
        Assert.notNull(pos, "Target Point of Service cannot be null");
        final StoreDTO storeDTO = super.convert(pos);
        return convert(pos, storeDTO);
    }

}

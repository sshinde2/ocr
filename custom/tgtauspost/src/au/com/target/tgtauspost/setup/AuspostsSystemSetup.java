/**
 * 
 */
package au.com.target.tgtauspost.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;

import java.util.ArrayList;
import java.util.List;

import au.com.target.tgtauspost.constants.TgtauspostConstants;


/**
 * @author rsamuel3
 *
 */
@SystemSetup(extension = TgtauspostConstants.EXTENSIONNAME)
public class AuspostsSystemSetup extends AbstractSystemSetup {

    /**
     * This method will be called during the system initialization.
     * 
     * @param context
     */
    @SystemSetup(type = Type.PROJECT, process = Process.ALL)
    public void createProjectData(final SystemSetupContext context) {
        importImpexFile(context, "/tgtauspost/import/auspost-chargezone-initialData.impex");
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.setup.AbstractSystemSetup#getInitializationOptions()
     */
    @Override
    public List<SystemSetupParameter> getInitializationOptions() {
        final List<SystemSetupParameter> params = new ArrayList<>();
        // add stuff here if you need params
        return params;
    }

}

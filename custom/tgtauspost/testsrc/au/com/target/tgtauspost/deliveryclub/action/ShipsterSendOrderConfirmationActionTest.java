package au.com.target.tgtauspost.deliveryclub.action;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtauspost.deliveryclub.service.ShipsterClientService;
import au.com.target.tgtauspost.integration.dto.ShipsterConfirmOrderResponseDTO;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ShipsterSendOrderConfirmationActionTest {
    @Mock
    private ShipsterClientService shipsterClientService;
    @InjectMocks
    private final ShipsterSendOrderConfirmationAction shipsterSendOrderConfirmationAction = new ShipsterSendOrderConfirmationAction();

    @Mock
    private OrderProcessModel process;

    @Mock
    private ShipsterConfirmOrderResponseDTO responseDto;

    @Mock
    private OrderModel order;

    @Before
    public void setUp() {
        given(process.getOrder()).willReturn(order);
        given(shipsterClientService.confirmOrder(order)).willReturn(responseDto);
    }

    @Test(expected = NullPointerException.class)
    public void testExecuteInternalWithNoProcessFound() throws RetryLaterException, Exception {
        shipsterSendOrderConfirmationAction.executeInternal(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteInternalWithNoOrderFound() throws RetryLaterException, Exception {
        given(process.getOrder()).willReturn(null);
        shipsterSendOrderConfirmationAction.executeInternal(process);
    }

    @Test
    public void testExecuteInternalWithResponseDtoAsNull() throws RetryLaterException, Exception {
        given(shipsterClientService.confirmOrder(order)).willReturn(null);
        assertThat(shipsterSendOrderConfirmationAction.executeInternal(process)).isEqualTo("OK");
    }

    @Test
    public void testExecuteInternalWithResponseNotNullAndResponseIsSubmittedAsTrue() throws RetryLaterException,
            Exception {
        given(Boolean.valueOf(responseDto.isSubmitted())).willReturn(Boolean.TRUE);
        assertThat(shipsterSendOrderConfirmationAction.executeInternal(process)).isEqualTo("OK");
    }

    @Test(expected = RetryLaterException.class)
    public void testExecuteInternalWithResponseNotNullAndResponseIsSubmittedAsFalse() throws RetryLaterException,
            Exception {
        given(Boolean.valueOf(responseDto.isSubmitted())).willReturn(Boolean.FALSE);
        given(Boolean.valueOf(responseDto.isRetry())).willReturn(Boolean.TRUE);
        shipsterSendOrderConfirmationAction.executeInternal(process);
    }

}

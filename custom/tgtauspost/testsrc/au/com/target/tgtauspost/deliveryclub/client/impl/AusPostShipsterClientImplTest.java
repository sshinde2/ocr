package au.com.target.tgtauspost.deliveryclub.client.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.springframework.test.web.client.match.RequestMatchers.method;
import static org.springframework.test.web.client.match.RequestMatchers.requestTo;
import static org.springframework.test.web.client.response.ResponseCreators.withBadRequest;
import static org.springframework.test.web.client.response.ResponseCreators.withServerError;
import static org.springframework.test.web.client.response.ResponseCreators.withStatus;
import static org.springframework.test.web.client.response.ResponseCreators.withSuccess;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.ResponseCreator;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtauspost.deliveryclub.dto.ShipsterVerifyEmailResponseDTO;
import au.com.target.tgtauspost.exception.AuspostShipsterClientException;
import au.com.target.tgtauspost.integration.dto.ShipsterConfirmOrderRequestDTO;
import au.com.target.tgtauspost.integration.dto.ShipsterConfirmOrderResponseDTO;
import au.com.target.tgtauspost.integration.dto.ShipsterDeliveryAddressDTO;


/**
 * 
 * @author bpottass
 *
 */
@UnitTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:tgtauspost-spring-test.xml")
public class AusPostShipsterClientImplTest {

    private final String verifyEmailResponseJson = "{\"verified\" : \"true\" }";

    private final String confirmOrderResponseJson = "{ \"submitted\": \"true\","
            + "\"transactionId\": \"44edc4d4defb25eb8e064fc936163cf880e9c945\" }";

    private MockRestServiceServer mockServer;

    private final String verifyEmailUrl = "v2/verify/";

    private final String deliveryClubBaseUrl = "https://digitalapi-stest.npe.auspost.com.au/delivery-club/";

    private final String confirmOrderUrl = "v2/confirm";

    private String verifyEmailFullyQualifiedUrl;

    private String confirmOrderFullyQualifiedUrl;

    private String hashedEmail;

    @Autowired
    private AusPostShipsterClientImpl ausPostShipsterClientImpl;

    @Autowired
    private RestTemplate deliveryClubRestTemplate;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockServer = MockRestServiceServer.createServer(deliveryClubRestTemplate);
        hashedEmail = "a6ad00ac113a19d953efb91820d8788e2263b28a";
        verifyEmailFullyQualifiedUrl = deliveryClubBaseUrl + verifyEmailUrl + hashedEmail;
        confirmOrderFullyQualifiedUrl = deliveryClubBaseUrl + confirmOrderUrl;
    }

    /**
     * 
     */
    private ShipsterConfirmOrderRequestDTO createConfirmOrderRequestDTO() {
        final ShipsterConfirmOrderRequestDTO request = new ShipsterConfirmOrderRequestDTO();

        request.setEmail(hashedEmail);
        request.setOrderReference("1234");
        request.setOrderTotal(Double.valueOf(50.00));
        request.setShippingFee(Double.valueOf(10.00));
        request.setShippingFeePaid(Double.valueOf(0.00));
        request.setShippingMethod("standard");

        final ShipsterDeliveryAddressDTO deliveryAddress = new ShipsterDeliveryAddressDTO();
        deliveryAddress.setCountry("AU");
        deliveryAddress.setFirstName("Jack");
        deliveryAddress.setLastName("Rabbit");
        deliveryAddress.setLine1("Delivery St");
        deliveryAddress.setLine2("");
        deliveryAddress.setPostcode("Geelong");
        deliveryAddress.setState("VIC");
        deliveryAddress.setSuburb("3220");

        request.setDeliveryAddress(deliveryAddress);
        return request;
    }

    @Test(expected = AuspostShipsterClientException.class)
    public void testVerifyEmailWithWrongFormatResponse() throws AuspostShipsterClientException {
        mockServer.expect(requestTo(verifyEmailFullyQualifiedUrl))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("json", MediaType.APPLICATION_JSON));
        ausPostShipsterClientImpl.verify(hashedEmail);
    }

    @Test(expected = AuspostShipsterClientException.class)
    public void testVerifyEmailWithConnectionError() throws AuspostShipsterClientException {
        mockServer.expect(requestTo(verifyEmailFullyQualifiedUrl))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withServerError());
        ausPostShipsterClientImpl.verify(hashedEmail);
    }

    @Test
    public void testVerifyEmail() throws AuspostShipsterClientException {
        mockServer.expect(requestTo(verifyEmailFullyQualifiedUrl))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(verifyEmailResponseJson, MediaType.APPLICATION_JSON));
        final ShipsterVerifyEmailResponseDTO response = ausPostShipsterClientImpl.verify(hashedEmail);
        assertThat(response.isVerified()).isTrue();
    }


    @Test(expected = AuspostShipsterClientException.class)
    public void testVerifyEmailWithBadRequest() throws AuspostShipsterClientException {
        mockServer.expect(requestTo(verifyEmailFullyQualifiedUrl))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withBadRequest());
        ausPostShipsterClientImpl.verify(hashedEmail);
    }

    @Test
    public void testConfirmOrder() throws AuspostShipsterClientException {
        mockServer.expect(requestTo(confirmOrderFullyQualifiedUrl))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(confirmOrderResponseJson, MediaType.APPLICATION_JSON));
        final ShipsterConfirmOrderResponseDTO response = ausPostShipsterClientImpl
                .confirmOrder(createConfirmOrderRequestDTO());
        assertThat(response.isSubmitted()).isTrue();
        assertThat(response.getTransactionId()).isNotEmpty();
    }

    @Test
    public void testConfirmOrderServerError() throws AuspostShipsterClientException {
        mockServer.expect(requestTo(confirmOrderFullyQualifiedUrl))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withServerError());
        final ShipsterConfirmOrderResponseDTO response = ausPostShipsterClientImpl
                .confirmOrder(createConfirmOrderRequestDTO());
        assertThat(response.isSubmitted()).isFalse();
        assertThat(response.isRetry()).isTrue();
    }

    public void testConfirmOrderWithBadRequest() throws AuspostShipsterClientException {
        mockServer.expect(requestTo(confirmOrderFullyQualifiedUrl))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withBadRequest());
        final ShipsterConfirmOrderResponseDTO response = ausPostShipsterClientImpl
                .confirmOrder(createConfirmOrderRequestDTO());
        assertThat(response.isSubmitted()).isFalse();
        assertThat(response.isRetry()).isFalse();
    }


    @Test
    public void testConfirmOrderTimeout() throws AuspostShipsterClientException {
        mockServer.expect(requestTo(confirmOrderFullyQualifiedUrl))
                .andExpect(method(HttpMethod.POST))
                .andRespond(new TimeoutResponseCreator());
        final ShipsterConfirmOrderResponseDTO response = ausPostShipsterClientImpl
                .confirmOrder(createConfirmOrderRequestDTO());
        assertThat(response.isSubmitted()).isFalse();
        assertThat(response.isRetry()).isTrue();
    }


    @Test
    public void testConfirmOrderTooManyRequests() throws AuspostShipsterClientException {
        mockServer.expect(requestTo(confirmOrderFullyQualifiedUrl))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.TOO_MANY_REQUESTS));
        final ShipsterConfirmOrderResponseDTO response = ausPostShipsterClientImpl
                .confirmOrder(createConfirmOrderRequestDTO());
        assertThat(response.isSubmitted()).isFalse();
        assertThat(response.isRetry()).isTrue();
    }


    @Test
    public void testConfirmOrderServiceUnavailable() throws AuspostShipsterClientException {
        mockServer.expect(requestTo(confirmOrderFullyQualifiedUrl))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.SERVICE_UNAVAILABLE));
        final ShipsterConfirmOrderResponseDTO response = ausPostShipsterClientImpl
                .confirmOrder(createConfirmOrderRequestDTO());
        assertThat(response.isSubmitted()).isFalse();
        assertThat(response.isRetry()).isTrue();
    }



    private class TimeoutResponseCreator implements ResponseCreator {

        @Override
        public ClientHttpResponse createResponse(final ClientHttpRequest clientHttpRequest) throws IOException {
            throw new RestClientException("Testing timeout exception");
        }

    }






}

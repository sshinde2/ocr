package au.com.target.tgtauspost.deliveryclub.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtauspost.deliveryclub.client.AusPostShipsterClient;
import au.com.target.tgtauspost.deliveryclub.dto.ShipsterVerifyEmailResponseDTO;
import au.com.target.tgtauspost.exception.AuspostShipsterClientException;
import au.com.target.tgtauspost.integration.dto.ShipsterConfirmOrderRequestDTO;
import au.com.target.tgtauspost.model.ShipsterConfigModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.shipster.ShipsterConfigService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ShipsterClientServiceImplTest {

    @InjectMocks
    private final ShipsterClientServiceImpl shipsterClientServiceImpl = new ShipsterClientServiceImpl();

    @Mock
    private AusPostShipsterClient ausPostShipsterClient;
    @Mock
    private ShipsterConfigService shipsterConfigService;

    @Mock
    private ShipsterVerifyEmailResponseDTO shipsterVerifyEmailResponseDTO;

    @Mock
    private OrderModel orderModel;

    @Mock
    private ShipsterConfigModel shipsterConfigModel;

    @Mock
    private ShipsterConfirmOrderRequestDTO request;

    @Mock
    private TargetZoneDeliveryModeModel deliveryModeModel;

    @Mock
    private TargetCustomerModel userModel;

    @Mock
    private AddressModel addressModel;

    @Mock
    private CountryModel country;


    @Before
    public void setUp() {

        shipsterVerifyEmailResponseDTO.setVerified(true);
        given(orderModel.getAusPostClubMember()).willReturn(Boolean.TRUE);
        given(orderModel.getDeliveryMode()).willReturn(deliveryModeModel);
        given(shipsterConfigService.getShipsterModelForDeliveryModes(deliveryModeModel))
                .willReturn(shipsterConfigModel);
        given(shipsterConfigModel.getActive()).willReturn(Boolean.TRUE);
    }

    @Test
    public void testVerifyEmailValidEmailAndVerifiedTrue() throws AuspostShipsterClientException {
        final String email = "test@test.com";
        given(ausPostShipsterClient.verify(anyString()))
                .willReturn(shipsterVerifyEmailResponseDTO);
        given(Boolean.valueOf(shipsterVerifyEmailResponseDTO.isVerified())).willReturn(Boolean.TRUE);
        final boolean verifyEmail = shipsterClientServiceImpl.verifyEmail(email);
        assertThat(verifyEmail).isTrue();
    }

    @Test(expected = AuspostShipsterClientException.class)
    public void testVerifyEmailWillThrowException() throws AuspostShipsterClientException {
        final String email = "test@test.com";
        given(ausPostShipsterClient.verify(anyString())).willThrow(new AuspostShipsterClientException("test"));
        shipsterClientServiceImpl.verifyEmail(email);
    }

    @Test
    public void testConfirmOrderForNotShipsterMember() throws AuspostShipsterClientException {
        given(orderModel.getAusPostClubMember()).willReturn(Boolean.FALSE);
        shipsterClientServiceImpl.confirmOrder(orderModel);
        verify(ausPostShipsterClient, never()).confirmOrder(request);
    }

    @Test
    public void testConfirmOrderForNotActiveDeliveryMode() throws AuspostShipsterClientException {
        given(shipsterConfigModel.getActive()).willReturn(Boolean.FALSE);
        shipsterClientServiceImpl.confirmOrder(orderModel);
        verify(ausPostShipsterClient, never()).confirmOrder(request);
    }

    @Test
    public void testConfirmOrderWhenShipsterConfigModelIsNull() throws AuspostShipsterClientException {
        given(shipsterConfigService.getShipsterModelForDeliveryModes(deliveryModeModel))
                .willReturn(null);
        shipsterClientServiceImpl.confirmOrder(orderModel);
        verify(ausPostShipsterClient, never()).confirmOrder(request);
    }

    @Test
    public void testConfirmOrderShipsterMemberForActiveDeliveryMode() throws AuspostShipsterClientException {
        shipsterClientServiceImpl.confirmOrder(orderModel);
        verify(ausPostShipsterClient).confirmOrder(any(ShipsterConfirmOrderRequestDTO.class));
    }

    @Test
    public void testCreateShipsterConfirmationOrderRequestDTOTargetRegisteredUser()
            throws AuspostShipsterClientException {
        given(orderModel.getCode()).willReturn("1234");
        given(orderModel.getUser()).willReturn(userModel);
        given(userModel.getContactEmail()).willReturn("test@test.com");

        mockData();
        request = shipsterClientServiceImpl.createShipsterConfirmationOrderRequestDTO(orderModel);
        verifyResponse();

    }

    @Test
    public void testCreateShipsterConfirmationOrderRequestDTOGuestUser() throws AuspostShipsterClientException {
        given(orderModel.getCode()).willReturn("1234");
        given(orderModel.getUser()).willReturn(userModel);
        given(userModel.getContactEmail()).willReturn("test@test.com");
        given(userModel.getType()).willReturn(CustomerType.GUEST);

        mockData();
        request = shipsterClientServiceImpl.createShipsterConfirmationOrderRequestDTO(orderModel);
        verifyResponse();

    }

    /**
     * 
     */
    private void verifyResponse() {
        assertThat(request.getOrderReference()).isEqualTo("1234");
        assertThat(request.getOrderTotal()).isEqualTo(Double.valueOf(50.00));
        assertThat(request.getShippingFee()).isEqualTo(Double.valueOf(10.00));
        assertThat(request.getShippingFeePaid()).isEqualTo(Double.valueOf(0.00));
        assertThat(request.getShippingMethod()).isEqualTo("standard");
        assertThat(request.getEmail()).isEqualTo("a6ad00ac113a19d953efb91820d8788e2263b28a");
        assertThat(request.getDeliveryAddress().getCountry()).isEqualTo("AU");
        assertThat(request.getDeliveryAddress().getFirstName()).isEqualTo("Jack");
        assertThat(request.getDeliveryAddress().getLastName()).isEqualTo("Rabbit");
        assertThat(request.getDeliveryAddress().getLine1()).isEqualTo("Delivery St");
        assertThat(request.getDeliveryAddress().getLine2()).isEqualTo("");
        assertThat(request.getDeliveryAddress().getPostcode()).isEqualTo("3220");
        assertThat(request.getDeliveryAddress().getState()).isEqualTo("VIC");
        assertThat(request.getDeliveryAddress().getSuburb()).isEqualTo("Geelong");
    }

    /**
     * 
     */
    private void mockData() {
        given(orderModel.getTotalPrice()).willReturn(Double.valueOf(50.00));
        given(orderModel.getOriginalDeliveryCost()).willReturn(Double.valueOf(10.00));
        given(orderModel.getDeliveryCost()).willReturn(Double.valueOf(0.00));

        given(shipsterConfigModel.getShipsterShippingMethod()).willReturn("standard");

        given(orderModel.getDeliveryAddress()).willReturn(addressModel);
        given(addressModel.getFirstname()).willReturn("Jack");
        given(addressModel.getLastname()).willReturn("Rabbit");
        given(addressModel.getLine1()).willReturn("Delivery St");
        given(addressModel.getLine2()).willReturn("");
        given(addressModel.getTown()).willReturn("Geelong");
        given(addressModel.getDistrict()).willReturn("VIC");
        given(addressModel.getPostalcode()).willReturn("3220");

        given(addressModel.getCountry()).willReturn(country);
        given(country.getIsocode()).willReturn("AU");
    }





}


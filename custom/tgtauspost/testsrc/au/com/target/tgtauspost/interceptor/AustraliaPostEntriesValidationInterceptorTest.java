/**
 * 
 */
package au.com.target.tgtauspost.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.Collections;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;


/**
 * @author smishra1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AustraliaPostEntriesValidationInterceptorTest {

    private static final String INTER_STORE_DELIVERY_ALLOWED = "Inter Store Delivery Enabled Store";
    private static final String HOME_DELIVERY_ENABLED_STORE = "Customer Delivery Enabled Store";
    private static final String HOME_DELIVERY_CODE = "home-delivery";
    private static final String EXPRESS_DELIVER_CODE = "express-delivery";

    @InjectMocks
    private final AustraliaPostEntriesValidationInterceptor interceptor = new AustraliaPostEntriesValidationInterceptor();

    @Mock
    private InterceptorContext ctx;

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @Mock
    private StoreFulfilmentCapabilitiesModel model;

    @Mock
    private TargetZoneDeliveryModeModel deliveryMode;

    /**
     * Method to test validation when start date is before end date
     */

    @Test
    public void testMerchantLocationIdValidationForInterStore() throws InterceptorException {
        Mockito.when(model.getAllowDeliveryToAnotherStore()).thenReturn(Boolean.valueOf(true));
        Mockito.when(model.getMerchantLocationId()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("Merchant Location ID (MLID) can not be null for : "
                        + INTER_STORE_DELIVERY_ALLOWED);
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testMerchantLocationIdValidationForHomeDelivery() throws InterceptorException {
        Mockito.when(model.getDeliveryModes()).thenReturn(Collections.singleton(deliveryMode));
        Mockito.when(deliveryMode.getCode()).thenReturn(HOME_DELIVERY_CODE);
        Mockito.when(model.getMerchantLocationId()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("Merchant Location ID (MLID) can not be null for : "
                        + HOME_DELIVERY_ENABLED_STORE);
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testMerchantLocationIdValidationForExpressDelivery() throws InterceptorException {
        Mockito.when(model.getDeliveryModes()).thenReturn(Collections.singleton(deliveryMode));
        Mockito.when(deliveryMode.getCode()).thenReturn(EXPRESS_DELIVER_CODE);
        Mockito.when(model.getMerchantLocationId()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("Merchant Location ID (MLID) can not be null for : "
                        + HOME_DELIVERY_ENABLED_STORE);
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testAllNullFeildsValidationForCNC() throws InterceptorException {
        Mockito.when(model.getDeliveryModes()).thenReturn(Collections.singleton(deliveryMode));
        Mockito.when(deliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.valueOf(true));
        Mockito.when(model.getMerchantLocationId()).thenReturn(null);
        Mockito.when(model.getChargeCode()).thenReturn(null);
        Mockito.when(model.getChargeAccountNumber()).thenReturn(null);
        Mockito.when(model.getUsername()).thenReturn(null);
        Mockito.when(model.getProductCode()).thenReturn(null);
        Mockito.when(model.getServiceCode()).thenReturn(null);

        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testChargeCodeValidationForInterStore() throws InterceptorException {
        Mockito.when(model.getAllowDeliveryToAnotherStore()).thenReturn(Boolean.valueOf(true));
        Mockito.when(model.getMerchantLocationId()).thenReturn("abc");
        Mockito.when(model.getChargeCode()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("Charge Code can not be null for : " + INTER_STORE_DELIVERY_ALLOWED);
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testChargeCodeValidationForHomeDelivery() throws InterceptorException {
        Mockito.when(model.getDeliveryModes()).thenReturn(Collections.singleton(deliveryMode));
        Mockito.when(deliveryMode.getCode()).thenReturn(HOME_DELIVERY_CODE);
        Mockito.when(model.getMerchantLocationId()).thenReturn("Melbourne");
        Mockito.when(model.getChargeCode()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("Charge Code can not be null for : " + HOME_DELIVERY_ENABLED_STORE);
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testChargeAccountNumberValidationForInterStore() throws InterceptorException {
        Mockito.when(model.getAllowDeliveryToAnotherStore()).thenReturn(Boolean.valueOf(true));
        Mockito.when(model.getMerchantLocationId()).thenReturn("Melbourne");
        Mockito.when(model.getChargeCode()).thenReturn("code-001");
        Mockito.when(model.getChargeAccountNumber()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("Charge Account Number can not be null for : "
                        + INTER_STORE_DELIVERY_ALLOWED);
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testChargeAccountNumberValidationForHomeDelivery() throws InterceptorException {
        Mockito.when(model.getDeliveryModes()).thenReturn(Collections.singleton(deliveryMode));
        Mockito.when(deliveryMode.getCode()).thenReturn(HOME_DELIVERY_CODE);
        Mockito.when(model.getMerchantLocationId()).thenReturn("Melbourne");
        Mockito.when(model.getChargeCode()).thenReturn("code-001");
        Mockito.when(model.getChargeAccountNumber()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("Charge Account Number can not be null for : " + HOME_DELIVERY_ENABLED_STORE);
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testUserNameValidationForInterStore() throws InterceptorException {
        Mockito.when(model.getAllowDeliveryToAnotherStore()).thenReturn(Boolean.valueOf(true));
        Mockito.when(model.getMerchantLocationId()).thenReturn("Melbourne");
        Mockito.when(model.getChargeCode()).thenReturn("code-001");
        Mockito.when(model.getChargeAccountNumber()).thenReturn(Integer.valueOf(001));
        Mockito.when(model.getUsername()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("User Name can not be null for : " + INTER_STORE_DELIVERY_ALLOWED);
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testUserNameValidationForHomeDelivery() throws InterceptorException {
        Mockito.when(model.getDeliveryModes()).thenReturn(Collections.singleton(deliveryMode));
        Mockito.when(deliveryMode.getCode()).thenReturn(HOME_DELIVERY_CODE);
        Mockito.when(model.getMerchantLocationId()).thenReturn("Melbourne");
        Mockito.when(model.getChargeCode()).thenReturn("code-001");
        Mockito.when(model.getChargeAccountNumber()).thenReturn(Integer.valueOf(001));
        Mockito.when(model.getUsername()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("User Name can not be null for : " + HOME_DELIVERY_ENABLED_STORE);
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testProductCodeValidationForInterStore() throws InterceptorException {
        Mockito.when(model.getAllowDeliveryToAnotherStore()).thenReturn(Boolean.valueOf(true));
        Mockito.when(deliveryMode.getCode()).thenReturn(HOME_DELIVERY_CODE);
        Mockito.when(model.getMerchantLocationId()).thenReturn("Melbourne");
        Mockito.when(model.getChargeCode()).thenReturn("code-001");
        Mockito.when(model.getChargeAccountNumber()).thenReturn(Integer.valueOf(001));
        Mockito.when(model.getUsername()).thenReturn("Shreeram001");
        Mockito.when(model.getProductCode()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("Product Code can not be null for : " + INTER_STORE_DELIVERY_ALLOWED);
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testProductCodeValidationForHomeDelivery() throws InterceptorException {
        Mockito.when(model.getDeliveryModes()).thenReturn(Collections.singleton(deliveryMode));
        Mockito.when(deliveryMode.getCode()).thenReturn(HOME_DELIVERY_CODE);
        Mockito.when(model.getMerchantLocationId()).thenReturn("Melbourne");
        Mockito.when(model.getChargeCode()).thenReturn("code-001");
        Mockito.when(model.getChargeAccountNumber()).thenReturn(Integer.valueOf(001));
        Mockito.when(model.getUsername()).thenReturn("Shreeram001");
        Mockito.when(model.getProductCode()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("Product Code can not be null for : " + HOME_DELIVERY_ENABLED_STORE);
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testServiceCodeValidationForInterStore() throws InterceptorException {
        Mockito.when(model.getAllowDeliveryToAnotherStore()).thenReturn(Boolean.valueOf(true));
        Mockito.when(model.getMerchantLocationId()).thenReturn("Melbourne");
        Mockito.when(model.getChargeCode()).thenReturn("code-001");
        Mockito.when(model.getChargeAccountNumber()).thenReturn(Integer.valueOf(001));
        Mockito.when(model.getUsername()).thenReturn("Shreeram001");
        Mockito.when(model.getProductCode()).thenReturn(Integer.valueOf(1));
        Mockito.when(model.getServiceCode()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("Service Code can not be null for : " + INTER_STORE_DELIVERY_ALLOWED);
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testServiceCodeValidationForHomeDelivery() throws InterceptorException {
        Mockito.when(model.getDeliveryModes()).thenReturn(Collections.singleton(deliveryMode));
        Mockito.when(deliveryMode.getCode()).thenReturn(HOME_DELIVERY_CODE);
        Mockito.when(model.getMerchantLocationId()).thenReturn("Melbourne");
        Mockito.when(model.getChargeCode()).thenReturn("code-001");
        Mockito.when(model.getChargeAccountNumber()).thenReturn(Integer.valueOf(001));
        Mockito.when(model.getUsername()).thenReturn("Shreeram001");
        Mockito.when(model.getProductCode()).thenReturn(Integer.valueOf(1));
        Mockito.when(model.getServiceCode()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("Service Code can not be null for : " + HOME_DELIVERY_ENABLED_STORE);
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testMerchantIdValidationForInterStore() throws InterceptorException {
        Mockito.when(model.getAllowDeliveryToAnotherStore()).thenReturn(Boolean.valueOf(true));
        Mockito.when(model.getMerchantLocationId()).thenReturn("Melbourne");
        Mockito.when(model.getChargeCode()).thenReturn("code-001");
        Mockito.when(model.getChargeAccountNumber()).thenReturn(Integer.valueOf(001));
        Mockito.when(model.getUsername()).thenReturn("Shreeram001");
        Mockito.when(model.getProductCode()).thenReturn(Integer.valueOf(1));
        Mockito.when(model.getServiceCode()).thenReturn(Integer.valueOf(2));
        Mockito.when(model.getMerchantId()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("Merchant ID can not be null for : " + INTER_STORE_DELIVERY_ALLOWED);
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testMerchantIdValidationForHomeDelivery() throws InterceptorException {
        Mockito.when(model.getDeliveryModes()).thenReturn(Collections.singleton(deliveryMode));
        Mockito.when(deliveryMode.getCode()).thenReturn(HOME_DELIVERY_CODE);
        Mockito.when(model.getMerchantLocationId()).thenReturn("Melbourne");
        Mockito.when(model.getChargeCode()).thenReturn("code-001");
        Mockito.when(model.getChargeAccountNumber()).thenReturn(Integer.valueOf(001));
        Mockito.when(model.getUsername()).thenReturn("Shreeram001");
        Mockito.when(model.getProductCode()).thenReturn(Integer.valueOf(1));
        Mockito.when(model.getServiceCode()).thenReturn(Integer.valueOf(2));
        Mockito.when(model.getMerchantId()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("Merchant ID can not be null for : " + HOME_DELIVERY_ENABLED_STORE);
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testLodgementFacilityValidationForInterStore() throws InterceptorException {
        Mockito.when(model.getAllowDeliveryToAnotherStore()).thenReturn(Boolean.valueOf(true));
        Mockito.when(model.getMerchantLocationId()).thenReturn("Melbourne");
        Mockito.when(model.getChargeCode()).thenReturn("code-001");
        Mockito.when(model.getChargeAccountNumber()).thenReturn(Integer.valueOf(001));
        Mockito.when(model.getUsername()).thenReturn("Shreeram001");
        Mockito.when(model.getProductCode()).thenReturn(Integer.valueOf(1));
        Mockito.when(model.getServiceCode()).thenReturn(Integer.valueOf(2));
        Mockito.when(model.getMerchantId()).thenReturn("123456789");
        Mockito.when(model.getLodgementFacility()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("Lodgement Facility can not be null for : " + INTER_STORE_DELIVERY_ALLOWED);
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testLodgementFacilityValidationForHomeDelivery() throws InterceptorException {
        Mockito.when(model.getDeliveryModes()).thenReturn(Collections.singleton(deliveryMode));
        Mockito.when(deliveryMode.getCode()).thenReturn(HOME_DELIVERY_CODE);
        Mockito.when(model.getMerchantLocationId()).thenReturn("Melbourne");
        Mockito.when(model.getChargeCode()).thenReturn("code-001");
        Mockito.when(model.getChargeAccountNumber()).thenReturn(Integer.valueOf(001));
        Mockito.when(model.getUsername()).thenReturn("Shreeram001");
        Mockito.when(model.getProductCode()).thenReturn(Integer.valueOf(1));
        Mockito.when(model.getServiceCode()).thenReturn(Integer.valueOf(2));
        Mockito.when(model.getMerchantId()).thenReturn("123456789");
        Mockito.when(model.getLodgementFacility()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("Lodgement Facility can not be null for : " + HOME_DELIVERY_ENABLED_STORE);
        interceptor.onValidate(model, ctx);
    }
}

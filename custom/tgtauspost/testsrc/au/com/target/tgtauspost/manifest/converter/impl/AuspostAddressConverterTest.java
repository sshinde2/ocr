package au.com.target.tgtauspost.manifest.converter.impl;

/**
 * 
 */

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.AddressModel;

import org.fest.assertions.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtauspost.integration.dto.AddressDTO;
import au.com.target.tgtauspost.integration.dto.DeliveryAddressDTO;


/**
 * @author sbryan6
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AuspostAddressConverterTest {

    private static final String POSTCODE = "3045";

    private static final String STATE = "VIC";

    private static final String SUBURB = "Geelong";

    private static final String STREET_NAME = "Success & Crt";

    private static final String STREET_NUM = "Unit 1 23";

    private static final String LAST_NAME = "Doe";

    private static final String FIRST_NAME = "Jane";

    private static final String COUNTRY = "Australia";

    private final AuspostAddressConverter converter = new AuspostAddressConverter();

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    //CHECKSTYLE:ON

    @Test
    public void testWithAddressModelNull() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Address cannot be null");
        converter.convert(null);
    }

    @Test
    public void testWithAddressModelNullWhenDeliveryAddressDTONotNull() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Address cannot be null");
        converter.convert(null, new DeliveryAddressDTO());
    }

    @Test
    public void testWithDeliveryAddressDTONull() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Address DTO cannot be null");
        converter.convert(new AddressModel(), null);
    }

    @Test
    public void testWithAddressValid() {
        final AddressDTO addressDto = converter.convert(AuspostConverterTestUtil.getAddressModel(
                FIRST_NAME, LAST_NAME, STREET_NAME, STREET_NUM, SUBURB, STATE, POSTCODE, COUNTRY));
        verifyAddressDetails(addressDto);
    }

    @Test
    public void testWithNullPostcode() {
        final AddressModel address = AuspostConverterTestUtil.getAddressModel(
                FIRST_NAME, LAST_NAME, STREET_NAME, STREET_NUM, SUBURB, STATE, null, COUNTRY);
        address.setPostalcode(null);
        final AddressDTO addressDto = converter.convert(address);
        Assertions.assertThat(addressDto.getPostcode()).isNull();
    }

    @Test
    public void testWithNullCountry() {
        final AddressModel address = AuspostConverterTestUtil.getAddressModel(
                FIRST_NAME, LAST_NAME, STREET_NAME, STREET_NUM, SUBURB, STATE, POSTCODE, null);
        final AddressDTO addressDto = converter.convert(address);
        Assertions.assertThat(addressDto.getCountry()).isNotNull();
        Assertions.assertThat(addressDto.getCountry()).isEqualTo(COUNTRY);
    }

    @Test
    public void testWithNullStreetNumber() {
        final AddressModel address = AuspostConverterTestUtil.getAddressModel(
                FIRST_NAME, LAST_NAME, STREET_NAME, null, SUBURB, STATE, POSTCODE, COUNTRY);
        final AddressDTO addressDto = converter.convert(address);
        Assertions.assertThat(addressDto.getAddressLine1()).isNotNull().isNotEmpty()
                .isEqualTo(STREET_NAME);
        Assertions.assertThat(addressDto.getAddressLine2()).isNotNull().isEmpty();
    }

    @Test
    public void testWithNullStreetName() {
        final AddressModel address = AuspostConverterTestUtil.getAddressModel(
                FIRST_NAME, LAST_NAME, null, STREET_NUM, SUBURB, STATE, POSTCODE, COUNTRY);
        final AddressDTO addressDto = converter.convert(address);
        Assertions.assertThat(addressDto.getAddressLine1()).isNull();
        Assertions.assertThat(addressDto.getAddressLine2()).isNotNull().isEqualTo(STREET_NUM);
    }

    /**
     * @param addressDto
     */
    private void verifyAddressDetails(final AddressDTO addressDto) {
        Assertions.assertThat(addressDto).isNotNull();

        // We add street name to address line 1
        Assertions.assertThat(addressDto.getAddressLine1()).isNotNull().isNotEmpty()
                .isEqualTo(STREET_NAME);

        // We add street number to address line 2
        Assertions.assertThat(addressDto.getAddressLine2()).isNotNull().isNotEmpty()
                .isEqualTo(STREET_NUM);

        Assertions.assertThat(addressDto.getCountry()).isNotNull().isNotEmpty().isEqualTo(COUNTRY);
        Assertions.assertThat(addressDto.getPostcode()).isNotNull().isEqualTo(POSTCODE);
        Assertions.assertThat(addressDto.getState()).isNotNull().isNotEmpty().isEqualTo(STATE);
        Assertions.assertThat(addressDto.getSuburb()).isNotNull().isNotEmpty().isEqualTo(SUBURB);
    }


}

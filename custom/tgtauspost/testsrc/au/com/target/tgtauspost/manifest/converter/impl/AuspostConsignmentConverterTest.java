package au.com.target.tgtauspost.manifest.converter.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

/**
 * 
 */

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtauspost.helper.ConsignmentDeliveryAddressNamer;
import au.com.target.tgtauspost.integration.dto.ConsignmentDetailsDTO;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.model.ConsignmentParcelModel;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AuspostConsignmentConverterTest {

    private static final String TRACKING_ID = "12345";

    private final AuspostDeliveryAddressConverter auspostDeliveryAddressConverter = new AuspostDeliveryAddressConverter();
    private final AuspostAddressConverter auspostAddressConverter = new AuspostAddressConverter();

    @Mock
    private AuspostParcelConverter parcelsConverter;

    @InjectMocks
    private final AuspostConsignmentConverter converter = new AuspostConsignmentConverter();

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private ConsignmentDeliveryAddressNamer consignmentDeliveryAddressNamer;

    @Mock
    private OrderModel order;

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    //CHECKSTYLE:ON

    @Before
    public void setup() {
        auspostDeliveryAddressConverter.setAuspostAddressConverter(auspostAddressConverter);
        converter.setAuspostDeliveryAddressConverter(auspostDeliveryAddressConverter);
        auspostDeliveryAddressConverter.setConsignmentDeliveryAddressNamer(consignmentDeliveryAddressNamer);
    }

    @Test
    public void testWithConsignmentModelNull() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Consignment cannot be null");
        converter.convert(null);
    }

    @Test
    public void testWithNullParcels() {
        given(consignment.getParcelsDetails()).willReturn(null);
        given(consignment.getShippingAddress()).willReturn(new AddressModel());

        final ConsignmentDetailsDTO consignmentDTO = converter.convert(consignment);
        assertThat(consignmentDTO.getParcels()).isEmpty();
    }

    @Test
    public void testWithEmptyParcelsList() {
        given(consignment.getParcelsDetails()).willReturn(new HashSet<ConsignmentParcelModel>());
        given(consignment.getShippingAddress()).willReturn(new AddressModel());

        final ConsignmentDetailsDTO consignmentDTO = converter.convert(consignment);
        assertThat(consignmentDTO.getParcels()).isEmpty();
    }

    @Test
    public void testWithNullShippingAddress() {
        given(consignment.getShippingAddress()).willReturn(null);
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Address cannot be null");
        converter.convert(consignment);
    }

    @Test
    public void testWithParcels() {
        final Set<ConsignmentParcelModel> parcels = new HashSet<>();
        final ConsignmentParcelModel parcel1 = AuspostConverterTestUtil.getConsignmentParcelModel("1.0", "1.0", "1.0",
                "1.0");
        final ConsignmentParcelModel parcel2 = AuspostConverterTestUtil.getConsignmentParcelModel("2.0", "2.0", "2.0",
                "2.0");
        parcels.add(parcel1);
        parcels.add(parcel2);
        given(consignment.getParcelsDetails()).willReturn(parcels);
        given(consignment.getShippingAddress()).willReturn(new AddressModel());
        given(consignment.getTrackingID()).willReturn(TRACKING_ID);

        final ConsignmentDetailsDTO consignmentDTO = converter.convert(consignment);
        verifyConsignmentDto(consignmentDTO);
        assertThat(consignmentDTO.getParcels()).isNotNull().isNotEmpty().hasSize(2);
        assertThat(consignmentDTO.getDeliveryAddress()).isNotNull();
    }

    /**
     * @param consignmentDto
     */
    private void verifyConsignmentDto(final ConsignmentDetailsDTO consignmentDto) {
        assertThat(consignmentDto).isNotNull();
        assertThat(consignmentDto.getTrackingId()).isNotEmpty().isEqualTo(TRACKING_ID);
        assertThat(consignmentDto.getDeliveryAddress()).isNotNull();
    }
}

/**
 * 
 */
package au.com.target.tgtauspost.manifest.converter.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.Date;

import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtfulfilment.model.ConsignmentParcelModel;
import au.com.target.tgtfulfilment.model.TargetManifestModel;


/**
 * @author jjayawa1
 *
 */
public final class AuspostConverterTestUtil {

    private AuspostConverterTestUtil() {
        // Empty Constructor
    }

    /**
     * @return AddressModel
     */
    public static AddressModel getAddressModel(final String firstname, final String lastname, final String streetname,
            final String streetnumber,
            final String suburb, final String state, final String postcode, final String country) {
        final AddressModel address = new AddressModel();
        address.setFirstname(firstname);
        address.setLastname(lastname);
        address.setStreetname(streetname);
        address.setStreetnumber(streetnumber);
        address.setTown(suburb);
        address.setDistrict(state);
        address.setPostalcode(postcode);
        if (country != null) {
            final CountryModel countryModel = Mockito.mock(CountryModel.class);
            BDDMockito.when(countryModel.getName()).thenReturn(country);
            address.setCountry(countryModel);
        }
        else {
            address.setCountry(null);
        }
        return address;
    }

    public static ConsignmentParcelModel getConsignmentParcelModel(final String weight, final String height,
            final String length, final String width) {
        final ConsignmentParcelModel parcel = new ConsignmentParcelModel();
        parcel.setActualWeight(Double.valueOf(weight));
        parcel.setHeight(Double.valueOf(height));
        parcel.setLength(Double.valueOf(length));
        parcel.setWidth(Double.valueOf(width));
        return parcel;
    }

    public static TargetManifestModel getManifestModel(final String manifestId,
            final Date creationDate) {
        final TargetManifestModel manifestModel = new TargetManifestModel();
        manifestModel.setCode(manifestId);
        manifestModel.setCreationtime(creationDate);
        manifestModel.setDate(creationDate);
        return manifestModel;
    }
}

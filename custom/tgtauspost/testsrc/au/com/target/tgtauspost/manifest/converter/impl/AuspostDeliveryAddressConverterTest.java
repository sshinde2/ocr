package au.com.target.tgtauspost.manifest.converter.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtauspost.helper.ConsignmentDeliveryAddressNamer;
import au.com.target.tgtauspost.integration.dto.DeliveryAddressDTO;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;


/**
 * @author sbryan6
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AuspostDeliveryAddressConverterTest {

    private static final String LAST_NAME = "Martha 'O";
    private static final String FIRST_NAME = "Sullivan!";
    private static final String COMPANY = "Target Australia";

    @Mock
    private CountryModel country;

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private AddressModel address;

    @Mock
    private ConsignmentDeliveryAddressNamer consignmentDeliveryAddressNamer;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @InjectMocks
    private final AuspostDeliveryAddressConverter converter = new AuspostDeliveryAddressConverter();

    private final AuspostAddressConverter auspostAddressConverter = new AuspostAddressConverter();

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() {
        converter.setAuspostAddressConverter(auspostAddressConverter);
    }

    @Test
    public void testWithConsignmentModelNull() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Consignment cannot be null");
        converter.convert(null);
    }

    @Test
    public void testWithConsignmentModelWithNoShippingAddress() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Address cannot be null");
        given(consignment.getShippingAddress()).willReturn(null);
        converter.convert(consignment);
    }

    @Test
    public void testWithConsignmentModelWhenDeliveryAndCompanyNameNull() {
        given(consignment.getShippingAddress()).willReturn(address);
        given(consignmentDeliveryAddressNamer.getDeliveryName(consignment)).willReturn(
                null);
        given(consignmentDeliveryAddressNamer.getCompanyName(consignment)).willReturn(null);
        final DeliveryAddressDTO deliveryAddress = converter.convert(consignment);
        assertThat(deliveryAddress.getName()).isNull();
        assertThat(deliveryAddress.getCompanyName()).isNull();
    }

    @Test
    public void testConsignmentModelWhenDeliveryAndCompanyNameNotNull() {
        given(consignment.getShippingAddress()).willReturn(address);
        given(consignmentDeliveryAddressNamer.getDeliveryName(consignment)).willReturn(
                FIRST_NAME + " " + LAST_NAME);
        given(consignmentDeliveryAddressNamer.getCompanyName(consignment)).willReturn(COMPANY);
        final DeliveryAddressDTO deliveryAddress = converter.convert(consignment);
        assertThat(deliveryAddress.getName()).isNotNull().isEqualTo(FIRST_NAME + " " + LAST_NAME);
        assertThat(deliveryAddress.getCompanyName()).isNotNull().isEqualTo(COMPANY);
    }

    @Test
    public void testAusPostSpecialCharsInNameDeliveryName() {
        given(consignment.getShippingAddress()).willReturn(address);
        given(consignmentDeliveryAddressNamer.getDeliveryName(consignment)).willReturn(
                FIRST_NAME + " " + LAST_NAME);
        given(consignmentDeliveryAddressNamer.getCompanyName(consignment)).willReturn(COMPANY);
        final DeliveryAddressDTO deliveryAddress = converter.convert(consignment);
        assertThat(deliveryAddress.getName()).isNotNull().isEqualTo(FIRST_NAME + " " + LAST_NAME);
        assertThat(deliveryAddress.getCompanyName()).isNotNull().isEqualTo(COMPANY);
    }

}

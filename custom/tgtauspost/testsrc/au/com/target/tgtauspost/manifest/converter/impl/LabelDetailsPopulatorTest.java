/**
 * 
 */
package au.com.target.tgtauspost.manifest.converter.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.AddressModel;

import org.fest.assertions.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtauspost.integration.dto.AddressDTO;
import au.com.target.tgtauspost.integration.dto.AuspostRequestDTO;
import au.com.target.tgtauspost.integration.dto.ConsignmentDetailsDTO;
import au.com.target.tgtauspost.integration.dto.StoreDTO;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.integration.dto.LabelLayout;


/**
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class LabelDetailsPopulatorTest {
    @Mock
    private TargetPointOfServiceModel pos;

    @Mock
    private TargetConsignmentModel consignment;

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException exception = ExpectedException.none();
    //CHECKSTYLE:ON

    @Mock
    private AuspostConsignmentConverter auspostConsignmentConverter;

    @Mock
    private AuspostAddressConverter auspostAddressConverter;

    @Mock
    private StoreDetailsConverter storeDetailsConverter;

    @InjectMocks
    private final LabelDetailsPopulator labelStoreDetailsConverter = new LabelDetailsPopulator();

    @Test
    public void testWithNullPOS() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Point of Service cannot be null");
        labelStoreDetailsConverter.populateLabelDetails(null, consignment, LabelLayout.getDefaultLayout(), false);
    }

    @Test
    public void testWithNullConsignment() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Consignment cannot be null");
        labelStoreDetailsConverter.populateLabelDetails(pos, null, LabelLayout.getDefaultLayout(), false);
    }

    @Test
    public void testWithNullLayout() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("LabelLayout cannot be null");
        labelStoreDetailsConverter.populateLabelDetails(pos, consignment, null, false);
    }

    @Test
    public void testWithAllValidData() {
        BDDMockito.when(storeDetailsConverter.convert(pos)).thenReturn(new StoreDTO());
        BDDMockito.when(auspostAddressConverter.convert(Mockito.any(AddressModel.class))).thenReturn(new AddressDTO());
        BDDMockito.when(auspostConsignmentConverter.convert(consignment)).thenReturn(new ConsignmentDetailsDTO());
        final AuspostRequestDTO request = labelStoreDetailsConverter.populateLabelDetails(pos, consignment,
                LabelLayout.getDefaultLayout(), false);
        Assertions.assertThat(request).isNotNull();
        Assertions.assertThat(request.getStore()).isNotNull();
        Assertions.assertThat(request.getStore().getAddress()).isNotNull();
        Assertions.assertThat(request.getStore().getLabel()).isNotNull();
        Assertions.assertThat(request.getStore().getManifest()).isNull();
        Assertions.assertThat(request.getStore().getConsignment()).isNotNull();
    }
}

package au.com.target.tgtauspost.manifest.converter.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

/**
 * 
 */

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtauspost.integration.dto.ConsignmentDetailsDTO;
import au.com.target.tgtauspost.integration.dto.DeliveryAddressDTO;
import au.com.target.tgtauspost.integration.dto.TargetManifestDTO;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.model.TargetManifestModel;
import au.com.target.tgtutility.util.TargetDateUtil;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ManifestConverterTest {

    private static final String MANIFEST_ID = "00001";

    private final Date currentDate = new Date();

    @Mock
    private AuspostConsignmentConverter auspostConsignmentConverter;

    @InjectMocks
    private final ManifestConverter converter = new ManifestConverter();

    @Mock
    private TargetManifestModel manifestModel;

    @Mock
    private TargetConsignmentModel consignment;

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Test
    public void testWithManifestModelNull() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Manifest cannot be null");
        converter.convert(null);
    }

    @Test
    public void testWithManifestModelNullConsignments() {
        given(manifestModel.getConsignments()).willReturn(null);
        given(manifestModel.getDate()).willReturn(new Date());
        final TargetManifestDTO manifestDTO = converter.convert(manifestModel);
        assertThat(manifestDTO.getConsignments()).isEmpty();
    }

    @Test
    public void testWithManifestModelEmptyConsignments() {
        given(manifestModel.getConsignments()).willReturn(new HashSet<TargetConsignmentModel>());
        given(manifestModel.getDate()).willReturn(new Date());
        final TargetManifestDTO manifestDTO = converter.convert(manifestModel);
        assertThat(manifestDTO.getConsignments()).isEmpty();
    }

    @Test
    public void testWithManifestModelNullDate() {
        given(manifestModel.getConsignments()).willReturn((new HashSet<TargetConsignmentModel>()));
        given(manifestModel.getDate()).willReturn(null);

        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Transaction date for manifest cannot be null");
        converter.convert(manifestModel);
    }

    @Test
    public void testWithManifestModelWithAllData() {
        final Set<TargetConsignmentModel> consignments = new HashSet<>();
        consignments.add(consignment);
        final TargetManifestModel manifest = AuspostConverterTestUtil.getManifestModel(MANIFEST_ID, currentDate);
        manifest.setConsignments(consignments);

        given(auspostConsignmentConverter.convert(Mockito.any(TargetConsignmentModel.class))).willReturn(
                new ConsignmentDetailsDTO());
        final ConsignmentDetailsDTO consignmentDTO = mock(ConsignmentDetailsDTO.class);
        given(auspostConsignmentConverter.convert(consignment)).willReturn(consignmentDTO);
        given(consignmentDTO.getDeliveryAddress()).willReturn(new DeliveryAddressDTO());

        final OrderModel order = mock(OrderModel.class);
        final TargetCustomerModel user = mock(TargetCustomerModel.class);
        final AddressModel shippingAddress = mock(AddressModel.class);

        given(order.getUser()).willReturn(user);
        final String email = "test@test.com";
        given(user.getContactEmail()).willReturn(email);
        given(consignment.getShippingAddress()).willReturn(shippingAddress);
        given(consignment.getOrder()).willReturn(order);

        final String phone = "0412345678";
        given(shippingAddress.getPhone1()).willReturn(phone);

        final TargetManifestDTO manifestDTO = converter.convert(manifest);
        assertThat(manifestDTO).isNotNull();
        assertThat(manifestDTO.getManifestNumber()).isNotNull().isEqualTo(MANIFEST_ID);
        assertThat(manifestDTO.getTransactionDateTime()).isNotNull()
                .isEqualTo(TargetDateUtil.getDateAsString(currentDate, ManifestConverter.YYYY_MM_DD));
        assertThat(manifestDTO.getConsignments()).isNotEmpty().hasSize(1);
    }

    @Test
    public void testManifestConvert() {

        final Set<TargetConsignmentModel> consignments = new HashSet<>();
        consignments.add(consignment);
        final TargetManifestModel manifest = AuspostConverterTestUtil.getManifestModel(MANIFEST_ID, currentDate);
        manifest.setConsignments(consignments);

        final ConsignmentDetailsDTO consignmentDTO = mock(ConsignmentDetailsDTO.class);
        given(auspostConsignmentConverter.convert(consignment)).willReturn(consignmentDTO);
        given(consignmentDTO.getDeliveryAddress()).willReturn(new DeliveryAddressDTO());

        final OrderModel order = mock(OrderModel.class);
        final TargetCustomerModel user = mock(TargetCustomerModel.class);
        final AddressModel shippingAddress = mock(AddressModel.class);

        given(order.getUser()).willReturn(user);
        final String email = "test@test.com";
        given(user.getContactEmail()).willReturn(email);
        given(consignment.getShippingAddress()).willReturn(shippingAddress);
        given(consignment.getOrder()).willReturn(order);

        final String phone = "0412345678";
        given(shippingAddress.getPhone1()).willReturn(phone);

        final TargetManifestDTO manifestDTO = converter.convert(manifest);
        final DeliveryAddressDTO deliveryAddress = manifestDTO.getConsignments().get(0).getConsignment()
                .getDeliveryAddress();

        assertThat(deliveryAddress.getPhone()).isEqualTo(phone);
        assertThat(deliveryAddress.getEmail()).isEqualTo(email);
        assertThat(deliveryAddress.getSendNotifications()).isEqualTo("Y");

    }

    @Test
    public void testManifestConvertAndInterStoreDelivery() {

        final Set<TargetConsignmentModel> consignments = new HashSet<>();
        consignments.add(consignment);
        final TargetManifestModel manifest = AuspostConverterTestUtil.getManifestModel(MANIFEST_ID, currentDate);
        manifest.setConsignments(consignments);

        final ConsignmentDetailsDTO consignmentDTO = mock(ConsignmentDetailsDTO.class);
        given(auspostConsignmentConverter.convert(consignment)).willReturn(consignmentDTO);
        given(consignmentDTO.getDeliveryAddress()).willReturn(new DeliveryAddressDTO());

        final OrderModel order = mock(OrderModel.class);
        final TargetCustomerModel user = mock(TargetCustomerModel.class);
        final AddressModel shippingAddress = mock(AddressModel.class);

        given(order.getUser()).willReturn(user);
        final String email = "test@test.com";
        given(user.getContactEmail()).willReturn(email);
        given(consignment.getShippingAddress()).willReturn(shippingAddress);
        given(consignment.getOrder()).willReturn(order);
        given(consignment.getOfcOrderType()).willReturn(OfcOrderType.INTERSTORE_DELIVERY);

        final String phone = "0412345678";
        given(shippingAddress.getPhone1()).willReturn(phone);

        final TargetManifestDTO manifestDTO = converter.convert(manifest);
        final DeliveryAddressDTO deliveryAddress = manifestDTO.getConsignments().get(0).getConsignment()
                .getDeliveryAddress();

        assertThat(deliveryAddress.getPhone()).isNull();
        assertThat(deliveryAddress.getEmail()).isNull();
        assertThat(deliveryAddress.getSendNotifications()).isEqualTo("N");

    }
}

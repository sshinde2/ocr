/**
 * 
 */
package au.com.target.tgtauspost.manifest.converter.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import org.fest.assertions.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;


/**
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StoreDetailsConverterTest {

    @Mock
    private TargetPointOfServiceModel pos;

    @Mock
    private StoreFulfilmentCapabilitiesModel fulfilmentCapalities;

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    //CHECKSTYLE:ON

    private final StoreDetailsConverter converter = new StoreDetailsConverter();

    @Test
    public void testWithNullPOS() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Target Point of Service cannot be null");
        converter.convert(null);
    }

    @Test
    public void testWithNullFulfilmentCapabilities() {
        BDDMockito.when(pos.getFulfilmentCapability()).thenReturn(null);
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("StoreFulfilmentCapabilities cannot be null");
        converter.convert(pos);
    }

    @Test
    public void testWithNullChargeAccountNumber() {
        BDDMockito.when(pos.getFulfilmentCapability()).thenReturn(fulfilmentCapalities);
        BDDMockito.when(fulfilmentCapalities.getChargeAccountNumber()).thenReturn(null);
        Assertions.assertThat(converter.convert(pos)).isNotNull();
        Assertions.assertThat(converter.convert(pos).getChargeToAccount()).isNull();
    }

    @Test
    public void testWithNullProductCode() {
        BDDMockito.when(pos.getFulfilmentCapability()).thenReturn(fulfilmentCapalities);
        BDDMockito.when(fulfilmentCapalities.getProductCode()).thenReturn(null);
        Assertions.assertThat(converter.convert(pos)).isNotNull();
        Assertions.assertThat(converter.convert(pos).getProductCode()).isNull();
    }

    @Test
    public void testWithNullServiceCode() {
        BDDMockito.when(pos.getFulfilmentCapability()).thenReturn(fulfilmentCapalities);
        BDDMockito.when(fulfilmentCapalities.getServiceCode()).thenReturn(null);
        Assertions.assertThat(converter.convert(pos)).isNotNull();
        Assertions.assertThat(converter.convert(pos).getServiceCode()).isNull();
    }

    @Test
    public void testWithSpecialCharactersInName() {
        when(pos.getFulfilmentCapability()).thenReturn(fulfilmentCapalities);
        when(pos.getName()).thenReturn("Thompson & Thompson");
        assertThat(converter.convert(pos)).isNotNull();
        assertThat(converter.convert(pos).getName()).isEqualTo("Thompson & Thompson");
    }

    @Test
    public void testWithAllValidData() {
        when(fulfilmentCapalities.getChargeAccountNumber()).thenReturn(Integer.valueOf(6616510));
        when(fulfilmentCapalities.getMerchantLocationId()).thenReturn("JDQ");
        when(fulfilmentCapalities.getProductCode()).thenReturn(Integer.valueOf(60));
        when(fulfilmentCapalities.getServiceCode()).thenReturn(Integer.valueOf(02));
        when(pos.getName()).thenReturn("Thompson & Thompson");
        when(pos.getFulfilmentCapability()).thenReturn(fulfilmentCapalities);
        assertThat(converter.convert(pos)).isNotNull();
        assertThat(converter.convert(pos).getName()).isEqualTo("Thompson & Thompson");
        assertThat(converter.convert(pos).getServiceCode()).isEqualTo("2");
        assertThat(converter.convert(pos).getProductCode()).isEqualTo("60");
        assertThat(converter.convert(pos).getMerchantLocationId()).isEqualTo("JDQ");
        assertThat(converter.convert(pos).getChargeToAccount()).isEqualTo("6616510");
    }
}

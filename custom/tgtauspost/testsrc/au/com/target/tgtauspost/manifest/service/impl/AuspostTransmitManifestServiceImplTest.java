/**
 * 
 */
package au.com.target.tgtauspost.manifest.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtauspost.integration.dto.AuspostRequestDTO;
import au.com.target.tgtauspost.manifest.client.TransmitManifestClient;
import au.com.target.tgtauspost.manifest.converter.impl.ManifestDetailsPopulator;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.dao.TargetManifestDao;
import au.com.target.tgtfulfilment.integration.dto.ManifestResponseDTO;
import au.com.target.tgtfulfilment.model.TargetManifestModel;
import au.com.target.tgtfulfilment.service.TargetManifestService;


/**
 * Unit tests for AuspostTransmitManifestServiceImpl.
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AuspostTransmitManifestServiceImplTest {

    @Mock
    private TransmitManifestClient transmitManifestClient;

    @Mock
    private TargetPointOfServiceModel targetPosModel;

    @Mock
    private TargetManifestModel manifestModel;

    @Mock
    private TargetConsignmentModel consignmentModel;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetManifestService targetManifestService;

    @Mock
    private WarehouseModel warehouseModel;

    @Mock
    private ManifestDetailsPopulator manifestDetailsPopulator;

    @Mock
    private TargetManifestDao targetManifestDao;

    @InjectMocks
    private final AuspostTransmitManifestServiceImpl auspostTransmitManifestService = new AuspostTransmitManifestServiceImpl();

    @Mock
    private AuspostRequestDTO requestDTO;

    private final ManifestResponseDTO responseDTO = new ManifestResponseDTO();

    @Before
    public void setup() {
        BDDMockito.given(manifestDetailsPopulator.populateManifestDetails(targetPosModel, manifestModel)).willReturn(
                requestDTO);
        BDDMockito.given(transmitManifestClient.transmitManifest(requestDTO)).willReturn(
                responseDTO);
    }

    @Test
    public void testTransmitManifestSuccess() {
        responseDTO.setSuccess(true);
        Assert.assertTrue(auspostTransmitManifestService.transmitManifest(targetPosModel, manifestModel).isSuccess());
        BDDMockito.verify(manifestDetailsPopulator, Mockito.times(1)).populateManifestDetails(
                Mockito.any(TargetPointOfServiceModel.class), Mockito.any(TargetManifestModel.class));
        BDDMockito.verify(transmitManifestClient, Mockito.times(1)).transmitManifest(
                Mockito.any(AuspostRequestDTO.class));
        BDDMockito.verifyNoMoreInteractions(transmitManifestClient);
        BDDMockito.verify(manifestModel, Mockito.times(1)).setSent(true);
        BDDMockito.verify(manifestModel, Mockito.times(1)).setDateTransmitted(Mockito.any(Date.class));
        BDDMockito.verify(modelService, Mockito.times(1)).save(manifestModel);
        BDDMockito.verify(modelService, Mockito.times(1)).refresh(manifestModel);
    }

    @Test
    public void testTransmitManifestFailure() {
        responseDTO.setSuccess(false);
        Assert.assertFalse(auspostTransmitManifestService.transmitManifest(targetPosModel, manifestModel).isSuccess());
        BDDMockito.verify(manifestDetailsPopulator, Mockito.times(1)).populateManifestDetails(
                Mockito.any(TargetPointOfServiceModel.class), Mockito.any(TargetManifestModel.class));
        BDDMockito.verify(transmitManifestClient, Mockito.times(1)).transmitManifest(
                Mockito.any(AuspostRequestDTO.class));
        BDDMockito.verifyNoMoreInteractions(transmitManifestClient);
        BDDMockito.verifyNoMoreInteractions(manifestModel);
        BDDMockito.verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testReTransmitManifestWithNoManifests() {
        BDDMockito.given(targetManifestDao.getNotTransmittedManifests()).willReturn(
                null);
        auspostTransmitManifestService.reTransmitManifests();
        BDDMockito.verify(manifestModel, Mockito.times(0)).getConsignments();
    }

    @Test
    public void testReTransmitManifestWithNoConsignments() {
        BDDMockito.given(targetManifestDao.getNotTransmittedManifests()).willReturn(
                Collections.singletonList(manifestModel));
        BDDMockito.given(manifestModel.getConsignments()).willReturn(
                null);
        auspostTransmitManifestService.reTransmitManifests();
        BDDMockito.verify(manifestModel, Mockito.times(1)).getConsignments();
    }

    @Test
    public void testReTransmitManifestWithConsignmentWithNoWarehouse() {
        BDDMockito.given(targetManifestDao.getNotTransmittedManifests()).willReturn(
                Collections.singletonList(manifestModel));
        BDDMockito.given(manifestModel.getConsignments()).willReturn(
                Collections.singleton(consignmentModel));
        BDDMockito.given(consignmentModel.getWarehouse()).willReturn(
                null);
        auspostTransmitManifestService.reTransmitManifests();
        BDDMockito.verify(manifestModel, Mockito.times(2)).getConsignments();
        BDDMockito.verify(consignmentModel, Mockito.times(1)).getWarehouse();
    }

    @Test
    public void testReTransmitManifestWithConsignmentWithWarehouseNoPOS() {
        BDDMockito.given(targetManifestDao.getNotTransmittedManifests()).willReturn(
                Collections.singletonList(manifestModel));
        BDDMockito.given(manifestModel.getConsignments()).willReturn(
                Collections.singleton(consignmentModel));
        BDDMockito.given(consignmentModel.getWarehouse()).willReturn(
                warehouseModel);
        auspostTransmitManifestService.reTransmitManifests();
        BDDMockito.verify(manifestModel, Mockito.times(2)).getConsignments();
        BDDMockito.verify(consignmentModel, Mockito.times(1)).getWarehouse();
        BDDMockito.verify(warehouseModel, Mockito.times(1)).getPointsOfService();
    }

    @Test
    public void testReTransmitManifestAndWebmethodsSuccess() {

        final AuspostTransmitManifestServiceImpl auspostTransmitManifestServiceSpy = Mockito
                .spy(auspostTransmitManifestService);

        BDDMockito.given(targetManifestDao.getNotTransmittedManifests()).willReturn(
                Collections.singletonList(manifestModel));
        BDDMockito.given(manifestModel.getConsignments()).willReturn(
                Collections.singleton(consignmentModel));
        BDDMockito.given(consignmentModel.getWarehouse()).willReturn(
                warehouseModel);
        BDDMockito.given(transmitManifestClient.transmitManifest(Mockito.any(AuspostRequestDTO.class))).willReturn(
                responseDTO);
        responseDTO.setSuccess(true);
        final Collection collection = new ArrayList();
        collection.add(targetPosModel);
        BDDMockito.given(warehouseModel.getPointsOfService()).willReturn(
                collection);
        auspostTransmitManifestServiceSpy.reTransmitManifests();
        BDDMockito.verify(manifestModel, Mockito.times(2)).getConsignments();
        BDDMockito.verify(consignmentModel, Mockito.times(1)).getWarehouse();
        BDDMockito.verify(warehouseModel, Mockito.times(2)).getPointsOfService();
        BDDMockito.verify(auspostTransmitManifestServiceSpy, Mockito.times(1)).transmitManifest(targetPosModel,
                manifestModel);

    }

}

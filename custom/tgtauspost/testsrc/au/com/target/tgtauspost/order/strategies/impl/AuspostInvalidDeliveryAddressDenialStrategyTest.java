/**
 * 
 */
package au.com.target.tgtauspost.order.strategies.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtauspost.model.AusPostChargeZoneModel;
import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.postcode.service.impl.TargetPostCodeServiceImpl;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AuspostInvalidDeliveryAddressDenialStrategyTest {

    @Mock
    private TargetPostCodeServiceImpl targetPostCodeService;

    @InjectMocks
    private final AuspostInvalidDeliveryAddressDenialStrategy invalidDeliveryAddressDenialStrategy = new AuspostInvalidDeliveryAddressDenialStrategy();

    @Mock
    private AddressModel addressModel;

    @Mock
    private TargetAddressModel targetAddressModel;

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private OEGParameterHelper oegParameterHelper;

    @Mock
    private PostCodeModel postcodeModel;

    @Mock
    private AusPostChargeZoneModel ausPostChargeZoneModel;

    @Test
    public void testWithTargetAddressModelPostcodeServiceValidPostCodeNoChargeZone() {
        when(targetAddressModel.getDistrict()).thenReturn("VIC");
        when(targetAddressModel.getPostalcode()).thenReturn("3000");
        when(targetPostCodeService.getPostCode(Mockito.anyString())).thenReturn(postcodeModel);
        when(postcodeModel.getAuspostChargeZone()).thenReturn(null);
        when(oegParameterHelper.getDeliveryAddress(oeg)).thenReturn(targetAddressModel);
        assertThat(invalidDeliveryAddressDenialStrategy.isDenied(oeg).isDenied()).isTrue();
    }

    @Test
    public void testWithTargetAddressModelPostcodeServiceValidPostCodeWithChargeZone() {
        when(targetAddressModel.getDistrict()).thenReturn("VIC");
        when(targetAddressModel.getPostalcode()).thenReturn("3000");
        when(targetPostCodeService.getPostCode(Mockito.anyString())).thenReturn(postcodeModel);
        when(postcodeModel.getAuspostChargeZone()).thenReturn(ausPostChargeZoneModel);
        when(oegParameterHelper.getDeliveryAddress(oeg)).thenReturn(targetAddressModel);
        assertThat(invalidDeliveryAddressDenialStrategy.isDenied(oeg).isDenied()).isFalse();
    }

    @Test
    public void testWithTargetAddressModelPostcodeServiceNullPostcodeModel() {
        when(targetAddressModel.getDistrict()).thenReturn("VIC");
        when(targetAddressModel.getPostalcode()).thenReturn("3000");
        when(targetPostCodeService.getPostCode(Mockito.anyString())).thenReturn(null);
        when(postcodeModel.getAuspostChargeZone()).thenReturn(null);
        when(oegParameterHelper.getDeliveryAddress(oeg)).thenReturn(targetAddressModel);
        assertThat(invalidDeliveryAddressDenialStrategy.isDenied(oeg).isDenied()).isTrue();
    }

}

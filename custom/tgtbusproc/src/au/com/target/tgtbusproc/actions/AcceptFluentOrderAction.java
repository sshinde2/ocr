package au.com.target.tgtbusproc.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import org.springframework.util.Assert;

import au.com.target.tgtbusproc.TargetBusinessProcessService;


/**
 * 
 * Action that will create and trigger
 * {@link au.com.target.tgtbusproc.constants.TgtbusprocConstants.BusinessProcess#ACCEPT_FLUENT_ORDER_PROCESS}
 * 
 */
public class AcceptFluentOrderAction extends AbstractProceduralAction<OrderProcessModel> {

    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process, "Orderprocess cannot be null");
        final OrderModel orderModel = process.getOrder();
        Assert.notNull(orderModel, "Ordermodel cannot be null");
        getTargetBusinessProcessService().startFluentOrderAcceptProcess(orderModel);
    }

    /**
     * Lookup for TargetBusinessProcessService
     * 
     * @return TargetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

}

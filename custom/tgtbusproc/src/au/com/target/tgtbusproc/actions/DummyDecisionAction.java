package au.com.target.tgtbusproc.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.Set;


public class DummyDecisionAction extends AbstractAction<OrderProcessModel> {

    private Set<String> expectedResponses;

    @Override
    public String execute(final OrderProcessModel process) throws RetryLaterException, Exception {
        return expectedResponses.iterator().next();
    }

    @Override
    public Set<String> getTransitions() {
        return expectedResponses;
    }

    /**
     * @param expectedResponses
     *            the expectedResponses to set
     */
    public void setExpectedResponses(final Set<String> expectedResponses) {
        this.expectedResponses = expectedResponses;
    }

}

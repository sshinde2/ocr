/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.util.Assert;


/**
 * @author jhermoso
 *
 */
public class EmailDecisionAction extends AbstractAction<OrderProcessModel> {
    public enum Transition {
        SEND_ORDER_REJECT, PREORDER_FULFILMENT_REJECT;

        public static Set<String> getStringValues() {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values()) {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    public Set<String> getTransitions() {
        return Transition.getStringValues();
    }

    @Override
    public String execute(final OrderProcessModel process) throws RetryLaterException, Exception {

        Assert.notNull(process, "Process cannot be null");
        if (process.getOrder().getNormalSaleStartDateTime() != null
                && BooleanUtils.isFalse(process.getOrder().getContainsPreOrderItems())) {
            //this will transition to sendDeclinedBalancePaymentEmail if the reject order is coming from pre-order fulfilment
            return Transition.PREORDER_FULFILMENT_REJECT.toString();
        }

        //this will transition to sendOrderRejectEmail if the reject order is coming from normal order and preorder
        return Transition.SEND_ORDER_REJECT.toString();
    }

}

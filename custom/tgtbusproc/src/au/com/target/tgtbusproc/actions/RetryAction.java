/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.processengine.model.ProcessTaskModel;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.task.RetryLaterException.Method;

import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.constants.TgtbusprocConstants;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;


/**
 * Base for RetryActions
 * 
 * @param <T>
 */
public abstract class RetryAction<T extends BusinessProcessModel> extends AbstractAction<T> {

    private static final Logger LOG = Logger.getLogger(RetryAction.class);

    private static final String ERR_RETRY_EXCEEDED = "ERROR_RETRYEXCEEDED: Retry exceeded for process={0} and processCode={1}";

    private static final String RETRY_EXCEEDED = "RETRYEXCEEDED";

    private OrderProcessParameterHelper orderProcessParameterHelper;


    /**
     * Execute the action body
     * 
     * @param process
     * @return status
     * @throws RetryLaterException
     * @throws Exception
     */
    protected abstract String executeInternal(final T process) throws RetryLaterException, Exception;

    /**
     * Get the available transitions
     * 
     * @return transitions
     */
    protected abstract Set<String> getTransitionsInternal();

    /**
     * Get the number of retries allowed
     * 
     * @return number
     */
    protected abstract int getAllowedRetries(T process);

    /**
     * Get the retry interval in milliseconds
     * 
     * @return number
     */
    protected abstract int getRetryInterval(T process);

    @Override
    public String execute(final T process) throws RetryLaterException, Exception {

        try {
            return executeInternal(process);
        }
        catch (final RetryLaterException rle) {
            final int retryCount = getRetryAttempts(process);

            if (retryCount >= getAllowedRetries(process)) {
                if (TgtbusprocConstants.RetryReason.RETRY_PAYMENT_FAILED.equals(rle.getMessage())) {
                    orderProcessParameterHelper.setPaymentFailedReason((OrderProcessModel)process, RETRY_EXCEEDED);
                }
                logRetryExceeded(process);
                return RETRY_EXCEEDED;
            }

            rle.setDelay(getRetryInterval(process));
            rle.setMethod(Method.LINEAR);
            throw rle;
        }
    }


    @Override
    public Set<String> getTransitions() {
        final Set<String> transitions = new HashSet<>();
        transitions.add(RETRY_EXCEEDED);
        transitions.addAll(getTransitionsInternal());
        return transitions;
    }

    private int getRetryAttempts(final T process) {
        if (process.getCurrentTasks() != null) {
            for (final ProcessTaskModel processTaskLog : process.getCurrentTasks()) {
                return processTaskLog.getRetry() == null ? 0 : processTaskLog.getRetry().intValue();
            }
        }
        return 0;
    }

    /**
     * Log retry exceeded information
     */
    protected void logRetryExceeded(final T process) {
        LOG.error(MessageFormat.format(ERR_RETRY_EXCEEDED, process.getProcessDefinitionName(), process.getCode()));
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }

    /**
     * @return the orderProcessParameterHelper
     */
    public OrderProcessParameterHelper getOrderProcessParameterHelper() {
        return orderProcessParameterHelper;
    }
}

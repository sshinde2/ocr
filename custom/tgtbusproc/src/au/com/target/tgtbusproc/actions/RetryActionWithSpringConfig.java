/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import de.hybris.platform.processengine.model.BusinessProcessModel;

import org.springframework.beans.factory.annotation.Required;


/**
 * Base for RetryActions with retries and interval set from spring config
 * 
 */
public abstract class RetryActionWithSpringConfig<T extends BusinessProcessModel> extends RetryAction<T> {


    private int allowedRetries;
    private int retryInterval;


    @Override
    protected int getAllowedRetries(final T process) {
        return allowedRetries;
    }

    @Override
    protected int getRetryInterval(final T process) {
        return retryInterval;
    }

    /**
     * @param allowedRetries
     *            the allowedRetries to set
     */
    @Required
    public void setAllowedRetries(final int allowedRetries) {
        this.allowedRetries = allowedRetries;
    }

    /**
     * @param retryInterval
     *            the retryInterval to set
     */
    @Required
    public void setRetryInterval(final int retryInterval) {
        this.retryInterval = retryInterval;
    }


}

/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;


/**
 * @author ragarwa3
 *
 */
public class TriggerPartnerUpdateProcessAction extends AbstractProceduralAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(TriggerPartnerUpdateProcessAction.class);

    private OrderProcessParameterHelper orderProcessParameterHelper;

    private String businessProcess;

    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process, "OrderProcessModel can not be null");
        Assert.notNull(process.getOrder(), "OrderModel can not be null");

        try {
            final ConsignmentModel consignmentModel = orderProcessParameterHelper.getConsignment(process);
            getTargetBusinessProcessService().startOrderProcessWithConsignment(process.getOrder(), consignmentModel,
                    businessProcess);
        }
        catch (final Exception e) {
            LOG.warn("Can not initiate business process. process=" + businessProcess, e);
        }
    }

    /**
     * Lookup for TargetBusinessProcessService
     * 
     * @return TargetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

    /**
     * @param businessProcess
     *            the businessProcess to set
     */
    @Required
    public void setBusinessProcess(final String businessProcess) {
        this.businessProcess = businessProcess;
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }



}

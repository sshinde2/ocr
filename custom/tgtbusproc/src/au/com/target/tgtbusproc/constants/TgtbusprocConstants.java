/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtbusproc.constants;

/**
 * Global class for all Tgtbusproc constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class TgtbusprocConstants extends GeneratedTgtbusprocConstants {
    public static final String EXTENSIONNAME = "tgtbusproc";

    private TgtbusprocConstants() {
        //empty to avoid instantiating this constant class
    }

    // implement here constants used by this extension

    public interface BusinessProcess {
        // order/consignment process
        String ACCEPT_FLUENT_ORDER_PROCESS = "acceptFluentOrderProcess";
        String FLUENT_ACCEPT_PREORDER_PROCESS = "fluentAcceptPreOrderProcess";
        String FLUENT_RELEASE_PREORDER_PROCESS = "fluentReleasePreOrderProcess";
        String FLUENT_PREORDER_RELEASE_ACCEPT_PROCESS = "fluentPreOrderReleaseAcceptProcess";
        String ACCEPT_ORDER_PROCESS = "acceptOrderProcess";
        String ACCEPT_PREORDER_PROCESS = "acceptPreOrderProcess";
        String CANCEL_ORDER_PROCESS = "orderCancelProcess";
        String CANCEL_ORDER_ITEMS_PROCESS = "orderItemsCancelProcess";
        String FLUENT_CANCEL_ORDER_ITEMS_PROCESS = "fluentOrderItemsCancelProcess";
        String CONSIGNMENT_PICKED_PROCESS = "consignmentPickedProcess";
        String CONSIGNMENT_INSTORE_PICKED_PROCESS = "consignmentInstorePickedProcess";
        String LAYBY_PAYMENT_PROCESS = "laybyPaymentProcess";
        String ORDER_COMPLETION_PROCESS = "orderCompletionProcess";
        String PAYMENT_FAILED_PROCESS = "paymentFailedProcess";
        String PLACE_ORDER_PROCESS = "placeOrderProcess";
        String PLACE_PREORDER_PROCESS = "placePreOrderProcess";
        String PLACE_FLUENT_ORDER_PROCESS = "placeFluentOrderProcess";
        String PLACE_FLUENT_PREORDER_PROCESS = "placeFluentPreOrderProcess";
        String POS_ADDRESS_CHANGE_PROCESS = "posAddressChangeProcess";
        String REFUND_ORDER_PROCESS = "orderRefundProcess";
        String REJECT_ORDER_PROCESS = "rejectOrderProcess";
        String REVIEW_ORDER_PROCESS = "reviewOrderProcess";
        String LAYBY_SECOND_PICK_INVOICED = "laybySecondPickInvoicedProcess";
        String RESEND_CONSIGNMENT_TO_WAREHOUSE = "resendConsignmentToWarehouseProcess";
        String RESEND_SHIPADVICE_TO_WAREHOUSE = "resendShipAdviceToWarehouseProcess";
        String REROUTE_CONSIGNMENT = "rerouteConsignmentProcess";
        String REVERSE_GIFTCARD_PROCESS = "reverseGiftCardProcess";

        // customer account process
        String CUSTOMER_REGISTRATION_PROCESS = "customerRegistrationProcess";
        String CUSTOMER_PASSWORD_CHANGE_PROCESS = "customerPasswordChangeProcess";
        String CUSTOMER_UPDATE_EMAIL_ADDR_PROCESS = "updateCustomerEmailAddrProcess";
        String FORGOTTEN_PASSWORD_PROCESS = "forgottenPasswordProcess";
        String RESEND_TAX_INVOICE_PROCESS = "resendTaxInvoiceProcess";
        String SEND_CLICK_AND_COLLECT_NOTIFICATION_PROCESS = "sendClickAndCollectNotificationProcess";
        String CUSTOMER_SUBSCRIPTION_PROCESS = "customerSubscriptionProcess";
        String CUSTOMER_NEWSLETTER_SUBSCRIPTION_PROCESS = "customerNewsletterSubscriptionProcess";
        String CUSTOMER_PERSONALDETAILS_SUBSCRIPTION_PROCESS = "customerPersonalDetailsSubscriptionProcess";
        String SEND_SMS_TO_STORE_FOR_OPEN_ORDERS = "sendSmsToStoreForOpenOrdersProcess";
        String SEND_CLICK_AND_COLLECT_PICKED_UP_NOTIFICATION_PROCESS = "sendClickAndCollectPickedupNotificationProcess";
        String SEND_CLICK_AND_COLLECT_RETURNED_TO_FLOOR_NOTIFICATION_PROCESS = "sendClickAndCollectReturnedToFloorNotificationProcess";

        // partner update processes
        String UPDATE_INVENTORY_TO_PARTNER_PROCESS = "updateInventoryToPartnerProcess";
        String UPDATE_SHIPMENT_TO_PARTNER_PROCESS = "updateShipmentToPartnerProcess";

        // send email with blackout 
        String SEND_EMAIL_WITH_BLACKOUT_PROCESS = "sendEmailWithBlackoutProcess";
        String FLUENT_CONSIGNMENT_PICKED_PROCESS = "fluentConsignmentPickedProcess";
        String FLUENT_CONSIGNMENT_SHIPPED_PROCESS = "fluentConsignmentShippedProcess";
        String FLUENT_ORDER_SHIPPED_PROCESS = "fluentOrderShippedProcess";
    }

    public interface RetryReason {
        String RETRY_PAYMENT_FAILED = "retryPaymentFailed";
    }
}

/**
 * 
 */
package au.com.target.tgtbusproc.email;

import de.hybris.platform.processengine.model.BusinessProcessModel;

import au.com.target.tgtbusproc.exceptions.BusinessProcessEmailException;


/**
 * Strategy allowing different ways of generating and sending emails
 * 
 */
public interface BusinessProcessEmailStrategy {

    /**
     * Send an email using the chosen strategy
     * 
     * @param process
     * @param frontendTemplateName
     * @throws BusinessProcessEmailException
     */
    void sendEmail(final BusinessProcessModel process, final String frontendTemplateName)
            throws BusinessProcessEmailException;

}

/**
 * 
 */
package au.com.target.tgtbusproc.email;

/**
 * @author Olivier Lamy
 */
public class EmailServiceException extends Exception {

    public EmailServiceException(final String message, final Throwable throwable) {
        super(message, throwable);
    }

}

/**
 * 
 */
package au.com.target.tgtbusproc.email;

import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;


/**
 * Service for sending a Hybris email message model
 */
public interface SendHybrisEmailService {

    /**
     * Send the given EmailMessageModel
     * 
     * @param email
     */
    void sendEmail(EmailMessageModel email) throws EmailServiceException;
}

/**
 * 
 */
package au.com.target.tgtbusproc.email.context;

import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.core.model.media.MediaModel;

import java.util.Collections;
import java.util.List;


/**
 * @author rmcalave
 * 
 */
public abstract class AbstractTargetEmailContext extends AbstractEmailContext {
    /**
     * Get the Target site URL that is used for display (from the config property tgtstorefront.website.display.url).
     * 
     * @return The display URL
     */
    public String getBaseUrlDisplay() {
        return getConfigurationService().getConfiguration().getString("tgtstorefront.website.display.url");
    }

    /**
     * Get the email address for Target (from the config property tgtstorefront.email.contact.emailaddress).
     * 
     * @return the contact email for Target
     */
    public String getTargetContactEmail() {
        return getConfigurationService().getConfiguration().getString("tgtstorefront.email.contact.emailaddress");
    }

    /**
     * Get the telephone number for Target (from the config property tgtstorefront.email.contact.number).
     * 
     * @return the contact number for Target
     */
    public String getTargetContactNumber() {
        return getConfigurationService().getConfiguration().getString("tgtstorefront.email.contact.number");
    }

    /**
     * Get the contact number for TradeMe (from the config property tgtstorefront.email.contact.number.trademe).
     * 
     * @return the contact number for TradeMe
     */
    public String getTradeMeCustomerNumber() {
        return getConfigurationService().getConfiguration().getString("tgtstorefront.email.contact.number.trademe");
    }

    /**
     * Get the Front end Base URL (from the config property email.target.http).
     * 
     * @return base url for Target front end
     */
    public String getEmailBaseUrl() {
        return getConfigurationService().getConfiguration().getString("email.target.http");
    }

    /**
     * Get the Tax Invoice
     * 
     * @return List<MediaModel>
     */
    public List<MediaModel> getAttachments() {
        return Collections.EMPTY_LIST;
    }
}

/**
 * 
 */
package au.com.target.tgtbusproc.email.data;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.returns.model.OrderReturnRecordEntryModel;


/**
 * Email context data objects for use when email is generated and sent externally.<br/>
 * For example code for flattening the data see OrderNotificationEmailContext.
 * 
 */
public class TargetEmailContext {

    private BaseSiteModel site;

    private CustomerModel customer;

    private OrderModel order;

    private OrderCancelRecordEntryModel cancelRequest;

    private OrderReturnRecordEntryModel returnRequest;

    // Tax invoice pdf
    private MediaModel taxInvoiceMedia;


    /**
     * @return the site
     */
    public BaseSiteModel getSite() {
        return site;
    }

    /**
     * @param site
     *            the site to set
     */
    public void setSite(final BaseSiteModel site) {
        this.site = site;
    }

    /**
     * @return the customer
     */
    public CustomerModel getCustomer() {
        return customer;
    }

    /**
     * @param customer
     *            the customer to set
     */
    public void setCustomer(final CustomerModel customer) {
        this.customer = customer;
    }

    /**
     * @return the order
     */
    public OrderModel getOrder() {
        return order;
    }

    /**
     * @param order
     *            the order to set
     */
    public void setOrder(final OrderModel order) {
        this.order = order;
    }

    /**
     * @return the cancelRequest
     */
    public OrderCancelRecordEntryModel getCancelRequest() {
        return cancelRequest;
    }

    /**
     * @param cancelRequest
     *            the cancelRequest to set
     */
    public void setCancelRequest(final OrderCancelRecordEntryModel cancelRequest) {
        this.cancelRequest = cancelRequest;
    }

    /**
     * @return the returnRequest
     */
    public OrderReturnRecordEntryModel getReturnRequest() {
        return returnRequest;
    }

    /**
     * @param returnRequest
     *            the returnRequest to set
     */
    public void setReturnRequest(final OrderReturnRecordEntryModel returnRequest) {
        this.returnRequest = returnRequest;
    }

    /**
     * @return the taxInvoiceMedia
     */
    public MediaModel getTaxInvoiceMedia() {
        return taxInvoiceMedia;
    }

    /**
     * @param taxInvoiceMedia
     *            the taxInvoiceMedia to set
     */
    public void setTaxInvoiceMedia(final MediaModel taxInvoiceMedia) {
        this.taxInvoiceMedia = taxInvoiceMedia;
    }

}

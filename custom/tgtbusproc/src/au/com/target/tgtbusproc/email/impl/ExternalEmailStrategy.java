/**
 * 
 */
package au.com.target.tgtbusproc.email.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.email.BusinessProcessEmailStrategy;
import au.com.target.tgtbusproc.email.EmailServiceException;
import au.com.target.tgtbusproc.email.SendExternalEmailService;
import au.com.target.tgtbusproc.email.data.TargetEmailContext;
import au.com.target.tgtbusproc.exceptions.BusinessProcessEmailException;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;


/**
 * Generate an email data context for sending via external email service
 * 
 */
public class ExternalEmailStrategy implements BusinessProcessEmailStrategy {

    private static final Logger LOG = Logger.getLogger(ExternalEmailStrategy.class);

    private SendExternalEmailService sendExternalEmailService;

    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Override
    public void sendEmail(final BusinessProcessModel process, final String frontendTemplateName)
            throws BusinessProcessEmailException {

        Assert.notNull(process, "Process cannot be null");

        final TargetEmailContext context = generateEmailContext(process);

        try {
            sendExternalEmailService.sendEmail(context, frontendTemplateName);
        }
        catch (final EmailServiceException e) {
            throw new BusinessProcessEmailException("Could not send email " + frontendTemplateName + " for process "
                    + process.getCode(), e);
        }
    }


    /**
     * Convert data from process to TargetEmailContext
     * 
     * @param process
     * @return TargetEmailContext
     */
    protected TargetEmailContext generateEmailContext(final BusinessProcessModel process) {

        Assert.notNull(process, "Process cannot be null");

        final TargetEmailContext context = new TargetEmailContext();

        // If we have an OrderProcessModel then populate the context with order related info
        if (process instanceof OrderProcessModel) {

            final OrderProcessModel orderProcess = (OrderProcessModel)process;
            final OrderModel order = orderProcess.getOrder();

            if (order != null) {

                context.setOrder(order);

                if (order.getSite() != null) {
                    context.setSite(order.getSite());
                }
                else {
                    LOG.warn("No Site found for order " + order.getCode() + ", process " + process.getCode());
                }

                if (order.getUser() != null) {
                    context.setCustomer((CustomerModel)order.getUser());
                }
                else {
                    LOG.warn("No Customer found for order " + order.getCode() + ", process " + process.getCode());
                }
            }
            else
            {
                LOG.warn("No order found in process " + process.getCode());
            }


            // The cancel request may be a populated object or null
            context.setCancelRequest(orderProcessParameterHelper.getOrderCancelRequest(orderProcess));

            // The return request may be a populated object or null
            context.setReturnRequest(orderProcessParameterHelper.getReturnRequest(orderProcess));


        }

        return context;
    }


    /**
     * @param sendExternalEmailService
     *            the sendExternalEmailService to set
     */
    @Required
    public void setSendExternalEmailService(final SendExternalEmailService sendExternalEmailService) {
        this.sendExternalEmailService = sendExternalEmailService;
    }


    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }


}

package au.com.target.tgtbusproc.email.impl;

import org.apache.log4j.Logger;

import au.com.target.tgtbusproc.email.EmailServiceException;
import au.com.target.tgtbusproc.email.SendExternalEmailService;
import au.com.target.tgtbusproc.email.data.TargetEmailContext;


/**
 * Mock for SendExternalEmailService. The real implementation should be in the tgtemail extension.
 * 
 */
public class SendExternalEmailServiceImpl implements SendExternalEmailService {

    private static final Logger LOG = Logger.getLogger(SendExternalEmailServiceImpl.class);

    @Override
    public void sendEmail(final TargetEmailContext context, final String template) throws EmailServiceException {

        LOG.info("SendExternalEmailService Mock: sending email for template " + template);
    }

}

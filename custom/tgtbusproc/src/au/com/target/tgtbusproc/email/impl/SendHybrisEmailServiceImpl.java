/**
 *
 */
package au.com.target.tgtbusproc.email.impl;

import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;

import org.apache.log4j.Logger;

import au.com.target.tgtbusproc.email.EmailServiceException;
import au.com.target.tgtbusproc.email.SendHybrisEmailService;


/**
 * Mock for SendHybrisEmailServiceImpl. The real implementation should be in the tgtjms extension.
 */
public class SendHybrisEmailServiceImpl implements SendHybrisEmailService {

    private static final Logger LOG = Logger.getLogger(SendHybrisEmailServiceImpl.class);

    @Override
    public void sendEmail(final EmailMessageModel email) throws EmailServiceException {

        LOG.info("SendHybrisEmailService Mock: sending email subject " + email.getSubject());

        LOG.info("SendHybrisEmailService Mock: sending email body " + email.getBody());


    }

}
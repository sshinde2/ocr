/**
 *
 */
package au.com.target.tgtbusproc.email.impl;

import de.hybris.platform.acceleratorservices.email.impl.DefaultEmailGenerationService;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailAttachmentModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaService;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.email.context.AbstractTargetEmailContext;


/**
 * Generate an email using Target Email Generation Service.
 *
 * Note: this is hard to integration test via yunit since it needs all the template content set up in tgtwebcore.<br/>
 * When testing via Groovy console, then for an unknown reason DefaultEmailContextFactory is unable to lookup the
 * context bean using getBeansOfType.<br/>
 * I have tested via creating and running a process in HMC under the 'Business Processes' section.
 */
public class TargetEmailGenerationService extends DefaultEmailGenerationService {
    private static final Logger LOG = Logger.getLogger(TargetEmailGenerationService.class);
    private MediaService mediaService;

    @Override
    protected EmailMessageModel createEmailMessage(final String emailSubject, final String emailBody,
            final AbstractEmailContext emailContext)
    {

        Assert.notNull(emailContext, "Email Context can't be null");

        final List<EmailAddressModel> toEmails = new ArrayList<>();
        final EmailAddressModel toAddress = getEmailService().getOrCreateEmailAddressForEmail(
                emailContext.getToEmail(),
                emailContext.getToDisplayName());
        toEmails.add(toAddress);
        final EmailAddressModel fromAddress = getEmailService().getOrCreateEmailAddressForEmail(
                emailContext.getFromEmail(),
                emailContext.getFromDisplayName());

        List<EmailAttachmentModel> attachments = null;
        if (emailContext instanceof AbstractTargetEmailContext) {
            final AbstractTargetEmailContext targetEmailContext = (AbstractTargetEmailContext)emailContext;
            final List<MediaModel> mediaModels = targetEmailContext.getAttachments();

            if (CollectionUtils.isNotEmpty(mediaModels)) {
                attachments = new ArrayList<>();
                for (final MediaModel mediaModel : mediaModels) {
                    try (final DataInputStream dis = new DataInputStream(mediaService.getStreamFromMedia(mediaModel));) {
                        final String formattedFileName = formatFileName(mediaModel.getRealFileName());
                        attachments.add(getEmailService().createEmailAttachment(dis,
                                formattedFileName,
                                mediaModel.getMime()));
                    }
                    catch (final IOException e) {
                        LOG.error("Failed while trying to add attachement media=" + mediaModel.getCode()
                                + " to email subject=\"" + emailSubject + "\"", e);
                    }
                }
            }
        }


        return getEmailService().createEmailMessage(toEmails, new ArrayList<EmailAddressModel>(),
                new ArrayList<EmailAddressModel>(), fromAddress, emailContext.getFromEmail(), emailSubject, emailBody,
                attachments);
    }

    /**
     * @param mediaService
     *            the mediaService to set
     */
    @Required
    public void setMediaService(final MediaService mediaService) {
        this.mediaService = mediaService;
    }

    protected String formatFileName(final String fileName) {
        String formattedFileName = "";
        if (StringUtils.isNotEmpty(fileName)) {
            final String[] fragments = fileName.split("\\.");
            final String formattedFragment = fragments[0] + "-" + System.currentTimeMillis();
            if (fragments.length > 1) {
                formattedFileName = formattedFragment + "." + fragments[1];
            }
            else {
                formattedFileName = formattedFragment;
            }
        }

        return formattedFileName;
    }
}

/**
 * 
 */
package au.com.target.tgtbusproc.email.impl;

import de.hybris.platform.acceleratorservices.email.impl.DefaultEmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailAttachmentModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.io.DataInputStream;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtutility.constants.TgtutilityConstants.ErrorCode;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * @author asingh78
 * 
 */
public class TargetEmailService extends DefaultEmailService {

    private static final Logger LOG = Logger.getLogger(TargetEmailService.class);

    private String defaultCatalogId;
    private String defaultCatalogVersion;

    private FlexibleSearchService flexibleSearchService;

    @Override
    public EmailAddressModel getOrCreateEmailAddressForEmail(final String emailAddress, final String displayName)
    {
        String actualDisplayName = displayName;
        if (null == actualDisplayName) {
            actualDisplayName = emailAddress;
        }
        return super.getOrCreateEmailAddressForEmail(emailAddress, actualDisplayName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EmailAttachmentModel createEmailAttachment(final DataInputStream masterDataStream, final String filename,
            final String mimeType)
    {
        final CatalogVersionModel catalogVersion =
                getCatalogVersionService().getCatalogVersion(defaultCatalogId, defaultCatalogVersion);

        final EmailAttachmentModel attachment = getModelService().create(EmailAttachmentModel.class);
        attachment.setCode(filename);
        attachment.setMime(mimeType);
        attachment.setRealFileName(filename);

        if (attachment.getCatalogVersion() == null) {

            attachment.setCatalogVersion(catalogVersion);
        }

        getModelService().save(attachment);

        getMediaService().setStreamForMedia(attachment, masterDataStream, filename, mimeType,
                getEmailAttachmentsMediaFolder());
        return attachment;
    }

    protected EmailAttachmentModel findExistingEmailAttachment(final String code,
            final CatalogVersionModel catalogVersion) {
        LOG.info(SplunkLogFormatter.formatMessage(
                "Finding email attachment with code : "
                        + code, ErrorCode.INFO_EMAIL_ATTACHMENT));
        final EmailAttachmentModel attachment = new EmailAttachmentModel();
        attachment.setCode(code);
        attachment.setCatalogVersion(catalogVersion);


        EmailAttachmentModel returnModel = null;
        try {
            returnModel = flexibleSearchService.getModelByExample(attachment);
        }
        catch (final Exception e) {
            LOG.info(e);
        }

        return returnModel;

    }

    /**
     * @param defaultCatalogId
     *            the defaultCatalogId to set
     */
    @Required
    public void setDefaultCatalogId(final String defaultCatalogId) {
        this.defaultCatalogId = defaultCatalogId;
    }

    /**
     * @param defaultCatalogVersion
     *            the defaultCatalogVersion to set
     */
    @Required
    public void setDefaultCatalogVersion(final String defaultCatalogVersion) {
        this.defaultCatalogVersion = defaultCatalogVersion;
    }

    /**
     * @param flexibleSearchService
     *            the flexibleSearchService to set
     */
    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

}

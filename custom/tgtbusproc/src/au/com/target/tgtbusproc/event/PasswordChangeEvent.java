package au.com.target.tgtbusproc.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;


/**
 * Password change event, implementation of {@link AbstractCommerceUserEvent}
 */
public class PasswordChangeEvent extends AbstractCommerceUserEvent<BaseSiteModel> {

    //Empty Class

}

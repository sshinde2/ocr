/**
 * 
 */
package au.com.target.tgtbusproc.event;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.events.SubmitOrderEvent;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;


public class TargetSubmitOrderEvent extends SubmitOrderEvent {


    private final PaymentTransactionEntryModel pte;

    private final boolean fluentOrder;

    /**
     * @param order
     */
    public TargetSubmitOrderEvent(final OrderModel order, final PaymentTransactionEntryModel pte,
            final boolean fluentOrder) {
        super(order);
        this.pte = pte;
        this.fluentOrder = fluentOrder;
    }

    /**
     * @return the pte
     */
    public PaymentTransactionEntryModel getPte() {
        return pte;
    }

    /**
     * @return the fluentOrder
     */
    public boolean isFluentOrder() {
        return fluentOrder;
    }
}

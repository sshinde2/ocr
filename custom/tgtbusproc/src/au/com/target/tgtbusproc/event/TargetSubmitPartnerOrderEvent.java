/**
 * 
 */
package au.com.target.tgtbusproc.event;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.events.SubmitOrderEvent;


/**
 * Event to trigger business processes after submitting an Partner order.
 * 
 * @author jjayawa1
 * 
 */
public class TargetSubmitPartnerOrderEvent extends SubmitOrderEvent {

    /**
     * @param order
     */
    public TargetSubmitPartnerOrderEvent(final OrderModel order) {
        super(order);
    }

}

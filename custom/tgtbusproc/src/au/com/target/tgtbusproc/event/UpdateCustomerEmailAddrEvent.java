/**
 * 
 */
package au.com.target.tgtbusproc.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;


/**
 * Event for update of customer email address, implementation of {@link AbstractCommerceUserEvent}
 * 
 */
public class UpdateCustomerEmailAddrEvent extends AbstractCommerceUserEvent<BaseSiteModel> {

    private String oldContactEmail;

    /**
     * @return the oldContactEmail
     */
    public String getOldContactEmail() {
        return oldContactEmail;
    }

    /**
     * @param oldContactEmail
     *            the oldContactEmail to set
     */
    public void setOldContactEmail(final String oldContactEmail) {
        this.oldContactEmail = oldContactEmail;
    }

}

package au.com.target.tgtbusproc.event.listeners;

import de.hybris.platform.commerceservices.event.ForgottenPwdEvent;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import au.com.target.tgtbusproc.TargetBusinessProcessService;


/**
 * Listener for "forgotten password" functionality event.
 */
public class ForgottenPasswordEventListener extends AbstractEventListener<ForgottenPwdEvent> {

    @Override
    protected void onEvent(final ForgottenPwdEvent event) {
        getTargetBusinessProcessService().startForgottenPasswordProcess(event.getCustomer(), event.getSite(),
                event.getToken());
    }

    public TargetBusinessProcessService getTargetBusinessProcessService()
    {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService.");
    }

}

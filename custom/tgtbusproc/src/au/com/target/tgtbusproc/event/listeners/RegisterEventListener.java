package au.com.target.tgtbusproc.event.listeners;

import de.hybris.platform.commerceservices.event.RegisterEvent;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import au.com.target.tgtbusproc.TargetBusinessProcessService;


/**
 * Listener for customer registration event and triggers the registration business process
 */
public class RegisterEventListener extends AbstractEventListener<RegisterEvent> {

    @Override
    protected void onEvent(final RegisterEvent event) {
        getTargetBusinessProcessService().startCustomerRegistrationProcess(event.getCustomer(), event.getSite());
    }

    public TargetBusinessProcessService getTargetBusinessProcessService()
    {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService.");
    }

}

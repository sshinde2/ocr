package au.com.target.tgtbusproc.event.listeners;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import org.apache.commons.lang.BooleanUtils;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.event.TargetSubmitOrderEvent;


/**
 * Listener for order submits.
 */
public class SubmitOrderEventListener extends AbstractEventListener<TargetSubmitOrderEvent> {

    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

    @Override
    protected void onEvent(final TargetSubmitOrderEvent event) {
        if (BooleanUtils.isTrue(event.getOrder().getContainsPreOrderItems())) {
            if (event.isFluentOrder()) {
                getTargetBusinessProcessService().startPlaceFluentPreOrderProcess(event.getOrder(), event.getPte());
            }
            else {
                getTargetBusinessProcessService().startPlacePreOrderProcess(event.getOrder(), event.getPte());
            }
        }
        else if (event.isFluentOrder()) {
            getTargetBusinessProcessService().startPlaceFluentOrderProcess(event.getOrder(), event.getPte());
        }
        else {
            getTargetBusinessProcessService().startPlaceOrderProcess(event.getOrder(), event.getPte());
        }
    }
}

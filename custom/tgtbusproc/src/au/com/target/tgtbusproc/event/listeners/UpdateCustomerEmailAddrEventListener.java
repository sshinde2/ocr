/**
 * 
 */
package au.com.target.tgtbusproc.event.listeners;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.event.UpdateCustomerEmailAddrEvent;


/**
 * Listener for customer update email address event and triggers the update email address business process
 * 
 */
public class UpdateCustomerEmailAddrEventListener extends AbstractEventListener<UpdateCustomerEmailAddrEvent> {

    public TargetBusinessProcessService getTargetBusinessProcessService()
    {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

    @Override
    protected void onEvent(final UpdateCustomerEmailAddrEvent event)
    {
        getTargetBusinessProcessService().startCustomerUpdateEmailAddrProcess(event.getCustomer(), event.getSite(),
                event.getOldContactEmail());
    }

}

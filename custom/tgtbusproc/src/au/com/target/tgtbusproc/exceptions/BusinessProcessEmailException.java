/**
 * 
 */
package au.com.target.tgtbusproc.exceptions;

/**
 * Thrown to indicate that a business exception occurred during generating or sending an email from a business process
 * 
 */
public class BusinessProcessEmailException extends Exception {
    /**
     * @param message
     */
    public BusinessProcessEmailException(final String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public BusinessProcessEmailException(final String message, final Throwable cause) {
        super(message, cause);
    }

}

/**
 * 
 */
package au.com.target.tgtbusproc.exceptions;



public class BusinessProcessException extends RuntimeException {

    private final String errorCode;


    /**
     * @param errorCode
     * @param errorMessage
     */
    public BusinessProcessException(final String errorCode, final String errorMessage, final Throwable throwable) {
        super(errorMessage, throwable);
        this.errorCode = errorCode;
    }

    /**
     * 
     * @param errorCode
     * @param errorMessage
     */
    public BusinessProcessException(final String errorCode, final String errorMessage) {
        super(errorMessage);
        this.errorCode = errorCode;
    }



    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

}

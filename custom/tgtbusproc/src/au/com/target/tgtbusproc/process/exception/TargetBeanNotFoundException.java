/**
 * 
 */
package au.com.target.tgtbusproc.process.exception;

/**
 * @author asingh78
 * 
 */
public class TargetBeanNotFoundException extends RuntimeException {


    /**
     * 
     * @param message 
     */
    public TargetBeanNotFoundException(final String message) {
        super(message);
    }

    /**
     * 
     * @param exception
     */
    public TargetBeanNotFoundException(final Throwable exception) {
        super(exception);
    }

    /**
     * 
     * @param message
     * @param exception
     */
    public TargetBeanNotFoundException(final String message, final Throwable exception) {
        super(message, exception);
    }

}

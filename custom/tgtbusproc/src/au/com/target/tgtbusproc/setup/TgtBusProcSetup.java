package au.com.target.tgtbusproc.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;

import java.util.List;

import au.com.target.tgtbusproc.constants.TgtbusprocConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 * 
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = TgtbusprocConstants.EXTENSIONNAME)
public class TgtBusProcSetup extends AbstractSystemSetup {

    @Override
    public List<SystemSetupParameter> getInitializationOptions() {
        return null;
    }

    /**
     * This method will be called during the system initialization.
     * 
     * @param context
     *            the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.PROJECT, process = Process.ALL)
    public void createProjectData(final SystemSetupContext context)
    {
        importImpexFile(context, "/tgtbusproc/import/fraudreviewgroup-users.impex");
        importImpexFile(context, "/tgtbusproc/import/fraudreviewgroup-access-rights.impex");
    }

}

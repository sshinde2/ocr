/**
 * 
 */
package au.com.target.tgtbusproc.util;

import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.processengine.helpers.impl.DefaultProcessParameterHelper;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;
import de.hybris.platform.returns.model.OrderReturnRecordEntryModel;

import java.math.BigDecimal;
import java.util.List;

import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntryList;


/**
 * Helper for retrieving data from the context object
 * 
 */
public class OrderProcessParameterHelper extends DefaultProcessParameterHelper {

    private enum ParameterName {
        ORDER_CANCEL_REQUEST, RETURN_REQUEST, REFUND_AMOUNT, TAX_INVOICE_MEDIA, PAYMENT_TRANSACTION_ENTRY, //
        CONSIGNMENT, PICK_TYPE, PAYMENT_FAILED_REASON, CSTICKET_MESSAGE, DO_CANCEL_EXTRACT, CANCEL_TYPE, //
        PICK_CONSIGNMENT_UPDATE_DATA, NEW_EMAIL_ADDRESS, CNC_NOTIFICATION_DATA, CUSTOMER_SUBSCRIPTION_DATA, //
        CONSIGNMENT_REJECT_REASON, SEND_SMS_TO_STORE_DATA, REFUNDED_AMOUNT_INFO, REFUND_PAYMENT_ENTRIES, //
        GIFTCARD_REVERSAL_DETAILS, GIFTCARD_REVERSAL_CAPTURE_ENTRY, REFUND_AMOUNT_REMAINING, PARTNER_UPDATE_REQUIRED, TARGET_ORDER_CANCEL_ENTRY_LIST;
    }

    public enum CancelTypeEnum {
        SHORT_PICK, ZERO_PICK, COCKPIT_CANCEL, COCKPIT_PARTIAL_CANCEL, LAYBY_CANCEL;
    }

    public enum PickTypeEnum {
        ZERO, SHORT, FULL;
    }


    public void setCSTicketMessage(final OrderProcessModel process, final String message) {
        setParameterForProcess(process, message, ParameterName.CSTICKET_MESSAGE.name());
    }

    public String getCSTicketMessage(final OrderProcessModel process) {
        final String message = (String)getParameterFromProcess(process,
                ParameterName.CSTICKET_MESSAGE.name());

        return message;
    }

    /**
     * Return the TargetOrderCancelEntryList if there is one for the process
     * 
     * @param process
     * @return TargetOrderCancelEntryList or null if there is not one
     */
    public TargetOrderCancelEntryList getTargetOrderCancelEntryList(final OrderProcessModel process) {

        final TargetOrderCancelEntryList request = (TargetOrderCancelEntryList)getParameterFromProcess(process,
                ParameterName.TARGET_ORDER_CANCEL_ENTRY_LIST.name());

        return request;
    }


    /**
     * Set the TargetOrderCancelEntryList in process params
     * 
     * @param process
     * @param targetOrderCancelEntryList
     */
    public void setTargetOrderCancelEntryList(final OrderProcessModel process,
            final TargetOrderCancelEntryList targetOrderCancelEntryList) {
        setParameterForProcess(process, targetOrderCancelEntryList,
                ParameterName.TARGET_ORDER_CANCEL_ENTRY_LIST.name());
    }

    /**
     * Return the OrderCancelRecordEntryModel if there is one for the process
     * 
     * @param process
     * @return OrderCancelRecordEntryModel or null if there is not one
     */
    public OrderCancelRecordEntryModel getOrderCancelRequest(final OrderProcessModel process) {

        final OrderCancelRecordEntryModel request = (OrderCancelRecordEntryModel)getParameterFromProcess(process,
                ParameterName.ORDER_CANCEL_REQUEST.name());

        return request;
    }


    /**
     * Set the OrderCancelRecordEntryModel in process params
     * 
     * @param process
     * @param orderCancelRequest
     */
    public void setOrderCancelRequest(final OrderProcessModel process,
            final OrderCancelRecordEntryModel orderCancelRequest) {

        // Convert the OrderCancelRequest to a serializable version
        setParameterForProcess(process, orderCancelRequest, ParameterName.ORDER_CANCEL_REQUEST.name());
    }


    /**
     * Return the ReturnRequestModel if there is one for the process
     * 
     * @param process
     * @return returnRequest
     */
    public OrderReturnRecordEntryModel getReturnRequest(final OrderProcessModel process) {

        final OrderReturnRecordEntryModel request = (OrderReturnRecordEntryModel)getParameterFromProcess(process,
                ParameterName.RETURN_REQUEST.name());

        return request;
    }


    /**
     * Set the ReturnRequestModel in process params
     * 
     * @param process
     * @param returnRequest
     */
    public void setReturnRequest(final OrderProcessModel process,
            final OrderReturnRecordEntryModel returnRequest) {

        // Convert the ReturnRequestModel to a serializable version
        setParameterForProcess(process, returnRequest, ParameterName.RETURN_REQUEST.name());
    }


    /**
     * Get the refund amount for the process if there is one set in params
     * 
     * @param process
     * @return amount or null if no value present
     */
    public BigDecimal getRefundAmount(final OrderProcessModel process) {

        final BigDecimal refundAmount = (BigDecimal)getParameterFromProcess(process,
                ParameterName.REFUND_AMOUNT.name());
        return refundAmount;
    }


    /**
     * Set the given refund amount in the process params
     * 
     * @param process
     * @param refundAmount
     */
    public void setRefundAmount(final OrderProcessModel process, final BigDecimal refundAmount) {

        setParameterForProcess(process, refundAmount, ParameterName.REFUND_AMOUNT.name());
    }


    /**
     * Get refunded information for CS ticket
     * 
     * @param process
     * @return refundedAmountInfo
     */
    public String getRefundedAmountInfo(final OrderProcessModel process) {

        final String refundedAmountInfo = (String)getParameterFromProcess(process,
                ParameterName.REFUNDED_AMOUNT_INFO.name());
        return refundedAmountInfo;
    }

    /**
     * Set refunded information for CS ticket
     * 
     * @param process
     * @param refundedAmountInfo
     */
    public void setRefundedAmountInfo(final OrderProcessModel process, final String refundedAmountInfo) {

        setParameterForProcess(process, refundedAmountInfo, ParameterName.REFUNDED_AMOUNT_INFO.name());
    }

    /**
     * Get the consignment for the process if there is one set in params
     * 
     * @param process
     * @return ConsignmentModel
     */
    public ConsignmentModel getConsignment(final OrderProcessModel process) {
        return (ConsignmentModel)getParameterFromProcess(process, ParameterName.CONSIGNMENT.name());
    }


    /**
     * Set the given consignment in the process params
     * 
     * @param process
     * @param consignment
     */
    public void setConsignment(final OrderProcessModel process, final ConsignmentModel consignment) {
        setParameterForProcess(process, consignment, ParameterName.CONSIGNMENT.name());
    }




    /**
     * Get the PaymentTransactionEntryModel from the process if there is one
     * 
     * @param process
     * @return PaymentTransactionEntryModel
     */
    public PaymentTransactionEntryModel getPaymentTransactionEntry(final OrderProcessModel process) {

        final PaymentTransactionEntryModel ptem = (PaymentTransactionEntryModel)getParameterFromProcess(process,
                ParameterName.PAYMENT_TRANSACTION_ENTRY.name());
        return ptem;
    }


    /**
     * Set the given PaymentTransactionEntryModel in the process params
     * 
     * @param process
     * @param ptem
     */
    public void setPaymentTransactionEntry(final OrderProcessModel process, final PaymentTransactionEntryModel ptem) {

        setParameterForProcess(process, ptem, ParameterName.PAYMENT_TRANSACTION_ENTRY.name());
    }

    /**
     * Get the PickTypeEnum from the process params
     * 
     * @param process
     * @return PickTypeEnum
     */
    public PickTypeEnum getPickType(final OrderProcessModel process) {

        final PickTypeEnum type = (PickTypeEnum)getParameterFromProcess(process,
                ParameterName.PICK_TYPE.name());
        return type;
    }

    /**
     * Set the given PickTypeEnum in the process params
     * 
     * @param process
     * @param type
     */
    public void setPickType(final OrderProcessModel process, final PickTypeEnum type) {

        setParameterForProcess(process, type, ParameterName.PICK_TYPE.name());
    }

    /**
     * get the payment failed reason
     * 
     * @param process
     * @return String
     */
    public String getPaymentFailedReason(final OrderProcessModel process) {

        final String reason = (String)getParameterFromProcess(process,
                ParameterName.PAYMENT_FAILED_REASON.name());
        return reason;
    }

    /**
     * set the payment failed reason
     * 
     * @param process
     * @param reason
     */
    public void setPaymentFailedReason(final OrderProcessModel process, final String reason) {

        setParameterForProcess(process, reason, ParameterName.PAYMENT_FAILED_REASON.name());
    }

    /**
     * Get the CancelTypeEnum from the process params
     * 
     * @param process
     * @return CancelTypeEnum
     */
    public CancelTypeEnum getCancelType(final OrderProcessModel process) {

        final CancelTypeEnum type = (CancelTypeEnum)getParameterFromProcess(process,
                ParameterName.CANCEL_TYPE.name());
        return type;
    }

    /**
     * Set the given CancelTypeEnum in the process params
     * 
     * @param process
     * @param type
     */
    public void setCancelType(final OrderProcessModel process, final CancelTypeEnum type) {

        setParameterForProcess(process, type, ParameterName.CANCEL_TYPE.name());
    }


    /**
     * get whether to do cancel extract
     * 
     * @param process
     * @return boolean
     */
    public boolean doCancelExtract(final OrderProcessModel process) {

        final Boolean doit = (Boolean)getParameterFromProcess(process,
                ParameterName.DO_CANCEL_EXTRACT.name());
        return (doit != null && doit.booleanValue());
    }

    /**
     * set PICK_CONSIGNMENT_UPDATE_DATA
     * 
     * @param process
     * @param data
     */
    public void setPickConsignmentUpdateData(final OrderProcessModel process, final Object data) {

        setParameterForProcess(process, data, ParameterName.PICK_CONSIGNMENT_UPDATE_DATA.name());
    }

    /**
     * get PICK_CONSIGNMENT_UPDATE_DATA
     * 
     * @param process
     * @return data
     */
    public Object getPickConsignmentUpdateData(final OrderProcessModel process) {

        return getParameterFromProcess(process, ParameterName.PICK_CONSIGNMENT_UPDATE_DATA.name());
    }

    /**
     * set whether to do cancel extract
     * 
     * @param process
     * @param doit
     */
    public void setDoCancelExtract(final OrderProcessModel process, final boolean doit) {

        setParameterForProcess(process, Boolean.valueOf(doit), ParameterName.DO_CANCEL_EXTRACT.name());
    }


    private Object getParameterFromProcess(final BusinessProcessModel process, final String name) {

        // Prevent spurious warnings from getProcessParameterByName if parameter does not exist
        if (!containsParameter(process, name)) {
            return null;
        }

        final BusinessProcessParameterModel bppm = getProcessParameterByName(process, name);
        if (bppm == null) {
            return null;
        }
        return bppm.getValue();
    }


    private void setParameterForProcess(final BusinessProcessModel process, final Object value, final String name) {
        setProcessParameter(process, name, value);

        // refresh the process model to reflect the change
        getModelService().refresh(process);
    }

    /**
     * Set NEW_EMAIL_ADDRESS
     * 
     * @param newEmailAddress
     */
    public void setNewEmailAddr(final OrderProcessModel process, final String newEmailAddress) {
        setParameterForProcess(process, newEmailAddress, ParameterName.NEW_EMAIL_ADDRESS.name());

    }

    /**
     * get NEW_EMAIL_ADDRESS
     * 
     * @param process
     * @return data
     */
    public Object getNewEmailAddr(final OrderProcessModel process) {

        return getParameterFromProcess(process, ParameterName.NEW_EMAIL_ADDRESS.name());
    }

    /**
     * set cnc notification data
     * 
     * @param process
     * @param cncData
     */
    public void setCncNotificationData(final OrderProcessModel process, final Object cncData) {
        setParameterForProcess(process, cncData, ParameterName.CNC_NOTIFICATION_DATA.name());
    }

    /**
     * get cnc notification data
     * 
     * @param process
     * @return Object cnc notification data
     */
    public Object getCncNotificationData(final OrderProcessModel process) {
        return getParameterFromProcess(process, ParameterName.CNC_NOTIFICATION_DATA.name());
    }

    /**
     * set customer subscription detail data
     * 
     * @param process
     * @param subscriptionDetailsData
     */
    public void setCustomerSubscriptionData(final BusinessProcessModel process, final Object subscriptionDetailsData) {
        setParameterForProcess(process, subscriptionDetailsData, ParameterName.CUSTOMER_SUBSCRIPTION_DATA.name());
    }

    /**
     * get customer subscription detail data
     * 
     * @param process
     * @return object subscriptionDetailsData
     */
    public Object getCustomerSubscriptionData(final BusinessProcessModel process) {
        return getParameterFromProcess(process, ParameterName.CUSTOMER_SUBSCRIPTION_DATA.name());
    }

    /**
     * get the reject reason for why the consignment is being rejected
     * 
     * @param process
     * @return ConsignmentRejectReason
     */
    public ConsignmentRejectReason getConsignmentRejectReason(final OrderProcessModel process) {
        return (ConsignmentRejectReason)getParameterFromProcess(process,
                ParameterName.CONSIGNMENT_REJECT_REASON.name());
    }


    /**
     * set the reject reason for the consignment
     * 
     * @param process
     * @param rejectReason
     */
    public void setConsignmentRejectReason(final OrderProcessModel process,
            final ConsignmentRejectReason rejectReason) {
        setParameterForProcess(process, rejectReason, ParameterName.CONSIGNMENT_REJECT_REASON.name());
    }

    /**
     * get send sms to store data
     * 
     * @param process
     * @return Object cnc notification data
     */
    public Object getSendSmsToStoreData(final BusinessProcessModel process) {
        return getParameterFromProcess(process,
                ParameterName.SEND_SMS_TO_STORE_DATA.name());
    }

    /**
     * set send sms to store data
     * 
     * @param process
     * @param sendSmsToStore
     */
    public void setSendSmsToStoreData(final BusinessProcessModel process,
            final Object sendSmsToStore) {
        setProcessParameter(process,
                ParameterName.SEND_SMS_TO_STORE_DATA.name(), sendSmsToStore);
    }

    public Object getGiftCardReversalData(final BusinessProcessModel process) {
        return getParameterFromProcess(process,
                ParameterName.GIFTCARD_REVERSAL_DETAILS.name());
    }

    public void setGiftCardReversalData(final BusinessProcessModel process, final Object giftCardReversalData) {
        setProcessParameter(process,
                ParameterName.GIFTCARD_REVERSAL_DETAILS.name(), giftCardReversalData);
    }

    public PaymentTransactionEntryModel getGiftCardReversalCaptureEntry(final BusinessProcessModel process) {
        return (PaymentTransactionEntryModel)getParameterFromProcess(process,
                ParameterName.GIFTCARD_REVERSAL_CAPTURE_ENTRY.name());
    }

    public void setGiftCardReversalCaptureEntry(final BusinessProcessModel process,
            final PaymentTransactionEntryModel captureEntry) {
        setProcessParameter(process,
                ParameterName.GIFTCARD_REVERSAL_CAPTURE_ENTRY.name(), captureEntry);
    }

    /**
     * Set the list of refunded payment entries.
     * 
     * @param process
     *            the business process model
     * @param refundPaymentEntries
     *            The list of refunded payment entries
     */
    public void setRefundPaymentEntryList(final BusinessProcessModel process,
            final List<PaymentTransactionEntryModel> refundPaymentEntries) {
        if (refundPaymentEntries == null) {
            return;
        }

        setProcessParameter(process, ParameterName.REFUND_PAYMENT_ENTRIES.name(), refundPaymentEntries);
    }

    /**
     * Get the list of refunded payment entries.
     * 
     * @param process
     *            the business process model
     * @return the list of refunded payment entries
     */
    public List<PaymentTransactionEntryModel> getRefundPaymentEntryList(final BusinessProcessModel process) {
        final Object refundPaymentEntries = getParameterFromProcess(process,
                ParameterName.REFUND_PAYMENT_ENTRIES.name());

        if (refundPaymentEntries instanceof List) {
            return (List<PaymentTransactionEntryModel>)refundPaymentEntries;
        }

        return null;
    }

    public void setRefundAmountRemaining(final BusinessProcessModel process, final BigDecimal refundAmountRemaining) {
        if (refundAmountRemaining == null) {
            return;
        }

        setProcessParameter(process, ParameterName.REFUND_AMOUNT_REMAINING.name(), refundAmountRemaining);
    }

    public BigDecimal getRefundAmountRemaining(final BusinessProcessModel process) {
        final Object refundAmountRemaining = getParameterFromProcess(process,
                ParameterName.REFUND_AMOUNT_REMAINING.name());
        if (refundAmountRemaining instanceof BigDecimal) {
            return (BigDecimal)refundAmountRemaining;
        }

        return null;
    }

    public void setPartnerUpdateRequired(final OrderProcessModel process, final boolean partnerUpdateRequired) {
        setParameterForProcess(process, Boolean.valueOf(partnerUpdateRequired),
                ParameterName.PARTNER_UPDATE_REQUIRED.name());
    }

    public boolean isPartnerUpdateRequired(final OrderProcessModel process) {
        final Object partnerUpdateRequired = getParameterFromProcess(process,
                ParameterName.PARTNER_UPDATE_REQUIRED.name());
        if (partnerUpdateRequired instanceof Boolean) {
            return ((Boolean)partnerUpdateRequired).booleanValue();
        }
        return false;
    }
}

/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;


/**
 * @author cbi
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AcceptFluentOrderActionTest {

    @Mock
    private OrderModel orderModel;

    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;

    @Mock
    private OrderProcessModel process;

    @InjectMocks
    @Spy
    private final AcceptFluentOrderAction acceptFluentOrderAction = new AcceptFluentOrderAction();

    @Test
    public void executeActionTest() throws RetryLaterException, Exception {
        given(process.getOrder()).willReturn(orderModel);
        final ArgumentCaptor<OrderModel> orderCaptor = ArgumentCaptor.forClass(OrderModel.class);
        doReturn(targetBusinessProcessService).when(acceptFluentOrderAction).getTargetBusinessProcessService();
        acceptFluentOrderAction.executeAction(process);
        verify(targetBusinessProcessService).startFluentOrderAcceptProcess(orderCaptor.capture());
        assertThat(orderCaptor.getValue()).isEqualTo(orderModel);
    }
}

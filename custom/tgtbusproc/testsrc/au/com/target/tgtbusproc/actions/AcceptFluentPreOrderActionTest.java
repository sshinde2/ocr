/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;


/**
 * @author bbaral1
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AcceptFluentPreOrderActionTest {

    @InjectMocks
    @Spy
    private final AcceptFluentPreOrderAction acceptFluentPreOrderAction = new AcceptFluentPreOrderAction();

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @Mock
    private OrderProcessModel orderProcess;

    @Mock
    private OrderModel order;

    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;

    /**
     * This method will throw Illegal argument exception because it's try to execute the action with null order process
     * value.
     * 
     * @throws Exception
     * @throws RetryLaterException
     */
    @Test
    public void testExecuteActionWithNullOrderProcess() throws RetryLaterException, Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Orderprocess cannot be null");
        acceptFluentPreOrderAction.executeAction(null);
    }
    

    /**
     * This method will throw Illegal argument exception because it's try to execute the action with null order model
     * value.
     * 
     * @throws Exception
     * @throws RetryLaterException
     */
    @Test
    public void testExecuteActionWithNullOrder() throws RetryLaterException, Exception {
        willReturn(null).given(orderProcess).getOrder();
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Ordermodel cannot be null");
        acceptFluentPreOrderAction.executeAction(orderProcess);
    }


    /**
     * This method will verify fluent accept pre-order process by passing proper order value.
     * 
     * @throws Exception
     * @throws RetryLaterException
     */
    @Test
    public void testExecuteActionWithOrder() throws RetryLaterException, Exception {
        given(orderProcess.getOrder()).willReturn(order);
        doReturn(targetBusinessProcessService).when(acceptFluentPreOrderAction).getTargetBusinessProcessService();
        final ArgumentCaptor<OrderModel> orderArgumentCaptor = ArgumentCaptor.forClass(OrderModel.class);
        acceptFluentPreOrderAction.executeAction(orderProcess);
        verify(targetBusinessProcessService).startFluentAcceptPreOrderProcess(orderArgumentCaptor.capture());
        assertThat(orderArgumentCaptor.getValue()).isEqualTo(order);
    }

}

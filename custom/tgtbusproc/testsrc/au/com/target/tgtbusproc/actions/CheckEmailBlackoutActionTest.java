/**
 * 
 */
package au.com.target.tgtbusproc.actions;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.EmailBlackoutService;


/**
 * @author Pradeep
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CheckEmailBlackoutActionTest {

    @Mock
    private EmailBlackoutService emailBlackoutService;

    private final OrderProcessModel process = new OrderProcessModel();

    @InjectMocks
    private final CheckEmailBlackoutAction checkEmailBlackoutAction = new CheckEmailBlackoutAction();

    @Test
    public void testExecuteInternal() throws RetryLaterException, Exception {
        Mockito.doReturn(Boolean.FALSE).when(emailBlackoutService).isEmailBlackout();
        final String result = checkEmailBlackoutAction.executeInternal(process);
        Assert.assertTrue(StringUtils.equals(result, "OK"));
    }

    @Test(expected = RetryLaterException.class)
    public void testExecuteInternalThrowingException() throws RetryLaterException, Exception {
        Mockito.doReturn(Boolean.TRUE).when(emailBlackoutService).isEmailBlackout();
        checkEmailBlackoutAction.executeInternal(process);
    }
}

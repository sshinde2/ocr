package au.com.target.tgtbusproc.actions;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;
import de.hybris.platform.task.RetryLaterException;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.email.BusinessProcessEmailStrategy;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendOrderAwaitingReviewEmailActionTest {

    @InjectMocks
    @Spy
    private final SendOrderAwaitingReviewEmailAction action = new SendOrderAwaitingReviewEmailAction();

    @Mock
    private OrderProcessModel orderProcess;

    @Mock
    private OrderModel orderModel;

    @Mock
    private BusinessProcessEmailStrategy businessProcessEmailStrategy;

    @Before
    public void setUp() {
        action.setFrontendTemplateName("TEMPLATE");
        willReturn(businessProcessEmailStrategy).given(action).getBusinessProcessEmailStrategy();
        given(orderProcess.getOrder()).willReturn(orderModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteActionNullOrderProcess() throws Exception {
        action.executeAction(null);
    }

    @Test
    public void testExecuteInternalForAPreOrderInReviewAfterFinalPayment() throws RetryLaterException, Exception {

        given(orderModel.getContainsPreOrderItems()).willReturn(Boolean.FALSE);
        final Date date = Mockito.mock(Date.class);
        given(orderModel.getNormalSaleStartDateTime()).willReturn(date);

        final Transition trans = action.executeAction(orderProcess);
        assertThat(trans).isEqualTo(Transition.OK);
        verifyZeroInteractions(businessProcessEmailStrategy);
    }

    @Test
    public void testExecuteInternalForAPreOrderInReviewAfterInitialDeposit() throws RetryLaterException, Exception {

        given(orderModel.getContainsPreOrderItems()).willReturn(Boolean.TRUE);
        final Date date = Mockito.mock(Date.class);
        given(orderModel.getNormalSaleStartDateTime()).willReturn(date);

        final Transition trans = action.executeAction(orderProcess);
        assertThat(trans).isEqualTo(Transition.OK);
        verify(businessProcessEmailStrategy).sendEmail(orderProcess, "TEMPLATE");
    }

    @Test
    public void testExecuteInternalForAnOrderThatIsPreOrder() throws RetryLaterException, Exception {

        given(orderModel.getContainsPreOrderItems()).willReturn(Boolean.FALSE);
        given(orderModel.getNormalSaleStartDateTime()).willReturn(null);

        final Transition trans = action.executeAction(orderProcess);
        assertThat(trans).isEqualTo(Transition.OK);
        verify(businessProcessEmailStrategy).sendEmail(orderProcess, "TEMPLATE");
    }

}

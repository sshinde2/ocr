/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendPaymentFailedCsAgentTicketDecisionTest {

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderProcessParameterHelper paramHelper;

    @InjectMocks
    private final SendPaymentFailedCsAgentTicketDecisionAction action = new SendPaymentFailedCsAgentTicketDecisionAction();

    @Test
    public void executeRetryExceeded() throws RetryLaterException, Exception {
        BDDMockito.given(paramHelper.getPaymentFailedReason(process)).willReturn("RETRYEXCEEDED");
        final String result = action.execute(process);
        Assert.assertEquals("RETRYEXCEEDED", result);
    }

    @Test
    public void executeRejected() throws RetryLaterException, Exception {
        BDDMockito.given(paramHelper.getPaymentFailedReason(process)).willReturn("REJECTED");
        final String result = action.execute(process);
        Assert.assertEquals("REJECTED", result);
    }

    @Test
    public void execute() throws RetryLaterException, Exception {
        BDDMockito.given(paramHelper.getPaymentFailedReason(process)).willReturn("TEST");
        final String result = action.execute(process);
        Assert.assertEquals("NOK", result);
    }
}

/**
 * 
 */
package au.com.target.tgtbusproc.email.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.returns.model.OrderReturnRecordEntryModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.email.EmailServiceException;
import au.com.target.tgtbusproc.email.SendExternalEmailService;
import au.com.target.tgtbusproc.email.data.TargetEmailContext;
import au.com.target.tgtbusproc.exceptions.BusinessProcessEmailException;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExternalEmailStrategyUnitTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @Mock
    private SendExternalEmailService sendExternalEmailService;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Mock
    private CustomerModel customer;

    @Mock
    private BaseSiteModel site;

    @Mock
    private OrderCancelRecordEntryModel orderCancelRequest;

    @Mock
    private OrderReturnRecordEntryModel returnRequest;

    @Mock
    private MediaModel taxInoviceMedia;

    @InjectMocks
    private final ExternalEmailStrategy strategy = new ExternalEmailStrategy();

    @Before
    public void setup() {

        BDDMockito.given(process.getOrder()).willReturn(order);
        BDDMockito.given(order.getUser()).willReturn(customer);
        BDDMockito.given(order.getSite()).willReturn(site);

        BDDMockito.given(order.getCode()).willReturn("testOrder");
        BDDMockito.given(process.getCode()).willReturn("testProcess");

        BDDMockito.given(orderProcessParameterHelper.getOrderCancelRequest(process)).willReturn(orderCancelRequest);
        BDDMockito.given(orderProcessParameterHelper.getReturnRequest(process)).willReturn(returnRequest);

    }

    @Test
    public void testGenerateEmailContext() {

        final TargetEmailContext context = strategy.generateEmailContext(process);

        Assert.assertEquals(order, context.getOrder());
        Assert.assertEquals(customer, context.getCustomer());
        Assert.assertEquals(site, context.getSite());
        Assert.assertEquals(orderCancelRequest, context.getCancelRequest());
        Assert.assertEquals(returnRequest, context.getReturnRequest());

    }

    @Test
    public void testGenerateEmailContextNullOrder() {

        BDDMockito.given(process.getOrder()).willReturn(null);

        final TargetEmailContext context = strategy.generateEmailContext(process);

        Assert.assertNull(context.getOrder());
        Assert.assertNull(context.getCustomer());
        Assert.assertNull(context.getSite());
    }

    @Test
    public void testGenerateEmailContextNullCustomer() {

        BDDMockito.given(order.getUser()).willReturn(null);

        final TargetEmailContext context = strategy.generateEmailContext(process);

        Assert.assertEquals(order, context.getOrder());
        Assert.assertNull(context.getCustomer());
        Assert.assertEquals(site, context.getSite());
    }

    @Test
    public void testGenerateEmailContextNullSite() {

        BDDMockito.given(order.getSite()).willReturn(null);

        final TargetEmailContext context = strategy.generateEmailContext(process);

        Assert.assertEquals(order, context.getOrder());
        Assert.assertEquals(customer, context.getCustomer());
        Assert.assertNull(context.getSite());
    }

    @Test
    public void testGenerateEmailContextNullCancelRequest() {

        BDDMockito.given(orderProcessParameterHelper.getOrderCancelRequest(process)).willReturn(null);

        final TargetEmailContext context = strategy.generateEmailContext(process);

        Assert.assertEquals(order, context.getOrder());
        Assert.assertNull(context.getCancelRequest());
    }

    @Test
    public void testSendEmailSuccess() throws BusinessProcessEmailException, EmailServiceException {

        strategy.sendEmail(process, "template");
        Mockito.verify(sendExternalEmailService).sendEmail(any(TargetEmailContext.class), anyString());
    }

    @Test
    public void testSendEmailFailure() throws BusinessProcessEmailException, EmailServiceException {

        expectedException.expect(BusinessProcessEmailException.class);

        Mockito.doThrow(new EmailServiceException("Email exception", null))
                .when(sendExternalEmailService).sendEmail(any(TargetEmailContext.class), anyString());

        strategy.sendEmail(process, "template");
    }

}

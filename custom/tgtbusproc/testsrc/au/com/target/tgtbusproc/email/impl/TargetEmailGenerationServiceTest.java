/**
 * 
 */
package au.com.target.tgtbusproc.email.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Test;


/**
 * Unit tests for TargetEmailGenerationService.
 * 
 * @author jjayawa1
 *
 */
@UnitTest
public class TargetEmailGenerationServiceTest {

    private final TargetEmailGenerationService emailGenerationService = new TargetEmailGenerationService();

    @Test
    public void testFormatFileNameForValidFile() {
        final String formattedString = emailGenerationService.formatFileName("test.pdf");
        Assert.assertTrue(formattedString.matches("test-[\\d]*.pdf"));
    }

    @Test
    public void testFormatFileNameWithNoExtension() {
        final String formattedString = emailGenerationService.formatFileName("testpdf");
        Assert.assertTrue(formattedString.matches("testpdf-[\\d]*"));
    }
}

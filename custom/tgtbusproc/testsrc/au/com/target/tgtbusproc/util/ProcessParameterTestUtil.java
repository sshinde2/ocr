/**
 * 
 */
package au.com.target.tgtbusproc.util;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.orderhistory.OrderHistoryService;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import au.com.target.tgtbusproc.constants.TgtbusprocConstants;


public final class ProcessParameterTestUtil {

    private ProcessParameterTestUtil() {
        // Nothing
    }

    public static OrderModel createOrderForCode(final String code,
            final ModelService modelService) {

        final CurrencyModel currency = modelService.create(CurrencyModel.class);
        currency.setIsocode("TEST");
        currency.setSymbol("TEST");
        modelService.save(currency);
        final CustomerModel user = modelService.create(CustomerModel.class);
        user.setUid("user@target.com.au");
        user.setName("Test User");
        modelService.save(user);

        final OrderModel order = new OrderModel();
        order.setCode(code);
        order.setCurrency(currency);
        order.setUser(user);
        order.setDate(new Date());
        order.setSalesApplication(SalesApplication.WEB);
        modelService.save(order);

        return order;
    }

    public static void setPaymentMethodToOrder(final OrderModel orderModel, final PaymentModeModel paymentModeModel,
            final ModelService modelService) {

        orderModel.setPaymentMode(paymentModeModel);
        modelService.save(orderModel);
    }

    public static OrderModel createOrderForCodeWithEntry(final String code,
            final ModelService modelService,
            final CatalogVersionService catalogVersionService) {

        final OrderModel order = createOrderForCode(code, modelService);

        final UnitModel unit = new UnitModel();
        unit.setCode("unit");
        unit.setUnitType("unittype");
        modelService.save(unit);

        final CatalogVersionModel cat = catalogVersionService.getAllCatalogVersions().iterator().next();

        final ProductModel product1 = new ProductModel();
        product1.setCode("product1");
        product1.setCatalogVersion(cat);
        modelService.save(product1);

        final ProductModel product2 = new ProductModel();
        product2.setCode("product2");
        product2.setCatalogVersion(cat);
        modelService.save(product2);

        final OrderEntryModel entry = new OrderEntryModel();
        entry.setQuantity(Long.valueOf(3));
        entry.setOrder(order);
        entry.setUnit(unit);
        entry.setProduct(product1);
        modelService.save(entry);

        final OrderEntryModel entry2 = new OrderEntryModel();
        entry2.setQuantity(Long.valueOf(1));
        entry2.setOrder(order);
        entry2.setUnit(unit);
        entry2.setProduct(product2);
        modelService.save(entry2);

        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        entries.add(entry);
        entries.add(entry2);
        order.setEntries(entries);
        modelService.save(order);

        return order;
    }

    public static OrderProcessModel createProcess(final OrderModel order, final String processCode,
            final ModelService modelService, final BusinessProcessService businessProcessService) {

        return createProcessWithParams(order, processCode, modelService, businessProcessService, null);
    }

    public static OrderProcessModel createProcessWithParams(final OrderModel order, final String processCode,
            final ModelService modelService, final BusinessProcessService businessProcessService,
            final Map<String, Object> params) {

        final OrderProcessModel acceptOrderProcess = businessProcessService.createProcess(processCode,
                TgtbusprocConstants.BusinessProcess.ACCEPT_ORDER_PROCESS, params);
        acceptOrderProcess.setOrder(order);
        modelService.save(acceptOrderProcess);

        return acceptOrderProcess;
    }

    public static void createHistorySnapshot(final OrderModel order, final String description,
            final OrderHistoryService orderHistoryService, final ModelService modelService) {
        // Create and save the snapshot version
        final OrderModel version = orderHistoryService.createHistorySnapshot(order);
        orderHistoryService.saveHistorySnapshot(version);

        // Add the version into the history entries for the order
        final OrderHistoryEntryModel historyEntry = modelService.create(OrderHistoryEntryModel.class);
        historyEntry.setOrder(order);
        historyEntry.setPreviousOrderVersion(version);
        historyEntry.setDescription(description);
        historyEntry.setTimestamp(new Date());
        modelService.save(historyEntry);
    }


}

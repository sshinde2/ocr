/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtcms.constants;

/**
 * Global class for all tgtcms constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class TgtcmsConstants extends GeneratedTgtcmsConstants
{
    // implement here constants used by this extension
    public static final String EXTENSIONNAME = "tgtcms";
}

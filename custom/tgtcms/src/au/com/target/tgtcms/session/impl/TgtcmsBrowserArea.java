/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtcms.session.impl;

import de.hybris.platform.cockpit.session.impl.DefaultSearchBrowserArea;


/**
 * Tgtcms browser area.
 */
public class TgtcmsBrowserArea extends DefaultSearchBrowserArea {
    // This class deliberately left blank
}

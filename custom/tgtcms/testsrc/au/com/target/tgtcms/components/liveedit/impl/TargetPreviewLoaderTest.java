package au.com.target.tgtcms.components.liveedit.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.restrictions.AbstractRestrictionModel;
import de.hybris.platform.cms2.model.restrictions.CMSCatalogRestrictionModel;
import de.hybris.platform.cms2.model.restrictions.CMSTimeRestrictionModel;
import de.hybris.platform.cms2.servicelayer.services.admin.CMSAdminRestrictionService;
import de.hybris.platform.cms2.servicelayer.services.admin.CMSAdminSiteService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtwebcore.model.cms2.pages.BrandPageModel;
import au.com.target.tgtwebcore.model.cms2.preview.TargetPreviewDataModel;
import au.com.target.tgtwebcore.model.cms2.restrictions.CMSBrandRestrictionModel;


/**
 * Unit test for {@link TargetPreviewLoader}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPreviewLoaderTest {

    @InjectMocks
    private final TargetPreviewLoader targetPreviewLoader = new TargetPreviewLoader();

    @Mock
    private CMSAdminRestrictionService cmsAdminRestrictionService;

    @Mock
    private CMSAdminSiteService cmsAdminSiteService;

    @Mock
    private CMSTimeRestrictionModel cmsTimeRestrictionModel;

    @Mock
    private CMSBrandRestrictionModel cmsBrandRestrictionModel;

    @Mock
    private CMSCatalogRestrictionModel cmsCatalogRestrictionModel;

    @Mock
    private BrandModel brandModel;

    @Mock
    private CatalogModel catalogModel;

    @Before
    public void setUp() {
        Mockito.when(cmsTimeRestrictionModel.getActiveFrom()).thenReturn(new Date());
        Mockito.when(cmsBrandRestrictionModel.getBrands()).thenReturn(Collections.singletonList(brandModel));
        Mockito.when(cmsCatalogRestrictionModel.getCatalogs()).thenReturn(Collections.singletonList(catalogModel));
    }

    @Test
    public void testLoadValuesForBrandPageWithOnlyOneRestrictionAllowed() {
        final TargetPreviewDataModel previewCtx = Mockito.mock(TargetPreviewDataModel.class);
        final BrandPageModel brandPageModel = Mockito.mock(BrandPageModel.class);

        Mockito.when(previewCtx.getPage()).thenReturn(brandPageModel);

        Mockito.when(Boolean.valueOf(brandPageModel.isOnlyOneRestrictionMustApply())).thenReturn(Boolean.TRUE);
        Mockito.when(brandPageModel.getRestrictions()).thenReturn(
                Arrays.asList(cmsTimeRestrictionModel, cmsBrandRestrictionModel));

        final CatalogVersionModel catalogVersionModel = Mockito.mock(CatalogVersionModel.class);

        targetPreviewLoader.loadValues(previewCtx, brandPageModel, Collections.singletonList(catalogVersionModel),
                false, null,
                null);

        Mockito.verify(previewCtx, Mockito.times(2)).setTime(Mockito.any(Date.class));
        Mockito.verify(previewCtx, Mockito.never()).setPreviewBrand(Mockito.any(BrandModel.class));
    }

    @Test
    public void testLoadValuesForBrandPageWithBrandRestriction() throws CMSItemNotFoundException {
        final TargetPreviewDataModel previewCtx = Mockito.mock(TargetPreviewDataModel.class);
        final BrandPageModel brandPageModel = Mockito.mock(BrandPageModel.class);

        Mockito.when(previewCtx.getPage()).thenReturn(brandPageModel);

        Mockito.when(Boolean.valueOf(brandPageModel.isOnlyOneRestrictionMustApply())).thenReturn(Boolean.FALSE);
        final List<AbstractRestrictionModel> restrictions = new ArrayList<>();
        restrictions.add(cmsBrandRestrictionModel);
        Mockito.when(brandPageModel.getRestrictions()).thenReturn(restrictions);
        Mockito.when(
                cmsAdminRestrictionService.getRestrictionsForPage(Mockito.eq(brandPageModel),
                        Mockito.eq(CMSBrandRestrictionModel._TYPECODE)))
                .thenReturn(restrictions);

        final CatalogVersionModel catalogVersionModel = Mockito.mock(CatalogVersionModel.class);

        targetPreviewLoader.loadValues(previewCtx, brandPageModel, Collections.singletonList(catalogVersionModel),
                false, null,
                null);

        Mockito.verify(previewCtx).setTime(Mockito.any(Date.class));
        Mockito.verify(previewCtx).setPreviewBrand(brandModel);
    }

    @Test
    public void testLoadValuesForBrandPageWithBrandAndCatalogRestriction() throws CMSItemNotFoundException {
        final TargetPreviewDataModel previewCtx = Mockito.mock(TargetPreviewDataModel.class);
        final BrandPageModel brandPageModel = Mockito.mock(BrandPageModel.class);

        Mockito.when(previewCtx.getPage()).thenReturn(brandPageModel);

        Mockito.when(Boolean.valueOf(brandPageModel.isOnlyOneRestrictionMustApply())).thenReturn(Boolean.FALSE);
        Mockito.when(brandPageModel.getRestrictions()).thenReturn(
                Arrays.asList(cmsCatalogRestrictionModel, cmsBrandRestrictionModel));

        final Collection<AbstractRestrictionModel> cmsBrandRestrictions = new ArrayList<>();
        cmsBrandRestrictions.add(cmsBrandRestrictionModel);
        final Collection<AbstractRestrictionModel> cmsCatalogRestrictions = new ArrayList<>();
        cmsBrandRestrictions.add(cmsCatalogRestrictionModel);
        Mockito.when(
                cmsAdminRestrictionService.getRestrictionsForPage(Mockito.eq(brandPageModel),
                        Mockito.eq(CMSBrandRestrictionModel._TYPECODE)))
                .thenReturn(cmsBrandRestrictions);
        Mockito.when(
                cmsAdminRestrictionService.getRestrictionsForPage(Mockito.eq(brandPageModel),
                        Mockito.eq(CMSCatalogRestrictionModel._TYPECODE)))
                .thenReturn(cmsCatalogRestrictions);

        final CatalogVersionModel catalogVersionModel = Mockito.mock(CatalogVersionModel.class);

        targetPreviewLoader.loadValues(previewCtx, brandPageModel, Collections.singletonList(catalogVersionModel),
                false, null,
                null);

        Mockito.verify(previewCtx).setTime(Mockito.any(Date.class));
        Mockito.verify(previewCtx).setPreviewBrand(brandModel);
        Mockito.verify(previewCtx).setPreviewCatalog(catalogModel);
    }
}

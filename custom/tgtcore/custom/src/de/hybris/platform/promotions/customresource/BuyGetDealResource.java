package de.hybris.platform.promotions.resource;

import de.hybris.platform.core.resource.link.LinkResource;
import de.hybris.platform.hmc.resource.HMCHistoryEntryResource;
import de.hybris.platform.promotions.dto.BuyGetDealDTO;
import de.hybris.platform.promotions.model.BuyGetDealModel;
import de.hybris.platform.promotions.model.ValueBundleDealModel;
import de.hybris.platform.webservices.AbstractYResource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 * Custom Resource class for type BuyGetDeal to invoke initdefaults interceptor
 */
public class BuyGetDealResource extends AbstractYResource<BuyGetDealModel>
{
	/**
	 * <i>constructor</i> - for generic creation.
	 */
	public BuyGetDealResource()
	{
		super("BuyGetDeal");
	}
	
	
	/**
	 * HTTP method for covering DELETE requests.
	 * @return {@link javax.ws.rs.core.Response}
	 */
	@DELETE
	public Response deleteBuyGetDeal()
	{
		return createDeleteResponse().build();
	}
	
	/**
	 * HTTP method for covering GET requests.
	 * @return {@link javax.ws.rs.core.Response}
	 */
	@GET
	public Response getBuyGetDeal()
	{
		return createGetResponse().build();
	}
	
	/**
	 * Convenience method which just delegates to {@link #getResourceValue()}
	 */
	public BuyGetDealModel getBuyGetDealModel()
	{
		return super.getResourceValue();
	}
	
	/**
	 *  getter for sub resource of type {@link HMCHistoryEntryResource} for current root resource 
	 */
	@Path("/hmchistoryentries/{hmchistoryentry}")
	public AbstractYResource getHMCHistoryEntryResource(@PathParam("hmchistoryentry")  final String resourceKey)
	{
		final HMCHistoryEntryResource  resource  = resourceCtx.getResource(HMCHistoryEntryResource.class);
		resource.setResourceId(resourceKey );
		resource.setParentResource(this);
		passUniqueMember(resource);
		return resource;
	}
	
	/**
	 *  getter for sub resource of type {@link LinkResource} for current root resource 
	 */
	@Path("/links/{link}")
	public AbstractYResource getLinkResource(@PathParam("link")  final String resourceKey)
	{
		final LinkResource  resource  = resourceCtx.getResource(LinkResource.class);
		resource.setResourceId(resourceKey );
		resource.setParentResource(this);
		passUniqueMember(resource);
		return resource;
	}
	
	/**
	 * HTTP method for covering PUT requests.
	 * @return {@link javax.ws.rs.core.Response}
	 */
	@PUT
	public Response putBuyGetDeal(final BuyGetDealDTO dto)
	{
		return createPutResponse(dto).build();
	}
	
	/**
	 * Gets the {@link BuyGetDealModel} resource which is addressed by current resource request.
	 * @see de.hybris.platform.webservices.AbstractYResource#readResource(String)
	 */
	@Override
	protected BuyGetDealModel readResource(final String resourceId) throws Exception
	{
		BuyGetDealModel model = new BuyGetDealModel();
		model.setCode(resourceId);
		return (BuyGetDealModel) readResourceInternal(model);
	}
	
	/**
	 * Convenience method which just delegates to {@link #setResourceValue(BuyGetDealModel)}
	 */
	public void setBuyGetDealModel(final BuyGetDealModel value)
	{
		super.setResourceValue(value);
	}
	
	/**
     * Overridden to invoke init defaults interceptor
     */
    @Override
    protected BuyGetDealModel createResource(@SuppressWarnings("unused") final Object reqEntity)
    {
        return getServiceLocator().getModelService().create(BuyGetDealModel.class);
    }
	
}

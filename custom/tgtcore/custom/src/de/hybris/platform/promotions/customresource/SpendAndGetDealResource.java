package de.hybris.platform.promotions.resource;

import de.hybris.platform.core.resource.link.LinkResource;
import de.hybris.platform.hmc.resource.HMCHistoryEntryResource;
import de.hybris.platform.promotions.dto.SpendAndGetDealDTO;
import de.hybris.platform.promotions.model.QuantityBreakDealModel;
import de.hybris.platform.promotions.model.SpendAndGetDealModel;
import de.hybris.platform.webservices.AbstractYResource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 * Custom resource class for type SpendAndGetDeal to invoke initdefaults interceptor
 */
public class SpendAndGetDealResource extends AbstractYResource<SpendAndGetDealModel>
{
	/**
	 * <i>constructor</i> - for generic creation.
	 */
	public SpendAndGetDealResource()
	{
		super("SpendAndGetDeal");
	}
	
	
	/**
	 * HTTP method for covering DELETE requests.
	 * @return {@link javax.ws.rs.core.Response}
	 */
	@DELETE
	public Response deleteSpendAndGetDeal()
	{
		return createDeleteResponse().build();
	}
	
	/**
	 *  getter for sub resource of type {@link HMCHistoryEntryResource} for current root resource 
	 */
	@Path("/hmchistoryentries/{hmchistoryentry}")
	public AbstractYResource getHMCHistoryEntryResource(@PathParam("hmchistoryentry")  final String resourceKey)
	{
		final HMCHistoryEntryResource  resource  = resourceCtx.getResource(HMCHistoryEntryResource.class);
		resource.setResourceId(resourceKey );
		resource.setParentResource(this);
		passUniqueMember(resource);
		return resource;
	}
	
	/**
	 *  getter for sub resource of type {@link LinkResource} for current root resource 
	 */
	@Path("/links/{link}")
	public AbstractYResource getLinkResource(@PathParam("link")  final String resourceKey)
	{
		final LinkResource  resource  = resourceCtx.getResource(LinkResource.class);
		resource.setResourceId(resourceKey );
		resource.setParentResource(this);
		passUniqueMember(resource);
		return resource;
	}
	
	/**
	 * HTTP method for covering GET requests.
	 * @return {@link javax.ws.rs.core.Response}
	 */
	@GET
	public Response getSpendAndGetDeal()
	{
		return createGetResponse().build();
	}
	
	/**
	 * Convenience method which just delegates to {@link #getResourceValue()}
	 */
	public SpendAndGetDealModel getSpendAndGetDealModel()
	{
		return super.getResourceValue();
	}
	
	/**
	 * HTTP method for covering PUT requests.
	 * @return {@link javax.ws.rs.core.Response}
	 */
	@PUT
	public Response putSpendAndGetDeal(final SpendAndGetDealDTO dto)
	{
		return createPutResponse(dto).build();
	}
	
	/**
	 * Gets the {@link SpendAndGetDealModel} resource which is addressed by current resource request.
	 * @see de.hybris.platform.webservices.AbstractYResource#readResource(String)
	 */
	@Override
	protected SpendAndGetDealModel readResource(final String resourceId) throws Exception
	{
		SpendAndGetDealModel model = new SpendAndGetDealModel();
		model.setCode(resourceId);
		return (SpendAndGetDealModel) readResourceInternal(model);
	}
	
	/**
	 * Convenience method which just delegates to {@link #setResourceValue(SpendAndGetDealModel)}
	 */
	public void setSpendAndGetDealModel(final SpendAndGetDealModel value)
	{
		super.setResourceValue(value);
	}
	
	/**
     * Overridden to invoke init defaults interceptor
     */
    @Override
    protected SpendAndGetDealModel createResource(@SuppressWarnings("unused") final Object reqEntity)
    {
        return getServiceLocator().getModelService().create(SpendAndGetDealModel.class);
    }
	
}

package de.hybris.platform.promotions.resource;

import de.hybris.platform.core.resource.link.LinkResource;
import de.hybris.platform.hmc.resource.HMCHistoryEntryResource;
import de.hybris.platform.promotions.dto.SpendAndSaveDealDTO;
import de.hybris.platform.promotions.model.SpendAndGetDealModel;
import de.hybris.platform.promotions.model.SpendAndSaveDealModel;
import de.hybris.platform.webservices.AbstractYResource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 *  Custom resource class for type SpendAndSaveDeal to invoke initdefaults interceptor
 */
public class SpendAndSaveDealResource extends AbstractYResource<SpendAndSaveDealModel>
{
	/**
	 * <i> constructor</i> - for generic creation.
	 */
	public SpendAndSaveDealResource()
	{
		super("SpendAndSaveDeal");
	}
	
	
	/**
	 *  HTTP method for covering DELETE requests.
	 * @return {@link javax.ws.rs.core.Response}
	 */
	@DELETE
	public Response deleteSpendAndSaveDeal()
	{
		return createDeleteResponse().build();
	}
	
	/**
	 *   getter for sub resource of type {@link HMCHistoryEntryResource} for current root resource 
	 */
	@Path("/hmchistoryentries/{hmchistoryentry}")
	public AbstractYResource getHMCHistoryEntryResource(@PathParam("hmchistoryentry")  final String resourceKey)
	{
		final HMCHistoryEntryResource  resource  = resourceCtx.getResource(HMCHistoryEntryResource.class);
		resource.setResourceId(resourceKey );
		resource.setParentResource(this);
		passUniqueMember(resource);
		return resource;
	}
	
	/**
	 *   getter for sub resource of type {@link LinkResource} for current root resource 
	 */
	@Path("/links/{link}")
	public AbstractYResource getLinkResource(@PathParam("link")  final String resourceKey)
	{
		final LinkResource  resource  = resourceCtx.getResource(LinkResource.class);
		resource.setResourceId(resourceKey );
		resource.setParentResource(this);
		passUniqueMember(resource);
		return resource;
	}
	
	/**
	 *  HTTP method for covering GET requests.
	 * @return {@link javax.ws.rs.core.Response}
	 */
	@GET
	public Response getSpendAndSaveDeal()
	{
		return createGetResponse().build();
	}
	
	/**
	 * Convenience method which just delegates to {@link #getResourceValue()}
	 */
	public SpendAndSaveDealModel getSpendAndSaveDealModel()
	{
		return super.getResourceValue();
	}
	
	/**
	 *  HTTP method for covering PUT requests.
	 * @return {@link javax.ws.rs.core.Response}
	 */
	@PUT
	public Response putSpendAndSaveDeal(final SpendAndSaveDealDTO dto)
	{
		return createPutResponse(dto).build();
	}
	
	/**
	 * Gets the {@link SpendAndSaveDealModel} resource which is addressed by current resource request.
	 * @see de.hybris.platform.webservices.AbstractYResource#readResource(String)
	 */
	@Override
	protected SpendAndSaveDealModel readResource(final String resourceId) throws Exception
	{
		SpendAndSaveDealModel model = new SpendAndSaveDealModel();
		model.setCode(resourceId);
		return (SpendAndSaveDealModel) readResourceInternal(model);
	}
	
	/**
	 * Convenience method which just delegates to {@link #setResourceValue(SpendAndSaveDealModel)}
	 */
	public void setSpendAndSaveDealModel(final SpendAndSaveDealModel value)
	{
		super.setResourceValue(value);
	}
	
	/**
     * Overridden to invoke init defaults interceptor
     */
    @Override
    protected SpendAndSaveDealModel createResource(@SuppressWarnings("unused") final Object reqEntity)
    {
        return getServiceLocator().getModelService().create(SpendAndSaveDealModel.class);
    }
	
}

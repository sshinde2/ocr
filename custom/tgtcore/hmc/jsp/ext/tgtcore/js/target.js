;(function( docElem, href ) {
	
	var isAdmin = /Administrator/.test( $$('title')[0].innerHTML ),
		isProductionHost = /www.target.com.au/.test( href ),
		isLocalhost = /localhost|tnet.internal/.test( href );
	
	var cl = docElem.className || "";
	
	// Assume tunnelling if localhost and not admin user
	if( isLocalhost && !isAdmin ) {
		return;
	}
	
	//if( isProductionHost ) {
	//		cl += ' target target-prod';
	//} else {
		cl += ' target target-pre-release';
	//}
	
	docElem.className = cl;
	
}(document.documentElement, document.location.href));

/**
 * 
 */
package au.com.target.tgtcore.hmc;

import de.hybris.platform.hmc.security.EncryptedPasswordEditorChip;
import de.hybris.platform.hmc.webchips.Chip;
import de.hybris.platform.hmc.webchips.DisplayState;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class TargetEncryptedPasswordEditorChip extends EncryptedPasswordEditorChip {

    private static final String[] PASSWORD_ENCODERS = { "targetmd5", "bcrypt" };

    /**
     * @param displayState
     * @param parent
     */
    public TargetEncryptedPasswordEditorChip(final DisplayState displayState, final Chip parent) {
        super(displayState, parent);
    }

    /**
     * Returns target password encodings.
     */
    @Override
    public List<String> getInstalledEncryptionMethods() {
        final List<String> targetInstalledEncryptionsList = new ArrayList<>();
        Collections.addAll(targetInstalledEncryptionsList, PASSWORD_ENCODERS);
        return targetInstalledEncryptionsList;
    }
}

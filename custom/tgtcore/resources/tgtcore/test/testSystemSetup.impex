# Test batch configuration
#
$productCatalog=targetProductCatalog
$contentCatalog=targetContentCatalog
$classificationCatalog=TargetClassification
$defaultCurrency=AUD
$languages=en
$defaultLanguage=en
$stagedProduct=catalogVersion(catalog(id[default=$productCatalog]),version[default='Staged'])[unique=true,default='$productCatalog:Staged']
$onlineProduct=catalogVersion(catalog(id[default=$productCatalog]),version[default='Online'])[unique=true,default='$productCatalog:Online']
$stagedContent=catalogVersion(catalog(id[default=$contentCatalog]),version[default='Staged'])[unique=true,default='$contentCatalog:Staged']
$onlineContent=catalogVersion(catalog(id[default=$contentCatalog]),version[default='Online'])[unique=true,default='$contentCatalog:Online']

INSERT_UPDATE Language;isocode[unique=true];active
;en;true

INSERT_UPDATE Currency;isocode[unique=true];name[lang=en];active;base;conversion;digits;symbol
;AUD;Australian Dollar;true;true;1;2;A$

INSERT_UPDATE Country;isocode[unique=true];name[lang=en];active[default=true]
;AU;"Australia"

INSERT_UPDATE Region;isocode[unique=true];name[lang=en];abbreviation[lang=en];active[default=true];country(isocode)
;AU-NSW;New South Wales;NSW;;AU
;AU-QLD;Queensland;QLD;;AU
;AU-SA;South Australia;SA;;AU
;AU-TAS;Tasmania;TAS;;AU
;AU-VIC;Victoria;VIC;;AU
;AU-WA;Western Australia;WA;;AU
;AU-ACT;Australian Capital Territory;ACT;;AU
;AU-NT;Northern Territory;NT;;AU

INSERT_UPDATE Vendor;code[unique=true];name
;Fastline;Fastline
;Target;Target
;Incomm;Incomm
;Cnp;Cnp

INSERT_UPDATE Warehouse;code[unique=true];name;vendor(code);default;surfaceStockOnline;integrationMethod(code);warehouseAutoTrigger(code);externalWarehouse[default=false]
;FastlineWarehouse;Fastline;Fastline;true;true;ESB;NONE
;IncommWarehouse;Incomm;Incomm;false;true;WEBMETHODS;SHIP;true
;CnpWarehouse;Cnp;Cnp;false;true;WEBMETHODS;NONE;true
;ConsolidatedStoreWarehouse;ConsolidatedStoreWarehouse;Target;true;true;ESB;NONE


INSERT_UPDATE UserTaxGroup;code[unique=true]
;aus-taxes

INSERT_UPDATE ProductTaxGroup;code[unique=true]
;aus-gst-1000
;aus-gst-500
;aus-gst-0

INSERT_UPDATE TargetTax;code[unique=true];value;currency(isocode);default[default=false]
;aus-gst-1000;10;;true
;aus-gst-500;5;;
;aus-gst-0;0;;

INSERT_UPDATE Unit;unitType[unique=true];code[unique=true];name[lang=en];conversion
;pieces;pieces;pieces;1

INSERT_UPDATE ProductType;code[unique=true];name;bulky
;normal;Normal;false
;bulky1;Bulky1;true
;bulky2;Bulky2;true
;mhd;MHD;true
;digital;Digital;false

INSERT_UPDATE Zone;code[unique=true];countries(isocode)
;australia;AU
PostCodeAwareZone;express;AU

INSERT_UPDATE TargetZoneDeliveryMode;code[unique=true];net;active[default=true];isDeliveryToStore;displayOrder;default[default=false];deliveryPromotionRank
;click-and-collect;false;true;true;1;;3
;home-delivery;false;true;false;2;true;2
;express-delivery;false;true;false;3;;1
;eBay-delivery;false;true;false;4;;4
;digital-gift-card;false;true;false;5;;5
;trademe-delivery;false;true;false;6;;6
;ebay-click-and-collect;false;true;true;7;;7
;ebay-express-delivery;false;true;false;8;;8

INSERT_UPDATE Catalog;id[unique=true];name[lang=en];defaultCatalog
;$productCatalog;$productCatalog;true
;$contentCatalog;$contentCatalog;true

INSERT_UPDATE CatalogVersion;catalog(id)[unique=true];version[unique=true];active;defaultCurrency(isocode);languages(isoCode)
;$productCatalog;Staged;false;$defaultCurrency;$languages
;$productCatalog;Online;true;$defaultCurrency;$languages
;$contentCatalog;Staged;false;$defaultCurrency;$languages
;$contentCatalog;Online;true;$defaultCurrency;$languages

INSERT_UPDATE ClassificationSystem;id[unique=true]
;$classificationCatalog

INSERT_UPDATE ClassificationSystemVersion;catalog(id)[unique=true];version[unique=true];active;inclPacking[virtual=true,default=true];inclDuty[virtual=true,default=true];inclFreight[virtual=true,default=true];inclAssurance[virtual=true,default=true]
;$classificationCatalog;1.0;true

INSERT_UPDATE UserGroup;uid[unique=true];groups(uid);
;customergroup;
;cockpitgroup;

INSERT_UPDATE TaxRow;$stagedProduct;tax(code)[unique=true];pg(code)[unique=true];ug(code)[unique=true]
;;aus-gst-1000;aus-gst-1000;aus-taxes
;;aus-gst-500;aus-gst-500;aus-taxes
;;aus-gst-0;aus-gst-0;aus-taxes

INSERT_UPDATE TaxRow;$onlineProduct;tax(code)[unique=true];pg(code)[unique=true];ug(code)[unique=true]
;;aus-gst-1000;aus-gst-1000;aus-taxes
;;aus-gst-500;aus-gst-500;aus-taxes
;;aus-gst-0;aus-gst-0;aus-taxes

INSERT_UPDATE Domain;code[unique=true];name;
;ticketSystemDomain;Ticket System Domain;

INSERT_UPDATE Component;code[unique=true];name;domain[unique=true](code);
;ticketSystem;Ticket System;ticketSystemDomain;

INSERT_UPDATE CommentType;code[unique=true];name;domain[unique=true](code);metaType(code)
;customerNote;Customer Note;ticketSystemDomain;CsCustomerEvent

INSERT_UPDATE StandardPaymentMode;code[unique=true];name[lang=en];description[lang=en];active;paymentinfotype(code);skipFraudRespValidation;allowedPaymentRetries;paymentRetryIntervalMillis
;creditcard;Credit Card;Payment with Credit Card;true;CreditCardPaymentInfo;false;1;30000
;paypal;PayPal;Payment with PayPal;true;PaypalPaymentInfo;false;1;30000
;pinpad;PinPad;Payment with PinPad;true;PinPadPaymentInfo;true;3;28800000

INSERT_UPDATE Title;code[unique=true]
;mr

INSERT_UPDATE TargetPointOfService;name[unique=true];storeNumber;type(code);fulfilmentCapability(&sfcId)
 ;Robina;7126;Target;robina-cap
 ;Rockhampton;7049;Target;rockhampton-cap
 ;Camberwell;7032;Target;camberwell-cap
 ;Morley;7043;Target;morley-cap

#Setup store with instore fulfilment capability.
INSERT_UPDATE StoreFulfilmentCapabilities;&sfcId;owner(TargetPointOfService.name)[unique=true];enabled;deliveryModes(code)[unique=true];allowDeliveryToThisStore;allowDeliveryToAnotherStore[default=false];fulfilmentPickPackDispatchStore[default=true]
 ;robina-cap;Robina;true;click-and-collect;true;
 ;rockhampton-cap;Rockhampton;true;click-and-collect;true;
 ;camberwell-cap;Camberwell;true;click-and-collect;true;
 ;morley-cap;Morley;true;click-and-collect;true;

 #Setup Warehouse for instore fulfilment capability
 INSERT_UPDATE Warehouse;code[unique=true];name;vendor(code);pointsOfService(name);default;integrationMethod(code)
;RobinaWarehouse;Robina;Target;Robina;false;NONE
;RockhamptonWarehouse;Rockhampton;Target;Rockhampton;false;NONE
;CamberwellWarehouse;Camberwell;Target;Camberwell;false;NONE
;MorleyWarehouse;Morley;Target;Morley;false;NONE
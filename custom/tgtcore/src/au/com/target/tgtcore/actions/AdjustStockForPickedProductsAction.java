/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;


/**
 * For a consignment will iterate over each entry and adjust the reserved stock and the available stock down.
 */
public class AdjustStockForPickedProductsAction extends AbstractProceduralAction<OrderProcessModel> {
    private static final Logger LOG = Logger.getLogger(AdjustStockForPickedProductsAction.class);
    private OrderProcessParameterHelper orderProcessParameterHelper;
    private TargetStockService targetStockService;
    private TargetWarehouseService targetWarehouseService;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException {
        Assert.notNull(process, "Order ProcessModel cannot be null");
        final ConsignmentModel currentConsignment = orderProcessParameterHelper.getConsignment(process);
        Assert.notNull(currentConsignment, "current consignment cannot be null");

        final Set<ConsignmentEntryModel> consignmentEntries = currentConsignment.getConsignmentEntries();
        if (CollectionUtils.isNotEmpty(consignmentEntries)) {
            final WarehouseModel warehouse = currentConsignment.getWarehouse();
            Assert.notNull(warehouse, "warehouse cannot be null");
            final String conCode = currentConsignment.getCode();
            final String msg = "Pick recieved for consignment=" + conCode;
            if (targetWarehouseService.isStockAdjustmentRequired(warehouse)) {
                for (final ConsignmentEntryModel conEnt : consignmentEntries) {
                    ProductModel product = null;
                    try {
                        product = conEnt.getOrderEntry().getProduct();
                        if (targetFeatureSwitchService
                                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)) {
                            if (TgtCoreConstants.FASTLINE_WAREHOUSE.equals(warehouse.getCode())) {
                                targetStockService.adjustActualAmount(product, warehouse, -conEnt.getShippedQuantity()
                                        .intValue(), msg);
                                //we need to release from RW warehouse for falcon
                                targetStockService.release(product, conEnt.getShippedQuantity().intValue(),
                                        "Releasing from RW");
                            }
                            else {
                                targetStockService.transferReserveToActualAmount(product,
                                        warehouse, conEnt.getShippedQuantity().intValue(), msg);
                            }
                        }
                        else {
                            targetStockService.transferReserveToActualAmount(product,
                                    warehouse, conEnt.getShippedQuantity().intValue(), msg);
                        }

                    }
                    catch (final Exception ex) {
                        if (product == null) {
                            LOG.error("Could not update stock level while processing pick for consignment=" + conCode,
                                    ex);
                        }
                        else {
                            LOG.error("Could not update stock level for product=" + product.getCode()
                                    + " while processing pick for consignment=" + conCode, ex);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }

    /**
     * @param targetStockService
     *            the targetStockService to set
     */
    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }

    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */
    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

}

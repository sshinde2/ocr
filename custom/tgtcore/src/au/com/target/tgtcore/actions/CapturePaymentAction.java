/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.task.RetryLaterException;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;
import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.methods.TargetPaymentMethod;
import au.com.target.tgtpayment.service.PaymentsInProgressService;
import au.com.target.tgtpayment.service.TargetPaymentService;
import au.com.target.tgtpayment.strategy.PaymentMethodStrategy;
import au.com.target.tgtpayment.util.PaymentTransactionHelper;
import au.com.target.tgtpayment.util.PriceCalculator;


/**
 * Action to trigger capture payment as part of business processes. Currently used to capture preOrder balance payment.
 * 
 * @author gsing236
 *
 */
public class CapturePaymentAction extends RetryActionWithSpringConfig<OrderProcessModel> {

    private static final String PREORDER_CAPTURE_BALANCE_PAYMENT_ERROR = "PREORDER_CAPTURE_BALANCE_PAYMENT_ERROR: OrderId={0}, message={1}";
    private static final String PREORDER_CAPTURE_BALANCE_PAYMENT = "PREORDER_CAPTURE_BALANCE_PAYMENT: OrderId={0}, message={1}";

    private static final Logger LOG = Logger.getLogger(CapturePaymentAction.class);

    private TargetPaymentService targetPaymentService;

    private PaymentMethodStrategy paymentMethodStrategy;

    private PaymentsInProgressService paymentsInProgressService;

    private FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    public enum Transition {
        OK, PAYMENT_FAILURE;

        public static Set<String> getStringValues() {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values()) {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    public Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }

    @Override
    public String executeInternal(final OrderProcessModel process) throws RetryLaterException, Exception {

        Assert.notNull(process, "Orderprocess cannot be null");
        final OrderModel orderModel = process.getOrder();
        Assert.notNull(orderModel, "Ordermodel cannot be null");

        if (orderModel.getNormalSaleStartDateTime() != null) {

            final Double amountPaidSoFar = findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(orderModel);
            if (PriceCalculator.equals(amountPaidSoFar, orderModel.getTotalPrice())) {

                // balance payment already captured.
                LOG.info(MessageFormat.format(PREORDER_CAPTURE_BALANCE_PAYMENT, orderModel.getCode(),
                        "Amount paid so far is equal to order total. Balance payment already captured and hence returning OK."));

                paymentsInProgressService.removeInProgressPayment(orderModel);
                return Transition.OK.toString();
            }

            final PaymentInfoModel paymentInfoModel = orderModel.getPaymentInfo();
            final CurrencyModel currency = orderModel.getCurrency();
            PaymentTransactionModel paymentTxnModel;
            final Double preOrderDepositAmount = orderModel.getPreOrderDepositAmount();
            final Double totalPrice = orderModel.getTotalPrice();
            final double balanceAmount = totalPrice.doubleValue() - preOrderDepositAmount.doubleValue();
            final BigDecimal amountToCapture = BigDecimal.valueOf(balanceAmount);
            final TargetPaymentMethod paymentMethod = paymentMethodStrategy.getPaymentMethod(paymentInfoModel);

            // check if customer updated CC while the order was in PARKED state
            PaymentTransactionEntryModel balancePaymentTransactionEntryModel = PaymentTransactionHelper
                    .findTransactionEntryWithState(
                            orderModel.getPaymentTransactions(), PaymentTransactionType.DEFERRED);

            if (balancePaymentTransactionEntryModel != null) {

                paymentTxnModel = balancePaymentTransactionEntryModel.getPaymentTransaction();
            }
            else {
                // DEFERRED ptem can be null for preOrder if customer haven't updated the CC details. If so, create a deferred one first
                // and then proceed to take preOrder balance payment
                final TargetQueryTransactionDetailsResult result = targetPaymentService.queryTransactionDetails(
                        paymentMethod,
                        paymentInfoModel);
                if (result == null || !result.isSuccess()) {
                    // call to IPG failed
                    LOG.error(MessageFormat.format(PREORDER_CAPTURE_BALANCE_PAYMENT_ERROR, orderModel.getCode(),
                            "IPG query transaction details did not return successful result"));
                    throw new RetryLaterException(
                            MessageFormat.format(PREORDER_CAPTURE_BALANCE_PAYMENT_ERROR, orderModel.getCode(),
                                    "IPG query transaction details did not return successful result, sending for retry"));
                }

                paymentTxnModel = targetPaymentService
                        .createTransactionWithQueryResult(orderModel, result, PaymentTransactionType.DEFERRED);

                balancePaymentTransactionEntryModel = PaymentTransactionHelper.findTransactionEntryWithState(
                        Arrays.asList(paymentTxnModel), PaymentTransactionType.DEFERRED);
            }

            // remove, if triggered by in-progress payment job
            paymentsInProgressService.removeInProgressPayment(orderModel);
            //verify if payment is captured already
            final boolean isPaymentCapturedAndUpdated = targetPaymentService
                    .verifyIfPaymentCapturedAndUpdateEntry(balancePaymentTransactionEntryModel);
            // payment not already captured, the capture the payment.
            if (!isPaymentCapturedAndUpdated) {
                targetPaymentService.capture(orderModel, paymentInfoModel, amountToCapture,
                        currency, PaymentCaptureType.RELEASEPREORDER, paymentTxnModel);
            }

            final String captureBalanceTransactionStatus = balancePaymentTransactionEntryModel.getTransactionStatus();
            if (TransactionStatus.REVIEW.toString().equals(captureBalanceTransactionStatus)) {
                // if transaction status is REVIEW, means capture payment failed with communication error to IPG
                paymentsInProgressService.removeInProgressPayment(orderModel);
                LOG.error(MessageFormat.format(PREORDER_CAPTURE_BALANCE_PAYMENT_ERROR, orderModel.getCode(),
                        "Capturing balance payment failed with possible connection issues with IPG."));
                throw new RetryLaterException(
                        MessageFormat.format(PREORDER_CAPTURE_BALANCE_PAYMENT_ERROR, orderModel.getCode(),
                                "Capturing balance payment failed with possible connection issues with IPG, send for retry"));
            }

            if (TransactionStatus.ERROR.toString().equals(captureBalanceTransactionStatus)
                    || TransactionStatus.REJECTED.toString().equals(captureBalanceTransactionStatus)) {

                paymentTxnModel.setIsCaptureSuccessful(Boolean.FALSE);
                getModelService().save(paymentTxnModel);
                getModelService().refresh(paymentTxnModel);
                paymentsInProgressService.removeInProgressPayment(orderModel);
                LOG.error(MessageFormat.format(PREORDER_CAPTURE_BALANCE_PAYMENT_ERROR, orderModel.getCode(),
                        "Capturing balance payment failed, returning PAYMENT_FAILURE."));
                return Transition.PAYMENT_FAILURE.toString();
            }

            balancePaymentTransactionEntryModel.getIpgPaymentInfo().setIsPaymentSucceeded(Boolean.TRUE);
            balancePaymentTransactionEntryModel.setType(PaymentTransactionType.CAPTURE);
            paymentTxnModel.setIsCaptureSuccessful(Boolean.TRUE);
            getModelService().save(balancePaymentTransactionEntryModel);
            getModelService().refresh(balancePaymentTransactionEntryModel);
            getModelService().save(paymentTxnModel);
            getModelService().refresh(paymentTxnModel);

            paymentsInProgressService.removeInProgressPayment(orderModel);

        }
        return Transition.OK.toString();
    }

    /**
     * Lookup for TargetBusinessProcessService
     *
     * @return TargetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

    /**
     * @param targetPaymentService
     *            the targetPaymentService to set
     */
    @Required
    public void setTargetPaymentService(final TargetPaymentService targetPaymentService) {
        this.targetPaymentService = targetPaymentService;
    }

    /**
     * @param paymentMethodStrategy
     *            the paymentMethodStrategy to set
     */
    @Required
    public void setPaymentMethodStrategy(final PaymentMethodStrategy paymentMethodStrategy) {
        this.paymentMethodStrategy = paymentMethodStrategy;
    }

    /**
     * @param paymentsInProgressService
     *            the paymentsInProgressService to set
     */
    @Required
    public void setPaymentsInProgressService(final PaymentsInProgressService paymentsInProgressService) {
        this.paymentsInProgressService = paymentsInProgressService;
    }

    /**
     * @param findOrderTotalPaymentMadeStrategy
     *            the findOrderTotalPaymentMadeStrategy to set
     */
    @Required
    public void setFindOrderTotalPaymentMadeStrategy(
            final FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy) {
        this.findOrderTotalPaymentMadeStrategy = findOrderTotalPaymentMadeStrategy;
    }

}

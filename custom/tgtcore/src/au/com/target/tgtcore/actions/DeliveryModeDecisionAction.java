package au.com.target.tgtcore.actions;

import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;


/**
 * Action that just returns the code of DeliveryModeModel on the consignment
 * 
 */
public class DeliveryModeDecisionAction extends AbstractAction<OrderProcessModel> {

    private OrderProcessParameterHelper orderProcessParameterHelper;

    public enum Transition
    {
        SHIP, PICK_UP;

        public static Set<String> getStringValues()
        {
            final Set<String> res = new HashSet<>();
            for (final Transition transisions : Transition.values())
            {
                res.add(transisions.toString());
            }
            return res;
        }
    }

    @Override
    public String execute(final OrderProcessModel process) throws RetryLaterException, Exception {

        // Get the consignment out of the process parameters
        final ConsignmentModel consignment = orderProcessParameterHelper.getConsignment(process);
        Assert.notNull(consignment, "Consignment cannot be null");

        final DeliveryModeModel deliveryMode = consignment.getDeliveryMode();
        Assert.notNull(deliveryMode, "DeliveryModeModel cannot be empty!");

        if (deliveryMode instanceof TargetZoneDeliveryModeModel) {
            final TargetZoneDeliveryModeModel targetZoneDeliveryMode = (TargetZoneDeliveryModeModel)deliveryMode;

            if (BooleanUtils.isTrue(targetZoneDeliveryMode.getIsDeliveryToStore())) {
                return Transition.PICK_UP.name();
            }

        }

        return Transition.SHIP.name();
    }

    @Override
    public Set<String> getTransitions()
    {
        return Transition.getStringValues();
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }

}

/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysConsumeResponseDto;
import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;
import au.com.target.tgtcore.flybuys.service.FlybuysDiscountService;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * The Class FlybuysConsumeAction.
 * 
 * @author umesh
 */
public class FlybuysConsumeAction extends RetryActionWithSpringConfig<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(FlybuysConsumeAction.class);

    private FlybuysDiscountService flybuysDiscountService;

    public enum Transition
    {
        ACCEPT, REJECT, RETRYEXCEEDED;

        public static Set<String> getStringValues()
        {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values())
            {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    protected String executeInternal(final OrderProcessModel process) throws RetryLaterException, Exception {

        final OrderModel order = process.getOrder();
        Assert.notNull(order, "Order cannot be null");

        return getTransition(getFlybuysDiscountService().consumeFlybuysPoints(order), order);
    }

    /**
     * Gets the transition.
     * 
     * @param status
     *            the status
     * @param order
     *            the order
     * @return the transition
     * @throws RetryLaterException
     *             the retry later exception
     */
    protected String getTransition(final FlybuysConsumeResponseDto status, final OrderModel order)
            throws RetryLaterException {

        Transition result = Transition.ACCEPT;

        if (status != null)
        {
            if (status.getResponse().equals(FlybuysResponseType.UNAVAILABLE))
            {
                throw new RetryLaterException("Flybuys service unavailable for order: " + order.getCode()
                        + ", Response Message : " + status.getResponse());
            }
            else if (status.getResponse().equals(FlybuysResponseType.INVALID)
                    || status.getResponse().equals(FlybuysResponseType.FLYBUYS_OTHER_ERROR))
            {
                result = Transition.REJECT;
                LOG.warn(SplunkLogFormatter.formatMessage(
                        "FlybuysConsumeAction REJECT due to response status=" + status.getResponse(),
                        TgtutilityConstants.ErrorCode.WARN_PAYMENT, order.getCode()));
            }
            else
            {
                result = Transition.ACCEPT;
            }

        }

        return result.toString();
    }

    /**
     * @return the flybuysDiscountService
     */
    protected FlybuysDiscountService getFlybuysDiscountService() {
        return flybuysDiscountService;
    }

    /**
     * @param flybuysDiscountService
     *            the flybuysDiscountService to set
     */
    @Required
    public void setFlybuysDiscountService(final FlybuysDiscountService flybuysDiscountService) {
        this.flybuysDiscountService = flybuysDiscountService;
    }

    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }

}

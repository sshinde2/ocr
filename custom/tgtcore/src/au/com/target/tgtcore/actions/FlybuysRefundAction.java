/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysRefundResponseDto;
import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;
import au.com.target.tgtcore.flybuys.service.FlybuysDiscountService;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * @author Nandini
 * 
 */
public class FlybuysRefundAction extends RetryActionWithSpringConfig<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(FlybuysRefundAction.class);
    private FlybuysDiscountService flybuysDiscountService;

    public enum Transition {
        OK, RETRYEXCEEDED;

        public static Set<String> getStringValues()
        {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values())
            {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    protected String executeInternal(final OrderProcessModel process) throws RetryLaterException, Exception {
        final OrderModel order = process.getOrder();
        Assert.notNull(order, "Order cannot be null");

        return getTransition(flybuysDiscountService.refundFlybuysPoints(order), order);
    }

    /**
     * Gets the transition.
     * 
     * @param status
     *            the status
     * @param order
     *            the order
     * @return the transition
     * @throws RetryLaterException
     *             the retry later exception
     */
    protected String getTransition(final FlybuysRefundResponseDto status, final OrderModel order)
            throws RetryLaterException {
        final Transition result = Transition.OK;
        if (status != null)
        {
            if (!status.getResponse().equals(FlybuysResponseType.SUCCESS))
            {
                LOG.warn(SplunkLogFormatter.formatMessage(
                        "FlybuysRefundAction FAILURE due to response status=" + status.getResponse(),
                        TgtutilityConstants.ErrorCode.WARN_PAYMENT, order.getCode()));
                throw new RetryLaterException("Flybuys points refund failure order: " + order.getCode()
                        + ", Response Message : " + status.getResponse());
            }
        }
        return result.toString();
    }

    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }

    /**
     * @param flybuysDiscountService
     *            the flybuysDiscountService to set
     */
    @Required
    public void setFlybuysDiscountService(final FlybuysDiscountService flybuysDiscountService) {
        this.flybuysDiscountService = flybuysDiscountService;
    }

}

package au.com.target.tgtcore.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtpayment.util.PriceCalculator;


/**
 * Refund for the entire order value.
 */
public class FullRefundOrderAction extends RefundOrderAction {

    protected FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    @Override
    public Transition executeAction(final OrderProcessModel orderProcess) throws RetryLaterException,
            Exception {

        final OrderModel order = orderProcess.getOrder();

        Assert.notNull(order, "Order can't be empty!");

        // Refund amount paid so far
        final Double dPaidSoFar = findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(order);

        final BigDecimal refundAmount = PriceCalculator.getAsPrice(dPaidSoFar);
        getOrderProcessParameterHelper().setRefundAmount(orderProcess, refundAmount);

        return refundAmount(orderProcess, order, refundAmount);
    }

    /**
     * @param findOrderTotalPaymentMadeStrategy
     *            the findOrderTotalPaymentMadeStrategy to set
     */
    @Required
    public void setFindOrderTotalPaymentMadeStrategy(
            final FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy) {
        this.findOrderTotalPaymentMadeStrategy = findOrderTotalPaymentMadeStrategy;
    }

}
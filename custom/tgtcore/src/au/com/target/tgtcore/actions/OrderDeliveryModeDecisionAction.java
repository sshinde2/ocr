package au.com.target.tgtcore.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.util.Assert;


/**
 * Action that just returns the code of DeliveryModeModel on the consignment
 * 
 */
public class OrderDeliveryModeDecisionAction extends AbstractAction<OrderProcessModel> {

    public enum Transition
    {
        SHIP, PICK_UP;

        public static Set<String> getStringValues()
        {
            final Set<String> res = new HashSet<>();
            for (final Transition transisions : Transition.values())
            {
                res.add(transisions.toString());
            }
            return res;
        }
    }

    @Override
    public String execute(final OrderProcessModel process) throws RetryLaterException, Exception {
        final OrderModel orderModel = process.getOrder();
        Assert.notNull(orderModel, "Order cannot be null");

        final DeliveryModeModel deliveryMode = orderModel.getDeliveryMode();
        Assert.notNull(deliveryMode, "DeliveryModeModel cannot be empty!");

        if (deliveryMode instanceof TargetZoneDeliveryModeModel) {
            final TargetZoneDeliveryModeModel targetZoneDeliveryMode = (TargetZoneDeliveryModeModel)deliveryMode;

            if (BooleanUtils.isTrue(targetZoneDeliveryMode.getIsDeliveryToStore())) {
                return Transition.PICK_UP.name();
            }

        }

        return Transition.SHIP.name();
    }

    @Override
    public Set<String> getTransitions()
    {
        return Transition.getStringValues();
    }

}

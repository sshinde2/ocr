/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;




/**
 * Generic cs ticket action for when refund failed
 */
public class RefundFailedSendCsAgentTicketAction extends AbstractProceduralAction<OrderProcessModel> {
    private static final Logger LOG = Logger.getLogger(RefundFailedSendCsAgentTicketAction.class);
    private TargetTicketBusinessService targetTicketBusinessService;
    private OrderProcessParameterHelper orderProcessParameterHelper;

    private String headline;
    private String subject;
    private String text;


    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {

        final StringBuilder subjectText = new StringBuilder();
        final StringBuilder bodyText = new StringBuilder();
        final String refundedAmountInfo = orderProcessParameterHelper.getRefundedAmountInfo(process);
        subjectText.append(subject);
        bodyText.append(text);
        if (StringUtils.isNotEmpty(refundedAmountInfo)) {
            subjectText.append(" | Refunded amount information is that").append(refundedAmountInfo);
        }

        // TODO: for short pick and partial cancel, extend this action to add the specific information about the modified items
        final OrderModel order = process.getOrder();
        if (order != null) {
            final List<AbstractOrderEntryModel> orderEntries = order.getEntries();
            if (CollectionUtils.isNotEmpty(orderEntries)) {
                bodyText.append("\n Order entries: ");
            }
            for (final AbstractOrderEntryModel entry : orderEntries) {
                final String itemCode = entry.getProduct().getCode();
                final String qtyPicked = entry.getQuantity().toString();
                bodyText.append(itemCode).append(" - ").append(entry.getProduct().getName()).append(" - ")
                        .append(qtyPicked)
                        .append(" - ").append(entry.getBasePrice()).append('\n');
            }
        }

        targetTicketBusinessService.createCsAgentTicket(headline, subjectText.toString(), bodyText.toString(), order,
                TgtCoreConstants.CsAgentGroup.REFUND_FAILED_CS_AGENT_GROUP);

        final StringBuilder logMsg = new StringBuilder();
        logMsg.append("Successfully raised ticket for cs agent");
        if (order != null) {
            logMsg.append(" for order:").append(order.getCode());
        }
        LOG.info(logMsg);
    }

    /**
     * @param targetTicketBusinessService
     *            the targetTicketBusinessService to set
     */
    @Required
    public void setTargetTicketBusinessService(final TargetTicketBusinessService targetTicketBusinessService) {
        this.targetTicketBusinessService = targetTicketBusinessService;
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }

    /**
     * @param headline
     *            the headline to set
     */
    @Required
    public void setHeadline(final String headline) {
        this.headline = headline;
    }

    /**
     * @param subject
     *            the subject to set
     */
    @Required
    public void setSubject(final String subject) {
        this.subject = subject;
    }

    /**
     * @param text
     *            the text to set
     */
    @Required
    public void setText(final String text) {
        this.text = text;
    }


}

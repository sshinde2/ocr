/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtpayment.service.TargetPaymentService;
import au.com.target.tgtpayment.util.PaymentTransactionHelper;


public abstract class RefundOrderAction extends AbstractSimpleDecisionAction<OrderProcessModel> {
    private static final Logger LOG = Logger.getLogger(RefundOrderAction.class);

    private static final String PARCIAL_REFUND_SUCCESS = "Total amount need to be refunded={0}. System has already succeeded to refund amount={1} automatically, another amount={2} need to be refunded manually.";
    private static final String FULL_REFUND_FAILURE = "System failed to refund automatically, refund amount={0} need to be refunded manually.";
    private static final String REFUND_INFOR = "Refund follow on detail information with total refund amount required={0}, and refunded amount={1} and refund amount remains={2} in the transaction={3} in order={4}.";
    protected TargetPaymentService targetPaymentService;
    private OrderProcessParameterHelper orderProcessParameterHelper;

    protected Transition refundAmount(final OrderProcessModel orderProcess, final OrderModel order,
            final BigDecimal refundAmount) {
        Assert.notNull(order);
        Assert.notNull(refundAmount);

        final List<PaymentTransactionModel> paymentTransactions = order.getPaymentTransactions();

        if (CollectionUtils.isEmpty(paymentTransactions)) {
            LOG.error("Payment Transactions empty: order=" + order.getCode());
            return Transition.NOK;
        }

        final PaymentTransactionModel paymentTransaction = PaymentTransactionHelper.findCaptureTransaction(order);

        if (paymentTransaction == null) {
            LOG.error("Cannot find a successful capture transaction against the order:"
                    + order.getCode());
            return Transition.NOK;
        }

        try {
            final List<PaymentTransactionEntryModel> refundedEntryList = targetPaymentService.refundFollowOn(
                    paymentTransaction, refundAmount);
            final BigDecimal refundedAmount = targetPaymentService
                    .findRefundedAmountAfterRefundFollowOn(refundedEntryList);
            orderProcessParameterHelper.setRefundPaymentEntryList(orderProcess, refundedEntryList);
            if (refundAmount.compareTo(refundedAmount) != 0) {
                final BigDecimal refundRemains = refundAmount.subtract(refundedAmount);
                if (refundedAmount.compareTo(BigDecimal.ZERO) > 0) {
                    orderProcessParameterHelper
                            .setRefundedAmountInfo(
                                    orderProcess,
                                    MessageFormat
                                            .format(PARCIAL_REFUND_SUCCESS,
                                                    refundAmount, refundedAmount, refundRemains));
                }
                else {
                    orderProcessParameterHelper
                            .setRefundedAmountInfo(
                                    orderProcess,
                                    MessageFormat
                                            .format(FULL_REFUND_FAILURE,
                                                    refundAmount));
                }
                LOG.info(MessageFormat
                        .format(REFUND_INFOR,
                                refundAmount, refundedAmount, refundRemains, paymentTransaction.getPk(),
                                paymentTransaction.getOrder() != null ? paymentTransaction.getOrder().getPk()
                                        : StringUtils.EMPTY));
                return Transition.NOK;
            }


        }
        catch (final Exception e) {
            LOG.warn("refund follow on error " + e.getMessage());
            return Transition.NOK;
        }


        return Transition.OK;
    }


    /**
     * @param targetPaymentService
     *            the targetPaymentService to set
     */
    @Required
    public void setTargetPaymentService(final TargetPaymentService targetPaymentService) {
        this.targetPaymentService = targetPaymentService;
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }


    /**
     * @return the orderProcessParameterHelper
     */
    protected OrderProcessParameterHelper getOrderProcessParameterHelper() {
        return orderProcessParameterHelper;
    }



}

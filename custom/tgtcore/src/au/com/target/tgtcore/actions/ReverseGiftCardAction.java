/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtpayment.dto.GiftCardReversalData;
import au.com.target.tgtpayment.service.TargetPaymentService;


/**
 * @author htan3
 *
 */
public class ReverseGiftCardAction extends RetryActionWithSpringConfig<BusinessProcessModel> {
    private static final Logger LOG = Logger.getLogger(ReverseGiftCardAction.class);

    private TargetPaymentService paymentService;

    public enum Transition {
        OK, NOK;

        public static Set<String> getStringValues() {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values()) {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    protected String executeInternal(final BusinessProcessModel process) throws RetryLaterException {
        String result = null;
        final OrderProcessParameterHelper orderProcessParameterHelper = getOrderProcessParameterHelper();
        final PaymentTransactionEntryModel captureEntry = orderProcessParameterHelper
                .getGiftCardReversalCaptureEntry(process);
        if (captureEntry == null) {
            final GiftCardReversalData reversalData = (GiftCardReversalData)orderProcessParameterHelper
                    .getGiftCardReversalData(process);
            result = reverseByReversalData(reversalData);
        }
        else {
            result = reverseByCaptureEntry(captureEntry);
        }
        return result;
    }

    /**
     * @param captureEntry
     * @return result
     * @throws RetryLaterException
     */
    private String reverseByCaptureEntry(final PaymentTransactionEntryModel captureEntry) throws RetryLaterException {
        final String result;
        boolean succeed = false;
        try {
            final PaymentTransactionEntryModel refundEntry = paymentService.refundFollowOn(captureEntry,
                    captureEntry.getAmount());
            succeed = TransactionStatus.ACCEPTED.toString().equals(refundEntry.getTransactionStatus());
        }
        catch (final Exception e) {
            throw new RetryLaterException("Failed to perform giftcard reversal", e);
        }
        if (succeed) {
            result = Transition.OK.toString();
        }
        else {
            throw new RetryLaterException("Failed to perform giftcard reversal");
        }
        return result;
    }


    /**
     * @param reversalData
     * @return result
     * @throws RetryLaterException
     */
    private String reverseByReversalData(final GiftCardReversalData reversalData)
            throws RetryLaterException {
        final String result;
        if (reversalData == null) {
            LOG.error("invalid parameter null");
            result = Transition.NOK.toString();
        }
        else {
            boolean succeed = false;
            try {
                succeed = paymentService.reversePayment(reversalData);
            }
            catch (final Exception e) {
                throw new RetryLaterException("Failed to perform giftcard reversal", e);
            }
            if (succeed) {
                result = Transition.OK.toString();
            }
            else {
                throw new RetryLaterException("Failed to perform giftcard reversal");
            }
        }
        return result;
    }


    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }

    /**
     * @param paymentService
     *            the paymentService to set
     */
    @Required
    public void setPaymentService(final TargetPaymentService paymentService) {
        this.paymentService = paymentService;
    }


}

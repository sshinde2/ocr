/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.actions.SendEmailAction;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;


/**
 * Action responsible to make a decision point for sending Order confirmation emails.
 * 
 */
public class SendOrderConfEmailDecisionPoint extends SendEmailAction {

    private static final Logger LOG = Logger.getLogger(SendOrderConfEmailDecisionPoint.class);

    private TargetSalesApplicationConfigService salesApplicationConfigService;

    @Override
    public Transition executeAction(final BusinessProcessModel process) throws RetryLaterException, Exception {

        Assert.notNull(process, "Process cannot be null");

        if (process instanceof OrderProcessModel) {
            final OrderModel order = ((OrderProcessModel)process).getOrder();
            Assert.notNull(order, "Order cannot be null");

            if (salesApplicationConfigService.isSuppressOrderConfMail(order.getSalesApplication())) {
                LOG.info("Sending order confirmation email is suppressed: OrderCode=" + order.getCode()
                        + ", SalesApplication=" + order.getSalesApplication());
                return Transition.OK;
            }
        }
        return super.executeAction(process);
    }

    /**
     * @param salesApplicationConfigService
     *            the salesApplicationConfigService to set
     */
    @Required
    public void setSalesApplicationConfigService(
            final TargetSalesApplicationConfigService salesApplicationConfigService) {
        this.salesApplicationConfigService = salesApplicationConfigService;
    }

}

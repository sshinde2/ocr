/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;
import au.com.target.tgtcore.sms.TargetSendSmsService;
import au.com.target.tgtcore.sms.dto.TargetSendSmsResponseDto;
import au.com.target.tgtcore.sms.enums.TargetSendSmsResponseType;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * @author vivek
 * 
 */
public class SendSmsToStoreForOpenOrdersAction extends RetryActionWithSpringConfig<BusinessProcessModel> {

    private static final Logger LOG = Logger.getLogger(SendSmsToStoreForOpenOrdersAction.class);

    private TargetSendSmsService targetSendSmsService;

    public enum Transition {
        OK, RETRYEXCEEDED, NOK;

        public static Set<String> getStringValues() {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values()) {
                res.add(t.toString());
            }
            return res;
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtbusproc.actions.RetryAction#executeInternal(de.hybris.platform.processengine.model.BusinessProcessModel)
     */
    @Override
    protected String executeInternal(final BusinessProcessModel process) throws RetryLaterException, Exception {
        final TargetSendSmsResponseDto targetSendSmsResponseDto;

        Assert.notNull(process, "process cannot be null");
        LOG.info(MessageFormat.format("Attempt to send sms message for process={0}", process.getCode()));
        targetSendSmsResponseDto = targetSendSmsService.sendSmsToStoreForOpenOrders(process);
        return getTransition(targetSendSmsResponseDto);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtbusproc.actions.RetryAction#getTransitionsInternal()
     */
    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }

    /**
     * Gets the transition.
     * 
     * @param targetSendSmsResponseDto
     *            the targetSendSmsResponseDto the order
     * @return the transition
     * @throws RetryLaterException
     *             the retry later exception
     */
    protected String getTransition(final TargetSendSmsResponseDto targetSendSmsResponseDto)
            throws RetryLaterException {

        Transition result = Transition.NOK;
        if (targetSendSmsResponseDto != null) {

            if (TargetSendSmsResponseType.UNAVAILABLE.equals(targetSendSmsResponseDto.getResponseType())) {
                throw new RetryLaterException("Sms service is unavailable for this store.");

            }
            else if (TargetSendSmsResponseType.INVALID.equals(targetSendSmsResponseDto.getResponseType())) {
                result = Transition.NOK;
                LOG.error(SplunkLogFormatter.formatMessage("Sms service has returned INVALID",
                        TgtutilityConstants.ErrorCode.WARN_SMS_SEND_FAIL));

            }
            else if (TargetSendSmsResponseType.OTHERS.equals(targetSendSmsResponseDto.getResponseType())) {
                result = Transition.NOK;
                LOG.warn(SplunkLogFormatter.formatMessage(
                        "Sms service has returned error",
                        TgtutilityConstants.ErrorCode.WARN_SMS_SEND_FAIL));
            }
            else {
                result = Transition.OK;
            }
        }
        return result.toString();
    }

    /**
     * @param targetSendSmsService
     *            the targetSendSmsService to set
     */
    @Required
    public void setTargetSendSmsService(final TargetSendSmsService targetSendSmsService) {
        this.targetSendSmsService = targetSendSmsService;
    }

}

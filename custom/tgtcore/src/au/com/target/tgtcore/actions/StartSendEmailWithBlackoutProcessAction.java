/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;


/**
 * @author pratik
 *
 */
public class StartSendEmailWithBlackoutProcessAction extends AbstractSimpleDecisionAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(StartSendEmailWithBlackoutProcessAction.class);

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Override
    public Transition executeAction(final OrderProcessModel orderProcess) throws RetryLaterException, Exception {
        Assert.notNull(orderProcess, "OrderProcessModel can't be null");
        Assert.notNull(orderProcess.getOrder(), "OrderModel can't be null");

        if (targetFeatureSwitchService.sendEmailWithBlackout()) {
            try {
                getTargetBusinessProcessService().startSendEmailWithBlackoutProcess(
                        orderProcess.getOrder(), orderProcessParameterHelper.getCncNotificationData(orderProcess));
            }
            catch (final Exception e) {
                LOG.error("Cannot start business process, order=" + (orderProcess.getOrder().getCode()), e);
            }
            return Transition.OK;
        }
        return Transition.NOK;
    }

    /**
     * Lookup for TargetBusinessProcessService
     * 
     * @return TargetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }
}

/**
 * 
 */
package au.com.target.tgtcore.attributes.dynamic;

import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import java.util.Date;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;


/**
 * @author siddharam
 *
 */
public class TargetNewLowerPriceFlagHandler implements
        DynamicAttributeHandler<Boolean, TargetColourVariantProductModel> {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler#get(de.hybris.platform.servicelayer.model.AbstractItemModel)
     */
    @Override
    public Boolean get(final TargetColourVariantProductModel model) {
        final Date startDate = model.getNewLowerPriceStartDate();
        final Date endDate = model.getNewLowerPriceEndDate();
        if (null != startDate && null != endDate) {
            if (isStartDateOnOrBeforeCurrentDate(startDate) && isEndDateGreaterThenCurrDate(endDate)) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    @Override
    public void set(final TargetColourVariantProductModel model, final Boolean arg1) {
        throw new UnsupportedOperationException();
    }

    /**
     * @param endDate
     * @return boolean
     */
    private boolean isEndDateGreaterThenCurrDate(final Date endDate) {
        return endDate.after(new Date());
    }


    /**
     * @param startDate
     * @return boolean
     */
    private boolean isStartDateOnOrBeforeCurrentDate(final Date startDate) {
        return startDate.equals(new Date()) || startDate.before(new Date());
    }

}

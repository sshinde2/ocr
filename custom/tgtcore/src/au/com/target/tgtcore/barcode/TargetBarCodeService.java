/**
 * 
 */
package au.com.target.tgtcore.barcode;

import java.io.ByteArrayOutputStream;

import au.com.target.tgtcore.exception.GenerateBarCodeFailedException;



public interface TargetBarCodeService {

    /**
     * Generate SVG format Barcode
     * 
     * @param encodedOrderId
     * @return ByteArrayOutputStream
     */
    ByteArrayOutputStream generateSvgBarcode(final String encodedOrderId)
            throws GenerateBarCodeFailedException;

    /**
     * Generate PNG format Barcode
     * 
     * @param encodedOrderId
     * @return ByteArrayOutputStream
     * @throws GenerateBarCodeFailedException
     */
    ByteArrayOutputStream generatePngBarcode(final String encodedOrderId)
            throws GenerateBarCodeFailedException;

    /**
     * Get the Barcode from the image
     * 
     * @param orderId
     * @return String
     */
    String getPlainBarcode(final String orderId);

    /**
     * Get Plain Barcode Without CheckDigit
     * 
     * @param orderId
     * @return String
     */
    String getPlainBarcodeWithoutCheckDigit(final String orderId);

}

/**
 * 
 */
package au.com.target.tgtcore.barcode.impl;

import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import org.apache.avalon.framework.configuration.DefaultConfiguration;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.krysalis.barcode4j.BarcodeGenerator;
import org.krysalis.barcode4j.BarcodeUtil;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.output.svg.SVGCanvasProvider;
import org.krysalis.barcode4j.tools.MimeTypes;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.barcode.BarcodeConfig;
import au.com.target.tgtcore.barcode.TargetBarCodeService;
import au.com.target.tgtcore.exception.GenerateBarCodeFailedException;
import au.com.target.tgtcore.util.OrderEncodingTools;
import au.com.target.tgtutility.util.BarcodeTools;


public class TargetBarCodeServiceImpl extends AbstractBusinessService implements TargetBarCodeService {

    private static final Logger LOG = Logger.getLogger(TargetBarCodeServiceImpl.class);

    private String storeNumber;

    private BarcodeConfig barcodeConfig;

    private DefaultConfiguration cfg = null;

    @Override
    public ByteArrayOutputStream generateSvgBarcode(final String encodedOrderId)
            throws GenerateBarCodeFailedException {

        Assert.notNull(encodedOrderId, "order id can't be null");

        final String decodedOrderId = OrderEncodingTools.decodeOrderId(encodedOrderId);
        final String barcode = storeNumber + decodedOrderId;

        final BarcodeUtil util = BarcodeUtil.getInstance();
        ByteArrayOutputStream bout = null;
        try {
            final BarcodeGenerator gen = util.createBarcodeGenerator(cfg);
            bout = new ByteArrayOutputStream(4096);

            //Create Barcode and render it to SVG
            final int orientation = 0;
            final SVGCanvasProvider svg = new SVGCanvasProvider(false, orientation);
            gen.generateBarcode(svg, barcode);
            final org.w3c.dom.DocumentFragment frag = svg.getDOMFragment();

            //Serialize SVG barcode
            final TransformerFactory factory = TransformerFactory.newInstance();
            final Transformer trans = factory.newTransformer();
            final Source src = new javax.xml.transform.dom.DOMSource(frag);
            final Result res = new javax.xml.transform.stream.StreamResult(bout);
            trans.transform(src, res);
        }
        catch (final Exception e) {
            LOG.error("Generation Barcode Error happened ... " + decodedOrderId);
            throw new GenerateBarCodeFailedException(e);
        }
        finally {
            IOUtils.closeQuietly(bout);
        }

        return bout;
    }

    @Override
    public ByteArrayOutputStream generatePngBarcode(final String encodedOrderId) throws GenerateBarCodeFailedException {
        Assert.notNull(encodedOrderId, "order id can't be null");

        final String decodedOrderId = OrderEncodingTools.decodeOrderId(encodedOrderId);
        final String barcode = storeNumber + decodedOrderId;

        final BarcodeUtil util = BarcodeUtil.getInstance();

        ByteArrayOutputStream bout = null;

        try {
            final BarcodeGenerator gen = util.createBarcodeGenerator(this.cfg);
            bout = new ByteArrayOutputStream(4096);

            final int resolution = Integer.parseInt(barcodeConfig.getBarcodeDpi());
            final int orientation = 0;
            final BitmapCanvasProvider bitmap = new BitmapCanvasProvider(
                    bout, MimeTypes.MIME_PNG, resolution,
                    BufferedImage.TYPE_BYTE_BINARY, false, orientation);
            gen.generateBarcode(bitmap, barcode);
            bitmap.finish();
        }
        catch (final Exception e) {
            LOG.error("Generation Barcode Error happened ... " + decodedOrderId);
            throw new GenerateBarCodeFailedException(e);
        }
        finally {
            IOUtils.closeQuietly(bout);
        }

        return bout;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.barcode.TargetBarCodeService#getPlainBarcode(java.lang.String)
     */
    @Override
    public String getPlainBarcode(final String encodedOrderId) {

        Assert.notNull(encodedOrderId, "Encoded Order Id can't be empty");
        final String orderId = OrderEncodingTools.decodeOrderId(encodedOrderId);
        final char checkDigit = BarcodeTools.findEANCheckDigit(storeNumber + orderId);

        final String plainBarcode = new StringBuilder(storeNumber).append(orderId).append(checkDigit)
                .toString();

        return plainBarcode;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.barcode.TargetBarCodeService#getPlainBarcode(java.lang.String)
     */
    @Override
    public String getPlainBarcodeWithoutCheckDigit(final String orderId) {

        Assert.notNull(orderId, "Encoded Order Id can't be empty");
        final String plainBarcode = new StringBuilder(storeNumber).append(orderId).toString();
        return plainBarcode;
    }

    /**
     * Init Barcode Configuration
     * 
     */
    public void initBarcodeCfg() {

        // do some pre-check
        Assert.notNull(storeNumber);
        storeNumber = storeNumber.trim();

        if (cfg != null) {
            return;
        }

        cfg = new DefaultConfiguration("barcode");
        //Get type
        DefaultConfiguration child = null;
        String barcodeType = barcodeConfig.getBarcodeType();
        if (StringUtils.isEmpty(barcodeType)) {
            barcodeType = "EAN13";
        }

        child = new DefaultConfiguration(barcodeType);
        cfg.addChild(child);

        //Get additional attributes
        final String height = barcodeConfig.getBarcodeHeight();
        if (StringUtils.isNotEmpty(height)) {
            final DefaultConfiguration attr = new DefaultConfiguration("height");
            attr.setValue(height);
            child.addChild(attr);
        }


        final String moduleWidth = barcodeConfig.getBarcodeModuleWidth();
        if (StringUtils.isNotEmpty(moduleWidth)) {
            final DefaultConfiguration attr = new DefaultConfiguration("module-width");
            attr.setValue(moduleWidth);
            child.addChild(attr);
        }

    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    @Required
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @param barcodeConfig
     *            the barcodeConfig to set
     */
    @Required
    public void setBarcodeConfig(final BarcodeConfig barcodeConfig) {
        this.barcodeConfig = barcodeConfig;
    }

}

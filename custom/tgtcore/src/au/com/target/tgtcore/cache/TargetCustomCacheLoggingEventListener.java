/**
 * 
 */
package au.com.target.tgtcore.cache;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import org.springframework.beans.factory.annotation.Required;


/**
 * @author pthoma20
 *
 */
public class TargetCustomCacheLoggingEventListener extends AbstractEventListener<TargetCustomCacheStatsLogEvent> {


    private TargetGenerateCacheStats targetGenerateCacheStats;

    @Override
    public void onEvent(final TargetCustomCacheStatsLogEvent event) {
        targetGenerateCacheStats.generateCacheStatsForEntityCache();
    }

    /**
     * @param targetGenerateCacheStats
     *            the targetGenerateCacheStats to set
     */
    @Required
    public void setTargetGenerateCacheStats(final TargetGenerateCacheStats targetGenerateCacheStats) {
        this.targetGenerateCacheStats = targetGenerateCacheStats;
    }
}
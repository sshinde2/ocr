/**
 * 
 */
package au.com.target.tgtcore.cache;

import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;


/**
 * @author pthoma20
 *
 */
public class TargetCustomCacheStatsLogEvent extends AbstractEvent implements ClusterAwareEvent {

    public TargetCustomCacheStatsLogEvent() {
        super();
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.event.ClusterAwareEvent#publish(int, int)
     */
    @Override
    public boolean publish(final int sourceNode, final int targetNode) {
        //This publishes event to all the clusters if set tot true
        return true;
    }
}
/**
 * 
 */
package au.com.target.tgtcore.cache;

import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;


/**
 * @author fkhan4
 *
 */
public class TargetUpdateStockCacheEvent extends AbstractEvent implements ClusterAwareEvent {

    public TargetUpdateStockCacheEvent() {
        super();
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.event.ClusterAwareEvent#publish(int, int)
     */
    @Override
    public boolean publish(final int sourceNodeId, final int targetNodeId) {
        //This publishes event to all the clusters if set to true
        return true;
    }

}

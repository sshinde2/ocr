/**
 * 
 */
package au.com.target.tgtcore.cache.dto;

/**
 * @author pthoma20
 *
 */
public class TargetCacheDataDTO {

    private String identifier;

    private String name;

    private String code;

    private long hits;

    private long fetches;

    private long misses;

    private long evictions;

    private long invalidations;

    private long instanceCount;


    /**
     * @return the hits
     */
    public long getHits() {
        return hits;
    }

    /**
     * @param hits
     *            the hits to set
     */
    public void setHits(final long hits) {
        this.hits = hits;
    }

    /**
     * @return the fetches
     */
    public long getFetches() {
        return fetches;
    }

    /**
     * @param fetches
     *            the fetches to set
     */
    public void setFetches(final long fetches) {
        this.fetches = fetches;
    }

    /**
     * @return the misses
     */
    public long getMisses() {
        return misses;
    }

    /**
     * @param misses
     *            the misses to set
     */
    public void setMisses(final long misses) {
        this.misses = misses;
    }

    /**
     * @return the evictions
     */
    public long getEvictions() {
        return evictions;
    }

    /**
     * @param evictions
     *            the evictions to set
     */
    public void setEvictions(final long evictions) {
        this.evictions = evictions;
    }

    /**
     * @return the invalidations
     */
    public long getInvalidations() {
        return invalidations;
    }

    /**
     * @param invalidations
     *            the invalidations to set
     */
    public void setInvalidations(final long invalidations) {
        this.invalidations = invalidations;
    }

    /**
     * @return the instanceCount
     */
    public long getInstanceCount() {
        return instanceCount;
    }

    /**
     * @param instanceCount
     *            the instanceCount to set
     */
    public void setInstanceCount(final long instanceCount) {
        this.instanceCount = instanceCount;
    }


    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * @param identifier
     *            the identifier to set
     */
    public void setIdentifier(final String identifier) {
        this.identifier = identifier;
    }

}

package au.com.target.tgtcore.cart.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeValueModel;
import de.hybris.platform.order.impl.DefaultCartService;
import de.hybris.platform.servicelayer.user.AddressService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Iterator;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.cart.TargetCartService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.SavedCncStoreDetailsModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtverifyaddr.TargetAddressVerificationService;
import au.com.target.tgtverifyaddr.exception.ServiceNotAvailableException;


/**
 * Implementation of {@link TargetCartService}
 * 
 */
public class TargetCartServiceImpl extends DefaultCartService implements TargetCartService {

    private static final Logger LOG = Logger.getLogger(TargetCartServiceImpl.class);

    private TargetDeliveryService targetDeliveryService;

    private TargetPointOfServiceService targetPointOfServiceService;

    private AddressService addressService;

    private UserService userService;

    private TargetAddressVerificationService targetAddressVerificationService;

    /**
     * {@inheritDoc}
     */
    @Override
    public void setDeliveryMode(final CartModel cart, final DeliveryModeModel deliveryMode) {
        validateParameterNotNullStandardMessage("cart", cart);
        validateParameterNotNullStandardMessage("deliveryMode", deliveryMode);

        cart.setDeliveryMode(deliveryMode);
        final AbstractTargetZoneDeliveryModeValueModel targetDeliveryModeValue = cart
                .getZoneDeliveryModeValue();

        //if there is no zone delivery mode value on the order then fetch it using the delivery mode and order
        //check if the delivery mode has changed for the order
        if (targetDeliveryModeValue == null
                || (!(targetDeliveryModeValue.getDeliveryMode().equals(deliveryMode)))) {
            try {
                final AbstractTargetZoneDeliveryModeValueModel targetDeliveryModeValueResult = getZoneDeliveryModeValueFromDeliveryMode(
                        cart);
                if (targetDeliveryModeValueResult != null) {
                    cart.setZoneDeliveryModeValue(targetDeliveryModeValueResult);
                }
            }
            catch (final TargetNoPostCodeException e) {
                cart.setZoneDeliveryModeValue(null);
            }

        }
        // ensure we recalculate with the new zone delivery mode value
        cart.setCalculated(Boolean.FALSE);

        // only click and collect orders should have a store number, so clear store number is need be
        if (deliveryMode instanceof TargetZoneDeliveryModeModel) {
            final TargetZoneDeliveryModeModel targetDeliveryMode = (TargetZoneDeliveryModeModel)deliveryMode;
            if (BooleanUtils.isNotTrue(targetDeliveryMode.getIsDeliveryToStore())) {
                // If the delivery mode is not eligible for store delivery but the store number
                // is set, then the delivery address (if set) will be a store address and is not valid.
                if (cart.getCncStoreNumber() != null) {
                    cart.setDeliveryAddress(null);
                }

                cart.setCncStoreNumber(null);
            }
            else {
                // If the delivery mode is eligible for store delivery but the store number is
                // not set, then the delivery address (if set) will not be a store address and is not valid.
                if (cart.getCncStoreNumber() == null) {
                    cart.setDeliveryAddress(null);
                }
            }
        }
        else {
            // If deliveryMode is not a TargetZoneDeliveryModeModel then the delivery mode can't be
            // eligible for store delivery. In this case, if the store number is set then the
            // delivery address (if set) will be a store address and is not valid.
            if (cart.getCncStoreNumber() != null) {
                cart.setDeliveryAddress(null);
            }

            cart.setCncStoreNumber(null);
        }

    }

    /**
     * get the zoneDeliveryModeValue using the targetDeliveryService.getZoneDeliveryModeValueForAbstractOrder
     * 
     * @param orderModel
     * @return AbstractTargetZoneDeliveryModeValueModel
     */
    protected AbstractTargetZoneDeliveryModeValueModel getZoneDeliveryModeValueFromDeliveryMode(
            final AbstractOrderModel orderModel) {
        final DeliveryModeModel deliveryMode = orderModel.getDeliveryMode();
        if (deliveryMode == null) {
            return null;
        }
        final ZoneDeliveryModeModel zoneDeliveryMode = (ZoneDeliveryModeModel)deliveryMode;

        final ZoneDeliveryModeValueModel deliveryModeValue = targetDeliveryService
                .getZoneDeliveryModeValueForAbstractOrderAndMode(zoneDeliveryMode, orderModel);
        final AbstractTargetZoneDeliveryModeValueModel targetDeliveryModeValueResult = (AbstractTargetZoneDeliveryModeValueModel)deliveryModeValue;
        return targetDeliveryModeValueResult;
    }

    @Override
    public void prePopulateCart(final CartModel cart, final TargetZoneDeliveryModeModel preferredDeliveryMode,
            final TargetCustomerModel customer, final CreditCardPaymentInfoModel paymentInfo,
            final OrderModel previousOrder) {
        cart.setPaymentInfo(paymentInfo);
        cart.setDeliveryMode(preferredDeliveryMode);
        if (customer.getDefaultPaymentAddress() != null) {
            cart.setPaymentAddress(customer.getDefaultPaymentAddress());
        }
        else if (previousOrder != null) {
            final boolean isAddressVerified = verifyAddress(previousOrder.getPaymentAddress());
            if (!isAddressVerified) {
                return;
            }

            cart.setPaymentAddress(previousOrder.getPaymentAddress());
        }
        cart.setFlyBuysCode(customer.getFlyBuysCode());

        if (BooleanUtils.isTrue(preferredDeliveryMode.getIsDeliveryToStore())) {
            setCnCDetails(cart, customer, previousOrder);
            final AddressModel shipmentAddress = cart.getDeliveryAddress();
            if (shipmentAddress != null && shipmentAddress.getPostalcode() != null) {
                getSessionService().setAttribute(TgtCoreConstants.SESSION_POSTCODE, shipmentAddress.getPostalcode());
            }
        }
        else {
            AddressModel shipmentAddress = null;
            if (customer.getDefaultShipmentAddress() != null) {
                shipmentAddress = customer.getDefaultShipmentAddress();

            }
            cart.setDeliveryAddress(shipmentAddress);
        }
        getModelService().save(cart);
    }

    /**
     * @param address
     * @throws ServiceNotAvailableException
     */
    private boolean verifyAddress(final AddressModel address) {
        final StringBuilder addressToVerify = new StringBuilder();

        addressToVerify.append(address.getStreetname());
        addressToVerify.append(", ");

        if (StringUtils.isNotBlank(address.getStreetnumber())) {
            addressToVerify.append(address.getStreetnumber());
            addressToVerify.append(", ");
        }

        addressToVerify.append(address.getTown());
        addressToVerify.append(' ');
        addressToVerify.append(address.getDistrict());
        addressToVerify.append(' ');
        addressToVerify.append(address.getPostalcode());

        addressToVerify.toString();

        return targetAddressVerificationService.isAddressVerified(addressToVerify.toString());
    }

    /**
     * Method to set cnc details like address, title, first name, last name and phone 1 to the cart model. If saved cnc
     * details exist then the details saved in cart are picked up from there otherwise from the previous order
     * 
     * @param cart
     * @param customer
     * @param previousOrder
     */
    private void setCnCDetails(final CartModel cart, final TargetCustomerModel customer,
            final OrderModel previousOrder) {
        if (CollectionUtils.isNotEmpty(customer.getSavedCncStoreDetails())) {
            final Iterator<SavedCncStoreDetailsModel> cncDetailsIterator = customer
                    .getSavedCncStoreDetails().iterator();
            final SavedCncStoreDetailsModel cncDetail = cncDetailsIterator.next();
            try {
                final TargetPointOfServiceModel pos = cncDetail.getStore();

                if (targetPointOfServiceService.isSupportAllProductTypesInCart(pos, cart)) {
                    cart.setCncStoreNumber(pos.getStoreNumber());
                    final AddressModel clone = addressService.cloneAddressForOwner(cncDetail.getStore().getAddress(),
                            cart);
                    if (cncDetail.getTitle() != null) {
                        clone.setTitle(userService.getTitleForCode(cncDetail.getTitle()));
                    }
                    clone.setFirstname(cncDetail.getFirstName());
                    clone.setLastname(cncDetail.getLastName());
                    clone.setPhone1(cncDetail.getPhoneNumber());
                    cart.setDeliveryAddress(clone);
                }

            }
            catch (final Exception e) {
                LOG.error(e.getMessage(), e);
            }
        }
        else {
            try {
                final TargetPointOfServiceModel pos = targetPointOfServiceService.getPOSByStoreNumber(previousOrder
                        .getCncStoreNumber());
                if (targetPointOfServiceService.isSupportAllProductTypesInCart(pos, cart)) {
                    cart.setCncStoreNumber(pos.getStoreNumber());
                    final AddressModel clone = addressService.cloneAddressForOwner(pos.getAddress(), cart);
                    clone.setTitle(previousOrder.getDeliveryAddress().getTitle());
                    clone.setFirstname(previousOrder.getDeliveryAddress().getFirstname());
                    clone.setLastname(previousOrder.getDeliveryAddress().getLastname());
                    clone.setPhone1(previousOrder.getDeliveryAddress().getPhone1());
                    cart.setDeliveryAddress(clone);
                }

            }
            catch (final Exception e) {
                LOG.error(e.getMessage(), e);
            }
        }
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @param targetPointOfServiceService
     *            the targetPointOfServiceService to set
     */
    @Required
    public void setTargetPointOfServiceService(final TargetPointOfServiceService targetPointOfServiceService) {
        this.targetPointOfServiceService = targetPointOfServiceService;
    }

    /**
     * @param targetDeliveryService
     *            the targetDeliveryService to set
     */
    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        this.targetDeliveryService = targetDeliveryService;
    }

    /**
     * @param addressService
     *            the addressService to set
     */
    @Required
    public void setAddressService(final AddressService addressService) {
        this.addressService = addressService;
    }

    /**
     * @param targetAddressVerificationService
     *            the targetAddressVerificationService to set
     */
    @Required
    public void setTargetAddressVerificationService(
            final TargetAddressVerificationService targetAddressVerificationService) {
        this.targetAddressVerificationService = targetAddressVerificationService;
    }

}

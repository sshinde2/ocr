package au.com.target.tgtcore.category;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Collection;
import java.util.Set;


/**
 * Target extension to default {@link CategoryService}.
 */
public interface TargetCategoryService extends CategoryService {

    /**
     * Returns collection of categories that have (variant)products.
     * 
     * @return the leaf category collection
     */
    Collection<CategoryModel> getLeafCategoriesWithProducts();

    /**
     * Returns collection containing all categories in the system.
     * 
     * @return collection of all categories
     */
    Collection<CategoryModel> getAllCategories();

    /**
     * Returns collection of categories that have (variant)products.
     * 
     * @return the leaf category collection
     */
    Collection<CategoryModel> getAllCategoriesWithProducts();

    /**
     * Get all super categories for product
     * 
     * @param product
     * @return List<CategoryModel>
     */
    Set<CategoryModel> getAllSuperCategoriesForProduct(ProductModel product);

    /**
     * Get all online super categories for product
     * 
     * @param product
     * @return collection CategoryModel
     */
    Collection<CategoryModel> findAllOnlineSuperCategoriesByProduct(ProductModel product);

}

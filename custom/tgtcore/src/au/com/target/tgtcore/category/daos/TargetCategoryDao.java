package au.com.target.tgtcore.category.daos;

import de.hybris.platform.category.daos.CategoryDao;
import de.hybris.platform.category.model.CategoryModel;

import java.util.Collection;


/**
 * Target extension to default {@link CategoryDao}.
 */
public interface TargetCategoryDao extends CategoryDao {

    /**
     * Retrieves a collection of leaf categories, which contain (variant)products.
     * 
     * @return collection of leaf categories
     */
    Collection<CategoryModel> findLeafCategoriesWithProducts();

    /**
     * Retrieves a collection of all categories.
     * 
     * @return collection of categories
     */
    Collection<CategoryModel> findAllCategories();

    /**
     * Retrieves a collection of all categories and subcategories which contain products.
     * 
     * @return collection of categories
     */
    Collection<CategoryModel> findAllCategoriesWithProducts();


}

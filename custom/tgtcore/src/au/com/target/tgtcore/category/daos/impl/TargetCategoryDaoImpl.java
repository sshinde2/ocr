package au.com.target.tgtcore.category.daos.impl;

import de.hybris.platform.category.daos.impl.DefaultCategoryDao;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.GenericQuery;
import de.hybris.platform.genericsearch.GenericSearchService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.jdbc.core.JdbcTemplate;

import au.com.target.tgtcore.category.daos.TargetCategoryDao;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * Default implementation for {@link TargetCategoryDao}.
 */
public class TargetCategoryDaoImpl extends DefaultCategoryDao implements TargetCategoryDao {

    private static final String FIND_CATEGORIES_BY_PK = "SELECT {" + TargetProductCategoryModel.PK + "} FROM {"
            + TargetProductCategoryModel._TYPECODE + "} WHERE {" + TargetProductCategoryModel.PK +
            " } IN (?keys)";

    private GenericSearchService searchService;

    private JdbcTemplate jdbcTemplate;

    @Override
    public Collection<CategoryModel> findLeafCategoriesWithProducts() {
        final StringBuilder query = new StringBuilder();
        query.append("select distinct {p." + TargetProductModel.PRIMARYSUPERCATEGORY + "}");
        query.append(
                " from {" + TargetProductModel._TYPECODE + " as p join " + VariantProductModel._TYPECODE + " as v");
        query.append(" on {v." + VariantProductModel.BASEPRODUCT + "} = {p." + TargetProductModel.PK + "}");
        query.append(" and {p." + TargetProductModel.PRIMARYSUPERCATEGORY + "} IS NOT NULL }");

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.setResultClassList(Collections.singletonList(CategoryModel.class));

        final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);

        return searchResult.getResult();
    }

    @Override
    public Collection<CategoryModel> findAllCategories() {
        final GenericQuery query = new GenericQuery(TargetProductCategoryModel._TYPECODE);
        return searchService.<CategoryModel> search(query).getResult();
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.category.daos.TargetCategoryDao#findAllCategoriesWithProducts()
     */
    @Override
    public Collection<CategoryModel> findAllCategoriesWithProducts() {

        final List<String> keys = findAllCategoryPKWithProducts();

        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_CATEGORIES_BY_PK);
        query.addQueryParameter("keys", keys);

        final SearchResult<CategoryModel> result = getFlexibleSearchService().search(query);
        return result.getResult();
    }


    private List<String> findAllCategoryPKWithProducts() {

        final StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("With rec_cte AS ( ");
        stringBuilder.append("  select distinct rel.sourcepk,rel.TargetPK ");
        stringBuilder.append("                        from products as p  ");
        stringBuilder.append("                        inner join products as v on v.p_baseProduct = p.pk  ");
        stringBuilder.append("                        inner join enumerationvalues as ps on ps.PK=v.p_approvalstatus ");
        stringBuilder.append("                        inner join catalogversions as cv on cv.pk=p.p_catalogversion ");
        stringBuilder.append("                        inner join catalogs as c on cv.p_catalog = c.PK ");
        stringBuilder.append("                        inner join categories as cat on cat.pk=p.primarySuperCategory ");
        stringBuilder.append("                        inner join composedtypes as ct on cat.TypePkString=ct.pk ");
        stringBuilder
                .append("                        inner join cat2catrel as rel on rel.TargetPK=p.primarySuperCategory ");
        stringBuilder.append("                        WHERE ");
        stringBuilder.append("                        p.primarySuperCategory IS NOT NULL  ");
        stringBuilder.append("                        AND p.p_producttype is not null ");
        stringBuilder.append("                        AND ps.Code='approved' ");
        stringBuilder.append("                        AND cv.p_version = 'Online' ");
        stringBuilder.append("                        AND ct.internalcode='TargetProductCategory' ");
        stringBuilder.append("                        AND v.p_catalogversion = cv.PK ");
        stringBuilder.append("                        AND cat.p_catalogversion = cv.PK ");
        stringBuilder.append("                        AND c.p_id = 'targetProductCatalog' ");
        stringBuilder.append("                    ");
        stringBuilder.append("  union all ");
        stringBuilder.append("  select rec.sourcepk, rec.targetpk from cat2catrel  rec ");
        stringBuilder.append("  inner join rec_cte on  ");
        stringBuilder.append("  rec.TargetPK = rec_cte.SourcePK ");
        stringBuilder.append("  where rec.sourcepk !=(select cat.pk from categories as cat ");
        stringBuilder
                .append("                       inner join catalogversions as cv on cv.pk=cat.p_catalogversion   ");
        stringBuilder.append("                       inner join catalogs as c on cv.p_catalog = c.PK ");
        stringBuilder.append("                       where cat.p_code='AP01' ");
        stringBuilder.append("                        AND cv.p_version = 'Online'  ");
        stringBuilder.append("                        AND c.p_id = 'targetProductCatalog') ");
        stringBuilder.append(")  ");
        stringBuilder.append("select distinct sourcepk pk from rec_cte  ");
        stringBuilder.append("  union  ");
        stringBuilder.append(" select distinct targetpk pk from rec_cte  ");

        final String query = stringBuilder.toString();
        return jdbcTemplate.queryForList(query, String.class);

    }

    /**
     * Returns the service used internally to perform search.
     * 
     * @return the search service
     */
    public GenericSearchService getSearchService() {
        return searchService;
    }

    /**
     * Sets the search service.
     * 
     * @param searchService
     *            the service to set
     */
    public void setSearchService(final GenericSearchService searchService) {
        this.searchService = searchService;
    }

    /**
     * @return the jdbcTemplate
     */
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    /**
     * @param jdbcTemplate
     *            the jdbcTemplate to set
     */
    @Required
    public void setJdbcTemplate(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


}

package au.com.target.tgtcore.category.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.daos.CategoryDao;
import de.hybris.platform.category.impl.DefaultCategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.category.TargetCategoryService;
import au.com.target.tgtcore.category.daos.TargetCategoryDao;
import au.com.target.tgtcore.constants.TgtCoreConstants;


/**
 * Default implementation for {@link TargetCategoryService}.
 */
public class TargetCategoryServiceImpl extends DefaultCategoryService implements TargetCategoryService {

    private TargetCategoryDao categoryDao;

    private SearchRestrictionService searchRestrictionService;

    @Override
    public Collection<CategoryModel> getLeafCategoriesWithProducts() {
        return categoryDao.findLeafCategoriesWithProducts();
    }

    @Override
    public Collection<CategoryModel> getAllCategories() {
        return categoryDao.findAllCategories();
    }

    /**
     * Returns internally used category DAO.
     * 
     * @return the category DAO
     */
    public CategoryDao getCategoryDao() {
        return categoryDao;
    }

    /**
     * Sets the category DAO.
     * 
     * @param categoryDao
     *            the DAO to set
     */
    @Override
    public void setCategoryDao(final CategoryDao categoryDao) {
        super.setCategoryDao(categoryDao);
        this.categoryDao = (TargetCategoryDao)categoryDao;
    }

    @Override
    public Set<CategoryModel> getAllSuperCategoriesForProduct(final ProductModel product) {

        return getSessionService().executeInLocalView(new SessionExecutionBody() {
            @Override
            public Object execute() {
                try {
                    searchRestrictionService.disableSearchRestrictions();
                    final Set<CategoryModel> categories = new HashSet<>();
                    if (product != null) {
                        final Collection<CategoryModel> productSuperCats = findAllOnlineSuperCategoriesByProduct(
                                product);
                        if (CollectionUtils.isNotEmpty(productSuperCats)) {
                            categories.addAll(productSuperCats);
                            for (final CategoryModel category : productSuperCats) {
                                final Collection<CategoryModel> superCategories = getAllSupercategoriesForCategory(
                                        category);
                                if (CollectionUtils.isNotEmpty(superCategories)) {
                                    categories.addAll(superCategories);
                                }
                            }
                        }
                    }
                    return categories;
                }
                finally {
                    searchRestrictionService.enableSearchRestrictions();
                }

            }
        });


    }

    @Override
    public Collection<CategoryModel> findAllOnlineSuperCategoriesByProduct(final ProductModel product) {
        validateParameterNotNull(product, "Parameter 'productModel' was null.");

        return getSessionService().executeInLocalView(new SessionExecutionBody() {
            @Override
            public Object execute() {
                try {
                    searchRestrictionService.disableSearchRestrictions();
                    final CatalogVersionModel onlineProductCatalog = getCatalogVersionService().getCatalogVersion(
                            TgtCoreConstants.Catalog.PRODUCTS, TgtCoreConstants.Catalog.ONLINE_VERSION);
                    return categoryDao.findCategoriesByCatalogVersionAndProduct(onlineProductCatalog, product);
                }
                finally {
                    searchRestrictionService.enableSearchRestrictions();
                }

            }
        });


    }

    @Override
    public Collection<CategoryModel> getAllCategoriesWithProducts() {
        return categoryDao.findAllCategoriesWithProducts();
    }

    /**
     * @return the catalogVersionService
     */
    public CatalogVersionService getCatalogVersionService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getCatalogVersionService().");
    }

    /**
     * @param searchRestrictionService
     *            the searchRestrictionService to set
     */
    @Required
    public void setSearchRestrictionService(final SearchRestrictionService searchRestrictionService) {
        this.searchRestrictionService = searchRestrictionService;
    }



}

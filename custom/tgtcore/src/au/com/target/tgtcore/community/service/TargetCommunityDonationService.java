/**
 * 
 */
package au.com.target.tgtcore.community.service;

import au.com.target.tgtcore.community.dto.TargetCommunityDonationRequestDto;


/**
 * @author bhuang3
 * 
 */
public interface TargetCommunityDonationService {

    /**
     * send the request to the web methods
     * 
     * @param targetCommunityDonationRequestDto
     * @return true:success false: failure
     */
    boolean sendDonationRequest(TargetCommunityDonationRequestDto targetCommunityDonationRequestDto);
}

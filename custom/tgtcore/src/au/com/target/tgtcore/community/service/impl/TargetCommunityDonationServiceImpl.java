/**
 * 
 */
package au.com.target.tgtcore.community.service.impl;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.community.client.TargetCommunityDonationClient;
import au.com.target.tgtcore.community.dto.TargetCommunityDonationRequestDto;
import au.com.target.tgtcore.community.service.TargetCommunityDonationService;


/**
 * @author bhuang3
 * 
 */
public class TargetCommunityDonationServiceImpl implements TargetCommunityDonationService {


    private TargetCommunityDonationClient targetCommunityDonationClient;

    @Override
    public boolean sendDonationRequest(final TargetCommunityDonationRequestDto targetCommunityDonationRequestDto) {

        return targetCommunityDonationClient.sendDonationRequestData(targetCommunityDonationRequestDto);
    }

    /**
     * @return the targetCommunityDonationClient
     */
    public TargetCommunityDonationClient getTargetCommunityDonationClient() {
        return targetCommunityDonationClient;
    }

    /**
     * @param targetCommunityDonationClient
     *            the targetCommunityDonationClient to set
     */
    @Required
    public void setTargetCommunityDonationClient(final TargetCommunityDonationClient targetCommunityDonationClient) {
        this.targetCommunityDonationClient = targetCommunityDonationClient;
    }



}

/**
 * 
 */
package au.com.target.tgtcore.config.impl;

import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.configuration.ConversionException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.config.daos.TargetSharedConfigDao;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetSharedConfigModel;


/**
 * @author bhuang3
 * 
 */
public class TargetSharedConfigServiceImpl implements TargetSharedConfigService {

    private static final Logger LOG = Logger.getLogger(TargetSharedConfigServiceImpl.class);
    private TargetSharedConfigDao targetSharedConfigDao;
    private ModelService modelService;

    @Override
    public String getConfigByCode(final String code) {
        TargetSharedConfigModel targetSharedConfigModel = null;
        if (StringUtils.isNotEmpty(code)) {
            try {
                targetSharedConfigModel = targetSharedConfigDao
                        .getSharedConfigByCode(code);

            }
            catch (final TargetUnknownIdentifierException unknownEx) {
                LOG.error("SharedConfig: not found, code=" + code);
            }
            catch (final TargetAmbiguousIdentifierException ambiguousEx) {
                LOG.error("Ambiguous SharedConfig: found, code=" + code);
            }
        }
        return (targetSharedConfigModel != null) ? targetSharedConfigModel.getValue() : null;
    }


    @Override
    public double getDouble(final String configCode, final double defaultValue) {
        final String value = getConfigByCode(configCode);
        if (StringUtils.isBlank(value)) {
            return defaultValue;
        }
        try {
            return Double.parseDouble(value);
        }
        catch (final NumberFormatException ex) {
            throw new ConversionException('\'' + configCode + "' doesn't map to an double object", ex);
        }
    }

    @Override
    public Double getDoubleWrapper(final String configCode, final double defaultValue) {
        final String value = getConfigByCode(configCode);
        if (StringUtils.isBlank(value)) {
            return Double.valueOf(defaultValue);
        }
        try {
            return Double.valueOf(value);
        }
        catch (final NumberFormatException ex) {
            throw new ConversionException('\'' + configCode + "' doesn't map to an double object", ex);
        }
    }

    @Override
    public int getInt(final String configCode, final int defaultValue) {
        final String value = getConfigByCode(configCode);
        if (StringUtils.isBlank(value)) {
            return defaultValue;
        }
        try {
            return Integer.parseInt(value);
        }
        catch (final NumberFormatException ex) {
            throw new ConversionException('\'' + configCode + "' doesn't map to an Integer object", ex);
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.config.TargetSharedConfigService#setSharedConfig(java.util.Map)
     */
    @Override
    public void setSharedConfigs(final Map<String, String> configEntries) {
        if (MapUtils.isNotEmpty(configEntries)) {
            for (final String eachKey : configEntries.keySet()) {
                TargetSharedConfigModel targetSharedConfigModel = getSharedConfigModel(eachKey);
                final String value = configEntries.get(eachKey);
                if (StringUtils.isEmpty(value)) {
                    if (targetSharedConfigModel != null) {
                        modelService.remove(targetSharedConfigModel);
                    }
                    continue;
                }
                if (null == targetSharedConfigModel) {
                    targetSharedConfigModel = modelService.create(TargetSharedConfigModel.class);
                    targetSharedConfigModel.setCode(eachKey);
                }
                targetSharedConfigModel.setValue(value);
                modelService.save(targetSharedConfigModel);
            }
        }

    }

    private TargetSharedConfigModel getSharedConfigModel(final String code) {
        try {
            return targetSharedConfigDao
                    .getSharedConfigByCode(code);

        }
        catch (final TargetUnknownIdentifierException unknownEx) {
            LOG.warn("SharedConfig: not found, code=" + code);

        }
        catch (final TargetAmbiguousIdentifierException ambiguousEx) {
            LOG.warn("Ambiguous SharedConfig: found, code=" + code);
        }
        return null;
    }

    /**
     * @return the targetSharedConfigDao
     */
    protected TargetSharedConfigDao getTargetSharedConfigDao() {
        return targetSharedConfigDao;
    }

    /**
     * @param targetSharedConfigDao
     *            the targetSharedConfigDao to set
     */
    @Required
    public void setTargetSharedConfigDao(final TargetSharedConfigDao targetSharedConfigDao) {
        this.targetSharedConfigDao = targetSharedConfigDao;
    }


    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }


}

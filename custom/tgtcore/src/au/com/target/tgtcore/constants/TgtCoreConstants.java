/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtcore.constants;


/**
 * Global class for all TgtCore constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class TgtCoreConstants extends GeneratedTgtCoreConstants {
    public static final String EXTENSIONNAME = "tgtcore";
    public static final String TARGET_BARCODE_FILE_PREFIX = "barcode_";

    public static final Integer ONLINE_STORE_NUMBER = Integer.valueOf(5599);
    /**
     * Prefix for order invoice document
     */
    public static final String TAX_INVOICE_PREFIX = "INV";

    /**
     * SUFFIX for order invoice document
     */
    public static final String TAX_INVOICE_PDF_SUFFIX = ".pdf";
    public static final String AUSTRALIAN_DOLLARS = "AUD";
    public static final int EXTENDED_BULKY_THRESHOLD = 2;
    public static final String COUNTRY_ISO_CODE_AUSTRALIA = "AU";
    public static final String COUNTRY_ISO_CODE_NEWZEALAND = "NZ";

    /**
     * Constant to hold post code in session.
     */
    public static final String SESSION_POSTCODE = "postcode";

    public static final String JALO_SESSION = "jalosession";
    public static final String SLSESSION = "_slsession_";
    public static final String USER = "user";
    public static final String PHYSICAL_GIFTCARD = "giftCard";
    public static final String DIGITAL = "digital";
    public static final String ENTITY_CACHE_REGION = "entityCacheRegion";
    public static final String STOCK_RESERVATION_WAREHOUSE = "StockReservationWarehouse";
    public static final String CONSOLIDATED_STORE_WAREHOUSE = "ConsolidatedStoreWarehouse";
    public static final String FASTLINE_WAREHOUSE = "FastlineWarehouse";
    public static final String FASTLINE_FALCON = "fastlineFalcon";
    public static final String CANCELLATION_WAREHOUSE = "CancellationWarehouse";

    private TgtCoreConstants() {
        //empty
    }

    public interface Catalog {
        // The IDs of the created catalogs
        String CONTENT = "targetContentCatalog";
        String PRODUCTS = "targetProductCatalog";
        String OFFLINE_VERSION = "Staged";
        String ONLINE_VERSION = "Online";
    }

    public interface Seperators {
        String UNDERSCORE = "_".intern();
        String HYPHEN = "-".intern();
        String COMMA = ",".intern();
        String DOT = ".".intern();

    }

    public interface Category {
        String ALL_PRODUCTS = "AP01";
    }

    public interface Config {
        String PREVIOUS_PERM_PRICE_MIN_AGE = "previousPermPriceMinAge";
        String CURRENT_PERM_PRICE_MAX_AGE = "currentPermPriceMaxAge";
        String INSTORE_STOCK_LIMITED_THRESHOLD = "inStoreStockVisibility.limitedStockThreshold";
        String INSTORE_NO_STOCK_THRESHOLD = "inStoreStockVisibility.noStockThreshold";
        String CART_ENTRY_THRESHOLD = "cart.threshold.maximumCartEntries";
        String MAX_PREORDER_THRESHOLD = "maxPreOrder.perOrder.quantity";
        String PREODER_DEPOSIT_AMOUNT = "preOrder.deposit";
        String MAX_QTY_FASTLINE_FULFILLED_PER_DAY = "maxQtyFastlineFulfillPerDay";
        String CONSIGNMENT_DEFAULT_TO_STORE = "consignment.default.to.store";
        String BIG_AND_BULKY_DEFAULT_MAX_WEIGHT = "bigAndBulky.default.maxWeight";
        String BIG_AND_BULKY_DEFAULT_MAX_SIZE = "bigAndBulky.default.maxSize";

    }

    public interface CsAgentGroup {
        String TRADE_ME_CS_AGENT_GROUP = "trademe-csagentgroup";
        String EBAY_CS_AGENT_GROUP = "ebay-csagentgroup";
        String REFUND_FAILED_CS_AGENT_GROUP = "refundfailures-csagentgroup";
    }

    public interface FeatureSwitch {
        String PRODUCT_PREVIEW_MODE = "product.preview.mode";
        String CUSTOMER_SEGMENT_COOKIE = "customerSegmentCookie";
        String CHECKOUT_ALLOW_GUEST = "checkout.allow.guest";
        String SEND_EMAIL_WITH_BLACKOUT = "sendEmailWithBlackout";
        String BCRYPT_PASSWORD_ENCODING = "bcrypt.password.encoding";
        String BCRYPT_PASSWORD_ENCODING_TIMING = "bcrypt.password.encoding.timing";
        String KIOSK_INSTORE_STOCK_VISIBILITY = "kioskInStoreStockVisibility";
        String SHOW_GOOGLE_DIRECTION_URL = "showGoogleDirectionUrl";
        String STEP_SHOP_THE_LOOK = "step.shop.the.look";
        String STEP_SHOP_THE_LOOK_APP = "step.shop.the.look.app";
        String STEP_SHOP_THE_LOOK_APP_LIMIT = "step.shop.the.look.app.limit";
        String ACCOUNT_CREATION_EMAIL_VIA_EXTERNAL_SERVICE = "custAccountCreationEmailViaExtService";
        String SPC_THANKYOU = "spc.thankyou";
        String FORCE_DEFAULT_WAREHOUSE_TO_ONLINE_POS = "forceDefaultWarehouseToOnlinePOS";
        String SHIPSTER = "shipster.available";
        String FLUENT = "fluent";
        String UI_REACT_PDP_CAROUSEL = "ui.react.pdp.carousel";
        String UI_REACT_PDP_DETAIL_PANEL = "ui.react.pdp.detail.panel";
        String UI_REACT_LISTING = "ui.react.listing";
        String SELECT_STORE_WITH_DISTANCE_RESTRICTION = "fulfilmentStoreWithDistanceRestriction";
        String CSCOCKPIT_CANCEL_BUTTON = "cscockpitCancelButton";
        String FASLINE_FALCON = "fastlineFalcon";
        String TASK_SERVICE = "taskService";
        String TASK_SERVICE_STALL_FAILED_PROCESS_SAME_ORDER = "taskServiceStallFailedProcessSameOrder";
        String CSCOCKPIT_FULL_CANCEL_BUTTON = "cscockpitFullCancelButton";
        String NEWIN_ONLINEFROM = "newin.onlinefrom";
        String FASTLINE_IN_STORE_SPLITTING = "fastlineInStoreSplitting";
        String ZIP = "payment.zip";
        String LOCATION_SERVICES = "location.services";
        String USE_CACHE_STOCK_FOR_FULFIMENT = "useCacheStockForFulfillment";
        String PDP_OPTIMISATION = "pdpOptimisation";
        String FULFILMENT_PPD = "fulfilmentPPD";
        String CATEGORY_SITEMAP_OPTIMIZATION = "categorySitemapOptimization";
        String SPLIT_ORDER_OPTIMISATION = "splitOrderOptimisation";
        String ERROR_ON_STOCKCHECK_FAILURE = "errorOnStockCheckFailure";
        String GEOCODE_ADDRESS_LOOKUP_WITH_COUNTRY_FILTER = "geocodeAddressLookupWithCountryFilter";
        String IPG_CREDENTIAL_ON_FILE_FEATURE = "ipg.credential.on.file";
        String AUSPOST_MANIFEST_OPTIMIZATION = "auspostManifestOptimization";

    }

    public interface DELIVERY_TYPE {
        String CC = "CC";
        String HD = "HD";
        String ED = "ED";
        String PO = "PO";
        String CONSOLIDATED_STORES_SOH = "CONSOLIDATED_STORES_SOH";
        String STORE_SOH = "STORE_SOH";
    }

    public interface DeliveryMode {
        String CLICK_AND_COLLECT = "click-and-collect";
    }

    public interface CART {
        String ERR_PREORDER_CART = "An error occurred while adding the preOrder product to cart.";
    }

    public interface GeocodeAddressComponentType {
        String LOCALITY = "locality";
        String POLITICAL = "political";
        String POSTAL_CODE = "postal_code";
        String ADMINISTRATIVE_AREA_LEVEL_1 = "administrative_area_level_1";
    }

    public interface GeocodeExceptionMessage {
        String ERROR_DELIVERY_LOCATION_CODE = "ERROR_DELIVERY_LOCATION_CODE";
        String ZERO_RESULTS = "No results found for the given location/postcode";
        String OVER_DAILY_LIMIT = "Error due to either API key is missing/invalid or Billing not enabled or method of payment is no longer valid";
        String OVER_QUERY_LIMIT = "You have exceeded your query limit";
        String REQUEST_DENIED = "The request was denied. Probably the request missing required parameters.";
        String INVALID_REQUEST = "The query missing location or post code. Invalid location or post code was given";
        String UNKNOWN_ERROR = "Request could not be processed due to a server error. May succeed on retry";
    }

    public interface GeocodeStatus {
        String OK = "OK";
        String OVER_DAILY_LIMIT = "OVER_DAILY_LIMIT";
        String ZERO_RESULTS = "ZERO_RESULTS";
        String OVER_QUERY_LIMIT = "OVER_QUERY_LIMIT";
        String REQUEST_DENIED = "REQUEST_DENIED";
        String INVALID_REQUEST = "INVALID_REQUEST";
        String UNKNOWN_ERROR = "UNKNOWN_ERROR";
    }

    public interface CncOrderDetailsConstants {
        public static final String ORDER_PLACED = "placed";
        public static final String ORDER_CONFIRMED = "confirmed";
        public static final String ORDER_INPROGRESS = "inprogress";
        public static final String ORDER_COMPLETED = "completed";
        public static final String READY_TO_COLLECT = "readyToCollect";
        public static final String ORDER_REFUNDED = "refunded";
    }
}

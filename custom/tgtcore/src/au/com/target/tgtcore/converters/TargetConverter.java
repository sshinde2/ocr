/**
 * 
 */
package au.com.target.tgtcore.converters;

/**
 * Interface for a lightweight converter that doesn't require the advantages provided by AbstractPopulatingConverter.
 * 
 * @author jjayawa1
 *
 */
public interface TargetConverter<TSOURCE, TTARGET> {
    TTARGET convert(TSOURCE SOURCE);
}

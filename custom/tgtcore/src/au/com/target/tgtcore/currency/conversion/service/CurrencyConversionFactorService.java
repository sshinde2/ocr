/**
 * 
 */
package au.com.target.tgtcore.currency.conversion.service;

import de.hybris.platform.core.model.order.OrderModel;

import au.com.target.tgtcore.model.CurrencyConversionFactorsModel;


/**
 * @author Nandini
 *
 */
public interface CurrencyConversionFactorService {

    /**
     * Method to fetch latest currency factor for the given date
     * 
     * @param order
     * @return CurrencyConversionFactorsModel
     */
    CurrencyConversionFactorsModel findLatestCurrencyFactor(OrderModel order);

    /**
     * Convert the price to base currency
     * 
     * @param price
     * @param order
     * @return price base on base currency
     */
    double convertPriceToBaseCurrency(double price, OrderModel order);

    /**
     * Find out if we can perform conversion to base currency, or we are already using base currency.
     * 
     * @param order
     * @return false if not using base currency and no conversion is possible, otherwise return true
     */
    boolean canConvertToBaseCurrency(OrderModel order);
}

/**
 * 
 */
package au.com.target.tgtcore.customer;

/**
 * @author pthoma20
 *
 *         This exception is intended to be thrown if a customer tries to re-use his existing password.
 *
 */
public class ReusePasswordException extends RuntimeException {

    public ReusePasswordException(final String message) {
        super(message);
    }
}

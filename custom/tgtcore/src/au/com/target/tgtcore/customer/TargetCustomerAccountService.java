package au.com.target.tgtcore.customer;

//CHECKSTYLE:OFF
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;

import javax.servlet.http.HttpSession;

import au.com.target.tgtbusproc.event.UpdateCustomerEmailAddrEvent;
import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtpayment.dto.TargetCardInfo;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;
import au.com.target.tgtpayment.model.ZippayPaymentInfoModel;
import au.com.target.tgtpayment.service.TargetPaymentService;


//CHECKSTYLE:ON

/**
 * Extended interface to support Target specific user account managment capabilities
 */
public interface TargetCustomerAccountService extends CustomerAccountService {


    /**
     * 
     * @param email
     * @param name
     * @throws DuplicateUidException
     */
    TargetCustomerModel registerGuestForAnonymousCheckout(final String email, final String name)
            throws DuplicateUidException;

    /**
     * Creates a new {@link CreditCardPaymentInfoModel}. Will internally call the payment gateway to store the card
     * details using {@link TargetPaymentService#tokenize(String, PaymentModeModel)}. Also creates a new
     * {@link AddressModel} and sets the same as billing address for the created {@link CreditCardPaymentInfoModel}
     * 
     * @param customerModel
     * @param paymentMode
     * @param cardInfo
     * @param titleCode
     * @param saveInAccount
     * @return CreditCardPaymentInfoModel
     */
    CreditCardPaymentInfoModel createCreditCardPaymentInfo(final CustomerModel customerModel,
            final PaymentModeModel paymentMode, final TargetCardInfo cardInfo, final String titleCode,
            final boolean saveInAccount);


    /**
     * Creates a new {@link PaypalPaymentInfoModel} for the {@link CustomerModel}
     * 
     * @param customerModel
     * @param payerId
     * @param token
     * @return paypalPaymentInfoModel
     */
    PaypalPaymentInfoModel createPaypalPaymentInfo(CustomerModel customerModel, String payerId, String token);

    /**
     * Creates a new {@link AfterpayPaymentInfoModel} for the {@link CustomerModel}
     * 
     * @param customerModel
     * @param afterpayToken
     * @return {@link AfterpayPaymentInfoModel}
     */
    AfterpayPaymentInfoModel createAfterpayPaymentInfo(CustomerModel customerModel, String afterpayToken);

    /**
     * update the given {@link CreditCardPaymentInfoModel} with new values should the payment decline and card has never
     * been used successfully
     * 
     * @param ccPaymentInfo
     * @param cardInfo
     * @param titleCode
     * @param saveInAccount
     * @param paymentMode
     */
    void updateCreditCardPaymentInfo(final CreditCardPaymentInfoModel ccPaymentInfo, final TargetCardInfo cardInfo,
            final String titleCode,
            final boolean saveInAccount, PaymentModeModel paymentMode);

    /**
     * Creates and publishes {@link UpdateCustomerEmailAddrEvent} for customer service
     * 
     * @param oldCustomerUid
     *            given old uid
     * @param customer
     *            the customer
     */
    void createAndPublishUpdateCustomerEmailAddrEventForCS(final String oldCustomerUid, final CustomerModel customer);

    /**
     * Method for creating the pin pad payment info as well as saving the billing address.
     * 
     * @param customerModel
     * @param targetBillingAddressModel
     * @return PinPadPaymentInfoModel
     */
    PinPadPaymentInfoModel createPinPadPaymentInfo(final CustomerModel customerModel,
            final TargetAddressModel targetBillingAddressModel);

    /**
     * Method for creating Google userID for cross device tracking. TrackingID come from uid with MD5encrypt
     */
    void createTrackingGUID();

    /**
     * update TargetCustomer record with the key if a matching record can be found.
     * 
     * @param email
     *            identifies TargetCustomer record
     * @param key
     *            is the customer subscribe key
     * @return true if there is a matching record and false if there isn't.
     */
    boolean updateTargetCustomerWithSubscrptionKey(String email, String key);

    /**
     * @param currentUser
     * @param targetBillingAddressModel
     * @param ipgPaymentTemplateType
     * @return IpgPaymentInfoModel
     */
    IpgPaymentInfoModel createIpgPaymentInfo(CustomerModel currentUser, TargetAddressModel targetBillingAddressModel,
            IpgPaymentTemplateType ipgPaymentTemplateType);

    /**
     * Removes the IPGCreditcardInofrmation for the customer
     * 
     * @param customerModel
     */
    void removeCustomerIPGSavedCards(CustomerModel customerModel);

    /**
     * This method gives back the customer info for the password token provided.
     * 
     * @param token
     * @return TargetCustomerModel
     */
    TargetCustomerModel getUserForResetPasswordToken(final String token);

    /**
     * Get current user model by http session
     * 
     * @param session
     * @return UserModel
     */
    UserModel getCurrentUserModelBySession(HttpSession session);

    /**
     * Update the Uid for Guest user
     * 
     * @param email
     * @param guestUserModel
     */
    void updateUidForGuestUser(String email, TargetCustomerModel guestUserModel)
            throws DuplicateUidException;

    /**
     * Creates ZipPayment Information for payment
     *
     * @param currentUser
     * @param checkoutId
     * @return
     */
    ZippayPaymentInfoModel createZipPaymentInfo(CustomerModel currentUser, String checkoutId);
}

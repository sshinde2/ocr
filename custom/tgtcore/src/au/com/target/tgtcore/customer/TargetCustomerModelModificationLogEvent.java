/**
 * 
 */
package au.com.target.tgtcore.customer;

import de.hybris.platform.core.PK;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.util.Map;


/**
 * @author bpottass
 *
 */
public class TargetCustomerModelModificationLogEvent extends AbstractEvent implements ClusterAwareEvent {

    private final PK itemPK;

    private final User user;

    private final Map<String, Object> newValueMap;

    private final Map<String, Object> previousValueMap;

    public TargetCustomerModelModificationLogEvent(final PK itemPK, final Map<String, Object> newValueMap,
            final Map<String, Object> previousValueMap, final User user) {
        super();
        this.itemPK = itemPK;
        this.user = user;
        this.newValueMap = newValueMap;
        this.previousValueMap = previousValueMap;
    }


    /**
     * @return the itemPK
     */
    public PK getItemPK() {
        return itemPK;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }


    /**
     * @return Map<String,Object> with attribute Name as key and attribute value as value
     */
    public Map<String, Object> getNewValueMap() {
        return newValueMap;
    }


    /**
     * @return Map<String,Object> with attribute Name as key and attribute value as value
     */
    public Map<String, Object> getPreviousValueMap() {
        return previousValueMap;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.event.ClusterAwareEvent#publish(int, int)
     */
    @Override
    public boolean publish(final int sourceNodeId, final int targetNodeId) {
        return sourceNodeId == targetNodeId;
    }

}

package au.com.target.tgtcore.customer;

import de.hybris.platform.persistence.security.MD5PasswordEncoder;
import de.hybris.platform.servicelayer.user.UserService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;


public class TargetMD5PasswordEncoder extends MD5PasswordEncoder {

    private static final Logger LOG = Logger.getLogger(TargetMD5PasswordEncoder.class);

    private String targetPasswordSalt;
    private String currentSystemPasswordEncoder;

    private UserService userService;
    private TargetFeatureSwitchService targetFeatureSwitchService;

    /**
     * Appends target salt to password and then encrypts using default md5 calculation
     * 
     * @param uid
     * @param plainPassword
     * @return md5 hash
     */
    @Override
    public String encode(final String uid, final String plainPassword) {

        String plainPwd = plainPassword;

        if (!plainPwd.startsWith(targetPasswordSalt.concat("::"))) {
            plainPwd = appendSalt(plainPwd);
        }
        return calculateMD5(plainPwd);
    }

    /**
     * 
     * @param uid
     * @param plainPassword
     * @param encodedPassword
     * @return true if the password md5 hashes match
     */
    @Override
    public boolean check(final String uid, final String encodedPassword, final String plainPassword) {

        String encodedPwd = encodedPassword;
        String plainPwd = plainPassword;

        if (encodedPwd == null) {

            encodedPwd = StringUtils.EMPTY;
        }
        if (plainPwd == null) {

            plainPwd = StringUtils.EMPTY;
        }

        final boolean matches = encodedPwd.equalsIgnoreCase(encode(uid, plainPwd));

        // Upgrade the password encoding
        if (matches && getTargetFeatureSwitchService()
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.BCRYPT_PASSWORD_ENCODING)) {
            LOG.info("Upgrading password to use the " + currentSystemPasswordEncoder + " encoder");
            userService.setPassword(uid, plainPassword, currentSystemPasswordEncoder);
        }

        return matches;
    }


    private String appendSalt(final String password) {
        String plainPwd = password;
        if (StringUtils.isBlank(plainPwd)) {
            plainPwd = StringUtils.EMPTY;
        }
        return new StringBuilder(targetPasswordSalt).append("::").append(plainPwd).toString();
    }

    /**
     * @param targetPasswordSalt
     *            the targetPasswordSalt to set
     */
    @Required
    public void setTargetPasswordSalt(final String targetPasswordSalt) {
        this.targetPasswordSalt = targetPasswordSalt;
    }

    /**
     * @param currentSystemPasswordEncoder
     *            the currentSystemPasswordEncoder to set
     */
    @Required
    public void setCurrentSystemPasswordEncoder(final String currentSystemPasswordEncoder) {
        this.currentSystemPasswordEncoder = currentSystemPasswordEncoder;
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @return the targetFeatureSwitchService
     */
    protected TargetFeatureSwitchService getTargetFeatureSwitchService() {
        return targetFeatureSwitchService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }
}

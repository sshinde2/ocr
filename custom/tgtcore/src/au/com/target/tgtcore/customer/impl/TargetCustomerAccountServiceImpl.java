package au.com.target.tgtcore.customer.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.PasswordMismatchException;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerAccountService;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.util.AbstractComparator;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.payment.dto.BillingInfo;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.session.Session;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.event.PasswordChangeEvent;
import au.com.target.tgtbusproc.event.UpdateCustomerEmailAddrEvent;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.customer.ReusePasswordException;
import au.com.target.tgtcore.customer.TargetCustomerAccountService;
import au.com.target.tgtcore.exception.NoModificationRequiredException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.login.TargetAuthenticationService;
import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtpayment.dto.TargetCardInfo;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;
import au.com.target.tgtpayment.model.ZippayPaymentInfoModel;
import au.com.target.tgtpayment.service.TargetPaymentService;
import au.com.target.tgtutility.util.StringTools;


/**
 * Default implementation of {@link TargetCustomerAccountService}
 */
public class TargetCustomerAccountServiceImpl extends DefaultCustomerAccountService implements
        TargetCustomerAccountService {

    private static final Logger LOG = Logger.getLogger(TargetCustomerAccountServiceImpl.class);
    private static final String GUID_PREFIX = "target";
    private TargetPaymentService targetPaymentService;
    private KeyGenerator keyGenerator;
    private TargetAuthenticationService targetAuthenticationService;
    private TargetFeatureSwitchService targetFeatureSwitchService;

    private String legacyPasswordEncoding;

    @Override
    public TargetCustomerModel registerGuestForAnonymousCheckout(final String email, final String name)
            throws DuplicateUidException {

        validateParameterNotNullStandardMessage("email", email);

        final TargetCustomerModel guestCustomer = getModelService().create(TargetCustomerModel.class);
        generateCustomerId(guestCustomer);

        //takes care of localizing the name based on the site language
        guestCustomer.setUid(guestCustomer.getCustomerID() + "|" + email);
        guestCustomer.setName(StringTools.trimAllWhiteSpaces(name));
        guestCustomer.setType(CustomerType.GUEST);
        guestCustomer.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
        guestCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());

        internalSaveCustomer(guestCustomer);
        return guestCustomer;
    }

    @Override
    public void register(final CustomerModel customerModel, final String password) throws DuplicateUidException {
        if (targetFeatureSwitchService
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.ACCOUNT_CREATION_EMAIL_VIA_EXTERNAL_SERVICE)) {
            registerCustomer(customerModel, password);
        }
        else {
            super.register(customerModel, password);
        }

    }

    @Override
    public CreditCardPaymentInfoModel createCreditCardPaymentInfo(final CustomerModel customerModel,
            final PaymentModeModel paymentMode, final TargetCardInfo cardInfo, final String titleCode,
            final boolean saveInAccount) {
        validateParameterNotNull(customerModel, "Customer cannot be null");
        validateParameterNotNull(paymentMode, "PaymentMode cannot be null");
        validateParameterNotNull(cardInfo, "CardInfo cannot be null");
        validateParameterNotNull(cardInfo.getBillingInfo(), "BillingInfo cannot be null");
        validateParameterNotNull(titleCode, "TitleCode cannot be null");

        // Save the card details stored in the hosted payment session first
        final String savedToken = targetPaymentService.tokenize(cardInfo.getPaymentSessionId(), paymentMode);

        if (StringUtils.isNotBlank(savedToken)) {
            final CreditCardPaymentInfoModel cardPaymentInfoModel = getModelService().create(
                    CreditCardPaymentInfoModel.class);
            cardPaymentInfoModel.setCode(customerModel.getUid() + TgtCoreConstants.Seperators.UNDERSCORE
                    + UUID.randomUUID());
            cardPaymentInfoModel.setUser(customerModel);

            saveCreditCardPaymentInfo(cardPaymentInfoModel, savedToken, cardInfo, saveInAccount);

            final TargetAddressModel billingAddress = getModelService().create(TargetAddressModel.class);
            saveBillingAddress(billingAddress, cardInfo.getBillingInfo(), titleCode);
            billingAddress.setOwner(cardPaymentInfoModel);
            cardPaymentInfoModel.setBillingAddress(billingAddress);
            getModelService().saveAll(billingAddress, cardPaymentInfoModel);
            getModelService().refresh(customerModel);
            return cardPaymentInfoModel;
        }

        return null;
    }

    @Override
    public PaypalPaymentInfoModel createPaypalPaymentInfo(final CustomerModel customerModel, final String payerId,
            final String token) {

        Assert.notNull(customerModel, "Customer cannot be null");
        Assert.notNull(payerId, "PayerId cannot be null");
        Assert.notNull(token, "Token cannot be null");

        final PaypalPaymentInfoModel paypalPaymentInfoModel = getModelService().create(
                PaypalPaymentInfoModel.class);
        paypalPaymentInfoModel.setCode(customerModel.getUid() + TgtCoreConstants.Seperators.UNDERSCORE
                + UUID.randomUUID());
        paypalPaymentInfoModel.setUser(customerModel);
        paypalPaymentInfoModel.setPayerId(payerId);
        paypalPaymentInfoModel.setToken(token);

        getModelService().save(paypalPaymentInfoModel);
        getModelService().refresh(customerModel);

        return paypalPaymentInfoModel;
    }

    @Override
    public AfterpayPaymentInfoModel createAfterpayPaymentInfo(final CustomerModel customerModel,
            final String token) {

        Assert.notNull(customerModel, "Customer cannot be null");
        Assert.notNull(token, "Token cannot be null");

        final AfterpayPaymentInfoModel afterpayPaymentInfoModel = getModelService().create(
                AfterpayPaymentInfoModel.class);
        afterpayPaymentInfoModel.setCode(customerModel.getUid() + TgtCoreConstants.Seperators.UNDERSCORE
                + UUID.randomUUID());
        afterpayPaymentInfoModel.setUser(customerModel);
        afterpayPaymentInfoModel.setAfterpayToken(token);

        getModelService().save(afterpayPaymentInfoModel);
        getModelService().refresh(customerModel);

        return afterpayPaymentInfoModel;
    }

    @Override
    public PinPadPaymentInfoModel createPinPadPaymentInfo(final CustomerModel customerModel,
            final TargetAddressModel targetBillingAddressModel) {

        Assert.notNull(customerModel, "Customer cannot be null");

        final PinPadPaymentInfoModel pinPadPaymentInfoModel = getModelService().create(
                PinPadPaymentInfoModel.class);
        pinPadPaymentInfoModel.setCode(customerModel.getUid() + TgtCoreConstants.Seperators.UNDERSCORE
                + UUID.randomUUID());
        pinPadPaymentInfoModel.setUser(customerModel);
        targetBillingAddressModel.setOwner(pinPadPaymentInfoModel);
        pinPadPaymentInfoModel.setBillingAddress(targetBillingAddressModel);
        getModelService().saveAll(targetBillingAddressModel, pinPadPaymentInfoModel);
        getModelService().refresh(customerModel);

        return pinPadPaymentInfoModel;
    }

    @Override
    public IpgPaymentInfoModel createIpgPaymentInfo(final CustomerModel customerModel,
            final TargetAddressModel targetBillingAddressModel, final IpgPaymentTemplateType ipgPaymentTemplateType) {

        Assert.notNull(customerModel, "Customer cannot be null");

        final IpgPaymentInfoModel ipgPaymentInfo = getModelService().create(IpgPaymentInfoModel.class);
        ipgPaymentInfo.setCode(customerModel.getUid() + TgtCoreConstants.Seperators.UNDERSCORE
                + UUID.randomUUID());
        ipgPaymentInfo.setUser(customerModel);
        ipgPaymentInfo.setIpgPaymentTemplateType(ipgPaymentTemplateType);
        if (targetBillingAddressModel != null) {
            targetBillingAddressModel.setOwner(ipgPaymentInfo);
            ipgPaymentInfo.setBillingAddress(targetBillingAddressModel);
            getModelService().saveAll(targetBillingAddressModel, ipgPaymentInfo);
        }
        else {
            getModelService().saveAll(ipgPaymentInfo);
        }
        getModelService().refresh(customerModel);

        return ipgPaymentInfo;
    }

    @Override
    public void changeUid(final String newUid, final String currentPassword) throws DuplicateUidException,
            PasswordMismatchException {
        Assert.hasText(newUid, "The field [newEmail] cannot be empty");
        Assert.hasText(currentPassword, "The field [currentPassword] cannot be empty");

        final String newUidLower = newUid.toLowerCase();
        final CustomerModel currentUser = (CustomerModel)getUserService().getCurrentUser();

        // Note the existing email address then set the new one
        final String oldContactEmail = currentUser.getUid();
        currentUser.setOriginalUid(newUid);

        // Check if the newUid is different to the current UID
        if (!currentUser.getUid().equals(newUid)) {
            checkUidUniqueness(newUidLower);
            adjustPassword(currentUser, newUidLower, currentPassword);

            // Raise event for starting the associated business process
            final UpdateCustomerEmailAddrEvent event = createUpdateCustomerEmailAddrEvent(currentUser, oldContactEmail);
            getEventService().publishEvent(event);
        }
        else {
            throw new DuplicateUidException("Uid id already set to the new value",
                    new NoModificationRequiredException());
        }
    }

    protected UpdateCustomerEmailAddrEvent createUpdateCustomerEmailAddrEvent(final CustomerModel customerModel,
            final String oldContactEmail) {
        final UpdateCustomerEmailAddrEvent event = new UpdateCustomerEmailAddrEvent();
        initializeEvent(event, customerModel);
        event.setOldContactEmail(oldContactEmail);
        return event;
    }

    /**
     * Overriden to publish the event {@link PasswordChangeEvent} in case of {@link CustomerModel}
     */
    @Override
    public void changePassword(final UserModel userModel, final String oldPassword, final String newPassword)
            throws PasswordMismatchException {
        ServicesUtil.validateParameterNotNullStandardMessage("userModel", userModel);
        if (!getUserService().isAnonymousUser(userModel)) {
            if (getPasswordEncoderService().isValid(userModel, oldPassword)) {
                getUserService().setPassword(userModel, newPassword, getPasswordEncoding());
                getModelService().save(userModel);

                if (userModel instanceof CustomerModel) {
                    getEventService()
                            .publishEvent(initializeEvent(new PasswordChangeEvent(), (CustomerModel)userModel));
                }
            }
            else {
                throw new PasswordMismatchException(userModel.getUid());
            }
        }
    }

    @Override
    public void updateCreditCardPaymentInfo(final CreditCardPaymentInfoModel ccPaymentInfo,
            final TargetCardInfo cardInfo,
            final String titleCode, final boolean saveInAccount, final PaymentModeModel paymentMode) {
        final String savedToken = targetPaymentService.tokenize(cardInfo.getPaymentSessionId(), paymentMode);
        //override the credit card payment info with the new values from Target Card Info
        saveCreditCardPaymentInfo(ccPaymentInfo, savedToken, cardInfo, saveInAccount);

        final TargetAddressModel targetAddress = (TargetAddressModel)ccPaymentInfo.getBillingAddress();
        //override the billing address with the new values from Target Card Info
        saveBillingAddress(targetAddress, cardInfo.getBillingInfo(), titleCode);
        targetAddress.setOwner(ccPaymentInfo);
        ccPaymentInfo.setBillingAddress(targetAddress);
        getModelService().saveAll(targetAddress, ccPaymentInfo);
    }

    @Override
    public List<CreditCardPaymentInfoModel> getCreditCardPaymentInfos(final CustomerModel customerModel,
            final boolean saved) {
        Assert.notNull(customerModel, "customer cannot be null");

        final List<CreditCardPaymentInfoModel> creditCards = super.getCreditCardPaymentInfos(customerModel, saved);
        if (CollectionUtils.isEmpty(creditCards)) {
            return Collections.EMPTY_LIST;
        }
        final List<CreditCardPaymentInfoModel> clonedCreditCards = new ArrayList<>();

        for (final CreditCardPaymentInfoModel ccpmi : creditCards) {
            if (BooleanUtils.isTrue(ccpmi.getIsPaymentSucceeded())) {
                clonedCreditCards.add(ccpmi);
            }
        }
        if (CollectionUtils.isNotEmpty(clonedCreditCards)) {
            sortCreditCards(clonedCreditCards);

        }
        return clonedCreditCards;
    }

    @Override
    protected void generateCustomerId(final CustomerModel customerModel) {
        customerModel.setCustomerID(String.valueOf(keyGenerator.generate()));
    }

    /**
     * save credit card payment info
     * 
     * @param cardPaymentInfoModel
     * @param savedToken
     * @param cardInfo
     * @param saveInAccount
     */
    private void saveCreditCardPaymentInfo(final CreditCardPaymentInfoModel cardPaymentInfoModel,
            final String savedToken, final TargetCardInfo cardInfo, final boolean saveInAccount) {
        cardPaymentInfoModel.setSubscriptionId(savedToken);
        //Payment gateway already returns the card number with the required masking.
        cardPaymentInfoModel.setNumber(cardInfo.getCardNumber());
        cardPaymentInfoModel.setType(cardInfo.getCardType());
        cardPaymentInfoModel.setCcOwner(cardInfo.getCardHolderFullName());
        cardPaymentInfoModel.setValidToMonth(String.valueOf(cardInfo.getExpirationMonth()));
        cardPaymentInfoModel.setValidToYear(String.valueOf(cardInfo.getExpirationYear()));
        cardPaymentInfoModel.setSaved(saveInAccount);
    }

    /**
     * save billing address
     * 
     * @param billingInfo
     * @param titleCode
     */
    private void saveBillingAddress(final TargetAddressModel billingAddress,
            final BillingInfo billingInfo,
            final String titleCode) {
        billingAddress.setTitle(getUserService().getTitleForCode(titleCode));
        billingAddress.setFirstname(billingInfo.getFirstName());
        billingAddress.setLastname(billingInfo.getLastName());
        billingAddress.setLine1(billingInfo.getStreet1());
        billingAddress.setLine2(billingInfo.getStreet2());
        billingAddress.setTown(billingInfo.getCity());
        billingAddress.setDistrict(billingInfo.getState());
        billingAddress.setPostalcode(billingInfo.getPostalCode());
        billingAddress.setCountry(getCommonI18NService().getCountry(billingInfo.getCountry()));
        billingAddress.setPhone1(billingInfo.getPhoneNumber());
        billingAddress.setBillingAddress(Boolean.TRUE);
    }

    private static class TargetCustomerCreditCardComparator extends AbstractComparator<CreditCardPaymentInfoModel> {
        private static final TargetCustomerCreditCardComparator INSTANCE = new TargetCustomerCreditCardComparator();

        @Override
        protected int compareInstances(final CreditCardPaymentInfoModel cc1, final CreditCardPaymentInfoModel cc2) {
            // Sort credit card by date created with newest credit card at top of the list
            final int comp = compareValues(cc2.getCreationtime(), cc1.getCreationtime());
            return comp;
        }
    }

    protected List<CreditCardPaymentInfoModel> sortCreditCards(
            final List<CreditCardPaymentInfoModel> clonedCreditCards) {
        Collections.sort(clonedCreditCards, TargetCustomerCreditCardComparator.INSTANCE);
        return clonedCreditCards;
    }

    @Override
    public void createAndPublishUpdateCustomerEmailAddrEventForCS(final String oldCustomerUid,
            final CustomerModel customer) {
        final UpdateCustomerEmailAddrEvent event = createUpdateCustomerEmailAddrEvent(customer, oldCustomerUid);
        getEventService().publishEvent(event);
    }

    /**
     * Override of the hybris updatePassword implementation. The only difference is that this implementation sets the
     * failedLoginAttempts on the customer to 0 when the password is reset. Another difference is that it returns the
     * user model which can be used to Autologin the user.
     * 
     * @param token
     *            the encrypted token for password reset
     * @param newPassword
     *            the new password to set
     * 
     * @see de.hybris.platform.commerceservices.customer.impl.DefaultCustomerAccountService#updatePassword(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public void updatePassword(final String token, final String newPassword)
            throws TokenInvalidatedException {

        Assert.hasText(newPassword, "The field [newPassword] cannot be empty");

        final SecureToken data = decryptToken(token);
        final TargetCustomerModel customer = getUserService().getUserForUID(data.getData(), TargetCustomerModel.class);
        if (customer == null) {
            throw new IllegalArgumentException("user for token not found");
        }
        if (!token.equals(customer.getToken())) {
            LOG.info("PASSWORD-RESET: customerPk=" + customer.getPk() + " token is invalid");
            throw new TokenInvalidatedException();
        }
        final boolean passwordMatches = Registry.getCurrentTenant().getJaloConnection()
                .getPasswordEncoder(customer.getPasswordEncoding())
                .check(customer.getUid(), customer.getEncodedPassword(), newPassword);

        if (passwordMatches) {
            LOG.info("PASSWORD-RESET: customerPk=" + customer.getPk()
                    + " tried to reuse the password while resetting");
            throw new ReusePasswordException("Password Cannot be re-used");
        }
        customer.setToken(null);

        if (getTargetAuthenticationService().checkLoginAttempts(customer)) {
            LOG.info(customer.getUid()
                    + " has successfully reset their password after reaching the login attempt limit.");
        }

        customer.setFailedLoginAttempts(Integer.valueOf(0));
        getModelService().save(customer);
        getUserService().setPassword(data.getData(), newPassword, getPasswordEncoding());
        LOG.info("PASSWORD-RESET: customerPk=" + customer.getPk() + " SUCCESS");
    }


    private SecureToken decryptToken(final String token) {
        Assert.hasText(token, "The field [token] cannot be empty");
        final SecureToken data = getSecureTokenService().decryptData(token);
        if (getTokenValiditySeconds() > 0L) {
            final long delta = new Date().getTime() - data.getTimeStamp();
            if (delta / 1000 > getTokenValiditySeconds()) {
                throw new IllegalArgumentException("token expired");
            }
        }
        return data;
    }

    @Override
    public TargetCustomerModel getUserForResetPasswordToken(final String token) {
        final SecureToken data = decryptToken(token);
        final TargetCustomerModel customer = getUserService().getUserForUID(data.getData(), TargetCustomerModel.class);
        if (customer == null) {
            throw new IllegalArgumentException("user for token not found");
        }
        return customer;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.customer.TargetCustomerAccountService#createTrackingGUID()
     */
    @Override
    public void createTrackingGUID() {
        final UserModel currentUser = getUserService().getCurrentUser();
        if (currentUser instanceof TargetCustomerModel) {
            final TargetCustomerModel currentCustomer = (TargetCustomerModel)currentUser;
            if (currentCustomer.getAllowTracking() == null) {
                currentCustomer.setAllowTracking(Boolean.TRUE);
            }
            if (currentCustomer.getAllowTracking().booleanValue()) {
                if (StringUtils.isBlank(currentCustomer.getTrackingGuid())) {
                    final String trackingId = DigestUtils.md5Hex(GUID_PREFIX + currentUser.getUid());
                    currentCustomer.setTrackingGuid(trackingId);
                    getModelService().save(currentCustomer);
                }
            }

        }
    }

    @Override
    public boolean updateTargetCustomerWithSubscrptionKey(final String email, final String subscribeKey) {
        boolean result = false;
        if (StringUtils.isEmpty(email) || StringUtils.isEmpty(subscribeKey)) {
            LOG.error("Invalid Data Provided. Email:" + email + " SubscriberKey:" + subscribeKey);
        }
        else if (getUserService().isUserExisting(email)) {
            final UserModel user = getUserService().getUserForUID(email);
            if (user instanceof TargetCustomerModel) {
                LOG.info("find mapped record customer:" + email);
                final TargetCustomerModel customer = (TargetCustomerModel)user;
                if (StringUtils.isEmpty(customer.getSubscriberKey())) {
                    customer.setSubscriberKey(subscribeKey);
                    getModelService().save(customer);
                    getModelService().refresh(customer);
                    result = true;
                }
                else {
                    LOG.warn("Customer:" + email + " already has subscriber key:" + customer.getSubscriberKey());
                }
            }
            else {
                LOG.error("invalid record found:" + email + " for key:" + subscribeKey);
            }
        }
        else {
            LOG.info("No record was found for customer:" + email + " for key:" + subscribeKey);
        }
        return result;
    }

    /**
     * Removes all the IPG saved cards for the customer
     * 
     * @param customerModel
     */
    @Override
    public void removeCustomerIPGSavedCards(final CustomerModel customerModel) {
        final List<CreditCardPaymentInfoModel> creditCardInfos = getCreditCardPaymentInfos(customerModel, true);
        for (final CreditCardPaymentInfoModel creditCardInfo : creditCardInfos) {
            if (creditCardInfo instanceof IpgCreditCardPaymentInfoModel) {
                unlinkCCPaymentInfo(customerModel, creditCardInfo);
            }
        }

    }

    @Override
    public void setDefaultPaymentInfo(final CustomerModel customerModel,
            final PaymentInfoModel paymentInfoModel) {
        if (paymentInfoModel instanceof IpgCreditCardPaymentInfoModel) {
            ((IpgCreditCardPaymentInfoModel)paymentInfoModel).setDefaultCard(Boolean.TRUE);
            getModelService().save(paymentInfoModel);
            getModelService().refresh(paymentInfoModel);
        }
        super.setDefaultPaymentInfo(customerModel, paymentInfoModel);
    }

    @Override
    public String getPasswordEncoding() {
        if (getTargetFeatureSwitchService().isFeatureEnabled(TgtCoreConstants.FeatureSwitch.BCRYPT_PASSWORD_ENCODING)) {
            return super.getPasswordEncoding();
        }

        return getLegacyPasswordEncoding();
    }

    @Override
    public UserModel getCurrentUserModelBySession(final HttpSession session) {
        final JaloSession jaloSession = (JaloSession)session.getAttribute(TgtCoreConstants.JALO_SESSION);
        if (jaloSession == null) {
            return null;
        }
        final Session hybrisSession = (Session)jaloSession.getAttribute(TgtCoreConstants.SLSESSION);
        if (hybrisSession == null) {
            return null;
        }
        return (UserModel)hybrisSession.getAttribute(TgtCoreConstants.USER);
    }

    @Override
    public void updateUidForGuestUser(final String email, final TargetCustomerModel guestUserModel)
            throws DuplicateUidException {
        if (guestUserModel != null && CustomerType.GUEST.equals(guestUserModel.getType())) {
            final String customerId = String.valueOf(keyGenerator.generate());
            guestUserModel.setCustomerID(customerId);
            guestUserModel.setUid(customerId + "|" + email);
            internalSaveCustomer(guestUserModel);
        }
    }

    @Override
    public ZippayPaymentInfoModel createZipPaymentInfo(final CustomerModel currentUser, final String checkoutId) {
        Assert.notNull(currentUser, "Customer cannot be null");
        Assert.notNull(checkoutId, "Token cannot be null");

        final ZippayPaymentInfoModel paymentInfo = getModelService().create(
                ZippayPaymentInfoModel.class);
        paymentInfo.setCode(currentUser.getUid() + TgtCoreConstants.Seperators.UNDERSCORE
                + UUID.randomUUID());
        paymentInfo.setUser(currentUser);
        paymentInfo.setCheckoutId(checkoutId);

        getModelService().save(paymentInfo);
        getModelService().refresh(currentUser);

        return paymentInfo;
    }

    /**
     * @param targetPaymentService
     *            the targetPaymentService to set
     */
    @Required
    public void setTargetPaymentService(final TargetPaymentService targetPaymentService) {
        this.targetPaymentService = targetPaymentService;
    }


    /**
     * @param keyGenerator
     *            the keyGenerator to set
     */
    @Required
    public void setKeyGenerator(final KeyGenerator keyGenerator) {
        this.keyGenerator = keyGenerator;
    }

    /**
     * @return the targetAuthenticationService
     */
    protected TargetAuthenticationService getTargetAuthenticationService() {
        return targetAuthenticationService;
    }

    /**
     * @param targetAuthenticationService
     *            the targetAuthenticationService to set
     */
    @Required
    public void setTargetAuthenticationService(final TargetAuthenticationService targetAuthenticationService) {
        this.targetAuthenticationService = targetAuthenticationService;
    }

    /**
     * @return the targetFeatureSwitchService
     */
    protected TargetFeatureSwitchService getTargetFeatureSwitchService() {
        return targetFeatureSwitchService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * @return the legacyPasswordEncoding
     */
    protected String getLegacyPasswordEncoding() {
        return legacyPasswordEncoding;
    }

    /**
     * @param legacyPasswordEncoding
     *            the legacyPasswordEncoding to set
     */
    @Required
    public void setLegacyPasswordEncoding(final String legacyPasswordEncoding) {
        this.legacyPasswordEncoding = legacyPasswordEncoding;
    }

}

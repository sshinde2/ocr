/**
 * 
 */
package au.com.target.tgtcore.customer.impl;


import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerEmailResolutionService;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.mail.MailUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtcore.model.TargetCustomerModel;



/**
 * @author asingh78
 * 
 */
public class TargetCustomerEmailResolutionService extends DefaultCustomerEmailResolutionService {

    private static final Logger LOG = Logger.getLogger(TargetCustomerEmailResolutionService.class);

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.customer.CustomerEmailResolutionService#getEmailForCustomer(de.hybris.platform.core.model.user.CustomerModel)
     */
    @Override
    public String getEmailForCustomer(final CustomerModel customerModel)
    {
        if (customerModel instanceof TargetCustomerModel) {
            final TargetCustomerModel targetCustomerModel = (TargetCustomerModel)customerModel;
            final String emailAfterProcessing = validateAndProcessEmailForCustomer(targetCustomerModel);

            return emailAfterProcessing;
        }
        return super.getEmailForCustomer(customerModel);
    }

    public String validateAndProcessEmailForCustomer(final TargetCustomerModel customerModel)
    {
        try
        {
            ServicesUtil.validateParameterNotNullStandardMessage("customerModel", customerModel);

            final String email = CustomerType.GUEST.equals(customerModel.getType()) ?
                    StringUtils.substringAfter(customerModel.getUid(), "|") : customerModel.getUid();

            MailUtils.validateEmailAddress(email, "customer email");
            return email;
        }
        catch (final Exception e)
        {
            LOG.info("Given uid is not appropriate email cause: " + e.getMessage());
        }
        return null;
    }
}

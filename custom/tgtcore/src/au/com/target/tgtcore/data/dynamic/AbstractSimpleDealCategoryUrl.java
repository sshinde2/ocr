/**
 *
 */
package au.com.target.tgtcore.data.dynamic;

import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import au.com.target.tgtcore.model.TargetDealCategoryModel;


/**
 * @author rmcalave
 *
 */
public abstract class AbstractSimpleDealCategoryUrl implements DynamicAttributeHandler<String, AbstractSimpleDealModel> {

    private static final String D = "d";
    private static final String SLASH = "/";

    protected String getCategoryUrl(final TargetDealCategoryModel targetDealCategory) {
        final String categoryTitle = targetDealCategory.getCode();
        return SLASH + D + SLASH + categoryTitle;
    }
}

/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;

import java.util.Date;

import au.com.target.tgtcore.model.TargetConsignmentModel;


/**
 * @author mgazal
 */
public class ConsignmentStatusDate extends
        AbstractDynamicAttributeHandler<Date, TargetConsignmentModel> {

    private static final String SENT_TO_WAREHOUSE = "SENT_TO_WAREHOUSE";

    private static final String CONFIRMED_BY_WAREHOUSE = "CONFIRMED_BY_WAREHOUSE";

    private static final String WAVED = "WAVED";

    private static final String PICKED = "PICKED";

    private static final String PACKED = "PACKED";

    private static final String SHIPPED = "SHIPPED";

    private static final String CANCELLED = "CANCELLED";

    @Override
    public Date get(final TargetConsignmentModel model) {
        Date statusDate;
        switch (model.getStatus().getCode()) {
            case SENT_TO_WAREHOUSE:
                statusDate = model.getSentToWarehouseDate();
                break;
            case CONFIRMED_BY_WAREHOUSE:
                statusDate = model.getSentToWarehouseDate();
                break;
            case WAVED:
                statusDate = model.getWavedDate();
                break;
            case PICKED:
                statusDate = model.getPickConfirmDate();
                break;
            case PACKED:
                statusDate = model.getPackedDate();
                break;
            case SHIPPED:
                statusDate = model.getShippingDate();
                break;
            case CANCELLED:
                statusDate = model.getCancelDate();
                break;
            default:
                statusDate = null;
                break;
        }
        return statusDate;
    }
}

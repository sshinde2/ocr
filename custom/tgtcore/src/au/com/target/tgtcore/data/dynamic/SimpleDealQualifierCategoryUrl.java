/**
 *
 */
package au.com.target.tgtcore.data.dynamic;


import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author bhuang3
 *
 */
public class SimpleDealQualifierCategoryUrl extends AbstractSimpleDealCategoryUrl {

    @Override
    public String get(final AbstractSimpleDealModel model) {
        String url = null;
        if (model != null) {
            if (CollectionUtils.isNotEmpty(model.getQualifierList())) {
                final DealQualifierModel dealQualifierModel = model.getQualifierList().get(0);
                if (CollectionUtils.isNotEmpty(dealQualifierModel.getDealCategory())) {
                    url = getCategoryUrl(dealQualifierModel.getDealCategory().get(0));
                }
            }
        }
        return url;
    }

    @Override
    public void set(final AbstractSimpleDealModel model, final String value) {
        throw new UnsupportedOperationException();
    }



}

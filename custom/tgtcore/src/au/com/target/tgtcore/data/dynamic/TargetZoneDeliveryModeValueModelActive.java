package au.com.target.tgtcore.data.dynamic;

import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;

import java.util.Date;

import javax.validation.constraints.NotNull;

import au.com.target.tgtcore.model.TargetZoneDeliveryModeValueModel;


/**
 * Dynamic attribute handler for the FeeStructureModel.active attribute.
 */
public class TargetZoneDeliveryModeValueModelActive extends
        AbstractDynamicAttributeHandler<Boolean, TargetZoneDeliveryModeValueModel> {

    @Override
    @NotNull
    public Boolean get(final TargetZoneDeliveryModeValueModel model) {
        final Date validFrom = model.getValidFrom();
        final Date validTo = model.getValidTo();
        final Date now = new Date();

        return Boolean.valueOf((validFrom == null || !now.before(validFrom))
                && (validTo == null || !now.after(validTo)));
    }
}

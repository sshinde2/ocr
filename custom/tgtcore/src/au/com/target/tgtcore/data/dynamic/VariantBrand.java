/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.springframework.util.Assert;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * Get the Brand from the base product of this variant.
 */
public class VariantBrand implements DynamicAttributeHandler<BrandModel, AbstractTargetVariantProductModel> {

    @Override
    public BrandModel get(final AbstractTargetVariantProductModel model) {
        Assert.notNull(model);

        final ProductModel baseProduct = model.getBaseProduct();
        if (baseProduct instanceof AbstractTargetVariantProductModel) {
            // recursively call till we get to the ProductModel
            return ((AbstractTargetVariantProductModel)baseProduct).getBrand();
        }
        else if (baseProduct instanceof TargetProductModel) {
            return ((TargetProductModel)baseProduct).getBrand();
        }

        return null;
    }

    @Override
    public void set(final AbstractTargetVariantProductModel model, final BrandModel value) {
        throw new UnsupportedOperationException();
    }

}

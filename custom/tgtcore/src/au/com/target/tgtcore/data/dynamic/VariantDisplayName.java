package au.com.target.tgtcore.data.dynamic;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * Dynamically calculates the product variant display name.
 */
public class VariantDisplayName implements DynamicAttributeHandler<String, AbstractTargetVariantProductModel> {

    private static final String SEPARATOR = " - ";

    @Override
    public String get(final AbstractTargetVariantProductModel model) {
        Assert.notNull(model, "Model cannot be [null]");

        if (model instanceof TargetSizeVariantProductModel) {
            final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)model;
            final ProductModel baseProduct = sizeVariant.getBaseProduct();
            Assert.notNull(baseProduct, "Size variant must have a base product");
            Assert.isInstanceOf(TargetColourVariantProductModel.class, baseProduct,
                    "Size variant must always have a colour variant as a base product");
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)baseProduct;
            return colourVariant.getDisplayName();
        }
        else if (model instanceof TargetColourVariantProductModel) {
            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)model;
            final ProductModel baseProduct = colourVariant.getBaseProduct();
            Assert.notNull(baseProduct, "Colour variant must have a base product");
            Assert.isInstanceOf(TargetProductModel.class, baseProduct,
                    "Colour variant must always have a target product as a base product");
            final TargetProductModel targetProduct = (TargetProductModel)baseProduct;
            final Collection<VariantProductModel> variants = targetProduct.getVariants();
            if (variants.size() > 1) {

                String displayColour = null;
                if (StringUtils.isNotEmpty(colourVariant.getSwatchName())
                        && null != colourVariant.getSwatch()
                        && colourVariant.getSwatch().getDisplay().booleanValue()) {
                    displayColour = colourVariant.getSwatchName();
                }
                else if (StringUtils.isNotEmpty(colourVariant.getColourName()) && null != colourVariant.getColour()
                        && colourVariant.getColour().getDisplay().booleanValue()) {
                    displayColour = colourVariant.getColourName();
                }

                if (StringUtils.isNotEmpty(displayColour)) {
                    return targetProduct.getName() + SEPARATOR + displayColour;
                }
                else {
                    return targetProduct.getName();
                }

            }
            else {
                return colourVariant.getName();
            }
        }
        return model.getName();
    }

    @Override
    public void set(final AbstractTargetVariantProductModel model, final String value) {
        throw new UnsupportedOperationException(
                "Product variant display name is a dynamic attribute, it cannot be set");
    }
}

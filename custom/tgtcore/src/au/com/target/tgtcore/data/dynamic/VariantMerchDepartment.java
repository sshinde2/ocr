package au.com.target.tgtcore.data.dynamic;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;

import org.springframework.util.Assert;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * Get the merchandise department from the base product of this variant.
 * 
 */
public class VariantMerchDepartment extends
        AbstractDynamicAttributeHandler<TargetMerchDepartmentModel, AbstractTargetVariantProductModel> {

    @Override
    public TargetMerchDepartmentModel get(final AbstractTargetVariantProductModel model)
    {
        Assert.notNull(model);

        final ProductModel baseProduct = model.getBaseProduct();

        if (baseProduct instanceof AbstractTargetVariantProductModel) {
            return ((AbstractTargetVariantProductModel)baseProduct).getMerchDepartment();
        }
        else if (baseProduct instanceof TargetProductModel) {
            return ((TargetProductModel)baseProduct).getMerchDepartment();
        }

        return null;
    }

}

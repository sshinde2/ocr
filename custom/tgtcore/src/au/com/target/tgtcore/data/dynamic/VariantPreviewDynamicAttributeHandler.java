/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtutility.util.TargetProductUtils;


/**
 * @author rmcalave
 *
 */
public class VariantPreviewDynamicAttributeHandler
        implements DynamicAttributeHandler<Boolean, AbstractTargetVariantProductModel> {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler#get(de.hybris.platform.servicelayer.model.AbstractItemModel)
     */
    @Override
    public Boolean get(final AbstractTargetVariantProductModel model) {
        final TargetProductModel baseProduct = TargetProductUtils.getBaseTargetProduct(model);
        return baseProduct.getPreview();

    }

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler#set(de.hybris.platform.servicelayer.model.AbstractItemModel, java.lang.Object)
     */
    @Override
    public void set(final AbstractTargetVariantProductModel model, final Boolean value) {
        throw new UnsupportedOperationException();
    }
}

package au.com.target.tgtcore.datasource;

import de.hybris.platform.core.DataSourceImplFactory;
import de.hybris.platform.core.Tenant;
import de.hybris.platform.jdbcwrapper.HybrisDataSource;
import de.hybris.platform.util.Config.SystemSpecificParams;
import de.hybris.platform.util.config.ConfigIntf;

import java.util.Map;

import org.jasypt.encryption.StringEncryptor;
import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.EnvironmentStringPBEConfig;
import org.jasypt.encryption.pbe.config.PBEConfig;
import org.jasypt.properties.PropertyValueEncryptionUtils;


/**
 * Custom data source factory which understands encrypted connection parameters. This implementation uses the jasypt
 * library for decryption.
 */
public class TargetDatasourceFactory extends DataSourceImplFactory {

    private static final String PBE_ALGORITHM = "db.pool.pbeAlgorithm";
    private static final String PBE_ENCRYPTOR_POOL_SIZE = "db.pool.pbeEncryptorPoolSize";
    private static final String PBE_PASSWORD_ENV_NAME = "db.pool.pbePasswordEnvName";

    private PooledPBEStringEncryptor encryptor;
    private EnvironmentStringPBEConfig pbeConfig;

    @Override
    public HybrisDataSource createDataSource(final String id, final Tenant tenant,
            final Map<String, String> connectionParams,
            final boolean readOnly)
    {
        decryptConnectionParamsIfRequired(tenant.getConfig(), connectionParams);
        return super.createDataSource(id, tenant, connectionParams, readOnly);
    }

    private void decryptConnectionParamsIfRequired(final ConfigIntf config, final Map<String, String> connectionParams) {

        // only checks password at the moment
        final String password = connectionParams.get(SystemSpecificParams.DB_PASSWORD);
        if (PropertyValueEncryptionUtils.isEncryptedValue(password)) {
            final String decryptedPassword = PropertyValueEncryptionUtils.decrypt(password,
                    getEncryptor(config));
            connectionParams.put(SystemSpecificParams.DB_PASSWORD, decryptedPassword);
        }
    }

    private StringEncryptor getEncryptor(final ConfigIntf config) {
        if (encryptor == null) {
            initEncryptor(config);
        }
        return encryptor;
    }

    private void initEncryptor(final ConfigIntf config) {
        encryptor = new PooledPBEStringEncryptor();
        encryptor.setPoolSize(getPBEPoolSize(config));
        encryptor.setConfig(getPBEConfig(config));
    }

    private PBEConfig getPBEConfig(final ConfigIntf config) {
        if (pbeConfig == null) {
            initPBEConfig(config);
        }
        return pbeConfig;
    }

    private void initPBEConfig(final ConfigIntf config) {
        pbeConfig = new EnvironmentStringPBEConfig();
        pbeConfig.setAlgorithm(getPBEAlgorithm(config));
        pbeConfig.setPasswordEnvName(getPBEPasswordEnvName(config));
    }

    private int getPBEPoolSize(final ConfigIntf config) {
        return config.getInt(PBE_ENCRYPTOR_POOL_SIZE, 3);
    }

    private String getPBEAlgorithm(final ConfigIntf config) {
        return config.getString(PBE_ALGORITHM, null);
    }

    private String getPBEPasswordEnvName(final ConfigIntf config) {
        return config.getString(PBE_PASSWORD_ENV_NAME, null);
    }

}

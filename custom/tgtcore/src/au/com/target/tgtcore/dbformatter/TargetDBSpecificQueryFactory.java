package au.com.target.tgtcore.dbformatter;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.dbformatter.formatter.TargetHsqlDbQueryFormatter;
import au.com.target.tgtcore.dbformatter.formatter.TargetMySqlQueryFormatter;
import au.com.target.tgtcore.dbformatter.formatter.TargetSqlServerQueryFormatter;



/**
 * This class will give the corresponding db query formatter based on the database connected. As of now HSQLDB,
 * SQLSERVER and MYSQL are supported.
 */
public class TargetDBSpecificQueryFactory {

    private static final String SQLSERVER = "sqlserver";

    private static final String HSQLDB = "hsqldb";

    private static final String MYSQL = "mysql";


    private String dbIdentifier;

    private TargetHsqlDbQueryFormatter targetHsqlDbQueryFormatter;

    private TargetSqlServerQueryFormatter targetSqlServerQueryFormatter;

    private TargetMySqlQueryFormatter targetMySqlQueryFormatter;

    /**
     * This class will give the corresponding db query formatter instance.
     * 
     * @return TargetDBSpecificQueryFormatter
     */
    public TargetDBSpecificQueryFormatter getDBSpecificQueryFormatter() {
        TargetDBSpecificQueryFormatter targetDBSpecificQueryFormatter = null;

        dbIdentifier = dbIdentifier.toLowerCase();
        if (StringUtils.contains(dbIdentifier, SQLSERVER)) {
            targetDBSpecificQueryFormatter = targetSqlServerQueryFormatter;
        }
        else if (StringUtils.contains(dbIdentifier, HSQLDB)) {
            targetDBSpecificQueryFormatter = targetHsqlDbQueryFormatter;
        }
        else if (StringUtils.contains(dbIdentifier, MYSQL)) {
            targetDBSpecificQueryFormatter = targetMySqlQueryFormatter;
        }

        return targetDBSpecificQueryFormatter;
    }

    /**
     * @param dbIdentifier
     *            the dbIdentifier to set
     */
    @Required
    public void setDbIdentifier(final String dbIdentifier) {
        this.dbIdentifier = dbIdentifier;
    }

    /**
     * @param targetHsqlDbQueryFormatter
     *            the targetHsqlDbQueryFormatter to set
     */
    @Required
    public void setTargetHsqlDbQueryFormatter(final TargetHsqlDbQueryFormatter targetHsqlDbQueryFormatter) {
        this.targetHsqlDbQueryFormatter = targetHsqlDbQueryFormatter;
    }

    /**
     * @param targetSqlServerQueryFormatter
     *            the targetSqlServerQueryFormatter to set
     */
    @Required
    public void setTargetSqlServerQueryFormatter(final TargetSqlServerQueryFormatter targetSqlServerQueryFormatter) {
        this.targetSqlServerQueryFormatter = targetSqlServerQueryFormatter;
    }

    /**
     * @param targetMySqlQueryFormatter
     *            the targetMySqlQueryFormatter to set
     */
    @Required
    public void setTargetMySqlQueryFormatter(final TargetMySqlQueryFormatter targetMySqlQueryFormatter) {
        this.targetMySqlQueryFormatter = targetMySqlQueryFormatter;
    }



}

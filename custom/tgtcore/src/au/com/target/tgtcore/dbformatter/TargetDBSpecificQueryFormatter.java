package au.com.target.tgtcore.dbformatter;

/**
 * This is an interface which will contain the declaration for the different formatter functions.
 */
public interface TargetDBSpecificQueryFormatter {

    String DAYS = "DAYS";
    String HOURS = "HOURS";
    String MINUTES = "MINUTES";
    String SECONDS = "SECONDS";


    /**
     * This method will give the formatted query for finding the difference between two dates based on the identifier.
     * The identifier supported are Hours, Minutes, Seconds.
     * 
     * @param identifier
     * @param date1
     * @param date2
     * @return formattedQuery
     */
    String formatDateDiff(final String identifier, final String date1, final String date2);

}

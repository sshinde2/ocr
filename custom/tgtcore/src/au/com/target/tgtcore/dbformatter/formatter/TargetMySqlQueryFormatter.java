/**
 * 
 */
package au.com.target.tgtcore.dbformatter.formatter;

import au.com.target.tgtcore.dbformatter.TargetDBSpecificQueryFormatter;


/**
 * This class is responsible for implementing methods which require MySql specific syntax and functions
 * 
 * @author pthoma20
 * 
 */
public class TargetMySqlQueryFormatter implements TargetDBSpecificQueryFormatter {

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.data.dbwrapper.TargetDBSpecificConverter#formatDateDiff(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String formatDateDiff(final String identifier, final String date1, final String date2) {
        String returnQuery = "";
        if (identifier.equalsIgnoreCase(DAYS)) {
            returnQuery = " TIMESTAMPDIFF(DAY," + date1 + "," + date2 + ") ";
        }
        else if (identifier.equalsIgnoreCase(HOURS)) {
            returnQuery = " TIMESTAMPDIFF(HOUR," + date1 + "," + date2 + ") ";
        }
        else if (identifier.equalsIgnoreCase(MINUTES)) {
            returnQuery = " TIMESTAMPDIFF(MINUTE," + date1 + "," + date2 + ") ";
        }
        else if (identifier.equalsIgnoreCase(SECONDS)) {
            returnQuery = " TIMESTAMPDIFF(SECOND," + date1 + "," + date2 + ") ";
        }
        return returnQuery;
    }
}

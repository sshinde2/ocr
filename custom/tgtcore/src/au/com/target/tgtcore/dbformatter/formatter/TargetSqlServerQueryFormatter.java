/**
 * 
 */
package au.com.target.tgtcore.dbformatter.formatter;

import au.com.target.tgtcore.dbformatter.TargetDBSpecificQueryFormatter;



/**
 * This class is responsible for implementing methods which require SQLSERVER specific syntax and functions
 * 
 * @author pthoma20
 * 
 */
public class TargetSqlServerQueryFormatter implements TargetDBSpecificQueryFormatter {

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.data.dbwrapper.TargetDBSpecificConverter#formatDateDiff(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String formatDateDiff(final String identifier, final String date1, final String date2) {
        String returnQuery = "";
        if (identifier.equalsIgnoreCase(DAYS)) {
            returnQuery = " DATEDIFF(day," + date1 + "," + date2 + ") ";
        }
        else if (identifier.equalsIgnoreCase(HOURS)) {
            returnQuery = " DATEDIFF(hour," + date1 + "," + date2 + ") ";
        }
        else if (identifier.equalsIgnoreCase(MINUTES)) {
            returnQuery = " DATEDIFF(minute," + date1 + "," + date2 + ") ";
        }
        else if (identifier.equalsIgnoreCase(SECONDS)) {
            returnQuery = " DATEDIFF(second," + date1 + "," + date2 + ") ";
        }
        return returnQuery;
    }
}

package au.com.target.tgtcore.deals;

// CHECKSTYLE:OFF
import au.com.target.tgtcore.model.MissingDealProductInfoModel;


//CHECKSTYLE:ON

/**
 * Service API to manage {@link MissingDealProductInfoModel}
 */
public interface MissingDealProductInfoService {


    /**
     * Remove the missing deal products records that belong to expired deals or those that no longer exists
     */
    void purgeMissingProductsForExpiredDeals();

    /**
     * Attempt to associate products from MissingDealProductInfo with their respective categories.
     * 
     * Removes any MissingDealProductInfoModel instances that were successfully linked.
     */
    void assocateMissingProductsWithCategories();
}

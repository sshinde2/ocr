package au.com.target.tgtcore.deals.dao;

import java.util.List;

import au.com.target.tgtcore.model.MissingDealProductInfoModel;


/**
 * DAO to manage {@link MissingDealProductInfoModel}
 */
public interface MissingDealProductInfoDao {

    /**
     * Find all the missing deal product records in the system
     * 
     * @return all missing deal product records
     */
    List<MissingDealProductInfoModel> findAll();


    /**
     * Find the missing deal products records that belong to expired and removed deals
     * 
     * @return missing deal products that belong to expired deals including removed ones
     */
    List<MissingDealProductInfoModel> findMissingProductsForExpiredDeals();

}

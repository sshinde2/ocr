package au.com.target.tgtcore.deals.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.deals.dao.MissingDealProductInfoDao;
import au.com.target.tgtcore.deals.dao.TargetDealDao;
import au.com.target.tgtcore.model.MissingDealProductInfoModel;


/**
 *
 */
public class MissingDealProductInfoDaoImpl extends DefaultGenericDao<MissingDealProductInfoModel> implements
        MissingDealProductInfoDao {

    private static final String FIND_MISSING_PRODUCTS_FOR_EXPIRED_DEALS = "SELECT {" + MissingDealProductInfoModel.PK
            + "} FROM {" + MissingDealProductInfoModel._TYPECODE + "} WHERE {" + MissingDealProductInfoModel.DEALID
            + "} NOT IN (?activeDealCodes)";

    private TargetDealDao targetDealDao;

    public MissingDealProductInfoDaoImpl() {
        super(MissingDealProductInfoModel._TYPECODE);
    }

    @Override
    public List<MissingDealProductInfoModel> findAll() {
        return find();
    }

    @Override
    public List<MissingDealProductInfoModel> findMissingProductsForExpiredDeals() {

        final List<String> activeDealCodes = targetDealDao.getAllActiveDealCodes();

        if (CollectionUtils.isEmpty(activeDealCodes)) {
            return findAll();
        }
        else {
            final SearchResult<MissingDealProductInfoModel> result = getFlexibleSearchService().search(
                    FIND_MISSING_PRODUCTS_FOR_EXPIRED_DEALS,
                    Collections.singletonMap("activeDealCodes", activeDealCodes));
            return result.getResult();
        }
    }

    /**
     * @param targetDealDao
     *            the targetDealDao to set
     */
    @Required
    public void setTargetDealDao(final TargetDealDao targetDealDao) {
        this.targetDealDao = targetDealDao;
    }

}

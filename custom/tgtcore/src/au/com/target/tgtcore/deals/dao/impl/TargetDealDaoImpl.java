package au.com.target.tgtcore.deals.dao.impl;

import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import au.com.target.tgtcore.deals.dao.TargetDealDao;


/**
 * Default implementation of {@link TargetDealDao}
 * 
 */
public class TargetDealDaoImpl extends DefaultGenericDao<AbstractDealModel> implements TargetDealDao {

    private static final String FIND_ALL_ACTIVE_DEALS = "SELECT {" + AbstractDealModel.PK + "} FROM {"
            + AbstractDealModel._TYPECODE
            + "} WHERE {" + AbstractDealModel.ENDDATE + "} IS NULL OR {" + AbstractDealModel.ENDDATE + "} >= ?now";


    private static final String FIND_ALL_ACTIVE_DEAL_CODES = "SELECT {" + AbstractDealModel.CODE + "} FROM {"
            + AbstractDealModel._TYPECODE + "} WHERE {" + AbstractDealModel.ENDDATE + "} IS NULL OR {"
            + AbstractDealModel.ENDDATE + "} >= ?now";

    private static final String FIND_DEAL_FOR_ID = "SELECT {" + AbstractDealModel.PK + "} FROM {"
            + AbstractDealModel._TYPECODE + "} WHERE {" + AbstractDealModel.CODE + "} = ?code AND {"
            + AbstractDealModel.IMMUTABLEKEYHASH + "} IS NULL";

    public TargetDealDaoImpl() {
        super(AbstractDealModel._TYPECODE);
    }

    @Override
    public AbstractDealModel getDealForId(final String dealId) {
        ServicesUtil.validateParameterNotNullStandardMessage("dealId", dealId);

        final SearchResult<AbstractDealModel> result = getFlexibleSearchService().search(FIND_DEAL_FOR_ID,
                Collections.singletonMap("code", dealId));
        final List<AbstractDealModel> deals = result.getResult();

        ServicesUtil.validateIfSingleResult(deals, AbstractDealModel.class, AbstractDealModel.CODE, dealId);
        return deals.get(0);
    }

    @Override
    public List<AbstractDealModel> getAllActiveDeals() {
        final SearchResult<AbstractDealModel> searchResult = getFlexibleSearchService().search(FIND_ALL_ACTIVE_DEALS,
                Collections.singletonMap("now", new Date()));
        return searchResult.getResult();
    }

    @Override
    public List<String> getAllActiveDealCodes() {
        final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(FIND_ALL_ACTIVE_DEAL_CODES);
        flexibleSearchQuery.setResultClassList(Collections.singletonList(String.class));
        flexibleSearchQuery.addQueryParameter("now", new Date());
        final SearchResult<String> searchResult = getFlexibleSearchService().search(flexibleSearchQuery);
        return searchResult.getResult();
    }
}

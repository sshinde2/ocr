package au.com.target.tgtcore.deals.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.deals.MissingDealProductInfoService;
import au.com.target.tgtcore.deals.dao.MissingDealProductInfoDao;
import au.com.target.tgtcore.model.MissingDealProductInfoModel;


/**
 * Default Implementation of {@link MissingDealProductInfoService}
 */
public class MissingDealProductInfoServiceImpl extends AbstractBusinessService implements MissingDealProductInfoService {

    private static final Logger LOG = Logger.getLogger(MissingDealProductInfoServiceImpl.class);

    private int batchSizeForRemove = 500;

    private MissingDealProductInfoDao missingDealProductInfoDao;

    private ProductService productService;
    private CategoryService categoryService;
    private CatalogVersionService catalogVersionService;

    @Override
    public void purgeMissingProductsForExpiredDeals() {

        final List<MissingDealProductInfoModel> missingProductsForExpiredDeals = missingDealProductInfoDao
                .findMissingProductsForExpiredDeals();

        if (CollectionUtils.isNotEmpty(missingProductsForExpiredDeals)) {
            LOG.info("Found " + missingProductsForExpiredDeals.size()
                    + " orphan records to remove from 'MissingDealProductInfo'");
            removeMissingProductsInBatch(missingProductsForExpiredDeals, batchSizeForRemove);
        }
        else {
            LOG.info("Did not find any orphan records to remove from 'MissingDealProductsInfo");
        }

    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtcore.deals.MissingDealProductInfoService#assocateMissingProductsWithCategories()
     */
    @Override
    public void assocateMissingProductsWithCategories() {
        // Retrieve all missing products
        final List<MissingDealProductInfoModel> missingDealProducts = missingDealProductInfoDao.findAll();

        if (CollectionUtils.isEmpty(missingDealProducts)) {
            return;
        }

        // Maps to store modified deal categories, with the key being the deal category code.
        // This is so that if we need to modify a category more than once we only need to 
        // retrieve and save it once.
        final Map<String, CategoryModel> stagedDealCategories = new HashMap<String, CategoryModel>();
        final Map<String, CategoryModel> onlineDealCategories = new HashMap<String, CategoryModel>();

        // A list to store all of the MissingDealProductInfoModel instances that get successfully linked to
        // a category (so that we can remove them).
        final List<MissingDealProductInfoModel> missingDealProductsSuccessfullyLinked = new ArrayList<>();

        final CatalogVersionModel stagedProductCatalog = getStagedProductCatalog();
        final CatalogVersionModel onlineProductCatalog = getOnlineProductCatalog();

        for (final MissingDealProductInfoModel missingDealProductInfo : missingDealProducts) {
            // Attempt to find the missing product in the Online catalog and link it to it's
            // deal category.
            final boolean onlineSuccess = associateProductWithCategory(missingDealProductInfo, onlineProductCatalog,
                    onlineDealCategories);

            // If we weren't able to link a missing product with it's Online deal category
            // there's no point attempting to link it with the Staged version.
            if (!onlineSuccess) {
                continue;
            }

            // Attempt to find the missing product in the Staged catalog and link it to it's
            // deal category. If it can't be done it will be handled during catalog sync later.
            final boolean stagedSuccess = associateProductWithCategory(missingDealProductInfo, stagedProductCatalog,
                    stagedDealCategories);

            if (stagedSuccess) {
                // This missing product was successfully linked with it's Staged and Online categories, so
                // we can remove it.
                missingDealProductsSuccessfullyLinked.add(missingDealProductInfo);
            }
        }

        // Save all the categories we changed...
        if (MapUtils.isNotEmpty(stagedDealCategories)) {
            getModelService().saveAll(stagedDealCategories.values());
        }
        if (MapUtils.isNotEmpty(onlineDealCategories)) {
            getModelService().saveAll(onlineDealCategories.values());
        }

        // ... and remove the missing product info models that were successfully linked.
        if (CollectionUtils.isNotEmpty(missingDealProductsSuccessfullyLinked)) {
            getModelService().removeAll(missingDealProductsSuccessfullyLinked);
        }
    }

    /**
     * @param missingProductsForExpiredDeals
     */
    private void removeMissingProductsInBatch(final List<MissingDealProductInfoModel> missingProductsForExpiredDeals,
            final int batchSize) {
        int count = 0;
        final int missingProductsToRemoveSize = missingProductsForExpiredDeals.size();
        final List<MissingDealProductInfoModel> missingProductsToRemove = new ArrayList<MissingDealProductInfoModel>(
                batchSize);
        for (final MissingDealProductInfoModel missingDealProductInfo : missingProductsForExpiredDeals) {
            count++;
            missingProductsToRemove.add(missingDealProductInfo);
            if (count % batchSize == 0) {
                getModelService().removeAll(missingProductsToRemove);
                LOG.info("Removed " + count + " out of " + missingProductsToRemoveSize
                        + " records from 'MissingDealProductInfo'");
                missingProductsToRemove.clear();
            }
        }

        if (CollectionUtils.isNotEmpty(missingProductsToRemove)) {
            getModelService().removeAll(missingProductsToRemove);
            LOG.info("Removed " + count + " out of " + missingProductsToRemoveSize
                    + " records from 'MissingDealProductInfo'");
        }

    }

    private boolean associateProductWithCategory(final MissingDealProductInfoModel missingDealProductInfo,
            final CatalogVersionModel catalogVersion, final Map<String, CategoryModel> dealCategories) {
        final String productCode = missingDealProductInfo.getProductCode();

        // Attempt to find the missing product
        ProductModel product = null;
        CategoryModel dealCategory = null;
        try {
            product = productService.getProductForCode(catalogVersion, productCode);

            // Found the product, get the deal category it belongs in
            // Have we used it already?
            dealCategory = dealCategories.get(missingDealProductInfo.getDealCategoryCode());

            if (dealCategory == null) {
                // We haven't used this category yet, so retrieve it
                dealCategory = categoryService.getCategoryForCode(catalogVersion,
                        missingDealProductInfo.getDealCategoryCode());
                dealCategories.put(dealCategory.getCode(), dealCategory);
            }
        }
        catch (final UnknownIdentifierException ex) {
            // Product or category still doesn't exist
            LOG.warn("Product or category does not exist for product: " + productCode + " : " + ex.getMessage());
            return false;
        }

        // Add the product to the category
        final List<ProductModel> dealCategoryProducts = new ArrayList<ProductModel>(dealCategory.getProducts());
        dealCategoryProducts.add(product);
        dealCategory.setProducts(dealCategoryProducts);

        return true;
    }

    private CatalogVersionModel getStagedProductCatalog() {
        return catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION);
    }

    private CatalogVersionModel getOnlineProductCatalog() {
        return catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.ONLINE_VERSION);
    }

    /**
     * @param missingDealProductInfoDao
     *            the missingDealProductInfoDao to set
     */
    @Required
    public void setMissingDealProductInfoDao(final MissingDealProductInfoDao missingDealProductInfoDao) {
        this.missingDealProductInfoDao = missingDealProductInfoDao;
    }

    /**
     * @param batchSizeToRemove
     *            the batchSizeToRemove to set
     */
    public void setBatchSizeForRemove(final int batchSizeToRemove) {
        this.batchSizeForRemove = batchSizeToRemove;
    }

    /**
     * @param productService
     *            the productService to set
     */
    @Required
    public void setProductService(final ProductService productService) {
        this.productService = productService;
    }

    /**
     * @param categoryService
     *            the categoryService to set
     */
    @Required
    public void setCategoryService(final CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }
}

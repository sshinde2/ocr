package au.com.target.tgtcore.deals.impl;

import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.deals.TargetDealService;
import au.com.target.tgtcore.deals.dao.TargetDealDao;
import au.com.target.tgtcore.model.TargetDealCategoryModel;


/**
 * 
 * Default implementation of {@link TargetDealService}
 * 
 */
public class TargetDealServiceImpl extends AbstractBusinessService implements TargetDealService {

    private TargetDealDao targetDealDao;

    @Override
    public AbstractDealModel getDealForId(final String dealId) {
        Assert.notNull(dealId, "Deal Id must not be null");
        final AbstractDealModel dealModel = targetDealDao.getDealForId(dealId);
        return dealModel;
    }

    @Override
    public List<AbstractDealModel> getAllActiveDeals() {
        final List<AbstractDealModel> activeDeals = targetDealDao.getAllActiveDeals();
        return activeDeals;
    }

    /**
     * @param targetDealDao
     *            the targetDealDao to set
     */
    @Required
    public void setTargetDealDao(final TargetDealDao targetDealDao) {
        this.targetDealDao = targetDealDao;
    }

    @Override
    public AbstractSimpleDealModel getDealByCategory(final TargetDealCategoryModel dealCategoryModel) {
        AbstractSimpleDealModel dealModel = null;
        if (dealCategoryModel != null) {
            if (CollectionUtils.isNotEmpty(dealCategoryModel.getDeal())) {
                final Collection<AbstractSimpleDealModel> dealsList = dealCategoryModel.getDeal();
                for (final AbstractSimpleDealModel abstractSimpleDealModel : dealsList) {
                    if (StringUtils.isBlank(abstractSimpleDealModel.getImmutableKeyHash())) {
                        dealModel = abstractSimpleDealModel;
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(dealCategoryModel.getQualifier())) {
                for (final DealQualifierModel qulifierModel : dealCategoryModel.getQualifier()) {
                    if (qulifierModel != null && qulifierModel.getDeal() != null
                            && StringUtils.isBlank(qulifierModel.getDeal().getImmutableKeyHash())) {
                        dealModel = qulifierModel.getDeal();
                    }
                }
            }
        }
        return dealModel;
    }


}

package au.com.target.tgtcore.deals.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.deals.MissingDealProductInfoService;
//CHECKSTYLE:OFF
import au.com.target.tgtcore.model.MissingDealProductInfoModel;


//CHECKSTYLE:ON

/**
 * Job that purge {@link MissingDealProductInfoModel} records that belong to expired or removed deals
 */
public class PurgeMissingDealProductsJob extends AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(PurgeMissingDealProductsJob.class);

    private MissingDealProductInfoService missingDealProductInfoService;

    @Override
    public PerformResult perform(final CronJobModel cronJob) {
        LOG.info("Starting PurgeMissingDealProductsJob");
        missingDealProductInfoService.purgeMissingProductsForExpiredDeals();
        LOG.info("Finished PurgeMissingDealProductsJob");
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    /**
     * @param missingDealProductInfoService
     *            the missingDealProductInfoService to set
     */
    @Required
    public void setMissingDealProductInfoService(final MissingDealProductInfoService missingDealProductInfoService) {
        this.missingDealProductInfoService = missingDealProductInfoService;
    }

}

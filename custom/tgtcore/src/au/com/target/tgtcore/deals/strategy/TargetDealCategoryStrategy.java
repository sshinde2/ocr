/**
 * 
 */
package au.com.target.tgtcore.deals.strategy;

import de.hybris.platform.promotions.model.AbstractDealModel;

import au.com.target.tgtcore.model.TargetDealCategoryModel;


/**
 * @author bhuang3
 * 
 */
public interface TargetDealCategoryStrategy {

    /**
     * check if the deal category is the qualifier deal category
     * 
     * @param targetDealCategoryModel
     * @return true or false
     */
    boolean isQualifierDealCategory(TargetDealCategoryModel targetDealCategoryModel);

    /**
     * check if the deal category is the reward deal category
     * 
     * @param targetDealCategoryModel
     * @return true or false
     */
    boolean isRewardDealCategory(TargetDealCategoryModel targetDealCategoryModel);

    /**
     * check if the deal is active
     * 
     * @param abstractDealModel
     * @return true or false
     */
    boolean isActiveDeal(AbstractDealModel abstractDealModel);
}

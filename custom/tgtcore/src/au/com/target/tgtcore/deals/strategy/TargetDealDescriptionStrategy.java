/**
 * 
 */
package au.com.target.tgtcore.deals.strategy;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;


/**
 * @author mjanarth
 * 
 */
public interface TargetDealDescriptionStrategy {


    /**
     * Gets the summary of deal description
     * 
     * @param variantModel
     * @return DealDescription
     */
    String getSummaryDealDescription(TargetColourVariantProductModel variantModel);


}

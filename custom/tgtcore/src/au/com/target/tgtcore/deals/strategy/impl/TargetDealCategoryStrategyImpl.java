/**
 * 
 */
package au.com.target.tgtcore.deals.strategy.impl;

import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;

import java.util.Date;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.deals.TargetDealService;
import au.com.target.tgtcore.deals.strategy.TargetDealCategoryStrategy;
import au.com.target.tgtcore.model.TargetDealCategoryModel;


/**
 * @author bhuang3
 * 
 */
public class TargetDealCategoryStrategyImpl implements TargetDealCategoryStrategy {

    private TargetDealService targetDealService;

    @Override
    public boolean isQualifierDealCategory(final TargetDealCategoryModel targetDealCategoryModel) {
        if (targetDealCategoryModel != null) {
            final AbstractSimpleDealModel dealModel = targetDealService.getDealByCategory(targetDealCategoryModel);
            if (dealModel != null) {
                for (final DealQualifierModel qualifierModel : dealModel.getQualifierList()) {
                    if (CollectionUtils.isNotEmpty(qualifierModel.getDealCategory())
                            && qualifierModel.getDealCategory().contains(targetDealCategoryModel)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    @Override
    public boolean isRewardDealCategory(final TargetDealCategoryModel targetDealCategoryModel) {
        if (targetDealCategoryModel != null) {
            final AbstractSimpleDealModel dealModel = targetDealService.getDealByCategory(targetDealCategoryModel);
            if (dealModel != null) {
                if (CollectionUtils.isNotEmpty(dealModel.getRewardCategory())
                        && dealModel.getRewardCategory().contains(targetDealCategoryModel)) {
                    return true;
                }
            }
        }
        return false;
    }


    @Required
    public void setTargetDealService(final TargetDealService targetDealService) {
        this.targetDealService = targetDealService;
    }


    @Override
    public boolean isActiveDeal(final AbstractDealModel abstractDealModel) {
        if (abstractDealModel == null) {
            return false;
        }
        final Boolean isEnable = abstractDealModel.getEnabled();
        if (isEnable == null || BooleanUtils.isFalse(isEnable)) {
            return false;
        }
        final Date now = new Date();
        final Date startDate = abstractDealModel.getStartDate();
        if (startDate == null || startDate.after(now)) {
            return false;
        }
        final Date endDate = abstractDealModel.getEndDate();
        if (endDate != null && endDate.before(now)) {
            return false;
        }
        return true;
    }
}

/**
 * 
 */
package au.com.target.tgtcore.deals.strategy.impl;

import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.deals.strategy.TargetDealDescriptionStrategy;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * @author mjanarth
 * 
 */
public class TargetDealDescriptionStrategyImpl implements TargetDealDescriptionStrategy {


    private PromotionsService promotionsService;

    private String defaultDealDescriptionMessage;

    @Override
    public String getSummaryDealDescription(final TargetColourVariantProductModel variantModel) {
        String dealDescription = null;
        final Date currentdate = new Date();
        final List<PromotionGroupModel> promotionGroupModelList = new ArrayList<>();
        List<ProductPromotionModel> promotionModelList = new ArrayList<>();
        promotionGroupModelList.add(promotionsService.getDefaultPromotionGroup());
        final Collection<VariantProductModel> sizeVariants = variantModel.getVariants();
        if (CollectionUtils.isNotEmpty(sizeVariants)) {
            boolean firstVariantHasNoDeal = false;
            boolean atLeastOneDealExistforSizeVariants = false;
            int counter = 0;
            String tempCode = null;
            for (final VariantProductModel sizeVariant : sizeVariants) {
                counter++;
                if (sizeVariant instanceof TargetSizeVariantProductModel) {
                    String promotionCode = null;
                    promotionModelList = promotionsService
                            .getProductPromotions(promotionGroupModelList, sizeVariant, true, currentdate);

                    final AbstractDealModel promotionModel = findAbstractDealModel(promotionModelList);
                    if (promotionModel != null) {
                        atLeastOneDealExistforSizeVariants = true;
                        promotionCode = promotionModel.getCode();
                        dealDescription = promotionModel.getDescription();
                        if (((StringUtils.isNotBlank(tempCode))
                                && (!(promotionCode.equalsIgnoreCase(tempCode)))) || (firstVariantHasNoDeal)) {
                            dealDescription = defaultDealDescriptionMessage;
                            break;
                        }
                        tempCode = promotionCode;
                    }
                    else {
                        if (counter == 1) {
                            firstVariantHasNoDeal = true;
                        }
                        else if (atLeastOneDealExistforSizeVariants) {
                            dealDescription = defaultDealDescriptionMessage;
                            break;
                        }
                    }

                }
            }

        }
        else {
            promotionModelList = promotionsService
                    .getProductPromotions(promotionGroupModelList, variantModel, true, currentdate);
            if (CollectionUtils.isNotEmpty(promotionModelList)) {
                final AbstractDealModel promotionModel = findAbstractDealModel(promotionModelList);
                if (promotionModel != null) {
                    dealDescription = promotionModel.getDescription();

                }

            }
        }

        return dealDescription;
    }

    private AbstractDealModel findAbstractDealModel(final List<ProductPromotionModel> promotionList)
    {
        AbstractDealModel abstractDealModel = null;
        if (CollectionUtils.isNotEmpty(promotionList)) {
            for (final ProductPromotionModel promotionModel : promotionList) {
                if (promotionModel instanceof AbstractDealModel) {
                    abstractDealModel = (AbstractDealModel)promotionModel;
                }
            }
        }
        return abstractDealModel;
    }

    /**
     * @param defaultDealDescriptionMessage
     *            the defaultDealDescriptionMessage to set
     */
    @Required
    public void setDefaultDealDescriptionMessage(final String defaultDealDescriptionMessage) {
        this.defaultDealDescriptionMessage = defaultDealDescriptionMessage;
    }

    /**
     * @param promotionsService
     *            the promotionsService to set
     */
    @Required
    public void setPromotionsService(final PromotionsService promotionsService) {
        this.promotionsService = promotionsService;
    }


}

/**
 * 
 */
package au.com.target.tgtcore.deals.wrappers;

import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.promotions.result.PromotionEvaluationContext;
import de.hybris.platform.promotions.result.PromotionOrderView;

import java.util.List;



/**
 * Wrapper to support calculating deals. This exists to overcome the following problems:
 * <ul>
 * <li>Jalo layer does not utilise the model classes from the service layer in hybris, this makes getting custom
 * attributes a little more awkward and unsafe</li>
 * <li>We want to leverage the POS logic for applying deals, but it assumes the product has ready access of the deal it
 * is in.</li>
 * 
 */
public interface DealWrapper {
    enum DealType {
        DOLLAR_OFF_EACH,
        DOLLAR_OFF_TOTAL,
        FIXED_DOLLAR_EACH,
        FIXED_DOLLAR_TOTAL,
        PERCENT_OFF_EACH
    }

    /**
     * @return the dealModel
     */
    AbstractDealModel getDealModel();

    /**
     * @return a list deal qualifier models
     */
    List<DealQualifierModel> getQualifierModelList();

    /**
     * @return the ctx
     */
    SessionContext getCtx();

    /**
     * @return the promoContext
     */
    PromotionEvaluationContext getPromoContext();

    /**
     * @return the orderView
     */
    PromotionOrderView getOrderView();

    /**
     * @return the type of the deal
     */
    DealType getDealType();
}

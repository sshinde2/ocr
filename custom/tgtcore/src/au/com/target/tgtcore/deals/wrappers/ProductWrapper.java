/**
 * 
 */
package au.com.target.tgtcore.deals.wrappers;

import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.promotions.model.DealQualifierModel;

import au.com.target.tgtcore.enums.DealItemTypeEnum;


/**
 * Wrapper to support calculating deals. This exists to overcome the following problems:
 * <ul>
 * <li>Jalo layer does not utilise the model classes from the service layer in hybris, this makes getting custom
 * attributes a little more awkward and unsafe</li>
 * <li>We want to leverage the POS logic for applying deals, but it assumes the product has ready access of the deal it
 * is in.</li>
 * 
 */
@SuppressWarnings("deprecation")
public interface ProductWrapper {
    /**
     * @return the qualifier model
     */
    DealQualifierModel getQualifierModel();

    /**
     * Price of a single unit.
     * 
     * @return price in cents
     */
    long getUnitSellPrice();

    /**
     * @return the adjusted unit sell price
     */
    long getAdjustedUnitSellPrice();

    /**
     * Set the required details of a fired deal.
     * 
     * @param inst
     *            which instance of this deal does this entry belong to
     * @param dealType
     *            are we a reward or a qualifying item
     * @param saveAmt
     *            the amout we markdown this item for this deal
     */
    void setDealApplied(final int inst, final DealItemTypeEnum dealType, final long saveAmt);

    /**
     * Remove all calculation that form part of a deal. The getItem method will still give details of the deal this item
     * is part off.
     */
    void clearDeal();

    /**
     * @return true if a deal has been applied to this item entry
     */
    boolean isInDeal();

    /**
     * @return the dealMarkdown
     */
    long getDealMarkdown();

    /**
     * @return the product
     */
    Product getProduct();

    /**
     * @return the type
     */
    DealItemTypeEnum getType();

    /**
     * @return the instance
     */
    int getInstance();

    /**
     * @return true if this product is part of the reward category
     */
    boolean isRewardProduct();

    /**
     * set the dealpartaillyconsumed as true
     */
    void setDealRewardsPartiallyConsumed();

    /**
     * 
     * @return true if the deal is partially consumed,i.e more rewards can be added
     */
    boolean getDealRewardsPartiallyConsumed();

    /**
     * set the dealpartaillyconsumed as false
     */
    void clearDealRewardsPartiallyConsumed();
}

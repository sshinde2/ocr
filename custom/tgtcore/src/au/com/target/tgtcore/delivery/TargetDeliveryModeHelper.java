/**
 * 
 */
package au.com.target.tgtcore.delivery;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;


/**
 * Methods for getting DeliveryModeModels for given delivery types
 * 
 */
public interface TargetDeliveryModeHelper {

    /**
     * Get the DeliveryModeModel for the given code
     * 
     * @param deliveryModeCode
     * @return DeliveryModeModel
     */
    TargetZoneDeliveryModeModel getDeliveryModeModel(final String deliveryModeCode);

    /**
     * Checks if the <code>product</code> is available for <code>deliveryMode</code>.
     * 
     * @param product
     *            the {@link ProductModel} to check availability for
     * @param deliveryMode
     *            the {@link DeliveryModeModel} to check availability against
     * @return <code>true</code> if <code>product</code> is available for <code>deliveryMode</code>, <code>false</code>
     *         otherwise
     */
    boolean isProductAvailableForDeliveryMode(ProductModel product, DeliveryModeModel deliveryMode);


    /**
     * check if the delivery mode is deliver to store
     * 
     * @param deliveryMode
     * @return true if delivery mode is store delivery
     */
    boolean isDeliveryModeStoreDelivery(DeliveryModeModel deliveryMode);


    /**
     * determines if the delivery mode is digital
     * 
     * @param deliveryMode
     * @return true if the delivery mode is digital
     */
    public boolean isDeliveryModeDigital(final DeliveryModeModel deliveryMode);


    /**
     * Does the product have a non digital delivery Mode
     * 
     * @param product
     * @return true if the delivery mode is not digital and false otherwise
     */
    boolean doesProductHavePhysicalDeliveryMode(final AbstractTargetVariantProductModel product);


    /**
     * check digital delivery mode if set on the product
     * 
     * @param product
     * @return digital delivery mode if set else return null
     */
    TargetZoneDeliveryModeModel getDigitalDeliveryMode(ProductModel product);


    /**
     * checks for whether the product has only digital delivery mode
     * 
     * @param product
     * @return true if the delivery mode is only digital and false otherwise
     */
    boolean hasOnlyDigitalDeliveryMode(final ProductModel product);

    /**
     * Get the delivery modes for the base product given a product which may be a variant
     * 
     * @param product
     * @return Set
     */
    Set<DeliveryModeModel> getDeliveryModesForProduct(final ProductModel product);

    /**
     * Get the delivery modes for the base product given a product which may be a variant. The delivery Modes are
     * sorted(ascending) based on the display order
     * 
     * @param product
     * @return Set
     */
    List<DeliveryModeModel> getDeliveryModesSortedForProduct(final ProductModel product);

    /**
     * returns true if the product has digital delivery mode
     * 
     * @param product
     * @return boolean
     */
    public boolean doesProductHaveDigitalDeliveryMode(final AbstractTargetVariantProductModel product);

    /**
     * get all the delivery modes that are active for the product
     * 
     * @param product
     * @return List
     */
    Collection<TargetZoneDeliveryModeModel> getAllCurrentActiveDeliveryModesForProduct(final ProductModel product);

    /**
     * Checks whether delivery mode is applicable for the cart (It should be applicable for all items) and applicaple to
     * atleast one if it is digital
     * 
     * @param cartModel
     * @param deliveryMode
     * @return true if delivery Mode passed is applicable for the order
     */
    boolean isDeliveryModeApplicableForOrder(CartModel cartModel, DeliveryModeModel deliveryMode);

    /**
     * Checks whether the order has one of the products that is digital
     * 
     * @param order
     */
    public boolean hasDigitalProducts(final AbstractOrderModel order);

}
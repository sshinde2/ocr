/**
 * 
 */
package au.com.target.tgtcore.delivery;

import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeValueModel;
import de.hybris.platform.deliveryzone.model.ZoneModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import au.com.target.tgtauspost.model.ShipsterConfigModel;
import au.com.target.tgtcore.delivery.dto.DeliveryCostDto;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;


/**
 * Extend the default DeliveryService
 * 
 */
public interface TargetDeliveryService extends DeliveryService {

    /**
     * Return the Regions (ie states) for the given country, or all Regions if country is null
     * 
     * @param country
     * @return list of RegionModel
     */
    List<RegionModel> getRegionsForCountry(CountryModel country);

    /**
     * Returns the delivery modes for the product from the highest level product
     * 
     * @param product
     * @return set of delivery modes for the product
     */
    Set<DeliveryModeModel> getVariantsDeliveryModesFromBaseProduct(final ProductModel product);

    /**
     * get the delivery cost for the order. If there is no order then return the initial delivery costs
     * 
     * @param deliveryValue
     * @param order
     * @return DeliveryCostDto
     */
    DeliveryCostDto getDeliveryCostForDeliveryType(final AbstractTargetZoneDeliveryModeValueModel deliveryValue,
            final AbstractOrderModel order);

    /**
     * Get the available delivery mode values for the given order.<br/>
     * If there is a current delivery mode assigned to the order then that will be used.
     * 
     * @param abstractOrder
     * @return the set of delivery mode values for the order
     */
    Collection<ZoneDeliveryModeValueModel> getSupportedDeliveryModeValuesForOrder(AbstractOrderModel abstractOrder);

    /**
     * get TargetZoneDeliveryModeValueModel based on a set of params and validate only one
     * TargetZoneDeliveryModeValueModel is returned. This is used in the TargetZoneDeliveryModeValueModel validation
     * interceptor
     * 
     * @param zone
     * @param currency
     * @param min
     * @param zoneDeliveryMode
     * @return AbstractTargetZoneDeliveryModeValueModel
     */
    List<AbstractTargetZoneDeliveryModeValueModel> getDeliveryValue(final ZoneModel zone, final CurrencyModel currency,
            final Double min,
            final ZoneDeliveryModeModel zoneDeliveryMode);

    /**
     * Gets the all currently active delivery modes for a given SalesApplication.
     * 
     * @param salesApplication
     * @return Collection<TargetZoneDeliveryModeModel> the all currently active delivery modes for the given
     *         SalesApplication
     */
    Collection<TargetZoneDeliveryModeModel> getAllCurrentlyActiveDeliveryModes(SalesApplication salesApplication);

    /**
     * Gets a sorted list of AbstractTargetZoneDeliveryModeValueModel based on a zone delivery mode, sorted descending
     * based on priority and minimum spend threshold applicable on a given order.
     * 
     * @param zoneDeliveryMode
     * @param abstractOrder
     * @return AbstractTargetZoneDeliveryModeValueModel
     */
    List<AbstractTargetZoneDeliveryModeValueModel> getAllApplicableDeliveryValuesForModeAndOrder(
            final ZoneDeliveryModeModel zoneDeliveryMode, final AbstractOrderModel abstractOrder);

    /**
     * Gets a sorted list of AbstractTargetZoneDeliveryModeValueModel based on a zone delivery mode, sorted descending
     * based on priority ,minimum spend threshold applicable on a given order without order threshold.
     * 
     * @param zoneDeliveryMode
     * @param abstractOrder
     * @return AbstractTargetZoneDeliveryModeValueModel
     */
    List<AbstractTargetZoneDeliveryModeValueModel> getAllApplicableDeliveryValuesForModeAndOrderWithoutOrderThreshold(
            final ZoneDeliveryModeModel zoneDeliveryMode, final AbstractOrderModel abstractOrder);

    /**
     * Gets a sorted list of AbstractTargetZoneDeliveryModeValueModel based on a zone delivery mode, sorted descending
     * based on priority and minimum spend threshold applicable on a given product.
     * 
     * @param zoneDeliveryMode
     * @param productModel
     * @return AbstractTargetZoneDeliveryModeValueModel
     */
    List<AbstractTargetZoneDeliveryModeValueModel> getAllApplicableDeliveryValuesForModeAndProduct(
            final ZoneDeliveryModeModel zoneDeliveryMode, final ProductModel productModel);

    /**
     * Get all the active delivery modes for order having at least one applicable mode value.
     * 
     * @param abstractOrder
     * @param salesApplication
     * @return Collection<TargetZoneDeliveryModeModel> collection of the current active delivery modes for the given
     *         SalesApplication
     */
    Collection<TargetZoneDeliveryModeModel> getAllApplicableDeliveryModesForOrder(
            final AbstractOrderModel abstractOrder, SalesApplication salesApplication);

    /**
     * get all the active delivery modes for the product having at least one applicable mode value.
     * 
     * @param productModel
     * @return collection of currently active delivery modes for the product
     */
    Collection<TargetZoneDeliveryModeModel> getAllApplicableDeliveryModesForProduct(final ProductModel productModel);

    /**
     * Return the default delivery mode
     * 
     * @return TargetZoneDeliveryModeModel
     */
    TargetZoneDeliveryModeModel getDefaultDeliveryMode();

    /**
     * Verifies whether a post code is valid for the given delivery mode. If delivery mode values contain a non post
     * code aware zone first then returns true. If there is a post code aware zone first then checks whether post code
     * exists and returns true or false. If there are no delivery mode values returns false.
     * 
     * @param deliveryMode
     * @param postcode
     * @param abstractOrder
     * @return boolean
     */
    boolean isDeliveryModePostCodeCombinationValid(DeliveryModeModel deliveryMode, String postcode,
            final AbstractOrderModel abstractOrder);

    /**
     * Verify whether an address contains a valid post code for the given delivery mode.
     * 
     * @param deliveryMode
     * @param address
     * @param abstractOrder
     * @return boolean
     */
    boolean isDeliveryModePostCodeCombinationValid(DeliveryModeModel deliveryMode, AddressModel address,
            final AbstractOrderModel abstractOrder);

    /**
     * Checks whether the provided order entries contains any bulky product.
     *
     * @param orderEntryList
     *            the order entry list
     * @return true if there is at least one bulky item in the order, false otherwise
     */
    boolean containsBulkyProducts(List<AbstractOrderEntryModel> orderEntryList);

    /**
     * Checks whether the given consignment contains any bulky product
     * 
     * @param consignmentEntry
     * @return boolean
     */
    boolean consignmentContainsBulkyProducts(Collection<ConsignmentEntryModel> consignmentEntry);

    /**
     * Extends the default to ensure we are using TargetZoneDeliveryModeValue and the value is currently active.<br/>
     * If order is null then the ZoneDeliveryModeValue is retrieved for the base currency and country.
     *
     * @param deliveryMode
     * @param abstractOrder
     * @return AbstractTargetZoneDeliveryModeValueModel
     */
    public ZoneDeliveryModeValueModel getZoneDeliveryModeValueForAbstractOrderAndMode(
            final ZoneDeliveryModeModel deliveryMode, final AbstractOrderModel abstractOrder);

    /**
     * Get all applicable delivery values for the product for the given delivery mode in descending order of the
     * priority.
     * 
     * @param zoneDeliveryMode
     * @param productModel
     * @param minCostOfProduct
     * @return List<AbstractTargetZoneDeliveryModeValueModel>
     */
    List<AbstractTargetZoneDeliveryModeValueModel> getApplicableDeliveryValuesForModeAndProductWithThreshold(
            ZoneDeliveryModeModel zoneDeliveryMode, final ProductModel productModel, double minCostOfProduct);

    /**
     * Method to check if applicable delivery cost is 0 for the provided product and delivery mode.
     * 
     * @param product
     * @param minCostOfProduct
     * @param zoneDeliveryMode
     * @return boolean
     */
    boolean isFreeDeliveryApplicableOnProductForDeliveryMode(ProductModel product, double minCostOfProduct,
            ZoneDeliveryModeModel zoneDeliveryMode);

    /**
     * Gets a sorted list of AbstractTargetZoneDeliveryModeValueModel based on a zone delivery mode, sorted descending
     * based on priority and minimum spend threshold applicable on a given order,
     * 
     * @param zoneDeliveryMode
     * @param abstractOrder
     * @param ignorePostcodeException
     *            if this is true then the TargetNoPostcodeException will be ignored and the
     *            AbstractTargetZoneDeliveryModeValueModel record that causes TargetNoPostcodeException will not be
     *            included in the return list
     * @return AbstractTargetZoneDeliveryModeValueModel
     */
    List<AbstractTargetZoneDeliveryModeValueModel> getAllApplicableDeliveryValuesForModeAndOrder(
            final ZoneDeliveryModeModel zoneDeliveryMode, final AbstractOrderModel abstractOrder,
            final boolean ignorePostcodeException);

    /**
     * Get ShipsterModel for all the applicable delivery modes.
     * 
     * @param targetZoneDeliveryModeModel
     * @return Lists of ShipsterModel
     */
    Collection<ShipsterConfigModel> getShipsterModelForDeliveryModes(
            Collection<TargetZoneDeliveryModeModel> targetZoneDeliveryModeModel);
}

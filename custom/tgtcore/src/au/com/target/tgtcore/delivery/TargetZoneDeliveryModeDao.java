/**
 * 
 */
package au.com.target.tgtcore.delivery;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import java.util.Collection;


/**
 * retrieves all information regarding the zonedDeliverymodes
 * 
 * @author rsamuel3
 * 
 */
public interface TargetZoneDeliveryModeDao {

    /**
     * Retrieves all the currently active delivery modes which include ones which are active and have a valid from and
     * to date
     * 
     * @param salesApplication
     * @return Collection<TargetZoneDeliveryModeModel> collection of deliveryModes that are active
     */
    Collection<TargetZoneDeliveryModeModel> findAllCurrentlyActiveDeliveryModes(SalesApplication salesApplication);

    /**
     * retrieves all the currently active delivery modes for the product
     * 
     * @param productModel
     * @return Collection of deliveryModes for the product
     */
    Collection<TargetZoneDeliveryModeModel> findAllCurrentlyActiveDeliveryModesForProduct(ProductModel productModel);

    /**
     * Return the default delivery mode
     * 
     * @return TargetZoneDeliveryModeModel
     */
    TargetZoneDeliveryModeModel getDefaultDeliveryMode();
}

/**
 * 
 */
package au.com.target.tgtcore.delivery;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;

import java.util.List;

import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;


public interface TargetZoneDeliveryModeValueDao extends GenericDao<AbstractTargetZoneDeliveryModeValueModel> {

    /**
     * Finds specific {@link AbstractTargetZoneDeliveryModeValueModel}s for the given zone, currency, minimum value, and
     * zone delivery mode.
     * 
     * @param zone
     *            the zone
     * @param currency
     *            the currency
     * @param min
     *            the minimum value
     * @param zoneDeliveryMode
     *            the zone delivery mode
     * @return all found {@link AbstractTargetZoneDeliveryModeValueModel}s, or empty list if not found.
     */
    List<AbstractTargetZoneDeliveryModeValueModel> findDeliveryValues(ZoneModel zone, CurrencyModel currency,
            Double min,
            ZoneDeliveryModeModel zoneDeliveryMode);

    /**
     * Finds all AbstractTargetZoneDeliveryModeValueModels for given zone delivery mode model sorted by priority
     * descending and then threshold descending.
     * 
     * @param del
     *            the zone delivery mode
     * @return all found {@link AbstractTargetZoneDeliveryModeValueModel}s, or empty list if not found.
     */
    List<AbstractTargetZoneDeliveryModeValueModel> findDeliveryValuesSortedByPriority(final ZoneDeliveryModeModel del);

    /**
     * Finds all AbstractTargetZoneDeliveryModeValueModels for given zone delivery mode model with zero threshold sorted
     * by priority descending and then non-zero threshold sorted by priority descending again.
     * 
     * @param del
     *            the zone delivery mode
     * @return all found {@link AbstractTargetZoneDeliveryModeValueModel}s, or empty list if not found.
     */
    List<AbstractTargetZoneDeliveryModeValueModel> findDeliveryValuesSortedByPriorityForProduct(
            final ZoneDeliveryModeModel del);
}

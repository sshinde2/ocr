/**
 * 
 */
package au.com.target.tgtcore.delivery;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;

import au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel;


/**
 * @author manoj
 * 
 */
public interface TargetZoneDeliveryModeValueRestrictionStrategy {

    /**
     * This method will evaluate all the Restrictions associated with modeValueModel
     * 
     * @param modeValueModel
     * @param paramAbstractOrder
     * @return true if all restrictions evaluates to true else false
     */
    public boolean isApplicableModeValue(RestrictableTargetZoneDeliveryModeValueModel modeValueModel,
            AbstractOrderModel paramAbstractOrder);

    /**
     * Evaluate restrictions according to product
     * 
     * @param modeValueModel
     * @param paramProductModel
     * @return true if all restrictions evaluates to true else false
     */
    public boolean isApplicableModeValue(RestrictableTargetZoneDeliveryModeValueModel modeValueModel,
            ProductModel paramProductModel);
}

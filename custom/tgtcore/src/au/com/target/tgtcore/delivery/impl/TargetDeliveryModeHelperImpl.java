/**
 * 
 */
package au.com.target.tgtcore.delivery.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.Validate;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * Methods for getting DeliveryModeModels for given delivery types
 * 
 */
public class TargetDeliveryModeHelperImpl implements TargetDeliveryModeHelper {

    private TargetDeliveryService targetDeliveryService;

    @Override
    public TargetZoneDeliveryModeModel getDeliveryModeModel(final String deliveryModeCode) {
        final DeliveryModeModel deliveryMode = targetDeliveryService.getDeliveryModeForCode(deliveryModeCode);
        if (deliveryMode instanceof TargetZoneDeliveryModeModel) {
            return (TargetZoneDeliveryModeModel)deliveryMode;
        }
        else {
            return null;
        }
    }

    /**
     * {@inheritDoc} If any of the arguments is null, the <code>IllegalArgumentException</code> is thrown.
     */
    @Override
    public boolean isProductAvailableForDeliveryMode(final ProductModel product, final DeliveryModeModel deliveryMode) {
        Validate.notNull(product, "product can not be null");
        Validate.notNull(deliveryMode, "deliveryMode can not be null");

        final Set<DeliveryModeModel> modes = getDeliveryModesForProduct(product);

        return CollectionUtils.isNotEmpty(modes) && modes.contains(deliveryMode);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebcore.delivery.TargetDeliveryModeHelper#isDeliveryModeStoreDelivery(de.hybris.platform.core.model.order.delivery.DeliveryModeModel)
     */
    @Override
    public boolean isDeliveryModeStoreDelivery(final DeliveryModeModel deliveryMode) {
        Assert.notNull(deliveryMode);
        if (deliveryMode instanceof TargetZoneDeliveryModeModel) {
            final TargetZoneDeliveryModeModel targetDeliveryMode = (TargetZoneDeliveryModeModel)deliveryMode;
            return BooleanUtils.isTrue(targetDeliveryMode.getIsDeliveryToStore());
        }
        return false;
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtwebcore.delivery.TargetDeliveryModeHelper#getDigitalDeliveryMode(de.hybris.platform.core.model.product.ProductModel)
     */
    @Override
    public TargetZoneDeliveryModeModel getDigitalDeliveryMode(final ProductModel product) {
        Assert.notNull(product);
        final Set<DeliveryModeModel> deliveryModes = getDeliveryModesForProduct(product);
        if (CollectionUtils.isNotEmpty(deliveryModes)) {
            for (final DeliveryModeModel deliveryMode : deliveryModes) {
                if (isDeliveryModeDigital(deliveryMode)) {
                    return (TargetZoneDeliveryModeModel)deliveryMode;
                }
            }
        }
        return null;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebcore.delivery.TargetDeliveryModeHelper#getDigitalDeliveryMode(de.hybris.platform.core.model.product.ProductModel)
     */
    @Override
    public boolean hasOnlyDigitalDeliveryMode(final ProductModel product) {
        Assert.notNull(product);
        final Set<DeliveryModeModel> deliveryModes = getDeliveryModesForProduct(product);
        if (CollectionUtils.isNotEmpty(deliveryModes)) {
            for (final DeliveryModeModel deliveryMode : deliveryModes) {
                if (!isDeliveryModeDigital(deliveryMode)) {
                    return false;
                }
            }
        }
        return true;
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtwebcore.delivery.TargetDeliveryModeHelper#isDeliveryModeDigital(de.hybris.platform.core.model.order.delivery.DeliveryModeModel)
     */
    @Override
    public boolean isDeliveryModeDigital(final DeliveryModeModel deliveryMode) {
        Assert.notNull(deliveryMode, "Delivery Mode cannot be null");
        if (deliveryMode instanceof TargetZoneDeliveryModeModel) {
            final TargetZoneDeliveryModeModel targetDeliveryMode = (TargetZoneDeliveryModeModel)deliveryMode;
            return BooleanUtils.isTrue(targetDeliveryMode.getIsDigital());
        }
        return false;
    }

    @Override
    public Set<DeliveryModeModel> getDeliveryModesForProduct(final ProductModel product) {

        // Use TargetDeliveryService to ensure we get the delivery modes on the base product
        final Set<DeliveryModeModel> modes;
        if (product instanceof VariantProductModel) {
            modes = targetDeliveryService.getVariantsDeliveryModesFromBaseProduct(product);
        }
        else {
            modes = product.getDeliveryModes();
        }

        return modes;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebcore.delivery.TargetDeliveryModeHelper#getAllCurrentActiveDeliveryModesForProduct(de.hybris.platform.core.model.product.ProductModel)
     */
    @Override
    public Collection<TargetZoneDeliveryModeModel> getAllCurrentActiveDeliveryModesForProduct(final ProductModel product) {
        // Use TargetDeliveryService to ensure we get the delivery modes on the base product
        Collection<TargetZoneDeliveryModeModel> modes = null;
        if (product instanceof VariantProductModel) {
            modes = getAllCurrentActiveDeliveryModesForProduct(((VariantProductModel)product).getBaseProduct());
        }
        else {
            modes = targetDeliveryService.getAllApplicableDeliveryModesForProduct(product);
        }

        return modes;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebcore.delivery.TargetDeliveryModeHelper#getDeliveryModesSortedForProduct(de.hybris.platform.core.model.product.ProductModel)
     */
    @Override
    public List<DeliveryModeModel> getDeliveryModesSortedForProduct(final ProductModel product) {
        final Set<DeliveryModeModel> deliveryModes = getDeliveryModesForProduct(product);
        if (CollectionUtils.isNotEmpty(deliveryModes)) {
            final List<DeliveryModeModel> listDeliveryModes = sortDeliveryModes(deliveryModes);
            return listDeliveryModes;
        }
        return Collections.EMPTY_LIST;
    }




    /**
     * Sorts the delivery modes in the ascending order of their display order
     * 
     * @param deliveryModes
     * @return List of delivery modes sorted in the ascending order
     */
    protected List<DeliveryModeModel> sortDeliveryModes(final Set<DeliveryModeModel> deliveryModes) {
        final List<DeliveryModeModel> listDeliveryModes = new ArrayList<DeliveryModeModel>(deliveryModes);

        Collections.sort(listDeliveryModes, new Comparator<DeliveryModeModel>() {

            @Override
            public int compare(final DeliveryModeModel deliveryMode1, final DeliveryModeModel deliveryMode2) {
                int displayOrder1 = 0;
                int displayOrder2 = 0;
                if (deliveryMode1 instanceof TargetZoneDeliveryModeModel) {
                    final TargetZoneDeliveryModeModel zoneDeliveryMode1 = (TargetZoneDeliveryModeModel)deliveryMode1;
                    displayOrder1 = zoneDeliveryMode1.getDisplayOrder() != null ? zoneDeliveryMode1
                            .getDisplayOrder().intValue() : 0;
                }
                if (deliveryMode2 instanceof TargetZoneDeliveryModeModel) {
                    final TargetZoneDeliveryModeModel zoneDeliveryMode2 = (TargetZoneDeliveryModeModel)deliveryMode2;
                    displayOrder2 = zoneDeliveryMode2.getDisplayOrder() != null ? zoneDeliveryMode2
                            .getDisplayOrder().intValue() : 0;
                }
                return displayOrder1 - displayOrder2;
            }
        });
        return listDeliveryModes;
    }

    @Override
    public boolean isDeliveryModeApplicableForOrder(final CartModel cartModel,
            final DeliveryModeModel deliveryModeModel) {
        int countOfProductsHavingDeliveryMode = 0;
        int countOfOtherProductHavingDigitalDelivery = 0;
        //Iterate Over products in Order
        for (final AbstractOrderEntryModel entry : cartModel.getEntries()) {
            if (entry.getProduct() instanceof AbstractTargetVariantProductModel) {
                final AbstractTargetVariantProductModel product = (AbstractTargetVariantProductModel)entry
                        .getProduct();
                if (isProductAvailableForDeliveryMode(product, deliveryModeModel)) {
                    countOfProductsHavingDeliveryMode++;
                }
                else if (doesProductHaveDigitalDeliveryMode(product)) {
                    countOfOtherProductHavingDigitalDelivery++;
                }
            }
        }
        return countOfProductsHavingDeliveryMode > 0
                && countOfProductsHavingDeliveryMode == (cartModel.getEntries().size()
                - countOfOtherProductHavingDigitalDelivery);
    }

    /**
     * @param product
     * @return true if the product has delivery mode that is digital
     */
    @Override
    public boolean doesProductHavePhysicalDeliveryMode(final AbstractTargetVariantProductModel product) {
        Assert.notNull(product, "Product cannot be null");
        final Set<DeliveryModeModel> deliveryModes = getDeliveryModesForProduct(product);
        if (CollectionUtils.isNotEmpty(deliveryModes)) {
            for (final DeliveryModeModel deliveryMode : deliveryModes) {
                if (!isDeliveryModeDigital(deliveryMode)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param product
     * @return true if the product has delivery mode that is digital
     */
    @Override
    public boolean doesProductHaveDigitalDeliveryMode(final AbstractTargetVariantProductModel product) {
        final Set<DeliveryModeModel> deliveryModes = getDeliveryModesForProduct(product);
        if (CollectionUtils.isNotEmpty(deliveryModes)) {
            for (final DeliveryModeModel deliveryMode : deliveryModes) {
                if (isDeliveryModeDigital(deliveryMode)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param order
     * 
     * @return true if the order has digital products
     */
    @Override
    public boolean hasDigitalProducts(final AbstractOrderModel order) {
        if (null == order) {
            return false;
        }
        final List<AbstractOrderEntryModel> orderEntries = order.getEntries();
        if (CollectionUtils.isNotEmpty(orderEntries)) {
            for (final AbstractOrderEntryModel entryModel : orderEntries) {
                final ProductModel product = entryModel.getProduct();
                if (null != product && product instanceof AbstractTargetVariantProductModel) {
                    final AbstractTargetVariantProductModel variantProduct = (AbstractTargetVariantProductModel)product;
                    if (doesProductHaveDigitalDeliveryMode(variantProduct)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    /**
     * Add or remove the given delivery mode to/from the given base product
     * 
     * @param product
     *            a base product
     * @param deliveryMode
     * @param available
     *            add if true or remove if false
     */
    protected void setDeliveryModeForProduct(final TargetProductModel product,
            final DeliveryModeModel deliveryMode,
            final boolean available) {

        final Set<DeliveryModeModel> newModes = getNewModes(product, deliveryMode, available);
        product.setDeliveryModes(newModes);
    }

    /**
     * Get updated modes set given product, mode and available flag<br/>
     * This method is protected so it can be unit tested.
     * 
     * @param product
     * @param deliveryMode
     * @param available
     * @return set
     */
    protected Set<DeliveryModeModel> getNewModes(final TargetProductModel product,
            final DeliveryModeModel deliveryMode, final boolean available) {

        final Set<DeliveryModeModel> modes = product.getDeliveryModes();
        final Set<DeliveryModeModel> newModes = new HashSet<>();

        if (CollectionUtils.isNotEmpty(modes))
        {
            newModes.addAll(modes);
        }

        if (available)
        {
            newModes.add(deliveryMode);
        }
        else
        {
            newModes.remove(deliveryMode);
        }

        return newModes;
    }

    /**
     * @param targetDeliveryService
     *            the targetDeliveryService to set
     */
    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        this.targetDeliveryService = targetDeliveryService;
    }


}
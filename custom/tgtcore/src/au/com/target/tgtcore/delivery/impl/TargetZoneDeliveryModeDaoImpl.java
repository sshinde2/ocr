/**
 * 
 */
package au.com.target.tgtcore.delivery.impl;

import de.hybris.platform.basecommerce.constants.BasecommerceConstants;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.jalo.link.Link;
import de.hybris.platform.order.daos.impl.DefaultZoneDeliveryModeDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtcore.delivery.TargetZoneDeliveryModeDao;
import au.com.target.tgtcore.model.SalesApplicationWrapperModel;


/**
 * @author rsamuel3
 * 
 */
public class TargetZoneDeliveryModeDaoImpl extends DefaultZoneDeliveryModeDao implements TargetZoneDeliveryModeDao {
    private static final Logger LOG = Logger.getLogger(TargetZoneDeliveryModeDaoImpl.class);

    private static final String FIND_DEFAULT_DELIVERY_MODE_QUERY = "SELECT {" + TargetZoneDeliveryModeModel.PK + "} " +
            "FROM { " + TargetZoneDeliveryModeModel._TYPECODE + "} " +
            "WHERE {" + TargetZoneDeliveryModeModel.DEFAULT + "}=?default";

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.delivery.TargetZoneDeliveryModeDao#findAllCurrentlyActiveDeliveryModes()
     */
    @Override
    public Collection<TargetZoneDeliveryModeModel> findAllCurrentlyActiveDeliveryModes(
            final SalesApplication salesApplication) {
        final Map<String, Object> params = new HashMap<String, Object>();
        final StringBuilder query = new StringBuilder("SELECT DISTINCT {zdm:" + ItemModel.PK);
        query.append("},").append("{zdm:").append(TargetZoneDeliveryModeModel.DISPLAYORDER).append("} FROM {")
                .append(TargetZoneDeliveryModeModel._TYPECODE)
                .append(" AS zdm}, {TargetZoneDeliveryMode2SalesApplicationWrapper  AS zdm2saw}, {")
                .append(SalesApplicationWrapperModel._TYPECODE).append(" AS saw}, {")
                .append(SalesApplication._TYPECODE).append(" AS sa}");
        query.append("WHERE {zdm:").append(ZoneDeliveryModeModel.ACTIVE).append("}=?active ");
        query.append("AND {zdm2saw:source} = {zdm:pk} AND {zdm2saw:target} ={saw:pk} AND {saw:salesApplication}={sa:pk} ");

        if (null != salesApplication) {
            query.append("AND {sa:code}=?salesApplication ");
            params.put("salesApplication", salesApplication.getCode());
        }

        query.append("ORDER BY {").append(TargetZoneDeliveryModeModel.DISPLAYORDER).append("} ASC");

        params.put("active", Boolean.TRUE);

        return doSearch(query.toString(), params, null);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.delivery.TargetZoneDeliveryModeDao#findAllCurrentlyActiveDeliveryModesForProduct(de.hybris.platform.core.model.product.ProductModel)
     */
    @Override
    @SuppressWarnings("deprecation")
    public Collection<TargetZoneDeliveryModeModel> findAllCurrentlyActiveDeliveryModesForProduct(
            final ProductModel productModel) {
        final StringBuilder query = new StringBuilder(" SELECT DISTINCT {zdm:");
        query.append(ItemModel.PK).append("},{zdm:").append(TargetZoneDeliveryModeModel.DISPLAYORDER);
        query.append("} FROM {").append(TargetZoneDeliveryModeModel._TYPECODE).append(" AS zdm},{");
        query.append(ProductModel._TYPECODE).append(" as vp},{");
        query.append(BasecommerceConstants.Relations.PRODUCTDELIVERYMODERELATION).append(" as pdmr}  where {");
        query.append("pdmr:").append(Link.TARGET).append("}={zdm:").append(ItemModel.PK).append("} and {");
        query.append("vp:").append(ItemModel.PK).append("}={pdmr:").append(Link.SOURCE).append("} ");
        query.append("AND {zdm:").append(TargetZoneDeliveryModeModel.ACTIVE).append("}=?active ");
        query.append(" and {vp:").append(ProductModel.CODE).append("}=?code ");
        query.append("ORDER BY {").append(TargetZoneDeliveryModeModel.DISPLAYORDER).append("} ASC");

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("active", Boolean.TRUE);
        params.put("code", productModel.getCode());

        return doSearch(query.toString(), params, null);
    }

    @Override
    public TargetZoneDeliveryModeModel getDefaultDeliveryMode() {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("default", Boolean.TRUE);

        final List<TargetZoneDeliveryModeModel> list =
                doSearch(FIND_DEFAULT_DELIVERY_MODE_QUERY, params, TargetZoneDeliveryModeModel.class);
        if (CollectionUtils.isEmpty(list))
        {
            LOG.warn("No default delivery mode found");
            return null;
        }
        if (list.size() > 1)
        {
            LOG.warn("More than one default delivery mode found");
        }

        return list.get(0);
    }

    /**
     * uses flexible search to execute the query
     * 
     * @param query
     * @param params
     * @param resultClass
     * @return resultset
     */
    protected <T> List<T> doSearch(final String query, final Map<String, Object> params, final Class resultClass)
    {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query);
        if (params != null)
        {
            fQuery.addQueryParameters(params);
        }
        if (resultClass != null)
        {
            fQuery.setResultClassList(Collections.singletonList(resultClass));
        }
        return getFlexibleSearchService().<T> search(fQuery).getResult();
    }

}

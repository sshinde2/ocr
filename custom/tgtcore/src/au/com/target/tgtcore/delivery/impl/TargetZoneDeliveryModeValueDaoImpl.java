/**
 * 
 */
package au.com.target.tgtcore.delivery.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.internal.dao.SortParameters;
import de.hybris.platform.servicelayer.internal.dao.SortParameters.SortOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.ListUtils;

import au.com.target.tgtcore.delivery.TargetZoneDeliveryModeValueDao;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;


public class TargetZoneDeliveryModeValueDaoImpl extends DefaultGenericDao<AbstractTargetZoneDeliveryModeValueModel>
        implements
        TargetZoneDeliveryModeValueDao {

    /**
     * The query to fetch AbstractTargetZoneDeliveryModeValueModels for a specific DeliveryMode sorted by priority and
     * threshold descending.
     */
    private static final String FIND_DEL_VALUES_SORTED_BY_PRIORITY_THRESHOLD = "SELECT {v:"
            + AbstractTargetZoneDeliveryModeValueModel.PK + "} FROM {"
            + AbstractTargetZoneDeliveryModeValueModel._TYPECODE +
            " AS v} WHERE {v:" + AbstractTargetZoneDeliveryModeValueModel.DELIVERYMODE
            + " } = ?deliveryMode ORDER BY {v:"
            + AbstractTargetZoneDeliveryModeValueModel.PRIORITY + "} " + SortOrder.DESCENDING + ", {v:"
            + AbstractTargetZoneDeliveryModeValueModel.MINIMUM + "} " + SortOrder.DESCENDING;

    /**
     * The query to fetch AbstractTargetZoneDeliveryModeValueModels for a specific DeliveryMode having non-zero
     * threshold sorted by priority and threshold descending.
     */
    private static final String FIND_DEL_VALUES_NONZERO_THRESHOLD_SORTED_BY_PRIORITY = "SELECT {v:"
            + AbstractTargetZoneDeliveryModeValueModel.PK + "} FROM {"
            + AbstractTargetZoneDeliveryModeValueModel._TYPECODE +
            " AS v} WHERE {v:" + AbstractTargetZoneDeliveryModeValueModel.DELIVERYMODE
            + " } = ?deliveryMode AND {v:" + AbstractTargetZoneDeliveryModeValueModel.MINIMUM + "} != "
            + Double.valueOf(0) + " ORDER BY {v:"
            + AbstractTargetZoneDeliveryModeValueModel.PRIORITY + "} " + SortOrder.DESCENDING + ", {v:"
            + AbstractTargetZoneDeliveryModeValueModel.MINIMUM + "} " + SortOrder.DESCENDING;

    public TargetZoneDeliveryModeValueDaoImpl()
    {
        super(AbstractTargetZoneDeliveryModeValueModel._TYPECODE);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.delivery.TargetZoneDeliveryModeValueDao#findDeliveryValues(de.hybris.platform.deliveryzone.model.ZoneModel, de.hybris.platform.core.model.c2l.CurrencyModel, java.lang.Double, de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel, java.util.Date, java.util.Date)
     */
    @Override
    public List<AbstractTargetZoneDeliveryModeValueModel> findDeliveryValues(final ZoneModel zone,
            final CurrencyModel currency,
            final Double min, final ZoneDeliveryModeModel zoneDeliveryMode) {

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(AbstractTargetZoneDeliveryModeValueModel.ZONE, zone);
        params.put(AbstractTargetZoneDeliveryModeValueModel.CURRENCY, currency);
        params.put(AbstractTargetZoneDeliveryModeValueModel.MINIMUM, min);
        params.put(AbstractTargetZoneDeliveryModeValueModel.DELIVERYMODE, zoneDeliveryMode);

        return find(params);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.delivery.TargetZoneDeliveryModeValueDao#findDeliveryValuesSorted(de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel)
     */
    @Override
    public List<AbstractTargetZoneDeliveryModeValueModel> findDeliveryValuesSortedByPriority(
            final ZoneDeliveryModeModel del) {

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(AbstractTargetZoneDeliveryModeValueModel.DELIVERYMODE, del);

        return getFlexibleSearchService().<AbstractTargetZoneDeliveryModeValueModel> search(
                FIND_DEL_VALUES_SORTED_BY_PRIORITY_THRESHOLD, params).getResult();
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.delivery.TargetZoneDeliveryModeValueDao#findDeliveryValuesSortedByPriorityForProduct(de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel)
     */
    @Override
    public List<AbstractTargetZoneDeliveryModeValueModel> findDeliveryValuesSortedByPriorityForProduct(
            final ZoneDeliveryModeModel del) {

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(AbstractTargetZoneDeliveryModeValueModel.DELIVERYMODE, del);
        params.put(AbstractTargetZoneDeliveryModeValueModel.MINIMUM, Double.valueOf(0));

        final SortParameters sp = new SortParameters();
        sp.addSortParameter(AbstractTargetZoneDeliveryModeValueModel.PRIORITY, SortOrder.DESCENDING);

        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDelValuesZeroThreshold = find(params, sp);
        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDelValuesNonZeroThreshold = getFlexibleSearchService()
                .<AbstractTargetZoneDeliveryModeValueModel> search(
                        FIND_DEL_VALUES_NONZERO_THRESHOLD_SORTED_BY_PRIORITY, params).getResult();

        return ListUtils.union(zoneDelValuesZeroThreshold, zoneDelValuesNonZeroThreshold);
    }

}

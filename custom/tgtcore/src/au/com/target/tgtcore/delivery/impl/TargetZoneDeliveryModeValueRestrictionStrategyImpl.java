/**
 * 
 */
package au.com.target.tgtcore.delivery.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.servicelayer.model.ModelService;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.delivery.TargetZoneDeliveryModeValueRestrictionStrategy;
import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.jalo.AbstractTargetZoneDeliveryModeValueRestriction;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueRestrictionModel;
import au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel;


/**
 * @author manoj
 * 
 */
@SuppressWarnings("deprecation")
public class TargetZoneDeliveryModeValueRestrictionStrategyImpl implements
        TargetZoneDeliveryModeValueRestrictionStrategy {

    private ModelService modelService;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.delivery.TargetZoneDeliveryModeValueRestrictionStrategy#isRestrictedModeValue(au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel, de.hybris.platform.jalo.order.AbstractOrder)
     */
    @Override
    public boolean isApplicableModeValue(final RestrictableTargetZoneDeliveryModeValueModel modeValueModel,
            final AbstractOrderModel abstractOrderModel) {
        if (null != abstractOrderModel && CollectionUtils.isNotEmpty(abstractOrderModel.getEntries())
                && null != modeValueModel
                && CollectionUtils.isNotEmpty(modeValueModel.getRestrictions())) {
            TargetNoPostCodeException postCodeEx = null;
            for (final AbstractTargetZoneDeliveryModeValueRestrictionModel restrictionModel : modeValueModel
                    .getRestrictions()) {
                final AbstractTargetZoneDeliveryModeValueRestriction restriction = modelService
                        .getSource(restrictionModel);
                try {
                    if (!restriction.evaluate(abstractOrderModel)) {
                        return false;
                    }
                }
                catch (final TargetNoPostCodeException ex) {
                    postCodeEx = ex;
                }
            }
            if (postCodeEx != null) {
                throw postCodeEx;
            }
        }
        return true;
    }

    @Override
    public boolean isApplicableModeValue(final RestrictableTargetZoneDeliveryModeValueModel modeValueModel,
            final ProductModel paramProductModel) {
        if (null != paramProductModel && null != modeValueModel
                && CollectionUtils.isNotEmpty(modeValueModel.getRestrictions())) {
            final Product product = modelService.getSource(paramProductModel);
            for (final AbstractTargetZoneDeliveryModeValueRestrictionModel restrictionModel : modeValueModel
                    .getRestrictions()) {
                final AbstractTargetZoneDeliveryModeValueRestriction restriction = modelService
                        .getSource(restrictionModel);
                if (!restriction.evaluate(product)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }
}

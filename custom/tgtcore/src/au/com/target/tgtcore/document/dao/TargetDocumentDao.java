/**
 * 
 */
package au.com.target.tgtcore.document.dao;

import de.hybris.platform.commons.model.DocumentModel;
import de.hybris.platform.core.model.ItemModel;

import au.com.target.tgtcore.enums.DocumentType;


/**
 * @author asingh78
 * 
 */
public interface TargetDocumentDao {
    /**
     * 
     * @param itemModel
     * @param documentType
     * @return DocumentModel
     */
    DocumentModel findDocumentModelBySourceItemAndDocumentType(ItemModel itemModel, DocumentType documentType);
}

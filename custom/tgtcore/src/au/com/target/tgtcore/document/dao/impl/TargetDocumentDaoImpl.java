/**
 * 
 */
package au.com.target.tgtcore.document.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commons.model.DocumentModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.document.dao.TargetDocumentDao;
import au.com.target.tgtcore.enums.DocumentType;


/**
 * @author asingh78
 * 
 */
public class TargetDocumentDaoImpl extends DefaultGenericDao implements TargetDocumentDao {

    public TargetDocumentDaoImpl() {
        super(DocumentModel._TYPECODE);

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.document.dao.TargetDocumentDao#findDocumentModelBySourceItemAndDocumentType(de.hybris.platform.core.model.ItemModel, au.com.target.tgtcore.enums.DocumentType)
     */
    @Override
    public DocumentModel findDocumentModelBySourceItemAndDocumentType(final ItemModel itemModel,
            final DocumentType documentType) {
        validateParameterNotNull(itemModel, "item  model cannot be null");
        validateParameterNotNull(documentType, "documentType cannot be null");
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(DocumentModel.SOURCEITEM, itemModel);
        params.put(DocumentModel.DOCUMENTTYPE, documentType);
        final List<DocumentModel> documents = find(params);
        if (CollectionUtils.isNotEmpty(documents)) {
            return documents.get(0);
        }
        return null;
    }

}

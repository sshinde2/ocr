/**
 * 
 */
package au.com.target.tgtcore.encryption;

//CHECKSTYLE:OFF
import de.hybris.platform.servicelayer.config.impl.HybrisConfiguration;
import de.hybris.platform.util.config.ConfigIntf;

import java.util.Iterator;

import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.commons.configuration.Configuration;
import org.jasypt.encryption.StringEncryptor;
import org.jasypt.properties.PropertyValueEncryptionUtils;
import org.springframework.util.Assert;


//CHECKSTYLE:ON

/**
 * Since {@link HybrisConfiguration} is NOT extensible, have to resort to copying the implementation here.
 * 
 * {@link HybrisConfiguration}
 * 
 * @author vmuthura
 * 
 */
public class EncryptableConfiguration extends AbstractConfiguration {

    private final ConfigIntf config;
    private final StringEncryptor encryptor;

    public EncryptableConfiguration(final ConfigIntf config, final StringEncryptor encryptor) {
        super();

        Assert.notNull(config, "Config is null");
        Assert.notNull(encryptor, "Encryptor is null");

        this.config = config;
        this.encryptor = encryptor;
    }

    /**
     * @see AbstractConfiguration#addPropertyDirect(String, Object)
     */
    @Override
    protected void addPropertyDirect(final String key, final Object value) {
        config.setParameter(key, (String)value);
    }

    /**
     * @see Configuration#containsKey(String)
     */
    @Override
    public boolean containsKey(final String key) {
        return config.getParameter(key) != null;
    }

    /**
     * @see Configuration#getKeys()
     */
    @Override
    public Iterator<String> getKeys() {
        return config.getAllParameters().keySet().iterator();
    }

    /**
     * @see Configuration#getProperty(String)
     */
    @Override
    public Object getProperty(final String key) {
        return convertPropertyValue(config.getParameter(key));
    }

    /**
     * @see Configuration#isEmpty()
     */
    @Override
    public boolean isEmpty() {
        //assume hybris config is never empty
        return false;
    }

    /**
     * @see Configuration#getString(String)
     */
    @Override
    public String getString(final String key) {
        return this.getString(key, "");
    }

    /**
     * Default behavior gets overriden with that one from hybris. This disables the
     * {@link org.apache.commons.configuration.interpol.ConfigurationInterpolator} which would be created/asked with
     * every call for an appropriate {@link org.apache.commons.lang.text.StrLookup} which itself deals with an
     * {@link org.apache.commons.lang.text.StrSubstitutor} and so on. (only pitfall is that variable replacement as
     * commons defines it doesn't work anymore)
     * 
     * @see org.apache.commons.configuration.AbstractConfiguration#getString(java.lang.String, java.lang.String)
     */
    @Override
    public String getString(final String key, final String defaultValue) {
        return convertPropertyValue(config.getString(key, defaultValue));
    }

    private String convertPropertyValue(final String originalValue) {
        if (PropertyValueEncryptionUtils.isEncryptedValue(originalValue)) {
            return PropertyValueEncryptionUtils.decrypt(originalValue, encryptor);
        }

        return originalValue;
    }

}

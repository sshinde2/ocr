package au.com.target.tgtcore.event;

import de.hybris.platform.commerceservices.setup.SetupImpexService;
import de.hybris.platform.servicelayer.event.events.AfterInitializationEndEvent;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import au.com.target.tgtcore.setup.SingleThreadImpexImport;


/**
 * Listens for {@link AfterInitializationEndEvent} and performs cleanup activity like removing unused items and
 * disabling/removing unused cronjobs. This is the hook to listen the completion of platform initialization and update
 * 
 */
public class CoreAfterInitializationEndEventListener extends AbstractEventListener<AfterInitializationEndEvent> {

    @Override
    protected void onEvent(final AfterInitializationEndEvent event) {
        ((SingleThreadImpexImport)getSetupImpexService()).importImpexFileSingleThread(
                "/tgtcore/import/cleanup/remove-unused-items.impex", true, false, 1, false);
    }

    /**
     * @return {@link SetupImpexService}
     */
    public SetupImpexService getSetupImpexService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getSetupImpexService().");
    }

}

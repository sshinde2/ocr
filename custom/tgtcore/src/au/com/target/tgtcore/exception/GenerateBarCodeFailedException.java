/**
 * 
 */
package au.com.target.tgtcore.exception;



/**
 * Target GenerateBarCodeFailedException. When the Flexible Query return more than one result, this Exception will be
 * thrown
 * 
 */
public class GenerateBarCodeFailedException extends Exception
{
    public GenerateBarCodeFailedException(final String message)
    {
        super(message);
    }

    public GenerateBarCodeFailedException(final Throwable cause)
    {
        super(cause);
    }

    public GenerateBarCodeFailedException(final String message, final Throwable cause)
    {
        super(message, cause);
    }
}

/**
 * 
 */
package au.com.target.tgtcore.exception;

/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class NoModificationRequiredException extends Exception {

    /**
     * 
     */
    public NoModificationRequiredException() {
        super();
    }

    /**
     * @param message
     * @param cause
     */
    public NoModificationRequiredException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     */
    public NoModificationRequiredException(final String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public NoModificationRequiredException(final Throwable cause) {
        super(cause);
    }

}

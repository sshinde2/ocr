/**
 * 
 */
package au.com.target.tgtcore.exception;



/**
 * Target UnknowIndentiferException. When the Flexible Query return more than one result, this Exception will be thrown
 * 
 */
public class TargetAmbiguousIdentifierException extends Exception
{
    public TargetAmbiguousIdentifierException(final String message)
    {
        super(message);
    }

    public TargetAmbiguousIdentifierException(final Throwable cause)
    {
        super(cause);
    }

    public TargetAmbiguousIdentifierException(final String message, final Throwable cause)
    {
        super(message, cause);
    }
}

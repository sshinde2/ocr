/**
 * 
 */
package au.com.target.tgtcore.exception;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;


/**
*
*/
public class TargetInsufficientStockLevelException extends InsufficientStockLevelException {

    private final AbstractOrderEntryModel cartEntry;

    /**
     * 
     * @param message
     * @param cartEntry
     * @param cause
     */
    public TargetInsufficientStockLevelException(final String message, final AbstractOrderEntryModel cartEntry,
            final Throwable cause) {
        super(message);
        this.cartEntry = cartEntry;
        initCause(cause);
    }



    /**
     * @return the cartEntry
     */
    public AbstractOrderEntryModel getCartEntry() {
        return cartEntry;
    }


}

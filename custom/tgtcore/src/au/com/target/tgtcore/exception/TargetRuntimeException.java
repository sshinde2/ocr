/**
 * 
 */
package au.com.target.tgtcore.exception;

/**
 * Generic RuntimeException, use this when there is a critical error which we don't expect to be handled by callers of
 * the methods where it is raised.
 * 
 * @author sbryan6
 * 
 */
public class TargetRuntimeException extends RuntimeException {

    /**
     * Constructor
     * 
     * @param message
     * @param cause
     */
    public TargetRuntimeException(final String message, final Throwable cause) {
        super(message, cause);
    }
}

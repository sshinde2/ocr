/**
 * 
 */
package au.com.target.tgtcore.exception;

/**
 * Exception thrown when stock check fails
 * 
 * @author pthoma20
 * 
 */
public class TargetStoreStockCheckException extends TargetRuntimeException {

    /**
     * Constructor
     * 
     * @param message
     * @param cause
     */
    public TargetStoreStockCheckException(final String message, final Throwable cause) {
        super(message, cause);
    }
}

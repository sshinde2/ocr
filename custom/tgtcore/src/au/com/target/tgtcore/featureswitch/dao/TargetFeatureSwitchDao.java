/**
 * 
 */
package au.com.target.tgtcore.featureswitch.dao;

import de.hybris.platform.servicelayer.internal.dao.GenericDao;

import java.util.List;

import au.com.target.tgtcore.model.TargetFeatureSwitchModel;


/**
 * Dao to fetch feature switches
 * 
 * @author pratik
 *
 */
public interface TargetFeatureSwitchDao extends GenericDao<TargetFeatureSwitchModel> {

    /**
     * Method to fetch a switch for a particular feature
     * 
     * @param featureName
     * @return TargetFeatureSwitchModel
     */
    TargetFeatureSwitchModel getFeatureByName(String featureName);

    /**
     * Method is to return enabled features as a List. When includeOnlyUIEnabled is true, it restricts to list of
     * features to ones that are also UI enabled.
     * 
     * @param includeOnlyUIEnabled
     * @return enabledFeaturesList
     */
    List<TargetFeatureSwitchModel> getEnabledFeatures(boolean includeOnlyUIEnabled);

    /**
     * Method to return the list of all the features in the system
     * 
     * @return featuresList
     */
    List<TargetFeatureSwitchModel> getAllFeatures();

}

/**
 * 
 */
package au.com.target.tgtcore.featureswitch.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.featureswitch.dao.TargetFeatureSwitchDao;
import au.com.target.tgtcore.model.TargetFeatureSwitchModel;


/**
 * Implementation class for TargetFeatureSwitchDao
 * 
 * @author pratik
 */
public class TargetFeatureSwitchDaoImpl extends DefaultGenericDao<TargetFeatureSwitchModel>
        implements TargetFeatureSwitchDao {

    public TargetFeatureSwitchDaoImpl() {
        super(TargetFeatureSwitchModel._TYPECODE);
    }

    /**
     * Method to fetch a particular feature
     * 
     * @return featureSwitch
     */
    @Override
    public TargetFeatureSwitchModel getFeatureByName(final String featureName) {
        final List<TargetFeatureSwitchModel> switches = find(Collections.singletonMap(TargetFeatureSwitchModel.NAME,
                featureName));
        return switches.isEmpty() ? null : switches.get(0);
    }

    /**
     * Method is to return enabled features as a List. When includeOnlyUIEnabled is true, it restricts to list of
     * features to ones that are also UI enabled.
     * 
     * @param includeOnlyUIEnabled
     * @return enabledFeaturesList
     */
    @Override
    public List<TargetFeatureSwitchModel> getEnabledFeatures(final boolean includeOnlyUIEnabled) {

        final Map<String, Object> enabledFeatures = new HashMap<>(2);
        enabledFeatures.put(TargetFeatureSwitchModel.ENABLED, Boolean.TRUE);

        if (includeOnlyUIEnabled) {
            enabledFeatures.put(TargetFeatureSwitchModel.UIENABLED, Boolean.TRUE);
        }

        final List<TargetFeatureSwitchModel> enabledFeaturesList = find(enabledFeatures);

        return enabledFeaturesList;
    }

    @Override
    public List<TargetFeatureSwitchModel> getAllFeatures() {
        final List<TargetFeatureSwitchModel> switches = find();
        return switches;
    }
}

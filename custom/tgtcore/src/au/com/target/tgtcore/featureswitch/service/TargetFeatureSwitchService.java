/**
 * 
 */
package au.com.target.tgtcore.featureswitch.service;

import java.util.List;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetFeatureSwitchModel;
import au.com.target.tgtfulfilment.model.ConsignmentExtractRecordModel;



/**
 * Service class for TargetFeatureSwitch
 * 
 * @author pratik
 *
 */
public interface TargetFeatureSwitchService {

    /**
     * Method to fetch if the switch is enabled or disabled if switch for a feature doesn't exist then that feature is
     * considered to be enabled
     * 
     * @param featureName
     * @return true if switch doesn't exist and if exists it is enabled
     */
    boolean isFeatureEnabled(String featureName);

    /**
     * Determine whether include billing address in accertify feed or not.
     * 
     * @return true if billing address needs to be included.
     */
    boolean includeBillingAddressInAccertifyFeed();

    /**
     * Method is to return enabled features as a List. When includeOnlyUIEnabled is true, it restricts to list of
     * features to ones that are also UI enabled.
     * 
     * @return list of enabled featured
     */
    List<TargetFeatureSwitchModel> getEnabledFeatures(boolean includeOnlyUIEnabled);

    /**
     * Method to save ConsignmentExtractRecord model
     * 
     * @param consignment
     * @param consignmentAsId
     */
    void saveConsignmentExtractRecord(TargetConsignmentModel consignment, boolean consignmentAsId);

    /**
     * Method to fetch ConsignmentExtractRecord by Consignment
     * 
     * @param consignment
     * @return ConsignmentExtractRecordModel
     */
    ConsignmentExtractRecordModel getConsExtractRecordByConsignment(TargetConsignmentModel consignment);

    /**
     * Determine to send email with blackout period
     * 
     * @return true - if feature is enabled
     */
    boolean sendEmailWithBlackout();

    /**
     * Method to fetch all the features
     * 
     * @return list of enabled featured
     */
    List<TargetFeatureSwitchModel> getAllFeatures();

}

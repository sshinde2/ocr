/**
 * 
 */
package au.com.target.tgtcore.flybuys.client;

import java.math.BigDecimal;
import java.util.Date;

import au.com.target.tgtcore.flybuys.dto.response.FlybuysAuthenticateResponseDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysConsumeResponseDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysRefundResponseDto;


/**
 * @author cwijesu1
 * 
 *         This will be used to authenticate against the flybuys
 * 
 */
public interface FlybuysClient {

    /**
     * Authenticate against flybuys to get available redeem options.
     * 
     * @param flybuysNumber
     *            the flybuys number
     * @param dob
     *            the dob
     * @param postcode
     *            the postcode
     * @return the flybuys authenticate response dto
     */
    FlybuysAuthenticateResponseDto flybuysAuthenticate(String flybuysNumber, Date dob, String postcode);

    /**
     * This will be used to consume Flybuy points.
     * 
     * @param flybuysCardNumber
     *            the flybuys card number
     * @param securityToken
     *            the security token
     * @param storeNumber
     *            the store number
     * @param points
     *            the points
     * @param dollars
     *            the dollars
     * @param redeemCode
     *            the redeem code
     * @param transactionId
     *            the transaction ID
     * @return the flybuys consume response dto
     */
    FlybuysConsumeResponseDto consumeFlybuysPoints(String flybuysCardNumber, String securityToken, String storeNumber,
            Integer points, BigDecimal dollars, String redeemCode, String transactionId);

    /**
     * Flybuys refund operation
     * 
     * @param flybuysCardNumber
     * @param securityToken
     * @param storeNumber
     * @param points
     * @param dollars
     * @param timeStamp
     * @param transactionId
     */
    FlybuysRefundResponseDto refundFlybuysPoints(String flybuysCardNumber, String securityToken, String storeNumber,
            Integer points, BigDecimal dollars, String timeStamp, String transactionId);
}

/**
 * 
 */
package au.com.target.tgtcore.flybuys.client.impl;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.log4j.Logger;

import au.com.target.tgtcore.flybuys.client.FlybuysClient;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysAuthenticateResponseDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysConsumeResponseDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysRefundResponseDto;


/**
 * @author rmcalave
 * 
 */
public class DefaultTargetFlybuysClientImpl implements FlybuysClient {

    private static final Logger LOG = Logger.getLogger(DefaultTargetFlybuysClientImpl.class);

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.client.FlybuysClient#flybuysAuthenticate(java.lang.String, java.util.Date, java.lang.String)
     */
    @Override
    public FlybuysAuthenticateResponseDto flybuysAuthenticate(final String flybuysNumber, final Date dob,
            final String postcode) {
        LOG.info("flybuysAuthenticate default implementation");
        return new FlybuysAuthenticateResponseDto();
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.client.FlybuysClient#consumeFlybuysPoints(java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.math.BigDecimal, java.lang.String, java.lang.String)
     */
    @Override
    public FlybuysConsumeResponseDto consumeFlybuysPoints(final String flybuysCardNumber, final String securityToken,
            final String storeNumber, final Integer points, final BigDecimal dollars, final String redeemCode,
            final String transactionId) {
        LOG.info("consumeFlybuysPoints default implementation");
        return new FlybuysConsumeResponseDto();
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.client.FlybuysClient#flybuysRefund(java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.math.BigDecimal, java.util.Date, java.lang.String)
     */
    @Override
    public FlybuysRefundResponseDto refundFlybuysPoints(final String flybuysCardNumber, final String securityToken,
            final String storeNumber,
            final Integer points, final BigDecimal dollars, final String timeStamp, final String transactionId) {
        LOG.info("flybuysRefundPoints default implementation");
        return new FlybuysRefundResponseDto();
    }

}

/**
 * 
 */
package au.com.target.tgtcore.flybuys.dao;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.FlybuysRedeemConfigModel;



/**
 * The Interface FlybuysRedeemConfigDao.
 * 
 * @author Nandini
 */
public interface FlybuysRedeemConfigDao {

    /**
     * Gets the fly buy redeem config.
     * 
     * @return the fly buy redeem config
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    FlybuysRedeemConfigModel getFlybuysRedeemConfig() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException;
}

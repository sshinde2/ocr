/**
 * 
 */
package au.com.target.tgtcore.flybuys.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.flybuys.dao.FlybuysRedeemConfigDao;
import au.com.target.tgtcore.model.FlybuysRedeemConfigModel;
import au.com.target.tgtcore.util.TargetServicesUtil;


/**
 * The Class FlybuysRedeemConfigDaoImpl.
 * 
 * @author Nandini
 */
public class FlybuysRedeemConfigDaoImpl implements FlybuysRedeemConfigDao {

    /** The Constant QUERY_FLYBUYS_CONFIG. */
    private static final String QUERY_FLYBUYS_CONFIG = "SELECT {" + FlybuysRedeemConfigModel.PK + "}"
            + "FROM {" + FlybuysRedeemConfigModel._TYPECODE + "} ";

    /** The flexible search service. */
    private FlexibleSearchService flexibleSearchService;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.dao.FlybuysRedeemConfigDao#getActiveFlyBuyRedeemConfig()
     */
    @Override
    public FlybuysRedeemConfigModel getFlybuysRedeemConfig() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_FLYBUYS_CONFIG);
        final List<FlybuysRedeemConfigModel> flybuysConfig = flexibleSearchService.<FlybuysRedeemConfigModel> search(
                query)
                .getResult();
        TargetServicesUtil.validateIfSingleResult(flybuysConfig,
                "No FlybuysRedeemConfigModel found in the system.",
                "More than one FlybuysRedeemConfigModel found in the system.");
        return flybuysConfig.get(0);
    }

    /**
     * Sets the flexible search service.
     * 
     * @param flexibleSearchService
     *            the flexibleSearchService to set
     */
    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }
}

/**
 * 
 */
package au.com.target.tgtcore.flybuys.dto;

/**
 * @author umesh
 * 
 */
public class FlybuysDiscountDenialDto {

    private boolean denied;
    private String reason;


    /**
     * @return the denied
     */
    public boolean isDenied() {
        return denied;
    }

    /**
     * @param denied
     *            the denied to set
     */
    public void setDenied(final boolean denied) {
        this.denied = denied;
    }

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason
     *            the reason to set
     */
    public void setReason(final String reason) {
        this.reason = reason;
    }


}

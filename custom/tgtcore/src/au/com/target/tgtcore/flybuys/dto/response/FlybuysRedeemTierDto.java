/**
 * 
 */
package au.com.target.tgtcore.flybuys.dto.response;

import java.math.BigDecimal;


/**
 * @author cwijesu1
 * 
 */
public class FlybuysRedeemTierDto {
    private Integer points;
    private BigDecimal dollarAmt;
    private String redeemCode;



    /**
     * @return the points
     */
    public Integer getPoints() {
        return points;
    }

    /**
     * @param points
     *            the points to set
     */
    public void setPoints(final int points) {
        this.points = new Integer(points);
    }

    /**
     * @return the dollarAmt
     */
    public BigDecimal getDollarAmt() {
        return dollarAmt;
    }

    /**
     * @param dollarAmt
     *            the dollarAmt to set
     */
    public void setDollarAmt(final BigDecimal dollarAmt) {
        this.dollarAmt = dollarAmt;
    }

    /**
     * @return the redeemCode
     */
    public String getRedeemCode() {
        return redeemCode;
    }

    /**
     * @param redeemCode
     *            the redeemCode to set
     */
    public void setRedeemCode(final String redeemCode) {
        this.redeemCode = redeemCode;
    }

}

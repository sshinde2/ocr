/**
 * 
 */
package au.com.target.tgtcore.flybuys.dto.response;

import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;


/**
 * @author knemalik
 * 
 */
public class FlybuysRefundResponseDto {
    private FlybuysResponseType response;
    private String confimationCode;
    private String responseMessage;
    private Integer responseCode;

    /**
     * @return the response
     */
    public FlybuysResponseType getResponse() {
        return response;
    }

    /**
     * @param response
     *            the response to set
     */
    public void setResponse(final FlybuysResponseType response) {
        this.response = response;
    }

    /**
     * @return the confimationCode
     */
    public String getConfimationCode() {
        return confimationCode;
    }

    /**
     * @param confimationCode
     *            the confimationCode to set
     */
    public void setConfimationCode(final String confimationCode) {
        this.confimationCode = confimationCode;
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage
     *            the responseMessage to set
     */
    public void setResponseMessage(final String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the responseCode
     */
    public Integer getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode
     *            the responseCode to set
     */
    public void setResponseCode(final Integer responseCode) {
        this.responseCode = responseCode;
    }




}

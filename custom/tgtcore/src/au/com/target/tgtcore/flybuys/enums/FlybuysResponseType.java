/**
 * 
 */
package au.com.target.tgtcore.flybuys.enums;

/**
 * @author knemalik
 * 
 */
public enum FlybuysResponseType {

    SUCCESS,
    FLYBUYS_OTHER_ERROR,
    UNAVAILABLE,
    INVALID;

    public String getMessageKey()
    {
        return this.toString().toLowerCase().replace("_", "");
    }

}

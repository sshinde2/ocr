/**
 * 
 */
package au.com.target.tgtcore.flybuys.service;

import au.com.target.tgtcore.model.FlybuysRedeemConfigModel;

/**
 * @author umesh
 *
 */
public interface FlybuysRedeemConfigService {

    /**
     * Gets the active fly buy redeem config.
     * 
     * @return the active fly buy redeem config
     */
    FlybuysRedeemConfigModel getFlybuysRedeemConfig();

}

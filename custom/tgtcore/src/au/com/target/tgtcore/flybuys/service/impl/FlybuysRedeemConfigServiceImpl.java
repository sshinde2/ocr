/**
 * 
 */
package au.com.target.tgtcore.flybuys.service.impl;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetRuntimeException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.flybuys.dao.FlybuysRedeemConfigDao;
import au.com.target.tgtcore.flybuys.service.FlybuysRedeemConfigService;
import au.com.target.tgtcore.model.FlybuysRedeemConfigModel;


/**
 * @author umesh
 * 
 */
public class FlybuysRedeemConfigServiceImpl implements FlybuysRedeemConfigService {

    private FlybuysRedeemConfigDao flybuysRedeemConfigDao;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.service.FlybuysRedeemConfigService#getFlybuysRedeemConfig()
     */
    @Override
    public FlybuysRedeemConfigModel getFlybuysRedeemConfig() {
        FlybuysRedeemConfigModel config = null;

        // If we fail to read the config, we'll throw a RuntimeException resulting in 500 errors for the user
        // We won't try to handle it in the callers of this method, since it should never happen.
        try {
            config = flybuysRedeemConfigDao.getFlybuysRedeemConfig();
        }
        catch (final TargetUnknownIdentifierException e) {
            throw new TargetRuntimeException("TargetUnknownIdentifierException while reading FlybuysRedeemConfig", e);
        }
        catch (final TargetAmbiguousIdentifierException e) {
            throw new TargetRuntimeException("TargetAmbiguousIdentifierException while reading FlybuysRedeemConfig", e);
        }

        return config;
    }

    /**
     * @param flybuysRedeemConfigDao
     *            the flybuysRedeemConfigDao to set
     */
    @Required
    public void setFlybuysRedeemConfigDao(final FlybuysRedeemConfigDao flybuysRedeemConfigDao) {
        this.flybuysRedeemConfigDao = flybuysRedeemConfigDao;
    }
}

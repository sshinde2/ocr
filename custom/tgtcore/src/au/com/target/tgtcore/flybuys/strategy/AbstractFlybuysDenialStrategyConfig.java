/**
 * 
 */
package au.com.target.tgtcore.flybuys.strategy;

import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;



/**
 * The Class AbstractFlybuysDenialStrategyConfig.
 * 
 * @author umesh
 */
public abstract class AbstractFlybuysDenialStrategyConfig implements FlybuysDenialStrategy {

    /**
     * Gets the FlybuysDiscountDenialDto.
     * 
     * @param reason
     *            the reason
     * @return the FlybuysDiscountDenialDto
     */
    protected FlybuysDiscountDenialDto getDenyDto(final String reason)
    {
        final FlybuysDiscountDenialDto deny = new FlybuysDiscountDenialDto();
        deny.setDenied(true);
        deny.setReason(reason);
        return deny;
    }
}

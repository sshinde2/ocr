/**
 * 
 */
package au.com.target.tgtcore.flybuys.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetRuntimeException;
import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;
import au.com.target.tgtcore.flybuys.service.FlybuysRedeemConfigService;
import au.com.target.tgtcore.flybuys.strategy.AbstractFlybuysDenialStrategyConfig;
import au.com.target.tgtcore.model.FlybuysRedeemConfigModel;


/**
 * @author Nandini
 * 
 */
public class FlybuysConfigOffDenialStrategy extends AbstractFlybuysDenialStrategyConfig {

    private FlybuysRedeemConfigService flybuysRedeemConfigService;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.strategy.FlybuysDenialStrategy#isFlybuysDiscountAllowed(de.hybris.platform.core.model.order.AbstractOrderModel)
     */
    @Override
    public FlybuysDiscountDenialDto isFlybuysDiscountAllowed(final AbstractOrderModel orderModel) {
        FlybuysRedeemConfigModel flybuysRedeemConfig = null;
        if (null != orderModel) {
            try {
                flybuysRedeemConfig = flybuysRedeemConfigService.getFlybuysRedeemConfig();
            }
            catch (final TargetRuntimeException e) {
                return getDenyDto(StringUtils.EMPTY);
            }
            if (flybuysRedeemConfig == null
                    || Boolean.FALSE.equals(flybuysRedeemConfig.getActive())) {
                return getDenyDto(StringUtils.EMPTY);
            }
        }
        return null;
    }

    /**
     * @param flybuysRedeemConfigService
     *            the flybuysRedeemConfigService to set
     */
    @Required
    public void setFlybuysRedeemConfigService(final FlybuysRedeemConfigService flybuysRedeemConfigService) {
        this.flybuysRedeemConfigService = flybuysRedeemConfigService;
    }
}
/**
 * 
 */
package au.com.target.tgtcore.flybuys.strategy.impl;

import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.UserModel;

import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;
import au.com.target.tgtcore.flybuys.strategy.FlybuysDenialStrategy;
import au.com.target.tgtcore.model.TargetCustomerModel;


/**
 * @author dcwillia
 * 
 */
public class GuestUserFlybuysDiscountDenialStrategy implements FlybuysDenialStrategy {

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.strategy.FlybuysDenialStrategy#isFlybuysDiscountAllowed(de.hybris.platform.core.model.order.AbstractOrderModel)
     */
    @Override
    public FlybuysDiscountDenialDto isFlybuysDiscountAllowed(final AbstractOrderModel orderModel) {
        if (isGuest(orderModel)) {
            final FlybuysDiscountDenialDto deny = new FlybuysDiscountDenialDto();
            deny.setDenied(true);
            deny.setReason("guest");
            return deny;
        }
        else {
            return null;
        }
    }

    private boolean isGuest(final AbstractOrderModel orderModel) {
        if (orderModel != null) {
            final UserModel user = orderModel.getUser();
            if (user instanceof TargetCustomerModel) {
                return CustomerType.GUEST.equals(((TargetCustomerModel)user).getType());
            }
        }
        return true; // shouldn't happen
    }
}

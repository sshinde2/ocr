/**
 * 
 */
package au.com.target.tgtcore.flybuys.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import org.apache.commons.lang.BooleanUtils;

import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;
import au.com.target.tgtcore.flybuys.strategy.FlybuysDenialStrategy;


/**
 * @author gsing236
 *
 */
public class PreOrderFlybuysDiscountDenialStrategy implements FlybuysDenialStrategy {

    /**
     * check if the order contains a preOrder item, then don't allow flybuys redemption.
     */
    @Override
    public FlybuysDiscountDenialDto isFlybuysDiscountAllowed(final AbstractOrderModel orderModel) {
        if (BooleanUtils.isTrue(orderModel.getContainsPreOrderItems())) {
            final FlybuysDiscountDenialDto deny = new FlybuysDiscountDenialDto();
            deny.setDenied(true);
            deny.setReason("preOrder");
            return deny;
        }
        else {
            return null;
        }
    }

}

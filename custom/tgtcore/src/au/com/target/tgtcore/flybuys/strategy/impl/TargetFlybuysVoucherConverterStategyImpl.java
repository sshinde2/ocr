/**
 * 
 */
package au.com.target.tgtcore.flybuys.strategy.impl;

import de.hybris.platform.voucher.model.VoucherModel;

import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtpayment.strategy.impl.TargetVoucherConverterStategyImpl;


/**
 * @author dcwillia
 * 
 */
public class TargetFlybuysVoucherConverterStategyImpl extends TargetVoucherConverterStategyImpl {
    public static final String FLYBUYS_LINE_NAME = "flybuys discount";

    @Override
    public String getVoucherDescription(final VoucherModel voucher, final String voucherCode) {
        if (voucher instanceof FlybuysDiscountModel) {
            return FLYBUYS_LINE_NAME;
        }
        else {
            return voucherCode;
        }
    }
}

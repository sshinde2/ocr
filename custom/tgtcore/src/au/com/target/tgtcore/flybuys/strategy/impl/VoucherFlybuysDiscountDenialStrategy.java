/**
 * 
 */
package au.com.target.tgtcore.flybuys.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.voucher.VoucherService;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;
import au.com.target.tgtcore.flybuys.strategy.FlybuysDenialStrategy;
import au.com.target.tgtcore.model.FlybuysDiscountModel;


/**
 * @author vivek
 * 
 */
public class VoucherFlybuysDiscountDenialStrategy implements FlybuysDenialStrategy {

    /** The Constant DENIAL_REASON_VOUCHER. */
    private static final String DENIAL_REASON_VOUCHER = "voucher";

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.strategy.FlybuysDenialStrategy#isFlybuysDiscountAllowed(de.hybris.platform.core.model.order.AbstractOrderModel)
     */
    private VoucherService voucherService;

    @Override
    public FlybuysDiscountDenialDto isFlybuysDiscountAllowed(final AbstractOrderModel orderModel) {
        final Collection<DiscountModel> discounts = getVoucherService().getAppliedVouchers(orderModel);
        if (isContainsVoucherButNotFlybuys(discounts)) {
            final FlybuysDiscountDenialDto deny = new FlybuysDiscountDenialDto();
            deny.setDenied(true);
            deny.setReason(DENIAL_REASON_VOUCHER);
            return deny;
        }
        return null;
    }


    /**
     * Checks if is order contains voucher but not flybuys.
     * 
     * @param discounts
     *            the discounts
     * @return true, if is order contains voucher but not flybuys
     */
    private boolean isContainsVoucherButNotFlybuys(final Collection<DiscountModel> discounts)
    {
        if (CollectionUtils.isEmpty(discounts)) {
            return false;
        }
        for (final DiscountModel discount : discounts) {
            if (discount instanceof FlybuysDiscountModel) {
                return false;
            }
        }
        return true;
    }


    /**
     * @return the voucherService
     */
    protected VoucherService getVoucherService() {
        return voucherService;
    }


    /**
     * @param voucherService
     *            the voucherService to set
     */
    @Required
    public void setVoucherService(final VoucherService voucherService) {
        this.voucherService = voucherService;
    }


}

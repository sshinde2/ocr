/**
 * 
 */
package au.com.target.tgtcore.formatter;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.model.TargetDealResultModel;
import de.hybris.platform.servicelayer.type.TypeService;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TMDiscountProductPromotionModel;
import au.com.target.tgtcore.model.TMDiscountPromotionModel;
import au.com.target.tgtcore.model.TargetValuePackModel;
import au.com.target.tgtcore.model.TargetValuePackTypeModel;
import au.com.target.tgtcore.taxinvoice.TaxInvoiceService;
import au.com.target.tgtcore.taxinvoice.data.TaxInvoiceDealSection;
import au.com.target.tgtcore.taxinvoice.data.TaxInvoiceItems;
import au.com.target.tgtcore.taxinvoice.data.TaxInvoiceLineItem;
import au.com.target.tgtcore.util.DealUtils;
import au.com.target.tgtutility.format.PriceFormatter;



/**
 * This class helps the Velocity Formatter to get the relevant details from the OrderModel
 * 
 * @author rsamuel3
 * 
 */
public class InvoiceFormatterHelper {

    private static final char ASTERISK = '*';

    private TaxInvoiceService taxInvoiceService;

    private TypeService typeService;

    private String taxInvoiceTMDMessage;

    private String taxInvoiceDealMessage;


    /**
     * get the compiled results for the Tax Invoice
     * 
     * @param orderModel
     * @return TaxInvoiceItems
     */
    public TaxInvoiceItems getTaxInvoiceItems(final OrderModel orderModel) {
        final TaxInvoiceItems taxInvoiceItems = new TaxInvoiceItems();
        final Double globalDiscounts = orderModel.getTotalDiscounts();
        if (globalDiscounts != null) {
            taxInvoiceItems.setVouchers(globalDiscounts.doubleValue());
        }

        final Map<AbstractOrderEntryModel, Long> entries = populateConsumedEntries(orderModel, taxInvoiceItems);
        if (MapUtils.isEmpty(entries)) {
            return taxInvoiceItems;
        }
        taxInvoiceItems.setTaxInvoiceLineItems(getLineItemsForTaxInvoice(entries));


        return taxInvoiceItems;
    }

    /**
     * @param flybuysNumber
     * @return maskedFlybuysNumber
     */
    public String getMaskedFlybuysNumber(final String flybuysNumber) {
        String maskedFlybuysNumber = null;
        if (StringUtils.isNotEmpty(flybuysNumber)) {
            final char[] flyBuysCharArray = flybuysNumber.toCharArray();
            for (int i = 3; i < flyBuysCharArray.length - 4; i++) {
                flyBuysCharArray[i] = ASTERISK;
            }
            maskedFlybuysNumber = String.copyValueOf(flyBuysCharArray);
        }
        return maskedFlybuysNumber;
    }


    private List<TaxInvoiceLineItem> getLineItemsForTaxInvoice(final Map<AbstractOrderEntryModel, Long> entries) {
        Assert.notNull(entries, "order cannot be null");


        final List<TaxInvoiceLineItem> taxInvoiceLineItems = new ArrayList<>();

        for (final AbstractOrderEntryModel entry : entries.keySet()) {
            if (entry == null) {
                continue;
            }

            final TaxInvoiceLineItem item = createTaxInvoiceItem(entry, entries.get(entry));

            final TargetValuePackTypeModel valuePackType = taxInvoiceService.findTargetValuePackType(entry
                    .getProduct().getCode());

            populateValuePackBundles(item, valuePackType);
            taxInvoiceLineItems.add(item);
        }
        return taxInvoiceLineItems;
    }

    /**
     * @param item
     * @param valuePackType
     */
    private void populateValuePackBundles(final TaxInvoiceLineItem item,
            final TargetValuePackTypeModel valuePackType) {
        if (valuePackType != null) {
            final List<TaxInvoiceLineItem> bundleItems = new ArrayList<>();
            final List<TargetValuePackModel> valuePacks = valuePackType.getValuePackListQualifier();
            String bundleType = "";
            for (final TargetValuePackModel valuePack : valuePacks) {
                bundleItems.add(createTaxInvoiceBundleItem(valuePack));
                if (StringUtils.isEmpty(bundleType)) {
                    bundleType = typeService.getEnumerationValue(valuePack.getValuePackDealType()).getName();
                }
            }
            item.setBundles(bundleItems);
            item.setBundleType(bundleType);
        }
    }

    /**
     * create a TaxInvoiceLineItem for a line item with bundle items
     * 
     * @param valuePack
     * @return TaxInvoiceLineItem
     */
    private TaxInvoiceLineItem createTaxInvoiceBundleItem(final TargetValuePackModel valuePack) {
        Assert.notNull(valuePack, "value pack cannot be null");

        final TaxInvoiceLineItem item = new TaxInvoiceLineItem();
        item.setProductCode(valuePack.getChildSKU());
        item.setProductName(valuePack.getChildSKUDescription());
        final long qty = valuePack.getQuantity().longValue();
        item.setQuantity(qty);
        final double basePrice = valuePack.getPrice().doubleValue();
        final double totalPrice = basePrice * qty;

        final String basePriceStr = PriceFormatter.twoDecimalFormat(basePrice);
        final String totalPriceStr = PriceFormatter.twoDecimalFormat(totalPrice);

        item.setUnitPrice(basePriceStr);
        item.setTotalPrice(totalPriceStr);
        item.setTotalPriceAsDouble(totalPrice);
        return item;
    }


    /**
     * create a TaxInvoiceLineItem for a normal line item
     * 
     * @param entry
     * @return TaxInvoiceLineItem
     */
    protected TaxInvoiceLineItem createTaxInvoiceItem(final AbstractOrderEntryModel entry, final Long quantity) {
        Assert.notNull(entry, "entry cannot be null");

        final TaxInvoiceLineItem item = new TaxInvoiceLineItem();
        final ProductModel product = entry.getProduct();

        String productName = product.getName();

        if (CollectionUtils.isNotEmpty(entry.getTaxValues())) {
            productName = productName + ASTERISK;
        }

        item.setProductCode(product.getCode());
        item.setProductName(productName);
        long qty = entry.getQuantity().longValue();
        if (quantity != null) {
            qty = quantity.longValue();
        }
        item.setQuantity(qty);

        final double basePrice = entry.getBasePrice().doubleValue();
        final double totalPrice = basePrice * qty;

        final String basePriceStr = PriceFormatter.twoDecimalFormat(basePrice);
        final String totalPriceStr = PriceFormatter.twoDecimalFormat(totalPrice);


        item.setUnitPrice(basePriceStr);
        item.setTotalPrice(totalPriceStr);
        item.setTotalPriceAsDouble(totalPrice);
        return item;
    }

    /**
     * populates the consumed entries
     * 
     * @param orderModel
     * @return Map of the entries not consumed and their quantity
     */
    private Map<AbstractOrderEntryModel, Long> populateConsumedEntries(final OrderModel orderModel,
            final TaxInvoiceItems taxInvoiceItems) {
        final Map<AbstractOrderEntryModel, Long> notConsumedOrderEntries = addAllOrderEntries(orderModel.getEntries());
        final Set<PromotionResultModel> promotionResults = orderModel.getAllPromotionResults();
        double overallDealSavings = 0.0d;

        for (final PromotionResultModel promoResult : promotionResults) {

            if (promoResult.getCertainty().floatValue() >= 1.0f) {
                final List<List<PromotionOrderEntryConsumedModel>> splitByInstanceEntries = splitConsumedEntriesByInstance(
                        promoResult);
                final AbstractPromotionModel promotion = promoResult.getPromotion();

                for (final List<PromotionOrderEntryConsumedModel> instanceEntries : splitByInstanceEntries) {
                    if (CollectionUtils.isNotEmpty(instanceEntries)) {
                        final TaxInvoiceDealSection dealSection = new TaxInvoiceDealSection();
                        double totalDiscount = 0d;
                        double tmdDiscount = 0d;

                        for (final PromotionOrderEntryConsumedModel consumedEntry : instanceEntries) {
                            final AbstractOrderEntryModel orderEntry = consumedEntry.getOrderEntry();
                            final long consumedQuantity = consumedEntry.getQuantity().longValue();
                            addAbstractOrderEntryToMap(notConsumedOrderEntries, orderEntry, consumedQuantity);

                            final TaxInvoiceLineItem invoiceLineItem = createTaxInvoiceLineItem(consumedEntry,
                                    orderEntry,
                                    consumedQuantity);
                            dealSection.addLineItem(invoiceLineItem);
                            if (promotion instanceof AbstractDealModel) {
                                final double discount = DealUtils.getTotalDiscounts(orderEntry, consumedEntry)
                                        * consumedEntry.getQuantity().longValue();
                                totalDiscount += discount;
                                if (discount > 0) {
                                    invoiceLineItem.setDiscount(PriceFormatter.twoDecimalFormat(discount));
                                }
                            }

                            // If a value pack is part of a deal that deal is really only going to be
                            // TMD, so there's no point looking for value packs for everything.
                            if (promotion instanceof TMDiscountPromotionModel) {
                                final TargetValuePackTypeModel valuePackType = taxInvoiceService
                                        .findTargetValuePackType(orderEntry.getProduct()
                                                .getCode());
                                final double discount = DealUtils.getTotalDiscounts(orderEntry, consumedEntry)
                                        * consumedEntry.getQuantity().longValue();
                                tmdDiscount += discount;

                                populateValuePackBundles(invoiceLineItem, valuePackType);
                            }
                        }

                        if (promotion instanceof AbstractDealModel) {
                            final String dealMessage = MessageFormat.format(getTaxInvoiceDealMessage(),
                                    promotion.getDescription(), PriceFormatter.twoDecimalFormat(totalDiscount));
                            final String cleanDealMessage = Jsoup.clean(dealMessage, Whitelist.none());

                            dealSection.setDealMessage(cleanDealMessage);
                            if (totalDiscount > 0.0d) {
                                overallDealSavings += totalDiscount;
                            }
                        }
                        else {
                            dealSection.setDealMessage(MessageFormat.format(getTaxInvoiceTMDMessage(),
                                    PriceFormatter.twoDecimalFormat(tmdDiscount)));
                        }
                        taxInvoiceItems.addTaxInvoiceDealSections(dealSection);
                    }
                }
            }
        }

        for (final PromotionResultModel promoResult : promotionResults) {

            if (promoResult.getCertainty().floatValue() >= 1.0f) {
                final AbstractPromotionModel promotion = promoResult.getPromotion();

                if (null != promoResult.getCustom() && promotion instanceof TMDiscountProductPromotionModel) {
                    final TMDiscountProductPromotionModel tmdPromotion = (TMDiscountProductPromotionModel)promotion;
                    final AbstractOrderEntryModel orderEntry = promoResult.getOrder().getEntries()
                            .get(Integer.parseInt(promoResult.getCustom()));
                    final ProductModel product = orderEntry.getProduct();
                    final List<TaxInvoiceLineItem> invoiceLineItems = findLineItem(product.getCode(), taxInvoiceItems);
                    long totalConsumedQuantity = 0;

                    if (null != invoiceLineItems) {
                        for (final TaxInvoiceLineItem invoiceLineItem : invoiceLineItems) {
                            totalConsumedQuantity = totalConsumedQuantity + invoiceLineItem.getQuantity();
                            final double nonTMDConsumedPrice = invoiceLineItem.getTotalPriceAsDouble();
                            final double tmdDiscountPerQty = (nonTMDConsumedPrice * tmdPromotion
                                    .getPercentageDiscount()
                                    .doubleValue()) / 100;
                            final double consumedTotalPrice = nonTMDConsumedPrice - tmdDiscountPerQty;
                            invoiceLineItem.setTotalPrice(PriceFormatter.twoDecimalFormat(consumedTotalPrice));
                            invoiceLineItem.setTotalPriceAsDouble(consumedTotalPrice);
                            if (invoiceLineItem.getQuantity() > 0) {
                                invoiceLineItem.setPromotionMessage(MessageFormat.format(getTaxInvoiceTMDMessage(),
                                        PriceFormatter.twoDecimalFormat(tmdDiscountPerQty)));
                            }
                        }
                    }
                    if (null == invoiceLineItems || totalConsumedQuantity < orderEntry.getQuantity().longValue()) {
                        final TaxInvoiceDealSection tmdDealSection = new TaxInvoiceDealSection();
                        tmdDealSection.addLineItem(createTaxInvoiceLineItemForRemainingNonConsumedQty(
                                tmdPromotion, orderEntry, product, totalConsumedQuantity));
                        addAbstractOrderEntryToMap(notConsumedOrderEntries, orderEntry, orderEntry.getQuantity()
                                .longValue());
                        taxInvoiceItems.addTaxInvoiceDealSections(tmdDealSection);
                    }
                }
            }
        }

        taxInvoiceItems.setItemsSavings(overallDealSavings);

        return notConsumedOrderEntries;
    }


    /**
     * Creates the tax invoice line item for remaining non consumed qty.
     * 
     * @param tmdPromotion
     *            the tmd promotion
     * @param orderEntry
     *            the order entry
     * @param product
     *            the product
     * @param totalConsumedQuantity
     *            the total consumed quantity
     * @return the tax invoice line item
     */
    private TaxInvoiceLineItem createTaxInvoiceLineItemForRemainingNonConsumedQty(
            final TMDiscountProductPromotionModel tmdPromotion, final AbstractOrderEntryModel orderEntry,
            final ProductModel product, final long totalConsumedQuantity) {
        final TaxInvoiceLineItem invoiceLineItem = new TaxInvoiceLineItem();
        invoiceLineItem.setProductCode(product.getCode());
        invoiceLineItem.setProductName(product.getName() + ASTERISK);
        final long quantity = orderEntry.getQuantity().longValue() - totalConsumedQuantity;
        invoiceLineItem.setQuantity(quantity);
        invoiceLineItem.setUnitPrice(PriceFormatter.twoDecimalFormat(orderEntry.getBasePrice()
                .doubleValue()));

        final double tmdDiscountPerQty = ((orderEntry.getBasePrice()
                .doubleValue() * tmdPromotion.getPercentageDiscount().doubleValue()) / 100);
        final double itemTotalPriceAsDouble = (orderEntry.getBasePrice()
                .doubleValue() - tmdDiscountPerQty) * quantity;
        invoiceLineItem.setTotalPrice(PriceFormatter.twoDecimalFormat(itemTotalPriceAsDouble));
        invoiceLineItem.setTotalPriceAsDouble(itemTotalPriceAsDouble);
        if (quantity > 0) {
            invoiceLineItem.setPromotionMessage(MessageFormat.format(getTaxInvoiceTMDMessage(),
                    PriceFormatter.twoDecimalFormat(tmdDiscountPerQty * quantity)));
        }
        return invoiceLineItem;
    }


    /**
     * Creates the tax invoice line item.
     * 
     * @param consumedEntry
     *            the consumed entry
     * @param orderEntry
     *            the order entry
     * @param consumedQuantity
     *            the consumed quantity
     * @return the tax invoice line item
     */
    private TaxInvoiceLineItem createTaxInvoiceLineItem(final PromotionOrderEntryConsumedModel consumedEntry,
            final AbstractOrderEntryModel orderEntry, final long consumedQuantity) {
        final TaxInvoiceLineItem invoiceLineItem = new TaxInvoiceLineItem();
        invoiceLineItem.setProductCode(orderEntry.getProduct().getCode());
        invoiceLineItem.setProductName(orderEntry.getProduct().getName() + ASTERISK);
        invoiceLineItem.setQuantity(consumedQuantity);
        invoiceLineItem.setUnitPrice(PriceFormatter.twoDecimalFormat(orderEntry.getBasePrice()
                .doubleValue()));
        final double itemTotalPriceAsDouble = consumedEntry
                .getAdjustedUnitPrice()
                .doubleValue()
                * consumedQuantity;
        invoiceLineItem.setTotalPrice(PriceFormatter.twoDecimalFormat(itemTotalPriceAsDouble));
        invoiceLineItem.setTotalPriceAsDouble(itemTotalPriceAsDouble);
        return invoiceLineItem;
    }

    /**
     * Utility method to find the TaxInvoiceLineItem if it available for product
     * 
     * @param productCode
     * @param taxInvoiceItems
     * @return lineItem if available else null
     */
    private List<TaxInvoiceLineItem> findLineItem(final String productCode, final TaxInvoiceItems taxInvoiceItems) {
        List<TaxInvoiceLineItem> lineItemList = null;
        if (StringUtils.isNotEmpty(productCode) && null != taxInvoiceItems
                && null != taxInvoiceItems.getTaxInvoiceDealSections()) {
            for (final TaxInvoiceDealSection dealSection : taxInvoiceItems.getTaxInvoiceDealSections()) {
                if (null != dealSection) {
                    for (final TaxInvoiceLineItem lineItem : dealSection.getLineItems()) {
                        if (null != lineItem && productCode.equals(lineItem.getProductCode())) {
                            if (null == lineItemList) {
                                lineItemList = new ArrayList<>();
                            }
                            lineItemList.add(lineItem);
                        }
                    }
                }
            }

        }
        return lineItemList;
    }

    /**
     * @param promoResult
     * @return List of all consumedEntries sepearted based on instance
     */
    private List<List<PromotionOrderEntryConsumedModel>> splitConsumedEntriesByInstance(
            final PromotionResultModel promoResult) {
        int instanceCountValue = 0;
        final AbstractPromotionModel promotion = promoResult.getPromotion();
        final List<List<PromotionOrderEntryConsumedModel>> resultsSplit = new ArrayList<List<PromotionOrderEntryConsumedModel>>();
        if (promotion instanceof TMDiscountPromotionModel) {
            final List<PromotionOrderEntryConsumedModel> filteredList = new ArrayList<>();
            filteredList.addAll(promoResult.getConsumedEntries());
            resultsSplit.add(filteredList);
            return resultsSplit;
        }
        if (promoResult instanceof TargetDealResultModel) {
            final TargetDealResultModel targetDealResultModel = (TargetDealResultModel)promoResult;
            final Integer instanceCount = targetDealResultModel.getInstanceCount();
            if (instanceCount != null) {
                instanceCountValue = instanceCount.intValue();
            }
        }
        final Collection<PromotionOrderEntryConsumedModel> consumedEntries = promoResult.getConsumedEntries();
        for (int i = 1; i <= instanceCountValue; i++) {
            final Collection<PromotionOrderEntryConsumedModel> filterbyIntance = DealUtils
                    .getConsumedEntriesForTheInstance(
                            i, consumedEntries);
            if (CollectionUtils.isNotEmpty(filterbyIntance)) {
                final List<PromotionOrderEntryConsumedModel> filteredList = new ArrayList<>();
                filteredList.addAll(filterbyIntance);
                resultsSplit.add(filteredList);
            }
        }
        return resultsSplit;
    }

    /**
     * @param list
     * @return Map of all the entries and their quantities
     */
    private Map<AbstractOrderEntryModel, Long> addAllOrderEntries(final List<AbstractOrderEntryModel> list) {
        final Map<AbstractOrderEntryModel, Long> allEntries = new HashMap<AbstractOrderEntryModel, Long>();
        for (final AbstractOrderEntryModel entryModel : list) {
            allEntries.put(entryModel, entryModel.getQuantity());
        }
        return allEntries;
    }

    /**
     * populate the AbstractOrderEntry to the map
     * 
     * @param notConsumedOrderEntries
     * @param orderEntry
     * @param quantityConsumed
     */
    private void addAbstractOrderEntryToMap(final Map<AbstractOrderEntryModel, Long> notConsumedOrderEntries,
            final AbstractOrderEntryModel orderEntry,
            final long quantityConsumed) {
        long remQuantity = orderEntry.getQuantity().longValue();
        if (MapUtils.isNotEmpty(notConsumedOrderEntries)) {
            if (notConsumedOrderEntries.containsKey(orderEntry)) {
                remQuantity = notConsumedOrderEntries.get(orderEntry).longValue();
            }
        }
        remQuantity -= quantityConsumed;
        if (remQuantity <= 0) {
            notConsumedOrderEntries.remove(orderEntry);
        }
        else {
            notConsumedOrderEntries.put(orderEntry, Long.valueOf(remQuantity));
        }
    }

    /**
     * @param taxInvoiceService
     *            the taxInvoiceService to set
     */
    @Required
    public void setTaxInvoiceService(final TaxInvoiceService taxInvoiceService) {
        this.taxInvoiceService = taxInvoiceService;
    }

    /**
     * @param typeService
     *            the typeService to set
     */
    @Required
    public void setTypeService(final TypeService typeService) {
        this.typeService = typeService;
    }

    /**
     * @return the taxInvoiceTMDMessage
     */
    public String getTaxInvoiceTMDMessage() {
        return taxInvoiceTMDMessage;
    }

    /**
     * @param taxInvoiceTMDMessage
     *            the taxInvoiceTMDMessage to set
     */
    @Required
    public void setTaxInvoiceTMDMessage(final String taxInvoiceTMDMessage) {
        this.taxInvoiceTMDMessage = taxInvoiceTMDMessage;
    }

    /**
     * @return the taxInvoiceDealMessage
     */
    public String getTaxInvoiceDealMessage() {
        return taxInvoiceDealMessage;
    }

    /**
     * @param taxInvoiceDealMessage
     *            the taxInvoiceDealMessage to set
     */
    @Required
    public void setTaxInvoiceDealMessage(final String taxInvoiceDealMessage) {
        this.taxInvoiceDealMessage = taxInvoiceDealMessage;
    }

}

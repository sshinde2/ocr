/**
 * 
 */
package au.com.target.tgtcore.formatter;

import de.hybris.platform.core.model.order.OrderModel;

import org.apache.velocity.VelocityContext;


/**
 * @author dcwillia
 * 
 */
public interface InvoiceOrderFormatter {
    /**
     * Add the order details to the velocity template context
     * 
     * @param vContext
     *            velocity template context
     * @param order
     *            order details
     */
    void addOrderDetails(final VelocityContext vContext, final OrderModel order);
}

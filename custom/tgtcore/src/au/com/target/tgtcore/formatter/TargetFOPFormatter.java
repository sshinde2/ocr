/**
 * 
 */
package au.com.target.tgtcore.formatter;

import de.hybris.platform.commons.jalo.FOPFormatter;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import eu.medsea.mimeutil.MimeType;
import eu.medsea.mimeutil.MimeUtil2;
import eu.medsea.mimeutil.detector.ExtensionMimeDetector;
import eu.medsea.mimeutil.detector.MagicMimeMimeDetector;


/**
 * The FOPFormatter has a memory issue with MimeUtil2, everytime the validate method gets called, it creates a new
 * instance of MimeUtil2 and initialize the MagicMimeMimeDector. This caused the same set of MagicMimeEntry adding to a
 * static list every time a pick confirm is received. This class is to override the validate method and make the
 * MimeUtil2 static.
 * 
 * @author htan3
 *
 */
public class TargetFOPFormatter extends FOPFormatter {

    private static final MimeUtil2 MIME_UTIL;

    static {
        MIME_UTIL = new MimeUtil2();
        MIME_UTIL.registerMimeDetector(ExtensionMimeDetector.class.getName());
        MIME_UTIL.registerMimeDetector(MagicMimeMimeDetector.class.getName());
    }

    @Override
    protected boolean validate(final File temporaryFile)
            throws IllegalArgumentException, IOException {
        if (temporaryFile == null) {
            throw new IllegalArgumentException(
                    "Validated pdf file path shouldn't be null");
        }

        for (final Iterator localIterator = MIME_UTIL.getMimeTypes(temporaryFile)
                .iterator(); localIterator.hasNext();) {
            final Object recognizedMime = localIterator.next();

            final MimeType mime = (MimeType)recognizedMime;
            if ("application/pdf".equalsIgnoreCase(mime.toString())) {
                return true;
            }
        }
        return false;
    }

}

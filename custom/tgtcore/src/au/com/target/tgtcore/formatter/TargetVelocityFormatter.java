/**
 * 
 */
package au.com.target.tgtcore.formatter;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commons.jalo.CommonsManager;
import de.hybris.platform.commons.jalo.VelocityFormatter;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloSystemException;
import de.hybris.platform.jalo.media.Media;
import de.hybris.platform.jalo.media.MediaManager;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.tools.ToolManager;
import org.springframework.beans.factory.annotation.Required;


/**
 * Formats the given item with a given Velocity template. As context prefix 'this' is used. Example: if the given item
 * is a product you'll get with "$this.code" in the template the code of the product.
 * 
 */
public class TargetVelocityFormatter extends VelocityFormatter
{
    private InvoiceOrderFormatter invoiceOrderFormatter;
    private ModelService modelService;
    private MediaService mediaService;
    private CatalogVersionService catalogVersionService;
    private String storeNumber;
    private KeyGenerator keyGenerator;

    @SuppressWarnings("deprecation")
    @Override
    public Media format(final Item item) {
        File tempfile = null;
        FileWriter fw = null;
        try {
            tempfile = File.createTempFile("velocityfile_temp", ".txt");
            final Properties velocityEngineProperties = new Properties();
            velocityEngineProperties.put("velocimacro.library", "");
            final VelocityEngine ve = CommonsManager.getInstance().getVelocityEngine(velocityEngineProperties);

            final ToolManager velocityToolManager = new ToolManager();
            velocityToolManager.setVelocityEngine(ve);
            final VelocityContext vContext = new VelocityContext(velocityToolManager.createContext());

            vContext.put("this", item);
            vContext.put("storeNumber", storeNumber);

            final OrderModel order = modelService.toModelLayer(item);

            invoiceOrderFormatter.addOrderDetails(vContext, order);

            // Get all the Media Images from media folder
            final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(
                    "targetContentCatalog", "Online");

            // In the fop, there is no way to include the web app context path
            // need to use the absolute file path to find the external image
            final MediaModel taxInvoiceHeader = mediaService.getMedia(catalogVersionModel, "taxInvoiceHeader");

            final String taxInvoiceHeaderUrl = taxInvoiceHeader != null ? mediaService.getFiles(taxInvoiceHeader)
                    .iterator().next().getPath() : "";

            vContext.put("taxInvoiceHeaderUrl", taxInvoiceHeaderUrl);

            fw = new FileWriter(tempfile);
            ve.evaluate(vContext, fw, "VelocityFormatter", new InputStreamReader(getDataFromStream()));
            fw.close();

            final Media ret = MediaManager.getInstance()
                    .createMedia("Velocity2Media-" + keyGenerator.generate());
            ret.setFile(tempfile);
            return ret;
        }
        catch (final Exception e) {
            throw new JaloSystemException(e, "Could not initialize velocity engine!", -1);
        }
        finally {
            IOUtils.closeQuietly(fw);
            FileUtils.deleteQuietly(tempfile);
        }
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    @Required
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param mediaService
     *            the mediaService to set
     */
    @Required
    public void setMediaService(final MediaService mediaService) {
        this.mediaService = mediaService;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * 
     * @param keyGenerator
     *            the keyGenerator to set
     */
    @Required
    public void setKeyGenerator(final KeyGenerator keyGenerator) {
        this.keyGenerator = keyGenerator;
    }

    /**
     * @param invoiceOrderFormatter
     *            the invoiceOrderFormatter to set
     */
    @Required
    public void setInvoiceOrderFormatter(final InvoiceOrderFormatter invoiceOrderFormatter) {
        this.invoiceOrderFormatter = invoiceOrderFormatter;
    }
}

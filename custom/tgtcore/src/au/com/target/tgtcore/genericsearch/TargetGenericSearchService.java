package au.com.target.tgtcore.genericsearch;

import de.hybris.platform.core.GenericQuery;
import de.hybris.platform.genericsearch.GenericSearchQuery;
import de.hybris.platform.genericsearch.GenericSearchService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;

/**
 * Target extension to {@link GenericSearchService}.
 */
public interface TargetGenericSearchService extends GenericSearchService {

    /**
     * Searches for a single object given the provided {@code query}.
     *
     * @param query the query
     * @param <T> the type of returned result
     * @return the unique record found
     * @throws ModelNotFoundException if no record found
     * @throws AmbiguousIdentifierException if multiple records found
     */
    <T> T getSingleResult(GenericQuery query) throws ModelNotFoundException, AmbiguousIdentifierException;

    /**
     * Searches for a single object given the provided {@code searchQuery}.
     *
     * @param searchQuery the search query
     * @param <T> the type of returned result
     * @return the unique record found
     * @throws ModelNotFoundException if no record found
     * @throws AmbiguousIdentifierException if multiple records found
     */
    <T> T getSingleResult(GenericSearchQuery searchQuery) throws ModelNotFoundException, AmbiguousIdentifierException;
}

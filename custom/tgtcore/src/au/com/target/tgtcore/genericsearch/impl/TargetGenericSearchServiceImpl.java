package au.com.target.tgtcore.genericsearch.impl;

import au.com.target.tgtcore.genericsearch.TargetGenericSearchService;
import de.hybris.platform.core.GenericQuery;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.type.ViewTypeModel;
import de.hybris.platform.genericsearch.GenericSearchQuery;
import de.hybris.platform.genericsearch.impl.DefaultGenericSearchService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.type.TypeService;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

/**
 * Default implementation of {@link TargetGenericSearchService}.
 */
public class TargetGenericSearchServiceImpl extends DefaultGenericSearchService implements TargetGenericSearchService {

    private static final Logger LOG = Logger.getLogger(TargetGenericSearchServiceImpl.class);

    private SessionService sessionService;
    private FlexibleSearchService flexibleSearchService;
    private TypeService typeService;
    private CommonI18NService commonI18NService;


    @Override
    public <T> T getSingleResult(final GenericQuery query)
            throws ModelNotFoundException, AmbiguousIdentifierException {
        final GenericSearchQuery searchQuery = new GenericSearchQuery(query);
        searchQuery.setLocale(getCommonI18NService().getLocaleForLanguage(getCommonI18NService().getCurrentLanguage()));
        return getSingleResult(searchQuery);
    }

    @Override
    public <T> T getSingleResult(final GenericSearchQuery searchQuery)
            throws ModelNotFoundException, AmbiguousIdentifierException {
        validateParameterNotNullStandardMessage("genericSearchQuery", searchQuery);
        validateParameterNotNullStandardMessage("genericSearchQuery.getQuery()", searchQuery.getQuery());
        return getSessionService().executeInLocalView(new SessionExecutionBody() {
            @Override
            public Object execute() {
                final ComposedTypeModel initialType = getTypeService().getComposedTypeForCode(
                        searchQuery.getQuery().getInitialTypeCode());
                if (initialType instanceof ViewTypeModel) {
                    throw new IllegalArgumentException("searching for ViewType is currently unimplemented");
                }
                else {
                    final Map<String, Object> values = new HashMap<String, Object>();
                    final String query = searchQuery.getQuery().toFlexibleSearch(values);
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("QUERY: " + query);
                        LOG.debug("VALUES: " + values);
                    }
                    final FlexibleSearchQuery fsq = new FlexibleSearchQuery(query);
                    fsq.setNeedTotal(searchQuery.isNeedTotal());
                    fsq.setResultClassList(searchQuery.getQuery().getResultClasses());
                    fsq.addQueryParameters(values);
                    fsq.setCount(searchQuery.getCount());
                    fsq.setStart(searchQuery.getStart());
                    fsq.setFailOnUnknownFields(false);
                    fsq.setLocale(searchQuery.getLocale());
                    fsq.setCatalogVersions(searchQuery.getCatalogVersions());
                    return getFlexibleSearchService().searchUnique(fsq);
                }
            }
        });
    }


    /**
     * Returns the session service.
     *
     * @return the session service
     */
    public SessionService getSessionService() {
        return sessionService;
    }

    @Override
    public void setSessionService(final SessionService sessionService) {
        super.setSessionService(sessionService);
        this.sessionService = sessionService;
    }

    /**
     * Returns the flexible search service.
     *
     * @return the flexible search service
     */
    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    @Override
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        super.setFlexibleSearchService(flexibleSearchService);
        this.flexibleSearchService = flexibleSearchService;
    }

    /**
     * Returns the type service.
     *
     * @return the type service
     */
    public TypeService getTypeService() {
        return typeService;
    }

    @Override
    public void setTypeService(final TypeService typeService) {
        super.setTypeService(typeService);
        this.typeService = typeService;
    }

    /**
     * Returns the common internationalization service.
     *
     * @return the common internationalization service
     */
    public CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    @Override
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        super.setCommonI18NService(commonI18NService);
        this.commonI18NService = commonI18NService;
    }
}

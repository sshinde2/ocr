/**
 * 
 */
package au.com.target.tgtcore.giftcards;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;

import au.com.target.tgtcore.model.GiftCardModel;
import au.com.target.tgtcore.product.exception.ProductNotFoundException;


/**
 * @author smishra1
 *
 */
public interface GiftCardService {

    /**
     * Method to return a GiftCardModel for a given brandId
     * 
     * @param brandId
     * 
     * @return {@link GiftCardModel}
     */
    public GiftCardModel getGiftCard(String brandId);

    /**
     * Method to return a GiftCardModel for a given productCode
     * 
     * @param productCode
     * @return {@link GiftCardModel}
     */
    public GiftCardModel getGiftCardForProduct(String productCode) throws ProductNotFoundException;


    /**
     * @param product
     * @return true if product is a gift card and false otherwise
     */
    public boolean isProductAGiftCard(ProductModel product);

    /**
     * Method to check if cart contains gift card product
     * 
     * @param cart
     * @return true - if cart contains a gift card
     */
    public boolean doesCartHaveAGiftCard(CartModel cart);

    /**
     * Method to check whether cart has gift cards only
     * 
     * @param cart
     * @return true - if all products are gift cards
     */
    public boolean doesCartHaveGiftCardsOnly(CartModel cart);

    /**
     * method to check if product is digital giftcard
     * 
     * @param product
     * @return true - is product is digital gift card
     */
    public boolean isProductADigitalGiftCard(final ProductModel product);

    /**
     * Method to check whether the cart has only physical gift card(s).
     *
     * @param cart
     * @return true - if all products are physical giftcard(s).
     */
    public boolean doesCartHavePhysicalGiftCardOnly(CartModel cart);

    /**
     * Method to check if the product is a physical gift card or not.
     *
     * @param productModel
     * @return true - if the product is a physical gift card.
     */
    public boolean isProductAPhysicalGiftCard(final ProductModel productModel);

    /**
     * Method to check whether the cart contains physical and digital gift card(s).
     *
     * @param cart
     * @return true - if all products are physical and digital gift card(s).
     */
    public boolean doesCartHavePhysicalAndDigitalGiftCardsOnly(CartModel cart);
}

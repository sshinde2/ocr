/**
 * 
 */
package au.com.target.tgtcore.giftcards;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;


/**
 *
 * Service interface for Gift Card operations.
 * 
 * @author jjayawa1
 *
 */
public interface GiftRecipientService {

    /**
     * Service method to add a gift recipient to an order entry.
     * 
     * @param recipient
     *            GiftRecipientDTO
     * @param orderEntry
     *            AbstractOrderEntryModel
     */
    void addGiftRecipientToOrderEntry(final GiftRecipientDTO recipient, AbstractOrderEntryModel orderEntry);
}

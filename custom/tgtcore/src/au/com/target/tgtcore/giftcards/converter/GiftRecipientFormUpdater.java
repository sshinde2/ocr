/**
 * 
 */
package au.com.target.tgtcore.giftcards.converter;

import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;
import au.com.target.tgtcore.model.GiftRecipientModel;


/**
 * @author sbryan6
 *
 */
public interface GiftRecipientFormUpdater {

    /**
     * Update the recipientModel given values from the DTO
     * 
     * @param recipientModel
     * @param recipientDto
     */
    void update(GiftRecipientModel recipientModel, GiftRecipientDTO recipientDto);
}

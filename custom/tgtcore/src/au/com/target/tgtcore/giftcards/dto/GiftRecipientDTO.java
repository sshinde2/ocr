/**
 * 
 */
package au.com.target.tgtcore.giftcards.dto;

/**
 * GiftRecipient DTO.
 * 
 * @author jjayawa1
 *
 */
public class GiftRecipientDTO {
    private String firstName;
    private String lastName;
    private String recipientEmailAddress;
    private String messageText;

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the recipientEmailAddress
     */
    public String getRecipientEmailAddress() {
        return recipientEmailAddress;
    }

    /**
     * @param recipientEmailAddress
     *            the recipientEmailAddress to set
     */
    public void setRecipientEmailAddress(final String recipientEmailAddress) {
        this.recipientEmailAddress = recipientEmailAddress;
    }

    /**
     * @return the messageText
     */
    public String getMessageText() {
        return messageText;
    }

    /**
     * @param messageText
     *            the messageText to set
     */
    public void setMessageText(final String messageText) {
        this.messageText = messageText;
    }

}

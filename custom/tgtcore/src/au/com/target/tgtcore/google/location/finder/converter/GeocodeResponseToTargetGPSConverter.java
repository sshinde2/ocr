/**
 * 
 */
package au.com.target.tgtcore.google.location.finder.converter;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Arrays;
import java.util.List;

import au.com.target.tgtcore.constants.TgtCoreConstants.GeocodeAddressComponentType;
import au.com.target.tgtcore.converters.TargetConverter;
import au.com.target.tgtcore.google.location.finder.data.GeocodeAddressComponent;
import au.com.target.tgtcore.google.location.finder.data.GeocodeGeometry;
import au.com.target.tgtcore.google.location.finder.data.GeocodeLocation;
import au.com.target.tgtcore.google.location.finder.data.GeocodeResponse;
import au.com.target.tgtcore.google.location.finder.data.GeocodeResultData;
import au.com.target.tgtcore.storelocator.impl.TargetGPS;


/**
 * @author bpottass
 *
 */
public class GeocodeResponseToTargetGPSConverter implements TargetConverter<GeocodeResponse, TargetGPS> {

    @Override
    public TargetGPS convert(final GeocodeResponse response) throws ConversionException {
        final List<GeocodeResultData> geocodeResultsList = response.getResults();

        //Google maps API always gives one result with this service call.
        final GeocodeResultData geocodingResultData = geocodeResultsList.get(0);
        final GeocodeLocation location = getGeocodeLocation(geocodingResultData);
        return createTargetGPS(geocodingResultData, location);
    }

    /**
     * @param geocodingResultData
     * @return geocodeLocation
     */
    private GeocodeLocation getGeocodeLocation(final GeocodeResultData geocodingResultData) {
        final GeocodeGeometry geocodeGeometry = geocodingResultData.getGeometry();
        return geocodeGeometry.getLocation();
    }

    /**
     * @param geocodingResultData
     * @param location
     * @return TargetGPS with locality, postcode and state.
     */
    private TargetGPS createTargetGPS(final GeocodeResultData geocodingResultData, final GeocodeLocation location) {
        final TargetGPS targetGPS = new TargetGPS().create(location.getLat(), location.getLng());
        for (final GeocodeAddressComponent geocoderAddressComponent : geocodingResultData.getAddressComponents()) {
            if (geocoderAddressComponent.getTypes().containsAll(
                    Arrays.asList(GeocodeAddressComponentType.LOCALITY, GeocodeAddressComponentType.POLITICAL))) {
                targetGPS.setLocality(geocoderAddressComponent.getLongName());
            }
            if (geocoderAddressComponent.getTypes().contains(GeocodeAddressComponentType.POSTAL_CODE)) {
                targetGPS.setPostcode(geocoderAddressComponent.getLongName());
            }
            if (geocoderAddressComponent.getTypes()
                    .containsAll(Arrays.asList(GeocodeAddressComponentType.ADMINISTRATIVE_AREA_LEVEL_1,
                            GeocodeAddressComponentType.POLITICAL))) {
                targetGPS.setState(geocoderAddressComponent.getShortName());
            }
        }
        if (targetGPS.getLocality() == null && geocodingResultData.getPostcodeLocalities() != null) {
            targetGPS.setLocality(geocodingResultData.getPostcodeLocalities()[0]);
        }
        return targetGPS;
    }

}

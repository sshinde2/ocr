/**
 * 
 */
package au.com.target.tgtcore.google.location.finder.data;

/**
 * @author bpottass
 *
 */
public class GeocodeGeometry {

    private GeocodeLocation location;

    /**
     * @return the location
     */
    public GeocodeLocation getLocation() {
        return location;
    }

    /**
     * @param location
     *            the location to set
     */
    public void setLocation(final GeocodeLocation location) {
        this.location = location;
    }

}

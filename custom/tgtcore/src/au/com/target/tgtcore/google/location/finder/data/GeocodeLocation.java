/**
 * 
 */
package au.com.target.tgtcore.google.location.finder.data;

/**
 * @author bpottass
 *
 */
public class GeocodeLocation {

    private double lat;

    private double lng;

    /**
     * @return the lat
     */
    public double getLat() {
        return lat;
    }

    /**
     * @param lat
     *            the lat to set
     */
    public void setLat(final double lat) {
        this.lat = lat;
    }

    /**
     * @return the lng
     */
    public double getLng() {
        return lng;
    }

    /**
     * @param lng
     *            the lng to set
     */
    public void setLng(final double lng) {
        this.lng = lng;
    }

}

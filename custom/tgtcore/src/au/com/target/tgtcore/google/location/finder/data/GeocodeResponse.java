/**
 * 
 */
package au.com.target.tgtcore.google.location.finder.data;

import java.util.List;


/**
 * @author bbaral1
 *
 */
public class GeocodeResponse {

    private String status;

    private List<GeocodeResultData> results;

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @return the results
     */
    public List<GeocodeResultData> getResults() {
        return results;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * @param results
     *            the results to set
     */
    public void setResults(final List<GeocodeResultData> results) {
        this.results = results;
    }
}

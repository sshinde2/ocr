/**
 * 
 */
package au.com.target.tgtcore.google.location.finder.data;

import java.util.List;

/**
 * @author bbaral1
 *
 */
public class GeocodeResultData {

    private List<GeocodeAddressComponent> addressComponents;
    private String[] postcodeLocalities;
    private GeocodeGeometry geometry;

    /**
     * @return the addressComponents
     */
    public List<GeocodeAddressComponent> getAddressComponents() {
        return addressComponents;
    }

    /**
     * @return the postcodeLocalities
     */
    public String[] getPostcodeLocalities() {
        return postcodeLocalities;
    }

    /**
     * @return the geometry
     */
    public GeocodeGeometry getGeometry() {
        return geometry;
    }

    /**
     * @param geometry
     *            the geometry to set
     */
    public void setGeometry(final GeocodeGeometry geometry) {
        this.geometry = geometry;
    }

    /**
     * @param addressComponents
     *            the addressComponents to set
     */
    public void setAddressComponents(final List<GeocodeAddressComponent> addressComponents) {
        this.addressComponents = addressComponents;
    }

    /**
     * @param postcodeLocalities
     *            the postcodeLocalities to set
     */
    public void setPostcodeLocalities(final String[] postcodeLocalities) {
        this.postcodeLocalities = postcodeLocalities;
    }

}

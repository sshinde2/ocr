package au.com.target.tgtcore.google.location.finder.route;

public interface TargetGeolocationFinderUrlBuilder {

    /**
     * Method will return url to fetch location details based on the input 'location' provided.
     * 
     * @param location
     * @return String
     */
    String getLocationUrlSuburb(String location);

    /**
     * Method will return url to fetch location details based on the input 'latitude' and 'longitude' provided.
     * 
     * @param latitude
     * @param longitude
     * @return String
     */
    String getLocationUrlByCoordinate(Double latitude, Double longitude);
}

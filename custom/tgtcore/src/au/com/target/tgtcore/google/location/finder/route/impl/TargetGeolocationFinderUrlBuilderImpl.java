package au.com.target.tgtcore.google.location.finder.route.impl;

import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.apache.log4j.Logger;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import au.com.target.tgtcore.google.location.finder.route.TargetGeolocationFinderUrlBuilder;
import au.com.target.tgtcore.storelocator.route.impl.GoogleUrlSigner;


/**
 * 
 * @author bbaral1
 *
 */
public class TargetGeolocationFinderUrlBuilderImpl
        implements TargetGeolocationFinderUrlBuilder {

    private static final Logger LOG = Logger.getLogger(TargetGeolocationFinderUrlBuilderImpl.class);

    private static final String JSON_TYPE = "json";

    private static final String BASE_URL_NOTNULL_ERROR_MESSAGE = "Base Url cannot be null";

    private static final String LOCATION_NOTNULL_ERROR_MESSAGE = "Location cannot be null";

    private static final String LATLNG_NOTNULL_ERROR_MESSAGE = "Latitude and Longitude cannot be null";

    private GoogleUrlSigner googleUrlSigner;

    private String locationSearchBaseUrl;

    @Override
    public String getLocationUrlSuburb(final String location) {
        ServicesUtil.validateParameterNotNull(getLocationSearchBaseUrl(), BASE_URL_NOTNULL_ERROR_MESSAGE);
        ServicesUtil.validateParameterNotNull(location, LOCATION_NOTNULL_ERROR_MESSAGE);
        final String urlSignature = addSignature(
                UriComponentsBuilder.fromHttpUrl(getLocationSearchBaseUrl()).path(JSON_TYPE)
                .queryParam("address", formatString(location))
                .queryParam("components", "country:AU")
                .build());
        LOG.info("INFO-GOOGLEAPI-LOOKUP-LOCATION_URI : Location URI= " + urlSignature);
        return urlSignature;
    }

    @Override
    public String getLocationUrlByCoordinate(final Double latitude, final Double longitude) {
        ServicesUtil.validateParameterNotNull(getLocationSearchBaseUrl(), BASE_URL_NOTNULL_ERROR_MESSAGE);
        ServicesUtil.validateParameterNotNull(latitude, LATLNG_NOTNULL_ERROR_MESSAGE);
        ServicesUtil.validateParameterNotNull(longitude, LATLNG_NOTNULL_ERROR_MESSAGE);
        final String urlSignature = addSignature(
                UriComponentsBuilder.fromHttpUrl(getLocationSearchBaseUrl()).path(JSON_TYPE)
                        .queryParam("latlng", latitude + "," + longitude)
                        .build());
        LOG.info("INFO-GOOGLEAPI-LOOKUP-COORDINATE_URI : Coordinate URI=" + urlSignature);
        return urlSignature;
    }

    /**
     * Add signature for business application.
     * 
     * @param uri
     *            the uri to add signature to
     * @return the string representing given {@code uri} with signature appended
     */
    private String addSignature(final UriComponents uri) {
        return uri.getScheme() + "://" + uri.getHost()
                + getGoogleUrlSigner().sign(uri.getPath() + '?' + uri.getQuery());
    }

    /**
     * Returns the Google URL signer.
     * 
     * @return the URL signer
     */
    public GoogleUrlSigner getGoogleUrlSigner() {
        return googleUrlSigner;
    }

    /**
     * Sets the Google URL signer.
     * 
     * @param googleUrlSigner
     *            the URL signer to use
     */
    public void setGoogleUrlSigner(final GoogleUrlSigner googleUrlSigner) {
        this.googleUrlSigner = googleUrlSigner;
    }

    private String formatString(final String start) {
        return start.replaceAll("\\s", "+");
    }

    /**
     * @return the locationSearchBaseUrl
     */
    public String getLocationSearchBaseUrl() {
        return locationSearchBaseUrl;
    }

    /**
     * @param locationSearchBaseUrl
     *            the locationSearchBaseUrl to set
     */
    public void setLocationSearchBaseUrl(final String locationSearchBaseUrl) {
        this.locationSearchBaseUrl = locationSearchBaseUrl;
    }

}

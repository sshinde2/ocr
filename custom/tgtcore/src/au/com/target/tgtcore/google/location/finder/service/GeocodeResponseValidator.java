/**
 * 
 */
package au.com.target.tgtcore.google.location.finder.service;

import au.com.target.tgtcore.google.location.finder.data.GeocodeResponse;
import au.com.target.tgtcore.google.location.finder.exception.GeocodeResponseException;


/**
 * @author bpottass
 *
 */
public interface GeocodeResponseValidator {


    public void validate(GeocodeResponse response) throws GeocodeResponseException;

}

/**
 * 
 */
package au.com.target.tgtcore.google.location.finder.service.impl;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.constants.TgtCoreConstants.GeocodeExceptionMessage;
import au.com.target.tgtcore.constants.TgtCoreConstants.GeocodeStatus;
import au.com.target.tgtcore.google.location.finder.data.GeocodeGeometry;
import au.com.target.tgtcore.google.location.finder.data.GeocodeResponse;
import au.com.target.tgtcore.google.location.finder.exception.GeocodeResponseException;
import au.com.target.tgtcore.google.location.finder.service.GeocodeResponseValidator;


/**
 * @author bpottass
 *
 */
public class GeocodeResponseValidatorImpl implements GeocodeResponseValidator {
    

    @Override
    public void validate(final GeocodeResponse response) throws GeocodeResponseException {
        if (response == null) {
            throw new IllegalArgumentException("Geocode Response cannot be null");
        }
        if (CollectionUtils.isEmpty(response.getResults()) || !response.getStatus().equals(GeocodeStatus.OK)) {
            throw new GeocodeResponseException(getErrorMessage(response.getStatus()));
        }
        validateGeocodeGeometry(response.getResults().get(0).getGeometry());
    }

    /**
     * Makes sure latitude and longitude are present in the response
     * 
     * @param geometry
     * @throws GeocodeResponseException
     */
    private void validateGeocodeGeometry(final GeocodeGeometry geometry) throws GeocodeResponseException {
        if (geometry == null || geometry.getLocation() == null) {
            throw new GeocodeResponseException("Error: Invalid geometry object");
        }
    }

    /**
     * Exception scenarios and appropriate message
     * 
     * @param status
     * @return String
     */
    private String getErrorMessage(final String status) {
        String errorMessage;
        if (GeocodeStatus.ZERO_RESULTS.equals(status)) {
            errorMessage = GeocodeExceptionMessage.ZERO_RESULTS;
        }
        else if (GeocodeStatus.OVER_DAILY_LIMIT.equals(status)) {
            errorMessage = GeocodeExceptionMessage.OVER_DAILY_LIMIT;
        }
        else if (GeocodeStatus.OVER_QUERY_LIMIT.equals(status)) {
            errorMessage = GeocodeExceptionMessage.OVER_QUERY_LIMIT;
        }
        else if (GeocodeStatus.REQUEST_DENIED.equals(status)) {
            errorMessage = GeocodeExceptionMessage.REQUEST_DENIED;
        }
        else if (GeocodeStatus.INVALID_REQUEST.equals(status)) {
            errorMessage = GeocodeExceptionMessage.INVALID_REQUEST;
        }
        else if (GeocodeStatus.UNKNOWN_ERROR.equals(status)) {
            errorMessage = GeocodeExceptionMessage.UNKNOWN_ERROR;
        }
        else {
            errorMessage = "ERROR: No matching geocode status";
        }
        return errorMessage;
    }

}

/**
 * 
 */
package au.com.target.tgtcore.google.location.finder.service.impl;

import de.hybris.platform.core.model.user.AddressModel;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

import au.com.target.tgtcore.google.location.finder.route.TargetGeolocationFinderUrlBuilder;

/**
 * @author  bpottass
 */
public class TargetGeocodeLocationFinderHelper {


    private TargetGeolocationFinderUrlBuilder targetGeolocationFinderUrlBuilder;

    /**
     * 
     * @param location
     * @param latitude
     * @param longitude
     * @return url
     * @throws MalformedURLException
     */
    public URL getUrl(final String location, final Double latitude, final Double longitude)
            throws MalformedURLException {
        return StringUtils.isNotEmpty(location) ? getUrl(location)
                : getUrl(latitude, longitude);
    }

    /**
     * 
     * @param address
     * @return url
     * @throws MalformedURLException
     */
    public URL getUrl(final AddressModel address) throws MalformedURLException {
        return getUrl(addressToString(address));
    }


    /**
     * @param locationOrPostCode
     * @return url
     * @throws MalformedURLException
     */
    public URL getUrl(final String locationOrPostCode) throws MalformedURLException {
        return new URL(getTargetGeolocationFinderUrlBuilder().getLocationUrlSuburb(locationOrPostCode));
    }

    /**
     * @param latitude
     * @param longitude
     * @return url
     * @throws MalformedURLException
     */
    public URL getUrl(final Double latitude, final Double longitude) throws MalformedURLException {
        return new URL(getTargetGeolocationFinderUrlBuilder().getLocationUrlByCoordinate(latitude, longitude));
    }

    /**
     * 
     * @param address
     * @return address as String
     */
    private String addressToString(final AddressModel address) {
        return Joiner.on(' ').skipNulls().join(
                Lists.newArrayList(
                        address.getStreetnumber(), address.getStreetname(),
                        address.getPostalcode(), address.getTown(), address.getDistrict(),
                        address.getCountry() != null ? address.getCountry().getIsocode() : "AU"));
    }

    /**
     * @return the targetGeolocationFinderUrlBuilder
     */
    public TargetGeolocationFinderUrlBuilder getTargetGeolocationFinderUrlBuilder() {
        return targetGeolocationFinderUrlBuilder;
    }

    /**
     * @param targetGeolocationFinderUrlBuilder
     *            the targetGeolocationFinderUrlBuilder to set
     */
    @Required
    public void setTargetGeolocationFinderUrlBuilder(
            final TargetGeolocationFinderUrlBuilder targetGeolocationFinderUrlBuilder) {
        this.targetGeolocationFinderUrlBuilder = targetGeolocationFinderUrlBuilder;
    }
}
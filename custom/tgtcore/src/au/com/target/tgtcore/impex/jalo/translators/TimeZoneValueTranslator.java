/**
 * 
 */
package au.com.target.tgtcore.impex.jalo.translators;

import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;

import java.util.TimeZone;

import org.apache.commons.lang.ArrayUtils;


/**
 * @author rmcalave
 * 
 */
public class TimeZoneValueTranslator extends AbstractValueTranslator {

    /* (non-Javadoc)
     * @see de.hybris.platform.impex.jalo.translators.AbstractValueTranslator#exportValue(java.lang.Object)
     */
    @Override
    public String exportValue(final Object obj) throws JaloInvalidParameterException {
        if (obj instanceof TimeZone) {
            return ((TimeZone)obj).getID();
        }

        return null;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.impex.jalo.translators.AbstractValueTranslator#importValue(java.lang.String, de.hybris.platform.jalo.Item)
     */
    @Override
    public Object importValue(final String timeZoneId, final Item item) throws JaloInvalidParameterException {
        final String[] availableTimeZoneIds = TimeZone.getAvailableIDs();
        if (!ArrayUtils.contains(availableTimeZoneIds, timeZoneId)) {
            setError();
            return null;
        }

        final TimeZone timeZone = TimeZone.getTimeZone(timeZoneId);
        return timeZone;
    }

}

/**
 * 
 */
package au.com.target.tgtcore.impex.jalo.translators;

import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;

import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.ObjectUtils;


/**
 * @author agarwalr
 *
 */
public class UtcOffsetValueTranslator extends AbstractValueTranslator {

    /**
     * Import UTC offset in hours with no consideration of Daylight savings e.g. will return always '+10' for
     * 'Australia/Victoria' timezone.
     */
    @Override
    public Object importValue(final String timeZoneId, final Item item) throws JaloInvalidParameterException {

        final String[] availableTimeZoneIds = TimeZone.getAvailableIDs();

        if (!ArrayUtils.contains(availableTimeZoneIds, timeZoneId)) {
            setError();
            return null;
        }

        final TimeZone timeZone = TimeZone.getTimeZone(timeZoneId);
        final int offsetInMiliSecs = timeZone.getRawOffset();

        final long offsetHours = TimeUnit.MILLISECONDS.toHours(offsetInMiliSecs);
        final long offsetMins = TimeUnit.MILLISECONDS.toMinutes(offsetInMiliSecs)
                - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(offsetInMiliSecs));

        final long absOffsetHours = Math.abs(offsetHours);
        final String formattedOffset = String.format("%02d:%02d", Long.valueOf(absOffsetHours),
                Long.valueOf(offsetMins));

        return offsetInMiliSecs < 0 ? "-" + formattedOffset : "+" + formattedOffset;
    }

    @Override
    public String exportValue(final Object obj) throws JaloInvalidParameterException {
        return ObjectUtils.toString(obj);
    }

    protected long getCurrentTimeMillis() {
        return System.currentTimeMillis();
    }

}

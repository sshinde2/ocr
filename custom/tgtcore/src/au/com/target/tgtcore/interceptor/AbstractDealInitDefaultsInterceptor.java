package au.com.target.tgtcore.interceptor;

import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.BuyGetDealModel;
import de.hybris.platform.promotions.model.SpendAndGetDealModel;
import de.hybris.platform.servicelayer.interceptor.InitDefaultsInterceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.springframework.beans.factory.annotation.Required;


/**
 * @author dcwillia
 * 
 */
public abstract class AbstractDealInitDefaultsInterceptor implements InitDefaultsInterceptor {

    private String title;
    private String description;
    private String firedMessage;
    private String couldHaveFiredMessage;
    private String couldHaveMoreRewardsMessage;

    @Override
    public void onInitDefaults(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof AbstractDealModel) {
            final AbstractDealModel dealModel = (AbstractDealModel)model;
            dealModel.setTitle(title);
            dealModel.setDescription(description);
            dealModel.setMessageFired(firedMessage);
            dealModel.setMessageCouldHaveFired(couldHaveFiredMessage);
            if (model instanceof BuyGetDealModel || model instanceof SpendAndGetDealModel) {
                dealModel.setMessageCouldHaveMoreRewards(couldHaveMoreRewardsMessage);
            }

        }
    }

    @Required
    public void setTitle(final String title) {
        this.title = title;
    }

    @Required
    public void setDescription(final String description) {
        this.description = description;
    }

    @Required
    public void setFiredMessage(final String firedMessage) {
        this.firedMessage = firedMessage;
    }

    @Required
    public void setCouldHaveFiredMessage(final String couldHaveFiredMessage) {
        this.couldHaveFiredMessage = couldHaveFiredMessage;
    }

    public void setCouldHaveMoreRewardsMessage(final String couldHaveMoreRewardsMessage) {
        this.couldHaveMoreRewardsMessage = couldHaveMoreRewardsMessage;
    }

}

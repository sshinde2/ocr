package au.com.target.tgtcore.interceptor;

import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.time.TimeService;

import java.util.Date;

import org.apache.log4j.Logger;


/**
 * Prepare interceptor to verify and adjust the end date on deals
 */
public class AbstractDealModelPrepareInterceptor implements PrepareInterceptor {

    private static final Logger LOG = Logger.getLogger(AbstractDealModelPrepareInterceptor.class);

    @Override
    public void onPrepare(final Object model, final InterceptorContext ctx) throws InterceptorException {

        if (!(model instanceof AbstractDealModel)) {
            return;
        }

        final AbstractDealModel dealModel = (AbstractDealModel)model;

        if (ctx.isModified(dealModel, AbstractDealModel.ENDDATE)) {
            final Date now = getTimeService().getCurrentTime();
            final Date endDate = dealModel.getEndDate();
            if (endDate != null && endDate.before(now)) {
                LOG.warn("End date for the deal " + dealModel.getCode()
                        + " is in the past so setting it to current time");
                dealModel.setEndDate(now);
            }
        }
    }

    public TimeService getTimeService() {
        throw new UnsupportedOperationException(
                "Please define a <lookup-bean> for getTimeService() in the spring configuration");
    }
}

/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.springframework.beans.factory.annotation.Required;


/**
 * @author rmcalave
 * 
 */
public class AbstractSimpleDealInitDefaultsInterceptor extends AbstractDealInitDefaultsInterceptor {

    private String pageTitle;
    private String facetTitle;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.interceptor.AbstractDealInitDefaultsInterceptor#onInitDefaults(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onInitDefaults(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof AbstractSimpleDealModel)) {
            return;
        }

        super.onInitDefaults(model, ctx);
        final AbstractSimpleDealModel simpleDealModel = (AbstractSimpleDealModel)model;

        simpleDealModel.setPageTitle(pageTitle);
        simpleDealModel.setFacetTitle(facetTitle);
    }

    @Required
    public void setPageTitle(final String pageTitle) {
        this.pageTitle = pageTitle;
    }

    @Required
    public void setFacetTitle(final String facetTitle) {
        this.facetTitle = facetTitle;
    }
}

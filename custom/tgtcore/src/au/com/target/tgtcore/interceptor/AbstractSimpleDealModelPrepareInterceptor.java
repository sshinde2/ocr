/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import java.util.ArrayList;
import java.util.List;



/**
 * @author dcwillia
 * 
 */
public class AbstractSimpleDealModelPrepareInterceptor implements PrepareInterceptor {
    @Override
    public void onPrepare(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof AbstractSimpleDealModel) {
            final AbstractSimpleDealModel dealModel = (AbstractSimpleDealModel)model;

            final List<CategoryModel> categoryList = new ArrayList<>();
            categoryList.addAll(dealModel.getRewardCategory());

            for (final DealQualifierModel dealQualifierModel : dealModel.getQualifierList()) {
                categoryList.addAll(dealQualifierModel.getDealCategory());
            }

            dealModel.setCategories(categoryList);
        }
    }
}

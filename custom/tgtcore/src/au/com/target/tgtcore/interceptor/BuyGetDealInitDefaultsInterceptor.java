package au.com.target.tgtcore.interceptor;

import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.promotions.model.BuyGetDealModel;
import de.hybris.platform.promotions.model.BuyGetSameListDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import au.com.target.tgtcore.enums.DealRewardTypeEnum;


/**
 * @author dcwillia
 * 
 */
public class BuyGetDealInitDefaultsInterceptor extends AbstractSimpleDealInitDefaultsInterceptor {

    @Override
    public void onInitDefaults(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof BuyGetDealModel || model instanceof BuyGetSameListDealModel) {
            super.onInitDefaults(model, ctx);
            final AbstractSimpleDealModel dealModel = (AbstractSimpleDealModel)model;
            dealModel.setRewardType(DealRewardTypeEnum.DOLLAROFFEACH);
            dealModel.setRewardMinSave(Double.valueOf(0.0d));
            dealModel.setRewardValue(Double.valueOf(1.0d));
        }
    }
}

/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.promotions.model.BuyGetDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.enums.DealRewardTypeEnum;
import au.com.target.tgtcore.model.TargetDealCategoryModel;


/**
 * @author rmcalave
 * 
 */
public class BuyGetDealModelValidateInterceptor implements ValidateInterceptor {

    private List<DealRewardTypeEnum> validDealTypes;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof BuyGetDealModel)) {
            return;
        }

        final BuyGetDealModel buyGetDeal = (BuyGetDealModel)model;

        if (!validDealTypes.contains(buyGetDeal.getRewardType())) {
            throw new InterceptorException(String.format(ErrorMessages.INVALID_DEAL_REWARD_TYPE,
                    ArrayUtils.toString(validDealTypes.toArray())));
        }

        if (CollectionUtils.size(buyGetDeal.getQualifierList()) > 1) {
            throw new InterceptorException(String.format(ErrorMessages.TOO_MANY_QUALIFIERS));
        }

        final List<TargetDealCategoryModel> rewardCategories = buyGetDeal.getRewardCategory();
        if (CollectionUtils.isEmpty(rewardCategories)) {
            throw new InterceptorException(String.format(ErrorMessages.REWARD_CAT_MINIMUM));
        }

        boolean listContainsStagedEntry = false;
        for (final TargetDealCategoryModel rewardCategory : rewardCategories) {
            if (Boolean.FALSE.equals(rewardCategory.getCatalogVersion().getActive())) {
                if (listContainsStagedEntry) {
                    throw new InterceptorException(String.format(ErrorMessages.REWARD_CAT_TOO_MANY_STAGED,
                            Integer.valueOf(1)));
                }

                listContainsStagedEntry = true;
            }
        }
    }

    protected interface ErrorMessages {
        String INVALID_DEAL_REWARD_TYPE = "'Reward Type' must be one of %s";
        String TOO_MANY_QUALIFIERS = "Deal must have no more than 1 qualifier";
        String REWARD_CAT_MINIMUM = "'Reward Category' must contain at least 1 category.";
        String REWARD_CAT_TOO_MANY_STAGED = "'Reward Category' must contain no more than 1 staged deal category.";
    }

    /**
     * @param validDealTypes
     *            the validDealTypes to set
     */
    @Required
    public void setValidDealTypes(final List<DealRewardTypeEnum> validDealTypes) {
        this.validDealTypes = validDealTypes;
    }
}

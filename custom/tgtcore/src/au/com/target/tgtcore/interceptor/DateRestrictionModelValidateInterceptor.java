/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import de.hybris.platform.voucher.model.DateRestrictionModel;
import de.hybris.platform.voucher.model.RestrictionModel;
import de.hybris.platform.voucher.model.VoucherModel;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;


public class DateRestrictionModelValidateInterceptor implements ValidateInterceptor<DateRestrictionModel> {

    @Override
    public void onValidate(final DateRestrictionModel dateRestrictionModel, final InterceptorContext ctx)
            throws InterceptorException {

        if (null != dateRestrictionModel.getVoucher()) {
            final VoucherModel voucherModel = dateRestrictionModel.getVoucher();
            if (isAnotherDateRestrictionPresent(voucherModel.getRestrictions(), dateRestrictionModel)) {
                throw new InterceptorException(
                        ErrorMessages.ERROR_MULTIPLE_DATE_RESTRICTION_VOUCHER);
            }
        }
    }

    protected interface ErrorMessages {
        public static final String ERROR_MULTIPLE_DATE_RESTRICTION_VOUCHER = "Only one Temporal Restriction is allowed for a voucher.";
    }

    private boolean isAnotherDateRestrictionPresent(final Set<RestrictionModel> restrictions,
            final DateRestrictionModel dateRestrictionModel) {

        if (CollectionUtils.isEmpty(restrictions)) {
            return false;
        }
        for (final RestrictionModel restrictionModel : restrictions) {
            if (restrictionModel instanceof DateRestrictionModel && !restrictionModel.equals(dateRestrictionModel)) {
                return true;
            }
        }
        return false;
    }
}

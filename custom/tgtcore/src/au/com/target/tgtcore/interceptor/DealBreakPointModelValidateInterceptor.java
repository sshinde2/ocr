/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.promotions.model.DealBreakPointModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;


/**
 * @author rmcalave
 * 
 */
public class DealBreakPointModelValidateInterceptor implements ValidateInterceptor {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof DealBreakPointModel)) {
            return;
        }

        final DealBreakPointModel dealBreakPoint = (DealBreakPointModel)model;

        if (dealBreakPoint.getQty().intValue() <= 1) {
            throw new InterceptorException(ErrorMessages.QTY_TOO_LOW);
        }

        if (dealBreakPoint.getUnitPrice().doubleValue() <= 0.0) {
            throw new InterceptorException(ErrorMessages.UNIT_PRICE_TOO_LOW);
        }
    }

    protected interface ErrorMessages {
        String QTY_TOO_LOW = "'Quantity' must be greater than 1";
        String UNIT_PRICE_TOO_LOW = "'Unit Price' must be greater than 0";
    }
}

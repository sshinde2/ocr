/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.TargetDealCategoryModel;


/**
 * @author rmcalave
 * 
 */
public class DealQualifierModelValidateInterceptor implements ValidateInterceptor {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof DealQualifierModel)) {
            return;
        }

        final DealQualifierModel dealQualifier = (DealQualifierModel)model;

        final Integer minQty = dealQualifier.getMinQty();
        final Double minAmt = dealQualifier.getMinAmt();

        if (minQty == null && minAmt == null) {
            throw new InterceptorException(ErrorMessages.MIN_QTY_MIN_AMT_REQUIRED);
        }

        int minQtyPrimitive = 0;
        if (minQty != null) {
            minQtyPrimitive = minQty.intValue();
            if (minQtyPrimitive < 0) {
                throw new InterceptorException(ErrorMessages.MIN_QTY_TOO_LOW);
            }
        }

        double minAmtPrimitive = 0;
        if (minAmt != null) {
            minAmtPrimitive = minAmt.doubleValue();
            if (minAmtPrimitive < 0.0) {
                throw new InterceptorException(ErrorMessages.MIN_AMT_TOO_LOW);
            }
        }

        if (minQtyPrimitive == 0 && minAmtPrimitive < 0.009) {
            throw new InterceptorException(ErrorMessages.MIN_AMT_AND_QTY_ZERO);
        }

        final List<TargetDealCategoryModel> dealCategories = dealQualifier.getDealCategory();
        if (CollectionUtils.isEmpty(dealCategories)) {
            throw new InterceptorException(ErrorMessages.DEAL_CAT_MINIMUM);
        }

        boolean listContainsStagedEntry = false;
        for (final TargetDealCategoryModel dealCategory : dealCategories) {
            if (Boolean.FALSE.equals(dealCategory.getCatalogVersion().getActive())) {
                if (listContainsStagedEntry) {
                    throw new InterceptorException(ErrorMessages.DEAL_CAT_TOO_MANY_STAGED);
                }

                listContainsStagedEntry = true;
            }
        }
    }

    protected interface ErrorMessages {
        public String MIN_QTY_MIN_AMT_REQUIRED = "A value must be provided for 'Minimum Quantity' or 'Minimum Amount'.";
        public String MIN_QTY_TOO_LOW = "'Minimum Quantity' must be greater than or equal to 0.";
        public String MIN_AMT_TOO_LOW = "'Minimum Amount' must be greater than or equal to 0.";
        public String MIN_AMT_AND_QTY_ZERO = "'Minimum Amount' and 'Minimum Qty' cannot both be 0";
        public String DEAL_CAT_MINIMUM = "'Deal Category' must contain at least 1 category.";
        public String DEAL_CAT_TOO_MANY_STAGED = "'Deal Category' must contain only 1 staged deal category.";
    }
}

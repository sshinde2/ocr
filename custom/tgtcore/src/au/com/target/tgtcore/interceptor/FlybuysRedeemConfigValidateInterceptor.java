/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import au.com.target.tgtcore.model.FlybuysRedeemConfigModel;


/**
 * @author Rahul
 * 
 */
public class FlybuysRedeemConfigValidateInterceptor implements ValidateInterceptor {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {

        if (model instanceof FlybuysRedeemConfigModel) {
            final FlybuysRedeemConfigModel configModel = (FlybuysRedeemConfigModel)model;
            if (null != configModel.getMinRedeemable()
                    && null != configModel.getMaxRedeemable()
                    && configModel.getMinRedeemable().compareTo(
                            configModel.getMaxRedeemable()) > 0) {
                throw new InterceptorException(
                        "Maximum redeemable amount can't be less than Minimum redeemable amount.");
            }

            if (null != configModel.getMinCartValue()
                    && configModel.getMinCartValue().compareTo(
                            configModel.getMinRedeemable()) < 0) {
                throw new InterceptorException(
                        "Minimum cart value can't be less than Minimum redeemable amount.");
            }
        }
    }
}

/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.constants.TgtCoreConstants.Catalog;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;


public class PrimaryCategoryValidateInterceptor implements ValidateInterceptor {

    /**
     * Validate the primary category must be one of the super categories
     * 
     * @throws InterceptorException
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof TargetProductModel) {
            final TargetProductModel targetProductModel = (TargetProductModel)model;
            final CatalogVersionModel versionModel = targetProductModel.getCatalogVersion();
            if (null != versionModel && !versionModel.getVersion().equals(Catalog.ONLINE_VERSION)) {
                final TargetProductCategoryModel primaryCategory = targetProductModel.getPrimarySuperCategory();

                if (primaryCategory == null) {
                    return;
                }

                final Collection<CategoryModel> superCategories = targetProductModel.getSupercategories();
                if (CollectionUtils.isEmpty(superCategories) || !superCategories.contains(primaryCategory)) {
                    throw new InterceptorException("Primary Category:" + primaryCategory.getCode()
                            + " must be in one of the supercategories of:" + targetProductModel.getCode());
                }
            }
        }
    }

}

/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.promotions.jalo.AbstractDeal;
import de.hybris.platform.promotions.jalo.AbstractPromotion;
import de.hybris.platform.promotions.jalo.PromotionOrderEntryConsumed;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.jalo.TargetPromotionOrderEntryConsumed;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.text.MessageFormat;
import java.util.Collection;


/**
 * @author rsamuel3
 * 
 */
public class PromotionResultValidateInterceptor implements ValidateInterceptor {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof PromotionResult) {
            final PromotionResult promotionResult = (PromotionResult)model;

            final AbstractPromotion promotion = promotionResult.getPromotion();
            if (promotion instanceof AbstractDeal) {
                final Collection<PromotionOrderEntryConsumed> consumedEntries = promotionResult
                        .getConsumedEntries();
                for (final PromotionOrderEntryConsumed consumedEntry : consumedEntries) {
                    if (consumedEntry instanceof TargetPromotionOrderEntryConsumed) {
                        final TargetPromotionOrderEntryConsumed tgtConsumedEntry = (TargetPromotionOrderEntryConsumed)consumedEntry;
                        if (tgtConsumedEntry.getDealItemType() == null) {
                            throw new InterceptorException(
                                    MessageFormat
                                            .format("There is no deal type defined for the promotion {} in the comsumed order Entry for the order {}",
                                                    promotion.getCode(), promotionResult.getOrder().getCode()));
                        }
                    }
                }
            }
        }
    }
}

/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import de.hybris.platform.voucher.model.PromotionVoucherModel;

import java.util.regex.Pattern;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * @author rmcalave
 * 
 */
public class PromotionVoucherModelValidateInterceptor implements ValidateInterceptor {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof PromotionVoucherModel)) {
            return;
        }

        final PromotionVoucherModel promotionVoucher = (PromotionVoucherModel)model;

        final String voucherCode = promotionVoucher.getVoucherCode();
        if (StringUtils.isNotBlank(voucherCode) && !getVoucherValidationPattern().matcher(voucherCode).matches()) {
            throw new InterceptorException(ErrorMessages.VOUCHER_PATTERN_INVALID);
        }
        if (BooleanUtils.isTrue(promotionVoucher.getEnabledOnline()) && StringUtils.isBlank(voucherCode)) {
            throw new InterceptorException(ErrorMessages.VOUCHER_CODE_MANDATORY);
        }
    }

    protected Pattern getVoucherValidationPattern() {
        return TargetValidationCommon.Voucher.PATTERN;
    }

    protected interface ErrorMessages {
        String VOUCHER_PATTERN_INVALID = "'Voucher Code' must only contain upper case letters, numbers and dashes.";
        String VOUCHER_CODE_MANDATORY = "'Voucher Code' must be present for it to be enabled online.";
    }

}

/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.promotions.model.QuantityBreakDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.TargetDealCategoryModel;


/**
 * @author rmcalave
 * 
 */
public class QuantityBreakDealModelValidateInterceptor implements ValidateInterceptor {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof QuantityBreakDealModel)) {
            return;
        }

        final QuantityBreakDealModel quantityBreakDeal = (QuantityBreakDealModel)model;

        final Collection<CategoryModel> categories = quantityBreakDeal.getCategories();
        if (CollectionUtils.isEmpty(categories)) {
            throw new InterceptorException(ErrorMessages.CAT_RANGE);
        }

        boolean listContainsStagedEntry = false;
        for (final CategoryModel category : categories) {
            if (!(category instanceof TargetDealCategoryModel)) {
                throw new InterceptorException(ErrorMessages.CAT_NOT_DEAL_CATEGORY);
            }

            if (Boolean.FALSE.equals(category.getCatalogVersion().getActive())) {
                if (listContainsStagedEntry) {
                    throw new InterceptorException(ErrorMessages.CAT_TOO_MANY_STAGED);
                }

                listContainsStagedEntry = true;
            }
        }

        if (Boolean.TRUE.equals(quantityBreakDeal.getEnabled())) {
            if (CollectionUtils.isEmpty(quantityBreakDeal.getBreakPoints())) {
                throw new InterceptorException(ErrorMessages.NO_BREAKPOINTS);
            }
        }
    }

    protected interface ErrorMessages {
        String CAT_RANGE = "'Categories' must contain 1 category.";
        String CAT_NOT_DEAL_CATEGORY = "'Categories' must contain only instances of TargetDealCategory.";
        String CAT_TOO_MANY_STAGED = "'Categories' must contain only 1 staged deal category.";
        String NO_BREAKPOINTS = "At least one breakpoint must be assigned to this deal if it is enabled.";
    }
}

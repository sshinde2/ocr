/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.model.SalesApplicationConfigModel;


/**
 * @author ragarwa3
 *
 */
public class SalesApplicationConfigValidateInterceptor implements ValidateInterceptor<SalesApplicationConfigModel> {

    static final String INVALID_CNC_STOCK_BUFFER = "Cnc stock buffer must have positive value.";
    static final String INVALID_DELIVERY_MODES = "Only applicable delivery Modes to sales aplication be added for SkipPartnerInventoryUpdateForDelModes";
    static final String NO_ACTIVE_DELIVERY_MODES = "No active delivery modes found for the selected sales application";

    private TargetDeliveryService targetDeliveryService;

    @Override
    public void onValidate(final SalesApplicationConfigModel model, final InterceptorContext context)
            throws InterceptorException {

        if (model.getCncStockBuffer() < 0) {
            throw new InterceptorException(INVALID_CNC_STOCK_BUFFER);
        }

        if (CollectionUtils.isNotEmpty(model.getSkipPartnerInventoryUpdateForDelModes())) {

            final Collection<TargetZoneDeliveryModeModel> currentlyActiveDeliveryModes = targetDeliveryService
                    .getAllCurrentlyActiveDeliveryModes(model.getSalesApplication());

            if (CollectionUtils.isEmpty(currentlyActiveDeliveryModes)) {

                throw new InterceptorException(NO_ACTIVE_DELIVERY_MODES);
            }

            if (!currentlyActiveDeliveryModes.containsAll(model.getSkipPartnerInventoryUpdateForDelModes())) {
                throw new InterceptorException(INVALID_DELIVERY_MODES);
            }

        }

    }

    /**
     * @param targetDeliveryService
     *            the targetDeliveryService to set
     */
    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        this.targetDeliveryService = targetDeliveryService;
    }


}

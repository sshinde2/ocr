package au.com.target.tgtcore.interceptor;

import de.hybris.platform.promotions.model.SpendAndGetDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import au.com.target.tgtcore.enums.DealRewardTypeEnum;


/**
 * @author dcwillia
 * 
 */
public class SpendAndGetDealInitDefaultsInterceptor extends AbstractSimpleDealInitDefaultsInterceptor {

    @Override
    public void onInitDefaults(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof SpendAndGetDealModel) {
            super.onInitDefaults(model, ctx);
            final SpendAndGetDealModel dealModel = (SpendAndGetDealModel)model;
            dealModel.setRewardType(DealRewardTypeEnum.DOLLAROFFEACH);
            dealModel.setRewardMinSave(Double.valueOf(0.0d));
            dealModel.setRewardValue(Double.valueOf(1.0d));
        }
    }
}

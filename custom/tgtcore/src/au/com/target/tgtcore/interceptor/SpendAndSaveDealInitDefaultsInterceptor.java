package au.com.target.tgtcore.interceptor;

import de.hybris.platform.promotions.model.SpendAndSaveDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import au.com.target.tgtcore.enums.DealRewardTypeEnum;


/**
 * @author dcwillia
 * 
 */
public class SpendAndSaveDealInitDefaultsInterceptor extends AbstractSimpleDealInitDefaultsInterceptor {

    @Override
    public void onInitDefaults(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof SpendAndSaveDealModel) {
            super.onInitDefaults(model, ctx);
            final SpendAndSaveDealModel dealModel = (SpendAndSaveDealModel)model;
            dealModel.setRewardType(DealRewardTypeEnum.DOLLAROFFEACH);
            dealModel.setRewardMaxQty(Integer.valueOf(0));
            dealModel.setRewardMinSave(Double.valueOf(0.0d));
            dealModel.setRewardValue(Double.valueOf(1.0d));
        }
    }
}

package au.com.target.tgtcore.interceptor;

import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.servicelayer.interceptor.InitDefaultsInterceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.user.daos.UserGroupDao;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.StoreEmployeeModel;


/**
 * @author dcwillia
 * 
 */
public class StoreEmployeeInitDefaultsInterceptor implements InitDefaultsInterceptor {
    private UserGroupDao userGroupDao;
    private String userGroup;

    @Override
    public void onInitDefaults(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof StoreEmployeeModel) {
            final StoreEmployeeModel storeEmployeeModel = (StoreEmployeeModel)model;
            storeEmployeeModel.setGroups(Collections.singleton(
                    (PrincipalGroupModel)userGroupDao.findUserGroupByUid(userGroup)));
        }
    }

    /**
     * @param userGroupDao
     *            the userGroupDao to set
     */
    @Required
    public void setUserGroupDao(final UserGroupDao userGroupDao) {
        this.userGroupDao = userGroupDao;
    }

    /**
     * @param userGroup
     *            the userGroup to set
     */
    @Required
    public void setUserGroup(final String userGroup) {
        this.userGroup = userGroup;
    }
}

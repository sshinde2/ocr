/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.StoreEmployeeModel;


public class StoreEmployeeValidateInterceptor implements ValidateInterceptor {
    private List<String> validUserGroups;

    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof UserModel) {
            final UserModel userModel = (UserModel)model;
            final Set<PrincipalGroupModel> groups = userModel.getGroups();
            if (model instanceof StoreEmployeeModel) {
                if (groups.size() != 1) { // because a store users  can be only a store employee or store manager and cannot be both
                    throw new InterceptorException("Store Users Can Only be in One Group.");
                }

                final PrincipalGroupModel principalGroupModel = groups.iterator().next();
                if (!validUserGroups.contains(principalGroupModel.getUid())) {
                    throw new InterceptorException("Must only be in store employee user group");
                }

            }
            else {
                for (final PrincipalGroupModel principalGroupModel : groups) {
                    if (validUserGroups.contains(principalGroupModel.getUid())) {
                        throw new InterceptorException("Only store employees can be in the store employee user groups");
                    }
                }
            }
        }
    }

    /**
     * @param validUserGroups
     *            the validUserGroups to set
     */
    @Required
    public void setValidUserGroups(final List<String> validUserGroups) {
        this.validUserGroups = validUserGroups;
    }

}

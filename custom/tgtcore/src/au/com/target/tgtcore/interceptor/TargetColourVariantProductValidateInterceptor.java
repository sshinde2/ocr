/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * @author rmcalave
 * 
 */
public class TargetColourVariantProductValidateInterceptor implements ValidateInterceptor {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof TargetColourVariantProductModel)) {
            return;
        }

        final TargetColourVariantProductModel targetColourVariantProduct = (TargetColourVariantProductModel)model;
        final Date startDate = targetColourVariantProduct.getNewLowerPriceStartDate();
        final Date endDate = targetColourVariantProduct.getNewLowerPriceEndDate();

        // Ensure that a variant can't be attached to a Bulky Board product that already has a variant
        if (Boolean.TRUE.equals(targetColourVariantProduct.getBulkyBoardProduct())) {
            final TargetProductModel baseProduct = (TargetProductModel)targetColourVariantProduct.getBaseProduct();

            final Collection<VariantProductModel> baseProductVariants = baseProduct.getVariants();
            if (CollectionUtils.isNotEmpty(baseProductVariants)) {
                if (baseProductVariants.size() > 1 || !baseProductVariants.contains(targetColourVariantProduct)) {
                    throw new InterceptorException(
                            "A Bulky Board product can only have a single colour variant attached.");
                }
            }
        }
        if (null != startDate) {
            if (startDate.after(endDate) || startDate.equals(endDate)) {
                throw new InterceptorException("Start Date should be before then End date .");
            }
        }
        else if (null != endDate) {
            throw new InterceptorException("Product New lower price start date can't be null.");
        }
    }
}

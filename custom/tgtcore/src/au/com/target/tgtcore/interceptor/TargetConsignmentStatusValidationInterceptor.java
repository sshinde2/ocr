/**
 *
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Hook into interceptor framework to validate ConsignmentStatus.
 *
 * @author jjayawa1
 *
 */
public class TargetConsignmentStatusValidationInterceptor implements ValidateInterceptor {

    private List<ConsignmentStatus> invalidConsignmentStatuses = null;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object obj, final InterceptorContext ctx) throws InterceptorException {
        if (obj instanceof ConsignmentModel) {
            final ConsignmentModel consignment = (ConsignmentModel)obj;
            if (CollectionUtils.isNotEmpty(invalidConsignmentStatuses)
                    && invalidConsignmentStatuses.contains(consignment.getStatus())) {
                throw new InterceptorException(
                        "We don't support consignment statuses " + invalidConsignmentStatuses.toString());
            }
        }
    }

    /**
     * @param invalidConsignmentStatuses
     *            the invalidConsignmentStatuses to set
     */
    @Required
    public void setInvalidConsignmentStatuses(final List<ConsignmentStatus> invalidConsignmentStatuses) {
        this.invalidConsignmentStatuses = invalidConsignmentStatuses;
    }

}

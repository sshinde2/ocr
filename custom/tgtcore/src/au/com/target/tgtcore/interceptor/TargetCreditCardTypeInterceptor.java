/**
 *
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;


/**
 * Hook into interceptor framework to validate CreditCardType.
 *
 * @author jjayawa1
 *
 */
public class TargetCreditCardTypeInterceptor implements ValidateInterceptor {

    private List<CreditCardType> invalidCreditCardTypes = null;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object obj, final InterceptorContext ctx) throws InterceptorException {
        if (obj instanceof CreditCardPaymentInfoModel) {
            final CreditCardPaymentInfoModel ccPaymentInfo = (CreditCardPaymentInfoModel)obj;
            if (CollectionUtils.isNotEmpty(invalidCreditCardTypes)
                    && invalidCreditCardTypes.contains(ccPaymentInfo.getType())) {
                throw new InterceptorException("We don't support " + invalidCreditCardTypes.toString());
            }
        }
        else if (obj instanceof PinPadPaymentInfoModel) {
            final PinPadPaymentInfoModel pinPadPaymentInfo = (PinPadPaymentInfoModel)obj;
            if (CollectionUtils.isNotEmpty(invalidCreditCardTypes)
                    && invalidCreditCardTypes.contains(pinPadPaymentInfo.getCardType())) {
                throw new InterceptorException("We don't support " + invalidCreditCardTypes.toString());
            }
        }
    }

    /**
     * @param invalidCreditCardTypes
     *            the invalidCreditCardTypes to set
     */
    @Required
    public void setInvalidCreditCardTypes(final List<CreditCardType> invalidCreditCardTypes) {
        this.invalidCreditCardTypes = invalidCreditCardTypes;
    }

}

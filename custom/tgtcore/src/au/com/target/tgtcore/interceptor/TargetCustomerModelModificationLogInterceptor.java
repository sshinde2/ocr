package au.com.target.tgtcore.interceptor;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import de.hybris.platform.servicelayer.model.ItemModelContext;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.customer.TargetCustomerModelModificationLogEvent;
import au.com.target.tgtcore.model.TargetCustomerModel;


/**
 * @author bpottass
 *
 */
public class TargetCustomerModelModificationLogInterceptor implements ValidateInterceptor<TargetCustomerModel> {


    private static final Logger LOG = Logger.getLogger(TargetCustomerModelModificationLogInterceptor.class);

    @Autowired
    private EventService eventService;

    private Set<String> attributesToLog;

    /**
     * @param attributesToLog
     *            the attributesToLog to set
     */
    @Required
    public void setAttributesToLog(final Set<String> attributesToLog) {
        this.attributesToLog = attributesToLog;
    }

    /* (non-Javadoc)
    * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
    */
    @Override
    public void onValidate(final TargetCustomerModel targetCustomerModel, final InterceptorContext interceptorContext)
            throws InterceptorException {

        final ItemModelContext itemModelContext = targetCustomerModel.getItemModelContext();
        final Set<String> dirtyAttributes = itemModelContext.getDirtyAttributes();

        if (!dirtyAttributes.isEmpty()) {
            final TargetCustomerModelModificationLogEvent targetCustomerModelModificationLogEvent = createModificationEvent(
                    targetCustomerModel, itemModelContext, dirtyAttributes);
            if (targetCustomerModelModificationLogEvent != null) {
                eventService.publishEvent(targetCustomerModelModificationLogEvent);
            }
        }

    }

    /**
     * @param targetCustomerModel
     * @param itemModelContext
     * @param dirtyAttributes
     */
    private TargetCustomerModelModificationLogEvent createModificationEvent(
            final TargetCustomerModel targetCustomerModel,
            final ItemModelContext itemModelContext, final Set<String> dirtyAttributes) {
        final Map<String, Object> newValueMap = new HashMap();
        final Map<String, Object> previousValueMap = new HashMap();
        for (final String dirtyAttribute : dirtyAttributes) {
            if (itemModelContext.isLoaded(dirtyAttribute) && isAttributeToLog(dirtyAttribute)) {
                try {
                    newValueMap.put(dirtyAttribute, PropertyUtils.getProperty(targetCustomerModel, dirtyAttribute));
                    //can throw IllegalStateException if original value is not loaded yet. 
                    previousValueMap.put(dirtyAttribute,
                            itemModelContext.getOriginalValue(dirtyAttribute));
                }
                catch (final IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {

                    LOG.error("Error retrieving current value from customer model using PropertyUtils " + e);
                }
            }
        }
        if (newValueMap.isEmpty() || previousValueMap.isEmpty()) {

            return null;
        }
        else {
            return new TargetCustomerModelModificationLogEvent(itemModelContext.getPK(), newValueMap, previousValueMap,
                    JaloSession.getCurrentSession().getUser());
        }

    }

    private boolean isAttributeToLog(final String attribute) {
        if (CollectionUtils.isEmpty(attributesToLog)) {
            return false;
        }
        return attributesToLog.contains(attribute);
    }




}

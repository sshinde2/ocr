/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.i18n.L10NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.order.TargetCommerceCheckoutService;
import au.com.target.tgtutility.util.PhoneValidationUtils;
import au.com.target.tgtutility.util.TargetValidationCommon;
import au.com.target.tgtutility.util.TargetValidationUtils;


public class TargetCustomerValidateInterceptor implements ValidateInterceptor {

    private TargetCommerceCheckoutService targetCommerceCheckoutService;

    private L10NService l10NService;

    @Override
    public void onValidate(final Object object, final InterceptorContext ctx) throws InterceptorException {
        if (object instanceof TargetCustomerModel) {
            final TargetCustomerModel model = (TargetCustomerModel)object;
            final String firstname = model.getFirstname();
            final String lastname = model.getLastname();
            final String email = model.getContactEmail();
            final String phonenumber = model.getPhoneNumber();
            final String mobilenumber = model.getMobileNumber();
            final String flybuyscode = model.getFlyBuysCode();

            final Set<String> inValidSet = new HashSet<>();

            if (!TargetValidationUtils.checkOptional(firstname, false)) {
                final String label = getL10NService().getLocalizedString("type.targetcustomer.firstname.name");
                inValidSet.add(label);
            }

            if (!TargetValidationUtils.checkOptional(lastname, false)) {
                inValidSet.add(getL10NService().getLocalizedString("type.targetcustomer.lastname.name"));
            }

            if (!TargetValidationUtils.checkOptional(email, false)) {
                inValidSet.add(getL10NService().getLocalizedString("type.targetcustomer.uid.name"));
            }

            if (!inValidSet.isEmpty()) {
                final String message = inValidSet.toString() + " cannot be blank";
                throw new InterceptorException(message);
            }

            if (!isValidFirstName(firstname)) {
                inValidSet.add(getL10NService().getLocalizedString("type.targetcustomer.firstname.name"));
            }

            if (!isValidLastName(lastname)) {
                inValidSet.add(getL10NService().getLocalizedString("type.targetcustomer.lastname.name"));
            }

            if (!isValidEmail(email)) {
                inValidSet.add(getL10NService().getLocalizedString("type.targetcustomer.uid.name"));
            }

            if (!isValidPhoneNumber(phonenumber)) {
                inValidSet.add(getL10NService().getLocalizedString("type.targetcustomer.phonenumber.name"));
            }

            if (!isValidMobileNumber(mobilenumber)) {
                inValidSet.add(getL10NService().getLocalizedString("type.targetcustomer.mobilenumber.name"));
            }

            if (!isValidFlybuysCode(flybuyscode)) {
                inValidSet.add(getL10NService().getLocalizedString("type.targetcustomer.flybuyscode.name"));
            }

            if (!inValidSet.isEmpty()) {
                final String message = inValidSet.toString() + " must be valid";
                throw new InterceptorException(message);
            }
        }
    }

    /**
     * @param flybuyscode
     *            the fly buys code
     * @return boolean
     */
    private boolean isValidFlybuysCode(final String flybuyscode) {

        return targetCommerceCheckoutService.isFlybuysNumberValid(flybuyscode);
    }

    /**
     * @param mobilenumber
     *            mobile phone number
     * @return boolean
     */
    private boolean isValidMobileNumber(final String mobilenumber) {

        return TargetValidationUtils.matchRegularExpression(mobilenumber, TargetValidationCommon.MobilePhone.PATTERN,
                TargetValidationCommon.MobilePhone.MIN_SIZE, TargetValidationCommon.MobilePhone.MAX_SIZE, true);
    }

    /**
     * @param phonenumber
     *            phone number
     * @return boolean
     */
    private boolean isValidPhoneNumber(final String phonenumber) {

        final String preparedPhonenumber = PhoneValidationUtils.preparePhoneNumber(phonenumber);

        return TargetValidationUtils.matchRegularExpression(preparedPhonenumber, TargetValidationCommon.Phone.PATTERN,
                TargetValidationCommon.Phone.MIN_SIZE, TargetValidationCommon.Phone.MAX_SIZE, true);
    }

    /**
     * @param email
     *            email
     * @return boolean
     */
    private boolean isValidEmail(final String email) {

        return matchRegularExpression(email, TargetValidationCommon.Email.PATTERN, false);
    }

    /**
     * @param lastname
     *            last name
     * @return boolean
     */
    private boolean isValidLastName(final String lastname) {

        return TargetValidationUtils.matchRegularExpression(lastname, TargetValidationCommon.LastName.PATTERN,
                TargetValidationCommon.Name.MIN_SIZE, TargetValidationCommon.Name.MAX_SIZE, false);
    }

    /**
     * 
     * @param firstname
     *            first name
     * @return boolean
     */
    private boolean isValidFirstName(final String firstname) {

        return TargetValidationUtils.matchRegularExpression(firstname, TargetValidationCommon.FirstName.PATTERN,
                TargetValidationCommon.Name.MIN_SIZE, TargetValidationCommon.Name.MAX_SIZE, false);
    }

    /**
     * 
     * @param value
     *            string for validation
     * @param pattern
     *            pattern for checking
     * @param isOptional
     *            optional value
     * @return boolean
     */
    public static boolean matchRegularExpression(final String value, final Pattern pattern, final boolean isOptional) {

        if (StringUtils.isEmpty(value)) {
            return isOptional;
        }
        return pattern.matcher(value).matches();
    }



    /**
     * 
     * @param targetCommerceCheckoutService
     *            set targetCommerceCheckoutService
     */
    @Required
    public void setTargetCommerceCheckoutService(final TargetCommerceCheckoutService targetCommerceCheckoutService) {
        this.targetCommerceCheckoutService = targetCommerceCheckoutService;
    }

    /**
     * @return the targetCommerceCheckoutService
     */
    public TargetCommerceCheckoutService getTargetCommerceCheckoutService() {
        return targetCommerceCheckoutService;
    }

    /**
     * @return the l10NService
     */
    public L10NService getL10NService() {
        return l10NService;
    }

    /**
     * @param l10nService
     *            the l10NService to set
     */
    @Autowired
    public void setL10NService(final L10NService l10nService) {
        l10NService = l10nService;
    }
}

/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants.Catalog;
import au.com.target.tgtcore.model.TargetDealCategoryModel;


/**
 * @author rmcalave
 * 
 */
public class TargetDealCategoryModelValidateInterceptor implements ValidateInterceptor {

    private String customerGroupUid;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof TargetDealCategoryModel)) {
            return;
        }

        final TargetDealCategoryModel targetDealCategory = (TargetDealCategoryModel)model;
        final CatalogVersionModel catalogVersion = targetDealCategory.getCatalogVersion();

        if (catalogVersion == null) {
            return;
        }

        final String catalogVersionName = catalogVersion.getVersion();

        if (StringUtils.isNotEmpty(catalogVersionName)
                && catalogVersionName.equals(Catalog.OFFLINE_VERSION)) {
            if (CollectionUtils.isNotEmpty(targetDealCategory.getSupercategories())) {
                throw new InterceptorException(ErrorMessages.NO_SUPERCATEGORIES);
            }

            if (CollectionUtils.isNotEmpty(targetDealCategory.getCategories())) {
                throw new InterceptorException(ErrorMessages.NO_SUBORDINATE_CATEGORIES);
            }

            boolean isVisibleToCustomerGroup = false;
            final List<PrincipalModel> allowedPrincipals = targetDealCategory.getAllowedPrincipals();
            for (final PrincipalModel allowedPrincipal : allowedPrincipals) {
                if (customerGroupUid.equals(allowedPrincipal.getUid())) {
                    isVisibleToCustomerGroup = true;
                    break;
                }
            }

            if (!isVisibleToCustomerGroup) {
                throw new InterceptorException(ErrorMessages.CUSTOMER_GROUP_REQUIRED);
            }
        }
    }

    protected interface ErrorMessages {
        public static final String NO_SUPERCATEGORIES = "A Target Deal Category cannot have any supercategories.";
        public static final String NO_SUBORDINATE_CATEGORIES = "A Target Deal Category cannot have any subordinate categories.";
        public static final String CUSTOMER_GROUP_REQUIRED = "A Target Deal Category must be visible to customergroup.";
    }

    /**
     * @param customerGroupUid
     *            the customerGroupUid to set
     */
    @Required
    public void setCustomerGroupUid(final String customerGroupUid) {
        this.customerGroupUid = customerGroupUid;
    }
}

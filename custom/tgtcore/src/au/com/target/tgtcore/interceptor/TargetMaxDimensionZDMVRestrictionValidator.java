/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.lang.ObjectUtils;

import au.com.target.tgtcore.model.TargetMaxDimensionZDMVRestrictionModel;


/**
 * @author pratik
 *
 */
public class TargetMaxDimensionZDMVRestrictionValidator implements
        ValidateInterceptor<TargetMaxDimensionZDMVRestrictionModel> {

    /**
     * Method to validate max dimension. Max dimension configured cannot be negative.
     */
    @Override
    public void onValidate(final TargetMaxDimensionZDMVRestrictionModel model, final InterceptorContext ctx)
            throws InterceptorException {
        if (ObjectUtils.compare(model.getMaxDimension(), Double.valueOf(0.0)) < 0) {
            throw new InterceptorException("Max dimension can't be negative.");
        }
    }
}

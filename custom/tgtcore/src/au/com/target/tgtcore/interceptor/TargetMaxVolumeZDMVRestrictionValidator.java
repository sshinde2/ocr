/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.lang.ObjectUtils;

import au.com.target.tgtcore.model.TargetMaxVolumeZDMVRestrictionModel;


/**
 * @author pratik
 *
 */
public class TargetMaxVolumeZDMVRestrictionValidator implements
        ValidateInterceptor<TargetMaxVolumeZDMVRestrictionModel> {

    /**
     * Method to validate the max volume configured in the restriction, it should always be a positive number
     */
    @Override
    public void onValidate(final TargetMaxVolumeZDMVRestrictionModel model, final InterceptorContext ctx)
            throws InterceptorException {
        if (ObjectUtils.compare(model.getMaxVolume(), Double.valueOf(0.0)) < 0) {
            throw new InterceptorException("Max volume can't be negative.");
        }
    }
}

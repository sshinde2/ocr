package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtutility.util.StringTools;


/**
 * Prepare Interceptor to strip off the leading zeroes from the attribute 'code'
 */
public class TargetMerchDepartmentPrepareInterceptor implements PrepareInterceptor {

    private static final String ZERO = "0";

    @Override
    public void onPrepare(final Object model, final InterceptorContext ctx) throws InterceptorException {

        if (!(model instanceof TargetMerchDepartmentModel)) {
            return;
        }

        final TargetMerchDepartmentModel merchDepartmentModel = (TargetMerchDepartmentModel)model;
        final String merchDepartmentNumber = merchDepartmentModel.getCode();

        if (StringUtils.isNotBlank(merchDepartmentNumber) && merchDepartmentNumber.startsWith(ZERO)) {
            merchDepartmentModel.setCode(StringTools.trimLeadingZeros(merchDepartmentNumber));
        }

    }
}

package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import au.com.target.tgtcore.model.TargetMerchDepartmentModel;


/**
 * Validate Interceptor which validates the code attribute
 */
public class TargetMerchDepartmentValidateInterceptor implements ValidateInterceptor {

    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof TargetMerchDepartmentModel)) {
            return;
        }

        final TargetMerchDepartmentModel merchDepartmentModel = (TargetMerchDepartmentModel)model;
        final String departmentNumberStr = merchDepartmentModel.getCode();

        if (StringUtils.isBlank(departmentNumberStr)) {
            throw new InterceptorException(ErrorMessages.CODE_NULL_EMPTY);
        }

        if (!NumberUtils.isDigits(departmentNumberStr)) {
            throw new InterceptorException(ErrorMessages.CODE_NOT_NUMBER);
        }

        if (departmentNumberStr.length() > 5) {
            throw new InterceptorException(ErrorMessages.CODE_TOO_LONG);
        }

        final int departmentNumber = Integer.parseInt(departmentNumberStr);

        if (departmentNumber <= 0) {
            throw new InterceptorException(ErrorMessages.CODE_ZERO_NEGATIVE);
        }
    }

    protected interface ErrorMessages {
        String CODE_NULL_EMPTY = "Code cannot be null or empty";
        String CODE_NOT_NUMBER = "Code must contain only positive numeric digits";
        String CODE_TOO_LONG = "Code cannot be more than 5 digits";
        String CODE_ZERO_NEGATIVE = "Code cannot be 0 or a negative number";
    }
}

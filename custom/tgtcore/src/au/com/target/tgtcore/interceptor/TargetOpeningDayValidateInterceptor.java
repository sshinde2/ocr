/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import de.hybris.platform.storelocator.model.OpeningDayModel;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import au.com.target.tgtcore.model.TargetOpeningDayModel;


public class TargetOpeningDayValidateInterceptor implements ValidateInterceptor {
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof TargetOpeningDayModel)) {
            throw new IllegalArgumentException("Expect only TargetOpeningDay model");
        }

        final TargetOpeningDayModel openingDayModel = (TargetOpeningDayModel)model;
        if (StringUtils.isBlank(openingDayModel.getTargetOpeningDayId())) {
            throw new InterceptorException("TargetopeningDayId cannot be null or Empty");
        }
        final Date open = openingDayModel.getOpeningTime();
        if (open == null) {
            throw new InterceptorException("Opening Time cannot be null");
        }
        final Date close = openingDayModel.getClosingTime();
        if (close == null) {
            throw new InterceptorException("Closing Time cannot be null");
        }

        if (!DateUtils.isSameDay(open, close)) {
            throw new InterceptorException("Opening and Closing Times must be the same day");
        }

        final OpeningScheduleModel schedule = openingDayModel.getOpeningSchedule();
        if (schedule == null) {
            throw new InterceptorException("Opening Schedule cannot be null");
        }

        final Collection<OpeningDayModel> openingDays = schedule.getOpeningDays();
        for (final OpeningDayModel otherDay : openingDays) {
            if (otherDay == openingDayModel) {
                // don't compare with myself
                continue;
            }
            if (DateUtils.isSameDay(open, otherDay.getOpeningTime())
                    || DateUtils.isSameDay(open, otherDay.getClosingTime())) {
                throw new InterceptorException("Only one Opening Time permitted per day:" + open.toString());
            }
        }
    }
}

/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.text.MessageFormat;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.TargetOrderRestrictionModel;


/**
 * @author rmcalave
 * 
 */
public class TargetOrderRestrictionValidateInterceptor implements ValidateInterceptor {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof TargetOrderRestrictionModel)) {
            return;
        }

        final TargetOrderRestrictionModel targetOrderRestriction = (TargetOrderRestrictionModel)model;

        final boolean hasIncludedCategories = CollectionUtils
                .isNotEmpty(targetOrderRestriction.getIncludedCategories());
        final boolean hasIncludedProducts = CollectionUtils.isNotEmpty(targetOrderRestriction.getIncludedProducts());
        final boolean hasExcludedCategories = CollectionUtils
                .isNotEmpty(targetOrderRestriction.getExcludedCategories());
        final boolean hasExcludedProducts = CollectionUtils.isNotEmpty(targetOrderRestriction.getExcludedProducts());

        if (!hasIncludedCategories && !hasIncludedProducts && !hasExcludedCategories && !hasExcludedProducts) {
            throw new InterceptorException(Messages.NO_PRODUCTS);
        }

        if (hasIncludedProducts) {
            if (!areAllProductsOnline(targetOrderRestriction.getIncludedProducts())) {
                throw new InterceptorException(MessageFormat.format(Messages.STAGED_ITEMS, Messages.INCLUDED_PRODUCTS));
            }
        }

        if (hasIncludedCategories) {
            if (!areAllCategoriesOnline(targetOrderRestriction.getIncludedCategories())) {
                throw new InterceptorException(
                        MessageFormat.format(Messages.STAGED_ITEMS, Messages.INCLUDED_CATEGORIES));
            }
        }

        if (hasExcludedProducts) {
            if (!areAllProductsOnline(targetOrderRestriction.getExcludedProducts())) {
                throw new InterceptorException(MessageFormat.format(Messages.STAGED_ITEMS, Messages.EXCLUDED_PRODUCTS));
            }
        }

        if (hasExcludedCategories) {
            if (!areAllCategoriesOnline(targetOrderRestriction.getExcludedCategories())) {
                throw new InterceptorException(
                        MessageFormat.format(Messages.STAGED_ITEMS, Messages.EXCLUDED_CATEGORIES));
            }
        }

        if (hasIncludedCategories && hasExcludedCategories) {
            if (CollectionUtils.containsAny(targetOrderRestriction.getIncludedCategories(),
                    targetOrderRestriction.getExcludedCategories())) {
                throw new InterceptorException(Messages.CATEGORY_INCLUDED_AND_EXCLUDED);
            }
        }

        if (hasIncludedProducts && hasExcludedProducts) {
            if (CollectionUtils.containsAny(targetOrderRestriction.getIncludedProducts(),
                    targetOrderRestriction.getExcludedProducts())) {
                throw new InterceptorException(Messages.PRODUCT_INCLUDED_AND_EXCLUDED);
            }
        }

        if (hasIncludedProducts) {
            final Collection<ProductModel> includedProducts = targetOrderRestriction.getIncludedProducts();
            for (final ProductModel includedProduct : includedProducts) {
                final Collection<CategoryModel> includedProductCategories = includedProduct.getSupercategories();
                if (CollectionUtils.containsAny(targetOrderRestriction.getExcludedCategories(),
                        includedProductCategories)) {
                    throw new InterceptorException(Messages.PRODUCT_EXCLUDED_BY_CATEGORY);
                }
            }
        }
    }

    private boolean areAllCategoriesOnline(final Collection<CategoryModel> categories) {
        for (final CategoryModel category : categories) {
            if (!Boolean.TRUE.equals(category.getCatalogVersion().getActive())) {
                return false;
            }
        }

        return true;
    }

    private boolean areAllProductsOnline(final Collection<ProductModel> products) {
        for (final ProductModel product : products) {
            if (!Boolean.TRUE.equals(product.getCatalogVersion().getActive())) {
                return false;
            }
        }

        return true;
    }

    protected interface Messages {
        String NO_PRODUCTS = "You must add at least one product or category for inclusion or exclusion.";
        String STAGED_ITEMS = "One or more {0} are not from the Online catalog.";
        String CATEGORY_INCLUDED_AND_EXCLUDED = "An included category is also excluded.";
        String PRODUCT_INCLUDED_AND_EXCLUDED = "An included product is also excluded.";
        String PRODUCT_EXCLUDED_BY_CATEGORY = "An included product would be excluded by an Excluded Category";

        String INCLUDED_PRODUCTS = "Included Products";
        String INCLUDED_CATEGORIES = "Included Categories";
        String EXCLUDED_PRODUCTS = "Excluded Products";
        String EXCLUDED_CATEGORIES = "Excluded Categories";
    }
}

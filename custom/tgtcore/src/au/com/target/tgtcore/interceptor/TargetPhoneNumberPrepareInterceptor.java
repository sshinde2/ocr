/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import au.com.target.tgtutility.util.PhoneValidationUtils;
import au.com.target.tgtcore.model.TargetAddressModel;

/**
 * Translates phone numbers in {@link TargetAddressModel}
 */
public class TargetPhoneNumberPrepareInterceptor implements PrepareInterceptor {

    /**
     * {@inheritDoc}
     */
    @Override
    public void onPrepare(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof TargetAddressModel) {
            final TargetAddressModel address = (TargetAddressModel)model;
            address.setPhone1(PhoneValidationUtils.preparePhoneNumber(address.getPhone1()));
            address.setPhone2(PhoneValidationUtils.preparePhoneNumber(address.getPhone2()));
        }
    }
}

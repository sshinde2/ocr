/**
 * 
 */
package au.com.target.tgtcore.interceptor;


import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtutility.util.TargetValidationCommon;
import au.com.target.tgtutility.util.TargetValidationCommon.Phone;



/**
 * Validates phone numbers in {@link TargetAddressModel}
 */

public class TargetPhoneNumberValidateInterceptor implements ValidateInterceptor {

    private static final String LENGTH_ERROR_MESSAGE = "Phone Number must be between %d and %d digits";

    /**
     * {@inheritDoc}
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {

        if (model instanceof TargetAddressModel) {
            final TargetAddressModel address = (TargetAddressModel)model;

            if (StringUtils.isNotBlank(address.getPhone1())) {
                validatePhoneNumber(address.getPhone1());
            }

            if (StringUtils.isNotBlank(address.getPhone2())) {
                validatePhoneNumber(address.getPhone2());
            }
        }
    }

    /**
     * Validates given phone numbers with next rules: <br>
     * - Phone Number must be between Math.min({@link Phone}.MIN_SIZE, {@link Phone}.MIN_SIZE_MIGRATED) and
     * {@link Phone}.MAX_SIZE digits and in a valid phone number format. <br>
     * 
     * @param phone
     *            given phone to translate
     */
    private void validatePhoneNumber(final String phone) {

        final int minSize = Math.min(Phone.MIN_SIZE, Phone.MIN_SIZE_MIGRATED);

        Assert.isTrue(phone.length() >= minSize && phone.length() <= Phone.MAX_SIZE,
                String.format(LENGTH_ERROR_MESSAGE, Integer.valueOf(minSize), Integer.valueOf(Phone.MAX_SIZE)));

        Assert.isTrue(Pattern.matches(TargetValidationCommon.Phone.VALID_REGEX.toString(), phone),
                String.format(LENGTH_ERROR_MESSAGE, Integer.valueOf(minSize), Integer.valueOf(Phone.MAX_SIZE)));
    }
}

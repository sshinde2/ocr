/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;


public class TargetPointOfServiceValidateInterceptor implements ValidateInterceptor {
    private static final int MAX_DIGITS = 4;
    private static final int MAX_STORE_NUM = (int)Math.pow(10, MAX_DIGITS) - 1;

    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof TargetPointOfServiceModel) {
            final TargetPointOfServiceModel pos = (TargetPointOfServiceModel)model;
            final Integer storeNumObj = pos.getStoreNumber();
            if (storeNumObj != null) {
                final int storeNum = storeNumObj.intValue();
                if (storeNum < 0) {
                    throw new InterceptorException("Store number cannot be negative", this);
                }
                else if (storeNum > MAX_STORE_NUM) {
                    throw new InterceptorException("Store number cannot have more than " + MAX_DIGITS + " digits",
                            this);
                }
            }
        }
    }
}

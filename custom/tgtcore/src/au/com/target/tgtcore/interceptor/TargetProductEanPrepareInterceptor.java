/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import au.com.target.tgtutility.util.BarcodeTools;


/**
 * Prepare interceptor to verify and adjust the Target Product's EAN
 *
 */
public class TargetProductEanPrepareInterceptor implements PrepareInterceptor<ProductModel> {

    @Override
    public void onPrepare(final ProductModel product, final InterceptorContext ctx) throws InterceptorException {
        if (null != product) {
            final String ean = product.getEan();
            product.setEan(BarcodeTools.getCleansedEan(ean));
        }
    }
}

/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import au.com.target.tgtutility.util.BarcodeTools;


/**
 * @author Vivek
 *
 */
public class TargetProductEanValidateInterceptor implements ValidateInterceptor<ProductModel> {

    @Override
    public void onValidate(final ProductModel product, final InterceptorContext ctx) throws InterceptorException {

        if (null != product) {
            final String ean = product.getEan();
            if (!BarcodeTools.isValidEan(ean)) {
                throw new InterceptorException(
                        "The ean entered is invalid, must be numeric and must pass ean checksum.");
            }
        }
    }
}

/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.TargetProductModel;


/**
 * Interceptor for validation on base TargetProduct
 * 
 */
public class TargetProductValidateInterceptor implements ValidateInterceptor {

    // Set via spring config
    private String mandatoryHomeDeliveryCode;
    private String clickAndCollectCode;

    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof TargetProductModel) {

            final TargetProductModel product = (TargetProductModel)model;

            // If the product is of type MHD then prevent the CNC delivery mode being set
            if (product.getProductType() != null
                    && mandatoryHomeDeliveryCode.equals(product.getProductType().getCode())) {

                final Set<DeliveryModeModel> deliveryModes = product.getDeliveryModes();
                if (CollectionUtils.isNotEmpty(deliveryModes)) {

                    for (final DeliveryModeModel deliveryMode : deliveryModes) {
                        if (clickAndCollectCode.equals(deliveryMode.getCode())) {
                            throw new InterceptorException(
                                    "Target base product with type MHD cannot have the CNC delivery mode: "
                                            + product.getCode());
                        }
                    }
                }
            }

            // If the product is a Bulky Board product, ensure it only has a single variant
            if (Boolean.TRUE.equals(product.getBulkyBoardProduct())) {
                if (CollectionUtils.isNotEmpty(product.getVariants())) {
                    if (product.getVariants().size() > 1) {
                        throw new InterceptorException("A Bulky Board product can only have a single variant attached.");
                    }

                    final VariantProductModel variant = product.getVariants().iterator().next();
                    if (CollectionUtils.isNotEmpty(variant.getVariants())) {
                        throw new InterceptorException(
                                "A variant attached to a Bulky Board product must not have variants of its own.");
                    }
                }
            }
        }
    }

    /**
     * @param mandatoryHomeDeliveryCode
     *            the mandatoryHomeDeliveryCode to set
     */
    public void setMandatoryHomeDeliveryCode(final String mandatoryHomeDeliveryCode) {
        this.mandatoryHomeDeliveryCode = mandatoryHomeDeliveryCode;
    }

    /**
     * @param clickAndCollectCode
     *            the clickAndCollectCode to set
     */
    public void setClickAndCollectCode(final String clickAndCollectCode) {
        this.clickAndCollectCode = clickAndCollectCode;
    }



}

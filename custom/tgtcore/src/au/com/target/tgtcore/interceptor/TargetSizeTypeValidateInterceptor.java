/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtcore.product.TargetSizeTypeService;


/**
 * The Class TargetSizeTypeValidateInterceptor.
 */
public class TargetSizeTypeValidateInterceptor implements ValidateInterceptor {

    /** The size type service. */
    private TargetSizeTypeService sizeTypeService;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {

        if (!(model instanceof SizeTypeModel)) {
            return;
        }

        final SizeTypeModel sizeTypeModel = (SizeTypeModel)model;
        if (BooleanUtils.isTrue(sizeTypeModel.getIsDefault())) {
            SizeTypeModel currentDefaultSizeType;
            try {
                currentDefaultSizeType = sizeTypeService.getDefaultSizeType();
            }
            catch (final UnknownIdentifierException ex) {
                currentDefaultSizeType = null;
            }

            if (currentDefaultSizeType != null && !currentDefaultSizeType.equals(sizeTypeModel))
            {
                throw new InterceptorException("You can not create more than one Default Target Size type.");
            }
        }

    }

    /**
     * Gets the size type service.
     * 
     * @return the sizeTypeService
     */
    public TargetSizeTypeService getSizeTypeService() {
        return sizeTypeService;
    }

    /**
     * Sets the size type service.
     * 
     * @param sizeTypeService
     *            the sizeTypeService to set
     */
    @Required
    public void setSizeTypeService(final TargetSizeTypeService sizeTypeService) {
        this.sizeTypeService = sizeTypeService;
    }

}
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;

import org.apache.log4j.Logger;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;
import au.com.target.tgtutility.util.StackTraceFormatter;


/**
 * Added for Logging the product variants which are getting removed along with trace to identify the action which causes
 * the removal
 * 
 */
public class TargetVariantProductRemoveInterceptor implements RemoveInterceptor
{

    private static final Logger LOG = Logger.getLogger(TargetVariantProductValidateInterceptor.class);

    @Override
    public void onRemove(final Object model, final InterceptorContext ctx) throws InterceptorException
    {
        if (model instanceof AbstractTargetVariantProductModel)
        {
            final AbstractTargetVariantProductModel variantProduct = (AbstractTargetVariantProductModel)model;

            LOG.error(SplunkLogFormatter.formatMessage(
                    "INTERCEPTOR-- > Removing Product (pk, code): (" + variantProduct.getPk() + ","
                            + variantProduct.getCode()
                            + ") -->",
                    TgtutilityConstants.ErrorCode.ERR_REMOVE_PRODUCT
                    , StackTraceFormatter.stackTraceToString(Thread.currentThread().getStackTrace())));

        }

    }

}

/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.order.DeliveryModeService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Required;


/**
 * Interceptor to validate the promotion rank supplied to the delivery mode, it should be a unique non-zero positive
 * number.
 * 
 * @author Pratik
 *
 */
public class TargetZoneDeliveryModeValidateInterceptor implements ValidateInterceptor {

    private DeliveryModeService deliveryModeService;

    /**
     * Method to validate the promotion rank supplied to the delivery mode
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof TargetZoneDeliveryModeModel) {
            final TargetZoneDeliveryModeModel targetZoneDeliveryModeModel = (TargetZoneDeliveryModeModel)model;
            if (targetZoneDeliveryModeModel.getDeliveryPromotionRank().intValue() <= 0) {
                throw new InterceptorException("Delivery promotion rank cannot be less than or equal to 0");
            }
            final Set<Integer> promotionRanks = getAllPromotionRanks(targetZoneDeliveryModeModel.getCode());
            if (promotionRanks.contains(targetZoneDeliveryModeModel.getDeliveryPromotionRank())) {
                throw new InterceptorException("Delivery promotion rank is not unique");
            }
        }
    }

    /**
     * Method to get promotion ranks for all delivery modes except the one being saved.
     * 
     * @return set of promotion ranks
     */
    private Set<Integer> getAllPromotionRanks(final String code) {
        final Collection<DeliveryModeModel> deliveryModes = deliveryModeService.getAllDeliveryModes();
        final Set<Integer> promotionRanks = new HashSet<>();
        for (final DeliveryModeModel deliveryMode : safe(deliveryModes)) {
            if (deliveryMode instanceof TargetZoneDeliveryModeModel && !deliveryMode.getCode().equals(code)) {
                promotionRanks.add(((TargetZoneDeliveryModeModel)deliveryMode).getDeliveryPromotionRank());
            }
        }
        return promotionRanks;
    }

    /**
     * Checks if the list is null, if it is then returns a empty list otherwise the delivery modes
     * 
     * @param deliveryModes
     * @return deliveryModes
     */
    private Collection<DeliveryModeModel> safe(final Collection<DeliveryModeModel> deliveryModes) {
        return deliveryModes == null ? Collections.EMPTY_LIST : deliveryModes;
    }

    /**
     * @param deliveryModeService
     *            the deliveryModeService to set
     */
    @Required
    public void setDeliveryModeService(final DeliveryModeService deliveryModeService) {
        this.deliveryModeService = deliveryModeService;
    }

}

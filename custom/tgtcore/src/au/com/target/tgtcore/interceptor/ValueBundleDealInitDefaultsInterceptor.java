package au.com.target.tgtcore.interceptor;

import de.hybris.platform.promotions.model.ValueBundleDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import au.com.target.tgtcore.enums.DealRewardTypeEnum;


/**
 * @author dcwillia
 * 
 */
public class ValueBundleDealInitDefaultsInterceptor extends AbstractSimpleDealInitDefaultsInterceptor {

    @Override
    public void onInitDefaults(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof ValueBundleDealModel) {
            super.onInitDefaults(model, ctx);
            final ValueBundleDealModel dealModel = (ValueBundleDealModel)model;
            dealModel.setRewardType(DealRewardTypeEnum.FIXEDDOLLARTOTAL);
        }
    }

}

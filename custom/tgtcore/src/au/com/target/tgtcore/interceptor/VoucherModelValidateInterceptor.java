/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import de.hybris.platform.voucher.model.VoucherModel;

import java.util.regex.Pattern;


/**
 * @author rmcalave
 * 
 */
public class VoucherModelValidateInterceptor implements ValidateInterceptor {

    private static final String PROMOTION_CODE_PATTERN_STRING = "[A-Z\\d]*";
    private static final Pattern PROMOTION_CODE_PATTERN = Pattern.compile(PROMOTION_CODE_PATTERN_STRING);


    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof VoucherModel)) {
            return;
        }

        final VoucherModel voucher = (VoucherModel)model;

        if (!PROMOTION_CODE_PATTERN.matcher(voucher.getCode()).matches()) {
            throw new InterceptorException(ErrorMessages.PROMOTION_PATTERN_INVALID);
        }
    }

    protected interface ErrorMessages {
        String PROMOTION_PATTERN_INVALID = "'Promotion Code' must only contain upper case letters and numbers.";
    }
}

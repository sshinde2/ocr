package au.com.target.tgtcore.jalo;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.user.User;


public class FlybuysDiscount extends GeneratedFlybuysDiscount
{

    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException
    {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }

    /*
     * (non-Javadoc)
     * @see de.hybris.platform.voucher.jalo.Voucher#checkVoucherCode(java.lang.String)
     */
    @Override
    public boolean checkVoucherCode(final String aVoucherCode)
    {
        return aVoucherCode.equals(getCode());
    }

    /*
     * (non-Javadoc)
     * @see de.hybris.platform.voucher.jalo.Voucher#getNextVoucherNumber(de.hybris.platform.jalo.SessionContext)
     */
    @Override
    protected int getNextVoucherNumber(final SessionContext ctx)
    {
        return 1;
    }

    /*
     * (non-Javadoc)
     * @see de.hybris.platform.voucher.jalo.Voucher#isReservable(java.lang.String, de.hybris.platform.jalo.user.User)
     */
    @Override
    public boolean isReservable(final String aVoucherCode, final User user)
    {
        return getInvalidations(aVoucherCode, user).size() < 1 && getInvalidations(aVoucherCode).size() < 1;
    }
}

package au.com.target.tgtcore.jalo;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.jalo.PromotionsManager;
import de.hybris.platform.promotions.result.PromotionEvaluationContext;
import de.hybris.platform.promotions.result.PromotionOrderEntry;
import de.hybris.platform.promotions.result.PromotionOrderView;
import de.hybris.platform.promotions.util.Helper;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.model.TMDiscountProductPromotionModel;
import au.com.target.tgtcore.order.TargetDiscountService;


/**
 * The Class TMDiscountProductPromotion.
 */
@SuppressWarnings("deprecation")
public class TMDiscountProductPromotion extends GeneratedTMDiscountProductPromotion
{
    /** The model service. */
    private final ModelService modelService = (ModelService)Registry.getApplicationContext().getBean(
            "modelService");

    /** The target discount service. */
    private final TargetDiscountService targetDiscountService = (TargetDiscountService)Registry.getApplicationContext()
            .getBean(
                    "targetDiscountService");

    /* (non-Javadoc)
     * @see de.hybris.platform.promotions.jalo.AbstractPromotion#evaluate(de.hybris.platform.jalo.SessionContext, de.hybris.platform.promotions.result.PromotionEvaluationContext)
     */
    @Override
    public List<PromotionResult> evaluate(final SessionContext ctx,
            final PromotionEvaluationContext promoContext) {
        final AbstractOrderModel order = modelService.get(promoContext.getOrder());
        modelService.refresh(order);

        final TMDiscountProductPromotionModel tmDiscountProductPromotionModel = modelService.get(this);

        if (StringUtils.isNotEmpty(order.getTmdCardNumber())
                && targetDiscountService.isValidCardType(order.getTmdCardNumber(),
                        tmDiscountProductPromotionModel)) {

            final List results = new ArrayList();
            final PromotionsManager.RestrictionSetResult rsr = findEligibleProductsInBasket(ctx, promoContext);

            if (rsr.isAllowedToContinue() && !rsr.getAllowedProducts().isEmpty()) {
                PromotionResult result = null;

                final PromotionOrderView view = promoContext.createView(ctx, this, rsr.getAllowedProducts());
                for (final PromotionOrderEntry entry : view.getAllEntries(ctx)) {

                    final AbstractOrderEntryModel entryModel = modelService.get(entry.getBaseOrderEntry());
                    modelService.refresh(entryModel);
                    final long quantityToDiscount = entryModel.getQuantity().longValue();
                    final double discountRate = getPercentageDiscount(ctx).doubleValue() / 100D;
                    final double totalEntryPrice = entryModel.getTotalPrice().doubleValue();
                    final Currency currency = promoContext.getOrder().getCurrency(ctx);
                    final BigDecimal adjustedEntryPrice = Helper.roundCurrencyValue(ctx, currency,
                            totalEntryPrice
                                    - totalEntryPrice * discountRate);

                    result = PromotionsManager.getInstance()
                            .createPromotionResult(ctx, this, promoContext.getOrder(), 1.0F);
                    final BigDecimal adjustment = Helper.roundCurrencyValue(ctx, currency,
                            adjustedEntryPrice.subtract(BigDecimal.valueOf(totalEntryPrice)));
                    result.addAction(ctx, PromotionsManager.getInstance()
                            .createPromotionOrderEntryAdjustAction(ctx, entry.getBaseOrderEntry(),
                                    quantityToDiscount,
                                    adjustment.doubleValue()));
                    result.setCustom(entryModel.getEntryNumber().toString());
                    results.add(result);
                }
            }
            return results;
        }
        return Collections.EMPTY_LIST;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.promotions.jalo.AbstractPromotion#getResultDescription(de.hybris.platform.jalo.SessionContext, de.hybris.platform.promotions.jalo.PromotionResult, java.util.Locale)
     */
    @Override
    public String getResultDescription(final SessionContext ctx,
            final PromotionResult promotionResult,
            final Locale locale) {
        final AbstractOrder order = promotionResult.getOrder(ctx);
        if (order != null) {
            final Currency orderCurrency = order.getCurrency(ctx);
            if (promotionResult.getFired(ctx)) {
                final Double totalDiscount = Double.valueOf(promotionResult.getTotalDiscount(ctx));
                final Double percentageDiscount = getPercentageDiscount(ctx);
                final Object args[] = { percentageDiscount, totalDiscount,
                        Helper.formatCurrencyAmount(ctx, locale, orderCurrency, totalDiscount.doubleValue()) };
                return formatMessage(getMessageFired(ctx), args, locale);
            }
        }
        return "";
    }

}

package au.com.target.tgtcore.jalo;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.jalo.PromotionsManager;
import de.hybris.platform.promotions.result.PromotionEvaluationContext;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;


@SuppressWarnings("deprecation")
public class TMDiscountPromotion extends GeneratedTMDiscountPromotion {

    private final ModelService modelService = (ModelService)Registry.getApplicationContext().getBean(
            "modelService");

    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type,
            final ItemAttributeMap allAttributes)
            throws JaloBusinessException
    {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.promotions.jalo.AbstractPromotion#evaluate(de.hybris.platform.jalo.SessionContext, de.hybris.platform.promotions.result.PromotionEvaluationContext)
     */
    @Override
    public List<PromotionResult> evaluate(final SessionContext ctx, final PromotionEvaluationContext promoContext) {

        final AbstractOrderModel order = modelService.get(promoContext.getOrder());
        if (StringUtils.isNotEmpty(order.getTmdCardNumber())) {
            return super.evaluate(ctx, promoContext);
        }
        return Collections.EMPTY_LIST;

    }

    /* (non-Javadoc)
     * @see de.hybris.platform.promotions.jalo.AbstractPromotion#getResultDescription(de.hybris.platform.jalo.SessionContext, de.hybris.platform.promotions.jalo.PromotionResult, java.util.Locale)
     */
    @Override
    public String getResultDescription(final SessionContext ctx, final PromotionResult promotionResult,
            final Locale locale) {
        return super.getResultDescription(ctx, promotionResult, locale);
    }

    @Override
    protected PromotionsManager.RestrictionSetResult findEligibleProductsInBasket(final SessionContext ctx,
            final PromotionEvaluationContext promoContext)
    {
        final Collection<Product> products = PromotionsManager.getBaseProductsForOrder(ctx, promoContext.getOrder());

        if (!products.isEmpty())
        {

            // Find the set of all products which are in the basket and can be considered by this promotion
            // Currently all products will be eligible
            final List<Product> cartProducts = new ArrayList<>();
            cartProducts.addAll(products);

            // Run restrictions if appropriate
            if (promoContext.getObserveRestrictions())
            {
                return PromotionsManager.getInstance().evaluateRestrictions(ctx, cartProducts, promoContext.getOrder(),
                        this,
                        promoContext.getDate());
            }
            else
            {
                return new PromotionsManager.RestrictionSetResult(cartProducts);
            }
        }
        else
        {
            // By default return proceed with an empty set
            return new PromotionsManager.RestrictionSetResult(new ArrayList<Product>(0));
        }
    }


}

package au.com.target.tgtcore.jalo;

import de.hybris.platform.category.jalo.Category;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.util.RestrictionUtils;


@SuppressWarnings("deprecation")
public class TargetAllProductCategoryZDMVRestriction extends GeneratedTargetAllProductCategoryZDMVRestriction {

    private final ModelService modelService = (ModelService)Registry.getApplicationContext().getBean("modelService");


    /**
     * Evaluates to true if all products in the cart/order belongs to configured products/categories in the restriction,
     * otherwise false.
     * 
     * @param abstractOrder
     *            the abstract order
     * @return boolean
     */
    @Override
    public boolean evaluate(final AbstractOrderModel abstractOrder) {
        final Collection<Product> includedProducts = getIncludedProducts();
        final Collection<Category> includedCategories = getIncludedCategories();
        final ProductType productType = getProductType();

        if (CollectionUtils.isEmpty(includedProducts) && CollectionUtils.isEmpty(includedCategories)
                && null == productType) {
            return true;
        }

        final List<AbstractOrderEntryModel> orderEntries = abstractOrder.getEntries();
        for (final AbstractOrderEntryModel orderEntry : orderEntries) {
            final Product orderEntryProduct = modelService.getSource(orderEntry.getProduct());

            if (orderEntry.getQuantity().longValue() == 0) {
                continue;
            }

            if (CollectionUtils.isNotEmpty(includedProducts)
                    && RestrictionUtils.containsProduct(includedProducts, orderEntryProduct)) {
                continue;
            }
            else if (CollectionUtils.isNotEmpty(includedCategories)
                    && RestrictionUtils.containsProductCategory(orderEntryProduct, includedCategories)) {
                continue;
            }
            else if (null != productType && productType.equals(RestrictionUtils.getProductType(orderEntryProduct))) {
                continue;
            }
            return false;
        }
        return true;
    }


    @Override
    public boolean evaluate(final Product product) {
        final Collection<Product> includedProducts = getIncludedProducts();
        final Collection<Category> includedCategories = getIncludedCategories();
        final ProductType productType = getProductType();

        if (CollectionUtils.isEmpty(includedProducts) && CollectionUtils.isEmpty(includedCategories)
                && null == productType) {
            return true;
        }

        if (CollectionUtils.isNotEmpty(includedProducts)
                && RestrictionUtils.containsProduct(includedProducts, product)) {
            return true;
        }
        else if (CollectionUtils.isNotEmpty(includedCategories)
                && RestrictionUtils.containsProductCategory(product, includedCategories)) {
            return true;
        }
        else if (null != productType && productType.equals(RestrictionUtils.getProductType(product))) {
            return true;
        }

        return false;
    }

}

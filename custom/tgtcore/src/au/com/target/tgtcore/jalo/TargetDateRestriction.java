package au.com.target.tgtcore.jalo;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.Cart;
import de.hybris.platform.jalo.type.ComposedType;

import java.util.Date;


public class TargetDateRestriction extends GeneratedTargetDateRestriction {
    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }

    @Override
    protected boolean isFulfilledInternal(final AbstractOrder anOrder) {
        if (anOrder instanceof Cart) {
            return super.isFulfilledInternal(anOrder);
        }
        else {
            final boolean start;
            final boolean end;
            final Date currentDate = anOrder.getDate();
            final Date startDate = getStartDate();
            final Date endDate = getEndDate();
            start = (startDate == null) || (startDate.before(currentDate) == isPositiveAsPrimitive());
            end = (endDate == null) || (endDate.after(currentDate) == isPositiveAsPrimitive());
            return start && end;
        }
    }
}

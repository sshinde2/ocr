package au.com.target.tgtcore.jalo;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.math.NumberUtils;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtcore.util.ProductUtil;


@SuppressWarnings("deprecation")
public class TargetMaxDimensionZDMVRestriction extends GeneratedTargetMaxDimensionZDMVRestriction {

    private final ModelService modelService = (ModelService)Registry.getApplicationContext().getBean("modelService");

    /**
     * Evaluates to check if all the products in cart have dimension below the configured dimension limit.
     */
    @Override
    public boolean evaluate(final AbstractOrderModel abstractOrder) {
        final Double maxDimension = getMaxDimensionFromAbstractOrder(abstractOrder);
        if (maxDimension != null && maxDimension.doubleValue() <= getMaxDimensionAsPrimitive()) {
            return true;
        }
        return false;
    }

    /**
     * Evaluates to check if product dimensions are below the configured dimension limit.
     */
    @Override
    public boolean evaluate(final Product product) {
        final ProductModel currentProductModel = getModelService().get(product);
        return evaluate(currentProductModel);
    }

    /**
     * Method to return max dimension from cart
     * 
     * @param abstractOrder
     * @return maxDimension from various products, null if data is missing or invalid
     */
    private Double getMaxDimensionFromAbstractOrder(final AbstractOrderModel abstractOrder) {
        if (null != abstractOrder && CollectionUtils.isNotEmpty(abstractOrder.getEntries())) {
            final List<Double> maxDimensions = new ArrayList<>();
            for (final AbstractOrderEntryModel orderEntry : abstractOrder.getEntries()) {
                final String productCode = ProductUtil.getProductTypeCode(orderEntry.getProduct());
                if (ProductType.DIGITAL.equals(productCode)) {
                    continue;
                }
                final TargetProductDimensionsModel productPackageDimensions = getProductPackageDimensionsModel(
                        orderEntry.getProduct());

                if (doesProductHaveValidDimensions(productPackageDimensions)) {
                    maxDimensions.add(Double.valueOf(getLargestDimensionValue(productPackageDimensions)));
                }
                else {
                    return null;
                }
            }
            if (CollectionUtils.isNotEmpty(maxDimensions)) {
                return Collections.max(maxDimensions);
            }
        }
        return null;
    }

    /**
     * Method to evaluate if product model and its related variants.
     * 
     * @param currentProductModel
     * @return boolean
     */
    private boolean evaluate(final ProductModel currentProductModel) {
        final Collection<VariantProductModel> productVariants = currentProductModel.getVariants();
        if (CollectionUtils.isEmpty(productVariants)) {
            return evaluateProductDimensions(currentProductModel);
        }
        for (final ProductModel variant : currentProductModel.getVariants()) {
            if (!evaluate(variant)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Method to evaluate product dimensions, if dimensions are negative or missing then dimension is invalid and
     * evaluation fails
     * 
     * @param variant
     * @return boolean
     */
    private boolean evaluateProductDimensions(final ProductModel variant) {
        final TargetProductDimensionsModel productPackageDimensions = getProductPackageDimensionsModel(variant);
        if (!doesProductHaveValidDimensions(productPackageDimensions)) {
            return false;
        }
        return getLargestDimensionValue(productPackageDimensions) <= getMaxDimensionAsPrimitive();
    }

    /**
     * Method to fetch TargetProductDimensionsModel
     * 
     * @param product
     * @return TargetProductDimensionsModel
     */
    private TargetProductDimensionsModel getProductPackageDimensionsModel(final ProductModel product) {
        return ((AbstractTargetVariantProductModel)product).getProductPackageDimensions();
    }

    /**
     * Method to return largest dimension value for the product
     * 
     * @param productPackageDimensions
     * @return largestDimensionValue
     */
    private double getLargestDimensionValue(final TargetProductDimensionsModel productPackageDimensions) {
        return NumberUtils.max(productPackageDimensions.getLength().doubleValue(), productPackageDimensions
                .getWidth().doubleValue(), productPackageDimensions.getHeight().doubleValue());
    }

    /**
     * Method to check if product has valid dimension, a valid dimension is a positive non zero value.
     * 
     * @param productPackageDimensions
     * @return boolean
     */
    private boolean doesProductHaveValidDimensions(final TargetProductDimensionsModel productPackageDimensions) {
        if (productPackageDimensions != null && isDimensionValid(productPackageDimensions.getLength())
                && isDimensionValid(productPackageDimensions.getWidth())
                && isDimensionValid(productPackageDimensions.getHeight())) {
            return true;
        }
        return false;
    }

    /**
     * Method to check if the dimension is valid, valid is a positive number
     * 
     * @param dimension
     * @return boolean
     */
    private boolean isDimensionValid(final Double dimension) {
        if (ObjectUtils.compare(dimension, Double.valueOf(0.0)) < 0) {
            return false;
        }
        return true;
    }

    protected ModelService getModelService() {
        return this.modelService;
    }
}

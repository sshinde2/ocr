package au.com.target.tgtcore.jalo;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.math.BigDecimal;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtcore.util.ProductUtil;


@SuppressWarnings("deprecation")
public class TargetMaxTotalDeadWeightZDMVRestriction extends GeneratedTargetMaxTotalDeadWeightZDMVRestriction {

    private final ModelService modelService = (ModelService)Registry.getApplicationContext().getBean("modelService");

    /* 
     * Evaluates that all products weight is less then the max weight
     */
    @Override
    public boolean evaluate(final AbstractOrderModel abstractOrder) {
        final Double totalCartWeight = getTotalCartWeight(abstractOrder);

        return checkIfWeightDoesNotExceed(totalCartWeight);
    }

    /* 
     * Evaluates that product weight is less then the max weight
     */
    @Override
    public boolean evaluate(final Product product) {
        final ProductModel currentProductModel = getModelService().get(product);
        return evaluate(currentProductModel);

    }


    private boolean evaluate(final ProductModel currentProductModel) {
        final Collection<VariantProductModel> productVariants = currentProductModel.getVariants();
        if (CollectionUtils.isEmpty(productVariants)) {
            return evaluateProductWeight(currentProductModel);
        }
        for (final ProductModel variant : currentProductModel.getVariants()) {
            if (!evaluate(variant)) {
                return false;
            }
        }
        return true;
    }

    private boolean evaluateProductWeight(final ProductModel variant) {
        final TargetProductDimensionsModel productPackageDimensions = getProductPackageDimensionsModel(variant);
        if (doesDimensionHaveWeight(productPackageDimensions)) {
            return checkIfWeightDoesNotExceed(productPackageDimensions.getWeight());
        }
        return false;
    }

    private TargetProductDimensionsModel getProductPackageDimensionsModel(final ProductModel product) {
        if (product instanceof AbstractTargetVariantProductModel) {
            return ((AbstractTargetVariantProductModel)product).getProductPackageDimensions();
        }
        return null;
    }

    /**
     * Returns total cart weight
     * 
     * @param abstractOrder
     * @return Double
     */
    private Double getTotalCartWeight(final AbstractOrderModel abstractOrder) {
        BigDecimal totalCartWeight = BigDecimal.ZERO;
        if (CollectionUtils.isNotEmpty(abstractOrder.getEntries())) {
            for (final AbstractOrderEntryModel orderEntry : abstractOrder.getEntries()) {
                final String productCode = ProductUtil.getProductTypeCode(orderEntry.getProduct());
                if (ProductType.DIGITAL.equals(productCode)) {
                    continue;
                }
                final TargetProductDimensionsModel productPackageDimensions = getProductPackageDimensionsModel(
                        orderEntry
                                .getProduct());

                if (doesDimensionHaveWeight(productPackageDimensions)) {
                    totalCartWeight = totalCartWeight.add(BigDecimal.valueOf(
                            productPackageDimensions.getWeight().doubleValue()).multiply(
                                    BigDecimal.valueOf(orderEntry.getQuantity().doubleValue())));
                }
                else {
                    return null;
                }
            }
        }
        return Double.valueOf(totalCartWeight.doubleValue());
    }

    private boolean doesDimensionHaveWeight(final TargetProductDimensionsModel productPackageDimensions) {
        return null != productPackageDimensions
                && null != productPackageDimensions.getWeight()
                && productPackageDimensions.getWeight().doubleValue() >= 0;
    }

    private boolean checkIfWeightDoesNotExceed(final Double weight) {
        final Double maxTotalDeadWeight = getMaxTotalDeadWeight();
        if (weight == null || weight.compareTo(maxTotalDeadWeight) > 0) {
            return false;
        }
        return true;
    }


    public ModelService getModelService() {
        return modelService;
    }
}
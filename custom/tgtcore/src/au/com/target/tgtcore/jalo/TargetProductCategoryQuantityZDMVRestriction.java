/*
 * 
 */
package au.com.target.tgtcore.jalo;

import de.hybris.platform.category.jalo.Category;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.util.RestrictionUtils;


/**
 * The Class TargetProductCategoryQuantityZDMVRestriction.
 */
@SuppressWarnings("deprecation")
public class TargetProductCategoryQuantityZDMVRestriction
        extends GeneratedTargetProductCategoryQuantityZDMVRestriction {

    private final ModelService modelService = (ModelService)Registry.getApplicationContext().getBean("modelService");

    /**
     * Evaluates to true if any product in the cart/order belongs to configured products/categories in the restriction
     * and also has the quantity up-to configured quantity in the restriction, otherwise false.
     * 
     * @param abstractOrder
     *            the abstract order
     * @return boolean
     */
    @Override
    public boolean evaluate(final AbstractOrderModel abstractOrder) {
        final Collection<Category> includedCategories = getIncludedCategories();
        final Collection<Product> includedProducts = getIncludedProducts();

        if ((CollectionUtils.isEmpty(includedProducts) && CollectionUtils.isEmpty(includedCategories))
                || null == getQuantity()) {
            return true;
        }

        final List<AbstractOrderEntryModel> orderEntries = abstractOrder.getEntries();

        int qtyCnt = 0;
        for (final AbstractOrderEntryModel orderEntry : orderEntries) {
            final Product orderEntryProduct = modelService.getSource(orderEntry.getProduct());

            if (CollectionUtils.isNotEmpty(includedProducts)) {
                if (RestrictionUtils.containsProduct(includedProducts, orderEntryProduct)) {
                    qtyCnt += orderEntry.getQuantity().intValue();
                    continue; // don't count it twice if it is both product list and category list
                }
            }

            if (CollectionUtils.isNotEmpty(includedCategories)) {
                if (RestrictionUtils.containsProductCategory(orderEntryProduct, includedCategories)) {
                    qtyCnt += orderEntry.getQuantity().intValue();
                }
            }
        }

        return qtyCnt > 0 && qtyCnt <= getQuantity().intValue();
    }

    /**
     * Evaluates to true if the given product belongs to the list of products/categories configured in the restriction.
     * 
     * @param product
     *            the product
     * @return boolean
     */
    @Override
    public boolean evaluate(final Product product) {
        final Integer qty = getQuantity();
        if (qty != null && qty.intValue() < 1) {
            return false;
        }

        final Collection<Product> includedProducts = getIncludedProducts();
        final Collection<Category> includedCategories = getIncludedCategories();
        if (CollectionUtils.isEmpty(includedProducts) && CollectionUtils.isEmpty(includedCategories)) {
            return true;
        }
        if (CollectionUtils.isNotEmpty(includedProducts)
                && RestrictionUtils.containsProduct(includedProducts, product)) {
            return true;
        }
        else if (CollectionUtils.isNotEmpty(includedCategories)
                && RestrictionUtils.containsProductCategory(product, includedCategories)) {
            return true;
        }

        return false;
    }

}

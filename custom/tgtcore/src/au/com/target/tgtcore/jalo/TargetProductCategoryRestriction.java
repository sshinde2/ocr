package au.com.target.tgtcore.jalo;

import de.hybris.platform.category.jalo.Category;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.voucher.jalo.util.VoucherEntry;
import de.hybris.platform.voucher.jalo.util.VoucherEntrySet;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import au.com.target.tgtcore.util.RestrictionUtils;


@SuppressWarnings("deprecation")
public class TargetProductCategoryRestriction extends GeneratedTargetProductCategoryRestriction {

    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }

    /**
     * Returns voucher Entry set by iterating over the products in the order to check whether any product satisfy the
     * Target Product Category Restriction.
     * 
     * @param anOrder
     * @return entries
     * @see de.hybris.platform.voucher.jalo.ProductRestriction#isFulfilledInternal(Product)
     */
    @Override
    public VoucherEntrySet getApplicableEntries(final AbstractOrder anOrder) {
        final Set<Product> fulfilledProducts = new HashSet<>();
        final Set<Product> unfulfilledProducts = new HashSet<>();
        final Collection<Category> restrictCategories = this.getCategories();
        final VoucherEntrySet entries = new VoucherEntrySet();
        for (final AbstractOrderEntry entry : anOrder.getEntries()) {
            final Product product = entry.getProduct();
            if (fulfilledProducts.contains(product)) {
                entries.add(new VoucherEntry(entry, entry.getQuantity().longValue(), entry.getUnit()));
                continue;
            }
            if (unfulfilledProducts.contains(product)) {
                continue;
            }
            final boolean fulfilledProduct = isFulfilled(product, restrictCategories);
            if (fulfilledProduct) {
                entries.add(new VoucherEntry(entry, entry.getQuantity().longValue(), entry.getUnit()));
                fulfilledProducts.add(product);
            }
            else {
                unfulfilledProducts.add(product);
            }
        }
        return entries;
    }

    /**
     * Returns <tt>true</tt> if the specified product fulfills this restriction. More formally, returns <tt>true</tt> if
     * the set of categories defined by this restriction has the specified product
     * 
     * @param product
     * @param restrictCategories
     * @return <tt>true</tt> if the specified product fulfills this restriction, <tt>false</tt> else.
     * @see de.hybris.platform.voucher.jalo.ProductRestriction#isFulfilledInternal(Product)
     */
    private boolean isFulfilled(final Product product, final Collection restrictCategories) {
        final boolean contained = RestrictionUtils.containsProductCategory(product, restrictCategories);
        return contained == isPositiveAsPrimitive();
    }
}

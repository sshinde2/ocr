package au.com.target.tgtcore.jalo;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.type.ComposedType;

import java.util.Collection;

import au.com.target.tgtcore.util.RestrictionUtils;


@SuppressWarnings("deprecation")
public class TargetProductRestriction extends GeneratedTargetProductRestriction {
    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }

    /**
     * Returns <tt>true</tt> if the specified product fulfills this restriction. More formally, returns <tt>true</tt> if
     * the set of products defined by this restriction contains the specified product, such that
     * <tt>(getProducts().contains(aProduct)==isPositive().booleanValue())</tt>.
     * 
     * @param aProduct
     *            the product to check whether it fulfills this restriction.
     * @return <tt>true</tt> if the specified product fulfills this restriction, <tt>false</tt> else.
     * @see de.hybris.platform.voucher.jalo.ProductRestriction#isFulfilledInternal(Product)
     */
    @Override
    protected boolean isFulfilledInternal(final Product aProduct) {
        final Collection<Product> products = super.getProducts();
        return RestrictionUtils.containsProduct(products, aProduct) == super.isPositiveAsPrimitive();
    }

}

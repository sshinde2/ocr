package au.com.target.tgtcore.jalo;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import au.com.target.tgtcore.util.RestrictionUtils;


/**
 * The Class TargetProductTypeQuantityZDMVRestriction.
 */
@SuppressWarnings("deprecation")
public class TargetProductTypeQuantityZDMVRestriction extends GeneratedTargetProductTypeQuantityZDMVRestriction {

    private final ModelService modelService = (ModelService)Registry.getApplicationContext().getBean("modelService");

    /**
     * Evaluates based on Product Type and Quantity.
     * 
     * @param order
     *            the order
     * @return true, if successful
     */
    @Override
    public boolean evaluate(final AbstractOrderModel order) {
        final Integer min = getMinQuantity();
        final Integer max = getMaxQuantity();
        long orderQty = 0;
        if ((min == null) && (max == null)) {
            return true;
        }
        final List<AbstractOrderEntryModel> orderEntries = order.getEntries();
        for (final AbstractOrderEntryModel orderEntry : orderEntries) {
            final ProductType entryProductType = RestrictionUtils
                    .getProductType((Product)modelService.getSource(orderEntry.getProduct()));
            if (entryProductType != null && getProductType().contains(entryProductType)) {
                orderQty += orderEntry.getQuantity().longValue();
            }
        }
        return isValidQuantity(min, max, Long.valueOf(orderQty));
    }

    /**
     * Checks if is valid quantity.
     * 
     * @param min
     *            the min
     * @param max
     *            the max
     * @param prodQty
     *            the prod qty
     * @return true, if is valid quantity
     */
    private boolean isValidQuantity(final Integer min, final Integer max, final Long prodQty) {
        if (prodQty.equals(Long.valueOf(0))) {
            return false;
        }

        if ((min == null) && (max != null) && (max.compareTo(Integer.valueOf(prodQty.intValue())) >= 0)) {
            return true;
        }
        if ((min != null) && (max == null) && (min.compareTo(Integer.valueOf(prodQty.intValue())) <= 0)) {
            return true;
        }
        if ((min != null) && (max != null) &&
                (min.compareTo(Integer.valueOf(prodQty.intValue())) <= 0) &&
                (max.compareTo(Integer.valueOf(prodQty.intValue())) >= 0)) {
            return true;
        }
        return false;
    }



    @Override
    public boolean evaluate(final Product product) {
        final Integer min = getMinQuantity();
        if (min != null && min.intValue() > 1) {
            return false;
        }

        final ProductType productType = RestrictionUtils.getProductType(product);
        return getProductType().contains(productType);
    }

}

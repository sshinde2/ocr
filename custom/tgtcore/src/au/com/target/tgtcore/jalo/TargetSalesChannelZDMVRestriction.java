package au.com.target.tgtcore.jalo;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;

import au.com.target.tgtcore.model.TargetSalesChannelZDMVRestrictionModel;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;


/**
 * The Class TargetSalesChannelZDMVRestriction.
 */
@SuppressWarnings("deprecation")
public class TargetSalesChannelZDMVRestriction extends GeneratedTargetSalesChannelZDMVRestriction {

    /** The model service. */
    private final ModelService modelService = (ModelService)Registry.getApplicationContext().getBean("modelService");

    /** The target sales application service. */
    private final TargetSalesApplicationService targetSalesApplicationService = (TargetSalesApplicationService)Registry
            .getApplicationContext().getBean("targetSalesApplicationService");

    /**
     * Evaluates to true if the cart/order's sales application match the restriction one.
     * 
     * @param abstractOrder
     *            the abstract order
     * @return true, if successful
     */
    @Override
    public boolean evaluate(final AbstractOrderModel abstractOrder) {

        final TargetSalesChannelZDMVRestrictionModel restrictionModel = modelService.get(this);
        final Collection<SalesApplication> salesApps = restrictionModel.getSalesApplication();

        if (abstractOrder instanceof CartModel
                && salesApps.contains(targetSalesApplicationService.getCurrentSalesApplication())) {
            return true;
        }

        if (abstractOrder instanceof OrderModel) {
            final OrderModel orderModel = (OrderModel)abstractOrder;
            if (null != orderModel.getSalesApplication()
                    && salesApps.contains(orderModel.getSalesApplication())) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean evaluate(final Product product) {
        final SalesApplication currentSalesApp = targetSalesApplicationService.getCurrentSalesApplication();
        if (currentSalesApp == null) {
            return true;
        }

        final TargetSalesChannelZDMVRestrictionModel restrictionModel = modelService.get(this);
        final Collection<SalesApplication> salesApps = restrictionModel.getSalesApplication();

        return salesApps.contains(targetSalesApplicationService.getCurrentSalesApplication());
    }

}

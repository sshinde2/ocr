/**
 * 
 */
package au.com.target.tgtcore.jalo;

import de.hybris.platform.core.GenericCondition;
import de.hybris.platform.core.GenericQuery;
import de.hybris.platform.jalo.order.Cart;
import de.hybris.platform.jalo.order.Order;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.voucher.jalo.Voucher;
import de.hybris.platform.voucher.jalo.VoucherInvalidation;
import de.hybris.platform.voucher.jalo.VoucherManager;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;


/**
 * @author rmcalave
 * 
 */
public class TargetVoucherManager extends VoucherManager {

    private TargetVoucherService targetVoucherService;

    /**
     * In addition to getting the first PromotionVoucher or a SerialVoucher for the given code, gets the first
     * FlybuysDiscount for the given code if there is no PromotionVoucher or SerialVoucher for that code.
     * 
     * @see de.hybris.platform.voucher.jalo.VoucherManager#getVoucher(java.lang.String)
     */
    @Override
    public Voucher getVoucher(final String voucherCode) {
        final Voucher voucher = super.getVoucher(voucherCode);

        if (voucher != null) {
            return voucher;
        }

        final Collection<Voucher> result = getFlybuysDiscounts(voucherCode);
        return result.isEmpty() ? null : result.iterator().next();
    }

    /**
     * Get all flybuys Discounts for the given code
     * 
     * @param code
     *            the discount code
     * @return a Collection of <code>FlybuysDiscount</code>s
     */
    @SuppressWarnings("deprecation")
    public Collection getFlybuysDiscounts(final String code)
    {
        return getSession().search(
                new GenericQuery(TgtCoreConstants.TC.FLYBUYSDISCOUNT, GenericCondition.equals(FlybuysDiscount.CODE,
                        code))).getResult();
    }


    @Override
    public boolean redeemVoucher(final String aVoucherCode, final Cart aCart)
            throws JaloPriceFactoryException
    {
        if (targetVoucherService.isVoucherEnabledOnline(aVoucherCode)) {
            return super.redeemVoucher(aVoucherCode, aCart);
        }
        return false;
    }


    @Override
    public VoucherInvalidation redeemVoucher(final String aVoucherCode, final Order anOrder)
    {
        if (targetVoucherService.isVoucherEnabledOnline(aVoucherCode)) {
            return super.redeemVoucher(aVoucherCode, anOrder);
        }
        return null;
    }

    /**
     * @param targetVoucherService
     *            the targetVoucherService to set
     */
    @Required
    public void setTargetVoucherService(final TargetVoucherService targetVoucherService) {
        this.targetVoucherService = targetVoucherService;
    }


}

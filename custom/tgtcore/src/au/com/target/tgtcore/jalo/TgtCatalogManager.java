/**
 * 
 */
package au.com.target.tgtcore.jalo;

import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.jalo.CatalogVersion;
import de.hybris.platform.catalog.jalo.SyncItemJob;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.flexiblesearch.FlexibleSearch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Preconditions;


/**
 * @author rsamuel3
 *
 */
@SuppressWarnings("deprecation")
public class TgtCatalogManager extends CatalogManager {
    @Override
    public SyncItemJob getSyncJob(final CatalogVersion source, final CatalogVersion target, final String qualifier)
    {
        Preconditions.checkArgument(source != null);
        Preconditions.checkArgument(target != null);
        Preconditions.checkArgument(!source.equals(target));
        final Map params = new HashMap();
        params.put("src", source);
        params.put("tgt", target);
        params.put("active", Boolean.TRUE);
        if (qualifier != null) {
            params.put("code", qualifier);
        }
        final StringBuilder query = new StringBuilder("SELECT {");
        query.append(Item.PK).append("} ").append("FROM {")
                .append(de.hybris.platform.catalog.constants.GeneratedCatalogConstants.TC.SYNCITEMJOB)
                .append("} ").append("WHERE {").append("sourceVersion").append("}=?src AND ").append("{")
                .append("targetVersion").append("}=?tgt ").append(qualifier != null ? " AND {code}=?code" : "")
                .append(" AND {active}=?active").append(" order by {priority} DESC");
        final List jobs = FlexibleSearch.getInstance().search(query.toString(), params, SyncItemJob.class).getResult();
        return jobs.isEmpty() ? null : (SyncItemJob)jobs.get(0);
    }
}

/**
 * 
 */
package au.com.target.tgtcore.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtpayment.resumer.PaymentResumerService;


public class ResendInterruptedOrdersJob extends AbstractJobPerformable<CronJobModel> {

    private PaymentResumerService paymentResumerService;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.CronJobModel)
     */
    @Override
    public PerformResult perform(final CronJobModel cronJob) {

        paymentResumerService.resumeProcessesForInterruptedPayments();

        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    /**
     * @param paymentResumerService
     *            the paymentResumerService to set
     */
    @Required
    public void setPaymentResumerService(final PaymentResumerService paymentResumerService) {
        this.paymentResumerService = paymentResumerService;
    }


}

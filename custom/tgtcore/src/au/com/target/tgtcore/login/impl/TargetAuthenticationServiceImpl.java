/**
 * 
 */
package au.com.target.tgtcore.login.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.security.auth.impl.DefaultAuthenticationService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.login.TargetAuthenticationService;
import au.com.target.tgtcore.model.TargetCustomerModel;


public class TargetAuthenticationServiceImpl extends DefaultAuthenticationService implements
        TargetAuthenticationService {

    private static final Logger LOG = Logger.getLogger(TargetAuthenticationServiceImpl.class);

    private Integer failedLoginAttemptsAllowed;

    @Override
    public boolean checkAndIncrementLoginAttempts(final CustomerModel customer) {
        if (!(customer instanceof TargetCustomerModel)) {
            throw new IllegalArgumentException("Expect only Target Customer Model");
        }
        final TargetCustomerModel targetCustomer = (TargetCustomerModel)customer;
        int loginAttempts = targetCustomer.getFailedLoginAttempts().intValue();
        if (loginAttempts < failedLoginAttemptsAllowed.intValue()) {
            loginAttempts++;
            targetCustomer.setFailedLoginAttempts(Integer.valueOf(loginAttempts));
            getModelService().save(targetCustomer);

            if (loginAttempts == failedLoginAttemptsAllowed.intValue()) {
                LOG.info(targetCustomer.getUid() + " has reached the maximum login attempt limit of "
                        + failedLoginAttemptsAllowed.intValue());
            }

            return false;
        }
        return true;
    }

    @Override
    public boolean checkLoginAttempts(final CustomerModel customer) {
        if (!(customer instanceof TargetCustomerModel)) {
            throw new IllegalArgumentException("Expect only Target Customer Model");
        }
        final TargetCustomerModel targetCustomer = (TargetCustomerModel)customer;
        final int loginAttempts = targetCustomer.getFailedLoginAttempts().intValue();
        if (loginAttempts < failedLoginAttemptsAllowed.intValue()) {
            return false;
        }
        return true;
    }

    @Override
    public void resetLoginAttempts(final CustomerModel customer) {
        final TargetCustomerModel targetCustomer = (TargetCustomerModel)customer;
        targetCustomer.setFailedLoginAttempts(Integer.valueOf(0));
        getModelService().save(targetCustomer);
    }

    /**
     * @param failedLoginAttemptsAllowed
     *            the failedLoginAttemptsAllowed to set
     */
    @Required
    public void setFailedLoginAttemptsAllowed(final Integer failedLoginAttemptsAllowed) {
        this.failedLoginAttemptsAllowed = failedLoginAttemptsAllowed;
    }

}

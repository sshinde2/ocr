/**
 * 
 */
package au.com.target.tgtcore.manifest.strategy;



/**
 * Strategy to generate unique Manifest Codes
 * 
 * @author Vivek
 *
 */
public interface TargetGenerateManifestCodeStrategy {

    /**
     * Generate Unique Manifest Code
     * 
     * @return TargetManifestModel
     */
    String generateManifestCode(int storeNumber);

}

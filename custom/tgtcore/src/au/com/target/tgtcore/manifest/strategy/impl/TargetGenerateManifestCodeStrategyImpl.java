/**
 * 
 */
package au.com.target.tgtcore.manifest.strategy.impl;

import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.manifest.strategy.TargetGenerateManifestCodeStrategy;


/**
 * @author Vivek
 *
 */
public class TargetGenerateManifestCodeStrategyImpl implements TargetGenerateManifestCodeStrategy {

    private KeyGenerator keyGenerator;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.manifest.strategy.TargetManifestCodeGenerateStrategy#generateManifestCode()
     */
    @Override
    public String generateManifestCode(final int storeNumber) {
        String manifestCode = null;
        final Object generatedCode = keyGenerator.generate();
        if (storeNumber != 0 && null != generatedCode) {
            manifestCode = String.valueOf(storeNumber).concat(generatedCode.toString());
        }
        return manifestCode;
    }

    @Required
    public void setKeyGenerator(final KeyGenerator keyGenerator) {
        this.keyGenerator = keyGenerator;
    }

}

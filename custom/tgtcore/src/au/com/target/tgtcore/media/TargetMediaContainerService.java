/**
 * 
 */
package au.com.target.tgtcore.media;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.media.MediaContainerService;


/**
 * @author mgazal
 *
 */
public interface TargetMediaContainerService extends MediaContainerService {
    /**
     * @param qualifier
     * @param catalogVersion
     * @return {@link MediaContainerModel}
     */
    MediaContainerModel getMediaContainerForQualifier(String qualifier, CatalogVersionModel catalogVersion);
}

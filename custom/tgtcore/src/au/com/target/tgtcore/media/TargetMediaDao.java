/**
 * 
 */
package au.com.target.tgtcore.media;

import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.impl.MediaDao;



/**
 * @author fkratoch
 * 
 */
public interface TargetMediaDao extends MediaDao {

    /**
     * Retrieve specific media type format from selected media container ensuring only single MediaModel is returned
     * 
     * @param container
     * @param format
     * @return MediaModel
     */
    @Override
    public MediaModel findMediaByFormat(final MediaContainerModel container, final MediaFormatModel format);

}
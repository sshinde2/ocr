/**
 * 
 */
package au.com.target.tgtcore.media.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.media.impl.DefaultMediaContainerService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.media.TargetMediaContainerDao;
import au.com.target.tgtcore.media.TargetMediaContainerService;


/**
 * @author mgazal
 *
 */
public class TargetMediaContainerServiceImpl extends DefaultMediaContainerService
        implements TargetMediaContainerService {

    private TargetMediaContainerDao targetMediaContainerDao;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.media.TargetMediaContainerService#getMediaContainerForQualifier(java.lang.String, de.hybris.platform.catalog.model.CatalogVersionModel)
     */
    @Override
    public MediaContainerModel getMediaContainerForQualifier(final String qualifier,
            final CatalogVersionModel catalogVersion) {
        ServicesUtil.validateParameterNotNull(qualifier, "Media container qualifier cannot be null");
        ServicesUtil.validateParameterNotNull(catalogVersion, "Media container catalogVersion cannot be null");

        final List<MediaContainerModel> mediaContainers = targetMediaContainerDao
                .findMediaContainersByQualifier(qualifier, catalogVersion);

        ServicesUtil.validateIfSingleResult(mediaContainers,
                "No media container with qualifier " + qualifier + ", catalogVersion " + catalogVersion
                        + " can be found.",
                "More than one media container with qualifier " + qualifier + ", catalogVersion " + catalogVersion
                        + " found.");
        return mediaContainers.iterator().next();
    }

    /**
     * @param targetMediaContainerDao
     *            the targetMediaContainerDao to set
     */
    @Required
    public void setTargetMediaContainerDao(final TargetMediaContainerDao targetMediaContainerDao) {
        this.targetMediaContainerDao = targetMediaContainerDao;
    }

}

/**
 * 
 */
package au.com.target.tgtcore.media.impl;

import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.media.impl.DefaultMediaDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.Map;
import java.util.TreeMap;

import au.com.target.tgtcore.media.TargetMediaDao;


/**
 * @author fkratoch
 * 
 */
public class TargetMediaDaoImpl extends DefaultMediaDao implements TargetMediaDao {

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.media.TargetMediaDao#findMediaByFormat(de.hybris.platform.core.model.media.MediaContainerModel, de.hybris.platform.core.model.media.MediaFormatModel)
     * 
     * Due to some inconsistent issues during image import from step, we can sometimes get
     * two swatch images in one media container, which would cause an exception.
     * This is a temporary fix/workaround to ensure we only return the first image in the list if the above happens.
     * 
     */
    @Override
    public MediaModel findMediaByFormat(final MediaContainerModel container, final MediaFormatModel format) {

        try
        {
            final Map<String, Object> params = new TreeMap<String, Object>();
            params.put("container", container);
            params.put("format", format);
            final FlexibleSearchQuery query = new FlexibleSearchQuery(//
                    "SELECT {" + MediaModel.PK + "} " + //
                            "FROM {" + MediaModel._TYPECODE + "} " + //
                            "WHERE {" + MediaModel.MEDIACONTAINER + "} = ?container " + //
                            "AND {" + MediaModel.MEDIAFORMAT + "} = ?format", //
                    params);
            query.setCount(1);
            return this.getFlexibleSearchService().searchUnique(query);

        }
        catch (final AmbiguousIdentifierException e)
        {
            throw new ModelNotFoundException("Data inconsistency: " + "Multiple medias with format '" + format
                    + "' reside in container '" + container + "'.", e);
        }

    }

}
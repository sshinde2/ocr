/**
 *
 */
package au.com.target.tgtcore.model.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.featureswitch.dao.TargetFeatureSwitchDao;
import au.com.target.tgtcore.model.TargetFeatureSwitchModel;


/**
 * Intercepter to check feature disability of same category
 *
 * @author siddharam
 *
 */
public class TargetFeatureSwitchModelInterceptor implements ValidateInterceptor<TargetFeatureSwitchModel> {

    private TargetFeatureSwitchDao targetFeatureSwitchDao;

    private List<List<String>> requireAtLeastOneLists;

    @Override
    public void onValidate(final TargetFeatureSwitchModel model, final InterceptorContext ctx)
            throws InterceptorException {
        if (BooleanUtils.isNotTrue(model.getEnabled())) {
            if (CollectionUtils.isNotEmpty(requireAtLeastOneLists)) {
                for (final List<String> features : requireAtLeastOneLists) {
                    for (final String feature : features) {
                        if (StringUtils.equalsIgnoreCase(model.getName(), feature)) {
                            validateAtLeastOne(features, model);
                        }
                    }
                }
            }
        }
    }

    /**
     * check if other corresponding feature enabled
     *
     * @param model
     * @param features
     * @return boolean
     */
    private boolean checkIfEnabledFeatureExists(final TargetFeatureSwitchModel model,
            final List<String> features) {
        TargetFeatureSwitchModel existingModel = null;
        boolean otherFeatureEnabled = false;
        if (CollectionUtils.isNotEmpty(features)) {
            for (final String ftr : features) {
                if (!ftr.equalsIgnoreCase(model.getName())) {
                    existingModel = targetFeatureSwitchDao
                            .getFeatureByName(ftr);
                    if (existingModel != null) {
                        if (BooleanUtils.isTrue(existingModel.getEnabled())) {
                            otherFeatureEnabled = true;
                            break;
                        }
                    }
                }
            }
        }
        return otherFeatureEnabled;
    }

    /**
     * Get the matching feature list all categories
     *
     * @throws InterceptorException
     */
    protected void validateAtLeastOne(final List<String> features, final TargetFeatureSwitchModel model)
            throws InterceptorException {
        final boolean isEnabledFeatureExist = checkIfEnabledFeatureExists(model, features);
        if (!isEnabledFeatureExist) {
            throw new InterceptorException("Can't change enabled to "
                    + model.getEnabled().booleanValue()
                    + " of " + model.getName() + " other feature of same category is set to "
                    + isEnabledFeatureExist);
        }
    }

    /**
     * @param targetFeatureSwitchDao
     *            the targetFeatureSwitchDao to set
     */
    @Required
    public void setTargetFeatureSwitchDao(final TargetFeatureSwitchDao targetFeatureSwitchDao) {
        this.targetFeatureSwitchDao = targetFeatureSwitchDao;
    }

    /**
     * @param requireAtLeastOneLists
     *            the requireAtLeastOneLists to set
     */
    public void setRequireAtLeastOneLists(final List<List<String>> requireAtLeastOneLists) {
        this.requireAtLeastOneLists = requireAtLeastOneLists;
    }

}
/**
 * 
 */
package au.com.target.tgtcore.order;

import de.hybris.platform.core.model.order.OrderModel;


/**
 * Strategy for finding out the total refund amount so far on shipping
 * 
 */
public interface FindOrderRefundedShippingStrategy {

    /**
     * Returns the shipping amount already refunded to customer.
     * 
     * @param order
     *            the order to retrieve already refunded shipping cost for
     * @return the refunded shipping cost
     */
    double getRefundedShippingAmount(OrderModel order);


}

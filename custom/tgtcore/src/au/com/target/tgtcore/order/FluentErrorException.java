/**
 * 
 */
package au.com.target.tgtcore.order;

/**
 * @author bpottass
 *
 */
public class FluentErrorException extends RuntimeException {

    public FluentErrorException() {
        super();
    }

    public FluentErrorException(final String message) {
        super(message);
    }

    public FluentErrorException(final String message, final Throwable cause) {
        super(message, cause);
    }

}

/**
 * 
 */
package au.com.target.tgtcore.order;

import de.hybris.platform.core.model.order.OrderModel;

import au.com.target.tgtfluent.data.Order;
import au.com.target.tgtfluent.data.OrderResponse;


/**
 * @author bpottass
 *
 */
public interface FluentOrderService {

    /**
     * Create order in fluent
     * 
     * @param orderModel
     * @return fluentId
     */
    public String createOrder(OrderModel orderModel) throws FluentOrderException;

    /**
     * Sends the event to fluent to cancel the order
     * 
     * @param orderModel
     */
    public void cancelOrder(OrderModel orderModel);

    /**
     * Sends the event to fluent to resume the order
     * 
     * @param orderModel
     */
    public void resumeOrder(OrderModel orderModel);

    /**
     * Update order status in hybris
     * 
     * @param order
     */
    public void updateOrder(Order order) throws FluentOrderException, FluentErrorException;

    /**
     * get the order details from fluent.
     * 
     * @param fluentOrderId
     * @return order response from fluent.
     */
    OrderResponse getOrder(String fluentOrderId) throws FluentOrderException;

    /**
     * Sends the event to fluent for customer cancel the order
     * 
     * @param orderModel
     * @return failure messages
     */
    String customerCancelOrder(OrderModel orderModel);

    /**
     * Send the event to fluent to release the order
     * 
     * @param orderModel
     * @return boolean
     */
    boolean releaseOrder(OrderModel orderModel);
}

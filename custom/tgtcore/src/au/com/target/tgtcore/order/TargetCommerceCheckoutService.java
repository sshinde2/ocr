package au.com.target.tgtcore.order;

import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;

import java.math.BigDecimal;
import java.util.List;

import au.com.target.tgtcore.model.StoreEmployeeModel;
import au.com.target.tgtpayment.commands.result.TargetCardResult;


/**
 * Extended interface to support Target specific Checkout and Place order process
 *
 */
public interface TargetCommerceCheckoutService extends CommerceCheckoutService {

    /**
     * When User choose Click And Collect, need to fill user details in the cart.
     *
     * @param cartModel
     *            the cart model
     * @param cncStoreNumber
     *            the cnc store number
     * @param title
     *            the title
     * @param firstName
     *            the first name
     * @param lastName
     *            the last name
     * @param phoneNumber
     *            the phone number
     * @param deliveryMode
     *            the delivery mode
     * @return true if successful
     */
    boolean fillCart4ClickAndCollect(CartModel cartModel, int cncStoreNumber, String title, String firstName,
            String lastName,
            String phoneNumber,
            DeliveryModeModel deliveryMode);

    /**
     * When customer places orders on third party sales channel like ebay, need to fill user delivery details in the
     * cart without cart calculation.
     *
     * @param cartModel
     *            the cart model
     * @param cncStoreNumber
     *            the cnc store number
     * @param titleModel
     *            the title model
     * @param firstName
     *            the first name
     * @param lastName
     *            the last name
     * @param phoneNumber
     *            the phone number
     * @param deliveryMode
     *            the delivery mode
     */
    void fillCart4ClickAndCollectWithoutRecalculate(CartModel cartModel, Integer cncStoreNumber, TitleModel titleModel,
            String firstName,
            String lastName,
            String phoneNumber,
            DeliveryModeModel deliveryMode);

    /**
     * Do multiple checks on flybuys number
     *
     * @param cartModel
     * @return true if flybuys number is valid
     */
    boolean isFlybuysNumberValid(CartModel cartModel);

    /**
     *
     *
     * Validates and Saves the TMD card number against the cart. Also recalculates the cart
     *
     * @param checkoutCartModel
     * @return true if TMD card number is applied on the checkout cart
     */
    boolean setTMDCardNumber(CartModel checkoutCartModel, String tmdCardNumber);

    /**
     * Validates and Saves the Flybuys card number against the checkout cart.
     *
     * @param checkoutCartModel
     * @return true if flybuys card number is applied on the checkout cart
     */
    boolean setFlybuysNumber(CartModel checkoutCartModel, String flybuysNumber);

    /**
     *
     * Do multiple checks on flybuys number
     *
     * @param flybuys
     * @return true if flybuys is valid
     */
    boolean isFlybuysNumberValid(String flybuys);

    /**
     * Before placing the order
     * 
     * @param cartModel
     * @param paymentTransactionModel
     * @return true if capture succeeds
     * @throws InsufficientStockLevelException
     */
    boolean beforePlaceOrder(CartModel cartModel, PaymentTransactionModel paymentTransactionModel)
            throws InsufficientStockLevelException;

    /**
     * @param cartModel
     * @param orderModel
     */
    void afterPlaceOrder(CartModel cartModel, OrderModel orderModel);

    /**
     * To check whether the payment has been captured successfully or not
     *
     * @param cartModel
     * @return true only if capture is successful
     */
    boolean isCaptureOrderSuccessful(final CartModel cartModel);

    /**
     * Set Payment Mode
     *
     * @param cartModel
     * @param paymentMode
     * @return true
     */
    boolean setPaymentMode(final CartModel cartModel, final PaymentModeModel paymentMode);

    /**
     * @param cartModel
     */
    void handlePaymentFailure(CartModel cartModel);

    /**
     * @param cartModel
     * @param kioskStoreNumber
     * @return true if the kiosk store number is set successfully on the cart, false otherwise
     */
    boolean setKioskStoreNumber(final CartModel cartModel, String kioskStoreNumber);

    /**
     * @param cartModel
     * @param storeEmployeeModel
     * @return true if the employee and store number is set successfully on the cart, false otherwise
     */
    boolean setStoreAssisted(final CartModel cartModel, StoreEmployeeModel storeEmployeeModel);

    /**
     * Remove the payment info from the cart
     *
     * @param cartModel
     *            the cart
     */
    void removePaymentInfo(final CartModel cartModel);

    /**
     * Returns total amount to be captured before placing the order
     * 
     * @param cartModel
     * @return order total
     */
    BigDecimal getAmountForPaymentCapture(CartModel cartModel);

    /**
     * Checks whether payment should be reversed for given cart
     * 
     * @param cartModel
     *            - cart model
     * @param cardResults
     *            - card details used for payment
     * @return should payments be reversed
     */
    boolean shouldPaymentBeReversed(final CartModel cartModel, final List<TargetCardResult> cardResults);

    /**
     * Filters gift cards from card used for payment
     * 
     * @param cardResults
     *            - card details used for payment
     * @return list of gift cards
     */
    List<TargetCardResult> getGiftCards(final List<TargetCardResult> cardResults);

    /**
     * Remove flybuys number from cart
     * 
     * @param checkoutCartModel
     * @return true or false
     */
    boolean removeFlybuysNumber(CartModel checkoutCartModel);
}

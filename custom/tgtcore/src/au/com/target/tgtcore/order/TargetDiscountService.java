package au.com.target.tgtcore.order;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.order.DiscountService;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;

import java.util.Collection;

import au.com.target.tgtcore.model.TMDiscountProductPromotionModel;
import au.com.target.tgtcore.order.data.AppliedVoucherData;


public interface TargetDiscountService extends DiscountService {
    /**
     * Check if the card number is team member discount card or not
     * 
     * @return true if we can accept this TMD card, otherwise false
     */
    boolean isValidTMDCard(final String cardNumber);

    /**
     * Find the total amount of team member discount for the given order
     * 
     * @param orderModel
     * @return total value of team member discount, 0 if no discount
     */
    double getTotalTMDiscount(final AbstractOrderModel orderModel);

    /**
     * Find the total discount amount for the given order
     * 
     * @param orderModel
     * @return total discount value, 0 if no discount
     */
    double getTotalDiscount(final AbstractOrderModel orderModel);

    /**
     * Find the amount of team member discount refunded via this Order modification, if any.
     * 
     * @param orderModRecordEntryModel
     * @return total value of team member discount, 0 if no discount
     */
    double getTotalTMDiscountRefunded(final OrderModificationRecordEntryModel orderModRecordEntryModel);

    /**
     * Creates and attaches a new discount model with the given name and value to the order. Also refreshes the order so
     * that the discount is immediately visible from the order during the next recalculation. <b>Note: Does not perform
     * recalculation and hence the order total remains unchanged. Caller needs to trigger calculation (see
     * {@link de.hybris.platform.order.CalculationService#calculate(AbstractOrderModel)} to get the discount applied to
     * the order total</b>
     * 
     * @param orderModel
     * @param discountValue
     * @param discountName
     * @return the newly created discount model
     * @throws IllegalArgumentException
     *             if the given {@link OrderModel} is null
     * @throws IllegalArgumentException
     *             if the given discountValue is less than or equal to zero.
     */
    DiscountModel createGlobalDiscountForOrder(OrderModel orderModel, double discountValue, String discountName);

    /**
     * Get the list of applied voucher data for the given order.
     * 
     * @param orderModel
     * @return collection of data
     */
    Collection<AppliedVoucherData> getAppliedVouchersAndFlybuysData(AbstractOrderModel orderModel);

    /**
     * Checks if is valid card type.
     * 
     * @param card
     *            the card
     * @param tmDiscountProductPromotionModel
     *            team member discount promotion model
     * @return true, if is valid card type
     */
    boolean isValidCardType(String card, TMDiscountProductPromotionModel tmDiscountProductPromotionModel);

    /**
     * Checks if any TMDiscountPromotion is applied to the order
     * 
     * @param orderModel
     * @return true, if is the total TMDiscount is greater than zero
     * 
     */
    boolean isTMDiscountAppliedToOrder(final AbstractOrderModel orderModel);

    /**
     * This method will iterate throught the promotions inorder to find the TMD discount applicable and update the
     * TotalTMDDiscount in the order/cart
     * 
     * @param orderModel
     */
    void calculateTotalTMDDiscount(final AbstractOrderModel orderModel);
}

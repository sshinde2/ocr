/**
 * 
 */
package au.com.target.tgtcore.order;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.order.strategies.calculation.FindDeliveryCostStrategy;

import au.com.target.tgtcore.delivery.dto.DeliveryCostDto;


/**
 * Extend FindDeliveryCostStrategy to add methods for getting delivery cost differences.
 * 
 */
public interface TargetFindDeliveryCostStrategy extends FindDeliveryCostStrategy {

    /**
     * Return the amount delivery cost for the order by deliveryMode
     * 
     * @param deliveryMode
     * @param abstractOrder
     * @return double amount
     */
    public double getDeliveryCost(final ZoneDeliveryModeModel deliveryMode, final AbstractOrderModel abstractOrder);

    /**
     * Return the outstanding delivery cost for the order, ie initial cost minus amount already refunded
     * 
     * @param order
     * @return double amount
     */
    double deliveryCostOutstandingForOrder(final OrderModel order);

    /**
     * As part of recalculating an order we need to reset the delivery code.
     * 
     * @param cart
     */
    void resetDeliveryValue(final CartModel cart);

    /**
     * reset the shipster attributes on cart modle
     * 
     * @param orderModel
     * @param deliveryCostDto
     */
    void resetShipsterAttibutes(AbstractOrderModel orderModel, DeliveryCostDto deliveryCostDto);

    /**
     * Get delivery cost dto which contains shipster details
     * 
     * @param order
     * @return DeliveryCostDto
     */
    DeliveryCostDto getDeliveryCostDto(final AbstractOrderModel order);
}

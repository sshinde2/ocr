/**
 * 
 */
package au.com.target.tgtcore.order;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.OrderService;

import java.util.List;


/**
 * @author asingh78
 * 
 */
public interface TargetOrderService extends OrderService {
    /**
     * get order for order id
     * 
     * @param orderId
     * @return OrderModel if order with orderId exist else return null
     */
    OrderModel findOrderModelForOrderId(final String orderId);

    /**
     * get a cart for order id
     * 
     * @param orderId
     * @return CartModel if order with orderId exist else return null
     */
    CartModel findCartModelForOrderId(final String orderId);

    /**
     * Find out if it is possible to update the delivery address on the supplied order.
     * 
     * @param orderModel
     *            the order to check
     * @return true if it is allowed to update the delivery address
     */
    boolean isUpdateDeliveryAddressPossible(final OrderModel orderModel);

    /**
     * Update the delivery address of an existing order.
     * 
     * @param orderModel
     *            existing order
     * @param addressModel
     *            address to set, needs to be already assigned to user
     * @return true if all went well else false
     */
    boolean updateDeliveryAddress(final OrderModel orderModel, final AddressModel addressModel);

    /**
     * 
     * @param orderModel
     * @return a boolean to indicate whether replace order is possible
     */
    boolean isReplacePossible(final OrderModel orderModel);

    /**
     * Method to find orders for given partner order id and sales channel.
     *
     * @param partnerOrderId
     * @param salesChannel
     * @return orderModel
     */
    List<OrderModel> findOrdersForPartnerOrderIdAndSalesChannel(String partnerOrderId, String salesChannel);

    /**
     * Method to fetch latest order model for user if it exists
     * 
     * @param userModel
     * @return OrderModel
     */
    OrderModel findLatestOrderForUser(UserModel userModel);

    /**
     * Creates a unique id as an attribute in cart which will be sent in session initiation with payment provider
     * 
     * @param cartModel
     * @return unique key
     */
    String createUniqueKeyForPaymentInitiation(final CartModel cartModel);

    /**
     * Update the owner of an existing order.
     * 
     * @param orderModel
     * @param userModel
     */
    void updateOwner(OrderModel orderModel, UserModel userModel);

    /**
     * @param fluentId
     * @return {@link OrderModel}
     */
    OrderModel findOrderByFluentId(String fluentId);

    /**
     * Retrieve all pending preOrders (PARKED, REVIEW and REVIEW_ON_HOLD) for a given product
     * 
     * @param productCode
     * @return list of {@link OrderModel}
     */
    List<OrderModel> findPendingPreOrdersForProduct(String productCode);
}

/**
 * 
 */
package au.com.target.tgtcore.order;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Date;
import java.util.List;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.product.data.ProductDisplayType;


/**
 * service to support preOrder operations
 * 
 * @author gsing236
 *
 */
public interface TargetPreOrderService {

    /**
     * Finds the stock level of the specified product from the preOrder warehouses and calculates the actual available
     * amount.
     * 
     * @param product
     *            - the product
     * @return actual available amount for the specific product
     */
    int getAvailablePreOrderQuantityForCurrentProduct(final ProductModel product);

    /**
     * checks if cart is having any preOrder product or not.
     * 
     * @param cartModel
     * @return boolean
     */
    boolean doesCartHavePreOrderProduct(final CartModel cartModel);

    /**
     * update preOrder details in cart. Checks if cart contains a preOrder product, then set preOrder boolean and
     * deposit amount in cart.
     * 
     * @param cartModel
     */
    void updatePreOrderDetails(final CartModel cartModel);


    /**
     * This service will calculate the productdisplaytype of the color variant Available values :
     * COMING_SOON,AVAILABLE_FOR_SALE,PREORDER_AVAILABLE
     * 
     * @param targetVariantProduct
     * @return ProductDisplayTYpe
     */
    ProductDisplayType getProductDisplayType(AbstractTargetVariantProductModel targetVariantProduct);

    /**
     * 
     * Returns a list of pre-orders which are in parked state for the specified release date
     * 
     * @param releaseDate
     * @param daysToAdvance
     */
    List<OrderModel> getParkedPreOrdersForReleaseDate(final Date releaseDate, final int daysToAdvance);

}

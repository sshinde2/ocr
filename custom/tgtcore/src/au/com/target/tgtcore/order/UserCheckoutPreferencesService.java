/**
 * 
 */
package au.com.target.tgtcore.order;

import de.hybris.platform.core.model.order.OrderModel;



/**
 * @author ajit
 *
 */
public interface UserCheckoutPreferencesService {

    /**
     * Save user preferences for single page checkout.
     *
     * @param orderModel
     *            the order model
     */
    void saveUserPreferences(OrderModel orderModel);


}

/**
 * 
 */
package au.com.target.tgtcore.order.data;

/**
 * Represent data of voucher that has been applied to an order.
 * 
 */
public class AppliedVoucherData {

    // This is the 3 character id code, not the voucher code entered by the user
    private String voucherCode;

    // The absolute value of the discount when applied
    private Double appliedValue;

    // True if absolute (ie dollar value) rather than relative (percentage) or 
    private Boolean absolute;

    // If relative, then this is the percent
    private Double percent;

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(final String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public Double getAppliedValue() {
        return appliedValue;
    }

    public void setAppliedValue(final Double appliedValue) {
        this.appliedValue = appliedValue;
    }

    public Boolean isAbsolute() {
        return absolute;
    }

    public void setAbsolute(final Boolean absolute) {
        this.absolute = absolute;
    }

    public Double getPercent() {
        return percent;
    }

    public void setPercent(final Double percent) {
        this.percent = percent;
    }

}

/**
 * 
 */
package au.com.target.tgtcore.order.impl;

import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.jalo.JaloSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.order.TargetDiscountService;
import au.com.target.tgtcore.order.TargetPreOrderService;


/**
 * @author pthoma20
 *
 */
public class TargetCartCalculationStrategy extends DefaultCommerceCartCalculationStrategy {
    private static final Logger LOG = Logger.getLogger(TargetCartCalculationStrategy.class);

    private TargetDiscountService targetDiscountService;
    private TargetPreOrderService preOrderService;

    @Override
    public boolean calculateCart(final CommerceCartParameter parameter) {
        checkValidityOfJsession(parameter);
        final JaloSession session = JaloSession.getCurrentSession();
        //added sync block to avoid issues with cart calculation
        boolean calculated = false;
        synchronized (session) {
            calculated = super.calculateCart(parameter);
            preOrderService.updatePreOrderDetails(parameter.getCart());
            targetDiscountService.calculateTotalTMDDiscount(parameter.getCart());

        }
        return calculated;

    }

    @Override
    public boolean recalculateCart(final CommerceCartParameter parameter) {

        checkValidityOfJsession(parameter);
        final JaloSession session = JaloSession.getCurrentSession();
        boolean recalculated = false;
        //added sync block to avoid issues with cart calculation
        synchronized (session) {
            recalculated = super.recalculateCart(parameter);
            preOrderService.updatePreOrderDetails(parameter.getCart());
            targetDiscountService.calculateTotalTMDDiscount(parameter.getCart());
            return recalculated;
        }
    }

    private void checkValidityOfJsession(final CommerceCartParameter parameter) {
        if (!JaloSession.hasCurrentSession()) {
            if (null != parameter && null != parameter.getCart()) {
                LOG.info("No session available for the cart " + parameter.getCart().getCode());

            }
            else {
                LOG.info("No session available for the cart");
            }
        }
    }

    /**
     * @param targetDiscountService
     *            the targetDiscountService to set
     */
    @Required
    public void setTargetDiscountService(final TargetDiscountService targetDiscountService) {
        this.targetDiscountService = targetDiscountService;
    }

    /**
     * @param preOrderService
     *            the preOrderService to set
     */
    @Required
    public void setPreOrderService(final TargetPreOrderService preOrderService) {
        this.preOrderService = preOrderService;
    }
}

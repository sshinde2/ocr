/**
 * 
 */
package au.com.target.tgtcore.order.impl;

import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCardTypeService;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.payment.dto.CardType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;


/**
 * @author pthoma20
 * 
 */
public class TargetCommerceCardTypeServiceImpl extends DefaultCommerceCardTypeService {
    protected static final Logger LOG = Logger.getLogger(TargetCommerceCardTypeServiceImpl.class);
    private static final String CREDIT_CARD_TYPE = "CreditCardType";
    private Collection<CardType> cardTypes;



    @Override
    protected void createCardTypeCollection()
    {
        final List<CreditCardType> creditCardTypes = getEnumerationService().getEnumerationValues(CREDIT_CARD_TYPE);
        this.cardTypes = new ArrayList();

        for (final CreditCardType creditCardType : creditCardTypes)
        {
            final CardType cardType = new CardType(creditCardType.getCode(), creditCardType, getTypeService()
                    .getEnumerationValue(creditCardType)
                    .getName());
            this.cardTypes.add(cardType);
        }
    }


    @Override
    public Collection<CardType> getCardTypes()
    {
        synchronized (this) {
            if (this.cardTypes == null)
            {
                createCardTypeCollection();
            }
        }
        return this.cardTypes;
    }

    @Override
    public CardType getCardTypeForCode(final String code)
    {
        final Collection<CardType> cardTypesToCheck = getCardTypes();

        for (final CardType cardType : cardTypesToCheck)
        {
            if (cardType.getCode().getCode().equals(code))
            {
                return cardType;
            }
        }
        return null;
    }
}

/**
 * 
 */
package au.com.target.tgtcore.order.impl;

import de.hybris.platform.commerceservices.order.impl.DefaultCommerceDeliveryAddressStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.delivery.TargetDeliveryService;


public class TargetDeliveryAddressStrategyImpl extends DefaultCommerceDeliveryAddressStrategy {

    private TargetDeliveryService targetDeliveryService;

    @Override
    protected boolean isValidDeliveryAddress(final CartModel cartModel, final AddressModel addressModel) {
        if (addressModel != null) {
            return targetDeliveryService.isDeliveryModePostCodeCombinationValid(cartModel.getDeliveryMode(),
                    addressModel, cartModel);
        }
        return true;
    }

    /**
     * @param targetDeliveryService
     *            the targetDeliveryService to set
     */
    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        this.targetDeliveryService = targetDeliveryService;
    }
}

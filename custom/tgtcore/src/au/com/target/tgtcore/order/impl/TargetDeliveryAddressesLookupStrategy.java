/**
 * 
 */
package au.com.target.tgtcore.order.impl;

import de.hybris.platform.commerceservices.strategies.impl.DefaultDeliveryAddressesLookupStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;


/**
 * @author bhuang3
 *
 */
public class TargetDeliveryAddressesLookupStrategy extends DefaultDeliveryAddressesLookupStrategy {

    @Override
    public List<AddressModel> getDeliveryAddressesForOrder(final AbstractOrderModel abstractOrder,
            final boolean visibleAddressesOnly) {
        final List addressesForOrder = new ArrayList();
        if (abstractOrder != null)
        {
            final UserModel user = abstractOrder.getUser();
            if (user instanceof CustomerModel)
            {
                if (visibleAddressesOnly)
                {
                    addressesForOrder.addAll(getCustomerAccountService().getAddressBookDeliveryEntries(
                            (CustomerModel)user));
                }
                else
                {
                    addressesForOrder.addAll(getCustomerAccountService().getAllAddressEntries((CustomerModel)user));

                }

                if ((getCheckoutCustomerStrategy().isAnonymousCheckout())
                        &&
                        (addressesForOrder.isEmpty())
                        && (abstractOrder.getDeliveryAddress() != null)
                        && !(abstractOrder.getDeliveryMode() instanceof TargetZoneDeliveryModeModel && (BooleanUtils
                                .toBoolean(((TargetZoneDeliveryModeModel)abstractOrder
                                        .getDeliveryMode()).getIsDeliveryToStore()))))
                {
                    addressesForOrder.add(abstractOrder.getDeliveryAddress());
                }
            }
        }
        return addressesForOrder;
    }



}

package au.com.target.tgtcore.order.impl;


import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.order.impl.DefaultDiscountService;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.jalo.ProductPromotion;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.voucher.VoucherService;
import de.hybris.platform.voucher.model.VoucherModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.constants.TgtCoreConstants.Seperators;
import au.com.target.tgtcore.jalo.TMDiscountProductPromotion;
import au.com.target.tgtcore.jalo.TMDiscountPromotion;
import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.model.TMDiscountProductPromotionModel;
import au.com.target.tgtcore.order.TargetDiscountService;
import au.com.target.tgtcore.order.data.AppliedFlybuysDiscountData;
import au.com.target.tgtcore.order.data.AppliedVoucherData;
import au.com.target.tgtpayment.strategy.FindVoucherValueStrategy;


public class TargetDiscountServiceImpl extends DefaultDiscountService implements TargetDiscountService {

    private static final String GLOBAL_DISCOUNT_STRING = "GD";

    private PromotionsService promotionsService;

    private VoucherService voucherService;

    private FindVoucherValueStrategy findVoucherValueStrategy;


    @Override
    public boolean isValidTMDCard(final String cardNumber) {
        // default implementation that will always return true
        return true;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.order.TargetDiscountService#isValidCardType(java.lang.String, au.com.target.tgtsale.model.TargetTMDPrefixModel)
     */
    @Override
    public boolean isValidCardType(final String card,
            final TMDiscountProductPromotionModel tmDiscountProductPromotionModel) {
        // default implementation that will always return true
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getTotalTMDiscount(final AbstractOrderModel orderModel) {
        Assert.notNull(orderModel, "Order is null");

        //The calculate Total TMD Discount is called just for taking into account the existing carts..
        if ((orderModel.getTotalTMDDiscount() == null || orderModel.getTotalTMDDiscount().doubleValue() <= 0)
                && StringUtils.isNotBlank(orderModel.getTmdCardNumber())) {
            final JaloSession session = JaloSession.getCurrentSession();
            synchronized (session) {
                calculateTotalTMDDiscount(orderModel);
            }
        }

        return orderModel.getTotalTMDDiscount() != null ? orderModel.getTotalTMDDiscount().doubleValue() : 0D;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void calculateTotalTMDDiscount(final AbstractOrderModel orderModel) {

        Assert.notNull(orderModel, "Order is null");

        final PromotionOrderResults promoResult = promotionsService.getPromotionResults(orderModel);
        BigDecimal total = BigDecimal.valueOf(0l);

        final List<PromotionResult> productPromoResults = promoResult.getAllResults();
        if (CollectionUtils.isNotEmpty(productPromoResults)) {
            for (final PromotionResult result : productPromoResults) {
                if (result.isApplied()
                        && (result.getPromotion() instanceof TMDiscountPromotion
                                || result.getPromotion() instanceof TMDiscountProductPromotion)) {
                    total = total.add(BigDecimal.valueOf(result.getTotalDiscount()));
                }
            }
        }
        orderModel.setTotalTMDDiscount(Double.valueOf(total.doubleValue()));
    }

    @Override
    public double getTotalDiscount(final AbstractOrderModel orderModel) {
        Assert.notNull(orderModel, "Order is null");

        final PromotionOrderResults promoResult = promotionsService.getPromotionResults(orderModel);
        BigDecimal total = BigDecimal.valueOf(0l);

        final List<PromotionResult> productPromoResults = promoResult.getAllResults();
        if (CollectionUtils.isNotEmpty(productPromoResults)) {
            for (final PromotionResult result : productPromoResults) {
                if (result.isApplied()
                        && (result.getPromotion() instanceof ProductPromotion)) {
                    total = total.add(BigDecimal.valueOf(result.getTotalDiscount()));
                }
            }
        }

        final List<PromotionResult> orderPromoResults = promoResult.getAllResults();
        if (CollectionUtils.isNotEmpty(orderPromoResults)) {
            for (final PromotionResult result : orderPromoResults) {
                if (result.isApplied()
                        && !(result.getPromotion() instanceof ProductPromotion)) {
                    total = total.add(BigDecimal.valueOf(result.getTotalDiscount()));
                }
            }
        }

        // Add the vouchers
        total = total.add(getVoucherDiscount(orderModel));

        return total.doubleValue();
    }

    @Override
    public boolean isTMDiscountAppliedToOrder(final AbstractOrderModel orderModel) {
        return orderModel.getTotalTMDDiscount().doubleValue() > 0;
    }

    /**
     * Get the voucher discount on the order
     * 
     * @param orderModel
     * @return total voucher discount as BigDecimal
     */
    protected BigDecimal getVoucherDiscount(final AbstractOrderModel orderModel) {

        final Collection<DiscountModel> discounts = voucherService.getAppliedVouchers(orderModel);
        BigDecimal total = BigDecimal.valueOf(0l);

        if (CollectionUtils.isNotEmpty(discounts)) {
            for (final DiscountModel discount : discounts) {
                if (discount instanceof VoucherModel) {

                    final VoucherModel voucher = (VoucherModel)discount;
                    final Double value = findVoucherValueStrategy.getDiscountValueAppliedValue(voucher, orderModel);
                    if (value != null) {
                        total = total.add(BigDecimal.valueOf(value.doubleValue()));
                    }
                }
            }
        }

        return total;
    }


    /**
     * Get the list of applied voucher data for the given order.
     * 
     * @param orderModel
     * @return collection of data
     */
    @Override
    public Collection<AppliedVoucherData> getAppliedVouchersAndFlybuysData(final AbstractOrderModel orderModel) {

        final Collection<AppliedVoucherData> appliedVoucherDataList = new ArrayList<>();

        final Collection<DiscountModel> discounts = voucherService.getAppliedVouchers(orderModel);

        if (CollectionUtils.isNotEmpty(discounts)) {
            for (final DiscountModel discount : discounts) {
                if (discount instanceof FlybuysDiscountModel) {
                    final FlybuysDiscountModel flybuysDiscount = (FlybuysDiscountModel)discount;
                    final AppliedFlybuysDiscountData appliedFlybuysDiscountData = new AppliedFlybuysDiscountData();
                    appliedFlybuysDiscountData.setVoucherCode(flybuysDiscount.getFlybuysCardNumber());
                    appliedFlybuysDiscountData.setAppliedValue(flybuysDiscount.getValue());
                    appliedFlybuysDiscountData.setPoints(orderModel.getFlybuysPointsConsumed());
                    appliedFlybuysDiscountData.setRedeemCode(flybuysDiscount.getRedeemCode());
                    appliedFlybuysDiscountData.setConfirmationCode(flybuysDiscount.getConfirmationCode());
                    appliedVoucherDataList.add(appliedFlybuysDiscountData);
                }
                else if (discount instanceof VoucherModel) {
                    final VoucherModel voucher = (VoucherModel)discount;
                    final Double value = findVoucherValueStrategy.getDiscountValueAppliedValue(voucher, orderModel);
                    if (value != null) {
                        appliedVoucherDataList.add(createAppliedVoucherData(voucher, value));
                    }
                }
            }
        }

        return appliedVoucherDataList;
    }

    private AppliedVoucherData createAppliedVoucherData(final VoucherModel voucher, final Double value) {

        final AppliedVoucherData data = new AppliedVoucherData();
        data.setAppliedValue(value);
        data.setAbsolute(voucher.getAbsolute());
        data.setVoucherCode(voucher.getCode());

        // For percent vouchers, set percent as the voucher value
        if (voucher.getAbsolute() != null && !voucher.getAbsolute().booleanValue()) {
            data.setPercent(voucher.getValue());
        }

        return data;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getTotalTMDiscountRefunded(final OrderModificationRecordEntryModel orderModRecordEntryModel) {
        Assert.notNull(orderModRecordEntryModel, "OrderModificationRecordEntryModel is null");
        final OrderModel current = orderModRecordEntryModel.getModificationRecord().getOrder();
        final OrderModel previous = orderModRecordEntryModel.getOriginalVersion().getPreviousOrderVersion();

        return getTotalTMDiscount(previous) - getTotalTMDiscount(current);
    }

    @Override
    public DiscountModel createGlobalDiscountForOrder(final OrderModel orderModel, final double discountValue,
            final String discountName) {

        Assert.notNull(orderModel, "Ordermodel cannot not be null");
        if (discountValue <= 0) {
            throw new IllegalArgumentException("Discount value cannot be less than or equal to zero.");
        }

        int orderDiscounts = orderModel.getDiscounts().size();

        final DiscountModel discountModel = getModelService().create(DiscountModel.class);
        discountModel.setCode(getGlobalDiscountPrefix(orderModel) + ++orderDiscounts);
        discountModel.setName(discountName);
        discountModel.setGlobal(Boolean.TRUE);
        discountModel.setCurrency(orderModel.getCurrency());
        discountModel.setValue(Double.valueOf(discountValue));

        final Collection<AbstractOrderModel> orders = new HashSet<AbstractOrderModel>(1);
        orders.add(orderModel);
        discountModel.setOrders(orders);

        getModelService().save(discountModel);

        getModelService().refresh(orderModel);
        return discountModel;
    }


    private String getGlobalDiscountPrefix(final OrderModel orderModel) {
        return orderModel.getCode() + Seperators.HYPHEN + GLOBAL_DISCOUNT_STRING + Seperators.HYPHEN;
    }

    /**
     * @param promotionsService
     *            the promotionsService to set
     */
    @Required
    public void setPromotionsService(final PromotionsService promotionsService) {
        this.promotionsService = promotionsService;
    }

    /**
     * @param voucherService
     *            the voucherService to set
     */
    @Required
    public void setVoucherService(final VoucherService voucherService) {
        this.voucherService = voucherService;
    }

    /**
     * @param findVoucherValueStrategy
     *            the findVoucherValueStrategy to set
     */
    @Required
    public void setFindVoucherValueStrategy(final FindVoucherValueStrategy findVoucherValueStrategy) {
        this.findVoucherValueStrategy = findVoucherValueStrategy;
    }

}

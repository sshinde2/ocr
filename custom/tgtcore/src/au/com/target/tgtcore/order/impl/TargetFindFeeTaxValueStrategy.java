/**
 * 
 */
package au.com.target.tgtcore.order.impl;

import de.hybris.platform.util.TaxValue;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetTaxModel;
import au.com.target.tgtcore.tax.dao.TargetTaxDao;


/**
 * Find the tax value to use for fees
 * 
 */
public class TargetFindFeeTaxValueStrategy {

    private TargetTaxDao targetTaxDao;

    /**
     * Return the tax value to use for fees.
     * 
     * @return TaxValue or null if none found
     */
    public TaxValue getTaxValueForFee() {

        // Get the default tax for fees and convert to a TaxValue
        final TargetTaxModel defaultTax = targetTaxDao.getDefaultTax();

        if (defaultTax == null)
        {
            return null;
        }

        String currIsoCode = "AUD";
        if (defaultTax.getCurrency() != null)
        {
            currIsoCode = defaultTax.getCurrency().getIsocode();
        }

        final TaxValue defaultTaxValue = new TaxValue(defaultTax.getCode(),
                defaultTax.getValue().doubleValue(),
                false,
                currIsoCode);

        return defaultTaxValue;
    }

    /**
     * @param targetTaxDao
     *            the targetTaxDao to set
     */
    @Required
    public void setTargetTaxDao(final TargetTaxDao targetTaxDao) {
        this.targetTaxDao = targetTaxDao;
    }


}

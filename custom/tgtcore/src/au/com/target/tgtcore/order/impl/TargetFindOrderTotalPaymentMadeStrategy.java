package au.com.target.tgtcore.order.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;

import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;


public class TargetFindOrderTotalPaymentMadeStrategy implements FindOrderTotalPaymentMadeStrategy {

    /**
     * this method support both order and cart, for cart, the REVIEW capture will be treated as paid and will be
     * excluded if it is an order.
     */
    @Override
    public Double getAmountPaidSoFar(final AbstractOrderModel abstractOrderModel) {

        // Add up all payment entries of type CAPTURE and subtract any existing refunds
        // This will work for all purchase options
        // This assumes that all payments are in the same currency.
        BigDecimal paidSoFar = new BigDecimal(0);

        final List<String> allowedStatuses = new ArrayList<>();
        allowedStatuses.add(TransactionStatus.ACCEPTED.toString());
        if (abstractOrderModel instanceof CartModel) {
            allowedStatuses.add(TransactionStatus.REVIEW.toString());
        }

        if (abstractOrderModel.getPaymentTransactions() != null) {
            for (final PaymentTransactionModel trans : abstractOrderModel.getPaymentTransactions()) {
                if (trans.getEntries() != null) {
                    for (final PaymentTransactionEntryModel transEntry : trans.getEntries()) {

                        if (PaymentTransactionType.CAPTURE.equals(transEntry.getType())) {
                            if (allowedStatuses.contains(transEntry.getTransactionStatus())) {
                                paidSoFar = paidSoFar.add(transEntry.getAmount());
                            }
                        }
                        //we regard entry with IpgGiftCardPaymentInfoModel with review status as already refunded, 
                        //because we raised cs ticket to refund giftcard manually. 
                        if ((transEntry.getType() == PaymentTransactionType.REFUND_FOLLOW_ON
                                && transEntry.getTransactionStatus().equals(TransactionStatus.ACCEPTED.name()))
                                || (transEntry.getType() == PaymentTransactionType.REFUND_STANDALONE
                                && transEntry.getTransactionStatus().equals(TransactionStatus.ACCEPTED.name()))) {
                            paidSoFar = paidSoFar.subtract(transEntry.getAmount());
                        }
                    }
                }
            }
        }

        // Round the amount paid so far to nearest cent
        final Double dPaidSoFar = Double.valueOf(paidSoFar.setScale(2, BigDecimal.ROUND_HALF_DOWN).doubleValue());
        return dPaidSoFar;
    }

    /**
     * Please use the above method instead, this method is deprecated and will cause error for split payments.
     * 
     * Get the total amount captured so far from the cart/order by considering the amounts on the
     * {@link PaymentTransactionEntryModel} of type {@link PaymentTransactionType#CAPTURE} with statues of either
     * {@link TransactionStatus#ACCEPTED} or either {@link TransactionStatus#REVIEW}
     * 
     * @deprecated
     */
    @SuppressWarnings("deprecation")
    @Deprecated
    @Override
    public BigDecimal getAmountCapturedSoFar(final AbstractOrderModel abstractOrderModel) {

        Assert.notNull(abstractOrderModel, "AbstractOrderModel cannot be null");

        final List<PaymentTransactionModel> paymentTransactions = abstractOrderModel.getPaymentTransactions();
        BigDecimal amountCaptured = BigDecimal.ZERO;
        if (CollectionUtils.isEmpty(paymentTransactions)) {
            return amountCaptured;
        }

        final List<String> allowedStatuses = Arrays.asList(TransactionStatus.ACCEPTED.toString(),
                TransactionStatus.REVIEW.toString());

        for (final PaymentTransactionModel transaction : paymentTransactions) {

            final List<PaymentTransactionEntryModel> entries = transaction.getEntries();

            if (CollectionUtils.isEmpty(entries)) {
                continue;
            }

            for (final PaymentTransactionEntryModel transactionEntry : entries) {
                if (PaymentTransactionType.CAPTURE.equals(transactionEntry.getType())) {
                    if (allowedStatuses.contains(transactionEntry.getTransactionStatus())) {
                        amountCaptured = amountCaptured.add(transactionEntry.getAmount());
                    }
                }
            }
        }

        return amountCaptured;
    }

}

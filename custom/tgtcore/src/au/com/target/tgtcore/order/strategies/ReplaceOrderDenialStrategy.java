/**
 * 
 */
package au.com.target.tgtcore.order.strategies;

import de.hybris.platform.core.model.order.OrderModel;


/**
 * @author xiaofanlu
 * 
 */
public interface ReplaceOrderDenialStrategy {
    /**
     * Determines whether an order can be replaced for given conditions.
     * 
     * @param order
     *            Order that is subject to be replaced
     * @return true if replacement is not possible for given order
     */
    boolean isDenied(OrderModel order);
}

/**
 * 
 */
package au.com.target.tgtcore.order.strategies;

import de.hybris.platform.core.model.order.OrderModel;


public interface TaxInvoiceGenerationDenialStrategy {
    /**
     * Determines TaxInvoice Generation possibility for given conditions.
     * 
     * @param order
     * @return true
     */
    boolean isDenied(OrderModel order);
}

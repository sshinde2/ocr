/**
 * 
 */
package au.com.target.tgtcore.order.strategies;

import de.hybris.platform.core.model.order.OrderModel;


/**
 * @author dcwillia
 * 
 */
public interface UpdateAddressDenialStrategy {
    /**
     * Determines delivery address update possibility for given conditions.
     * 
     * @param order
     *            Order that is subject to cancel
     * @return true if delivery address update is denied for given order
     */
    boolean isDenied(OrderModel order);
}

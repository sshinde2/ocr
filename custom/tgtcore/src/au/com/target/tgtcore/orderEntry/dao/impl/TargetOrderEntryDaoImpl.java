/**
 * 
 */
package au.com.target.tgtcore.orderEntry.dao.impl;

import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.orderEntry.dao.TargetOrderEntryDao;


/**
 * @author bhuang3
 *
 */
public class TargetOrderEntryDaoImpl implements TargetOrderEntryDao {

    private static final String FIND_ORDER_ENTRY_MISSING_PRODUCT_MODEL = "select {" + OrderEntryModel.PK
            + "} from {"
            + OrderEntryModel._TYPECODE + "} Where {" + OrderEntryModel.PRODUCT + "} is null";
    private FlexibleSearchService flexibleSearchService;

    private int maxSize;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.orderEntry.dao.TargetOrderEntryDao#findOrderEntryMissingProductModel()
     */
    @Override
    public List<OrderEntryModel> findOrderEntryMissingProductModel() {
        final SearchResult<OrderEntryModel> searchResult = getFlexibleSearchService().search(
                FIND_ORDER_ENTRY_MISSING_PRODUCT_MODEL,
                Collections.<String, Object> singletonMap("maxSize", Integer.valueOf(maxSize)));
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(FIND_ORDER_ENTRY_MISSING_PRODUCT_MODEL);
        searchQuery.setResultClassList(Collections.singletonList(OrderEntryModel.class));
        searchQuery.setStart(0);
        searchQuery.setCount(maxSize);
        return searchResult.getResult();
    }

    /**
     * @return the flexibleSearchService
     */
    protected FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    /**
     * @param flexibleSearchService
     *            the flexibleSearchService to set
     */
    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    /**
     * @param maxSize
     *            the maxSize to set
     */
    @Required
    public void setMaxSize(final int maxSize) {
        this.maxSize = maxSize;
    }




}

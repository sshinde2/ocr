package au.com.target.tgtcore.ordercancel;

import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelRequest;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.ordercancel.data.IpgNewRefundInfoDTO;
import au.com.target.tgtcore.ordercancel.enums.RequestOrigin;


/**
 * Target extension to {@link OrderCancelRequest} which is able to specify shipping amount to refund.
 */
public class TargetOrderCancelRequest extends OrderCancelRequest {

    private Double shippingAmountToRefund;

    private PaymentInfoModel newPayment;

    private IpgNewRefundInfoDTO ipgNewRefundInfoDTO;

    private RequestOrigin requestOrigin;

    /**
     * Creates new order cancel request base on given {@code order}.
     * 
     * @param order
     *            the order to create cancel request for
     */
    public TargetOrderCancelRequest(final OrderModel order) {
        super(order);
    }

    /**
     * Creates new order cancel request based on given {@code order} and {@code cancelReason}.
     * 
     * @param order
     *            the order to create cancel request for
     * @param cancelReason
     *            the cancellation reason
     */
    public TargetOrderCancelRequest(final OrderModel order, final CancelReason cancelReason) {
        super(order, cancelReason);
    }

    /**
     * Creates new order cancel request based on given {@code order}, {@code cancelReason} and {@code notes}.
     * 
     * @param order
     *            the order to create cancel request for
     * @param cancelReason
     *            the cancellation reason
     * @param notes
     *            the cancellation notes
     */
    public TargetOrderCancelRequest(final OrderModel order, final CancelReason cancelReason, final String notes) {
        super(order, cancelReason, notes);
    }

    /**
     * Creates new order cancel request based on given {@code order} and {@code orderCancelEntries}.
     * 
     * @param order
     *            the order to create cancel request for
     * @param orderCancelEntries
     *            the cancellation record entries
     */
    public TargetOrderCancelRequest(final OrderModel order, final List<OrderCancelEntry> orderCancelEntries) {
        super(order, orderCancelEntries);
    }

    /**
     * 
     * @param order
     *            the order to create cancel request for
     * @param orderCancelEntries
     *            the cancellation record entries
     * @param notes
     *            the cancellation notes
     */
    public TargetOrderCancelRequest(final OrderModel order, final List<OrderCancelEntry> orderCancelEntries,
            final String notes) {
        super(order, orderCancelEntries, notes);
    }

    @Override
    public boolean isPartialCancel() {
        if (!super.isPartialCancel()) {
            return false;
        }

        final List<AbstractOrderEntryModel> orderEntries = getOrder().getEntries();

        if (CollectionUtils.isNotEmpty(orderEntries)) {
            for (final AbstractOrderEntryModel entry : orderEntries) {
                if (entry.getQuantity().intValue() > 0) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns the desired shipping amount to refund.
     * 
     * @return the shipping amount to refund
     */
    public Double getShippingAmountToRefund() {
        return shippingAmountToRefund;
    }

    /**
     * Sets the shipping amount to refund.
     * 
     * @param shippingAmountToRefund
     *            the shipping amount to refund
     */
    public void setShippingAmountToRefund(final Double shippingAmountToRefund) {
        this.shippingAmountToRefund = shippingAmountToRefund;
    }

    /**
     * Return the new credit card payment info
     * 
     * @return the new payment info for a new credit card payment
     */
    public PaymentInfoModel getNewPayment() {
        return newPayment;
    }

    /**
     * set new payment info for a new credit card payment
     * 
     * @param newPayment
     */
    public void setNewPayment(final PaymentInfoModel newPayment) {
        this.newPayment = newPayment;
    }

    /**
     * @return the ipgNewRefundInfoDTO
     */
    public IpgNewRefundInfoDTO getIpgNewRefundInfoDTO() {
        return ipgNewRefundInfoDTO;
    }

    /**
     * @param ipgNewRefundInfoDTO
     *            the ipgNewRefundInfoDTO to set
     */
    public void setIpgNewRefundInfoDTO(final IpgNewRefundInfoDTO ipgNewRefundInfoDTO) {
        this.ipgNewRefundInfoDTO = ipgNewRefundInfoDTO;
    }

    /**
     * Return the request origin
     * 
     * @return the requestOrigin
     */
    public RequestOrigin getRequestOrigin() {
        return requestOrigin;
    }

    /**
     * set request origin
     * 
     * @param requestOrigin
     *            the requestOrigin to set
     */
    public void setRequestOrigin(final RequestOrigin requestOrigin) {
        this.requestOrigin = requestOrigin;
    }

}

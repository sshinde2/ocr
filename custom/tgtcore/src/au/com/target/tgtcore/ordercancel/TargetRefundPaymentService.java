/**
 * 
 */
package au.com.target.tgtcore.ordercancel;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import java.math.BigDecimal;
import java.util.List;

import au.com.target.tgtcore.ordercancel.data.IpgNewRefundInfoDTO;
import au.com.target.tgtcore.ordercancel.data.PaymentTransactionDTO;
import au.com.target.tgtcore.ordercancel.data.PaymentTransactionEntryDTO;
import au.com.target.tgtcore.ordercancel.data.RefundInfoDTO;


/**
 * 
 */
public interface TargetRefundPaymentService {


    /**
     * Perform a follow on refund on this order for given amount
     * 
     * @param order
     * @param refundAmount
     * @return the payment transaction entry data for the refund
     */
    List<PaymentTransactionEntryModel> performFollowOnRefund(final OrderModel order, BigDecimal refundAmount);

    /**
     * Perform refund on order for amount, using the new credit card payment information.
     * 
     * 
     * @param order
     * @param refundAmount
     * @param newPayment
     * @return the payment transaction data for the refund
     * 
     */
    PaymentTransactionDTO performStandaloneRefund(OrderModel order, BigDecimal refundAmount,
            PaymentInfoModel newPayment);

    /**
     * Perform ipg manual refund on order for amount, using the refund information from cs manual refund
     * 
     * 
     * @param order
     * @param refundAmount
     * @param refundInfoDTO
     * @return the payment transaction entry data for the refund
     * 
     */
    List<PaymentTransactionEntryModel> performIpgManualRefund(OrderModel order, BigDecimal refundAmount,
            IpgNewRefundInfoDTO refundInfoDTO);

    /**
     * Create a refund payment transaction entry for the given data
     * 
     * @param order
     * @param dataList
     */
    void createFollowOnRefundPaymentTransactionEntries(OrderModel order, List<PaymentTransactionEntryDTO> dataList);

    /**
     * Create a refund payment transaction for the given data
     * 
     * @param order
     * @param paymentInfo
     * @param transaction
     */
    void createStandaloneRefundPaymentTransaction(OrderModel order, PaymentInfoModel paymentInfo,
            PaymentTransactionDTO transaction);

    /**
     * Get refunded amount from refunded entries
     * 
     * @param entryDtoList
     * @return refundedAmount
     */
    BigDecimal findRefundedAmountAfterRefund(List<PaymentTransactionEntryDTO> entryDtoList);

    /**
     * create refunded info message
     * 
     * @param refundedentryDtoList
     * @param refundAmount
     * @return String message of partial refund info for CS ticket or exception
     */
    RefundInfoDTO createRefundedInfo(List<PaymentTransactionEntryDTO> refundedentryDtoList, BigDecimal refundAmount);

    /**
     * Convert a list of <code>PaymentTransactionEntryModel</code> to a list of <code>PaymentTransactionEntryDTO</code>.
     * 
     * @param paymentTransactionEntries
     *            The entries to convert
     * @return The converted entries
     */
    List<PaymentTransactionEntryDTO> convertPaymentTransactionEntries(
            final List<PaymentTransactionEntryModel> paymentTransactionEntries);
}

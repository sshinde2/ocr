/**
 * 
 */
package au.com.target.tgtcore.ordercancel.data;

import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.CancelTypeEnum;


/**
 * Returns data from transactional cancel sections
 * 
 */
public class CancelRequestResponse {

    private BigDecimal refundAmount;
    private OrderModificationRecordEntryModel orderModificationRecordEntryModel;
    private List<PaymentTransactionEntryModel> followOnRefundPaymentList;
    private List<PaymentTransactionEntryDTO> followOnRefundPaymentDataList;
    private PaymentTransactionDTO standaloneRefundPaymentData;
    private BigDecimal refundedAmount;
    private BigDecimal refundAmountRemaining;
    private CancelTypeEnum cancelType;

    /**
     * If refund failed return the message.
     * 
     * @return failure message or null if refund success
     */
    public String getRefundFailureMessage() {

        if (CollectionUtils.isNotEmpty(getFollowOnRefundPaymentDataList())) {
            return createRefundFailureMessageForRefundFollowOn();
        }

        if (getStandaloneRefundPaymentData() != null) {
            for (final PaymentTransactionEntryDTO entry : getStandaloneRefundPaymentData().getEntries()) {
                final String error = refundFailureMessageForEntry(entry);
                if (error != null) {
                    return error;
                }
            }
        }

        return null;
    }

    /**
     * create the refund failure message base on the following conditions, failure message is used for roll back, if it
     * is null, it will trigger the roll back. Because the multiple payment entry, any entry succeed the refund, we
     * cannot roll back, so we set message null
     * 
     * @return failure message
     */
    private String createRefundFailureMessageForRefundFollowOn() {
        final StringBuilder failureMessage = new StringBuilder();
        for (final PaymentTransactionEntryDTO entryDto : getFollowOnRefundPaymentDataList()) {
            if (TransactionStatus.ACCEPTED.toString().equalsIgnoreCase(entryDto.getTransactionStatus())) {
                return null;
            }
            else {
                failureMessage.append("Transaction status is " + entryDto.getTransactionStatus() + ", details: "
                        + entryDto.getTransactionStatusDetails() + " | ");
            }
        }
        return failureMessage.toString();
    }

    private String refundFailureMessageForEntry(final PaymentTransactionEntryDTO entry) {

        if (!entry.getTransactionStatus().equals(
                de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED.toString())) {

            return "Transaction status is " + entry.getTransactionStatus() + ", details: "
                    + entry.getTransactionStatusDetails();
        }
        return null;
    }



    /**
     * @return the refundAmount
     */
    public BigDecimal getRefundAmount() {
        return refundAmount;
    }

    /**
     * @param refundAmount
     *            the refundAmount to set
     */
    public void setRefundAmount(final BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }

    /**
     * @return the orderModificationRecordEntryModel
     */
    public OrderModificationRecordEntryModel getOrderModificationRecordEntryModel() {
        return orderModificationRecordEntryModel;
    }

    /**
     * @param orderModificationRecordEntryModel
     *            the orderModificationRecordEntryModel to set
     */
    public void setOrderModificationRecordEntryModel(
            final OrderModificationRecordEntryModel orderModificationRecordEntryModel) {
        this.orderModificationRecordEntryModel = orderModificationRecordEntryModel;
    }

    /**
     * @return the standaloneRefundPaymentData
     */
    public PaymentTransactionDTO getStandaloneRefundPaymentData() {
        return standaloneRefundPaymentData;
    }

    /**
     * @param standaloneRefundPaymentData
     *            the standaloneRefundPaymentData to set
     */
    public void setStandaloneRefundPaymentData(final PaymentTransactionDTO standaloneRefundPaymentData) {
        this.standaloneRefundPaymentData = standaloneRefundPaymentData;
    }

    /**
     * @return the followOnRefundPaymentDataList
     */
    public List<PaymentTransactionEntryDTO> getFollowOnRefundPaymentDataList() {
        return followOnRefundPaymentDataList;
    }

    /**
     * @param followOnRefundPaymentDataList
     *            the followOnRefundPaymentDataList to set
     */
    public void setFollowOnRefundPaymentDataList(final List<PaymentTransactionEntryDTO> followOnRefundPaymentDataList) {
        this.followOnRefundPaymentDataList = followOnRefundPaymentDataList;
    }

    /**
     * @return the refundedAmount
     */
    public BigDecimal getRefundedAmount() {
        return refundedAmount;
    }

    /**
     * @param refundedAmount
     *            the refundedAmount to set
     */
    public void setRefundedAmount(final BigDecimal refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    /**
     * @return the followOnRefundPaymentList
     */
    public List<PaymentTransactionEntryModel> getFollowOnRefundPaymentList() {
        return followOnRefundPaymentList;
    }

    /**
     * @param followOnRefundPaymentList
     *            the followOnRefundPaymentList to set
     */
    public void setFollowOnRefundPaymentList(final List<PaymentTransactionEntryModel> followOnRefundPaymentList) {
        this.followOnRefundPaymentList = followOnRefundPaymentList;
    }

    /**
     * @return the refundAmountRemaining
     */
    public BigDecimal getRefundAmountRemaining() {
        return refundAmountRemaining;
    }

    /**
     * @param refundAmountRemaining
     *            the refundAmountRemaining to set
     */
    public void setRefundAmountRemaining(final BigDecimal refundAmountRemaining) {
        this.refundAmountRemaining = refundAmountRemaining;
    }

    /**
     * @return the cancelType
     */
    public CancelTypeEnum getCancelType() {
        return cancelType;
    }

    /**
     * @param cancelType
     *            the cancelType to set
     */
    public void setCancelType(final CancelTypeEnum cancelType) {
        this.cancelType = cancelType;
    }

}

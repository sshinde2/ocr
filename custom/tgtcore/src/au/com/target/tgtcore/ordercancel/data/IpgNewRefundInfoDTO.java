/**
 * 
 */
package au.com.target.tgtcore.ordercancel.data;

/**
 * @author bhuang3
 *
 */
public class IpgNewRefundInfoDTO {

    private Double amount;
    private String receiptNo;

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final Double amount) {
        this.amount = amount;
    }

    /**
     * @return the receiptNo
     */
    public String getReceiptNo() {
        return receiptNo;
    }

    /**
     * @param receiptNo
     *            the receiptNo to set
     */
    public void setReceiptNo(final String receiptNo) {
        this.receiptNo = receiptNo;
    }


}

/**
 * 
 */
package au.com.target.tgtcore.ordercancel.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * POJO for retaining and passing payment transaction details out of cancel transactions.
 * 
 */
public class PaymentTransactionDTO {

    private String code;
    private BigDecimal amount;
    private String provider;
    private List<PaymentTransactionEntryDTO> entries = new ArrayList<>();

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider
     *            the provider to set
     */
    public void setProvider(final String provider) {
        this.provider = provider;
    }

    /**
     * @return the entries
     */
    public List<PaymentTransactionEntryDTO> getEntries() {
        return entries;
    }

    /**
     * @param entries
     *            the entries to set
     */
    public void setEntries(final List<PaymentTransactionEntryDTO> entries) {
        this.entries = entries;
    }


}

/**
 * 
 */
package au.com.target.tgtcore.ordercancel.data;

import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;

import java.math.BigDecimal;
import java.util.Date;


/**
 * POJO for retaining and passing payment transaction entry details out of cancel transactions.
 * 
 */
public class PaymentTransactionEntryDTO {

    private String code;
    private String transactionStatus;
    private String transactionStatusDetails;
    private Date time;
    private BigDecimal amount;
    private PaymentTransactionType type;
    private String declineDetails;
    private PaymentInfoModel ipgPaymentInfo;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the transactionStatus
     */
    public String getTransactionStatus() {
        return transactionStatus;
    }

    /**
     * @param transactionStatus
     *            the transactionStatus to set
     */
    public void setTransactionStatus(final String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    /**
     * @return the transactionStatusDetails
     */
    public String getTransactionStatusDetails() {
        return transactionStatusDetails;
    }

    /**
     * @param transactionStatusDetails
     *            the transactionStatusDetails to set
     */
    public void setTransactionStatusDetails(final String transactionStatusDetails) {
        this.transactionStatusDetails = transactionStatusDetails;
    }

    /**
     * @return the time
     */
    public Date getTime() {
        return time;
    }

    /**
     * @param time
     *            the time to set
     */
    public void setTime(final Date time) {
        this.time = time;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the type
     */
    public PaymentTransactionType getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final PaymentTransactionType type) {
        this.type = type;
    }

    /**
     * @return the declineDetails
     */
    public String getDeclineDetails() {
        return declineDetails;
    }

    /**
     * @param declineDetails
     *            the declineDetails to set
     */
    public void setDeclineDetails(final String declineDetails) {
        this.declineDetails = declineDetails;
    }

    /**
     * @return the ipgPaymentInfo
     */
    public PaymentInfoModel getIpgPaymentInfo() {
        return ipgPaymentInfo;
    }

    /**
     * @param ipgPaymentInfo
     *            the ipgPaymentInfo to set
     */
    public void setIpgPaymentInfo(final PaymentInfoModel ipgPaymentInfo) {
        this.ipgPaymentInfo = ipgPaymentInfo;
    }


}

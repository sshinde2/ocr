/**
 * 
 */
package au.com.target.tgtcore.ordercancel.data;

import java.math.BigDecimal;

/**
 * @author rmcalave
 *
 */
public class RefundInfoDTO {
    private BigDecimal refundAmountRemaining;

    private String message;

    /**
     * @return the refundAmountRemaining
     */
    public BigDecimal getRefundAmountRemaining() {
        return refundAmountRemaining;
    }

    /**
     * @param refundAmountRemaining
     *            the refundAmountRemaining to set
     */
    public void setRefundAmountRemaining(final BigDecimal refundAmountRemaining) {
        this.refundAmountRemaining = refundAmountRemaining;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(final String message) {
        this.message = message;
    }

}

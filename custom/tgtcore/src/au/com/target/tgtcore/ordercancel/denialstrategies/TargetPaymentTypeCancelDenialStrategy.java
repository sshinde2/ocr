/**
 * 
 */
package au.com.target.tgtcore.ordercancel.denialstrategies;

import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.ordercancel.OrderCancelDenialReason;
import de.hybris.platform.ordercancel.OrderCancelDenialStrategy;
import de.hybris.platform.ordercancel.impl.denialstrategies.AbstractCancelDenialStrategy;
import de.hybris.platform.ordercancel.model.OrderCancelConfigModel;

import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;


/**
 * @author rmcalave
 * 
 */
public class TargetPaymentTypeCancelDenialStrategy extends AbstractCancelDenialStrategy implements
        OrderCancelDenialStrategy {

    /**
     * Determines if cancellation is allowed based on the payment type of the order.
     * 
     * Cancellation is not allowed if the payment info is from a pin pad, and the card type used for payment is a
     * BasicsCard.
     * 
     * @see de.hybris.platform.ordercancel.OrderCancelDenialStrategy#getCancelDenialReason(de.hybris.platform.ordercancel.model.OrderCancelConfigModel,
     *      de.hybris.platform.core.model.order.OrderModel, de.hybris.platform.core.model.security.PrincipalModel,
     *      boolean, boolean)
     */
    @Override
    public OrderCancelDenialReason getCancelDenialReason(final OrderCancelConfigModel configuration,
            final OrderModel order,
            final PrincipalModel requestor, final boolean partialCancel, final boolean partialEntryCancel) {

        if (order.getPaymentInfo() instanceof PinPadPaymentInfoModel) {
            final PinPadPaymentInfoModel pinPadPaymentInfo = (PinPadPaymentInfoModel)order.getPaymentInfo();

            if (CreditCardType.BASICSCARD.equals(pinPadPaymentInfo.getCardType())) {
                return getReason();
            }
        }

        return null;
    }

}

/**
 * 
 */
package au.com.target.tgtcore.ordercancel.denialstrategies;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.ordercancel.OrderCancelDenialReason;
import de.hybris.platform.ordercancel.OrderCancelDenialStrategy;
import de.hybris.platform.ordercancel.impl.denialstrategies.AbstractCancelDenialStrategy;
import de.hybris.platform.ordercancel.model.OrderCancelConfigModel;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;


/**
 * @author Nandini
 *
 */
public class TargetSalesChannelCancelDenialStrategy extends AbstractCancelDenialStrategy implements
        OrderCancelDenialStrategy {

    private TargetSalesApplicationConfigService salesApplicationConfigService;

    /**
     * Determines if cancellation is allowed based on the sales application of the order.
     * 
     * Cancellation is not allowed if the sales application of the order is Trade Me
     * 
     * @see de.hybris.platform.ordercancel.OrderCancelDenialStrategy#getCancelDenialReason(de.hybris.platform.ordercancel.model.OrderCancelConfigModel,
     *      de.hybris.platform.core.model.order.OrderModel, de.hybris.platform.core.model.security.PrincipalModel,
     *      boolean, boolean)
     */
    @Override
    public OrderCancelDenialReason getCancelDenialReason(final OrderCancelConfigModel paramOrderCancelConfigModel,
            final OrderModel order, final PrincipalModel requestor, final boolean partialCancel,
            final boolean partialEntryCancel) {
        if (salesApplicationConfigService
                .isDenyCancellation(order.getSalesApplication())) {
            return getReason();
        }
        return null;
    }

    /**
     * @param salesApplicationConfigService
     *            the salesApplicationConfigService to set
     */
    @Required
    public void setSalesApplicationConfigService(final TargetSalesApplicationConfigService salesApplicationConfigService) {
        this.salesApplicationConfigService = salesApplicationConfigService;
    }


}
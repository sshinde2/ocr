/**
 * 
 */
package au.com.target.tgtcore.ordercancel.discountstrategy;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;



/**
 * @author rmcalave
 * 
 */
public interface DiscountValueCorrectionStrategy {

    /**
     * Updates the Discount Values applied to the provided order entry based on modifications to the quantity.
     * 
     * @param orderEntry
     *            The order entry being modified
     * @param previousQuantity
     *            The previous quantity of the items in the order entry
     */
    void correctDiscountValues(AbstractOrderEntryModel orderEntry, long previousQuantity);

}
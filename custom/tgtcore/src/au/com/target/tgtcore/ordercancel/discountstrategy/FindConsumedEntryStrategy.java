/**
 * 
 */
package au.com.target.tgtcore.ordercancel.discountstrategy;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.PromotionResultModel;


/**
 * Strategy for correctly finding the next order entry consumed record to remove or update
 * 
 */
public interface FindConsumedEntryStrategy {

    /**
     * Find the next order entry consumed record to remove or update
     * 
     * @param dealResult
     * @param orderEntry
     * @return the found order entry consumed record, or null if not found
     */
    PromotionOrderEntryConsumedModel findConsumedEntry(final PromotionResultModel dealResult,
            final AbstractOrderEntryModel orderEntry);

}

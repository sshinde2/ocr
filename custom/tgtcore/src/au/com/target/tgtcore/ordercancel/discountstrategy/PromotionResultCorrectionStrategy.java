/**
 * 
 */
package au.com.target.tgtcore.ordercancel.discountstrategy;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;


/**
 * @author sbryan6
 * 
 */
public interface PromotionResultCorrectionStrategy {


    /**
     * Update the records in the PromotionResult corresponding to an order entry that has been modified by a cancel
     * 
     * For deals, we will be setting newQty to zero for each action which will result in deletion of the records.<br/>
     * 
     * For old TMD, we may be deleting, or just reducing the qty on an adjust action and consumed entry<br/>
     * and update of the action adjust amount.
     * 
     * @param actionId
     *            GUID to identify the PromotionResult action records corresponding to a removed discount value
     * @param orderEntry
     *            The order entry being modified
     * @param unitDiscountAmount
     *            The discount amount per unit from the discount value
     * @param newQty
     *            The new quantity on the adjustment record, zero to delete the records
     * 
     */
    void correctPromotionResultRecords(String actionId, AbstractOrderEntryModel orderEntry, double unitDiscountAmount,
            int newQty, boolean updateConsumedEntries);

}

/**
 * 
 */
package au.com.target.tgtcore.ordercancel.discountstrategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.promotions.model.AbstractPromotionActionModel;
import de.hybris.platform.promotions.model.PromotionOrderEntryAdjustActionModel;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.ordercancel.discountstrategy.FindConsumedEntryStrategy;
import au.com.target.tgtcore.ordercancel.discountstrategy.PromotionResultCorrectionStrategy;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * @author sbryan6
 * 
 */
public class PromotionResultCorrectionStrategyImpl implements PromotionResultCorrectionStrategy {

    private static final Logger LOG = Logger.getLogger(PromotionResultCorrectionStrategyImpl.class);

    private ModelService modelService;

    private FindConsumedEntryStrategy findConsumedEntryStrategy;


    /* (non-Javadoc)
     * @see au.com.target.tgtcore.ordercancel.discountstrategy.PromotionResultCorrectionStrategy#correctPromotionResultRecords(java.util.Collection, de.hybris.platform.core.model.order.AbstractOrderEntryModel, long)
     */
    @Override
    public void correctPromotionResultRecords(final String actionId, final AbstractOrderEntryModel orderEntry,
            final double unitDiscountAmount, final int newQty, final boolean updateConsumedEntries) {

        Assert.notNull(actionId, "actionId should not be null");
        Assert.notNull(orderEntry, "orderEntry should not be null");

        final AbstractOrderModel order = orderEntry.getOrder();
        Assert.notNull(order, "order should not be null");
        final String orderCode = order.getCode();

        // Get product code just for logging
        final String productCode = orderEntry.getProduct() == null ? "" : orderEntry.getProduct().getCode();

        if (newQty < 0) {
            LOG.error(SplunkLogFormatter.formatOrderMessage(
                    "Negative qty " + newQty + " for action with guid: " + actionId,
                    TgtutilityConstants.ErrorCode.ERR_TGTCORE, orderCode));
            return;
        }


        // Find the adjust action
        final PromotionOrderEntryAdjustActionModel adjustAction = getAdjustActionForOrder(actionId, order);
        if (adjustAction == null) {
            LOG.error(SplunkLogFormatter.formatOrderMessage(
                    "Could not find the adjust action for guid: " + actionId,
                    TgtutilityConstants.ErrorCode.ERR_TGTCORE, orderCode));
            return;
        }

        // Remember the deal result, so we can use it to find the consumed record
        final PromotionResultModel dealResult = adjustAction.getPromotionResult();
        if (dealResult == null) {
            LOG.error(SplunkLogFormatter.formatOrderMessage(
                    "Could not find the deal result for action with guid: " + actionId,
                    TgtutilityConstants.ErrorCode.ERR_TGTCORE, orderCode));
            return;
        }

        // Find the consumed order entry record
        if (updateConsumedEntries) {
            final PromotionOrderEntryConsumedModel consumedEntry = findConsumedEntryStrategy.findConsumedEntry(
                    dealResult,
                    orderEntry);
            if (consumedEntry == null) {
                LOG.error(SplunkLogFormatter.formatOrderMessage(
                        "Could not find consumed entry for deal: " + dealResult.getPk() + ", product: "
                                + productCode,
                        TgtutilityConstants.ErrorCode.ERR_TGTCORE, orderCode));
                return;
            }

            // Now do the updates/deletes on the records depending on given qty
            if (newQty == 0)
            {
                removeConsumedEntry(consumedEntry);
            }
            else {
                updateConsumedEntry(consumedEntry, newQty);
            }
        }

        if (newQty == 0) {
            removeAdjustAction(adjustAction);
        }
        else {
            updateAdjustAction(adjustAction, newQty, unitDiscountAmount);
        }

        // At this stage we're not updating the instance count on the deal result. It doesn't seem to break anything.
        modelService.refresh(dealResult);

    }


    /**
     * Find the action with given guid in the given order
     * 
     * @param actionId
     * @param order
     * @return found action or null if none found
     */
    protected PromotionOrderEntryAdjustActionModel getAdjustActionForOrder(final String actionId,
            final AbstractOrderModel order) {

        Assert.notNull(actionId, "actionId should not be null");
        Assert.notNull(order, "order should not be null");

        final Set<PromotionResultModel> promotionResults = order.getAllPromotionResults();
        if (CollectionUtils.isEmpty(promotionResults)) {
            return null;
        }

        PromotionOrderEntryAdjustActionModel adjustAction = null;
        for (final PromotionResultModel result : promotionResults) {

            adjustAction = getAdjustActionForPromotionResult(actionId, result);
            if (adjustAction != null) {
                return adjustAction;
            }
        }

        return null;
    }


    /**
     * Find adjust action with given guid in the given Promotion Result
     * 
     * @param actionId
     * @param result
     * @return adjust action or null if not found
     */
    protected PromotionOrderEntryAdjustActionModel getAdjustActionForPromotionResult(final String actionId,
            final PromotionResultModel result) {

        Assert.notNull(actionId, "actionId should not be null");
        Assert.notNull(result, "result should not be null");

        if (result.getActions() != null) {

            for (final AbstractPromotionActionModel action : result.getActions()) {

                if (action.getGuid().equals(actionId)) {

                    if (!(action instanceof PromotionOrderEntryAdjustActionModel)) {

                        LOG.error("Expected action to be instance of PromotionOrderEntryAdjustActionModel: " + actionId);
                        return null;
                    }

                    return (PromotionOrderEntryAdjustActionModel)action;
                }
            }
        }

        return null;
    }


    private void removeAdjustAction(final PromotionOrderEntryAdjustActionModel adjustAction) {
        modelService.remove(adjustAction);
    }

    private void updateAdjustAction(final PromotionOrderEntryAdjustActionModel adjustAction,
            final int newQty, final double unitDiscountAmount) {

        adjustAction.setOrderEntryQuantity(Long.valueOf(newQty));

        adjustAction.setAmount(Double.valueOf(0 - (unitDiscountAmount * newQty)));

        modelService.save(adjustAction);
    }

    private void removeConsumedEntry(final PromotionOrderEntryConsumedModel consumedEntry) {
        modelService.remove(consumedEntry);
    }

    private void updateConsumedEntry(final PromotionOrderEntryConsumedModel consumedEntry,
            final int newQty) {

        consumedEntry.setQuantity(Long.valueOf(newQty));
        modelService.save(consumedEntry);
    }


    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    @Required
    public void setFindConsumedEntryStrategy(final FindConsumedEntryStrategy findConsumedEntryStrategy) {
        this.findConsumedEntryStrategy = findConsumedEntryStrategy;
    }

}

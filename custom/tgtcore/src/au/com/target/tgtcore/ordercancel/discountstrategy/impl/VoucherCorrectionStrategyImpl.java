/**
 * 
 */
package au.com.target.tgtcore.ordercancel.discountstrategy.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.voucher.model.VoucherModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.flybuys.service.FlybuysDiscountService;
import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.ordercancel.TargetCancelService;
import au.com.target.tgtcore.ordercancel.discountstrategy.VoucherCorrectionStrategy;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;


/**
 * @author rmcalave
 * 
 */
public class VoucherCorrectionStrategyImpl implements VoucherCorrectionStrategy {

    private TargetCancelService targetCancelService;

    private TargetVoucherService targetVoucherService;

    private FlybuysDiscountService flybuysDiscountService;

    /**
     * Correct any vouchers on the order to ensure that the order total wont fall below 0.
     * 
     * Removes or updates the value of any voucher DiscountValues on the order, and removes any vouchers that no longer
     * have a DiscountValue on the order.
     * 
     * Recalculates the order before finishing.
     * 
     * @see au.com.target.tgtcore.ordercancel.discountstrategy.VoucherCorrectionStrategy#correctAppliedVouchers(de.hybris.platform.core.model.order.OrderModel)
     */
    @Override
    public void correctAppliedVouchers(final OrderModel orderModel) {
        BigDecimal totalPrice = BigDecimal.valueOf(orderModel.getTotalPrice().doubleValue());
        totalPrice = totalPrice.setScale(2, RoundingMode.HALF_UP);

        // If the order total is greater than 0 we don't need to change anything.
        if (totalPrice.compareTo(BigDecimal.ZERO) > 0) {
            return;
        }

        // If the order has no discounts there is nothing to change.
        if (CollectionUtils.isEmpty(orderModel.getDiscounts())) {
            return;
        }

        final List<DiscountValue> newGlobalDiscountValues = new ArrayList<>();
        final List<DiscountModel> newDiscounts = new ArrayList<>();

        // If the order total is 0, we want to remove all vouchers but
        // keep any other discounts.
        if (totalPrice.compareTo(BigDecimal.ZERO) == 0) {
            for (final DiscountValue discountValue : orderModel.getGlobalDiscountValues()) {
                if (isDiscountValueForFlybuys(discountValue, orderModel.getDiscounts())
                        || !isDiscountValueForVoucher(discountValue, orderModel.getDiscounts())) {
                    newGlobalDiscountValues.add(discountValue);
                }
            }

            for (final DiscountModel discount : orderModel.getDiscounts()) {
                if (discount instanceof FlybuysDiscountModel || !(discount instanceof VoucherModel)) {
                    newDiscounts.add(discount);
                }
            }
        }
        // Here the order total must be below 0. In this case we need to either adjust or remove
        // vouchers in order to make the order total 0.
        else {
            // Loop over the global discount values and adjust them until the order will calculate
            // to 0.
            BigDecimal newDiscountAmount = totalPrice;
            BigDecimal fbRefundAmount = BigDecimal.ZERO;
            for (final DiscountValue discountValue : orderModel.getGlobalDiscountValues()) {
                if (isDiscountValueForVoucher(discountValue, orderModel.getDiscounts())) {
                    if (newDiscountAmount.compareTo(BigDecimal.ZERO) < 0) { // We need to correct vouchers while newDiscountAmount is < 0. If it's not at this point we can discard all other vouchers
                        fbRefundAmount = newDiscountAmount.negate();
                        newDiscountAmount = BigDecimal.valueOf(discountValue.getValue()).add(newDiscountAmount); // Because totalPrice is negative we'll add it to subtract it
                        if (newDiscountAmount.compareTo(BigDecimal.ZERO) > 0
                                || isDiscountValueForFlybuys(discountValue, orderModel.getDiscounts())) {
                            final DiscountValue newDiscountValue = new DiscountValue(discountValue.getCode(),
                                    newDiscountAmount.doubleValue(),
                                    discountValue.isAbsolute(), discountValue.getCurrencyIsoCode());
                            newGlobalDiscountValues.add(newDiscountValue);
                        }
                    }
                    if (isDiscountValueForFlybuys(discountValue, orderModel.getDiscounts())) {
                        final FlybuysDiscountModel flybuysDiscountModel = targetVoucherService
                                .getFlybuysDiscountForOrder(orderModel);
                        final BigDecimal refundFlybuysPoints = flybuysDiscountService.getFlybuysPointsToRefund(
                                flybuysDiscountModel, fbRefundAmount);
                        final int originalFlybuysConsumed = orderModel.getFlybuysPointsConsumed()
                                .intValue();
                        orderModel.setFlybuysPointsConsumed(Integer.valueOf(originalFlybuysConsumed
                                - refundFlybuysPoints.intValue()));
                    }
                }
                else {
                    newGlobalDiscountValues.add(discountValue);
                }
            }

            // Now loop over the discounts and add any vouchers that still have discount values
            // on the order. For any non-voucher discounts, add them too.
            for (final DiscountModel discount : orderModel.getDiscounts()) {
                if (discount instanceof VoucherModel) {
                    for (final DiscountValue globalDiscountValue : newGlobalDiscountValues) {
                        if (discount.getCode().equals(globalDiscountValue.getCode())) {
                            newDiscounts.add(discount);
                        }
                    }
                }
                else {
                    newDiscounts.add(discount);
                }
            }
        }

        orderModel.setGlobalDiscountValues(newGlobalDiscountValues);
        orderModel.setDiscounts(newDiscounts);

        targetCancelService.recalculateOrder(orderModel);
    }

    private boolean isDiscountValueForVoucher(final DiscountValue discountValue, final List<DiscountModel> discounts) {
        for (final DiscountModel discount : discounts) {
            if (discount instanceof VoucherModel && discount.getCode().equals(discountValue.getCode())) {
                return true;
            }
        }
        return false;
    }

    private boolean isDiscountValueForFlybuys(final DiscountValue discountValue, final List<DiscountModel> discounts) {
        for (final DiscountModel discount : discounts) {
            if (discount instanceof FlybuysDiscountModel
                    && discount.getCode().equals(discountValue.getCode())) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param targetCancelService
     *            the targetCancelService to set
     */
    @Required
    public void setTargetCancelService(final TargetCancelService targetCancelService) {
        this.targetCancelService = targetCancelService;
    }

    /**
     * @param targetVoucherService
     *            the targetVoucherService to set
     */
    @Required
    public void setTargetVoucherService(final TargetVoucherService targetVoucherService) {
        this.targetVoucherService = targetVoucherService;
    }

    /**
     * @param flybuysDiscountService
     *            the flybuysDiscountService to set
     */
    @Required
    public void setFlybuysDiscountService(final FlybuysDiscountService flybuysDiscountService) {
        this.flybuysDiscountService = flybuysDiscountService;
    }
}

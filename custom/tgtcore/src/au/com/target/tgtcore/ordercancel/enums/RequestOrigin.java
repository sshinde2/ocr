/**
 * 
 */
package au.com.target.tgtcore.ordercancel.enums;

/**
 * Enum to hold the request origin.
 * 
 * @author Nandini
 *
 */
public enum RequestOrigin {
    WAREHOUSE, CSCOCKPIT, SYSTEM;
}

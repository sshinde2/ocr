package au.com.target.tgtcore.ordercancel.impl;

import de.hybris.platform.basecommerce.enums.OrderCancelEntryStatus;
import de.hybris.platform.basecommerce.enums.OrderEntryStatus;
import de.hybris.platform.basecommerce.enums.OrderModificationEntryStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.exceptions.OrderCancelRecordsHandlerException;
import de.hybris.platform.ordercancel.impl.DefaultOrderCancelRecordsHandler;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordModel;
import de.hybris.platform.ordercancel.model.OrderEntryCancelRecordEntryModel;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.ordercancel.TargetOrderCancelRequest;
import au.com.target.tgtcore.util.OrderHandlerUtil;


/**
 * This class is created to only prevent creating {@link OrderEntryModificationRecordEntryModel} for dead
 * {@link OrderEntryModel}
 */
public class TargetOrderCancelRecordsHandler extends DefaultOrderCancelRecordsHandler {

    private UserService userService;
    private OrderHandlerUtil orderHandlerUtil;

    @Override
    protected OrderCancelRecordEntryModel createCancelRecordEntry(final OrderCancelRequest request,
            final OrderModel order,
            final OrderCancelRecordModel cancelRecord, final OrderHistoryEntryModel snapshot,
            final Map<Integer, AbstractOrderEntryModel> originalOrderEntriesMapping)
            throws OrderCancelRecordsHandlerException
    {
        final OrderCancelRecordEntryModel cancelRecordEntry = getModelService().create(
                OrderCancelRecordEntryModel.class);
        cancelRecordEntry.setTimestamp(new Date());
        cancelRecordEntry.setCode(generateEntryCode(snapshot));
        cancelRecordEntry.setOriginalVersion(snapshot);
        cancelRecordEntry.setModificationRecord(cancelRecord);
        cancelRecordEntry.setPrincipal(userService.getCurrentUser());
        cancelRecordEntry.setOwner(cancelRecord);
        cancelRecordEntry.setStatus(OrderModificationEntryStatus.INPROGRESS);
        cancelRecordEntry.setCancelResult(request.isPartialCancel() ? OrderCancelEntryStatus.PARTIAL
                : OrderCancelEntryStatus.FULL);
        cancelRecordEntry.setCancelReason(request.getCancelReason());
        cancelRecordEntry.setNotes(request.getNotes());

        if (request instanceof TargetOrderCancelRequest) {
            final TargetOrderCancelRequest requestExt = (TargetOrderCancelRequest)request;
            cancelRecordEntry.setRefundedShippingAmount(requestExt.getShippingAmountToRefund() != null ? requestExt
                    .getShippingAmountToRefund() : Double.valueOf(0.0d));
        }

        getModelService().save(cancelRecordEntry);

        final List<OrderEntryModificationRecordEntryModel> orderEntriesRecords = new ArrayList<>();
        for (final OrderCancelEntry cancelRequestEntry : request.getEntriesToCancel())
        {
            // Logic to skip dead entries
            if (OrderEntryStatus.DEAD.equals(cancelRequestEntry.getOrderEntry().getQuantityStatus())) {
                continue;
            }
            final OrderEntryCancelRecordEntryModel orderEntryRecordEntry = getModelService().create(
                    OrderEntryCancelRecordEntryModel.class);
            orderEntryRecordEntry.setCode(cancelRequestEntry.getOrderEntry().getPk().toString());

            if (!request.isPartialCancel()) {
                orderEntryRecordEntry.setCancelRequestQuantity(Integer.valueOf(cancelRequestEntry.getOrderEntry()
                        .getQuantity().intValue()));
            }
            else {
                orderEntryRecordEntry.setCancelRequestQuantity(Integer.valueOf((int)cancelRequestEntry
                        .getCancelQuantity()));
            }
            orderEntryRecordEntry.setModificationRecordEntry(cancelRecordEntry);
            orderEntryRecordEntry.setOrderEntry((OrderEntryModel)cancelRequestEntry.getOrderEntry());
            orderEntryRecordEntry.setOriginalOrderEntry(getOriginalOrderEntry(originalOrderEntriesMapping,
                    cancelRequestEntry));
            orderEntryRecordEntry.setNotes(cancelRequestEntry.getNotes());
            orderEntryRecordEntry.setCancelReason(cancelRequestEntry.getCancelReason());
            getModelService().save(orderEntryRecordEntry);
            orderEntriesRecords.add(orderEntryRecordEntry);
        }
        cancelRecordEntry.setOrderEntriesModificationEntries(orderEntriesRecords);
        getModelService().save(cancelRecordEntry);

        return cancelRecordEntry;
    }


    @Override
    protected OrderHistoryEntryModel createSnaphot(final OrderModel order, final OrderModel version,
            final String description, final PrincipalModel requestor)
    {
        return orderHandlerUtil.createOrderSnapshot(order, version, description, requestor);
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Override
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
        super.setUserService(userService);
    }

    /**
     * @param orderHandlerUtil
     *            the orderHandlerUtil to set
     */
    @Required
    public void setOrderHandlerUtil(final OrderHandlerUtil orderHandlerUtil) {
        this.orderHandlerUtil = orderHandlerUtil;
    }

}

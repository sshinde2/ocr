/**
 * 
 */
package au.com.target.tgtcore.ordercancel.impl;

import de.hybris.platform.basecommerce.enums.OrderCancelState;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.ordercancel.CancelDecision;
import de.hybris.platform.ordercancel.OrderCancelDeniedException;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.OrderCancelRequestExecutor;
import de.hybris.platform.ordercancel.impl.DefaultOrderCancelService;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;

import au.com.target.tgtcore.ordercancel.impl.executors.TargetCancelRequestExecutor;


/**
 * Extends the Default hybris service so we can override methods
 * 
 */
public class TargetOrderCancelService extends DefaultOrderCancelService {

    /* 
     * Override the default implementation so that the creation of the record entry model can happen in the executor,
     * inside a transaction.
     */
    @Override
    public OrderCancelRecordEntryModel requestOrderCancel(final OrderCancelRequest orderCancelRequest,
            final PrincipalModel requestor) throws OrderCancelException
    {
        OrderCancelRecordEntryModel result = null;

        final CancelDecision cancelDecision = isCancelPossible(orderCancelRequest.getOrder(), requestor,
                orderCancelRequest
                        .isPartialCancel(), orderCancelRequest.isPartialEntryCancel());
        if (cancelDecision.isAllowed())
        {
            final OrderCancelState currentCancelState = getStateMappingStrategy().getOrderCancelState(
                    orderCancelRequest.getOrder());

            final OrderCancelRequestExecutor ocre = getRequestExecutorsMap().get(currentCancelState);

            if (ocre == null || !(ocre instanceof TargetCancelRequestExecutor))
            {
                throw new IllegalStateException("Cannot find request executor for cancel state: "
                        + currentCancelState.name());
            }

            // We assume it will be a TargetCancelRequestExecutor, which has a different method to call
            final TargetCancelRequestExecutor targetExecutor = (TargetCancelRequestExecutor)ocre;
            result = targetExecutor.processCancelRequest(orderCancelRequest, requestor);

        }
        else
        {
            throw new OrderCancelDeniedException(orderCancelRequest.getOrder().getCode(), cancelDecision);
        }

        return result;
    }

}

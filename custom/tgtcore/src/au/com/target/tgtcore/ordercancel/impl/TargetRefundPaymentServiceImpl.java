package au.com.target.tgtcore.ordercancel.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.ordercancel.TargetRefundPaymentService;
import au.com.target.tgtcore.ordercancel.data.IpgNewRefundInfoDTO;
import au.com.target.tgtcore.ordercancel.data.PaymentTransactionDTO;
import au.com.target.tgtcore.ordercancel.data.PaymentTransactionEntryDTO;
import au.com.target.tgtcore.ordercancel.data.RefundInfoDTO;
import au.com.target.tgtpayment.exceptions.PaymentException;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;
import au.com.target.tgtpayment.service.TargetPaymentService;
import au.com.target.tgtpayment.util.PaymentTransactionHelper;
import au.com.target.tgtpayment.util.PriceCalculator;
import au.com.target.tgtutility.constants.TgtutilityConstants.ErrorCode;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * Responsible for processing a refund using {@link TargetPaymentService}
 */
public class TargetRefundPaymentServiceImpl implements TargetRefundPaymentService {

    private static final Logger LOG = Logger.getLogger(TargetRefundPaymentServiceImpl.class);
    private static final String PARTIAL_REFUND_SUCCESS = "Total amount need to be refunded={0}. System has already succeeded to refund amount={1} automatically, another amount={2} need to be refunded manually";
    private static final String FULL_REFUND_FAILURE = "System failed to refund automatically, refund amount={0} need to be refunded manually";

    private TargetPaymentService targetPaymentService;
    private ModelService modelService;
    private KeyGenerator vpcCaptureCodeGenerator;

    @Override
    public List<PaymentTransactionEntryModel> performFollowOnRefund(final OrderModel order,
            final BigDecimal refundAmount) {

        Assert.notNull(order, "Order cannot be null");
        Assert.notNull(refundAmount, "refundAmount cannot be null");
        final PaymentTransactionModel captureTransaction = findCaptureTransaction(order);
        final List<PaymentTransactionEntryModel> transactionEntryModelList = targetPaymentService.refundFollowOn(
                captureTransaction,
                refundAmount);
        modelService.refresh(order);
        return transactionEntryModelList;
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtcore.ordercancel.TargetRefundPaymentService#convertPaymentTransactionEntries(java.util.List)
     */
    @Override
    public List<PaymentTransactionEntryDTO> convertPaymentTransactionEntries(
            final List<PaymentTransactionEntryModel> paymentTransactionEntries) {
        final List<PaymentTransactionEntryDTO> paymentTransactionEntryDTOList = new ArrayList<>();

        for (final PaymentTransactionEntryModel entry : paymentTransactionEntries) {
            final PaymentTransactionEntryDTO entryDto = convertPaymentTransactionEntry(entry);
            paymentTransactionEntryDTOList.add(entryDto);
        }

        return paymentTransactionEntryDTOList;
    }

    @Override
    public PaymentTransactionDTO performStandaloneRefund(final OrderModel order, final BigDecimal refundAmount,
            final PaymentInfoModel newPayment) {

        Assert.notNull(order, "Order cannot be null");
        Assert.notNull(refundAmount, "refundAmount cannot be null");
        Assert.notNull(newPayment, "newPayment cannot be null");

        // Capture step
        final String captureOrderId = String.valueOf(vpcCaptureCodeGenerator.generate());
        final PaymentTransactionEntryModel captureEntry = targetPaymentService.refundStandaloneCapture(order,
                newPayment, captureOrderId);


        // retrieve and convert the payment transaction 
        final PaymentTransactionModel captureTransaction = captureEntry.getPaymentTransaction();
        if (captureTransaction == null) {
            throw new PaymentException("captureTransaction is null after refundStandaloneCapture");
        }

        final PaymentTransactionDTO paymentTransactionData = convertPaymentTransaction(captureTransaction);
        paymentTransactionData.getEntries().add(convertPaymentTransactionEntry(captureEntry));


        // Check success before proceeding with the refund part
        if (!captureEntry.getTransactionStatus().equals(TransactionStatus.ACCEPTED.toString())) {
            LOG.info(SplunkLogFormatter.formatMessage("Transaction Successful. Proceeding for refund.",
                    ErrorCode.INFO_REFUND));
            return paymentTransactionData;
        }


        // Refund step
        final PaymentTransactionEntryModel refundEntry = targetPaymentService.refundStandaloneRefund(order,
                captureEntry, refundAmount, captureOrderId);

        paymentTransactionData.getEntries().add(convertPaymentTransactionEntry(refundEntry));

        return paymentTransactionData;
    }

    @Override
    public List<PaymentTransactionEntryModel> performIpgManualRefund(final OrderModel order,
            final BigDecimal refundAmount,
            final IpgNewRefundInfoDTO refundInfoDTO) {
        Assert.notNull(order, "Order cannot be null");
        final List<PaymentTransactionEntryModel> transactionEntryList = new ArrayList<>();
        if (refundInfoDTO != null) {
            final BigDecimal refundedAmount = PriceCalculator.getAsPrice(refundInfoDTO.getAmount());
            if (refundAmount.compareTo(refundedAmount) != 0) {
                throw new PaymentException(
                        MessageFormat
                                .format("Could not refund because the refunded amount does not match. System generated "
                                        + "refund amount based on the item and delivery fee should be {0} but the refunded "
                                        + "amount from manual input is {1}. Please enter correct refund amount in the add payment form.",
                                        refundAmount, refundedAmount));
            }
            final PaymentTransactionModel captureTransaction = findCaptureTransaction(order);
            final PaymentTransactionEntryModel entry = targetPaymentService.refundIpgManualRefund(captureTransaction,
                    refundAmount, refundInfoDTO.getReceiptNo());
            if (captureTransaction.getInfo() instanceof AfterpayPaymentInfoModel) {
                ((AfterpayPaymentInfoModel)captureTransaction.getInfo())
                        .setAfterpayRefundId(refundInfoDTO.getReceiptNo());
                modelService.save(captureTransaction.getInfo());
            }
            transactionEntryList.add(entry);
        }

        return transactionEntryList;
    }

    @Override
    public void createStandaloneRefundPaymentTransaction(final OrderModel order, final PaymentInfoModel paymentInfo,
            final PaymentTransactionDTO transaction) {

        // Create the PaymentTransaction model
        final PaymentTransactionModel paymentTransaction = modelService.create(PaymentTransactionModel.class);
        paymentTransaction.setCode(transaction.getCode());
        paymentTransaction.setInfo(paymentInfo);
        paymentTransaction.setOrder(order);
        paymentTransaction.setPlannedAmount(transaction.getAmount());
        paymentTransaction.setCurrency(order.getCurrency());
        paymentTransaction.setPaymentProvider(transaction.getProvider());
        modelService.save(paymentTransaction);

        for (final PaymentTransactionEntryDTO entry : transaction.getEntries()) {
            createPaymentTransactionEntry(entry, paymentTransaction, order.getCurrency());
        }
    }


    @Override
    public void createFollowOnRefundPaymentTransactionEntries(final OrderModel order,
            final List<PaymentTransactionEntryDTO> dataList) {
        for (final PaymentTransactionEntryDTO data : dataList) {
            createPaymentTransactionEntry(data, PaymentTransactionHelper.findCaptureTransaction(order),
                    order.getCurrency());
        }
    }


    @Override
    public BigDecimal findRefundedAmountAfterRefund(final List<PaymentTransactionEntryDTO> entryDtoList) {
        BigDecimal refundedAmount = new BigDecimal(0);
        if (entryDtoList != null) {
            for (final PaymentTransactionEntryDTO entryDto : entryDtoList) {
                if (TransactionStatus.ACCEPTED
                        .equals(TransactionStatus.valueOf(entryDto.getTransactionStatus()))
                        && (PaymentTransactionType.REFUND_FOLLOW_ON.equals(entryDto.getType())
                                || PaymentTransactionType.REFUND_STANDALONE
                                        .equals(entryDto.getType()))
                        && entryDto.getAmount() != null && entryDto.getAmount().compareTo(BigDecimal.ZERO) >= 0) {
                    refundedAmount = refundedAmount.add(entryDto.getAmount());
                }
            }
        }
        return refundedAmount;
    }

    @Override
    public RefundInfoDTO createRefundedInfo(final List<PaymentTransactionEntryDTO> refundedentryDtoList,
            final BigDecimal refundAmount) {
        final RefundInfoDTO refundInfo = new RefundInfoDTO();

        if (refundAmount == null) {
            refundInfo.setMessage(StringUtils.EMPTY);
            return refundInfo;
        }
        final BigDecimal refundedAmount = findRefundedAmountAfterRefund(refundedentryDtoList);
        if (refundAmount.compareTo(refundedAmount) != 0) {
            final BigDecimal refundRemains = refundAmount.subtract(refundedAmount);
            refundInfo.setRefundAmountRemaining(refundRemains);
            if (refundedAmount.compareTo(BigDecimal.ZERO) > 0) {
                refundInfo.setMessage(MessageFormat
                        .format(PARTIAL_REFUND_SUCCESS, refundAmount, refundedAmount, refundRemains));
            }
            else {
                refundInfo.setMessage(MessageFormat.format(FULL_REFUND_FAILURE, refundAmount));
            }
        }
        return refundInfo;
    }

    protected void createPaymentTransactionEntry(final PaymentTransactionEntryDTO data,
            final PaymentTransactionModel paymentTransaction, final CurrencyModel currency) {

        final PaymentTransactionEntryModel entry = modelService.create(PaymentTransactionEntryModel.class);
        entry.setPaymentTransaction(paymentTransaction);
        entry.setType(data.getType());
        entry.setCode(data.getCode());
        entry.setAmount(data.getAmount());
        entry.setCurrency(currency);
        entry.setTime(new Date());
        entry.setDeclinedTransactionStatusInfo(data.getDeclineDetails());
        entry.setTransactionStatus(data.getTransactionStatus());
        entry.setTransactionStatusDetails(data.getTransactionStatusDetails());
        entry.setIpgPaymentInfo(data.getIpgPaymentInfo());
        modelService.save(entry);
    }

    protected PaymentTransactionModel findCaptureTransaction(final OrderModel orderModel) {

        final PaymentTransactionModel paymentTransaction = PaymentTransactionHelper.findCaptureTransaction(orderModel);

        if (paymentTransaction == null) {
            throw new TargetOrderCancelException("Cannot find a successful capture transaction against the order:"
                    + orderModel.getCode());
        }


        return paymentTransaction;
    }


    protected PaymentTransactionEntryDTO convertPaymentTransactionEntry(
            final PaymentTransactionEntryModel refundTransactionEntry) {

        final PaymentTransactionEntryDTO data = new PaymentTransactionEntryDTO();
        data.setCode(refundTransactionEntry.getCode());
        data.setTransactionStatus(refundTransactionEntry.getTransactionStatus());
        data.setTransactionStatusDetails(refundTransactionEntry.getTransactionStatusDetails());
        data.setAmount(refundTransactionEntry.getAmount());
        data.setTime(refundTransactionEntry.getTime());
        data.setType(refundTransactionEntry.getType());
        data.setDeclineDetails(refundTransactionEntry.getDeclinedTransactionStatusInfo());
        data.setIpgPaymentInfo(refundTransactionEntry.getIpgPaymentInfo());
        return data;
    }

    protected PaymentTransactionDTO convertPaymentTransaction(final PaymentTransactionModel refundTransaction) {

        final PaymentTransactionDTO data = new PaymentTransactionDTO();
        data.setCode(refundTransaction.getCode());
        data.setAmount(refundTransaction.getPlannedAmount());
        data.setProvider(refundTransaction.getPaymentProvider());

        return data;
    }


    /**
     * @param targetPaymentService
     *            the targetPaymentService to set
     */
    @Required
    public void setTargetPaymentService(final TargetPaymentService targetPaymentService) {
        this.targetPaymentService = targetPaymentService;
    }


    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param vpcCaptureCodeGenerator
     *            the vpcCaptureCodeGenerator to set
     */
    @Required
    public void setVpcCaptureCodeGenerator(final KeyGenerator vpcCaptureCodeGenerator) {
        this.vpcCaptureCodeGenerator = vpcCaptureCodeGenerator;
    }

}

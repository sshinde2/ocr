package au.com.target.tgtcore.ordercancel.impl.executors;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRecordsHandler;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.OrderCancelRequestExecutor;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.CancelTypeEnum;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.flybuys.service.FlybuysDiscountService;
import au.com.target.tgtcore.order.FluentOrderService;
import au.com.target.tgtcore.order.impl.TargetFindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtcore.ordercancel.TargetCancelService;
import au.com.target.tgtcore.ordercancel.TargetOrderCancelRequest;
import au.com.target.tgtcore.ordercancel.TargetRefundPaymentService;
import au.com.target.tgtcore.ordercancel.data.CancelRequestResponse;
import au.com.target.tgtcore.ordercancel.data.IpgNewRefundInfoDTO;
import au.com.target.tgtcore.ordercancel.data.PaymentTransactionEntryDTO;
import au.com.target.tgtcore.ordercancel.data.RefundInfoDTO;
import au.com.target.tgtcore.ordercancel.discountstrategy.VoucherCorrectionStrategy;
import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;
import au.com.target.tgtpayment.util.PriceCalculator;
import au.com.target.tgtutility.constants.TgtutilityConstants.ErrorCode;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * Target implementation of {@link OrderCancelRequestExecutor}
 * 
 * 1) Modify order/consignment entries <br>
 * 2) Recalculate and refund the difference <br>
 * 3) Release the appropriate product stock <br>
 * 4) Update Order status accordingly <br>
 * 5) Start the cancel business process to perform the offline activities like TLOG export, submitting to Accertify,
 * email etc
 */
public class TargetCancelRequestExecutor implements OrderCancelRequestExecutor {

    private static final Logger LOG = Logger.getLogger(TargetCancelRequestExecutor.class);
    private static final String REFUND_TICKET_MESSAGE = " Order cancel is partial failed due to failure to refund | Refund information: {0}";
    private static final String REFUND_TICKET_HEAD = "Order cancel or partial cancel refund failed";
    private static final String REFUND_REJECT_MESSAGE = "refund rejected: refund amount:{0}, and failure message:{1} ";
    private static final String UNKNOWN_CANCEL_FAILED_EVENT_MESSAGE = "The order was not able to be cancelled due to a system issue. Please retry.";
    private static final String REFUND_CUSTOMER_FAILED_NOTIFY_CS_COCKPIT_USER = "The order has been cancelled and a CS Ticket has been raised. Please ensure the customer is refunded.";

    private TargetCancelService targetCancelService;
    private TargetRefundPaymentService targetRefundPaymentService;
    private TransactionTemplate transactionTemplate;
    private ModelService modelService;
    private TargetFindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;
    private TargetBusinessProcessService targetBusinessProcessService;
    private OrderCancelRecordsHandler orderCancelRecordsHandler;
    private VoucherCorrectionStrategy voucherCorrectionStrategy;
    private FlybuysDiscountService flybuysDiscountService;
    private TargetTicketBusinessService targetTicketBusinessService;
    private TargetFeatureSwitchService targetFeatureSwitchService;
    private FluentOrderService fluentOrderService;

    @Override
    public void processCancelRequest(final OrderCancelRequest orderCancelRequest,
            final OrderCancelRecordEntryModel unusedCancelRequestRecordEntry) throws OrderCancelException {
        throw new UnsupportedOperationException(
                "Use alternate method processCancelRequest(orderCancelRequest, requestor");
    }

    /**
     * Alternative method creates the OrderCancelRecordEntryModel inside the transaction and returns it
     * 
     * @param orderCancelRequest
     * @param requestor
     * @return created OrderCancelRecordEntryModel
     * @throws OrderCancelException
     */
    public OrderCancelRecordEntryModel processCancelRequest(final OrderCancelRequest orderCancelRequest,
            final PrincipalModel requestor) throws OrderCancelException {

        Assert.isInstanceOf(TargetOrderCancelRequest.class, orderCancelRequest,
                "Cancel Request must be of type TargetOrderCancelRequest");
        final OrderModel order = orderCancelRequest.getOrder();
        Assert.notNull(order, "order cannot be null");

        final TargetOrderCancelRequest targetOrderCancelRequest = (TargetOrderCancelRequest)orderCancelRequest;
        final CreditCardPaymentInfoModel newPayment = (CreditCardPaymentInfoModel)targetOrderCancelRequest
                .getNewPayment();


        // Remember whether we should run a cancel extract to warehouse later in the bus proc
        final boolean doCancelExtract = targetCancelService.isExtractCancelToWarehouseRequired(order);

        final CancelRequestResponse response = new CancelRequestResponse();

        // Process cancel request inside a transaction
        final OrderCancelException orderCancelException = transactionTemplate.execute(
                new TransactionCallback<OrderCancelException>() {

                    @Override
                    public OrderCancelException doInTransaction(final TransactionStatus status) {

                        try {
                            LOG.info(SplunkLogFormatter.formatMessage("Trying to process cancel request for order: "
                                    + order.getCode(), ErrorCode.INFO_CANCELORDER));
                            sendCancelEventToFluent(order);
                            processInternalCancelRequest(targetOrderCancelRequest, requestor,
                                    response, newPayment);
                            // If the refund payment failed then throw an exception to force the rollback
                            final String refundFailureMessage = response.getRefundFailureMessage();
                            if (refundFailureMessage != null) {

                                throw new OrderCancelException(order.getCode(), MessageFormat.format(
                                        REFUND_REJECT_MESSAGE,
                                        response.getRefundAmount(), refundFailureMessage));
                            }
                        }
                        catch (final OrderCancelException e) {
                            shouldRollBack(status);
                            return e;
                        }
                        catch (final Exception e) {
                            shouldRollBack(status);
                            return new OrderCancelException(orderCancelRequest.getOrder().getCode(), MessageFormat
                                    .format(REFUND_REJECT_MESSAGE,
                                            response.getRefundAmount(), e.getMessage()));
                        }

                        return null;

                    }

                    /**
                     * @param status
                     */
                    private void shouldRollBack(final TransactionStatus status) {
                        if (!targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)) {
                            status.setRollbackOnly();
                        }
                    }
                });


        final CancelTypeEnum cancelType = orderCancelRequest.isPartialCancel() ? CancelTypeEnum.COCKPIT_PARTIAL_CANCEL
                : CancelTypeEnum.COCKPIT_CANCEL;

        final RefundInfoDTO refundedInfo = getRefundInfo(response);

        final String refundMessage = refundedInfo.getMessage();

        // Handle failure
        if (orderCancelException != null) {
            if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)) {
                cancelOrderRaiseCSTicket(order, doCancelExtract, response, cancelType, orderCancelException);
            }
            else {
                createPaymentEntries(order, newPayment, response, orderCancelException);
            }
        }

        startOrderCancelProcess(order, doCancelExtract, response, cancelType, refundedInfo);

        if (StringUtils.isNotEmpty(refundMessage)) {
            raiseCSTicket(order, refundMessage);

            LOG.info(
                    MessageFormat.format(
                            "CS cancel refund partially successful for order={0}.CS ticket has been raised={1}",
                            new Object[] { order.getCode(), refundMessage }));

            throw new OrderCancelException(order.getCode(), refundMessage
                    + " CS ticket has been raised, please refresh the order, and fix the ticket");

        }
        return (OrderCancelRecordEntryModel)response.getOrderModificationRecordEntryModel();

    }

    private RefundInfoDTO getRefundInfo(final CancelRequestResponse response) {
        return targetRefundPaymentService
                .createRefundedInfo(response.getFollowOnRefundPaymentDataList(), response.getRefundAmount());
    }

    /**
     * @param order
     * @param newPayment
     * @param response
     * @param orderCancelException
     * @throws OrderCancelException
     */
    private void createPaymentEntries(final OrderModel order, final CreditCardPaymentInfoModel newPayment,
            final CancelRequestResponse response, final OrderCancelException orderCancelException)
            throws OrderCancelException {
        // If there was a refund payment attempt then recreate the payment models, now the transaction has rolled back
        if (CollectionUtils.isNotEmpty(response.getFollowOnRefundPaymentDataList())) {

            targetRefundPaymentService.createFollowOnRefundPaymentTransactionEntries(order,
                    response.getFollowOnRefundPaymentDataList());
        }

        if (response.getStandaloneRefundPaymentData() != null) {
            targetRefundPaymentService.createStandaloneRefundPaymentTransaction(order, newPayment,
                    response.getStandaloneRefundPaymentData());
        }

        throw orderCancelException;
    }

    /**
     * @param order
     * @param doCancelExtract
     * @param response
     * @param cancelType
     * @param orderCancelException
     * @throws OrderCancelException
     */
    private void cancelOrderRaiseCSTicket(final OrderModel order, final boolean doCancelExtract,
            final CancelRequestResponse response, final CancelTypeEnum cancelType,
            final OrderCancelException orderCancelException) throws OrderCancelException {

        final boolean isRefundFail = CollectionUtils.isNotEmpty(response.getFollowOnRefundPaymentDataList())
                || response.getStandaloneRefundPaymentData() != null;

        if (isRefundFail) {

            final RefundInfoDTO refundedInfo = getRefundInfo(response);
            final String refundMessage = refundedInfo.getMessage();

            startOrderCancelProcess(order, doCancelExtract, response, cancelType, refundedInfo);

            final String csTicket = raiseCSTicket(order, refundedInfo.getMessage());

            LOG.info(
                    MessageFormat.format(
                            "Failure attempting refund order= {0}"
                                    + ". CS Ticket raised={1}  , refund message={2}",
                            new Object[] { order.getCode(), csTicket, refundMessage }));

            throw new OrderCancelException(order.getCode(), refundMessage +
                    REFUND_CUSTOMER_FAILED_NOTIFY_CS_COCKPIT_USER);
        }

        throw orderCancelException;
    }

    /**
     * @param order
     * @param refundMessage
     * @throws OrderCancelException
     */
    private String raiseCSTicket(final OrderModel order, final String refundMessage) throws OrderCancelException {

        if (StringUtils.isNotEmpty(refundMessage)) {

            final String formattedRefundMessage = MessageFormat.format(REFUND_TICKET_MESSAGE, refundMessage);

            return targetTicketBusinessService.createCsAgentTicket(REFUND_TICKET_HEAD,
                    REFUND_TICKET_HEAD,
                    formattedRefundMessage, order, TgtCoreConstants.CsAgentGroup.REFUND_FAILED_CS_AGENT_GROUP);

        }
        return null;
    }

    /**
     * The business process will: <br>
     * 1) Export TLOG to POS<br>
     * 2) Submit the refund transaction to Accertify<br>
     * 3) Notify the warehouse <br>
     * 4)Email notification to the customer
     * 
     * @param order
     * @param doCancelExtract
     * @param response
     * @param cancelType
     * @param refundedInfo
     */
    private void startOrderCancelProcess(final OrderModel order, final boolean doCancelExtract,
            final CancelRequestResponse response, final CancelTypeEnum cancelType, final RefundInfoDTO refundedInfo) {

        getTargetBusinessProcessService().startOrderCancelProcess(order,
                (OrderCancelRecordEntryModel)response.getOrderModificationRecordEntryModel(),
                response.getRefundAmount(),
                cancelType, doCancelExtract, response.getFollowOnRefundPaymentList(),
                refundedInfo.getRefundAmountRemaining());
    }

    /**
     * @param orderModel
     * @throws OrderCancelException
     */
    protected void sendCancelEventToFluent(final OrderModel orderModel) throws OrderCancelException {
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)
                && StringUtils.isNotEmpty(orderModel.getFluentId())) {
            String message = UNKNOWN_CANCEL_FAILED_EVENT_MESSAGE;
            try {
                message = fluentOrderService.customerCancelOrder(orderModel);
                if (StringUtils.isNotBlank(message)) {
                    throw new OrderCancelException(orderModel.getCode(), message);
                }
            }
            catch (final Exception e) {
                LOG.error(MessageFormat.format("Failed to cancel the order:{0}, due to the reason:{1}",
                        new Object[] { orderModel.getCode(), e.getMessage() }));
                throw new OrderCancelException(orderModel.getCode(), message);
            }
        }

    }

    protected void processInternalCancelRequest(final TargetOrderCancelRequest orderCancelRequest,
            final PrincipalModel requestor,
            final CancelRequestResponse response,
            final CreditCardPaymentInfoModel newPayment) throws OrderCancelException {

        // Create cancel record entry model and also history snapshot
        final OrderCancelRecordEntryModel orderCancelRecordEntryModel = orderCancelRecordsHandler.createRecordEntry(
                orderCancelRequest,
                requestor);

        final OrderModel order = orderCancelRequest.getOrder();
        modelService.refresh(order);

        if (StringUtils.isBlank(order.getFluentId())) {
            // This is up front since it uses the existing consignment statuses before they get modified below.
            targetCancelService.releaseStock(orderCancelRequest);
        }

        //modify order and consignment entries
        targetCancelService.modifyOrderAccordingToRequest(orderCancelRequest, order);
        if (targetCancelService.willCancellationIncreaseOrderValue(order)) {
            throw new OrderCancelException(order.getCode(), "This action would cause an increase in the order total.");
        }

        final Double orderTotalBeforeVoucherUpdate = order.getTotalPrice();

        if (order.getInitialDeliveryCost().doubleValue() > 0.0d
                && orderCancelRequest.getShippingAmountToRefund().compareTo(Double.valueOf(0)) == 0) {
            targetCancelService.updateShippingAmountForCancel(order, orderCancelRecordEntryModel);
            targetCancelService.recalculateOrder(order);
        }

        voucherCorrectionStrategy.correctAppliedVouchers(order);

        targetCancelService.updateOrderStatus(orderCancelRecordEntryModel, order);
        targetCancelService.updateConsignmentStatus(orderCancelRequest);

        targetCancelService.updateRecordEntry(orderCancelRequest);

        final BigDecimal refundAmount = PriceCalculator.subtract(order.getTotalPrice(),
                findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(order));

        orderCancelRecordEntryModel.setRefundedAmount(Double.valueOf(refundAmount.doubleValue()));

        flybuysDiscountService
                .setFlybuysRefundDetailsInModificationEntry(orderCancelRecordEntryModel, order,
                        orderTotalBeforeVoucherUpdate);
        modelService.save(orderCancelRecordEntryModel);

        // Pass some data out in the response
        response.setRefundAmount(refundAmount);
        response.setOrderModificationRecordEntryModel(orderCancelRecordEntryModel);

        if (refundAmount.compareTo(BigDecimal.ZERO) > 0) {
            final IpgNewRefundInfoDTO newRefundInfoDTO = orderCancelRequest.getIpgNewRefundInfoDTO();
            // Do the refund at the end to minimise chance of rollback afterwards
            // Note we save the PTEM data in response in case we roll everything back so we can recreate them
            if (isFollowOnRefund(newPayment, newRefundInfoDTO)) {
                LOG.info(SplunkLogFormatter.formatMessage("Initiating Follow-on Refund for order: " + order.getCode(),
                        ErrorCode.INFO_REFUND));
                final List<PaymentTransactionEntryModel> refundedEntryList = targetRefundPaymentService
                        .performFollowOnRefund(order, refundAmount);
                final List<PaymentTransactionEntryDTO> refundedEntryDtoList = targetRefundPaymentService
                        .convertPaymentTransactionEntries(refundedEntryList);
                response.setFollowOnRefundPaymentList(refundedEntryList);
                response.setFollowOnRefundPaymentDataList(refundedEntryDtoList);
                response.setRefundedAmount(targetRefundPaymentService
                        .findRefundedAmountAfterRefund(refundedEntryDtoList));
            }
            else if (isStandaloneRefund(newPayment, newRefundInfoDTO)) {
                LOG.info(SplunkLogFormatter.formatMessage("Initiating Standalone Refund for order: " + order.getCode(),
                        ErrorCode.INFO_REFUND));
                response.setStandaloneRefundPaymentData(targetRefundPaymentService.performStandaloneRefund(order,
                        refundAmount, newPayment));
                modelService.save(newPayment);
            }
            else if (isIpgManualRefund(newPayment, newRefundInfoDTO)) {
                final List<PaymentTransactionEntryModel> refundedEntryList = targetRefundPaymentService
                        .performIpgManualRefund(order, refundAmount, newRefundInfoDTO);
                final List<PaymentTransactionEntryDTO> refundedEntryDtoList = targetRefundPaymentService
                        .convertPaymentTransactionEntries(refundedEntryList);
                response.setFollowOnRefundPaymentList(refundedEntryList);
                response.setFollowOnRefundPaymentDataList(refundedEntryDtoList);
                response.setRefundedAmount(targetRefundPaymentService
                        .findRefundedAmountAfterRefund(refundedEntryDtoList));
            }
        }
    }

    private boolean isFollowOnRefund(final CreditCardPaymentInfoModel newPayment,
            final IpgNewRefundInfoDTO newRefundInfoDTO) {
        return (newRefundInfoDTO == null && (newPayment == null || newPayment.getSubscriptionId() == null || newPayment
                .getSubscriptionId()
                .isEmpty()));
    }

    private boolean isStandaloneRefund(final CreditCardPaymentInfoModel newPayment,
            final IpgNewRefundInfoDTO newRefundInfoDTO) {
        return (newRefundInfoDTO == null && !(newPayment == null || newPayment.getSubscriptionId() == null || newPayment
                .getSubscriptionId()
                .isEmpty()));
    }

    private boolean isIpgManualRefund(final CreditCardPaymentInfoModel newPayment,
            final IpgNewRefundInfoDTO newRefundInfoDTO) {
        return (newRefundInfoDTO != null && (newPayment == null || newPayment.getSubscriptionId() == null || newPayment
                .getSubscriptionId()
                .isEmpty()));
    }

    /**
     * @param transactionTemplate
     *            the transactionTemplate to set
     */
    @Required
    public void setTransactionTemplate(final TransactionTemplate transactionTemplate) {
        this.transactionTemplate = transactionTemplate;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param findOrderTotalPaymentMadeStrategy
     *            the findOrderTotalPaymentMadeStrategy to set
     */
    @Required
    public void setFindOrderTotalPaymentMadeStrategy(
            final TargetFindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy) {
        this.findOrderTotalPaymentMadeStrategy = findOrderTotalPaymentMadeStrategy;
    }

    /**
     * @param targetCancelService
     *            the targetCancelService to set
     */
    @Required
    public void setTargetCancelService(final TargetCancelService targetCancelService) {
        this.targetCancelService = targetCancelService;
    }

    /**
     * @return the targetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        return targetBusinessProcessService;
    }

    /**
     * @param targetBusinessProcessService
     *            the targetBusinessProcessService to set
     */
    @Required
    public void setTargetBusinessProcessService(final TargetBusinessProcessService targetBusinessProcessService) {
        this.targetBusinessProcessService = targetBusinessProcessService;
    }

    /**
     * @param orderCancelRecordsHandler
     *            the orderCancelRecordsHandler to set
     */
    @Required
    public void setOrderCancelRecordsHandler(final OrderCancelRecordsHandler orderCancelRecordsHandler) {
        this.orderCancelRecordsHandler = orderCancelRecordsHandler;
    }

    /**
     * @param targetRefundPaymentService
     *            the targetRefundPaymentService to set
     */
    @Required
    public void setTargetRefundPaymentService(final TargetRefundPaymentService targetRefundPaymentService) {
        this.targetRefundPaymentService = targetRefundPaymentService;
    }


    /**
     * @param voucherCorrectionStrategy
     *            the voucherCorrectionStrategy to set
     */
    @Required
    public void setVoucherCorrectionStrategy(final VoucherCorrectionStrategy voucherCorrectionStrategy) {
        this.voucherCorrectionStrategy = voucherCorrectionStrategy;
    }

    /**
     * @param flybuysDiscountService
     *            the flybuysDiscountService to set
     */
    @Required
    public void setFlybuysDiscountService(final FlybuysDiscountService flybuysDiscountService) {
        this.flybuysDiscountService = flybuysDiscountService;
    }


    /**
     * @param targetTicketBusinessService
     *            the targetTicketBusinessService to set
     */
    @Required
    public void setTargetTicketBusinessService(final TargetTicketBusinessService targetTicketBusinessService) {
        this.targetTicketBusinessService = targetTicketBusinessService;
    }


    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }


    /**
     * @param fluentOrderService
     *            the fluentOrderService to set
     */
    @Required
    public void setFluentOrderService(final FluentOrderService fluentOrderService) {
        this.fluentOrderService = fluentOrderService;
    }



}
/**
 * 
 */
package au.com.target.tgtcore.ordercancel.util;

import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelRecordsHandler;
import de.hybris.platform.ordercancel.exceptions.OrderCancelRecordsHandlerException;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.ordercancel.TargetOrderCancelRequest;


/**
 * Helper for creating an OrderCancelRequest
 * 
 */
public class OrderCancelHelper {

    private OrderCancelRecordsHandler orderCancelRecordsHandler;


    /**
     * Create a TargetOrderCancelRequest for cancelling the entire order.<br/>
     * 
     * @param order
     * @param reasonNotes
     *            reason description
     * @param reason
     *            the reason code
     * @return TargetOrderCancelRequest
     */
    public TargetOrderCancelRequest createOrderFullCancelRecord(final OrderModel order, final String reasonNotes,
            final CancelReason reason) {

        final TargetOrderCancelRequest orderCancelRequest = new TargetOrderCancelRequest(order,
                getCancelEntriesForFullOrder(order, reasonNotes, reason), reasonNotes);
        orderCancelRequest.setCancelReason(reason);

        // Refund all shipping
        orderCancelRequest.setShippingAmountToRefund(Double.valueOf(order.getInitialDeliveryCost().doubleValue()));

        return orderCancelRequest;
    }


    /**
     * Given the orderCancelRequest pojo, create an OrderCancelRecordEntryModel NOTE:
     * orderCancelRecordsHandler.createRecordEntry api also creates an order history snapshot.<br/>
     * 
     * @param orderCancelRequest
     * @return OrderCancelRecordEntryModel
     * @throws OrderCancelRecordsHandlerException
     */
    public OrderCancelRecordEntryModel createOrderFullCancelRecordEntry(
            final TargetOrderCancelRequest orderCancelRequest)
            throws OrderCancelRecordsHandlerException {

        final OrderCancelRecordEntryModel orderCancelRequestEntry = orderCancelRecordsHandler
                .createRecordEntry(orderCancelRequest);

        return orderCancelRequestEntry;
    }



    /**
     * Get cancel entries for the full order
     * 
     * @param order
     * @param reasonNotes
     * @param reason
     * @return list of OrderCancelEntry
     */
    protected List<OrderCancelEntry> getCancelEntriesForFullOrder(final OrderModel order, final String reasonNotes,
            final CancelReason reason) {

        final List<OrderCancelEntry> orderCancelEntries = new ArrayList<>();

        if (order.getEntries() != null) {

            for (final AbstractOrderEntryModel orderEntry : order.getEntries()) {

                final long cancelQty = orderEntry.getQuantity() == null ? 0 : orderEntry.getQuantity().longValue();
                orderCancelEntries.add(new OrderCancelEntry(orderEntry, cancelQty, reasonNotes, reason));
            }
        }

        return orderCancelEntries;
    }


    /**
     * @param orderCancelRecordsHandler
     *            the orderCancelRecordsHandler to set
     */
    @Required
    public void setOrderCancelRecordsHandler(final OrderCancelRecordsHandler orderCancelRecordsHandler) {
        this.orderCancelRecordsHandler = orderCancelRecordsHandler;
    }


}

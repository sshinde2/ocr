package au.com.target.tgtcore.orderreturn;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.OrderReturnRecordHandler;
import de.hybris.platform.returns.OrderReturnRecordsHandlerException;
import de.hybris.platform.returns.model.OrderReturnRecordEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;

/**
 * Target extension to {@link OrderReturnRecordHandler}.
 */
public interface TargetOrderReturnRecordHandler extends OrderReturnRecordHandler {

    /**
     * Creates and persists new order modification entry for given {@code order} based on
     * given {@code request}.
     *
     * @param order the order which is being modified
     * @param request the order return or refund request submitted from UI
     * @param description the modification entry description
     * @return persisted order modification record
     * @throws OrderReturnRecordsHandlerException if order is inconsistent state
     */
    OrderReturnRecordEntryModel createRefundEntry(final OrderModel order, final ReturnRequestModel request,
                                                  final String description) throws OrderReturnRecordsHandlerException;
}

/**
 * 
 */
package au.com.target.tgtcore.orderreturn.impl;

import de.hybris.platform.basecommerce.enums.OrderModificationEntryStatus;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordercancel.exceptions.OrderCancelDaoException;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.refund.model.OrderRefundRecordEntryModel;
import de.hybris.platform.returns.OrderReturnRecordsHandlerException;
import de.hybris.platform.returns.impl.DefaultOrderReturnRecordsHandler;
import de.hybris.platform.returns.model.OrderEntryReturnRecordEntryModel;
import de.hybris.platform.returns.model.OrderReturnRecordEntryModel;
import de.hybris.platform.returns.model.OrderReturnRecordModel;
import de.hybris.platform.returns.model.RefundEntryModel;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.orderreturn.TargetOrderReturnRecordHandler;
import au.com.target.tgtcore.util.OrderHandlerUtil;


/**
 * Default implementation of {@link TargetOrderReturnRecordHandler}.
 */
public class TargetOrderReturnRecordHandlerImpl extends DefaultOrderReturnRecordsHandler
        implements TargetOrderReturnRecordHandler {

    private UserService userService;
    private OrderHandlerUtil orderHandlerUtil;

    @Override
    public OrderReturnRecordEntryModel createRefundEntry(final OrderModel order, final ReturnRequestModel request,
            final String description) throws OrderReturnRecordsHandlerException {

        final List<RefundEntryModel> refunds = new ArrayList<>();
        for (final ReturnEntryModel returnEntry : request.getReturnEntries()) {
            refunds.add((RefundEntryModel)returnEntry);
        }

        final OrderReturnRecordEntryModel entry = createRefundEntry(order, refunds, description);
        entry.setRefundedShippingAmount(request.getShippingAmountToRefund());
        entry.setRefundedAmount(request.getRefundedAmount());
        entry.setPrincipal(userService.getCurrentUser());
        getModelService().save(entry);

        return entry;
    }

    @Override
    public OrderReturnRecordEntryModel createRefundEntry(final OrderModel order, final List<RefundEntryModel> refunds,
            final String description)
            throws OrderReturnRecordsHandlerException
    {
        try
        {
            final OrderReturnRecordModel returnRecord = getOrCreateReturnRecord(order);
            if (returnRecord.isInProgress())
            {
                throw new IllegalStateException(
                        "Cannot create new Order return request - the order return record indicates: Return already in progress");
            }

            final OrderHistoryEntryModel snapshot = createSnaphot(order, description);
            if (userService.getCurrentUser() instanceof EmployeeModel) {
                snapshot.setEmployee((EmployeeModel)userService.getCurrentUser());
                getModelService().save(snapshot);
            }

            returnRecord.setInProgress(true);
            getModelService().save(returnRecord);
            return createRefundRecordEntry(order, returnRecord, snapshot, refunds, null);

        }
        catch (final OrderCancelDaoException e)
        {
            throw new OrderReturnRecordsHandlerException(order.getCode(), e);
        }
    }

    @Override
    protected OrderReturnRecordEntryModel createRefundRecordEntry(final OrderModel order,
            final OrderReturnRecordModel returnRecord, final OrderHistoryEntryModel snapshot,
            final List<RefundEntryModel> refunds,
            final UserModel principal) throws OrderReturnRecordsHandlerException
    {

        final OrderRefundRecordEntryModel refundRecordEntry = getModelService().create(
                OrderRefundRecordEntryModel.class);
        refundRecordEntry.setTimestamp(new Date());
        refundRecordEntry.setCode(generateEntryCode(snapshot));
        refundRecordEntry.setOriginalVersion(snapshot);
        refundRecordEntry.setModificationRecord(returnRecord);
        refundRecordEntry.setPrincipal(principal);
        refundRecordEntry.setOwner(returnRecord);
        refundRecordEntry.setStatus(OrderModificationEntryStatus.SUCCESSFULL);

        getModelService().save(refundRecordEntry);

        final List<OrderEntryModificationRecordEntryModel> orderEntriesRecords = new ArrayList<>();

        for (final RefundEntryModel refundEntry : refunds)
        {
            final OrderEntryReturnRecordEntryModel orderEntryRefundEntry = getModelService().create(
                    OrderEntryReturnRecordEntryModel.class);
            orderEntryRefundEntry.setCode(refundEntry.getOrderEntry().getPk().toString());
            orderEntryRefundEntry.setExpectedQuantity(Long.valueOf(refundEntry.getExpectedQuantity().longValue()));
            orderEntryRefundEntry.setModificationRecordEntry(refundRecordEntry);
            orderEntryRefundEntry.setOriginalOrderEntry(getOriginalOrderEntry(snapshot, refundEntry));
            orderEntryRefundEntry.setReturnedQuantity(Long.valueOf(refundEntry.getExpectedQuantity().longValue()));
            orderEntryRefundEntry.setRefundReason(refundEntry.getReason().toString());
            orderEntryRefundEntry.setOrderEntry((OrderEntryModel)refundEntry.getOrderEntry());
            getModelService().save(orderEntryRefundEntry);
            orderEntriesRecords.add(orderEntryRefundEntry);
        }

        refundRecordEntry.setOrderEntriesModificationEntries(orderEntriesRecords);

        getModelService().saveAll(orderEntriesRecords);

        return refundRecordEntry;
    }

    @Override
    protected OrderHistoryEntryModel createSnaphot(final OrderModel order, final String description)
    {
        return orderHandlerUtil.createOrderSnapshot(order, description, null);
    }

    @Override
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @param orderHandlerUtil
     *            the orderHandlerUtil to set
     */
    @Required
    public void setOrderHandlerUtil(final OrderHandlerUtil orderHandlerUtil) {
        this.orderHandlerUtil = orderHandlerUtil;
    }

}

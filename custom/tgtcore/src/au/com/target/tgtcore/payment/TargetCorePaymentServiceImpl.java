/**
 * 
 */
package au.com.target.tgtcore.payment;

import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.dto.BillingInfo;

import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtpayment.service.impl.TargetPaymentServiceImpl;


/**
 * @author dcwillia
 * 
 */
public class TargetCorePaymentServiceImpl extends TargetPaymentServiceImpl {
    @Override
    protected AddressModel createBillingAddress() {
        return getModelService().create(TargetAddressModel.class);
    }

    @Override
    protected void populateBillingAddress(final AddressModel billingAddress, final BillingInfo billingInfo) {
        super.populateBillingAddress(billingAddress, billingInfo);
        final RegionModel regionModel = billingAddress.getRegion();
        if (regionModel != null) {
            billingAddress.setDistrict(regionModel.getAbbreviation());
        }
    }
}

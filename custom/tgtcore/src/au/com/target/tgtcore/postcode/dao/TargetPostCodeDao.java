/**
 * 
 */
package au.com.target.tgtcore.postcode.dao;

import au.com.target.tgtcore.model.PostCodeModel;



/**
 * @author rmcalave
 * 
 */
public interface TargetPostCodeDao {

    /**
     * Find a {code}PostCodeModel{code} by post code.
     * 
     * @param code
     *            The post code to find
     * @return The model representing the provided post code, null if one doesn't exist
     */
    PostCodeModel findPostCodeByCode(String code);

}
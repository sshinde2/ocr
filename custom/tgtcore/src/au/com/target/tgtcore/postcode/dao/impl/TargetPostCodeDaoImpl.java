/**
 * 
 */
package au.com.target.tgtcore.postcode.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgtcore.postcode.dao.TargetPostCodeDao;
import au.com.target.tgtcore.util.TargetServicesUtil;


/**
 * @author rmcalave
 * 
 */
public class TargetPostCodeDaoImpl extends DefaultGenericDao<PostCodeModel> implements TargetPostCodeDao {

    public TargetPostCodeDaoImpl() {
        super(PostCodeModel._TYPECODE);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.postcode.dao.impl.PostCodeDao#findPostCodeByCode(java.lang.String)
     */
    @Override
    public PostCodeModel findPostCodeByCode(final String code) {
        ServicesUtil.validateParameterNotNullStandardMessage("code", code);

        final Map<String, String> params = new HashMap<String, String>();
        params.put(PostCodeModel.POSTCODE, code);

        final List<PostCodeModel> postCodes = find(params);

        try {
            TargetServicesUtil.validateIfSingleResult(postCodes, PostCodeModel.class, PostCodeModel.POSTCODE, code);
        }
        catch (final TargetUnknownIdentifierException ex) {
            return null;
        }
        catch (final TargetAmbiguousIdentifierException ex) {
            return null;
        }

        return postCodes.get(0);
    }
}

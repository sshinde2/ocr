/**
 * 
 */
package au.com.target.tgtcore.postcode.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.PostCodeGroupModel;
import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgtcore.postcode.dao.TargetPostCodeDao;
import au.com.target.tgtcore.postcode.service.TargetPostCodeService;


/**
 * @author rmcalave
 * 
 */
public class TargetPostCodeServiceImpl extends AbstractBusinessService implements TargetPostCodeService {

    private TargetPostCodeDao targetPostCodeDao;

    @Override
    public PostCodeModel getPostCode(final String code) {
        return targetPostCodeDao.findPostCodeByCode(code);
    }

    @Override
    public boolean doesPostCodeBelongsToGroups(final String postCode, final Collection<PostCodeGroupModel> pcGroups) {

        if (StringUtils.isBlank(postCode) || CollectionUtils.isEmpty(pcGroups)) {
            return false;
        }

        final Collection<PostCodeGroupModel> groupsOfPostCode = getPostCodeGroupByPostcode(postCode);

        if (CollectionUtils.isEmpty(groupsOfPostCode)) {
            return false;
        }

        for (final PostCodeGroupModel pcgModel : groupsOfPostCode) {
            if (pcGroups.contains(pcgModel)) {
                return true;
            }
        }

        return false;
    }


    @Override
    public String getPostalCodeFromCartOrSession(final AbstractOrderModel abstractOrder) {
        if (null != abstractOrder.getDeliveryAddress()) {
            final String postCodeFromAddress = abstractOrder.getDeliveryAddress().getPostalcode();
            if (StringUtils.isNotEmpty(postCodeFromAddress)) {
                return StringUtils.trim(postCodeFromAddress);
            }
        }
        final String postCodeFromSession = ObjectUtils.toString(getSessionService().getAttribute(
                TgtCoreConstants.SESSION_POSTCODE));
        if (StringUtils.isNotEmpty(postCodeFromSession)) {
            return StringUtils.trim(postCodeFromSession);
        }

        return StringUtils.EMPTY;
    }

    @Override
    public Collection<PostCodeGroupModel> getPostCodeGroupByPostcode(final String postcode) {
        if (StringUtils.isEmpty(postcode)) {
            return null;
        }
        final PostCodeModel postCodeModel = getPostCode(postcode);
        if (postCodeModel == null) {
            return null;
        }
        return postCodeModel.getPostCodeGroups();
    }

    /**
     * @param targetPostCodeDao
     *            the targetPostCodeDao to set
     */
    @Required
    public void setTargetPostCodeDao(final TargetPostCodeDao targetPostCodeDao) {
        this.targetPostCodeDao = targetPostCodeDao;
    }

}

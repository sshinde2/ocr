/**
 * 
 */
package au.com.target.tgtcore.price;

import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.core.model.product.ProductModel;

import au.com.target.tgtcore.price.data.PriceRangeInformation;


/**
 * Extend CommercePriceService to add method for returning price range information.
 * 
 */
public interface TargetCommercePriceService extends CommercePriceService {

    /**
     * Get price range information
     * 
     * @param product
     * @return the price range info
     */
    public PriceRangeInformation getPriceRangeInfoForProduct(ProductModel product);

}

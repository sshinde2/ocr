/**
 * 
 */
package au.com.target.tgtcore.price.daos;

import de.hybris.platform.core.model.c2l.CurrencyModel;

import java.util.List;

import au.com.target.tgtcore.model.TargetPreviousPermanentPriceModel;


/**
 * @author knemalik
 * 
 */
public interface TargetPreviousPermanentPriceDao {

    /**
     * retrieves the product that has null end date
     * 
     * @param productCode
     * @return TargetPermanentPriceModel
     */
    List<TargetPreviousPermanentPriceModel> findProductWithNullEndDate(String productCode);

    /**
     * retrieves the previous product price by productCode, end date not null, end date after current date minus
     * currentPermPriceMaxAge, start date less than end date minus previousPermPriceMinAge, currency.
     * 
     * @param productCode
     * @param currentPermPriceMaxAge
     * @param previousPermPriceMinAge
     * @param currency
     * @return TargetPreviousPermanentPriceModel
     */
    TargetPreviousPermanentPriceModel findValidProductPreviousPrice(String productCode, String currentPermPriceMaxAge,
            String previousPermPriceMinAge, CurrencyModel currency);

    /**
     * Retrieves a List of Previous Permanent Price records that can be removed as they no longer meet the requirements
     * for use. The number of records returned is limited to {batchSize}.
     * 
     * @param currentPermPriceMaxAgeDays
     *            The amount of time, in days, that the end date of valid records can be in the past
     * @param previousPermPriceMinAgeDays
     *            The age, in days, of valid records
     * @param batchSize
     * @return A List of records to be deleted, containing no more records than the value of {batchSize}
     */
    List<TargetPreviousPermanentPriceModel> findAllRemovableTargetPreviousPermanentPriceRecords(
            int currentPermPriceMaxAgeDays, int previousPermPriceMinAgeDays, int batchSize);
}

/**
 * 
 */
package au.com.target.tgtcore.price.daos.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.joda.time.Days;
import org.joda.time.Seconds;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.dbformatter.TargetDBSpecificQueryFactory;
import au.com.target.tgtcore.model.TargetPreviousPermanentPriceModel;
import au.com.target.tgtcore.price.daos.TargetPreviousPermanentPriceDao;
import au.com.target.tgtutility.util.TargetDateUtil;

import com.google.common.base.Preconditions;


/**
 * @author knemalik
 * 
 */
public class TargetPreviousPermanentPriceDaoImpl extends DefaultGenericDao<TargetPreviousPermanentPriceModel> implements
        TargetPreviousPermanentPriceDao {

    private static final Logger LOG = Logger.getLogger(TargetPreviousPermanentPriceDaoImpl.class);
    private static final String SELECT_PREV_PERM_PRICE_RECORDS_QUERY = "select {"
            + TargetPreviousPermanentPriceModel.PK
            + "} from {" + TargetPreviousPermanentPriceModel._TYPECODE + "}";
    private static final String SELECT_ALL_ENDED_PREV_PERM_PRICE_RECORDS_QUERY = SELECT_PREV_PERM_PRICE_RECORDS_QUERY +
            " WHERE {" + TargetPreviousPermanentPriceModel.ENDDATE + "} IS NOT NULL";

    private TargetDBSpecificQueryFactory targetDBSpecificQueryFactory;

    public TargetPreviousPermanentPriceDaoImpl() {
        super(TargetPreviousPermanentPriceModel._TYPECODE);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.price.daos.TargetPreviousPermanentPriceDao#findProductWithNullEndDate(java.lang.String)
     */
    @Override
    public List<TargetPreviousPermanentPriceModel> findProductWithNullEndDate(final String productCode) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(productCode), "Product Code must be not empty");
        // search for the null end date on product
        final StringBuilder queryString = new StringBuilder(SELECT_PREV_PERM_PRICE_RECORDS_QUERY);
        queryString.append(" WHERE {TargetPreviousPermanentPrice.VARIANTCODE} = ?productCode "
                + "AND {TargetPreviousPermanentPrice.ENDDATE} IS NULL");
        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString.toString());
        query.addQueryParameter("productCode", productCode);
        final SearchResult<TargetPreviousPermanentPriceModel> searchResult = getFlexibleSearchService().search(query);
        return searchResult.getResult();
    }


    @Override
    public TargetPreviousPermanentPriceModel findValidProductPreviousPrice(final String productCode,
            final String currentPermPriceMaxAge,
            final String previousPermPriceMinAge, final CurrencyModel currency) {
        Integer maxTimeDays = null;
        Integer minTimeSecond = null;

        try {
            maxTimeDays = Integer.valueOf(Integer.parseInt(currentPermPriceMaxAge));
            minTimeSecond = Integer.valueOf(Integer.parseInt(previousPermPriceMinAge) * 3600 * 24);
        }
        catch (final NumberFormatException e) {
            LOG.error("Number format exception while converting the number of days to integer ", e);
            return null;
        }
        if (maxTimeDays == null || minTimeSecond == null || currency == null || StringUtils.isEmpty(productCode)) {
            LOG.error("Any one of productCode,maxAge,MinAge,Curreny cannot be null");
            return null;
        }

        final StringBuilder queryString = new StringBuilder(SELECT_PREV_PERM_PRICE_RECORDS_QUERY);
        final String queryProductCode = " WHERE {" + TargetPreviousPermanentPriceModel.VARIANTCODE + "} =?productCode ";
        final String queryEndNullCheck = "AND {" + TargetPreviousPermanentPriceModel.ENDDATE + "} IS NOT NULL";
        final String queryMaxTimesOld = " AND "
                + targetDBSpecificQueryFactory.getDBSpecificQueryFormatter().formatDateDiff("DAYS",
                        "{" + TargetPreviousPermanentPriceModel.ENDDATE + "}", "?currentDate")
                + " <=  ?maxTimesOld";
        final String queryMinDisplayAge = " AND"
                + targetDBSpecificQueryFactory.getDBSpecificQueryFormatter().formatDateDiff("SECONDS",
                        "{" + TargetPreviousPermanentPriceModel.STARTDATE + "}",
                        "{" + TargetPreviousPermanentPriceModel.ENDDATE + "}")
                + " >= ?minDisplayAge ";
        final String queryCurrency = " AND {" + TargetPreviousPermanentPriceModel.CURRENCY + "} =?currency ";
        final String queryOrderBy = " ORDER BY {" + TargetPreviousPermanentPriceModel.ENDDATE + "} DESC";
        queryString.append(queryProductCode).append(queryEndNullCheck).append(queryMaxTimesOld)
                .append(queryMinDisplayAge).append(queryCurrency).append(queryOrderBy);
        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString.toString());
        query.addQueryParameter("currentDate", TargetDateUtil.getDateWithMidnightTime(new Date()));
        query.addQueryParameter("productCode", productCode);
        query.addQueryParameter("currency", currency);
        query.addQueryParameter("minDisplayAge", minTimeSecond);
        query.addQueryParameter("maxTimesOld", maxTimeDays);
        final SearchResult<TargetPreviousPermanentPriceModel> searchResult = getFlexibleSearchService().search(query);
        final List<TargetPreviousPermanentPriceModel> resultList = searchResult.getResult();
        if (CollectionUtils.isNotEmpty(resultList)) {
            return resultList.get(0);
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtcore.price.daos.TargetPreviousPermanentPriceDao#findAllRemovableTargetPreviousPermanentPriceRecords(int, int, int)
     */
    @Override
    public List<TargetPreviousPermanentPriceModel> findAllRemovableTargetPreviousPermanentPriceRecords(
            final int currentPermPriceMaxAgeDays,
            final int previousPermPriceMinAgeDays, final int batchSize) {
        final Seconds currentPermPriceMaxAgeSeconds = Days.days(currentPermPriceMaxAgeDays).toStandardSeconds();
        final Seconds previousPermPriceMinAgeSeconds = Days.days(previousPermPriceMinAgeDays).toStandardSeconds();

        // Find all ended records
        final StringBuilder queryBuilder = new StringBuilder(SELECT_ALL_ENDED_PREV_PERM_PRICE_RECORDS_QUERY);

        // Add the record freshness clause
        queryBuilder.append(" AND ("
                + targetDBSpecificQueryFactory.getDBSpecificQueryFormatter().formatDateDiff("SECONDS",
                        "{" + TargetPreviousPermanentPriceModel.ENDDATE + "}", "?currentDate")
                + " >  ?currentPermPriceMaxAgeSeconds");

        // Add the record age clause
        queryBuilder.append(" OR "
                + targetDBSpecificQueryFactory.getDBSpecificQueryFormatter().formatDateDiff("SECONDS",
                        "{" + TargetPreviousPermanentPriceModel.STARTDATE + "}",
                        "{" + TargetPreviousPermanentPriceModel.ENDDATE + "}")
                + " < ?previousPermPriceMinAgeSeconds)");

        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryBuilder.toString());
        query.addQueryParameter("currentDate", DateUtils.round(new Date(), Calendar.MINUTE)); // round to give some hope of database optimisation
        query.addQueryParameter("currentPermPriceMaxAgeSeconds",
                Integer.valueOf(currentPermPriceMaxAgeSeconds.getSeconds()));
        query.addQueryParameter("previousPermPriceMinAgeSeconds",
                Integer.valueOf(previousPermPriceMinAgeSeconds.getSeconds()));

        query.setCount(batchSize);

        final SearchResult<TargetPreviousPermanentPriceModel> results = getFlexibleSearchService()
                .<TargetPreviousPermanentPriceModel> search(query);

        return results.getResult();
    }

    @Required
    public void setTargetDBSpecificQueryFactory(final TargetDBSpecificQueryFactory targetDBSpecificQueryFactory) {
        this.targetDBSpecificQueryFactory = targetDBSpecificQueryFactory;
    }

}

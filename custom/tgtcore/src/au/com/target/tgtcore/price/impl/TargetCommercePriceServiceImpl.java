/**
 * 
 */
package au.com.target.tgtcore.price.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.price.impl.DefaultCommercePriceService;
import de.hybris.platform.commerceservices.util.AbstractComparator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.jalo.TargetPriceRow;
import au.com.target.tgtcore.model.TargetPreviousPermanentPriceModel;
import au.com.target.tgtcore.model.TargetPriceRowModel;
import au.com.target.tgtcore.price.TargetCommercePriceService;
import au.com.target.tgtcore.price.daos.TargetPreviousPermanentPriceDao;
import au.com.target.tgtcore.price.data.PriceRangeInformation;
import au.com.target.tgtcore.stock.TargetStockService;


/**
 * Extend implementation to allow getting of price range information for a product
 * 
 */
public class TargetCommercePriceServiceImpl extends DefaultCommercePriceService implements TargetCommercePriceService {

    private static final Logger LOG = Logger.getLogger(TargetCommercePriceServiceImpl.class);

    private static final String PRICE_ROW_TYPECODE = PriceRowModel._TYPECODE.toLowerCase();
    private static final String PRICE_ROW_WASPRICE = TargetPriceRowModel.WASPRICE.toLowerCase();
    private static final String PROMO_EVENT = TargetPriceRowModel.PROMOEVENT.toLowerCase();
    private static final String CURRENT_PERM_PRICE_AGE = "currentPermPriceMaxAge";
    private static final String PREVIOUS_PERM_PRICE_AGE = "previousPermPriceMinAge";

    private TargetStockService targetStockService;

    private TargetPreviousPermanentPriceDao targetPreviousPermanentPriceDao;

    private CommonI18NService commonI18NService;

    private TargetSharedConfigService targetSharedConfigService;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.price.TargetCommercePriceService#getPriceRangeInfoForProduct(de.hybris.platform.core.model.product.ProductModel)
     */
    @Override
    public PriceRangeInformation getPriceRangeInfoForProduct(final ProductModel product) {
        validateParameterNotNull(product, "Product model cannot be null");

        if (CollectionUtils.isEmpty(product.getVariants())) {
            final PriceInformation priceInfo = getWebPriceForProduct(product);
            final Double sellPrice = getDoublePriceFromPriceInformation(priceInfo);
            Double wasPrice = null;
            if (getPromoEventFromPriceInformation(priceInfo)) {
                wasPrice = getWasPriceForProduct(product);
                // If the was price is invalid then clear it
                if (sellPrice != null && !wasPriceIsValidForSellPrice(sellPrice.doubleValue(), wasPrice)) {
                    wasPrice = null;
                }
            }
            else {
                wasPrice = getWasPriceFromTargetPreviousPermanentPrice(product.getCode());
                if (sellPrice != null && !wasPriceIsValidForSellPrice(sellPrice.doubleValue(), wasPrice)) {
                    wasPrice = null;
                }
            }

            final PriceRangeInformation priceRangeInfo = new PriceRangeInformation(
                    sellPrice, sellPrice,
                    wasPrice, wasPrice);

            return priceRangeInfo;
        }

        PriceInformation lowPrice = null;
        PriceInformation highPrice = null;
        Double lowWasPriceValue = null;
        Double highWasPriceValue = null;

        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.PDP_OPTIMISATION)) {

            final Collection<PriceInformation> prices = new ArrayList<>();
            final Collection<Double> wasPrices = new ArrayList<>();

            fillAllVariantPricesAndWasPrices(product, prices, wasPrices);

            if (CollectionUtils.isNotEmpty(prices)) {
                lowPrice = Collections.min(prices, PriceInformationComparator.INSTANCE);
                highPrice = Collections.max(prices, PriceInformationComparator.INSTANCE);
            }

            // Check was prices are valid for the variants, and if so get highest and lowest 
            if (CollectionUtils.isNotEmpty(wasPrices) && wasPriceIsValid(product)) {
                lowWasPriceValue = Collections.min(wasPrices);
                highWasPriceValue = Collections.max(wasPrices);
            }

        }
        else {
            lowPrice = getLowestVariantPrice(product);
            highPrice = getHighestVariantPrice(product);

            // Check was prices are valid for the variants, and if so get highest and lowest 
            if (wasPriceIsValid(product)) {
                lowWasPriceValue = getLowestVariantWasPrice(product);
                highWasPriceValue = getHighestVariantWasPrice(product);
            }
        }

        // Create a PriceRangeInformation to capture the sell and was price ranges
        final PriceRangeInformation priceRangeInfo = new PriceRangeInformation(
                getDoublePriceFromPriceInformation(lowPrice),
                getDoublePriceFromPriceInformation(highPrice),
                lowWasPriceValue, highWasPriceValue);

        return priceRangeInfo;


    }

    /**
     * fill all variant prices and was prices with one iteration.
     * 
     * @param product
     * @param prices
     */
    private void fillAllVariantPricesAndWasPrices(final ProductModel product,
            final Collection<PriceInformation> prices, final Collection<Double> wasPrices) {
        // Get the price for the current product
        final PriceInformation priceInformation = getWebPriceForProduct(product);
        if (priceInformation != null) {
            prices.add(priceInformation);
            if (getPromoEventFromPriceInformation(priceInformation)) {
                wasPrices.add(getWasPrice(priceInformation));
            }
            else {
                final Double wasPriceFromPrevious = getWasPriceFromTargetPreviousPermanentPrice(product.getCode());
                if (wasPriceFromPrevious != null) {
                    wasPrices.add(wasPriceFromPrevious);
                }
            }

        }

        final Collection<VariantProductModel> variants = product.getVariants();
        if (CollectionUtils.isNotEmpty(variants)) {
            for (final VariantProductModel variant : variants) {
                fillAllVariantPricesAndWasPrices(variant, prices, wasPrices);
            }
        }
    }

    protected Double getWasPriceFromTargetPreviousPermanentPrice(final String productCode) {
        Double wasPrice = null;
        final TargetPreviousPermanentPriceModel targetPreviousPermanentPriceModel = targetPreviousPermanentPriceDao
                .findValidProductPreviousPrice(productCode,
                        targetSharedConfigService.getConfigByCode(CURRENT_PERM_PRICE_AGE),
                        targetSharedConfigService.getConfigByCode(PREVIOUS_PERM_PRICE_AGE),
                        commonI18NService.getCurrency(TgtCoreConstants.AUSTRALIAN_DOLLARS));
        if (targetPreviousPermanentPriceModel != null) {
            wasPrice = targetPreviousPermanentPriceModel.getPrice();
        }
        return wasPrice;
    }

    @Override
    protected void fillAllVariantPrices(final ProductModel product, final Collection<PriceInformation> prices) {
        final PriceInformation priceInformation = getWebPriceForProduct(product);
        if (priceInformation != null) {
            prices.add(priceInformation);
        }

        final Collection<VariantProductModel> variants = product.getVariants();
        if (CollectionUtils.isNotEmpty(variants)) {
            for (final VariantProductModel variant : variants) {
                fillAllVariantPrices(variant, prices);
            }
        }
    }

    /**
     * Get the was price if there is one relating to the web price
     * 
     * @param product
     * @return Double or null if there is no was price
     */
    protected Double getWasPriceForProduct(final ProductModel product) {
        final PriceInformation priceInfo = getWebPriceForProduct(product);
        return getWasPrice(priceInfo);
    }


    protected PriceInformation getHighestVariantPrice(final ProductModel product) {
        final Collection<PriceInformation> allVariantPrices = getAllVariantPrices(product);
        if (CollectionUtils.isNotEmpty(allVariantPrices)) {
            return Collections.max(allVariantPrices, PriceInformationComparator.INSTANCE);
        }
        return null;
    }


    /**
     * Return true if the was prices are valid for each variant, <br/>
     * ie not null and less than the sell price.
     * 
     * @param product
     * @return true if was price is valid
     */
    protected boolean wasPriceIsValid(final ProductModel product) {

        boolean isValid = true;

        // Check the price & was price for each variant that has a valid web price
        // If the was price is null or less than the price then return invalid
        for (final VariantProductModel var : product.getVariants()) {
            final PriceInformation priceInfo = getWebPriceForProduct(var);
            if (priceInfo == null || priceInfo.getPriceValue() == null) {
                isValid = false;
                break;
            }
            final double sellPrice = priceInfo.getPriceValue().getValue();
            if (getPromoEventFromPriceInformation(priceInfo)) {
                final Double wasPrice = getWasPrice(priceInfo);
                if (!wasPriceIsValidForSellPrice(sellPrice, wasPrice)) {
                    isValid = false;
                    break;
                }

            }
            else {
                final Double wasPrice = getWasPriceFromTargetPreviousPermanentPrice(var.getCode());
                if (!wasPriceIsValidForSellPrice(sellPrice, wasPrice)) {
                    isValid = false;
                    break;
                }

            }

        }

        return isValid;
    }


    private boolean wasPriceIsValidForSellPrice(final double sellPrice, final Double wasPrice) {
        return (wasPrice != null && wasPrice.doubleValue() > sellPrice);
    }


    protected Double getLowestVariantWasPrice(final ProductModel product) {
        Double lowestWasPriceInfo = null;

        final Collection<Double> allVariantPrices = getAllVariantWasPrices(product);
        if (CollectionUtils.isNotEmpty(allVariantPrices)) {
            lowestWasPriceInfo = Collections.min(allVariantPrices);
        }
        return lowestWasPriceInfo;
    }


    protected Double getHighestVariantWasPrice(final ProductModel product) {
        Double highestWasPriceInfo = null;

        final Collection<Double> allVariantPrices = getAllVariantWasPrices(product);
        if (CollectionUtils.isNotEmpty(allVariantPrices)) {
            highestWasPriceInfo = Collections.max(allVariantPrices);
        }

        return highestWasPriceInfo;
    }

    /**
     * get all the variant prices with promoEvent true
     * 
     * @param product
     * @return Collection<PriceInformation>
     */
    protected Collection<Double> getAllVariantWasPrices(final ProductModel product)
    {
        final Collection<Double> prices = new ArrayList<>();
        fillAllVariantWasPrices(product, prices);
        return prices;
    }

    /**
     * fill all variant prices with promoEvent true
     * 
     * @param product
     * @param prices
     */
    protected void fillAllVariantWasPrices(final ProductModel product,
            final Collection<Double> prices)
    {
        // Get the price for the current product
        final PriceInformation priceInformation = getWebPriceForProduct(product);
        if (priceInformation != null) {
            if (getPromoEventFromPriceInformation(priceInformation))
            {
                prices.add(getWasPrice(priceInformation));
            }
            else {
                final Double wasPriceFromPreious = getWasPriceFromTargetPreviousPermanentPrice(product.getCode());
                if (wasPriceFromPreious != null) {
                    prices.add(wasPriceFromPreious);
                }
            }

        }

        final Collection<VariantProductModel> variants = product.getVariants();
        if (CollectionUtils.isNotEmpty(variants))
        {
            for (final VariantProductModel variant : variants)
            {
                fillAllVariantWasPrices(variant, prices);
            }
        }
    }

    /**
     * Convert the given PriceInformation web price to Double format
     * 
     * @param priceInfo
     * @return Double value for web price
     */
    protected Double getDoublePriceFromPriceInformation(final PriceInformation priceInfo) {
        Double doublePrice = null;

        if (priceInfo != null && priceInfo.getPriceValue() != null) {
            doublePrice = new Double(priceInfo.getPriceValue().getValue());
        }

        return doublePrice;
    }

    /**
     * Extract the was price value from the give PriceInformation
     * 
     * @param priceInfo
     * @return Double or null if there is no was price
     */
    protected static Double getWasPrice(final PriceInformation priceInfo) {
        Double rowWasPrice = null;

        if (priceInfo != null && priceInfo.getPriceValue() != null) {
            // Unwrap the PriceRow and extract wasPrice if it is an instance of TargetPriceRow
            // Exceptions are swallowed with a logged error since showing no was price is preferable to throwing up an error to user
            final PriceRow row = (PriceRow)priceInfo.getQualifierValue(PRICE_ROW_TYPECODE);

            if (row instanceof TargetPriceRow) {
                try {
                    rowWasPrice = (Double)row.getAttribute(PRICE_ROW_WASPRICE);
                }
                catch (final JaloInvalidParameterException e) {
                    LOG.error("JaloInvalidParameterException in getWasPrice", e);
                }
                catch (final JaloSecurityException e) {
                    LOG.error("JaloSecurityException in getWasPrice", e);
                }
            }
            else {
                LOG.warn("Encountered a PriceRow which is not an instance of TargetPriceRow");
            }
        }
        return rowWasPrice;
    }

    /**
     * get promoEvent from from price information
     * 
     * @param priceInfo
     * @return boolean promoEvent, default is true to run the old logic
     */
    protected static boolean getPromoEventFromPriceInformation(final PriceInformation priceInfo) {
        boolean promoEvent = true;

        if (priceInfo != null && priceInfo.getPriceValue() != null) {

            final PriceRow row = (PriceRow)priceInfo.getQualifierValue(PRICE_ROW_TYPECODE);

            if (row instanceof TargetPriceRow) {
                try {
                    promoEvent = (Boolean)row.getAttribute(PROMO_EVENT) == null ? true : ((Boolean)row
                            .getAttribute(PROMO_EVENT)).booleanValue();
                }
                catch (final JaloInvalidParameterException e) {
                    LOG.error("JaloInvalidParameterException in getWasPrice", e);
                }
                catch (final JaloSecurityException e) {
                    LOG.error("JaloSecurityException in getWasPrice", e);
                }
            }
            else {
                LOG.warn("Encountered a PriceRow which is not an instance of TargetPriceRow");
            }
        }
        return promoEvent;
    }


    /**
     * Comparator to order PriceInformation by was price value ascending
     */
    public static class WasPriceInformationComparator extends AbstractComparator<PriceInformation> {
        public static final WasPriceInformationComparator INSTANCE = new WasPriceInformationComparator();

        @Override
        protected int compareInstances(final PriceInformation price1, final PriceInformation price2) {
            Assert.isTrue(price1.getPriceValue().getCurrencyIso().equals(price2.getPriceValue().getCurrencyIso()),
                    "differing currency of prices");

            final Double d1 = getWasPrice(price1);
            final Double d2 = getWasPrice(price2);

            // Check for one or other of the instances being null - nulls are ordered first
            if (d1 == null) {
                return -1;
            }
            if (d2 == null) {
                return 1;
            }
            return compareValues(d1.doubleValue(), d2.doubleValue());
        }
    }

    /**
     * @return the targetStockService
     */
    protected TargetStockService getTargetStockService() {
        return targetStockService;
    }


    /**
     * @param targetStockService
     *            the targetStockService to set
     */
    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }

    /**
     * @param targetPreviousPermanentPriceDao
     *            the targetPreviousPermanentPriceDao to set
     */
    @Required
    public void setTargetPreviousPermanentPriceDao(final TargetPreviousPermanentPriceDao targetPreviousPermanentPriceDao) {
        this.targetPreviousPermanentPriceDao = targetPreviousPermanentPriceDao;
    }

    /**
     * @param commonI18NService
     *            the commonI18NService to set
     */
    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    /**
     * @param targetSharedConfigService
     *            the targetSharedConfigService to set
     */
    @Required
    public void setTargetSharedConfigService(final TargetSharedConfigService targetSharedConfigService) {
        this.targetSharedConfigService = targetSharedConfigService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitch to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }



}

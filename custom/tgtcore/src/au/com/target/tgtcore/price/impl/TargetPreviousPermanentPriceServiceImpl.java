/**
 * 
 */
package au.com.target.tgtcore.price.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetPreviousPermanentPriceModel;
import au.com.target.tgtcore.model.TargetPriceRowModel;
import au.com.target.tgtcore.price.TargetPreviousPermanentPriceService;
import au.com.target.tgtcore.price.daos.TargetPreviousPermanentPriceDao;
import au.com.target.tgtcore.product.TargetProductPriceService;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * @author knemalik
 * 
 */
public class TargetPreviousPermanentPriceServiceImpl extends AbstractBusinessService implements
        TargetPreviousPermanentPriceService {

    private static final Logger LOG = Logger.getLogger(TargetPreviousPermanentPriceServiceImpl.class);
    private int cleanUpBatchSize;

    private TargetPreviousPermanentPriceDao targetPreviousPermanentPriceDao;
    private TargetProductService productService;
    private CatalogVersionService catalogVersionService;
    private TargetProductPriceService targetProductPriceService;
    private CommonI18NService commonI18NService;

    private TargetSharedConfigService targetSharedConfigService;

    /* (non-Javadoc)
    * @see au.com.target.tgtcore.price.TargetPreviousPermanentPriceService#updatePreviousPermanentPriceEndDate(java.lang.String)
    */
    @Override
    public void updatePreviousPermanentPriceEndDate(final String productCode) {
        validateParameterNotNull(productCode, "Product code cannot be null");
        final List<TargetPreviousPermanentPriceModel> productCodeWithNullEndDate = targetPreviousPermanentPriceDao
                .findProductWithNullEndDate(productCode);

        if (CollectionUtils.isEmpty(productCodeWithNullEndDate)) {
            return;
        }

        if (productCodeWithNullEndDate.size() > 1) {
            LOG.warn(SplunkLogFormatter.formatMessage("Multiple records found for the product code with null enddate :"
                    + productCode,
                    TgtutilityConstants.ErrorCode.WARN_TGTCORE));
        }

        for (final TargetPreviousPermanentPriceModel targetPreviousPermanentPriceModel : productCodeWithNullEndDate) {
            targetPreviousPermanentPriceModel.setEndDate(new Date());
            getModelService().save(targetPreviousPermanentPriceModel);
        }

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.price.TargetPreviousPermanentPriceService#getPreviousPermanentPriceWithNullEndDate(java.lang.String)
     */
    @Override
    public TargetPreviousPermanentPriceModel getPreviousPermanentPriceWithNullEndDate(final String variantCode) {
        validateParameterNotNull(variantCode, "Product code cannot be null");
        final List<TargetPreviousPermanentPriceModel> productCodeWithNullEndDate = targetPreviousPermanentPriceDao
                .findProductWithNullEndDate(variantCode);

        if (CollectionUtils.isEmpty(productCodeWithNullEndDate)) {
            return null;
        }

        if (productCodeWithNullEndDate.size() > 1) {
            LOG.warn(SplunkLogFormatter.formatMessage("Multiple records found for the product code with null enddate :"
                    + variantCode,
                    TgtutilityConstants.ErrorCode.WARN_TGTCORE));
        }
        return productCodeWithNullEndDate.get(0);
    }


    @Override
    public void createOrUpdatePermanentPrice(final String variantCode, final Double price, final boolean promoEvent) {

        final TargetPreviousPermanentPriceModel permPriceModel = getPreviousPermanentPriceWithNullEndDate(variantCode);
        final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION);

        final ProductModel productModel = productService.getProductForCode(catalogVersion, variantCode);
        final CurrencyModel currencyModel = commonI18NService.getCurrency(TgtCoreConstants.AUSTRALIAN_DOLLARS);
        //shouldn't create or update previous Permanent price if the product status is unapproved or check
        if (!ArticleApprovalStatus.APPROVED.equals(productModel.getApprovalStatus())) {
            return;
        }
        if (null != permPriceModel) {
            //if there is a Permanent Price with unended date exists
            final BigDecimal priceFromPOS = BigDecimal.valueOf(price.doubleValue());
            final BigDecimal previousPermPrice = BigDecimal.valueOf(permPriceModel.getPrice().doubleValue());
            // update the previous permanent price table,if the price is not  same and promoevent is false
            if (!promoEvent && ((priceFromPOS.compareTo(previousPermPrice)) != 0)) {
                //end the current previous price
                updatePreviousPermanentPriceEndDate(variantCode);
                createNewPreviousPermanentPrice(
                        variantCode, price, currencyModel);

            }
            else if (promoEvent) {
                //if its a promoevent and previous permanent price exists with unended date,then update the enddate
                updatePreviousPermanentPriceEndDate(variantCode);
            }

            return;
        }
        else {
            if (!promoEvent) {
                //No previous permanent price exists and it is not a promoevent ,then create one
                createNewPreviousPermanentPrice(variantCode, price, currencyModel);
                return;

            }

        }

    }

    /**
     * 
     * Creates an instance of TargetPreviousPermanentPriceModel with current startDate
     * 
     * @param variantCode
     * @param price
     * @param currency
     * 
     */
    protected void createNewPreviousPermanentPrice(final String variantCode,
            final Double price, final CurrencyModel currency) {
        final TargetPreviousPermanentPriceModel targetPreviousPriceModel = getModelService().create(
                TargetPreviousPermanentPriceModel.class);
        targetPreviousPriceModel.setVariantCode(variantCode);
        targetPreviousPriceModel.setPrice(price);
        targetPreviousPriceModel.setStartDate(new Date());
        targetPreviousPriceModel.setEndDate(null);
        targetPreviousPriceModel.setCurrency(currency);
        getModelService().save(targetPreviousPriceModel);
    }

    /* (non-Javadoc)
    * @see au.com.target.tgtcore.price.TargetPreviousPermanentPriceService#insertPreviousPermanentPrice(java.lang.String)
    */
    @Override
    public void createPreviousPermanentPrice(final String productCode) {

        final TargetPriceRowModel targetPriceRowModel = targetProductPriceService
                .getCurrentTargetPriceRowModelFromStaged(productCode);
        if (targetPriceRowModel == null) {
            return;
        }
        final Double currentTargetPrice = targetPriceRowModel.getPrice();
        if (BooleanUtils.isFalse(targetPriceRowModel.getPromoEvent()) || targetPriceRowModel.getPromoEvent() == null) {
            createNewPreviousPermanentPrice(productCode, currentTargetPrice,
                    commonI18NService.getCurrency(TgtCoreConstants.AUSTRALIAN_DOLLARS));
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.price.TargetPreviousPermanentPriceService#cleanupPreviousPermanentPriceRecords()
     */
    @Override
    public boolean cleanupPreviousPermanentPriceRecords() {
        final int previousPermPriceMinAge = Integer.parseInt(targetSharedConfigService
                .getConfigByCode(TgtCoreConstants.Config.PREVIOUS_PERM_PRICE_MIN_AGE));
        final int currentPermPriceMaxAge = Integer.parseInt(targetSharedConfigService
                .getConfigByCode(TgtCoreConstants.Config.CURRENT_PERM_PRICE_MAX_AGE));

        final List<TargetPreviousPermanentPriceModel> removablePrevPermPrices = targetPreviousPermanentPriceDao
                .findAllRemovableTargetPreviousPermanentPriceRecords(currentPermPriceMaxAge, previousPermPriceMinAge,
                        cleanUpBatchSize);

        if (CollectionUtils.isEmpty(removablePrevPermPrices)) {
            return false;
        }

        getModelService().removeAll(removablePrevPermPrices);
        return true;
    }

    /**
     * @param targetPreviousPermanentPriceDao
     *            the targetPreviousPermanentPriceDao to set
     */
    @Required
    public void setTargetPreviousPermanentPriceDao(final TargetPreviousPermanentPriceDao targetPreviousPermanentPriceDao) {
        this.targetPreviousPermanentPriceDao = targetPreviousPermanentPriceDao;

    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @param productService
     *            the productService to set
     */
    @Required
    public void setProductService(final TargetProductService productService) {
        this.productService = productService;

    }

    /**
     * @param targetProductPriceService
     *            the targetProductPriceService to set
     */
    @Required
    public void setTargetProductPriceService(final TargetProductPriceService targetProductPriceService) {
        this.targetProductPriceService = targetProductPriceService;
    }


    /**
     * @param commonI18NService
     *            the commonI18NService to set
     */
    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    /**
     * @param targetSharedConfigService
     *            the targetSharedConfigService to set
     */
    @Required
    public void setTargetSharedConfigService(final TargetSharedConfigService targetSharedConfigService) {
        this.targetSharedConfigService = targetSharedConfigService;
    }

    /**
     * @param cleanUpBatchSize
     *            the cleanUpBatchSize to set
     */
    @Required
    public void setCleanUpBatchSize(final int cleanUpBatchSize) {
        this.cleanUpBatchSize = cleanUpBatchSize;
    }
}
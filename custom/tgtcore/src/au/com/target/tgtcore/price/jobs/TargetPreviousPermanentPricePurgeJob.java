/**
 * 
 */
package au.com.target.tgtcore.price.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.price.TargetPreviousPermanentPriceService;


/**
 * @author rmcalave
 * 
 */
public class TargetPreviousPermanentPricePurgeJob extends AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(TargetPreviousPermanentPricePurgeJob.class);

    private TargetPreviousPermanentPriceService targetPreviousPermanentPriceService;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.CronJobModel)
     */
    @Override
    public PerformResult perform(final CronJobModel cronJob) {
        LOG.info("Starting purge of TargetPreviousPermanentPrice records");
        while (targetPreviousPermanentPriceService.cleanupPreviousPermanentPriceRecords()) {
            if (clearAbortRequestedIfNeeded(cronJob)) {
                LOG.warn("Aborted: purge of TargetPreviousPermanentPrice records");
                return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
            }
        }

        LOG.info("Finished successfully: purge of TargetPreviousPermanentPrice records");
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#isAbortable()
     */
    @Override
    public boolean isAbortable() {
        return true;
    }



    /**
     * @param targetPreviousPermanentPriceService
     *            the targetPreviousPermanentPriceService to set
     */
    @Required
    public void setTargetPreviousPermanentPriceService(
            final TargetPreviousPermanentPriceService targetPreviousPermanentPriceService) {
        this.targetPreviousPermanentPriceService = targetPreviousPermanentPriceService;
    }
}

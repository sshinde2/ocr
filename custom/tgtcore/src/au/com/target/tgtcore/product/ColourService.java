/**
 * 
 */
package au.com.target.tgtcore.product;

import au.com.target.tgtcore.model.ColourModel;


/**
 * Target Colour Service
 * 
 */
public interface ColourService {
    /**
     * Get Colour By Name
     * 
     * @param name
     * @return ColorModel
     */
    ColourModel getColourForFuzzyName(String name, boolean createIfInexistent, boolean isDisplay);
}

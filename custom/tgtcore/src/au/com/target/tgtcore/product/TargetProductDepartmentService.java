/**
 * 
 */
package au.com.target.tgtcore.product;

import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.List;

import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.data.TargetProductCountMerchDepRowData;


/**
 * @author pthoma20
 * 
 *         This class updates the department model passed to it and associates target product model with the department.
 * 
 */
public interface TargetProductDepartmentService {

    /**
     * This method will validate the department found. It will validate whether the product is already having same
     * department- No update required. If the product is having a different department associated - remove existing and
     * associate new department. If the product is having a no department - add the new department.
     * 
     * @param targetProductModel
     * @param targetMerchDepartmentModel
     * @return isUpdateRequired
     */
    public boolean processMerchDeparment(final TargetProductModel targetProductModel,
            final TargetMerchDepartmentModel targetMerchDepartmentModel);

    /**
     * Retrieves the Department Category Model based on the department passed and the catalog version for which it is
     * required.
     * 
     * @param catalogVersionModel
     * @param department
     * @return categoryModel
     */
    public TargetMerchDepartmentModel getDepartmentCategoryModel(final CatalogVersionModel catalogVersionModel,
            final Integer department);

    /**
     * find the product count for each merchDepartment. if the merch dep has no product, it won't be in the list
     * 
     * @return List<TargetProductCountMerchDepRowData>
     */
    public List<TargetProductCountMerchDepRowData> findProductCountForEachMerchDep();

}

package au.com.target.tgtcore.product;

import de.hybris.platform.catalog.model.CatalogVersionModel;

import au.com.target.tgtcore.model.TargetProductDimensionsModel;


/**
 * Service to manage {@link TargetProductDimensionsModel}
 * 
 */
public interface TargetProductDimensionsService {

    /**
     * Get the product dimensions for a particular dimension code for corresponding catalog version
     * 
     * @param productDimensionsCode
     * @return {@link TargetProductDimensionsModel}
     */
    TargetProductDimensionsModel getProductDimensionsForCode(final CatalogVersionModel catalogVersion,
            String productDimensionsCode);



}

package au.com.target.tgtcore.product;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;

import java.util.List;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


public interface TargetProductSearchService
{
    /**
     * Get TargetSizeVariantProductModel List by baseProduct
     * 
     * @param baseProduct
     * @return TargetSizeVariantProductModel List
     */
    List<TargetSizeVariantProductModel> getTargetSizeVariantProductModelByBaseProduct(String baseProduct);

    /**
     * Get TargetSizeVariantProductModel List by produce code
     * 
     * @param code
     * @return TargetSizeVariantProductModel List
     */
    List<TargetSizeVariantProductModel> getTargetSizeVariantProductModelByCode(String code);

    /**
     * Finds unique color variant model for given {@code baseProductCode} and {@code swatchCode} or throws an exception.
     * 
     * @param baseProductCode
     *            the base product code to look variant for
     * @param swatchCode
     *            the swatch code to look variant for
     * @return the color variant model
     * @throws ModelNotFoundException
     *             if no record found
     * @throws AmbiguousIdentifierException
     *             if multiple records found
     */
    TargetColourVariantProductModel getColourVariantByBaseProductAndSwatch(String baseProductCode, String swatchCode)
            throws ModelNotFoundException, AmbiguousIdentifierException;

    /**
     * Returns all Target Product Models by taking catalog version, start position and count as input
     * 
     * @param catalogVersion
     *            catalog version - can be online or staged.
     * 
     * 
     * @return List of TargetProductModel
     */
    List<TargetProductModel> getAllTargetProductsWithNoDepartment(final String catalogVersion, int start, int count);

    /**
     * Checks if product is available for purchase i.e. any of its sellable variant is approved, have defined price and
     * stock.
     *
     * @param productModel
     *            the product model
     * @return true, if is product available for purchase
     */
    boolean isProductAvailableToSell(ProductModel productModel);

}

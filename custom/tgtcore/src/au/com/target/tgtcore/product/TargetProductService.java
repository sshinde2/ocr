/**
 * 
 */
package au.com.target.tgtcore.product;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;

import java.util.List;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;



/**
 * @author rmcalave
 * 
 */
public interface TargetProductService extends ProductService {

    List<ProductModel> getProductsForPrimarySuperCategory(CategoryModel category);

    List<AbstractTargetVariantProductModel> getBulkyBoardProducts();

    /**
     * This method returns the list of productModel for the DeptCode
     * 
     * @param targetMerchDeptModel
     * @param start
     * @param count
     * @return List of TargetColourVariantProductModel
     */
    List<TargetColourVariantProductModel> getProductsForMerchDeptCode(
            final TargetMerchDepartmentModel targetMerchDeptModel,
            int start,
            int count);

    /**
     * Set Online Date to the colour variant, if it qualifies and to all its sub-variants.
     * 
     * @param product
     */
    void setOnlineDateIfApplicable(ProductModel product);

    /**
     * Retrieve product model by product ean.
     * 
     * @param ean
     * @return product
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    ProductModel getProductByEan(String ean)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;

    /**
     * Check if the given product is a drop ship product.
     * 
     * @param product
     *            The product to check
     * @return true if the given product is a drop ship product, false otherwise
     */
    boolean isDropShipProduct(ProductModel product);

    /**
     * get productModel for admin with catalogversion and code
     * 
     * @return productModel
     */
    ProductModel getProductByCatalogVersion(final CatalogVersionModel catalogVersion, final String code);

    /**
     * Finds products for which onlineDate needs to be set and then sets it
     */
    void setOnlineDateForApplicableProducts();
}
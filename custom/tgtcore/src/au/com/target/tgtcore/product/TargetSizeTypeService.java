/**
 * 
 */
package au.com.target.tgtcore.product;

import au.com.target.tgtcore.model.SizeTypeModel;


/**
 * The Interface TargetSizeTypeService.
 * 
 * @author nandini
 */
public interface TargetSizeTypeService {

    /**
     * Find size type for code.
     * 
     * @param code
     *            the code
     * @return the size type model
     */
    SizeTypeModel findSizeTypeForCode(final String code);

    /**
     * Gets the default size type.
     * 
     * @return the default size type
     */
    SizeTypeModel getDefaultSizeType();

}

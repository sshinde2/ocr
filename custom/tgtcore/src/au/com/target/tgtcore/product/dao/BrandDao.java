package au.com.target.tgtcore.product.dao;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.BrandModel;


/**
 * Brand Dao
 */
public interface BrandDao {

    /**
     * @param code
     * @return {@link BrandModel}
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    BrandModel getBrandByCode(String code) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException;

}

/**
 * 
 */
package au.com.target.tgtcore.product.dao;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.ColourModel;


/**
 * 
 * Colour Dao Class
 * 
 */
public interface ColourDao {
    /**
     * Get Colour by Code
     * 
     * @param code
     * @return ColourModel
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    ColourModel getColourByCode(String code) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException;

}

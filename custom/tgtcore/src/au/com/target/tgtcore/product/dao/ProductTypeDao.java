package au.com.target.tgtcore.product.dao;

import au.com.target.tgtcore.model.ProductTypeModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;

/**
 * The DAO for the {@link ProductTypeModel}.
 */
public interface ProductTypeDao {

    /**
     * Returns the product type for a given {@code code}.
     *
     * @param code the product type code
     * @return the product type instance
     * @throws ModelNotFoundException if no record found
     * @throws AmbiguousIdentifierException if multiple records found
     */
    ProductTypeModel getByCode(String code) throws ModelNotFoundException, AmbiguousIdentifierException;
}

package au.com.target.tgtcore.product.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;


/**
 * TargetProductDimensionsDao
 */
public interface TargetProductDimensionsDao {

    /**
     * Get Product Dimensions for a partcular dimension code and corresponding catalog version
     * 
     * @param dimensionCode
     * @param catalogVersion
     * @return {@link TargetProductDimensionsModel}
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    TargetProductDimensionsModel getTargetProductDimensionByCode(CatalogVersionModel catalogVersion,
            String dimensionCode)
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException;

}

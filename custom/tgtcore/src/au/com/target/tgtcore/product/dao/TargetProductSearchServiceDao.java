package au.com.target.tgtcore.product.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;

import java.util.List;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


public interface TargetProductSearchServiceDao
{
    /**
     * Get TargetSizeVariantProductModel List by baseProduct
     * 
     * @param baseProduct
     * @return TargetSizeVariantProductModel List
     */
    List<TargetSizeVariantProductModel> getTargetSizeVariantProductModelByBaseProduct(String baseProduct);

    /**
     * Get TargetSizeVariantProductModel List by produce code
     * 
     * @param code
     * @return TargetSizeVariantProductModel List
     */
    List<TargetSizeVariantProductModel> getTargetSizeVariantProductModelByCode(String code);

    /**
     * Find all product variants from Online catalog
     * 
     * @param query
     * @param start
     *            the position to start getting products from
     * @return List<AbstractTargetVariantProductModel>
     */
    List<AbstractTargetVariantProductModel> getAllProductVariants(final String query, final int start);

    /**
     * Return a product color variant by given {@code baseProductCode} and {@code swatchCode}. Note that it searches for
     * products in "Staged" catalog version.
     * 
     * @param baseProductCode
     *            the base product code to look variant for
     * @param swatchCode
     *            the swatch code to look variant for
     * @param productCatalog
     *            the product catalog version
     * @return the color variant model
     * @throws ModelNotFoundException
     *             if no record found
     * @throws AmbiguousIdentifierException
     *             if multiple records found
     */
    TargetColourVariantProductModel getByBaseProductAndSwatch(final String baseProductCode, final String swatchCode,
            final CatalogVersionModel productCatalog)
            throws ModelNotFoundException, AmbiguousIdentifierException;

    /**
     * Find all TargetProducts From provided catalog version and not having any department.
     * 
     * @param catalogVersion
     * @param start
     * @param count
     *            Takes the catalog version model, start index and batch size as input before querying the products from
     *            target product staged catalog
     * @return List<AbstractTargetVariantProductModel>
     */
    List<TargetProductModel> getAllTargetProductsWithNoDepartment(final CatalogVersionModel catalogVersion,
            final int start,
            final int count);

}

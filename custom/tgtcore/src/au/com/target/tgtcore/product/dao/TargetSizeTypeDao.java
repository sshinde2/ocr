/**
 * 
 */
package au.com.target.tgtcore.product.dao;

import au.com.target.tgtcore.model.SizeTypeModel;


/**
 * @author nandini
 * 
 */
public interface TargetSizeTypeDao {

    /**
     * Gets the size type by code.
     * 
     * @param code
     *            the code
     * @return the size type by code
     */
    SizeTypeModel getSizeTypeByCode(String code);

    /**
     * Gets the default size type.
     * 
     * @return the default size type
     */
    SizeTypeModel getDefaultSizeType();

}

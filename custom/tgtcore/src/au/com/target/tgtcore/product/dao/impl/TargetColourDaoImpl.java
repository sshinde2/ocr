/**
 * 
 */
package au.com.target.tgtcore.product.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.ColourModel;
import au.com.target.tgtcore.product.dao.ColourDao;
import au.com.target.tgtcore.util.TargetServicesUtil;


/**
 * Implementation of TargetColourDao see more {@link ColourDao}
 * 
 */
public class TargetColourDaoImpl extends DefaultGenericDao<ColourModel> implements ColourDao {
    /**
     * Default Constructor
     */
    public TargetColourDaoImpl() {
        super(ColourModel._TYPECODE);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.product.dao.TargetColourDao#getColourByCode(java.lang.String)
     */
    @Override
    public ColourModel getColourByCode(final String code) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        ServicesUtil.validateParameterNotNullStandardMessage("code", code);

        final Map<String, String> params = new HashMap<String, String>();
        params.put(ColourModel.CODE, code);
        final List<ColourModel> colourModels = find(params);

        TargetServicesUtil.validateIfSingleResult(colourModels, ColourModel.class, ColourModel.CODE, code);

        return colourModels.get(0);
    }

}

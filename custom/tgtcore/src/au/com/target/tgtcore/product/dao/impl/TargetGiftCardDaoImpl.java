/**
 * 
 */
package au.com.target.tgtcore.product.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.GiftCardModel;
import au.com.target.tgtcore.product.dao.GiftCardDao;
import au.com.target.tgtcore.util.TargetServicesUtil;


/**
 * @author smishra1
 *
 */
public class TargetGiftCardDaoImpl extends DefaultGenericDao implements GiftCardDao {

    public TargetGiftCardDaoImpl() {
        super(GiftCardModel._TYPECODE);
    }

    @Override
    public GiftCardModel getGiftCardByBrandId(final String brandId) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        ServicesUtil.validateParameterNotNullStandardMessage("brandId", brandId);
        final Map<String, String> params = new HashMap<String, String>();
        params.put(GiftCardModel.BRANDID, brandId);
        final List<GiftCardModel> giftCards = find(params);
        TargetServicesUtil.validateIfSingleResult(giftCards, GiftCardModel.class, GiftCardModel.BRANDID, brandId);

        return giftCards.get(0);
    }
}

package au.com.target.tgtcore.product.dao.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtcore.product.dao.TargetProductDimensionsDao;
import au.com.target.tgtcore.util.TargetServicesUtil;


/**
 * Default implementation of {@link TargetProductDimensionsDao}
 */
public class TargetProductDimensionsDaoImpl extends DefaultGenericDao implements TargetProductDimensionsDao {


    public TargetProductDimensionsDaoImpl() {
        super(TargetProductDimensionsModel._TYPECODE);
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtcore.product.dao.TargetProductDimensionsDao#getTargetProductDimensionByCode(java.lang.String)
     */
    @Override
    public TargetProductDimensionsModel getTargetProductDimensionByCode(final CatalogVersionModel catalogVersion,
            final String dimensionCode)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        ServicesUtil.validateParameterNotNullStandardMessage(TargetProductDimensionsModel.DIMENSIONCODE, dimensionCode);
        ServicesUtil.validateParameterNotNullStandardMessage(TargetProductDimensionsModel.CATALOGVERSION,
                catalogVersion);
        final Map params = new HashMap();
        params.put(TargetProductDimensionsModel.DIMENSIONCODE, dimensionCode);
        params.put(TargetProductDimensionsModel.CATALOGVERSION, catalogVersion);
        final List<TargetProductDimensionsModel> targetProductDimensionsModels = find(params);
        TargetServicesUtil.validateIfSingleResult(targetProductDimensionsModels, TargetProductDimensionsModel.class,
                TargetProductDimensionsModel.DIMENSIONCODE,
                dimensionCode);

        return targetProductDimensionsModels.get(0);
    }
}

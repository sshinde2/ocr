/**
 * 
 */
package au.com.target.tgtcore.product.exception;

import de.hybris.platform.servicelayer.exceptions.BusinessException;


/**
 * Class for Product Not Found exception
 * 
 * @author pthoma20
 */
public class ProductNotFoundException extends BusinessException {

    public ProductNotFoundException(final String message) {
        super(message);
    }


    public ProductNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

}

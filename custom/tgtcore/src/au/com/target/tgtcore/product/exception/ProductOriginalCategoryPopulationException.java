/**
 * 
 */
package au.com.target.tgtcore.product.exception;

/**
 * Class for Product Original Category Population exception
 * 
 * @nullah
 */
public class ProductOriginalCategoryPopulationException extends Exception {

    public ProductOriginalCategoryPopulationException(final String message) {
        super(message);
    }


    public ProductOriginalCategoryPopulationException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
/**
 * 
 */
package au.com.target.tgtcore.product.impl;

import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.util.StandardDateRange;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtcore.model.TargetPriceRowModel;
import au.com.target.tgtcore.product.data.TargetPriceRowData;


/**
 * Strategy for determining whether an update to prices is required
 * 
 */
public class PriceUpdateRequiredStrategy {

    private static final Logger LOG = Logger.getLogger(PriceUpdateRequiredStrategy.class);

    /**
     * Determine whether there is a difference between the current and given new prices
     * 
     * @param currentPrices
     * @param newPrice
     * @param newWasPrice
     * @param futurePrices
     * @return true if there is a difference
     */
    public boolean isPriceUpdateRequired(final Collection<PriceRowModel> currentPrices, final double newPrice,
            final double newWasPrice, final boolean promoEvent, final List<TargetPriceRowData> futurePrices) {

        // There are no existing prices so we are adding
        if (CollectionUtils.isEmpty(currentPrices)) {
            return true;
        }

        // First check if the number of future prices has decreased to none
        if (futurePrices == null && currentPrices.size() > 1) {
            return true;
        }

        // Otherwise check for different number of futures
        if (futurePrices != null && futurePrices.size() + 1 != currentPrices.size()) {
            return true;
        }

        final Iterator<PriceRowModel> itCurrentPrices = currentPrices.iterator();

        // The first price in current list is the base price.
        final PriceRowModel basePrice = itCurrentPrices.next();

        if (isBasePriceUpdateRequired(basePrice, newPrice, newWasPrice, promoEvent)) {
            return true;
        }


        // Now for the future prices
        while (itCurrentPrices.hasNext()) {

            final PriceRowModel currFuturePrice = itCurrentPrices.next();

            if (isFuturePriceUpdateRequired(currFuturePrice, futurePrices)) {
                return true;
            }
        }

        return false;
    }


    private boolean isBasePriceUpdateRequired(final PriceRowModel basePrice, final double newPrice,
            final double newWasPrice, final boolean promoEvent) {

        // If the current effective price is null or different from supplied then it should be updated
        if (basePrice.getPrice() == null
                || basePrice.getPrice().compareTo(Double.valueOf(newPrice)) != 0) {
            return true;
        }

        // Compare was prices
        if (basePrice instanceof TargetPriceRowModel) {
            final TargetPriceRowModel targetPriceRow = (TargetPriceRowModel)basePrice;
            final Double effectiveWas = targetPriceRow.getWasPrice();

            // Check if we have a new was
            if (effectiveWas == null && Double.valueOf(newWasPrice).compareTo(Double.valueOf(newPrice)) != 0) {
                return true;
            }

            if (effectiveWas != null && effectiveWas.compareTo(Double.valueOf(newWasPrice)) != 0) {
                return true;
            }
            if (targetPriceRow.getPromoEvent() == null || targetPriceRow.getPromoEvent().booleanValue() != promoEvent) {
                return true;
            }
        }

        return false;
    }


    private boolean isFuturePriceUpdateRequired(final PriceRowModel currFuturePrice,
            final List<TargetPriceRowData> futurePrices) {

        // No price on the future price? we need a price row update.
        if (currFuturePrice.getPrice() == null) {
            return true;
        }

        final Date now = new Date();
        final Date nextMonth = DateUtils.addMonths(now, 1);
        final StandardDateRange currDateRange = currFuturePrice.getDateRange();

        // No date range on future price? we need a price row update.
        if (currDateRange == null || currDateRange.getStart() == null) {
            return true;
        }

        final boolean priceStarted = currDateRange.getStart().before(now);

        // Find a match in the update future prices
        TargetPriceRowData priceDataFound = null;

        for (final TargetPriceRowData priceData : futurePrices) {

            // The given price data date range should not be null - if it is then warn and continue
            final StandardDateRange dateRange = priceData.getDateRange();
            if (dateRange == null) {
                LOG.warn("Unexpected null dateRange in supplied price data");
                continue;
            }

            // Match on range if:
            // a) the start dates are equal or the ranges have already started
            // AND b) the end dates are equal or the current end is more than a month away
            if (currDateRange.getStart().equals(dateRange.getStart())
                    || (priceStarted && dateRange.getStart().before(now))) {

                if (currDateRange.getEnd().after(nextMonth)
                        || currDateRange.getEnd().equals(dateRange.getEnd())) {
                    priceDataFound = priceData;
                    break;
                }
            }
        }

        // If no match found then update
        if (priceDataFound == null) {
            return true;
        }

        // Check if price has changed for the future row
        if (Double.valueOf(priceDataFound.getSellPrice()).compareTo(currFuturePrice.getPrice()) != 0) {
            return true;
        }

        return false;
    }
}

package au.com.target.tgtcore.product.impl;

import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.product.BrandService;
import au.com.target.tgtcore.product.dao.BrandDao;
import au.com.target.tgtcore.util.TargetServicesUtil;


/**
 * Default implementation of {@link BrandService}
 * 
 */
public class TargetBrandServiceImpl extends AbstractBusinessService implements BrandService {

    /**
     * 
     */
    private static final String UNIQUE = "UNIQUE";

    private static final Logger LOG = Logger.getLogger(TargetBrandServiceImpl.class);

    private BrandDao brandDao;

    @Override
    public BrandModel getBrandForFuzzyName(final String brandName, final boolean createIfMissing) {

        ServicesUtil.validateParameterNotNullStandardMessage("name", brandName);

        final String brandCode = TargetServicesUtil.extractSafeCodeFromName(brandName, true);

        BrandModel brand = null;
        try {
            brand = brandDao.getBrandByCode(brandCode);
        }
        catch (final TargetUnknownIdentifierException e) {
            if (createIfMissing) {
                brand = createBrand(brandName);
            }
        }
        catch (final TargetAmbiguousIdentifierException e) {
            LOG.error(e.getMessage(), e);
        }
        return brand;
    }


    @Override
    public BrandModel getBrandForCode(final String brandCode) {

        ServicesUtil.validateParameterNotNullStandardMessage("code", brandCode);

        BrandModel brand = null;
        try {
            brand = brandDao.getBrandByCode(brandCode);
        }
        catch (final TargetUnknownIdentifierException e) {
            LOG.error(e.getMessage(), e);
        }
        catch (final TargetAmbiguousIdentifierException e) {
            LOG.error(e.getMessage(), e);
        }
        return brand;
    }


    protected BrandModel createBrand(final String brandName) {
        try {
            final BrandModel brand = getModelService().create(BrandModel.class);
            brand.setCode(TargetServicesUtil.extractSafeCodeFromName(brandName, true));
            brand.setName(brandName);
            getModelService().save(brand);
            return brand;
        }
        catch (final ModelSavingException mse) {
            // this is in place because of:
            // Hybris issue SUP-9353 - Duplicate items are getting created
            if (mse.getMessage().toUpperCase().contains(UNIQUE)) {
                final String brandCode = TargetServicesUtil.extractSafeCodeFromName(brandName, true);
                try {
                    return brandDao.getBrandByCode(brandCode);
                }
                catch (final Exception e) {
                    LOG.error(e.getMessage(), e);
                }
            }
        }
        return null;
    }

    /**
     * @param brandDao
     */
    @Required
    public void setBrandDao(final BrandDao brandDao) {
        this.brandDao = brandDao;
    }

}

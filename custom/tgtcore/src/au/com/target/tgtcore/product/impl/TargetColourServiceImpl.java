/**
 * 
 */
package au.com.target.tgtcore.product.impl;

import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.ColourModel;
import au.com.target.tgtcore.product.ColourService;
import au.com.target.tgtcore.product.dao.ColourDao;
import au.com.target.tgtcore.util.TargetServicesUtil;


/**
 * Default Implementation of TargetColourService, see more in {@link ColourService}
 * 
 */
public class TargetColourServiceImpl extends AbstractBusinessService implements ColourService {
    /**
     * 
     */
    private static final String UNIQUE = "UNIQUE";

    private static final Logger LOG = Logger.getLogger(TargetColourServiceImpl.class);

    private ColourDao colourDao;


    /* (non-Javadoc)
     * @see au.com.target.tgtcore.product.TargetColourService#getColourByName(java.lang.String)
     */
    @Override
    public ColourModel getColourForFuzzyName(final String colourName, final boolean createIfInexistent,
            final boolean isDisplay) {
        ServicesUtil.validateParameterNotNullStandardMessage("colourName", colourName);

        final String colourCode = TargetServicesUtil.extractSafeCodeFromName(colourName, false);

        ColourModel colourModel = null;
        try {
            colourModel = colourDao.getColourByCode(colourCode);
        }
        catch (final TargetUnknownIdentifierException e) {
            if (createIfInexistent) {
                colourModel = createColour(colourName, isDisplay);
            }
            else {
                LOG.error(e.getMessage(), e);
            }
        }
        catch (final TargetAmbiguousIdentifierException e) {
            LOG.error(e.getMessage(), e);
        }

        return colourModel;
    }

    protected ColourModel createColour(final String name, final boolean isDisplay) {
        try {
            final ColourModel colourModel = getModelService().create(ColourModel.class);
            colourModel.setCode(TargetServicesUtil.extractSafeCodeFromName(name, false));
            colourModel.setName(name);
            colourModel.setDisplay(Boolean.valueOf(isDisplay));
            getModelService().save(colourModel);
            return colourModel;
        }
        catch (final ModelSavingException mse) {
            // this is in place because of:
            // Hybris issue SUP-9353 - Duplicate items are getting created
            if (mse.getMessage().toUpperCase().contains(UNIQUE)) {
                final String colourCode = TargetServicesUtil.extractSafeCodeFromName(name, false);
                try {
                    return colourDao.getColourByCode(colourCode);
                }
                catch (final Exception e) {
                    LOG.error(e.getMessage(), e);
                }
            }
        }
        return null;
    }


    /**
     * @param colourDao
     *            the colourDao to set
     */
    @Required
    public void setColourDao(final ColourDao colourDao) {
        this.colourDao = colourDao;
    }



}

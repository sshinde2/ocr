package au.com.target.tgtcore.product.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hsqldb.lib.StringUtil;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.collect.Iterables;

import au.com.target.tgtcore.category.TargetCategoryService;
import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.TargetOriginalCategoryPopulatorService;
import au.com.target.tgtcore.product.dao.TargetProductDao;
import au.com.target.tgtcore.product.exception.ProductOriginalCategoryPopulationException;


/**
 * @author nullah
 *
 */
public class TargetOriginalCategoryPopulatorServiceImpl implements TargetOriginalCategoryPopulatorService {

    /**
     * 
     */
    private static final String TGT_ORIG_CATEGORY_UPDATE_FAILED_ERROR = "TGT-ORIG-CATEGORY-POPULATE: Update failed for product=%s, reason=%s";

    private static final String CLEARANCE_CATEGORY_CODE = "products.clearanceCategoryCode";

    private static final String ORIGINAL_CATEGORY_BATCHLIMIT = "products.originalCategory.populate.batchlimit";

    private static final String ORIGINAL_CATEGORY_SUB_BATCHLIMIT = "products.originalCategory.populate.sub.batchlimit";

    private static final Logger LOG = Logger.getLogger(TargetOriginalCategoryPopulatorServiceImpl.class);

    private TargetProductDao targetProductDao;

    private CatalogVersionService catalogVersionService;

    private TargetCategoryService targetCategoryService;

    private TargetSharedConfigService targetSharedConfigService;

    private ModelService modelService;

    /**
     * It reads all the products whose original category is NULL and approved status is approved/unapproved and in
     * Staged. It sets original category based on below mentioned rules 1)if it is not a clearance product then
     * originalCategory field is set as last category available 2)if it is a clearance product then originalCategory is
     * set as by taking category of product and comparing with all the top most categories and setting the found one
     * 
     * 
     * @throws ProductOriginalCategoryPopulationException
     */

    @Override
    public void populateOriginalCategories() throws ProductOriginalCategoryPopulationException {

        final String clearanceCategoryCode = getClearanceCategoryCode();

        final CatalogVersionModel stagedProductCatalog = catalogVersionService.getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS, TgtCoreConstants.Catalog.OFFLINE_VERSION);

        final CategoryModel clearanceCategory = targetCategoryService.getCategoryForCode(stagedProductCatalog,
                clearanceCategoryCode);

        final CategoryModel allProductsCategory = targetCategoryService.getCategoryForCode(stagedProductCatalog,
                TgtCoreConstants.Category.ALL_PRODUCTS);


        final int batchSize = targetSharedConfigService.getInt(ORIGINAL_CATEGORY_BATCHLIMIT, 10000);
        final int subBatchLimit = targetSharedConfigService.getInt(ORIGINAL_CATEGORY_SUB_BATCHLIMIT, 100);

        LOG.info(String.format("TGT-ORIG-CATEGORY-POPULATE: Begin update of original categories, batchSize=%s",
                Integer.valueOf(batchSize)));

        int noOfProductsFetched = 0;
        List<TargetProductModel> products;

        final List<String> erroredProducts = new ArrayList<>();
        do {

            final int pageSize = (batchSize - noOfProductsFetched > subBatchLimit) ? subBatchLimit
                    : (batchSize - noOfProductsFetched);

            products = targetProductDao
                    .findProductsByOriginalCategoryNull(stagedProductCatalog, pageSize, erroredProducts);

            if (CollectionUtils.isEmpty(products)) {
                LOG.info("TGT-ORIG-CATEGORY-POPULATE: No products found to update, ending update");
                return;
            }

            noOfProductsFetched += subBatchLimit;

            final List<TargetProductModel> updatedProducts = new ArrayList<>();


            for (final TargetProductModel targetProduct : products) {
                if (targetProduct.getPrimarySuperCategory() == null) {
                    LOG.info(String.format(
                            TGT_ORIG_CATEGORY_UPDATE_FAILED_ERROR,
                            targetProduct.getCode(), "primarySuperCategory is null"));
                    erroredProducts.add(targetProduct.getCode());
                    continue;
                }
                final List<TargetProductCategoryModel> categories = getCategoryList(targetProduct);

                final boolean isClearance = clearanceCategory.equals(Iterables.getFirst(categories, null));

                if (isClearance) {
                    populateOriginalCategoryForClearanceProduct(updatedProducts, erroredProducts, targetProduct,
                            categories,
                            allProductsCategory);

                }
                else {
                    populateOriginalForNormalProducts(updatedProducts, targetProduct, categories);
                }
            }

            saveProducts(updatedProducts);
        }
        while (noOfProductsFetched < batchSize && products.size() == subBatchLimit);

        LOG.info("TGT-ORIG-CATEGORY-POPULATE: Update complete!");
    }

    /**
     * Save modified products
     * 
     * @param updatedProducts
     *            list of products whose original category got updated
     * @throws ProductOriginalCategoryPopulationException
     *             some exception occured while saving
     */
    private void saveProducts(final List<TargetProductModel> updatedProducts)
            throws ProductOriginalCategoryPopulationException {
        LOG.info("TGT-ORIG-CATEGORY-POPULATE: Saving updated products");
        try {
            if (CollectionUtils.isNotEmpty(updatedProducts)) {
                modelService.saveAll(updatedProducts);
                LOG.info(String.format("TGT-ORIG-CATEGORY Successfully updated %s products",
                        Integer.valueOf(updatedProducts.size())));
            }
        }
        catch (final ModelSavingException ex) {
            LOG.info(
                    "TGT-ORIG-CATEGORY-POPULATE: Failed to save small batch of products!. so saving individual products");
            for (final TargetProductModel targetProductModel : updatedProducts) {
                saveIndividualProducts(targetProductModel);
            }
        }
    }

    /**
     * Saving individual product
     * 
     * @param targetProductModel
     *            product to be saved
     */
    private void saveIndividualProducts(final TargetProductModel targetProductModel) {
        try {
            modelService.save(targetProductModel);
        }
        catch (final ModelSavingException ex) {
            LOG.error(String.format(TGT_ORIG_CATEGORY_UPDATE_FAILED_ERROR,
                    targetProductModel.getCode(), "unable to save"), ex);
        }

    }

    /**
     * Fetch clearance category Code from shared configuration
     * 
     * @return clearance category code
     * @throws ProductOriginalCategoryPopulationException
     *             if clearance config is not found by given code
     */
    private String getClearanceCategoryCode() throws ProductOriginalCategoryPopulationException {
        final String clearanceCategoryCode = targetSharedConfigService
                .getConfigByCode(CLEARANCE_CATEGORY_CODE);
        if (StringUtil.isEmpty(clearanceCategoryCode)) {
            throw new ProductOriginalCategoryPopulationException(
                    "Clearance Category Code not set in the shared config");
        }
        return clearanceCategoryCode;
    }

    /**
     * It populate original category field for normal products
     * 
     * @param updatedProducts
     *            list of products whose original category updated
     * @param targetProduct
     *            product whose original category needs to be updated
     * @param categories
     *            product category hierarchy
     */
    private void populateOriginalForNormalProducts(final List<TargetProductModel> updatedProducts,
            final TargetProductModel targetProduct, final List<TargetProductCategoryModel> categories) {
        final TargetProductCategoryModel originalCategory = Iterables.getLast(categories);

        targetProduct.setOriginalCategory(originalCategory);
        updatedProducts.add(targetProduct);

        LOG.info(String.format(
                "TGT-ORIG-CATEGORY-POPULATE: Updated productCode=%s, originalCategory=%s (%s)",
                targetProduct.getCode(), originalCategory.getName(),
                originalCategory.getCode()));
    }

    /**
     * It populates original category field of products who belongs to clearance category
     * 
     * @param updatedProducts
     *            list of products whose original category updated
     * @param erroredProducts
     * @param targetProduct
     *            product whose original category needs to be updated
     * @param categories
     *            product category hierarchy
     * @param allProductsCategory
     *            top most category hierarchy
     */
    private void populateOriginalCategoryForClearanceProduct(final List<TargetProductModel> updatedProducts,
            final List<String> erroredProducts, final TargetProductModel targetProduct,
            final List<TargetProductCategoryModel> categories, final CategoryModel allProductsCategory) {

        CategoryModel originalCategory = null;
        Collection<CategoryModel> categoriesToMatch = allProductsCategory.getCategories();

        final List<String> categoryStrings = new ArrayList<>();
        for (int i = 1; i < categories.size(); i++) {
            final TargetProductCategoryModel targetProductCategory = categories.get(i);

            categoryStrings.add(
                    String.format("%s (%s)", targetProductCategory.getName(), targetProductCategory.getCode()));
            final CategoryModel matchedCategory = findCategoryMatch(targetProductCategory, categoriesToMatch);

            if (matchedCategory == null) {
                break;
            }

            originalCategory = matchedCategory;
            categoriesToMatch = originalCategory.getCategories();
        }

        final String productCode = targetProduct.getCode();
        if (originalCategory != null) {
            targetProduct.setOriginalCategory((TargetProductCategoryModel)originalCategory);
            updatedProducts.add(targetProduct);

            LOG.info(String.format(
                    "TGT-ORIG-CATEGORY-POPULATE: Updated productCode=%s, currentCategories=[%s], originalCategory=%s (%s)",
                    productCode, StringUtils.join(categoryStrings, ", "),
                    originalCategory.getName(), originalCategory.getCode()));
        }
        else {
            erroredProducts.add(productCode);
            LOG.info(String.format(TGT_ORIG_CATEGORY_UPDATE_FAILED_ERROR,
                    productCode, "Could not find a match for categories : " + StringUtils.join(categoryStrings, ", ")));
        }

    }

    /**
     * Fetches category hierarchy for a given product.
     * 
     * @param product
     *            whose category hierarchy has to be fetched
     * @return list of categories of a given product
     */
    private List<TargetProductCategoryModel> getCategoryList(final TargetProductModel product) {
        final List<TargetProductCategoryModel> categories = new ArrayList<>();

        final TargetProductCategoryModel primarySuperCategory = product.getPrimarySuperCategory();

        TargetProductCategoryModel supercategory = primarySuperCategory;
        categories.add(0, supercategory);
        do {
            supercategory = getSupercategory(supercategory);

            if (supercategory != null) {
                if (TgtCoreConstants.Category.ALL_PRODUCTS.equals(supercategory.getCode())) {
                    break;
                }

                categories.add(0, supercategory);
            }
        }
        while (supercategory != null);

        return categories;
    }

    /**
     * Fetch super category of given category
     * 
     * @param category
     *            whose super category needs to be fetched
     * @return super category
     */
    private TargetProductCategoryModel getSupercategory(final TargetProductCategoryModel category) {
        for (final CategoryModel supercategory : category.getSupercategories()) {
            if (supercategory instanceof TargetProductCategoryModel) {
                return (TargetProductCategoryModel)supercategory;
            }
        }

        return null;
    }

    /**
     * Gets category from top level categories which matches with clearance category name
     * 
     * @param clearanceCategory
     *            category to be matched
     * @param categories
     *            top level categories
     * @return matched category or null
     */
    private CategoryModel findCategoryMatch(final CategoryModel clearanceCategory,
            final Collection<CategoryModel> categories) {
        for (final CategoryModel category : categories) {
            if (category.getName().equals(clearanceCategory.getName())) {
                return category;
            }
        }

        return null;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @param targetCategoryService
     *            the targetCategoryService to set
     */
    @Required
    public void setTargetCategoryService(final TargetCategoryService targetCategoryService) {
        this.targetCategoryService = targetCategoryService;
    }

    /**
     * @param targetSharedConfigService
     *            the targetSharedConfigService to set
     */
    @Required
    public void setTargetSharedConfigService(final TargetSharedConfigService targetSharedConfigService) {
        this.targetSharedConfigService = targetSharedConfigService;
    }

    public TargetProductDao getTargetProductDao() {
        return this.targetProductDao;
    }

    /**
     * @param targetProductDao
     *            the targetProductDao to set
     */
    @Required
    public void setTargetProductDao(final TargetProductDao targetProductDao) {
        this.targetProductDao = targetProductDao;
    }


    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

}

/**
 * 
 */
package au.com.target.tgtcore.product.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtcore.product.TargetProductDimensionsService;
import au.com.target.tgtcore.product.dao.TargetProductDimensionsDao;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * @author pthoma20
 * 
 */
public class TargetProductDimensionsServiceImpl extends AbstractBusinessService implements
        TargetProductDimensionsService {

    private static final Logger LOG = Logger.getLogger(TargetProductDimensionsServiceImpl.class);

    private TargetProductDimensionsDao targetProductDimensionsDao;



    /* (non-Javadoc)
     * @see au.com.target.tgtcore.product.TargetProductDimensionsService#getProductDimensionsForCode(java.lang.String)
     */
    @Override
    public TargetProductDimensionsModel getProductDimensionsForCode(final CatalogVersionModel catalogVersion,
            final String productDimensionsCode) {
        ServicesUtil.validateParameterNotNullStandardMessage(TargetProductDimensionsModel.DIMENSIONCODE,
                productDimensionsCode);
        ServicesUtil.validateParameterNotNullStandardMessage(TargetProductDimensionsModel.CATALOGVERSION,
                catalogVersion);

        TargetProductDimensionsModel targetProductDimensionsModel = null;
        try {
            targetProductDimensionsModel = targetProductDimensionsDao
                    .getTargetProductDimensionByCode(catalogVersion, productDimensionsCode);
        }
        catch (final TargetUnknownIdentifierException e) {
            /*Having this statement as info due to the fact that when the product dimension comes in 
            first time for a product it has no product dimensions.*/
            LOG.info("Product Dimension Not Found for code :" + productDimensionsCode + " for catalog version : "
                    + catalogVersion.getVersion());
        }
        catch (final TargetAmbiguousIdentifierException e) {
            LOG.error(SplunkLogFormatter.formatMessage(
                    "Multiple Product Dimensions Found for code :" + productDimensionsCode + " for catalog version : "
                            + catalogVersion.getVersion(),
                    TgtutilityConstants.ErrorCode.WARN_PRODUCT_DIMENSIONS_NOT_FOUND));
        }
        return targetProductDimensionsModel;
    }

    /**
     * @param targetProductDimensionsDao
     *            the targetProductDimensionsDao to set
     */
    @Required
    public void setTargetProductDimensionsDao(final TargetProductDimensionsDao targetProductDimensionsDao) {
        this.targetProductDimensionsDao = targetProductDimensionsDao;
    }


}

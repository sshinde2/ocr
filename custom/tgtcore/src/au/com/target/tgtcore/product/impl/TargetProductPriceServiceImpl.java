/**
 * 
 */
package au.com.target.tgtcore.product.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.europe1.enums.ProductTaxGroup;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.util.StandardDateRange;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPriceRowModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.TargetProductPriceService;
import au.com.target.tgtcore.product.data.TargetPriceRowData;
import au.com.target.tgtcore.tax.TargetTaxGroupService;


public class TargetProductPriceServiceImpl implements TargetProductPriceService {

    private static final Logger LOG = LoggerFactory.getLogger(TargetProductPriceServiceImpl.class);

    private static final String LOGGER_INFO_PRICE_UPDATE = "ERROR_INFO_PRICE_UPDATE: price cant be updated for the productCode={0} because product has variants";

    private String defaultUnitCode = "pieces";

    private CatalogVersionService catalogVersionService;

    private ProductService productService;

    private ModelService modelService;

    private FlexibleSearchService flexibleSearchService;

    private UnitService unitService;

    private TargetTaxGroupService targetTaxGroupService;

    private PriceUpdateRequiredStrategy priceUpdateRequiredStrategy;

    private ProductTaxGroupUpdateRequiredStrategy productTaxGroupUpdateRequiredStrategy;

    @Override
    public List<String> removeProductFromSale(final List<String> productsNotOnSale,
            final List<CatalogVersionModel> catalogs) {

        final List<ProductModel> productsToUpdate = new ArrayList<>();
        final List<String> errorMessages = new ArrayList<>();
        // Remove product from sale for each catalog version
        for (final String productCode : productsNotOnSale) {
            for (final CatalogVersionModel catVersion : catalogs) {
                try {
                    LOG.debug("DEBUG-PRICEUPDATE-REMOVESALE : removing product {} from sale: ", productCode);
                    final ProductModel productToRemoveFromSale = productToRemoveFromSale(productCode, catVersion);
                    if (productToRemoveFromSale != null) {
                        productsToUpdate.add(productToRemoveFromSale);
                    }
                }
                catch (final Exception e) {
                    LOG.warn("Exception during removing product {} from sale: " + e.getMessage(), productCode, e);
                    errorMessages.add("Exception occured while removing product {} from sale: " + e.getMessage());
                }
            }
        }
        if (CollectionUtils.isNotEmpty(productsToUpdate)) {
            modelService.saveAll(productsToUpdate);
        }

        return errorMessages;
    }

    @Override
    public boolean updateProductPrices(final List<ProductModel> productsToUpdate) {

        boolean updated = false;

        if (CollectionUtils.isNotEmpty(productsToUpdate)) {
            modelService.saveAll(productsToUpdate);
            updated = true;
        }
        return updated;
    }

    /**
     * Get the product to remove from sale in given catalog
     * 
     * @param productCode
     * @param catVersion
     */
    private ProductModel productToRemoveFromSale(final String productCode, final CatalogVersionModel catVersion)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        final ProductModel productModel = getProductTranslateExceptions(productCode, catVersion);

        // If the product approval status is not already set to CHECK, then make it so 
        if (!productModel.getApprovalStatus().equals(ArticleApprovalStatus.CHECK)) {
            LOG.info("Product [" + productCode + "] has been removed from sale, ArticleApprovalStatus changed from ["
                    + productModel.getApprovalStatus().getCode() + "] to [" + ArticleApprovalStatus.CHECK.getCode()
                    + "]");

            productModel.setApprovalStatus(ArticleApprovalStatus.CHECK);

            return productModel;
        }

        return null;

    }

    /**
     * return a list of products for the given catalogs with the given prices, but only if they have changed.
     * 
     * @param productCode
     * @param catalogs
     * @param currentPrice
     * @param wasPrice
     * @param futurePrices
     * @return true if update was required
     */
    @Override
    public List<ProductModel> getProductModelForCatalogs(final String productCode,
            final List<CatalogVersionModel> catalogs,
            final double currentPrice,
            final double wasPrice,
            final List<TargetPriceRowData> futurePrices,
            final int gstRate,
            final boolean promoEvent)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        final List<ProductModel> productsToUpdate = new ArrayList<>();

        for (final CatalogVersionModel catVersion : catalogs) {

            final ProductModel productModel = getProductTranslateExceptions(productCode, catVersion);

            if (CollectionUtils.isEmpty(productModel.getVariants()) && !isBaseProduct(productModel)) {
                boolean bSaveRequired = false;
                final Collection<PriceRowModel> currentPrices = productModel.getEurope1Prices();
                if (priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, currentPrice, wasPrice,
                        promoEvent,
                        futurePrices)) {

                    final Collection<PriceRowModel> prices = createNewPricesForProduct(productModel, catVersion,
                            currentPrice, wasPrice, promoEvent, futurePrices);

                    productModel.setEurope1Prices(prices);
                    bSaveRequired = true;
                }

                if (productTaxGroupUpdateRequiredStrategy.isTaxGroupUpdateRequired(productModel, gstRate)) {

                    updateProductTaxGroup(productModel, gstRate, catVersion);
                    bSaveRequired = true;
                }

                if (bSaveRequired) {
                    LOG.debug("DEBUG-PRICEUPDATE-TOUPDATE - Adding product {} to the collection to add");
                    productsToUpdate.add(productModel);
                }
            }
            else {
                LOG.info(MessageFormat.format(LOGGER_INFO_PRICE_UPDATE, productModel.getCode()));
            }
        }

        return productsToUpdate;

    }

    /**
     * @param productModel
     * @return boolean
     */
    private boolean isBaseProduct(final ProductModel productModel) {

        if (productModel instanceof TargetProductModel) {
            return true;
        }
        return false;
    }

    /**
     * Create a collection of new price rows for the given product and supplied price data.<br/>
     * This method is protected so it can be unit tested.
     * 
     * @param productModel
     * @param catVersion
     * @param currentPrice
     * @param wasPrice
     * @param futurePrices
     * @return collection of PriceRowModel
     */
    protected Collection<PriceRowModel> createNewPricesForProduct(final ProductModel productModel,
            final CatalogVersionModel catVersion,
            final double currentPrice,
            final double wasPrice,
            final boolean promoEvent,
            final List<TargetPriceRowData> futurePrices) {


        final Collection<PriceRowModel> prices = new ArrayList<>();

        // Create the first one with no date range
        final TargetPriceRowModel currentPriceModel = createPriceRowModel(catVersion, productModel,
                currentPrice,
                wasPrice,
                null,
                promoEvent);
        prices.add(currentPriceModel);

        if (CollectionUtils.isNotEmpty(futurePrices)) {

            for (final TargetPriceRowData futurePriceData : futurePrices) {
                final TargetPriceRowModel futurePriceModel = createPriceRowModel(catVersion, productModel,
                        futurePriceData.getSellPrice(), futurePriceData.getWasPrice(),
                        futurePriceData.getDateRange(), futurePriceData.isPromoEvent());
                prices.add(futurePriceModel);
            }
        }

        return prices;
    }

    /**
     * Create a TargetPriceRowModel for the given data
     * 
     * @param catVersion
     * @param productModel
     * @param currentPrice
     * @param wasPrice
     * @param dateRange
     * @return TargetPriceRowModel
     */
    protected TargetPriceRowModel createPriceRowModel(final CatalogVersionModel catVersion,
            final ProductModel productModel,
            final double currentPrice,
            final double wasPrice,
            final StandardDateRange dateRange,
            final boolean promoEvent) {

        final TargetPriceRowModel priceModel = modelService.create(TargetPriceRowModel.class);

        if (Double.compare(wasPrice, currentPrice) != 0) {
            priceModel.setWasPrice(Double.valueOf(wasPrice));
        }

        priceModel.setNet(Boolean.FALSE);
        priceModel.setGiveAwayPrice(Boolean.FALSE);
        priceModel.setPrice(Double.valueOf(currentPrice));
        priceModel.setCurrency(getCurrency());
        priceModel.setUnit(getDefaultUnitModel());
        priceModel.setCatalogVersion(catVersion);
        priceModel.setProduct(productModel);
        priceModel.setPromoEvent(Boolean.valueOf(promoEvent));


        if (dateRange != null) {
            priceModel.setDateRange(dateRange);
        }

        modelService.save(priceModel);
        modelService.refresh(priceModel);

        return priceModel;
    }


    /**
     * Associate the product with a product tax group for the given rate.<br/>
     * Creates all the tax reference data if required for the given rate.
     * 
     * @param productModel
     * @param rate
     * @param catVersion
     */
    protected void updateProductTaxGroup(final ProductModel productModel,
            final int rate,
            final CatalogVersionModel catVersion) {

        final ProductTaxGroup ptg = targetTaxGroupService.getProductTaxGroupWithCreate(rate, catVersion);
        productModel.setEurope1PriceFactory_PTG(ptg);
    }


    protected ProductModel getProductTranslateExceptions(final String productCode, final CatalogVersionModel catVersion)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        ProductModel productModel = null;
        try {
            productModel = productService.getProductForCode(catVersion, productCode);
        }
        catch (final UnknownIdentifierException e) {
            throw new TargetUnknownIdentifierException(MessageFormat.format(
                    "UnknownIdentifierException for productCode:{0} and catalogVersion {1}", productCode,
                    catVersion.getVersion()), e);
        }
        catch (final AmbiguousIdentifierException e) {
            throw new TargetAmbiguousIdentifierException(MessageFormat.format(
                    "AmbiguousIdentifierException for productCode:{0} and catalogVersion {1}", productCode,
                    catVersion.getVersion()), e);
        }

        if (productModel == null) {
            throw new TargetUnknownIdentifierException(MessageFormat.format(
                    "Null product returned for productCode:{0} and catalogVersion {1}", productCode,
                    catVersion.getVersion()));
        }

        return productModel;
    }


    /**
     * Return a list of both online and offline product catalogs
     * 
     * @return List<CatalogVersionModel>
     */
    @Override
    public List<CatalogVersionModel> getCatalogs() {

        final List<CatalogVersionModel> listCatalogs = new ArrayList<>();

        listCatalogs.add(catalogVersionService.getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION));

        listCatalogs.add(catalogVersionService.getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.ONLINE_VERSION));

        return listCatalogs;
    }


    @Override
    public TargetPriceRowModel getCurrentTargetPriceRowModelFromStaged(final String productCode) {
        ProductModel productModel = null;
        try {
            productModel = productService.getProductForCode(catalogVersionService.getCatalogVersion(
                    TgtCoreConstants.Catalog.PRODUCTS,
                    TgtCoreConstants.Catalog.OFFLINE_VERSION), productCode);
        }
        catch (final UnknownIdentifierException e) {
            LOG.warn("UnknownIdentifierException for productCode:" + productCode);
            return null;
        }
        catch (final AmbiguousIdentifierException e) {
            LOG.warn("AmbiguousIdentifierException for productCode:" + productCode);
            return null;
        }
        if (productModel != null) {
            final Collection<PriceRowModel> currentPrices = productModel.getEurope1Prices();
            if (CollectionUtils.isNotEmpty(currentPrices)) {
                final Iterator<PriceRowModel> itCurrentPrices = currentPrices.iterator();
                final PriceRowModel basePrice = itCurrentPrices.next();
                if (basePrice instanceof TargetPriceRowModel) {
                    return (TargetPriceRowModel)basePrice;
                }
            }
        }
        LOG.warn("TargetPriceRowModel does not exist for productCode:" + productCode);
        return null;
    }

    private CurrencyModel getCurrency() {
        return getAustralianDollar();
    }

    private CurrencyModel getAustralianDollar() {
        final CurrencyModel example = new CurrencyModel();
        example.setIsocode("AUD");
        final List<CurrencyModel> list = flexibleSearchService.getModelsByExample(example);
        if (CollectionUtils.isNotEmpty(list)) {
            return list.get(0);
        }
        return null;
    }


    private UnitModel getDefaultUnitModel() {
        final UnitModel unit = unitService.getUnitForCode(defaultUnitCode);
        return unit;
    }



    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @param productService
     *            the productService to set
     */
    public void setProductService(final ProductService productService) {
        this.productService = productService;
    }

    /**
     * @param unitService
     *            the unitService to set
     */
    public void setUnitService(final UnitService unitService) {
        this.unitService = unitService;
    }

    /**
     * @param targetTaxGroupService
     *            the targetTaxGroupService to set
     */
    public void setTargetTaxGroupService(final TargetTaxGroupService targetTaxGroupService) {
        this.targetTaxGroupService = targetTaxGroupService;
    }

    /**
     * @param taxGroupUpdateRequiredStrategy
     *            the taxGroupUpdateRequiredStrategy to set
     */
    public void setProductTaxGroupUpdateRequiredStrategy(
            final ProductTaxGroupUpdateRequiredStrategy taxGroupUpdateRequiredStrategy) {
        this.productTaxGroupUpdateRequiredStrategy = taxGroupUpdateRequiredStrategy;
    }

    /**
     * @param priceUpdateRequiredStrategy
     *            the priceUpdateRequiredStrategy to set
     */
    public void setPriceUpdateRequiredStrategy(final PriceUpdateRequiredStrategy priceUpdateRequiredStrategy) {
        this.priceUpdateRequiredStrategy = priceUpdateRequiredStrategy;
    }

    /**
     * @param defaultUnitCode
     *            the defaultUnitCode to set
     */
    public void setDefaultUnitCode(final String defaultUnitCode) {
        this.defaultUnitCode = defaultUnitCode;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param flexibleSearchService
     *            the flexibleSearchService to set
     */
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }


}

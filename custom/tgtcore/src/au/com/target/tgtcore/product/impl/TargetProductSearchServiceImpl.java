package au.com.target.tgtcore.product.impl;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.fluent.FluentStockService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.price.TargetCommercePriceService;
import au.com.target.tgtcore.product.TargetProductSearchService;
import au.com.target.tgtcore.product.dao.TargetProductSearchServiceDao;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtutility.util.TargetProductUtils;


public class TargetProductSearchServiceImpl extends AbstractBusinessService implements TargetProductSearchService {

    private TargetProductSearchServiceDao targetProductSearchServiceDao;

    private CatalogVersionService catalogVersionService;

    private TargetCommercePriceService targetCommercePriceService;

    private TargetStockService targetStockService;

    private FluentStockService fluentStockService;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Override
    public List<TargetSizeVariantProductModel> getTargetSizeVariantProductModelByBaseProduct(final String baseProduct) {
        return getTargetProductSearchServiceDao().getTargetSizeVariantProductModelByBaseProduct(baseProduct);
    }

    @Override
    public List<TargetSizeVariantProductModel> getTargetSizeVariantProductModelByCode(final String code) {
        return getTargetProductSearchServiceDao().getTargetSizeVariantProductModelByCode(code);

    }

    @Override
    public TargetColourVariantProductModel getColourVariantByBaseProductAndSwatch(
            final String baseProductCode, final String swatchCode) {
        final CatalogVersionModel productStagedCatalog = getCatalogVersionService()
                .getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS, TgtCoreConstants.Catalog.OFFLINE_VERSION);
        return getTargetProductSearchServiceDao()
                .getByBaseProductAndSwatch(baseProductCode, swatchCode, productStagedCatalog);
    }

    @Override
    public List<TargetProductModel> getAllTargetProductsWithNoDepartment(final String catalogVersionString,
            final int start,
            final int count) {
        final CatalogVersionModel catalogVersion = getCatalogVersionService()
                .getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS, catalogVersionString);
        return getTargetProductSearchServiceDao().getAllTargetProductsWithNoDepartment(catalogVersion, start, count);
    }

    @Override
    public boolean isProductAvailableToSell(final ProductModel productModel) {
        Assert.notNull(productModel, "Product can't be null.");

        final List<AbstractTargetVariantProductModel> sellableVariants = TargetProductUtils
                .getAllSellableVariants(productModel);
        for (final AbstractTargetVariantProductModel sellableVariant : sellableVariants) {
            if (doesProductHaveCorrectStatusPriceAndStock(sellableVariant)) {
                return true;
            }
        }

        return false;
    }

    protected boolean doesProductHaveCorrectStatusPriceAndStock(final ProductModel productModel) {
        boolean displayOnly = false;
        if (productModel instanceof AbstractTargetVariantProductModel) {
            displayOnly = BooleanUtils.isTrue(((AbstractTargetVariantProductModel)productModel).getDisplayOnly());
        }
        //check status, price info and stock of current sellable variant
        if (ArticleApprovalStatus.APPROVED.equals(productModel.getApprovalStatus())
                && null != getTargetCommercePriceService().getWebPriceForProduct(productModel)) {
            if (displayOnly) {
                return true;
            }
            final StockLevelStatus stockLevelStatus;
            if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)) {
                stockLevelStatus = fluentStockService.getProductOnlineStockStatus(productModel.getCode());
            }
            else {
                stockLevelStatus = getTargetStockService()
                        .getProductStatus(productModel);
            }
            if (stockLevelStatus != null && !StockLevelStatus.OUTOFSTOCK.equals(stockLevelStatus)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return the targetProductSearchServiceDao
     */
    protected TargetProductSearchServiceDao getTargetProductSearchServiceDao() {
        return targetProductSearchServiceDao;
    }

    /**
     * @param targetProductSearchServiceDao
     *            the targetProductSearchServiceDao to set
     */
    @Required
    public void setTargetProductSearchServiceDao(final TargetProductSearchServiceDao targetProductSearchServiceDao) {
        this.targetProductSearchServiceDao = targetProductSearchServiceDao;
    }

    /**
     * Returns the catalog version service
     * 
     * @return the catalog version service
     */
    protected CatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    /**
     * Sets the catalog version service.
     * 
     * @param catalogVersionService
     *            the catalog version service to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @return the targetCommercePriceService
     */
    protected TargetCommercePriceService getTargetCommercePriceService() {
        return targetCommercePriceService;
    }

    /**
     * @param targetCommercePriceService
     *            the targetCommercePriceService to set
     */
    @Required
    public void setTargetCommercePriceService(final TargetCommercePriceService targetCommercePriceService) {
        this.targetCommercePriceService = targetCommercePriceService;
    }

    /**
     * @return the targetStockService
     */
    protected TargetStockService getTargetStockService() {
        return targetStockService;
    }

    /**
     * @param targetStockService
     *            the targetStockService to set
     */
    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }

    /**
     * @param fluentStockService
     *            the fluentStockService to set
     */
    @Required
    public void setFluentStockService(final FluentStockService fluentStockService) {
        this.fluentStockService = fluentStockService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

}

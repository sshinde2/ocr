/**
 * 
 */
package au.com.target.tgtcore.product.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateIfSingleResult;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static java.lang.String.format;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.constants.CatalogConstants;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.product.impl.DefaultProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.product.TargetProductSearchService;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtcore.product.dao.TargetProductDao;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.util.TargetServicesUtil;
import au.com.target.tgtutility.util.TargetDateUtil;


/**
 * @author rmcalave
 * 
 */
public class TargetProductServiceImpl extends DefaultProductService implements TargetProductService {

    private static final Logger LOG = Logger.getLogger(TargetProductServiceImpl.class);

    private TargetProductDao targetProductDao;

    private CatalogVersionService catalogVersionService;

    private TargetProductSearchService targetProductSearchService;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private TargetStockService targetStockService;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.product.impl.TargetProductService#getProductsForPrimarySuperCategory(de.hybris.platform.category.model.CategoryModel)
     */
    @Override
    public List<ProductModel> getProductsForPrimarySuperCategory(final CategoryModel category) {
        validateParameterNotNull(category, "Parameter category was null");
        return getTargetProductDao().findOnlineProductsByPrimarySuperCategory(category);
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtcore.product.TargetProductService#getBulkyBoardProducts()
     */
    @Override
    public List<AbstractTargetVariantProductModel> getBulkyBoardProducts() {
        final CatalogVersionModel stagedCatalog = getCatalogVersionService().getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS, TgtCoreConstants.Catalog.OFFLINE_VERSION);

        final List<TargetProductModel> bulkyBoardProducts = getTargetProductDao().findBulkyBoardProducts(stagedCatalog);

        final List<AbstractTargetVariantProductModel> bulkyBoardVariantsForUpdate = new ArrayList<>();
        if (CollectionUtils.isEmpty(bulkyBoardProducts)) {
            return bulkyBoardVariantsForUpdate;
        }

        // Add all variants of the base products to the list.
        // By agreement, bulky board products will only have a single colour variant and no size variants.
        // So, we only need to go down one level from the base product.
        for (final TargetProductModel bulkyBoardProduct : bulkyBoardProducts) {
            if (CollectionUtils.isEmpty(bulkyBoardProduct.getVariants())) {
                continue;
            }

            if (!isUpdateRequired(bulkyBoardProduct)) {
                continue;
            }

            for (final VariantProductModel productVariant : bulkyBoardProduct.getVariants()) {
                if (productVariant instanceof AbstractTargetVariantProductModel) {
                    if (!isUpdateRequired(productVariant)) {
                        continue;
                    }

                    final AbstractTargetVariantProductModel abstractTargetVariantProduct = (AbstractTargetVariantProductModel)productVariant;
                    bulkyBoardVariantsForUpdate.add(abstractTargetVariantProduct);
                }
            }
        }

        return bulkyBoardVariantsForUpdate;
    }


    @Override
    public List<TargetColourVariantProductModel> getProductsForMerchDeptCode(
            final TargetMerchDepartmentModel merchDepartment,
            final int start, final int count) {
        List<TargetColourVariantProductModel> targetProductModelList = new ArrayList<>();
        if (null != merchDepartment) {
            targetProductModelList = getTargetProductDao().findProductByMerchDepartment(merchDepartment, start,
                    count);
        }
        return targetProductModelList;
    }

    private boolean isUpdateRequired(final ProductModel product) {
        // Don't update not approved products or variants.
        if (!ArticleApprovalStatus.APPROVED.equals(product.getApprovalStatus())) {
            return false;
        }

        // Don't update products or variants that are not online yet
        // (by date).
        if (product.getOnlineDate() != null) {
            final Date onlineDate = product.getOnlineDate();

            final DateTime onlineDateTime = new DateTime(onlineDate);
            if (onlineDateTime.isAfter(DateTime.now())) {
                return false;
            }
        }

        // Don't update products or variants that are no longer available online
        // (by date).
        if (product.getOfflineDate() != null) {
            final Date offlineDate = product.getOfflineDate();

            final DateTime offlineDateTime = new DateTime(offlineDate);
            if (offlineDateTime.isBefore(DateTime.now())) {
                return false;
            }
        }

        return true;
    }

    @Override
    public void setOnlineDateIfApplicable(final ProductModel product) {
        if (product instanceof TargetColourVariantProductModel) {
            setOnlineDateToColourVariantIfApplicable((TargetColourVariantProductModel)product);
        }
        else if (product instanceof TargetSizeVariantProductModel) {
            final ProductModel baseProduct = ((TargetSizeVariantProductModel)product).getBaseProduct();
            if (baseProduct instanceof TargetColourVariantProductModel) {
                setOnlineDateToColourVariantIfApplicable((TargetColourVariantProductModel)baseProduct);
            }
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.product.TargetProductService#getProductByEan(java.lang.String)
     */
    @Override
    public ProductModel getProductByEan(final String ean)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        Assert.notNull(ean, "Product Ean can't be null");
        final List<ProductModel> products = targetProductDao.findProductByEan(ean);
        TargetServicesUtil.validateIfSingleResult(products, ProductModel.class, ProductModel.EAN, ean);
        return products.get(0);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.product.TargetProductFacade#isDropShipProduct()
     */
    @Override
    public boolean isDropShipProduct(final ProductModel product) {
        final Collection<StockLevelModel> stockLevels = targetStockService.getAllStockLevels(product);

        for (final StockLevelModel stockLevel : stockLevels) {
            if (stockLevel.getWarehouse().isDropShipWarehouse()) {
                return true;
            }
        }

        return false;
    }

    @Override
    public ProductModel getProductByCatalogVersion(final CatalogVersionModel catalogVersion, final String code) {
        return (ProductModel)getSessionService().executeInLocalView(new SessionExecutionBody() {

            @Override
            public ProductModel execute() {

                final JaloSession session = JaloSession.getCurrentSession();
                final Collection cvs = (Collection)session
                        .getAttribute(CatalogConstants.SESSION_CATALOG_VERSIONS);

                if (cvs == null || cvs.isEmpty()
                        || (cvs.size() == 1 && cvs.contains(CatalogConstants.NO_VERSIONS_AVAILABLE_DUMMY))) {
                    final List<CatalogVersionModel> catalogVersions = new ArrayList<CatalogVersionModel>();
                    catalogVersions.add(catalogVersion);
                    session.setAttribute(CatalogConstants.SESSION_CATALOG_VERSIONS, catalogVersions);
                }
                final List<ProductModel> products = targetProductDao.findProductsByCode(catalogVersion, code);

                validateIfSingleResult(
                        products,
                        format("Product with code '%s' and CatalogVersion '%s.%s' not found!", code, catalogVersion
                                .getCatalog().getId(),
                                catalogVersion.getVersion()),
                        format("Code '%s' and CatalogVersion '%s.%s' are not unique. %d products found!", code,
                                catalogVersion
                                        .getCatalog().getId(),
                                catalogVersion.getVersion(),
                                Integer.valueOf(products.size())));

                return products.get(0);
            }
        });
    }

    @Override
    public void setOnlineDateForApplicableProducts() {
        final CatalogVersionModel stagedCatalog = getCatalogVersionService().getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS, TgtCoreConstants.Catalog.OFFLINE_VERSION);
        final CatalogVersionModel onlineCatalog = getCatalogVersionService().getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS, TgtCoreConstants.Catalog.ONLINE_VERSION);

        final List<String> productCodes = targetProductDao.findProductsEligibleForNewOnlineDate();
        LOG.info("setting onlineDate for products=" + productCodes);
        ProductModel productModel;
        for (final String code : productCodes) {
            productModel = getProductForCode(stagedCatalog, code);
            setOnlineDateToProduct(productModel);
            try {
                productModel = getProductForCode(onlineCatalog, code);
                setOnlineDateToProduct(productModel);
            }
            catch (final UnknownIdentifierException e) {
                // the product hasn't been synced yet, ignore
            }
        }
    }

    /**
     * @param productModel
     */
    protected void setOnlineDateToProduct(final ProductModel productModel) {
        if (productModel instanceof TargetColourVariantProductModel) {
            setOnlineDateToColourVariant((TargetColourVariantProductModel)productModel);
        }
        else {
            LOG.warn("product with code=" + productModel.getCode() + " is not a colourVariant, but of type="
                    + productModel.getClass());
        }
    }

    /**
     * 
     * @param colourVariant
     */
    protected void setOnlineDateToColourVariantIfApplicable(final TargetColourVariantProductModel colourVariant) {
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.NEWIN_ONLINEFROM)
                && null == colourVariant.getOnlineDate()
                && targetProductSearchService.isProductAvailableToSell(colourVariant)) {
            setOnlineDateToColourVariant(colourVariant);
        }
    }

    /**
     * @param colourVariant
     */
    protected void setOnlineDateToColourVariant(final TargetColourVariantProductModel colourVariant) {
        final Date dateToSet = TargetDateUtil.getDateWithMidnightTime(new Date());
        final Collection<VariantProductModel> subVariants = colourVariant.getVariants();

        if (CollectionUtils.isNotEmpty(subVariants)) {
            for (final VariantProductModel variant : subVariants) {
                variant.setOnlineDate(dateToSet);
                getModelService().save(variant);
            }
        }
        colourVariant.setOnlineDate(dateToSet);
        getModelService().save(colourVariant);
    }

    /**
     * @return the targetProductDao
     */
    protected TargetProductDao getTargetProductDao() {
        return targetProductDao;
    }

    /**
     * @param targetProductDao
     *            the productDao to set
     */
    @Required
    public void setTargetProductDao(final TargetProductDao targetProductDao) {
        this.targetProductDao = targetProductDao;
    }

    /**
     * @return the catalogVersionService
     */
    protected CatalogVersionService getCatalogVersionService() {
        return catalogVersionService;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @param targetProductSearchService
     *            the targetProductSearchService to set
     */
    @Required
    public void setTargetProductSearchService(final TargetProductSearchService targetProductSearchService) {
        this.targetProductSearchService = targetProductSearchService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * @param targetStockService
     *            the targetStockService to set
     */
    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }
}

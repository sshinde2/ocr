package au.com.target.tgtcore.product.impl;

import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.product.ProductTypeService;
import au.com.target.tgtcore.product.dao.ProductTypeDao;

/**
 * Default implementation for {@link ProductTypeService}.
 */
public class TargetProductTypeServiceImpl implements ProductTypeService {

    private ProductTypeDao productTypeDao;

    @Override
    public ProductTypeModel getByCode(final String code) {
        return getProductTypeDao().getByCode(code);
    }

    /**
     * Returns the product type DAO.
     *
     * @return the product type DAO
     */
    public ProductTypeDao getProductTypeDao() {
        return productTypeDao;
    }

    /**
     * Sets the product type DAO.
     *
     * @param productTypeDao the product type DAO to set
     */
    public void setProductTypeDao(final ProductTypeDao productTypeDao) {
        this.productTypeDao = productTypeDao;
    }
}

/**
 * 
 */
package au.com.target.tgtcore.refund;


/**
 * Runtime exception related to order refund.
 */
public class TargetOrderRefundException extends RuntimeException {

    /**
     * @param message
     * @param cause
     */
    public TargetOrderRefundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     */
    public TargetOrderRefundException(final String message) {
        super(message);
    }
}

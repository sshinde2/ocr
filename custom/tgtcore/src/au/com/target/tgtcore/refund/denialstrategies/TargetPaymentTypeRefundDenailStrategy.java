/**
 * 
 */
package au.com.target.tgtcore.refund.denialstrategies;

import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.strategy.ReturnableCheck;

import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;


/**
 * @author rmcalave
 * 
 */
public class TargetPaymentTypeRefundDenailStrategy implements ReturnableCheck {

    /**
     * Determines if refund is allowed based on the payment type of the order.
     * 
     * Refund is not allowed if the payment info is from a pin pad, and the card type used for payment is a BasicsCard.
     * 
     * @see de.hybris.platform.returns.strategy.ReturnableCheck#perform(de.hybris.platform.core.model.order.OrderModel,
     *      de.hybris.platform.core.model.order.AbstractOrderEntryModel, long)
     */
    @Override
    public boolean perform(final OrderModel order, final AbstractOrderEntryModel entry, final long returnQuantity) {
        if (order.getPaymentInfo() instanceof PinPadPaymentInfoModel) {
            final PinPadPaymentInfoModel pinPadPaymentInfo = (PinPadPaymentInfoModel)order.getPaymentInfo();

            if (CreditCardType.BASICSCARD.equals(pinPadPaymentInfo.getCardType())) {
                return false;
            }
        }

        return true;
    }

}

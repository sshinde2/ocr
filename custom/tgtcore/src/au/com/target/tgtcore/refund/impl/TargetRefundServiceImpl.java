/**
 * 
 */
package au.com.target.tgtcore.refund.impl;

import de.hybris.platform.basecommerce.enums.OrderEntryStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.orderhistory.OrderHistoryService;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.refund.impl.DefaultRefundService;
import de.hybris.platform.returns.OrderReturnRecordsHandlerException;
import de.hybris.platform.returns.model.OrderReturnRecordEntryModel;
import de.hybris.platform.returns.model.OrderReturnRecordModel;
import de.hybris.platform.returns.model.RefundEntryModel;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.returns.strategy.ReturnableCheck;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.flybuys.service.FlybuysDiscountService;
import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtcore.order.TargetFindDeliveryCostStrategy;
import au.com.target.tgtcore.ordercancel.TargetCancelService;
import au.com.target.tgtcore.ordercancel.TargetRefundPaymentService;
import au.com.target.tgtcore.ordercancel.data.CancelRequestResponse;
import au.com.target.tgtcore.ordercancel.data.IpgNewRefundInfoDTO;
import au.com.target.tgtcore.ordercancel.data.PaymentTransactionEntryDTO;
import au.com.target.tgtcore.ordercancel.data.RefundInfoDTO;
import au.com.target.tgtcore.ordercancel.discountstrategy.DiscountValueCorrectionStrategy;
import au.com.target.tgtcore.ordercancel.discountstrategy.VoucherCorrectionStrategy;
import au.com.target.tgtcore.orderreturn.TargetOrderReturnRecordHandler;
import au.com.target.tgtcore.refund.TargetOrderRefundException;
import au.com.target.tgtcore.refund.TargetRefundService;
import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;
import au.com.target.tgtpayment.util.PriceCalculator;


public class TargetRefundServiceImpl extends DefaultRefundService implements TargetRefundService {

    private static final Logger LOG = Logger.getLogger(TargetRefundServiceImpl.class);
    private static final String REFUND_TICKET_MESSAGE = " Order refund is partial failed due to failure to refund | Refund information: {0}";
    private static final String REFUND_TICKET_HEAD = "Order refund failed";
    private static final String REFUND_REJECT_MESSAGE = "refund rejected: refund amount:{0}, and failure message:{1} ";

    protected TargetBusinessProcessService businessProcessService;

    protected FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    protected TransactionTemplate transactionTemplate;

    private TargetRefundPaymentService targetRefundPaymentService;

    private TargetCancelService targetCancelService;

    private DiscountValueCorrectionStrategy discountValueCorrectionStrategy;

    private VoucherCorrectionStrategy voucherCorrectionStrategy;

    private FlybuysDiscountService flybuysDiscountService;

    private OrderHistoryService orderHistoryService;

    private PromotionsService promotionsService;

    private TargetFindDeliveryCostStrategy targetFindDeliveryCostStrategy;

    private List<ReturnableCheck> refundableChecks;


    private TargetTicketBusinessService targetTicketBusinessService;

    /**
     * {@inheritDoc}
     * 
     * Copies modification records to returned instance. This is a prerequisite for correct delivery cost recalculation.
     * 
     * @param original
     *            the original order to create preview mode for
     * @return the recalculated copy of {@code original}
     */
    @Override
    public OrderModel createRefundOrderPreview(final OrderModel original) {
        final OrderModel preview = orderHistoryService.createHistorySnapshot(original);
        preview.setReturnRequests(null);
        preview.setAllPromotionResults(Collections.<PromotionResultModel> emptySet());
        getModelService().save(preview);
        promotionsService.transferPromotionsToOrder(original, preview, false);

        final Set<OrderModificationRecordModel> originalRecords = original.getModificationRecords();
        final Set<OrderModificationRecordModel> previewRecords = new HashSet<>();

        int recordIndex = 1;
        for (final OrderModificationRecordModel record : originalRecords) {
            final OrderModificationRecordModel clonedRecord = getModelService().clone(record);
            clonedRecord.setIdentifier(record.getIdentifier() + "_" + preview.getVersionID() + "_" + recordIndex);
            recordIndex++;

            final Collection<OrderModificationRecordEntryModel> originalRecordEntries = record
                    .getModificationRecordEntries();
            final List<OrderModificationRecordEntryModel> previewRecordEntries = new ArrayList<>();

            int recordEntryIndex = 1;
            for (final OrderModificationRecordEntryModel recordEntry : originalRecordEntries) {
                final OrderModificationRecordEntryModel clonedRecordEntry = getModelService().clone(recordEntry);
                clonedRecordEntry
                        .setCode(recordEntry.getCode() + "_" + preview.getVersionID() + "_" + recordEntryIndex);
                recordEntryIndex++;

                previewRecordEntries.add(clonedRecordEntry);
            }

            clonedRecord.setModificationRecordEntries(previewRecordEntries);
            previewRecords.add(clonedRecord);
        }

        preview.setModificationRecords(previewRecords);
        return preview;
    }

    @Override
    public void apply(final ReturnRequestModel request, final OrderModel order) {
        apply(request, order, null, null);
    }

    @Override
    public void apply(final ReturnRequestModel request, final OrderModel order,
            final CreditCardPaymentInfoModel newPayment, final IpgNewRefundInfoDTO ipgNewRefundInfoDTO) {

        Assert.notNull(request, "request can't be null");
        Assert.notNull(order, "Order can't be null");

        final List<RefundEntryModel> refunds = new ArrayList<>();
        for (final ReturnEntryModel returnEntry : request.getReturnEntries()) {
            refunds.add((RefundEntryModel)returnEntry);
        }

        if (isPreviewOrder(order)) {
            final double refundDeliveryCost = request.getShippingAmountToRefund().doubleValue();
            applyForPreview(refunds, order, refundDeliveryCost);
            order.setTotalPrice(Double.valueOf(order.getTotalPrice().doubleValue() - refundDeliveryCost));
            return;
        }

        final CancelRequestResponse response = new CancelRequestResponse();

        final TargetOrderRefundException refundException = transactionTemplate.execute(
                new TransactionCallback<TargetOrderRefundException>() {

                    @Override
                    public TargetOrderRefundException doInTransaction(final TransactionStatus status) {

                        try {
                            processInternalRefundRequest(request, response, order, newPayment, refunds,
                                    ipgNewRefundInfoDTO);

                            // If the refund payment failed then throw an exception to force the rollback
                            final String refundFailureMessage = response.getRefundFailureMessage();
                            if (refundFailureMessage != null) {

                                throw new TargetOrderRefundException(MessageFormat.format(
                                        REFUND_REJECT_MESSAGE,
                                        response.getRefundAmount(), refundFailureMessage));
                            }

                        }
                        catch (final TargetOrderRefundException e) {
                            status.setRollbackOnly();
                            return e;
                        }
                        catch (final Exception e) {
                            status.setRollbackOnly();
                            return new TargetOrderRefundException(MessageFormat.format(
                                    REFUND_REJECT_MESSAGE,
                                    response.getRefundAmount(), e.getMessage()));
                        }

                        return null;

                    }
                });


        // Handle failure
        if (refundException != null) {

            // If there was a refund payment attempt then recreate the payment models, now the transaction has rolled back
            if (CollectionUtils.isNotEmpty(response.getFollowOnRefundPaymentDataList())) {
                targetRefundPaymentService.createFollowOnRefundPaymentTransactionEntries(order,
                        response.getFollowOnRefundPaymentDataList());
            }
            if (response.getStandaloneRefundPaymentData() != null) {
                targetRefundPaymentService.createStandaloneRefundPaymentTransaction(order, newPayment,
                        response.getStandaloneRefundPaymentData());
            }

            throw refundException;
        }

        final RefundInfoDTO refundedInfo = targetRefundPaymentService.createRefundedInfo(
                response.getFollowOnRefundPaymentDataList(), response.getRefundAmount());

        // The business process will be responsible for exporting TLOG to POS, submitting the refund transaction to Accertify etc.
        businessProcessService.startOrderRefundProcess(order,
                (OrderReturnRecordEntryModel)response.getOrderModificationRecordEntryModel(),
                response.getRefundAmount(), response.getFollowOnRefundPaymentList(),
                refundedInfo.getRefundAmountRemaining());

        //Raise ticket if partial refund success(multiple payment refund entry).
        if (StringUtils.isNotEmpty(refundedInfo.getMessage())) {
            final String refundMessage = MessageFormat.format(REFUND_TICKET_MESSAGE, refundedInfo.getMessage());
            targetTicketBusinessService.createCsAgentTicket(REFUND_TICKET_HEAD, REFUND_TICKET_HEAD, refundMessage,
                    order);
            final String inforMessage = "Order refund is partially successful for order:" + order.getCode()
                    + refundMessage
                    + " CS ticket has been raised";
            LOG.info(inforMessage);
            throw new TargetOrderRefundException(inforMessage);
        }

    }

    public void applyForPreview(final List<RefundEntryModel> refunds, final OrderModel order,
            final double previewDeliveryCostRefund) {
        for (final Iterator it = refunds.iterator(); it.hasNext();) {
            final RefundEntryModel refund = (RefundEntryModel)it.next();
            final AbstractOrderEntryModel orderEntry = getOrderEntry(refund, order);
            if (orderEntry == null) {
                continue;
            }
            final long previousQuantity = orderEntry.getQuantity().longValue();
            final long newQuantity = previousQuantity - refund.getExpectedQuantity().longValue();
            orderEntry.setQuantity(Long.valueOf(newQuantity));
            orderEntry.setCalculated(Boolean.FALSE);


            if (newQuantity <= 0L) {
                orderEntry.setQuantityStatus(OrderEntryStatus.DEAD);
            }

            discountValueCorrectionStrategy.correctDiscountValues(orderEntry, previousQuantity);

            getModelService().save(orderEntry);

        }

        order.setCalculated(Boolean.FALSE);
        getModelService().save(order);

        targetCancelService.previewOrderPreventingIncrease(order, previewDeliveryCostRefund);
    }

    protected void processInternalRefundRequest(final ReturnRequestModel request, final CancelRequestResponse response,
            final OrderModel order, final CreditCardPaymentInfoModel newPayment, final List<RefundEntryModel> refunds,
            final IpgNewRefundInfoDTO ipgNewRefundInfoDTO)
                    throws OrderReturnRecordsHandlerException {
        // Create History Snapshot first.
        final TargetOrderReturnRecordHandler modificationHandler = (TargetOrderReturnRecordHandler)getModificationHandler();
        final OrderReturnRecordEntryModel orderReturnRecordEntryModel = modificationHandler
                .createRefundEntry(order, request, "Refund request for order: " + order.getCode());

        updateOrder(refunds, order);
        getModelService().refresh(order);

        targetCancelService.recalculateOrderPreventingIncrease(order, null);

        final Double orderTotalBeforeVoucherUpdate = order.getTotalPrice();

        voucherCorrectionStrategy.correctAppliedVouchers(order);

        flybuysDiscountService
                .setFlybuysRefundDetailsInModificationEntry(orderReturnRecordEntryModel, order,
                        orderTotalBeforeVoucherUpdate);

        // remove return request inprogress status
        final OrderReturnRecordModel returnRecord = (OrderReturnRecordModel)orderReturnRecordEntryModel
                .getOwner();
        returnRecord.setInProgress(false);
        getModelService().saveAll(returnRecord, orderReturnRecordEntryModel);
        getModelService().refresh(orderReturnRecordEntryModel);

        // Then you can calculate the difference
        final Double paidSoFar = findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(order);
        final BigDecimal refundAmount = PriceCalculator.subtract(order.getTotalPrice(), paidSoFar);

        if (refundAmount.compareTo(BigDecimal.ZERO) < 0) {
            throw new TargetOrderRefundException("Can't refund the amount below 0, amount="
                    + refundAmount.doubleValue()
                    + " , orderId=" + order.getCode());
        }

        // Pass some data out in the response
        response.setRefundAmount(refundAmount);
        response.setOrderModificationRecordEntryModel(orderReturnRecordEntryModel);

        // Do the refund at the end to minimise chance of rollback afterwards
        if (refundAmount.compareTo(BigDecimal.ZERO) > 0) {
            // Note we save the PTEM data in response in case we roll everything back so we can recreate them
            if (isFollowOnRefund(newPayment, ipgNewRefundInfoDTO)) {

                final List<PaymentTransactionEntryModel> refundedEntryList = targetRefundPaymentService
                        .performFollowOnRefund(order, refundAmount);
                final List<PaymentTransactionEntryDTO> refundedEntryDtoList = targetRefundPaymentService
                        .convertPaymentTransactionEntries(refundedEntryList);
                response.setFollowOnRefundPaymentList(refundedEntryList);
                response.setFollowOnRefundPaymentDataList(refundedEntryDtoList);
                response.setRefundedAmount(targetRefundPaymentService
                        .findRefundedAmountAfterRefund(refundedEntryDtoList));
            }
            else if (isStandaloneRefund(newPayment, ipgNewRefundInfoDTO)) {
                response.setStandaloneRefundPaymentData(targetRefundPaymentService.performStandaloneRefund(order,
                        refundAmount, newPayment));
                getModelService().save(newPayment);
            }
            else if (isIpgManualRefund(newPayment, ipgNewRefundInfoDTO)) {
                final List<PaymentTransactionEntryModel> refundedEntryList = targetRefundPaymentService
                        .performIpgManualRefund(order, refundAmount, ipgNewRefundInfoDTO);
                final List<PaymentTransactionEntryDTO> refundedEntryDtoList = targetRefundPaymentService
                        .convertPaymentTransactionEntries(refundedEntryList);
                response.setFollowOnRefundPaymentList(refundedEntryList);
                response.setFollowOnRefundPaymentDataList(refundedEntryDtoList);
                response.setRefundedAmount(targetRefundPaymentService
                        .findRefundedAmountAfterRefund(refundedEntryDtoList));
            }
        }
    }

    private boolean isFollowOnRefund(final CreditCardPaymentInfoModel newPayment,
            final IpgNewRefundInfoDTO newRefundInfoDTO) {
        return (newRefundInfoDTO == null && (newPayment == null || newPayment.getSubscriptionId() == null || newPayment
                .getSubscriptionId()
                .isEmpty()));
    }

    private boolean isStandaloneRefund(final CreditCardPaymentInfoModel newPayment,
            final IpgNewRefundInfoDTO newRefundInfoDTO) {
        return (newRefundInfoDTO == null && !(newPayment == null || newPayment.getSubscriptionId() == null || newPayment
                .getSubscriptionId()
                .isEmpty()));
    }

    private boolean isIpgManualRefund(final CreditCardPaymentInfoModel newPayment,
            final IpgNewRefundInfoDTO newRefundInfoDTO) {
        return (newRefundInfoDTO != null && (newPayment == null || newPayment.getSubscriptionId() == null || newPayment
                .getSubscriptionId()
                .isEmpty()));
    }

    protected void updateOrder(final List<RefundEntryModel> refunds, final OrderModel order) {

        for (final Iterator it = refunds.iterator(); it.hasNext();) {
            final RefundEntryModel refund = (RefundEntryModel)it.next();
            final AbstractOrderEntryModel orderEntry = getOrderEntry(refund, order);
            if (orderEntry != null) {
                final long previousQuantity = orderEntry.getQuantity().longValue();
                final long newQuantity = previousQuantity - refund.getExpectedQuantity().longValue();
                orderEntry.setQuantity(Long.valueOf(newQuantity));
                orderEntry.setCalculated(Boolean.FALSE);

                if (newQuantity <= 0) {
                    orderEntry.setQuantityStatus(OrderEntryStatus.DEAD);
                }

                discountValueCorrectionStrategy.correctDiscountValues(orderEntry, previousQuantity);

                getModelService().save(orderEntry);
            }
        }

    }

    /**
     * @param order
     * @return true or false
     */
    protected boolean isPreviewOrder(final OrderModel order) {
        Assert.notNull(order);

        if (order.getOriginalVersion() == null) {
            return false;
        }

        return true;
    }

    @Override
    public boolean isDeliveryCostRefundable(final OrderModel order) {
        Assert.notNull(order, "Order can't be null");

        return isRefundable(order, order.getEntries().get(0), 0)
                && targetFindDeliveryCostStrategy.deliveryCostOutstandingForOrder(order) > 0;
    }

    @Override
    public boolean isRefundable(final OrderModel order, final AbstractOrderEntryModel entry,
            final long returnQuantity) {
        boolean isRefundable = CollectionUtils.isEmpty(refundableChecks);
        if (refundableChecks != null) {
            for (final ReturnableCheck strategy : refundableChecks) {
                isRefundable = strategy.perform(order, entry, returnQuantity);

                if (!(isRefundable)) {
                    return false;
                }
            }
        }
        return isRefundable;
    }

    /**
     * @param businessProcessService
     *            the businessProcessService to set
     */
    @Required
    public void setBusinessProcessService(final TargetBusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
    }

    /**
     * @param findOrderTotalPaymentMadeStrategy
     *            the findOrderTotalPaymentMadeStrategy to set
     */
    @Required
    public void setFindOrderTotalPaymentMadeStrategy(
            final FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy) {
        this.findOrderTotalPaymentMadeStrategy = findOrderTotalPaymentMadeStrategy;
    }

    /**
     * @param transactionTemplate
     *            the transactionTemplate to set
     */
    @Required
    public void setTransactionTemplate(final TransactionTemplate transactionTemplate) {
        this.transactionTemplate = transactionTemplate;
    }

    /**
     * @param targetRefundPaymentService
     *            the targetRefundPaymentService to set
     */
    @Required
    public void setTargetRefundPaymentService(final TargetRefundPaymentService targetRefundPaymentService) {
        this.targetRefundPaymentService = targetRefundPaymentService;
    }

    /**
     * @param targetCancelService
     *            the targetCancelService to set
     */
    @Required
    public void setTargetCancelService(final TargetCancelService targetCancelService) {
        this.targetCancelService = targetCancelService;
    }

    /**
     * @param voucherCorrectionStrategy
     *            the voucherCorrectionStrategy to set
     */
    @Required
    public void setVoucherCorrectionStrategy(final VoucherCorrectionStrategy voucherCorrectionStrategy) {
        this.voucherCorrectionStrategy = voucherCorrectionStrategy;
    }

    /**
     * @param discountValueCorrectionStrategy
     *            the discountValueCorrectionStrategy to set
     */
    @Required
    public void setDiscountValueCorrectionStrategy(
            final DiscountValueCorrectionStrategy discountValueCorrectionStrategy) {
        this.discountValueCorrectionStrategy = discountValueCorrectionStrategy;
    }

    /**
     * @param flybuysDiscountService
     *            the flybuysDiscountService to set
     */
    @Required
    public void setFlybuysDiscountService(final FlybuysDiscountService flybuysDiscountService) {
        this.flybuysDiscountService = flybuysDiscountService;
    }

    /**
     * @param orderHistoryService
     *            the orderHistoryService to set
     */
    @Required
    public void setOrderHistoryService(final OrderHistoryService orderHistoryService) {
        this.orderHistoryService = orderHistoryService;
    }

    /**
     * @param promotionsService
     *            the promotionsService to set
     */
    @Required
    public void setPromotionsService(final PromotionsService promotionsService) {
        this.promotionsService = promotionsService;
    }


    /**
     * @param targetFindDeliveryCostStrategy
     *            the targetFindDeliveryCostStrategy to set
     */
    @Required
    public void setTargetFindDeliveryCostStrategy(final TargetFindDeliveryCostStrategy targetFindDeliveryCostStrategy) {
        this.targetFindDeliveryCostStrategy = targetFindDeliveryCostStrategy;
    }

    /**
     * @param refundableChecks
     *            the refundableChecks to set
     */
    @Required
    public void setRefundableChecks(final List<ReturnableCheck> refundableChecks) {
        this.refundableChecks = refundableChecks;
    }

    /**
     * @param targetTicketBusinessService
     *            the targetTicketBusinessService to set
     */
    @Required
    public void setTargetTicketBusinessService(final TargetTicketBusinessService targetTicketBusinessService) {
        this.targetTicketBusinessService = targetTicketBusinessService;
    }

}

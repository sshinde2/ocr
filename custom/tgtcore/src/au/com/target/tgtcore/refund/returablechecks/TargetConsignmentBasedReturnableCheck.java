/**
 * 
 */
package au.com.target.tgtcore.refund.returablechecks;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.returns.strategy.impl.DefaultConsignmentBasedReturnableCheck;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;


/**
 * for a particular product(for more than 1 in quantity), check if it is split into multiple 'shipped' consignments. Add
 * all the quantities for that product in shipped consignments.
 * 
 * @author gsing236
 *
 */
public class TargetConsignmentBasedReturnableCheck extends DefaultConsignmentBasedReturnableCheck {

    @Override
    public boolean perform(final OrderModel order, final AbstractOrderEntryModel orderentry,
            final long returnQuantity) {

        if ((returnQuantity < 1L) || (orderentry.getQuantity().longValue() < returnQuantity)) {
            return false;
        }

        if (OrderStatus.COMPLETED != order.getStatus()) {
            return false;
        }

        final Set<ConsignmentModel> consignments = order.getConsignments();
        boolean isReturnable = false;

        if (CollectionUtils.isNotEmpty(consignments)) {
            long shippedQuantityOfProduct = 0;
            for (final ConsignmentModel consignment : consignments) {

                if (ConsignmentStatus.SHIPPED == consignment.getStatus()) {
                    shippedQuantityOfProduct += getShippedQuantityOfProduct(orderentry, consignment);
                }
            }
            isReturnable = shippedQuantityOfProduct >= returnQuantity;
        }

        return isReturnable;
    }

    /**
     * @param orderentry
     * @param consignment
     */
    private long getShippedQuantityOfProduct(final AbstractOrderEntryModel orderentry,
            final ConsignmentModel consignment) {

        long shippedQuantity = 0;
        final Set<ConsignmentEntryModel> consignmentEntries = consignment.getConsignmentEntries();
        for (final ConsignmentEntryModel entryModel : consignmentEntries) {
            if (entryModel.getOrderEntry().equals(orderentry)
                    && entryModel.getShippedQuantity() != null) {
                    shippedQuantity = entryModel.getShippedQuantity().longValue();
            }
        }
        return shippedQuantity;
    }
}

/**
 * 
 */
package au.com.target.tgtcore.resumer.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.OrderService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Collections;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.order.TargetCommerceCheckoutService;
import au.com.target.tgtpayment.resumer.PaymentProcessResumer;
import au.com.target.tgtpayment.service.PaymentsInProgressService;


/**
 * Resumer for interruption in place order.
 * 
 */
public class PlaceOrderProcessResumer implements PaymentProcessResumer {

    private static final String BASE_STORE_NAME = "target";
    private static final String BASE_SITE_NAME = "target";

    private static final Logger LOG = Logger.getLogger(PlaceOrderProcessResumer.class);

    private TargetCommerceCheckoutService targetCommerceCheckoutService;

    private FlexibleSearchService flexibleSearchService;

    private BaseSiteService baseSiteService;

    private BaseStoreService baseStoreService;

    private OrderService orderService;

    private PromotionsService promotionService;

    private CalculationService calculationService;

    private ModelService modelService;

    private PaymentsInProgressService paymentsInProgressService;

    @Override
    public void resumeProcess(final AbstractOrderModel abstractOrderModel,
            final PaymentTransactionEntryModel paymentTransactionEntryModel) {

        // Note for place order the PaymentTransactionEntryModel is extracted from the order in SubmitOrderEventListener,
        // so we don't need to do anything to pass it to the business process here.

        Assert.notNull(abstractOrderModel, "abstractOrderModel cannot be null");
        Assert.isTrue(abstractOrderModel instanceof CartModel, "abstractOrderModel must be a CartModel");

        final CartModel cart = (CartModel)abstractOrderModel;

        //Recalculate cart is case it is not calculated
        if (Boolean.FALSE.equals(cart.getCalculated())) {
            try {
                calculationService.calculateTotals(cart, false);
            }
            catch (final CalculationException e) {
                LOG.error("Not able to recalculate Cart", e);
            }
        }

        // The payment transaction entry will definitely have been created, since this is done before PIP is created.
        // If the capture wasn't actually done then entry status will be the default REVIEW.
        // We'll proceed anyway, and check later in the CheckOrderAction in the business process.


        // Need to check whether the order has been created or not
        final OrderModel existingOrder = findExistingModel(cart);

        if (existingOrder == null) {
            try {
                final CommerceOrderResult orderResult = targetCommerceCheckoutService
                        .placeOrder(createCommerceCheckoutParameterForCart(cart));

                final OrderModel newOrder = orderResult.getOrder();
                // Do this because setting them from current session doesn't work in placeOrder
                setOrderBaseDetails(newOrder);

                targetCommerceCheckoutService.afterPlaceOrder(cart, newOrder);
            }
            catch (final InvalidCartException e) {
                LOG.error("The Cart is not Valid", e);
            }

            return;
        }

        if (checkOrderWasPlacedSuccessfully(existingOrder)) {
            paymentsInProgressService.removeInProgressPayment(cart);
            return;
        }

        recoverFromPreviousOrder(existingOrder, cart);
        targetCommerceCheckoutService.afterPlaceOrder(cart, existingOrder);
    }

    /**
     * @return CommerceCheckoutParameter
     */
    protected CommerceCheckoutParameter createCommerceCheckoutParameterForCart(final CartModel cart) {
        final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
        checkoutParameter.setCart(cart);
        determineSalesApplication(cart, checkoutParameter);
        return checkoutParameter;
    }

    /**
     * @param cart
     * @param checkoutParameter
     */
    private void determineSalesApplication(final CartModel cart, final CommerceCheckoutParameter checkoutParameter) {
        if (StringUtils.isNotEmpty(cart.getKioskStore())) {
            checkoutParameter.setSalesApplication(SalesApplication.KIOSK);
        }
        else if (null != cart.getEBayOrderNumber()) {
            checkoutParameter.setSalesApplication(SalesApplication.EBAY);
        }
        else {
            checkoutParameter.setSalesApplication(SalesApplication.WEB);
        }
    }

    /**
     * Check if the existing order was actually placed ok.
     * 
     * @param existingOrder
     * @return true if order was placed ok
     */
    protected boolean checkOrderWasPlacedSuccessfully(final OrderModel existingOrder) {

        return (existingOrder.getStatus() != null
                && existingOrder.getStatus() != OrderStatus.PENDING
                && existingOrder.getStatus() != OrderStatus.CREATED);
    }

    /**
     * This logic is mainly copied from DefaultCommerceCheckoutService<br/>
     * Need to pretty much populate this values and save it again.
     * 
     * @param existingOrder
     * @param cart
     */
    protected void recoverFromPreviousOrder(final OrderModel existingOrder, final CartModel cart) {
        final CustomerModel customer = (CustomerModel)existingOrder.getUser();
        validateParameterNotNull(customer, "Customer model cannot be null");

        existingOrder.setDate(existingOrder.getDate() == null ? new Date() : existingOrder.getDate());

        setOrderBaseDetails(existingOrder);

        // clear the promotionResults that where cloned from cart PromotionService.transferPromotionsToOrder will copy them over bellow.
        existingOrder.setAllPromotionResults(Collections.<PromotionResultModel> emptySet());

        if (cart.getPaymentInfo() != null) {
            existingOrder.setPaymentAddress(cart.getPaymentInfo().getBillingAddress());
            existingOrder.getPaymentInfo().setBillingAddress(cart.getPaymentInfo().getBillingAddress());
            getModelService().save(existingOrder.getPaymentInfo());
        }
        getModelService().save(existingOrder);

        // Transfer promotions to the order
        // Don't know what will happen, if the promotion has been applied to the order
        promotionService.transferPromotionsToOrder(cart, existingOrder, false);

        // Calculate the order now that it has been copied
        try {
            calculationService.calculateTotals(existingOrder, false);
        }
        catch (final CalculationException ex) {
            LOG.error("Failed to calculate order [" + existingOrder + "]", ex);
        }

        getModelService().refresh(existingOrder);
        getModelService().refresh(customer);
        orderService.submitOrder(existingOrder);
    }

    /**
     * Check whether the existing cart has already been converted to an Order yet
     * 
     * @param cart
     * @return OrderModel if it has been converted, otherwise return null
     */
    protected OrderModel findExistingModel(final CartModel cart) {
        final String orderCode = cart.getCode();

        OrderModel example = new OrderModel();
        example.setCode(orderCode);

        try {
            example = flexibleSearchService.getModelByExample(example);
        }
        catch (final ModelNotFoundException exp) {
            example = null;
            LOG.warn("The cart has not been converted to Order. " + cart.getCode());
        }

        return example;
    }

    /**
     * Store the base site, store and SalesApplication details on the order
     * 
     * @param order
     */
    private void setOrderBaseDetails(final OrderModel order) {

        order.setSite(getBaseSite());
        order.setStore(getBaseStore());
        order.setSalesApplication(SalesApplication.WEB);
        getModelService().save(order);
    }

    private BaseSiteModel getBaseSite() {

        return baseSiteService.getBaseSiteForUID(BASE_SITE_NAME);
    }

    private BaseStoreModel getBaseStore() {

        return baseStoreService.getBaseStoreForUid(BASE_STORE_NAME);
    }

    /**
     * @param targetCommerceCheckoutService
     *            the targetCommerceCheckoutService to set
     */
    @Required
    public void setTargetCommerceCheckoutService(final TargetCommerceCheckoutService targetCommerceCheckoutService) {
        this.targetCommerceCheckoutService = targetCommerceCheckoutService;
    }

    /**
     * @param flexibleSearchService
     *            the flexibleSearchService to set
     */
    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    /**
     * @param baseSiteService
     *            the baseSiteService to set
     */
    @Required
    public void setBaseSiteService(final BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }

    /**
     * @param baseStoreService
     *            the baseStoreService to set
     */
    @Required
    public void setBaseStoreService(final BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    /**
     * @param orderService
     *            the orderService to set
     */
    @Required
    public void setOrderService(final OrderService orderService) {
        this.orderService = orderService;
    }

    /**
     * @param promotionService
     *            the promotionService to set
     */
    @Required
    public void setPromotionService(final PromotionsService promotionService) {
        this.promotionService = promotionService;
    }

    /**
     * @param calculationService
     *            the calculationService to set
     */
    @Required
    public void setCalculationService(final CalculationService calculationService) {
        this.calculationService = calculationService;
    }

    /**
     * @return the modelService
     */
    public ModelService getModelService() {
        return modelService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }
}

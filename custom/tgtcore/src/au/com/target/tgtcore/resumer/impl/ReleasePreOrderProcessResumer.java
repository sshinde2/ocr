/**
 * 
 */
package au.com.target.tgtcore.resumer.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtpayment.resumer.PaymentProcessResumer;
import au.com.target.tgtpayment.service.PaymentsInProgressService;


/**
 * Resumer for interruption in release pre-order.
 * 
 * @author gsing236
 *
 */
public class ReleasePreOrderProcessResumer implements PaymentProcessResumer {

    private PaymentsInProgressService paymentsInProgressService;

    @Override
    public void resumeProcess(final AbstractOrderModel abstractOrderModel,
            final PaymentTransactionEntryModel paymentTransactionEntryModel) {
        final OrderModel orderModel = (OrderModel)abstractOrderModel;
        // if order is not in PARKED state then just remove the entry as it has been processed
        // but somehow entry remained in table.
        if (OrderStatus.PARKED != orderModel.getStatus()) {
            paymentsInProgressService.removeInProgressPayment(orderModel);
            return;
        }
        getTargetBusinessProcessService().startFluentReleasePreOrderProcess(orderModel);

    }

    /**
     * Lookup for TargetBusinessProcessService
     * 
     * @return TargetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

    /**
     * @param paymentsInProgressService
     *            the paymentsInProgressService to set
     */
    @Required
    public void setPaymentsInProgressService(final PaymentsInProgressService paymentsInProgressService) {
        this.paymentsInProgressService = paymentsInProgressService;
    }
}

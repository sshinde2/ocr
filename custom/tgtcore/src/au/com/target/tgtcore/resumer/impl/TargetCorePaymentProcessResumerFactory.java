/**
 * 
 */
package au.com.target.tgtcore.resumer.impl;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.resumer.PaymentProcessResumer;
import au.com.target.tgtpayment.resumer.impl.TargetPaymentProcessResumerFactory;


/**
 * Default resumer factory can only return a place order resumer
 * 
 */
public class TargetCorePaymentProcessResumerFactory extends TargetPaymentProcessResumerFactory {

    private PlaceOrderProcessResumer placeOrderProcessResumer;
    private ReleasePreOrderProcessResumer releasePreOrderProcessResumer;

    @Override
    public PaymentProcessResumer getPaymentProcessResumer(final PaymentCaptureType paymentCaptureType) {
        Assert.notNull(paymentCaptureType);

        if (paymentCaptureType.equals(PaymentCaptureType.PLACEORDER)) {
            return placeOrderProcessResumer;
        }
        else if (PaymentCaptureType.RELEASEPREORDER.equals(paymentCaptureType)) {
            return releasePreOrderProcessResumer;
        }

        return null;
    }


    /**
     * @param placeOrderProcessResumer
     *            the placeOrderProcessResumer to set
     */
    @Required
    public void setPlaceOrderProcessResumer(final PlaceOrderProcessResumer placeOrderProcessResumer) {
        this.placeOrderProcessResumer = placeOrderProcessResumer;
    }


    /**
     * @param releasePreOrderProcessResumer
     *            the releasePreOrderProcessResumer to set
     */
    @Required
    public void setReleasePreOrderProcessResumer(final ReleasePreOrderProcessResumer releasePreOrderProcessResumer) {
        this.releasePreOrderProcessResumer = releasePreOrderProcessResumer;
    }

}

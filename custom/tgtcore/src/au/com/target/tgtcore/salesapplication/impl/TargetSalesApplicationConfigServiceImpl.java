/**
 * 
 */
package au.com.target.tgtcore.salesapplication.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.SalesApplicationConfigModel;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;


/**
 * @author ragarwa3
 *
 */
public class TargetSalesApplicationConfigServiceImpl implements TargetSalesApplicationConfigService {

    private static final Logger LOG = Logger.getLogger(TargetSalesApplicationConfigServiceImpl.class);

    private GenericDao<SalesApplicationConfigModel> salesApplicationConfigDao;

    @Override
    public SalesApplicationConfigModel getConfigForSalesApplication(final SalesApplication salesApp) {

        if (null == salesApp) {
            return null;
        }

        final List<SalesApplicationConfigModel> configs = salesApplicationConfigDao
                .find(Collections.singletonMap(SalesApplicationConfigModel.SALESAPPLICATION, salesApp));

        if (CollectionUtils.isNotEmpty(configs)) {
            return configs.get(0);
        }
        else {
            return null;
        }
    }

    @Override
    public List<SalesApplicationConfigModel> getAllPartnerSalesApplicationConfigs() {
        final List<SalesApplicationConfigModel> allConfigs = salesApplicationConfigDao.find();

        if (CollectionUtils.isEmpty(allConfigs)) {
            LOG.warn("No sales application configs found.");
            return ListUtils.EMPTY_LIST;
        }

        final List<SalesApplicationConfigModel> partnerConfigs = new ArrayList<>();

        for (final SalesApplicationConfigModel config : allConfigs) {
            if (config.isIsPartnerChannel()) {
                partnerConfigs.add(config);
            }
        }

        if (CollectionUtils.isEmpty(partnerConfigs)) {
            LOG.warn("No partner sales application configs found.");
        }

        return partnerConfigs;
    }

    @Override
    public boolean isSkipFraudCheck(final SalesApplication salesApp) {

        final SalesApplicationConfigModel salesAppConfig = getConfigForSalesApplication(salesApp);

        if (null != salesAppConfig && salesAppConfig.isSkipFraudCheck()) {
            return true;
        }

        return false;
    }

    @Override
    public boolean isRefundDenied(final SalesApplication salesApp) {

        final SalesApplicationConfigModel salesAppConfig = getConfigForSalesApplication(salesApp);

        if (null != salesAppConfig && salesAppConfig.isDenyRefunds()) {
            return true;
        }

        return false;
    }

    @Override
    public boolean isDenyCancellation(final SalesApplication salesApp) {
        final SalesApplicationConfigModel salesAppConfig = getConfigForSalesApplication(salesApp);

        if (null != salesAppConfig && salesAppConfig.isDenyCancellation()) {
            return true;
        }

        return false;
    }

    @Override
    public boolean isSuppressTaxCalculation(final SalesApplication salesApp) {

        final SalesApplicationConfigModel salesAppConfig = getConfigForSalesApplication(salesApp);

        if (salesAppConfig != null && salesAppConfig.isSuppressTaxCalculation()) {
            return true;
        }

        return false;
    }

    @Override
    public boolean isReserveStockOnAcceptOrder(final SalesApplication salesApp) {

        final SalesApplicationConfigModel salesAppConfig = getConfigForSalesApplication(salesApp);

        if (salesAppConfig != null && salesAppConfig.isReserveStockOnAcceptOrder()) {
            return true;
        }

        return false;
    }

    @Override
    public boolean isSuppressOrderConfMail(final SalesApplication salesApp) {
        final SalesApplicationConfigModel salesAppConfig = getConfigForSalesApplication(salesApp);
        if (salesAppConfig != null && salesAppConfig.isSuppressOrderConfMail()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isSuppressTaxInvoice(final SalesApplication salesApp) {
        final SalesApplicationConfigModel salesAppConfig = getConfigForSalesApplication(salesApp);

        if (null != salesAppConfig && salesAppConfig.isSuppressTaxInvoice()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isPartnerChannel(final SalesApplication salesApp) {
        final SalesApplicationConfigModel salesAppConfig = getConfigForSalesApplication(salesApp);

        if (null != salesAppConfig && salesAppConfig.isIsPartnerChannel()) {
            return true;
        }
        return false;
    }

    /**
     * @param salesApplicationConfigDao
     *            the salesApplicationConfigDao to set
     */
    @Required
    public void setSalesApplicationConfigDao(final GenericDao<SalesApplicationConfigModel> salesApplicationConfigDao) {
        this.salesApplicationConfigDao = salesApplicationConfigDao;
    }

}

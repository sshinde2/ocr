/**
 * 
 */
package au.com.target.tgtcore.salesapplication.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.servicelayer.session.SessionService;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;


/**
 * @author jjayawa1
 * 
 */
public class TargetSalesApplicationServiceImpl implements TargetSalesApplicationService {

    protected static final String SELECTED_SALES_APPLICATION = "Selected-Sales-Application";

    private SessionService sessionService;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.salesapplication.TargetSalesApplicationService#getCurrentSalesApplication()
     */
    @Override
    public SalesApplication getCurrentSalesApplication() {
        return getSessionService().getAttribute(SELECTED_SALES_APPLICATION);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.salesapplication.TargetSalesApplicationService#setCurrentSalesApplication(de.hybris.platform.commerceservices.enums.SalesApplication)
     */
    @Override
    public void setCurrentSalesApplication(final SalesApplication salesApplication) {
        getSessionService().setAttribute(SELECTED_SALES_APPLICATION, salesApplication);
    }

    /**
     * @param sessionService
     *            the sessionService to set
     */
    @Required
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

    /**
     * @return the sessionService
     */
    protected SessionService getSessionService() {
        return sessionService;
    }

}

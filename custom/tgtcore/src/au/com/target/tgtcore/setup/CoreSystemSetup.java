/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtcore.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.validation.services.ValidationService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 * 
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = TgtCoreConstants.EXTENSIONNAME)
public class CoreSystemSetup extends AbstractSystemSetup {
    public static final String IMPORT_SITES = "importSites";
    public static final String IMPORT_SYNC_CATALOGS = "syncProducts&ContentCatalogs";
    public static final String IMPORT_ACCESS_RIGHTS = "accessRights";
    public static final String TARGET = "target";

    private CommonI18NService commonI18NService;
    private String isoLanguageCode;
    private String isoCurrencyCode;


    /**
     * @param commonI18NService
     *            the commonI18NService to set
     */
    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    /**
     * @param isoLanguageCode
     *            the isoLanguageCode to set
     */
    @Required
    public void setIsoLanguageCode(final String isoLanguageCode) {
        this.isoLanguageCode = isoLanguageCode;
    }

    /**
     * @param isoCurrencyCode
     *            the isoCurrencyCode to set
     */
    @Required
    public void setIsoCurrencyCode(final String isoCurrencyCode) {
        this.isoCurrencyCode = isoCurrencyCode;
    }

    /**
     * This method will be called by system creator during initialization and system update. Be sure that this method
     * can be called repeatedly.
     * 
     * @param context
     *            the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
    public void createEssentialData(final SystemSetupContext context) {
        importImpexFile(context, "/tgtcore/import/common/countries.impex");
        importImpexFile(context, "/tgtcore/import/common/states.impex");

        if (context.getProcess().equals(Process.INIT)) {
            importImpexFile(context, "/tgtcore/import/common/setup-media-folders.impex");
        }

        importImpexFile(context, "/tgtcore/import/productCatalogs/" + TARGET + "ProductCatalog/catalog.impex", true);
        importImpexFile(context, "/tgtcore/import/common/essential-data.impex");

        if (context.getProcess().equals(Process.INIT)) {
            importImpexFile(context, "/tgtcore/import/common/essential-data-init.impex");
        }

        importImpexFile(context, "/tgtcore/import/common/mcc-sites-links.impex");
        importImpexFile(context, "/tgtcore/import/common/themes.impex");
        importImpexFile(context, "/tgtcore/import/stores/target/store.impex", true);
        importImpexFile(context, "/tgtcore/import/common/was-now-price-config.impex");

        //Set the default language (EN) and currency (AUD) in session context
        commonI18NService.setCurrentCurrency(commonI18NService.getCurrency(isoCurrencyCode));
        commonI18NService.setCurrentLanguage(commonI18NService.getLanguage(isoLanguageCode));

        createProductCatalogSyncJob(context, TARGET + "ProductCatalog");

        importImpexFile(context, "/tgtcore/import/productCatalogs/" + TARGET
                + "ProductCatalog/remove-unwanted-roottypes-from-sync.impex");

        importImpexFile(context, "/tgtcore/import/common/product-owner-user-group.impex");
    }

    /**
     * Generates the Dropdown and Multi-select boxes for the project data import
     */
    @Override
    @SystemSetupParameterMethod
    public List<SystemSetupParameter> getInitializationOptions() {
        final List<SystemSetupParameter> params = new ArrayList<>();

        params.add(createBooleanSystemSetupParameter(IMPORT_SITES, "Import Product", true));
        params.add(createBooleanSystemSetupParameter(IMPORT_SYNC_CATALOGS, "Sync Products Catalogs", true));
        params.add(createBooleanSystemSetupParameter(IMPORT_ACCESS_RIGHTS, "Import Users & Groups", true));

        return params;
    }

    /**
     * This method will be called during the system initialization.
     * 
     * @param context
     *            the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.PROJECT, process = Process.ALL)
    public void createProjectData(final SystemSetupContext context) {
        final boolean importSites = getBooleanSystemSetupParameter(context, IMPORT_SITES);
        final boolean importAccessRights = getBooleanSystemSetupParameter(context, IMPORT_ACCESS_RIGHTS);
        final boolean syncProductCatalog = getBooleanSystemSetupParameter(context, IMPORT_SYNC_CATALOGS);
        if (importSites) {

            final AbstractImportCatalogAwareDataStrategy target = new AbstractImportCatalogAwareDataStrategy(context) {

                @Override
                protected String getProductCatalogName() {
                    return TARGET;
                }

                @Override
                protected List<String> getContentCatalogNames() {
                    return Arrays.asList(TARGET);
                }

                @Override
                protected List<String> getStoreNames() {
                    return Arrays.asList(TARGET);
                }


            };
            target.importAllData();
            if (syncProductCatalog) {
                target.syncProductCatalog();
            }

            final ValidationService validation = getBeanForName("validationService");
            validation.reloadValidationEngine();
        }

        final List<String> extensionNames = getExtensionNames();

        if (importAccessRights && extensionNames.contains("cscockpit")) {
            importImpexFile(context, "/tgtcore/import/cockpits/cscockpit/cscockpit-users.impex");
            importImpexFile(context, "/tgtcore/import/cockpits/cscockpit/cscockpit-access-rights.impex");
        }

        importImpexFile(context, "/tgtcore/import/common/jobs-cleanup.impex");
        importImpexFile(context, "/tgtcore/import/common/jobs.impex");
        importImpexFile(context, "/tgtcore/import/common/job-triggers.impex");

        importImpexFile(context, "/tgtcore/import/common/target_catalog_version_sync_job.impex");

        importImpexFile(context, "/tgtcore/import/common/store-user-group.impex");

        importImpexFile(context, "/tgtcore/import/common/employee-group.impex");

        importImpexFile(context, "/tgtcore/import/search/search.impex");

        importImpexFile(context, "/tgtcore/import/common/flybuys-redeem-config.impex");

        importImpexFile(context, "/tgtcore/import/common/size-ordering-user-groups.impex");
    }

    protected List<String> getExtensionNames() {
        return Registry.getCurrentTenant().getTenantSpecificExtensionNames();
    }

    protected <T> T getBeanForName(final String name) {
        return (T)Registry.getApplicationContext().getBean(name);
    }


    /**
     * Abstraction for a import process which should be adopted to use a dependent CatalogVersionSyncJob
     * 
     */
    private abstract class AbstractImportCatalogAwareDataStrategy {

        private final SystemSetupContext context;

        public AbstractImportCatalogAwareDataStrategy(final SystemSetupContext context) {
            this.context = context;
        }

        protected abstract String getProductCatalogName();

        protected abstract List<String> getContentCatalogNames();

        protected abstract List<String> getStoreNames();

        public void importAllData() {
            createProductCatalog(getProductCatalogName());//
            assignDependent(getProductCatalogName(), getContentCatalogNames());


        }

        public void syncProductCatalog() {

            if (!synchronizeProductCatalog(getProductCatalogName(), true)) {
                logError(context, "Rerunning product catalog synchronization for [" + getProductCatalogName()
                        + "], failed please consult logs for more details.", null);
            }
        }


        private void assignDependent(final String dependsOnProduct, final List<String> dependentContents) {
            if (CollectionUtils.isNotEmpty(dependentContents) && StringUtils.isNotBlank(dependsOnProduct)) {
                final Set<String> dependentSyncJobsNames = new HashSet<>();
                for (final String content : dependentContents) {
                    dependentSyncJobsNames.add(content + "ContentCatalog");
                }

                getSetupSyncJobService().assignDependentSyncJobs(dependsOnProduct + "ProductCatalog",
                        dependentSyncJobsNames);
            }
        }



        private void createProductCatalog(final String productCatalogName) {
            logInfo(context, "Begin importing catalog [" + productCatalogName + "]");

            // Import Product Categories
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/category-baby.impex", false);
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/category-kids.impex", false);
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/category-school.impex", false);
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/category-home.impex", false);
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/category-bodybeauty.impex", false);
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/category-toys.impex", false);
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/category-women.impex", false);
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/category-men.impex", false);
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/category-more.impex", false);
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/category-christmas.impex", false);
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/category-midseason.impex", false);
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/category-entertainment.impex", false);
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/category-alltargetproducts.impex", false);


            // Import Classification Categories
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/kids-classifications.impex", false);
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/toys-classifications.impex", false);
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/baby-classifications.impex", false);
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/body_beauty-classifications.impex", false);
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/men-classifications.impex", false);
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/home-classifications.impex", false);
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/women-classifications.impex", false);
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/school-classifications.impex", false);

            //Promotions
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/promotions.impex", false);

            //Size Types
            importImpexFile(context, "/tgtcore/import/productCatalogs/" + productCatalogName
                    + "ProductCatalog/size-types.impex", false);
        }


        protected boolean synchronizeProductCatalog(final String catalogName, final boolean sync) {
            logInfo(context, "Begin synchronizing Product Catalog [" + catalogName + "] - "
                    + (sync ? "synchronizing" : "initializing job"));

            boolean result = true;

            if (sync) {
                final PerformResult syncCronJobResult = executeCatalogSyncJob(context, catalogName + "ProductCatalog");
                if (isSyncRerunNeeded(syncCronJobResult)) {
                    logInfo(context, "Product catalog [" + catalogName + "] sync has issues.");
                    result = false;
                }
            }

            logInfo(context, "Done " + (sync ? "synchronizing" : "initializing job") + " Product Catalog ["
                    + catalogName
                    + "]");
            return result;
        }
    }

}

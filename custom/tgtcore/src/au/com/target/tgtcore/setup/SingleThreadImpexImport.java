/**
 * 
 */
package au.com.target.tgtcore.setup;



public interface SingleThreadImpexImport {

    /**
     * import impex file using one thread
     * 
     * @param file
     * @param legacyMode
     * @param maxThreads
     */
    void importImpexFileSingleThread(final String file, final boolean errorIfMissing, final boolean legacyMode,
            int maxThreads);

    /**
     * @param file
     * @param errorIfMissing
     * @param legacyMode
     * @param maxThreads
     * @param dumpingEnabled
     */
    void importImpexFileSingleThread(final String file, final boolean errorIfMissing, final boolean legacyMode,
            int maxThreads, boolean dumpingEnabled);

}

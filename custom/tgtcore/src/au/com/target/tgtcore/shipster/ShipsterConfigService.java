/**
 * 
 */
package au.com.target.tgtcore.shipster;

import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import au.com.target.tgtauspost.model.ShipsterConfigModel;


/**
 * @author bhuang3
 *
 */
public interface ShipsterConfigService {

    ShipsterConfigModel getShipsterModelForDeliveryModes(TargetZoneDeliveryModeModel deliveryModeModel);

}

/**
 * 
 */
package au.com.target.tgtcore.shipster.dao;


import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import java.util.Collection;

import au.com.target.tgtauspost.model.ShipsterConfigModel;


/**
 * @author pvarghe2
 *
 */
public interface ShipsterDao {

    /**
     * Get list of Shipster model for the list of delivery modes.
     * 
     * @param deliveryModes
     * @return List<ShipsterConfigModel>
     */
    Collection<ShipsterConfigModel> getShipsterModelForDeliveryModes(
            Collection<TargetZoneDeliveryModeModel> deliveryModes);
}

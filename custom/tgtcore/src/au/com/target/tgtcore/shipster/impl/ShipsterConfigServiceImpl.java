/**
 * 
 */
package au.com.target.tgtcore.shipster.impl;

import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtauspost.model.ShipsterConfigModel;
import au.com.target.tgtcore.shipster.ShipsterConfigService;
import au.com.target.tgtcore.shipster.dao.ShipsterDao;


/**
 * @author bhuang3
 *
 */
public class ShipsterConfigServiceImpl implements ShipsterConfigService {

    private ShipsterDao shipsterDao;

    @Override
    public ShipsterConfigModel getShipsterModelForDeliveryModes(final TargetZoneDeliveryModeModel deliveryModeModel) {
        final List<ShipsterConfigModel> shipsterConfigModelList = (List<ShipsterConfigModel>)shipsterDao
                .getShipsterModelForDeliveryModes(Collections.singletonList(deliveryModeModel));
        if (CollectionUtils.isNotEmpty(shipsterConfigModelList)) {
            return shipsterConfigModelList.get(0);
        }
        return null;
    }

    /**
     * @param shipsterDao
     *            the shipsterDao to set
     */
    @Required
    public void setShipsterDao(final ShipsterDao shipsterDao) {
        this.shipsterDao = shipsterDao;
    }



}

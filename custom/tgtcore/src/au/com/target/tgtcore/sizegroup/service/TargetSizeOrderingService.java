/**
 * 
 */
package au.com.target.tgtcore.sizegroup.service;

import au.com.target.tgtcore.model.TargetProductSizeModel;
import au.com.target.tgtcore.model.TargetSizeGroupModel;


/**
 * @author mjanarth
 *
 */
public interface TargetSizeOrderingService {

    /**
     * Retrieves/creates TargetSizeGroupModel based on the groupName,creates one if missing is true
     * 
     * @param groupName
     * @param createIfMissing
     * @return TargetSizeGroupModel
     */
    TargetSizeGroupModel getTargetSizeGroupByName(String groupName, String productCode, boolean createIfMissing);


    /**
     * Retrieves/creates TargetProductSizeModel based on the size,sizegroup ,creates one if missing is true
     * 
     * @param size
     * @param sizeGroupName
     * @param productCode
     * @return TargetProductSizeModel
     */
    TargetProductSizeModel getTargetProductSize(String size, TargetSizeGroupModel sizeGroupName,
            boolean createIfMissing, String productCode);


}

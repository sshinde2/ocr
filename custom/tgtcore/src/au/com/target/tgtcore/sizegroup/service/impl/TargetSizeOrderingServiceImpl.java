/**
 * 
 */
package au.com.target.tgtcore.sizegroup.service.impl;

import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.text.MessageFormat;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetProductSizeModel;
import au.com.target.tgtcore.model.TargetSizeGroupModel;
import au.com.target.tgtcore.sizegroup.service.TargetSizeOrderingService;
import au.com.target.tgtcore.sizeorder.dao.TargetProductSizeDao;
import au.com.target.tgtcore.sizeorder.dao.TargetSizeGroupDao;
import au.com.target.tgtcore.util.TargetServicesUtil;


/**
 * @author mjanarth
 *
 */
public class TargetSizeOrderingServiceImpl extends AbstractBusinessService implements TargetSizeOrderingService {

    private static final Logger LOG = Logger.getLogger(TargetSizeOrderingServiceImpl.class);

    private static final String ERR_SIZEGROUP = "ERR_SIZEORDERING : Error creating/finding sizegroup name={0}";
    private static final String INFO_TARGETPRODUCTSIZE = "INFO_SIZEORDERING_CREATINGNEWSIZE : Creating new size={0} for sizegroup name={1} at position={2} for product={3}";
    private static final String INFO_SIZEGROUP = "INFO_SIZEORDERING_CREATINGNEWSIZEGROUP : Creating new sizegroup name={0} at position={1} for product={2}";
    private static final String ERR_PRODUCTSIZE = "ERR_SIZEORDERING : Error creating/finding target product size sizegroup = {0},size= {1}";


    private TargetSizeGroupDao targetSizeGroupDao;

    private TargetProductSizeDao targetProductSizeDao;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.sizegroup.service.TargetSizeGroupService#getTargetSizeGroup(java.lang.String, boolean)
     */
    @Override
    public TargetSizeGroupModel getTargetSizeGroupByName(final String groupName, final String productCode,
            final boolean createIfMissing)
            throws ModelSavingException {
        ServicesUtil.validateParameterNotNullStandardMessage("name", groupName);

        final String sizeGroupCode = TargetServicesUtil.extractSafeCodeFromName(groupName, true);

        TargetSizeGroupModel sizeGroup = null;
        try {
            sizeGroup = targetSizeGroupDao.findSizeGroupbyCode(sizeGroupCode);
        }
        catch (final TargetUnknownIdentifierException e) {
            if (createIfMissing) {
                Integer maxPosition = targetSizeGroupDao.getMaxSizeGroupPosition();
                if (null != maxPosition) {
                    maxPosition = Integer.valueOf(maxPosition.intValue() + 1);
                }
                else {
                    maxPosition = Integer.valueOf(1);
                }
                sizeGroup = createSizeGroup(groupName, sizeGroupCode, maxPosition, productCode);
            }
        }
        catch (final TargetAmbiguousIdentifierException e) {
            LOG.error(MessageFormat.format(ERR_SIZEGROUP, sizeGroupCode, e));
        }
        return sizeGroup;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.sizegroup.service.TargetSizeOrderingService#getTargetProductSize(java.lang.String, java.lang.String)
     */
    @Override
    public TargetProductSizeModel getTargetProductSize(final String size, final TargetSizeGroupModel sizeGroup,
            final boolean createIfMissing, final String productCode) throws ModelSavingException {
        ServicesUtil.validateParameterNotNullStandardMessage("sizegroupname", sizeGroup);
        ServicesUtil.validateParameterNotNullStandardMessage("size", size);
        TargetProductSizeModel productSize = null;
        try {
            productSize = targetProductSizeDao.findTargetProductSizeBySizeGroup(size, sizeGroup);
        }
        catch (final TargetUnknownIdentifierException e) {
            Integer position = targetProductSizeDao.getMaxProductSizePosition(sizeGroup);
            if (null != position) {
                position = Integer.valueOf(position.intValue() + 1);

            }
            else {
                position = Integer.valueOf(1);
            }
            if (createIfMissing) {
                productSize = createTargetProductSize(size, sizeGroup, position, productCode);
            }
        }
        catch (final TargetAmbiguousIdentifierException e) {
            LOG.error(MessageFormat.format(ERR_PRODUCTSIZE, size, sizeGroup.getCode(), e));
        }
        return productSize;
    }


    /**
     * 
     * @param name
     * @param sizeGroupCode
     * @param position
     * @return TargetSizeGroupModel
     * @throws ModelSavingException
     */
    protected TargetSizeGroupModel createSizeGroup(final String name, final String sizeGroupCode,
            final Integer position, final String productCode)
            throws ModelSavingException {
        final TargetSizeGroupModel sizeGroup = getModelService().create(TargetSizeGroupModel.class);
        sizeGroup.setCode(sizeGroupCode);
        sizeGroup.setName(name);
        sizeGroup.setPosition(position);
        getModelService().save(sizeGroup);
        getModelService().refresh(sizeGroup);
        LOG.info(MessageFormat.format(INFO_SIZEGROUP, name, sizeGroup.getPosition(), productCode));
        return sizeGroup;
    }


    /**
     * 
     * @param size
     * @param sizeGroup
     * @param position
     * @return TargetProductSizeModel
     * @throws ModelSavingException
     */
    protected TargetProductSizeModel createTargetProductSize(final String size, final TargetSizeGroupModel sizeGroup,
            final Integer position, final String productCode)
            throws ModelSavingException {
        final TargetProductSizeModel productSize = getModelService().create(TargetProductSizeModel.class);
        productSize.setSize(size);
        productSize.setSizeGroup(sizeGroup);
        productSize.setPosition(position);
        getModelService().save(productSize);
        getModelService().refresh(productSize);
        LOG.info(MessageFormat.format(INFO_TARGETPRODUCTSIZE, size, sizeGroup.getName(), productSize.getPosition(),
                productCode));
        return productSize;

    }

    /**
     * @param targetSizeGroupDao
     *            the targetSizeGroupDao to set
     */
    @Required
    public void setTargetSizeGroupDao(final TargetSizeGroupDao targetSizeGroupDao) {
        this.targetSizeGroupDao = targetSizeGroupDao;
    }

    /**
     * @param targetProductSizeDao
     *            the targetProductSizeDao to set
     */
    @Required
    public void setTargetProductSizeDao(final TargetProductSizeDao targetProductSizeDao) {
        this.targetProductSizeDao = targetProductSizeDao;
    }


}

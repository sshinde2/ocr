/**
 * 
 */
package au.com.target.tgtcore.sms.dto;

import au.com.target.tgtcore.sms.enums.TargetSendSmsResponseType;


/**
 * @author mjanarth
 * 
 */
public class TargetSendSmsResponseDto {

    private Integer responseCode;
    private String responseMessage;
    private TargetSendSmsResponseType responseType;



    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage
     *            the responseMessage to set
     */
    public void setResponseMessage(final String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the responseType
     */
    public TargetSendSmsResponseType getResponseType() {
        return responseType;
    }

    /**
     * @param responseType
     *            the responseType to set
     */
    public void setResponseType(final TargetSendSmsResponseType responseType) {
        this.responseType = responseType;
    }

    /**
     * @return the responseCode
     */
    public Integer getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode
     *            the responseCode to set
     */
    public void setResponseCode(final Integer responseCode) {
        this.responseCode = responseCode;
    }

}

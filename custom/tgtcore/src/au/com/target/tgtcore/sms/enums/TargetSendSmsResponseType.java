/**
 * 
 */
package au.com.target.tgtcore.sms.enums;

/**
 * @author mjanarth
 * 
 */
public enum TargetSendSmsResponseType {

    SUCCESS("success"),
    UNAVAILABLE("unavailable"),
    INVALID("invalid"),
    OTHERS("others");

    private String response;

    /**
     * 
     * @param response
     */
    private TargetSendSmsResponseType(final String response)
    {
        this.response = response;
    }

    /**
     * @return the response
     */
    public String getResponse() {
        return response;
    }


}

/**
 * 
 */
package au.com.target.tgtcore.stock;

import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.stock.impl.StockLevelDao;

import java.util.List;
import java.util.Map;


/**
 * @author dcwillia
 * 
 */
public interface TargetStockLevelDao extends StockLevelDao {

    /**
     * Adjust the actual stock level by the amount specified.<br />
     * NOTE: direct database reservation with jdbc query must be used.
     * 
     * @param stockLevel
     *            the stock level to be updated
     * 
     * @param adjust
     *            how much to adjust the actual stock level by (+ve for up, -ve for down)
     */
    void adjustActualAmount(StockLevelModel stockLevel, int adjust);

    /**
     * Move stock from reserve to actual amount, ie subtract from reserve and subtract from actualAmount in one atomic
     * operation.<br />
     * Ensures the calculation of (actualAmount - reserve) is not altered. <br />
     * <br />
     * NOTE: direct database reservation with jdbc query must be used.
     * 
     * @param stockLevel
     *            the stock level to be updated
     * 
     * @param adjust
     *            how much to transfer from reserve to actualAmount
     */
    void transferReserveToActualAmount(StockLevelModel stockLevel, int adjust);

    /**
     * This method force reserve the stock
     * 
     * @param stockLevel
     * @param amount
     */
    Integer forceReserve(StockLevelModel stockLevel, int amount);

    /**
     * This method takes a list of item codes and a warehouse code and looks up the stock for the items in the database.
     * The warehouse code is not mandatory and if not supplied will look at the default warehouse which is setup.
     * 
     * @param itemCodes
     * @param warehouseCode
     * @return map<productCode,availableQty>
     */
    Map<String, Integer> findStockForProductCodesAndWarehouse(final List<String> itemCodes,
            final String warehouseCode);

    /**
     * 
     * @param paramStockLevelModel
     * @param paramInt
     * @param preOrderProduct
     * @return Integer
     */
    Integer reserve(StockLevelModel paramStockLevelModel, int paramInt, boolean preOrderProduct);

    /**
     * Release the stock quantity for the specified stock level, if it is a pre-order product then preOrder quantity
     * will be updated instead of reserved.
     * 
     * @param stockLevelModel
     * @param amount
     * @param preOrderProduct
     * @return Integer
     */
    Integer release(StockLevelModel stockLevelModel, int amount, boolean preOrderProduct);

    /**
     * update total quantity fulfilled by the warehosue
     * 
     * @param currentQuantity
     * @param warehouseModel
     */
    void updateQuantityFulfilledByWarehouse(int currentQuantity, WarehouseModel warehouseModel);

}

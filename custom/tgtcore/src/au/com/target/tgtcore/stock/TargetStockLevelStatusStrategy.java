/**
 * 
 */
package au.com.target.tgtcore.stock;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;


public interface TargetStockLevelStatusStrategy {
    StockLevelStatus checkStatus(String soh);
}

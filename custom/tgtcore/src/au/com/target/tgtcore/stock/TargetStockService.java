/**
 * 
 */
package au.com.target.tgtcore.stock;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.stock.exception.StockLevelNotFoundException;

import java.util.Collection;
import java.util.List;
import java.util.Map;


/**
 * @author dcwillia
 * 
 */
public interface TargetStockService extends StockService {
    /**
     * Adjust the actual stock level by the amount specified.
     * 
     * @param product
     *            the product
     * @param warehouse
     *            the warehouse where product is 'stored'
     * @param adjust
     *            how much to adjust the actual stock level by (+ve for up, -ve for down)
     * @param comment
     *            the comment for the update operation
     * @throws StockLevelNotFoundException
     *             if there is no stock level for specified product in specified warehouse
     */
    void adjustActualAmount(ProductModel product, WarehouseModel warehouse, int adjust, final String comment)
            throws StockLevelNotFoundException;

    /**
     * Move stock from reserve to actual amount, ie subtract from reserve and subtract from actualAmount in one atomic
     * operation.<br />
     * Ensures the calculation of (actualAmount - reserve) is not altered.
     * 
     * @param product
     *            the product
     * @param warehouse
     *            the warehouse where product is 'stored'
     * @param adjust
     *            how much to transfer from reserve to actualAmount
     */
    void transferReserveToActualAmount(ProductModel product, WarehouseModel warehouse, int adjust,
            final String comment);

    /**
     * Force reserve the stock
     * 
     * @param product
     * @param warehouse
     * @param amount
     * @param comment
     */
    public void forceReserve(ProductModel product, WarehouseModel warehouse, int amount, String comment);

    /**
     * Finds the stock level of the specified product from the online warehouse and calculates the actual available
     * amount.
     * 
     * @param product
     * @return actual available amount for the specific product
     */
    int getStockLevelAmountFromOnlineWarehouses(ProductModel product);

    /**
     * Check if stock available at Warehouse.
     * 
     * @param orderEntry
     * @param warehouse
     * @return Boolean True/False
     */
    boolean isOrderEntryInStockAtWarehouse(AbstractOrderEntryModel orderEntry, WarehouseModel warehouse);

    /**
     * Check if stock available at Warehouse.
     * 
     * @param orderEntry
     * @param warehouse
     * @param quantity
     * @return Boolean True/False
     */
    boolean isOrderEntryInStockAtWarehouse(final AbstractOrderEntryModel orderEntry,
            final WarehouseModel warehouse, Long quantity);

    /**
     * Transfers reserve amount from one warehouse to another warehouse. Logs the comment using consignmentCode.
     * 
     * @param product
     *            the product
     * @param quantity
     *            how much reserve amount to transfer from one warehouse to another
     * @param warehouseFrom
     *            warehouse from which the amount will be transfered
     * @param warehouseTo
     *            warehouse to which the amount will be transfered
     * @param consignmentCode
     *            passing consignmentCode only for logging
     * 
     */
    void transferReserveAmount(ProductModel product, int quantity, WarehouseModel warehouseFrom,
            WarehouseModel warehouseTo, String consignmentCode);

    /**
     * Reserves the product
     * 
     * @param product
     *            the product
     * 
     * @param amount
     *            the amount of the reservation
     * @param comment
     *            the comment for the reservation
     * @throws InsufficientStockLevelException
     *             if not enough products are available in the stock
     */
    void reserve(ProductModel product, int amount, String comment)
            throws InsufficientStockLevelException;

    /**
     * Releases the product
     * 
     * @param product
     *            the product
     * 
     * @param amount
     *            the amount of the release
     * @param comment
     *            the comment for the release
     */
    void release(ProductModel product, int amount, String comment);

    /**
     * Update the soh for the product if there is Stock level available. Preventing the creation of new stock level, If
     * there is no Stock level found for the product
     * 
     * @param product
     *            the product
     * @param warehouse
     *            the warehouse
     * @param amount
     *            the actual amount of the product
     * @param comment
     *            the comment for updating soh
     */
    void updateStockLevel(ProductModel product,
            WarehouseModel warehouse, int amount, String comment) throws StockLevelNotFoundException;

    /**
     * @param product
     *            the product
     * @param warehouse
     *            the warehouse
     * @return the stock level for the product in the warehouse
     */
    StockLevelModel checkAndGetStockLevel(ProductModel product, WarehouseModel warehouse);

    /**
     * Returns the stock availability for item codes in a warehouse. If the warehouse is passed in as null, it will use
     * the default warehouse
     * 
     * @param itemCodes
     *            the list of sellable variant codes.
     * @param warehouseCode
     *            the warehouse warehouse code
     * @return a map which has available qty for each product in the warehouse passed
     */
    Map<String, Integer> getStockForProductsInWarehouse(List<String> itemCodes, String warehouseCode);

    /**
     * Finds the stock level of the specified product from the preOrder warehouses and calculates the actual available
     * amount.
     * 
     * @param product
     *            - the product
     * @return actual available amount for the specific product
     */
    int getStockLevelAmountFromPreOrderWarehouses(ProductModel product);


    /**
     * @param currentQuantity
     * @param warehouseModel
     */
    void updateQuantityFulfilledByWarehouse(int currentQuantity, WarehouseModel warehouseModel);

    /**
     * get status of preOrder products
     * 
     * @param product
     * @param warehouses
     */
    StockLevelStatus getPreOrderProductStatus(final ProductModel product, final Collection<WarehouseModel> warehouses);

    /**
     * get stock status of the product. Method will determine whether to invoke the normal product stock data or
     * preOrder product stock data.
     * 
     * @param product
     * @return StockData which has both amount and status
     */
    StockData getStockLevelAndStatusAmount(ProductModel product);

    /**
     * Method will return stock level status of the product.
     * 
     * @param product
     * @return stock level status
     */
    StockLevelStatus getProductStatus(final ProductModel product);
}

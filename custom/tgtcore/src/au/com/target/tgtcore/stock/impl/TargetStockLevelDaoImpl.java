/**
 * 
 */
package au.com.target.tgtcore.stock.impl;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.stock.impl.DefaultStockLevelDao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.stock.TargetStockLevelDao;
import au.com.target.tgtcore.warehouse.dao.TargetWarehouseDao;


/**
 * 
 */
public class TargetStockLevelDaoImpl extends DefaultStockLevelDao implements TargetStockLevelDao {

    private static final Logger LOG = Logger.getLogger(TargetStockLevelDaoImpl.class);
    private TransactionTemplate transactionTemplate;
    private TypeService typeService;
    private StockLevelColumns stockLevelColumns;
    private WarehouseColumns warehouseColumns;
    private JdbcTemplate jdbcTemplate;
    private TargetFeatureSwitchService targetFeatureSwitchService;
    private TargetWarehouseDao targetWarehouseDao;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.stock.TargetStockLevelDao#adjustActualAmount(de.hybris.platform.ordersplitting.model.StockLevelModel, int)
     */
    @Override
    public void adjustActualAmount(final StockLevelModel stockLevel, final int adjust) {
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(final TransactionStatus arg0) {
                try {
                    final int rows = runAdjustStockLevelJdbcQuery(adjust, stockLevel);
                    if (rows > 1) {
                        throw new IllegalStateException("more stock level rows found for the update: ["
                                + stockLevel.getPk() + "]");
                    }
                }
                catch (final DataAccessException dae) {
                    throw new SystemException(dae);
                }
            }
        });
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.stock.TargetStockLevelDao#transferReserveToActualAmount(de.hybris.platform.ordersplitting.model.StockLevelModel, int)
     */
    @Override
    public void transferReserveToActualAmount(final StockLevelModel stockLevel, final int adjust) {
        final Integer rows = transactionTemplate.execute(new TransactionCallback<Integer>() {
            @Override
            public Integer doInTransaction(final TransactionStatus arg0) {
                try {
                    final int rows = runTransferJdbcQuery(adjust, stockLevel);
                    return Integer.valueOf(rows);

                }
                catch (final DataAccessException dae) {
                    throw new SystemException(dae);
                }
            }
        });
        final Integer afterRelease = peekStockLevelReserved(rows.intValue(), stockLevel.getPk(),
                "more rows found for the release: [", false);
        //we don't allow the negative value of the reserve quantity,set the negative reserved quantity to 0.
        adjustReservedStockToZero(afterRelease, stockLevel);
    }

    // differs from the default in that it will NOT reset the reserve value to zero
    @Override
    public void updateActualAmount(final StockLevelModel stockLevel, final int actualAmount) {
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(final TransactionStatus arg0) {
                try {
                    final int rows = runUpdateStockLevelJdbcQuery(actualAmount, stockLevel);
                    if (rows > 1) {
                        throw new IllegalStateException("more stock level rows found for the update: ["
                                + stockLevel.getPk() + "]");
                    }
                }
                catch (final DataAccessException dae) {
                    throw new SystemException(dae);
                }
            }
        });
    }

    @Override
    public Integer reserve(final StockLevelModel stockLevel, final int amount) {
        return this.reserve(stockLevel, amount, false);
    }

    // overridden from default to include modified timestamp
    @Override
    public Integer reserve(final StockLevelModel stockLevel, final int amount, final boolean preOrderProduct) {
        final Integer rows = transactionTemplate.execute(new TransactionCallback<Integer>() {
            @Override
            public Integer doInTransaction(final TransactionStatus status) {
                try {
                    return Integer.valueOf(reserveStock(stockLevel, amount, preOrderProduct));
                }
                catch (final DataAccessException dae) {
                    throw new SystemException(dae);
                }
            }
        });

        final Integer afterReserved = peekStockLevelReserved(rows.intValue(), stockLevel.getPk(),
                "more rows found for the reservation: [", preOrderProduct);
        return afterReserved;

    }

    /**
     * @param stockLevel
     * @param amount
     * @return rows
     */
    protected int reserveStock(final StockLevelModel stockLevel, final int amount) {
        return reserveStock(stockLevel, amount, false);
    }

    /**
     * @param stockLevel
     * @param amount
     * @param preOrderProduct
     * @return rows
     */
    protected int reserveStock(final StockLevelModel stockLevel, final int amount, final boolean preOrderProduct) {

        final InStockStatus inStockStatus = stockLevel.getInStockStatus();
        String reserveQuery = null;
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FASTLINE_FALCON) && !preOrderProduct) {
            reserveQuery = assembleReservationWarehouseReserveStockLevelQuery(stockLevel);
        }
        else {
            reserveQuery = assembleReserveStockLevelQuery(inStockStatus, preOrderProduct);
        }
        final Integer amountObj = Integer.valueOf(amount);
        final Long pkObj = Long.valueOf(stockLevel.getPk().getLongValue());
        final Date modifiedDate = new Date();
        final int rows;
        if (InStockStatus.FORCEINSTOCK == inStockStatus) {
            rows = jdbcTemplate.update(reserveQuery, amountObj, modifiedDate, pkObj);
        }
        else {
            rows = jdbcTemplate.update(reserveQuery, amountObj, modifiedDate, pkObj, amountObj);
        }
        return rows;
    }

    /**
     * @param stockLevel
     * @return query
     */
    private String assembleReservationWarehouseReserveStockLevelQuery(final StockLevelModel stockLevel) {
        prepareStockLevelColumns();
        final StringBuilder query = new StringBuilder("WITH TMP_STOCK_RESERVE AS (");
        query.append(" SELECT " + stockLevelColumns.productCode + ", ");
        query.append(" SUM(" + stockLevelColumns.availableCol + ") + SUM (" + stockLevelColumns.oversellingCol
                + ") - SUM(" + stockLevelColumns.reservedCol + ") AS stockpos ");
        query.append(" FROM " + stockLevelColumns.tableName + " WHERE " + stockLevelColumns.productCode + "='"
                + stockLevel.getProductCode() + "'");
        query.append(" AND " + stockLevelColumns.warehouse);
        query.append(" IN (" + getWarehousePks(targetWarehouseDao.getWarehouses(stockLevel.getProductCode())) + ")");
        query.append(" GROUP BY " + stockLevelColumns.productCode + " )");
        query.append(" UPDATE " + stockLevelColumns.tableName);
        query.append(" SET " + stockLevelColumns.reservedCol + " = " + stockLevelColumns.reservedCol + " + ?, ");
        query.append("  " + stockLevelColumns.modifiedDateCol + " = ?");
        query.append(" FROM TMP_STOCK_RESERVE tmpStock ");
        query.append(" WHERE " + stockLevelColumns.tableName + "." + stockLevelColumns.productCode
                + "=tmpStock.p_productcode ");
        query.append(" AND " + stockLevelColumns.tableName + "." + stockLevelColumns.pkCol + "= ?");
        query.append(" AND tmpStock.stockpos >= ?");

        return query.toString();
    }

    /**
     * @param warehouses
     * @return comma separated PKs
     */
    private String getWarehousePks(final List<WarehouseModel> warehouses) {
        final List<Long> pkLists = new ArrayList<>();
        for (final WarehouseModel whModel : warehouses) {
            pkLists.add(whModel.getPk().getLong());
        }
        return StringUtils.join(pkLists, ",");
    }

    // overridden from default to include modified timestamp
    @Override
    public Integer release(final StockLevelModel stockLevel, final int amount) {
        return release(stockLevel, amount, false);
    }

    @Override
    public Integer release(final StockLevelModel stockLevel, final int amount, final boolean preOrderProduct) {
        final Integer rows = transactionTemplate.execute(new TransactionCallback<Integer>() {
            @Override
            public Integer doInTransaction(final TransactionStatus status) {
                try {
                    final int rows = runJdbcQuery(assembleReleaseStockLevelQuery(preOrderProduct), amount, stockLevel);

                    return Integer.valueOf(rows);
                }
                catch (final DataAccessException dae) {
                    throw new SystemException(dae);
                }
            }
        });
        final Integer afterRelease = peekStockLevelReserved(rows.intValue(), stockLevel.getPk(),
                "more rows found for the release: [", preOrderProduct);
        //we don't allow the negative value of the reserve quantity,adjust the negative reserved quantity to 0.
        return adjustReservedStockToZero(afterRelease, stockLevel);
    }

    /**
     * Assembles the request query for adjustment the current available and reset the reserved to zero.
     */
    private String assembleAdjustStockLevelQuery() {
        prepareStockLevelColumns();
        final StringBuilder query = new StringBuilder("UPDATE " + stockLevelColumns.tableName);
        query.append(" SET " + stockLevelColumns.availableCol + " = " + stockLevelColumns.availableCol + " + ?,");
        query.append("     " + stockLevelColumns.modifiedDateCol + " = ?");
        query.append(" WHERE " + stockLevelColumns.pkCol + " = ?");
        return query.toString();
    }

    private int runAdjustStockLevelJdbcQuery(final int amount, final StockLevelModel stockLevel) {
        final Integer amountObj = Integer.valueOf(amount);
        final Long pkObj = Long.valueOf(stockLevel.getPk().getLongValue());
        final int rows;
        final Date modifiedDate = new Date();
        rows = jdbcTemplate.update(assembleAdjustStockLevelQuery(), amountObj, modifiedDate, pkObj);
        return rows;
    }

    /**
     * Assembles the request query for adjustment the current available and reset the reserved to zero.
     */
    private String assembleTransferQuery() {
        prepareStockLevelColumns();
        final StringBuilder query = new StringBuilder("UPDATE " + stockLevelColumns.tableName);
        query.append(" SET " + stockLevelColumns.reservedCol + " = " + stockLevelColumns.reservedCol + " - ?, ");
        query.append("     " + stockLevelColumns.availableCol + " = " + stockLevelColumns.availableCol + " - ?, ");
        query.append("     " + stockLevelColumns.modifiedDateCol + " = ?");
        query.append(" WHERE " + stockLevelColumns.pkCol + " = ?");
        return query.toString();
    }


    private int runTransferJdbcQuery(final int amount, final StockLevelModel stockLevel) {
        final Integer amountObj = Integer.valueOf(amount);
        final Long pkObj = Long.valueOf(stockLevel.getPk().getLongValue());
        final int rows;
        final Date modifiedDate = new Date();
        rows = jdbcTemplate.update(assembleTransferQuery(), amountObj, amountObj, modifiedDate, pkObj);
        return rows;
    }

    /**
     * Assembles the request query for update the current available and reset the reserved to zero.
     */
    private String assembleUpdateStockLevelQuery() {
        prepareStockLevelColumns();
        final StringBuilder query = new StringBuilder("UPDATE " + stockLevelColumns.tableName);
        query.append(" SET " + stockLevelColumns.availableCol + " = ?, ");
        query.append("     " + stockLevelColumns.modifiedDateCol + " = ?");
        query.append(" WHERE " + stockLevelColumns.pkCol + " = ?");
        return query.toString();
    }

    private int runUpdateStockLevelJdbcQuery(final int amount, final StockLevelModel stockLevel) {
        final Integer amountObj = Integer.valueOf(amount);
        final Long pkObj = Long.valueOf(stockLevel.getPk().getLongValue());
        final int rows;
        final Date modifiedDate = new Date();
        rows = jdbcTemplate.update(assembleUpdateStockLevelQuery(), amountObj, modifiedDate, pkObj);
        return rows;
    }


    private void prepareStockLevelColumns() {
        if (this.stockLevelColumns == null) {
            this.stockLevelColumns = new StockLevelColumns(typeService);
        }
    }

    private void prepareWarehouseColumns() {
        if (this.warehouseColumns == null) {
            this.warehouseColumns = new WarehouseColumns(typeService);
        }
    }

    /**
     * Assembles the request query for reserved amount.
     */
    private String assembleRequestStockLevelQuery(final boolean preOrderProduct) {
        if (preOrderProduct) {
            return assembleRequestPreOrderStockLevelQuery();
        }
        return assembleRequestNormalStockLevelQuery();
    }

    private String assembleRequestNormalStockLevelQuery() {
        prepareStockLevelColumns();
        final StringBuilder query = new StringBuilder("SELECT " + stockLevelColumns.reservedCol);
        query.append(" FROM " + stockLevelColumns.tableName + " WHERE " + stockLevelColumns.pkCol + "=?");
        return query.toString();
    }

    /**
     * Assembles the request query for reserved amount.
     */
    private String assembleRequestPreOrderStockLevelQuery() {
        prepareStockLevelColumns();
        final StringBuilder query = new StringBuilder("SELECT " + stockLevelColumns.preOrderCol);
        query.append(" FROM " + stockLevelColumns.tableName + " WHERE " + stockLevelColumns.pkCol + "=?");
        return query.toString();
    }

    /**
     * Assembles the request query for release amount.
     * 
     * @param preOrderProduct
     */
    private String assembleReleaseStockLevelQuery(final boolean preOrderProduct) {
        if (preOrderProduct) {
            return assembleReleasePreOrderStockLevelQuery();
        }
        return assembleReleaseNormalStockLevelQuery();
    }



    /**
     * Return the release query for a pre-order product.
     * 
     * @return query
     */
    private String assembleReleasePreOrderStockLevelQuery() {
        prepareStockLevelColumns();
        final StringBuilder query = new StringBuilder("UPDATE " + stockLevelColumns.tableName);
        query.append(" SET " + stockLevelColumns.preOrderCol + " = " + stockLevelColumns.preOrderCol + " - ?, ");
        query.append("     " + stockLevelColumns.modifiedDateCol + " = ?");
        query.append(" WHERE " + stockLevelColumns.pkCol + "=?");
        return query.toString();
    }

    /**
     * Return the release query for a normal product.
     * 
     * @return query
     */
    private String assembleReleaseNormalStockLevelQuery() {
        prepareStockLevelColumns();
        final StringBuilder query = new StringBuilder("UPDATE " + stockLevelColumns.tableName);
        query.append(" SET " + stockLevelColumns.reservedCol + " = " + stockLevelColumns.reservedCol + " - ?, ");
        query.append("     " + stockLevelColumns.modifiedDateCol + " = ?");
        query.append(" WHERE " + stockLevelColumns.pkCol + "=?");
        return query.toString();
    }

    /**
     * Assembles the UPDATE reservation query.
     */
    private String assembleReserveStockLevelQuery(final InStockStatus inStockStatus, final boolean preOrderProduct) {
        if (preOrderProduct) {
            return assemblePreOrderReserveStockLevelQuery(inStockStatus);
        }
        return assembleNormalReserveStockLevelQuery(inStockStatus);
    }

    /**
     * Assembles the UPDATE reservation query for normal product.
     */
    private String assembleNormalReserveStockLevelQuery(final InStockStatus inStockStatus) {
        prepareStockLevelColumns();
        final StringBuilder query = new StringBuilder("UPDATE " + stockLevelColumns.tableName);
        query.append(" SET " + stockLevelColumns.reservedCol + " = " + stockLevelColumns.reservedCol + " + ?, ");
        query.append("     " + stockLevelColumns.modifiedDateCol + " = ?");
        query.append(" WHERE " + stockLevelColumns.pkCol + "=?");

        if (InStockStatus.FORCEINSTOCK != inStockStatus) {
            query.append(" AND " + stockLevelColumns.availableCol + " + " + stockLevelColumns.oversellingCol + " - "
                    + stockLevelColumns.reservedCol + " >= ? ");
        }

        return query.toString();
    }

    /**
     * Assembles the UPDATE reservation query for PreOrder product.
     */
    private String assemblePreOrderReserveStockLevelQuery(final InStockStatus inStockStatus) {
        prepareStockLevelColumns();
        final StringBuilder query = new StringBuilder("UPDATE " + stockLevelColumns.tableName);
        query.append(" SET " + stockLevelColumns.preOrderCol + " = " + stockLevelColumns.preOrderCol + " + ?, ");
        query.append("     " + stockLevelColumns.modifiedDateCol + " = ?");
        query.append(" WHERE " + stockLevelColumns.pkCol + "=?");

        if (InStockStatus.FORCEINSTOCK != inStockStatus) {
            query.append(" AND " + stockLevelColumns.maxPreOrderCol + " - "
                    + stockLevelColumns.preOrderCol + " >= ? ");
        }

        return query.toString();
    }

    private int runJdbcQuery(final String query, final int amount, final StockLevelModel stockLevel) {
        final Integer amountObj = Integer.valueOf(amount);
        final Long pkObj = Long.valueOf(stockLevel.getPk().getLongValue());
        final int rows;
        final Date modifiedDate = new Date();
        rows = jdbcTemplate.update(query, amountObj, modifiedDate, pkObj);
        return rows;
    }

    private String createStockLookupQueryForProductsInWarehouse(final String warehouseCode,
            final List<String> itemCodes) {
        final StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT " + stockLevelColumns.productCode + " as productCode, ("
                + stockLevelColumns.availableCol + "-" + stockLevelColumns.reservedCol + ") as availableQty");
        queryString
                .append(" FROM " + stockLevelColumns.tableName + " as stck, " + warehouseColumns.tableName + " as wh ");
        queryString.append(" WHERE stck." + stockLevelColumns.warehouse + " = wh." + warehouseColumns.pkCol + " ");
        if (StringUtils.isEmpty(warehouseCode)) {
            queryString.append(" AND wh." + warehouseColumns.isDefault + " = 1 ");
        }
        else {
            queryString.append(" AND wh." + warehouseColumns.warehouseCode + " = '" + warehouseCode + "' ");
        }

        queryString.append(" AND stck." + stockLevelColumns.productCode + " IN (");
        int counter = 1;
        for (final String itemCode : itemCodes) {
            queryString.append("'" + itemCode + "'");
            if (counter != itemCodes.size()) {
                queryString.append(",");
            }
            counter++;
        }
        queryString.append(")");
        return queryString.toString();
    }

    @Override
    public Map<String, Integer> findStockForProductCodesAndWarehouse(final List<String> itemCodes,
            final String warehouseCode) {
        prepareStockLevelColumns();
        prepareWarehouseColumns();
        final List<Map<String, Object>> rows = jdbcTemplate.queryForList(
                createStockLookupQueryForProductsInWarehouse(warehouseCode, itemCodes));
        if (CollectionUtils.isEmpty(rows)) {
            return Collections.EMPTY_MAP;
        }
        final Map<String, Integer> stockLevelMap = new HashMap<String, Integer>();
        for (final Map<String, Object> row : rows) {
            final Object productCode = row.get("productCode");
            final Object availQty = row.get("availableQty");
            if (productCode == null || availQty == null) {
                continue;
            }
            stockLevelMap.put(StringUtils.lowerCase((String)productCode), (Integer)availQty);
        }
        return stockLevelMap;
    }


    private String assembleForceReserveStockLevelQuery() {
        prepareStockLevelColumns();
        final StringBuilder query = new StringBuilder("UPDATE " + stockLevelColumns.tableName);
        query.append(" SET " + stockLevelColumns.reservedCol + " = " + stockLevelColumns.reservedCol + " + ?, ");
        query.append("     " + stockLevelColumns.modifiedDateCol + " = ?");
        query.append(" WHERE " + stockLevelColumns.pkCol + "=?");
        return query.toString();
    }

    /**
     * get the current reserved stock level
     * 
     * @param preOrderProduct
     *            YTODO
     */
    private Integer peekStockLevelReserved(final int rows, final PK pk, final String message,
            final boolean preOrderProduct) {

        return transactionTemplate.execute(new TransactionCallback<Integer>() {
            @Override
            public Integer doInTransaction(final TransactionStatus status) {
                try {



                    final Object[] pkargs = new Object[] { Long.valueOf(pk.getLongValue()) };
                    int currentReserved = -1;
                    if (rows == 1) {
                        //get current reserved value via another JDBC query
                        final String requestAmountQuery = assembleRequestStockLevelQuery(preOrderProduct);
                        currentReserved = getCurrentReservedValue(pkargs, requestAmountQuery);
                    }
                    else if (rows > 1) {
                        throw new IllegalStateException(message + rows + "] rows for stock level [" + pkargs[0] + "]");
                    }
                    return rows == 1 ? Integer.valueOf(currentReserved) : null;

                }
                catch (final DataAccessException dae) {
                    throw new SystemException(dae);
                }
            }
        });

    }

    /**
     * @param pkargs
     * @param requestAmountQuery
     * @return reserved Value
     */
    private int getCurrentReservedValue(final Object[] pkargs, final String requestAmountQuery) {
        final Integer reservedValue = jdbcTemplate.queryForObject(requestAmountQuery, pkargs, Integer.class);
        return ((reservedValue != null) ? reservedValue.intValue() : 0);
    }

    private class StockLevelColumns {
        private final String tableName;
        private final String pkCol;
        private final String reservedCol;
        private final String availableCol;
        private final String modifiedDateCol;
        private final String oversellingCol;
        private final String maxPreOrderCol;
        private final String productCode;
        private final String warehouse;
        private final String preOrderCol;

        private StockLevelColumns(final TypeService typeService) {
            final ComposedTypeModel stockLevelType = typeService.getComposedTypeForClass(StockLevelModel.class);
            tableName = stockLevelType.getTable();
            pkCol = typeService.getAttributeDescriptor(stockLevelType, StockLevelModel.PK).getDatabaseColumn();
            reservedCol = typeService.getAttributeDescriptor(stockLevelType, StockLevelModel.RESERVED)
                    .getDatabaseColumn();
            availableCol = typeService.getAttributeDescriptor(stockLevelType, StockLevelModel.AVAILABLE)
                    .getDatabaseColumn();
            modifiedDateCol = typeService.getAttributeDescriptor(stockLevelType, StockLevelModel.MODIFIEDTIME)
                    .getDatabaseColumn();
            oversellingCol = typeService.getAttributeDescriptor(stockLevelType, StockLevelModel.OVERSELLING)
                    .getDatabaseColumn();
            productCode = typeService.getAttributeDescriptor(stockLevelType, StockLevelModel.PRODUCTCODE)
                    .getDatabaseColumn();
            warehouse = typeService.getAttributeDescriptor(stockLevelType, StockLevelModel.WAREHOUSE)
                    .getDatabaseColumn();
            maxPreOrderCol = typeService.getAttributeDescriptor(stockLevelType, StockLevelModel.MAXPREORDER)
                    .getDatabaseColumn();
            preOrderCol = typeService.getAttributeDescriptor(stockLevelType, StockLevelModel.PREORDER)
                    .getDatabaseColumn();
        }

    }

    private class WarehouseColumns {
        private final String tableName;
        private final String pkCol;
        private final String isDefault;
        private final String warehouseCode;
        private final String totalQtyFulfilledAtFastlineToday;

        private WarehouseColumns(final TypeService typeService) {
            final ComposedTypeModel warehouseModelType = typeService.getComposedTypeForClass(WarehouseModel.class);
            tableName = warehouseModelType.getTable();
            pkCol = typeService.getAttributeDescriptor(warehouseModelType, WarehouseModel.PK).getDatabaseColumn();
            isDefault = typeService.getAttributeDescriptor(warehouseModelType, WarehouseModel.DEFAULT)
                    .getDatabaseColumn();
            warehouseCode = typeService.getAttributeDescriptor(warehouseModelType, WarehouseModel.CODE)
                    .getDatabaseColumn();
            totalQtyFulfilledAtFastlineToday = typeService
                    .getAttributeDescriptor(warehouseModelType, WarehouseModel.TOTALQTYFULFILLEDATFASTLINETODAY)
                    .getDatabaseColumn();
        }
    }

    /**
     * adjust the reserved stock to zero, if reserved stock go negative after release.
     * 
     * @param afterRelease
     * @param stockLevel
     * @return reserved stock after adjustment
     */
    private Integer adjustReservedStockToZero(final Integer afterRelease, final StockLevelModel stockLevel) {
        if (afterRelease != null && afterRelease.intValue() < 0) {
            LOG.error("Reserved quantity has negative value " + afterRelease.intValue()
                    + " after release the reserved quantity, try to adjust the reserved quantity to 0");
            final int rows = runJdbcQuery(assembleReleaseStockLevelQuery(false), afterRelease.intValue(), stockLevel);
            return peekStockLevelReserved(rows, stockLevel.getPk(),
                    "more rows found for the release: [", false);
        }
        return afterRelease;
    }

    @Required
    @Override
    public void setTransactionTemplate(final TransactionTemplate transactionTemplate) {
        this.transactionTemplate = transactionTemplate;
        super.setTransactionTemplate(transactionTemplate);
    }

    @Required
    @Override
    public void setTypeService(final TypeService typeService) {
        this.typeService = typeService;
        super.setTypeService(typeService);
    }

    @Required
    @Override
    public void setJdbcTemplate(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        super.setJdbcTemplate(jdbcTemplate);
    }

    @Override
    public Integer forceReserve(final StockLevelModel stockLevel, final int amount) {
        final Integer rows = transactionTemplate.execute(new TransactionCallback<Integer>() {
            @Override
            public Integer doInTransaction(final TransactionStatus status) {
                try {
                    final String reserveQuery = assembleForceReserveStockLevelQuery();
                    final Date modifiedDate = new Date();
                    final Integer amt = Integer.valueOf(amount);
                    final Long pk = Long.valueOf(stockLevel.getPk().getLongValue());
                    final int rows;
                    rows = jdbcTemplate.update(reserveQuery, amt, modifiedDate, pk);
                    return Integer.valueOf(rows);
                }
                catch (final DataAccessException dae) {
                    throw new SystemException(dae);
                }
            }
        });

        final Integer afterReserved = peekStockLevelReserved(rows.intValue(), stockLevel.getPk(),
                "more rows found for the reservation: [", false);
        return afterReserved;

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.stock.TargetStockLevelDao#updateQuantityFulfilledByWarehouse(int,warehouseModel)
     */
    @Override
    public void updateQuantityFulfilledByWarehouse(final int currentQuantity, final WarehouseModel warehouseModel) {

        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(final TransactionStatus arg0) {
                try {
                    final String updateTotalQuantityQuery = getTotalQuantityQuery(currentQuantity,
                            warehouseModel.getCode());
                    final int rows = jdbcTemplate.update(updateTotalQuantityQuery);
                    if (rows > 1) {
                        throw new IllegalStateException("more Warehosue found to update the quantity: ["
                                + currentQuantity + "]");
                    }
                }
                catch (final DataAccessException dae) {
                    throw new SystemException(dae);
                }
            }
        });

    }

    private String getTotalQuantityQuery(final int currentQuantity, final String warehouseCode) {
        prepareWarehouseColumns();
        final StringBuilder query = new StringBuilder("UPDATE " + warehouseColumns.tableName);
        query.append(" SET " + warehouseColumns.totalQtyFulfilledAtFastlineToday + "="
                + warehouseColumns.totalQtyFulfilledAtFastlineToday + "+" + currentQuantity);
        query.append(" WHERE " + warehouseColumns.warehouseCode + "='" + warehouseCode + "'");
        return query.toString();
    }



    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * @param targetWarehouseDao
     *            the targetWarehouseDao to set
     */
    @Required
    public void setTargetWarehouseDao(final TargetWarehouseDao targetWarehouseDao) {
        this.targetWarehouseDao = targetWarehouseDao;
    }

}

/**
 * 
 */
package au.com.target.tgtcore.stock.impl;


import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.basecommerce.enums.StockLevelUpdateType;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.daos.WarehouseDao;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.stock.exception.StockLevelNotFoundException;
import de.hybris.platform.stock.impl.DefaultStockService;
import de.hybris.platform.stock.model.StockLevelHistoryEntryModel;
import de.hybris.platform.stock.strategy.StockLevelProductStrategy;
import de.hybris.platform.util.Utilities;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.stock.TargetStockLevelDao;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.stock.strategies.impl.PreOrderStockLevelStatusStrategy;
import au.com.target.tgtcore.util.ProductUtil;
import au.com.target.tgtcore.warehouse.dao.TargetWarehouseDao;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtutility.util.TargetProductUtils;



/**
 * @author dcwillia
 * 
 */
public class TargetStockServiceImpl extends DefaultStockService implements TargetStockService {

    private static final Logger LOG = Logger.getLogger(TargetStockServiceImpl.class);

    private TargetStockLevelDao targetStockLevelDao;
    private StockLevelProductStrategy stockLevelProductStrategy;
    private TargetWarehouseDao targetWarehouseDao;
    private TargetWarehouseService targetWarehouseService;
    private TargetFeatureSwitchService targetFeatureSwitchService;
    private PreOrderStockLevelStatusStrategy preOrderStockLevelStatusStrategy;

    private int defaultLowStockThreshold;

    private TargetSharedConfigService targetSharedConfigService;



    @Override
    public void adjustActualAmount(final ProductModel product, final WarehouseModel warehouse, final int adjust,
            final String comment) {
        ServicesUtil.validateParameterNotNullStandardMessage("product", product);
        ServicesUtil.validateParameterNotNullStandardMessage("warehouse", warehouse);
        final StockLevelModel stockLevel;
        stockLevel = this.checkAndGetStockLevel(product, warehouse);

        try {
            targetStockLevelDao.adjustActualAmount(stockLevel, adjust);
            clearCacheForItem(stockLevel);
            createStockLevelHistoryEntry(stockLevel, StockLevelUpdateType.WAREHOUSE, stockLevel.getReserved(), comment);
        }
        catch (final Exception e) {
            LOG.error("update not successful: " + e.getMessage());
            throw new SystemException(e);
        }
    }

    @Override
    public void transferReserveToActualAmount(final ProductModel product, final WarehouseModel warehouse,
            final int adjust, final String comment) {
        ServicesUtil.validateParameterNotNullStandardMessage("product", product);
        ServicesUtil.validateParameterNotNullStandardMessage("warehouse", warehouse);
        final StockLevelModel stockLevel;
        stockLevel = this.checkAndGetStockLevel(product, warehouse);

        try {
            targetStockLevelDao.transferReserveToActualAmount(stockLevel, adjust);
            clearCacheForItem(stockLevel);
            createStockLevelHistoryEntry(stockLevel, StockLevelUpdateType.WAREHOUSE, stockLevel.getReserved(), comment);
        }
        catch (final Exception e) {
            LOG.error("update not successful: " + e.getMessage());
            throw new SystemException(e);
        }
    }

    @Override
    public void forceReserve(final ProductModel product, final WarehouseModel warehouse, final int amount,
            final String comment) {
        final StockLevelModel currentStockLevel = checkAndGetStockLevel(product, warehouse);
        final Integer reserved = this.targetStockLevelDao.forceReserve(currentStockLevel, amount);
        clearCacheForItem(currentStockLevel);
        if (null != reserved) {
            createStockLevelHistoryEntry(currentStockLevel, StockLevelUpdateType.CUSTOMER_RESERVE, reserved.intValue(),
                    comment);
        }
        else {
            createStockLevelHistoryEntry(currentStockLevel, StockLevelUpdateType.CUSTOMER_RESERVE,
                    currentStockLevel.getReserved(),
                    comment + " warn: more than one rows of stock level updated for the reservation");
        }
    }

    /**
     * Checks the stock level status of the product from the online warehouse. If there is no online warehouse will
     * return OUTOFSTOCK
     * 
     * @param product
     * @return stock level status
     */
    private StockLevelStatus getProductStatusFromOnlineWarehouses(final ProductModel product) {
        Assert.notNull(product);
        logAccessForStock(product.getCode(), "getProductStatusFromOnlineWarehouses");
        final List<WarehouseModel> warehouses = targetWarehouseDao.getSurfaceStockOnlineWarehouses();
        return getProductStatus(product, warehouses);
    }

    /**
     * Checks the stock level status of the product from the pre-order warehouse. If there is no pre-order warehouse
     * will return OUTOFSTOCK
     *
     * @param product
     * @return stock level status
     */
    private StockLevelStatus getProductStatusFromPreOrderWarehouses(final ProductModel product) {
        Assert.notNull(product);
        logAccessForStock(product.getCode(), "getProductStatusFromPreOrderWarehouses");
        final List<WarehouseModel> warehouses = targetWarehouseDao.getPreOrderWarehouses();
        return getPreOrderProductStatus(product, warehouses);
    }

    /**
     * @param product
     * @param warehouses
     * @return StockLevelStatus
     */
    @Override
    public StockLevelStatus getProductStatus(final ProductModel product, final Collection<WarehouseModel> warehouses) {


        if (CollectionUtils.isEmpty(warehouses)) {
            return StockLevelStatus.OUTOFSTOCK;
        }

        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)
                && !ProductUtil.isProductTypeDigital(product) && !ProductUtil.isProductTypePhysicalGiftcard(product)
                && !TargetProductUtils.isProductOpenForPreOrder(product)) {
            return getProductStatusForMultipleWarehouses(product, warehouses);
        }

        if (warehouses.size() == 1) {
            return getProductStatus(product, warehouses.iterator().next());
        }

        boolean foundLowStock = false;

        for (final WarehouseModel warehouse : warehouses) {
            final StockLevelStatus status = getProductStatus(product, warehouse);
            if (StockLevelStatus.INSTOCK == status) {

                return status;
            }

            if (StockLevelStatus.LOWSTOCK == status) {

                foundLowStock = true;
            }
        }

        if (foundLowStock) {
            return StockLevelStatus.LOWSTOCK;
        }

        return StockLevelStatus.OUTOFSTOCK;
    }

    /**
     * Method return stock level status from pre-order warehouses if the product is open for pre-order or else return
     * stock level status from online warehouses.
     * 
     * @param product
     * @return stock level status
     */
    @Override
    public StockLevelStatus getProductStatus(final ProductModel product) {
        if (TargetProductUtils.isProductOpenForPreOrder(product)) {
            return getProductStatusFromPreOrderWarehouses(product);
        }
        return getProductStatusFromOnlineWarehouses(product);
    }


    private StockLevelStatus getProductStatusForMultipleWarehouses(final ProductModel product,
            final Collection<WarehouseModel> warehouses) {

        final int calculatedStockLevel = calculateTotalActualAmount(this.getStockLevels(product, warehouses));
        if (calculatedStockLevel > defaultLowStockThreshold) {
            return StockLevelStatus.INSTOCK;

        }
        else if (calculatedStockLevel != 0 && calculatedStockLevel <= defaultLowStockThreshold) {
            return StockLevelStatus.LOWSTOCK;
        }
        return StockLevelStatus.OUTOFSTOCK;
    }

    @Override
    public StockLevelStatus getPreOrderProductStatus(final ProductModel product,
            final Collection<WarehouseModel> warehouses) {

        if (CollectionUtils.isEmpty(warehouses)) {
            return StockLevelStatus.OUTOFSTOCK;
        }

        if (warehouses.size() == 1) {
            return getPreOrderProductStatus(product, warehouses.iterator().next());
        }

        for (final WarehouseModel warehouse : warehouses) {
            final StockLevelStatus status = getPreOrderProductStatus(product, warehouse);
            if (StockLevelStatus.INSTOCK == status) {

                return StockLevelStatus.INSTOCK;
            }

        }
        return StockLevelStatus.OUTOFSTOCK;

    }

    private StockLevelStatus getPreOrderProductStatus(final ProductModel product, final WarehouseModel warehouse) {
        final StockLevelModel stockLevel = this.targetStockLevelDao
                .findStockLevel(this.stockLevelProductStrategy.convert(product), warehouse);
        final StockLevelStatus status = this.preOrderStockLevelStatusStrategy.checkStatus(stockLevel);
        return StockLevelStatus.LOWSTOCK == status ? StockLevelStatus.INSTOCK : status;
    }

    @Override
    public int getStockLevelAmountFromOnlineWarehouses(final ProductModel product) {
        Assert.notNull(product);

        logAccessForStock(product.getCode(), "getStockLevelAmountFromOnlineWarehouses");

        //if the product is embargoed then there wont be any normal stock.
        if (TargetProductUtils.isProductEmbargoed(product)) {
            return 0;
        }

        final List<WarehouseModel> warehouses = targetWarehouseDao.getSurfaceStockOnlineWarehouses();
        if (CollectionUtils.isEmpty(warehouses)) {
            return 0;
        }
        return calculateTotalActualAmount(this.getStockLevels(product, warehouses));
    }

    /**
     * @param actualAmount
     * @return availableStockFulfillByFastline
     */
    private int availableStockFulfillByFastline(int actualAmount) {
        final int maxQtyFastlineFulfillPerDay = targetSharedConfigService
                .getInt(TgtCoreConstants.Config.MAX_QTY_FASTLINE_FULFILLED_PER_DAY, 100);
        final int totalQtyFulfilledAtFastlineToday = targetWarehouseService.getDefaultOnlineWarehouse()
                .getTotalQtyFulfilledAtFastlineToday();
        int availableQtyFulfilledByFastline = maxQtyFastlineFulfillPerDay - totalQtyFulfilledAtFastlineToday;
        availableQtyFulfilledByFastline = availableQtyFulfilledByFastline > 0 ? availableQtyFulfilledByFastline : 0;
        actualAmount = availableQtyFulfilledByFastline > actualAmount ? actualAmount
                : availableQtyFulfilledByFastline;

        return actualAmount;
    }

    @Override
    public StockData getStockLevelAndStatusAmount(final ProductModel product) {

        if (TargetProductUtils.isProductOpenForPreOrder(product)) {
            return getStockLevelAndStatusAmountFromPreOrderWarehouses(product);
        }
        return getStockLevelAndStatusAmountFromOnlineWarehouses(product);
    }

    /**
     * Finds the stock level of the preOrder product from the preOrder warehouses and calculates the actual available
     * amount and sets the status of the stock
     * 
     * @param product
     * @return StockData which has both amount and status
     */
    private StockData getStockLevelAndStatusAmountFromPreOrderWarehouses(final ProductModel product) {
        Assert.notNull(product, "Product cannot be null");

        logAccessForStock(product.getCode(), "getStockLevelAndStatusAmountFromPreOrderWarehouses");

        final StockData stockInWarehouses = new StockData();

        final List<WarehouseModel> warehouses = targetWarehouseDao.getPreOrderWarehouses();

        if (CollectionUtils.isEmpty(warehouses)) {
            stockInWarehouses.setStockLevel(Long.valueOf(0L));
        }
        else {

            stockInWarehouses.setStockLevel(
                    Long.valueOf(calculateTotalPreOrderActualAmount(this.getStockLevels(product, warehouses))));
        }

        stockInWarehouses.setStockLevelStatus(getPreOrderProductStatus(product, warehouses));

        return stockInWarehouses;
    }

    /**
     * Finds the stock level of the specified product from the online warehouse and calculates the actual available
     * amount and sets the status of the stock
     * 
     * @param product
     * @return StockData which has both amount and status
     */
    private StockData getStockLevelAndStatusAmountFromOnlineWarehouses(final ProductModel product) {
        Assert.notNull(product, "Product cannot be null");

        logAccessForStock(product.getCode(), "getStockLevelAndStatusAmountFromOnlineWarehouses");

        final StockData stockInWarehouses = new StockData();

        //if the product is embargoed then there wont be any normal stock.
        if (TargetProductUtils.isProductEmbargoed(product)) {
            stockInWarehouses.setStockLevel(Long.valueOf(0L));

            //TO DO Correct the status to OUTOFSTOCK along with the PDF ui story.
            stockInWarehouses.setStockLevelStatus(StockLevelStatus.INSTOCK);
            return stockInWarehouses;
        }


        final List<WarehouseModel> warehouses = targetWarehouseDao.getSurfaceStockOnlineWarehouses();

        if (CollectionUtils.isEmpty(warehouses)) {
            stockInWarehouses.setStockLevel(Long.valueOf(0L));
        }
        else {

            stockInWarehouses.setStockLevel(Long.valueOf(Integer.valueOf(
                    calculateTotalActualAmount(this.getStockLevels(product, warehouses))).longValue()));
        }

        stockInWarehouses.setStockLevelStatus(getProductStatus(product, warehouses));

        return stockInWarehouses;
    }

    private void logAccessForStock(final String productCode, final String methodName) {

        if (LOG.isDebugEnabled()) {
            LOG.debug("STOCKCALL: ThreadId=" + Thread.currentThread().getId() + " - Method=" + methodName
                    + " - productcode="
                    + productCode);
        }
    }

    protected int calculateTotalActualAmount(final Collection<StockLevelModel> stockLevels) {
        int totalActualAmount = 0;
        if (CollectionUtils.isNotEmpty(stockLevels)) {
            for (final StockLevelModel stockLevel : stockLevels) {
                int actualAmount = 0;
                if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)
                        && TgtCoreConstants.FASTLINE_WAREHOUSE.equals(stockLevel.getWarehouse().getCode())) {
                    actualAmount = availableStockFulfillByFastline(
                            stockLevel.getAvailable() - stockLevel.getReserved());
                }
                else {
                    actualAmount = stockLevel.getAvailable() - stockLevel.getReserved();
                    if (((actualAmount <= 0) && (stockLevel.isTreatNegativeAsZero()))
                            || InStockStatus.FORCEOUTOFSTOCK.equals(stockLevel.getInStockStatus())) {
                        continue;
                    }

                }

                totalActualAmount += actualAmount;

            }
        }
        return totalActualAmount < 0 ? 0 : totalActualAmount;
    }


    @Override
    public StockLevelModel checkAndGetStockLevel(final ProductModel product, final WarehouseModel warehouse) {

        logAccessForStock(product.getCode(), "getStockLevelAndStatusAmountFromOnlineWarehouses");

        final StockLevelModel stockLevel = targetStockLevelDao
                .findStockLevel(stockLevelProductStrategy.convert(product), warehouse);
        if (stockLevel == null) {
            throw new StockLevelNotFoundException("no stock level for product [" + product + "] in warehouse ["
                    + warehouse.getName() + "] found.");
        }
        return stockLevel;
    }


    /**
     * Clear cache on all clusters then refresh our local copy of the stock level.
     * 
     * @param stockLevel
     */
    protected void clearCacheForItem(final StockLevelModel stockLevel) {
        Utilities.invalidateCache(stockLevel.getPk());
        getModelService().refresh(stockLevel);
    }

    /**
     * Create a stock history entry with the given details.
     * 
     * @param stockLevel
     * @param updateType
     * @param reserved
     * @param comment
     * @return the new stock history entry
     */
    protected StockLevelHistoryEntryModel createStockLevelHistoryEntry(final StockLevelModel stockLevel,
            final StockLevelUpdateType updateType, final int reserved, final String comment) {
        return createStockLevelHistoryEntry(stockLevel, updateType, reserved, comment, false);
    }

    /**
     * Create a stock history entry with the given details.
     * 
     * @param stockLevel
     * @param updateType
     * @param reserved
     * @param comment
     * @param preOrderProduct
     * @return the new stock history entry
     */
    protected StockLevelHistoryEntryModel createStockLevelHistoryEntry(final StockLevelModel stockLevel,
            final StockLevelUpdateType updateType, final int reserved, final String comment,
            final boolean preOrderProduct) {
        final StockLevelHistoryEntryModel historyEntry = getModelService().create(StockLevelHistoryEntryModel.class);
        historyEntry.setStockLevel(stockLevel);
        historyEntry.setActual(stockLevel.getAvailable());
        historyEntry.setPreOrderQuantity(stockLevel.getMaxPreOrder());
        if (preOrderProduct) {
            historyEntry.setReservedPreOrderQuantity(reserved);
            // preserve historic data if any for reserved
            historyEntry.setReserved(stockLevel.getReserved());
        }
        else {
            historyEntry.setReserved(reserved);
            // preserve historic data of PreOrder if any
            historyEntry.setReservedPreOrderQuantity(stockLevel.getPreOrder());
        }
        historyEntry.setUpdateType(updateType);
        if (comment != null) {
            historyEntry.setComment(comment);
        }
        historyEntry.setUpdateDate(new Date());
        getModelService().save(historyEntry);
        return historyEntry;
    }

    @Override
    public boolean isOrderEntryInStockAtWarehouse(final AbstractOrderEntryModel orderEntry,
            final WarehouseModel warehouse) {

        Assert.notNull(orderEntry, "Order Entry can not be null");


        if (null == orderEntry.getProduct() || null == orderEntry.getQuantity()) {
            LOG.warn("Product or Quantity is missing in order entry.");
            return false;
        }

        logAccessForStock(orderEntry.getProduct().getCode(), "isOrderEntryInStockAtWarehouse");

        try {
            // Warehouses like incomm have infinite stock modelled by FORCEINSTOCK
            final InStockStatus instockStatus = getInStockStatus(orderEntry.getProduct(), warehouse);
            if (instockStatus != null && instockStatus.equals(InStockStatus.FORCEINSTOCK)) {
                return true;
            }

            // Otherwise check available amount against requested quantity
            final int availableStock = getStockLevelAmount(orderEntry.getProduct(), warehouse);
            if (availableStock < orderEntry.getQuantity().intValue()) {
                return false;
            }
        }
        catch (final StockLevelNotFoundException e) {
            // no stock level found for current entry's product, return false
            return false;
        }
        return true;
    }

    @Override
    public boolean isOrderEntryInStockAtWarehouse(final AbstractOrderEntryModel orderEntry,
            final WarehouseModel warehouse, final Long quantity) {

        Assert.notNull(orderEntry, "Order Entry can not be null");


        if (null == orderEntry.getProduct() || null == orderEntry.getQuantity()) {
            LOG.warn("Product or Quantity is missing in order entry.");
            return false;
        }

        logAccessForStock(orderEntry.getProduct().getCode(), "isOrderEntryInStockAtWarehouse");

        try {
            // Warehouses like incomm have infinite stock modelled by FORCEINSTOCK
            final InStockStatus instockStatus = getInStockStatus(orderEntry.getProduct(), warehouse);
            if (instockStatus != null && instockStatus.equals(InStockStatus.FORCEINSTOCK)) {
                return true;
            }

            // Otherwise check available amount against requested quantity
            final int availableStock = getStockLevelAmount(orderEntry.getProduct(), warehouse);
            if (availableStock < quantity.intValue()) {
                return false;
            }
        }
        catch (final StockLevelNotFoundException e) {
            // no stock level found for current entry's product, return false
            return false;
        }
        return true;
    }

    /**
     * Transfers reserve amount from one warehouse to another warehouse
     * 
     * @param product
     *            the product
     * @param quantity
     *            how much reserve amount to transfer from one warehouse to another
     * @param warehouseFrom
     *            warehouse from which the amount will be transfered
     * @param warehouseTo
     *            warehouse to which the amount will be transfered
     */
    @Override
    public void transferReserveAmount(final ProductModel product, final int quantity,
            final WarehouseModel warehouseFrom,
            final WarehouseModel warehouseTo, final String consignmentCode) {
        try {

            reserve(product, warehouseTo, quantity, "Reserving stock in " + warehouseTo.getName()
                    + " for consignment : " + consignmentCode);
            release(product, warehouseFrom, quantity, "Releasing stock from " + warehouseFrom.getName()
                    + " for consignment : " + consignmentCode);
        }
        catch (final InsufficientStockLevelException e) {
            LOG.error("Fulfilment: Error Transferring reserve amount from " + warehouseFrom + " to " + warehouseTo
                    + ", consignment=" + consignmentCode, e);
        }
    }

    @Override
    public void reserve(final ProductModel product, final int amount, final String comment)
            throws InsufficientStockLevelException {
        if (amount <= 0) {
            throw new IllegalArgumentException("amount must be greater than zero.");
        }
        final Collection<StockLevelModel> stockLevels = getAllStockLevels(product);
        if (CollectionUtils.isEmpty(stockLevels)) {
            throw new InsufficientStockLevelException("No stock in warehouses for product:" + product.getCode());
        }

        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FASTLINE_FALCON)
                && !TargetProductUtils.isProductOpenForPreOrder(product)) {
            for (final StockLevelModel stockLevel : stockLevels) {
                final WarehouseModel assignedWarehouse = stockLevel.getWarehouse();
                if (targetWarehouseService.isStockAdjustmentRequired(assignedWarehouse)
                        && TgtCoreConstants.STOCK_RESERVATION_WAREHOUSE.equals(assignedWarehouse.getCode())) {
                    reserveStock(product, stockLevel, amount, comment);
                    break;
                }
                LOG.info("Not reserving stock in non default warehouse=" + assignedWarehouse.getCode()
                        + " for product=" + product.getCode());
            }
        }
        else {
            for (final StockLevelModel stockLevel : stockLevels) {
                final WarehouseModel assignedWarehouse = stockLevel.getWarehouse();
                if (targetWarehouseService.isStockAdjustmentRequired(assignedWarehouse)
                        && !TgtCoreConstants.STOCK_RESERVATION_WAREHOUSE.equals(assignedWarehouse.getCode())) {
                    reserveStock(product, stockLevel, amount, comment);
                    break;
                }
                LOG.info("Not reserving stock in non default warehouse=" + assignedWarehouse.getCode()
                        + " for product=" + product.getCode());

            }
        }
    }

    /**
     * Copy of the super class method(reserve) since it calls private method which needs modification.
     * 
     * @param product
     * @param stockLevel
     * @param amount
     * @param comment
     * @throws InsufficientStockLevelException
     */
    private void reserveStock(final ProductModel product, final StockLevelModel stockLevel, final int amount,
            final String comment)
            throws InsufficientStockLevelException {
        final boolean preOrderProduct = TargetProductUtils.isProductOpenForPreOrder(product);
        final Integer reserved = this.targetStockLevelDao.reserve(stockLevel, amount, preOrderProduct);
        if (reserved == null) {
            throw new InsufficientStockLevelException(
                    "insufficient available amount for stock level [" + stockLevel.getPk() + "]");
        }

        clearCacheForItem(stockLevel);
        createStockLevelHistoryEntry(stockLevel, StockLevelUpdateType.CUSTOMER_RESERVE,
                reserved.intValue(), comment, preOrderProduct);
    }

    @Override
    public void release(final ProductModel product, final int amount, final String comment) {

        if (amount <= 0) {
            throw new IllegalArgumentException("amount must be greater than zero.");
        }

        final Collection<StockLevelModel> stockLevels = getAllStockLevels(product);
        if (CollectionUtils.isNotEmpty(stockLevels)) {
            if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FASTLINE_FALCON)
                    && !TargetProductUtils.isProductOpenForPreOrder(product)) {
                for (final StockLevelModel stockLevel : stockLevels) {
                    final WarehouseModel assignedWarehouse = stockLevel.getWarehouse();
                    if (targetWarehouseService.isStockAdjustmentRequired(assignedWarehouse)
                            && TgtCoreConstants.STOCK_RESERVATION_WAREHOUSE.equals(assignedWarehouse.getCode())) {
                        releaseStock(stockLevel, amount, comment, TargetProductUtils.isProductOpenForPreOrder(product));
                        break;
                    }
                    LOG.info("Not releasing stock in non default warehouse=" + assignedWarehouse.getCode()
                            + " for product=" + product.getCode());
                }
            }
            else {
                for (final StockLevelModel stockLevel : stockLevels) {
                    final WarehouseModel assignedWarehouse = stockLevel.getWarehouse();
                    if (targetWarehouseService.isStockAdjustmentRequired(assignedWarehouse)
                            && !TgtCoreConstants.STOCK_RESERVATION_WAREHOUSE.equals(assignedWarehouse.getCode())) {
                        releaseStock(stockLevel, amount, comment, TargetProductUtils.isProductOpenForPreOrder(product));
                        break;
                    }
                    LOG.info("Not releasing stock in non default warehouse=" + assignedWarehouse.getCode()
                            + " for product=" + product.getCode());
                }
            }
        }
    }

    /**
     * Copy of super class 'release' method
     * 
     * @param stockLevel
     * @param amount
     * @param comment
     */
    private void releaseStock(final StockLevelModel stockLevel, final int amount, final String comment,
            final boolean preOrderProduct) {
        final Integer reserved = this.targetStockLevelDao.release(stockLevel, amount, preOrderProduct);
        if (reserved == null) {
            throw new SystemException("release failed for stock level [" + stockLevel.getPk() + "]!");
        }

        clearCacheForItem(stockLevel);
        createStockLevelHistoryEntry(stockLevel, StockLevelUpdateType.CUSTOMER_RELEASE,
                reserved.intValue(), comment, preOrderProduct);
    }

    @Override
    public void updateStockLevel(final ProductModel product, final WarehouseModel warehouse,
            final int actualAmount,
            final String comment) {
        final StockLevelModel stockLevel;

        stockLevel = checkAndGetStockLevel(product, warehouse);

        try {
            final int amount;
            if (actualAmount < 0) {
                amount = 0;
                LOG.warn("actual amount is negative for product= " + product.getCode() + " in warehouse= "
                        + warehouse.getCode() + ", changing amount to 0");
            }
            else {
                amount = actualAmount;
            }
            targetStockLevelDao.updateActualAmount(stockLevel, amount);
            clearCacheForItem(stockLevel);
            createStockLevelHistoryEntry(stockLevel,
                    StockLevelUpdateType.WAREHOUSE, 0, comment);
        }
        catch (final Exception e) {
            LOG.error("update not successful for product= " + product.getCode() + " in warehouse= "
                    + warehouse.getCode() + e.getMessage());
            throw new SystemException(e);
        }
    }

    @Override
    public Map<String, Integer> getStockForProductsInWarehouse(final List<String> itemCodes,
            final String warehouseCode) {
        return targetStockLevelDao.findStockForProductCodesAndWarehouse(itemCodes, warehouseCode);
    }

    @Override
    public int getStockLevelAmountFromPreOrderWarehouses(final ProductModel product) {

        Assert.notNull(product);

        if (!TargetProductUtils.isProductOpenForPreOrder(product)) {
            return 0;
        }

        logAccessForStock(product.getCode(), "getStockLevelAmountFromPreOrderWarehouses");

        final List<WarehouseModel> warehouses = targetWarehouseDao.getPreOrderWarehouses();
        if (CollectionUtils.isEmpty(warehouses)) {
            return 0;
        }
        return calculateTotalPreOrderActualAmount(this.getStockLevels(product, warehouses));
    }

    private int calculateTotalPreOrderActualAmount(final Collection<StockLevelModel> stockLevels) {
        int totalpreOrderActualAmount = 0;
        if (CollectionUtils.isNotEmpty(stockLevels)) {
            for (final StockLevelModel stockLevel : stockLevels) {
                final int actualPreOrderAmount = stockLevel.getMaxPreOrder() - stockLevel.getPreOrder();
                if (actualPreOrderAmount <= 0 && stockLevel.isTreatNegativeAsZero()) {
                    continue;
                }
                totalpreOrderActualAmount += actualPreOrderAmount;
            }
        }

        return totalpreOrderActualAmount;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.stock.TargetStockService#updateQuantityFulfilledByWarehouse(int,warehouseModel)
     */
    @Override
    public void updateQuantityFulfilledByWarehouse(final int currentQuantity, final WarehouseModel warehouseModel) {
        targetStockLevelDao.updateQuantityFulfilledByWarehouse(currentQuantity, warehouseModel);
    }

    @Override
    public int getStockLevelAmount(final ProductModel product, final WarehouseModel warehouse) {
        int amount = 0;
        try {
            amount = super.getStockLevelAmount(product, warehouse);
            if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)
                    && TgtCoreConstants.FASTLINE_WAREHOUSE.equals(warehouse.getCode())) {
                amount = availableStockFulfillByFastline(amount);
            }
        }
        catch (final StockLevelNotFoundException stockLevelNotFoundException) {
            LOG.error("No stock level for product [" + product + "] in warehouse [" + warehouse.getName() + "] found.");
        }
        return amount;
    }

    @Required
    public void setTargetStockLevelDao(final TargetStockLevelDao targetStockLevelDao) {
        this.targetStockLevelDao = targetStockLevelDao;
        super.setStockLevelDao(targetStockLevelDao);
    }

    /**
     * @param stockLevelProductStrategy
     *            the stockLevelProductStrategy to set
     */
    @Required
    @Override
    public void setStockLevelProductStrategy(final StockLevelProductStrategy stockLevelProductStrategy) {
        this.stockLevelProductStrategy = stockLevelProductStrategy;
        super.setStockLevelProductStrategy(stockLevelProductStrategy);
    }

    /**
     * @return the targetWarehouseDao
     */
    protected WarehouseDao getWarehouseDao() {
        return targetWarehouseDao;
    }

    /**
     * @param targetWarehouseDao
     *            the targetWarehouseDao to set
     */
    @Required
    public void setTargetWarehouseDao(final TargetWarehouseDao targetWarehouseDao) {
        this.targetWarehouseDao = targetWarehouseDao;
    }

    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */
    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }



    /**
     * @param defaultLowStockThreshold
     *            the defaultLowStockThreshold to set
     */
    @Required
    public void setDefaultLowStockThreshold(final int defaultLowStockThreshold) {
        this.defaultLowStockThreshold = defaultLowStockThreshold;
    }

    /**
     * @param targetSharedConfigService
     *            the targetSharedConfigService to set
     */
    @Required
    public void setTargetSharedConfigService(final TargetSharedConfigService targetSharedConfigService) {
        this.targetSharedConfigService = targetSharedConfigService;
    }

    /**
     * @param preOrderStockLevelStatusStrategy
     *            the perOrderStockLevelStatusStrategy to set
     */
    @Required
    public void setPreOrderStockLevelStatusStrategy(
            final PreOrderStockLevelStatusStrategy preOrderStockLevelStatusStrategy) {
        this.preOrderStockLevelStatusStrategy = preOrderStockLevelStatusStrategy;
    }

}

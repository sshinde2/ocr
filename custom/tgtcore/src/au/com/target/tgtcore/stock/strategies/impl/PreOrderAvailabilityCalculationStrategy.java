package au.com.target.tgtcore.stock.strategies.impl;

import de.hybris.platform.commerceservices.stock.strategies.impl.DefaultCommerceAvailabilityCalculationStrategy;
import de.hybris.platform.ordersplitting.model.StockLevelModel;


/**
 * availability of preorder product. re-use the existing hybris implementation by overriding the calculation.
 * 
 * @author gsing236
 *
 */
public class PreOrderAvailabilityCalculationStrategy extends DefaultCommerceAvailabilityCalculationStrategy {

    @Override
    protected long getAvailableToSellQuantity(final StockLevelModel stockLevel) {
        return stockLevel.getMaxPreOrder() - stockLevel.getPreOrder();
    }
}


/**
 * 
 */
package au.com.target.tgtcore.storefinder;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.StoreFinderService;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Collection;


/**
 * @author bhuang3
 *
 */
public interface TargetStoreFinderService<ITEM extends PointOfServiceDistanceData, RESULT extends StoreFinderSearchPageData<ITEM>>
        extends StoreFinderService<ITEM, StoreFinderSearchPageData<ITEM>> {

    /**
     * do search from the collection of the PointOfServiceModel
     *
     * @param locationText
     * @param centerPoint
     * @param pageableData
     * @param pointOfServiceModelList
     * @return StoreFinderSearchPageData<ITEM>
     */
    public StoreFinderSearchPageData<ITEM> doSearch(final String locationText,
            final GeoPoint centerPoint, final PageableData pageableData,
            final Collection<PointOfServiceModel> pointOfServiceModelList);

    /**
     * do search by location from the collection of the PointOfServiceModel
     * 
     * @param locationText
     * @param pageableData
     * @param baseStore
     * @param pointOfServiceModelList
     * @return StoreFinderSearchPageData<ITEM>
     */
    public StoreFinderSearchPageData<ITEM> doSearchByLocation(final BaseStoreModel baseStore,
            final String locationText,
            final PageableData pageableData,
            final Collection<PointOfServiceModel> pointOfServiceModelList);

    /**
     * do search by location and return with lookup postcode/suburb/state.
     *
     * @param baseStore
     * @param locationText
     * @param pageableData
     * @return StoreFinderSearchPageData<ITEM>
     */
    public StoreFinderSearchPageData<ITEM> doSearchByLocation(BaseStoreModel baseStore, String locationText,
            PageableData pageableData);

    /**
     * do search by location and return with lookup postcode/suburb/state.
     *
     * @param baseStore
     * @param locationText
     * @param pageableData
     * @return StoreFinderSearchPageData<ITEM>
     */
    public StoreFinderSearchPageData<ITEM> doSearchClickAndCollectStoresByLocation(BaseStoreModel baseStore,
            String locationText,
            PageableData pageableData);

    /**
     * search with look up for delivery location information.
     * @param locationText
     * @param centerPoint
     * @param pageableData
     * @return StoreFinderSearchPageData<ITEM>
     */
    public StoreFinderSearchPageData<ITEM> doSearchNearestClickAndCollectLocation(String locationText,
            GeoPoint centerPoint, PageableData pageableData);

}

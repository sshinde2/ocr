/**
 * 
 */
package au.com.target.tgtcore.storefinder.helpers;

import de.hybris.platform.basecommerce.enums.DistanceUnit;
import de.hybris.platform.commerceservices.storefinder.helpers.impl.DefaultDistanceHelper;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.beans.factory.annotation.Required;


/**
 * @author rmcalave
 *
 */
public class TargetRoundingDistanceHelper extends DefaultDistanceHelper {

    private BigDecimal decimalLimit;

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.storefinder.helpers.impl.DefaultDistanceHelper#getDistance(de.hybris.platform.basecommerce.enums.DistanceUnit, double)
     */
    @Override
    protected Double getDistance(final DistanceUnit distanceUnit, final double distanceInKm) {
        final Double distance = super.getDistance(distanceUnit, distanceInKm);

        if (distance.doubleValue() < decimalLimit.doubleValue()) {
            return distance;
        }

        final BigDecimal distanceBigDecimal = BigDecimal.valueOf(distance.doubleValue());
        return Double.valueOf(distanceBigDecimal.setScale(0, RoundingMode.HALF_UP).doubleValue());
    }

    /**
     * @param decimalLimit
     *            the decimalLimit to set
     */
    @Required
    public void setDecimalLimit(final BigDecimal decimalLimit) {
        this.decimalLimit = decimalLimit;
    }

}

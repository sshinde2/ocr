/**
 *
 */
package au.com.target.tgtcore.storelocator;

import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.Date;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.model.TargetOpeningDayModel;


/**
 * @author Pradeep
 *
 */
public interface TargetOpeningDayService {


    /**
     * To generate Custom unique Opening Day Id
     *
     * @param storeNumber
     * @param openingDate
     * @return targetOpening Id
     */
    String getTargetOpeningDayId(String storeNumber, Date openingDate);

    /**
     * Get the opening day for the given id.
     * 
     * @param schedule
     *            the opening schedule
     * @param id
     *            the Target opening day ID
     * @return The OpeningDayModel
     * @throws TargetAmbiguousIdentifierException
     */
    TargetOpeningDayModel getTargetOpeningDay(OpeningScheduleModel schedule, String id)
            throws TargetAmbiguousIdentifierException;
}

/**
 * 
 */
package au.com.target.tgtcore.storelocator.dao;

import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.model.TargetOpeningDayModel;



/**
 * @author rmcalave
 *
 */
public interface TargetOpeningDayDao {

    /**
     * Find Target opening day by opening schedule and id.
     * 
     * @param id
     *            The id
     * @param schedule
     *            The schedule
     * @return The Target opening day, or null if not found
     * @throws TargetAmbiguousIdentifierException
     */
    TargetOpeningDayModel findTargetOpeningDayByScheduleAndId(OpeningScheduleModel schedule, String id)
            throws TargetAmbiguousIdentifierException;

}
/**
 * 
 */
package au.com.target.tgtcore.storelocator.impl;

import de.hybris.platform.storelocator.GeolocationResponseParser;
import de.hybris.platform.storelocator.data.MapLocationData;
import de.hybris.platform.storelocator.exception.GeoDocumentParsingException;

import java.io.IOException;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.springframework.http.client.ClientHttpResponse;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import au.com.target.tgtcore.storelocator.data.TargetMapLocationData;


/**
 * @author htan3
 *
 */
public class TargetFirstPlacemarkWinsGoogleResponseParser implements
        GeolocationResponseParser<MapLocationData> {

    private static final Logger LOG = Logger.getLogger(TargetFirstPlacemarkWinsGoogleResponseParser.class);

    @Deprecated
    @Override
    @SuppressWarnings("deprecation")
    public MapLocationData parseResponseDocument(final Document response) {
        throw new UnsupportedOperationException(
                "Method no longer needed, will be removed in next release");
    }

    @Override
    public MapLocationData extractData(final ClientHttpResponse response)
            throws IOException {
        final TargetMapLocationData locationData = new TargetMapLocationData();
        final XPath xpath = XPathFactory.newInstance().newXPath();
        final InputSource inputSource = new InputSource(response.getBody());
        LOG.info("INFO-GOOGLEAPI-UNMARSHAL : Unmarshalling the results from the api search");
        try {
            final Node root = (Node)xpath.evaluate("/", inputSource,
                    XPathConstants.NODE);

            final String lat = xpath.evaluate(
                    "/GeocodeResponse/result[1]/geometry/location/lat", root);
            final String longitude = xpath.evaluate(
                    "/GeocodeResponse/result[1]/geometry/location/lng", root);
            locationData.setLatitude(lat);
            locationData.setLongitude(longitude);
            locationData.setLocality(xpath.evaluate(
                    "/GeocodeResponse/result[1]/address_component[type='locality']/short_name", root));
            locationData.setPostcode(xpath.evaluate(
                    "/GeocodeResponse/result[1]/address_component[type='postal_code']/short_name", root));
            locationData
                    .setState(xpath
                            .evaluate(
                                    "/GeocodeResponse/result[1]/address_component[type='administrative_area_level_1']/short_name",
                                    root));
        }
        catch (final XPathExpressionException e) {
            throw new GeoDocumentParsingException(e.getMessage());
        }
        return locationData;
    }
}

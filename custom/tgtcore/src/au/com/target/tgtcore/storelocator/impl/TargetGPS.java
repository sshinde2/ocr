/**
 * 
 */
package au.com.target.tgtcore.storelocator.impl;

import de.hybris.platform.storelocator.GPS;
import de.hybris.platform.storelocator.constants.GeolocationMaths;
import de.hybris.platform.storelocator.constants.GeolocationUtils;
import de.hybris.platform.storelocator.exception.GeoLocatorException;

import java.io.Serializable;
import java.text.DecimalFormat;


/**
 * @author htan3
 *
 */
public class TargetGPS implements GPS, Serializable {
    static final long serialVersionUID = 999153875285740631L;
    private double latitude;
    private double longitude;
    private int[] latitudeDMS;
    private int[] longitudeDMS;
    private String locality;
    private String postcode;
    private String state;

    public TargetGPS() {
    }

    public TargetGPS(final double latitude, final double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public TargetGPS(final int[] latitudeDMS, final int[] longitudeDMS) {
        this.latitudeDMS = latitudeDMS;
        this.longitudeDMS = longitudeDMS;
    }

    @Override
    public TargetGPS create(final String lat, final String lng)
            throws GeoLocatorException {
        if ((GeolocationUtils.validateDMSLatitude(lat))
                && (GeolocationUtils.validateDMSLongitude(lng))) {
            final TargetGPS gps = new TargetGPS(
                    GeolocationUtils.separateDMS(lat),
                    GeolocationUtils.separateDMS(lng));
            gps.latitude = GeolocationMaths.dms2Decimal(gps.latitudeDMS);
            gps.longitude = GeolocationMaths.dms2Decimal(gps.longitudeDMS);
            return gps;
        }
        return null;
    }

    @Override
    public TargetGPS create(final double lat, final double lng)
            throws GeoLocatorException {
        if ((GeolocationUtils.validateLatitude(lat))
                && (GeolocationUtils.validateLongitude(lng))) {
            final TargetGPS gps = new TargetGPS(lat, lng);
            gps.latitudeDMS = GeolocationMaths.decimal2DMS(lat);
            gps.longitudeDMS = GeolocationMaths.decimal2DMS(lng);
            return gps;
        }
        return null;
    }

    @Override
    public double getDecimalLatitude() {
        return this.latitude;
    }

    @Override
    public double getDecimalLongitude() {
        return this.longitude;
    }

    @Override
    public String toDMSString() {
        return String.format(
                "(%1$d\u00b0%2$d'%3$d\"%4$s, %5$d\u00b0%6$d'%7$d\"%8$s)",
                Integer.valueOf(Math.abs(this.latitudeDMS[0])),
                Integer.valueOf(this.latitudeDMS[1]),
                Integer.valueOf(this.latitudeDMS[2]),
                (this.latitudeDMS[0] > 0) ? "N" : "S",
                Integer.valueOf(Math.abs(this.longitudeDMS[0])),
                Integer.valueOf(this.longitudeDMS[1]),
                Integer.valueOf(this.longitudeDMS[2]),
                (this.longitudeDMS[0] > 0) ? "E" : "W");
    }

    @Override
    public String toString() {
        final DecimalFormat format = new DecimalFormat(".######");
        return "(" + format.format(this.latitude) + ", "
                + format.format(this.longitude) + ")";
    }

    @Override
    public String toGeocodeServiceFormat() {
        final DecimalFormat format = new DecimalFormat(".######");
        return format.format(this.latitude).replace(',', '.') + ", "
                + format.format(this.longitude).replace(',', '.');
    }

    /**
     * @return the locality
     */
    public String getLocality() {
        return locality;
    }

    /**
     * @param locality
     *            the locality to set
     */
    public void setLocality(final String locality) {
        this.locality = locality;
    }

    /**
     * @return the postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * @param postcode
     *            the postcode to set
     */
    public void setPostcode(final String postcode) {
        this.postcode = postcode;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(final String state) {
        this.state = state;
    }


}

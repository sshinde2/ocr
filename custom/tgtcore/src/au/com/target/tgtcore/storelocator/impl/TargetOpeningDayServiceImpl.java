/**
 *
 */
package au.com.target.tgtcore.storelocator.impl;


import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.model.TargetOpeningDayModel;
import au.com.target.tgtcore.storelocator.TargetOpeningDayService;
import au.com.target.tgtcore.storelocator.dao.TargetOpeningDayDao;
import au.com.target.tgtutility.util.TargetDateUtil;


/**
 * @author Pradeep
 *
 */
public class TargetOpeningDayServiceImpl implements TargetOpeningDayService {

    private static final String OPENING_DATE_FORMAT = "dd-MM-yyyy";
    private TargetOpeningDayDao targetOpeningDayDao;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.storelocator.TargetOpeningDayService#getTargetOpeningDayId(String,Date)
     */
    @Override
    public String getTargetOpeningDayId(final String storeNumber, final Date openingDate) {
        Validate.notNull(openingDate, "Openingdate Should Not be Null");
        Validate.isTrue(StringUtils.isNotBlank(storeNumber), "Store Number should Not be Empty");
        return storeNumber + '-' + TargetDateUtil.getDateAsString(openingDate, OPENING_DATE_FORMAT);
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtcore.storelocator.TargetOpeningDayService#getTargetOpeningDay(de.hybris.platform.storelocator.model.OpeningScheduleModel, java.lang.String)
     */
    @Override
    public TargetOpeningDayModel getTargetOpeningDay(final OpeningScheduleModel schedule, final String id)
            throws TargetAmbiguousIdentifierException {
        Validate.notNull(schedule, "schedule must not be null");
        Validate.notEmpty(id, "id must contain a value");

        return targetOpeningDayDao.findTargetOpeningDayByScheduleAndId(schedule, id);
    }

    /**
     * @param targetOpeningDayDao
     *            the targetOpeningDayDao to set
     */
    @Required
    public void setTargetOpeningDayDao(final TargetOpeningDayDao targetOpeningDayDao) {
        this.targetOpeningDayDao = targetOpeningDayDao;
    }

}

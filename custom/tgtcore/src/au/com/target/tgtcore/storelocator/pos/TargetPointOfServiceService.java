/**
 * 
 */
package au.com.target.tgtcore.storelocator.pos;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.storelocator.pos.PointOfServiceService;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Table;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;


/**
 * Service to retrieve TargetPointOfService
 * 
 */
public interface TargetPointOfServiceService extends PointOfServiceService {
    /**
     * get Target PointOfService By StoreNumber
     * 
     * @param storeNumber
     * @throws TargetUnknownIdentifierException
     *             if no result return
     * @throws TargetAmbiguousIdentifierException
     *             if more than one result return
     * @return TargetPointOfServiceModel
     */
    TargetPointOfServiceModel getPOSByStoreNumber(Integer storeNumber) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException;


    /**
     * get states and their corresponding stores based on product types from the cart
     * 
     * @param cartModel
     * @return map of states and their stores
     */
    Map<String, Set<TargetPointOfServiceModel>> getStateAndStoresForCart(AbstractOrderModel cartModel);

    /**
     * get stores based on product types from the cart
     * 
     * @param cartModel
     * @return list of stores
     */
    List<TargetPointOfServiceModel> getStoresForCart(AbstractOrderModel cartModel);

    /**
     * get all stores
     * 
     * @return list of stores
     */
    Map<String, Set<TargetPointOfServiceModel>> getAllStateAndStores();

    /**
     * get all stores as list
     * 
     * @return list of stores
     */
    List<TargetPointOfServiceModel> getAllOpenStores();

    /**
     * check if the target point of service supports all the product in the cart.
     * 
     * @param pointOfService
     * @param cartModel
     * @return true or false
     */
    boolean isSupportAllProductTypesInCart(TargetPointOfServiceModel pointOfService, AbstractOrderModel cartModel);

    /**
     * check if the given store number exists in the system.
     * 
     * @param storeNumber
     * @return true or false
     */
    boolean validateStoreNumber(Integer storeNumber);

    /**
     * get the quantity of products pending fulfilment at a point of service.
     * 
     * @param storeNumber
     * @param product
     * @return quantity
     */
    int getFulfilmentPendingQuantity(Integer storeNumber, ProductModel product);

    /**
     * get the quantity of products pending fulfilment at a point of service.
     * 
     * @param storeNumber
     * @param productCode
     * @return quantity
     */
    int getFulfilmentPendingQuantityByProductCode(final Integer storeNumber, final String productCode);

    /**
     * get the quantity of products pending fulfilment in the list POS.
     * 
     * @param storeNumbers
     * @param products
     * @return quantity for each product and store.
     */
    Table<Integer, String, Long> getFulfilmentPendingQuantityByProductCodeForStores(final List<String> storeNumbers,
            final List<ProductModel> products);

    /**
     * get all stores
     * 
     * @return {@link TargetPointOfServiceModel}
     */
    List<TargetPointOfServiceModel> getAllStoresForFluent();

    /**
     * get warehouse by store number
     * 
     * @param storeNumber
     * @return warehouse model
     */
    WarehouseModel getWarehouseByStoreNumber(final Integer storeNumber) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException;

    /**
     * get tpos of default warehouse
     * 
     * @return TargetPointOfServiceModel
     */
    TargetPointOfServiceModel getTposOfDefaultWarehouse();
}

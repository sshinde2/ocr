/**
 * 
 */
package au.com.target.tgtcore.storelocator.pos.dao.impl;


import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.impl.DefaultPointOfServiceDao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.dao.TargetPointOfServiceDao;
import au.com.target.tgtcore.util.TargetServicesUtil;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;



/**
 * Default implementation of {@link TargetPointOfServiceDao}
 * 
 */
public class TargetPointOfServiceDaoImpl extends DefaultPointOfServiceDao implements TargetPointOfServiceDao {

    /**
     * 
     */
    private static final String VIRTUAL_POS = "virtualPOS";

    private static final Logger LOG = Logger.getLogger(TargetPointOfServiceDaoImpl.class);

    private static final String POS_SELECT_PREFIX = "SELECT {" + TargetPointOfServiceModel.PK + "} FROM {"
            + TargetPointOfServiceModel._TYPECODE + "}";

    private static final String POS_WHERE_CLOSED_SUFFIX = " AND {" + TargetPointOfServiceModel.CLOSED + "} = ?closed";

    private static final String QUERY_GET_POS_BY_STORE_NUMBER = POS_SELECT_PREFIX + " WHERE {" +
            TargetPointOfServiceModel.STORENUMBER + "} = ?storeNumber";

    private static final String QUERY_TARGET_POS_BY_TYPE = POS_SELECT_PREFIX + " WHERE {"
            + TargetPointOfServiceModel.TYPE
            + "} IN ({{select {pk} from {" + PointOfServiceTypeEnum._TYPECODE
            + "} where {code} IN ( ?typeCode )}}) AND ( {"
            + TargetPointOfServiceModel.VIRTUALPOS + "} is NULL"
            + " OR {" + TargetPointOfServiceModel.VIRTUALPOS + "} = ?virtualPOS )";

    private static final String QUERY_TARGET_POS_BY_TYPE_AND_BASESTORE = QUERY_TARGET_POS_BY_TYPE + " AND {" +
            TargetPointOfServiceModel.BASESTORE + "} = ?baseStore AND ( {"
            + TargetPointOfServiceModel.VIRTUALPOS + "} is NULL"
            + " OR {" + TargetPointOfServiceModel.VIRTUALPOS + "} = ?virtualPOS )";

    private static final String QUERY_TARGET_POS_BY_TYPE_FOR_CNC = QUERY_TARGET_POS_BY_TYPE + " AND {"
            + TargetPointOfServiceModel.ACCEPTCNC
            + "} = ?acceptCNC" + POS_WHERE_CLOSED_SUFFIX;

    private static final String QUERY_TARGET_POS_BY_TYPE_CLOSED = QUERY_TARGET_POS_BY_TYPE + POS_WHERE_CLOSED_SUFFIX;

    private static final String QUERY_TARGET_POS_FULFILMENT_ENABLED = "SELECT {pos." + TargetPointOfServiceModel.PK
            + "} FROM {" + TargetPointOfServiceModel._TYPECODE + " AS pos JOIN "
            + StoreFulfilmentCapabilitiesModel._TYPECODE +
            " AS sfc on {sfc." + StoreFulfilmentCapabilitiesModel.PK + "} = {pos."
            + TargetPointOfServiceModel.FULFILMENTCAPABILITY
            + "}} WHERE {sfc." + StoreFulfilmentCapabilitiesModel.ENABLED + "} = ?enabled";

    private static final String QUERY_CONDITION_PPD_ENABLE = " and {sfc."
            + StoreFulfilmentCapabilitiesModel.FULFILMENTPICKPACKDISPATCHSTORE + "} = ?ppdEnabled";

    private static final String QUERY_TARGET_POS_FULFILMENT_ENABLED_PPD_ENABLED = QUERY_TARGET_POS_FULFILMENT_ENABLED
            + QUERY_CONDITION_PPD_ENABLE;

    private static final String QUERY_TARGET_POS_FULFILMENT_ENABLED_STATE = "SELECT {" + TargetPointOfServiceModel.PK
            + "} FROM {" + TargetPointOfServiceModel._TYPECODE + " AS pos JOIN " + AddressModel._TYPECODE
            + " AS addr on {addr." + AddressModel.PK + "} = {pos." + TargetPointOfServiceModel.ADDRESS + "} JOIN "
            + StoreFulfilmentCapabilitiesModel._TYPECODE + " AS sfc on {sfc." + StoreFulfilmentCapabilitiesModel.PK
            + "} = {pos." + TargetPointOfServiceModel.FULFILMENTCAPABILITY + "}} where {addr."
            + AddressModel.DISTRICT + "} = ?state and {sfc." + StoreFulfilmentCapabilitiesModel.ENABLED
            + "} = ?enabled";

    private static final String QUERY_FULFILMENT_PENDING_QUANTITY = "SELECT SUM({oe:" + OrderEntryModel.QUANTITY
            + "}) FROM {" + OrderEntryModel._TYPECODE + " AS oe JOIN " + ConsignmentEntryModel._TYPECODE
            + " AS ce ON {oe:" + OrderEntryModel.PK + "} = {ce:"
            + ConsignmentEntryModel.ORDERENTRY + "} JOIN " + ConsignmentModel._TYPECODE + " AS c ON {ce:"
            + ConsignmentEntryModel.CONSIGNMENT + "} = {c:" + ConsignmentModel.PK + "} JOIN "
            + WarehouseModel._TYPECODE
            + " AS w ON {c:" + ConsignmentModel.WAREHOUSE
            + "} = {w:pk} JOIN PoS2WarehouseRel AS rel ON {rel:target} = {w:" + WarehouseModel.PK + "} JOIN "
            + TargetPointOfServiceModel._TYPECODE + " AS tpos ON {tpos:" + TargetPointOfServiceModel.PK
            + "} = {rel:source}} WHERE {c:"
            + ConsignmentModel.STATUS + "} IN (?consignmentStatuses) AND {tpos:"
            + TargetPointOfServiceModel.STORENUMBER
            + "} = ?storeNumber AND {oe:" + OrderEntryModel.PRODUCT + "} = ?product";

    private static final String QUERY_REGION_FOR_DISTRICT = "SELECT {reg." + RegionModel.PK
            + "} FROM {" + RegionModel._TYPECODE
            + " as reg} where {" + RegionModel.ABBREVIATION
            + "}=?abbreviation";

    private static final String QUERY_FULFILMENT_PENDING_QUANTITY_MULTI_STORE = "SELECT {tpos:"
            + TargetPointOfServiceModel.STORENUMBER + "}, {oe:" + OrderEntryModel.PRODUCT + "}, SUM({oe:"
            + OrderEntryModel.QUANTITY
            + "}) FROM {" + OrderEntryModel._TYPECODE + " AS oe JOIN " + ConsignmentEntryModel._TYPECODE
            + " AS ce ON {oe:" + OrderEntryModel.PK + "} = {ce:"
            + ConsignmentEntryModel.ORDERENTRY + "} JOIN " + ConsignmentModel._TYPECODE + " AS c ON {ce:"
            + ConsignmentEntryModel.CONSIGNMENT + "} = {c:" + ConsignmentModel.PK + "} JOIN "
            + WarehouseModel._TYPECODE
            + " AS w ON {c:" + ConsignmentModel.WAREHOUSE
            + "} = {w:pk} JOIN PoS2WarehouseRel AS rel ON {rel:target} = {w:" + WarehouseModel.PK + "} JOIN "
            + TargetPointOfServiceModel._TYPECODE + " AS tpos ON {tpos:" + TargetPointOfServiceModel.PK
            + "} = {rel:source}} WHERE {c:"
            + ConsignmentModel.STATUS + "} IN (?consignmentStatuses) AND {tpos:"
            + TargetPointOfServiceModel.STORENUMBER
            + "} IN (?storeNumbers) AND {oe:" + OrderEntryModel.PRODUCT + "} IN (?products)"
            + " GROUP BY {oe:" + OrderEntryModel.PRODUCT + "}, {tpos:" + TargetPointOfServiceModel.STORENUMBER + "}";

    private static final String QUERY_PARAM_BASESTORE = "baseStore";

    private static final String QUERY_PARAM_TYPECODE = "typeCode";

    private Set<String> targetPosEnums;

    private List<ConsignmentStatus> pendingConsignmentStatuses;

    @Override
    public TargetPointOfServiceModel getPOSByStoreNumber(final Integer storeNumber)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final SearchResult<TargetPointOfServiceModel> searchResult = getFlexibleSearchService().search(
                QUERY_GET_POS_BY_STORE_NUMBER,
                Collections.singletonMap(TargetPointOfServiceModel.STORENUMBER, storeNumber));
        final List<TargetPointOfServiceModel> models = searchResult.getResult();
        TargetServicesUtil.validateIfSingleResult(models, TargetPointOfServiceModel.class,
                TargetPointOfServiceModel.STORENUMBER, storeNumber);

        return models.get(0);
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtcore.storelocator.pos.dao.TargetPointOfServiceDao#getRegionByAbbreviation(java.lang.String)
     */
    @Override
    public RegionModel getRegionByAbbreviation(final String abbreviation) {
        if (StringUtils.isNotEmpty(abbreviation)) {
            final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_REGION_FOR_DISTRICT);
            query.addQueryParameter("abbreviation", abbreviation);

            try {
                return getFlexibleSearchService().searchUnique(query);
            }
            catch (final ModelNotFoundException | AmbiguousIdentifierException e) {
                LOG.warn("Either no or more than one Region found with abbreviation as: " + abbreviation, e);
                return null;
            }
        }
        return null;
    }

    @Override
    public List<TargetPointOfServiceModel> getAllOpenPOS() {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_TARGET_POS_BY_TYPE_CLOSED);
        query.addQueryParameter(QUERY_PARAM_TYPECODE, targetPosEnums);
        query.addQueryParameter(TargetPointOfServiceModel.CLOSED, Boolean.FALSE);
        query.addQueryParameter(VIRTUAL_POS, Boolean.FALSE);

        final SearchResult<TargetPointOfServiceModel> result = getFlexibleSearchService().search(query);
        return result.getResult();
    }

    @Override
    public List<TargetPointOfServiceModel> getAllOpenCncPOS() {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_TARGET_POS_BY_TYPE_FOR_CNC);
        query.addQueryParameter(QUERY_PARAM_TYPECODE, targetPosEnums);
        query.addQueryParameter(TargetPointOfServiceModel.ACCEPTCNC, Boolean.TRUE);
        query.addQueryParameter(TargetPointOfServiceModel.CLOSED, Boolean.FALSE);
        query.addQueryParameter(VIRTUAL_POS, Boolean.FALSE);

        final SearchResult<TargetPointOfServiceModel> result = getFlexibleSearchService().search(query);
        return result.getResult();
    }

    @Override
    public List<TargetPointOfServiceModel> getAllPOSByTypes(final BaseStoreModel baseStore) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_TARGET_POS_BY_TYPE_AND_BASESTORE);
        query.addQueryParameter(QUERY_PARAM_BASESTORE, baseStore);
        query.addQueryParameter(QUERY_PARAM_TYPECODE, targetPosEnums);
        query.addQueryParameter(VIRTUAL_POS, Boolean.FALSE);

        final SearchResult<TargetPointOfServiceModel> result = getFlexibleSearchService().search(query);
        return result.getResult();
    }

    @Override
    public List<TargetPointOfServiceModel> getAllFulfilmentEnabledPOS() {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_TARGET_POS_FULFILMENT_ENABLED);
        query.addQueryParameter("enabled", Boolean.TRUE);

        final SearchResult<TargetPointOfServiceModel> result = getFlexibleSearchService().search(query);
        return result.getResult();
    }

    @Override
    public List<TargetPointOfServiceModel> getAllFulfilmentPPDEnabledPOS() {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_TARGET_POS_FULFILMENT_ENABLED_PPD_ENABLED);
        query.addQueryParameter("enabled", Boolean.TRUE);
        query.addQueryParameter("ppdEnabled", Boolean.TRUE);
        final SearchResult<TargetPointOfServiceModel> result = getFlexibleSearchService().search(query);
        return result.getResult();
    }

    @Override
    public List<TargetPointOfServiceModel> getAllFulfilmentEnabledPOSInState(final String state) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_TARGET_POS_FULFILMENT_ENABLED_STATE);
        query.addQueryParameter("enabled", Boolean.TRUE);
        query.addQueryParameter("state", state);

        final SearchResult<TargetPointOfServiceModel> result = getFlexibleSearchService().search(query);
        return result.getResult();
    }

    @Override
    public int getFulfilmentPendingQuantity(final Integer storeNumber, final ProductModel product) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_FULFILMENT_PENDING_QUANTITY);
        query.addQueryParameter("consignmentStatuses", pendingConsignmentStatuses);
        query.addQueryParameter("storeNumber", storeNumber);
        query.addQueryParameter("product", product);

        final List resultClassList = new ArrayList();
        resultClassList.add(Integer.class);
        query.setResultClassList(resultClassList);

        final SearchResult<Integer> resultSet = getFlexibleSearchService().search(query);
        if (resultSet != null && CollectionUtils.isNotEmpty(resultSet.getResult())) {
            final Integer sum = resultSet.getResult().get(0);
            if (sum != null) {
                return sum.intValue();
            }
        }
        return 0;
    }

    @Override
    public List<List> getFulfilmentPendingQuantityByProductCodeForStores(
            final List<String> storeNumbers, final List<ProductModel> products) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_FULFILMENT_PENDING_QUANTITY_MULTI_STORE);
        query.addQueryParameter("consignmentStatuses", pendingConsignmentStatuses);
        query.addQueryParameter("storeNumbers", storeNumbers);
        query.addQueryParameter("products", products);

        final List resultClassList = new ArrayList();
        resultClassList.add(Integer.class);
        resultClassList.add(ProductModel.class);
        resultClassList.add(Long.class);
        query.setResultClassList(resultClassList);

        final SearchResult<List> resultSet = getFlexibleSearchService().search(query);
        if (resultSet != null && CollectionUtils.isNotEmpty(resultSet.getResult())) {
            return resultSet.getResult();
        }
        return ListUtils.EMPTY_LIST;
    }


    @Override
    public List<TargetPointOfServiceModel> getAllPosForFluent() {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_TARGET_POS_BY_TYPE);
        query.addQueryParameter(QUERY_PARAM_TYPECODE, targetPosEnums);
        query.addQueryParameter(VIRTUAL_POS, Boolean.FALSE);
        final SearchResult<TargetPointOfServiceModel> result = getFlexibleSearchService().search(query);
        return result.getResult();
    }

    /**
     * @param targetPosEnums
     *            the targetPosEnums to set
     */
    @Required
    public void setTargetPosEnums(final Set<String> targetPosEnums) {
        this.targetPosEnums = targetPosEnums;
    }

    /**
     * @param pendingConsignmentStatuses
     *            the pendingConsignmentStatuses to set
     */
    @Required
    public void setPendingConsignmentStatuses(final List<ConsignmentStatus> pendingConsignmentStatuses) {
        this.pendingConsignmentStatuses = pendingConsignmentStatuses;
    }

}

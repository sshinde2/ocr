/**
 * 
 */
package au.com.target.tgtcore.storelocator.pos.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commerceservices.util.AbstractComparator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.storelocator.pos.impl.DefaultPointOfServiceService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtcore.storelocator.pos.dao.TargetPointOfServiceDao;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;


/**
 * Default implementation of {@link TargetPointOfServiceService}
 * 
 */
public class TargetPointOfServiceServiceImpl extends DefaultPointOfServiceService implements
        TargetPointOfServiceService {

    private static final Logger LOG = Logger.getLogger(TargetPointOfServiceServiceImpl.class);

    private static final String DISABLE_RESTRICTIONS = "disableRestrictions";

    private TargetPointOfServiceDao targetPointOfServiceDao;

    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    private TargetProductService targetProductService;

    private CatalogVersionService catalogVersionService;

    private SessionService sessionService;

    private TargetWarehouseService targetWarehouseService;

    @Override
    public TargetPointOfServiceModel getPOSByStoreNumber(final Integer storeNumber)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        ServicesUtil.validateParameterNotNullStandardMessage("Store Number", storeNumber);
        return targetPointOfServiceDao.getPOSByStoreNumber(storeNumber);
    }

    @Override
    public WarehouseModel getWarehouseByStoreNumber(final Integer storeNumber) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetPointOfServiceModel pos = this.getPOSByStoreNumber(storeNumber);
        return targetWarehouseService.getWarehouseForPointOfService(pos);
    }

    @Override
    public Map<String, Set<TargetPointOfServiceModel>> getStateAndStoresForCart(final AbstractOrderModel cartModel) {
        Assert.notNull(cartModel);
        final Set<ProductTypeModel> productTypesSet = buildProductTypeSet(cartModel);

        final List<TargetPointOfServiceModel> targetPointOfServices = getCommonStores(
                productTypesSet, cartModel);

        return buildStateAndStoreMap(targetPointOfServices);
    }

    @Override
    public List<TargetPointOfServiceModel> getStoresForCart(final AbstractOrderModel cartModel) {
        Assert.notNull(cartModel);
        final Set<ProductTypeModel> productTypesSet = buildProductTypeSet(cartModel);

        return getCommonStores(productTypesSet, cartModel);
    }

    @Override
    public boolean validateStoreNumber(final Integer storeNumber) {
        try {
            getPOSByStoreNumber(storeNumber);
        }
        catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
            return false;
        }
        return true;
    }

    protected Map<String, Set<TargetPointOfServiceModel>> buildStateAndStoreMap(
            final List<TargetPointOfServiceModel> targetPointOfServices) {
        if (CollectionUtils.isEmpty(targetPointOfServices)) {
            return null;
        }

        final Map<String, Set<TargetPointOfServiceModel>> storesMap = new HashMap<String, Set<TargetPointOfServiceModel>>();
        for (final TargetPointOfServiceModel tpos : targetPointOfServices) {

            // If the store address is null then we can't add it to a state set,
            // log a warning instead
            if (tpos.getAddress() == null) {
                LOG.warn("The following store has an empty address: " + tpos.getName());
            }
            else {
                final String state = tpos.getAddress().getDistrict();
                if (storesMap.containsKey(state)) {
                    final Set<TargetPointOfServiceModel> tposForStateSet = storesMap.get(state);
                    tposForStateSet.add(tpos);
                }
                else {
                    final Set<TargetPointOfServiceModel> newStateSet = new TreeSet<TargetPointOfServiceModel>(
                            TargetPOSComparator.INSTANCE);
                    newStateSet.add(tpos);
                    storesMap.put(state, newStateSet);
                }
            }
        }
        return storesMap;
    }

    private Set<ProductTypeModel> buildProductTypeSet(final AbstractOrderModel cartModel) {
        Assert.notNull(cartModel);
        final List<AbstractOrderEntryModel> orderEntryModel = cartModel.getEntries();
        final Set<ProductTypeModel> productTypeModelSet = new HashSet<>();
        for (final AbstractOrderEntryModel entry : orderEntryModel) {
            final ProductModel product = entry.getProduct();
            final AbstractTargetVariantProductModel targetProduct = (AbstractTargetVariantProductModel)product;
            if (targetDeliveryModeHelper.doesProductHavePhysicalDeliveryMode(targetProduct)) {
                final ProductTypeModel productTypeModel = targetProduct.getProductType();
                productTypeModelSet.add(productTypeModel);
            }
        }
        return productTypeModelSet;
    }

    private List<TargetPointOfServiceModel> getCommonStores(
            final Set<ProductTypeModel> productTypesSet, final AbstractOrderModel cartModel) {
        Assert.notNull(cartModel);

        final List<TargetPointOfServiceModel> targetPointOfServices = targetPointOfServiceDao.getAllOpenCncPOS();

        if (CollectionUtils.isEmpty(targetPointOfServices)) {
            return targetPointOfServices;
        }

        // This will return an Unmodifiable List, so can't really manipulate the list later
        // Hence need to create another list and do some modifications
        final List<TargetPointOfServiceModel> result = new ArrayList<>();

        for (final TargetPointOfServiceModel pointOfService : targetPointOfServices) {
            final Set<ProductTypeModel> productTypes = pointOfService.getProductTypes();

            if (CollectionUtils.isEmpty(productTypes)) {
                continue;
            }

            // Only if there is the store support all different product types
            // it will include into the result
            if (CollectionUtils.isSubCollection(productTypesSet, productTypes)) {
                result.add(pointOfService);
            }
        }

        return result;
    }

    @Override
    public Map<String, Set<TargetPointOfServiceModel>> getAllStateAndStores() {
        final List<TargetPointOfServiceModel> targetPointOfService = targetPointOfServiceDao.getAllOpenPOS();
        return buildStateAndStoreMap(targetPointOfService);
    }

    @Override
    public List<TargetPointOfServiceModel> getAllOpenStores() {
        return targetPointOfServiceDao.getAllOpenPOS();
    }

    @Override
    public boolean isSupportAllProductTypesInCart(final TargetPointOfServiceModel pointOfService,
            final AbstractOrderModel cartModel) {
        boolean result = false;
        Assert.notNull(cartModel);
        Assert.notNull(pointOfService);
        final Set<ProductTypeModel> productTypesSet = buildProductTypeSet(cartModel);

        final Set<ProductTypeModel> productTypes = pointOfService.getProductTypes();

        if (CollectionUtils.isNotEmpty(productTypes)) {
            result = CollectionUtils.isSubCollection(productTypesSet, productTypes);
        }
        return result;
    }

    @Override
    public int getFulfilmentPendingQuantity(final Integer storeNumber, final ProductModel product) {
        final Integer qty = sessionService.executeInLocalView(new SessionExecutionBody() {
            @Override
            public Object execute() {
                // we are disabling restriction because we want to access closed tpos
                sessionService.setAttribute(DISABLE_RESTRICTIONS, Boolean.TRUE);
                return Integer.valueOf(targetPointOfServiceDao.getFulfilmentPendingQuantity(storeNumber, product));
            }

        });
        return qty.intValue();
    }

    @Override
    public int getFulfilmentPendingQuantityByProductCode(final Integer storeNumber, final String productCode) {
        final int quantity = 0;
        ProductModel productModel = null;
        try {
            productModel = targetProductService.getProductByCatalogVersion(
                    catalogVersionService.getCatalogVersion(
                            TgtCoreConstants.Catalog.PRODUCTS,
                            TgtCoreConstants.Catalog.ONLINE_VERSION),
                    productCode);
        }
        catch (final UnknownIdentifierException e) {
            return quantity;
        }
        catch (final AmbiguousIdentifierException e) {
            return quantity;
        }
        return targetPointOfServiceDao.getFulfilmentPendingQuantity(storeNumber, productModel);
    }

    @Override
    public Table<Integer, String, Long> getFulfilmentPendingQuantityByProductCodeForStores(
            final List<String> storeNumbers, final List<ProductModel> products) {
        final Table<Integer, String, Long> pendingQuantityTable = HashBasedTable.create();
        final List<List> storeProductSOHList = targetPointOfServiceDao
                .getFulfilmentPendingQuantityByProductCodeForStores(storeNumbers, products);
        if (CollectionUtils.isNotEmpty(storeProductSOHList)) {
            for (final List productSOH : storeProductSOHList) {
                final Integer storeNumber = (Integer)productSOH.get(0);
                final String productCode = ((ProductModel)productSOH.get(1)).getCode();
                final Long soh = (Long)productSOH.get(2);
                pendingQuantityTable.put(storeNumber, productCode, soh);
            }
        }
        return pendingQuantityTable;
    }

    @Override
    public List<TargetPointOfServiceModel> getAllStoresForFluent() {
        return targetPointOfServiceDao.getAllPosForFluent();
    }


    @Override
    public TargetPointOfServiceModel getTposOfDefaultWarehouse() {
        TargetPointOfServiceModel tpos = null;
        final WarehouseModel warehouse = targetWarehouseService.getDefaultOnlineWarehouse();
        if (warehouse != null) {
            tpos = sessionService.executeInLocalView(new SessionExecutionBody() {
                @Override
                public Object execute() {
                    // we are disabling restriction because we want to access closed tpos
                    sessionService.setAttribute(DISABLE_RESTRICTIONS, Boolean.TRUE);
                    if (CollectionUtils.isNotEmpty(warehouse.getPointsOfService())) {
                        // Assume there is one tpos and return it
                        return warehouse.getPointsOfService().iterator().next();
                    }
                    return null;
                }

            });
        }

        return tpos;
    }

    /**
     * Comparator to order TargetPointOfServiceModel by store name ascending (not case sensitive)
     */
    private static class TargetPOSComparator extends AbstractComparator<TargetPointOfServiceModel> {
        public static final TargetPOSComparator INSTANCE = new TargetPOSComparator();

        @Override
        protected int compareInstances(final TargetPointOfServiceModel pos1, final TargetPointOfServiceModel pos2) {
            // Equal only if the refs are equal, otherwise compare alphabetically
            final int comp = compareValues(pos1.getName(), pos2.getName(), false);
            if (comp == 0) {
                if (!pos1.equals(pos2)) {
                    // If the towns are the same but the references different, really should not happen, so just compare based on store number
                    final Integer num1 = pos1.getStoreNumber(), num2 = pos2.getStoreNumber();
                    return (num1 == null ? 0 : num1.intValue()) - (num2 == null ? 0 : num2.intValue());
                }
            }

            return comp;
        }
    }

    /**
     * @return the targetPointOfServiceDao
     */
    protected TargetPointOfServiceDao getTargetPointOfServiceDao() {
        return targetPointOfServiceDao;
    }

    /**
     * @param targetPointOfServiceDao
     *            the pointOfServiceDao to set
     */
    @Required
    public void setTargetPointOfServiceDao(final TargetPointOfServiceDao targetPointOfServiceDao) {
        this.targetPointOfServiceDao = targetPointOfServiceDao;
    }

    /**
     * @param targetDeliveryModeHelper
     *            the targetDeliveryModeHelper to set
     */
    @Required
    public void setTargetDeliveryModeHelper(final TargetDeliveryModeHelper targetDeliveryModeHelper) {
        this.targetDeliveryModeHelper = targetDeliveryModeHelper;
    }

    @Required
    public void setTargetProductService(final TargetProductService targetProductService) {
        this.targetProductService = targetProductService;
    }

    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @param sessionService
     *            the sessionService to set
     */
    @Required
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */
    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }


}

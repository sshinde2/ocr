package au.com.target.tgtcore.storelocator.route;

import de.hybris.platform.storelocator.data.AddressData;
import de.hybris.platform.storelocator.route.GeolocationDirectionsUrlBuilder;

import java.util.Map;


/**
 * Target extension to {@link GeolocationDirectionsUrlBuilder}.
 */
public interface TargetGeolocationDirectionsUrlBuilder extends GeolocationDirectionsUrlBuilder {

    /**
     * Returns the URL for address geo-location API.
     *
     * @param baseUrl
     *            the base url
     * @param address
     *            the address to geo-locate
     * @param params
     *            the http parameter map
     * @return the composed URL
     */
    String getAddressWebServiceUrl(final String baseUrl, final AddressData address, final Map params);

    /**
     * Returns the URL for showing directions on google maps.
     * 
     * @param baseUrl
     * @param source
     * @param destination
     * @return the composed URL
     */
    String getDirectionUrl(String baseUrl, String source, String destination);
}

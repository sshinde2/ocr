package au.com.target.tgtcore.storelocator.route.impl;

import org.apache.commons.codec.binary.Base64;

import javax.annotation.PostConstruct;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Encapsulates the logic for signing URLs that we send to Google Maps server.
 * For more details, see https://developers.google.com/maps/documentation/business/webservices/auth.
 */
public class GoogleUrlSigner {

    private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";

    private String clientId;
    private String privateKey;

    private Mac mac;
    private Base64 base64 = new Base64();


    /**
     * Initializes the signature key.
     *
     * @throws NoSuchAlgorithmException if HmacSHA1 algorithm is not supported
     * @throws InvalidKeyException if provided {@code privateKey} is invalid
     */
    @PostConstruct
    public void afterPropertiesSet() throws NoSuchAlgorithmException, InvalidKeyException {
        final byte[] key = base64.decode(getPrivateKey().replace('-', '+').replace('_', '/'));
        final SecretKeySpec sha1Key = new SecretKeySpec(key, HMAC_SHA1_ALGORITHM);
        mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
        mac.init(sha1Key);
    }

    /**
     * Adds Google required {@code client} and {@code signature} parameters to provided {@code url}.
     *
     * @param url the url to be signed
     * @return signed url
     */
    public String sign(final String url) {
        final String urlWithClientId = url + "&client=" + getClientId();
        final byte[] signatureBytes = mac.doFinal(urlWithClientId.getBytes());
        final String signature64 = base64.encodeAsString(signatureBytes).replace('+', '-').replace('/', '_');
        return urlWithClientId + "&signature=" + signature64;
    }

    /**
     * Returns the Google client ID.
     *
     * @return the client ID
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * Sets the Google client ID.
     *
     * @param clientId the client ID to set
     */
    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    /**
     * Returns the shared secret key.
     *
     * @return the shared secret key
     */
    public String getPrivateKey() {
        return privateKey;
    }

    /**
     * Sets the shared secret key.
     *
     * @param privateKey the shared secret key to set
     */
    public void setPrivateKey(final String privateKey) {
        this.privateKey = privateKey;
    }
}

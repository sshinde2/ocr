/**
 * 
 */
package au.com.target.tgtcore.strategy;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.jobs.maintenance.MaintenanceCleanupStrategy;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;



/**
 * The cleanup strategy removes carts older than cartAge defined in properties. Implemented according to maintenance
 * framework
 * 
 */
public class CleanupCartStrategy implements MaintenanceCleanupStrategy<ItemModel, CronJobModel> {
    private static final Logger LOG = Logger.getLogger(CleanupCartStrategy.class);

    private Integer cartAge;
    private ModelService modelService;

    @Override
    public FlexibleSearchQuery createFetchQuery(final CronJobModel cjm) {

        Assert.notNull(cartAge, "cartAge can not be null ");

        final String query = "SELECT {PK} "
                + "FROM { " + CartModel._TYPECODE + "} WHERE {modifiedtime} < ?age";
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
        searchQuery.addQueryParameter("age", getAge());
        return searchQuery;
    }

    @Override
    public void process(final List<ItemModel> elements) {
        if (CollectionUtils.isNotEmpty(elements)) {
            try {
                modelService.removeAll(elements);
            }
            catch (final Exception e) {
                LOG.warn("can not remove carts ", e);
            }
        }
    }

    /**
     * Calculates age of carts to remove depends on cartAge param
     * 
     * @return {@link Date}
     */
    protected Date getAge() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -cartAge.intValue());
        return cal.getTime();
    }

    /**
     * @param cartAge
     *            the cartAge to set
     */
    @Required
    public void setCartAge(final Integer cartAge) {
        this.cartAge = cartAge;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }
}

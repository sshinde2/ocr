/**
 * 
 */
package au.com.target.tgtcore.tax;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.europe1.enums.ProductTaxGroup;


/**
 * Retrieve existing ProductTaxGroup for given rate or create a new one if the rate is new
 * 
 */
public interface TargetTaxGroupService {

    /**
     * Get a product tax group for the given rate, or create a new one if one does not already exist.
     * 
     * @param rate
     *            in hundredths of a percent
     * @param catVersion
     * @return ProductTaxGroup
     */
    ProductTaxGroup getProductTaxGroupWithCreate(int rate, CatalogVersionModel catVersion);

}

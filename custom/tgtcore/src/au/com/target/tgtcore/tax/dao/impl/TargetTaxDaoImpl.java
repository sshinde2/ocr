/**
 * 
 */
package au.com.target.tgtcore.tax.dao.impl;

import de.hybris.platform.order.daos.impl.DefaultTaxDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtcore.model.TargetTaxModel;
import au.com.target.tgtcore.tax.dao.TargetTaxDao;


/**
 * Add method to get the default tax
 * 
 */
public class TargetTaxDaoImpl extends DefaultTaxDao implements TargetTaxDao {
    private static final Logger LOG = Logger.getLogger(TargetTaxDaoImpl.class);

    private static final String FIND_DEFAULT_TAX_QUERY = "SELECT {" + TargetTaxModel.PK + "} " +
            "FROM { " + TargetTaxModel._TYPECODE + "} " +
            "WHERE {" + TargetTaxModel.DEFAULT + "}=?default";

    @Override
    public TargetTaxModel getDefaultTax() {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("default", Boolean.TRUE);

        final List<TargetTaxModel> list = doSearch(FIND_DEFAULT_TAX_QUERY, params, TargetTaxModel.class);
        if (CollectionUtils.isEmpty(list))
        {
            LOG.warn("No default tax found");
            return null;
        }
        if (list.size() > 1)
        {
            LOG.warn("More than one default tax found");
        }

        return list.get(0);
    }

    /**
     * Run the given flexible search
     * 
     * @param query
     * @param params
     * @param resultClass
     * @return list of search results
     */
    protected <T> List<T> doSearch(final String query, final Map<String, Object> params, final Class<T> resultClass)
    {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query);
        if (params != null)
        {
            fQuery.addQueryParameters(params);
        }

        fQuery.setResultClassList(Collections.singletonList(resultClass));

        final SearchResult<T> searchResult = search(fQuery);
        return searchResult.getResult();
    }


}

/**
 * 
 */
package au.com.target.tgtcore.tax.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.order.price.TaxModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.europe1.enums.ProductTaxGroup;
import de.hybris.platform.europe1.enums.UserTaxGroup;
import de.hybris.platform.europe1.model.TaxRowModel;
import de.hybris.platform.order.TaxService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.TargetTaxModel;
import au.com.target.tgtcore.tax.TargetTaxGroupService;


public class TargetTaxGroupServiceImpl implements TargetTaxGroupService {

    private String ptgPrefix = "aus-gst-";
    private String taxPrefix = "aus-gst-";
    private String utgCode = "aus-taxes";

    private ModelService modelService;
    private EnumerationService enumerationService;
    private TaxService taxService;
    private FlexibleSearchService flexibleSearchService;

    @Override
    public ProductTaxGroup getProductTaxGroupWithCreate(final int rate, final CatalogVersionModel catVersion) {

        final String code = ptgPrefix + rate;

        // If the required PTG exists in the enum just return it
        ProductTaxGroup ptg = findProductTaxGroup(code);
        if (ptg != null) {
            return ptg;
        }

        // Otherwise create a new one
        ptg = createProductTaxGroup(code);

        // Create an associated tax
        final TaxModel tax = findOrCreateTargetTax(rate);

        // Create an associated tax row
        createTaxRow(tax, ptg, catVersion);

        return ptg;
    }


    /**
     * Find ProductTaxGroup for the given code if it exists
     * 
     * @param code
     * @return ProductTaxGroup or null if none found for the code
     */
    protected ProductTaxGroup findProductTaxGroup(final String code) {
        final List<ProductTaxGroup> listPtg = enumerationService.getEnumerationValues(ProductTaxGroup.class);
        for (final ProductTaxGroup ptg : listPtg) {
            if (ptg.getCode().equals(code)) {
                return ptg;
            }
        }

        return null;
    }


    /**
     * Create and return a ProductTaxGroup for the given code
     * 
     * @param code
     * @return ProductTaxGroup
     */
    protected ProductTaxGroup createProductTaxGroup(final String code) {

        final ProductTaxGroup ptg = ProductTaxGroup.valueOf(code);

        modelService.save(ptg);
        modelService.refresh(ptg);

        // Set the name
        enumerationService.setEnumerationName(ptg, code);

        return ptg;
    }


    /**
     * Find or create a Tax with the given rate
     * 
     * @param rate
     * @return TaxModel
     */
    protected TaxModel findOrCreateTargetTax(final int rate) {

        final TaxModel tax = findTax(rate);
        if (tax != null) {
            return tax;
        }

        final TargetTaxModel targetTax = createTargetTax(rate);
        return targetTax;
    }


    /**
     * Check whether a TaxRow exists for the given data
     * 
     * @param tax
     * @param ptg
     * @param catVersion
     * @return true if TaxRow exists
     */
    protected boolean existsTaxRow(final TaxModel tax, final ProductTaxGroup ptg,
            final CatalogVersionModel catVersion) {

        final TaxRowModel example = new TaxRowModel();
        example.setCatalogVersion(catVersion);
        example.setTax(tax);
        example.setPg(ptg);

        final List<TaxRowModel> found = flexibleSearchService.getModelsByExample(example);
        return (CollectionUtils.isNotEmpty(found));
    }


    protected TaxRowModel createTaxRow(final TaxModel tax, final ProductTaxGroup ptg,
            final CatalogVersionModel catVersion) {

        if (existsTaxRow(tax, ptg, catVersion)) {
            return null;
        }

        final TaxRowModel row = new TaxRowModel();

        row.setCatalogVersion(catVersion);
        row.setTax(tax);
        row.setPg(ptg);
        row.setUg(getUserTaxGroup());

        modelService.save(row);
        modelService.refresh(row);

        return row;
    }

    protected UserTaxGroup getUserTaxGroup() {
        final List<UserTaxGroup> listUtg = enumerationService.getEnumerationValues(UserTaxGroup.class);
        for (final UserTaxGroup utg : listUtg) {
            if (utg.getCode().equals(utgCode)) {
                return utg;
            }
        }

        return null;
    }


    private TaxModel findTax(final int rate) {

        final String code = taxPrefix + rate;

        TaxModel tax = null;
        try {
            tax = taxService.getTaxForCode(code);
        }
        catch (final UnknownIdentifierException e) {
            tax = null;
        }

        return tax;
    }


    private TargetTaxModel createTargetTax(final int rate) {

        final String code = taxPrefix + rate;

        final TargetTaxModel targetTax = new TargetTaxModel();
        targetTax.setCode(code);
        targetTax.setValue(Double.valueOf(convertRateToPercentValue(rate)));
        targetTax.setDefault(Boolean.FALSE);
        targetTax.setName(code, getDefaultLocale());

        modelService.save(targetTax);
        modelService.refresh(targetTax);

        return targetTax;
    }

    private double convertRateToPercentValue(final int rate) {

        final BigDecimal bd = BigDecimal.valueOf(rate / 100.0f);
        final BigDecimal rounded = bd.setScale(2, BigDecimal.ROUND_DOWN);
        return rounded.doubleValue();
    }

    private Locale getDefaultLocale() {
        return Locale.ENGLISH;
    }


    /**
     * @param modelService
     *            the modelService to set
     */
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param enumerationService
     *            the enumerationService to set
     */
    public void setEnumerationService(final EnumerationService enumerationService) {
        this.enumerationService = enumerationService;
    }


    /**
     * @param taxService
     *            the taxService to set
     */
    public void setTaxService(final TaxService taxService) {
        this.taxService = taxService;
    }


    /**
     * @param flexibleSearchService
     *            the flexibleSearchService to set
     */
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }


    /**
     * @param ptgPrefix
     *            the ptgPrefix to set
     */
    public void setPtgPrefix(final String ptgPrefix) {
        this.ptgPrefix = ptgPrefix;
    }


    /**
     * @param taxPrefix
     *            the taxPrefix to set
     */
    public void setTaxPrefix(final String taxPrefix) {
        this.taxPrefix = taxPrefix;
    }


    /**
     * @param utgCode
     *            the utgCode to set
     */
    public void setUtgCode(final String utgCode) {
        this.utgCode = utgCode;
    }


}

package au.com.target.tgtcore.taxinvoice;


import de.hybris.platform.commons.model.DocumentModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.promotions.result.PromotionOrderResults;

import au.com.target.tgtcore.model.TargetValuePackTypeModel;


/**
 *
 */
public interface TaxInvoiceService {

    /**
     * @param orderModel
     * @return generated invoice media
     * @throws TaxInvoiceException
     */
    MediaModel generateTaxInvoiceForOrder(OrderModel orderModel) throws TaxInvoiceException;

    /**
     * get Tax Invoice for order
     * 
     * @param orderModel
     * @return DocumentModel
     */
    DocumentModel getTaxInvoiceForOrder(OrderModel orderModel);

    /**
     * 
     * @param orderModel
     * @return true if the order is in the correct status
     */
    boolean isTaxInvoiceResendAllowed(OrderModel orderModel);

    /**
     * get the ValuePack Type Model for this product
     * 
     * @param code
     * @return TargetValuePackTypeModel
     */
    TargetValuePackTypeModel findTargetValuePackType(String code);

    /**
     * get the promotion results object for a given order
     * 
     * @param orderModel
     * @return PromotionOrderResults
     */
    PromotionOrderResults getPromotionResults(OrderModel orderModel);
}

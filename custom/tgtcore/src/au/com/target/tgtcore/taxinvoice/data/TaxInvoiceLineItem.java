/**
 * 
 */
package au.com.target.tgtcore.taxinvoice.data;

import java.util.List;


public class TaxInvoiceLineItem {

    private String productCode;
    private String productName;
    private String unitPrice;
    private String totalPrice;
    private double totalPriceAsDouble;
    private String bundleType;
    private long quantity;
    private String discount;
    private String promotionMessage;
    private List<TaxInvoiceLineItem> bundles;

    /**
     * @return the bundleType
     */
    public String getBundleType() {
        return bundleType;
    }

    /**
     * @param bundleType
     *            the bundleType to set
     */
    public void setBundleType(final String bundleType) {
        this.bundleType = bundleType;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName
     *            the productName to set
     */
    public void setProductName(final String productName) {
        this.productName = productName;
    }

    /**
     * @return the unitPrice
     */
    public String getUnitPrice() {
        return unitPrice;
    }

    /**
     * @param unitPrice
     *            the unitPrice to set
     */
    public void setUnitPrice(final String unitPrice) {
        this.unitPrice = unitPrice;
    }

    /**
     * @return the totalPrice
     */
    public String getTotalPrice() {
        return totalPrice;
    }

    /**
     * @param totalPrice
     *            the totalPrice to set
     */
    public void setTotalPrice(final String totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * @return the totalPriceAsDouble
     */
    public double getTotalPriceAsDouble() {
        return totalPriceAsDouble;
    }

    /**
     * @param totalPriceAsDouble
     *            the totalPriceAsDouble to set
     */
    public void setTotalPriceAsDouble(final double totalPriceAsDouble) {
        this.totalPriceAsDouble = totalPriceAsDouble;
    }

    /**
     * @return the quantity
     */
    public long getQuantity() {
        return quantity;
    }

    /**
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(final long quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the bundles
     */
    public List<TaxInvoiceLineItem> getBundles() {
        return bundles;
    }

    /**
     * @param bundles
     *            the bundles to set
     */
    public void setBundles(final List<TaxInvoiceLineItem> bundles) {
        this.bundles = bundles;
    }

    /**
     * @return the discount
     */
    public String getDiscount() {
        return discount;
    }

    /**
     * @param discount
     *            the discount to set
     */
    public void setDiscount(final String discount) {
        this.discount = discount;
    }

    /**
     * @return the promotionMessage
     */
    public String getPromotionMessage() {
        return promotionMessage;
    }

    /**
     * @param promotionMessage
     *            the promotionMessage to set
     */
    public void setPromotionMessage(final String promotionMessage) {
        this.promotionMessage = promotionMessage;
    }

}
/**
 * 
 */
package au.com.target.tgtcore.ticket.service;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ticket.service.TicketBusinessService;


/**
 * 
 */
public interface TargetTicketBusinessService extends TicketBusinessService {

    /**
     * Create a ticket and assign to the csagent.
     * 
     * @param headline
     *            The headline of the ticket
     * @param subject
     *            The subject of the create event to associate with the new ticket
     * @param text
     *            The text of the create event to associate with the new ticket
     * @param order
     *            The order the ticket relates to (can be null)
     * @return the id of the created ticket
     */
    String createCsAgentTicket(final String headline, final String subject, final String text, final OrderModel order);

    /**
     * Create a ticket and assign to the csagent.
     * 
     * @param headline
     *            The headline of the ticket
     * @param subject
     *            The subject of the create event to associate with the new ticket
     * @param text
     *            The text of the create event to associate with the new ticket
     * @param order
     *            The order the ticket relates to (can be null)
     * @param agentGroup
     *            The csagent group where the ticket needs to be assigned
     * @return the id of the created ticket
     */
    String createCsAgentTicket(final String headline, final String subject, final String text, final OrderModel order,
            String agentGroup);
}

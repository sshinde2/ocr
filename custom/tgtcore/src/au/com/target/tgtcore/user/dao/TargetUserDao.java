/**
 * 
 */
package au.com.target.tgtcore.user.dao;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetCustomerModel;


/**
 * @author mjanarth
 *
 */
public interface TargetUserDao {

    /**
     * 
     * @param subscriptionId
     * @return UserModel
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    TargetCustomerModel findUserBySubscriptionId(String subscriptionId) throws TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException;
}

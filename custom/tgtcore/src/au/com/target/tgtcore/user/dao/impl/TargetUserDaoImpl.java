/**
 * 
 */
package au.com.target.tgtcore.user.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.user.dao.TargetUserDao;
import au.com.target.tgtcore.util.TargetServicesUtil;


/**
 * @author mjanarth
 *
 */
public class TargetUserDaoImpl extends DefaultGenericDao implements TargetUserDao {

    /**
     */
    public TargetUserDaoImpl() {
        super(TargetCustomerModel._TYPECODE);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.user.dao.TargetUserDao#findUserBySubscriptionId(java.lang.String)
     */
    @Override
    public TargetCustomerModel findUserBySubscriptionId(final String subscriptionId)
            throws TargetAmbiguousIdentifierException, TargetUnknownIdentifierException {
        final Map<String, String> params = new HashMap<String, String>();
        params.put(TargetCustomerModel.SUBSCRIBERKEY, subscriptionId);
        final List<TargetCustomerModel> customerModels = find(params);
        TargetServicesUtil.validateIfSingleResult(customerModels, TargetCustomerModel.class,
                TargetCustomerModel.SUBSCRIBERKEY, subscriptionId);
        return customerModels.get(0);
    }

}

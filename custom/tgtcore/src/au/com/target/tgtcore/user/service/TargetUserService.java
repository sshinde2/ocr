/**
 * 
 */
package au.com.target.tgtcore.user.service;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetCustomerModel;


/**
 * @author mjanarth
 *
 */
public interface TargetUserService {

    /**
     * 
     * @param subscriptionId
     * @return TargetCustomerModel
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    TargetCustomerModel getUserBySubscriptionId(String subscriptionId) throws TargetAmbiguousIdentifierException,
            TargetUnknownIdentifierException;
}

/**
 * 
 */
package au.com.target.tgtcore.user.service.impl;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.user.dao.TargetUserDao;
import au.com.target.tgtcore.user.service.TargetUserService;


/**
 * @author mjanarth
 *
 */
public class TargetUserServiceImpl implements TargetUserService {

    private TargetUserDao targetUserDao;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.user.service.TargetUserService#getUserBySubscriptionId(java.lang.String)
     */
    @Override
    public TargetCustomerModel getUserBySubscriptionId(final String subscriptionId)
            throws TargetAmbiguousIdentifierException, TargetUnknownIdentifierException {
        if (null != subscriptionId) {
            return targetUserDao.findUserBySubscriptionId(subscriptionId);

        }
        return null;
    }

    /**
     * @param targetUserDao
     *            the targetUserDao to set
     */
    @Required
    public void setTargetUserDao(final TargetUserDao targetUserDao) {
        this.targetUserDao = targetUserDao;
    }

}

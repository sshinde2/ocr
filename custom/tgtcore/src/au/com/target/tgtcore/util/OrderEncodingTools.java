package au.com.target.tgtcore.util;

import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;
import de.hybris.platform.core.Registry;

import org.springframework.util.Assert;


/**
 * 
 * @author jspence8
 * 
 */
public final class OrderEncodingTools {

    private static final String SPACE = " ";
    private static final String PLUS = "+";
    private static SecureTokenService secureTokenService = (SecureTokenService)Registry.getApplicationContext()
            .getBean("secureTokenService");


    private OrderEncodingTools() {
    }

    /**
     * encode order Id
     * 
     * @param orderId
     * @return encoded order id
     */
    public static String encodeOrderId(final String orderId) {
        Assert.notNull(orderId, "Order Id can't be empty");

        final SecureToken secureToken = new SecureToken(orderId, System.currentTimeMillis());
        return secureTokenService.encryptData(secureToken);
    }

    /**
     * decoded Order id
     * 
     * @param encodedOrderId
     * @return decoded Order id
     */
    public static String decodeOrderId(final String encodedOrderId) {
        Assert.notNull(encodedOrderId, "Encoded Order Id can't be empty");

        String encryptedToken = encodedOrderId;
        // Just a hack to replace the 'space' which should have been '%2B' instead of '+' until we fix this permanently
        if (encodedOrderId.indexOf(SPACE) != -1) {
            encryptedToken = encodedOrderId.replaceAll(SPACE, PLUS);
        }

        final SecureToken secureToken = secureTokenService.decryptData(encryptedToken);

        Assert.notNull(secureToken, "Secure Token can't be empty");

        return secureToken.getData();
    }
}

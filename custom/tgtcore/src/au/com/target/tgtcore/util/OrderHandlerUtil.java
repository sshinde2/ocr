/**
 * 
 */
package au.com.target.tgtcore.util;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;


/**
 * Interface to create snapshot of an order during cancellation and refund.
 * 
 * @author Pratik
 *
 */
public interface OrderHandlerUtil {

    /**
     * Method creates a version of order model and then creates order snapshot.
     * 
     * @param order
     * @param description
     * @param requestor
     * @return OrderHistoryEntryModel
     */
    public OrderHistoryEntryModel createOrderSnapshot(OrderModel order, String description, PrincipalModel requestor);

    /**
     * Method creates order snapshot.
     * 
     * @param order
     * @param version
     * @param description
     * @param requestor
     * @return OrderHistoryEntryModel
     */
    public OrderHistoryEntryModel createOrderSnapshot(OrderModel order, OrderModel version,
            String description, PrincipalModel requestor);

}

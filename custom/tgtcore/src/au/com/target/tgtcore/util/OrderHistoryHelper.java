/**
 * 
 */
package au.com.target.tgtcore.util;

import de.hybris.platform.basecommerce.enums.OrderModificationEntryStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.orderhistory.OrderHistoryService;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import au.com.target.tgtcore.model.AddressModificationRecordEntryModel;
import au.com.target.tgtcore.model.AddressModificationRecordModel;



/**
 * Helper for creating an order history snapshot properly
 * 
 */
public class OrderHistoryHelper {

    protected static final String ADDRESS_UPDATE_NOTE = "Updated Delivery Address";

    private static final String NEW_VALUE = "new value";
    private static final String OLD_VALUE = "old value";

    private OrderHistoryService orderHistoryService;

    private ModelService modelService;

    private UserService userService;

    /**
     * Create a history snapshot for the given order.
     * 
     * @param order
     * @param description
     */
    public void createHistorySnapshot(final OrderModel order, final String description)
    {
        // Create and save the snapshot version
        final OrderModel version = orderHistoryService.createHistorySnapshot(order);
        orderHistoryService.saveHistorySnapshot(version);

        // Add the version into the history entries for the order
        final OrderHistoryEntryModel historyEntry = modelService.create(OrderHistoryEntryModel.class);
        historyEntry.setOrder(order);
        historyEntry.setPreviousOrderVersion(version);
        historyEntry.setDescription(description);
        historyEntry.setTimestamp(new Date());
        modelService.save(historyEntry);

        // Refresh the order to update the entries
        modelService.refresh(order);
    }


    public void createHistorySnapshot(final OrderModel order, final AddressModel newAddress) {
        createHistorySnapshot(order, newAddress, ADDRESS_UPDATE_NOTE);
    }

    /**
     * Create a history snapshot for the given order and address.
     * 
     * @param order
     * @param newAddress
     * @param description
     */
    public void createHistorySnapshot(final OrderModel order, final AddressModel newAddress, final String description)
    {
        // Create and save the snapshot version
        final OrderModel version = orderHistoryService.createHistorySnapshot(order);

        // Add the version into the history entries for the order
        final OrderHistoryEntryModel snapshot = createSnaphot(order, version, description,
                userService != null ? userService.getCurrentUser() : null);

        final AddressModificationRecordModel adressModRecord = (AddressModificationRecordModel)modelService
                .create(
                AddressModificationRecordModel.class);
        adressModRecord.setOrder(order);
        adressModRecord.setOwner(order);
        adressModRecord.setInProgress(false);

        final AddressModificationRecordEntryModel oldEntry = modelService.create(
                AddressModificationRecordEntryModel.class);
        oldEntry.setOriginalVersion(snapshot);
        oldEntry.setCode(generateEntryCode(snapshot) + "_old");
        oldEntry.setTimestamp(new Date());
        oldEntry.setModificationRecord(adressModRecord);
        oldEntry.setOwner(adressModRecord);
        oldEntry.setStatus(OrderModificationEntryStatus.SUCCESSFULL);
        oldEntry.setChangedAddress(version.getDeliveryAddress());
        oldEntry.setNotes(OLD_VALUE);
        modelService.save(oldEntry);

        final AddressModificationRecordEntryModel newEntry = modelService.create(
                AddressModificationRecordEntryModel.class);
        newEntry.setOriginalVersion(snapshot);
        newEntry.setCode(generateEntryCode(snapshot) + "_new");
        newEntry.setTimestamp(new Date());
        newEntry.setModificationRecord(adressModRecord);
        newEntry.setOwner(adressModRecord);
        newEntry.setStatus(OrderModificationEntryStatus.SUCCESSFULL);
        newEntry.setChangedAddress(newAddress);
        newEntry.setNotes(NEW_VALUE);
        modelService.save(newEntry);

        final List entries = new ArrayList<>();
        entries.add(oldEntry);
        entries.add(newEntry);

        adressModRecord.setModificationRecordEntries(entries);
        modelService.save(adressModRecord);

        // Refresh the order to update the entries
        modelService.refresh(order);
    }

    protected OrderHistoryEntryModel createSnaphot(final OrderModel order, final OrderModel version,
            final String description, final PrincipalModel requestor)
    {
        this.orderHistoryService.saveHistorySnapshot(version);

        final OrderHistoryEntryModel historyEntry = (OrderHistoryEntryModel)modelService.create(
                OrderHistoryEntryModel.class);
        historyEntry.setOrder(order);
        historyEntry.setPreviousOrderVersion(version);
        historyEntry.setDescription(description);
        historyEntry.setTimestamp(new Date());
        if (requestor instanceof EmployeeModel)
        {
            historyEntry.setEmployee((EmployeeModel)requestor);
        }
        modelService.save(historyEntry);
        return historyEntry;
    }

    protected String generateEntryCode(final OrderHistoryEntryModel snapshot)
    {
        return snapshot.getOrder().getCode() + "_v" + snapshot.getPreviousOrderVersion().getVersionID() + "_c";
    }


    /**
     * @param orderHistoryService
     *            the orderHistoryService to set
     */
    public void setOrderHistoryService(final OrderHistoryService orderHistoryService) {
        this.orderHistoryService = orderHistoryService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }


    /**
     * 
     * @param userService
     *            the userService to set
     */
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }




}

/**
 * 
 */
package au.com.target.tgtcore.util;

import org.joda.time.DateTime;


/**
 * @author pthoma20
 *
 */
public class RestrictionStartDateEndDateResult {

    private DateTime startDate;

    private DateTime endDate;

    /**
     * @return the startDate
     */
    public DateTime getStartDate() {
        return startDate;
    }

    /**
     * @param startDate
     *            the startDate to set
     */
    public void setStartDate(final DateTime startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public DateTime getEndDate() {
        return endDate;
    }

    /**
     * @param endDate
     *            the endDate to set
     */
    public void setEndDate(final DateTime endDate) {
        this.endDate = endDate;
    }
}

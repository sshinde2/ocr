/**
 * 
 */
package au.com.target.tgtcore.util;

import de.hybris.platform.category.jalo.Category;
import de.hybris.platform.category.jalo.CategoryManager;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.variants.jalo.VariantProduct;
import de.hybris.platform.voucher.model.DateRestrictionModel;
import de.hybris.platform.voucher.model.RestrictionModel;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;

import au.com.target.tgtcore.jalo.AbstractTargetVariantProduct;
import au.com.target.tgtcore.jalo.ProductType;
import au.com.target.tgtcore.jalo.TargetProduct;


/**
 * @author rmcalave
 * 
 */
@SuppressWarnings("deprecation")
public final class RestrictionUtils {
    private RestrictionUtils() {
    }

    /**
     * Retrieves all the products and its parent hierarchy to get all the categories and super categories then in turn
     * calls the contains category to match it with the restricted category. (Checks if the product is included in the
     * restricted categories)
     * 
     * @param product
     * @param restrictCategories
     * @return true if this product is a restricted one.
     */
    public static boolean containsProductCategory(final Product product, final Collection restrictCategories) {
        boolean result = false;
        Product productCopy = product;
        final CategoryManager catManager = CategoryManager.getInstance();
        final Collection categories = new HashSet();
        while (productCopy instanceof VariantProduct) {
            categories.addAll(catManager.getSupercategories(productCopy));
            productCopy = ((VariantProduct)productCopy).getBaseProduct();
        }
        categories.addAll(catManager.getSupercategories(productCopy));
        result = containsCategory(categories, restrictCategories);
        return result;
    }

    /**
     * Recursive check for super Categories
     * 
     * @param categories
     * @param restrictCategories
     * @return true if any of the category matches with the restrict Categories.
     */
    public static boolean containsCategory(final Collection<Category> categories,
            final Collection<Category> restrictCategories) {
        if (categories.isEmpty()) {
            return false;
        }
        boolean result = false;
        for (final Category category : categories) {
            if (restrictCategories.contains(category)) {
                result = true;
                break;
            }
        }
        //only when the category is not found, the super categories will be checked
        if (!result) {
            for (final Category category : categories) {
                result = containsCategory(category.getSupercategories(), restrictCategories);
                if (result) {
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Checks whether this product matches a chosen product of the Restriction entries. VariantProducts are also matched
     * if the BaseProduct is restricted (recursively).
     * 
     * @param products
     * @param product
     * @return true if this product is a restricted one.
     */
    public static boolean containsProduct(final Collection<Product> products, final Product product) {
        boolean result = products.contains(product);
        if (!result && product instanceof VariantProduct) {
            result = containsProduct(products, ((VariantProduct)product).getBaseProduct());
        }
        return result;
    }


    /**
     * This method takes in the restrictions and looks up the Date Restriction to find out the applicable Start date and
     * end date. This is will return the start date and end date of the first time restriction found.
     * 
     * @param restrictions
     * @return dateMap
     */
    public static RestrictionStartDateEndDateResult findStartAndEndDateBasedOnRestriction(
            final Set<RestrictionModel> restrictions) {

        if (CollectionUtils.isEmpty(restrictions)) {
            return null;
        }
        for (final RestrictionModel restrictionModel : restrictions) {
            if (restrictionModel instanceof DateRestrictionModel) {
                final DateRestrictionModel dateRestrictionModel = (DateRestrictionModel)restrictionModel;
                final RestrictionStartDateEndDateResult restrictionStartDateEndDateResult = new RestrictionStartDateEndDateResult();
                restrictionStartDateEndDateResult
                        .setStartDate((null != dateRestrictionModel.getStartDate()) ? new DateTime(
                                dateRestrictionModel.getStartDate()) : null);
                restrictionStartDateEndDateResult
                        .setEndDate((null != dateRestrictionModel.getEndDate()) ? new DateTime(
                                dateRestrictionModel.getEndDate()) : null);
                return restrictionStartDateEndDateResult;
            }
        }
        return null;
    }

    /**
     * Gets the product type.
     * 
     * @param orderEntryProduct
     *            the order entry product
     * @return the product type
     */
    public static ProductType getProductType(final Product orderEntryProduct) {
        if (orderEntryProduct instanceof TargetProduct) {
            final TargetProduct product = (TargetProduct)orderEntryProduct;
            return product.getProductType();
        }
        if (orderEntryProduct instanceof AbstractTargetVariantProduct) {
            final AbstractTargetVariantProduct variantProduct = (AbstractTargetVariantProduct)orderEntryProduct;
            return getProductType(variantProduct.getBaseProduct());
        }
        return null;
    }
}

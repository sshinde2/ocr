/**
 * 
 */
package au.com.target.tgtcore.util;

import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Collection;

import org.springframework.util.Assert;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;



/**
 * Utilities for handling e.g. exceptions in DAO classes in a common way, parameters etc. Internally use Hybris
 * ServicesUtil, for more details please check {@link ServicesUtil}
 */
public final class TargetServicesUtil {

    private TargetServicesUtil() {
    }

    /**
     * see more in {@link ServicesUtil validateIfSingleResult}
     * 
     * @param resultToCheck
     * @param unknownIdException
     * @param ambiguousIdException
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    public static void validateIfSingleResult(final Collection<? extends Object> resultToCheck,
            final String unknownIdException, final String ambiguousIdException)
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        try {
            ServicesUtil.validateIfSingleResult(resultToCheck, unknownIdException, ambiguousIdException);
        }
        catch (final UnknownIdentifierException unKnownIdExcep) {
            throw new TargetUnknownIdentifierException(unKnownIdExcep);
        }
        catch (final AmbiguousIdentifierException ambiguousIdExcep) {
            throw new TargetAmbiguousIdentifierException(ambiguousIdExcep);
        }
    }

    /**
     * see more in {@link ServicesUtil validateIfSingleResult}
     * 
     * @param resultToCheck
     * @param type
     * @param qualifier
     * @param qualifierValue
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    public static void validateIfSingleResult(final Collection<? extends Object> resultToCheck, final Class type,
            final String qualifier, final Object qualifierValue) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        validateIfSingleResult(resultToCheck,
                type.getSimpleName() + " with " + qualifier + " '" + qualifierValue.toString()
                        + "' not found!", type.getSimpleName() + " " + qualifier + " '" + qualifierValue.toString()
                        + "' is not unique, "
                        + resultToCheck.size() + " instances  of type " + type.getSimpleName() + " found!");
    }

    /**
     * see more in {@link ServicesUtil validateIfAnyResult}
     * 
     * @param resultToCheck
     * @param unknownIdException
     * @throws TargetUnknownIdentifierException
     */
    public static void validateIfAnyResult(final Collection resultToCheck, final String unknownIdException)
            throws TargetUnknownIdentifierException
    {
        try {
            ServicesUtil.validateIfAnyResult(resultToCheck, unknownIdException);
        }
        catch (final UnknownIdentifierException unKnownIdExcep) {
            throw new TargetUnknownIdentifierException(unKnownIdExcep);
        }

    }

    /**
     * Make sure that the code passed in will only lowercase text and internal spaces.<br/>
     * Upercase text is changed to lowercase, leading and trailing spaces are stripped, all other chars removed.<br />
     * 
     * @param string
     *            code to make safe
     * @return nothing but lowercase chars and spaces
     */
    public static String extractSafeCodeFromName(final String string, final boolean allowNumbers) {
        Assert.notNull(string);

        final StringBuilder safe = new StringBuilder(string.toLowerCase());
        int pos = 0;
        while (pos < safe.length()) {
            final char curChar = safe.charAt(pos);
            // we explicitly mention the chars to allow here instead of using the Character.isDigit() type methods
            // as there are not latin chars that will return true from isDigit and isLowercase.
            if (curChar >= 'a' && curChar <= 'z') {
                pos++;
            }
            else if (allowNumbers && curChar >= '0' && curChar <= '9') {
                pos++;
            }
            else {
                safe.deleteCharAt(pos);
            }
        }

        return safe.toString();
    }



}

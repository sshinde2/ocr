package au.com.target.tgtcore.util.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.product.TargetProductService;


public class NLPImporterContext
{

    private static final Logger LOG = Logger.getLogger(NLPImporterContext.class);

    private static final int CSV_COL_ONE = 1;

    private static final int CSV_COL_TWO = 2;

    private static final int CSV_COL_THREE = 3;

    private static final int KEY_ELEVEN = 11;

    private static final int KEY_TWELVE = 12;

    private static final int KEY_THIRTEEN = 13;

    private TargetProductService targetProductService;

    private CatalogVersionService catalogVersionService;

    public NLPImporterContext()
    {
        // default constructor          
    }


    /**
     * Method will prepare data for the TCVM.
     */
    public void prepareNLPDates(final Map<Integer, String> line)
    {
        final String prodCode = getValue(line, CSV_COL_ONE);
        ProductModel product = null;
        try {
            product = targetProductService.getProductForCode(
                    catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                            TgtCoreConstants.Catalog.OFFLINE_VERSION), prodCode);

        }
        catch (final UnknownIdentifierException ex) {
            LOG.error("Product Code not available in Hybris ::" + prodCode);
        }
        if (product != null) {
            if (product instanceof TargetColourVariantProductModel) {
                putLines(line, prodCode);
            }
            else if (product instanceof TargetSizeVariantProductModel) {
                final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)product;
                putLines(line, sizeVariant.getBaseProduct().getCode());
            }
        }


    }

    private void putLines(final Map<Integer, String> line, final String prodCode) {
        line.put(getKey(KEY_ELEVEN), prodCode);
        line.put(getKey(KEY_TWELVE), getValue(line, CSV_COL_TWO));
        line.put(getKey(KEY_THIRTEEN), getValue(line, CSV_COL_THREE));
    }

    private String getValue(final Map<Integer, String> line, final int index)
    {
        return line.get(Integer.valueOf(index));
    }

    private Integer getKey(final int index)
    {
        return Integer.valueOf(index);
    }


    /**
     * @param targetProductService
     *            the targetProductService to set
     */
    @Required
    public void setTargetProductService(final TargetProductService targetProductService) {
        this.targetProductService = targetProductService;
    }


    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }
}

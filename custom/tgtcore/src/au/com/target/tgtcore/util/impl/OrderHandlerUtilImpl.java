/**
 * 
 */
package au.com.target.tgtcore.util.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.orderhistory.OrderHistoryService;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collections;
import java.util.Date;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.util.OrderHandlerUtil;


/**
 * Utility class to create snapshot of an order during cancellation and refund.
 * 
 * @author Pratik
 *
 */
public class OrderHandlerUtilImpl implements OrderHandlerUtil {

    private PromotionsService promotionsService;
    private OrderHistoryService orderHistoryService;
    private ModelService modelService;

    /**
     * Method creates a version of order model and then creates order snapshot.
     * 
     * @param order
     * @param description
     * @param requestor
     * @return OrderHistoryEntryModel
     */
    @Override
    public OrderHistoryEntryModel createOrderSnapshot(final OrderModel order, final String description,
            final PrincipalModel requestor) {
        final OrderModel version = orderHistoryService.createHistorySnapshot(order);
        return createOrderSnapshot(order, version, description, requestor);
    }

    /**
     * Method creates order snapshot.
     * 
     * @param order
     * @param version
     * @param description
     * @param requestor
     * @return OrderHistoryEntryModel
     */
    @Override
    public OrderHistoryEntryModel createOrderSnapshot(final OrderModel order, final OrderModel version,
            final String description, final PrincipalModel requestor) {
        version.setAllPromotionResults(Collections.<PromotionResultModel> emptySet());
        orderHistoryService.saveHistorySnapshot(version);

        final OrderHistoryEntryModel orderHistoryEntryModel = (OrderHistoryEntryModel)modelService.create(
                OrderHistoryEntryModel.class);
        orderHistoryEntryModel.setOrder(order);
        orderHistoryEntryModel.setPreviousOrderVersion(version);
        orderHistoryEntryModel.setDescription(description);
        orderHistoryEntryModel.setTimestamp(new Date());
        if ((requestor instanceof EmployeeModel))
        {
            orderHistoryEntryModel.setEmployee((EmployeeModel)requestor);
        }
        modelService.save(orderHistoryEntryModel);

        promotionsService.transferPromotionsToOrder(order, version, false);
        return orderHistoryEntryModel;
    }

    /**
     * @param promotionsService
     *            the promotionsService to set
     */
    @Required
    public void setPromotionsService(final PromotionsService promotionsService) {
        this.promotionsService = promotionsService;
    }

    /**
     * @param orderHistoryService
     *            the orderHistoryService to set
     */
    @Required
    public void setOrderHistoryService(final OrderHistoryService orderHistoryService) {
        this.orderHistoryService = orderHistoryService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

}

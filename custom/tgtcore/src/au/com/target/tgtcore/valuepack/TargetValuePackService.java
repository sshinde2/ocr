package au.com.target.tgtcore.valuepack;

import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;

import au.com.target.tgtcore.model.TargetValuePackModel;

/**
 * Service providing functions to work with {@link TargetValuePackModel}.
 */
public interface TargetValuePackService {

    /**
     * Returns a model representing a value pack for a given {@code childSku}.
     *
     * @param childSku the SKU to search value pack for
     * @return the value pack (a single product in value pack type)
     * @throws ModelNotFoundException if no record found
     * @throws AmbiguousIdentifierException if multiple records found
     */
    TargetValuePackModel getByChildSku(String childSku) throws ModelNotFoundException, AmbiguousIdentifierException;
}

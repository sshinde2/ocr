package au.com.target.tgtcore.valuepack.dao.impl;

import de.hybris.platform.core.GenericCondition;
import de.hybris.platform.core.GenericQuery;

import au.com.target.tgtcore.genericsearch.TargetGenericSearchService;
import au.com.target.tgtcore.model.TargetValuePackModel;
import au.com.target.tgtcore.valuepack.dao.TargetValuePackDao;

/**
 * Default implementation for {@link TargetValuePackDao}.
 */
public class TargetValuePackDaoImpl implements TargetValuePackDao {

    private TargetGenericSearchService genericSearchService;


    @Override
    public TargetValuePackModel getByChildSku(final String childSku) {
        final GenericQuery query = new GenericQuery(TargetValuePackModel._TYPECODE);
        query.addCondition(GenericCondition.equals(TargetValuePackModel.CHILDSKU, childSku));
        return getGenericSearchService().getSingleResult(query);
    }


    /**
     * Returns the generic search service.
     *
     * @return the generic search service
     */
    public TargetGenericSearchService getGenericSearchService() {
        return genericSearchService;
    }

    /**
     * Sets the generic search service.
     *
     * @param genericSearchService the generic search service to use in this DAO
     */
    public void setGenericSearchService(final TargetGenericSearchService genericSearchService) {
        this.genericSearchService = genericSearchService;
    }
}

package au.com.target.tgtcore.valuepack.dao.impl;

import de.hybris.platform.core.GenericCondition;
import de.hybris.platform.core.GenericQuery;

import au.com.target.tgtcore.genericsearch.TargetGenericSearchService;
import au.com.target.tgtcore.model.TargetValuePackTypeModel;
import au.com.target.tgtcore.valuepack.dao.TargetValuePackTypeDao;

/**
 * Default implementation for {@link TargetValuePackTypeDao}.
 */
public class TargetValuePackTypeDaoImpl implements TargetValuePackTypeDao {

    private TargetGenericSearchService genericSearchService;


    @Override
    public TargetValuePackTypeModel getByLeadSku(final String leadSku) {
        final GenericQuery query = new GenericQuery(TargetValuePackTypeModel._TYPECODE);
        query.addCondition(GenericCondition.equals(TargetValuePackTypeModel.LEADSKU, leadSku));
        return getGenericSearchService().getSingleResult(query);
    }

    /**
     * Returns the generic search service.
     *
     * @return the generic search service
     */
    public TargetGenericSearchService getGenericSearchService() {
        return genericSearchService;
    }

    /**
     * Sets the generic search service.
     *
     * @param genericSearchService the generic search service to use
     */
    public void setGenericSearchService(final TargetGenericSearchService genericSearchService) {
        this.genericSearchService = genericSearchService;
    }
}

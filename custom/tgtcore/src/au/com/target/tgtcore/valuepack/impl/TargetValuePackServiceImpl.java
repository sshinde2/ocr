package au.com.target.tgtcore.valuepack.impl;

import au.com.target.tgtcore.model.TargetValuePackModel;
import au.com.target.tgtcore.valuepack.TargetValuePackService;
import au.com.target.tgtcore.valuepack.dao.TargetValuePackDao;

/**
 * Default implementation for {@link TargetValuePackService}.
 */
public class TargetValuePackServiceImpl implements TargetValuePackService {

    private TargetValuePackDao valuePackDao;


    @Override
    public TargetValuePackModel getByChildSku(final String childSku) {
        return getValuePackDao().getByChildSku(childSku);
    }

    /**
     * Returns the value pack DAO.
     *
     * @return the internally used value pack DAO
     */
    public TargetValuePackDao getValuePackDao() {
        return valuePackDao;
    }

    /**
     * Sets the value pack DAO.
     *
     * @param valuePackDao the value pack DAO to set
     */
    public void setValuePackDao(final TargetValuePackDao valuePackDao) {
        this.valuePackDao = valuePackDao;
    }
}

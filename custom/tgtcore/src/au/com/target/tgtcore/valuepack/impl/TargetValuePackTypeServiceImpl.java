package au.com.target.tgtcore.valuepack.impl;

import au.com.target.tgtcore.model.TargetValuePackTypeModel;
import au.com.target.tgtcore.valuepack.TargetValuePackTypeService;
import au.com.target.tgtcore.valuepack.dao.TargetValuePackTypeDao;

/**
 * Default implementation for {@link TargetValuePackTypeService}.
 */
public class TargetValuePackTypeServiceImpl implements TargetValuePackTypeService {

    private TargetValuePackTypeDao valuePackTypeDao;


    @Override
    public TargetValuePackTypeModel getByLeadSku(final String leadSku) {
        return getValuePackTypeDao().getByLeadSku(leadSku);
    }


    /**
     * Returns the value pack type DAO.
     *
     * @return the value pack type DAO
     */
    public TargetValuePackTypeDao getValuePackTypeDao() {
        return valuePackTypeDao;
    }

    /**
     * Sets the value pack type DAO.
     *
     * @param valuePackTypeDao the value pack type DAO to set
     */
    public void setValuePackTypeDao(final TargetValuePackTypeDao valuePackTypeDao) {
        this.valuePackTypeDao = valuePackTypeDao;
    }
}

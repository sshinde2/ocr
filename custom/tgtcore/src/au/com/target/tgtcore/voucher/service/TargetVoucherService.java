/**
 * 
 */
package au.com.target.tgtcore.voucher.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.voucher.VoucherService;

import java.math.BigDecimal;
import java.util.Collection;

import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;
import au.com.target.tgtcore.model.FlybuysDiscountModel;


/**
 * @author rmcalave
 * 
 */
public interface TargetVoucherService extends VoucherService {

    /**
     * 
     * @param code
     *            the code
     * @return a Collection of {@link FlybuysDiscountModel}s
     */
    Collection<FlybuysDiscountModel> getFlybuysDiscounts(final String code);

    /**
     * Apply the flybuys Discount to the cart
     * 
     * @param cartModel
     *            the cart
     * @return true if successfully applied
     */
    boolean applyFlybuysDiscount(CartModel cartModel, String redeemCode, BigDecimal value, Integer pointsConsumed,
            String securityToken);

    /**
     * Checks if is discount allowed.
     * 
     * @param orderModel
     *            the order model
     * @return the flybuys discount denial dto if not allowed otherwise null
     */
    FlybuysDiscountDenialDto getFlybuysDiscountAllowed(AbstractOrderModel orderModel);

    /**
     * Remove the flybuys Discount from the cart, and remove the voucher from the system.
     * 
     * @param cartModel
     *            The cart
     * @return true if the discount was successfully removed, false otherwise
     */
    boolean removeFlybuysDiscount(final CartModel cartModel);

    /**
     * Returns a FlybuysDiscountModel if there is one, or null if not found.
     * 
     * @param order
     * @return FlybuysDiscountModel
     */
    FlybuysDiscountModel getFlybuysDiscountForOrder(AbstractOrderModel order);

    /**
     * This method checks whether a particular voucher is enabled for online. For all the vouchers it is by default
     * enabled for Online except for PromotionVoucher which is based on the enabledOnline flag. If no voucher found then
     * it returns as disabled.
     * 
     * @param voucherCode
     * @return Boolean - whether voucher is enabledOnline
     */
    boolean isVoucherEnabledOnline(String voucherCode);
}

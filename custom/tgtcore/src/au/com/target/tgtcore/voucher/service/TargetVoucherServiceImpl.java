/**
 * 
 */
package au.com.target.tgtcore.voucher.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.voucher.impl.DefaultVoucherService;
import de.hybris.platform.voucher.jalo.VoucherManager;
import de.hybris.platform.voucher.model.PromotionVoucherModel;
import de.hybris.platform.voucher.model.VoucherModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;
import au.com.target.tgtcore.flybuys.strategy.FlybuysDenialStrategy;
import au.com.target.tgtcore.jalo.TargetVoucherManager;
import au.com.target.tgtcore.model.FlybuysDiscountModel;


/**
 * @author rmcalave
 * 
 */
public class TargetVoucherServiceImpl extends DefaultVoucherService implements TargetVoucherService {

    private static final Logger LOG = Logger.getLogger(TargetVoucherServiceImpl.class);

    private List<FlybuysDenialStrategy> denialStrategies;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.voucher.service.TargetVoucherService#getFlybuysDiscounts(java.lang.String)
     */
    @Override
    public Collection<FlybuysDiscountModel> getFlybuysDiscounts(final String code) {
        return getModelService().getAll(((TargetVoucherManager)VoucherManager.getInstance()).getFlybuysDiscounts(code),
                new ArrayList<FlybuysDiscountModel>());
    }

    @Override
    public boolean redeemVoucher(final String voucherCode, final CartModel cart) throws JaloPriceFactoryException {
        if (getFlybuysDiscountForOrder(cart) == null) {
            return super.redeemVoucher(voucherCode, cart);
        }
        else {
            return false;
        }
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.service.FlybuysDiscountService#applyFlybuysDiscount(de.hybris.platform.core.model.order.CartModel, java.lang.String, java.math.BigDecimal, java.lang.Integer, java.lang.String)
     */
    @Override
    public boolean applyFlybuysDiscount(final CartModel cartModel, final String redeemCode, final BigDecimal value,
            final Integer pointsConsumed, final String securityToken) {
        // If the cart has any vouchers don't apply flybuys Discount
        if (CollectionUtils.isNotEmpty(getAppliedVouchers(cartModel))) {
            return false;
        }

        // If the cart value (excluding the delivery fee) is less than the discount amount
        // don't apply flybuys Discount
        final BigDecimal subtotal = BigDecimal.valueOf(cartModel.getSubtotal().doubleValue());
        if (subtotal.compareTo(value) < 0) {
            return false;
        }

        final String discountCode = (cartModel.getCode() + redeemCode).toUpperCase();

        FlybuysDiscountModel flybuysDiscountModel = null;

        // Check if a flybuys Discount for this redeem code already exists. If it does, get it and we'll update it.
        final Collection<FlybuysDiscountModel> flybuysDiscountsForCode = getFlybuysDiscounts(
                discountCode);
        if (CollectionUtils.isNotEmpty(flybuysDiscountsForCode)) {
            flybuysDiscountModel = flybuysDiscountsForCode.iterator().next();
        }

        // If there isn't a flybuys Discount for this redeem code already, create one.
        if (flybuysDiscountModel == null) {
            flybuysDiscountModel = getModelService().create(FlybuysDiscountModel.class);
            flybuysDiscountModel.setCode(discountCode);
        }
        flybuysDiscountModel.setFlybuysCardNumber(cartModel.getFlyBuysCode());
        flybuysDiscountModel.setRedeemCode(redeemCode);
        flybuysDiscountModel.setValue(Double.valueOf(value.doubleValue()));
        flybuysDiscountModel.setCurrency(cartModel.getCurrency());
        flybuysDiscountModel.setPointsConsumed(pointsConsumed);
        flybuysDiscountModel.setSecurityToken(securityToken);

        save(flybuysDiscountModel);

        try {
            final boolean result = redeemVoucher(flybuysDiscountModel.getCode(), cartModel);
            if (result) {
                cartModel.setFlybuysPointsConsumed(pointsConsumed);
                getModelService().save(cartModel);
                getModelService().refresh(cartModel);
                cartModel.setCalculated(Boolean.FALSE);
            }
            return result;
        }
        catch (final JaloPriceFactoryException ex) {
            LOG.warn("Unable to redeem flybuys discount", ex);
            return false;
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.service.FlybuysDiscountService#getFlybuysDiscountForOrder(de.hybris.platform.core.model.order.AbstractOrderModel)
     */
    @Override
    public FlybuysDiscountModel getFlybuysDiscountForOrder(final AbstractOrderModel orderModel) {
        Assert.notNull(orderModel, "Ordermodel cannot not be null");
        final Collection<DiscountModel> vouchers = getAppliedVouchers(orderModel);
        if (CollectionUtils.isNotEmpty(vouchers)) {
            for (final DiscountModel discountModel : vouchers) {
                if (discountModel instanceof FlybuysDiscountModel) {
                    return (FlybuysDiscountModel)discountModel;
                }
            }
        }
        return null;
    }

    @Override
    public FlybuysDiscountDenialDto getFlybuysDiscountAllowed(final AbstractOrderModel orderModel) {

        final List<FlybuysDenialStrategy> strategies = getDenialStrategies();
        if (CollectionUtils.isNotEmpty(strategies)) {
            for (final FlybuysDenialStrategy strategy : strategies) {

                final FlybuysDiscountDenialDto flybuysDiscountDenialDto = strategy.isFlybuysDiscountAllowed(orderModel);
                if (flybuysDiscountDenialDto != null) {
                    return flybuysDiscountDenialDto;
                }
            }
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtcore.flybuys.service.FlybuysDiscountService#removeFlybuysDiscount(de.hybris.platform.core.model.order.CartModel)
     */
    @Override
    public boolean removeFlybuysDiscount(final CartModel cartModel) {
        final FlybuysDiscountModel appliedVoucher = getFlybuysDiscountForOrder(cartModel);

        if (null == appliedVoucher) {
            return true;
        }

        try {
            releaseVoucher(appliedVoucher.getCode(), cartModel);
            delete(appliedVoucher);
            cartModel.setFlybuysPointsConsumed(null);
            cartModel.setCalculated(Boolean.FALSE);
            getModelService().save(cartModel);
            getModelService().refresh(cartModel);
            return true;
        }
        catch (final JaloPriceFactoryException ex) {
            LOG.error("Unable to remove flybuys Discount from cart " + cartModel.getCode(), ex);
        }

        return false;
    }

    @Override
    public boolean isVoucherEnabledOnline(final String voucherCode) {
        final VoucherModel voucherModel = getVoucher(voucherCode);
        if (null == voucherModel) {
            return false;
        }
        if (!(voucherModel instanceof PromotionVoucherModel)) {
            return true;
        }
        final PromotionVoucherModel promotionVoucherModel = (PromotionVoucherModel)voucherModel;
        if (BooleanUtils.isTrue(promotionVoucherModel.getEnabledOnline())) {
            return true;
        }
        return false;
    }

    /**
     * @return the denialStrategies
     */
    protected List<FlybuysDenialStrategy> getDenialStrategies() {
        return denialStrategies;
    }

    /**
     * @param denialStrategies
     *            the denialStrategies to set
     */
    @Required
    public void setDenialStrategies(final List<FlybuysDenialStrategy> denialStrategies) {
        this.denialStrategies = denialStrategies;
    }

}

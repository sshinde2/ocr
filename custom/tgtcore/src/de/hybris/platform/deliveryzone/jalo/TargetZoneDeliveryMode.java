package de.hybris.platform.deliveryzone.jalo;

import de.hybris.platform.jalo.ConsistencyCheckException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.type.ComposedType;


/**
 * 
 * Intentionally keeping this class in 'de.hybris.platform.*' package to override the method
 * {@link ZoneDeliveryMode#checkAllowedNewValue(Currency, Zone, double, double)} with default access level.
 * 
 * 
 */
public class TargetZoneDeliveryMode extends GeneratedTargetZoneDeliveryMode {
    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.deliveryzone.jalo.ZoneDeliveryMode#checkAllowedNewValue(de.hybris.platform.jalo.c2l.Currency, de.hybris.platform.deliveryzone.jalo.Zone, double, double)
     */
    @Override
    void checkAllowedNewValue(final Currency curr, final Zone zone, final double min, final double value)
            throws ConsistencyCheckException {
        // Not doing anything here as the uniqueattributesinerceptor should kick in and perform the necessary  validations
    }

}

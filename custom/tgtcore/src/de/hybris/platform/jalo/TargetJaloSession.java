package de.hybris.platform.jalo;

import de.hybris.platform.util.Config;


/**
 * 
 * Customized to enable conditional removal of session cart during session close Intentionally keeping this namespace to
 * access one of the protected methods(removeFromSessionCache()) from JaloConnection.
 * 
 */
public class TargetJaloSession extends JaloSession {

    @Override
    protected void cleanup()
    {
        if (Config.getBoolean("session.remove.cart.on.close", true)) {
            super.cleanup();
        }
        else {
            // Do not remove session cart from db but remove this session from session cache so it becomes eligible for GC
            getTenant().getJaloConnection().removeFromSessionCache(this);
        }

    }

}

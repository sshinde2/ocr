package de.hybris.platform.promotions.jalo;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.promotions.util.Helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import au.com.target.tgtcore.deals.wrappers.DealWrapper;
import au.com.target.tgtcore.deals.wrappers.ProductWrapper;
import au.com.target.tgtcore.enums.DealItemTypeEnum;


//*************************************************************************
// Buy and Get, where qualifier and reward list are not the same.
// Example of this type of deal:
//          buy 2 pairs of pants and get a shirt for free
//          buy a pair of shoes and get 25% off the socks
//          buy a game console and get a game for $10.00
//
// The Qualifing record specifies the minimum quantity of item required to
//  achieve the reward.  The can only be one Qualifing record.
// Reward types that are valid are:
//          DOLLAR_OFF_EACH
//          FIXED_DOLLAR_EACH
//          PERCENT_OFF_EACH
// The reward quantity is the maximum number of reward items for each
//  instance.
// If the reward is a DOLLAR_OFF_EACH, the final value of the item cannot
//  be negative, if the reward is more than the value of the item then the
//  item will given for free.
// If the reward is a FIXED_DOLLAR_EACH then we will only reduce the value
//  of an item not increase it.
public class BuyGetDeal extends GeneratedBuyGetDeal {
    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }

    //CHECKSTYLE:OFF allow the long method, so we can keep the same structure as POS method (to make easier if they change in the future)
    @Override
    protected List<PromotionResult> applyDeal(final DealWrapper wrappedDeal,
            final List<ProductWrapper> wrappedProducts) {
        //CHECKSTYLE:ON
        final ProductWrapper[] getList = getArrayOfSortedRewardProducts(wrappedProducts, wrappedDeal);

        // make a list of the buy (qualifing) items
        // start with the items that are not also reward items,
        // then have the common items from most expensive to least
        int buyCnt = 0;
        for (final ProductWrapper product : wrappedProducts) {
            if (product.getQualifierModel() != null) {
                buyCnt++;
            }
        }
        final ProductWrapper buyList[] = new ProductWrapper[buyCnt];
        buyCnt = 0;
        // start with qualifing items that are NOT reward items
        for (final ProductWrapper product : wrappedProducts) {
            if ((product.getQualifierModel() != null) && !product.isRewardProduct()) {
                buyList[buyCnt++] = product;
            }
        }
        // Now the items that are both qualifing and reward,
        // the getList is already sorted for us so we can use it,
        // however it is in reverse order.
        for (int cnt = getList.length - 1; cnt >= 0; cnt--) {
            if (getList[cnt].getQualifierModel() != null) {
                buyList[buyCnt++] = getList[cnt];
            }
        }

        int instCnt = 0;
        int getMax = getRewardMaxQtyAsPrimitive();
        if (getMax == 0) {
            // setting get to the number of items means that all get items
            // will be given as a reward on the first deal instance.
            getMax = getList.length;
        }
        final long dealAmt = Math.round(getRewardValueAsPrimitive() * 100);
        // A reminder in case you have forgotten,
        // getList we have the items sorted least expensive
        // to most expensive, and the buy list we have the
        // buy only item first, then the common (buy and get)
        // items sorted most expensive to least expensive.
        // That means we get the buy items from the start of
        // the list moving to the end, and the get items from
        // the end of the list moving to the start.
        int getCnt = (getList.length - 1);
        buyCnt = 0;
        final int minQty = wrappedDeal.getQualifierModelList().get(0).getMinQty().intValue();
        int numberofRewardsSofar = 0;
        int partialInstCnt = 0;

        int given;
        for (;;) {// looping around until we've met all possible instances of this deal in the transaction

            //if  all qualifying items and no reward in basket build results of couldhave more rewards
            if (getList.length < 1) {
                if (buyList.length >= minQty) {
                    return buildCouldHaveFiredResultListForAllQualifiersButNoRewards(wrappedProducts, wrappedDeal,
                            calcQtyCertainty(wrappedProducts), getMax);
                }

            }
            if (getCnt < 0) {
                List<PromotionResult> promotionResults = new ArrayList<>();
                // return the list of deal instances applied
                if (instCnt > 0) {
                    //if the deal is partially consumed,that is still more rewards can be added
                    if ((partialInstCnt > 0) && (instCnt > 1)) {
                        final List<ProductWrapper> otherProducts = new ArrayList<>();
                        final List<ProductWrapper> partialRewardProducts = createPartailRewardProductList(
                                wrappedProducts, otherProducts);
                        promotionResults = buildFiredResultListWithRewards(partialRewardProducts, wrappedDeal,
                                partialInstCnt,
                                numberofRewardsSofar, getMax);
                        promotionResults.addAll(buildFiredResultListWithRewards(otherProducts, wrappedDeal,
                                instCnt - partialInstCnt,
                                getMax, getMax));
                        return promotionResults;
                    }
                    else {
                        promotionResults.addAll(buildFiredResultListWithRewards(wrappedProducts, wrappedDeal, instCnt,
                                numberofRewardsSofar, getMax));
                    }

                }
                //no more qualifiers and rewards
                else if (buyCnt > buyList.length) {
                    promotionResults = new ArrayList<>();
                }
                final float certainty = calcQtyCertainty(wrappedProducts);
                if (certainty >= 0.0001) {
                    promotionResults.addAll(buildCouldHaveFiredResultListForAllQualifiersButNoRewards(wrappedProducts,
                            wrappedDeal,
                            certainty, getMax));
                }
                return promotionResults;
            }

            // we could at this point check if the adjust type is fixed dollar each
            // if it was we could check the next item in the get list and if
            // it was less than the deal amount we could return as all the
            // remaining instances will have no value, but the business decided
            // they wanted to see the deal on the receipt even if there is
            // no value to that deal.

            // note at this point it still may be possible that we cannot meet
            // a deal, this would occur when one of these reward items is also needed
            // to be one of the qualifiers.  Too difficult to check now, we will
            // just deal with that later

            // see if there are enough qualifiers for another instance

            int sumQty = 0;
            for (int itemLoop = buyCnt; itemLoop < buyList.length; itemLoop++) {
                if ((buyList[itemLoop].getQualifierModel() != null) &&
                        !buyList[itemLoop].isInDeal()) {
                    sumQty++;
                }
            }

            //number of qualifiers in basket are less than min number of qualifiers required
            if (sumQty < minQty) {
                List<PromotionResult> resultList = null;
                if (instCnt > 0) {
                    //if not  all rewards are added
                    resultList = buildFiredResultListWithRewards(wrappedProducts, wrappedDeal, instCnt,
                            numberofRewardsSofar, getMax);
                }
                // product that could have met another deal if we had more products
                final float certainty = (float)sumQty / (float)minQty;
                if (resultList != null && (certainty > 0.0001 || !(getCnt < 0))) {
                    resultList.addAll(buildCouldHaveFiredResultList(wrappedProducts, wrappedDeal, certainty));
                }
                return resultList;
            }

            // we are here so we know we have enough qualifing items
            // for another instance of this deal (and probably enough reward items)
            instCnt++;

            // set the qualifing items as part of the deal
            sumQty = 0;
            for (int itemLoop = buyCnt; itemLoop < buyList.length; itemLoop++) {
                if ((buyList[itemLoop].getQualifierModel() != null)
                        && !buyList[itemLoop].isInDeal()) {
                    buyList[itemLoop].setDealApplied(instCnt, DealItemTypeEnum.QUALIFIER, 0);
                    buyList[itemLoop].setDealRewardsPartiallyConsumed();
                    sumQty++;
                    buyCnt++;
                    if (sumQty == minQty) {
                        break;
                    }
                }
            }

            // update the reward items
            for (given = 0; (given < getMax) && (getCnt >= 0); getCnt--) {
                if (getList[getCnt].isInDeal()) {
                    continue;
                }

                final long unitPrice = getList[getCnt].getUnitSellPrice();
                final long tmp;
                switch (wrappedDeal.getDealType()) {
                    case DOLLAR_OFF_EACH:
                        if (dealAmt < unitPrice) {
                            getList[getCnt].setDealApplied(instCnt, DealItemTypeEnum.REWARD, dealAmt);
                            getList[getCnt].setDealRewardsPartiallyConsumed();
                        }
                        else {
                            getList[getCnt].setDealApplied(instCnt, DealItemTypeEnum.REWARD, unitPrice);
                            getList[getCnt].setDealRewardsPartiallyConsumed();
                        }
                        given++;
                        break;
                    case FIXED_DOLLAR_EACH:
                        if (dealAmt < unitPrice) {
                            getList[getCnt].setDealApplied(instCnt, DealItemTypeEnum.REWARD, unitPrice - dealAmt);
                            getList[getCnt].setDealRewardsPartiallyConsumed();
                        }
                        else {
                            getList[getCnt].setDealApplied(instCnt, DealItemTypeEnum.REWARD, 0);
                            getList[getCnt].setDealRewardsPartiallyConsumed();
                        }
                        given++;
                        break;
                    case PERCENT_OFF_EACH:
                        tmp = DealHelper.calcDiscTruncated(unitPrice, (int)dealAmt);
                        getList[getCnt].setDealApplied(instCnt, DealItemTypeEnum.REWARD, tmp);
                        getList[getCnt].setDealRewardsPartiallyConsumed();
                        given++;
                        break;
                    default:
                        break;
                }
            }
            if (given == 0) {
                // there was no items to give the reward for this instance
                // so we need to clear the qualifing list flags and update the instCnt
                for (int cnt = 0; cnt < buyList.length; cnt++) {
                    if (buyList[cnt].getInstance() == instCnt) {
                        buyList[cnt].clearDeal();
                    }
                }
                instCnt--;
            }
            else {
                numberofRewardsSofar = given;
                if (given < getMax) {
                    partialInstCnt++;
                }
                else {
                    clearDealPartiallyConsumed(buyList, getList, instCnt);
                }
            }
        }
    }

    @Override
    public String getResultDescription(final SessionContext ctx, final PromotionResult promotionResult,
            final Locale locale) {
        final AbstractOrder order = promotionResult.getOrder(ctx);
        if (order != null) {
            final Currency orderCurrency = order.getCurrency(ctx);
            final List<DealQualifier> qualList = getQualifierList();
            final int maxRewardQty = getRewardMaxQtyAsPrimitive(ctx);
            int qualCnt = 0;
            for (final DealQualifier dealQualifier : qualList) {
                qualCnt += dealQualifier.getMinQtyAsPrimitive();
            }
            if (promotionResult.getFired(ctx)) {


                if ((promotionResult instanceof TargetDealWithRewardResult)
                        && ((TargetDealWithRewardResult)promotionResult).getCouldhaveMoreRewards(ctx)) {
                    final int numberOfRewardsSofar = ((TargetDealWithRewardResult)promotionResult)
                            .getNumberofRewardsSofarAsPrimitive();
                    final Object[] args = { Integer.valueOf(numberOfRewardsSofar),
                            Integer.valueOf(maxRewardQty - numberOfRewardsSofar) };
                    return formatMessage(this.getMessageCouldHaveMoreRewards(ctx), args, locale);
                }

                else {
                    final double totalDiscount = promotionResult.getTotalDiscount(ctx);

                    final Object[] args = {
                            Double.valueOf(totalDiscount),
                            Helper.formatCurrencyAmount(ctx, locale, orderCurrency, totalDiscount),
                            ((TargetDealResult)promotionResult).getInstanceCount()
                    };
                    return formatMessage(this.getMessageFired(ctx), args, locale);
                }
            }
            else if (promotionResult.getCouldFire(ctx)) {

                if (promotionResult instanceof TargetDealWithRewardResult) {
                    if (((TargetDealWithRewardResult)promotionResult).getCouldhaveMoreRewards(ctx)) {
                        final int numberOfRewardsSofar = ((TargetDealWithRewardResult)promotionResult)
                                .getNumberofRewardsSofarAsPrimitive();

                        final Object[] args = { Integer.valueOf(numberOfRewardsSofar),
                                Integer.valueOf(maxRewardQty - numberOfRewardsSofar) };
                        return formatMessage(this.getMessageCouldHaveMoreRewards(ctx), args, locale);
                    }

                }
                final int foundCnt = Math.round(qualCnt * promotionResult.getCertaintyAsPrimitive());

                final Object[] args = {
                        Integer.valueOf(qualCnt),
                        Integer.valueOf(foundCnt),
                        Integer.valueOf(qualCnt - foundCnt)
                };
                return formatMessage(this.getMessageCouldHaveFired(ctx), args, locale);

            }

        }
        return "";
    }

    /**
     * This method clears the partiallyConsumedflag
     * 
     * @param buyList
     * @param getList
     * @param instance
     */
    protected void clearDealPartiallyConsumed(final ProductWrapper buyList[], final ProductWrapper getList[],
            final int instance) {
        for (int cnt = 0; cnt < buyList.length; cnt++) {
            if (buyList[cnt].getInstance() == instance) {
                buyList[cnt].clearDealRewardsPartiallyConsumed();
            }
            for (int gCnt = 0; gCnt < getList.length; gCnt++) {
                if (getList[gCnt].getInstance() == instance) {
                    getList[gCnt].clearDealRewardsPartiallyConsumed();
                }
            }
        }
    }

    /**
     * This create two list of productWraperlist ,one with the dealpartially consumed and other with rest of the
     * products
     * 
     * @param wrappedProducts
     * @param productList
     * @return partiallyConsumed
     */
    protected List<ProductWrapper> createPartailRewardProductList(final List<ProductWrapper> wrappedProducts,
            final List<ProductWrapper> productList) {
        final List<ProductWrapper> partiallyConsumed = new ArrayList<>();
        for (final ProductWrapper product : wrappedProducts) {
            if (product.isInDeal() && product.getDealRewardsPartiallyConsumed()) {
                partiallyConsumed.add(product);

            }
            else {
                productList.add(product);
            }
        }
        return partiallyConsumed;

    }
}
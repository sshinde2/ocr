/**
 * 
 */
package de.hybris.platform.promotions.jalo;

/******************************************************************************
 * For use with TgtMath.apportionDiscount.
 * 
 * @see DealHelper#apportionDiscount(DealHelperDiscountableInterface[], long) <code>apportionDiscount</code>.
 */
public class DealHelperDiscountableAdaptor implements DealHelperDiscountableInterface {
    protected long preDiscAmt;
    protected long discAmt;

    //*************************************************************************
    public DealHelperDiscountableAdaptor(final long preDiscAmt) {
        super();
        this.preDiscAmt = preDiscAmt;
        discAmt = 0;
    }


    //*************************************************************************
    @Override
    public long getPreDiscAmt() {
        return preDiscAmt;
    }


    //*************************************************************************
    @Override
    public long getDiscAmt() {
        return discAmt;
    }


    //*************************************************************************
    @Override
    public void setDiscAmt(final long discAmt) {
        this.discAmt = discAmt;
    }

}

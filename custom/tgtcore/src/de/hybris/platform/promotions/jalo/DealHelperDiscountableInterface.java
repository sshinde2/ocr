/**
 * 
 */
package de.hybris.platform.promotions.jalo;

/******************************************************************************
 * For use with TgtMath.apportionDiscount.
 * 
 * @see DealHelper#apportionDiscount(DealHelperDiscountableInterface[], long) <code>apportionDiscount</code>.
 */
public interface DealHelperDiscountableInterface {
    /**
     * Get the amount prior to discount
     * 
     * @return amount in cents
     */
    long getPreDiscAmt();

    /**
     * Get the amount of the discount
     * 
     * @return amount in cents
     */
    long getDiscAmt();

    /**
     * Set the amount of the discount
     * 
     * @param amount
     *            in cents
     */
    void setDiscAmt(long amount);
}

package de.hybris.platform.promotions.jalo;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.promotions.util.Helper;

import java.util.List;
import java.util.Locale;

import au.com.target.tgtcore.deals.wrappers.DealWrapper;
import au.com.target.tgtcore.deals.wrappers.ProductWrapper;
import au.com.target.tgtcore.enums.DealItemTypeEnum;


//*************************************************************************
// Spend and save
// Example of this type of deal:
//          spend $100.00 in manchester and save 10% off manchester
//          spend $20.00 in DVDs and save $5.00 off those DVDs
//
// Spend and save deals only have a qualifing list, as the items that
//  qualify are also the items that receive the rewards.
//
// Only percent of total and dollar off total type savings are allowed. Note
//  however that a percent of each and percent off total mathamatically are
//  identical (exept for a few rounding cents which we shall ignore).
public class SpendAndSaveDeal extends GeneratedSpendAndSaveDeal {
    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }

    @Override
    protected List<PromotionResult> applyDeal(final DealWrapper wrappedDeal, final List<ProductWrapper> wrappedProducts) {
        final int instCnt = 1;
        final long dealAmt = Math.round(getRewardValueAsPrimitive() * 100);

        // by the time we get here we already know we qualify so we know we
        // have a deal, all all that is left to do is apply the savings
        switch (wrappedDeal.getDealType()) {
            case PERCENT_OFF_EACH:
                for (final ProductWrapper thisItem : wrappedProducts) {
                    final long tmp = DealHelper.calcDiscTruncated(thisItem.getUnitSellPrice(), (int)dealAmt);
                    thisItem.setDealApplied(instCnt, DealItemTypeEnum.REWARD, tmp);
                }
                break;
            case DOLLAR_OFF_TOTAL:
                final MathDisc[] mathDisc = new MathDisc[wrappedProducts.size()];
                int cnt = 0;
                for (final ProductWrapper thisItem : wrappedProducts) {
                    mathDisc[cnt++] = new MathDisc(thisItem)
                    {
                        @Override
                        public void setDiscAmt(final long amount) {
                            getItem().setDealApplied(instCnt, DealItemTypeEnum.REWARD, amount);
                        }
                    };
                }
                DealHelper.apportionDiscount(mathDisc, dealAmt);
                break;
            default:
                // should not happen
                break;
        }

        return buildFiredResultList(wrappedProducts, wrappedDeal, instCnt);
    }

    @SuppressWarnings("deprecation")
    @Override
    public String getResultDescription(final SessionContext ctx, final PromotionResult promotionResult,
            final Locale locale) {
        final AbstractOrder order = promotionResult.getOrder(ctx);
        if (order != null) {
            final Currency orderCurrency = order.getCurrency(ctx);
            final List<DealQualifier> qualList = getQualifierList();

            if (promotionResult.getFired(ctx)) {
                final double totalDiscount = promotionResult.getTotalDiscount(ctx);

                final Object[] args = {
                        Double.valueOf(totalDiscount),
                        Helper.formatCurrencyAmount(ctx, locale, orderCurrency, totalDiscount)
                };
                return formatMessage(this.getMessageFired(ctx), args, locale);
            }
            else if (promotionResult.getCouldFire(ctx)) {

                double qualAmt = 0;
                for (final DealQualifier dealQualifier : qualList) {
                    qualAmt += dealQualifier.getMinAmtAsPrimitive();
                }
                final double foundAmt = qualAmt * promotionResult.getCertaintyAsPrimitive();
                final double shortAmt = qualAmt - foundAmt;

                final Object[] args = {
                        Double.valueOf(qualAmt),
                        Helper.formatCurrencyAmount(ctx, locale, orderCurrency, qualAmt),
                        Double.valueOf(foundAmt),
                        Helper.formatCurrencyAmount(ctx, locale, orderCurrency, foundAmt),
                        Double.valueOf(shortAmt),
                        Helper.formatCurrencyAmount(ctx, locale, orderCurrency, shortAmt)
                };
                return formatMessage(this.getMessageCouldHaveFired(ctx), args, locale);
            }
        }
        return "";
    }
}

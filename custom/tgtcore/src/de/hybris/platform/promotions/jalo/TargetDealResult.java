package de.hybris.platform.promotions.jalo;


import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.order.Order;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloAbstractTypeException;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import de.hybris.platform.jalo.type.TypeManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;


@SuppressWarnings("deprecation")
public class TargetDealResult extends GeneratedTargetDealResult {

    private static final Logger LOG = Logger.getLogger(TargetDealResult.class);

    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }

    /**
     * Creates a deep clone of this promotion result and attach it to the specified order.
     * 
     * @param ctx
     *            The hybris context
     * @param target
     *            the target order to transfer to
     * @return the cloned promotion result
     */
    @Override
    PromotionResult transferToOrder(final SessionContext ctx, final Order target)
    {
        if (LOG.isDebugEnabled())
        {
            LOG.debug("(" + getPK() + ") transferToOrder [" + this + "] order=[" + target + "]");
        }

        if (ctx != null && target != null)
        {
            final AbstractPromotion promotion = getPromotion(ctx);
            if (promotion != null)
            {
                // The order must hold an immutable copy of the promotion, so we try to find an existing
                // immutable clone, failing that we create a new clone
                final AbstractPromotion dupPromotion = promotion.findOrCreateImmutableClone(ctx);
                if (dupPromotion != null)
                {
                    // We always need to deep clone the actions and consumed order entries so that they are
                    // associated with the target order.
                    final Collection<AbstractPromotionAction> dupActions = deepCloneAllActions(ctx);
                    final Collection<PromotionOrderEntryConsumed> dupConsumedEntries = deepCloneConsumedEntriesAndAttachToOrder(
                            ctx,
                            target);

                    // Create the new PromotionResult
                    PromotionResult dupPromotionResult = null;

                    final Map promotionResultValues = new HashMap();
                    promotionResultValues.put(CERTAINTY, getCertainty(ctx));
                    promotionResultValues.put(PROMOTION, dupPromotion);
                    promotionResultValues.put(ORDER, target);
                    promotionResultValues.put(ACTIONS, dupActions);
                    promotionResultValues.put(CONSUMEDENTRIES, dupConsumedEntries);
                    promotionResultValues.put(INSTANCECOUNT, getInstanceCount(ctx));

                    final ComposedType type = TypeManager.getInstance().getComposedType(TargetDealResult.class);
                    try
                    {
                        dupPromotionResult = (PromotionResult)type.newInstance(ctx, promotionResultValues);
                    }
                    catch (final JaloGenericCreationException ex)
                    {
                        LOG.warn("(" + getPK() + ") transferToOrder: failed to create instance of PromotionResult", ex);
                    }
                    catch (final JaloAbstractTypeException ex)
                    {
                        LOG.warn("(" + getPK() + ") transferToOrder: failed to create instance of PromotionResult", ex);
                    }

                    if (dupPromotionResult != null)
                    {
                        // Associate the duplicated PromotionOrderEntryConsumed with the duplicated PromotionResult
                        for (final PromotionOrderEntryConsumed dupConsumedEntry : dupConsumedEntries)
                        {
                            dupConsumedEntry.setPromotionResult(ctx, dupPromotionResult);
                        }
                    }
                    return dupPromotionResult;
                }
            }
        }
        return null;
    }

    /**
     * Clones all the actions attached to this promotion result.
     * 
     * @param ctx
     *            the hybris context
     * @return the cloned actions
     */
    protected Collection<AbstractPromotionAction> deepCloneAllActions(final SessionContext ctx)
    {
        final Collection<AbstractPromotionAction> dupActions = new ArrayList<>();

        final Collection<AbstractPromotionAction> actions = getActions(ctx);
        if (actions != null && !actions.isEmpty())
        {
            for (final AbstractPromotionAction a : actions)
            {
                dupActions.add(a.deepClone(ctx));
            }
        }

        return dupActions;
    }

    /**
     * Clones of the consumed order entries attached to this promotion result and attach them to order entries in the
     * specified order.
     * 
     * @param ctx
     *            the hybris context
     * @param target
     *            the target order to attach the consumed order entries to
     * @return the cloned order entries
     */
    protected Collection<PromotionOrderEntryConsumed> deepCloneConsumedEntriesAndAttachToOrder(
            final SessionContext ctx,
            final Order target)
    {
        final Collection<PromotionOrderEntryConsumed> dupConsumedEntries = new ArrayList<>();

        // Get all the order entries in the target order
        final List<AbstractOrderEntry> allTargetEntries = target.getAllEntries();

        final Collection<PromotionOrderEntryConsumed> consumedEntries = getConsumedEntries(ctx);
        if (consumedEntries != null && !consumedEntries.isEmpty())
        {
            for (final PromotionOrderEntryConsumed poe : consumedEntries)
            {
                final PromotionOrderEntryConsumed consumedEntry = deepCloneConsumedEntryAndAttachToOrder(ctx, poe,
                        allTargetEntries);
                if (consumedEntry != null)
                {
                    dupConsumedEntries.add(consumedEntry);
                }
            }
        }

        return dupConsumedEntries;
    }

    /**
     * Clones a consumed order entry and attach it to an order entry from the list specified.
     * 
     * @param ctx
     *            the hybris context
     * @param source
     *            the consumed entry to clone
     * @param allTargetEntries
     *            the list of order entries in the target order
     * @return the cloned consumed order entry
     */
    private static PromotionOrderEntryConsumed deepCloneConsumedEntryAndAttachToOrder(final SessionContext ctx,
            final PromotionOrderEntryConsumed source, final List<AbstractOrderEntry> allTargetEntries)
    {
        final AbstractOrderEntry sourceOrderEntry = source.getOrderEntry(ctx);
        if (sourceOrderEntry != null)
        {
            final int entryNumber = sourceOrderEntry.getEntryNumber().intValue();

            // Get target order entry with same entry number. This is the same order entry as our sourceOrderEntry but attached to the target order
            final AbstractOrderEntry targetOrderEntry = findOrderEntryWithEntryNumber(allTargetEntries, entryNumber);
            if (targetOrderEntry == null)
            {
                // Cannot find matching entry with same entry number
                LOG.warn("cloneConsumedEntryToOrder source=[" + source
                        + "] cannot find matching order entry with entryNumber=["
                        + entryNumber + "]");
            }
            else
            {
                // Check that the targetOrderEntry has the same product and quantity as our sourceOrderEntry
                if (!sourceOrderEntry.getProduct(ctx).equals(targetOrderEntry.getProduct(ctx)))
                {
                    LOG.warn("transferToOrder source=[" + source + "] order entry with entryNumber=[" + entryNumber
                            + "] has different product. expected=[" + sourceOrderEntry.getProduct(ctx) + "] actual=["
                            + targetOrderEntry.getProduct(ctx) + "]");
                }
                else if (!sourceOrderEntry.getQuantity(ctx).equals((targetOrderEntry.getQuantity(ctx))))
                {
                    LOG.warn("transferToOrder source=[" + source + "] order entry with entryNumber=[" + entryNumber
                            + "] has different quantity. expected=[" + sourceOrderEntry.getQuantity(ctx) + "] actual=["
                            + targetOrderEntry.getQuantity(ctx) + "]");
                }
                else if (source instanceof TargetPromotionOrderEntryConsumed) {
                    final TargetPromotionOrderEntryConsumed entryConsumedSource = (TargetPromotionOrderEntryConsumed)source;
                    // Create a new consumed order entry attached to the target order entry
                    final TargetPromotionOrderEntryConsumed orderEntryConsumed = (TargetPromotionOrderEntryConsumed)PromotionsManager
                            .getInstance().createPromotionOrderEntryConsumed(ctx,
                                    entryConsumedSource.getCode(ctx),
                                    targetOrderEntry, entryConsumedSource.getQuantity(ctx).longValue(),
                                    entryConsumedSource.getAdjustedUnitPrice(ctx).doubleValue());
                    orderEntryConsumed.setInstance(entryConsumedSource.getInstance());
                    orderEntryConsumed.setDealItemType(entryConsumedSource.getDealItemType());
                    return orderEntryConsumed;
                }
                else
                {
                    // Create a new consumed order entry attached to the target order entry
                    return PromotionsManager.getInstance().createPromotionOrderEntryConsumed(ctx, source.getCode(ctx),
                            targetOrderEntry, source.getQuantity(ctx).longValue(),
                            source.getAdjustedUnitPrice(ctx).doubleValue());
                }
            }
        }
        return null;
    }

    /**
     * Finds the order entry with the specified entry number.
     * 
     * @param allTargetEntries
     *            the list of order entries
     * @param entryNumber
     *            the entry number to search for
     * @return the order entry with the entry number specified or null of not found.
     */
    protected static AbstractOrderEntry findOrderEntryWithEntryNumber(final List<AbstractOrderEntry> allTargetEntries,
            final int entryNumber)
    {
        for (final AbstractOrderEntry entry : allTargetEntries)
        {
            if (entryNumber == entry.getEntryNumber().intValue())
            {
                return entry;
            }
        }
        return null;
    }

}

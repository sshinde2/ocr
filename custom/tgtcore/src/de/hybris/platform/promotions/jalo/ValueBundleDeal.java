package de.hybris.platform.promotions.jalo;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.promotions.util.Helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import au.com.target.tgtcore.deals.wrappers.DealWrapper;
import au.com.target.tgtcore.deals.wrappers.ProductWrapper;
import au.com.target.tgtcore.enums.DealItemTypeEnum;


//*************************************************************************
// Value bundle.
// Example of this type of deal:
//          buy a playstation and 2 games for $1000.00
//          buy a TV and DVD player for $200.00 save $50.00
//
// All items are in the qualifying lists, no reward lists required.
// A minimum markdown amount may be specified, in this case they final sell
//  price may be less than the specified "get" value to ensure the save
//  amount is met.  However the items will never be negative.
// The most expensive qualifying items get picked first, just so the
//  customer gets the better deal.
public class ValueBundleDeal extends GeneratedValueBundleDeal {
    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }

    //CHECKSTYLE:OFF allow the long method, so we can keep the same structure as POS method (to make easier if they change in the future)
    @Override
    protected List<PromotionResult> applyDeal(final DealWrapper wrappedDeal,
            final List<ProductWrapper> wrappedProducts) {
        //CHECKSTYLE:ON
        final ProductWrapper[] getList;
        getList = getArrayOfSortedProducts(wrappedProducts, wrappedDeal);
        int instCnt = 0;
        int minQty, sumQty, spendAmt;
        final long dealAmt = Math.round(getRewardValueAsPrimitive() * 100);
        for (;;) {
            spendAmt = 0; // spend amount is the original cost of the qualifying items
            for (final DealQualifierModel qualModel : wrappedDeal.getQualifierModelList()) {
                minQty = qualModel.getMinQty() == null ? 0 : qualModel.getMinQty().intValue();
                sumQty = 0;
                for (int itemLoop = getList.length - 1; itemLoop >= 0; itemLoop--) {

                    if (getList[itemLoop].isInDeal()) {
                        continue;
                    }

                    if (!qualModel.equals(getList[itemLoop].getQualifierModel())) {
                        continue;
                    }

                    spendAmt += getList[itemLoop].getUnitSellPrice();
                    sumQty++;
                    if (sumQty == minQty) {
                        break;
                    }
                }
                if (sumQty < minQty) {
                    // deal cannot be met again

                    // list of deal instances applied
                    final List<PromotionResult> resultList;
                    if (instCnt > 0) {
                        resultList = buildFiredResultList(wrappedProducts, wrappedDeal, instCnt);
                    }
                    else {
                        resultList = new ArrayList<>();
                    }

                    // product that could have met another deal if we had more products
                    final float certainty = calcQtyCertainty(wrappedProducts);
                    if (certainty > 0.0001) {
                        resultList.addAll(buildCouldHaveFiredResultList(wrappedProducts, wrappedDeal, certainty));
                    }
                    return resultList;
                }
            }

            // we get here then we have the items for another deal
            instCnt++;

            // Time to find out how much we are saving on this deal instance
            long saveAmt = spendAmt - dealAmt; // the amount of savings left to give the customer
            final long minSave = Math.round(getRewardMinSaveAsPrimitive() * 100);
            if (minSave > saveAmt) {
                saveAmt = minSave;
                if (saveAmt > spendAmt) {
                    saveAmt = spendAmt;
                }
            }
            final long totalSave = saveAmt;

            // now spread the savings around
            for (final DealQualifierModel qualModel : wrappedDeal.getQualifierModelList()) {
                minQty = qualModel.getMinQty() == null ? 0 : qualModel.getMinQty().intValue();
                sumQty = 0;
                for (int itemLoop = getList.length - 1; itemLoop >= 0; itemLoop--) {

                    if (getList[itemLoop].isInDeal()) {
                        continue;
                    }

                    if (!qualModel.equals(getList[itemLoop].getQualifierModel())) {
                        continue;
                    }

                    long lTemp = getList[itemLoop].getUnitSellPrice();
                    lTemp *= totalSave;
                    lTemp *= 10; // the *10, +5, /10 is for rounding purposes
                    lTemp /= spendAmt;
                    lTemp += 5;
                    lTemp /= 10;
                    if (lTemp > getList[itemLoop].getUnitSellPrice()) {
                        lTemp = getList[itemLoop].getUnitSellPrice();
                    }
                    getList[itemLoop].setDealApplied(instCnt, DealItemTypeEnum.REWARD, lTemp);
                    saveAmt -= lTemp;

                    sumQty++;
                    if (sumQty == minQty) {
                        break;
                    }
                }
            }

            // Because rounding does not always work out perfect, apportion
            // the last few cents.
            int temp = 1;
            if (saveAmt < 0) {
                temp = -1;
            }
            for (int cnt = 0; saveAmt != 0; cnt++) {
                if (cnt == wrappedProducts.size()) {
                    //CHECKSTYLE:OFF changing the control variable is deliberate
                    cnt = 0;
                    //CHECKSTYLE:ON
                }

                final ProductWrapper thisItem = wrappedProducts.get(cnt);
                if (!thisItem.isInDeal() || thisItem.getInstance() != instCnt) {
                    continue;
                }

                if (temp > 0) // giving more discount
                {
                    // make sure we don't go negative
                    if (thisItem.getAdjustedUnitSellPrice() < 1) {
                        continue;
                    }
                }

                thisItem.setDealApplied(instCnt, DealItemTypeEnum.REWARD, thisItem.getDealMarkdown() + temp);
                saveAmt -= temp;
            }
        }
    }

    @Override
    public String getResultDescription(final SessionContext ctx, final PromotionResult promotionResult,
            final Locale locale) {
        final AbstractOrder order = promotionResult.getOrder(ctx);
        if (order != null) {
            final Currency orderCurrency = order.getCurrency(ctx);
            final List<DealQualifier> qualList = getQualifierList();

            int qualCnt = 0;
            for (final DealQualifier dealQualifier : qualList) {
                qualCnt += dealQualifier.getMinQtyAsPrimitive();
            }

            if (promotionResult.getFired(ctx)) {
                final double totalDiscount = promotionResult.getTotalDiscount(ctx);

                double totalAdjustedPrice = 0;
                final Collection<PromotionOrderEntryConsumed> entries = promotionResult.getConsumedEntries(ctx);
                for (final PromotionOrderEntryConsumed entry : entries) {
                    totalAdjustedPrice += entry.getAdjustedEntryPrice();
                }

                final double previousTotalPrice = round(totalAdjustedPrice + totalDiscount);


                final Object[] args = {
                        Integer.valueOf(qualCnt),
                        Double.valueOf(totalAdjustedPrice),
                        Helper.formatCurrencyAmount(ctx, locale, orderCurrency, totalAdjustedPrice),
                        Double.valueOf(totalDiscount),
                        Helper.formatCurrencyAmount(ctx, locale, orderCurrency, totalDiscount),
                        Double.valueOf(previousTotalPrice),
                        Helper.formatCurrencyAmount(ctx, locale, orderCurrency, previousTotalPrice),
                        ((TargetDealResult)promotionResult).getInstanceCount()
                };
                return formatMessage(this.getMessageFired(ctx), args, locale);
            }
            else if (promotionResult.getCouldFire(ctx)) {

                final int foundCnt = Math.round(qualCnt * promotionResult.getCertaintyAsPrimitive());

                final Object[] args = {
                        Integer.valueOf(qualCnt),
                        getRewardValue(),
                        Helper.formatCurrencyAmount(ctx, locale, orderCurrency, getRewardValueAsPrimitive()),
                        getRewardMinSave(),
                        Helper.formatCurrencyAmount(ctx, locale, orderCurrency, getRewardMinSaveAsPrimitive()),
                        Integer.valueOf(qualCnt - foundCnt)
                };
                return formatMessage(this.getMessageCouldHaveFired(ctx), args, locale);
            }
        }
        return "";
    }
}

/**
 * 
 */
package de.hybris.platform.task.impl;

import de.hybris.platform.core.Initialization;
import de.hybris.platform.core.MasterTenant;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.Tenant;
import de.hybris.platform.jalo.ConsistencyCheckException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloConnection;
import de.hybris.platform.jalo.JaloItemNotFoundException;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SearchResult;
import de.hybris.platform.jalo.flexiblesearch.FlexibleSearch;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.TypeManager;
import de.hybris.platform.jalo.user.UserManager;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.task.InvalidTaskStateError;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.task.Task;
import de.hybris.platform.task.TaskCondition;
import de.hybris.platform.task.TaskConditionModel;
import de.hybris.platform.task.TaskEngine;
import de.hybris.platform.task.TaskModel;
import de.hybris.platform.task.TaskRunner;
import de.hybris.platform.task.TaskService;
import de.hybris.platform.task.TaskTimeoutException;
import de.hybris.platform.task.constants.GeneratedTaskConstants;
import de.hybris.platform.util.RedeployUtilities;
import de.hybris.platform.util.config.ConfigIntf;
import de.hybris.platform.util.jeeapi.YNoSuchEntityException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.BeanNotOfRequiredTypeException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;


/**
 * @author bhuang3
 *
 */
@SuppressWarnings("deprecation")
public class TargetTaskService implements TaskService {
    public static final int DEFAULT_WORKER_MAX = 10;
    public static final int DEFAULT_POLLING_INTERVAL = 30;
    public static final int DEFAULT_SHUTDOWN_WAIT = 10;
    private static final Logger LOG = Logger.getLogger(TargetTaskService.class.getName());

    //Get all Tasks and its corresponding processes along with order associations
    private static final String ALL_TASK_PROCESS_ORDER_DATA_QUERY = " WITH allTaskProcessData AS ( " +
            " SELECT   allTsk.pk as taskPK " +
            " , allTsk.createdTS as tskCreated " +
            " , allTsk.p_action as tskAction " +
            " , allTsk.p_runningonclusternode as isTaskRunning    " +
            " , allTsk.p_failed as isTaskFailed " +
            " , allTsk.p_expirationtimemillis as expirationtimemillis " +
            " , allTsk.p_executiontimemillis as execTimeInMillis " +
            " , allTsk.p_process as procPK " +
            " , procs.p_code as procCode " +
            " , procs.p_processdefinitionname as procDef " +
            " , procs.p_state as procState " +
            " , procs.createdTs as procCreated " +
            " , procs.p_order as ordPk " +
            " , p_nodeid as nodeid " +
            " FROM    tasks allTsk " +
            " LEFT OUTER JOIN processes procs " +
            " ON procs.pk = allTsk.p_process " +
            " ) ";

    // Select the earlierst process, task combination for task associated to an order 
    private static final String EARLIEST_PROCESS_TASK_ORDER_COMBINATION_QUERY = " ,OrderWithMinProcCreated AS ( " +
            " SELECT ordPk, MIN(procCreated) as procCreatedMin FROM allTaskProcessData " +
            " where  ordPk IS NOT NULL  " +
            " GROUP BY ordPk " +
            " ) ";

    // Select the earliest task task from the process selected above
    private static final String ORDER_WITH_EARLIEST_TASK_CREATED_QUERY = " ,OrderWithMinTaskCreated AS ( " +
            " SELECT allTaskProcessData.ordPk, MIN(tskCreated) as tskCreatedMin " +
            " FROM allTaskProcessData, OrderWithMinProcCreated " +
            " where allTaskProcessData.ordPk = OrderWithMinProcCreated.ordPk " +
            " and allTaskProcessData.procCreated = OrderWithMinProcCreated.procCreatedMin " +
            " and allTaskProcessData.isTaskRunning = :noNode " +
            " GROUP BY allTaskProcessData.ordPk " +
            " ) ";

    //Select the tasks & processes associated to Order to be executed " +
    private static final String TASK_ASSOC_ORDER_FILTERED_QUERY = " ,taskAssociatedToOrderFiltered AS ( " +
            " SELECT tskProcData.* " +
            " FROM allTaskProcessData tskProcData " +
            " INNER JOIN OrderWithMinTaskCreated   " +
            " ON tskProcData.ordPk = OrderWithMinTaskCreated.ordPk  " +
            " and tskProcData.tskCreated = OrderWithMinTaskCreated.tskCreatedMin " +
            " WHERE  ";

    //Ignore if any task for the order is in running state in another process
    private static final String FILTER_RUNNING_TASK_DIFFERENT_PROC_SAME_ORDER_QUERY = " NOT EXISTS ( " +
            " SELECT 1 FROM allTaskProcessData tskRunning " +
            " WHERE tskRunning.isTaskRunning!= :noNode  " +
            " AND tskRunning.ordPk= tskProcData.ordPk  " +
            " AND tskRunning.procCode!= tskProcData.procCode " +
            " ) ";
    // Ignore all processes for an order if there is any one process in failed state " +
    private static final String FILTER_PROCESS_HAVING_FAILED_PROCESS_ANOTHER_ORDER = "      AND NOT EXISTS ( " +
            " SELECT 1 FROM PROCESSES procs  " +
            " WHERE procs.p_order = tskProcData.ordPK " +
            " AND procs.p_state = :failedProcessState " +
            " ) ";
    //task combined is to combined the filtered tasks associated to the order and the ones which aren't
    private static final String COMBINE_FILTERED_TASKS_HAVING_ORDER_TO_WITHOUT_ORDER = " ) ,taskCombined AS ( " +
            " SELECT * FROM taskAssociatedToOrderFiltered " +
            " UNION  " +
            " SELECT * FROM allTaskProcessData " +
            " WHERE allTaskProcessData.ordPk IS NULL " +
            " ) " +
            // filters for time based and the failure
            " SELECT * from taskCombined " +
            " WHERE  taskCombined.isTaskFailed = :false  " +
            " AND ( " +
            " taskCombined.expirationtimemillis < :now  " +
            " OR ( " +
            " taskCombined.execTimeInMillis <= :now  " +
            " AND ( " +
            " taskCombined.nodeid = :nodeId  " +
            " OR taskCombined.nodeid IS NULL  " +
            " ) " +
            " AND taskCombined.isTaskRunning = :noNode  " +
            " AND NOT EXISTS ( " +
            " SELECT pk  " +
            " FROM taskconditions   " +
            " WHERE p_task=taskCombined.taskPK " +
            " AND ( p_fulfilled = :false OR p_fulfilled IS NULL )  " +
            " )  " +
            " ) " +
            " ) ORDER BY execTimeInMillis ASC";

    protected String getTaskQuery(final boolean stallWhenOneTaskFailed) {
        final StringBuilder getTaskQuery = new StringBuilder();
        getTaskQuery.append(ALL_TASK_PROCESS_ORDER_DATA_QUERY).append(EARLIEST_PROCESS_TASK_ORDER_COMBINATION_QUERY)
                .append(ORDER_WITH_EARLIEST_TASK_CREATED_QUERY).append(TASK_ASSOC_ORDER_FILTERED_QUERY)
                .append(FILTER_RUNNING_TASK_DIFFERENT_PROC_SAME_ORDER_QUERY);
        if (stallWhenOneTaskFailed) {
            getTaskQuery.append(FILTER_PROCESS_HAVING_FAILED_PROCESS_ANOTHER_ORDER);
        }
        getTaskQuery.append(COMBINE_FILTERED_TASKS_HAVING_ORDER_TO_WITHOUT_ORDER);
        return getTaskQuery.toString();
    }

    private NamedParameterJdbcTemplate jdbcTemplate;
    private TransactionTemplate transactionTemplate;
    private TargetFeatureSwitchService targetFeatureSwitchService;
    private TypeService typeService;
    private Long processStateFailedPk;
    private Set<Long> currentTodos;
    private Poll pollingThread;
    private TaskDAO dao;
    private ExecutorService executor;
    private volatile boolean isStartingOrStopping = false;
    private final int shutdownWait;
    private final Tenant tenant;
    private EventService eventService;
    private ModelService modelService;
    private Map<Class<? extends TaskRunner>, TaskExecutionStrategy> taskExecutionStrategies;
    private ScheduleAndTriggerStrategy scheduleAndTriggerStrategy;
    private final TaskEngine engine = new TaskEngine() {
        @Override
        public void stop() {
            TargetTaskService.this.destroy();
        }

        @Override
        public void start() {
            TargetTaskService.this.init();
        }

        @Override
        public void repoll() {
            TargetTaskService.this.repoll();
        }

        @Override
        public void triggerRepoll(final Integer nodeID) {
            TargetTaskService.this.triggerRepoll(nodeID);
        }

        @Override
        public boolean isRunning() {
            return (!TargetTaskService.this.isStartingOrStopping) && (TargetTaskService.this.pollingThread != null) &&
                    (TargetTaskService.this.pollingThread.isRunning());
        }
    };

    public TargetTaskService() {
        this.tenant = Registry.getCurrentTenantNoFallback();
        if (this.tenant == null) {
            throw new IllegalStateException("cannot create task service without tenant");
        }
        this.shutdownWait = readShutdownWaitFromConfig();
    }

    /**
     * override the init to add setting process state pk
     */
    public void init() {
        LOG.info("Target task engine is starting init.");
        final PK pk = typeService.getEnumerationValue(ProcessState.class.getSimpleName(),
                ProcessState.FAILED.toString()).getPk();
        processStateFailedPk = pk.getLong();

        if (!this.isStartingOrStopping) {
            synchronized (this) {
                if (!this.isStartingOrStopping) {
                    this.isStartingOrStopping = true;
                    try {
                        if (checkSystemOK()) {
                            if (this.pollingThread != null) {
                                LOG.debug("Task engine has already been started - skiping startup.");
                            }
                            else if (!isAllowedToStart()) {
                                LOG.info("Task engine is disabled at this node - wont startup.");
                            }
                            else {
                                this.executor = this.createExecutor();
                                this.pollingThread = new Poll(getTenant().getTenantID());
                                if (LOG.isInfoEnabled()) {
                                    LOG.info("Task engine starting up (min:" + getMinWorkers() + " max:"
                                            + getMaxWorkers() + " idle:" +
                                            getWorkerIdleTime() + "s interval:" + getPollingInterval() / 1000L + "s)");
                                }
                                this.pollingThread.start();
                            }
                        }
                    }
                    finally {
                        this.isStartingOrStopping = false;
                    }
                }
            }
        }
    }


    private List<Long> queryTasks() {

        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.TASK_SERVICE)) {
            return queryNewTasks(targetFeatureSwitchService
                    .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.TASK_SERVICE_STALL_FAILED_PROCESS_SAME_ORDER));
        }
        else {
            return originalQueryTasks();
        }
    }

    @SuppressWarnings("deprecation")
    private List<Long> originalQueryTasks() {
        getTenant().forceMasterDataSource();

        final Map<String, Object> parameters = new HashMap(3);
        parameters.put("now", Long.valueOf(System.currentTimeMillis()));
        parameters.put("false", Boolean.FALSE);
        parameters.put("nodeId", Integer.valueOf(MasterTenant.getInstance().getClusterID()));
        parameters.put("noNode", Integer.valueOf(-1));

        final List<Long> ret = FlexibleSearch.getInstance().search(
                "SELECT {" + Item.PK + "} " +
                        "FROM {" + GeneratedTaskConstants.TC.TASK + " AS t} " +
                        "WHERE " +
                        "{" + "failed" + "} = ?false " +
                        "AND (" +
                        "( " +
                        "\t{" + "expirationTimeMillis" + "} < ?now " +
                        ") " +
                        "OR " +
                        "(" +
                        "\t{" + "executionTimeMillis" + "} <= ?now " +
                        "\tAND ({" + "nodeId" + "} = ?nodeId OR {" + "nodeId" + "} IS NULL ) " +
                        "\tAND {" + "runningOnClusterNode" + "} = ?noNode " +
                        "\tAND NOT EXISTS({{ " +
                        "  \tSELECT {" + Item.PK + "} FROM {" + GeneratedTaskConstants.TC.TASKCONDITION + "} " +
                        "   \tWHERE {" + "task" + "}={t." + Item.PK + "} " +
                        "     \tAND ( {" + "fulfilled" + "} = ?false OR {" + "fulfilled" + "} IS NULL ) " +

                        "\t}}) " +
                        ") )" +
                        "ORDER BY {" + "executionTimeMillis" + "} ASC",
                parameters, Long.class).getResult();

        return ret;
    }

    public List<Long> queryNewTasks(final boolean stallWhenOneProcessFailedForOrder) {
        return transactionTemplate.execute(new TransactionCallback<List>() {
            @Override
            public List<Long> doInTransaction(final TransactionStatus status) {
                try {
                    final Map<String, Object> argMap = new HashMap<String, Object>();
                    argMap.put("now", Long.valueOf(System.currentTimeMillis()));
                    argMap.put("false", Boolean.FALSE);
                    argMap.put("nodeId", Integer.valueOf(MasterTenant.getInstance().getClusterID()));
                    argMap.put("noNode", Integer.valueOf(-1));
                    argMap.put("failedProcessState", processStateFailedPk);
                    final List<Map<String, Object>> rows = jdbcTemplate
                            .queryForList(getTaskQuery(stallWhenOneProcessFailedForOrder), argMap);
                    final List<Long> taskList = new ArrayList<>();
                    if (CollectionUtils.isNotEmpty(rows)) {
                        for (final Map<String, Object> row : rows) {
                            // taskPk is defined in task_query
                            taskList.add((Long)row.get("taskPK"));
                        }
                    }
                    return taskList;
                }
                catch (final DataAccessException dae) {
                    throw new SystemException(dae);
                }
            }
        });
    }

    public void triggerRepoll(final Integer nodeId) {
        if ((nodeId != null) && (!isAllowedToStart(nodeId.intValue()))) {
            LOG.warn("cannot trigger repoll for excluded node " + nodeId + "'");
        }
        else {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Publishing repoll event.");
            }
            this.eventService.publishEvent(new RepollEvent(nodeId));
        }
    }

    @Override
    public void triggerEvent(final String uniqueId) {
        if (this.scheduleAndTriggerStrategy.triggerEvent(uniqueId)) {
            triggerRepoll(null);
        }
    }

    @Override
    public void triggerEvent(final String uniqueId, final Date expirationDate) {
        if (this.scheduleAndTriggerStrategy.triggerEvent(uniqueId, expirationDate)) {
            triggerRepoll(null);
        }
    }

    @Override
    public void scheduleTask(final TaskModel task) {
        checkTask(task);
        final Date scheduledDate = task.getExecutionDate();
        final Integer nodeID = task.getNodeId();
        this.scheduleAndTriggerStrategy.scheduleTask(task);
        triggerRepollAfterSchedule(scheduledDate, nodeID);
    }

    protected void triggerRepollAfterSchedule(final Date scheduledDate, final Integer nodeId) {
        if ((scheduledDate != null) && (scheduledDate.getTime() > System.currentTimeMillis())) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Skipping repoll event. Execution date in future.");
            }
        }
        else {
            triggerRepoll(nodeId);
        }
    }

    protected void checkTask(final TaskModel toBeScheduled) {
        if ((toBeScheduled.getRunnerBean() == null) || (toBeScheduled.getRunnerBean().trim().length() == 0)) {
            throw new IllegalArgumentException("Task " + toBeScheduled + " has no runner bean");
        }
        final Date scheduled = toBeScheduled.getExecutionDate() != null ? toBeScheduled.getExecutionDate() : new Date();
        final Date expires = toBeScheduled.getExpirationDate();
        if ((expires != null) && (expires.before(scheduled))) {
            throw new IllegalArgumentException("Task " + toBeScheduled + " has expiration date < execution date");
        }
        final Set<TaskConditionModel> conditions = toBeScheduled.getConditions();
        if ((conditions != null) && (!conditions.isEmpty())) {
            for (final TaskConditionModel cond : conditions) {
                if (cond != null) {
                    checkTaskConditions(toBeScheduled, cond);
                }
            }
        }
    }

    protected void checkTaskConditions(final TaskModel toBeScheduled, final TaskConditionModel cond) {
        if (cond.getTask() == null) {
            cond.setTask(toBeScheduled);
        }
        if ((cond.getUniqueID() == null) || (cond.getUniqueID().trim().length() == 0)) {
            throw new IllegalArgumentException("Task condition " + cond + " from " + toBeScheduled + " has no ID");
        }
        final Date scheduled = toBeScheduled.getExecutionDate() != null ? toBeScheduled.getExecutionDate() : new Date();
        final Date expires = cond.getExpirationDate();
        if ((expires != null) && (expires.before(scheduled))) {
            throw new IllegalArgumentException("Task condition " + cond + " from " + toBeScheduled +
                    " has expiration date <  task execution date");
        }
    }

    public void repoll() {
        if (getEngine().isRunning()) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Received repoll event.");
            }
            this.pollingThread.repoll();
        }
    }

    protected boolean isAllowedToStart() {
        return isAllowedToStart(Registry.getMasterTenant().getClusterID());
    }

    protected boolean isAllowedToStart(final int clusterId) {
        final String clusterIdStr = Integer.toString(clusterId);
        final String excluded = getTenant().getConfig().getParameter("task.excluded.cluster.ids");
        if ((excluded != null) && (excluded.length() > 0)) {
            for (final String idStr : excluded.split("[,; ]")) {
                if ((idStr != null) && (idStr.trim().equals(clusterIdStr))) {
                    return false;
                }
            }
        }
        return true;
    }

    protected boolean isAllowedToPoll(final int clusterId) {
        final String clusterIdStr = Integer.toString(clusterId);
        final String excluded = getTenant().getConfig().getParameter("target.task.poll.excluded.cluster.ids");
        if ((excluded != null) && (excluded.length() > 0)) {
            for (final String idStr : excluded.split("[,; ]")) {
                if ((idStr != null) && (idStr.trim().equals(clusterIdStr))) {
                    return false;
                }
            }
        }
        return true;
    }

    @SuppressWarnings("deprecation")
    protected boolean checkSystemOK() {
        if (!JaloConnection.getInstance().isSystemInitialized()) {
            LOG.warn("System not initialized. Not starting task module.");
            return false;
        }
        if (RedeployUtilities.isShutdownInProgress()) {
            LOG.warn("System about to shutdown. Not starting task module.");
            return false;
        }
        ComposedType taskType = null;
        try {
            taskType = TypeManager.getInstance().getComposedType(Task.class);
        }
        catch (final JaloItemNotFoundException localJaloItemNotFoundException1) {
            LOG.warn("System needs to be updated in order to use task module - cannot find Task type.");
            return false;
        }
        try {
            taskType.getAttributeDescriptorIncludingPrivate("failed");
        }
        catch (final JaloItemNotFoundException localJaloItemNotFoundException2) {
            LOG.warn("System needs to be updated in order to use task module - cannot find Task.failed attribute.");
            return false;
        }
        return true;
    }

    protected void poll() {
        if ((!getEngine().isRunning()) || (!checkSystemOK())
                || !isAllowedToPoll(Registry.getMasterTenant().getClusterID())) {
            return;
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Polling tasks.");
        }
        final Set<Long> newCurrentTodos = new LinkedHashSet(queryTasks());
        newCurrentTodos.addAll(queryTimedOutConditions());
        Set<Long> oldCurrentTodos = null;
        synchronized (this) {
            oldCurrentTodos = this.currentTodos;
            this.currentTodos = newCurrentTodos;
        }
        final Set<Long> toBeScheduled = new LinkedHashSet(this.currentTodos);
        if (oldCurrentTodos != null) {
            toBeScheduled.removeAll(oldCurrentTodos);
        }

        for (final Long pk : toBeScheduled) {
            this.executor.execute(new Runnable() {
                @Override
                public void run() {
                    final Thread t = Thread.currentThread();
                    final Object prepareCtx = TargetTaskService.this.prepareWorker(t);
                    try {
                        TargetTaskService.this.processInTenant(pk);
                    }
                    finally {
                        TargetTaskService.this.resetWorker(t, pk, prepareCtx);
                    }
                }
            });
        }
    }

    private class Poll extends Thread {
        private final Object wait = new Object();
        private final String tenantID;
        private volatile boolean running = true;
        private volatile boolean repollRequested = false;
        private final long interval;

        private Poll(final String tenantId) {
            super();
            setDaemon(true);
            this.tenantID = tenantId;
            this.interval = (1000 * TargetTaskService.this.readPollingIntevalFromConfig());
        }

        public void repoll() {
            synchronized (this.wait) {
                this.repollRequested = true;
                this.wait.notifyAll();
            }
        }

        void terminate() {
            if (TargetTaskService.LOG.isInfoEnabled()) {
                TargetTaskService.LOG.info("Terminating task polling thread.");
            }
            this.running = false;
            interrupt();
            try {
                join(10000L);
            }
            catch (final InterruptedException e) {
                TargetTaskService.LOG.warn("Thread " + Thread.currentThread()
                        + " was aborted waiting for polling thread " + this + " to finish.", e);
            }
            if (isAlive()) {
                TargetTaskService.LOG.warn("Polling thread did not die as expected!");
            }
        }

        boolean isRunning() {
            return this.running;
        }

        @Override
        public void run() {
            try {
                if ((processingAllowed()) && (activateTenant())) {
                    prepareProcessing();
                    while (processingAllowed()) {
                        process();
                        waitForNextTurn(-1L);
                    }
                }
                this.running = false;
            }
            catch (final Throwable t) {
                handleAbnormalError(t);
            }
            finally {
                this.running = false;
                cleanup();
            }
        }

        private void cleanup() {
            if (!RedeployUtilities.isShutdownInProgress()) {
                JaloSession.deactivate();
                Registry.unsetCurrentTenant();
            }
        }

        private void process() {
            try {
                if (!Registry.hasCurrentTenant()) {
                    Registry.setCurrentTenantByID(this.tenantID);
                    TargetTaskService.LOG.warn("Polling thread did not have active tenant - reactivated "
                            + this.tenantID);
                }
                TargetTaskService.this.poll();
            }
            catch (final Throwable thr) {
                handleProcessingError(thr);
            }
        }

        private void handleProcessingError(final Throwable e) {
            TargetTaskService.LOG.error("Caught exception while polling pending tasks.", e);
        }

        private void handleAbnormalError(final Throwable e) {
            if (RedeployUtilities.isShutdownInProgress()) {
                TargetTaskService.LOG.warn("unexpected error in task polling thread during shutdown : "
                        + e.getMessage());
            }
            else {
                TargetTaskService.LOG.error("unexpected error in task polling thread - terminating", e);
            }
        }

        private long waitForNextTurn(final long wakeUpTime) {
            long newWakeUpTime = wakeUpTime;
            synchronized (this.wait) {
                if (processingAllowed()) {
                    final long currentTime = System.currentTimeMillis();

                    final long delay = wakeUpTime > currentTime ? wakeUpTime - currentTime : this.interval;
                    newWakeUpTime = currentTime + delay - 1L;
                    try {
                        if (!this.repollRequested) {
                            this.wait.wait(delay);
                        }
                    }
                    catch (final InterruptedException localInterruptedException) {
                        if (TargetTaskService.LOG.isDebugEnabled()) {
                            TargetTaskService.LOG.debug("Polling thread was interrupted.");
                        }
                    }
                    finally {
                        this.repollRequested = false;
                    }
                }
            }
            return newWakeUpTime;
        }

        private boolean processingAllowed() {
            return (isRunning()) && (!Thread.currentThread().isInterrupted())
                    && (!RedeployUtilities.isShutdownInProgress()) &&
                    (!Initialization.isTenantInitializingLocally(TargetTaskService.this.getTenant()));
        }

        private void prepareProcessing() {
            if (TargetTaskService.LOG.isInfoEnabled()) {
                TargetTaskService.LOG.info("Polling thread started.");
            }
            final JaloSession session = JaloSession.getCurrentSession();

            session.setTimeout(-1);

            session.setUser(UserManager.getInstance().getAdminEmployee());

            TargetTaskService.this.recoverRunning();
        }

        private boolean activateTenant() {
            try {
                Registry.setCurrentTenantByID(this.tenantID);
                return true;
            }
            catch (final Exception localException) {
                TargetTaskService.LOG.error("Cannot poll tasks since tenant is not available - terminating");
            }
            return false;
        }
    }


    /**
     * @param jdbcTemplate
     *            the jdbcTemplate to set
     */
    @Required
    public void setJdbcTemplate(final NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * @param transactionTemplate
     *            the transactionTemplate to set
     */
    @Required
    public void setTransactionTemplate(final TransactionTemplate transactionTemplate) {
        this.transactionTemplate = transactionTemplate;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }


    /**
     * @param typeService
     *            the typeService to set
     */
    @Required
    public void setTypeService(final TypeService typeService) {
        this.typeService = typeService;
    }



    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    @Required
    public void setEventService(final EventService eventService) {
        this.eventService = eventService;
    }

    @Required
    public void setScheduleAndTriggerStrategy(final ScheduleAndTriggerStrategy scheduleAndTriggerStrategy) {
        this.scheduleAndTriggerStrategy = scheduleAndTriggerStrategy;
    }

    @Override
    public TaskEngine getEngine() {
        return this.engine;
    }

    protected TaskDAO getTaskDao() {
        return this.dao;
    }

    @Required
    public void setTaskDao(final TaskDAO taskDao) {
        this.dao = taskDao;
    }

    protected int getMinWorkers() {
        return this.executor != null ? ((ThreadPoolExecutor)this.executor).getCorePoolSize() : -1;
    }

    protected int getMaxWorkers() {
        return this.executor != null ? ((ThreadPoolExecutor)this.executor).getMaximumPoolSize() : -1;
    }

    protected int getWorkerIdleTime() {
        return (int)(this.executor != null ? ((ThreadPoolExecutor)this.executor).getKeepAliveTime(TimeUnit.SECONDS)
                : -1L);
    }

    protected long getPollingInterval() {
        return this.pollingThread != null ? this.pollingThread.interval : -1L;
    }

    protected int readPollingIntevalFromConfig() {
        final ConfigIntf cfg = getTenant().getConfig();
        final int tmp = cfg.getInt("task.polling.interval", 30);
        if (tmp <= 0) {
            LOG.warn("invalid polling interval " + tmp + " - using default " + 30 + " sec instead");
            return 30;
        }
        return tmp;
    }

    protected int readMaxWorkersFromConfig() {
        final ConfigIntf cfg = getTenant().getConfig();
        final int wMax = cfg.getInt("task.workers.max", 10);
        if (wMax <= 0) {
            LOG.warn("invalid maximum worker number " + wMax + " - using " + 10 + " instead");
            return 10;
        }
        return wMax;
    }

    protected int readWorkersIdleFromConfig() {
        final ConfigIntf cfg = getTenant().getConfig();

        final int pollingInterval = readPollingIntevalFromConfig();
        int idle = cfg.getInt("task.workers.idle", 2 * pollingInterval);
        if (idle < 0) {
            LOG.warn("invalid worker idle time " + idle + " - using " + 2 * pollingInterval + " instead");
            idle = 2 * pollingInterval;
        }
        return idle;
    }

    protected Set<Integer> readExcludedNodesFromConfig() {
        Set<Integer> ret = null;
        final String excluded = getTenant().getConfig().getParameter("task.excluded.cluster.ids");
        if ((excluded != null) && (excluded.length() > 0)) {
            for (final String idStr : excluded.split("[,; ]")) {
                if (idStr != null) {
                    try {
                        final Integer id = Integer.valueOf(idStr);
                        if (ret == null) {
                            ret = new LinkedHashSet();
                        }
                        ret.add(id);
                    }
                    catch (final NumberFormatException localNumberFormatException) {
                        LOG.warn("invalid excluded cluster node id '" + idStr + "' found - please check your " +
                                "task.excluded.cluster.ids" + " settings");
                    }
                }
            }
        }
        return ret != null ? ret : Collections.EMPTY_SET;
    }

    protected int readShutdownWaitFromConfig() {
        final ConfigIntf cfg = getTenant().getConfig();

        int localShutdownWait = 10;
        try {
            localShutdownWait = cfg.getInt("tasks.shutdown.wait", 10);
        }
        catch (final NumberFormatException localNumberFormatException) {
            LOG.warn("Wrong number value for tasks.shutdown.wait set in confuguration. Default value =10 will be used");
        }
        return localShutdownWait;
    }

    private ExecutorService createExecutor() {
        final int workers = readMaxWorkersFromConfig();
        final ThreadPoolExecutor ret = new ThreadPoolExecutor(
                workers,
                workers,
                readWorkersIdleFromConfig(), TimeUnit.SECONDS,
                new LinkedBlockingQueue(),
                new ThreadFactory() {
                    @Override
                    public Thread newThread(final Runnable runnable) {
                        final Thread thread = new Thread(runnable);
                        thread.setName("TaskExecutor-" + TargetTaskService.this.getTenant().getTenantID() + "-"
                                + thread.getId());
                        return thread;
                    }
                });
        ret.allowCoreThreadTimeOut(true);
        return ret;
    }

    public void destroy() {
        if (!this.isStartingOrStopping) {
            synchronized (this) {
                if (!this.isStartingOrStopping) {
                    this.isStartingOrStopping = true;
                    try {
                        if (LOG.isInfoEnabled()) {
                            LOG.info("Shutting down task engine for tenant " + getTenant().getTenantID() + ".");
                        }
                        if (this.pollingThread != null) {
                            this.pollingThread.terminate();
                            this.pollingThread = null;
                        }
                        this.currentTodos = null;
                        if (this.executor != null) {
                            try {
                                this.executor.shutdownNow();
                                this.executor.awaitTermination(this.shutdownWait, TimeUnit.SECONDS);
                            }
                            catch (final InterruptedException localInterruptedException) {
                                LOG.warn("Failed to wait for currently running tasks.");
                            }
                            this.executor = null;
                        }
                        if (LOG.isInfoEnabled()) {
                            LOG.info("Stopped task engine for tenant " + getTenant().getTenantID() + ".");
                        }
                    }
                    finally {
                        this.isStartingOrStopping = false;
                    }
                }
            }
        }
    }

    private Object prepareWorker(final Thread t) {
        final ClassLoader ctxCl = t.getContextClassLoader();

        return ctxCl;
    }

    /**
     * @param pk
     */
    private void resetWorker(final Thread t, final Long pk, final Object prepareCtx) {
        this.modelService.detachAll();
        t.setContextClassLoader((ClassLoader)prepareCtx);
    }

    @SuppressWarnings("deprecation")
    private List<Long> queryTimedOutConditions() {
        getTenant().forceMasterDataSource();

        final Map<String, Object> parameters = new HashMap(3);
        parameters.put("now", Long.valueOf(System.currentTimeMillis()));
        parameters.put("false", Boolean.FALSE);

        return FlexibleSearch.getInstance().search(
                "SELECT {" + Item.PK + "} " +
                        "FROM {" + GeneratedTaskConstants.TC.TASKCONDITION + "} " +
                        "WHERE {" + "expirationTimeMillis" + "} < ?now " +
                        "AND {" + "task" + "} IS NULL " +
                        "AND ( {" + "fulfilled" + "} = ?false OR {" + "fulfilled" + "} IS NULL ) ",
                parameters, Long.class).getResult();
    }

    @SuppressWarnings("deprecation")
    private void recoverRunning() {
        if (!getTenant().getJaloConnection().isSystemInitialized()) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("System is not initialized. Skipping task recovery.");
            }
            return;
        }
        final int nodeId = MasterTenant.getInstance().getClusterID();
        if (LOG.isDebugEnabled()) {
            LOG.debug("Searching for task marked running on nodeId " + nodeId + " (after system crash?).");
        }
        final SearchResult<Task> rs = FlexibleSearch.getInstance().search(
                "select {" + Item.PK + "} " +
                        "from {" + GeneratedTaskConstants.TC.TASK + "} " +
                        "where {" + "runningOnClusterNode" + "} = ?nodeId",
                Collections.singletonMap("nodeId", Integer.valueOf(nodeId)),
                Task.class);
        for (final Task task : rs.getResult()) {
            try {
                LOG.error("Recovering task to run on node " + nodeId + ": " + task.getPK());
                getTaskDao().unlock(Long.valueOf(task.getPK().getLongValue()));
            }
            catch (final IllegalStateException e) {
                LOG.error("Failed to recover task '" + task + "' marked running on node " + nodeId + " on startup.", e);
            }
        }
    }

    private synchronized boolean isCurrentTodo(final Long l) {
        return (this.currentTodos != null) && (this.currentTodos.contains(l));
    }

    private Item getScheduledItem(final Long l) {
        try {
            return JaloSession.getCurrentSession().getItem(PK.fromLong(l.longValue()));
        }
        catch (final JaloItemNotFoundException localJaloItemNotFoundException) {
            return null;
        }
        catch (final YNoSuchEntityException localYNoSuchEntityException) {
            //
        }
        return null;
    }

    protected void processInTenant(final Long l) {
        try {
            Registry.setCurrentTenant(getTenant());
            try {
                process(l);
            }
            finally {
                if (JaloSession.hasCurrentSession()) {
                    JaloSession.getCurrentSession().close();
                }
                JaloSession.deactivate();
            }
        }
        finally {
            Registry.unsetCurrentTenant();
        }
    }

    protected void process(final Long l) {
        if (!isCurrentTodo(l)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Task #" + l + " is not in current todo set. Skipping it.");
            }
            return;
        }
        final Item item = getScheduledItem(l);
        if (item == null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("scheduled item " + l + " does not exist any more. Skipping it.");
            }
        }
        else {
            adjustCurrentThreadName(item);
            if ((item instanceof Task)) {
                processTask((Task)item);
            }
            else if ((item instanceof TaskCondition)) {
                processCondition((TaskCondition)item);
            }
            else {
                LOG.error("unexpected item in DefaultTaskManager.execute : " + item + ", class = " + item.getClass());
            }
        }
    }

    private void adjustCurrentThreadName(final Item item) {
        final StringBuilder newName = new StringBuilder(Thread.currentThread().getName());
        if (newName.toString().endsWith("]")) {
            final int lastSeparatorIdx = newName.toString().lastIndexOf("-");
            newName.setLength(lastSeparatorIdx);
        }
        newName.append("-").append(item.getClass().getSimpleName()).append(" [").append(item.getPK()).append("]");
        Thread.currentThread().setName(newName.toString());
    }

    protected void processCondition(final TaskCondition cond) {
        try {
            if ((cond.isAlive()) && (cond.getTask() == null)) {
                LOG.warn("removing timed out task condition " + cond + " - no task has been matching yet");
                cond.remove();
            }
        }
        catch (final Exception e) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("error removing timed out condition : " + e.getMessage(), e);
            }
        }
    }

    protected void processTask(final Task task) {
        final Long l = Long.valueOf(task.getPK().getLongValue());
        if (!getTaskDao().lock(l)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Failed to retrieve lock on task #" + l + ". Skipping it.");
            }
            return;
        }
        try {
            final Set<TaskCondition> conditions = task.getConditions();
            for (final TaskCondition condition : conditions) {
                try {
                    condition.remove();
                }
                catch (final ConsistencyCheckException e) {
                    throw new SystemException(e.getMessage(), e);
                }
            }
            final TaskModel model = (TaskModel)this.modelService.get(task);
            if (BooleanUtils.isTrue(model.getFailed())) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Skip processing of failed task #" + l + ".");
                }
                return;
            }
            final Object runner = getRunner(task);
            final TaskExecutionStrategy executionStrategy = getExecutionStrategy((TaskRunner)runner);
            Throwable error = null;
            boolean finished = false;
            try {
                final Date expirationDate = task.getExpirationDate();
                if ((expirationDate != null) && (System.currentTimeMillis() > expirationDate.getTime())) {
                    error = executionStrategy.handleError(this, (TaskRunner)runner, model, new TaskTimeoutException(
                            "task " + task + " timed out",
                            expirationDate));

                    markTaskFailed(task);
                    finished = true;
                }
                else {
                    executionStrategy.run(this, (TaskRunner)runner, model);
                    finished = true;
                }
            }
            catch (final RetryLaterException e) {
                final int currentRetry = task.getRetryAsPrimitive();
                final Date allowedRetry = executionStrategy.handleRetry(this, (TaskRunner)runner, model, e,
                        currentRetry);
                if (allowedRetry != null) {
                    scheduleRetry(task, allowedRetry, currentRetry + 1);
                }
                else {
                    markTaskFailed(task);
                    error = executionStrategy.handleError(this, (TaskRunner)runner, model, new InvalidTaskStateError(
                            "no more retries allowed for task " + task + " after " + currentRetry + " retries"));
                    finished = true;
                }
            }
            catch (final Throwable e) {
                LOG.error("Failed to execute task #" + l + ".", e);
                if (task.isAlive()) {
                    markTaskFailed(task);
                }
                error = executionStrategy.handleError(this, (TaskRunner)runner, model, e);
                finished = true;
            }
            finally {
                if (finished) {
                    executionStrategy.finished(this, (TaskRunner)runner, model, error);
                }
            }
        }
        finally {
            if (task.isAlive()) {
                getTaskDao().unlock(l);
            }
        }
        if (task.isAlive()) {
            getTaskDao().unlock(l);
        }
    }

    protected void scheduleRetry(final Task task, final Date nextExecution, final int turn) {
        task.setExecutionDate(nextExecution);

        task.setProperty("retry", Integer.valueOf(turn));
    }

    protected void markTaskFailed(final Task task) {
        task.setProperty("failed", Boolean.TRUE);
    }

    protected TaskRunner getRunner(final Task task) {
        return getRunner(task.getRunnerBean());
    }

    protected TaskRunner getRunner(final String runnerBean)
            throws IllegalStateException {
        try {
            final TaskRunner ret = Registry.getCoreApplicationContext().getBean(runnerBean, TaskRunner.class);
            if (ret == null) {
                throw new IllegalStateException("Unknown task runner bean id '" + runnerBean + "' !");
            }
            return ret;
        }
        catch (final NoSuchBeanDefinitionException localNoSuchBeanDefinitionException) {
            throw new IllegalStateException("Unknown task runner bean id '" + runnerBean + "' !");
        }
        catch (final BeanNotOfRequiredTypeException e) {
            throw new IllegalStateException("Invalid type of task runner bean '" + runnerBean + "'" + "' - expected " +
                    e.getRequiredType() + " but got " + e.getActualType());
        }
        catch (final Exception e) {
            LOG.error("error getting task runner bean", e);
            throw new IllegalStateException("error getting task runner bean '" + runnerBean + "' : " + e.getMessage());
        }
    }

    protected Tenant getTenant() {
        return this.tenant;
    }

    protected TaskExecutionStrategy getExecutionStrategy(final TaskRunner<? extends TaskModel> runner) {
        TaskExecutionStrategy strategy = this.taskExecutionStrategies.get(runner.getClass());
        if (strategy == null) {
            strategy = this.taskExecutionStrategies.get(TaskRunner.class);
        }
        if (strategy == null) {
            throw new IllegalStateException("No TaskExecutionStrategy found for runner of class " + runner.getClass());
        }
        return strategy;
    }

    @Autowired
    public void setTaskExecutionStrategies(final Collection<? extends TaskExecutionStrategy> taskExecutionStrategies) {
        this.taskExecutionStrategies = new HashMap();
        for (final TaskExecutionStrategy executionStrategy : taskExecutionStrategies) {
            this.taskExecutionStrategies.put(executionStrategy.getRunnerClass(), executionStrategy);
        }
    }
}
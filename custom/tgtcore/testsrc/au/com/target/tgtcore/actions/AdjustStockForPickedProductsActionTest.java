/**
 * 
 */
package au.com.target.tgtcore.actions;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;


/**
 * Unit test for {@link AdjustStockForPickedProductsAction}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AdjustStockForPickedProductsActionTest {
    /**
     * 
     */
    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;
    @Mock
    private TargetStockService targetStockService;

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private WarehouseModel defaultWarehouseModel;

    @Mock
    private WarehouseModel anotherWarehouseModel;

    @Mock
    private OrderProcessModel process;

    @Mock
    private ConsignmentModel consignment;

    @InjectMocks
    private final AdjustStockForPickedProductsAction action = new AdjustStockForPickedProductsAction();

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Test
    public void testExecuteActionSimpleHappyPath() throws Exception {
        // con stuff
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignment);
        final Set<ConsignmentEntryModel> conEntries = new HashSet<>();
        given(consignment.getConsignmentEntries()).willReturn(conEntries);
        given(Boolean.valueOf(targetWarehouseService.isStockAdjustmentRequired(defaultWarehouseModel)))
                .willReturn(Boolean.TRUE);
        given(consignment.getWarehouse()).willReturn(defaultWarehouseModel);

        // first product
        final ConsignmentEntryModel conEnt1 = Mockito.mock(ConsignmentEntryModel.class);
        conEntries.add(conEnt1);
        final AbstractOrderEntryModel orderEnt1 = Mockito.mock(AbstractOrderEntryModel.class);
        given(conEnt1.getOrderEntry()).willReturn(orderEnt1);
        final ProductModel product1 = Mockito.mock(ProductModel.class);
        given(orderEnt1.getProduct()).willReturn(product1);

        action.executeAction(process);

        Mockito.verify(targetStockService).transferReserveToActualAmount(Mockito.eq(product1),
                (WarehouseModel)Mockito.any(), Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    public void testExecuteActionMultipleItems() throws Exception {
        // con stuff
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignment);
        given(Boolean.valueOf(targetWarehouseService.isStockAdjustmentRequired(defaultWarehouseModel)))
                .willReturn(Boolean.TRUE);
        given(consignment.getWarehouse()).willReturn(defaultWarehouseModel);
        final Set<ConsignmentEntryModel> conEntries = new HashSet<>();
        given(consignment.getConsignmentEntries()).willReturn(conEntries);

        // first product
        final ConsignmentEntryModel conEnt1 = Mockito.mock(ConsignmentEntryModel.class);
        conEntries.add(conEnt1);
        final AbstractOrderEntryModel orderEnt1 = Mockito.mock(AbstractOrderEntryModel.class);
        given(conEnt1.getOrderEntry()).willReturn(orderEnt1);
        final ProductModel product1 = Mockito.mock(ProductModel.class);
        given(orderEnt1.getProduct()).willReturn(product1);

        // second product
        final ConsignmentEntryModel conEnt2 = Mockito.mock(ConsignmentEntryModel.class);
        conEntries.add(conEnt2);
        final AbstractOrderEntryModel orderEnt2 = Mockito.mock(AbstractOrderEntryModel.class);
        given(conEnt2.getOrderEntry()).willReturn(orderEnt2);
        final ProductModel product2 = Mockito.mock(ProductModel.class);
        given(orderEnt2.getProduct()).willReturn(product2);

        action.executeAction(process);

        Mockito.verify(targetStockService).transferReserveToActualAmount(Mockito.eq(product1),
                (WarehouseModel)Mockito.any(), Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(targetStockService).transferReserveToActualAmount(Mockito.eq(product2),
                (WarehouseModel)Mockito.any(), Mockito.anyInt(), Mockito.anyString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteActionNullProcessModel() throws Exception {
        action.executeAction(null);
    }

    @Test
    public void testExecuteActionNoConsignmentEntries() throws Exception {
        // con stuff
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignment);
        final Set<ConsignmentEntryModel> conEntries = new HashSet<>();
        given(consignment.getConsignmentEntries()).willReturn(conEntries);

        action.executeAction(process);

        Mockito.verifyZeroInteractions(targetStockService);
    }

    @Test
    public void testExecuteActionNullConsignmentEntries() throws Exception {
        // con stuff
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignment);
        given(consignment.getConsignmentEntries()).willReturn(null);

        action.executeAction(process);

        Mockito.verifyZeroInteractions(targetStockService);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteActionWarehouseInConsignmentNull() throws Exception {
        // con stuff
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignment);
        final Set<ConsignmentEntryModel> conEntries = new HashSet<>();
        given(consignment.getConsignmentEntries()).willReturn(conEntries);
        given(consignment.getWarehouse()).willReturn(null);

        // first product
        final ConsignmentEntryModel conEnt1 = Mockito.mock(ConsignmentEntryModel.class);
        conEntries.add(conEnt1);
        final AbstractOrderEntryModel orderEnt1 = Mockito.mock(AbstractOrderEntryModel.class);
        given(conEnt1.getOrderEntry()).willReturn(orderEnt1);

        action.executeAction(process);

        Mockito.verifyZeroInteractions(targetStockService);
    }

    @Test
    public void testExecuteActionWithStockAdjustmentFalse() throws Exception {
        // con stuff
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignment);
        final Set<ConsignmentEntryModel> conEntries = new HashSet<>();
        given(consignment.getConsignmentEntries()).willReturn(conEntries);
        given(Boolean.valueOf(targetWarehouseService.isStockAdjustmentRequired(defaultWarehouseModel)))
                .willReturn(Boolean.FALSE);
        given(consignment.getWarehouse()).willReturn(defaultWarehouseModel);

        // first product
        final ConsignmentEntryModel conEnt1 = Mockito.mock(ConsignmentEntryModel.class);
        conEntries.add(conEnt1);
        final AbstractOrderEntryModel orderEnt1 = Mockito.mock(AbstractOrderEntryModel.class);
        given(conEnt1.getOrderEntry()).willReturn(orderEnt1);

        action.executeAction(process);

        Mockito.verifyZeroInteractions(targetStockService);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteActionNullConsignment() throws Exception {
        action.executeAction(process);
    }

    /**
     * This will test an error during the process to ensure it continues on
     * 
     * @throws Exception
     */
    @Test
    public void testExecuteActionProductNull() throws Exception {
        // con stuff
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignment);
        given(Boolean.valueOf(targetWarehouseService.isStockAdjustmentRequired(defaultWarehouseModel)))
                .willReturn(Boolean.TRUE);
        given(consignment.getWarehouse()).willReturn(defaultWarehouseModel);
        final Set<ConsignmentEntryModel> conEntries = new HashSet<>();
        given(consignment.getConsignmentEntries()).willReturn(conEntries);

        // first product
        final ConsignmentEntryModel conEnt1 = Mockito.mock(ConsignmentEntryModel.class);
        conEntries.add(conEnt1);
        final AbstractOrderEntryModel orderEnt1 = Mockito.mock(AbstractOrderEntryModel.class);
        given(conEnt1.getOrderEntry()).willReturn(orderEnt1);
        final ProductModel product1 = Mockito.mock(ProductModel.class);

        // second product
        final ConsignmentEntryModel conEnt2 = Mockito.mock(ConsignmentEntryModel.class);
        conEntries.add(conEnt2);
        final AbstractOrderEntryModel orderEnt2 = Mockito.mock(AbstractOrderEntryModel.class);
        given(conEnt2.getOrderEntry()).willReturn(orderEnt2);
        final ProductModel product2 = Mockito.mock(ProductModel.class);
        given(orderEnt2.getProduct()).willReturn(product2);

        action.executeAction(process);

        Mockito.verify(targetStockService, Mockito.times(0)).transferReserveToActualAmount(
                Mockito.eq(product1), (WarehouseModel)Mockito.any(), Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(targetStockService, Mockito.times(1)).transferReserveToActualAmount(
                Mockito.eq(product2), (WarehouseModel)Mockito.any(), Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    public void testExecuteActionSimpleHappyPathWithFalconOn() throws Exception {
        // con stuff
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignment);
        final Set<ConsignmentEntryModel> conEntries = new HashSet<>();
        given(consignment.getConsignmentEntries()).willReturn(conEntries);
        given(Boolean.valueOf(targetWarehouseService.isStockAdjustmentRequired(defaultWarehouseModel)))
                .willReturn(Boolean.TRUE);
        given(consignment.getWarehouse()).willReturn(defaultWarehouseModel);
        given(defaultWarehouseModel.getCode()).willReturn(TgtCoreConstants.FASTLINE_WAREHOUSE);
        given(
                Boolean.valueOf(targetFeatureSwitchService
                        .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON))).willReturn(Boolean.TRUE);

        // first product
        final ConsignmentEntryModel conEnt1 = Mockito.mock(ConsignmentEntryModel.class);
        conEntries.add(conEnt1);
        final AbstractOrderEntryModel orderEnt1 = Mockito.mock(AbstractOrderEntryModel.class);
        given(conEnt1.getOrderEntry()).willReturn(orderEnt1);
        final ProductModel product1 = Mockito.mock(ProductModel.class);
        given(orderEnt1.getProduct()).willReturn(product1);

        action.executeAction(process);

        Mockito.verify(targetStockService).adjustActualAmount(Mockito.eq(product1),
                (WarehouseModel)Mockito.any(), Mockito.anyInt(), Mockito.anyString());

    }
}

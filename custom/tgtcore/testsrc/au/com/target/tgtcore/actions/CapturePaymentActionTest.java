/**
 * 
 */
package au.com.target.tgtcore.actions;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.methods.TargetPaymentMethod;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.service.PaymentsInProgressService;
import au.com.target.tgtpayment.service.TargetPaymentService;
import au.com.target.tgtpayment.strategy.PaymentMethodStrategy;


/**
 * @author gsing236
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CapturePaymentActionTest {

    @InjectMocks
    @Spy
    private final CapturePaymentAction action = new CapturePaymentAction();

    @Mock
    private TargetPaymentService targetPaymentService;

    @Mock
    private PaymentMethodStrategy paymentMethodStrategy;

    @Mock
    private OrderProcessModel orderProcessModel;

    @Mock
    private OrderModel orderModel;

    @Mock
    private PaymentTransactionModel paymentTxnModel;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetQueryTransactionDetailsResult txnQueryResult;

    @Mock
    private TargetPaymentMethod paymentMethod;

    @Mock
    private PaymentTransactionEntryModel ptem;

    @Mock
    private PaymentsInProgressService paymentsInProgressService;

    @Mock
    private FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;


    @Before
    public void setUp() {

        given(action.getModelService()).willReturn(modelService);
        final PaymentInfoModel paymentInfo = mock(PaymentInfoModel.class);
        given(orderProcessModel.getOrder()).willReturn(orderModel);
        given(orderModel.getTotalPrice()).willReturn(Double.valueOf(450.0));
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(orderModel)).willReturn(Double.valueOf(0d));
        willReturn(Boolean.FALSE).given(orderModel).getContainsPreOrderItems();
        given(orderModel.getNormalSaleStartDateTime()).willReturn(new Date());
        given(orderModel.getPaymentInfo()).willReturn(paymentInfo);
        given(orderModel.getPreOrderDepositAmount()).willReturn(Double.valueOf(10.0));
        given(paymentMethodStrategy.getPaymentMethod(paymentInfo)).willReturn(paymentMethod);
        given(targetPaymentService.queryTransactionDetails(paymentMethod, paymentInfo)).willReturn(txnQueryResult);

        given(targetPaymentService.createTransactionWithQueryResult(orderModel, txnQueryResult,
                PaymentTransactionType.DEFERRED)).willReturn(paymentTxnModel);
        willReturn(Boolean.FALSE).given(targetPaymentService).verifyIfPaymentCapturedAndUpdateEntry(ptem);

        given(ptem.getType()).willReturn(PaymentTransactionType.DEFERRED);
        given(ptem.getPaymentTransaction()).willReturn(paymentTxnModel);
        final IpgPaymentInfoModel ipgPaymentInfo = mock(IpgPaymentInfoModel.class);
        given(ptem.getIpgPaymentInfo()).willReturn(ipgPaymentInfo);
        given(paymentTxnModel.getEntries()).willReturn(Arrays.asList(ptem));
        willReturn(Boolean.TRUE).given(txnQueryResult).isSuccess();

    }

    // CC for preOrder not updated.
    @Test
    public void testExecuteInternalAction() throws Exception {

        final String status = action.executeInternal(orderProcessModel);

        final PaymentInfoModel paymentInfo = orderModel.getPaymentInfo();
        verify(paymentMethodStrategy).getPaymentMethod(paymentInfo);
        verify(targetPaymentService).queryTransactionDetails(paymentMethod, paymentInfo);
        verify(targetPaymentService).createTransactionWithQueryResult(orderModel, txnQueryResult,
                PaymentTransactionType.DEFERRED);
        verify(targetPaymentService).capture(orderModel, paymentInfo, BigDecimal.valueOf(440.0), null,
                PaymentCaptureType.RELEASEPREORDER, paymentTxnModel);
        verify(ptem).setType(PaymentTransactionType.CAPTURE);
        verify(modelService).save(ptem);
        verify(modelService).refresh(ptem);
        verify(paymentsInProgressService, times(2)).removeInProgressPayment(orderModel);
        verify(targetPaymentService).verifyIfPaymentCapturedAndUpdateEntry(ptem);
        verify(paymentTxnModel).setIsCaptureSuccessful(Boolean.TRUE);
        verify(modelService).save(paymentTxnModel);
        assertThat(status).isEqualTo(CapturePaymentAction.Transition.OK.toString());

    }


    // CC updated for preOrder
    @Test
    public void testExecuteInternalActionWhenDeferredTransactionExists() throws Exception {

        given(orderModel.getPaymentTransactions()).willReturn(Arrays.asList(paymentTxnModel));

        final String status = action.executeInternal(orderProcessModel);

        final PaymentInfoModel paymentInfo = orderModel.getPaymentInfo();
        verify(targetPaymentService, never()).queryTransactionDetails(paymentMethod, paymentInfo);
        verify(targetPaymentService, never()).createTransactionWithQueryResult(orderModel, txnQueryResult,
                PaymentTransactionType.DEFERRED);

        verify(targetPaymentService).capture(orderModel, paymentInfo, BigDecimal.valueOf(440.0), null,
                PaymentCaptureType.RELEASEPREORDER, paymentTxnModel);
        verify(ptem).setType(PaymentTransactionType.CAPTURE);
        verify(modelService).save(ptem);
        verify(modelService).refresh(ptem);
        verify(paymentsInProgressService, times(2)).removeInProgressPayment(orderModel);
        verify(targetPaymentService).verifyIfPaymentCapturedAndUpdateEntry(ptem);
        verify(paymentTxnModel).setIsCaptureSuccessful(Boolean.TRUE);
        verify(modelService).save(paymentTxnModel);
        assertThat(status).isEqualTo(CapturePaymentAction.Transition.OK.toString());

    }

    @Test
    public void testExecuteInternalActionWhenPaymentStatusError() throws Exception {

        given(ptem.getTransactionStatus()).willReturn(TransactionStatus.ERROR.toString());
        final String status = action.executeInternal(orderProcessModel);

        final PaymentInfoModel paymentInfo = orderModel.getPaymentInfo();
        verify(paymentMethodStrategy).getPaymentMethod(paymentInfo);
        verify(targetPaymentService).queryTransactionDetails(paymentMethod, paymentInfo);
        verify(targetPaymentService).createTransactionWithQueryResult(orderModel, txnQueryResult,
                PaymentTransactionType.DEFERRED);
        verify(targetPaymentService).capture(orderModel, paymentInfo, BigDecimal.valueOf(440.0), null,
                PaymentCaptureType.RELEASEPREORDER, paymentTxnModel);
        verify(ptem, never()).setType(PaymentTransactionType.CAPTURE);
        verify(modelService, never()).save(ptem);
        verify(modelService, never()).refresh(ptem);
        verify(paymentsInProgressService, times(2)).removeInProgressPayment(orderModel);
        verify(targetPaymentService).verifyIfPaymentCapturedAndUpdateEntry(ptem);
        verify(paymentTxnModel).setIsCaptureSuccessful(Boolean.FALSE);
        verify(modelService).save(paymentTxnModel);
        verify(modelService).refresh(paymentTxnModel);
        assertThat(status).isEqualTo(CapturePaymentAction.Transition.PAYMENT_FAILURE.toString());

    }

    @Test
    public void testExecuteInternalActionWhenPaymentStatusRejected() throws Exception {

        given(ptem.getTransactionStatus()).willReturn(TransactionStatus.REJECTED.toString());
        final String status = action.executeInternal(orderProcessModel);

        final PaymentInfoModel paymentInfo = orderModel.getPaymentInfo();
        verify(paymentMethodStrategy).getPaymentMethod(paymentInfo);
        verify(targetPaymentService).queryTransactionDetails(paymentMethod, paymentInfo);
        verify(targetPaymentService).createTransactionWithQueryResult(orderModel, txnQueryResult,
                PaymentTransactionType.DEFERRED);
        verify(targetPaymentService).capture(orderModel, paymentInfo, BigDecimal.valueOf(440.0), null,
                PaymentCaptureType.RELEASEPREORDER, paymentTxnModel);
        verify(ptem, never()).setType(PaymentTransactionType.CAPTURE);
        verify(modelService, never()).save(ptem);
        verify(modelService, never()).refresh(ptem);
        verify(paymentsInProgressService, times(2)).removeInProgressPayment(orderModel);
        verify(targetPaymentService).verifyIfPaymentCapturedAndUpdateEntry(ptem);
        verify(paymentTxnModel).setIsCaptureSuccessful(Boolean.FALSE);
        verify(modelService).save(paymentTxnModel);
        verify(modelService).refresh(paymentTxnModel);
        assertThat(status).isEqualTo(CapturePaymentAction.Transition.PAYMENT_FAILURE.toString());

    }

    @Test
    public void testExecuteActionRetryExceededWhenIPGQueryTaxResultFailed() throws Exception {
        given(ptem.getTransactionStatus()).willReturn(TransactionStatus.REJECTED.toString());
        willReturn(Boolean.FALSE).given(txnQueryResult).isSuccess();
        final String status = action.execute(orderProcessModel);

        final PaymentInfoModel paymentInfo = orderModel.getPaymentInfo();
        verify(paymentMethodStrategy).getPaymentMethod(paymentInfo);
        verify(targetPaymentService).queryTransactionDetails(paymentMethod, paymentInfo);
        verify(targetPaymentService, never()).createTransactionWithQueryResult(orderModel, txnQueryResult,
                PaymentTransactionType.DEFERRED);
        verify(targetPaymentService, never()).capture(orderModel, paymentInfo, BigDecimal.valueOf(440.0), null,
                PaymentCaptureType.RELEASEPREORDER, paymentTxnModel);
        verify(ptem, never()).setType(PaymentTransactionType.CAPTURE);
        verify(modelService, never()).save(ptem);
        verify(modelService, never()).refresh(ptem);
        verify(paymentTxnModel, never()).setIsCaptureSuccessful(Boolean.TRUE);
        verify(modelService, never()).save(paymentTxnModel);
        verify(paymentsInProgressService, never()).removeInProgressPayment(orderModel);
        verify(targetPaymentService, never()).verifyIfPaymentCapturedAndUpdateEntry(ptem);
        assertThat(status).isEqualTo("RETRYEXCEEDED");

    }

    @Test(expected = RetryLaterException.class)
    public void testExecuteInternalRetryExceptionWhenIPGQueryTaxResultFailed() throws Exception {
        given(ptem.getTransactionStatus()).willReturn(TransactionStatus.REJECTED.toString());
        willReturn(Boolean.FALSE).given(txnQueryResult).isSuccess();
        action.executeInternal(orderProcessModel);
    }

    @Test
    public void testExecuteActionRetryExceededWhenCapturePaymentFailedIPGCommunication() throws Exception {
        given(ptem.getTransactionStatus()).willReturn(TransactionStatus.REVIEW.toString());
        willReturn(Boolean.TRUE).given(txnQueryResult).isSuccess();
        final String status = action.execute(orderProcessModel);

        final PaymentInfoModel paymentInfo = orderModel.getPaymentInfo();
        verify(paymentMethodStrategy).getPaymentMethod(paymentInfo);
        verify(targetPaymentService).queryTransactionDetails(paymentMethod, paymentInfo);
        verify(targetPaymentService).createTransactionWithQueryResult(orderModel, txnQueryResult,
                PaymentTransactionType.DEFERRED);
        verify(targetPaymentService).capture(orderModel, paymentInfo, BigDecimal.valueOf(440.0), null,
                PaymentCaptureType.RELEASEPREORDER, paymentTxnModel);
        verify(ptem, never()).setType(PaymentTransactionType.CAPTURE);
        verify(modelService, never()).save(ptem);
        verify(modelService, never()).refresh(ptem);
        verify(modelService, never()).save(paymentTxnModel);
        verify(modelService, never()).refresh(paymentTxnModel);
        verify(paymentsInProgressService, times(2)).removeInProgressPayment(orderModel);
        verify(targetPaymentService).verifyIfPaymentCapturedAndUpdateEntry(ptem);
        assertThat(status).isEqualTo("RETRYEXCEEDED");
        verify(paymentTxnModel, never()).setIsCaptureSuccessful(Boolean.TRUE);
    }

    @Test(expected = RetryLaterException.class)
    public void testExecuteInternalExceptionActionWhenCapturePaymentFailedIPGCommunication() throws Exception {
        given(ptem.getTransactionStatus()).willReturn(TransactionStatus.REVIEW.toString());
        willReturn(Boolean.TRUE).given(txnQueryResult).isSuccess();
        action.executeInternal(orderProcessModel);
    }

    @Test
    public void testExecuteInternalActionWhenBalancePaymentAlreadyCaptured() throws Exception {
        given(orderModel.getTotalPrice()).willReturn(Double.valueOf(404.0));
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(orderModel)).willReturn(Double.valueOf(404.0));
        final String status = action.executeInternal(orderProcessModel);

        assertThat(status).isEqualTo(CapturePaymentAction.Transition.OK.toString());
        verify(findOrderTotalPaymentMadeStrategy).getAmountPaidSoFar(orderModel);
        verify(orderModel, never()).getPaymentInfo();
        verify(orderModel, never()).getCurrency();
        verify(orderModel, never()).getPreOrderDepositAmount();
        verify(paymentsInProgressService).removeInProgressPayment(orderModel);
        verifyZeroInteractions(paymentTxnModel);
        verifyZeroInteractions(ptem);
        verifyZeroInteractions(paymentMethodStrategy);
        verifyZeroInteractions(targetPaymentService);
        verifyZeroInteractions(modelService);
        verify(ptem, never()).setType(PaymentTransactionType.CAPTURE);

    }

    @Test
    public void testExecuteInternalActionWhenPaymentCapturedByIpgButNotStoredByHybris() throws Exception {
        given(orderModel.getTotalPrice()).willReturn(Double.valueOf(404.0));
        willReturn(Boolean.TRUE).given(targetPaymentService).verifyIfPaymentCapturedAndUpdateEntry(ptem);

        final String status = action.executeInternal(orderProcessModel);

        assertThat(status).isEqualTo(CapturePaymentAction.Transition.OK.toString());
        final PaymentInfoModel paymentInfo = orderModel.getPaymentInfo();
        verify(paymentMethodStrategy).getPaymentMethod(paymentInfo);
        verify(targetPaymentService).queryTransactionDetails(paymentMethod, paymentInfo);
        verify(targetPaymentService).createTransactionWithQueryResult(orderModel, txnQueryResult,
                PaymentTransactionType.DEFERRED);

        verify(targetPaymentService, never()).capture(orderModel, paymentInfo, BigDecimal.valueOf(440.0), null,
                PaymentCaptureType.RELEASEPREORDER, paymentTxnModel);

        verify(ptem).setType(PaymentTransactionType.CAPTURE);
        verify(modelService).save(ptem);
        verify(modelService).refresh(ptem);
        verify(modelService).save(paymentTxnModel);
        verify(modelService).refresh(paymentTxnModel);
        verify(paymentsInProgressService, times(2)).removeInProgressPayment(orderModel);
        verify(targetPaymentService).verifyIfPaymentCapturedAndUpdateEntry(ptem);
        assertThat(status).isEqualTo(CapturePaymentAction.Transition.OK.toString());

    }

    @Test
    public void testExecuteInternalActionForNormalOrder() throws Exception {
        given(orderModel.getNormalSaleStartDateTime()).willReturn(null);

        final String status = action.executeInternal(orderProcessModel);

        verifyZeroInteractions(targetPaymentService);
        verifyZeroInteractions(paymentMethodStrategy);
        verifyZeroInteractions(paymentTxnModel);
        verifyZeroInteractions(modelService);
        verifyZeroInteractions(txnQueryResult);
        verifyZeroInteractions(paymentMethod);
        verifyZeroInteractions(ptem);
        verifyZeroInteractions(paymentsInProgressService);
        verifyZeroInteractions(findOrderTotalPaymentMadeStrategy);
        assertThat(status).isEqualTo(CapturePaymentAction.Transition.OK.toString());


    }

}

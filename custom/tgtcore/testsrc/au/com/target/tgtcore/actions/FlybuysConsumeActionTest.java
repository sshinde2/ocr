/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.flybuys.dto.response.FlybuysConsumeResponseDto;
import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;
import au.com.target.tgtcore.flybuys.service.FlybuysDiscountService;


/**
 * The Class FlybuysConsumeActionTest.
 * 
 * @author umesh
 */
@UnitTest
public class FlybuysConsumeActionTest {

    @InjectMocks
    private final FlybuysConsumeAction flybuysConsumeAction = new FlybuysConsumeAction();

    @Mock
    private FlybuysDiscountService flybuysDiscountService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testExecuteInternalWithFlybuysDiscount() throws RetryLaterException, Exception
    {
        final OrderProcessModel process = Mockito.mock(OrderProcessModel.class);
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final FlybuysConsumeResponseDto status = Mockito.mock(FlybuysConsumeResponseDto.class);
        BDDMockito.given(process.getOrder()).willReturn(orderModel);
        BDDMockito.given(flybuysDiscountService
                .consumeFlybuysPoints(orderModel)).willReturn(status);
        BDDMockito.given(status.getResponse()).willReturn(FlybuysResponseType.SUCCESS);
        final String result = flybuysConsumeAction.executeInternal(process);

        Assert.assertEquals("ACCEPT", result);
    }

    @Test
    public void testExecuteInternalWithFlybuysDiscountWithResponseINVALID() throws RetryLaterException, Exception
    {
        final OrderProcessModel process = Mockito.mock(OrderProcessModel.class);
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final FlybuysConsumeResponseDto status = Mockito.mock(FlybuysConsumeResponseDto.class);
        BDDMockito.given(process.getOrder()).willReturn(orderModel);
        BDDMockito.given(flybuysDiscountService
                .consumeFlybuysPoints(orderModel)).willReturn(status);
        BDDMockito.given(status.getResponse()).willReturn(FlybuysResponseType.INVALID);
        final String result = flybuysConsumeAction.executeInternal(process);

        Assert.assertEquals("REJECT", result);
    }

    @Test(expected = RetryLaterException.class)
    public void testExecuteInternalWithFlybuysDiscountWithResponseUNAVAILABLE() throws RetryLaterException,
            Exception
    {
        final OrderProcessModel process = Mockito.mock(OrderProcessModel.class);
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final FlybuysConsumeResponseDto status = Mockito.mock(FlybuysConsumeResponseDto.class);
        BDDMockito.given(process.getOrder()).willReturn(orderModel);
        BDDMockito.given(flybuysDiscountService
                .consumeFlybuysPoints(orderModel)).willReturn(status);
        BDDMockito.given(status.getResponse()).willReturn(FlybuysResponseType.UNAVAILABLE);
        flybuysConsumeAction.executeInternal(process);
    }

    @Test
    public void testExecuteInternalWithFlybuysDiscountWithResponseERROR() throws RetryLaterException, Exception
    {
        final OrderProcessModel process = Mockito.mock(OrderProcessModel.class);
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final FlybuysConsumeResponseDto status = Mockito.mock(FlybuysConsumeResponseDto.class);
        BDDMockito.given(process.getOrder()).willReturn(orderModel);
        BDDMockito.given(flybuysDiscountService
                .consumeFlybuysPoints(orderModel)).willReturn(status);
        BDDMockito.given(status.getResponse()).willReturn(FlybuysResponseType.FLYBUYS_OTHER_ERROR);
        final String result = flybuysConsumeAction.executeInternal(process);

        Assert.assertEquals("REJECT", result);
    }
}

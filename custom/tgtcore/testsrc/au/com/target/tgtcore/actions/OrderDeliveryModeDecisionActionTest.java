/**
 * 
 */
package au.com.target.tgtcore.actions;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderDeliveryModeDecisionActionTest {

    @InjectMocks
    private final OrderDeliveryModeDecisionAction action = new OrderDeliveryModeDecisionAction();

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Mock
    private TargetZoneDeliveryModeModel targetZoneDeliveryMode;

    @Test
    public void testExecute() throws RetryLaterException, Exception {
        given(process.getOrder()).willReturn(order);
        given(order.getDeliveryMode()).willReturn(targetZoneDeliveryMode);
        given(targetZoneDeliveryMode.getIsDeliveryToStore()).willReturn(Boolean.TRUE);
        final String result = action.execute(process);
        assertThat(result).isEqualTo("PICK_UP");
    }

    @Test
    public void testExecuteWithNotDeliveryToStore() throws RetryLaterException, Exception {
        given(process.getOrder()).willReturn(order);
        given(order.getDeliveryMode()).willReturn(targetZoneDeliveryMode);
        given(targetZoneDeliveryMode.getIsDeliveryToStore()).willReturn(Boolean.FALSE);
        final String result = action.execute(process);
        assertThat(result).isEqualTo("SHIP");
    }
}

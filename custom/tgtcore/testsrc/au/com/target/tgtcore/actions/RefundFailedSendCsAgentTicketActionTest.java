/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import java.math.BigDecimal;

import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;


/**
 *
 */
@UnitTest
public class RefundFailedSendCsAgentTicketActionTest {

    @Test
    public void testExecuteAction() throws Exception {
        final RefundFailedSendCsAgentTicketAction action = new RefundFailedSendCsAgentTicketAction();
        final TargetTicketBusinessService targetTicketBusinessService = Mockito.mock(TargetTicketBusinessService.class);
        action.setTargetTicketBusinessService(targetTicketBusinessService);
        final OrderProcessModel process = Mockito.mock(OrderProcessModel.class);
        final OrderModel order = Mockito.mock(OrderModel.class);
        BDDMockito.given(process.getOrder()).willReturn(order);
        final OrderProcessParameterHelper opph = Mockito.mock(OrderProcessParameterHelper.class);
        action.setOrderProcessParameterHelper(opph);
        BDDMockito.given(opph.getRefundAmount(process)).willReturn(BigDecimal.valueOf(10.0));
        action.executeAction(process);

        Mockito.verify(targetTicketBusinessService, Mockito.times(1))
                .createCsAgentTicket(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.eq(order),
                        Mockito.eq(TgtCoreConstants.CsAgentGroup.REFUND_FAILED_CS_AGENT_GROUP));
    }
}

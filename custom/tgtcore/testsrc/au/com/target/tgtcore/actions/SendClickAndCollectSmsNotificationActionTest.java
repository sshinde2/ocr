/**
 * 
 */
package au.com.target.tgtcore.actions;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.sms.TargetSendSmsService;
import au.com.target.tgtcore.sms.dto.TargetSendSmsResponseDto;
import au.com.target.tgtcore.sms.enums.TargetSendSmsResponseType;


/**
 * 
 * @author knemlaik
 */
@UnitTest
public class SendClickAndCollectSmsNotificationActionTest {
    private static final String FRONTENDTEMPLATE = "CNCAvailableForCollectSMSTemplate";

    @InjectMocks
    private final SendClickAndCollectSmsNotificationAction sendClickAndCollectSmsNotificationAction = new SendClickAndCollectSmsNotificationAction();

    @Mock
    private TargetSendSmsService targetSendSmsService;

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel orderModel;

    @Mock
    private AddressModel deliveryAddressModel;




    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        given(orderModel.getDeliveryAddress()).willReturn(deliveryAddressModel);
        given(deliveryAddressModel.getPhone1()).willReturn("044444444444");
        given(process.getOrder()).willReturn(orderModel);
        sendClickAndCollectSmsNotificationAction.setFrontendTemplateName(FRONTENDTEMPLATE);
    }

    @Test
    public void testSendSmsActionExecuteInternalWithOkStatus() throws RetryLaterException, Exception
    {

        final TargetSendSmsResponseDto targetSendSmsResponseDto = new TargetSendSmsResponseDto();
        targetSendSmsResponseDto.setResponseType(TargetSendSmsResponseType.SUCCESS);
        given(
                targetSendSmsService.sendSmsForCncNotification(process,
                        FRONTENDTEMPLATE)).willReturn(
                targetSendSmsResponseDto);

        final String result = sendClickAndCollectSmsNotificationAction.executeInternal(process);
        assertThat("OK").isEqualTo(result);
    }

    @Test
    public void testSendSmsActionExecuteInternalWithInvalidStatus() throws RetryLaterException, Exception
    {
        final TargetSendSmsResponseDto targetSendSmsResponseDto = new TargetSendSmsResponseDto();
        targetSendSmsResponseDto.setResponseType(TargetSendSmsResponseType.INVALID);
        given(
                targetSendSmsService.sendSmsForCncNotification(process,
                        FRONTENDTEMPLATE)).willReturn(
                targetSendSmsResponseDto);

        final String result = sendClickAndCollectSmsNotificationAction.executeInternal(process);
        assertThat("NOK").isEqualTo(result);
    }


    @Test
    public void testSendSmsActionExecuteInternalWithOthersStatus() throws RetryLaterException, Exception
    {

        final TargetSendSmsResponseDto targetSendSmsResponseDto = new TargetSendSmsResponseDto();
        targetSendSmsResponseDto.setResponseType(TargetSendSmsResponseType.OTHERS);
        given(
                targetSendSmsService.sendSmsForCncNotification(process,
                        FRONTENDTEMPLATE)).willReturn(
                targetSendSmsResponseDto);
        final String result = sendClickAndCollectSmsNotificationAction.executeInternal(process);
        assertThat("NOK").isEqualTo(result);

    }

    @Test
    public void testSendSmsActionExecuteInternalWithNullPhoneNumber() throws RetryLaterException, Exception
    {
        given(deliveryAddressModel.getPhone1()).willReturn(null);
        final String result = sendClickAndCollectSmsNotificationAction.executeInternal(process);
        assertThat("OK").isEqualTo(result);
    }

    @Test
    public void testSendSmsActionExecuteInternalWithoutAddress() throws RetryLaterException, Exception
    {
        given(orderModel.getDeliveryAddress()).willReturn(null);
        final String result = sendClickAndCollectSmsNotificationAction.executeInternal(process);
        assertThat("OK").isEqualTo(result);
    }

    @Test(expected = RetryLaterException.class)
    public void testSendSmsActionExecuteInternalWithUnavailableStatus() throws RetryLaterException, Exception
    {
        final TargetSendSmsResponseDto targetSendSmsResponseDto = new TargetSendSmsResponseDto();
        targetSendSmsResponseDto.setResponseType(TargetSendSmsResponseType.UNAVAILABLE);
        given(
                targetSendSmsService.sendSmsForCncNotification(process,
                        FRONTENDTEMPLATE)).willReturn(
                targetSendSmsResponseDto);
        sendClickAndCollectSmsNotificationAction.executeInternal(process);

    }

}

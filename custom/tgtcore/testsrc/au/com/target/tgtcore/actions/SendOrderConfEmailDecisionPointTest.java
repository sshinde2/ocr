/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.email.BusinessProcessEmailStrategy;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import junit.framework.Assert;


/**
 * @author Nandini
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendOrderConfEmailDecisionPointTest {

    @InjectMocks
    @Spy
    private final SendOrderConfEmailDecisionPoint action = new SendOrderConfEmailDecisionPoint();

    @Mock
    private OrderProcessModel orderProcess;

    @Mock
    private OrderModel order;

    @Mock
    private BusinessProcessEmailStrategy businessProcessEmailStrategy;

    @Mock
    private TargetSalesApplicationConfigService targetSalesApplicationConfigService;

    @Before
    public void setUp() {
        action.setFrontendTemplateName("TEMPLATE");
        BDDMockito.willReturn(businessProcessEmailStrategy).given(action).getBusinessProcessEmailStrategy();
        Mockito.when(orderProcess.getOrder()).thenReturn(order);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteActionNullOrderProcess() throws Exception {
        action.executeAction(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteActionNullOrder() throws Exception {
        Mockito.when(orderProcess.getOrder()).thenReturn(null);
        action.executeAction(orderProcess);
    }

    @Test
    public void testExecuteActionWhenSalesAppSuppressesOrderConfEmail() throws Exception {
        Mockito.when(order.getSalesApplication()).thenReturn(SalesApplication.EBAY);
        Mockito.when(
                Boolean.valueOf(targetSalesApplicationConfigService.isSuppressOrderConfMail(SalesApplication.EBAY)))
                .thenReturn(Boolean.FALSE);
        final Transition trans = action.executeAction(orderProcess);
        Mockito.verify(businessProcessEmailStrategy).sendEmail(orderProcess, "TEMPLATE");
        Assert.assertEquals(Transition.OK, trans);
    }

    @Test
    public void testExecuteActionSalesAppDoesntSuppressesOrderConfEmail() throws Exception {
        Mockito.when(order.getSalesApplication()).thenReturn(SalesApplication.EBAY);
        Mockito.when(
                Boolean.valueOf(targetSalesApplicationConfigService.isSuppressOrderConfMail(SalesApplication.EBAY)))
                .thenReturn(Boolean.TRUE);
        final Transition trans = action.executeAction(orderProcess);
        Mockito.verifyZeroInteractions(businessProcessEmailStrategy);
        Assert.assertEquals(Transition.OK, trans);
    }

}

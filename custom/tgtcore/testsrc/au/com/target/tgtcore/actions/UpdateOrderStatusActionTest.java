/**
 * 
 */
package au.com.target.tgtcore.actions;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UpdateOrderStatusActionTest {

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private final UpdateOrderStatusAction action = new UpdateOrderStatusAction();

    @Before
    public void setup() {
        action.setOrderStatus(OrderStatus.CREATED);
    }

    @Test
    public void testSetOrderStatus() throws RetryLaterException, Exception {
        given(process.getOrder()).willReturn(order);
        action.executeAction(process);
        verify(order).setStatus(OrderStatus.CREATED);
    }

    @Test
    public void testSetOrderStatusWithRestrictions() throws RetryLaterException, Exception {
        final List<OrderStatus> prevStatList = new ArrayList<>();
        prevStatList.add(OrderStatus.ON_VALIDATION);
        action.setOnlyIfPreviousStatusList(prevStatList);
        given(process.getOrder()).willReturn(order);
        given(order.getStatus()).willReturn(OrderStatus.ON_VALIDATION);
        action.executeAction(process);
        verify(order).setStatus(OrderStatus.CREATED);
    }

    @Test
    public void testSetOrderStatusCurrentStatusNotInList() throws RetryLaterException, Exception {
        final List<OrderStatus> prevStatList = new ArrayList<>();
        prevStatList.add(OrderStatus.ON_VALIDATION);
        action.setOnlyIfPreviousStatusList(prevStatList);
        given(process.getOrder()).willReturn(order);
        given(order.getStatus()).willReturn(OrderStatus.CREATED);
        action.executeAction(process);
        verify(order, Mockito.times(0)).setStatus(OrderStatus.CREATED);
    }

    /**
     * This method will verify to set Order status to Parked.
     * 
     * @throws RetryLaterException
     * @throws Exception
     */
    @Test
    public void testSetOrderStatusParked() throws RetryLaterException, Exception {

        given(process.getOrder()).willReturn(order);

        action.setOrderStatus(OrderStatus.PARKED);

        action.executeAction(process);

        verify(order).setStatus(OrderStatus.PARKED);
    }
}

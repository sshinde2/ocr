/**
 * 
 */
package au.com.target.tgtcore.attributes.dynamic;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;


/**
 * @author ajit
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetNewLowerPriceFlagHandlerTest {

    @Mock
    private TargetColourVariantProductModel colorVariantModel;

    @InjectMocks
    private final TargetNewLowerPriceFlagHandler newLowerPriceFlagHandler = new TargetNewLowerPriceFlagHandler();

    /**
     * Test target color variant product start date is null.
     */
    @Test
    public void testTargetColorVariantProductStartDateIsNull() {
        Mockito.when(colorVariantModel.getNewLowerPriceStartDate()).thenReturn(null);
        Assert.assertFalse(newLowerPriceFlagHandler.get(colorVariantModel).booleanValue());
    }


    /**
     * Test target color variant product end date is null.
     */
    @Test
    public void testTargetColorVariantProductEndDateIsNull() {
        Mockito.when(colorVariantModel.getNewLowerPriceStartDate()).thenReturn(new Date());
        Mockito.when(colorVariantModel.getNewLowerPriceEndDate()).thenReturn(null);
        Assert.assertFalse(newLowerPriceFlagHandler.get(colorVariantModel).booleanValue());
    }

    /**
     * Test target color variant product start date and end date is current date.
     */
    @Test
    public void testTargetColorVariantProductStartIsCurrentDate() {
        Mockito.when(colorVariantModel.getNewLowerPriceStartDate()).thenReturn(new Date());
        Mockito.when(colorVariantModel.getNewLowerPriceEndDate()).thenReturn(new Date());
        Assert.assertFalse(newLowerPriceFlagHandler.get(colorVariantModel).booleanValue());
    }

    /**
     * Test target color variant product start date less then end date.
     */
    @Test
    public void testTargetColorVariantProductStartDateLTEndDate() {
        Mockito.when(colorVariantModel.getNewLowerPriceStartDate()).thenReturn(DateUtils.addHours(new Date(), -2));
        Mockito.when(colorVariantModel.getNewLowerPriceEndDate()).thenReturn(DateUtils.addHours(new Date(), 10));
        Assert.assertTrue(newLowerPriceFlagHandler.get(colorVariantModel).booleanValue());
    }

    /**
     * Test target color variant product start date greater then end date.
     */
    @Test
    public void testTargetColorVariantProductStartDateGTEndDate() {
        Mockito.when(colorVariantModel.getNewLowerPriceStartDate()).thenReturn(DateUtils.addHours(new Date(), 5));
        Mockito.when(colorVariantModel.getNewLowerPriceEndDate()).thenReturn(DateUtils.addHours(new Date(), 2));
        Assert.assertFalse(newLowerPriceFlagHandler.get(colorVariantModel).booleanValue());
    }
}

/**
 * 
 */
package au.com.target.tgtcore.cart.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.AddressService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.SavedCncStoreDetailsModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtpayment.enums.PaymentProviderTypeEnum;
import au.com.target.tgtverifyaddr.TargetAddressVerificationService;
import junit.framework.Assert;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCartServiceImplTest {

    @Mock
    private TargetDeliveryService targetDeliveryService;

    @Mock
    private CartModel cartModel;

    @Mock
    private InterceptorContext ctx;

    @Mock
    private AbstractTargetZoneDeliveryModeValueModel targetZoneDeliveryModeValueModel;

    @Mock
    private TargetZoneDeliveryModeModel targetZoneDeliveryModeModel;

    @Mock
    private TargetZoneDeliveryModeModel targetZoneDeliveryModeModel2;

    @InjectMocks
    private final TargetCartServiceImpl targetCartService = new TargetCartServiceImpl();

    @Mock
    private AddressModel shippingAddress;

    @Mock
    private AddressModel paymentAddress;

    @Mock
    private CreditCardPaymentInfoModel paymentInfo;

    @Mock
    private TargetZoneDeliveryModeModel preferredDeliveryMode;

    @Mock
    private TargetCustomerModel customer;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetPointOfServiceService targetPointOfServiceService;

    @Mock
    private TargetPointOfServiceModel targetPointOfService;

    @Mock
    private PaymentModeService paymentModeService;

    @Mock
    private PaymentModeModel paymentMode;

    @Mock
    private AddressService addressService;

    @Mock
    private OrderModel previousOrder;

    @Mock
    private Collection<SavedCncStoreDetailsModel> cncDetails;

    @Mock
    private SavedCncStoreDetailsModel cncDetail;

    @Mock
    private Iterator<SavedCncStoreDetailsModel> cncDetailsIterator;

    @Mock
    private UserService userService;

    @Mock
    private TitleModel title;

    @Mock
    private AddressModel clonedAddressModel;

    @Mock
    private SessionService sessionService;

    @Mock
    private TargetAddressVerificationService targetAddressVerificationService;


    @Before
    public void setUp() {
        given(customer.getDefaultPaymentAddress()).willReturn(paymentAddress);
        given(customer.getDefaultShipmentAddress()).willReturn(shippingAddress);
        given(customer.getFlyBuysCode()).willReturn("123");
        given(paymentModeService.getPaymentModeForCode(PaymentProviderTypeEnum.CREDITCARD.getCode()))
                .willReturn(paymentMode);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullModel() throws InterceptorException {
        cartModel = null;
        targetZoneDeliveryModeModel = null;
        targetCartService.setDeliveryMode(cartModel, targetZoneDeliveryModeModel);
    }

    @Test
    public void testNullOrderTargetZoneDeliveryModeValue() throws InterceptorException {
        given(cartModel.getZoneDeliveryModeValue()).willReturn(null);
        given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        given(targetCartService.getZoneDeliveryModeValueFromDeliveryMode(cartModel)).willReturn(
                targetZoneDeliveryModeValueModel);
        targetCartService.setDeliveryMode(cartModel, targetZoneDeliveryModeModel);
        verify(cartModel).setZoneDeliveryModeValue(targetZoneDeliveryModeValueModel);
    }

    @Test
    public void testExistingOrderTargetZoneDeliveryModeValue() throws InterceptorException {
        given(cartModel.getZoneDeliveryModeValue()).willReturn(targetZoneDeliveryModeValueModel);
        given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        given(targetZoneDeliveryModeValueModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        given(targetCartService.getZoneDeliveryModeValueFromDeliveryMode(cartModel)).willReturn(
                targetZoneDeliveryModeValueModel);
        targetCartService.setDeliveryMode(cartModel, targetZoneDeliveryModeModel2);
        verify(cartModel).setZoneDeliveryModeValue(targetZoneDeliveryModeValueModel);
    }

    @Test
    public void testTargetZoneDeliveryModeValuegivenZDMVNull() throws InterceptorException {
        given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        given(targetZoneDeliveryModeValueModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        given(targetCartService.getZoneDeliveryModeValueFromDeliveryMode(cartModel)).willThrow(
                new TargetNoPostCodeException("No postcode found"));
        targetCartService.setDeliveryMode(cartModel, targetZoneDeliveryModeModel2);
        verify(cartModel).setZoneDeliveryModeValue(null);
        Assert.assertEquals(null, cartModel.getZoneDeliveryModeValue());
        Assert.assertEquals(cartModel.getDeliveryMode(), targetZoneDeliveryModeModel);
    }

    @Test
    public void testSetCncStoreNumberDeliveryTypeCNC() throws InterceptorException {
        given(cartModel.getCncStoreNumber()).willReturn(null);
        given(cartModel.getZoneDeliveryModeValue()).willReturn(null);
        given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        given(targetCartService.getZoneDeliveryModeValueFromDeliveryMode(cartModel)).willReturn(
                targetZoneDeliveryModeValueModel);
        given(targetZoneDeliveryModeModel.getIsDeliveryToStore()).willReturn(Boolean.TRUE);

        targetCartService.setDeliveryMode(cartModel, targetZoneDeliveryModeModel);
        verify(cartModel).setZoneDeliveryModeValue(targetZoneDeliveryModeValueModel);
        verify(cartModel).setCalculated(Boolean.FALSE);
        verify(cartModel, never()).setCncStoreNumber(null);
        verify(cartModel).setDeliveryAddress(null);
    }

    @Test
    public void testSetCncStoreNumberDeliveryTypeHD() throws InterceptorException {
        given(cartModel.getCncStoreNumber()).willReturn(Integer.valueOf(5001));
        given(cartModel.getZoneDeliveryModeValue()).willReturn(null);
        given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        given(targetCartService.getZoneDeliveryModeValueFromDeliveryMode(cartModel)).willReturn(
                targetZoneDeliveryModeValueModel);
        given(targetZoneDeliveryModeModel.getIsDeliveryToStore()).willReturn(Boolean.FALSE);

        targetCartService.setDeliveryMode(cartModel, targetZoneDeliveryModeModel);
        verify(cartModel).setZoneDeliveryModeValue(targetZoneDeliveryModeValueModel);
        verify(cartModel).setCalculated(Boolean.FALSE);
        verify(cartModel).setCncStoreNumber(null);
        verify(cartModel).setDeliveryAddress(null);
    }

    @Test
    public void testSetCncStoreNumberDeliveryTypeNull() throws InterceptorException {
        given(cartModel.getCncStoreNumber()).willReturn(null);
        given(cartModel.getZoneDeliveryModeValue()).willReturn(null);
        given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        given(targetCartService.getZoneDeliveryModeValueFromDeliveryMode(cartModel)).willReturn(
                targetZoneDeliveryModeValueModel);
        given(targetZoneDeliveryModeModel.getIsDeliveryToStore()).willReturn(null);

        targetCartService.setDeliveryMode(cartModel, targetZoneDeliveryModeModel);
        verify(cartModel).setZoneDeliveryModeValue(targetZoneDeliveryModeValueModel);
        verify(cartModel).setCalculated(Boolean.FALSE);
        verify(cartModel).setCncStoreNumber(null);
        verify(cartModel, never()).setDeliveryAddress(null);
    }

    @Test
    public void testSetDeliveryModeWithDeliveryModeModelNoStoreNumber() throws InterceptorException {
        given(cartModel.getCncStoreNumber()).willReturn(null);
        given(cartModel.getZoneDeliveryModeValue()).willReturn(null);
        given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        given(targetCartService.getZoneDeliveryModeValueFromDeliveryMode(cartModel)).willReturn(
                targetZoneDeliveryModeValueModel);

        final DeliveryModeModel mockDeliveryMode = mock(DeliveryModeModel.class);

        targetCartService.setDeliveryMode(cartModel, mockDeliveryMode);
        verify(cartModel).setZoneDeliveryModeValue(targetZoneDeliveryModeValueModel);
        verify(cartModel).setCalculated(Boolean.FALSE);
        verify(cartModel).setCncStoreNumber(null);
        verify(cartModel, never()).setDeliveryAddress(null);
    }

    @Test
    public void testSetDeliveryModeWithDeliveryModeModelWithStoreNumber() throws InterceptorException {
        given(cartModel.getCncStoreNumber()).willReturn(Integer.valueOf(5001));
        given(cartModel.getZoneDeliveryModeValue()).willReturn(null);
        given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        given(targetCartService.getZoneDeliveryModeValueFromDeliveryMode(cartModel)).willReturn(
                targetZoneDeliveryModeValueModel);

        final DeliveryModeModel mockDeliveryMode = mock(DeliveryModeModel.class);

        targetCartService.setDeliveryMode(cartModel, mockDeliveryMode);
        verify(cartModel).setZoneDeliveryModeValue(targetZoneDeliveryModeValueModel);
        verify(cartModel).setCalculated(Boolean.FALSE);
        verify(cartModel).setCncStoreNumber(null);
        verify(cartModel).setDeliveryAddress(null);
    }

    @Test
    public void testSetDeliveryModeDeliveryTypeCNCWithStoreNumber() throws InterceptorException {
        given(cartModel.getCncStoreNumber()).willReturn(Integer.valueOf(5001));
        given(cartModel.getZoneDeliveryModeValue()).willReturn(null);
        given(cartModel.getDeliveryMode()).willReturn(targetZoneDeliveryModeModel);
        given(targetCartService.getZoneDeliveryModeValueFromDeliveryMode(cartModel)).willReturn(
                targetZoneDeliveryModeValueModel);
        given(targetZoneDeliveryModeModel.getIsDeliveryToStore()).willReturn(Boolean.TRUE);

        targetCartService.setDeliveryMode(cartModel, targetZoneDeliveryModeModel);
        verify(cartModel).setZoneDeliveryModeValue(targetZoneDeliveryModeValueModel);
        verify(cartModel).setCalculated(Boolean.FALSE);
        verify(cartModel, never()).setCncStoreNumber(null);
        verify(cartModel, never()).setDeliveryAddress(null);
    }

    /**
     * Method to test for a non cnc order.
     */
    @Test
    public void testPrePopulateForNonCnCOrder() {
        targetCartService.prePopulateCart(cartModel, preferredDeliveryMode, customer, paymentInfo, previousOrder);
        given(preferredDeliveryMode.getIsDeliveryToStore()).willReturn(Boolean.FALSE);
        verify(cartModel).setPaymentInfo(paymentInfo);
        verify(cartModel).setDeliveryMode(preferredDeliveryMode);
        verify(cartModel).setPaymentAddress(paymentAddress);
        verify(cartModel).setFlyBuysCode("123");
        verify(cartModel, never()).setPaymentMode(paymentMode);
        verify(cartModel).setDeliveryAddress(shippingAddress);
        verify(modelService).save(cartModel);
        verify(sessionService, never()).setAttribute(anyString(),
                anyString());
    }

    /**
     * Method to test for a cnc order.
     * 
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    @Test
    public void testPrePopulateForCnCOrder() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final Integer storeCnCNumber = Integer.valueOf(100);
        given(preferredDeliveryMode.getIsDeliveryToStore()).willReturn(Boolean.TRUE);
        given(customer.getSavedCncStoreDetails()).willReturn(cncDetails);
        given(cncDetails.iterator()).willReturn(cncDetailsIterator);
        given(customer.getSavedCncStoreDetails()).willReturn(cncDetails);
        given(userService.getTitleForCode("mr")).willReturn(title);
        given(cncDetailsIterator.next()).willReturn(cncDetail);
        given(addressService.cloneAddressForOwner(targetPointOfService.getAddress(), cartModel)).willReturn(
                shippingAddress);
        given(shippingAddress.getPostalcode()).willReturn("3000");
        given(cncDetail.getStore()).willReturn(targetPointOfService);
        given(targetPointOfService.getStoreNumber()).willReturn(storeCnCNumber);
        given(cncDetail.getTitle()).willReturn("mr");
        given(cartModel.getDeliveryAddress()).willReturn(shippingAddress);
        given(
                Boolean.valueOf(targetPointOfServiceService.isSupportAllProductTypesInCart(targetPointOfService,
                        cartModel)))
                                .willReturn(Boolean.valueOf(true));
        targetCartService.prePopulateCart(cartModel, preferredDeliveryMode, customer, paymentInfo, previousOrder);

        verify(cartModel).setPaymentInfo(paymentInfo);
        verify(cartModel).setDeliveryMode(preferredDeliveryMode);
        verify(cartModel).setPaymentAddress(paymentAddress);
        verify(cartModel).setFlyBuysCode("123");
        verify(cartModel, never()).setPaymentMode(paymentMode);
        verify(cartModel).setCncStoreNumber(targetPointOfService.getStoreNumber());
        verify(shippingAddress).setTitle(title);
        verify(shippingAddress).setFirstname(cncDetail.getFirstName());
        verify(shippingAddress).setLastname(cncDetail.getLastName());
        verify(shippingAddress).setPhone1(cncDetail.getPhoneNumber());
        verify(cartModel).setDeliveryAddress(shippingAddress);
        verify(modelService).save(cartModel);
        verify(sessionService).setAttribute(TgtCoreConstants.SESSION_POSTCODE, "3000");
    }

    @Test
    public void testPrePopulateForCnCOrderWithUnsupportedProductTypes() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final Integer storeCnCNumber = Integer.valueOf(100);
        given(preferredDeliveryMode.getIsDeliveryToStore()).willReturn(Boolean.TRUE);
        given(customer.getSavedCncStoreDetails()).willReturn(cncDetails);
        given(cncDetails.iterator()).willReturn(cncDetailsIterator);
        given(customer.getSavedCncStoreDetails()).willReturn(cncDetails);
        given(userService.getTitleForCode("mr")).willReturn(title);
        given(cncDetailsIterator.next()).willReturn(cncDetail);
        given(addressService.cloneAddressForOwner(targetPointOfService.getAddress(), cartModel)).willReturn(
                shippingAddress);
        given(shippingAddress.getPostalcode()).willReturn("3000");
        given(cncDetail.getStore()).willReturn(targetPointOfService);
        given(targetPointOfService.getStoreNumber()).willReturn(storeCnCNumber);
        given(cncDetail.getTitle()).willReturn("mr");
        given(cartModel.getDeliveryAddress()).willReturn(shippingAddress);
        given(
                Boolean.valueOf(targetPointOfServiceService.isSupportAllProductTypesInCart(targetPointOfService,
                        cartModel)))
                                .willReturn(Boolean.valueOf(false));
        targetCartService.prePopulateCart(cartModel, preferredDeliveryMode, customer, paymentInfo, previousOrder);

        verify(cartModel).setPaymentInfo(paymentInfo);
        verify(cartModel).setDeliveryMode(preferredDeliveryMode);
        verify(cartModel).setPaymentAddress(paymentAddress);
        verify(cartModel).setFlyBuysCode("123");
        verify(cartModel, never()).setPaymentMode(paymentMode);
        verify(cartModel, never()).setCncStoreNumber(targetPointOfService.getStoreNumber());
        verify(shippingAddress, never()).setTitle(title);
        verify(shippingAddress, never()).setFirstname(cncDetail.getFirstName());
        verify(shippingAddress, never()).setLastname(cncDetail.getLastName());
        verify(shippingAddress, never()).setPhone1(cncDetail.getPhoneNumber());
        verify(cartModel, never()).setDeliveryAddress(shippingAddress);
        verify(modelService).save(cartModel);
        verify(sessionService).setAttribute(TgtCoreConstants.SESSION_POSTCODE, "3000");
    }

    /**
     * Method to test for a cnc order picking up details from previous order.
     * 
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    @Test
    public void testPrePopulateForCnCOrderUsingPreviousOrder() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final Integer storeCnCNumber = Integer.valueOf(100);
        given(preferredDeliveryMode.getIsDeliveryToStore()).willReturn(Boolean.TRUE);
        given(customer.getSavedCncStoreDetails()).willReturn(CollectionUtils.EMPTY_COLLECTION);
        given(targetPointOfServiceService.getPOSByStoreNumber(storeCnCNumber)).willReturn(
                targetPointOfService);
        given(addressService.cloneAddressForOwner(targetPointOfService.getAddress(), cartModel)).willReturn(
                clonedAddressModel);
        given(previousOrder.getCncStoreNumber()).willReturn(Integer.valueOf(100));
        given(previousOrder.getDeliveryAddress()).willReturn(shippingAddress);
        given(shippingAddress.getPostalcode()).willReturn("3000");
        given(cartModel.getDeliveryAddress()).willReturn(shippingAddress);
        given(
                Boolean.valueOf(targetPointOfServiceService.isSupportAllProductTypesInCart(targetPointOfService,
                        cartModel)))
                                .willReturn(Boolean.valueOf(true));
        targetCartService.prePopulateCart(cartModel, preferredDeliveryMode, customer, paymentInfo, previousOrder);

        verify(cartModel).setPaymentInfo(paymentInfo);
        verify(cartModel).setDeliveryMode(preferredDeliveryMode);
        verify(cartModel).setPaymentAddress(paymentAddress);
        verify(cartModel).setFlyBuysCode("123");
        verify(cartModel, never()).setPaymentMode(paymentMode);
        verify(cartModel).setCncStoreNumber(targetPointOfService.getStoreNumber());
        verify(clonedAddressModel).setTitle(shippingAddress.getTitle());
        verify(clonedAddressModel).setFirstname(shippingAddress.getFirstname());
        verify(clonedAddressModel).setLastname(shippingAddress.getLastname());
        verify(clonedAddressModel).setPhone1(shippingAddress.getPhone1());
        verify(cartModel).setDeliveryAddress(clonedAddressModel);
        verify(modelService).save(cartModel);
        verify(sessionService).setAttribute(TgtCoreConstants.SESSION_POSTCODE, "3000");
    }

    @Test
    public void testPrePopulateForCnCOrderUsingPreviousOrderWithUnsupportedProductTypes()
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final Integer storeCnCNumber = Integer.valueOf(100);
        given(preferredDeliveryMode.getIsDeliveryToStore()).willReturn(Boolean.TRUE);
        given(customer.getSavedCncStoreDetails()).willReturn(CollectionUtils.EMPTY_COLLECTION);
        given(targetPointOfServiceService.getPOSByStoreNumber(storeCnCNumber)).willReturn(
                targetPointOfService);
        given(addressService.cloneAddressForOwner(targetPointOfService.getAddress(), cartModel)).willReturn(
                clonedAddressModel);
        given(previousOrder.getCncStoreNumber()).willReturn(Integer.valueOf(100));
        given(previousOrder.getDeliveryAddress()).willReturn(shippingAddress);
        given(shippingAddress.getPostalcode()).willReturn("3000");
        given(cartModel.getDeliveryAddress()).willReturn(shippingAddress);
        given(
                Boolean.valueOf(targetPointOfServiceService.isSupportAllProductTypesInCart(targetPointOfService,
                        cartModel)))
                                .willReturn(Boolean.valueOf(false));
        targetCartService.prePopulateCart(cartModel, preferredDeliveryMode, customer, paymentInfo, previousOrder);

        verify(cartModel).setPaymentInfo(paymentInfo);
        verify(cartModel).setDeliveryMode(preferredDeliveryMode);
        verify(cartModel).setPaymentAddress(paymentAddress);
        verify(cartModel).setFlyBuysCode("123");
        verify(cartModel, never()).setPaymentMode(paymentMode);
        verify(cartModel, never()).setCncStoreNumber(targetPointOfService.getStoreNumber());
        verify(clonedAddressModel, never()).setTitle(shippingAddress.getTitle());
        verify(clonedAddressModel, never()).setFirstname(shippingAddress.getFirstname());
        verify(clonedAddressModel, never()).setLastname(shippingAddress.getLastname());
        verify(clonedAddressModel, never()).setPhone1(shippingAddress.getPhone1());
        verify(cartModel, never()).setDeliveryAddress(clonedAddressModel);
        verify(modelService).save(cartModel);
        verify(sessionService).setAttribute(TgtCoreConstants.SESSION_POSTCODE, "3000");
    }

    @Test
    public void testPrePopulateForNonCnCOrderFromPreviousOrder() {
        given(previousOrder.getDeliveryAddress()).willReturn(shippingAddress);
        given(previousOrder.getPaymentAddress()).willReturn(paymentAddress);
        given(shippingAddress.getPostalcode()).willReturn("3000");
        given(customer.getDefaultPaymentAddress()).willReturn(null);
        given(customer.getDefaultShipmentAddress()).willReturn(null);
        given(paymentAddress.getStreetname()).willReturn("12/14 Thompson Rd");
        given(paymentAddress.getStreetnumber()).willReturn(null);
        given(paymentAddress.getTown()).willReturn("North Geelong");
        given(paymentAddress.getDistrict()).willReturn("VIC");
        given(paymentAddress.getPostalcode()).willReturn("3215");
        willReturn(Boolean.TRUE)
                .given(targetAddressVerificationService).isAddressVerified("12/14 Thompson Rd, North Geelong VIC 3215");
        targetCartService.prePopulateCart(cartModel, preferredDeliveryMode, customer, paymentInfo, previousOrder);
        verify(cartModel).setPaymentInfo(paymentInfo);
        verify(cartModel).setDeliveryMode(preferredDeliveryMode);
        verify(cartModel).setPaymentAddress(previousOrder.getPaymentAddress());
        verify(cartModel).setFlyBuysCode("123");
        verify(cartModel, never()).setPaymentMode(paymentMode);
        verify(cartModel).setDeliveryAddress(null);
        verify(targetAddressVerificationService).isAddressVerified("12/14 Thompson Rd, North Geelong VIC 3215");
        verify(sessionService, never()).setAttribute(TgtCoreConstants.SESSION_POSTCODE, "3000");
    }
}
package au.com.target.tgtcore.category.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.session.MockSessionService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.category.daos.TargetCategoryDao;
import au.com.target.tgtcore.constants.TgtCoreConstants;



/**
 * Test suite for {@link TargetCategoryServiceImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCategoryServiceImplTest {

    @Mock
    private TargetCategoryDao categoryDao;

    @Mock
    private SearchRestrictionService searchRestrictionService;

    private SessionService sessionService;

    @Mock
    private CatalogVersionService catalogVersionService;

    @Mock
    private CatalogVersionModel onlineCatalog;

    @Spy
    @InjectMocks
    private final TargetCategoryServiceImpl categoryService = new TargetCategoryServiceImpl();

    /**
     * Verifies that leaf categories are obtained through the category DAO object.
     */
    @Before
    public void setup() {
        BDDMockito.doReturn(catalogVersionService).when(categoryService).getCatalogVersionService();
        sessionService = new MockSessionService();
        categoryService.setSessionService(sessionService);
        given(catalogVersionService.getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS, TgtCoreConstants.Catalog.ONLINE_VERSION)).willReturn(onlineCatalog);
    }

    @Test
    public void testGetLeafCategories() {
        categoryService.getLeafCategoriesWithProducts();
        verify(categoryDao).findLeafCategoriesWithProducts();
    }

    /**
     * Verifies that all categories are obtained through the category DAO object.
     */
    @Test
    public void testGetAllCategories() {
        categoryService.getAllCategories();
        verify(categoryDao).findAllCategories();
    }

    @Test
    public void testGetAllCategoriesWithProducts() {
        categoryService.getAllCategoriesWithProducts();
        verify(categoryDao).findAllCategoriesWithProducts();
    }

    @Test
    public void testGetAllSuperCategoriesForProductWithNullProduct() {
        final ProductModel product = null;
        final Set<CategoryModel> result = categoryService.getAllSuperCategoriesForProduct(product);
        Assert.assertTrue(result.isEmpty());
    }

    @Test
    public void testGetAllSuperCategoriesForProductWithNullProductSuperCategories() {
        final ProductModel product = Mockito.mock(ProductModel.class);
        BDDMockito.doReturn(null).when(categoryService).findAllOnlineSuperCategoriesByProduct(product);
        final Set<CategoryModel> result = categoryService.getAllSuperCategoriesForProduct(product);
        Assert.assertTrue(result.isEmpty());
    }

    @Test
    public void testGetAllSuperCategoriesForProductWithFirstLevelCategories() {
        final CategoryModel categoryFirstLevel1 = Mockito.mock(CategoryModel.class);
        final CategoryModel categoryFirstLevel2 = Mockito.mock(CategoryModel.class);
        final Collection<CategoryModel> productSuperCategories = Arrays
                .asList(categoryFirstLevel1, categoryFirstLevel2);
        final ProductModel product = Mockito.mock(ProductModel.class);
        BDDMockito.doReturn(productSuperCategories).when(categoryService)
                .findAllOnlineSuperCategoriesByProduct(product);
        final Set<CategoryModel> result = categoryService.getAllSuperCategoriesForProduct(product);
        Assert.assertTrue(result.containsAll(productSuperCategories));
    }

    @Test
    public void testGetAllSuperCategoriesForProductWithMultiLevelCategories() {
        final CategoryModel categoryFirstLevel1 = Mockito.mock(CategoryModel.class);

        final CategoryModel categorySecondLevel1 = Mockito.mock(CategoryModel.class);
        final CategoryModel categorySecondLevel2 = Mockito.mock(CategoryModel.class);

        given(categoryFirstLevel1.getSupercategories()).willReturn(
                Arrays.asList(categorySecondLevel1, categorySecondLevel2));

        final CategoryModel categoryThirdLevel1 = Mockito.mock(CategoryModel.class);
        final CategoryModel categoryThirdLevel2 = Mockito.mock(CategoryModel.class);

        given(categorySecondLevel1.getSupercategories()).willReturn(Arrays.asList(categoryThirdLevel1));
        given(categorySecondLevel2.getSupercategories()).willReturn(Arrays.asList(categoryThirdLevel2));

        final CategoryModel categoryrootLevel = Mockito.mock(CategoryModel.class);
        given(categoryThirdLevel1.getSupercategories()).willReturn(Arrays.asList(categoryrootLevel));
        given(categoryThirdLevel2.getSupercategories()).willReturn(Arrays.asList(categoryrootLevel));


        final CategoryModel categoryFirstLevel2 = Mockito.mock(CategoryModel.class);

        final CategoryModel categorySecondLevel3 = Mockito.mock(CategoryModel.class);
        final CategoryModel categorySecondLevel4 = Mockito.mock(CategoryModel.class);

        given(categoryFirstLevel2.getSupercategories()).willReturn(
                Arrays.asList(categorySecondLevel3, categorySecondLevel4));

        given(categorySecondLevel3.getSupercategories()).willReturn(Arrays.asList(categoryThirdLevel1));
        given(categorySecondLevel4.getSupercategories()).willReturn(Arrays.asList(categoryThirdLevel2));

        final ProductModel product = Mockito.mock(ProductModel.class);
        BDDMockito.doReturn(Arrays.asList(categoryFirstLevel1, categoryFirstLevel2)).when(categoryService)
                .findAllOnlineSuperCategoriesByProduct(product);
        final Set<CategoryModel> result = categoryService.getAllSuperCategoriesForProduct(product);

        final Collection<CategoryModel> expectResult = Arrays.asList(categoryFirstLevel1, categoryFirstLevel2,
                categorySecondLevel1, categorySecondLevel2, categorySecondLevel3, categorySecondLevel4,
                categoryThirdLevel1, categoryThirdLevel2, categoryrootLevel);
        Assert.assertTrue(result.containsAll(expectResult));
        Assert.assertTrue(CollectionUtils.isEqualCollection(expectResult, result));
    }

    @Test
    public void testFindAllOnlineSuperCategoriesByProduct() {
        final CategoryModel category1 = Mockito.mock(CategoryModel.class);
        final CategoryModel category2 = Mockito.mock(CategoryModel.class);
        final Collection<CategoryModel> expectedcategories = new ArrayList<CategoryModel>(Arrays.asList(category1,
                category2));
        final ProductModel product = Mockito.mock(ProductModel.class);
        given(categoryDao.findCategoriesByCatalogVersionAndProduct(onlineCatalog, product)).willReturn(
                expectedcategories);
        final Collection<CategoryModel> result = categoryService.findAllOnlineSuperCategoriesByProduct(product);
        Assert.assertEquals(expectedcategories, result);
    }

    @Test
    public void testFindAllOnlineSuperCategoriesByProductWithExpection() {
        final ProductModel product = Mockito.mock(ProductModel.class);
        given(categoryDao.findCategoriesByCatalogVersionAndProduct(onlineCatalog, product)).willThrow(
                new IllegalArgumentException());
        try {
            categoryService.findAllOnlineSuperCategoriesByProduct(product);
            Assert.fail();
        }
        catch (final Exception e) {
            Assert.assertTrue(true);
        }

        verify(searchRestrictionService).enableSearchRestrictions();
    }
}

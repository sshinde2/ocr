/**
 * 
 */
package au.com.target.tgtcore.config.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.ConversionException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.config.daos.TargetSharedConfigDao;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetSharedConfigModel;


/**
 * @author bhuang3
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetSharedConfigServiceImplTest {

    @InjectMocks
    @Spy
    private final TargetSharedConfigService targetSharedConfigService = new TargetSharedConfigServiceImpl();

    @Mock
    private TargetSharedConfigDao targetSharedConfigDao;

    @Mock
    private ModelService modelService;

    @Test
    public void testGetConfigByCode() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final TargetSharedConfigModel targetSharedConfigModel = new TargetSharedConfigModel();
        targetSharedConfigModel.setCode("test code");
        targetSharedConfigModel.setValue("test result");
        given(targetSharedConfigDao.getSharedConfigByCode(Mockito.anyString())).willReturn(
                targetSharedConfigModel);
        final String result = targetSharedConfigService.getConfigByCode("test code");
        assertThat(result).isEqualTo("test result");
    }

    @Test
    public void testGetConfigByCodeReturnNull() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetSharedConfigModel targetSharedConfigModel = new TargetSharedConfigModel();
        targetSharedConfigModel.setCode("test code");
        targetSharedConfigModel.setValue("test result");
        given(targetSharedConfigDao.getSharedConfigByCode(Mockito.anyString())).willThrow(
                new TargetUnknownIdentifierException("unknow identifier"));
        final String result = targetSharedConfigService.getConfigByCode("test code");
        assertThat(result).isNull();
    }


    @Test
    public void testSetSharedConfigWithEmptyEntries() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final Map configEntries = new HashMap<String, String>();
        targetSharedConfigService.setSharedConfigs(configEntries);
        verify(modelService, Mockito.never()).save(Mockito.any(TargetSharedConfigModel.class));
    }


    @Test
    public void testSetSharedConfigwithNull() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final Map configEntries = new HashMap<String, String>();
        configEntries.put("db.type.system.name", "DEFAULT");
        given(targetSharedConfigDao.getSharedConfigByCode(Mockito.anyString())).willThrow(
                new TargetUnknownIdentifierException("unknow identifier"));
        given(modelService.create(TargetSharedConfigModel.class)).willReturn(new TargetSharedConfigModel());
        targetSharedConfigService.setSharedConfigs(configEntries);
        verify(modelService).save(Mockito.any(TargetSharedConfigModel.class));
        verify(modelService, Mockito.never()).remove(Mockito.any(TargetSharedConfigModel.class));
    }

    @Test
    public void testSetSharedConfig() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final Map configEntries = new HashMap<String, String>();
        configEntries.put("db.type.system.name", "DEFAULT");
        final TargetSharedConfigModel targetSharedConfigModel = new TargetSharedConfigModel();
        targetSharedConfigModel.setCode("db.type.system.name");
        targetSharedConfigModel.setValue("DEFAULT");
        targetSharedConfigModel.setCode("key2");
        targetSharedConfigModel.setValue("value2");
        given(targetSharedConfigDao.getSharedConfigByCode(Mockito.anyString())).willReturn(
                targetSharedConfigModel);
        targetSharedConfigService.setSharedConfigs(configEntries);
        verify(modelService).save(Mockito.any(TargetSharedConfigModel.class));
    }


    @Test
    public void testSetSharedConfigToRemoveEntry() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final Map configEntries = new HashMap<String, String>();
        configEntries.put("db.type.system.name", null);
        final TargetSharedConfigModel targetSharedConfigModel = new TargetSharedConfigModel();
        targetSharedConfigModel.setCode("db.type.system.name");
        targetSharedConfigModel.setValue("DEFAULT");
        given(targetSharedConfigDao.getSharedConfigByCode(Mockito.anyString())).willReturn(
                targetSharedConfigModel);
        targetSharedConfigService.setSharedConfigs(configEntries);
        verify(modelService, Mockito.never()).save(Mockito.any(TargetSharedConfigModel.class));
        verify(modelService).remove(Mockito.any(TargetSharedConfigModel.class));
    }

    @Test
    public void testGetIntWithNoValue() {
        doReturn(null).when(targetSharedConfigService).getConfigByCode("valueKey");
        final int result = targetSharedConfigService.getInt("valueKey", 7);
        assertThat(result).isEqualTo(7);
    }

    @Test(expected = ConversionException.class)
    public void testGetIntWithNonIntValue() {
        doReturn("elephant").when(targetSharedConfigService).getConfigByCode("valueKey");
        targetSharedConfigService.getInt("valueKey", 7);
    }

    @Test
    public void testGetInt() {
        doReturn("11").when(targetSharedConfigService).getConfigByCode("valueKey");
        final int result = targetSharedConfigService.getInt("valueKey", 7);
        assertThat(result).isEqualTo(11);
    }


    @Test
    public void testGetDoubleWithNoValue() {
        doReturn(null).when(targetSharedConfigService).getConfigByCode("valueKey");
        final double result = targetSharedConfigService.getDouble("valueKey", 100.55d);
        assertThat(result).isEqualTo(100.55d);
    }

    @Test(expected = ConversionException.class)
    public void testGetDoubleWithNonIntValue() {
        doReturn("elephant").when(targetSharedConfigService).getConfigByCode("valueKey");
        targetSharedConfigService.getDouble("valueKey", 100.55d);
    }

    @Test
    public void testGetDoubleWrapper() {
        doReturn("111.15").when(targetSharedConfigService).getConfigByCode("valueKey");
        final Double result = targetSharedConfigService.getDoubleWrapper("valueKey", 100.55d);
        assertThat(result).isEqualTo(Double.valueOf(111.15));
    }

    @Test
    public void testGetDoubleWrapperWithNoValue() {
        doReturn(null).when(targetSharedConfigService).getConfigByCode("valueKey");
        final Double result = targetSharedConfigService.getDoubleWrapper("valueKey", 100.55d);
        assertThat(result).isEqualTo(Double.valueOf(100.55));
    }

    @Test(expected = ConversionException.class)
    public void testGetDoubleWrapperWithNonIntValue() {
        doReturn("elephant").when(targetSharedConfigService).getConfigByCode("valueKey");
        targetSharedConfigService.getDoubleWrapper("valueKey", 100.55d);
    }

    @Test
    public void testGetDouble() {
        doReturn("111.15").when(targetSharedConfigService).getConfigByCode("valueKey");
        final double result = targetSharedConfigService.getDouble("valueKey", 100.55d);
        assertThat(result).isEqualTo(111.15);
    }
}

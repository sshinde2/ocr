/**
 * 
 */
package au.com.target.tgtcore.customer.impl;


import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.PasswordMismatchException;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.JaloConnection;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.payment.dto.BillingInfo;
import de.hybris.platform.persistence.property.PersistenceManager;
import de.hybris.platform.regioncache.CacheController;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.Session;
import de.hybris.platform.servicelayer.tenant.MockTenant;
import de.hybris.platform.servicelayer.user.PasswordEncoderService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import au.com.target.tgtbusproc.event.PasswordChangeEvent;
import au.com.target.tgtbusproc.event.UpdateCustomerEmailAddrEvent;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.customer.ReusePasswordException;
import au.com.target.tgtcore.customer.TargetBcryptPasswordEncoder;
import au.com.target.tgtcore.customer.TargetMD5PasswordEncoder;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.login.TargetAuthenticationService;
import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtpayment.dto.TargetCardInfo;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;
import au.com.target.tgtpayment.service.TargetPaymentService;


/**
 * Unit test for {@link TargetCustomerAccountServiceImpl}
 *
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Registry.class })
public class TargetCustomerAccountServiceImplTest {
    //CHECKSTYLE:OFF
    private static String CARDNUMBER = "41111111111111111";
    private static String CARD_EXP_MONTH = "11";
    private static String CARD_EXP_YEAR = "2013";
    private static String CARD_HOLDER_NAME = "Test";
    //CHECKSTYLE:ON

    @Mock
    private CacheController controller;

    @Mock
    private MockTenant currentTenant;

    @Mock
    private PersistenceManager persistanceManager;

    @Mock
    private JaloConnection jaloConnection;


    @Mock
    private SecureTokenService mockSecureTokenService;

    @Mock
    private TargetAuthenticationService mockTargetAuthenticationService;

    @Mock
    private UserService userService;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetFeatureSwitchService mockTargetFeatureSwitchService;


    @Mock
    private TargetBcryptPasswordEncoder bcryptPasswordEncoder;

    @Mock
    private TargetMD5PasswordEncoder md5PasswordEncoder;

    @Mock
    private KeyGenerator keyGenerator;


    @Mock
    private CustomerModel customerModel;

    @Mock
    private PaymentModeModel paymentModeModel;

    @Mock
    private TargetCardInfo targetCardInfo;

    @Mock
    private BillingInfo billingInfo;

    @Mock
    private CreditCardPaymentInfoModel creditCardPaymentInfoModel;

    @Mock
    private PinPadPaymentInfoModel pinPadPaymentInfoModel;

    @Mock
    private CreditCardPaymentInfoModel creditCardPaymentInfoModel2;

    @Mock
    private CreditCardPaymentInfoModel creditCardPaymentInfoModel3;

    @Mock
    private TargetAddressModel addressModel;

    @Mock
    private TitleModel titleModel;

    @Mock
    private CountryModel countryModel;

    @Mock
    private PaypalPaymentInfoModel paypalPaymentInfoModel;

    @Mock
    private AfterpayPaymentInfoModel afterpayPaymentInfoModel;

    @Mock
    private TargetPaymentService targetPaymentService;


    @Mock
    private CommonI18NService commonI18NService;


    @Mock
    private BaseStoreService baseStoreService;

    @Mock
    private BaseSiteService baseSiteService;

    @Mock
    private PasswordEncoderService mockPasswordEncoderService;

    @Mock
    private EventService mockEventService;

    private TargetCustomerAccountServiceImpl spy;


    @InjectMocks
    private final TargetCustomerAccountServiceImpl targetCustomerAccountServiceImpl = new TargetCustomerAccountServiceImpl();


    @Before
    public void setupTest() {
        PowerMockito.mockStatic(Registry.class, Mockito.RETURNS_MOCKS);
        given(Registry.getCurrentTenant()).willReturn(currentTenant);
        given(currentTenant.getJaloConnection()).willReturn(jaloConnection);
        given(jaloConnection.getPasswordEncoder("bcrypt")).willReturn(bcryptPasswordEncoder);
        given(jaloConnection.getPasswordEncoder("targetmd5")).willReturn(md5PasswordEncoder);


        given(customerModel.getUid()).willReturn("customer");
        given(modelService.create(CreditCardPaymentInfoModel.class)).willReturn(creditCardPaymentInfoModel);
        given(modelService.create(PaypalPaymentInfoModel.class)).willReturn(paypalPaymentInfoModel);
        given(modelService.create(AfterpayPaymentInfoModel.class)).willReturn(afterpayPaymentInfoModel);
        given(modelService.create(TargetAddressModel.class)).willReturn(addressModel);

        given(userService.getTitleForCode(Mockito.anyString())).willReturn(titleModel);
        given(commonI18NService.getCountry(Mockito.anyString())).willReturn(countryModel);

        given(targetCardInfo.getBillingInfo()).willReturn(billingInfo);
        given(targetPaymentService.tokenize(Mockito.anyString(), Mockito.any(PaymentModeModel.class)))
                .willReturn("123456");

        given(targetCardInfo.getCardNumber()).willReturn(CARDNUMBER);
        given(targetCardInfo.getCardType()).willReturn(CreditCardType.VISA);
        given(targetCardInfo.getExpirationMonth()).willReturn(Integer.valueOf(CARD_EXP_MONTH));
        given(targetCardInfo.getExpirationYear()).willReturn(Integer.valueOf(CARD_EXP_YEAR));
        given(targetCardInfo.getCardHolderFullName()).willReturn(CARD_HOLDER_NAME);

        given(billingInfo.getFirstName()).willReturn("firstName");
        given(billingInfo.getLastName()).willReturn("lastName");
        given(billingInfo.getStreet1()).willReturn("street1");
        given(billingInfo.getStreet2()).willReturn("street2");
        given(billingInfo.getCity()).willReturn("city");
        given(billingInfo.getState()).willReturn("state");
        given(billingInfo.getPostalCode()).willReturn("postalcode");
        given(billingInfo.getCountry()).willReturn("country");
        given(billingInfo.getPhoneNumber()).willReturn("phonenumber");

        given(modelService.create(PinPadPaymentInfoModel.class)).willReturn(pinPadPaymentInfoModel);
        targetCustomerAccountServiceImpl.setModelService(modelService);
        spy = spy(targetCustomerAccountServiceImpl);

        targetCustomerAccountServiceImpl.setPasswordEncoding("bcrypt");
        targetCustomerAccountServiceImpl.setLegacyPasswordEncoding("targetmd5");
    }


    @Test(expected = IllegalArgumentException.class)
    public void testCreateCreditCardPaymentInfoWithNullCustomer() {
        targetCustomerAccountServiceImpl.createCreditCardPaymentInfo(null, null, null, null, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCreditCardPaymentInfoWithNullPaymentMode() {
        targetCustomerAccountServiceImpl.createCreditCardPaymentInfo(customerModel, null, null, null, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCreditCardPaymentInfoWithNullCardInfo() {
        targetCustomerAccountServiceImpl
                .createCreditCardPaymentInfo(customerModel, paymentModeModel, null, null, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCreditCardPaymentInfoWithNullBillingInfo() {
        given(targetCardInfo.getBillingInfo()).willReturn(null);
        targetCustomerAccountServiceImpl
                .createCreditCardPaymentInfo(customerModel, paymentModeModel, targetCardInfo, null, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCreditCardPaymentInfoWithNullTitle() {
        targetCustomerAccountServiceImpl
                .createCreditCardPaymentInfo(customerModel, paymentModeModel, targetCardInfo, null, false);

    }

    @Test
    public void testCreateCreditCardPaymentInfoWithNullToken() {

        given(targetPaymentService.tokenize(Mockito.anyString(), Mockito.any(PaymentModeModel.class)))
                .willReturn(null);

        final CreditCardPaymentInfoModel result = targetCustomerAccountServiceImpl
                .createCreditCardPaymentInfo(customerModel, paymentModeModel, targetCardInfo, "mr", false);

        Assert.assertNull(result);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testCreateCreditCardPaymentInfo() {

        final CreditCardPaymentInfoModel result = targetCustomerAccountServiceImpl
                .createCreditCardPaymentInfo(customerModel, paymentModeModel, targetCardInfo, "mr", false);

        Assert.assertNotNull(result);

        //Verify the required interactions

        verify(userService).getTitleForCode("mr");
        verify(commonI18NService).getCountry(billingInfo.getCountry());

        verify(creditCardPaymentInfoModel).setUser(customerModel);
        verify(creditCardPaymentInfoModel).setSubscriptionId("123456");
        verify(creditCardPaymentInfoModel).setNumber(CARDNUMBER);
        verify(creditCardPaymentInfoModel).setValidToMonth(CARD_EXP_MONTH);
        verify(creditCardPaymentInfoModel).setValidToYear(CARD_EXP_YEAR);
        verify(creditCardPaymentInfoModel).setCcOwner(CARD_HOLDER_NAME);
        verify(creditCardPaymentInfoModel).setType(CreditCardType.VISA);
        verify(creditCardPaymentInfoModel).setSaved(false);
        verify(creditCardPaymentInfoModel).setBillingAddress(addressModel);

        verify(addressModel).setBillingAddress(Boolean.TRUE);
        verify(addressModel).setTitle(titleModel);
        verify(addressModel).setFirstname(billingInfo.getFirstName());
        verify(addressModel).setLastname(billingInfo.getLastName());
        verify(addressModel).setLine1(billingInfo.getStreet1());
        verify(addressModel).setLine2(billingInfo.getStreet2());
        verify(addressModel).setTown(billingInfo.getCity());
        verify(addressModel).setDistrict(billingInfo.getState());
        verify(addressModel).setCountry(countryModel);
        verify(addressModel).setPhone1(billingInfo.getPhoneNumber());
        verify(addressModel).setOwner(creditCardPaymentInfoModel);

        verify(modelService).saveAll(addressModel, creditCardPaymentInfoModel);
        verify(modelService).refresh(customerModel);

    }

    @Test
    public void testCreatePaypalPaymentInfoWithNullCart() {
        try {
            targetCustomerAccountServiceImpl.createPaypalPaymentInfo(null, null, null);
        }
        catch (final Exception ex) {
            assertThat(ex).isInstanceOf(IllegalArgumentException.class);
            assertThat(ex.getMessage()).isEqualTo("Customer cannot be null");
        }
    }

    @Test
    public void testCreatePaypalPaymentInfoWithNullPayerId() {
        try {
            targetCustomerAccountServiceImpl.createPaypalPaymentInfo(customerModel, null, null);
        }
        catch (final Exception ex) {
            assertThat(ex).isInstanceOf(IllegalArgumentException.class);
            assertThat(ex.getMessage()).isEqualTo("PayerId cannot be null");
        }
    }

    @Test
    public void testCreatePaypalPaymentInfoWithNullToken() {
        try {
            targetCustomerAccountServiceImpl.createPaypalPaymentInfo(customerModel, "payerId", null);
        }
        catch (final Exception ex) {
            assertThat(ex).isInstanceOf(IllegalArgumentException.class);
            assertThat(ex.getMessage()).isEqualTo("Token cannot be null");
        }
    }

    @Test
    public void testCreatePaypalPaymentInfo() {
        final PaypalPaymentInfoModel result = targetCustomerAccountServiceImpl.createPaypalPaymentInfo(customerModel,
                "payerId",
                "token");

        Assert.assertNotNull(result);
        Assert.assertEquals(paypalPaymentInfoModel, result);

        verify(paypalPaymentInfoModel).setCode(Mockito.anyString());
        verify(paypalPaymentInfoModel).setUser(customerModel);
        verify(paypalPaymentInfoModel).setToken("token");
        verify(paypalPaymentInfoModel).setPayerId("payerId");

        verify(modelService).save(paypalPaymentInfoModel);
        verify(modelService).refresh(customerModel);
    }

    @Test
    public void testGetCreditCardsPaymentInfosSorted() throws ParseException {
        final List<CreditCardPaymentInfoModel> creditCards = new ArrayList<>();
        creditCards.add(creditCardPaymentInfoModel);
        creditCards.add(creditCardPaymentInfoModel2);
        creditCards.add(creditCardPaymentInfoModel3);

        //create created dates for addresses
        final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        final Date date1 = sdf.parse("20/07/2013 00:00:00");
        final Date date2 = sdf.parse("19/06/2013 00:00:00");
        final Date date3 = sdf.parse("18/06/2013 00:00:00");
        given(creditCardPaymentInfoModel.getCreationtime()).willReturn(date2);
        given(creditCardPaymentInfoModel2.getCreationtime()).willReturn(date1);
        given(creditCardPaymentInfoModel3.getCreationtime()).willReturn(date3);

        final List<CreditCardPaymentInfoModel> result = targetCustomerAccountServiceImpl.sortCreditCards(creditCards);
        Assert.assertEquals(3, result.size());
        Assert.assertEquals(creditCardPaymentInfoModel2, result.get(0));
        Assert.assertEquals(creditCardPaymentInfoModel, result.get(1));
        Assert.assertEquals(creditCardPaymentInfoModel3, result.get(2));

    }

    @Test
    public void testUpdateCreditCardPaymentInfo() {
        given(targetPaymentService.tokenize(Mockito.anyString(), Mockito.any(PaymentModeModel.class)))
                .willReturn("test");
        final TargetCardInfo tci = Mockito.mock(TargetCardInfo.class);
        given(tci.getCardNumber()).willReturn("123");
        given(tci.getCardType()).willReturn(CreditCardType.VISA);
        given(tci.getExpirationMonth()).willReturn(Integer.valueOf("12"));
        given(tci.getExpirationYear()).willReturn(Integer.valueOf("1858"));
        given(tci.getCardHolderFullName()).willReturn("tester");
        given(userService.getTitleForCode("test")).willReturn(titleModel);
        given(creditCardPaymentInfoModel.getBillingAddress()).willReturn(addressModel);
        given(tci.getBillingInfo()).willReturn(billingInfo);
        targetCustomerAccountServiceImpl.updateCreditCardPaymentInfo(creditCardPaymentInfoModel, tci,
                "test", true, paymentModeModel);
        verify(creditCardPaymentInfoModel).setValidToYear("1858");
        verify(creditCardPaymentInfoModel).setValidToMonth("12");
        verify(creditCardPaymentInfoModel).setNumber("123");
        verify(modelService).saveAll(addressModel, creditCardPaymentInfoModel);
    }


    @Test
    public void testCreateUpdateCustomerEmailAddrEvent() {

        final String oldUid = "old@target.com.au";
        final UpdateCustomerEmailAddrEvent event = targetCustomerAccountServiceImpl.createUpdateCustomerEmailAddrEvent(
                customerModel, oldUid);
        Assert.assertNotNull(event);
        Assert.assertEquals(customerModel, event.getCustomer());
        Assert.assertEquals(oldUid, event.getOldContactEmail());
    }


    @Test(expected = IllegalArgumentException.class)
    public void testRegisterGuestForAnonymousCheckoutEmailAsNull() throws DuplicateUidException {
        final String email = null;
        targetCustomerAccountServiceImpl.registerGuestForAnonymousCheckout(email, "Guest");
    }




    @Test
    public void testRegisterGuestForAnonymousCheckout() throws DuplicateUidException {
        final String email = "test@test.com";
        final String name = "Guest";
        final String guid = "guid";

        given(keyGenerator.generate()).willReturn(guid);

        final LanguageModel languageModel = BDDMockito.mock(LanguageModel.class);
        final CurrencyModel currencyModel = BDDMockito.mock(CurrencyModel.class);

        final TargetCustomerModel guestCustomer = new TargetCustomerModel();
        given(modelService.create(TargetCustomerModel.class)).willReturn(guestCustomer);

        given(commonI18NService.getCurrentLanguage()).willReturn(languageModel);
        given(commonI18NService.getCurrentCurrency()).willReturn(currencyModel);

        final TargetCustomerModel targetCustomer = targetCustomerAccountServiceImpl.registerGuestForAnonymousCheckout(
                email, name);
        Assert.assertEquals(targetCustomer.getName(), name);
        Assert.assertEquals(StringUtils.substringBefore(targetCustomer.getUid(), "|"), guid);
        Assert.assertEquals(StringUtils.substringAfter(targetCustomer.getUid(), "|"), email);
        Assert.assertEquals(targetCustomer.getCustomerID(), guid);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdatePasswordWithNullToken() throws TokenInvalidatedException {
        targetCustomerAccountServiceImpl.updatePassword(null, "newPassword");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdatePasswordWithNullNewPassword() throws TokenInvalidatedException {
        targetCustomerAccountServiceImpl.updatePassword("token", null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdatePasswordWithBlankToken() throws TokenInvalidatedException {
        targetCustomerAccountServiceImpl.updatePassword("", "newPassword");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdatePasswordWithBlankNewPassword() throws TokenInvalidatedException {
        targetCustomerAccountServiceImpl.updatePassword("token", "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdatePasswordWithNonDecryptableToken() throws TokenInvalidatedException {
        final String token = "token";

        given(mockSecureTokenService.decryptData(token)).willThrow(
                new IllegalArgumentException("Decryption failure"));

        targetCustomerAccountServiceImpl.updatePassword(token, "newPassword");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdatePasswordWithExpiredToken() throws TokenInvalidatedException {
        final String token = "token";

        final String userId = "foo@bar.com";
        final long timeStamp = DateTime.now().minusSeconds(500).getMillis();

        final SecureToken secureToken = new SecureToken(userId, timeStamp);

        given(mockSecureTokenService.decryptData(token)).willReturn(secureToken);

        targetCustomerAccountServiceImpl.setTokenValiditySeconds(300);
        targetCustomerAccountServiceImpl.updatePassword(token, "newPassword");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdatePasswordWithNotExistingUser() throws TokenInvalidatedException {
        final String token = "token";

        final String userId = "foo@bar.com";
        final long timeStamp = DateTime.now().plusSeconds(500).getMillis();

        final SecureToken secureToken = new SecureToken(userId, timeStamp);
        given(mockSecureTokenService.decryptData(token)).willReturn(secureToken);

        targetCustomerAccountServiceImpl.setTokenValiditySeconds(300);
        targetCustomerAccountServiceImpl.updatePassword(token, "newPassword");
    }

    @Test(expected = TokenInvalidatedException.class)
    public void testUpdatePasswordWithNotMatchingTokens() throws TokenInvalidatedException {
        final String token = "token";
        final String userToken = "userToken";

        final String userId = "foo@bar.com";
        final long timeStamp = DateTime.now().plusSeconds(500).getMillis();

        final SecureToken secureToken = new SecureToken(userId, timeStamp);
        given(mockSecureTokenService.decryptData(token)).willReturn(secureToken);

        final TargetCustomerModel mockCustomerModel = mock(TargetCustomerModel.class);
        given(mockCustomerModel.getToken()).willReturn(userToken);
        given(userService.getUserForUID(userId, TargetCustomerModel.class)).willReturn(mockCustomerModel);

        targetCustomerAccountServiceImpl.setTokenValiditySeconds(300);
        targetCustomerAccountServiceImpl.updatePassword(token, "newPassword");
    }



    @Test
    public void testUpdatePasswordWithBcryptFeatureSwitchDisabled() throws TokenInvalidatedException {
        final String passwordEncoding = "bcrypt";
        final String legacyPasswordEncoding = "targetmd5";

        final String token = "token";
        final String newPassword = "newPassword";

        final String userId = "foo@bar.com";

        final long timeStamp = DateTime.now().plusSeconds(500).getMillis();

        final SecureToken secureToken = new SecureToken(userId, timeStamp);
        given(mockSecureTokenService.decryptData(token)).willReturn(secureToken);

        final TargetCustomerModel mockCustomerModel = mock(TargetCustomerModel.class);
        given(mockCustomerModel.getToken()).willReturn(token);
        given(userService.getUserForUID(userId, TargetCustomerModel.class)).willReturn(mockCustomerModel);
        given(mockCustomerModel.getEncodedPassword()).willReturn("oldEncryptedPass");
        given(mockCustomerModel.getPasswordEncoding()).willReturn("targetmd5");
        given(mockCustomerModel.getUid()).willReturn(userId);

        targetCustomerAccountServiceImpl.setTokenValiditySeconds(300);
        targetCustomerAccountServiceImpl.setPasswordEncoding(passwordEncoding);
        targetCustomerAccountServiceImpl.setLegacyPasswordEncoding(legacyPasswordEncoding);
        targetCustomerAccountServiceImpl.updatePassword(token, newPassword);

        verify(mockCustomerModel).setToken(null);
        verify(mockCustomerModel).setFailedLoginAttempts(Integer.valueOf(0));
        verify(modelService).save(mockCustomerModel);
        verify(userService).setPassword(userId, newPassword, legacyPasswordEncoding);
    }

    @Test
    public void testUpdatePasswordWithBcryptFeatureSwitchEnabled() throws TokenInvalidatedException {

        final String passwordEncoding = "bcrypt";
        final String legacyPasswordEncoding = "targetmd5";

        final String token = "token";
        final String newPassword = "newPassword";

        final String userId = "foo@bar.com";
        final long timeStamp = DateTime.now().plusSeconds(500).getMillis();
        given(bcryptPasswordEncoder.encode(userId, newPassword)).willReturn("bcryptEncryptedPassword");
        given(md5PasswordEncoder.encode(userId, newPassword)).willReturn("md5EncryptedPassword");

        final SecureToken secureToken = new SecureToken(userId, timeStamp);
        given(mockSecureTokenService.decryptData(token)).willReturn(secureToken);

        final TargetCustomerModel mockCustomerModel = mock(TargetCustomerModel.class);
        given(mockCustomerModel.getToken()).willReturn(token);

        given(mockCustomerModel.getEncodedPassword()).willReturn("oldEncryptedPass");
        given(mockCustomerModel.getPasswordEncoding()).willReturn("bcrypt");
        given(userService.getUserForUID(userId, TargetCustomerModel.class)).willReturn(mockCustomerModel);

        doReturn(Boolean.TRUE).when(mockTargetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.BCRYPT_PASSWORD_ENCODING);

        targetCustomerAccountServiceImpl.setTokenValiditySeconds(300);
        targetCustomerAccountServiceImpl.setPasswordEncoding(passwordEncoding);
        targetCustomerAccountServiceImpl.setLegacyPasswordEncoding(legacyPasswordEncoding);
        targetCustomerAccountServiceImpl.updatePassword(token, newPassword);

        verify(mockCustomerModel).setToken(null);
        verify(mockCustomerModel).setFailedLoginAttempts(Integer.valueOf(0));
        verify(modelService).save(mockCustomerModel);
        verify(userService).setPassword(userId, newPassword, passwordEncoding);
    }


    @Test(expected = ReusePasswordException.class)
    public void testThrowExceptionWhenUpdatingSamePasswordUsingBcrypt() throws TokenInvalidatedException {

        final String passwordEncoding = "bcrypt";
        final String legacyPasswordEncoding = "targetmd5";

        final String token = "token";
        final String newPassword = "newPassword";

        final String userId = "foo@bar.com";
        final long timeStamp = DateTime.now().plusSeconds(500).getMillis();

        given(Boolean.valueOf(bcryptPasswordEncoder.check(userId, "bcryptEncryptedPassword", newPassword)))
                .willReturn(Boolean.TRUE);


        final SecureToken secureToken = new SecureToken(userId, timeStamp);
        given(mockSecureTokenService.decryptData(token)).willReturn(secureToken);

        final TargetCustomerModel mockCustomerModel = mock(TargetCustomerModel.class);
        given(mockCustomerModel.getToken()).willReturn(token);
        given(mockCustomerModel.getUid()).willReturn(userId);
        given(mockCustomerModel.getEncodedPassword()).willReturn("bcryptEncryptedPassword");
        given(mockCustomerModel.getPasswordEncoding()).willReturn("bcrypt");
        given(userService.getUserForUID(userId, TargetCustomerModel.class)).willReturn(mockCustomerModel);

        targetCustomerAccountServiceImpl.setTokenValiditySeconds(300);
        targetCustomerAccountServiceImpl.setPasswordEncoding(passwordEncoding);
        targetCustomerAccountServiceImpl.setLegacyPasswordEncoding(legacyPasswordEncoding);
        targetCustomerAccountServiceImpl.updatePassword(token, newPassword);
    }

    @Test(expected = ReusePasswordException.class)
    public void testThrowExceptionWhenUpdatingSamePasswordUsingMD5() throws TokenInvalidatedException {

        final String passwordEncoding = "bcrypt";
        final String legacyPasswordEncoding = "targetmd5";

        final String token = "token";
        final String newPassword = "newPassword";

        final String userId = "foo@bar.com";
        final long timeStamp = DateTime.now().plusSeconds(500).getMillis();

        given(Boolean.valueOf(md5PasswordEncoder.check(userId, "md5EncryptedPassword", newPassword)))
                .willReturn(Boolean.TRUE);


        final SecureToken secureToken = new SecureToken(userId, timeStamp);
        given(mockSecureTokenService.decryptData(token)).willReturn(secureToken);

        final TargetCustomerModel mockCustomerModel = mock(TargetCustomerModel.class);
        given(mockCustomerModel.getToken()).willReturn(token);
        given(mockCustomerModel.getUid()).willReturn(userId);
        given(mockCustomerModel.getEncodedPassword()).willReturn("md5EncryptedPassword");
        given(mockCustomerModel.getPasswordEncoding()).willReturn("targetmd5");
        given(userService.getUserForUID(userId, TargetCustomerModel.class)).willReturn(mockCustomerModel);

        targetCustomerAccountServiceImpl.setTokenValiditySeconds(300);
        targetCustomerAccountServiceImpl.setPasswordEncoding(passwordEncoding);
        targetCustomerAccountServiceImpl.setLegacyPasswordEncoding(legacyPasswordEncoding);
        targetCustomerAccountServiceImpl.updatePassword(token, newPassword);
    }




    @Test
    public void testCreatePinPadPaymentInfo() {

        final TargetAddressModel targetAddressModel = new TargetAddressModel();

        final PinPadPaymentInfoModel result = targetCustomerAccountServiceImpl
                .createPinPadPaymentInfo(customerModel, targetAddressModel);

        Assert.assertNotNull(result);

        //Verify the required interactions

        verify(pinPadPaymentInfoModel).setUser(customerModel);
        verify(pinPadPaymentInfoModel).setBillingAddress(targetAddressModel);
        verify(modelService).saveAll(targetAddressModel, pinPadPaymentInfoModel);
        verify(modelService).refresh(customerModel);

    }

    @Test
    public void testCreateTrackingGUIDWithAllowTrackingFalse() {
        //init
        final TargetCustomerModel targetCustomerModel = new TargetCustomerModel();
        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        targetCustomerModel.setUid("customerUid");
        //allowTracking is false
        targetCustomerModel.setAllowTracking(Boolean.FALSE);
        targetCustomerModel.setTrackingGuid(null);
        targetCustomerAccountServiceImpl.createTrackingGUID();
        Assert.assertNull(targetCustomerModel.getTrackingGuid());
    }

    @Test
    public void testCreateTrackingGUIDWithAllowTrackingNullGuidNull() {
        //init
        final TargetCustomerModel targetCustomerModel = new TargetCustomerModel();
        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        targetCustomerModel.setUid("customerUid");
        //allowTracking is null GUID null
        targetCustomerModel.setAllowTracking(null);
        targetCustomerModel.setTrackingGuid(null);
        targetCustomerAccountServiceImpl.createTrackingGUID();
        Assert.assertEquals(Boolean.TRUE, targetCustomerModel.getAllowTracking());
        Assert.assertNotNull(targetCustomerModel.getTrackingGuid());
        Assert.assertNotSame(targetCustomerModel.getTrackingGuid(), targetCustomerModel.getUid());
    }

    @Test
    public void testCreateTrackingGUIDWithAllowTrackingTrueGuidNull() {
        //init
        final TargetCustomerModel targetCustomerModel = new TargetCustomerModel();
        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        targetCustomerModel.setUid("customerUid");
        //allowTracking is true GUID null
        targetCustomerModel.setAllowTracking(Boolean.TRUE);
        targetCustomerModel.setTrackingGuid(null);
        targetCustomerAccountServiceImpl.createTrackingGUID();
        Assert.assertNotNull(targetCustomerModel.getTrackingGuid());
        Assert.assertNotSame(targetCustomerModel.getTrackingGuid(), targetCustomerModel.getUid());
    }

    @Test
    public void testCreateTrackingGUIDWithAllowTrackingNullGuidNotNull() {
        //init
        final TargetCustomerModel targetCustomerModel = new TargetCustomerModel();
        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        targetCustomerModel.setUid("customerUid");
        //allowTracking is true GUID null
        targetCustomerModel.setAllowTracking(null);
        targetCustomerModel.setTrackingGuid("111111111");
        targetCustomerAccountServiceImpl.createTrackingGUID();
        Assert.assertNotNull(targetCustomerModel.getTrackingGuid());
        Assert.assertNotSame(targetCustomerModel.getTrackingGuid(), targetCustomerModel.getUid());
    }

    @Test
    public void testCreateTrackingGUIDWithAllowTrackingTrueGuidNotNull() {
        //init
        final TargetCustomerModel targetCustomerModel = new TargetCustomerModel();
        given(userService.getCurrentUser()).willReturn(targetCustomerModel);
        targetCustomerModel.setUid("customerUid");
        //allowTracking is true GUID null
        targetCustomerModel.setAllowTracking(Boolean.TRUE);
        targetCustomerModel.setTrackingGuid("111111111");
        targetCustomerAccountServiceImpl.createTrackingGUID();
        Assert.assertNotNull(targetCustomerModel.getTrackingGuid());
        Assert.assertNotSame(targetCustomerModel.getTrackingGuid(), targetCustomerModel.getUid());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testUpdateTargetCustomerSubscriberKey() {
        final TargetCustomerModel targetCustomerModel = new TargetCustomerModel();
        given(userService.isUserExisting("Email")).willReturn(true);
        given(userService.getUserForUID("Email")).willReturn(targetCustomerModel);
        assertTrue(targetCustomerAccountServiceImpl.updateTargetCustomerWithSubscrptionKey("Email", "key"));
        verify(modelService).save(targetCustomerModel);
        verify(modelService).refresh(targetCustomerModel);
        assertEquals("key", targetCustomerModel.getSubscriberKey());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testUpdateTargetCustomerSubscriberKeyWithNoMatch() {
        given(userService.isUserExisting("Email")).willReturn(false);
        assertFalse(targetCustomerAccountServiceImpl.updateTargetCustomerWithSubscrptionKey("Email", "key"));
        verify(userService).isUserExisting("Email");
        verifyNoMoreInteractions(userService);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testUpdateTargetCustomerSubscriberKeyWithKeyAsNull() {
        assertFalse(targetCustomerAccountServiceImpl.updateTargetCustomerWithSubscrptionKey("Email", null));
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testUpdateTargetCustomerSubscriberKeyWithEmailAsNull() {
        assertFalse(targetCustomerAccountServiceImpl.updateTargetCustomerWithSubscrptionKey(null, "key"));
        verifyZeroInteractions(modelService);
    }

    @Test
    public void removeCustomerIPGSavedCardsWithNoSavedCards() {
        final List<CreditCardPaymentInfoModel> creditCardInfos = new ArrayList<>();
        doReturn(creditCardInfos).when(spy).getCreditCardPaymentInfos(customerModel, true);
        spy.removeCustomerIPGSavedCards(customerModel);
        verifyZeroInteractions(modelService);

    }

    @Test
    public void removeCustomerIPGSavedCardsWithNoIPGCreditCards() {
        final List<CreditCardPaymentInfoModel> creditCardInfos = new ArrayList<>();
        final CreditCardPaymentInfoModel creditCard = Mockito.mock(CreditCardPaymentInfoModel.class);
        creditCardInfos.add(creditCard);
        doReturn(creditCardInfos).when(spy).getCreditCardPaymentInfos(customerModel, true);
        spy.removeCustomerIPGSavedCards(customerModel);
        verifyZeroInteractions(modelService);

    }

    @Test
    public void removeCustomerIPGSavedCardsWithIPGCreditCards() {
        final List<PaymentInfoModel> creditCardInfos = new ArrayList<>();
        final IpgCreditCardPaymentInfoModel creditCard = Mockito.mock(IpgCreditCardPaymentInfoModel.class);
        creditCardInfos.add(creditCard);
        given(customerModel.getPaymentInfos()).willReturn(creditCardInfos);
        doReturn(creditCardInfos).when(spy).getCreditCardPaymentInfos(customerModel, true);
        spy.removeCustomerIPGSavedCards(customerModel);
        verify(modelService).save(customerModel);
    }

    @Test
    public void setDefaultPaymentInfoWillSetDefaultFlagForIpgCreditCard() {
        final IpgCreditCardPaymentInfoModel ipgPaymentInfo = mock(IpgCreditCardPaymentInfoModel.class);
        spy.setDefaultPaymentInfo(customerModel, ipgPaymentInfo);

        verify(ipgPaymentInfo).setDefaultCard(Boolean.TRUE);
        verify(modelService).save(ipgPaymentInfo);
        verify(modelService).refresh(ipgPaymentInfo);
    }

    @Test
    public void testCreateIpgPaymentInfo() {
        final IpgPaymentInfoModel ipgPaymentInfo = Mockito.mock(IpgPaymentInfoModel.class);
        given(modelService.create(IpgPaymentInfoModel.class)).willReturn(ipgPaymentInfo);

        final IpgPaymentInfoModel result = targetCustomerAccountServiceImpl
                .createIpgPaymentInfo(customerModel, addressModel, IpgPaymentTemplateType.CREDITCARDSINGLE);

        Assert.assertNotNull(result);

        //Verify the required interactions
        verify(ipgPaymentInfo).setCode(BDDMockito.anyString());
        verify(ipgPaymentInfo).setUser(customerModel);
        verify(addressModel).setOwner(ipgPaymentInfo);
        verify(ipgPaymentInfo).setBillingAddress(addressModel);
        verify(ipgPaymentInfo).setIpgPaymentTemplateType(IpgPaymentTemplateType.CREDITCARDSINGLE);
        verify(modelService).saveAll(addressModel, ipgPaymentInfo);
        verify(modelService).refresh(customerModel);

    }

    @Test
    public void testCreateIpgPaymentInfoWithNoBillingAddress() {
        final IpgPaymentInfoModel ipgPaymentInfo = Mockito.mock(IpgPaymentInfoModel.class);
        given(modelService.create(IpgPaymentInfoModel.class)).willReturn(ipgPaymentInfo);

        final IpgPaymentInfoModel result = targetCustomerAccountServiceImpl
                .createIpgPaymentInfo(customerModel, null, IpgPaymentTemplateType.CREDITCARDSINGLE);

        Assert.assertNotNull(result);

        //Verify the required interactions
        verify(ipgPaymentInfo).setCode(BDDMockito.anyString());
        verify(ipgPaymentInfo).setUser(customerModel);
        verify(ipgPaymentInfo).setIpgPaymentTemplateType(IpgPaymentTemplateType.CREDITCARDSINGLE);
        verify(modelService).saveAll(ipgPaymentInfo);
        verify(modelService).refresh(customerModel);

    }

    @Test
    public void testGetUserForResetPasswordToken() {
        final String token = "token";
        final String userId = "foo@bar.com";
        final long timeStamp = DateTime.now().plusSeconds(500).getMillis();

        final SecureToken secureToken = new SecureToken(userId, timeStamp);
        given(mockSecureTokenService.decryptData(token)).willReturn(secureToken);
        final TargetCustomerModel mockCustomerModel = mock(TargetCustomerModel.class);
        given(userService.getUserForUID(userId, TargetCustomerModel.class)).willReturn(mockCustomerModel);
        final TargetCustomerModel targetCustomerModel = targetCustomerAccountServiceImpl
                .getUserForResetPasswordToken(token);
        assertThat(targetCustomerModel).isEqualTo(mockCustomerModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetUserForResetPasswordTokenWhenUserCannotBeFound() throws TokenInvalidatedException {
        final String token = "token";

        final String userId = "foo@bar.com";
        final long timeStamp = DateTime.now().plusSeconds(500).getMillis();

        final SecureToken secureToken = new SecureToken(userId, timeStamp);
        given(mockSecureTokenService.decryptData(token)).willReturn(secureToken);

        targetCustomerAccountServiceImpl.getUserForResetPasswordToken(token);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testChangePasswordWithNullUser() throws PasswordMismatchException {
        targetCustomerAccountServiceImpl.changePassword(null, "old$password", "new#password");
    }

    @Test
    public void testChangePasswordWithAnonymousUser() throws PasswordMismatchException {
        final CustomerModel mockCustomer = mock(CustomerModel.class);

        doReturn(Boolean.TRUE).when(userService).isAnonymousUser(mockCustomer);

        targetCustomerAccountServiceImpl.changePassword(mockCustomer, "old$password", "new#password");

        verifyZeroInteractions(mockPasswordEncoderService, modelService, mockEventService);
        verify(userService).isAnonymousUser(mockCustomer);
        verifyNoMoreInteractions(userService);
    }

    @Test(expected = PasswordMismatchException.class)
    public void testChangePasswordWithIncorrectOldPassword() throws PasswordMismatchException {
        final CustomerModel mockCustomer = mock(CustomerModel.class);

        doReturn(Boolean.FALSE).when(userService).isAnonymousUser(mockCustomer);
        doReturn(Boolean.FALSE).when(mockPasswordEncoderService).isValid(mockCustomer, "old$password");

        targetCustomerAccountServiceImpl.changePassword(mockCustomer, "old$password", "new#password");
    }

    @Test
    public void testChangePasswordWithBcryptFeatureDisabled() throws PasswordMismatchException {
        final String newPassword = "new#password";

        final CustomerModel mockCustomer = mock(CustomerModel.class);

        doReturn(Boolean.FALSE).when(userService).isAnonymousUser(mockCustomer);
        doReturn(Boolean.TRUE).when(mockPasswordEncoderService).isValid(mockCustomer, "old$password");

        doReturn(Boolean.FALSE).when(mockTargetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.BCRYPT_PASSWORD_ENCODING);

        final BaseStoreModel mockBaseStore = mock(BaseStoreModel.class);
        given(baseStoreService.getCurrentBaseStore()).willReturn(mockBaseStore);

        final BaseSiteModel mockBaseSite = mock(BaseSiteModel.class);
        given(baseSiteService.getCurrentBaseSite()).willReturn(mockBaseSite);

        final LanguageModel mockLanguage = mock(LanguageModel.class);
        given(commonI18NService.getCurrentLanguage()).willReturn(mockLanguage);

        final CurrencyModel mockCurrency = mock(CurrencyModel.class);
        given(commonI18NService.getCurrentCurrency()).willReturn(mockCurrency);

        targetCustomerAccountServiceImpl.changePassword(mockCustomer, "old$password", newPassword);

        verify(userService).setPassword(mockCustomer, newPassword, "targetmd5");
        verify(modelService).save(mockCustomer);

        final ArgumentCaptor<AbstractCommerceUserEvent> abstractEventCaptor = ArgumentCaptor
                .forClass(AbstractCommerceUserEvent.class);
        verify(mockEventService).publishEvent(abstractEventCaptor.capture());

        final AbstractCommerceUserEvent capturedEvent = abstractEventCaptor.getValue();
        assertThat(capturedEvent).isInstanceOf(PasswordChangeEvent.class);

        assertThat(capturedEvent.getBaseStore()).isEqualTo(mockBaseStore);
        assertThat(capturedEvent.getSite()).isEqualTo(mockBaseSite);
        assertThat(capturedEvent.getCustomer()).isEqualTo(mockCustomer);
        assertThat(capturedEvent.getLanguage()).isEqualTo(mockLanguage);
        assertThat(capturedEvent.getCurrency()).isEqualTo(mockCurrency);
    }

    @Test
    public void testChangePasswordWithBcryptFeatureEnabled() throws PasswordMismatchException {
        final String newPassword = "new#password";

        final CustomerModel mockCustomer = mock(CustomerModel.class);

        doReturn(Boolean.FALSE).when(userService).isAnonymousUser(mockCustomer);
        doReturn(Boolean.TRUE).when(mockPasswordEncoderService).isValid(mockCustomer, "old$password");

        doReturn(Boolean.TRUE).when(mockTargetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.BCRYPT_PASSWORD_ENCODING);

        final BaseStoreModel mockBaseStore = mock(BaseStoreModel.class);
        given(baseStoreService.getCurrentBaseStore()).willReturn(mockBaseStore);

        final BaseSiteModel mockBaseSite = mock(BaseSiteModel.class);
        given(baseSiteService.getCurrentBaseSite()).willReturn(mockBaseSite);

        final LanguageModel mockLanguage = mock(LanguageModel.class);
        given(commonI18NService.getCurrentLanguage()).willReturn(mockLanguage);

        final CurrencyModel mockCurrency = mock(CurrencyModel.class);
        given(commonI18NService.getCurrentCurrency()).willReturn(mockCurrency);

        targetCustomerAccountServiceImpl.changePassword(mockCustomer, "old$password", newPassword);

        verify(userService).setPassword(mockCustomer, newPassword, "bcrypt");
        verify(modelService).save(mockCustomer);

        final ArgumentCaptor<AbstractCommerceUserEvent> abstractEventCaptor = ArgumentCaptor
                .forClass(AbstractCommerceUserEvent.class);
        verify(mockEventService).publishEvent(abstractEventCaptor.capture());

        final AbstractCommerceUserEvent capturedEvent = abstractEventCaptor.getValue();
        assertThat(capturedEvent).isInstanceOf(PasswordChangeEvent.class);

        assertThat(capturedEvent.getBaseStore()).isEqualTo(mockBaseStore);
        assertThat(capturedEvent.getSite()).isEqualTo(mockBaseSite);
        assertThat(capturedEvent.getCustomer()).isEqualTo(mockCustomer);
        assertThat(capturedEvent.getLanguage()).isEqualTo(mockLanguage);
        assertThat(capturedEvent.getCurrency()).isEqualTo(mockCurrency);
    }

    @Test
    public void testChangePasswordWithBcryptFeatureDisabledNotCustomer() throws PasswordMismatchException {
        final String newPassword = "new#password";

        final UserModel mockUser = mock(UserModel.class);

        doReturn(Boolean.FALSE).when(userService).isAnonymousUser(mockUser);
        doReturn(Boolean.TRUE).when(mockPasswordEncoderService).isValid(mockUser, "old$password");

        doReturn(Boolean.FALSE).when(mockTargetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.BCRYPT_PASSWORD_ENCODING);

        final BaseStoreModel mockBaseStore = mock(BaseStoreModel.class);
        given(baseStoreService.getCurrentBaseStore()).willReturn(mockBaseStore);

        final BaseSiteModel mockBaseSite = mock(BaseSiteModel.class);
        given(baseSiteService.getCurrentBaseSite()).willReturn(mockBaseSite);

        final LanguageModel mockLanguage = mock(LanguageModel.class);
        given(commonI18NService.getCurrentLanguage()).willReturn(mockLanguage);

        final CurrencyModel mockCurrency = mock(CurrencyModel.class);
        given(commonI18NService.getCurrentCurrency()).willReturn(mockCurrency);

        targetCustomerAccountServiceImpl.changePassword(mockUser, "old$password", newPassword);

        verify(userService).setPassword(mockUser, newPassword, "targetmd5");
        verify(modelService).save(mockUser);

        verifyZeroInteractions(mockEventService);
    }

    @Test
    public void testChangePasswordWithBcryptFeatureEnabledNotCustomer() throws PasswordMismatchException {
        final String newPassword = "new#password";

        final UserModel mockUser = mock(UserModel.class);

        doReturn(Boolean.FALSE).when(userService).isAnonymousUser(mockUser);
        doReturn(Boolean.TRUE).when(mockPasswordEncoderService).isValid(mockUser, "old$password");

        doReturn(Boolean.TRUE).when(mockTargetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.BCRYPT_PASSWORD_ENCODING);

        final BaseStoreModel mockBaseStore = mock(BaseStoreModel.class);
        given(baseStoreService.getCurrentBaseStore()).willReturn(mockBaseStore);

        final BaseSiteModel mockBaseSite = mock(BaseSiteModel.class);
        given(baseSiteService.getCurrentBaseSite()).willReturn(mockBaseSite);

        final LanguageModel mockLanguage = mock(LanguageModel.class);
        given(commonI18NService.getCurrentLanguage()).willReturn(mockLanguage);

        final CurrencyModel mockCurrency = mock(CurrencyModel.class);
        given(commonI18NService.getCurrentCurrency()).willReturn(mockCurrency);

        targetCustomerAccountServiceImpl.changePassword(mockUser, "old$password", newPassword);

        verify(userService).setPassword(mockUser, newPassword, "bcrypt");
        verify(modelService).save(mockUser);

        verifyZeroInteractions(mockEventService);
    }

    @Test
    public void testGetCurrentUserModelBySessionWithNullJalo() {
        final HttpSession session = mock(HttpSession.class);
        given(session.getAttribute(TgtCoreConstants.JALO_SESSION)).willReturn(null);
        final UserModel result = targetCustomerAccountServiceImpl.getCurrentUserModelBySession(session);
        assertThat(result).isNull();
    }

    @Test
    public void testGetCurrentUserModelBySessionWithHybrisSessionNull() {
        final HttpSession session = mock(HttpSession.class);
        final JaloSession jaloSession = mock(JaloSession.class);
        given(session.getAttribute(TgtCoreConstants.JALO_SESSION)).willReturn(jaloSession);
        given(jaloSession.getAttribute(TgtCoreConstants.SLSESSION)).willReturn(null);
        final UserModel result = targetCustomerAccountServiceImpl.getCurrentUserModelBySession(session);
        assertThat(result).isNull();
    }

    @Test
    public void testGetCurrentUserModelBySessionWithUserNull() {
        final HttpSession session = mock(HttpSession.class);
        final JaloSession jaloSession = mock(JaloSession.class);
        final Session hybrisSession = mock(Session.class);
        final UserModel userModel = null;
        given(session.getAttribute(TgtCoreConstants.JALO_SESSION)).willReturn(jaloSession);
        given(jaloSession.getAttribute(TgtCoreConstants.SLSESSION)).willReturn(hybrisSession);
        given(hybrisSession.getAttribute(TgtCoreConstants.USER)).willReturn(userModel);
        final UserModel result = targetCustomerAccountServiceImpl.getCurrentUserModelBySession(session);
        assertThat(result).isNull();
    }

    @Test
    public void testGetCurrentUserModelBySessionWithUserReturned() {
        final HttpSession session = mock(HttpSession.class);
        final JaloSession jaloSession = mock(JaloSession.class);
        final Session hybrisSession = mock(Session.class);
        final UserModel userModel = mock(UserModel.class);
        given(session.getAttribute(TgtCoreConstants.JALO_SESSION)).willReturn(jaloSession);
        given(jaloSession.getAttribute(TgtCoreConstants.SLSESSION)).willReturn(hybrisSession);
        given(hybrisSession.getAttribute(TgtCoreConstants.USER)).willReturn(userModel);
        final UserModel result = targetCustomerAccountServiceImpl.getCurrentUserModelBySession(session);
        assertThat(result).isEqualTo(userModel);
    }

    @Test
    public void testUpdateUidForGuestUser() throws DuplicateUidException {
        final TargetCustomerModel guestUserModel = mock(TargetCustomerModel.class);
        given(guestUserModel.getType()).willReturn(CustomerType.GUEST);
        targetCustomerAccountServiceImpl.updateUidForGuestUser("test@test.com", guestUserModel);
        verify(modelService).save(guestUserModel);
    }

    @Test
    public void testUpdateUidForGuestUserWithNullUserModel() throws DuplicateUidException {
        targetCustomerAccountServiceImpl.updateUidForGuestUser("test@test.com", null);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testUpdateUidForGuestUserWithNotGuestUserModel() throws DuplicateUidException {
        final TargetCustomerModel guestUserModel = mock(TargetCustomerModel.class);
        given(guestUserModel.getType()).willReturn(null);
        targetCustomerAccountServiceImpl.updateUidForGuestUser("test@test.com", guestUserModel);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testCreateAfterpayPaymentInfo() {
        final AfterpayPaymentInfoModel result = targetCustomerAccountServiceImpl.createAfterpayPaymentInfo(
                customerModel,
                "token");

        assertThat(result).isNotNull();
        assertThat(afterpayPaymentInfoModel).isEqualTo(result);

        verify(afterpayPaymentInfoModel).setUser(customerModel);
        verify(afterpayPaymentInfoModel).setAfterpayToken("token");

        verify(modelService).save(afterpayPaymentInfoModel);
        verify(modelService).refresh(customerModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateAfterpayPaymentInfoWithNullCart() {
        targetCustomerAccountServiceImpl.createAfterpayPaymentInfo(null, null);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testCreateAfterpayPaymentInfoWithNullToken() {
        targetCustomerAccountServiceImpl.createAfterpayPaymentInfo(customerModel, null);
    }
}

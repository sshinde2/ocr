/**
 * 
 */
package au.com.target.tgtcore.customer.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.model.TargetCustomerModel;
import junit.framework.Assert;


/**
 * @author asingh78
 * 
 */
@UnitTest
public class TargetCustomerEmailResolutionServiceTest {
    private static final String EMAIL = "test@test.com";
    private static final String GUSTUSERUID = "testasasas|test@test.com";
    private TargetCustomerEmailResolutionService targetCustomerEmailResolutionService;



    @Before
    public void startUp() {
        targetCustomerEmailResolutionService = new TargetCustomerEmailResolutionService();
    }

    @Test
    public void validateAndProcessEmailForCustomerNotGuestUserTest() {

        final TargetCustomerModel customerModel = Mockito.mock(TargetCustomerModel.class);
        BDDMockito.given(customerModel.getType()).willReturn(null);
        BDDMockito.given(customerModel.getUid()).willReturn(EMAIL);
        ServicesUtil.validateParameterNotNullStandardMessage("customerModel", customerModel);
        final String email = targetCustomerEmailResolutionService.validateAndProcessEmailForCustomer(customerModel);
        Assert.assertEquals(EMAIL, email);

    }

    @Test
    public void validateAndProcessEmailForCustomerGuestUserTest() {
        final TargetCustomerModel customerModel = Mockito.mock(TargetCustomerModel.class);
        BDDMockito.given(customerModel.getType()).willReturn(CustomerType.GUEST);
        BDDMockito.given(customerModel.getUid()).willReturn(GUSTUSERUID);
        ServicesUtil.validateParameterNotNullStandardMessage("customerModel", customerModel);
        final String email = targetCustomerEmailResolutionService.validateAndProcessEmailForCustomer(customerModel);
        Assert.assertEquals(EMAIL, email);

    }


}

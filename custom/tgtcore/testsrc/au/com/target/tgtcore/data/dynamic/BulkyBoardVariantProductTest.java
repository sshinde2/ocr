package au.com.target.tgtcore.data.dynamic;

import de.hybris.bootstrap.annotations.UnitTest;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


@UnitTest
public class BulkyBoardVariantProductTest {

    private final BulkyBoardVariantProduct dynamicAttributeBulkyBoardVariantProduct = new BulkyBoardVariantProduct();

    @Test
    public void testGetWithColourVariantBulkyBoardBaseProduct() {
        final TargetProductModel mockProduct = Mockito.mock(TargetProductModel.class);
        BDDMockito.given(mockProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);

        final TargetColourVariantProductModel mockColourVariant = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(mockColourVariant.getBaseProduct()).willReturn(mockProduct);

        final Boolean bulkyBoardProduct = dynamicAttributeBulkyBoardVariantProduct.get(mockColourVariant);

        Assertions.assertThat(bulkyBoardProduct).isTrue();
    }

    @Test
    public void testGetWithColourVariantNonBulkyBoardBaseProduct() {
        final TargetProductModel mockProduct = Mockito.mock(TargetProductModel.class);
        BDDMockito.given(mockProduct.getBulkyBoardProduct()).willReturn(Boolean.FALSE);

        final TargetColourVariantProductModel mockColourVariant = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(mockColourVariant.getBaseProduct()).willReturn(mockProduct);

        final Boolean bulkyBoardProduct = dynamicAttributeBulkyBoardVariantProduct.get(mockColourVariant);

        Assertions.assertThat(bulkyBoardProduct).isFalse();
    }

    @Test
    public void testGetWithSizeVariantBulkyBoardBaseProduct() {
        final TargetProductModel mockProduct = Mockito.mock(TargetProductModel.class);
        BDDMockito.given(mockProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);

        final TargetColourVariantProductModel mockColourVariant = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(mockColourVariant.getBulkyBoardProduct()).willAnswer(new Answer<Boolean>() {

            /* (non-Javadoc)
             * @see org.mockito.stubbing.Answer#answer(org.mockito.invocation.InvocationOnMock)
             */
            @Override
            public Boolean answer(final InvocationOnMock invocation) {
                return dynamicAttributeBulkyBoardVariantProduct.get(mockColourVariant);
            }

        });
        BDDMockito.given(mockColourVariant.getBaseProduct()).willReturn(mockProduct);

        final TargetSizeVariantProductModel mockSizeVariant = Mockito.mock(TargetSizeVariantProductModel.class);
        BDDMockito.given(mockSizeVariant.getBaseProduct()).willReturn(mockColourVariant);

        final Boolean bulkyBoardProduct = dynamicAttributeBulkyBoardVariantProduct.get(mockSizeVariant);

        Assertions.assertThat(bulkyBoardProduct).isTrue();
    }

    @Test
    public void testGetWithSizeVariantNonBulkyBoardBaseProduct() {
        final TargetProductModel mockProduct = Mockito.mock(TargetProductModel.class);
        BDDMockito.given(mockProduct.getBulkyBoardProduct()).willReturn(Boolean.FALSE);

        final TargetColourVariantProductModel mockColourVariant = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(mockColourVariant.getBulkyBoardProduct()).willAnswer(new Answer<Boolean>() {

            /* (non-Javadoc)
             * @see org.mockito.stubbing.Answer#answer(org.mockito.invocation.InvocationOnMock)
             */
            @Override
            public Boolean answer(final InvocationOnMock invocation) {
                return dynamicAttributeBulkyBoardVariantProduct.get(mockColourVariant);
            }

        });
        BDDMockito.given(mockColourVariant.getBaseProduct()).willReturn(mockProduct);

        final TargetSizeVariantProductModel mockSizeVariant = Mockito.mock(TargetSizeVariantProductModel.class);
        BDDMockito.given(mockSizeVariant.getBaseProduct()).willReturn(mockColourVariant);

        final Boolean bulkyBoardProduct = dynamicAttributeBulkyBoardVariantProduct.get(mockSizeVariant);

        Assertions.assertThat(bulkyBoardProduct).isFalse();
    }

    @Test
    public void testGetWithColourVariantNoBaseProduct() {
        final TargetColourVariantProductModel mockColourVariant = Mockito.mock(TargetColourVariantProductModel.class);

        final Boolean bulkyBoardProduct = dynamicAttributeBulkyBoardVariantProduct.get(mockColourVariant);

        Assertions.assertThat(bulkyBoardProduct).isNull();
    }
}

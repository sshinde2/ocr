/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.model.ColourModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;


/**
 * 
 */
@UnitTest
public class ColourNameTest {
    private ColourName dynamicAttribute;

    @Before
    public void prepare() {
        dynamicAttribute = new ColourName();
    }

    /*
     *  test getting name of colour when there is a null colour object
     */
    @Test
    public void testGetColourNameNullColourObject() throws Exception {
        final TargetColourVariantProductModel model = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(null);

        final String name = dynamicAttribute.get(model);

        Assert.assertNull(name);
    }

    /*
     *  test getting name of colour when a colour and name exist
     */
    @Test
    public void testGetColourNameHappyCase() throws Exception {
        final ColourModel colour = Mockito.mock(ColourModel.class);
        BDDMockito.given(colour.getName()).willReturn("Blue");
        BDDMockito.given(colour.getDisplay()).willReturn(Boolean.TRUE);

        final TargetColourVariantProductModel model = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(model.getColour()).willReturn(colour);

        final String name = dynamicAttribute.get(model);

        Assert.assertEquals("Blue", name);
    }

    @Test
    public void testGetColourNotVisible() {
        final ColourModel colour = Mockito.mock(ColourModel.class);
        BDDMockito.given(colour.getName()).willReturn("Blue");
        BDDMockito.given(colour.getDisplay()).willReturn(Boolean.FALSE);

        final TargetColourVariantProductModel model = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(model.getColour()).willReturn(colour);

        final String name = dynamicAttribute.get(model);

        Assert.assertNull(name);
    }
}

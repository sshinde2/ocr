/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.willReturn;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.data.dynamic.ConsignmentStatusDate;
import au.com.target.tgtcore.model.TargetConsignmentModel;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ConsignmentStatusDateTest {

    private final ConsignmentStatusDate consignmentStatusDate = new ConsignmentStatusDate();

    @Mock
    private TargetConsignmentModel targetConsignment;

    @Mock
    private Date statusDate;

    @Test
    public void testSentToWarehouse() {
        willReturn(ConsignmentStatus.SENT_TO_WAREHOUSE).given(targetConsignment).getStatus();
        willReturn(statusDate).given(targetConsignment).getSentToWarehouseDate();

        assertThat(consignmentStatusDate.get(targetConsignment)).isEqualTo(statusDate);
    }

    @Test
    public void testConfirmedByWarehouse() {
        willReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE).given(targetConsignment).getStatus();
        willReturn(statusDate).given(targetConsignment).getSentToWarehouseDate();

        assertThat(consignmentStatusDate.get(targetConsignment)).isEqualTo(statusDate);
    }

    @Test
    public void testWaved() {
        willReturn(ConsignmentStatus.WAVED).given(targetConsignment).getStatus();
        willReturn(statusDate).given(targetConsignment).getWavedDate();

        assertThat(consignmentStatusDate.get(targetConsignment)).isEqualTo(statusDate);
    }

    @Test
    public void testPicked() {
        willReturn(ConsignmentStatus.PICKED).given(targetConsignment).getStatus();
        willReturn(statusDate).given(targetConsignment).getPickConfirmDate();

        assertThat(consignmentStatusDate.get(targetConsignment)).isEqualTo(statusDate);
    }

    @Test
    public void testPacked() {
        willReturn(ConsignmentStatus.PACKED).given(targetConsignment).getStatus();
        willReturn(statusDate).given(targetConsignment).getPackedDate();

        assertThat(consignmentStatusDate.get(targetConsignment)).isEqualTo(statusDate);
    }

    @Test
    public void testShipped() {
        willReturn(ConsignmentStatus.SHIPPED).given(targetConsignment).getStatus();
        willReturn(statusDate).given(targetConsignment).getShippingDate();

        assertThat(consignmentStatusDate.get(targetConsignment)).isEqualTo(statusDate);
    }

    @Test
    public void testCancelled() {
        willReturn(ConsignmentStatus.CANCELLED).given(targetConsignment).getStatus();
        willReturn(statusDate).given(targetConsignment).getCancelDate();

        assertThat(consignmentStatusDate.get(targetConsignment)).isEqualTo(statusDate);
    }

    @Test
    public void testDefault() {
        willReturn(ConsignmentStatus.CREATED).given(targetConsignment).getStatus();
        willReturn(statusDate).given(targetConsignment).getSentToWarehouseDate();
        willReturn(statusDate).given(targetConsignment).getWavedDate();
        willReturn(statusDate).given(targetConsignment).getPickConfirmDate();
        willReturn(statusDate).given(targetConsignment).getPackedDate();
        willReturn(statusDate).given(targetConsignment).getShippingDate();
        willReturn(statusDate).given(targetConsignment).getCancelDate();

        assertThat(consignmentStatusDate.get(targetConsignment)).isNull();
    }
}

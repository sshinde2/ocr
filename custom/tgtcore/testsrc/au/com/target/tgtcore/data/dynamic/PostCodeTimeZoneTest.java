package au.com.target.tgtcore.data.dynamic;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.TimeZone;

import org.junit.Test;

import au.com.target.tgtcore.model.PostCodeModel;


@UnitTest
public class PostCodeTimeZoneTest {

    private final PostCodeTimeZone postCodeTimeZone = new PostCodeTimeZone();

    @Test
    public void testGetWithNoTimeZone() throws Exception {
        final PostCodeModel postCode = mock(PostCodeModel.class);
        given(postCode.getTimeZone()).willReturn(null);

        final String result = postCodeTimeZone.get(postCode);
        assertThat(result).isNull();
    }

    @Test
    public void testGet() throws Exception {
        final String timeZoneId = "Australia/Victoria";
        final TimeZone timeZone = TimeZone.getTimeZone(timeZoneId);

        final PostCodeModel postCode = mock(PostCodeModel.class);
        given(postCode.getTimeZone()).willReturn(timeZone);

        final String result = postCodeTimeZone.get(postCode);
        assertThat(result).isEqualTo(timeZoneId);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSet() throws Exception {
        final PostCodeModel postCode = mock(PostCodeModel.class);
        final String timeZoneId = "Australia/Victoria";

        postCodeTimeZone.set(postCode, timeZoneId);
    }

}

/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * @author rsamuel3
 *
 */
@UnitTest
public class ProductDisplayOnlyTest {
    private final ProductDisplayOnly dynamicDisplayOnly = new ProductDisplayOnly();

    @Test
    public void testProductModelWIthNoVariants() {
        final TargetProductModel productModel = new TargetProductModel();
        final Boolean displayOnly = dynamicDisplayOnly.get(productModel);
        assertThat(displayOnly).isFalse();
    }

    @Test
    public void testProductModelWIthOnlyVariantsWithNullDisplayOnly() {
        final TargetProductModel productModel = new TargetProductModel();
        final List<VariantProductModel> variants = new ArrayList<VariantProductModel>();
        variants.add(new TargetColourVariantProductModel());
        productModel.setVariants(variants);
        final Boolean displayOnly = dynamicDisplayOnly.get(productModel);
        assertThat(displayOnly).isFalse();
    }

    @Test
    public void testProductModelWIthOnlyVariantsAllOnline() {
        final TargetProductModel productModel = new TargetProductModel();
        productModel.setVariants(buildColourVariants(3, 0));
        final Boolean displayOnly = dynamicDisplayOnly.get(productModel);
        assertThat(displayOnly).isFalse();
    }

    @Test
    public void testProductModelWIthOnlyVariantsOneDisplay() {
        final TargetProductModel productModel = new TargetProductModel();
        productModel.setVariants(buildColourVariants(3, 1));
        final Boolean displayOnly = dynamicDisplayOnly.get(productModel);
        assertThat(displayOnly).isFalse();
    }

    @Test
    public void testProductModelWIthOnlyVariantsAllThreeDisplay() {
        final TargetProductModel productModel = new TargetProductModel();
        productModel.setVariants(buildColourVariants(3, 3));
        final Boolean displayOnly = dynamicDisplayOnly.get(productModel);
        assertThat(displayOnly).isTrue();
    }

    @Test
    public void testProductModelWIthSizeVariantsAllOnline() {
        final TargetProductModel productModel = new TargetProductModel();
        productModel.setVariants(buildSizeVariants(3, 0));
        final Boolean displayOnly = dynamicDisplayOnly.get(productModel);
        assertThat(displayOnly).isFalse();
    }


    @Test
    public void testProductModelWIthSizeVariantsOneDisplay() {
        final TargetProductModel productModel = new TargetProductModel();
        productModel.setVariants(buildSizeVariants(3, 1));
        final Boolean displayOnly = dynamicDisplayOnly.get(productModel);
        assertThat(displayOnly).isFalse();
    }

    @Test
    public void testProductModelWIthSizeVariantsAllThreeDisplay() {
        final TargetProductModel productModel = new TargetProductModel();
        productModel.setVariants(buildSizeVariants(3, 3));
        final Boolean displayOnly = dynamicDisplayOnly.get(productModel);
        assertThat(displayOnly).isTrue();
    }

    /**
     * @param numberOfVariants
     * @param numberDisplayOnly
     * @return collection of variants
     */
    private Collection<VariantProductModel> buildColourVariants(int numberOfVariants,
            int numberDisplayOnly) {
        final Collection<VariantProductModel> colourVariants = new ArrayList<VariantProductModel>();
        while (numberOfVariants > 0) {
            final TargetColourVariantProductModel colourVariant = new TargetColourVariantProductModel();
            if (numberDisplayOnly > 0) {
                colourVariant.setDisplayOnly(Boolean.TRUE);
                numberDisplayOnly--;
            }
            else {
                colourVariant.setDisplayOnly(Boolean.FALSE);
            }
            numberOfVariants--;
            colourVariants.add(colourVariant);
        }
        return colourVariants;
    }

    /**
     * @param numberOfVariants
     * @param numberDisplayOnly
     * @return Collection of colourVariants with sizes in the,
     */
    private Collection<VariantProductModel> buildSizeVariants(int numberOfVariants, int numberDisplayOnly) {
        final Collection<VariantProductModel> colourVariants = new ArrayList<VariantProductModel>();
        final TargetColourVariantProductModel colourVariant = new TargetColourVariantProductModel();
        final Collection<VariantProductModel> sizeVariants = new ArrayList<VariantProductModel>();
        while (numberOfVariants > 0) {
            final TargetSizeVariantProductModel sizeVariant = new TargetSizeVariantProductModel();
            if (numberDisplayOnly > 0) {
                sizeVariant.setDisplayOnly(Boolean.TRUE);
                numberDisplayOnly--;
            }
            else {
                sizeVariant.setDisplayOnly(Boolean.FALSE);
            }
            numberOfVariants--;
            sizeVariants.add(sizeVariant);
        }

        colourVariant.setVariants(sizeVariants);
        colourVariants.add(colourVariant);
        return colourVariants;
    }
}

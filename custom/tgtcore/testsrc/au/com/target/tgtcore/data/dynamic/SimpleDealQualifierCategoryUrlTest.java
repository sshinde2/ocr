/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import au.com.target.tgtcore.model.TargetDealCategoryModel;


/**
 * @author bhuang3
 * 
 */
@UnitTest
public class SimpleDealQualifierCategoryUrlTest {

    private final SimpleDealQualifierCategoryUrl abstractDealQualifiedCategoryUrlTest = new SimpleDealQualifierCategoryUrl();

    @Test
    public void testGetWithNullModel() {
        final AbstractSimpleDealModel abstractSimpleDealModel = mock(AbstractSimpleDealModel.class);
        given(abstractSimpleDealModel.getQualifierList()).willReturn(null);
        final String result = abstractDealQualifiedCategoryUrlTest.get(abstractSimpleDealModel);
        Assert.assertNull(result);
    }

    @Test
    public void testGetWithQualifierListEmpty() {
        final AbstractSimpleDealModel abstractSimpleDealModel = mock(AbstractSimpleDealModel.class);
        final List<DealQualifierModel> list = new ArrayList<>();
        given(abstractSimpleDealModel.getQualifierList()).willReturn(list);
        given(abstractSimpleDealModel.getFacetTitle()).willReturn("testTitle");
        given(abstractSimpleDealModel.getCode()).willReturn("testCode");
        final String result = abstractDealQualifiedCategoryUrlTest.get(abstractSimpleDealModel);
        Assert.assertEquals(null, result);
    }

    @Test
    public void testGetWithDealCategoryEmpty() {
        final AbstractSimpleDealModel abstractSimpleDealModel = mock(AbstractSimpleDealModel.class);
        final List<DealQualifierModel> list = new ArrayList<>();
        final DealQualifierModel dealQualifierModel = new DealQualifierModel();
        final List<TargetDealCategoryModel> targetDealCategoryModelList = new ArrayList<>();
        dealQualifierModel.setDealCategory(targetDealCategoryModelList);
        list.add(dealQualifierModel);
        given(abstractSimpleDealModel.getQualifierList()).willReturn(list);
        given(abstractSimpleDealModel.getFacetTitle()).willReturn("testTitle");
        given(abstractSimpleDealModel.getCode()).willReturn("testCode");
        final String result = abstractDealQualifiedCategoryUrlTest.get(abstractSimpleDealModel);
        Assert.assertEquals(null, result);
    }

    @Test
    public void testGet() {
        final AbstractSimpleDealModel abstractSimpleDealModel = mock(AbstractSimpleDealModel.class);
        final List<DealQualifierModel> list = new ArrayList<>();
        final DealQualifierModel dealQualifierModel = new DealQualifierModel();
        final List<TargetDealCategoryModel> targetDealCategoryModelList = new ArrayList<>();
        final TargetDealCategoryModel targetDealCategoryModel = new TargetDealCategoryModel();
        targetDealCategoryModel.setCode("testCode");
        targetDealCategoryModelList.add(targetDealCategoryModel);
        dealQualifierModel.setDealCategory(targetDealCategoryModelList);
        list.add(dealQualifierModel);
        given(abstractSimpleDealModel.getQualifierList()).willReturn(list);
        given(abstractSimpleDealModel.getCode()).willReturn("testCode");
        final String result = abstractDealQualifiedCategoryUrlTest.get(abstractSimpleDealModel);
        Assert.assertEquals("/d/testCode", result);
    }
}

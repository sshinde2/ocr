/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.model.ColourModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;


/**
 * 
 */
@UnitTest
public class SwatchNameTest {
    private SwatchName dynamicAttribute;

    @Before
    public void prepare() {
        dynamicAttribute = new SwatchName();
    }

    /*
     *  test getting name of the swatch when there is a null colour object
     */
    @Test
    public void testGetColourNameNullColourObject() throws Exception {
        final TargetColourVariantProductModel model = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(null);

        final String name = dynamicAttribute.get(model);

        org.junit.Assert.assertNull(name);
    }

    /*
     *  test getting name of the swatch when a colour and name exist
     */
    @Test
    public void testGetColourNameHappyCase() throws Exception {
        final ColourModel colour = Mockito.mock(ColourModel.class);
        BDDMockito.given(colour.getName()).willReturn("Deep Midnight Blue");

        final TargetColourVariantProductModel model = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(model.getSwatch()).willReturn(colour);

        final String name = dynamicAttribute.get(model);

        org.junit.Assert.assertEquals("Deep Midnight Blue", name);
    }
}

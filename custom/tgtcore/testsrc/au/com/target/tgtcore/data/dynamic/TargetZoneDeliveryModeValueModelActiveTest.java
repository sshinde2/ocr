package au.com.target.tgtcore.data.dynamic;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.model.TargetZoneDeliveryModeValueModel;


/**
 * 
 * Unit Test for {@link TargetZoneDeliveryModeValueModelActive}
 */
@UnitTest
public class TargetZoneDeliveryModeValueModelActiveTest {

    private final TargetZoneDeliveryModeValueModelActive attrHandler = new TargetZoneDeliveryModeValueModelActive();
    private Date now;
    private Date soon;
    private Date today;
    private Date yesterday;
    private Date tomorrow;

    @Before
    public void setup() {
        now = new Date();
        soon = DateUtils.addSeconds(now, 5); // more than long enough for this test to run
        today = DateUtils.truncate(now, Calendar.DAY_OF_MONTH);
        yesterday = DateUtils.addDays(today, -1);
        tomorrow = DateUtils.addDays(today, 1);
    }

    @Test
    public void testActiveForNullFromTo() {
        final TargetZoneDeliveryModeValueModel model = Mockito.mock(TargetZoneDeliveryModeValueModel.class);
        Assert.assertEquals(Boolean.TRUE, attrHandler.get(model));
    }

    @Test
    public void testActiveForNullFromPastTo() {
        final TargetZoneDeliveryModeValueModel model = Mockito.mock(TargetZoneDeliveryModeValueModel.class);
        BDDMockito.given(model.getValidTo()).willReturn(yesterday);

        Assert.assertEquals(Boolean.FALSE, attrHandler.get(model));
    }

    @Test
    public void testActiveForNullFromTodayTo() {
        final TargetZoneDeliveryModeValueModel model = Mockito.mock(TargetZoneDeliveryModeValueModel.class);
        BDDMockito.given(model.getValidTo()).willReturn(soon);

        Assert.assertEquals(Boolean.TRUE, attrHandler.get(model));
    }

    @Test
    public void testActiveForNullFromFutureTo() {
        final TargetZoneDeliveryModeValueModel model = Mockito.mock(TargetZoneDeliveryModeValueModel.class);
        BDDMockito.given(model.getValidTo()).willReturn(tomorrow);

        Assert.assertEquals(Boolean.TRUE, attrHandler.get(model));
    }

    @Test
    public void testActiveForNullToPastFrom() {
        final TargetZoneDeliveryModeValueModel model = Mockito.mock(TargetZoneDeliveryModeValueModel.class);
        BDDMockito.given(model.getValidFrom()).willReturn(yesterday);

        Assert.assertEquals(Boolean.TRUE, attrHandler.get(model));
    }

    @Test
    public void testActiveForNullToTodayFrom() {
        final TargetZoneDeliveryModeValueModel model = Mockito.mock(TargetZoneDeliveryModeValueModel.class);
        BDDMockito.given(model.getValidFrom()).willReturn(today);

        Assert.assertEquals(Boolean.TRUE, attrHandler.get(model));
    }

    @Test
    public void testActiveForNullToFutureFrom() {
        final TargetZoneDeliveryModeValueModel model = Mockito.mock(TargetZoneDeliveryModeValueModel.class);
        BDDMockito.given(model.getValidFrom()).willReturn(tomorrow);

        Assert.assertEquals(Boolean.FALSE, attrHandler.get(model));
    }

    @Test
    public void testActiveForPastToFrom() {
        final TargetZoneDeliveryModeValueModel model = Mockito.mock(TargetZoneDeliveryModeValueModel.class);
        BDDMockito.given(model.getValidTo()).willReturn(yesterday);
        BDDMockito.given(model.getValidFrom()).willReturn(yesterday);

        Assert.assertEquals(Boolean.FALSE, attrHandler.get(model));
    }

    @Test
    public void testActiveForTodayToFrom() {
        final TargetZoneDeliveryModeValueModel model = Mockito.mock(TargetZoneDeliveryModeValueModel.class);
        BDDMockito.given(model.getValidTo()).willReturn(soon);
        BDDMockito.given(model.getValidFrom()).willReturn(now);

        Assert.assertEquals(Boolean.TRUE, attrHandler.get(model));
    }

    @Test
    public void testActiveForFutureToFrom() {
        final TargetZoneDeliveryModeValueModel model = Mockito.mock(TargetZoneDeliveryModeValueModel.class);
        BDDMockito.given(model.getValidTo()).willReturn(tomorrow);
        BDDMockito.given(model.getValidFrom()).willReturn(tomorrow);

        Assert.assertEquals(Boolean.FALSE, attrHandler.get(model));
    }

    @Test
    public void testActiveForFutureToPastFrom() {
        final TargetZoneDeliveryModeValueModel model = Mockito.mock(TargetZoneDeliveryModeValueModel.class);
        BDDMockito.given(model.getValidTo()).willReturn(tomorrow);
        BDDMockito.given(model.getValidFrom()).willReturn(yesterday);

        Assert.assertEquals(Boolean.TRUE, attrHandler.get(model));
    }

    // obviously invalid, but handle it anyway
    @Test
    public void testActiveForPastToFutureFrom() {
        final TargetZoneDeliveryModeValueModel model = Mockito.mock(TargetZoneDeliveryModeValueModel.class);
        BDDMockito.given(model.getValidTo()).willReturn(yesterday);
        BDDMockito.given(model.getValidFrom()).willReturn(tomorrow);

        Assert.assertEquals(Boolean.FALSE, attrHandler.get(model));
    }

}

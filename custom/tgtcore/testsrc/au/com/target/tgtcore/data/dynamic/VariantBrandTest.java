/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * 
 */
@UnitTest
public class VariantBrandTest {
    private VariantBrand dynamicAttribute;

    @Before
    public void prepare() {
        dynamicAttribute = new VariantBrand();
    }

    /*
     *  test variant that does not have a base product
     */
    @Test
    public void testGetVariantBaseBrandNull() throws Exception {
        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(null);

        final BrandModel brand = dynamicAttribute.get(model);

        org.junit.Assert.assertNull(brand);
    }

    /*
     *  test variant that has a product as the base, that base has null value for available for brand
     */
    @Test
    public void testGetVariantBaseProductBrandlNull() throws Exception {
        final TargetProductModel parent = Mockito.mock(TargetProductModel.class);
        BDDMockito.given(parent.getBrand()).willReturn(null);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final BrandModel brand = dynamicAttribute.get(model);

        org.junit.Assert.assertNull(brand);
    }

    /*
     *  test variant that has a product as the base, that base has true value for available for brand
     */
    @Test
    public void testGetVariantBaseProductBrandNotNull() throws Exception {
        final BrandModel mockBrand = Mockito.mock(BrandModel.class);
        final TargetProductModel parent = Mockito.mock(TargetProductModel.class);
        BDDMockito.given(parent.getBrand()).willReturn(mockBrand);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final BrandModel brand = dynamicAttribute.get(model);

        org.junit.Assert.assertNotNull(brand);
        org.junit.Assert.assertSame(mockBrand, brand);
    }

    /*
     *  test variant that has a another variant as the base, that base has null value for available for brand
     */
    @Test
    public void testGetVariantBaseVariantBrandNull() throws Exception {
        final AbstractTargetVariantProductModel parent = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(parent.getBrand()).willReturn(null);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final BrandModel brand = dynamicAttribute.get(model);

        org.junit.Assert.assertNull(brand);
    }

    /*
     *  test variant that has a another variant as the base, that base has true value for available for brand
     */
    @Test
    public void testGetVariantBaseVariantBrandNotNull() throws Exception {
        final BrandModel mockBrand = Mockito.mock(BrandModel.class);
        final AbstractTargetVariantProductModel parent = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(parent.getBrand()).willReturn(mockBrand);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final BrandModel brand = dynamicAttribute.get(model);

        org.junit.Assert.assertNotNull(brand);
        org.junit.Assert.assertSame(mockBrand, brand);
    }

    /*
     * It should never happen, but target variant where base product is not a target product
     */
    @Test
    public void testGetNonTargetParent() throws Exception {
        final ProductModel parent = Mockito.mock(ProductModel.class);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final BrandModel brand = dynamicAttribute.get(model);

        org.junit.Assert.assertNull(brand);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetNullModel() throws Exception {
        dynamicAttribute.get(null);
    }
}

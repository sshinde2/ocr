package au.com.target.tgtcore.data.dynamic;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.variants.model.VariantProductModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ColourModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;

import com.google.common.collect.ImmutableList;


/**
 * Test suite for {@link VariantDisplayName}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VariantDisplayNameTest {

    private static final String PRODUCT_NAME = "product";
    private static final String ABSTRACT_VARIANT_NAME = "abstract";
    private static final String COLOUR_VARIANT_NAME = "colour";
    private static final String COLOUR = "blue";
    private static final String SWATCH_COLOUR = "Celeste";
    private static final String SWATCH_NO_COLOUR_COLOUR = "No Colour";
    private static final String NO_COLOUR = "No Colour";

    private final VariantDisplayName attributeHandler = new VariantDisplayName();

    @Mock
    private AbstractTargetVariantProductModel abstractVariant;
    @Mock
    private TargetColourVariantProductModel colourVariant;
    @Mock
    private TargetSizeVariantProductModel sizeVariant;
    @Mock
    private TargetProductModel baseProduct;

    @Mock
    private ColourModel colourModel;

    @Mock
    private ColourModel swatchModel;

    /**
     * Initializes test cases before run.
     */
    @Before
    public void setUp() {
        when(sizeVariant.getBaseProduct()).thenReturn(colourVariant);
        when(colourVariant.getBaseProduct()).thenReturn(baseProduct);

        when(baseProduct.getName()).thenReturn(PRODUCT_NAME);
        when(abstractVariant.getName()).thenReturn(ABSTRACT_VARIANT_NAME);
        when(colourVariant.getName()).thenReturn(COLOUR_VARIANT_NAME);
        when(colourVariant.getColour()).thenReturn(colourModel);
        when(colourVariant.getSwatch()).thenReturn(swatchModel);
        when(colourModel.getDisplay()).thenReturn(Boolean.TRUE);

    }

    /**
     * Verifies that for unknown variant types, this attribute handler will return just a product name.
     */
    @Test
    public void testUnknownVariantType() {
        assertEquals(ABSTRACT_VARIANT_NAME, attributeHandler.get(abstractVariant));
    }

    /**
     * Verifies that for colour variant type, the attribute handler will attempt to resolve a display name based on the
     * number of available colour variants for the parent product (a.k.a. base product). If a product has a single
     * colour variant, the handler will return a colour variant name without any adjustments.
     * 
     */
    @Test
    public void testSingleColourVariantType() {
        when(baseProduct.getVariants()).thenReturn(
                ImmutableList.of((VariantProductModel)sizeVariant));
        assertEquals(COLOUR_VARIANT_NAME, attributeHandler.get(colourVariant));
    }

    /**
     * For a base product that have a multiple colour variants, the attribute handler will dynamically calculate the
     * display name based on a parent product and swatch colourname
     */
    @Test
    public void testMultipleColourVariantTypeWithSwatches() {
        when(baseProduct.getVariants()).thenReturn(
                ImmutableList.of((VariantProductModel)sizeVariant, abstractVariant));

        when(swatchModel.getDisplay()).thenReturn(Boolean.TRUE);
        when(colourVariant.getColourName()).thenReturn(COLOUR);
        when(colourVariant.getSwatchName()).thenReturn(SWATCH_COLOUR);
        assertEquals(PRODUCT_NAME + " - " + SWATCH_COLOUR, attributeHandler.get(colourVariant));
    }

    /**
     * For a base product that have a multiple colour variants, the attribute handler will dynamically calculate the
     * display name based on a parent product and swatch colourname. This test is for ensuring that if swatch colour
     * display is set to false then it will check and display the colour name if it is set to true.
     */
    @Test
    public void testMultipleColourVariantTypeWithSwatchesDisplayFalse() {
        when(baseProduct.getVariants()).thenReturn(
                ImmutableList.of((VariantProductModel)sizeVariant, abstractVariant));
        when(swatchModel.getDisplay()).thenReturn(Boolean.FALSE);
        when(colourVariant.getColourName()).thenReturn(COLOUR);
        when(colourVariant.getSwatchName()).thenReturn(SWATCH_NO_COLOUR_COLOUR);
        assertEquals(PRODUCT_NAME + " - " + COLOUR, attributeHandler.get(colourVariant));
    }


    /**
     * For a base product that have a multiple colour variants, the attribute handler will dynamically calculate the
     * display name based on a parent product and a color name.
     */
    @Test
    public void testMultipleColourVariantType() {
        when(baseProduct.getVariants()).thenReturn(
                ImmutableList.of((VariantProductModel)sizeVariant, abstractVariant));
        when(colourVariant.getColourName()).thenReturn(COLOUR);
        when(colourModel.getDisplay()).thenReturn(Boolean.TRUE);

        assertEquals(PRODUCT_NAME + " - " + COLOUR, attributeHandler.get(colourVariant));
    }

    /**
     * For a base product that have a multiple colour variants, the attribute handler will dynamically calculate the
     * display name based on a parent product and a color name provided there is no swatch colour or display is set to
     * false. This test is to ensure that if the colour variant itself is having no colour and display false then it
     * should display only product name.
     */
    @Test
    public void testMultipleColourVariantWithDisplayFalse() {
        when(baseProduct.getVariants()).thenReturn(
                ImmutableList.of((VariantProductModel)sizeVariant, abstractVariant));
        when(colourVariant.getColourName()).thenReturn(NO_COLOUR);
        when(colourVariant.getColour().getDisplay()).thenReturn(Boolean.FALSE);
        assertEquals(PRODUCT_NAME, attributeHandler.get(colourVariant));
    }


    /**
     * Verifies that for size variants we are delegating the flow of display name calculation to parent colour variant.
     */
    @Test
    public void testSizeVariantType() {
        when(colourVariant.getDisplayName()).thenReturn(COLOUR_VARIANT_NAME);
        assertEquals(COLOUR_VARIANT_NAME, attributeHandler.get(sizeVariant));
    }
}

package au.com.target.tgtcore.data.dynamic;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * Unit test for {@link VariantMerchDepartment}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VariantMerchDepartmentTest {

    // CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    // CHECKSTYLE:ON

    private final VariantMerchDepartment variantMerchDepartment = new VariantMerchDepartment();

    @Test
    public void testSet() {

        final AbstractTargetVariantProductModel variantProductModel = Mockito
                .mock(AbstractTargetVariantProductModel.class);
        final TargetMerchDepartmentModel merchDepartmentModel = Mockito.mock(TargetMerchDepartmentModel.class);

        expectedException.expect(UnsupportedOperationException.class);

        variantMerchDepartment.set(variantProductModel, merchDepartmentModel);
    }

    @Test
    public void testGetForNullModel() {
        expectedException.expect(IllegalArgumentException.class);

        variantMerchDepartment.get(null);
    }

    @Test
    public void testGetWithNoBaseProduct() {

        final AbstractTargetVariantProductModel variantProductModel = Mockito
                .mock(AbstractTargetVariantProductModel.class);

        Mockito.when(variantProductModel.getBaseProduct()).thenReturn(null);

        final TargetMerchDepartmentModel result = variantMerchDepartment.get(variantProductModel);

        Assert.assertNull(result);
    }

    @Test
    public void testGetWithNoAssignedMerchDepartment() {

        final AbstractTargetVariantProductModel variantProductModel = Mockito
                .mock(AbstractTargetVariantProductModel.class);
        final TargetProductModel baseProduct = Mockito.mock(TargetProductModel.class);

        Mockito.when(variantProductModel.getBaseProduct()).thenReturn(baseProduct);
        Mockito.when(baseProduct.getMerchDepartment()).thenReturn(null);

        final TargetMerchDepartmentModel result = variantMerchDepartment.get(variantProductModel);

        Assert.assertNull(result);
    }

    @Test
    public void testGetWithAssignedMerchDepartment() {

        final AbstractTargetVariantProductModel variantProductModel = Mockito
                .mock(AbstractTargetVariantProductModel.class);
        final TargetProductModel baseProduct = Mockito.mock(TargetProductModel.class);
        final TargetMerchDepartmentModel merchDepartmentModel = Mockito.mock(TargetMerchDepartmentModel.class);

        Mockito.when(variantProductModel.getBaseProduct()).thenReturn(baseProduct);
        Mockito.when(baseProduct.getMerchDepartment()).thenReturn(merchDepartmentModel);

        final TargetMerchDepartmentModel result = variantMerchDepartment.get(variantProductModel);

        Assert.assertEquals(merchDepartmentModel, result);
        Mockito.verify(baseProduct).getMerchDepartment();
    }

    @Test
    public void testGetForTwoLevelVariants() {

        final AbstractTargetVariantProductModel colourVariant = Mockito
                .mock(AbstractTargetVariantProductModel.class);
        final AbstractTargetVariantProductModel sizeVariant = Mockito
                .mock(AbstractTargetVariantProductModel.class);
        final TargetProductModel baseProduct = Mockito.mock(TargetProductModel.class);
        final TargetMerchDepartmentModel merchDepartmentModel = Mockito.mock(TargetMerchDepartmentModel.class);

        Mockito.when(baseProduct.getMerchDepartment()).thenReturn(merchDepartmentModel);
        Mockito.when(sizeVariant.getBaseProduct()).thenReturn(colourVariant);
        Mockito.when(colourVariant.getBaseProduct()).thenReturn(baseProduct);

        variantMerchDepartment.get(sizeVariant);

        // verifies just the recursive call
        Mockito.verify(colourVariant).getMerchDepartment();
    }

}

/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;


@UnitTest
public class VariantPrimaryCategoryTest {
    private VariantPrimaryCategory dynamicAttribute;

    @Before
    public void prepare() {
        dynamicAttribute = new VariantPrimaryCategory();
    }

    /*
     *  test variant that does not have a base product
     */
    @Test
    public void testGetVariantPrimaryCategoryNull() throws Exception {
        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(null);

        final CategoryModel category = dynamicAttribute.get(model);

        org.junit.Assert.assertNull(category);
    }

    /*
     *  test variant that has a product as the base, that base has null value for available for category
     */
    @Test
    public void testGetVariantBaseProductPrimaryCategoryNull() throws Exception {
        final TargetProductModel parent = Mockito.mock(TargetProductModel.class);
        BDDMockito.given(parent.getPrimarySuperCategory()).willReturn(null);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final CategoryModel category = dynamicAttribute.get(model);

        org.junit.Assert.assertNull(category);
    }

    /*
     *  test variant that has a product as the base, that base has true value for available for category
     */
    @Test
    public void testGetVariantBaseProductPrimaryCategoryNotNull() throws Exception {
        final TargetProductCategoryModel mockCategory = Mockito.mock(TargetProductCategoryModel.class);
        final TargetProductModel parent = Mockito.mock(TargetProductModel.class);
        BDDMockito.given(parent.getPrimarySuperCategory()).willReturn(mockCategory);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final CategoryModel category = dynamicAttribute.get(model);

        org.junit.Assert.assertNotNull(category);
        org.junit.Assert.assertSame(mockCategory, category);
    }

    /*
     *  test variant that has a another variant as the base, that base has null value for available for category
     */
    @Test
    public void testGetVariantBaseVariantPrimaryCategoryNull() throws Exception {
        final AbstractTargetVariantProductModel parent = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(parent.getPrimarySuperCategory()).willReturn(null);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final CategoryModel category = dynamicAttribute.get(model);

        org.junit.Assert.assertNull(category);
    }

    /*
     *  test variant that has a another variant as the base, that base has true value for available for category
     */
    @Test
    public void testGetVariantBaseVariantPrimaryCategoryNotNull() throws Exception {
        final TargetProductCategoryModel mockCategory = Mockito.mock(TargetProductCategoryModel.class);
        final AbstractTargetVariantProductModel parent = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(parent.getPrimarySuperCategory()).willReturn(mockCategory);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final CategoryModel category = dynamicAttribute.get(model);

        org.junit.Assert.assertNotNull(category);
        org.junit.Assert.assertSame(mockCategory, category);
    }

    /*
     * It should never happen, but target variant where base product is not a target product
     */
    @Test
    public void testGetNonTargetParent() throws Exception {
        final ProductModel parent = Mockito.mock(ProductModel.class);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final CategoryModel category = dynamicAttribute.get(model);

        org.junit.Assert.assertNull(category);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetNullModel() throws Exception {
        dynamicAttribute.get(null);
    }
}

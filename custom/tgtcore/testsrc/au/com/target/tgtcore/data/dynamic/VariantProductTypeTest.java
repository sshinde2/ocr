/**
 * 
 */
package au.com.target.tgtcore.data.dynamic;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * 
 */
@UnitTest
public class VariantProductTypeTest {
    private VariantProductType dynamicAttribute;

    @Before
    public void prepare() {
        dynamicAttribute = new VariantProductType();
    }

    /*
     *  test variant that does not have a base product
     */
    @Test
    public void testGetVariantBaseProductTypeNull() throws Exception {
        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(null);

        final ProductTypeModel productType = dynamicAttribute.get(model);

        org.junit.Assert.assertNull(productType);
    }

    /*
     *  test variant that has a product as the base, that base has null value for available for product type
     */
    @Test
    public void testGetVariantBaseProductProductTypelNull() throws Exception {
        final TargetProductModel parent = Mockito.mock(TargetProductModel.class);
        BDDMockito.given(parent.getProductType()).willReturn(null);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final ProductTypeModel productType = dynamicAttribute.get(model);

        org.junit.Assert.assertNull(productType);
    }

    /*
     *  test variant that has a product as the base, that base has true value for available for product type
     */
    @Test
    public void testGetVariantBaseProductProductTypeNotNull() throws Exception {
        final ProductTypeModel mockProductType = Mockito.mock(ProductTypeModel.class);
        final TargetProductModel parent = Mockito.mock(TargetProductModel.class);
        BDDMockito.given(parent.getProductType()).willReturn(mockProductType);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final ProductTypeModel productType = dynamicAttribute.get(model);

        org.junit.Assert.assertNotNull(productType);
        org.junit.Assert.assertSame(mockProductType, productType);
    }

    /*
     *  test variant that has a another variant as the base, that base has null value for available for product type
     */
    @Test
    public void testGetVariantBaseVariantProductTypeNull() throws Exception {
        final AbstractTargetVariantProductModel parent = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(parent.getProductType()).willReturn(null);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final ProductTypeModel productType = dynamicAttribute.get(model);

        org.junit.Assert.assertNull(productType);
    }

    /*
     *  test variant that has a another variant as the base, that base has true value for available for product type
     */
    @Test
    public void testGetVariantBaseVariantProductTypeNotNull() throws Exception {
        final ProductTypeModel mockProductType = Mockito.mock(ProductTypeModel.class);
        final AbstractTargetVariantProductModel parent = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(parent.getProductType()).willReturn(mockProductType);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final ProductTypeModel productType = dynamicAttribute.get(model);

        org.junit.Assert.assertNotNull(productType);
        org.junit.Assert.assertSame(mockProductType, productType);
    }

    /*
     * It should never happen, but target variant where base product is not a target product
     */
    @Test
    public void testGetNonTargetParent() throws Exception {
        final ProductModel parent = Mockito.mock(ProductModel.class);

        final AbstractTargetVariantProductModel model = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(model.getBaseProduct()).willReturn(parent);

        final ProductTypeModel productType = dynamicAttribute.get(model);

        org.junit.Assert.assertNull(productType);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetNullModel() throws Exception {
        dynamicAttribute.get(null);
    }
}

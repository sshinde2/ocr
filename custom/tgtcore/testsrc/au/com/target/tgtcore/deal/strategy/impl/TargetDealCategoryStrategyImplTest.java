/**
 * 
 */
package au.com.target.tgtcore.deal.strategy.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.deals.TargetDealService;
import au.com.target.tgtcore.deals.strategy.impl.TargetDealCategoryStrategyImpl;
import au.com.target.tgtcore.model.TargetDealCategoryModel;


/**
 * @author bhuang3
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetDealCategoryStrategyImplTest {

    @InjectMocks
    private final TargetDealCategoryStrategyImpl targetDealCategoryStrategyImpl = new TargetDealCategoryStrategyImpl();

    @Mock
    private TargetDealService targetDealService;


    @Test
    public void testIsQualifierDealCategory() {
        final TargetDealCategoryModel targetDealCategoryModel = new TargetDealCategoryModel();
        final AbstractSimpleDealModel dealModel = new AbstractSimpleDealModel();
        final DealQualifierModel qulifierModel = new DealQualifierModel();
        final List<TargetDealCategoryModel> categoryList = new ArrayList<>();
        categoryList.add(targetDealCategoryModel);
        qulifierModel.setDealCategory(categoryList);
        final List<DealQualifierModel> qualifierList = new ArrayList<>();
        qualifierList.add(qulifierModel);
        dealModel.setQualifierList(qualifierList);
        given(targetDealService.getDealByCategory(targetDealCategoryModel)).willReturn(dealModel);
        Assert.assertTrue(targetDealCategoryStrategyImpl.isQualifierDealCategory(targetDealCategoryModel));
    }

    @Test
    public void testIsQualifierDealCategoryNoMatch() {
        final TargetDealCategoryModel targetDealCategoryModel = new TargetDealCategoryModel();
        final AbstractSimpleDealModel dealModel = new AbstractSimpleDealModel();
        final DealQualifierModel qulifierModel = new DealQualifierModel();
        final List<TargetDealCategoryModel> categoryList = new ArrayList<>();
        qulifierModel.setDealCategory(categoryList);
        final List<DealQualifierModel> qualifierList = new ArrayList<>();
        qualifierList.add(qulifierModel);
        dealModel.setQualifierList(qualifierList);
        given(targetDealService.getDealByCategory(targetDealCategoryModel)).willReturn(dealModel);
        Assert.assertFalse(targetDealCategoryStrategyImpl.isQualifierDealCategory(targetDealCategoryModel));
    }

    @Test
    public void testIsQualifierDealCategoryWithEmptyQualifierList() {
        final TargetDealCategoryModel targetDealCategoryModel = new TargetDealCategoryModel();
        final AbstractSimpleDealModel dealModel = new AbstractSimpleDealModel();
        final List<DealQualifierModel> qualifierList = new ArrayList<>();
        dealModel.setQualifierList(qualifierList);
        given(targetDealService.getDealByCategory(targetDealCategoryModel)).willReturn(dealModel);
        Assert.assertFalse(targetDealCategoryStrategyImpl.isQualifierDealCategory(targetDealCategoryModel));
    }

    @Test
    public void testIsQualifierDealCategoryWithNullTargetDealCategoryModel() {
        final TargetDealCategoryModel targetDealCategoryModel = null;
        Assert.assertFalse(targetDealCategoryStrategyImpl.isQualifierDealCategory(targetDealCategoryModel));
    }

    @Test
    public void testIsRewardDealCategory() {
        final TargetDealCategoryModel targetDealCategoryModel = new TargetDealCategoryModel();
        final AbstractSimpleDealModel dealModel = new AbstractSimpleDealModel();
        final List<TargetDealCategoryModel> categoryList = new ArrayList<>();
        categoryList.add(targetDealCategoryModel);
        dealModel.setRewardCategory(categoryList);
        given(targetDealService.getDealByCategory(targetDealCategoryModel)).willReturn(dealModel);
        Assert.assertTrue(targetDealCategoryStrategyImpl.isRewardDealCategory(targetDealCategoryModel));
    }

    @Test
    public void testIsRewardDealCategoryWithEmptyCategoryList() {
        final TargetDealCategoryModel targetDealCategoryModel = new TargetDealCategoryModel();
        final AbstractSimpleDealModel dealModel = new AbstractSimpleDealModel();
        final List<TargetDealCategoryModel> categoryList = new ArrayList<>();
        dealModel.setRewardCategory(categoryList);
        given(targetDealService.getDealByCategory(targetDealCategoryModel)).willReturn(dealModel);
        Assert.assertFalse(targetDealCategoryStrategyImpl.isRewardDealCategory(targetDealCategoryModel));
    }

    @Test
    public void testIsRewardDealCategoryWithNullTargetDealCategoryModel() {
        final TargetDealCategoryModel targetDealCategoryModel = null;
        Assert.assertFalse(targetDealCategoryStrategyImpl.isRewardDealCategory(targetDealCategoryModel));
    }

    @Test
    public void testIsActiveDeal() throws ParseException {
        final AbstractDealModel dealModel = new AbstractDealModel();
        dealModel.setEnabled(Boolean.TRUE);
        final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        final Date startDate = sdf.parse("01/01/1980 12:00:00");
        final Date endDate = sdf.parse("01/01/2080 12:00:00");
        dealModel.setStartDate(startDate);
        dealModel.setEndDate(endDate);
        Assert.assertTrue(targetDealCategoryStrategyImpl.isActiveDeal(dealModel));
    }

    @Test
    public void testIsActiveDealWithDisable() throws ParseException {
        final AbstractDealModel dealModel = new AbstractDealModel();
        dealModel.setEnabled(Boolean.FALSE);
        final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        final Date startDate = sdf.parse("01/01/1980 12:00:00");
        final Date endDate = sdf.parse("01/01/2080 12:00:00");
        dealModel.setStartDate(startDate);
        dealModel.setEndDate(endDate);
        Assert.assertFalse(targetDealCategoryStrategyImpl.isActiveDeal(dealModel));
    }

    @Test
    public void testIsActiveDealWithEnableNull() throws ParseException {
        final AbstractDealModel dealModel = new AbstractDealModel();
        dealModel.setEnabled(null);
        final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        final Date startDate = sdf.parse("01/01/1980 12:00:00");
        final Date endDate = sdf.parse("01/01/2080 12:00:00");
        dealModel.setStartDate(startDate);
        dealModel.setEndDate(endDate);
        Assert.assertFalse(targetDealCategoryStrategyImpl.isActiveDeal(dealModel));
    }

    @Test
    public void testIsActiveDealWithEnableStartDateAfterNow() throws ParseException {
        final AbstractDealModel dealModel = new AbstractDealModel();
        dealModel.setEnabled(Boolean.TRUE);
        final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        final Date startDate = sdf.parse("01/01/2080 12:00:00");
        final Date endDate = sdf.parse("01/01/2080 12:00:00");
        dealModel.setStartDate(startDate);
        dealModel.setEndDate(endDate);
        Assert.assertFalse(targetDealCategoryStrategyImpl.isActiveDeal(dealModel));
    }

    @Test
    public void testIsActiveDealWithEnableEndDateBeforeNow() throws ParseException {
        final AbstractDealModel dealModel = new AbstractDealModel();
        dealModel.setEnabled(Boolean.TRUE);
        final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        final Date startDate = sdf.parse("01/01/1980 12:00:00");
        final Date endDate = sdf.parse("01/01/1980 12:00:00");
        dealModel.setStartDate(startDate);
        dealModel.setEndDate(endDate);
        Assert.assertFalse(targetDealCategoryStrategyImpl.isActiveDeal(dealModel));
    }
}

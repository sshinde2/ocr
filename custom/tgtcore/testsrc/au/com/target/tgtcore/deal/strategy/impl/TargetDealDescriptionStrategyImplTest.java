/**
 * 
 */
package au.com.target.tgtcore.deal.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.promotions.model.ValueBundleDealModel;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.deals.strategy.impl.TargetDealDescriptionStrategyImpl;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * @author mjanarth
 * 
 */
@UnitTest
public class TargetDealDescriptionStrategyImplTest {

    private static final String GENERIC_MESSAGE = "This item is a part of deal";

    @Mock
    private PromotionsService promotionsService;
    @InjectMocks
    private final TargetDealDescriptionStrategyImpl strategy = new TargetDealDescriptionStrategyImpl();

    @Before
    public void prepare() {
        promotionsService = Mockito.mock(PromotionsService.class);
        strategy.setDefaultDealDescriptionMessage(GENERIC_MESSAGE);
        final PromotionGroupModel dfPromotionGrp = Mockito.mock(PromotionGroupModel.class);
        BDDMockito.given(promotionsService.getDefaultPromotionGroup()).willReturn(dfPromotionGrp);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSizeVariantProductModelWithDeal() throws FieldValueProviderException {

        final VariantProductModel sizevariant = new TargetSizeVariantProductModel();
        final ProductPromotionModel dealModel = Mockito.mock(ValueBundleDealModel.class);
        BDDMockito.given(dealModel.getDescription()).willReturn("Description");
        final TargetColourVariantProductModel model = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(model.getVariants()).willReturn(Collections.singletonList(sizevariant));
        BDDMockito
                .given(promotionsService.getProductPromotions(Mockito.anyList(),
                        Mockito.any(VariantProductModel.class),
                        Mockito.eq(true), Mockito.any(Date.class)))
                .willReturn(
                        Collections.singletonList(
                                dealModel));
        final String dealDescription = strategy.getSummaryDealDescription(model);
        Assert.assertNotNull(dealDescription);
        Assert.assertEquals("Description", dealDescription);

    }

    @Test
    public void testSizeVariantProductModelWithNoDeal() throws FieldValueProviderException {


        final ProductPromotionModel dealModel = Mockito.mock(ValueBundleDealModel.class);
        final VariantProductModel sizevariant = new TargetSizeVariantProductModel();
        BDDMockito.given(dealModel.getDescription()).willReturn("Description");
        final TargetColourVariantProductModel model = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(model.getVariants()).willReturn(Collections.singletonList(sizevariant));
        BDDMockito
                .given(promotionsService.getProductPromotions(Mockito.anyList(), Mockito.eq(sizevariant),
                        Mockito.eq(true), Mockito.any(Date.class)))
                .willReturn(
                        null);
        final String dealDescription = strategy.getSummaryDealDescription(model);
        Assert.assertNull(dealDescription);


    }

    @Test
    public void testSizeVariantProductModelWithDiffDeal() throws FieldValueProviderException {


        final VariantProductModel sizevariant = new TargetSizeVariantProductModel();
        sizevariant.setCode("productA");
        final VariantProductModel sizevariantA = new TargetSizeVariantProductModel();
        sizevariantA.setCode("productB");
        final List<VariantProductModel> variantsList = new ArrayList<>();
        variantsList.add(sizevariantA);
        variantsList.add(sizevariant);
        final ProductPromotionModel dealModel = Mockito.mock(ValueBundleDealModel.class);
        final ProductPromotionModel dealModelB = Mockito.mock(ValueBundleDealModel.class);
        final ProductPromotionModel dealModelC = Mockito.mock(ValueBundleDealModel.class);
        final List<ProductPromotionModel> promotionListA = new ArrayList<>();
        final List<ProductPromotionModel> promotionListB = new ArrayList<>();
        promotionListA.add(dealModel);
        promotionListB.add(dealModelB);
        BDDMockito.given(dealModel.getDescription()).willReturn("Description");
        BDDMockito.given(dealModel.getCode()).willReturn("Code A");
        BDDMockito.given(dealModelB.getDescription()).willReturn("Description B");
        BDDMockito.given(dealModelB.getCode()).willReturn("Code B");
        BDDMockito.given(dealModelC.getDescription()).willReturn("Description C");
        BDDMockito.given(dealModelC.getCode()).willReturn("Code C");
        final TargetColourVariantProductModel model = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(model.getVariants()).willReturn(variantsList);
        BDDMockito
                .given(promotionsService.getProductPromotions(Mockito.anyList(),
                        Mockito.eq(sizevariant),
                        Mockito.eq(true), Mockito.any(Date.class)))
                .willReturn(
                        promotionListA);
        BDDMockito
                .given(promotionsService.getProductPromotions(Mockito.anyList(),
                        Mockito.eq(sizevariantA),
                        Mockito.eq(true), Mockito.any(Date.class)))
                .willReturn(
                        promotionListB);
        final String dealDescription = strategy.getSummaryDealDescription(model);
        Assert.assertNotNull(dealDescription);
        Assert.assertEquals(GENERIC_MESSAGE, dealDescription);

    }

    @Test
    public void testSizeVariantProductModelWithAllSameDeal() throws FieldValueProviderException {

        final VariantProductModel sizevariant = new TargetSizeVariantProductModel();
        sizevariant.setCode("productA");
        final VariantProductModel sizevariantA = new TargetSizeVariantProductModel();
        sizevariantA.setCode("productB");
        final List<VariantProductModel> variantsList = new ArrayList<>();
        variantsList.add(sizevariantA);
        variantsList.add(sizevariant);
        final ProductPromotionModel dealModel = Mockito.mock(ValueBundleDealModel.class);
        final List<ProductPromotionModel> promotionListA = new ArrayList<>();
        final List<ProductPromotionModel> promotionListB = new ArrayList<>();
        promotionListA.add(dealModel);
        promotionListB.add(dealModel);
        BDDMockito.given(dealModel.getDescription()).willReturn("Description");
        BDDMockito.given(dealModel.getCode()).willReturn("Code A");
        final TargetColourVariantProductModel model = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(model.getVariants()).willReturn(variantsList);
        BDDMockito
                .given(promotionsService.getProductPromotions(Mockito.anyList(),
                        Mockito.eq(sizevariant),
                        Mockito.eq(true), Mockito.any(Date.class)))
                .willReturn(
                        promotionListA);
        BDDMockito
                .given(promotionsService.getProductPromotions(Mockito.anyList(),
                        Mockito.eq(sizevariantA),
                        Mockito.eq(true), Mockito.any(Date.class)))
                .willReturn(
                        promotionListB);
        final String dealDescription = strategy.getSummaryDealDescription(model);
        Assert.assertNotNull(dealDescription);
        Assert.assertEquals("Description", dealDescription);

    }

    @Test
    public void testSizeVariantProductModelWithDealAndNoDeal() throws FieldValueProviderException {

        final VariantProductModel sizevariant = new TargetSizeVariantProductModel();
        sizevariant.setCode("productA");
        final VariantProductModel sizevariantA = new TargetSizeVariantProductModel();
        sizevariantA.setCode("productB");
        final List<VariantProductModel> variantsList = new ArrayList<>();
        variantsList.add(sizevariant);
        variantsList.add(sizevariantA);
        final ProductPromotionModel dealModel = Mockito.mock(ValueBundleDealModel.class);
        final List<ProductPromotionModel> promotionListA = new ArrayList<>();
        promotionListA.add(dealModel);
        BDDMockito.given(dealModel.getDescription()).willReturn("Description");
        BDDMockito.given(dealModel.getCode()).willReturn("Code A");
        final TargetColourVariantProductModel model = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(model.getVariants()).willReturn(variantsList);
        BDDMockito
                .given(promotionsService.getProductPromotions(Mockito.anyList(),
                        Mockito.eq(sizevariant),
                        Mockito.eq(true), Mockito.any(Date.class)))
                .willReturn(
                        promotionListA);
        BDDMockito
                .given(promotionsService.getProductPromotions(Mockito.anyList(),
                        Mockito.eq(sizevariantA),
                        Mockito.eq(true), Mockito.any(Date.class)))
                .willReturn(
                        null);
        final String dealDescription = strategy.getSummaryDealDescription(model);
        Assert.assertNotNull(dealDescription);
        Assert.assertEquals(GENERIC_MESSAGE, dealDescription);

    }

    @Test
    public void testColorVariantProductModelWithDeal() throws FieldValueProviderException {


        final ProductPromotionModel dealModel = Mockito.mock(ValueBundleDealModel.class);
        BDDMockito.given(dealModel.getDescription()).willReturn("Color Variant Deal");
        final TargetColourVariantProductModel model = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(model.getVariants()).willReturn(null);
        BDDMockito
                .given(promotionsService.getProductPromotions(Mockito.anyList(), Mockito.eq(model),
                        Mockito.eq(true), Mockito.any(Date.class)))
                .willReturn(
                        Collections.singletonList(
                                dealModel));
        final String dealDescription = strategy.getSummaryDealDescription(model);
        Assert.assertNotNull(dealDescription);
        Assert.assertEquals("Color Variant Deal", dealDescription);

    }

    @Test
    public void testColorVariantProductModelWithNoDeal() throws FieldValueProviderException {

        final ProductPromotionModel dealModel = Mockito.mock(ValueBundleDealModel.class);
        BDDMockito.given(dealModel.getDescription()).willReturn("Description");
        final TargetColourVariantProductModel model = Mockito.mock(TargetColourVariantProductModel.class);
        BDDMockito.given(model.getVariants()).willReturn(null);
        BDDMockito
                .given(promotionsService.getProductPromotions(Mockito.anyList(), Mockito.eq(model),
                        Mockito.eq(true), Mockito.any(Date.class)))
                .willReturn(
                        null);
        Assert.assertNull(strategy.getSummaryDealDescription(model));

    }

}

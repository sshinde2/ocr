package au.com.target.tgtcore.deals.dao.impl;

import de.hybris.platform.core.Registry;
import de.hybris.platform.jalo.ConsistencyCheckException;
import de.hybris.platform.promotions.jalo.AbstractDeal;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.ValueBundleDealModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;


/**
 * 
 */
public final class DealsTestHelper {

    private static ModelService modelService = (ModelService)Registry.getApplicationContext().getBean("modelService");

    private DealsTestHelper() {
        // never instantiate
    }

    public static AbstractDealModel createValueBundleDeal(final String dealId, final String eventId,
            final double rewardValue) {
        return createValueBundleDeal(dealId, eventId, rewardValue, null);
    }


    public static AbstractDealModel createValueBundleDeal(final String dealId, final String eventId,
            final double rewardValue, final Date endDate) {
        final DateTime now = new DateTime();
        final ValueBundleDealModel valueBundleDeal = modelService.create(ValueBundleDealModel.class);
        valueBundleDeal.setCode(dealId);
        valueBundleDeal.setEventId(eventId);
        valueBundleDeal.setRewardValue(Double.valueOf(rewardValue));
        valueBundleDeal.setRewardCategory(Collections.EMPTY_LIST);
        valueBundleDeal.setQualifierList(Collections.EMPTY_LIST);
        valueBundleDeal.setEndDate(endDate);
        valueBundleDeal.setPageTitle("Page Title");
        modelService.save(valueBundleDeal);

        // Using persistence(Jalo) layer directly as a hack to by-pass an interceptor(AbstractDealModelPrepareInterceptor)
        // which resets the end date to current time if end date is in the past
        if (endDate != null && endDate.before(now.toDate())) {
            final AbstractDeal expiredItem = modelService.toPersistenceLayer(valueBundleDeal);
            expiredItem.setEndDate(endDate);
        }

        return valueBundleDeal;
    }

    public static void removeDeal(final List<AbstractDealModel> abstractDealList) {
        if (CollectionUtils.isEmpty(abstractDealList)) {
            return;
        }
        for (final AbstractDealModel abstractDealModel : abstractDealList) {
            DealsTestHelper.removeDeal(abstractDealModel);
        }
    }

    public static void removeDeal(final AbstractDealModel removedDeal) {
        final AbstractDeal removedDealItem = modelService.toPersistenceLayer(removedDeal);
        try {
            // Using persistence(Jalo) layer directly as a hack to by-pass an interceptor(AbstractDealModelRemoveInterceptor)
            // which prevents removing deals with start date in the past or null
            removedDealItem.remove();
        }
        //CHECKSTYLE:OFF deliberate consumption of exception without action
        catch (final ConsistencyCheckException e) {
            // no op
        }
        //CHECKSTYLE:ON
    }

}

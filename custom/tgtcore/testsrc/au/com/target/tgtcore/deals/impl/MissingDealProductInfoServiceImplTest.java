package au.com.target.tgtcore.deals.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.category.TargetCategoryService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.deals.dao.MissingDealProductInfoDao;
import au.com.target.tgtcore.model.MissingDealProductInfoModel;
import au.com.target.tgtcore.product.TargetProductService;


/**
 * Unit test for {@link MissingDealProductInfoServiceImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MissingDealProductInfoServiceImplTest {

    @InjectMocks
    private final MissingDealProductInfoServiceImpl missingDealProductInfoServiceImpl = new MissingDealProductInfoServiceImpl();

    @Mock
    private MissingDealProductInfoDao missingDealProductInfoDao;

    @Mock
    private ModelService modelService;

    @Mock
    private CatalogVersionService catalogVersionService;

    @Mock
    private TargetCategoryService categoryService;

    @Mock
    private TargetProductService productService;

    private CatalogVersionModel mockStagedProductCatalog;
    private CatalogVersionModel mockOnlineProductCatalog;

    private List<MissingDealProductInfoModel> missingDealProductInfos;

    @Before
    public void setup() {
        mockStagedProductCatalog = Mockito.mock(CatalogVersionModel.class);
        BDDMockito.given(
                catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                        TgtCoreConstants.Catalog.OFFLINE_VERSION)).willReturn(
                mockStagedProductCatalog);

        mockOnlineProductCatalog = Mockito.mock(CatalogVersionModel.class);
        BDDMockito.given(
                catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                        TgtCoreConstants.Catalog.ONLINE_VERSION)).willReturn(
                mockOnlineProductCatalog);

        missingDealProductInfos = new ArrayList<>();
        BDDMockito.given(missingDealProductInfoDao.findAll()).willReturn(missingDealProductInfos);
    }

    @Test
    public void testPurgeMissingProductsForExpiredDealsForEmpty() {
        BDDMockito.given(missingDealProductInfoDao.findMissingProductsForExpiredDeals()).willReturn(
                Collections.EMPTY_LIST);

        missingDealProductInfoServiceImpl.purgeMissingProductsForExpiredDeals();

        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testPurgeMissingProductsForExpiredDeals() {

        final int size = 5;
        final List<MissingDealProductInfoModel> missingDealProductInfoModels = new ArrayList<>();
        for (int index = 0; index < size; index++) {
            missingDealProductInfoModels.add(Mockito.mock(MissingDealProductInfoModel.class));
        }

        BDDMockito.given(missingDealProductInfoDao.findMissingProductsForExpiredDeals()).willReturn(
                missingDealProductInfoModels);

        missingDealProductInfoServiceImpl.setBatchSizeForRemove(2);
        missingDealProductInfoServiceImpl.purgeMissingProductsForExpiredDeals();

        Mockito.verify(modelService, Mockito.times(3)).removeAll(
                Mockito.anyCollectionOf(MissingDealProductInfoModel.class));
    }

    @Test
    public void testAssocateMissingProductsWithCategoriesWithNoMissingProducts() {
        missingDealProductInfoServiceImpl.assocateMissingProductsWithCategories();
    }

    @Test
    public void testAssocateMissingProductsWithCategoriesWithMissingProductsThatDontExistYet() {
        final MissingDealProductInfoModel mockMissingDealProductInfo1 = Mockito.mock(MissingDealProductInfoModel.class);
        BDDMockito.given(mockMissingDealProductInfo1.getProductCode()).willReturn("12345678");
        BDDMockito.given(mockMissingDealProductInfo1.getDealCategoryCode()).willReturn("Deal123Cat1");

        final MissingDealProductInfoModel mockMissingDealProductInfo2 = Mockito.mock(MissingDealProductInfoModel.class);
        BDDMockito.given(mockMissingDealProductInfo2.getProductCode()).willReturn("23456789");
        BDDMockito.given(mockMissingDealProductInfo2.getDealCategoryCode()).willReturn("Deal123Cat2");

        final MissingDealProductInfoModel mockMissingDealProductInfo3 = Mockito.mock(MissingDealProductInfoModel.class);
        BDDMockito.given(mockMissingDealProductInfo3.getProductCode()).willReturn("34567890");
        BDDMockito.given(mockMissingDealProductInfo3.getDealCategoryCode()).willReturn("Deal123Cat3");

        missingDealProductInfos.add(mockMissingDealProductInfo1);
        missingDealProductInfos.add(mockMissingDealProductInfo2);
        missingDealProductInfos.add(mockMissingDealProductInfo3);

        BDDMockito.given(productService.getProductForCode(mockOnlineProductCatalog, "12345678")).willThrow(
                new UnknownIdentifierException("No product"));
        BDDMockito.given(productService.getProductForCode(mockOnlineProductCatalog, "23456789")).willThrow(
                new UnknownIdentifierException("No product"));
        BDDMockito.given(productService.getProductForCode(mockOnlineProductCatalog, "34567890")).willThrow(
                new UnknownIdentifierException("No product"));

        missingDealProductInfoServiceImpl.assocateMissingProductsWithCategories();

        Mockito.verify(productService, Mockito.never()).getProductForCode(mockStagedProductCatalog, "12345678");
        Mockito.verify(productService, Mockito.never()).getProductForCode(mockStagedProductCatalog, "23456789");
        Mockito.verify(productService, Mockito.never()).getProductForCode(mockStagedProductCatalog, "34567890");

        Mockito.verify(categoryService, Mockito.never()).getCategoryForCode(mockOnlineProductCatalog, "Deal123Cat1");
        Mockito.verify(categoryService, Mockito.never()).getCategoryForCode(mockStagedProductCatalog, "Deal123Cat2");
        Mockito.verify(categoryService, Mockito.never()).getCategoryForCode(mockOnlineProductCatalog, "Deal123Cat1");
        Mockito.verify(categoryService, Mockito.never()).getCategoryForCode(mockStagedProductCatalog, "Deal123Cat2");
        Mockito.verify(categoryService, Mockito.never()).getCategoryForCode(mockOnlineProductCatalog, "Deal123Cat1");
        Mockito.verify(categoryService, Mockito.never()).getCategoryForCode(mockStagedProductCatalog, "Deal123Cat2");

        Mockito.verify(modelService, Mockito.never()).saveAll(Mockito.anyCollection());
        Mockito.verify(modelService, Mockito.never()).removeAll(Mockito.anyCollection());
    }

    @Test
    public void testAssocateMissingProductsWithCategoriesWithProductsInTheSameCategory() {
        final MissingDealProductInfoModel mockMissingDealProductInfo1 = Mockito.mock(MissingDealProductInfoModel.class);
        BDDMockito.given(mockMissingDealProductInfo1.getProductCode()).willReturn("12345678");
        BDDMockito.given(mockMissingDealProductInfo1.getDealCategoryCode()).willReturn("Deal123Cat1");

        final MissingDealProductInfoModel mockMissingDealProductInfo2 = Mockito.mock(MissingDealProductInfoModel.class);
        BDDMockito.given(mockMissingDealProductInfo2.getProductCode()).willReturn("23456789");
        BDDMockito.given(mockMissingDealProductInfo2.getDealCategoryCode()).willReturn("Deal123Cat1");

        missingDealProductInfos.add(mockMissingDealProductInfo1);
        missingDealProductInfos.add(mockMissingDealProductInfo2);

        final ProductModel mockOnlineProduct1 = Mockito.mock(ProductModel.class);
        BDDMockito.given(productService.getProductForCode(mockOnlineProductCatalog, "12345678")).willReturn(
                mockOnlineProduct1);
        final ProductModel mockStagedProduct1 = Mockito.mock(ProductModel.class);
        BDDMockito.given(productService.getProductForCode(mockStagedProductCatalog, "12345678")).willReturn(
                mockStagedProduct1);

        final ProductModel mockOnlineProduct2 = Mockito.mock(ProductModel.class);
        BDDMockito.given(productService.getProductForCode(mockOnlineProductCatalog, "23456789")).willReturn(
                mockOnlineProduct2);
        final ProductModel mockStagedProduct2 = Mockito.mock(ProductModel.class);
        BDDMockito.given(productService.getProductForCode(mockStagedProductCatalog, "23456789")).willReturn(
                mockStagedProduct2);

        final CategoryModel onlineCategory1 = new SettingGettingCategoryModel();
        final CategoryModel spyOnlineCategory1 = Mockito.spy(onlineCategory1);
        BDDMockito.given(spyOnlineCategory1.getCode()).willReturn("Deal123Cat1");
        BDDMockito.given(categoryService.getCategoryForCode(mockOnlineProductCatalog, "Deal123Cat1")).willReturn(
                spyOnlineCategory1);

        final CategoryModel stagedCategory1 = new SettingGettingCategoryModel();
        final CategoryModel spyStagedCategory1 = Mockito.spy(stagedCategory1);
        BDDMockito.given(spyStagedCategory1.getCode()).willReturn("Deal123Cat1");
        BDDMockito.given(categoryService.getCategoryForCode(mockStagedProductCatalog, "Deal123Cat1")).willReturn(
                spyStagedCategory1);

        missingDealProductInfoServiceImpl.assocateMissingProductsWithCategories();

        Mockito.verify(categoryService).getCategoryForCode(mockOnlineProductCatalog, "Deal123Cat1");
        Mockito.verify(categoryService).getCategoryForCode(mockStagedProductCatalog, "Deal123Cat1");

        final ArgumentCaptor<Collection> categoryCaptor = ArgumentCaptor.forClass(Collection.class);
        BDDMockito.verify(modelService, Mockito.times(2)).saveAll(categoryCaptor.capture());

        final ArgumentCaptor<List> missingDealProductsSuccessfullyLinkedCaptor = ArgumentCaptor.forClass(List.class);
        BDDMockito.verify(modelService).removeAll(missingDealProductsSuccessfullyLinkedCaptor.capture());

        final List<Collection> categoryLists = categoryCaptor.getAllValues();
        Assertions.assertThat(categoryLists).isNotEmpty().hasSize(2);

        final List missingDealProductsSuccessfullyLinked = missingDealProductsSuccessfullyLinkedCaptor.getValue();
        Assertions.assertThat(missingDealProductsSuccessfullyLinked).isNotEmpty().hasSize(2)
                .containsOnly(mockMissingDealProductInfo1, mockMissingDealProductInfo2);

        final List stagedProductsFromStagedCategory1 = spyStagedCategory1.getProducts();
        Assertions.assertThat(stagedProductsFromStagedCategory1).isNotEmpty()
                .containsOnly(mockStagedProduct1, mockStagedProduct2);
        final List onlineProductsFromOnlineCategory1 = spyOnlineCategory1.getProducts();
        Assertions.assertThat(onlineProductsFromOnlineCategory1).isNotEmpty()
                .containsOnly(mockOnlineProduct1, mockOnlineProduct2);
    }

    @Test
    public void testAssocateMissingProductsWithCategories() throws Exception {
        final MissingDealProductInfoModel mockMissingDealProductInfo1 = Mockito.mock(MissingDealProductInfoModel.class);
        BDDMockito.given(mockMissingDealProductInfo1.getProductCode()).willReturn("12345678");
        BDDMockito.given(mockMissingDealProductInfo1.getDealCategoryCode()).willReturn("Deal123Cat1");

        final MissingDealProductInfoModel mockMissingDealProductInfo2 = Mockito.mock(MissingDealProductInfoModel.class);
        BDDMockito.given(mockMissingDealProductInfo2.getProductCode()).willReturn("23456789");
        BDDMockito.given(mockMissingDealProductInfo2.getDealCategoryCode()).willReturn("Deal123Cat2");

        final MissingDealProductInfoModel mockMissingDealProductInfo3 = Mockito.mock(MissingDealProductInfoModel.class);
        BDDMockito.given(mockMissingDealProductInfo3.getProductCode()).willReturn("34567890");
        BDDMockito.given(mockMissingDealProductInfo3.getDealCategoryCode()).willReturn("Deal123Cat3");

        missingDealProductInfos.add(mockMissingDealProductInfo1);
        missingDealProductInfos.add(mockMissingDealProductInfo2);
        missingDealProductInfos.add(mockMissingDealProductInfo3);

        final ProductModel mockOnlineProduct1 = Mockito.mock(ProductModel.class);
        BDDMockito.given(productService.getProductForCode(mockOnlineProductCatalog, "12345678")).willReturn(
                mockOnlineProduct1);
        final ProductModel mockStagedProduct1 = Mockito.mock(ProductModel.class);
        BDDMockito.given(productService.getProductForCode(mockStagedProductCatalog, "12345678")).willReturn(
                mockStagedProduct1);

        BDDMockito.given(productService.getProductForCode(mockOnlineProductCatalog, "23456789")).willThrow(
                new UnknownIdentifierException("No product"));

        final ProductModel mockOnlineProduct3 = Mockito.mock(ProductModel.class);
        BDDMockito.given(productService.getProductForCode(mockOnlineProductCatalog, "34567890")).willReturn(
                mockOnlineProduct3);
        final ProductModel mockStagedProduct3 = Mockito.mock(ProductModel.class);
        BDDMockito.given(productService.getProductForCode(mockStagedProductCatalog, "34567890")).willReturn(
                mockStagedProduct3);

        final CategoryModel mockOnlineCategory1 = Mockito.mock(CategoryModel.class);
        BDDMockito.given(mockOnlineCategory1.getCode()).willReturn("Deal123Cat1");
        final List<ProductModel> productsForMockOnlineCategory1 = new ArrayList<>();
        BDDMockito.given(mockOnlineCategory1.getProducts()).willReturn(productsForMockOnlineCategory1);
        BDDMockito.given(categoryService.getCategoryForCode(mockOnlineProductCatalog, "Deal123Cat1")).willReturn(
                mockOnlineCategory1);
        final CategoryModel mockStagedCategory1 = Mockito.mock(CategoryModel.class);
        BDDMockito.given(mockStagedCategory1.getCode()).willReturn("Deal123Cat1");
        final List<ProductModel> productsForMockStagedCategory1 = new ArrayList<>();
        BDDMockito.given(mockStagedCategory1.getProducts()).willReturn(productsForMockStagedCategory1);
        BDDMockito.given(categoryService.getCategoryForCode(mockStagedProductCatalog, "Deal123Cat1")).willReturn(
                mockStagedCategory1);

        final CategoryModel mockOnlineCategory3 = Mockito.mock(CategoryModel.class);
        BDDMockito.given(mockOnlineCategory3.getCode()).willReturn("Deal123Cat3");
        final List<ProductModel> productsForMockOnlineCategory3 = new ArrayList<>();
        BDDMockito.given(mockOnlineCategory3.getProducts()).willReturn(productsForMockOnlineCategory3);
        BDDMockito.given(categoryService.getCategoryForCode(mockOnlineProductCatalog, "Deal123Cat3")).willReturn(
                mockOnlineCategory3);
        final CategoryModel mockStagedCategory3 = Mockito.mock(CategoryModel.class);
        BDDMockito.given(mockStagedCategory3.getCode()).willReturn("Deal123Cat3");
        final List<ProductModel> productsForMockStagedCategory3 = new ArrayList<>();
        BDDMockito.given(mockStagedCategory3.getProducts()).willReturn(productsForMockStagedCategory3);
        BDDMockito.given(categoryService.getCategoryForCode(mockStagedProductCatalog, "Deal123Cat3")).willReturn(
                mockStagedCategory3);

        missingDealProductInfoServiceImpl.assocateMissingProductsWithCategories();

        Mockito.verify(productService, Mockito.never()).getProductForCode(mockStagedProductCatalog, "23456789");

        Mockito.verify(categoryService, Mockito.never()).getCategoryForCode(mockOnlineProductCatalog, "Deal123Cat2");
        Mockito.verify(categoryService, Mockito.never()).getCategoryForCode(mockStagedProductCatalog, "Deal123Cat2");

        final ArgumentCaptor<Collection> categoryCaptor = ArgumentCaptor.forClass(Collection.class);
        BDDMockito.verify(modelService, Mockito.times(2)).saveAll(categoryCaptor.capture());

        final ArgumentCaptor<List> missingDealProductsSuccessfullyLinkedCaptor = ArgumentCaptor.forClass(List.class);
        BDDMockito.verify(modelService).removeAll(missingDealProductsSuccessfullyLinkedCaptor.capture());

        final ArgumentCaptor<List> stagedProductsFromStagedCategory1Captor = ArgumentCaptor.forClass(List.class);
        BDDMockito.verify(mockStagedCategory1).setProducts(stagedProductsFromStagedCategory1Captor.capture());
        final ArgumentCaptor<List> stagedProductsFromStagedCategory3Captor = ArgumentCaptor.forClass(List.class);
        BDDMockito.verify(mockStagedCategory3).setProducts(stagedProductsFromStagedCategory3Captor.capture());
        final ArgumentCaptor<List> onlineProductsFromOnlineCategory1Captor = ArgumentCaptor.forClass(List.class);
        BDDMockito.verify(mockOnlineCategory1).setProducts(onlineProductsFromOnlineCategory1Captor.capture());
        final ArgumentCaptor<List> onlineProductsFromOnlineCategory3Captor = ArgumentCaptor.forClass(List.class);
        BDDMockito.verify(mockOnlineCategory3).setProducts(onlineProductsFromOnlineCategory3Captor.capture());

        final List<Collection> categoryLists = categoryCaptor.getAllValues();
        Assertions.assertThat(categoryLists).isNotEmpty().hasSize(2);

        final Collection stagedCategories = categoryLists.get(0);
        Assertions.assertThat(stagedCategories).containsOnly(mockStagedCategory1, mockStagedCategory3);

        final Collection onlineCategories = categoryLists.get(1);
        Assertions.assertThat(onlineCategories).containsOnly(mockOnlineCategory1, mockOnlineCategory3);

        final List missingDealProductsSuccessfullyLinked = missingDealProductsSuccessfullyLinkedCaptor.getValue();
        Assertions.assertThat(missingDealProductsSuccessfullyLinked).isNotEmpty().hasSize(2)
                .containsOnly(mockMissingDealProductInfo1, mockMissingDealProductInfo3);

        final List stagedProductsFromStagedCategory1 = stagedProductsFromStagedCategory1Captor.getValue();
        Assertions.assertThat(stagedProductsFromStagedCategory1).isNotEmpty().containsOnly(mockStagedProduct1);
        final List stagedProductsFromStagedCategory3 = stagedProductsFromStagedCategory3Captor.getValue();
        Assertions.assertThat(stagedProductsFromStagedCategory3).isNotEmpty().containsOnly(mockStagedProduct3);
        final List onlineProductsFromOnlineCategory1 = onlineProductsFromOnlineCategory1Captor.getValue();
        Assertions.assertThat(onlineProductsFromOnlineCategory1).isNotEmpty().containsOnly(mockOnlineProduct1);
        final List onlineProductsFromOnlineCategory3 = onlineProductsFromOnlineCategory3Captor.getValue();
        Assertions.assertThat(onlineProductsFromOnlineCategory3).isNotEmpty().containsOnly(mockOnlineProduct3);
    }

    @Test
    public void testAssocateMissingProductsWithCategoriesWhenCategoryNotAvailable() {
        final MissingDealProductInfoModel mockMissingDealProductInfo1 = Mockito.mock(MissingDealProductInfoModel.class);
        BDDMockito.given(mockMissingDealProductInfo1.getProductCode()).willReturn("12345678");
        BDDMockito.given(mockMissingDealProductInfo1.getDealCategoryCode()).willReturn("Deal123Cat1");

        missingDealProductInfos.add(mockMissingDealProductInfo1);

        final ProductModel mockOnlineProduct1 = Mockito.mock(ProductModel.class);
        BDDMockito.given(productService.getProductForCode(mockOnlineProductCatalog, "12345678")).willReturn(
                mockOnlineProduct1);
        final ProductModel mockStagedProduct1 = Mockito.mock(ProductModel.class);
        BDDMockito.given(productService.getProductForCode(mockStagedProductCatalog, "12345678")).willReturn(
                mockStagedProduct1);


        BDDMockito.given(categoryService.getCategoryForCode(mockOnlineProductCatalog, "Deal123Cat1")).willThrow(
                new UnknownIdentifierException("Category unavailable"));

        final CategoryModel stagedCategory1 = new SettingGettingCategoryModel();
        final CategoryModel spyStagedCategory1 = Mockito.spy(stagedCategory1);
        BDDMockito.given(spyStagedCategory1.getCode()).willReturn("Deal123Cat1");
        BDDMockito.given(categoryService.getCategoryForCode(mockStagedProductCatalog, "Deal123Cat1")).willReturn(
                spyStagedCategory1);

        missingDealProductInfoServiceImpl.assocateMissingProductsWithCategories();

        final ArgumentCaptor<Collection> categoryCaptor = ArgumentCaptor.forClass(Collection.class);
        BDDMockito.verify(modelService, Mockito.never()).saveAll(categoryCaptor.capture());

    }

    protected class SettingGettingCategoryModel extends CategoryModel {
        private List<ProductModel> products;

        public SettingGettingCategoryModel() {
            products = new ArrayList<>();
        }

        /* (non-Javadoc)
         * @see de.hybris.platform.category.model.CategoryModel#getProducts()
         */
        @Override
        public List<ProductModel> getProducts() {
            return products;
        }

        /* (non-Javadoc)
         * @see de.hybris.platform.category.model.CategoryModel#setProducts(java.util.List)
         */
        @Override
        public void setProducts(final List<ProductModel> products) {
            this.products = products;
        }
    }
}

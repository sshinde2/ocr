package au.com.target.tgtcore.deals.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.deals.dao.TargetDealDao;
import au.com.target.tgtcore.model.TargetDealCategoryModel;


/**
 * Unit test for {@link TargetDealServiceImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetDealServiceImplTest {

    // CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    // CHECKSTYLE:ON

    @InjectMocks
    private final TargetDealServiceImpl targetDealService = new TargetDealServiceImpl();

    @Mock
    private TargetDealDao targetDealDao;

    @Test
    public void testGetDealForNullId() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Deal Id must not be null");
        targetDealService.getDealForId(null);

        Mockito.verifyZeroInteractions(targetDealDao);
    }

    @Test
    public void testGetDealForId() {
        final String dealId = "1234";
        targetDealService.getDealForId(dealId);
        Mockito.verify(targetDealDao).getDealForId(dealId);
    }

    @Test
    public void testGetAllActiveDeals() {
        targetDealService.getAllActiveDeals();
        Mockito.verify(targetDealDao).getAllActiveDeals();
    }

    @Test
    public void testGetDealByCategoryFromReward() {
        final AbstractSimpleDealModel abstractSimpleDealModel = new AbstractSimpleDealModel();
        final TargetDealCategoryModel categoryModel = new TargetDealCategoryModel();
        final List<AbstractSimpleDealModel> abstractSimpleDealModellList = new ArrayList<>();
        abstractSimpleDealModellList.add(abstractSimpleDealModel);
        categoryModel.setDeal(abstractSimpleDealModellList);

        final AbstractSimpleDealModel result = targetDealService.getDealByCategory(categoryModel);
        Assert.assertEquals(abstractSimpleDealModel, result);
    }

    @Test
    public void testGetDealByCategoryFromQualifier() {
        final AbstractSimpleDealModel abstractSimpleDealModel = new AbstractSimpleDealModel();
        final TargetDealCategoryModel categoryModel = new TargetDealCategoryModel();
        final DealQualifierModel qualiferModel = new DealQualifierModel();
        qualiferModel.setDeal(abstractSimpleDealModel);
        final List<DealQualifierModel> qualiferModelList = new ArrayList<>();
        qualiferModelList.add(qualiferModel);
        categoryModel.setQualifier(qualiferModelList);

        final AbstractSimpleDealModel result = targetDealService.getDealByCategory(categoryModel);
        Assert.assertEquals(abstractSimpleDealModel, result);
    }

    @Test
    public void testGetDealByCategoryFromBothQualifierReward() {
        final AbstractSimpleDealModel abstractSimpleDealModel = new AbstractSimpleDealModel();
        final AbstractSimpleDealModel abstractSimpleDealModel2 = new AbstractSimpleDealModel();
        final TargetDealCategoryModel categoryModel = new TargetDealCategoryModel();
        final DealQualifierModel qualiferModel = new DealQualifierModel();
        qualiferModel.setDeal(abstractSimpleDealModel);
        final List<DealQualifierModel> qualiferModelList = new ArrayList<>();
        qualiferModelList.add(qualiferModel);
        categoryModel.setQualifier(qualiferModelList);
        final List<AbstractSimpleDealModel> abstractSimpleDealModellList2 = new ArrayList<>();
        abstractSimpleDealModellList2.add(abstractSimpleDealModel2);
        categoryModel.setDeal(abstractSimpleDealModellList2);

        final AbstractSimpleDealModel result = targetDealService.getDealByCategory(categoryModel);
        Assert.assertEquals(abstractSimpleDealModel, result);
    }


    @Test
    public void testGetDealByCategoryWhenQualifierCategoryAssociatedToAnImmutableDealExists() {
        final AbstractSimpleDealModel abstractSimpleDealModel = Mockito.mock(AbstractSimpleDealModel.class);
        final AbstractSimpleDealModel abstractSimpleDealModelImmutable = Mockito.mock(AbstractSimpleDealModel.class);
        BDDMockito.given(abstractSimpleDealModelImmutable.getImmutableKeyHash()).willReturn("123123123123");
        BDDMockito.given(abstractSimpleDealModel.getImmutableKeyHash()).willReturn(null);
        final TargetDealCategoryModel categoryModel = new TargetDealCategoryModel();
        final DealQualifierModel qualiferModel = new DealQualifierModel();
        final DealQualifierModel qualiferModelOfImmutable = new DealQualifierModel();
        qualiferModel.setDeal(abstractSimpleDealModel);
        qualiferModelOfImmutable.setDeal(abstractSimpleDealModelImmutable);
        final List<DealQualifierModel> qualiferModelList = new ArrayList<>();
        qualiferModelList.add(qualiferModel);
        qualiferModelList.add(qualiferModelOfImmutable);
        categoryModel.setQualifier(qualiferModelList);

        final AbstractSimpleDealModel result = targetDealService.getDealByCategory(categoryModel);
        Assert.assertEquals(abstractSimpleDealModel, result);
    }

    @Test
    public void testGetDealByCategoryWhenRewardCategoryAssociatedToAnImmutableDealExists() {
        final AbstractSimpleDealModel abstractSimpleDealModel = Mockito.mock(AbstractSimpleDealModel.class);
        final AbstractSimpleDealModel abstractSimpleDealModelImmutable = Mockito.mock(AbstractSimpleDealModel.class);
        BDDMockito.given(abstractSimpleDealModelImmutable.getImmutableKeyHash()).willReturn("123123123123");
        BDDMockito.given(abstractSimpleDealModel.getImmutableKeyHash()).willReturn(null);
        final TargetDealCategoryModel categoryModel = new TargetDealCategoryModel();
        categoryModel.setDeal(Arrays.asList(abstractSimpleDealModel, abstractSimpleDealModelImmutable));

        final AbstractSimpleDealModel result = targetDealService.getDealByCategory(categoryModel);
        Assert.assertEquals(abstractSimpleDealModel, result);
    }


    @Test
    public void testGetDealByCategoryFromNeitherQualifierReward() {
        final TargetDealCategoryModel categoryModel = new TargetDealCategoryModel();

        final AbstractSimpleDealModel result = targetDealService.getDealByCategory(categoryModel);
        Assert.assertEquals(null, result);
    }

    @Test
    public void testGetDealByCategoryWithNullCategory() {
        final TargetDealCategoryModel categoryModel = null;
        final AbstractSimpleDealModel result = targetDealService.getDealByCategory(categoryModel);
        Assert.assertEquals(null, result);
    }

}

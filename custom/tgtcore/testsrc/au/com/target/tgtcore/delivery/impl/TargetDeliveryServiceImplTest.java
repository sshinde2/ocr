package au.com.target.tgtcore.delivery.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeValueModel;
import de.hybris.platform.deliveryzone.model.ZoneModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.fest.assertions.Delta;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtauspost.model.ShipsterConfigModel;
import au.com.target.tgtcore.delivery.TargetZoneDeliveryModeDao;
import au.com.target.tgtcore.delivery.TargetZoneDeliveryModeValueDao;
import au.com.target.tgtcore.delivery.TargetZoneDeliveryModeValueRestrictionStrategy;
import au.com.target.tgtcore.delivery.dao.impl.TargetCountryZoneDeliveryModeDao;
import au.com.target.tgtcore.delivery.dto.DeliveryCostDto;
import au.com.target.tgtcore.delivery.dto.DeliveryCostEnumType;
import au.com.target.tgtcore.delivery.dto.DeliveryShipsterStatusCode;
import au.com.target.tgtcore.delivery.impl.TargetDeliveryServiceImpl.ProductQuantity;
import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.PostCodeAwareZoneModel;
import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.order.TargetDiscountService;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtcore.shipster.ShipsterConfigService;
import au.com.target.tgtcore.shipster.dao.ShipsterDao;



@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetDeliveryServiceImplTest {

    @InjectMocks
    private final TargetDeliveryServiceImpl deliveryService = new TargetDeliveryServiceImpl();

    @Mock
    private CommonI18NService commonI18NService;

    @Mock
    private TargetCountryZoneDeliveryModeDao targetCountryZoneDeliveryModeDao;

    @Mock
    private TargetZoneDeliveryModeDao targetZoneDeliveryModeDao;

    @Mock
    private FlexibleSearchService flexibleSearchService;

    @Mock
    private TargetZoneDeliveryModeValueRestrictionStrategy modeValueRestrictionStrategy;

    @Mock
    private AbstractOrderEntryModel abstractOrderEntryModel;

    @Mock
    private AbstractOrderModel abstractOrderModel;

    private AbstractTargetZoneDeliveryModeValueModel targetZoneDeliveryModeValueModel;
    private AbstractTargetZoneDeliveryModeValueModel restrictedZoneDeliveryModeValueModel;

    @Mock
    private TargetDiscountService targetDiscountService;

    @Mock
    private CartModel cartModel;

    @Mock
    private AddressModel address;

    @Mock
    private AddressModel address2;

    @Mock
    private AddressModel address3;

    @Mock
    private CountryModel country;

    @Mock
    private CurrencyModel currency;

    @Mock
    private UserModel user;

    @Mock
    private TargetZoneDeliveryModeValueDao targetZoneDeliveryModeValueDao;

    @Mock
    private TargetSalesApplicationService targetSalesApplicationService;

    @Mock
    private ZoneModel zone;

    @Mock
    private AbstractTargetVariantProductModel variantBulky;

    @Mock
    private AbstractTargetVariantProductModel variantNonBulky;

    @Mock
    private ProductTypeModel ptmBulky;

    @Mock
    private ProductTypeModel ptmNonBulky;

    @Mock
    private ShipsterDao shipsterDao;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private ShipsterConfigService shipsterConfigService;

    private DeliveryModeModel deliveryModeModel;
    private ZoneDeliveryModeModel zoneDeliveryModeModel;
    private TargetZoneDeliveryModeModel deliveryModeModel1;
    private TargetZoneDeliveryModeModel deliveryModeModel2;
    private TargetZoneDeliveryModeModel deliveryModeModel3;

    private AbstractTargetZoneDeliveryModeValueModel deliveryModeValueModel;

    private PostCodeModel postCode1;
    private PostCodeModel postCode2;

    private AbstractTargetZoneDeliveryModeValueModel zoneDeliveryModeValue1;
    private AbstractTargetZoneDeliveryModeValueModel zoneDeliveryModeValue2;

    private AbstractTargetZoneDeliveryModeValueModel restrictedZoneDeliveryModeValue1;
    private AbstractTargetZoneDeliveryModeValueModel restrictedZoneDeliveryModeValue2;

    private PostCodeAwareZoneModel postCodeAwareZone;
    private ZoneModel nonPostCoeAwareZone;

    @Mock
    private ModelService modelService;

    @Before
    public void setup() {

        deliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        // Bt default setup the cart with valid address and currency
        given(cartModel.getCurrency()).willReturn(currency);
        given(cartModel.getDeliveryAddress()).willReturn(address);
        given(address.getCountry()).willReturn(country);

        // Set up base store for when there is no currency, country in the cart
        final BaseStoreModel baseStore = mock(BaseStoreModel.class);

        given(flexibleSearchService.getModelsByExample(any(BaseStoreModel.class))).willReturn(
                Collections.singletonList(baseStore));
        given(baseStore.getCurrencies()).willReturn(Collections.singleton(currency));
        given(baseStore.getDeliveryCountries()).willReturn(Collections.singleton(country));

        // Variant products, a bulky and a non-bulky
        given(variantBulky.getProductType()).willReturn(ptmBulky);
        given(variantNonBulky.getProductType()).willReturn(ptmNonBulky);
        given(variantBulky.getProductType().getBulky()).willReturn(Boolean.TRUE);
        given(variantNonBulky.getProductType().getBulky()).willReturn(Boolean.FALSE);

        // Set up restrictableTargetZoneDeliveryModeValueModel values
        restrictedZoneDeliveryModeValueModel = mock(RestrictableTargetZoneDeliveryModeValueModel.class);
        given(((RestrictableTargetZoneDeliveryModeValueModel)restrictedZoneDeliveryModeValueModel).getPriority())
                .willReturn(Integer.valueOf(1000));
        // Set up targetZoneDeliveryModeValueModel values
        targetZoneDeliveryModeValueModel = mock(TargetZoneDeliveryModeValueModel.class);
        given(((TargetZoneDeliveryModeValueModel)targetZoneDeliveryModeValueModel).getExtendedBulky()).willReturn(
                Double.valueOf(98.0));
        given(((TargetZoneDeliveryModeValueModel)targetZoneDeliveryModeValueModel).getBulky()).willReturn(
                Double.valueOf(49.0));
        given(targetZoneDeliveryModeValueModel.getValue()).willReturn(Double.valueOf(9.0));
        given(((TargetZoneDeliveryModeValueModel)targetZoneDeliveryModeValueModel).getActive())
                .willReturn(Boolean.TRUE);

    }

    private void setupMultipleDeliveryModesAttachedToSalesApplication() {
        deliveryModeModel1 = mock(TargetZoneDeliveryModeModel.class);
        deliveryModeModel2 = mock(TargetZoneDeliveryModeModel.class);
        deliveryModeModel3 = mock(TargetZoneDeliveryModeModel.class);

        final List<TargetZoneDeliveryModeModel> deliveryModesForSalesAppWeb = new ArrayList<>();
        deliveryModesForSalesAppWeb.add(deliveryModeModel1);
        deliveryModesForSalesAppWeb.add(deliveryModeModel2);

        final List<TargetZoneDeliveryModeModel> deliveryModesForSalesAppEbay = new ArrayList<>();
        deliveryModesForSalesAppEbay.add(deliveryModeModel3);

        final List<TargetZoneDeliveryModeModel> allDeliveryModes = new ArrayList<>();
        allDeliveryModes.add(deliveryModeModel1);
        allDeliveryModes.add(deliveryModeModel2);
        allDeliveryModes.add(deliveryModeModel3);

        given(targetZoneDeliveryModeDao.findAllCurrentlyActiveDeliveryModes(SalesApplication.WEB)).willReturn(
                deliveryModesForSalesAppWeb);
        given(targetZoneDeliveryModeDao.findAllCurrentlyActiveDeliveryModes(SalesApplication.EBAY)).willReturn(
                deliveryModesForSalesAppEbay);
        given(targetZoneDeliveryModeDao.findAllCurrentlyActiveDeliveryModes(null)).willReturn(allDeliveryModes);
    }

    private void setupForSupportedDeliveryMode() {

        // Setup so the dao will find one delivery mode
        given(targetCountryZoneDeliveryModeDao.findDeliveryModes(cartModel))
                .willReturn(Collections.singletonList(deliveryModeModel));
    }

    private void setupDeliveryModeAndValue() {

        zoneDeliveryModeModel = (ZoneDeliveryModeModel)deliveryModeModel;
        deliveryModeValueModel = mock(TargetZoneDeliveryModeValueModel.class);
        given(zoneDeliveryModeModel.getValues())
                .willReturn(Collections.singleton((ZoneDeliveryModeValueModel)deliveryModeValueModel));
        given(deliveryModeValueModel.getCurrency()).willReturn(currency);
        given(deliveryModeValueModel.getZone()).willReturn(zone);
        given(((TargetZoneDeliveryModeValueModel)deliveryModeValueModel).getActive()).willReturn(Boolean.TRUE);
        given(zone.getCountries()).willReturn(Collections.singleton(country));
        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel))
                .willReturn(Collections.singletonList(deliveryModeValueModel));
        given(deliveryService.getSupportedDeliveryModeListForOrder(abstractOrderModel))
                .willReturn(Collections.singletonList(deliveryModeModel));

    }

    @SuppressWarnings("boxing")
    private void setupRestrictableDeliveryModeAndValue() {
        zoneDeliveryModeModel = (ZoneDeliveryModeModel)deliveryModeModel;
        deliveryModeValueModel = mock(RestrictableTargetZoneDeliveryModeValueModel.class);
        given(zoneDeliveryModeModel.getValues())
                .willReturn(Collections.singleton((ZoneDeliveryModeValueModel)deliveryModeValueModel));
        given(deliveryModeValueModel.getCurrency()).willReturn(currency);
        given(deliveryModeValueModel.getZone()).willReturn(zone);
        given(zone.getCountries()).willReturn(Collections.singleton(country));
        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel))
                .willReturn(Collections.singletonList(deliveryModeValueModel));
        given(
                modeValueRestrictionStrategy.isApplicableModeValue(
                        (RestrictableTargetZoneDeliveryModeValueModel)deliveryModeValueModel,
                        cartModel)).willReturn(true);
    }

    public void setupZoneDeliveryModeAndPostCodes() {
        postCode1 = mock(PostCodeModel.class);
        given(postCode1.getPostCode()).willReturn("0200");

        postCode2 = mock(PostCodeModel.class);
        given(postCode2.getPostCode()).willReturn("3000");

        final List<PostCodeModel> postCodes = new ArrayList<>();
        postCodes.add(postCode1);
        postCodes.add(postCode2);

        postCodeAwareZone = mock(PostCodeAwareZoneModel.class);
        given(postCodeAwareZone.getPostCodes()).willReturn(postCodes);

        nonPostCoeAwareZone = mock(ZoneModel.class);

        zoneDeliveryModeValue1 = mock(TargetZoneDeliveryModeValueModel.class);
        given(zoneDeliveryModeValue1.getZone()).willReturn(nonPostCoeAwareZone);
        given(((TargetZoneDeliveryModeValueModel)zoneDeliveryModeValue1).getActive()).willReturn(Boolean.TRUE);

        zoneDeliveryModeValue2 = mock(TargetZoneDeliveryModeValueModel.class);
        given(zoneDeliveryModeValue2.getZone()).willReturn(postCodeAwareZone);
        given(((TargetZoneDeliveryModeValueModel)zoneDeliveryModeValue2).getActive()).willReturn(Boolean.TRUE);
    }

    public void setupZoneDeliveryModeAndPostCodesWithRestrict() {
        postCode1 = mock(PostCodeModel.class);
        given(postCode1.getPostCode()).willReturn("0200");

        postCode2 = mock(PostCodeModel.class);
        given(postCode2.getPostCode()).willReturn("3000");

        final List<PostCodeModel> postCodes = new ArrayList<>();
        postCodes.add(postCode1);
        postCodes.add(postCode2);

        postCodeAwareZone = mock(PostCodeAwareZoneModel.class);
        given(postCodeAwareZone.getPostCodes()).willReturn(postCodes);

        nonPostCoeAwareZone = mock(ZoneModel.class);

        restrictedZoneDeliveryModeValue1 = mock(RestrictableTargetZoneDeliveryModeValueModel.class);
        willReturn(Boolean.TRUE).given(modeValueRestrictionStrategy).isApplicableModeValue(
                (RestrictableTargetZoneDeliveryModeValueModel)restrictedZoneDeliveryModeValueModel, cartModel);

        restrictedZoneDeliveryModeValue2 = mock(RestrictableTargetZoneDeliveryModeValueModel.class);
        willReturn(Boolean.TRUE).given(modeValueRestrictionStrategy).isApplicableModeValue(
                (RestrictableTargetZoneDeliveryModeValueModel)restrictedZoneDeliveryModeValue2, cartModel);

    }

    @Test
    public void testGetSupportedDeliveryModesEmpty() {

        final List<DeliveryModeModel> deliveryModes = deliveryService
                .getSupportedDeliveryModeListForOrder(cartModel);

        assertThat(deliveryModes).isEmpty();
    }

    @Test
    public void testGetSupportedDeliveryModesFilteredWithValue() {

        setupForSupportedDeliveryMode();
        setupDeliveryModeAndValue();

        given(
                targetZoneDeliveryModeValueDao
                        .findDeliveryValuesSortedByPriority((ZoneDeliveryModeModel)deliveryModeModel))
                                .willReturn(Collections.singletonList(targetZoneDeliveryModeValueModel));

        final List<DeliveryModeModel> deliveryModes = deliveryService
                .getSupportedDeliveryModeListForOrder(cartModel);

        verify(cartModel).getDeliveryAddress();
        verify(cartModel, times(2)).getCurrency();
        assertThat(deliveryModeModel).isEqualTo(deliveryModes.iterator().next());
    }

    @Test
    public void testGetSupportedDeliveryModesFilteredWithNoValue() {

        setupForSupportedDeliveryMode();

        final List<DeliveryModeModel> deliveryModes = deliveryService
                .getSupportedDeliveryModeListForOrder(cartModel);

        verify(cartModel).getDeliveryAddress();
        verify(cartModel).getCurrency();
        assertThat(deliveryModes).isEmpty();
    }

    @Test
    public void testGetSupportedDeliveryModesNoOrderCountryOrCurrency() {

        setupForSupportedDeliveryMode();
        // Test case of not having currency or delivery address in cart
        given(cartModel.getCurrency()).willReturn(null);
        given(cartModel.getDeliveryAddress()).willReturn(null);

        final List<DeliveryModeModel> deliveryModes = deliveryService
                .getSupportedDeliveryModeListForOrder(cartModel);

        verify(cartModel).getDeliveryAddress();
        verify(cartModel).getCurrency();
        assertThat(deliveryModes).isEmpty();
    }

    @Test
    public void testgetZoneDeliveryModeValueForAbstractOrderAndModeWithoutZDMV() {

        setupDeliveryModeAndValue();

        final ZoneDeliveryModeValueModel modeValue = deliveryService
                .getZoneDeliveryModeValueForAbstractOrderAndMode(zoneDeliveryModeModel, cartModel);
        verify(cartModel).getDeliveryAddress();
        verify(cartModel, times(2)).getCurrency();
        assertThat(deliveryModeValueModel).isEqualTo(modeValue);
    }

    @Test
    public void testgetZoneDeliveryModeValueForAbstractOrderAndModeWithNonMatchingZDMV() {

        setupDeliveryModeAndValue();

        // The non-matching value in the order has a different mode
        given(cartModel.getZoneDeliveryModeValue()).willReturn(deliveryModeValueModel);
        final ZoneDeliveryModeModel zoneDeliveryModeModel2 = mock(TargetZoneDeliveryModeModel.class);
        given(deliveryModeValueModel.getDeliveryMode()).willReturn(zoneDeliveryModeModel2);

        final ZoneDeliveryModeValueModel modeValue = deliveryService
                .getZoneDeliveryModeValueForAbstractOrderAndMode(zoneDeliveryModeModel, cartModel);
        verify(cartModel).getDeliveryAddress();
        verify(cartModel, times(2)).getCurrency();
        assertThat(deliveryModeValueModel).isEqualTo(modeValue);
    }

    @Test
    public void testgetZoneDeliveryModeValueForAbstractOrderAndModeWithMatchingZDMV() {

        setupDeliveryModeAndValue();

        // Value in the order matches the mode
        given(deliveryModeValueModel.getDeliveryMode()).willReturn(zoneDeliveryModeModel);
        given(cartModel.getZoneDeliveryModeValue()).willReturn(deliveryModeValueModel);

        final ZoneDeliveryModeValueModel modeValue = deliveryService
                .getZoneDeliveryModeValueForAbstractOrderAndMode(zoneDeliveryModeModel, cartModel);

        // The matching value should come straight from the order so no search through the mode values will occur
        verify(zoneDeliveryModeModel, never()).getValues();
        assertThat(deliveryModeValueModel).isEqualTo(modeValue);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetZoneDeliveryModeValueForNullAbstractOrder() {

        deliveryService.getZoneDeliveryModeValueForAbstractOrderAndMode(zoneDeliveryModeModel, null);
        fail();
    }

    @Test
    public void testgetZoneDeliveryModeValueForAbstractOrderAndModeForCancelWithoutZDMV() {

        setupDeliveryModeAndValue();

        final ZoneDeliveryModeValueModel modeValue = deliveryService
                .getZoneDeliveryModeValueForAbstractOrderAndMode(zoneDeliveryModeModel, cartModel);
        verify(cartModel).getDeliveryAddress();
        verify(cartModel, times(2)).getCurrency();
        assertThat(deliveryModeValueModel).isEqualTo(modeValue);
    }

    @Test
    public void testgetZoneDeliveryModeValueForAbstractOrderAndModeForCancelWithNonMatchingZDMV() {

        setupRestrictableDeliveryModeAndValue();

        // The non-matching value in the order has a different mode
        given(cartModel.getZoneDeliveryModeValue()).willReturn(deliveryModeValueModel);
        final ZoneDeliveryModeModel zoneDeliveryModeModel2 = mock(TargetZoneDeliveryModeModel.class);
        given(deliveryModeValueModel.getDeliveryMode()).willReturn(zoneDeliveryModeModel2);

        deliveryService.getZoneDeliveryModeValueForAbstractOrderAndMode(zoneDeliveryModeModel, cartModel);
        verify(cartModel).getDeliveryAddress();
        verify(cartModel, times(2)).getCurrency();
    }

    @Test
    public void testGetSupportedDeliveryModeValuesEmpty() {

        // No supported modes hence no values
        final Collection<ZoneDeliveryModeValueModel> deliveryModeValues = deliveryService
                .getSupportedDeliveryModeValuesForOrder(cartModel);

        assertThat(deliveryModeValues).isEmpty();
    }

    @Test
    public void testGetSupportedDeliveryModeValues() {

        // Setup so one delivery mode is returned with one associated value
        // No value is stored in the order

        setupForSupportedDeliveryMode();
        setupDeliveryModeAndValue();
        final Collection<ZoneDeliveryModeValueModel> deliveryModeValues = deliveryService
                .getSupportedDeliveryModeValuesForOrder(cartModel);

        assertThat(deliveryModeValues).isNotNull();
        assertThat(deliveryModeValues).hasSize(1);
        assertThat(deliveryModeValueModel).isEqualTo(deliveryModeValues.iterator().next());
    }

    @Test
    public void testGetSupportedDeliveryModeValuesWithOneInOrder() {

        setupForSupportedDeliveryMode();
        setupDeliveryModeAndValue();

        // A value is stored in the order
        final TargetZoneDeliveryModeValueModel snapshotZoneDeliveryModeValueModel = mock(
                TargetZoneDeliveryModeValueModel.class);
        given(cartModel.getZoneDeliveryModeValue()).willReturn(snapshotZoneDeliveryModeValueModel);
        given(snapshotZoneDeliveryModeValueModel.getDeliveryMode()).willReturn(zoneDeliveryModeModel);

        final Collection<ZoneDeliveryModeValueModel> deliveryModeValues = deliveryService
                .getSupportedDeliveryModeValuesForOrder(cartModel);

        assertThat(deliveryModeValues).isNotNull();
        assertThat(deliveryModeValues).hasSize(1);
    }

    @Test
    public void testGetSupportedDeliveryModeValuesWithOneNonMatchingInOrder() {

        // Assume the non-matching value should not come through in the list, since the delivery mode is not in the supported list
        setupForSupportedDeliveryMode();
        setupDeliveryModeAndValue();

        // A value is stored in the order - delivery mode is different
        final TargetZoneDeliveryModeValueModel snapshotZoneDeliveryModeValueModel = mock(
                TargetZoneDeliveryModeValueModel.class);
        given(cartModel.getZoneDeliveryModeValue()).willReturn(snapshotZoneDeliveryModeValueModel);

        final ZoneDeliveryModeModel zoneDeliveryModeModel2 = mock(TargetZoneDeliveryModeModel.class);
        given(snapshotZoneDeliveryModeValueModel.getDeliveryMode()).willReturn(zoneDeliveryModeModel2);
        final Collection<ZoneDeliveryModeValueModel> deliveryModeValues = deliveryService
                .getSupportedDeliveryModeValuesForOrder(cartModel);

        assertThat(deliveryModeValues).isNotNull();
        assertThat(deliveryModeValues).hasSize(1);

        // The non-snapshot value should be retrieved
        assertThat(deliveryModeValueModel).isEqualTo(deliveryModeValues.iterator().next());
    }

    @Test
    public void testGetStatesForNullCountry() {
        // All states is a list with just one in it, which will be returned
        final RegionModel state = mock(RegionModel.class);
        given(commonI18NService.getAllRegions()).willReturn(Collections.singletonList(state));
        final List<RegionModel> states = deliveryService.getRegionsForCountry(null);

        assertThat(states).hasSize(1);
        assertThat(state).isEqualTo(states.get(0));

    }

    @Test
    public void testGetStatesForGivenCountry() {
        // All states is a list with just one in it
        final RegionModel state = mock(RegionModel.class);
        given(commonI18NService.getAllRegions()).willReturn(Collections.singletonList(state));

        final CountryModel aus = mock(CountryModel.class);
        final CountryModel nz = mock(CountryModel.class);

        // country for state is aus so result is retained
        given(state.getCountry()).willReturn(aus);
        List<RegionModel> states = deliveryService.getRegionsForCountry(aus);
        assertThat(states).hasSize(1);
        assertThat(state).isEqualTo(states.get(0));

        // country for state is nz so it will be filtered out
        given(state.getCountry()).willReturn(nz);
        states = deliveryService.getRegionsForCountry(aus);
        assertThat(states).isEmpty();
    }

    @Test
    public void testGetVariantsDelModesFromBaseProduct() {
        final VariantProductModel variantProduct = Mockito.mock(VariantProductModel.class);
        final ProductModel baseProduct = Mockito.mock(ProductModel.class);
        final Set<DeliveryModeModel> prodDeliveryModes = new HashSet<>();
        prodDeliveryModes.add(deliveryModeModel2);
        given(variantProduct.getBaseProduct()).willReturn(baseProduct);
        given(baseProduct.getDeliveryModes()).willReturn(prodDeliveryModes);

        final Set<DeliveryModeModel> set = deliveryService.getVariantsDeliveryModesFromBaseProduct(variantProduct);

        assertThat(set).hasSize(1);
    }

    @Test
    public void testGetVariantsDelModesFromPreOrderProduct() {
        final TargetColourVariantProductModel preOrderProduct = Mockito.mock(TargetColourVariantProductModel.class);
        final ProductModel baseProduct = Mockito.mock(ProductModel.class);
        final Set<DeliveryModeModel> prodDeliveryModes = setupDeliveryModeForPreOrder();

        final Calendar start = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), Calendar.FEBRUARY, 15, 0, 0, 0);

        final Calendar end = Calendar.getInstance();
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 2, 22, 0, 0, 0);

        given(preOrderProduct.getPreOrderEndDateTime()).willReturn(end.getTime());
        given(preOrderProduct.getPreOrderStartDateTime()).willReturn(start.getTime());

        given(preOrderProduct.getBaseProduct()).willReturn(baseProduct);
        given(baseProduct.getDeliveryModes()).willReturn(prodDeliveryModes);

        final Set<DeliveryModeModel> set = deliveryService.getVariantsDeliveryModesFromBaseProduct(preOrderProduct);

        assertThat(set).isNotEmpty().hasSize(1).contains(deliveryModeModel3);
    }

    /**
     * This will set the express delivery mode for pre-order product
     * 
     * @return prodDeliveryModes
     */
    private Set<DeliveryModeModel> setupDeliveryModeForPreOrder() {
        final Set<DeliveryModeModel> prodDeliveryModes = new HashSet<>();

        deliveryModeModel1 = mock(TargetZoneDeliveryModeModel.class);
        deliveryModeModel2 = mock(TargetZoneDeliveryModeModel.class);
        deliveryModeModel3 = mock(TargetZoneDeliveryModeModel.class);

        willReturn(Boolean.TRUE).given(deliveryModeModel3).isAvailableForPreOrder();

        prodDeliveryModes.add(deliveryModeModel1);
        prodDeliveryModes.add(deliveryModeModel2);
        prodDeliveryModes.add(deliveryModeModel3);
        return prodDeliveryModes;
    }

    @Test
    public void testGetVariantsDelModesFromBaseProductNullPreOrderProduct() {
        final VariantProductModel variantProduct = Mockito.mock(VariantProductModel.class);
        given(variantProduct.getBaseProduct()).willReturn(null);

        final Set<DeliveryModeModel> set = deliveryService.getVariantsDeliveryModesFromBaseProduct(variantProduct);

        assertThat(set).isEmpty();
    }

    @Test
    public void testGetVariantsDelModesFromBaseProductNullBaseProduct() {
        final VariantProductModel variantProduct = Mockito.mock(VariantProductModel.class);
        given(variantProduct.getBaseProduct()).willReturn(null);

        final Set<DeliveryModeModel> set = deliveryService.getVariantsDeliveryModesFromBaseProduct(variantProduct);

        assertThat(set).isEmpty();
    }

    @Test
    public void testGetBulkyQuantityWithBulky() {
        final List<ProductQuantity> prodQuantities = new ArrayList<>();

        final TargetDeliveryServiceImpl.ProductQuantity prodQuantity1 = deliveryService.new ProductQuantity(
                variantBulky, 2);
        prodQuantities.add(prodQuantity1);

        final TargetDeliveryServiceImpl.ProductQuantity prodQuantity2 = deliveryService.new ProductQuantity(
                variantNonBulky, 3);
        prodQuantities.add(prodQuantity2);

        final int qty = deliveryService.getBulkyQuantity(prodQuantities);
        assertThat(qty).isEqualTo(2);
    }

    @Test
    public void testGetBulkyQuantityWithNoBulky() {
        final List<ProductQuantity> prodQuantities = new ArrayList<>();
        final TargetDeliveryServiceImpl.ProductQuantity prodQuantity = deliveryService.new ProductQuantity(
                variantNonBulky, 2);
        prodQuantities.add(prodQuantity);

        final int qty = deliveryService.getBulkyQuantity(prodQuantities);
        assertThat(qty).isEqualTo(0);
    }

    @Test
    public void testDeliveryCostGivenZeroBulky() {
        final double cost = deliveryService.getDeliveryCostGivenBulkyQuantity(
                (TargetZoneDeliveryModeValueModel)targetZoneDeliveryModeValueModel, 0);
        assertThat(cost).isEqualTo(9.0, Delta.delta(0.0));
    }

    @Test
    public void testDeliveryCostGivenOneBulky() {
        final double cost = deliveryService.getDeliveryCostGivenBulkyQuantity(
                (TargetZoneDeliveryModeValueModel)targetZoneDeliveryModeValueModel, 1);
        assertThat(cost).isEqualTo(49.0, Delta.delta(0.0));
    }

    @Test
    public void testDeliveryCostGivenTwoBulky() {
        final double cost = deliveryService.getDeliveryCostGivenBulkyQuantity(
                (TargetZoneDeliveryModeValueModel)targetZoneDeliveryModeValueModel, 2);
        assertThat(cost).isEqualTo(49.0, Delta.delta(0.0));
    }

    @Test
    public void testDeliveryCostGivenThreeBulky() {
        final double cost = deliveryService.getDeliveryCostGivenBulkyQuantity(
                (TargetZoneDeliveryModeValueModel)targetZoneDeliveryModeValueModel, 3);
        assertThat(cost).isEqualTo(98.0, Delta.delta(0.0));
    }

    @Test
    public void testGetDeliveryCostForDeliveryTypeExtendedBulky() {

        final List<AbstractOrderEntryModel> entries = Collections.singletonList(abstractOrderEntryModel);
        given(abstractOrderModel.getEntries()).willReturn(entries);
        given(abstractOrderEntryModel.getProduct()).willReturn(variantBulky);
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(3L));

        final double result = deliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel,
                abstractOrderModel).getDeliveryCost();
        assertThat(result).isEqualTo(98.0, Delta.delta(0.0));

    }

    @Test
    public void testGetDeliveryCostForDeliveryTypeBulky() {

        final List<AbstractOrderEntryModel> entries = Collections.singletonList(abstractOrderEntryModel);
        given(abstractOrderModel.getEntries()).willReturn(entries);
        given(abstractOrderEntryModel.getProduct()).willReturn(variantBulky);
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(2L));

        final double result = deliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel,

        abstractOrderModel).getDeliveryCost();
        assertThat(result).isEqualTo(49.0, Delta.delta(0.0));

    }

    @Test
    public void testGetDeliveryCostForDeliveryTypeNormal() {

        final List<AbstractOrderEntryModel> entries = Collections.singletonList(abstractOrderEntryModel);
        given(abstractOrderModel.getEntries()).willReturn(entries);
        given(abstractOrderEntryModel.getProduct()).willReturn(variantNonBulky);
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(2L));

        final double result = deliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel,
                abstractOrderModel).getDeliveryCost();
        assertThat(result).isEqualTo(9.0, Delta.delta(0.0));

    }

    @Test
    public void testGetDeliveryCostForEmptyOrder() {

        given(abstractOrderModel.getEntries()).willReturn(Collections.EMPTY_LIST);

        final double result = deliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel,

        abstractOrderModel).getDeliveryCost();
        assertThat(result).isEqualTo(0.0, Delta.delta(0.0));
    }

    @Test
    public void testGetDeliveryCostForDeliveryTypeExtendedBulkyWithCockpitFreeDelivery() {
        final List<AbstractOrderEntryModel> entries = Collections.singletonList(abstractOrderEntryModel);
        given(abstractOrderEntryModel.getProduct()).willReturn(variantBulky);
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(3L));

        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getEntries()).willReturn(entries);
        given(mockCartModel.getCockpitFreeDelivery()).willReturn(Boolean.TRUE);

        final double result = deliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel,
                mockCartModel).getDeliveryCost();
        assertThat(result).isEqualTo(0.0, Delta.delta(0.0));
    }

    @Test
    public void testGetDeliveryCostForDeliveryTypeBulkyWithCockpitFreeDelivery() {
        final List<AbstractOrderEntryModel> entries = Collections.singletonList(abstractOrderEntryModel);
        given(abstractOrderEntryModel.getProduct()).willReturn(variantBulky);
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(2L));

        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getEntries()).willReturn(entries);
        given(mockCartModel.getCockpitFreeDelivery()).willReturn(Boolean.TRUE);

        final double result = deliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel,
                mockCartModel).getDeliveryCost();
        assertThat(result).isEqualTo(0.0, Delta.delta(0.0));

    }

    @Test
    public void testGetDeliveryCostForDeliveryTypeNormalWithCockpitFreeDelivery() {
        final List<AbstractOrderEntryModel> entries = Collections.singletonList(abstractOrderEntryModel);
        given(abstractOrderEntryModel.getProduct()).willReturn(variantNonBulky);
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(2L));

        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getEntries()).willReturn(entries);
        given(mockCartModel.getCockpitFreeDelivery()).willReturn(Boolean.TRUE);

        final double result = deliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel,
                mockCartModel).getDeliveryCost();
        assertThat(result).isEqualTo(0.0, Delta.delta(0.0));

    }

    @Test
    public void testGetDeliveryCostForEmptyOrderWithCockpitFreeDelivery() {
        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getEntries()).willReturn(Collections.EMPTY_LIST);
        given(mockCartModel.getCockpitFreeDelivery()).willReturn(Boolean.TRUE);

        final double result = deliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel,
                mockCartModel).getDeliveryCost();
        assertThat(result).isEqualTo(0.0, Delta.delta(0.0));

    }

    @Test
    public void testGetDeliveryCostForDeliveryTypeWithShipster() {
        final List<AbstractOrderEntryModel> entries = Collections.singletonList(abstractOrderEntryModel);
        given(abstractOrderEntryModel.getProduct()).willReturn(variantNonBulky);
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(2L));
        given(targetZoneDeliveryModeValueModel.getValue()).willReturn(Double.valueOf(20d));
        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled("shipster.available"))).willReturn(
                Boolean.TRUE);
        final TargetZoneDeliveryModeModel mockDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        given(targetZoneDeliveryModeValueModel.getDeliveryMode()).willReturn(mockDeliveryModeModel);
        final ShipsterConfigModel shipsterConfigModel = mock(ShipsterConfigModel.class);
        given(shipsterConfigModel.getActive()).willReturn(Boolean.TRUE);
        given(shipsterConfigModel.getMaxShippingThresholdValue()).willReturn(Double.valueOf(20.0));
        given(shipsterConfigModel.getMinOrderTotalValue()).willReturn(Double.valueOf(25.0));
        given(shipsterConfigService.getShipsterModelForDeliveryModes(mockDeliveryModeModel)).willReturn(
                shipsterConfigModel);
        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getEntries()).willReturn(entries);
        given(mockCartModel.getAusPostClubMember()).willReturn(Boolean.TRUE);
        given(mockCartModel.getSubtotal()).willReturn(Double.valueOf(25d));
        given(mockCartModel.getTotalDiscounts()).willReturn(Double.valueOf(0d));

        final DeliveryCostDto result = deliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel,
                mockCartModel);
        assertThat(result.getDeliveryCost()).isEqualTo(0);
        assertThat(result.getType()).isEqualTo(DeliveryCostEnumType.SHIPSTERFREEDELIVERY);
        assertThat(result.getOriginalDeliveryCost()).isEqualTo(20d);
    }

    @Test
    public void testGetDeliveryCostForDeliveryTypeWithShipsterWithDelvieryCostOverThreshold() {
        final List<AbstractOrderEntryModel> entries = Collections.singletonList(abstractOrderEntryModel);
        given(abstractOrderEntryModel.getProduct()).willReturn(variantNonBulky);
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(2L));
        //setup the delivery cost
        given(targetZoneDeliveryModeValueModel.getValue()).willReturn(Double.valueOf(21d));
        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled("shipster.available"))).willReturn(
                Boolean.TRUE);
        final TargetZoneDeliveryModeModel mockDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        given(targetZoneDeliveryModeValueModel.getDeliveryMode()).willReturn(mockDeliveryModeModel);
        final ShipsterConfigModel shipsterConfigModel = mock(ShipsterConfigModel.class);
        given(shipsterConfigModel.getActive()).willReturn(Boolean.TRUE);
        given(shipsterConfigModel.getMaxShippingThresholdValue()).willReturn(Double.valueOf(20));
        given(shipsterConfigModel.getMinOrderTotalValue()).willReturn(Double.valueOf(25));
        given(shipsterConfigService.getShipsterModelForDeliveryModes(mockDeliveryModeModel)).willReturn(
                shipsterConfigModel);
        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getEntries()).willReturn(entries);
        given(mockCartModel.getAusPostClubMember()).willReturn(Boolean.TRUE);
        given(mockCartModel.getSubtotal()).willReturn(Double.valueOf(30d));
        given(mockCartModel.getTotalDiscounts()).willReturn(Double.valueOf(4d));

        final DeliveryCostDto result = deliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel,
                mockCartModel);
        assertThat(result.getDeliveryCost()).isEqualTo(21);
        assertThat(result.getType()).isEqualTo(DeliveryCostEnumType.NORMAL);
    }

    @Test
    public void testGetDeliveryCostForDeliveryTypeWithShipsterWithCartTotalLessThanThreshold() {
        final List<AbstractOrderEntryModel> entries = Collections.singletonList(abstractOrderEntryModel);
        given(abstractOrderEntryModel.getProduct()).willReturn(variantNonBulky);
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(2L));
        given(targetZoneDeliveryModeValueModel.getValue()).willReturn(Double.valueOf(15d));
        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled("shipster.available"))).willReturn(
                Boolean.TRUE);
        final TargetZoneDeliveryModeModel mockDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        given(targetZoneDeliveryModeValueModel.getDeliveryMode()).willReturn(mockDeliveryModeModel);
        final ShipsterConfigModel shipsterConfigModel = mock(ShipsterConfigModel.class);
        given(shipsterConfigModel.getActive()).willReturn(Boolean.TRUE);
        given(shipsterConfigModel.getMaxShippingThresholdValue()).willReturn(Double.valueOf(20));
        given(shipsterConfigModel.getMinOrderTotalValue()).willReturn(Double.valueOf(25));
        given(shipsterConfigService.getShipsterModelForDeliveryModes(mockDeliveryModeModel)).willReturn(
                shipsterConfigModel);
        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getEntries()).willReturn(entries);
        given(mockCartModel.getAusPostClubMember()).willReturn(Boolean.TRUE);
        // cart total
        given(mockCartModel.getSubtotal()).willReturn(Double.valueOf(30d));
        given(mockCartModel.getTotalDiscounts()).willReturn(Double.valueOf(6d));

        final DeliveryCostDto result = deliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel,
                mockCartModel);
        assertThat(result.getDeliveryCost()).isEqualTo(15);
        assertThat(result.getType()).isEqualTo(DeliveryCostEnumType.NORMAL);
        assertThat(result.getShipsterStatusCode()).isEqualTo(DeliveryShipsterStatusCode.ERR_SHIPSTER_LOW_VALUE);
    }

    @Test
    public void testGetDeliveryCostForDeliveryTypeWithShipsterWithDeliveryClubNotActive() {
        final List<AbstractOrderEntryModel> entries = Collections.singletonList(abstractOrderEntryModel);
        given(abstractOrderEntryModel.getProduct()).willReturn(variantNonBulky);
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(2L));
        given(targetZoneDeliveryModeValueModel.getValue()).willReturn(Double.valueOf(15d));
        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled("shipster.available"))).willReturn(
                Boolean.TRUE);
        final TargetZoneDeliveryModeModel mockDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        given(targetZoneDeliveryModeValueModel.getDeliveryMode()).willReturn(mockDeliveryModeModel);
        final ShipsterConfigModel shipsterConfigModel = mock(ShipsterConfigModel.class);
        //disable
        given(shipsterConfigModel.getActive()).willReturn(Boolean.FALSE);
        given(shipsterConfigModel.getMaxShippingThresholdValue()).willReturn(Double.valueOf(20.0));
        given(shipsterConfigModel.getMinOrderTotalValue()).willReturn(Double.valueOf(25.0));
        given(shipsterConfigService.getShipsterModelForDeliveryModes(mockDeliveryModeModel)).willReturn(
                shipsterConfigModel);
        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getEntries()).willReturn(entries);
        given(mockCartModel.getAusPostClubMember()).willReturn(Boolean.TRUE);
        given(mockCartModel.getSubtotal()).willReturn(Double.valueOf(30d));
        given(mockCartModel.getTotalDiscounts()).willReturn(Double.valueOf(4d));

        final DeliveryCostDto result = deliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel,
                mockCartModel);
        assertThat(result.getDeliveryCost()).isEqualTo(15);
        assertThat(result.getType()).isEqualTo(DeliveryCostEnumType.NORMAL);
        assertThat(result.getShipsterStatusCode()).isEqualTo(DeliveryShipsterStatusCode.ERR_SHIPSTER_NOT_CONFIGURED);
    }

    @Test
    public void testGetDeliveryCostForDeliveryTypeWithShipsterWithNoDeliveryClubModelFound() {
        final List<AbstractOrderEntryModel> entries = Collections.singletonList(abstractOrderEntryModel);
        given(abstractOrderEntryModel.getProduct()).willReturn(variantNonBulky);
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(2L));
        given(targetZoneDeliveryModeValueModel.getValue()).willReturn(Double.valueOf(15d));
        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled("shipster.available"))).willReturn(
                Boolean.TRUE);
        final TargetZoneDeliveryModeModel mockDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        given(targetZoneDeliveryModeValueModel.getDeliveryMode()).willReturn(mockDeliveryModeModel);
        // return null
        given(shipsterConfigService.getShipsterModelForDeliveryModes(mockDeliveryModeModel)).willReturn(null);
        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getEntries()).willReturn(entries);
        given(mockCartModel.getAusPostClubMember()).willReturn(Boolean.TRUE);
        given(mockCartModel.getSubtotal()).willReturn(Double.valueOf(30d));
        given(mockCartModel.getTotalDiscounts()).willReturn(Double.valueOf(4d));

        final DeliveryCostDto result = deliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel,
                mockCartModel);
        assertThat(result.getDeliveryCost()).isEqualTo(15);
        assertThat(result.getType()).isEqualTo(DeliveryCostEnumType.NORMAL);
    }

    @Test
    public void testGetDeliveryCostForDeliveryTypeWithShipsterWithNotClubMember() {
        final List<AbstractOrderEntryModel> entries = Collections.singletonList(abstractOrderEntryModel);
        given(abstractOrderEntryModel.getProduct()).willReturn(variantNonBulky);
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(2L));
        given(targetZoneDeliveryModeValueModel.getValue()).willReturn(Double.valueOf(15d));
        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled("shipster.available"))).willReturn(
                Boolean.TRUE);
        final TargetZoneDeliveryModeModel mockDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        given(targetZoneDeliveryModeValueModel.getDeliveryMode()).willReturn(mockDeliveryModeModel);
        final ShipsterConfigModel shipsterConfigModel = mock(ShipsterConfigModel.class);
        given(shipsterConfigModel.getActive()).willReturn(Boolean.TRUE);
        given(shipsterConfigModel.getMaxShippingThresholdValue()).willReturn(Double.valueOf(20.0));
        given(shipsterConfigModel.getMinOrderTotalValue()).willReturn(Double.valueOf(25.0));
        given(shipsterConfigService.getShipsterModelForDeliveryModes(mockDeliveryModeModel)).willReturn(
                shipsterConfigModel);
        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getEntries()).willReturn(entries);
        // set member false
        given(mockCartModel.getAusPostClubMember()).willReturn(Boolean.FALSE);
        given(mockCartModel.getSubtotal()).willReturn(Double.valueOf(30d));
        given(mockCartModel.getTotalDiscounts()).willReturn(Double.valueOf(4d));

        final DeliveryCostDto result = deliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel,
                mockCartModel);
        assertThat(result.getDeliveryCost()).isEqualTo(15);
        assertThat(result.getType()).isEqualTo(DeliveryCostEnumType.NORMAL);
        assertThat(result.getShipsterStatusCode()).isEqualTo(DeliveryShipsterStatusCode.ERR_SHIPSTER_NOT_SUBSCRIBED);
    }

    @Test
    public void testGetDeliveryCostForDeliveryTypeWithShipsterWithShipsterFeatureOff() {
        final List<AbstractOrderEntryModel> entries = Collections.singletonList(abstractOrderEntryModel);
        given(abstractOrderEntryModel.getProduct()).willReturn(variantNonBulky);
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(2L));
        given(targetZoneDeliveryModeValueModel.getValue()).willReturn(Double.valueOf(15d));
        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled("shipster.available"))).willReturn(
                Boolean.FALSE);
        final TargetZoneDeliveryModeModel mockDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        given(targetZoneDeliveryModeValueModel.getDeliveryMode()).willReturn(mockDeliveryModeModel);
        final ShipsterConfigModel shipsterConfigModel = mock(ShipsterConfigModel.class);
        given(shipsterConfigModel.getActive()).willReturn(Boolean.TRUE);
        given(shipsterConfigModel.getMaxShippingThresholdValue()).willReturn(Double.valueOf(20.0));
        given(shipsterConfigModel.getMinOrderTotalValue()).willReturn(Double.valueOf(25.0));
        given(shipsterConfigService.getShipsterModelForDeliveryModes(mockDeliveryModeModel)).willReturn(
                shipsterConfigModel);
        final CartModel mockCartModel = mock(CartModel.class);
        given(mockCartModel.getEntries()).willReturn(entries);
        // set member false
        given(mockCartModel.getAusPostClubMember()).willReturn(Boolean.FALSE);
        given(mockCartModel.getSubtotal()).willReturn(Double.valueOf(30d));
        given(mockCartModel.getTotalDiscounts()).willReturn(Double.valueOf(4d));

        final DeliveryCostDto result = deliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel,
                mockCartModel);
        assertThat(result.getDeliveryCost()).isEqualTo(15);
        assertThat(result.getType()).isEqualTo(DeliveryCostEnumType.NORMAL);
    }

    @Test
    public void testgetZoneDeliveryModeValueForAbstractOrderAndModeWithValidOrder() {
        setupDeliveryModeAndValue();
        final List<AbstractTargetZoneDeliveryModeValueModel> list = new ArrayList<>();
        final TargetZoneDeliveryModeValueModel model1 = Mockito.mock(TargetZoneDeliveryModeValueModel.class);
        final TargetZoneDeliveryModeValueModel model2 = Mockito.mock(TargetZoneDeliveryModeValueModel.class);
        list.add(model2);
        list.add(model1);
        given(model1.getMinimum()).willReturn(Double.valueOf(1.00));
        given(model1.getMinimum()).willReturn(Double.valueOf(200.00));
        given(model1.getActive()).willReturn(Boolean.TRUE);
        given(model2.getActive()).willReturn(Boolean.TRUE);
        given(cartModel.getSubtotal()).willReturn(Double.valueOf(100.00));
        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                list);
        final ZoneDeliveryModeValueModel modeValue = deliveryService
                .getZoneDeliveryModeValueForAbstractOrderAndMode(zoneDeliveryModeModel, cartModel);
        assertThat(model2).isEqualTo(modeValue);
    }

    @Test
    public void testgetZoneDeliveryModeValueForAbstractOrderAndModeCurrentModeValid() {
        setupDeliveryModeAndValue();
        final List<AbstractTargetZoneDeliveryModeValueModel> list = new ArrayList<>();
        final TargetZoneDeliveryModeValueModel model1 = mock(TargetZoneDeliveryModeValueModel.class);
        final TargetZoneDeliveryModeValueModel model2 = mock(TargetZoneDeliveryModeValueModel.class);
        list.add(model2);
        list.add(model1);
        given(model1.getMinimum()).willReturn(Double.valueOf(1.00));
        given(model2.getMinimum()).willReturn(Double.valueOf(200.00));
        given(model1.getActive()).willReturn(Boolean.TRUE);
        given(model2.getActive()).willReturn(Boolean.TRUE);
        given(model1.getDeliveryMode()).willReturn(zoneDeliveryModeModel);
        given(model2.getDeliveryMode()).willReturn(zoneDeliveryModeModel);
        given(cartModel.getSubtotal()).willReturn(Double.valueOf(100.00));
        given(cartModel.getZoneDeliveryModeValue()).willReturn(model1);
        given(cartModel.getDeliveryMode()).willReturn(zoneDeliveryModeModel);
        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                list);
        deliveryService.getZoneDeliveryModeValueForAbstractOrderAndMode(zoneDeliveryModeModel, cartModel);

        verify(targetZoneDeliveryModeValueDao).findDeliveryValuesSortedByPriority(zoneDeliveryModeModel);
    }

    @Test
    public void testgetZoneDeliveryModeValueForAbstractOrderAndModeCurrentModeNotValid() {
        setupDeliveryModeAndValue();
        final List<AbstractTargetZoneDeliveryModeValueModel> list = new ArrayList<>();
        final TargetZoneDeliveryModeValueModel model1 = mock(TargetZoneDeliveryModeValueModel.class);
        final TargetZoneDeliveryModeValueModel model2 = mock(TargetZoneDeliveryModeValueModel.class);
        list.add(model2);
        list.add(model1);
        given(model1.getMinimum()).willReturn(Double.valueOf(1.00));
        given(model2.getMinimum()).willReturn(Double.valueOf(200.00));
        given(model1.getActive()).willReturn(Boolean.TRUE);
        given(model2.getActive()).willReturn(Boolean.TRUE);
        given(model1.getDeliveryMode()).willReturn(zoneDeliveryModeModel);
        given(model2.getDeliveryMode()).willReturn(zoneDeliveryModeModel);
        given(cartModel.getSubtotal()).willReturn(Double.valueOf(100.00));
        given(cartModel.getZoneDeliveryModeValue()).willReturn(model2);
        given(cartModel.getDeliveryMode()).willReturn(zoneDeliveryModeModel);
        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                list);
        deliveryService.getZoneDeliveryModeValueForAbstractOrderAndMode(zoneDeliveryModeModel, cartModel);

        verify(targetZoneDeliveryModeValueDao, times(1)).findDeliveryValuesSortedByPriority(
                any(ZoneDeliveryModeModel.class));
    }

    @Test
    public void testGetSupportedDeliveryAddressesForOrderSorted() throws ParseException {
        final List<AddressModel> addresses = new ArrayList<>();
        // scramble the sort order to test the sorting works
        addresses.add(address2);
        addresses.add(address3);
        addresses.add(address);
        given(user.getDefaultShipmentAddress()).willReturn(address2);

        //return same user for all three addresses
        given(address.getOwner()).willReturn(user);
        given(address2.getOwner()).willReturn(user);
        given(address3.getOwner()).willReturn(user);

        //create created dates for addresses
        final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        final Date date1 = sdf.parse("20/07/2013 00:00:00");
        final Date date2 = sdf.parse("19/06/2013 00:00:00");
        final Date date3 = sdf.parse("18/06/2013 00:00:00");
        given(address.getCreationtime()).willReturn(date1);
        given(address2.getCreationtime()).willReturn(date2);
        given(address3.getCreationtime()).willReturn(date3);

        final List<AddressModel> result = deliveryService.sortAddresses(addresses);
        assertThat(result).hasSize(3);
        assertThat(address2).isEqualTo(result.get(0));
        assertThat(address).isEqualTo(result.get(1));
        assertThat(address3).isEqualTo(result.get(2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetCurrentDeliveryValuesNullMode() throws Exception {
        deliveryService.getAllApplicableDeliveryValuesForModeAndOrder(null, cartModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetCurrentDeliveryValuesNullModeForModeAndOrderWithoutOrderThreshold() throws Exception {
        deliveryService.getAllApplicableDeliveryValuesForModeAndOrderWithoutOrderThreshold(null, cartModel);
    }


    @Test
    public void testGetCurrentDeliveryValuesNoValuesExist() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);

        final List<AbstractTargetZoneDeliveryModeValueModel> values = deliveryService
                .getAllApplicableDeliveryValuesForModeAndOrder(zoneDeliveryModeModel, cartModel);

        assertThat(values).isNull();
    }

    @Test
    public void testGetCurrentDeliveryValuesNoValuesExistForModeAndOrderWithoutOrderThreshold() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);

        final List<AbstractTargetZoneDeliveryModeValueModel> values = deliveryService
                .getAllApplicableDeliveryValuesForModeAndOrderWithoutOrderThreshold(zoneDeliveryModeModel, cartModel);

        assertThat(values).isNull();
    }


    @Test
    public void testGetCurrentDeliveryValuesNotRestrictedOnlyOneValue() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                Collections.singletonList(restrictedZoneDeliveryModeValueModel));

        willReturn(Boolean.TRUE).given(modeValueRestrictionStrategy).isApplicableModeValue(
                (RestrictableTargetZoneDeliveryModeValueModel)restrictedZoneDeliveryModeValueModel, cartModel);

        final List<AbstractTargetZoneDeliveryModeValueModel> values = deliveryService
                .getAllApplicableDeliveryValuesForModeAndOrder(zoneDeliveryModeModel, cartModel);

        assertThat(values).isNotNull();
        assertThat(values).hasSize(1);
        assertThat(restrictedZoneDeliveryModeValueModel).isSameAs(values.get(0));
    }

    @Test
    public void testGetCurrentDeliveryValuesRestrictedOnlyOneValue() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                Collections.singletonList(restrictedZoneDeliveryModeValueModel));

        willReturn(Boolean.FALSE).given(modeValueRestrictionStrategy).isApplicableModeValue(
                (RestrictableTargetZoneDeliveryModeValueModel)restrictedZoneDeliveryModeValueModel, cartModel);

        final List<AbstractTargetZoneDeliveryModeValueModel> values = deliveryService
                .getAllApplicableDeliveryValuesForModeAndOrder(zoneDeliveryModeModel, cartModel);

        assertThat(values).isNull();
    }

    @Test
    public void testGetCurrentDeliveryValuesOnlyOneValue() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                Collections.singletonList(targetZoneDeliveryModeValueModel));

        final List<AbstractTargetZoneDeliveryModeValueModel> values = deliveryService
                .getAllApplicableDeliveryValuesForModeAndOrder(zoneDeliveryModeModel, cartModel);

        assertThat(values).isNotNull();
        assertThat(values).hasSize(1);
        assertThat(targetZoneDeliveryModeValueModel).isSameAs(values.get(0));
    }

    @Test
    public void testGetCurrentDeliveryValuesWithInActive() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);

        final TargetZoneDeliveryModeValueModel inActive = mock(TargetZoneDeliveryModeValueModel.class);
        given(inActive.getExtendedBulky()).willReturn(Double.valueOf(98.0));
        given(inActive.getBulky()).willReturn(Double.valueOf(49.0));
        given(inActive.getValue()).willReturn(Double.valueOf(9.0));
        given(inActive.getActive()).willReturn(Boolean.FALSE);

        final List<AbstractTargetZoneDeliveryModeValueModel> valuesList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                2);
        valuesList.add(inActive);
        valuesList.add(targetZoneDeliveryModeValueModel);
        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                valuesList);

        final List<AbstractTargetZoneDeliveryModeValueModel> values = deliveryService
                .getAllApplicableDeliveryValuesForModeAndOrder(zoneDeliveryModeModel, cartModel);

        assertThat(values).isNotNull();
        assertThat(values).hasSize(1);
        assertThat(targetZoneDeliveryModeValueModel).isSameAs(values.get(0));
    }

    @Test
    public void testGetCurrentDeliveryValuesOnlyRestrected() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);

        final RestrictableTargetZoneDeliveryModeValueModel restricted1 = mock(
                RestrictableTargetZoneDeliveryModeValueModel.class);
        willReturn(Boolean.FALSE).given(modeValueRestrictionStrategy).isApplicableModeValue(restricted1, cartModel);

        final RestrictableTargetZoneDeliveryModeValueModel restricted2 = mock(
                RestrictableTargetZoneDeliveryModeValueModel.class);
        willReturn(Boolean.FALSE).given(modeValueRestrictionStrategy).isApplicableModeValue(restricted2, cartModel);
        final List<AbstractTargetZoneDeliveryModeValueModel> valuesList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                2);
        valuesList.add(restricted1);
        valuesList.add(restricted2);
        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                valuesList);

        final List<AbstractTargetZoneDeliveryModeValueModel> values = deliveryService
                .getAllApplicableDeliveryValuesForModeAndOrder(zoneDeliveryModeModel, cartModel);

        assertThat(values).isNullOrEmpty();
    }

    @Test
    public void testGetCurrentDeliveryValuesOnlyOneRestrected() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);

        final RestrictableTargetZoneDeliveryModeValueModel restricted1 = mock(
                RestrictableTargetZoneDeliveryModeValueModel.class);
        willReturn(Boolean.FALSE).given(modeValueRestrictionStrategy).isApplicableModeValue(restricted1, cartModel);

        final RestrictableTargetZoneDeliveryModeValueModel nonRestricted2 = mock(
                RestrictableTargetZoneDeliveryModeValueModel.class);
        willReturn(Boolean.TRUE).given(modeValueRestrictionStrategy).isApplicableModeValue(nonRestricted2, cartModel);
        final List<AbstractTargetZoneDeliveryModeValueModel> valuesList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                2);
        valuesList.add(restricted1);
        valuesList.add(nonRestricted2);
        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                valuesList);

        final List<AbstractTargetZoneDeliveryModeValueModel> values = deliveryService
                .getAllApplicableDeliveryValuesForModeAndOrder(zoneDeliveryModeModel, cartModel);

        assertThat(values).isNotNull();
        assertThat(values).hasSize(1);
        assertThat(nonRestricted2).isSameAs(values.get(0));

    }

    @Test
    public void testGetCurrentDeliveryValuesOnlyInActive() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);

        final TargetZoneDeliveryModeValueModel inActive = mock(TargetZoneDeliveryModeValueModel.class);
        given(inActive.getExtendedBulky()).willReturn(Double.valueOf(98.0));
        given(inActive.getBulky()).willReturn(Double.valueOf(49.0));
        given(inActive.getValue()).willReturn(Double.valueOf(9.0));
        given(inActive.getActive()).willReturn(Boolean.FALSE);

        final TargetZoneDeliveryModeValueModel inActive1 = mock(TargetZoneDeliveryModeValueModel.class);
        given(inActive1.getExtendedBulky()).willReturn(Double.valueOf(0));
        given(inActive1.getBulky()).willReturn(Double.valueOf(0));
        given(inActive1.getValue()).willReturn(Double.valueOf(0));
        given(inActive1.getActive()).willReturn(Boolean.FALSE);

        final List<AbstractTargetZoneDeliveryModeValueModel> valuesList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                2);
        valuesList.add(inActive);
        valuesList.add(inActive1);
        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                valuesList);

        final List<AbstractTargetZoneDeliveryModeValueModel> values = deliveryService
                .getAllApplicableDeliveryValuesForModeAndOrder(zoneDeliveryModeModel, cartModel);

        assertThat(values).isNullOrEmpty();
    }

    @Test
    public void testGetCurrentDeliveryValuesNoValueSatisfingThreshold() {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);

        given(targetZoneDeliveryModeValueModel.getMinimum()).willReturn(Double.valueOf(100.0d));
        given(cartModel.getSubtotal()).willReturn(Double.valueOf(75.0d));
        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                Collections.singletonList(targetZoneDeliveryModeValueModel));

        final List<AbstractTargetZoneDeliveryModeValueModel> values = deliveryService
                .getAllApplicableDeliveryValuesForModeAndOrder(
                        zoneDeliveryModeModel, cartModel);

        assertThat(values).isNull();
    }

    @Test
    public void testZoneCombinationWithNonPostCodeAwareFirstAndValidPostCode() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodes();
        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                2);
        zoneDeliveryModeValueModelList.add(zoneDeliveryModeValue1);
        zoneDeliveryModeValueModelList.add(zoneDeliveryModeValue2);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel, "3000",
                cartModel)).isTrue();
    }

    @Test
    public void testRestrictedZoneCombinationWithNonPostCodeAwareFirstAndValidPostCode() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodesWithRestrict();
        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                2);
        zoneDeliveryModeValueModelList.add(restrictedZoneDeliveryModeValue1);
        zoneDeliveryModeValueModelList.add(restrictedZoneDeliveryModeValue2);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel, "3000",
                cartModel)).isTrue();
    }

    @Test
    public void testZoneCombinationWithNonPostCodeAwareFirstAndInValidPostCode() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodes();
        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                2);
        zoneDeliveryModeValueModelList.add(zoneDeliveryModeValue1);
        zoneDeliveryModeValueModelList.add(zoneDeliveryModeValue2);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel, "3220",
                cartModel)).isTrue();
    }

    @Test
    public void testRestrictedZoneCombinationWithNonPostCodeAwareFirstAndInValidPostCode() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodesWithRestrict();
        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                2);
        zoneDeliveryModeValueModelList.add(restrictedZoneDeliveryModeValue1);
        zoneDeliveryModeValueModelList.add(restrictedZoneDeliveryModeValue2);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel, "3220",
                cartModel)).isTrue();
    }


    @Test
    public void testZoneCombinationWithPostCodeAwareFirstAndValidPostCode() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodes();
        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                2);
        zoneDeliveryModeValueModelList.add(zoneDeliveryModeValue2);
        zoneDeliveryModeValueModelList.add(zoneDeliveryModeValue1);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel, "3000",
                cartModel)).isTrue();
    }

    @Test
    public void testRestrictedZoneCombinationWithPostCodeAwareFirstAndValidPostCode() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodesWithRestrict();
        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                2);
        zoneDeliveryModeValueModelList.add(restrictedZoneDeliveryModeValue2);
        zoneDeliveryModeValueModelList.add(restrictedZoneDeliveryModeValue1);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel, "3000",
                cartModel)).isTrue();
    }

    @Test
    public void testZoneCombinationWithPostCodeAwareFirstAndInValidPostCode() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodes();
        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                2);
        zoneDeliveryModeValueModelList.add(zoneDeliveryModeValue2);
        zoneDeliveryModeValueModelList.add(zoneDeliveryModeValue1);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel, "3220",
                cartModel)).isTrue();
    }

    @Test
    public void testRestrictedZoneCombinationWithPostCodeAwareFirstAndInValidPostCode() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodesWithRestrict();
        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                2);
        zoneDeliveryModeValueModelList.add(restrictedZoneDeliveryModeValue2);
        zoneDeliveryModeValueModelList.add(restrictedZoneDeliveryModeValue1);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel, "3220",
                cartModel)).isTrue();
    }

    @Test
    public void testPostCodeCombinationIsValidForGivenAddressWithValidPostCode() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodes();
        final AddressModel addressWithValidPC = mock(AddressModel.class);
        given(addressWithValidPC.getPostalcode()).willReturn("3000");

        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                2);
        zoneDeliveryModeValueModelList.add(zoneDeliveryModeValue2);
        zoneDeliveryModeValueModelList.add(zoneDeliveryModeValue1);


        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel,
                addressWithValidPC, cartModel)).isTrue();
    }

    @Test
    public void testRestrictedPostCodeCombinationIsValidForGivenAddressWithValidPostCode() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodesWithRestrict();
        final AddressModel addressWithValidPC = mock(AddressModel.class);
        given(addressWithValidPC.getPostalcode()).willReturn("3000");

        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                2);
        zoneDeliveryModeValueModelList.add(restrictedZoneDeliveryModeValue2);
        zoneDeliveryModeValueModelList.add(restrictedZoneDeliveryModeValue1);


        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel,
                addressWithValidPC, cartModel)).isTrue();
    }

    @Test
    public void testPostCodeCombinationIsValidForGivenAddressWithInValidPostCode() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodes();

        final AddressModel addressWithInValidPC = mock(AddressModel.class);
        given(addressWithInValidPC.getPostalcode()).willReturn("3220");

        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                2);
        zoneDeliveryModeValueModelList.add(zoneDeliveryModeValue2);
        zoneDeliveryModeValueModelList.add(zoneDeliveryModeValue1);


        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);
        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel,
                addressWithInValidPC, cartModel)).isTrue();
    }

    @Test
    public void testRestrictedPostCodeCombinationIsValidForGivenAddressWithInValidPostCode() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodesWithRestrict();

        final AddressModel addressWithInValidPC = mock(AddressModel.class);
        given(addressWithInValidPC.getPostalcode()).willReturn("3220");

        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                2);
        zoneDeliveryModeValueModelList.add(restrictedZoneDeliveryModeValue2);
        zoneDeliveryModeValueModelList.add(restrictedZoneDeliveryModeValue1);


        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);
        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel,
                addressWithInValidPC, cartModel)).isTrue();
    }

    @Test
    public void testPostCodeCombinationWithEmptyZones() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodes();

        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<>();

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel, "3000",
                cartModel)).isFalse();
    }

    @Test
    public void testRestrictedPostCodeCombinationWithEmptyZones() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodesWithRestrict();

        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<>();

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel, "3000",
                cartModel)).isFalse();
    }

    @Test
    public void testNullPostCodeModels() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodes();

        postCodeAwareZone = mock(PostCodeAwareZoneModel.class);
        given(postCodeAwareZone.getPostCodes()).willReturn(null);

        final TargetZoneDeliveryModeValueModel zoneDeliveryModeValue = mock(TargetZoneDeliveryModeValueModel.class);
        given(zoneDeliveryModeValue.getZone()).willReturn(postCodeAwareZone);
        given(zoneDeliveryModeValue.getActive()).willReturn(Boolean.TRUE);

        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                1);
        zoneDeliveryModeValueModelList.add(zoneDeliveryModeValue);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel, "3000",
                cartModel)).isFalse();
    }

    @Test
    public void testIsDeliveryModePostCodeCombinationValidWithPostCodeException() throws Exception {
        final CurrencyModel currencyModel = mock(CurrencyModel.class);
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        final ZoneDeliveryModeModel deliveryMode = mock(ZoneDeliveryModeModel.class);
        final RestrictableTargetZoneDeliveryModeValueModel deliveryModeValue = Mockito
                .mock(RestrictableTargetZoneDeliveryModeValueModel.class);
        final List<AbstractTargetZoneDeliveryModeValueModel> deliveryModeValues = new ArrayList<>();
        deliveryModeValues.add(deliveryModeValue);
        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(deliveryMode)).willReturn(
                deliveryModeValues);
        given(deliveryModeValue.getCurrency()).willReturn(currencyModel);
        given(deliveryModeValue.getMinimum()).willReturn(new Double(0.0));
        given(orderModel.getCurrency()).willReturn(currencyModel);
        final TargetNoPostCodeException targetNoPostCodeException = new TargetNoPostCodeException("exception");
        given(
                Boolean.valueOf(modeValueRestrictionStrategy.isApplicableModeValue(deliveryModeValue,
                        orderModel)))
                                .willThrow(targetNoPostCodeException);
        assertThat(
                deliveryService.isDeliveryModePostCodeCombinationValid(deliveryMode, "1234", orderModel)).isFalse();
    }


    @Test
    public void testIsDeliveryModePostCodeCombinationValidWithoutPostCodeException() throws Exception {
        final CurrencyModel currencyModel = mock(CurrencyModel.class);
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        final ZoneDeliveryModeModel deliveryMode = mock(ZoneDeliveryModeModel.class);
        final RestrictableTargetZoneDeliveryModeValueModel deliveryModeValue = Mockito
                .mock(RestrictableTargetZoneDeliveryModeValueModel.class);
        final List<AbstractTargetZoneDeliveryModeValueModel> deliveryModeValues = new ArrayList<>();
        deliveryModeValues.add(deliveryModeValue);
        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(deliveryMode)).willReturn(
                deliveryModeValues);
        given(deliveryModeValue.getCurrency()).willReturn(currencyModel);
        given(deliveryModeValue.getMinimum()).willReturn(new Double(0.0));
        given(orderModel.getCurrency()).willReturn(currencyModel);
        willReturn(Boolean.TRUE).given(modeValueRestrictionStrategy).isApplicableModeValue(deliveryModeValue,
                orderModel);
        assertThat(
                deliveryService.isDeliveryModePostCodeCombinationValid(deliveryMode, "1234", orderModel)).isTrue();
    }

    @Test
    public void testRestrictNullPostCodeModels() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodesWithRestrict();

        postCodeAwareZone = mock(PostCodeAwareZoneModel.class);
        given(postCodeAwareZone.getPostCodes()).willReturn(null);
        final RestrictableTargetZoneDeliveryModeValueModel zoneDeliveryModeValue = mock(
                RestrictableTargetZoneDeliveryModeValueModel.class);
        willReturn(Boolean.TRUE).given(modeValueRestrictionStrategy).isApplicableModeValue(zoneDeliveryModeValue,
                cartModel);

        given(zoneDeliveryModeValue.getZone()).willReturn(postCodeAwareZone);

        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                1);
        zoneDeliveryModeValueModelList.add(zoneDeliveryModeValue);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel, "3000",
                cartModel)).isFalse();
    }

    @Test
    public void testDeliveryModeNotZoneDeliveryModeModel() throws Exception {
        deliveryModeModel = new DeliveryModeModel();

        assertThat(
                deliveryService.isDeliveryModePostCodeCombinationValid(deliveryModeModel, "3000", cartModel)).isFalse();
    }

    @Test
    public void testOnlyPostCodeAwareZoneWithValidPostCode() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodes();
        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                1);
        zoneDeliveryModeValueModelList.add(zoneDeliveryModeValue2);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel, "3000",
                cartModel)).isTrue();
    }

    @Test
    public void testRestrictedOnlyPostCodeAwareZoneWithValidPostCode() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodesWithRestrict();
        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                1);
        zoneDeliveryModeValueModelList.add(restrictedZoneDeliveryModeValue2);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel, "3000",
                cartModel)).isTrue();
    }

    @Test
    public void testOnlyPostCodeAwareZoneWithInValidPostCode() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodes();
        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                1);
        zoneDeliveryModeValueModelList.add(zoneDeliveryModeValue2);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel, "3220",
                cartModel)).isFalse();
    }

    @Test
    public void testRestrictedOnlyPostCodeAwareZoneWithInValidPostCode() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodesWithRestrict();
        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                1);
        zoneDeliveryModeValueModelList.add(restrictedZoneDeliveryModeValue1);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel, "3220",
                cartModel)).isFalse();
    }

    @Test
    public void testNullPostCode() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodes();
        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                1);
        zoneDeliveryModeValueModelList.add(zoneDeliveryModeValue2);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        final String postCode = null;
        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel, postCode,
                cartModel)).isFalse();
    }

    @Test
    public void testRestrictedNullPostCode() throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        setupZoneDeliveryModeAndPostCodesWithRestrict();
        final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModelList = new ArrayList<AbstractTargetZoneDeliveryModeValueModel>(
                1);
        zoneDeliveryModeValueModelList.add(restrictedZoneDeliveryModeValue1);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                zoneDeliveryModeValueModelList);

        final String postCode = null;
        assertThat(deliveryService.isDeliveryModePostCodeCombinationValid(zoneDeliveryModeModel, postCode,
                cartModel)).isFalse();
    }

    @Test
    public void testGetAllCurrentActiveDeliveryModesEmpty() {
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        given(targetZoneDeliveryModeDao.findAllCurrentlyActiveDeliveryModes(SalesApplication.WEB)).willReturn(null);

        assertThat(deliveryService.getAllApplicableDeliveryModesForOrder(cartModel, SalesApplication.WEB)).isNull();
    }

    @Test
    public void testGetAllCurrentActiveDeliveryModesActiveNoValues() {
        final TargetDeliveryServiceImpl targetDeliveryService = mock(TargetDeliveryServiceImpl.class);
        final Collection<TargetZoneDeliveryModeModel> targetZoneDeliveryModeModels = new ArrayList<>();
        final TargetZoneDeliveryModeModel targetZoneDeliveryModeModel1 = mock(TargetZoneDeliveryModeModel.class);
        final TargetZoneDeliveryModeModel targetZoneDeliveryModeModel2 = mock(TargetZoneDeliveryModeModel.class);

        targetZoneDeliveryModeModel1.setActive(Boolean.TRUE);
        targetZoneDeliveryModeModel2.setActive(Boolean.TRUE);
        targetZoneDeliveryModeModels.add(targetZoneDeliveryModeModel1);
        targetZoneDeliveryModeModels.add(targetZoneDeliveryModeModel2);
        targetDeliveryService.setTargetZoneDeliveryModeDao(targetZoneDeliveryModeDao);


        given(targetDeliveryService.getTargetZoneDeliveryModeDao()).willReturn(targetZoneDeliveryModeDao);
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        given(targetZoneDeliveryModeDao.findAllCurrentlyActiveDeliveryModes(SalesApplication.WEB)).willReturn(
                targetZoneDeliveryModeModels);
        given(
                targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(targetZoneDeliveryModeModel1,
                        cartModel)).willReturn(
                                Collections.EMPTY_LIST);
        given(
                targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(targetZoneDeliveryModeModel2,
                        cartModel)).willReturn(
                                Collections.EMPTY_LIST);
        given(targetDeliveryService.getAllApplicableDeliveryModesForOrder(cartModel, SalesApplication.WEB))
                .willCallRealMethod();

        assertThat(CollectionUtils.isEmpty(targetDeliveryService
                .getAllApplicableDeliveryModesForOrder(cartModel, SalesApplication.WEB))).isTrue();
    }

    @Test
    public void testGetAllCurrentActiveDeliveryModesActiveWithValidValues() {
        final TargetDeliveryServiceImpl targetDeliveryService = mock(TargetDeliveryServiceImpl.class);
        final Collection<TargetZoneDeliveryModeModel> targetZoneDeliveryModeModels = new ArrayList<>();
        final TargetZoneDeliveryModeModel targetZoneDeliveryModeModel1 = mock(TargetZoneDeliveryModeModel.class);
        final TargetZoneDeliveryModeModel targetZoneDeliveryModeModel2 = mock(TargetZoneDeliveryModeModel.class);

        targetZoneDeliveryModeModel1.setActive(Boolean.TRUE);
        targetZoneDeliveryModeModel2.setActive(Boolean.TRUE);
        targetZoneDeliveryModeModels.add(targetZoneDeliveryModeModel1);
        targetZoneDeliveryModeModels.add(targetZoneDeliveryModeModel2);
        targetDeliveryService.setTargetZoneDeliveryModeDao(targetZoneDeliveryModeDao);

        given(targetDeliveryService.getTargetZoneDeliveryModeDao()).willReturn(targetZoneDeliveryModeDao);
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        given(targetZoneDeliveryModeDao.findAllCurrentlyActiveDeliveryModes(SalesApplication.WEB)).willReturn(
                targetZoneDeliveryModeModels);
        given(
                targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(targetZoneDeliveryModeModel1,
                        cartModel)).willReturn(
                                Collections.singletonList(targetZoneDeliveryModeValueModel));
        given(
                targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(targetZoneDeliveryModeModel2,
                        cartModel)).willReturn(
                                Collections.singletonList(targetZoneDeliveryModeValueModel));
        given(targetDeliveryService.getAllApplicableDeliveryModesForOrder(cartModel, SalesApplication.WEB))
                .willCallRealMethod();

        final Collection<TargetZoneDeliveryModeModel> result = targetDeliveryService
                .getAllApplicableDeliveryModesForOrder(cartModel, SalesApplication.WEB);
        assertThat(CollectionUtils.isNotEmpty(result)).isTrue();
        assertThat(result).hasSize(2);

        final Object[] resultArray = result.toArray();
        assertThat(targetZoneDeliveryModeModel1).isEqualTo(resultArray[0]);
        assertThat(targetZoneDeliveryModeModel2).isEqualTo(resultArray[1]);
    }


    @Test
    public void testGetAllCurrentActiveDeliveryModesForProductEmpty() {
        final ProductModel productModel = mock(ProductModel.class);
        given(targetZoneDeliveryModeDao.findAllCurrentlyActiveDeliveryModesForProduct(productModel)).willReturn(null);

        assertThat(deliveryService.getAllApplicableDeliveryModesForProduct(productModel)).isNull();
    }

    @Test
    public void testGetAllCurrentActiveDeliveryModesForProductNoValues() {
        final TargetDeliveryServiceImpl targetDeliveryService = mock(TargetDeliveryServiceImpl.class);
        final Collection<TargetZoneDeliveryModeModel> targetZoneDeliveryModeModels = new ArrayList<>();
        final TargetZoneDeliveryModeModel targetZoneDeliveryModeModel1 = mock(TargetZoneDeliveryModeModel.class);
        final TargetZoneDeliveryModeModel targetZoneDeliveryModeModel2 = mock(TargetZoneDeliveryModeModel.class);
        final ProductModel productModel = mock(ProductModel.class);

        targetZoneDeliveryModeModel1.setActive(Boolean.TRUE);
        targetZoneDeliveryModeModel2.setActive(Boolean.TRUE);
        targetZoneDeliveryModeModels.add(targetZoneDeliveryModeModel1);
        targetZoneDeliveryModeModels.add(targetZoneDeliveryModeModel2);
        targetDeliveryService.setTargetZoneDeliveryModeDao(targetZoneDeliveryModeDao);

        given(targetDeliveryService.getTargetZoneDeliveryModeDao()).willReturn(targetZoneDeliveryModeDao);
        given(targetZoneDeliveryModeDao.findAllCurrentlyActiveDeliveryModesForProduct(productModel)).willReturn(
                targetZoneDeliveryModeModels);
        given(
                targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(targetZoneDeliveryModeModel1,
                        (AbstractOrderModel)null))
                                .willReturn(Collections.EMPTY_LIST);
        given(
                targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(targetZoneDeliveryModeModel2,
                        (AbstractOrderModel)null))
                                .willReturn(Collections.EMPTY_LIST);
        given(targetDeliveryService.getAllApplicableDeliveryModesForProduct(productModel)).willCallRealMethod();

        assertThat(CollectionUtils.isEmpty(targetDeliveryService
                .getAllApplicableDeliveryModesForProduct(productModel))).isTrue();
    }

    @Test
    public void testGetAllCurrentActiveDeliveryModesForProductWithValidValues() {
        final TargetDeliveryServiceImpl targetDeliveryService = mock(TargetDeliveryServiceImpl.class);
        final Collection<TargetZoneDeliveryModeModel> targetZoneDeliveryModeModels = new ArrayList<>();
        final TargetZoneDeliveryModeModel targetZoneDeliveryModeModel1 = mock(TargetZoneDeliveryModeModel.class);
        final TargetZoneDeliveryModeModel targetZoneDeliveryModeModel2 = mock(TargetZoneDeliveryModeModel.class);
        final ProductModel productModel = mock(ProductModel.class);

        targetZoneDeliveryModeModel1.setActive(Boolean.TRUE);
        targetZoneDeliveryModeModel2.setActive(Boolean.TRUE);
        targetZoneDeliveryModeModels.add(targetZoneDeliveryModeModel1);
        targetZoneDeliveryModeModels.add(targetZoneDeliveryModeModel2);
        targetDeliveryService.setTargetZoneDeliveryModeDao(targetZoneDeliveryModeDao);

        given(targetDeliveryService.getTargetZoneDeliveryModeDao()).willReturn(targetZoneDeliveryModeDao);
        given(targetZoneDeliveryModeDao.findAllCurrentlyActiveDeliveryModesForProduct(productModel)).willReturn(
                targetZoneDeliveryModeModels);
        given(
                targetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(targetZoneDeliveryModeModel1,
                        productModel))
                                .willReturn(Collections.singletonList(targetZoneDeliveryModeValueModel));
        given(
                targetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(targetZoneDeliveryModeModel2,
                        productModel))
                                .willReturn(Collections.singletonList(targetZoneDeliveryModeValueModel));
        given(targetDeliveryService.getAllApplicableDeliveryModesForProduct(productModel)).willCallRealMethod();

        final Collection<TargetZoneDeliveryModeModel> result = targetDeliveryService
                .getAllApplicableDeliveryModesForProduct(productModel);
        assertThat(CollectionUtils.isNotEmpty(result)).isTrue();
        assertThat(result).hasSize(2);

        final Object[] resultArray = result.toArray();
        assertThat(targetZoneDeliveryModeModel1).isEqualTo(resultArray[0]);
        assertThat(targetZoneDeliveryModeModel2).isEqualTo(resultArray[1]);
    }

    @Test
    public void testGetAllActiveDeliveryModesForSalesChannel() {
        setupMultipleDeliveryModesAttachedToSalesApplication();
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        final Collection<TargetZoneDeliveryModeModel> deliveryModesForWeb = deliveryService
                .getAllCurrentlyActiveDeliveryModes(SalesApplication.WEB);
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.EBAY);
        final Collection<TargetZoneDeliveryModeModel> deliveryModesForEbay = deliveryService
                .getAllCurrentlyActiveDeliveryModes(SalesApplication.EBAY);
        assertThat(deliveryModesForWeb).hasSize(2);
        assertThat(deliveryModesForEbay).hasSize(1);
    }

    @Test
    public void testGetAllCurrentlyActiveDeliveryModesWhenSalesApplicationIsNull() {
        setupMultipleDeliveryModesAttachedToSalesApplication();
        final Collection<TargetZoneDeliveryModeModel> deliveryModes = deliveryService
                .getAllCurrentlyActiveDeliveryModes(null);
        assertThat(deliveryModes).hasSize(3);
    }

    @Test
    public void testGetCurrentDeliveryValuesNotRestrictedOnlyOneValueForModeAndOrderWithoutOrderThreshold()
            throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                Collections.singletonList(restrictedZoneDeliveryModeValueModel));

        willReturn(Boolean.TRUE).given(modeValueRestrictionStrategy).isApplicableModeValue(
                (RestrictableTargetZoneDeliveryModeValueModel)restrictedZoneDeliveryModeValueModel, cartModel);

        final List<AbstractTargetZoneDeliveryModeValueModel> values = deliveryService
                .getAllApplicableDeliveryValuesForModeAndOrderWithoutOrderThreshold(zoneDeliveryModeModel, cartModel);

        assertThat(values).isNotNull();
        assertThat(values).hasSize(1);
        assertThat(restrictedZoneDeliveryModeValueModel).isEqualTo(values.get(0));
    }

    @Test
    public void testGetCurrentDeliveryValuesRestrictedOnlyOneValueForModeAndOrderWithoutOrderThreshold()
            throws Exception {
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                Collections.singletonList(restrictedZoneDeliveryModeValueModel));

        willReturn(Boolean.FALSE).given(modeValueRestrictionStrategy).isApplicableModeValue(
                (RestrictableTargetZoneDeliveryModeValueModel)restrictedZoneDeliveryModeValueModel, cartModel);

        final List<AbstractTargetZoneDeliveryModeValueModel> values = deliveryService
                .getAllApplicableDeliveryValuesForModeAndOrderWithoutOrderThreshold(zoneDeliveryModeModel, cartModel);

        assertThat(values).isNull();
    }

    @Test
    public void testContainsBulkyItemsWithNoItems() {
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();

        final boolean result = deliveryService.containsBulkyProducts(orderEntries);

        assertThat(result).isFalse();
    }

    @Test
    public void testContainsBulkyProductsWithNoBulkyItems() {
        final ProductTypeModel mockProductTypeNotBulky = mock(ProductTypeModel.class);
        given(mockProductTypeNotBulky.getBulky()).willReturn(Boolean.FALSE);

        final AbstractTargetVariantProductModel mockProduct = mock(AbstractTargetVariantProductModel.class);
        given(mockProduct.getProductType()).willReturn(mockProductTypeNotBulky);

        final AbstractOrderEntryModel mockAbstractOrderEntry = mock(AbstractOrderEntryModel.class);
        given(mockAbstractOrderEntry.getProduct()).willReturn(mockProduct);

        final AbstractTargetVariantProductModel mockProduct2 = mock(AbstractTargetVariantProductModel.class);
        given(mockProduct2.getProductType()).willReturn(mockProductTypeNotBulky);

        final AbstractOrderEntryModel mockAbstractOrderEntry2 = mock(AbstractOrderEntryModel.class);
        given(mockAbstractOrderEntry2.getProduct()).willReturn(mockProduct2);

        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries.add(mockAbstractOrderEntry);
        orderEntries.add(mockAbstractOrderEntry2);

        final boolean result = deliveryService.containsBulkyProducts(orderEntries);

        assertThat(result).isFalse();
    }

    @Test
    public void testContainsBulkyProductsWithNullBulkyType() {
        final ProductTypeModel mockProductTypeNotBulky = mock(ProductTypeModel.class);
        given(mockProductTypeNotBulky.getBulky()).willReturn(null);

        final AbstractTargetVariantProductModel mockProduct = mock(AbstractTargetVariantProductModel.class);
        given(mockProduct.getProductType()).willReturn(mockProductTypeNotBulky);

        final AbstractOrderEntryModel mockAbstractOrderEntry = mock(AbstractOrderEntryModel.class);
        given(mockAbstractOrderEntry.getProduct()).willReturn(mockProduct);

        final AbstractTargetVariantProductModel mockProduct2 = mock(AbstractTargetVariantProductModel.class);
        given(mockProduct2.getProductType()).willReturn(mockProductTypeNotBulky);

        final AbstractOrderEntryModel mockAbstractOrderEntry2 = mock(AbstractOrderEntryModel.class);
        given(mockAbstractOrderEntry2.getProduct()).willReturn(mockProduct2);

        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries.add(mockAbstractOrderEntry);
        orderEntries.add(mockAbstractOrderEntry2);

        final boolean result = deliveryService.containsBulkyProducts(orderEntries);

        assertThat(result).isFalse();
    }

    @Test
    public void testContainsBulkyProductsWithABulkyItem() {
        final ProductTypeModel mockProductTypeNotBulky = mock(ProductTypeModel.class);
        given(mockProductTypeNotBulky.getBulky()).willReturn(Boolean.FALSE);

        final AbstractTargetVariantProductModel mockProduct = mock(AbstractTargetVariantProductModel.class);
        given(mockProduct.getProductType()).willReturn(mockProductTypeNotBulky);

        final AbstractOrderEntryModel mockAbstractOrderEntry = mock(AbstractOrderEntryModel.class);
        given(mockAbstractOrderEntry.getProduct()).willReturn(mockProduct);

        final ProductTypeModel mockProductTypeBulky = mock(ProductTypeModel.class);
        given(mockProductTypeBulky.getBulky()).willReturn(Boolean.TRUE);

        final AbstractTargetVariantProductModel mockBulkyProduct = mock(AbstractTargetVariantProductModel.class);
        given(mockBulkyProduct.getProductType()).willReturn(mockProductTypeBulky);

        final AbstractOrderEntryModel mockAbstractOrderEntry2 = mock(AbstractOrderEntryModel.class);
        given(mockAbstractOrderEntry2.getProduct()).willReturn(mockBulkyProduct);

        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries.add(mockAbstractOrderEntry);
        orderEntries.add(mockAbstractOrderEntry2);

        final boolean result = deliveryService.containsBulkyProducts(orderEntries);

        assertThat(result).isTrue();
    }

    @Test
    public void testConsignmentContainsBulkyItemsWithNoItems() {
        final List<ConsignmentEntryModel> conEntries = new ArrayList<>();

        final boolean result = deliveryService.consignmentContainsBulkyProducts(conEntries);

        assertThat(result).isFalse();
    }

    @Test
    public void testConsignmentContainsBulkyProductsWithNoBulkyItems() {
        final ProductTypeModel mockProductTypeNotBulky = mock(ProductTypeModel.class);
        given(mockProductTypeNotBulky.getBulky()).willReturn(Boolean.FALSE);

        final AbstractTargetVariantProductModel mockProduct = mock(AbstractTargetVariantProductModel.class);
        given(mockProduct.getProductType()).willReturn(mockProductTypeNotBulky);

        final AbstractOrderEntryModel mockAbstractOrderEntry = mock(AbstractOrderEntryModel.class);
        given(mockAbstractOrderEntry.getProduct()).willReturn(mockProduct);

        final AbstractTargetVariantProductModel mockProduct2 = mock(AbstractTargetVariantProductModel.class);
        given(mockProduct2.getProductType()).willReturn(mockProductTypeNotBulky);

        final AbstractOrderEntryModel mockAbstractOrderEntry2 = mock(AbstractOrderEntryModel.class);
        given(mockAbstractOrderEntry2.getProduct()).willReturn(mockProduct2);

        final ConsignmentEntryModel mockConEntry1 = mock(ConsignmentEntryModel.class);
        given(mockConEntry1.getOrderEntry()).willReturn(mockAbstractOrderEntry);

        final ConsignmentEntryModel mockConEntry2 = mock(ConsignmentEntryModel.class);
        given(mockConEntry2.getOrderEntry()).willReturn(mockAbstractOrderEntry2);

        final List<ConsignmentEntryModel> conEntries = new ArrayList<>();
        conEntries.add(mockConEntry1);
        conEntries.add(mockConEntry2);

        final boolean result = deliveryService.consignmentContainsBulkyProducts(conEntries);

        assertThat(result).isFalse();
    }

    @Test
    public void testConsignmentContainsBulkyProductsWithNullBulkyType() {
        final ProductTypeModel mockProductTypeNotBulky = mock(ProductTypeModel.class);
        given(mockProductTypeNotBulky.getBulky()).willReturn(null);

        final AbstractTargetVariantProductModel mockProduct = mock(AbstractTargetVariantProductModel.class);
        given(mockProduct.getProductType()).willReturn(mockProductTypeNotBulky);

        final AbstractOrderEntryModel mockAbstractOrderEntry = mock(AbstractOrderEntryModel.class);
        given(mockAbstractOrderEntry.getProduct()).willReturn(mockProduct);

        final AbstractTargetVariantProductModel mockProduct2 = mock(AbstractTargetVariantProductModel.class);
        given(mockProduct2.getProductType()).willReturn(mockProductTypeNotBulky);

        final AbstractOrderEntryModel mockAbstractOrderEntry2 = mock(AbstractOrderEntryModel.class);
        given(mockAbstractOrderEntry2.getProduct()).willReturn(mockProduct2);

        final ConsignmentEntryModel mockConEntry1 = mock(ConsignmentEntryModel.class);
        given(mockConEntry1.getOrderEntry()).willReturn(mockAbstractOrderEntry);

        final ConsignmentEntryModel mockConEntry2 = mock(ConsignmentEntryModel.class);
        given(mockConEntry2.getOrderEntry()).willReturn(mockAbstractOrderEntry2);

        final List<ConsignmentEntryModel> conEntries = new ArrayList<>();
        conEntries.add(mockConEntry1);
        conEntries.add(mockConEntry2);

        final boolean result = deliveryService.consignmentContainsBulkyProducts(conEntries);

        assertThat(result).isFalse();
    }

    @Test
    public void testConsignmentContainsBulkyProductsWithABulkyItem() {
        final ProductTypeModel mockProductTypeNotBulky = mock(ProductTypeModel.class);
        given(mockProductTypeNotBulky.getBulky()).willReturn(Boolean.FALSE);

        final AbstractTargetVariantProductModel mockProduct = mock(AbstractTargetVariantProductModel.class);
        given(mockProduct.getProductType()).willReturn(mockProductTypeNotBulky);

        final AbstractOrderEntryModel mockAbstractOrderEntry = mock(AbstractOrderEntryModel.class);
        given(mockAbstractOrderEntry.getProduct()).willReturn(mockProduct);

        final ProductTypeModel mockProductTypeBulky = mock(ProductTypeModel.class);
        given(mockProductTypeBulky.getBulky()).willReturn(Boolean.TRUE);

        final AbstractTargetVariantProductModel mockBulkyProduct = mock(AbstractTargetVariantProductModel.class);
        given(mockBulkyProduct.getProductType()).willReturn(mockProductTypeBulky);

        final AbstractOrderEntryModel mockAbstractOrderEntry2 = mock(AbstractOrderEntryModel.class);
        given(mockAbstractOrderEntry2.getProduct()).willReturn(mockBulkyProduct);

        final ConsignmentEntryModel mockConEntry1 = mock(ConsignmentEntryModel.class);
        given(mockConEntry1.getOrderEntry()).willReturn(mockAbstractOrderEntry);

        final ConsignmentEntryModel mockConEntry2 = mock(ConsignmentEntryModel.class);
        given(mockConEntry2.getOrderEntry()).willReturn(mockAbstractOrderEntry2);

        final List<ConsignmentEntryModel> conEntries = new ArrayList<>();
        conEntries.add(mockConEntry1);
        conEntries.add(mockConEntry2);

        final boolean result = deliveryService.consignmentContainsBulkyProducts(conEntries);

        assertThat(result).isTrue();
    }

    @Test
    public void testGetSupportedDeliveryModeListForOrderWhenNoPostCodeFound() {

        final TargetDeliveryServiceImpl deliveryServiceImpl = spy(deliveryService);

        final TargetZoneDeliveryModeModel deliveryMode1 = new TargetZoneDeliveryModeModel();
        final TargetZoneDeliveryModeModel deliveryMode2 = new TargetZoneDeliveryModeModel();
        final TargetZoneDeliveryModeModel deliveryMode3 = new TargetZoneDeliveryModeModel();
        final List<DeliveryModeModel> allDeliveryModes = new ArrayList<>();
        allDeliveryModes.add(deliveryMode1);
        allDeliveryModes.add(deliveryMode2);
        allDeliveryModes.add(deliveryMode3);

        final AbstractTargetZoneDeliveryModeValueModel abstractRestrictibleCNC = new RestrictableTargetZoneDeliveryModeValueModel();
        abstractRestrictibleCNC.setMinimum(Double.valueOf(3d));
        final AbstractTargetZoneDeliveryModeValueModel abstractRestrictibleHD = new RestrictableTargetZoneDeliveryModeValueModel();
        abstractRestrictibleHD.setMinimum(Double.valueOf(3d));
        final AbstractTargetZoneDeliveryModeValueModel abstractRestrictibleED = new RestrictableTargetZoneDeliveryModeValueModel();
        abstractRestrictibleED.setMinimum(Double.valueOf(3d));
        given(targetCountryZoneDeliveryModeDao.findDeliveryModes(any(CountryModel.class),
                any(CurrencyModel.class), Boolean.valueOf(anyBoolean()))).willReturn(allDeliveryModes);

        final Double goodsValue = Double.valueOf(5d);
        given(Double.valueOf(targetDiscountService.getTotalTMDiscount(abstractOrderModel))).willReturn(goodsValue);

        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(deliveryMode1)).willReturn(
                Arrays.asList(abstractRestrictibleCNC));
        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(deliveryMode2)).willReturn(
                Arrays.asList(abstractRestrictibleHD));
        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(deliveryMode3)).willReturn(
                Arrays.asList(abstractRestrictibleED));

        final CurrencyModel currencyModel = mock(CurrencyModel.class);
        given(abstractOrderModel.getCurrency()).willReturn(currencyModel);
        given(currencyModel.getDigits()).willReturn(Integer.valueOf(5));
        given(Double.valueOf(commonI18NService.roundCurrency(goodsValue.doubleValue(), currencyModel.getDigits()
                .intValue()))).willReturn(Double.valueOf(5.0d));

        willThrow(new TargetNoPostCodeException("No Post code found")).given(modeValueRestrictionStrategy)
                .isApplicableModeValue(
                        (RestrictableTargetZoneDeliveryModeValueModel)abstractRestrictibleHD, abstractOrderModel);

        willReturn(Boolean.TRUE).given(modeValueRestrictionStrategy).isApplicableModeValue(
                (RestrictableTargetZoneDeliveryModeValueModel)abstractRestrictibleCNC, abstractOrderModel);

        willReturn(Boolean.TRUE).given(modeValueRestrictionStrategy).isApplicableModeValue(
                (RestrictableTargetZoneDeliveryModeValueModel)abstractRestrictibleED, abstractOrderModel);

        willDoNothing().given(deliveryServiceImpl).sortDeliveryModes(allDeliveryModes, abstractOrderModel);
        assertThat(deliveryServiceImpl.getSupportedDeliveryModeListForOrder(abstractOrderModel)).hasSize(3);
    }

    @Test
    public void testGetAllApplicableDeliveryModesForOrderWhenPostCodeExceptionIsThrown() {
        final TargetZoneDeliveryModeModel testDeliveryModeModel = new TargetZoneDeliveryModeModel();
        final List<TargetZoneDeliveryModeModel> deliveryModesForSalesAppCallCenter = new ArrayList<>();
        deliveryModesForSalesAppCallCenter.add(testDeliveryModeModel);
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.CALLCENTER);
        given(targetZoneDeliveryModeDao.findAllCurrentlyActiveDeliveryModes(SalesApplication.CALLCENTER)).willReturn(
                deliveryModesForSalesAppCallCenter);
        given(deliveryService.getAllApplicableDeliveryValuesForModeAndOrder(testDeliveryModeModel, abstractOrderModel))
                .willThrow(new TargetNoPostCodeException("Exception occured"));

        assertThat(deliveryService.getAllApplicableDeliveryModesForOrder(cartModel,
                SalesApplication.CALLCENTER)).isNotNull();
    }

    @Test
    public void testGetApplicableDeliveryValuesForModeAndProductWithThresholdWhenDeliveryFeesExist() {
        final TargetProductModel productModel = mock(TargetProductModel.class);
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        final TargetDeliveryServiceImpl spyDeliveryService = spy(deliveryService);
        final RestrictableTargetZoneDeliveryModeValueModel firstRestrictedValue = mock(
                RestrictableTargetZoneDeliveryModeValueModel.class);
        final RestrictableTargetZoneDeliveryModeValueModel secondRestrictedValue = mock(
                RestrictableTargetZoneDeliveryModeValueModel.class);
        final RestrictableTargetZoneDeliveryModeValueModel thirdRestrictedValue = mock(
                RestrictableTargetZoneDeliveryModeValueModel.class);
        final TargetZoneDeliveryModeValueModel fourthValue = mock(TargetZoneDeliveryModeValueModel.class);
        final TargetZoneDeliveryModeValueModel fifthValue = null;
        final List<AbstractTargetZoneDeliveryModeValueModel> deliveryValuesForMode = new ArrayList<>();
        deliveryValuesForMode.add(firstRestrictedValue);
        deliveryValuesForMode.add(secondRestrictedValue);
        deliveryValuesForMode.add(thirdRestrictedValue);
        deliveryValuesForMode.add(fourthValue);
        deliveryValuesForMode.add(fifthValue);

        willReturn(Boolean.FALSE).given(modeValueRestrictionStrategy).isApplicableModeValue(firstRestrictedValue,
                productModel);
        willReturn(Boolean.TRUE).given(modeValueRestrictionStrategy).isApplicableModeValue(secondRestrictedValue,
                productModel);
        given(secondRestrictedValue.getMinimum()).willReturn(new Double(20.0));
        willReturn(Boolean.TRUE).given(modeValueRestrictionStrategy).isApplicableModeValue(thirdRestrictedValue,
                productModel);
        given(thirdRestrictedValue.getMinimum()).willReturn(new Double(19.0));
        given(spyDeliveryService.getDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                deliveryValuesForMode);

        final List<AbstractTargetZoneDeliveryModeValueModel> deliveryValuesForProductAndMode = spyDeliveryService
                .getApplicableDeliveryValuesForModeAndProductWithThreshold(zoneDeliveryModeModel, productModel, 19.0);
        assertThat(deliveryValuesForProductAndMode).isNotNull();
        assertThat(thirdRestrictedValue).isEqualTo(deliveryValuesForProductAndMode.get(0));
    }

    @Test
    public void testGetApplicableDeliveryValuesForModeAndProductWithThresholdWhenDeliveryFeesDoesntExist() {
        final TargetProductModel productModel = mock(TargetProductModel.class);
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        final TargetDeliveryServiceImpl spyDeliveryService = spy(deliveryService);
        given(spyDeliveryService.getDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(
                new ArrayList<AbstractTargetZoneDeliveryModeValueModel>());
        final List<AbstractTargetZoneDeliveryModeValueModel> deliveryValuesForProductAndMode = spyDeliveryService
                .getApplicableDeliveryValuesForModeAndProductWithThreshold(zoneDeliveryModeModel, productModel, 19.0);
        assertThat(deliveryValuesForProductAndMode).isNull();
    }

    @Test
    public void testGetApplicableDeliveryValuesForModeAndProductWithThresholdWhenDeliveryFeesIsNull() {
        final TargetProductModel productModel = mock(TargetProductModel.class);
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        final TargetDeliveryServiceImpl spyDeliveryService = spy(deliveryService);
        given(spyDeliveryService.getDeliveryValuesSortedByPriority(zoneDeliveryModeModel)).willReturn(null);
        final List<AbstractTargetZoneDeliveryModeValueModel> deliveryValuesForProductAndMode = spyDeliveryService
                .getApplicableDeliveryValuesForModeAndProductWithThreshold(zoneDeliveryModeModel, productModel, 19.0);
        assertThat(deliveryValuesForProductAndMode).isNull();
    }

    @Test
    public void testIsFreeDeliveryApplicableOnProductForDeliveryModeWhenFreeDeliveryIsApplicable() {
        final TargetProductModel productModel = mock(TargetProductModel.class);
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        final TargetDeliveryServiceImpl spyDeliveryService = spy(deliveryService);
        mockDeliveryValues(productModel, spyDeliveryService, 0.0);

        assertThat(spyDeliveryService.isFreeDeliveryApplicableOnProductForDeliveryMode(productModel, 10.0,
                zoneDeliveryModeModel)).isTrue();
    }

    @Test
    public void testIsFreeDeliveryApplicableOnProductForDeliveryModeWhenFreeDeliveryIsNotApplicable() {
        final TargetProductModel productModel = mock(TargetProductModel.class);
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        final TargetDeliveryServiceImpl spyDeliveryService = spy(deliveryService);
        mockDeliveryValues(productModel, spyDeliveryService, 0.01);

        assertThat(spyDeliveryService.isFreeDeliveryApplicableOnProductForDeliveryMode(productModel, 10.0,
                zoneDeliveryModeModel)).isFalse();
    }

    @Test
    public void testIsFreeDeliveryApplicableOnProductForDeliveryModeWhenNoDeliveryValuesConfigured() {
        final TargetProductModel productModel = mock(TargetProductModel.class);
        zoneDeliveryModeModel = mock(ZoneDeliveryModeModel.class);
        final TargetDeliveryServiceImpl spyDeliveryService = spy(deliveryService);
        willReturn(null).given(spyDeliveryService).getApplicableDeliveryValuesForModeAndProductWithThreshold(
                zoneDeliveryModeModel, productModel, 10.0);
        assertThat(spyDeliveryService.isFreeDeliveryApplicableOnProductForDeliveryMode(productModel, 10.0,
                zoneDeliveryModeModel)).isFalse();
    }

    @Test
    public void isActiveWhenRestrictedDeliveryModeNotApplicable() {
        final RestrictableTargetZoneDeliveryModeValueModel deliveryMode = Mockito
                .mock(RestrictableTargetZoneDeliveryModeValueModel.class);
        willReturn(Boolean.FALSE).given(modeValueRestrictionStrategy).isApplicableModeValue(deliveryMode,
                abstractOrderModel);
        assertThat(deliveryService.isActive(deliveryMode, abstractOrderModel)).isFalse();
    }

    @Test(expected = TargetNoPostCodeException.class)
    public void isActiveWhenPostcodeExceptionIsThrown() {
        final RestrictableTargetZoneDeliveryModeValueModel deliveryMode = Mockito
                .mock(RestrictableTargetZoneDeliveryModeValueModel.class);
        final TargetNoPostCodeException targetNoPostCodeException = new TargetNoPostCodeException("exception");
        given(
                Boolean.valueOf(modeValueRestrictionStrategy.isApplicableModeValue(deliveryMode, abstractOrderModel)))
                        .willThrow(targetNoPostCodeException);
        deliveryService.isActive(deliveryMode, abstractOrderModel);
    }

    @Test
    public void isActiveIgnorePostcodeException() {
        final RestrictableTargetZoneDeliveryModeValueModel deliveryMode = Mockito
                .mock(RestrictableTargetZoneDeliveryModeValueModel.class);
        final TargetNoPostCodeException targetNoPostCodeException = new TargetNoPostCodeException("exception");
        given(
                Boolean.valueOf(modeValueRestrictionStrategy.isApplicableModeValue(deliveryMode, abstractOrderModel)))
                        .willThrow(targetNoPostCodeException);
        assertThat(deliveryService.isActive(deliveryMode, abstractOrderModel, true)).isFalse();
    }

    @Test
    public void isActiveWhenRestrictedDeliveryModeApplicable() {
        final RestrictableTargetZoneDeliveryModeValueModel deliveryMode = Mockito
                .mock(RestrictableTargetZoneDeliveryModeValueModel.class);
        willReturn(Boolean.TRUE).given(modeValueRestrictionStrategy).isApplicableModeValue(deliveryMode,
                abstractOrderModel);
        assertThat(deliveryService.isActive(deliveryMode, abstractOrderModel)).isTrue();
    }

    @Test
    public void isActiveWhenZoneDeliveryModeNotActive() {
        final TargetZoneDeliveryModeValueModel deliveryMode = Mockito
                .mock(TargetZoneDeliveryModeValueModel.class);
        given(deliveryMode.getActive()).willReturn(Boolean.FALSE);
        assertThat(deliveryService.isActive(deliveryMode, abstractOrderModel)).isFalse();
    }

    @Test
    public void isActiveWhenZoneDeliveryModeActive() {
        final TargetZoneDeliveryModeValueModel deliveryMode = Mockito
                .mock(TargetZoneDeliveryModeValueModel.class);
        given(deliveryMode.getActive()).willReturn(Boolean.TRUE);
        assertThat(deliveryService.isActive(deliveryMode, abstractOrderModel)).isTrue();
    }

    @Test
    public void isActiveWhenBaseType() {
        final AbstractTargetZoneDeliveryModeValueModel deliveryMode = Mockito
                .mock(AbstractTargetZoneDeliveryModeValueModel.class);
        assertThat(deliveryService.isActive(deliveryMode, abstractOrderModel)).isFalse();
    }

    @Test(expected = IllegalArgumentException.class)
    public void getAllApplicableDeliveryValuesForModeAndOrderWithPostcodeZoneDeliveryModeNull() {
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        deliveryService.getAllApplicableDeliveryValuesForModeAndOrder(null, orderModel, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getAllApplicableDeliveryValuesForModeAndOrderWithPostcodeAbstractOrderNull() {
        final ZoneDeliveryModeModel deliveryMode = mock(ZoneDeliveryModeModel.class);
        deliveryService.getAllApplicableDeliveryValuesForModeAndOrder(deliveryMode, null, true);
    }

    @Test
    public void getAllApplicableDeliveryValuesForModeAndOrderWithPostcodeEmptyDeliveryMode() {
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        final ZoneDeliveryModeModel deliveryMode = mock(ZoneDeliveryModeModel.class);
        assertThat(
                deliveryService.getAllApplicableDeliveryValuesForModeAndOrder(deliveryMode,
                        orderModel, true))
                                .isNull();
    }

    @Test
    public void getAllApplicableDeliveryValuesForModeAndOrderWithPostcodeInactive() {
        final TargetDeliveryServiceImpl deliveryServiceSpy = spy(deliveryService);
        final CurrencyModel currencyModel = mock(CurrencyModel.class);
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        final ZoneDeliveryModeModel deliveryMode = mock(ZoneDeliveryModeModel.class);
        final AbstractTargetZoneDeliveryModeValueModel deliveryModeValue = mock(
                AbstractTargetZoneDeliveryModeValueModel.class);
        final List<AbstractTargetZoneDeliveryModeValueModel> deliveryModeValues = new ArrayList<>();
        deliveryModeValues.add(deliveryModeValue);
        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(deliveryMode)).willReturn(
                deliveryModeValues);
        given(deliveryModeValue.getCurrency()).willReturn(currencyModel);
        given(deliveryModeValue.getMinimum()).willReturn(new Double(0.0));
        given(orderModel.getCurrency()).willReturn(currencyModel);
        willReturn(Boolean.FALSE).given(deliveryServiceSpy).isActive(deliveryModeValue, orderModel);
        assertThat(
                deliveryServiceSpy.getAllApplicableDeliveryValuesForModeAndOrder(deliveryMode,
                        orderModel, true))
                                .isNull();
    }

    @Test
    public void getAllApplicableDeliveryValuesForModeAndOrderWithPostcodeActive() {
        final TargetDeliveryServiceImpl deliveryServiceSpy = spy(deliveryService);
        final CurrencyModel currencyModel = mock(CurrencyModel.class);
        final AbstractOrderModel orderModel = mock(AbstractOrderModel.class);
        final ZoneDeliveryModeModel deliveryMode = mock(ZoneDeliveryModeModel.class);
        final AbstractTargetZoneDeliveryModeValueModel deliveryModeValue = mock(
                AbstractTargetZoneDeliveryModeValueModel.class);
        final List<AbstractTargetZoneDeliveryModeValueModel> deliveryModeValues = new ArrayList<>();
        deliveryModeValues.add(deliveryModeValue);
        given(targetZoneDeliveryModeValueDao.findDeliveryValuesSortedByPriority(deliveryMode)).willReturn(
                deliveryModeValues);
        given(deliveryModeValue.getCurrency()).willReturn(currencyModel);
        given(deliveryModeValue.getMinimum()).willReturn(new Double(0.0));
        given(orderModel.getCurrency()).willReturn(currencyModel);
        willReturn(Boolean.TRUE).given(deliveryServiceSpy).isActive(deliveryModeValue, orderModel, true);
        assertThat(
                deliveryServiceSpy.getAllApplicableDeliveryValuesForModeAndOrder(deliveryMode,
                        orderModel, true))
                                .isNotNull();
    }

    @Test
    public void testGetShipsterModelForDeliveryModesWithEmptyDeliveryMode() {
        final TargetDeliveryServiceImpl spyDeliveryService = spy(deliveryService);
        assertThat(spyDeliveryService.getShipsterModelForDeliveryModes(null)).isEmpty();
    }

    @Test
    public void testGetShipsterModelForDeliveryModes() {
        final TargetDeliveryServiceImpl spyDeliveryService = spy(deliveryService);
        final List<TargetZoneDeliveryModeModel> targetZoneDeliveryModeModels = new ArrayList<>();
        deliveryModeModel1 = mock(TargetZoneDeliveryModeModel.class);
        targetZoneDeliveryModeModels.add(deliveryModeModel1);
        spyDeliveryService.getShipsterModelForDeliveryModes(targetZoneDeliveryModeModels);
        verify(shipsterDao).getShipsterModelForDeliveryModes(targetZoneDeliveryModeModels);
    }

    @Test
    public void testGetDeliveryCostForDeliveryTypePreOrder() {

        final List<AbstractOrderEntryModel> entries = Collections.singletonList(abstractOrderEntryModel);
        given(abstractOrderModel.getEntries()).willReturn(entries);
        willReturn(Boolean.TRUE).given(abstractOrderModel).getContainsPreOrderItems();
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(3L));

        final TargetZoneDeliveryModeModel deliveryMode = mock(TargetZoneDeliveryModeModel.class);
        given(deliveryMode.getMaxPreOrderValue()).willReturn(Double.valueOf(5.0));
        given(targetZoneDeliveryModeValueModel.getDeliveryMode()).willReturn(deliveryMode);

        final double result = deliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel,
                abstractOrderModel).getDeliveryCost();
        assertThat(result).isEqualTo(5.0, Delta.delta(0.0));

    }

    @Test
    public void testGetDeliveryCostForDeliveryTypeWhenPreOrderDeliveryFeeGreaterThenExpressDeliveryFee() {

        final List<AbstractOrderEntryModel> entries = Collections.singletonList(abstractOrderEntryModel);
        given(abstractOrderModel.getEntries()).willReturn(entries);
        willReturn(Boolean.TRUE).given(abstractOrderModel).getContainsPreOrderItems();
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(3L));

        final TargetZoneDeliveryModeModel deliveryMode = mock(TargetZoneDeliveryModeModel.class);
        given(deliveryMode.getMaxPreOrderValue()).willReturn(Double.valueOf(15.0));
        given(targetZoneDeliveryModeValueModel.getDeliveryMode()).willReturn(deliveryMode);

        given(targetZoneDeliveryModeValueModel.getValue()).willReturn(Double.valueOf(9.0));

        final double result = deliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel,
                abstractOrderModel).getDeliveryCost();
        assertThat(result).isEqualTo(9.0, Delta.delta(0.0));

    }

    @Test
    public void testGetDeliveryCostForDeliveryTypeWhenPreOrderDeliveryFeeNotDefined() {

        final List<AbstractOrderEntryModel> entries = Collections.singletonList(abstractOrderEntryModel);
        given(abstractOrderModel.getEntries()).willReturn(entries);
        willReturn(Boolean.TRUE).given(abstractOrderModel).getContainsPreOrderItems();
        given(abstractOrderEntryModel.getQuantity()).willReturn(Long.valueOf(3L));

        final TargetZoneDeliveryModeModel deliveryMode = mock(TargetZoneDeliveryModeModel.class);
        given(deliveryMode.getMaxPreOrderValue()).willReturn(null);
        given(targetZoneDeliveryModeValueModel.getDeliveryMode()).willReturn(deliveryMode);

        given(targetZoneDeliveryModeValueModel.getValue()).willReturn(Double.valueOf(9.0));

        final double result = deliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel,
                abstractOrderModel).getDeliveryCost();
        assertThat(result).isEqualTo(9.0, Delta.delta(0.0));

    }

    /**
     * Method to mock delivery values
     * 
     * @param productModel
     * @param spyDeliveryService
     */
    private void mockDeliveryValues(final TargetProductModel productModel,
            final TargetDeliveryServiceImpl spyDeliveryService, final double value) {
        final RestrictableTargetZoneDeliveryModeValueModel firstRestrictedValue = mock(
                RestrictableTargetZoneDeliveryModeValueModel.class);
        final RestrictableTargetZoneDeliveryModeValueModel secondRestrictedValue = mock(
                RestrictableTargetZoneDeliveryModeValueModel.class);
        final List<AbstractTargetZoneDeliveryModeValueModel> deliveryValuesForProductForAGivenMode = new ArrayList<>();
        deliveryValuesForProductForAGivenMode.add(firstRestrictedValue);
        deliveryValuesForProductForAGivenMode.add(secondRestrictedValue);
        willReturn(deliveryValuesForProductForAGivenMode).given(spyDeliveryService)
                .getApplicableDeliveryValuesForModeAndProductWithThreshold(zoneDeliveryModeModel, productModel, 10.0);
        willReturn(new Double(value)).given(firstRestrictedValue).getValue();
    }
}

/**
 * 
 */
package au.com.target.tgtcore.delivery.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.jalo.order.Cart;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.jalo.AbstractTargetZoneDeliveryModeValueRestriction;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueRestrictionModel;
import au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel;


/**
 * @author manoj
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetZoneDeliveryModeValueRestrictionStrategyTest {

    private TargetZoneDeliveryModeValueRestrictionStrategyImpl modeValueRestrictionStrategy;

    private Set<AbstractTargetZoneDeliveryModeValueRestrictionModel> restrictions;

    private List<AbstractOrderEntryModel> entries;

    @Mock
    private ModelService modelService;

    @Mock
    private CartModel cartModel;

    @Mock
    private Cart cart;

    @Mock
    private AbstractOrderEntryModel entryModel;

    @Mock
    private AbstractOrderModel abstractOrder;

    @Mock
    private AbstractTargetZoneDeliveryModeValueRestriction restrictionEvaluated;

    @Mock
    private AbstractTargetZoneDeliveryModeValueRestriction restrictionNonEvaluated;

    @Mock
    private AbstractTargetZoneDeliveryModeValueRestrictionModel restrictionEvaluatedModel1;

    @Mock
    private AbstractTargetZoneDeliveryModeValueRestrictionModel restrictionEvaluatedModel2;

    @Mock
    private AbstractTargetZoneDeliveryModeValueRestrictionModel restrictionNonEvaluatedModel;

    @Mock
    private RestrictableTargetZoneDeliveryModeValueModel modeValueModel;

    @Before
    public void setup() {

        modeValueRestrictionStrategy = new TargetZoneDeliveryModeValueRestrictionStrategyImpl();
        modeValueRestrictionStrategy.setModelService(modelService);

        entries = new ArrayList<>();
        entries.add(entryModel);

        given(cartModel.getEntries()).willReturn(entries);

        given(modelService.getSource(cartModel)).willReturn(abstractOrder);
        given(modelService.getSource(restrictionEvaluatedModel1)).willReturn(restrictionEvaluated);
        given(modelService.getSource(restrictionEvaluatedModel2)).willReturn(restrictionEvaluated);
        given(modelService.getSource(restrictionNonEvaluatedModel)).willReturn(restrictionNonEvaluated);

        given(Boolean.valueOf(restrictionEvaluated.evaluate(cartModel))).willReturn(Boolean.TRUE);
        given(Boolean.valueOf(restrictionNonEvaluated.evaluate(abstractOrder))).willReturn(Boolean.FALSE);

    }

    @Test
    public void testIsApplicableModeValueWithNullOrder() {

        assertThat(modeValueRestrictionStrategy.isApplicableModeValue(modeValueModel, (AbstractOrderModel)null))
                .isTrue();
    }

    @Test
    public void testIsApplicableModeValueWithNullDeliveryModeValue() {

        assertThat(modeValueRestrictionStrategy.isApplicableModeValue(null, cartModel)).isTrue();
    }

    @Test
    public void testIsApplicableModeValueWithEmptyRestriction() {

        assertThat(modeValueRestrictionStrategy.isApplicableModeValue(modeValueModel, cartModel)).isTrue();
    }

    @Test
    public void testIsApplicableModeValueWithRestrictionOneEvaluated() {

        restrictions = new HashSet<>();
        restrictions.add(restrictionEvaluatedModel1);
        given(modeValueModel.getRestrictions()).willReturn(restrictions);
        assertThat(modeValueRestrictionStrategy.isApplicableModeValue(modeValueModel, cartModel)).isTrue();
    }

    @Test
    public void testIsApplicableModeValueWithRestrictionOneNonEvaluated() {

        restrictions = new HashSet<>();
        restrictions.add(restrictionNonEvaluatedModel);
        given(modeValueModel.getRestrictions()).willReturn(restrictions);
        assertThat(modeValueRestrictionStrategy.isApplicableModeValue(modeValueModel, cartModel)).isFalse();
    }

    @Test(expected = TargetNoPostCodeException.class)
    public void isApplicableModeValueWithRestrictionOneThrowsException() {
        restrictions = new HashSet<>();
        restrictions.add(restrictionEvaluatedModel1);
        given(Boolean.valueOf(restrictionEvaluated.evaluate(cartModel))).willThrow(
                new TargetNoPostCodeException("Invalid postcode"));
        given(modeValueModel.getRestrictions()).willReturn(restrictions);
        modeValueRestrictionStrategy.isApplicableModeValue(modeValueModel, cartModel);
    }

    @Test
    public void testIsApplicableModeValueWithRestrictionOneEvaluatedAndOneNonEvaluated() {

        restrictions = new HashSet<>();
        restrictions.add(restrictionEvaluatedModel1);
        restrictions.add(restrictionNonEvaluatedModel);
        given(modeValueModel.getRestrictions()).willReturn(restrictions);
        assertThat(modeValueRestrictionStrategy.isApplicableModeValue(modeValueModel, cartModel)).isFalse();
    }

    @Test
    public void testIsApplicableModeValueWithRestrictionWithAllEvaluated() {

        restrictions = new HashSet<>();
        restrictions.add(restrictionEvaluatedModel1);
        restrictions.add(restrictionEvaluatedModel2);
        given(modeValueModel.getRestrictions()).willReturn(restrictions);
        assertThat(modeValueRestrictionStrategy.isApplicableModeValue(modeValueModel, cartModel)).isTrue();
    }


}

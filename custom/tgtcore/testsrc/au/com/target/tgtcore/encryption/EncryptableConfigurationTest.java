/**
 * 
 */
package au.com.target.tgtcore.encryption;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.util.config.HybrisConfig;

import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author mjanarth
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EncryptableConfigurationTest {

    @Mock
    private HybrisConfig config;

    private EncryptableConfiguration encCfg;

    @Before
    public void setUp() {
        encCfg = new EncryptableConfiguration(config, new PooledPBEStringEncryptor());
    }


    @Test
    public void testGetString() {
        encCfg.getString("tgtstorefront.host.fqdn");
        Mockito.verify(config).getString("tgtstorefront.host.fqdn", "");
    }
}

/**
 * 
 */
package au.com.target.tgtcore.evaluator;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeTypeRestrictionModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * @author nandini
 * 
 */
@UnitTest
public class TargetSizeTypeRestrictionEvaluatorTest {

    private TargetSizeTypeRestrictionEvaluator sizeTypeRestrictionEvaluator;

    @Mock
    private TargetSizeTypeRestrictionModel sizeTypeRestrictionModel;

    @Mock
    private RestrictionData restrictionData;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        sizeTypeRestrictionEvaluator = new TargetSizeTypeRestrictionEvaluator();
    }

    @Test
    public void testEvaluateNull() {
        Assert.assertTrue(sizeTypeRestrictionEvaluator.evaluate(sizeTypeRestrictionModel, null));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testEvaluateNoProduct() {
        given(restrictionData.hasProduct()).willReturn(false);

        Assert.assertFalse(sizeTypeRestrictionEvaluator.evaluate(sizeTypeRestrictionModel, restrictionData));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testEvaluateNoSizeProduct() {
        final TargetColourVariantProductModel productModel = Mockito.mock(TargetColourVariantProductModel.class);

        given(restrictionData.hasProduct()).willReturn(true);
        given(restrictionData.getProduct()).willReturn(productModel);

        Assert.assertFalse(sizeTypeRestrictionEvaluator.evaluate(sizeTypeRestrictionModel, restrictionData));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testEvaluatePositive() {
        final TargetSizeVariantProductModel productModel = Mockito.mock(TargetSizeVariantProductModel.class);
        final SizeTypeModel sizeType = Mockito.mock(SizeTypeModel.class);

        given(sizeTypeRestrictionModel.getSizes()).willReturn(Collections.singletonList(sizeType));
        given(restrictionData.hasProduct()).willReturn(true);
        given(restrictionData.getProduct()).willReturn(productModel);
        given(productModel.getSizeType()).willReturn("testSize");
        given(sizeType.getCode()).willReturn("testSize");

        Assert.assertTrue(sizeTypeRestrictionEvaluator.evaluate(sizeTypeRestrictionModel, restrictionData));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testEvaluateNegative() {
        final TargetSizeVariantProductModel productModel = Mockito.mock(TargetSizeVariantProductModel.class);
        final SizeTypeModel sizeType = Mockito.mock(SizeTypeModel.class);

        given(sizeTypeRestrictionModel.getSizes()).willReturn(Collections.singletonList(sizeType));
        given(restrictionData.hasProduct()).willReturn(true);
        given(restrictionData.getProduct()).willReturn(productModel);
        given(productModel.getSizeType()).willReturn("testSizeAnother");
        given(sizeType.getCode()).willReturn("testSize");

        Assert.assertFalse(sizeTypeRestrictionEvaluator.evaluate(sizeTypeRestrictionModel, restrictionData));
    }
}

/**
 * 
 */
package au.com.target.tgtcore.flybuys.service;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetRuntimeException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.flybuys.dao.FlybuysRedeemConfigDao;
import au.com.target.tgtcore.flybuys.service.impl.FlybuysRedeemConfigServiceImpl;
import au.com.target.tgtcore.model.FlybuysRedeemConfigModel;


/**
 * @author umesh
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FlybuysRedeemConfigServiceImplTest {

    @InjectMocks
    private final FlybuysRedeemConfigServiceImpl flybuysRedeemConfigService = new FlybuysRedeemConfigServiceImpl();

    @Mock
    private FlybuysRedeemConfigDao flybuysRedeemConfigDao;

    @Before
    public void setup() {
        flybuysRedeemConfigService.setFlybuysRedeemConfigDao(flybuysRedeemConfigDao);
    }

    @Test(expected = TargetRuntimeException.class)
    public void testGetFlyBuyRedeemConfigThrowsTargetAmbiguousIdentifierException()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(flybuysRedeemConfigDao.getFlybuysRedeemConfig()).willThrow(new TargetAmbiguousIdentifierException(""));
        flybuysRedeemConfigService.getFlybuysRedeemConfig();
        Assert.fail();
    }

    @Test(expected = TargetRuntimeException.class)
    public void testGetFlyBuyRedeemConfigThrowsTargetUnknownIdentifierException()
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        given(flybuysRedeemConfigDao.getFlybuysRedeemConfig()).willThrow(new TargetUnknownIdentifierException(""));
        flybuysRedeemConfigService.getFlybuysRedeemConfig();
        Assert.fail();
    }

    @Test
    public void testGetFlyBuyRedeemConfigNull() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        given(flybuysRedeemConfigDao.getFlybuysRedeemConfig()).willReturn(null);

        assertThat(flybuysRedeemConfigService.getFlybuysRedeemConfig()).isNull();
    }

    @Test
    public void testGetFlyBuyRedeemConfig() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final FlybuysRedeemConfigModel fbConfigModel = mock(FlybuysRedeemConfigModel.class);
        given(flybuysRedeemConfigDao.getFlybuysRedeemConfig()).willReturn(fbConfigModel);

        assertThat(flybuysRedeemConfigService.getFlybuysRedeemConfig()).isEqualTo(fbConfigModel);
    }
}
/**
 * 
 */
package au.com.target.tgtcore.flybuys.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetRuntimeException;
import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;
import au.com.target.tgtcore.flybuys.service.FlybuysRedeemConfigService;
import au.com.target.tgtcore.model.FlybuysRedeemConfigModel;


/**
 * @author Nandini
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FlybuysConfigOffDenialStrategyTest {

    @Mock
    private FlybuysRedeemConfigModel flybuysRedeemConfig;

    @Mock
    private AbstractOrderModel order;

    @Mock
    private FlybuysRedeemConfigService flybuysRedeemConfigService;

    @InjectMocks
    private final FlybuysConfigOffDenialStrategy strategy = new FlybuysConfigOffDenialStrategy();

    @Test
    public void testIsFlybuysDiscountAllowedNullOrder() {
        // return value is not defined and we don't care, so just make sure it does not throw an exception
        strategy.isFlybuysDiscountAllowed(null);
    }

    @Test
    public void testNullFlybuysDiscountAllowed() {
        given(flybuysRedeemConfigService.getFlybuysRedeemConfig()).willReturn(null);
        final FlybuysDiscountDenialDto response = strategy.isFlybuysDiscountAllowed(order);
        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
        assertThat(response.getReason()).isEqualTo(StringUtils.EMPTY);
    }

    @Test
    public void testFlybuysDiscountNotAllowed() {
        given(flybuysRedeemConfigService.getFlybuysRedeemConfig()).willReturn(flybuysRedeemConfig);
        given(flybuysRedeemConfig.getActive()).willReturn(Boolean.FALSE);
        final FlybuysDiscountDenialDto response = strategy.isFlybuysDiscountAllowed(order);
        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
        assertThat(response.getReason()).isEqualTo(StringUtils.EMPTY);
    }

    @Test
    public void testIsFlybuysDiscountNotAllowedOnException() {
        given(flybuysRedeemConfigService.getFlybuysRedeemConfig()).willThrow(new TargetRuntimeException(null, null));
        final FlybuysDiscountDenialDto response = strategy.isFlybuysDiscountAllowed(order);
        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
        assertThat(response.getReason()).isEqualTo(StringUtils.EMPTY);
    }

    @Test
    public void testIsFlybuysDiscountAllowed() {
        given(flybuysRedeemConfigService.getFlybuysRedeemConfig()).willReturn(flybuysRedeemConfig);
        given(flybuysRedeemConfig.getActive()).willReturn(Boolean.TRUE);
        final FlybuysDiscountDenialDto response = strategy.isFlybuysDiscountAllowed(order);
        assertThat(response).isNull();
    }

}

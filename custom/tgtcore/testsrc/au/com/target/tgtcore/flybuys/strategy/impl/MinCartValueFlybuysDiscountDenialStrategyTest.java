package au.com.target.tgtcore.flybuys.strategy.impl;


import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;
import au.com.target.tgtcore.flybuys.service.FlybuysRedeemConfigService;
import au.com.target.tgtcore.model.FlybuysRedeemConfigModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MinCartValueFlybuysDiscountDenialStrategyTest {

    private static final String SUB_TOTAL = "subtotal";

    @InjectMocks
    private final MinCartValueFlybuysDiscountDenialStrategy stategy = new MinCartValueFlybuysDiscountDenialStrategy();

    @Mock
    private FlybuysRedeemConfigService flybuysRedeemConfigService;

    @Mock
    private AbstractOrderModel order;

    @Mock
    private FlybuysRedeemConfigModel flybuysRedeemConfigModel;

    @Before
    public void setup() {
        given(flybuysRedeemConfigService.getFlybuysRedeemConfig()).willReturn(flybuysRedeemConfigModel);
    }

    @Test
    public void testIsFlybuysDiscountAllowedNullOrder() {
        // return value is not defined and we don't care, so just make sure it does not throw an exception
        stategy.isFlybuysDiscountAllowed(null);
    }

    @Test
    public void testIsFlybuysDiscountAllowedNullFlybuysRedeemConfig()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(flybuysRedeemConfigService.getFlybuysRedeemConfig()).willReturn(null);

        final FlybuysDiscountDenialDto response = stategy.isFlybuysDiscountAllowed(order);
        assertThat(response).isNull();
    }

    @Test
    public void testIsFlybuysDiscountAllowedWhenConfMinCartValueIsNull()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(flybuysRedeemConfigModel.getMinCartValue()).willReturn(null);
        given(order.getSubtotal()).willReturn(Double.valueOf(10d));

        final FlybuysDiscountDenialDto response = stategy.isFlybuysDiscountAllowed(order);
        assertThat(response).isNull();
    }

    @Test
    public void testIsFlybuysDiscountAllowedWhenCartSubTotalIsNull()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(flybuysRedeemConfigModel.getMinCartValue()).willReturn(Double.valueOf(10d));
        given(order.getSubtotal()).willReturn(null);

        final FlybuysDiscountDenialDto response = stategy.isFlybuysDiscountAllowed(order);
        assertThat(response).isNull();
    }

    @Test
    public void testIsFlybuysDiscountAllowedWhenCartSubTotalIsLessThanConfMinCartValue()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(flybuysRedeemConfigModel.getMinCartValue()).willReturn(Double.valueOf(20d));
        given(order.getSubtotal()).willReturn(Double.valueOf(10d));

        final FlybuysDiscountDenialDto response = stategy.isFlybuysDiscountAllowed(order);
        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
        assertThat(response.getReason()).isEqualTo(SUB_TOTAL);
    }

    @Test
    public void testIsFlybuysDiscountAllowedWhenCartSubTotalIsEqualToConfMinCartValue()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(flybuysRedeemConfigModel.getMinCartValue()).willReturn(Double.valueOf(10d));
        given(order.getSubtotal()).willReturn(Double.valueOf(10d));

        final FlybuysDiscountDenialDto response = stategy.isFlybuysDiscountAllowed(order);
        assertThat(response).isNull();
    }

    @Test
    public void testIsFlybuysDiscountAllowedWhenCartSubTotalIsGreaterThanConfMinCartValue()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(flybuysRedeemConfigModel.getMinCartValue()).willReturn(Double.valueOf(10d));
        given(order.getSubtotal()).willReturn(Double.valueOf(20d));

        final FlybuysDiscountDenialDto response = stategy.isFlybuysDiscountAllowed(order);
        assertThat(response).isNull();
    }
}
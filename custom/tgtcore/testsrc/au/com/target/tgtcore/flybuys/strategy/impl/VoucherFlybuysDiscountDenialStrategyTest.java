/**
 * 
 */
package au.com.target.tgtcore.flybuys.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.voucher.VoucherService;
import de.hybris.platform.voucher.model.VoucherModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;
import au.com.target.tgtcore.model.FlybuysDiscountModel;


/**
 * @author vivek
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VoucherFlybuysDiscountDenialStrategyTest {

    /** The Constant DENIAL_REASON_VOUCHER. */
    private static final String DENIAL_REASON_VOUCHER = "voucher";

    @Mock
    private AbstractOrderModel mockOrder;

    @Mock
    private VoucherService voucherService;

    @InjectMocks
    private final VoucherFlybuysDiscountDenialStrategy strategy = new VoucherFlybuysDiscountDenialStrategy();

    @Test
    public void testIsFlybuysDiscountAllowedNullOrder() {
        // return value is not defined and we don't care, so just make sure it does not throw an exception
        strategy.isFlybuysDiscountAllowed(null);
    }

    @Test
    public void testIsFlybuysDiscountAllowedNoVoucher() {
        given(voucherService.getAppliedVouchers(mockOrder)).willReturn(null);
        final FlybuysDiscountDenialDto response = strategy.isFlybuysDiscountAllowed(mockOrder);
        assertThat(response).isNull();
    }

    @Test
    public void testIsFlybuysDiscountAllowedExistingVoucher() {
        final VoucherModel voucher = mock(VoucherModel.class);
        final List<DiscountModel> discounts = new ArrayList<>();
        discounts.add(voucher);

        given(voucherService.getAppliedVouchers(mockOrder)).willReturn(discounts);
        final FlybuysDiscountDenialDto response = strategy.isFlybuysDiscountAllowed(mockOrder);

        assertThat(response).isNotNull();
        assertThat(response.isDenied()).isTrue();
        assertThat(response.getReason()).isEqualTo(DENIAL_REASON_VOUCHER);
    }

    @Test
    public void testIsFlybuysDiscountAllowedExistingFlybuys() {
        final FlybuysDiscountModel flybuys = mock(FlybuysDiscountModel.class);
        final List<DiscountModel> discounts = new ArrayList<>();
        discounts.add(flybuys);

        given(voucherService.getAppliedVouchers(mockOrder)).willReturn(discounts);
        final FlybuysDiscountDenialDto response = strategy.isFlybuysDiscountAllowed(mockOrder);

        assertThat(response).isNull();
    }

}

/**
 * 
 */
package au.com.target.tgtcore.formatter.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.IpgGiftCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.type.TypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.velocity.VelocityContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.barcode.TargetBarCodeService;
import au.com.target.tgtcore.formatter.InvoiceFormatterHelper;
import au.com.target.tgtcore.formatter.data.PaymentInfoData;
import au.com.target.tgtcore.order.TargetDiscountService;
import au.com.target.tgtcore.taxinvoice.data.TaxInvoiceItems;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;
import au.com.target.tgtpayment.model.ZippayPaymentInfoModel;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class InvoiceOrderFormatterImplTest {

    @InjectMocks
    private final InvoiceOrderFormatterImpl formatter = new InvoiceOrderFormatterImpl();
    @Mock
    private InvoiceFormatterHelper invoiceFormatterHelper;
    @Mock
    private TargetVoucherService targetVoucherService;
    @Mock
    private TargetBarCodeService targetBarCodeService;
    @Mock
    private TargetDiscountService targetDiscountService;
    @Mock
    private TypeService typeService;
    @Mock
    private VelocityContext vContext;
    @Mock
    private OrderModel order;
    @Mock
    private PaymentTransactionModel paymentTransaction;
    @Mock
    private PaymentTransactionEntryModel paymentTransEntry;
    @Mock
    private EnumerationValueModel mcCardType;
    @Mock
    private EnumerationValueModel visaCardType;
    @Mock
    private PaymentModeModel paymentMode;
    @Mock
    private Date paymentDate;

    @Before
    public void setUp() {
        final List<PaymentTransactionModel> trans = new ArrayList<>();
        trans.add(paymentTransaction);
        final List<PaymentTransactionEntryModel> transEntires = new ArrayList<>();
        transEntires.add(paymentTransEntry);
        when(paymentTransaction.getEntries()).thenReturn(transEntires);
        when(order.getPaymentTransactions()).thenReturn(trans);
        final TaxInvoiceItems itemEntries = new TaxInvoiceItems();
        when(invoiceFormatterHelper.getTaxInvoiceItems(order)).thenReturn(itemEntries);

        when(mcCardType.getName()).thenReturn("MasterCard");
        when(typeService.getEnumerationValue(CreditCardType.MASTERCARD)).thenReturn(mcCardType);

        when(visaCardType.getName()).thenReturn("VISA");
        when(typeService.getEnumerationValue(CreditCardType.VISA)).thenReturn(visaCardType);
    }

    @Test
    public void testFormatIpgOrderWithNoAcceptedTransaction() {
        final IpgCreditCardPaymentInfoModel ipgPaymentInfo = mock(IpgCreditCardPaymentInfoModel.class);
        when(paymentTransEntry.getIpgPaymentInfo()).thenReturn(ipgPaymentInfo);
        when(paymentTransEntry.getTransactionStatus()).thenReturn(TransactionStatus.REJECTED.name());
        when(order.getPaymentMode()).thenReturn(paymentMode);

        formatter.addOrderDetails(vContext, order);

        verify(vContext, never()).put("paymentMethod", "MasterCard");
        verify(vContext, never()).put("ipgCCPaymentInfo", ipgPaymentInfo);
    }

    @Test
    public void testGetPaymentsWhenCaptureTransactionHasNoEntries() {
        final List<PaymentTransactionModel> paymentTransactions = new ArrayList<>();
        paymentTransactions.add(paymentTransaction);
        given(order.getPaymentTransactions()).willReturn(paymentTransactions);
        given(paymentTransaction.getEntries()).willReturn(null);

        final List<PaymentInfoData> payments = formatter.getPayments(order);

        assertThat(payments).isEmpty();
    }

    @Test
    public void testGetPaymentsWhenCaptureTransactionHasPinPadPayment() {
        final BigDecimal amount = new BigDecimal("10.00");
        final String maskedCardNumber = "424242******4242";
        final String receiptNumber = "123456789";
        final String paymentModeCode = "pinpad";
        final String paymentModeName = "Pin Pad";

        populateApprovedTransactionInformation(amount, null, false, false);

        final PinPadPaymentInfoModel pinpadPaymentInfoModel = mock(PinPadPaymentInfoModel.class);
        final PaymentModeModel paymentModeModel = mock(PaymentModeModel.class);
        given(paymentModeModel.getCode()).willReturn(paymentModeCode);
        given(paymentModeModel.getName()).willReturn(paymentModeName);
        given(order.getPaymentMode()).willReturn(paymentModeModel);
        given(order.getPaymentInfo()).willReturn(pinpadPaymentInfoModel);
        given(pinpadPaymentInfoModel.getMaskedCardNumber()).willReturn(maskedCardNumber);
        given(pinpadPaymentInfoModel.getRrn()).willReturn(receiptNumber);

        final List<PaymentInfoData> payments = formatter.getPayments(order);

        assertThat(payments).hasSize(1);
        assertThat(payments).onProperty("paymentMethod").containsExactly(paymentModeName);
        assertThat(payments).onProperty("amount").containsExactly("$" + amount.toString());
        assertThat(payments).onProperty("receiptNumber").containsExactly(receiptNumber);
        assertThat(payments).onProperty("accountId").containsExactly(maskedCardNumber);
    }

    @Test
    public void testGetPaymentsWhenCaptureTransactionHasCreditCardPayment() {
        final BigDecimal amount = new BigDecimal("10.00");
        final String maskedCardNumber = "424242******4242";
        final String receiptNumber = "123456789";
        final String paymentModeCode = "creditcard";
        final String paymentModeName = "VISA";
        final EnumerationValueModel visaModel = mock(EnumerationValueModel.class);
        given(visaModel.getName()).willReturn(paymentModeName);

        populateApprovedTransactionInformation(amount, receiptNumber, false, false);

        final CreditCardPaymentInfoModel creditcardPaymentInfoModel = mock(CreditCardPaymentInfoModel.class);
        final PaymentModeModel paymentModeModel = mock(PaymentModeModel.class);
        given(paymentModeModel.getCode()).willReturn(paymentModeCode);
        given(paymentModeModel.getName()).willReturn(paymentModeName);
        given(order.getPaymentMode()).willReturn(paymentModeModel);
        given(order.getPaymentInfo()).willReturn(creditcardPaymentInfoModel);
        given(creditcardPaymentInfoModel.getNumber()).willReturn(maskedCardNumber);
        given(creditcardPaymentInfoModel.getType()).willReturn(CreditCardType.VISA);
        given(typeService.getEnumerationValue(CreditCardType.VISA)).willReturn(visaModel);

        final List<PaymentInfoData> payments = formatter.getPayments(order);

        assertThat(payments).hasSize(1);
        assertThat(payments).onProperty("paymentMethod").containsExactly(paymentModeName);
        assertThat(payments).onProperty("amount").containsExactly("$" + amount.toString());
        assertThat(payments).onProperty("paymentDate").containsExactly(paymentDate);
        assertThat(payments).onProperty("receiptNumber").containsExactly(receiptNumber);
        assertThat(payments).onProperty("accountId").containsExactly(maskedCardNumber);
    }

    @Test
    public void testGetPaymentsWhenCaptureTransactionHasPaypalPayment() {
        final BigDecimal amount = new BigDecimal("10.00");
        final String receiptNumber = "123456789";
        final String paymentModeCode = "paypal";
        final String paymentModeName = "Pay Pal";
        final String emailId = "target@target.com.au";

        populateApprovedTransactionInformation(amount, receiptNumber, false, false);

        final PaypalPaymentInfoModel paypalPaymentInfoModel = mock(PaypalPaymentInfoModel.class);
        final PaymentModeModel paymentModeModel = mock(PaymentModeModel.class);
        given(paymentModeModel.getCode()).willReturn(paymentModeCode);
        given(paymentModeModel.getName()).willReturn(paymentModeName);
        given(order.getPaymentMode()).willReturn(paymentModeModel);
        given(order.getPaymentInfo()).willReturn(paypalPaymentInfoModel);
        given(paypalPaymentInfoModel.getEmailId()).willReturn(emailId);

        final List<PaymentInfoData> payments = formatter.getPayments(order);

        assertThat(payments).hasSize(1);
        assertThat(payments).onProperty("paymentMethod").containsExactly(paymentModeName);
        assertThat(payments).onProperty("amount").containsExactly("$" + amount.toString());
        assertThat(payments).onProperty("paymentDate").containsExactly(paymentDate);
        assertThat(payments).onProperty("receiptNumber").containsExactly(receiptNumber);
        assertThat(payments).onProperty("accountId").containsExactly(emailId);
    }

    @Test
    public void testGetPaymentsWhenCaptureTransactionHasAfterpayPayment() {
        final BigDecimal amount = new BigDecimal("10.00");
        final String receiptNumber = "123456789";
        final String paymentModeCode = "afterpay";
        final String paymentModeName = "Afterpay";

        populateApprovedTransactionInformation(amount, receiptNumber, false, false);

        final AfterpayPaymentInfoModel mockAfterpayPaymentInfo = mock(AfterpayPaymentInfoModel.class);
        given(order.getPaymentInfo()).willReturn(mockAfterpayPaymentInfo);

        final PaymentModeModel paymentModeModel = mock(PaymentModeModel.class);
        given(paymentModeModel.getCode()).willReturn(paymentModeCode);
        given(paymentModeModel.getName()).willReturn(paymentModeName);
        given(order.getPaymentMode()).willReturn(paymentModeModel);

        final List<PaymentInfoData> payments = formatter.getPayments(order);

        assertThat(payments).hasSize(1);
        assertThat(payments).onProperty("paymentMethod").containsExactly(paymentModeName);
        assertThat(payments).onProperty("amount").containsExactly("$" + amount.toString());
        assertThat(payments).onProperty("paymentDate").containsExactly(paymentDate);
        assertThat(payments).onProperty("receiptNumber").containsExactly(receiptNumber);
        assertThat(payments).onProperty("accountId").containsExactly(new Object[] { null });
    }

	@Test
	public void testGetPaymentsWhenCaptureTransactionHasZipPayment() {
		final BigDecimal amount = new BigDecimal("10.00");
		final String receiptNumber = "123456789";
		final String paymentModeCode = "zippay";
		final String paymentModeName = "Zip";

		populateApprovedTransactionInformation(amount, receiptNumber, false, false);

		final ZippayPaymentInfoModel mockZippayPaymentInfo = mock(ZippayPaymentInfoModel.class);

		given(mockZippayPaymentInfo.getReceiptNo()).willReturn("123456789");
		given(order.getPaymentInfo()).willReturn(mockZippayPaymentInfo);


		final PaymentModeModel paymentModeModel = mock(PaymentModeModel.class);
		given(paymentModeModel.getCode()).willReturn(paymentModeCode);
		given(paymentModeModel.getName()).willReturn(paymentModeName);
		given(order.getPaymentMode()).willReturn(paymentModeModel);

		final List<PaymentInfoData> payments = formatter.getPayments(order);

		assertThat(payments).hasSize(1);
		assertThat(payments).onProperty("paymentMethod").containsExactly(paymentModeName);
		assertThat(payments).onProperty("amount").containsExactly("$" + amount.toString());
		assertThat(payments).onProperty("paymentDate").containsExactly(paymentDate);
		assertThat(payments).onProperty("receiptNumber").containsExactly(receiptNumber);
		assertThat(payments).onProperty("accountId").containsExactly(new Object[] { null });
	}

    @Test
    public void testGetPaymentsWhenCaptureTransactionHasIpgCreditCardPayment() {
        final BigDecimal amount = new BigDecimal("10.00");
        final String receiptNumber = "123456789";
        final String paymentModeCode = "ipg";
        final String paymentModeName = "VISA";
        final String maskedCardNumber = "424242******4242";

        populateApprovedTransactionInformation(amount, receiptNumber, false, true);

        final PaymentModeModel paymentModeModel = mock(PaymentModeModel.class);
        given(paymentModeModel.getCode()).willReturn(paymentModeCode);
        given(paymentModeModel.getName()).willReturn(paymentModeName);
        given(order.getPaymentMode()).willReturn(paymentModeModel);

        final List<PaymentInfoData> payments = formatter.getPayments(order);

        assertThat(payments).hasSize(1);
        assertThat(payments).onProperty("paymentMethod").containsExactly(paymentModeName);
        assertThat(payments).onProperty("amount").containsExactly("$" + amount.toString());
        assertThat(payments).onProperty("paymentDate").containsExactly(paymentDate);
        assertThat(payments).onProperty("receiptNumber").containsExactly(receiptNumber);
        assertThat(payments).onProperty("accountId").containsExactly(maskedCardNumber);
    }

    @Test
    public void testGetPaymentsWhenCaptureTransactionHasIpgGiftCardPayment() {
        final BigDecimal amount = new BigDecimal("10.00");
        final String receiptNumber = "123456789";
        final String paymentModeCode = "giftcard";
        final String paymentModeName = "Gift Card";
        final String maskedCardNumber = "6273353******4134";

        populateApprovedTransactionInformation(amount, receiptNumber, true, false);

        final PaymentModeModel paymentModeModel = mock(PaymentModeModel.class);
        given(paymentModeModel.getCode()).willReturn(paymentModeCode);
        given(paymentModeModel.getName()).willReturn(paymentModeName);
        given(order.getPaymentMode()).willReturn(paymentModeModel);

        final List<PaymentInfoData> payments = formatter.getPayments(order);

        assertThat(payments).hasSize(1);
        assertThat(payments).onProperty("paymentMethod").containsExactly(paymentModeName);
        assertThat(payments).onProperty("amount").containsExactly("$" + amount.toString());
        assertThat(payments).onProperty("paymentDate").containsExactly(paymentDate);
        assertThat(payments).onProperty("receiptNumber").containsExactly(receiptNumber);
        assertThat(payments).onProperty("accountId").containsExactly(maskedCardNumber);
    }

    @Test
    public void testVerifyRefundFollowOnTransactionIncludedInTaxInvoice() {
        final BigDecimal amount = new BigDecimal("10.00");
        final String receiptNumber = "123456789";
        final String paymentModeCode = "ipg";
        final String paymentModeName = "VISA";
        final String maskedCardNumber = "424242******4242";

        final List<PaymentTransactionModel> paymentTransactions = new ArrayList<>();
        paymentTransactions.add(paymentTransaction);
        given(paymentTransaction.getIsCaptureSuccessful()).willReturn(Boolean.TRUE);

        final IpgPaymentInfoModel ipgPaymentInfoModel = mock(IpgPaymentInfoModel.class);
        given(order.getPaymentInfo()).willReturn(ipgPaymentInfoModel);

        final List<PaymentTransactionEntryModel> paymentTransactionEntries = new ArrayList<>();
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        paymentTransactionEntries.add(paymentTransactionEntryModel);
        given(paymentTransaction.getEntries()).willReturn(paymentTransactionEntries);
        given(paymentTransaction.getIsCaptureSuccessful()).willReturn(Boolean.TRUE);
        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransaction);
        given(paymentTransactionEntryModel.getReceiptNo()).willReturn(receiptNumber);
        given(paymentTransactionEntryModel.getAmount()).willReturn(amount);

        final IpgCreditCardPaymentInfoModel ccPaymentInfoModel = mock(IpgCreditCardPaymentInfoModel.class);
        given(ccPaymentInfoModel.getType()).willReturn(CreditCardType.VISA);
        given(ccPaymentInfoModel.getNumber()).willReturn(maskedCardNumber);
        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(ccPaymentInfoModel);

        final TransactionStatus status = TransactionStatus.ACCEPTED;
        final PaymentTransactionType type = PaymentTransactionType.REFUND_FOLLOW_ON;
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(status.toString());
        given(paymentTransactionEntryModel.getType()).willReturn(type);
        given(paymentTransactionEntryModel.getAmount()).willReturn(amount);
        given(paymentTransactionEntryModel.getReceiptNo()).willReturn(receiptNumber);

        final PaymentModeModel paymentModeModel = mock(PaymentModeModel.class);
        given(paymentModeModel.getCode()).willReturn(paymentModeCode);
        given(paymentModeModel.getName()).willReturn(paymentModeName);
        given(order.getPaymentMode()).willReturn(paymentModeModel);

        final List<PaymentInfoData> payments = formatter.getPayments(order);

        assertThat(payments).hasSize(1);
        assertThat(payments).onProperty("amount").containsExactly("-$" + amount.toString());
        assertThat(payments).onProperty("receiptNumber").containsExactly(receiptNumber);
        assertThat(payments).onProperty("accountId").containsExactly(maskedCardNumber);
    }

    @Test
    public void testVerifyRefundStandaloneTransactionIncludedInTaxInvoice() {
        final BigDecimal amount = new BigDecimal("10.00");
        final String receiptNumber = "123456789";
        final String paymentModeCode = "ipg";
        final String paymentModeName = "VISA";

        final List<PaymentTransactionModel> paymentTransactions = new ArrayList<>();
        paymentTransactions.add(paymentTransaction);
        given(paymentTransaction.getIsCaptureSuccessful()).willReturn(Boolean.TRUE);

        final IpgPaymentInfoModel ipgPaymentInfoModel = mock(IpgPaymentInfoModel.class);
        given(order.getPaymentInfo()).willReturn(ipgPaymentInfoModel);

        final List<PaymentTransactionEntryModel> paymentTransactionEntries = new ArrayList<>();
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        paymentTransactionEntries.add(paymentTransactionEntryModel);
        given(paymentTransaction.getEntries()).willReturn(paymentTransactionEntries);
        given(paymentTransaction.getIsCaptureSuccessful()).willReturn(Boolean.TRUE);
        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransaction);
        given(paymentTransactionEntryModel.getReceiptNo()).willReturn(receiptNumber);
        given(paymentTransactionEntryModel.getAmount()).willReturn(amount);

        final TransactionStatus status = TransactionStatus.ACCEPTED;
        final PaymentTransactionType type = PaymentTransactionType.REFUND_STANDALONE;
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(status.toString());
        given(paymentTransactionEntryModel.getType()).willReturn(type);
        given(paymentTransactionEntryModel.getAmount()).willReturn(amount);
        given(paymentTransactionEntryModel.getReceiptNo()).willReturn(receiptNumber);

        final PaymentModeModel paymentModeModel = mock(PaymentModeModel.class);
        given(paymentModeModel.getCode()).willReturn(paymentModeCode);
        given(paymentModeModel.getName()).willReturn(paymentModeName);
        given(order.getPaymentMode()).willReturn(paymentModeModel);

        final List<PaymentInfoData> payments = formatter.getPayments(order);

        assertThat(payments).hasSize(1);
        assertThat(payments).onProperty("amount").containsExactly("-$" + amount.toString());
        assertThat(payments).onProperty("receiptNumber").containsExactly(receiptNumber);

        assertThat(payments.get(0).getAccountId()).isEmpty();
    }

    private void populateApprovedTransactionInformation(final BigDecimal amount, final String receiptNumber,
            final boolean isIpgGiftCard, final boolean isIpgCreditCard) {

        final List<PaymentTransactionModel> paymentTransactions = new ArrayList<>();
        paymentTransactions.add(paymentTransaction);

        given(order.getPaymentTransactions()).willReturn(paymentTransactions);
        given(paymentTransaction.getEntries()).willReturn(null);

        final List<PaymentTransactionEntryModel> paymentTransactionEntries = new ArrayList<>();
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
        paymentTransactionEntries.add(paymentTransactionEntryModel);
        given(paymentTransaction.getEntries()).willReturn(paymentTransactionEntries);
        given(paymentTransaction.getIsCaptureSuccessful()).willReturn(Boolean.TRUE);
        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(paymentTransaction);
        given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(null);

        final TransactionStatus status = TransactionStatus.ACCEPTED;
        final PaymentTransactionType type = PaymentTransactionType.CAPTURE;
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(status.toString());
        given(paymentTransactionEntryModel.getType()).willReturn(type);
        given(paymentTransactionEntryModel.getAmount()).willReturn(amount);
        given(paymentTransactionEntryModel.getCreationtime()).willReturn(paymentDate);
        given(paymentTransactionEntryModel.getReceiptNo()).willReturn(receiptNumber);

        if (isIpgCreditCard) {
            final String paymentModeName = "VISA";
            final EnumerationValueModel visaModel = mock(EnumerationValueModel.class);
            given(visaModel.getName()).willReturn(paymentModeName);

            final IpgCreditCardPaymentInfoModel ipgCreditcardPaymentInfoModel = mock(
                    IpgCreditCardPaymentInfoModel.class);
            given(ipgCreditcardPaymentInfoModel.getType()).willReturn(CreditCardType.VISA);
            given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(ipgCreditcardPaymentInfoModel);
            given(typeService.getEnumerationValue(CreditCardType.VISA)).willReturn(visaModel);
            given(ipgCreditcardPaymentInfoModel.getNumber()).willReturn("424242******4242");
        }

        if (isIpgGiftCard) {
            final String paymentModeName = "Gift Card";
            final EnumerationValueModel giftcardModel = mock(EnumerationValueModel.class);
            given(giftcardModel.getName()).willReturn(paymentModeName);

            final IpgGiftCardPaymentInfoModel ipgGiftCardPaymentInfoModel = mock(IpgGiftCardPaymentInfoModel.class);
            given(ipgGiftCardPaymentInfoModel.getCardType()).willReturn(CreditCardType.GIFTCARD);
            given(paymentTransactionEntryModel.getIpgPaymentInfo()).willReturn(ipgGiftCardPaymentInfoModel);
            given(typeService.getEnumerationValue(CreditCardType.GIFTCARD)).willReturn(giftcardModel);
            given(ipgGiftCardPaymentInfoModel.getMaskedNumber()).willReturn("6273353******4134");
        }

    }

    @Test
    public void testAddStoreBarcodesFor() {

        when(order.getCode()).thenReturn("12345678");
        when(targetBarCodeService.getPlainBarcodeWithoutCheckDigit(order.getCode())).thenReturn("568312345678");
        formatter.addStoreBarcodes(vContext, order);
        verify(vContext, never()).put("barCode", "568312345678");
    }

    @Test
    public void testAddStoreBarcodesForCNCOrder() {

        final TargetZoneDeliveryModeModel cncDMode = new TargetZoneDeliveryModeModel();
        cncDMode.setIsDeliveryToStore(Boolean.TRUE);
        order.setDeliveryMode(cncDMode);
        when(order.getCode()).thenReturn("12345678");
        formatter.addStoreBarcodes(vContext, order);
        verify(targetBarCodeService).getPlainBarcodeWithoutCheckDigit(order.getCode());
    }

    @Test
    public void testAddStoreBarcodesForHomeDeliveryModeOrder() {
        final TargetZoneDeliveryModeModel hmDMode = new TargetZoneDeliveryModeModel();
        hmDMode.setIsDeliveryToStore(Boolean.FALSE);
        when(order.getCode()).thenReturn("12345678");
        formatter.addStoreBarcodes(vContext, order);
        verify(targetBarCodeService).getPlainBarcodeWithoutCheckDigit(order.getCode());
    }

    @Test
    public void testAddMaskedFlybuysCode() {
        given(order.getFlyBuysCode()).willReturn("6008943218616910");
        given(invoiceFormatterHelper.getMaskedFlybuysNumber("6008943218616910")).willReturn("600*********6910");
        formatter.addMaskedFlybuysCode(vContext, order);
        verify(vContext).put("maskedFlyBuysCode", "600*********6910");
    }

    @Test
    public void testAddMaskedFlybuysCodeEmpty() {
        given(order.getFlyBuysCode()).willReturn(null);
        given(invoiceFormatterHelper.getMaskedFlybuysNumber(null)).willReturn(null);
        formatter.addMaskedFlybuysCode(vContext, order);
        verify(vContext, never()).put(eq("maskedFlyBuysCode"), any());
    }

}
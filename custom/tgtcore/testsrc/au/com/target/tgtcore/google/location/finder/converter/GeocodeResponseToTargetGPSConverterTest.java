package au.com.target.tgtcore.google.location.finder.converter;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.google.location.finder.data.GeocodeAddressComponent;
import au.com.target.tgtcore.google.location.finder.data.GeocodeGeometry;
import au.com.target.tgtcore.google.location.finder.data.GeocodeLocation;
import au.com.target.tgtcore.google.location.finder.data.GeocodeResponse;
import au.com.target.tgtcore.google.location.finder.data.GeocodeResultData;
import au.com.target.tgtcore.storelocator.impl.TargetGPS;


/**
 * 
 * @author bpottass
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GeocodeResponseToTargetGPSConverterTest {

    @Mock
    private GeocodeResponse response;

    @Mock
    private GeocodeResultData resultData;

    @Mock
    private GeocodeGeometry gecodeGeometry;

    @Mock
    private GeocodeLocation geocodeLocation;

    @Mock
    private GeocodeAddressComponent geocodeAddressComponentLocality;

    @Mock
    private GeocodeAddressComponent geocodeAddressComponentPostCode;

    @Mock
    private GeocodeAddressComponent geocodeAddressComponentAdminArea1;

    @InjectMocks
    private final GeocodeResponseToTargetGPSConverter geocodeResponseToTargetGPSConverter = new GeocodeResponseToTargetGPSConverter();


    @Before
    public void setUp() {
        final List<GeocodeResultData> geocodeResultsList = new ArrayList<>();
        geocodeResultsList.add(resultData);

        given(response.getResults()).willReturn(geocodeResultsList);
        given(resultData.getGeometry()).willReturn(gecodeGeometry);
        given(gecodeGeometry.getLocation()).willReturn(geocodeLocation);
        given(Double.valueOf(geocodeLocation.getLat())).willReturn(Double.valueOf(-38.1062834));
        given(Double.valueOf(geocodeLocation.getLng())).willReturn(Double.valueOf(145.3484256));

        given(geocodeAddressComponentPostCode.getTypes()).willReturn(Arrays.asList("postal_code"));
        given(geocodeAddressComponentPostCode.getLongName()).willReturn("3978");

        given(geocodeAddressComponentAdminArea1.getTypes())
                .willReturn(Arrays.asList("administrative_area_level_1", "political"));
        given(geocodeAddressComponentAdminArea1.getShortName()).willReturn("VIC");


    }

    @Test
    public void testConvertSuccess() {

        given(geocodeAddressComponentLocality.getTypes()).willReturn(Arrays.asList("locality", "political"));
        given(geocodeAddressComponentLocality.getLongName()).willReturn("Clyde North");

        given(resultData.getAddressComponents()).willReturn(Arrays.asList(geocodeAddressComponentLocality,
                geocodeAddressComponentPostCode, geocodeAddressComponentAdminArea1));

        final TargetGPS gps = geocodeResponseToTargetGPSConverter.convert(response);

        assertThat(gps.getDecimalLatitude()).isEqualTo(-38.1062834);
        assertThat(gps.getDecimalLongitude()).isEqualTo(145.3484256);
        assertThat(gps.getLocality()).isEqualTo("Clyde North");
        assertThat(gps.getPostcode()).isEqualTo("3978");
        assertThat(gps.getState()).isEqualTo("VIC");
    }

    @Test
    public void testConvertWithPostCodeLocalities() {
        given(geocodeAddressComponentLocality.getTypes()).willReturn(null);
        final String[] localities = { "Hobart", "North Hobart" };
        given(resultData.getPostcodeLocalities()).willReturn(localities);

        given(resultData.getAddressComponents())
                .willReturn(Arrays.asList(geocodeAddressComponentPostCode, geocodeAddressComponentAdminArea1));

        final TargetGPS gps = geocodeResponseToTargetGPSConverter.convert(response);

        assertThat(gps.getLocality()).isEqualTo("Hobart");

    }

}

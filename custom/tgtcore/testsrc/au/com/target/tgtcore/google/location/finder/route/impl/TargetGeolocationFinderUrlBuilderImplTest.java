/**
 * 
 */
package au.com.target.tgtcore.google.location.finder.route.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import au.com.target.tgtcore.storelocator.route.impl.GoogleUrlSigner;


/**
 * @author bbaral1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetGeolocationFinderUrlBuilderImplTest {

    private static final String BASE_URL = "http://maps.googleapis.com/maps/api/geocode/";

    private static final String SIGNER_SUFFIX = "&client=target&signature=cryptic_string";

    @Mock
    private GoogleUrlSigner googleUrlSigner;

    @InjectMocks
    private final TargetGeolocationFinderUrlBuilderImpl targetGeoLocationFinderUrlBuilderImpl = new TargetGeolocationFinderUrlBuilderImpl();

    /**
     * This method will initialize the test data before executing test cases.
     */
    @Before
    public void setUp() {
        when(googleUrlSigner.sign(anyString())).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(final InvocationOnMock invocationOnMock) {
                final String url = (String)invocationOnMock.getArguments()[0];
                return url + SIGNER_SUFFIX;
            }
        });
    }

    /**
     * This method will verify the url signture before making a geocoding request. Here the method will accept location
     * for fetching location details.
     */
    @Test
    public void testGetLocationUrlSuburbWithLocationInput() {
        targetGeoLocationFinderUrlBuilderImpl.setLocationSearchBaseUrl(BASE_URL);
        final String url = targetGeoLocationFinderUrlBuilderImpl.getLocationUrlSuburb("Carnegie");
        final String expectedUrlSignature = composeExpectedUrl(
                "json?address=Carnegie&components=country:AU");
        assertThat(url).as("URL Signature").isEqualTo(expectedUrlSignature);
    }

    /**
     * This method will verify the url signture before making a geocoding request. Here the method will accept postcode
     * for fetching location details.
     */
    @Test
    public void testGetLocationUrlSuburbWithPostCodeInput() {
        targetGeoLocationFinderUrlBuilderImpl.setLocationSearchBaseUrl(BASE_URL);
        final String url = targetGeoLocationFinderUrlBuilderImpl.getLocationUrlSuburb("3145");
        final String expectedUrlSignature = composeExpectedUrl(
                "json?address=3145&components=country:AU");
        assertThat(url).as("URL Signature").isEqualTo(expectedUrlSignature);
    }

    /**
     * This method will verify the url signture before making a geocoding request. Here the method will accept latitude
     * and longitude for fetching location details.
     */
    @Test
    public void testGetLocationUrlLatLng() {
        targetGeoLocationFinderUrlBuilderImpl.setLocationSearchBaseUrl(BASE_URL);
        final Double latitude = Double.valueOf(-38.16047628099622);
        final Double longitude = Double.valueOf(144.3548583984375);
        final String url = targetGeoLocationFinderUrlBuilderImpl.getLocationUrlByCoordinate(latitude, longitude);
        final String expectedUrlSignature = composeExpectedUrl(
                "json?latlng=-38.16047628099622,144.3548583984375");
        assertThat(url).as("URL Signature").isEqualTo(expectedUrlSignature);
    }

    /**
     * Method will throw an excpetion if no parameter suppied while making a geocoding request.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetLocationUrlSuburbWithNullLocation() {
        targetGeoLocationFinderUrlBuilderImpl.getLocationUrlSuburb(StringUtils.EMPTY);
    }

    /**
     * Method will throw an excpetion if no parameter suppied while making a geocoding request.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetLocationUrlLatLngWithNullLatLng() {
        targetGeoLocationFinderUrlBuilderImpl.getLocationUrlByCoordinate(null, null);
    }

    /**
     * Returns the absolute URL composed from static prefix and suffix, and {@code expectedPart}.
     *
     * @param expectedPart
     *            the expected part of URL
     * @return the absolute URL
     */
    private String composeExpectedUrl(final String expectedPart) {
        return BASE_URL + expectedPart + SIGNER_SUFFIX;
    }


}

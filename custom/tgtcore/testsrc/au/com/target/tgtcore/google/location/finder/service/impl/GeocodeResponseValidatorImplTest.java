package au.com.target.tgtcore.google.location.finder.service.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Arrays;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.Test.None;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants.GeocodeExceptionMessage;
import au.com.target.tgtcore.constants.TgtCoreConstants.GeocodeStatus;
import au.com.target.tgtcore.google.location.finder.data.GeocodeGeometry;
import au.com.target.tgtcore.google.location.finder.data.GeocodeLocation;
import au.com.target.tgtcore.google.location.finder.data.GeocodeResponse;
import au.com.target.tgtcore.google.location.finder.data.GeocodeResultData;
import au.com.target.tgtcore.google.location.finder.exception.GeocodeResponseException;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GeocodeResponseValidatorImplTest {


    @Mock
    private GeocodeResponse response;

    @Mock
    private GeocodeResultData geocodeResultData;

    @Mock
    private GeocodeGeometry geometry;

    @Mock
    private GeocodeLocation location;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();


    @InjectMocks
    private final GeocodeResponseValidatorImpl geocodeResponseValidator = new GeocodeResponseValidatorImpl();

    @Test
    public void testValidateForZeroResultsResponse() {

        given(response.getResults()).willReturn(Arrays.asList(geocodeResultData));
        given(response.getStatus()).willReturn(GeocodeStatus.ZERO_RESULTS);
        expectedException.expect(GeocodeResponseException.class);
        expectedException.expectMessage(GeocodeExceptionMessage.ZERO_RESULTS);

        geocodeResponseValidator.validate(response);
    }

    @Test
    public void testValidateForOverDailyLimitStatusResponse() {

        given(response.getResults()).willReturn(Arrays.asList(geocodeResultData));
        given(response.getStatus()).willReturn(GeocodeStatus.OVER_DAILY_LIMIT);
        expectedException.expect(GeocodeResponseException.class);
        expectedException.expectMessage(GeocodeExceptionMessage.OVER_DAILY_LIMIT);

        geocodeResponseValidator.validate(response);
    }

    @Test
    public void testValidateForOverQueryLimitStatusResponse() {

        given(response.getResults()).willReturn(Arrays.asList(geocodeResultData));
        given(response.getStatus()).willReturn(GeocodeStatus.OVER_QUERY_LIMIT);
        expectedException.expect(GeocodeResponseException.class);
        expectedException.expectMessage(GeocodeExceptionMessage.OVER_QUERY_LIMIT);

        geocodeResponseValidator.validate(response);
    }

    @Test
    public void testValidateForRequestDenied() {

        given(response.getResults()).willReturn(Arrays.asList(geocodeResultData));
        given(response.getStatus()).willReturn(GeocodeStatus.REQUEST_DENIED);
        expectedException.expect(GeocodeResponseException.class);
        expectedException.expectMessage(GeocodeExceptionMessage.REQUEST_DENIED);

        geocodeResponseValidator.validate(response);
    }

    @Test
    public void testValidateForInvalidRequest() {

        given(response.getResults()).willReturn(Arrays.asList(geocodeResultData));
        given(response.getStatus()).willReturn(GeocodeStatus.INVALID_REQUEST);
        expectedException.expect(GeocodeResponseException.class);
        expectedException.expectMessage(GeocodeExceptionMessage.INVALID_REQUEST);

        geocodeResponseValidator.validate(response);
    }

    @Test
    public void testValidateForUnknownError() {

        given(response.getResults()).willReturn(Arrays.asList(geocodeResultData));
        given(response.getStatus()).willReturn(GeocodeStatus.UNKNOWN_ERROR);
        expectedException.expect(GeocodeResponseException.class);
        expectedException.expectMessage(GeocodeExceptionMessage.UNKNOWN_ERROR);

        geocodeResponseValidator.validate(response);
    }

    @Test
    public void testValidateForNoStatusMatch() {

        given(response.getResults()).willReturn(Arrays.asList(geocodeResultData));
        given(response.getStatus()).willReturn("no match");
        expectedException.expect(GeocodeResponseException.class);
        expectedException.expectMessage("ERROR: No matching geocode status");

        geocodeResponseValidator.validate(response);
    }

    @Test(expected = GeocodeResponseException.class)
    public void testValidateForNullResults() {
        given(response.getResults()).willReturn(null);

        geocodeResponseValidator.validate(response);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateForNullResponse() {
        given(response.getResults()).willReturn(null);

        geocodeResponseValidator.validate(null);
    }

    @Test(expected = GeocodeResponseException.class)
    public void testValidateForNullGeometryInResponse() {

        final List<GeocodeResultData> results = Arrays.asList(geocodeResultData);
        given(response.getResults()).willReturn(results);
        given(response.getStatus()).willReturn(GeocodeStatus.OK);
        given(geocodeResultData.getGeometry()).willReturn(null);

        geocodeResponseValidator.validate(response);
    }

    @Test(expected = GeocodeResponseException.class)
    public void testValidateForNullLocationInResponse() {

        final List<GeocodeResultData> results = Arrays.asList(geocodeResultData);
        given(response.getResults()).willReturn(results);
        given(response.getStatus()).willReturn(GeocodeStatus.OK);
        given(geocodeResultData.getGeometry()).willReturn(geometry);
        given(geometry.getLocation()).willReturn(null);
        geocodeResponseValidator.validate(response);
    }

    @Test(expected = None.class)
    public void testValidateForValidResponse() {

        final List<GeocodeResultData> results = Arrays.asList(geocodeResultData);
        given(response.getResults()).willReturn(results);
        given(response.getStatus()).willReturn(GeocodeStatus.OK);
        given(geocodeResultData.getGeometry()).willReturn(geometry);
        given(geometry.getLocation()).willReturn(location);

        geocodeResponseValidator.validate(response);
    }



}

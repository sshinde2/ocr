package au.com.target.tgtcore.google.location.finder.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.google.location.finder.route.TargetGeolocationFinderUrlBuilder;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetGeocodeLocationFinderHelperTest {

    private static final String BASE_URL = "http://maps.googleapis.com/maps/api/geocode/";

    private static final String SIGNER_SUFFIX = "&client=target&signature=cryptic_string";

    private static final Double LATITUDE = Double.valueOf(-38.16047628099622);

    private static final Double LONGITUDE = Double.valueOf(144.3548583984375);

    @Mock
    private AddressModel address;

    @Mock
    private CountryModel country;

    @Mock
    private TargetGeolocationFinderUrlBuilder targetGeolocationFinderUrlBuilder;

    @InjectMocks
    private final TargetGeocodeLocationFinderHelper targetGeocodeLocationFinderHelper = new TargetGeocodeLocationFinderHelper();


    @Test
    public void testGetUrlForAddressModel() throws MalformedURLException {

        final String urlString = BASE_URL
                + "json?address=12+Thompson+Road++3215+North+Geelong+VIC+AU&components=country:AU"
                + SIGNER_SUFFIX;
        final URL url = new URL(urlString);
        given(targetGeolocationFinderUrlBuilder.getLocationUrlSuburb(anyString())).willReturn(urlString);

        assertThat(targetGeocodeLocationFinderHelper.getUrl(address)).isEqualTo(url);

    }


    @Test(expected = MalformedURLException.class)
    public void testGetUrlForAddressModelWithMalformedURL() throws MalformedURLException {
        given(targetGeolocationFinderUrlBuilder.getLocationUrlSuburb(anyString())).willReturn(null);
        targetGeocodeLocationFinderHelper.getUrl(address);
    }


    @Test
    public void testGetUrlForLocation() throws MalformedURLException {
        final String urlString = BASE_URL
                + "/maps/api/geocode/json?address=3145&components=country:AU"
                + SIGNER_SUFFIX;
        final URL url = new URL(urlString);
        given(targetGeolocationFinderUrlBuilder.getLocationUrlSuburb(anyString())).willReturn(urlString);

        assertThat(targetGeocodeLocationFinderHelper.getUrl("3145", null, null)).isEqualTo(url);

    }

    @Test(expected = MalformedURLException.class)
    public void testGetUrlForLocationWithMalformedURL() throws MalformedURLException {
        given(targetGeolocationFinderUrlBuilder.getLocationUrlSuburb(anyString())).willReturn(null);
        targetGeocodeLocationFinderHelper.getUrl("3145", null, null);

    }

    @Test
    public void testGetUrlWithThreeParametersForLatitudeLongitude() throws MalformedURLException {
        final String urlString = BASE_URL
                + "/maps/api/geocode/json?latlng=-38.16047628099622,144.3548583984375"
                + SIGNER_SUFFIX;
        final URL url = new URL(urlString);
        given(targetGeolocationFinderUrlBuilder.getLocationUrlByCoordinate(LATITUDE, LONGITUDE)).willReturn(urlString);

        assertThat(targetGeocodeLocationFinderHelper.getUrl(null, LATITUDE, LONGITUDE)).isEqualTo(url);

    }


    @Test
    public void testGetUrlForLatitudeLongitude() throws MalformedURLException {
        final String urlString = BASE_URL
                + "/maps/api/geocode/json?latlng=-38.16047628099622,144.3548583984375"
                + SIGNER_SUFFIX;
        final URL url = new URL(urlString);
        given(targetGeolocationFinderUrlBuilder.getLocationUrlByCoordinate(LATITUDE, LONGITUDE)).willReturn(urlString);

        assertThat(targetGeocodeLocationFinderHelper.getUrl(LATITUDE, LONGITUDE)).isEqualTo(url);

    }

    @Test(expected = MalformedURLException.class)
    public void testGetUrlForLatitudeLongitudeMalformedURL() throws MalformedURLException {
        given(targetGeolocationFinderUrlBuilder.getLocationUrlByCoordinate(LATITUDE, LONGITUDE)).willReturn(null);
        targetGeocodeLocationFinderHelper.getUrl(LATITUDE, LONGITUDE);

    }

}

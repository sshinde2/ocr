/**
 * 
 */
package au.com.target.tgtcore.helper;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.returns.model.ReturnRequestModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.model.TargetConsignmentModel;


/**
 * @author bbaral1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ClickAndCollectOrderDetailsHelperTest {

    private static final String HOME_DELIVERY = "home-delivery";

    private final ClickAndCollectOrderDetailsHelper clickAndCollectOrderDetailsHelper = new ClickAndCollectOrderDetailsHelper();

    @Mock
    private OrderModel orderModelMock;

    @Mock
    private TargetConsignmentModel targetConsignmentModelMock1;

    @Mock
    private TargetConsignmentModel targetConsignmentModelMock2;

    @Mock
    private DeliveryModeModel deliveryModeModelMock;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Mock
    private OrderModificationRecordModel orderModificationRecordModelMock;

    @Mock
    private OrderModificationRecordEntryModel orderModificationRecordEntryModelMock;

    @Mock
    private ReturnRequestModel returnRequestModelMock;

    /**
     * Method will verify delivery mode of the order. Return true for the delivery mode click-and-collect.
     */
    @Test
    public void testIsCncDeliveryModeTrue() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INPROGRESS,
                ConsignmentStatus.CREATED,
                ConsignmentStatus.CREATED);
        assertThat(clickAndCollectOrderDetailsHelper.isCncDeliveryMode(orderModelMock)).isTrue();
    }

    /**
     * Method will verify delivery mode of the order. Return false for the delivery mode other than click-and-collect.
     */
    @Test
    public void testIsCncDeliveryModeFalse() {
        setCncOrderData(HOME_DELIVERY, OrderStatus.INPROGRESS,
                ConsignmentStatus.PACKED,
                ConsignmentStatus.CANCELLED);
        assertThat(clickAndCollectOrderDetailsHelper.isCncDeliveryMode(orderModelMock)).isFalse();
    }

    /**
     * Method will verify delivery mode of the order. Return true for the delivery mode other than click-and-collect.
     */
    @Test
    public void testIsNonCncDeliveryModeTrue() {
        setCncOrderData(HOME_DELIVERY, OrderStatus.INPROGRESS,
                ConsignmentStatus.CREATED,
                ConsignmentStatus.CREATED);
        assertThat(clickAndCollectOrderDetailsHelper.isNonCncDeliveryMode(orderModelMock)).isTrue();
    }

    /**
     * Method will verify delivery mode of the order. Return false for the delivery is click-and-collect delivery mode.
     */
    @Test
    public void testIsNonCncDeliveryModeFalse() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INPROGRESS,
                ConsignmentStatus.PACKED,
                ConsignmentStatus.CANCELLED);
        assertThat(clickAndCollectOrderDetailsHelper.isNonCncDeliveryMode(orderModelMock)).isFalse();
    }

    /**
     * Method verify and return empty order status.
     */
    @Test
    public void testOrderStatusEmpty() {
        setCncOrderData(HOME_DELIVERY, OrderStatus.INPROGRESS,
                ConsignmentStatus.PACKED,
                ConsignmentStatus.CANCELLED);
        assertThat(clickAndCollectOrderDetailsHelper.getCustomOrderStatus(orderModelMock)).isEmpty();
    }

    /**
     * Method will verify to placed order status when cnc order is placed with Created status.
     */
    @Test
    public void testOrderStatusCreated() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.CREATED,
                null, null);
        assertThat(clickAndCollectOrderDetailsHelper.getCustomOrderStatus(orderModelMock)).isEqualTo("placed");
    }

    /**
     * Method verify to confirmed order status when cnc order is placed with InProgress status with multiple
     * Consignments(one of it's consignment in Created Status).
     */
    @Test
    public void testOrderStatusWithConsignmentStatusCreated() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INPROGRESS,
                ConsignmentStatus.CREATED,
                ConsignmentStatus.CANCELLED);
        assertThat(clickAndCollectOrderDetailsHelper.getCustomOrderStatus(orderModelMock)).isEqualTo("confirmed");
    }

    /**
     * Method verify to confirmed order status when cnc order is placed with InProgress status with multiple
     * Consignments(one of it's consignment in Confirmed by Warehouse Status).
     */
    @Test
    public void testOrderStatusWithConsignmentStatusConfirmedByWareHouse() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INPROGRESS,
                ConsignmentStatus.CONFIRMED_BY_WAREHOUSE,
                ConsignmentStatus.CANCELLED);
        assertThat(clickAndCollectOrderDetailsHelper.getCustomOrderStatus(orderModelMock)).isEqualTo("confirmed");
    }

    /**
     * Method verify to confirmed order status when cnc order is placed with InProgress status with multiple
     * Consignments(one of it's consignment in Sent to Warehouse Status).
     */
    @Test
    public void testOrderStatusWithConsignmentStatusSentToWareHouse() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INPROGRESS,
                ConsignmentStatus.SENT_TO_WAREHOUSE,
                ConsignmentStatus.CANCELLED);
        assertThat(clickAndCollectOrderDetailsHelper.getCustomOrderStatus(orderModelMock)).isEqualTo("confirmed");
    }


    /**
     * Method verify to invoiced order status when cnc order is placed with InProgress status with multiple
     * Consignments(packed).
     */
    @Test
    public void testOrderStatusInvoiced() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INVOICED,
                ConsignmentStatus.PACKED,
                ConsignmentStatus.CREATED);
        assertThat(clickAndCollectOrderDetailsHelper.getCustomOrderStatus(orderModelMock)).isEqualTo("inprogress");
    }

    /**
     * Method verify to inprogress order status when cnc order is placed with InProgress status with multiple
     * Consignments.
     */
    @Test
    public void testOrderStatusInProgress() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INPROGRESS,
                ConsignmentStatus.PACKED,
                ConsignmentStatus.CANCELLED);
        assertThat(clickAndCollectOrderDetailsHelper.getCustomOrderStatus(orderModelMock)).isEqualTo("inprogress");
    }

    /**
     * Method verify to inprogress order status when cnc order is placed with InProgress status with multiple
     * Consignments.
     */
    @Test
    public void testOrderStatusInProgressWithConsignmentInCreatedWavedPickedState() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INPROGRESS,
                ConsignmentStatus.PACKED,
                ConsignmentStatus.CANCELLED);
        assertThat(clickAndCollectOrderDetailsHelper.getCustomOrderStatus(orderModelMock)).isEqualTo("inprogress");
    }

    /**
     * Method verify to "completed" custom order status when cnc order is placed. Subject to Completed order status and
     * Picked Up Date is available for the given consignment.
     *
     * @throws ParseException
     */
    @Test
    public void testOrderStatusCompletedWithConsignmentPickedUpDate() throws ParseException {
        setCncCompletedOrderData(OrderStatus.COMPLETED,
                ConsignmentStatus.SHIPPED, "17/04/1983");
        assertThat(clickAndCollectOrderDetailsHelper.getCustomOrderStatus(orderModelMock)).isEqualTo("completed");
    }

    /**
     * Method verify to "inprogress" custom order status when cnc order is placed. Subject to Completed order status and
     * Ready for Pick Up Date is not available for the given consignment(s).
     *
     * @throws ParseException
     */
    @Test
    public void testOrderStatusCompletedWithConsignmentPacked() throws ParseException {
        given(orderModelMock.getCode()).willReturn("00078888");
        given(orderModelMock.getStatus()).willReturn(OrderStatus.COMPLETED);
        given(targetConsignmentModelMock1.getTrackingID()).willReturn("Track007");
        given(targetConsignmentModelMock1.getCode()).willReturn("Consignment_1");
        given(targetConsignmentModelMock1.getStatus()).willReturn(ConsignmentStatus.PACKED);
        final Set<ConsignmentModel> consignmentModels = new HashSet<>();
        consignmentModels.add(targetConsignmentModelMock1);
        given(orderModelMock.getConsignments()).willReturn(consignmentModels);
        given(deliveryModeModelMock.getCode()).willReturn(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT);
        given(orderModelMock.getDeliveryMode()).willReturn(deliveryModeModelMock);
        assertThat(clickAndCollectOrderDetailsHelper.getCustomOrderStatus(orderModelMock)).isEqualTo("inprogress");
    }

    /**
     * Method verify to "completed" custom order status when cnc order is placed. Subject to Completed order status,
     * Ready for pick up date is available and Picked Up Date is not available for the given consignment(s).
     *
     * @throws ParseException
     */
    @Test
    public void testOrderStatusCompletedWithConsignmentReadyForPickedUpDateAndNoPickedUpDate() throws ParseException {
        given(orderModelMock.getCode()).willReturn("00078888");
        given(orderModelMock.getStatus()).willReturn(OrderStatus.COMPLETED);
        given(orderModelMock.getSubtotal()).willReturn(Double.valueOf(100.0));
        given(targetConsignmentModelMock1.getTrackingID()).willReturn("Track007");
        given(targetConsignmentModelMock1.getCode()).willReturn("Consignment_1");
        given(targetConsignmentModelMock1.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        final Date readyForPickUpDate = simpleDateFormat.parse("17/04/1983");
        given(targetConsignmentModelMock1.getReadyForPickUpDate()).willReturn(readyForPickUpDate);
        final Set<ConsignmentModel> consignmentModels = new HashSet<>();
        consignmentModels.add(targetConsignmentModelMock1);
        given(orderModelMock.getConsignments()).willReturn(consignmentModels);
        given(deliveryModeModelMock.getCode()).willReturn(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT);
        given(orderModelMock.getDeliveryMode()).willReturn(deliveryModeModelMock);
        assertThat(clickAndCollectOrderDetailsHelper.getCustomOrderStatus(orderModelMock)).as("Custom Order Status")
                .isEqualTo("readyToCollect");
    }

    /**
     * Method verify to "refunded" custom order status when cnc order is placed. Subject to Completed order status,
     * Ready for pick up date is available, Picked Up Date is not available and Order Subtotal is 0 for the given
     * consignment(s).
     *
     * @throws ParseException
     */
    @Test
    public void testOrderStatusRefunded() throws ParseException {
        given(orderModelMock.getCode()).willReturn("00078888");
        given(orderModelMock.getStatus()).willReturn(OrderStatus.COMPLETED);
        given(orderModelMock.getSubtotal()).willReturn(Double.valueOf(0.0));
        given(targetConsignmentModelMock1.getTrackingID()).willReturn("Track007");
        given(targetConsignmentModelMock1.getCode()).willReturn("Consignment_1");
        given(targetConsignmentModelMock1.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        final Date readyForPickUpDate = simpleDateFormat.parse("17/04/1983");
        given(targetConsignmentModelMock1.getReadyForPickUpDate()).willReturn(readyForPickUpDate);
        final Set<ConsignmentModel> consignmentModels = new HashSet<>();
        consignmentModels.add(targetConsignmentModelMock1);
        given(orderModelMock.getConsignments()).willReturn(consignmentModels);
        given(deliveryModeModelMock.getCode()).willReturn(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT);
        given(orderModelMock.getDeliveryMode()).willReturn(deliveryModeModelMock);
        assertThat(clickAndCollectOrderDetailsHelper.getCustomOrderStatus(orderModelMock)).as("Custom Order Status")
                .isEqualTo("refunded");
    }


    /**
     * Method will verify to true for cnc for inprogress order with Packed/cancelled consignments.
     */
    @Test
    public void testIsInProgressOrder() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INPROGRESS,
                ConsignmentStatus.PACKED,
                ConsignmentStatus.CANCELLED);
        assertThat(clickAndCollectOrderDetailsHelper.isNonCncNonInvoicedOrder(orderModelMock)).isTrue();
    }


    /**
     * Method will verify to false for cnc for invoiced order with Packed/cancelled consignments.
     */
    @Test
    public void testIsNonCncNonInvoicedOrderFalse() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INVOICED,
                ConsignmentStatus.PACKED,
                ConsignmentStatus.CANCELLED);
        assertThat(clickAndCollectOrderDetailsHelper.isNonCncNonInvoicedOrder(orderModelMock)).isFalse();
    }


    /**
     * Method will verify to true for cnc for invoiced order with Packed/cancelled consignments.
     */
    @Test
    public void testIsNonCncNonInvoicedOrderFalseTrue() {
        setCncOrderData(HOME_DELIVERY, OrderStatus.INVOICED,
                ConsignmentStatus.PACKED,
                ConsignmentStatus.CANCELLED);
        assertThat(clickAndCollectOrderDetailsHelper.isNonCncNonInvoicedOrder(orderModelMock)).isTrue();
    }

    /**
     * Verifying if consignment list is empty.
     */
    @Test
    public void testOrderStatusEmptyConsignmentList() {
        final OrderModel orderModel = mock(OrderModel.class);
        given(orderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);
        final DeliveryModeModel deliveryModeModel = new DeliveryModeModel();
        deliveryModeModel.setCode(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT);
        orderModel.setDeliveryMode(deliveryModeModel);
        given(orderModel.getConsignments()).willReturn(Collections.EMPTY_SET);
        assertThat(clickAndCollectOrderDetailsHelper.getCustomOrderStatus(orderModel))
                .isEmpty();
    }

    /**
     * Method will verify to true when cnc order is placed with Created status.
     */
    @Test
    public void testIsOrderCreatedStatus() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.CREATED,
                null, null);
        assertThat(clickAndCollectOrderDetailsHelper.isOrderInCreatedStatus(orderModelMock)).isTrue();
    }

    /**
     * Method will verify to true when cnc order is placed and changed to In Progress status.
     */
    @Test
    public void testIsOrderNotInCreatedStatus() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INPROGRESS,
                null, null);
        assertThat(clickAndCollectOrderDetailsHelper.isOrderInCreatedStatus(orderModelMock)).isFalse();
    }


    /**
     * Method will verify to true when cnc order with multiple Consignments when one consignment in created status.
     */
    @Test
    public void testIsConsignmentInCreatedStatus() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INPROGRESS,
                ConsignmentStatus.CREATED,
                ConsignmentStatus.CANCELLED);
        assertThat(clickAndCollectOrderDetailsHelper.isConsignmentConfirmedByWarehouse(orderModelMock)).isTrue();
    }

    /**
     * Method will verify to true when cnc order with multiple Consignments when one consignment in Confirmed by
     * Warehouse status.
     */
    @Test
    public void testIsConsignmentInConfirmedByWareHouseStatus() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INPROGRESS,
                ConsignmentStatus.CONFIRMED_BY_WAREHOUSE,
                ConsignmentStatus.CANCELLED);
        assertThat(clickAndCollectOrderDetailsHelper.isConsignmentConfirmedByWarehouse(orderModelMock)).isTrue();
    }

    /**
     * Method will verify to true when cnc order with multiple Consignments when one consignment in Sent to Warehouse
     * status.
     */
    @Test
    public void testIsConsignmentInSentToWareHouseStatus() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INPROGRESS,
                ConsignmentStatus.SENT_TO_WAREHOUSE,
                ConsignmentStatus.CANCELLED);
        assertThat(clickAndCollectOrderDetailsHelper.isConsignmentConfirmedByWarehouse(orderModelMock)).isTrue();
    }

    /**
     * Method will verify to false when cnc order with empty consignment list.
     */
    @Test
    public void testIsConsignmentInCreatedStatusWithEmptyConsignmentList() {
        final OrderModel orderModel = new OrderModel();
        orderModel.setConsignments(Collections.EMPTY_SET);
        assertThat(clickAndCollectOrderDetailsHelper.isConsignmentConfirmedByWarehouse(orderModelMock)).isFalse();
    }


    /**
     * Method will verify to true when cnc order with multiple Consignments with none consignment in Created status.
     */
    @Test
    public void testIsAnyConsignmentInCreatedStatusFalse() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INPROGRESS,
                ConsignmentStatus.CANCELLED,
                ConsignmentStatus.CANCELLED);
        assertThat(clickAndCollectOrderDetailsHelper.isConsignmentConfirmedByWarehouse(orderModelMock)).isFalse();
    }


    /**
     * Method will verify to true for cnc order with terminal states for respective consignments.
     */
    @Test
    public void testIsAnyConsignmentInTerminalStateTrue() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INPROGRESS,
                ConsignmentStatus.PACKED,
                ConsignmentStatus.CANCELLED);
        assertThat(clickAndCollectOrderDetailsHelper.isConsignmentInTerminalState(orderModelMock)).isTrue();
    }

    /**
     * Method will verify to true for cnc order with Packed/Shipped for consignments.
     */
    @Test
    public void testIsAnyConsignmentInPackedOrShippedWhenConsignmentPacked() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INVOICED,
                ConsignmentStatus.PACKED,
                ConsignmentStatus.CREATED);
        assertThat(clickAndCollectOrderDetailsHelper.isConsignmentInPackedOrShippedStatus(orderModelMock)).isTrue();
    }

    /**
     * Method will verify to true for cnc order with Packed/Shipped for consignments.
     */
    @Test
    public void testIsAnyConsignmentInPackedOrShippedWhenConsignmentShipped() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INVOICED,
                ConsignmentStatus.SHIPPED, ConsignmentStatus.CANCELLED);
        assertThat(clickAndCollectOrderDetailsHelper.isConsignmentInPackedOrShippedStatus(orderModelMock)).isTrue();
    }

    /**
     * Method will verify to true for cnc order with Packed/Shipped for consignments.
     */
    @Test
    public void testIsAnyConsignmentInPackedOrShippedWhenStatusEmpty() {
        final OrderModel orderModel = new OrderModel();
        orderModel.setConsignments(Collections.EMPTY_SET);
        assertThat(clickAndCollectOrderDetailsHelper.isConsignmentInPackedOrShippedStatus(orderModelMock)).isFalse();
    }


    /**
     * Method will verify to false for cnc order with non-terminal states for respective consignments.
     */
    @Test
    public void testIsAnyConsignmentInTerminalStateFalse() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INPROGRESS,
                ConsignmentStatus.WAVED,
                ConsignmentStatus.WAITING);
        assertThat(clickAndCollectOrderDetailsHelper.isConsignmentInTerminalState(orderModelMock)).isFalse();
    }

    /**
     * Method will verify to false for cnc order with empty consignments.
     */
    @Test
    public void testIsAnyConsignmentInTerminalStateEmptyList() {
        final OrderModel orderModel = new OrderModel();
        orderModel.setConsignments(Collections.EMPTY_SET);
        assertThat(clickAndCollectOrderDetailsHelper.isConsignmentInTerminalState(orderModelMock)).isFalse();
    }

    /**
     * Method will verify to false for cnc order with non - Packed/Shipped for consignments.
     */
    @Test
    public void testIsAnyConsignmentInPackedOrShippedFalse() {
        setCncOrderData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT,
                OrderStatus.INVOICED,
                ConsignmentStatus.WAITING,
                ConsignmentStatus.WAVED);
        assertThat(clickAndCollectOrderDetailsHelper.isConsignmentInPackedOrShippedStatus(orderModelMock)).isFalse();
    }

    /**
     * Method will verify to true for completed cnc order with picked up date available.
     *
     * @throws ParseException
     */
    @Test
    public void testIsOrderCompletedWithConsignmentPickedUpDateAvailable() throws ParseException {
        setCncCompletedOrderData(OrderStatus.COMPLETED,
                ConsignmentStatus.SHIPPED, "17/04/1983");
        assertThat(clickAndCollectOrderDetailsHelper.isOrderCompletedWithConsignmentPickedUpDate(orderModelMock))
                .isTrue();
    }

    /**
     * Method will verify to false for completed cnc order with picked up date not available.
     *
     * @throws ParseException
     */
    @Test
    public void testIsOrderCompletedWithConsignmentPickedUpDateNotAvailable() throws ParseException {
        given(orderModelMock.getCode()).willReturn("00078888");
        given(orderModelMock.getStatus()).willReturn(OrderStatus.COMPLETED);
        given(orderModelMock.getSubtotal()).willReturn(Double.valueOf(100.0));
        given(targetConsignmentModelMock1.getTrackingID()).willReturn("Track007");
        given(targetConsignmentModelMock1.getCode()).willReturn("Consignment_1");
        given(targetConsignmentModelMock1.getStatus()).willReturn(ConsignmentStatus.PACKED);
        final Set<ConsignmentModel> consignmentModels = new HashSet<>();
        consignmentModels.add(targetConsignmentModelMock1);
        given(orderModelMock.getConsignments()).willReturn(consignmentModels);
        given(deliveryModeModelMock.getCode()).willReturn(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT);
        given(orderModelMock.getDeliveryMode()).willReturn(deliveryModeModelMock);
        assertThat(clickAndCollectOrderDetailsHelper.isOrderCompletedWithConsignmentPickedUpDate(orderModelMock))
                .isFalse();
    }

    /**
     * Method will verify to false for completed cnc order with with empty consignment list.
     */
    @Test
    public void testIsOrderCompletedWithConsignmentPickedUpDateEmptyConsignment() {
        final OrderModel orderModel = new OrderModel();
        orderModel.setStatus(OrderStatus.COMPLETED);
        orderModel.setConsignments(Collections.EMPTY_SET);
        assertThat(clickAndCollectOrderDetailsHelper.isOrderCompletedWithConsignmentPickedUpDate(orderModel))
                .isFalse();
    }

    /**
     * Method will verify to true for completed cnc order with ready for pick up date available and picked up date not
     * available.
     *
     * @throws ParseException
     */
    @Test
    public void testisOrderReadyForPickupTrue() throws ParseException {
        given(orderModelMock.getCode()).willReturn("00078888");
        given(orderModelMock.getStatus()).willReturn(OrderStatus.COMPLETED);
        given(orderModelMock.getSubtotal()).willReturn(Double.valueOf(0.0));
        given(targetConsignmentModelMock1.getTrackingID()).willReturn("Track007");
        given(targetConsignmentModelMock1.getCode()).willReturn("Consignment_1");
        given(targetConsignmentModelMock1.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        final Date readyForPickUpDate = simpleDateFormat.parse("17/04/1983");
        given(targetConsignmentModelMock1.getReadyForPickUpDate()).willReturn(readyForPickUpDate);
        given(targetConsignmentModelMock2.getTrackingID()).willReturn("Track008");
        given(targetConsignmentModelMock2.getCode()).willReturn("Consignment_2");
        given(targetConsignmentModelMock2.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        final Set<ConsignmentModel> consignmentModels = new HashSet<>();
        consignmentModels.add(targetConsignmentModelMock1);
        consignmentModels.add(targetConsignmentModelMock2);
        given(orderModelMock.getConsignments()).willReturn(consignmentModels);
        given(deliveryModeModelMock.getCode()).willReturn(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT);
        given(orderModelMock.getDeliveryMode()).willReturn(deliveryModeModelMock);
        assertThat(clickAndCollectOrderDetailsHelper.isOrderReadyForPickup(orderModelMock)).isTrue();
    }

    /**
     * Method will verify to false for completed cnc order with ready for pick up date available and picked up date
     * available.
     *
     * @throws ParseException
     */
    @Test
    public void testIsOrderReadyForPickupFalse() throws ParseException {
        given(orderModelMock.getCode()).willReturn("00078888");
        given(orderModelMock.getStatus()).willReturn(OrderStatus.COMPLETED);
        given(targetConsignmentModelMock1.getTrackingID()).willReturn("Track007");
        given(targetConsignmentModelMock1.getCode()).willReturn("Consignment_1");
        given(targetConsignmentModelMock1.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        final Date readyForPickUpDate = simpleDateFormat.parse("17/04/1983");
        final Date pickedUpDate = simpleDateFormat.parse("18/04/1983");
        given(targetConsignmentModelMock1.getReadyForPickUpDate()).willReturn(readyForPickUpDate);
        given(targetConsignmentModelMock1.getPickedUpDate()).willReturn(pickedUpDate);
        final Set<ConsignmentModel> consignmentModels = new HashSet<>();
        consignmentModels.add(targetConsignmentModelMock1);
        consignmentModels.add(targetConsignmentModelMock2);
        given(orderModelMock.getConsignments()).willReturn(consignmentModels);
        given(deliveryModeModelMock.getCode()).willReturn(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT);
        given(orderModelMock.getDeliveryMode()).willReturn(deliveryModeModelMock);
        assertThat(clickAndCollectOrderDetailsHelper.isOrderReadyForPickup(orderModelMock)).isFalse();
    }

    /**
     * Method will verify to false for completed cnc order with with empty consignment list.
     */
    @Test
    public void testIsOrderReadyForPickupEmptyConsignmentList() {
        final OrderModel orderModel = new OrderModel();
        orderModel.setStatus(OrderStatus.COMPLETED);
        orderModel.setConsignments(Collections.EMPTY_SET);
        assertThat(clickAndCollectOrderDetailsHelper.isOrderReadyForPickup(orderModel)).isFalse();
    }

    /**
     * Method will verify to true subject to conditions: 1. OrderStatus = Completed 2. Ready For Pick Up date is
     * available 3. Picked Up Date not available 4. OrderTotal = 0
     * 
     * @throws ParseException
     */
    @Test
    public void testIsOrderRefundedTrue() throws ParseException {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        final Date readyForPickUpDate = simpleDateFormat.parse("17/04/1983");
        setCncRefundedOrderData(OrderStatus.COMPLETED, readyForPickUpDate, null,
                Double.valueOf(0.0));
        assertThat(clickAndCollectOrderDetailsHelper.isOrderRefunded(orderModelMock)).as("Order Refunded?").isTrue();
    }

    /**
     * Method will verify to false subject to conditions: 1. OrderStatus = Completed 2. Ready For Pick Up date is
     * available 3. Picked Up Date not available 4. OrderTotal = 10
     * 
     * @throws ParseException
     */
    @Test
    public void testIsOrderRefundedFalse() throws ParseException {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        final Date pickedUpDate = simpleDateFormat.parse("17/04/1983");
        setCncRefundedOrderData(OrderStatus.COMPLETED, null, pickedUpDate,
                Double.valueOf(100.0));
        assertThat(clickAndCollectOrderDetailsHelper.isOrderRefunded(orderModelMock)).as("Order Refunded?").isFalse();
    }

    /**
     * Method will verify to true for completed cnc order with ready for pick up date not available
     *
     * @throws ParseException
     */
    @Test
    public void testIsCompleteOrderConsignmentInPackedOrShippedStatusTrue() throws ParseException {
        given(orderModelMock.getCode()).willReturn("00078888");
        given(orderModelMock.getStatus()).willReturn(OrderStatus.COMPLETED);
        given(targetConsignmentModelMock1.getTrackingID()).willReturn("Track007");
        given(targetConsignmentModelMock1.getCode()).willReturn("Consignment_1");
        given(targetConsignmentModelMock1.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        final Set<ConsignmentModel> consignmentModels = new HashSet<>();
        consignmentModels.add(targetConsignmentModelMock1);
        given(orderModelMock.getConsignments()).willReturn(consignmentModels);
        given(deliveryModeModelMock.getCode()).willReturn(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT);
        given(orderModelMock.getDeliveryMode()).willReturn(deliveryModeModelMock);
        assertThat(clickAndCollectOrderDetailsHelper.isCompleteOrderConsignmentInPackedOrShippedStatus(orderModelMock))
                .isTrue();
    }

    /**
     * Method will verify to true for completed cnc order with ready for pick up date available
     *
     * @throws ParseException
     */
    @Test
    public void testIsCompleteOrderConsignmentInPackedOrShippedStatusFalse() throws ParseException {
        given(orderModelMock.getCode()).willReturn("00078888");
        given(orderModelMock.getStatus()).willReturn(OrderStatus.COMPLETED);
        given(targetConsignmentModelMock1.getTrackingID()).willReturn("Track007");
        given(targetConsignmentModelMock1.getCode()).willReturn("Consignment_1");
        given(targetConsignmentModelMock1.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        final Date readyForPickUpDate = simpleDateFormat.parse("17/04/1983");
        given(targetConsignmentModelMock1.getReadyForPickUpDate()).willReturn(readyForPickUpDate);
        final Set<ConsignmentModel> consignmentModels = new HashSet<>();
        consignmentModels.add(targetConsignmentModelMock1);
        given(orderModelMock.getConsignments()).willReturn(consignmentModels);
        given(deliveryModeModelMock.getCode()).willReturn(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT);
        given(orderModelMock.getDeliveryMode()).willReturn(deliveryModeModelMock);
        assertThat(clickAndCollectOrderDetailsHelper.isCompleteOrderConsignmentInPackedOrShippedStatus(orderModelMock))
                .isFalse();
    }

    /**
     * Method will verify to true for InProgress cnc order with waived consignment status
     *
     */
    @Test
    public void testIsConsignmentInCreatedWavedPickedStateTrue() {
        given(orderModelMock.getCode()).willReturn("00078888");
        given(orderModelMock.getStatus()).willReturn(OrderStatus.INPROGRESS);
        given(targetConsignmentModelMock1.getTrackingID()).willReturn("Track007");
        given(targetConsignmentModelMock1.getCode()).willReturn("Consignment_1");
        given(targetConsignmentModelMock1.getStatus()).willReturn(ConsignmentStatus.WAVED);
        final Set<ConsignmentModel> consignmentModels = new HashSet<>();
        consignmentModels.add(targetConsignmentModelMock1);
        given(orderModelMock.getConsignments()).willReturn(consignmentModels);
        given(deliveryModeModelMock.getCode()).willReturn(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT);
        given(orderModelMock.getDeliveryMode()).willReturn(deliveryModeModelMock);
        assertThat(clickAndCollectOrderDetailsHelper.isConsignmentInCreatedWavedPickedState(orderModelMock))
                .isTrue();
    }

    /**
     * Method will verify to false for Completed cnc order with picked consignment status.
     *
     */
    @Test
    public void testIsConsignmentInCreatedWavedPickedStateFalse() {
        given(orderModelMock.getCode()).willReturn("00078888");
        given(orderModelMock.getStatus()).willReturn(OrderStatus.COMPLETED);
        given(targetConsignmentModelMock1.getTrackingID()).willReturn("Track007");
        given(targetConsignmentModelMock1.getCode()).willReturn("Consignment_1");
        given(targetConsignmentModelMock1.getStatus()).willReturn(ConsignmentStatus.PICKED);
        final Set<ConsignmentModel> consignmentModels = new HashSet<>();
        consignmentModels.add(targetConsignmentModelMock1);
        given(orderModelMock.getConsignments()).willReturn(consignmentModels);
        given(deliveryModeModelMock.getCode()).willReturn(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT);
        given(orderModelMock.getDeliveryMode()).willReturn(deliveryModeModelMock);
        assertThat(clickAndCollectOrderDetailsHelper.isConsignmentInCreatedWavedPickedState(orderModelMock))
                .isFalse();
    }

    /**
     * Method will verify to completed if the order partially refunded.
     *
     */
    @Test
    public void testGetCustomOrderStatusIsCncOrderPartialRefundTrue() {
        final List<ReturnRequestModel> returnRequestModels = new ArrayList<>();
        given(returnRequestModelMock.getCode()).willReturn("P1000");
        returnRequestModels.add(returnRequestModelMock);
        setCncPartialRefundData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT, OrderStatus.COMPLETED,
                returnRequestModels);
        assertThat(clickAndCollectOrderDetailsHelper.getCustomOrderStatus(orderModelMock)).isEqualTo("completed");
    }

    /**
     * Method will verify to true for if the order partially refunded.
     *
     */
    @Test
    public void testIsCncOrderPartialRefundTrue() {
        final List<ReturnRequestModel> returnRequestModels = new ArrayList<>();
        given(returnRequestModelMock.getCode()).willReturn("P1000");
        returnRequestModels.add(returnRequestModelMock);
        setCncPartialRefundData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT, OrderStatus.COMPLETED,
                returnRequestModels);
        assertThat(clickAndCollectOrderDetailsHelper.isCncOrderPartialRefund(orderModelMock))
                .isTrue();
    }

    /**
     * Method will verify to false for if the order not partial refunded.
     *
     */
    @Test
    public void testIsCncOrderPartialRefundFalse() {
        final List<ReturnRequestModel> returnRequestModels = new ArrayList<>();
        setCncPartialRefundData(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT, OrderStatus.COMPLETED,
                returnRequestModels);
        assertThat(clickAndCollectOrderDetailsHelper.isCncOrderPartialRefund(orderModelMock))
                .isFalse();
    }

    /**
     * Setting Cnc refunded order data.
     * 
     * @param orderStatus
     * @param readyForPickupDate
     * @param pickedUpDate
     * @param subTotal
     */
    private void setCncRefundedOrderData(final OrderStatus orderStatus, final Date readyForPickupDate,
            final Date pickedUpDate, final Double subTotal) {
        given(orderModelMock.getCode()).willReturn("00024444");
        given(orderModelMock.getStatus()).willReturn(orderStatus);
        given(orderModelMock.getSubtotal()).willReturn(subTotal);
        given(targetConsignmentModelMock1.getTrackingID()).willReturn("Track001");
        given(targetConsignmentModelMock1.getCode()).willReturn("Consignment_1");
        given(targetConsignmentModelMock1.getStatus()).willReturn(ConsignmentStatus.WAITING);
        given(targetConsignmentModelMock1.getReadyForPickUpDate()).willReturn(readyForPickupDate);
        given(targetConsignmentModelMock1.getPickedUpDate()).willReturn(pickedUpDate);
        given(targetConsignmentModelMock2.getTrackingID()).willReturn("Track002");
        given(targetConsignmentModelMock2.getCode()).willReturn("Consignment_2");
        given(targetConsignmentModelMock2.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        final Set<ConsignmentModel> consignmentModels = new HashSet<>();
        consignmentModels.add(targetConsignmentModelMock1);
        consignmentModels.add(targetConsignmentModelMock2);
        given(orderModelMock.getConsignments()).willReturn(consignmentModels);
        given(deliveryModeModelMock.getCode()).willReturn(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT);
        given(orderModelMock.getDeliveryMode()).willReturn(deliveryModeModelMock);
    }

    /**
     * Setting Cnc order data to verify multiple scenarios.
     * 
     */
    private void setCncOrderData(final String deliveryMode, final OrderStatus orderStatus,
            final ConsignmentStatus consignmentStatus1, final ConsignmentStatus consignmentStatus2) {
        given(orderModelMock.getCode()).willReturn("00024444");
        given(orderModelMock.getStatus()).willReturn(orderStatus);
        given(targetConsignmentModelMock1.getTrackingID()).willReturn("Track001");
        given(targetConsignmentModelMock1.getCode()).willReturn("Consignment_1");
        given(targetConsignmentModelMock1.getStatus()).willReturn(consignmentStatus1);
        given(targetConsignmentModelMock2.getTrackingID()).willReturn("Track002");
        given(targetConsignmentModelMock2.getCode()).willReturn("Consignment_2");
        given(targetConsignmentModelMock2.getStatus()).willReturn(consignmentStatus2);
        final Set<ConsignmentModel> consignmentModels = new HashSet<>();
        consignmentModels.add(targetConsignmentModelMock1);
        consignmentModels.add(targetConsignmentModelMock2);
        given(orderModelMock.getConsignments()).willReturn(consignmentModels);
        given(deliveryModeModelMock.getCode()).willReturn(deliveryMode);
        given(orderModelMock.getDeliveryMode()).willReturn(deliveryModeModelMock);
    }

    /**
     * Setting Cnc order data to verify multiple scenarios.
     *
     * @throws ParseException
     */
    private void setCncCompletedOrderData(final OrderStatus orderStatus,
            final ConsignmentStatus consignmentStatus, final String pickedUpDateValue) throws ParseException {
        given(orderModelMock.getCode()).willReturn("00078888");
        given(orderModelMock.getStatus()).willReturn(orderStatus);
        given(targetConsignmentModelMock1.getTrackingID()).willReturn("Track007");
        given(targetConsignmentModelMock1.getCode()).willReturn("Consignment_1");
        given(targetConsignmentModelMock1.getStatus()).willReturn(consignmentStatus);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        final Date pickedUpDate = simpleDateFormat.parse(pickedUpDateValue);
        given(targetConsignmentModelMock1.getPickedUpDate()).willReturn(pickedUpDate);
        final Set<ConsignmentModel> consignmentModels = new HashSet<>();
        consignmentModels.add(targetConsignmentModelMock1);
        given(orderModelMock.getConsignments()).willReturn(consignmentModels);
        given(deliveryModeModelMock.getCode()).willReturn(TgtCoreConstants.DeliveryMode.CLICK_AND_COLLECT);
        given(orderModelMock.getDeliveryMode()).willReturn(deliveryModeModelMock);
    }

    /**
     * Method to set partial refund data for cnc order.
     *
     * @param deliveryMode
     * @param orderStatus
     * @param returnRequestModels
     */
    public void setCncPartialRefundData(final String deliveryMode, final OrderStatus orderStatus,
            final List<ReturnRequestModel> returnRequestModels) {
        final Double refundAmount = new Double(10.0);
        given(orderModelMock.getCode()).willReturn("00078888");
        given(orderModelMock.getSubtotal()).willReturn(new Double(100.0));
        given(orderModelMock.getStatus()).willReturn(orderStatus);
        given(targetConsignmentModelMock1.getTrackingID()).willReturn("Track007");
        given(targetConsignmentModelMock1.getCode()).willReturn("Consignment_1");
        given(targetConsignmentModelMock2.getTrackingID()).willReturn("Track008");
        given(targetConsignmentModelMock2.getCode()).willReturn("Consignment_2");
        final Set<ConsignmentModel> consignmentModels = new HashSet<>();
        consignmentModels.add(targetConsignmentModelMock1);
        consignmentModels.add(targetConsignmentModelMock2);
        given(orderModelMock.getConsignments()).willReturn(consignmentModels);
        given(deliveryModeModelMock.getCode()).willReturn(deliveryMode);
        given(orderModelMock.getDeliveryMode()).willReturn(deliveryModeModelMock);
        final Set<OrderModificationRecordModel> orderModificationRecordModels = new HashSet<>();
        given(orderModificationRecordModelMock.getIdentifier()).willReturn("Normal_Product_Item");
        final Collection<OrderModificationRecordEntryModel> collection = new ArrayList<>();
        given(orderModificationRecordEntryModelMock.getRefundedAmount()).willReturn(refundAmount);
        collection.add(orderModificationRecordEntryModelMock);
        given(orderModificationRecordModelMock.getModificationRecordEntries()).willReturn(collection);
        orderModificationRecordModels.add(orderModificationRecordModelMock);
        given(orderModelMock.getModificationRecords()).willReturn(orderModificationRecordModels);
        given(orderModelMock.getReturnRequests()).willReturn(returnRequestModels);
    }

}

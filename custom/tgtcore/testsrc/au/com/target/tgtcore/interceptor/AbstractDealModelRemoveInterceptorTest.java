package au.com.target.tgtcore.interceptor;

import static org.junit.Assert.fail;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.time.TimeService;

import java.util.Date;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AbstractDealModelRemoveInterceptorTest {

    //CHECKSTYLE:OFF

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @Spy
    private final AbstractDealModelRemoveInterceptor dealModelRemoveInterceptor = new AbstractDealModelRemoveInterceptor();

    @Mock
    private TimeService timeService;

    @Mock
    private InterceptorContext interceptorContext;

    @Mock
    private AbstractDealModel dealModel;

    @Before
    public void setUp() {
        BDDMockito.willReturn(timeService).given(dealModelRemoveInterceptor).getTimeService();
    }

    @Test
    public void testOnRemoveForNullStartDate() throws InterceptorException {
        BDDMockito.given(dealModel.getStartDate()).willReturn(null);
        expectedException.expect(InterceptorException.class);
        dealModelRemoveInterceptor.onRemove(dealModel, interceptorContext);
    }

    @Test
    public void testOnRemoveForPastStartDate() throws InterceptorException {

        final DateTime now = new DateTime();
        BDDMockito.given(timeService.getCurrentTime()).willReturn(now.toDate());

        final Date startDate = new DateTime().minusHours(1).toDate(); // one hour in the past

        BDDMockito.given(dealModel.getEndDate()).willReturn(startDate);

        expectedException.expect(InterceptorException.class);
        dealModelRemoveInterceptor.onRemove(dealModel, interceptorContext);
    }

    @Test
    public void testOnRemoveForFutureStartDate() throws InterceptorException {
        BDDMockito.given(Boolean.valueOf(interceptorContext.isModified(dealModel, AbstractDealModel.ENDDATE)))
                .willReturn(Boolean.TRUE);

        final DateTime now = new DateTime();
        BDDMockito.given(timeService.getCurrentTime()).willReturn(now.toDate());

        final Date startDate = now.plusHours(1).toDate(); // one hour in future

        BDDMockito.given(dealModel.getStartDate()).willReturn(startDate);
        try {
            dealModelRemoveInterceptor.onRemove(dealModel, interceptorContext);
        }
        catch (final InterceptorException ie) {
            fail("Should not have thrown Interceptor exception");
        }
    }

}

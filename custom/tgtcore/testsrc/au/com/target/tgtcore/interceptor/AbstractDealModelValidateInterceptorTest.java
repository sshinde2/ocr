package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.fest.assertions.Assertions;
import org.joda.time.DateTime;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.interceptor.AbstractDealModelValidateInterceptor.ErrorMessages;
import junit.framework.Assert;


@UnitTest
public class AbstractDealModelValidateInterceptorTest {

    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";

    private static final int DEAL_ID_MIN_VALUE = 1;
    private static final int DEAL_ID_MAX_VALUE = 999999999;

    private final AbstractDealModelValidateInterceptor abstractDealModelValidateInterceptor = new AbstractDealModelValidateInterceptor();

    public AbstractDealModelValidateInterceptorTest() {
        abstractDealModelValidateInterceptor.setDealIdMinValue(DEAL_ID_MIN_VALUE);
        abstractDealModelValidateInterceptor.setDealIdMaxValue(DEAL_ID_MAX_VALUE);
    }

    @Test
    public void testOnValidateWithNullModel() throws InterceptorException {
        abstractDealModelValidateInterceptor.onValidate(null, null);
    }

    @Test
    public void testOnValidateWithWrongModelType() throws InterceptorException {
        final ProductPromotionModel mockProductPromotionModel = Mockito.mock(ProductPromotionModel.class);

        abstractDealModelValidateInterceptor.onValidate(mockProductPromotionModel, null);

        Mockito.verifyZeroInteractions(mockProductPromotionModel);
    }

    @Test
    public void testOnValidateWithNoDealId() throws InterceptorException {
        final AbstractDealModel mockAbstractDealModel = Mockito.mock(AbstractDealModel.class);

        try {
            abstractDealModelValidateInterceptor.onValidate(mockAbstractDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).startsWith(EXCEPTION_MESSAGE_PREFIX + ErrorMessages.DEAL_ID_INVALID);
        }
    }

    @Test
    public void testOnValidateWithInvalidDealId() throws InterceptorException {
        final AbstractDealModel mockAbstractDealModel = Mockito.mock(AbstractDealModel.class);
        BDDMockito.given(mockAbstractDealModel.getCode()).willReturn("AwesomeDeal");

        try {
            abstractDealModelValidateInterceptor.onValidate(mockAbstractDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).startsWith(EXCEPTION_MESSAGE_PREFIX + ErrorMessages.DEAL_ID_INVALID);
        }
    }

    @Test
    public void testOnValidateWithLowDealId() throws InterceptorException {
        final AbstractDealModel mockAbstractDealModel = Mockito.mock(AbstractDealModel.class);
        BDDMockito.given(mockAbstractDealModel.getCode()).willReturn(String.valueOf(DEAL_ID_MIN_VALUE - 1));

        try {
            abstractDealModelValidateInterceptor.onValidate(mockAbstractDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).startsWith(EXCEPTION_MESSAGE_PREFIX + ErrorMessages.DEAL_ID_INVALID);
        }
    }

    @Test
    public void testOnValidateWithHighDealId() throws InterceptorException {
        final AbstractDealModel mockAbstractDealModel = Mockito.mock(AbstractDealModel.class);
        BDDMockito.given(mockAbstractDealModel.getCode()).willReturn(String.valueOf(DEAL_ID_MAX_VALUE + 1));

        try {
            abstractDealModelValidateInterceptor.onValidate(mockAbstractDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).startsWith(EXCEPTION_MESSAGE_PREFIX + ErrorMessages.DEAL_ID_INVALID);
        }
    }

    @Test
    public void testOnValidateWithEndDateBeforeStartDate() throws InterceptorException {
        final AbstractDealModel mockAbstractDealModel = Mockito.mock(AbstractDealModel.class);
        BDDMockito.given(mockAbstractDealModel.getCode()).willReturn(String.valueOf(DEAL_ID_MIN_VALUE));
        BDDMockito.given(mockAbstractDealModel.getStartDate()).willReturn(DateTime.now().plusDays(1).toDate());
        BDDMockito.given(mockAbstractDealModel.getEndDate()).willReturn(DateTime.now().toDate());

        try {
            abstractDealModelValidateInterceptor.onValidate(mockAbstractDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.END_DATE_BEFORE_START_DATE);
        }
    }

    @Test
    public void testOnValidateWithStartDateBeforeEndDate() throws InterceptorException {
        final AbstractDealModel mockAbstractDealModel = Mockito.mock(AbstractDealModel.class);
        BDDMockito.given(mockAbstractDealModel.getCode()).willReturn(String.valueOf(DEAL_ID_MIN_VALUE));
        BDDMockito.given(mockAbstractDealModel.getStartDate()).willReturn(DateTime.now().toDate());
        BDDMockito.given(mockAbstractDealModel.getEndDate()).willReturn(DateTime.now().plusDays(1).toDate());

        abstractDealModelValidateInterceptor.onValidate(mockAbstractDealModel, null);
    }

    @Test
    public void testOnValidateWithStartDateOnly() throws InterceptorException {
        final AbstractDealModel mockAbstractDealModel = Mockito.mock(AbstractDealModel.class);
        BDDMockito.given(mockAbstractDealModel.getCode()).willReturn(String.valueOf(DEAL_ID_MIN_VALUE));
        BDDMockito.given(mockAbstractDealModel.getStartDate()).willReturn(DateTime.now().plusDays(1).toDate());

        abstractDealModelValidateInterceptor.onValidate(mockAbstractDealModel, null);
    }

    @Test
    public void testOnValidateWithEndDateOnly() throws InterceptorException {
        final AbstractDealModel mockAbstractDealModel = Mockito.mock(AbstractDealModel.class);
        BDDMockito.given(mockAbstractDealModel.getCode()).willReturn(String.valueOf(DEAL_ID_MIN_VALUE));
        BDDMockito.given(mockAbstractDealModel.getEndDate()).willReturn(DateTime.now().plusDays(1).toDate());

        abstractDealModelValidateInterceptor.onValidate(mockAbstractDealModel, null);
    }

    @Test
    public void testOnValidateDisabled() throws InterceptorException {
        final AbstractDealModel mockAbstractDealModel = Mockito.mock(AbstractDealModel.class);
        BDDMockito.given(mockAbstractDealModel.getCode()).willReturn(String.valueOf(DEAL_ID_MIN_VALUE));
        BDDMockito.given(mockAbstractDealModel.getEnabled()).willReturn(Boolean.FALSE);

        abstractDealModelValidateInterceptor.onValidate(mockAbstractDealModel, null);
    }

    @Test
    public void testOnValidateEnabledWithNoDescription() throws InterceptorException {
        final AbstractDealModel mockAbstractDealModel = Mockito.mock(AbstractDealModel.class);
        BDDMockito.given(mockAbstractDealModel.getCode()).willReturn(String.valueOf(DEAL_ID_MIN_VALUE));
        BDDMockito.given(mockAbstractDealModel.getEnabled()).willReturn(Boolean.TRUE);

        try {
            abstractDealModelValidateInterceptor.onValidate(mockAbstractDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.DESCRIPTION_REQUIRED);
        }
    }

    @Test
    public void testOnValidateEnabledWithNoFiredMessage() throws InterceptorException {
        final AbstractDealModel mockAbstractDealModel = Mockito.mock(AbstractDealModel.class);
        BDDMockito.given(mockAbstractDealModel.getCode()).willReturn(String.valueOf(DEAL_ID_MIN_VALUE));
        BDDMockito.given(mockAbstractDealModel.getEnabled()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockAbstractDealModel.getDescription()).willReturn("Deal description");

        try {
            abstractDealModelValidateInterceptor.onValidate(mockAbstractDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.FIRED_MESSAGE_REQUIRED);
        }
    }

    @Test
    public void testOnValidateEnabledWithNoCouldHaveFiredMessage() throws InterceptorException {
        final AbstractDealModel mockAbstractDealModel = Mockito.mock(AbstractDealModel.class);
        BDDMockito.given(mockAbstractDealModel.getCode()).willReturn(String.valueOf(DEAL_ID_MIN_VALUE));
        BDDMockito.given(mockAbstractDealModel.getEnabled()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockAbstractDealModel.getDescription()).willReturn("Deal description");
        BDDMockito.given(mockAbstractDealModel.getMessageFired()).willReturn("Fired message");

        try {
            abstractDealModelValidateInterceptor.onValidate(mockAbstractDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.COULD_HAVE_FIRED_MESSAGE_REQUIRED);
        }
    }

    @Test
    public void testOnValidateEnabled() throws InterceptorException {
        final AbstractDealModel mockAbstractDealModel = Mockito.mock(AbstractDealModel.class);
        BDDMockito.given(mockAbstractDealModel.getCode()).willReturn(String.valueOf(DEAL_ID_MIN_VALUE));
        BDDMockito.given(mockAbstractDealModel.getEnabled()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockAbstractDealModel.getDescription()).willReturn("Deal description");
        BDDMockito.given(mockAbstractDealModel.getMessageFired()).willReturn("Fired message");
        BDDMockito.given(mockAbstractDealModel.getMessageCouldHaveFired()).willReturn("Could Have Fired message");

        abstractDealModelValidateInterceptor.onValidate(mockAbstractDealModel, null);
    }
}

package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.Collections;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.interceptor.AbstractSimpleDealModelValidateInterceptor.ErrorMessages;
import junit.framework.Assert;


@UnitTest
public class AbstractSimpleDealModelValidateInterceptorTest {

    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";

    private final AbstractSimpleDealModelValidateInterceptor abstractSimpleDealModelValidateInterceptor = new AbstractSimpleDealModelValidateInterceptor();

    @Test
    public void testOnValidateWithNullModel() throws InterceptorException {
        abstractSimpleDealModelValidateInterceptor.onValidate(null, null);
    }

    @Test
    public void testOnValidateWithWrongModelType() throws InterceptorException {
        final ProductPromotionModel mockProductPromotionModel = Mockito.mock(ProductPromotionModel.class);

        abstractSimpleDealModelValidateInterceptor.onValidate(mockProductPromotionModel, null);

        Mockito.verifyZeroInteractions(mockProductPromotionModel);
    }

    @Test
    public void testOnValidateDisabled() throws InterceptorException {
        final AbstractSimpleDealModel mockAbstractSimpleDealModel = Mockito.mock(AbstractSimpleDealModel.class);
        BDDMockito.given(mockAbstractSimpleDealModel.getEnabled()).willReturn(Boolean.FALSE);

        abstractSimpleDealModelValidateInterceptor.onValidate(mockAbstractSimpleDealModel, null);
    }

    @Test
    public void testOnValidateEnabledNoQualifiers() throws InterceptorException {
        final AbstractSimpleDealModel mockAbstractSimpleDealModel = Mockito.mock(AbstractSimpleDealModel.class);
        BDDMockito.given(mockAbstractSimpleDealModel.getEnabled()).willReturn(Boolean.TRUE);

        try {
            abstractSimpleDealModelValidateInterceptor.onValidate(mockAbstractSimpleDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.ONE_QUALIFIER_REQUIRED);
        }
    }

    @Test
    public void testOnValidateEnabledOneQualifier() throws InterceptorException {
        final AbstractSimpleDealModel mockAbstractSimpleDealModel = Mockito.mock(AbstractSimpleDealModel.class);
        BDDMockito.given(mockAbstractSimpleDealModel.getEnabled()).willReturn(Boolean.TRUE);

        final DealQualifierModel mockDealQualifierModel = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockAbstractSimpleDealModel.getQualifierList()).willReturn(
                Collections.singletonList(mockDealQualifierModel));

        abstractSimpleDealModelValidateInterceptor.onValidate(mockAbstractSimpleDealModel, null);
    }
}

package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.promotions.model.BuyGetDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.ArrayList;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.enums.DealRewardTypeEnum;
import au.com.target.tgtcore.interceptor.BuyGetDealModelValidateInterceptor.ErrorMessages;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import junit.framework.Assert;


@UnitTest
public class BuyGetDealModelValidateInterceptorTest {

    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";

    private List<DealRewardTypeEnum> validDealTypes;

    private final BuyGetDealModelValidateInterceptor buyGetDealModelValidateInterceptor = new BuyGetDealModelValidateInterceptor();

    @Before
    public void setUp() {
        validDealTypes = new ArrayList<>();
        validDealTypes.add(DealRewardTypeEnum.DOLLAROFFEACH);
        validDealTypes.add(DealRewardTypeEnum.FIXEDDOLLAREACH);
        validDealTypes.add(DealRewardTypeEnum.PERCENTOFFEACH);

        buyGetDealModelValidateInterceptor.setValidDealTypes(validDealTypes);
    }

    @Test
    public void testOnValidateWithNullModel() throws InterceptorException {
        buyGetDealModelValidateInterceptor.onValidate(null, null);
    }

    @Test
    public void testOnValidateWithWrongModelType() throws InterceptorException {
        final ProductPromotionModel mockProductPromotionModel = Mockito.mock(ProductPromotionModel.class);

        buyGetDealModelValidateInterceptor.onValidate(mockProductPromotionModel, null);

        Mockito.verifyZeroInteractions(mockProductPromotionModel);
    }

    @Test
    public void testOnValidateWithInvalidDealType() {
        final BuyGetDealModel mockBuyGetDealModel = Mockito.mock(BuyGetDealModel.class);
        BDDMockito.given(mockBuyGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.FIXEDDOLLARTOTAL);

        try {
            buyGetDealModelValidateInterceptor.onValidate(mockBuyGetDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).startsWith(
                    EXCEPTION_MESSAGE_PREFIX + String.format(ErrorMessages.INVALID_DEAL_REWARD_TYPE, ""));
        }
    }

    @Test
    public void testOnValidateWithTooManyQualifiers() {
        final BuyGetDealModel mockBuyGetDealModel = Mockito.mock(BuyGetDealModel.class);
        BDDMockito.given(mockBuyGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.FIXEDDOLLAREACH);

        final DealQualifierModel mockDealQualifier1 = Mockito.mock(DealQualifierModel.class);
        final DealQualifierModel mockDealQualifier2 = Mockito.mock(DealQualifierModel.class);

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        dealQualifierList.add(mockDealQualifier1);
        dealQualifierList.add(mockDealQualifier2);

        BDDMockito.given(mockBuyGetDealModel.getQualifierList()).willReturn(dealQualifierList);

        try {
            buyGetDealModelValidateInterceptor.onValidate(mockBuyGetDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + String.format(ErrorMessages.TOO_MANY_QUALIFIERS));
        }
    }

    @Test
    public void testOnValidateWithNoRewardCategories() throws InterceptorException {
        final BuyGetDealModel mockBuyGetDealModel = Mockito.mock(BuyGetDealModel.class);
        BDDMockito.given(mockBuyGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.FIXEDDOLLAREACH);

        final DealQualifierModel mockDealQualifier = Mockito.mock(DealQualifierModel.class);

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        dealQualifierList.add(mockDealQualifier);

        BDDMockito.given(mockBuyGetDealModel.getQualifierList()).willReturn(dealQualifierList);

        try {
            buyGetDealModelValidateInterceptor.onValidate(mockBuyGetDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage())
                    .isEqualTo(EXCEPTION_MESSAGE_PREFIX + ErrorMessages.REWARD_CAT_MINIMUM);
        }
    }

    @Test
    public void testOnValidateWithTooManyStagedRewardCategories() throws InterceptorException {
        final BuyGetDealModel mockBuyGetDealModel = Mockito.mock(BuyGetDealModel.class);
        BDDMockito.given(mockBuyGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.FIXEDDOLLAREACH);

        final DealQualifierModel mockDealQualifier = Mockito.mock(DealQualifierModel.class);

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        dealQualifierList.add(mockDealQualifier);

        BDDMockito.given(mockBuyGetDealModel.getQualifierList()).willReturn(dealQualifierList);

        final CatalogVersionModel mockCatalogVersion = BDDMockito.mock(CatalogVersionModel.class);
        BDDMockito.given(mockCatalogVersion.getActive()).willReturn(Boolean.FALSE);

        final List<TargetDealCategoryModel> rewardCategories = new ArrayList<>();
        final TargetDealCategoryModel mockTargetRewardCategoryModel1 = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(mockTargetRewardCategoryModel1.getCatalogVersion()).willReturn(mockCatalogVersion);
        rewardCategories.add(mockTargetRewardCategoryModel1);
        final TargetDealCategoryModel mockTargetRewardCategoryModel2 = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(mockTargetRewardCategoryModel2.getCatalogVersion()).willReturn(mockCatalogVersion);
        rewardCategories.add(mockTargetRewardCategoryModel2);
        BDDMockito.given(mockBuyGetDealModel.getRewardCategory()).willReturn(rewardCategories);

        try {
            buyGetDealModelValidateInterceptor.onValidate(mockBuyGetDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.REWARD_CAT_TOO_MANY_STAGED);
        }
    }

    @Test
    public void testOnValidate() throws InterceptorException {
        final BuyGetDealModel mockBuyGetDealModel = Mockito.mock(BuyGetDealModel.class);
        BDDMockito.given(mockBuyGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.FIXEDDOLLAREACH);

        final DealQualifierModel mockDealQualifier = Mockito.mock(DealQualifierModel.class);

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        dealQualifierList.add(mockDealQualifier);

        BDDMockito.given(mockBuyGetDealModel.getQualifierList()).willReturn(dealQualifierList);

        final CatalogVersionModel mockCatalogVersionStaged = BDDMockito.mock(CatalogVersionModel.class);
        BDDMockito.given(mockCatalogVersionStaged.getActive()).willReturn(Boolean.FALSE);

        final CatalogVersionModel mockCatalogVersionOnline = BDDMockito.mock(CatalogVersionModel.class);
        BDDMockito.given(mockCatalogVersionStaged.getActive()).willReturn(Boolean.TRUE);

        final List<TargetDealCategoryModel> rewardCategories = new ArrayList<>();
        final TargetDealCategoryModel mockTargetRewardCategoryModel1 = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(mockTargetRewardCategoryModel1.getCatalogVersion()).willReturn(mockCatalogVersionStaged);
        rewardCategories.add(mockTargetRewardCategoryModel1);
        final TargetDealCategoryModel mockTargetRewardCategoryModel2 = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(mockTargetRewardCategoryModel2.getCatalogVersion()).willReturn(mockCatalogVersionOnline);
        rewardCategories.add(mockTargetRewardCategoryModel2);
        BDDMockito.given(mockBuyGetDealModel.getRewardCategory()).willReturn(rewardCategories);

        buyGetDealModelValidateInterceptor.onValidate(mockBuyGetDealModel, null);
    }
}

package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.BuyGetDealModel;
import de.hybris.platform.promotions.model.BuyGetSameListDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.ArrayList;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.enums.DealRewardTypeEnum;
import au.com.target.tgtcore.interceptor.BuyGetSameListDealModelValidateInterceptor.ErrorMessages;
import junit.framework.Assert;


@UnitTest
public class BuyGetSameListDealModelValidateInterceptorTest {

    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";

    private List<DealRewardTypeEnum> validDealTypes;

    private final BuyGetSameListDealModelValidateInterceptor buyGetSameListDealModelValidateInterceptor = new BuyGetSameListDealModelValidateInterceptor();

    @Before
    public void setUp() {
        validDealTypes = new ArrayList<>();
        validDealTypes.add(DealRewardTypeEnum.DOLLAROFFEACH);
        validDealTypes.add(DealRewardTypeEnum.FIXEDDOLLAREACH);
        validDealTypes.add(DealRewardTypeEnum.PERCENTOFFEACH);

        buyGetSameListDealModelValidateInterceptor.setValidDealTypes(validDealTypes);
    }

    @Test
    public void testOnValidateWithNullModel() throws InterceptorException {
        buyGetSameListDealModelValidateInterceptor.onValidate(null, null);
    }

    @Test
    public void testOnValidateWithWrongModelType() throws InterceptorException {
        final BuyGetDealModel mockModel = Mockito.mock(BuyGetDealModel.class);

        buyGetSameListDealModelValidateInterceptor.onValidate(mockModel, null);

        Mockito.verifyZeroInteractions(mockModel);
    }

    @Test
    public void testOnValidateWithInvalidDealType() {
        final BuyGetSameListDealModel mockBuyGetDealModel = Mockito.mock(BuyGetSameListDealModel.class);
        BDDMockito.given(mockBuyGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.FIXEDDOLLARTOTAL);

        try {
            buyGetSameListDealModelValidateInterceptor.onValidate(mockBuyGetDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).startsWith(
                    EXCEPTION_MESSAGE_PREFIX + String.format(ErrorMessages.INVALID_DEAL_REWARD_TYPE, ""));
        }
    }

    @Test
    public void testOnValidateWithDealEnabledNotEnoughQualifiers() {
        final BuyGetSameListDealModel mockBuyGetDealModel = Mockito.mock(BuyGetSameListDealModel.class);
        BDDMockito.given(mockBuyGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.FIXEDDOLLAREACH);
        BDDMockito.given(mockBuyGetDealModel.getEnabled()).willReturn(Boolean.TRUE);

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        BDDMockito.given(mockBuyGetDealModel.getQualifierList()).willReturn(dealQualifierList);

        try {
            buyGetSameListDealModelValidateInterceptor.onValidate(mockBuyGetDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.NOT_ENOUGH_QUALIFIERS);
        }
    }

    @Test
    public void testOnValidateWithTooManyQualifiers() {
        final BuyGetSameListDealModel mockBuyGetDealModel = Mockito.mock(BuyGetSameListDealModel.class);
        BDDMockito.given(mockBuyGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.FIXEDDOLLAREACH);

        final DealQualifierModel mockDealQualifier1 = Mockito.mock(DealQualifierModel.class);
        final DealQualifierModel mockDealQualifier2 = Mockito.mock(DealQualifierModel.class);

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        dealQualifierList.add(mockDealQualifier1);
        dealQualifierList.add(mockDealQualifier2);

        BDDMockito.given(mockBuyGetDealModel.getQualifierList()).willReturn(dealQualifierList);

        try {
            buyGetSameListDealModelValidateInterceptor.onValidate(mockBuyGetDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.TOO_MANY_QUALIFIERS);
        }
    }

    @Test
    public void testOnValidateWithNoAmountOnQualifier() {
        final BuyGetSameListDealModel mockBuyGetDealModel = Mockito.mock(BuyGetSameListDealModel.class);
        BDDMockito.given(mockBuyGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.FIXEDDOLLAREACH);

        final DealQualifierModel mockDealQualifier = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifier.getMinQty()).willReturn(null);

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        dealQualifierList.add(mockDealQualifier);

        BDDMockito.given(mockBuyGetDealModel.getQualifierList()).willReturn(dealQualifierList);

        try {
            buyGetSameListDealModelValidateInterceptor.onValidate(mockBuyGetDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.QUALIFIER_REQUIRES_QUANTITY);
        }
    }

    @Test
    public void testOnValidateWithZeroAmountOnQualifier() {
        final BuyGetSameListDealModel mockBuyGetDealModel = Mockito.mock(BuyGetSameListDealModel.class);
        BDDMockito.given(mockBuyGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.FIXEDDOLLAREACH);

        final DealQualifierModel mockDealQualifier = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifier.getMinQty()).willReturn(Integer.valueOf(0));

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        dealQualifierList.add(mockDealQualifier);

        BDDMockito.given(mockBuyGetDealModel.getQualifierList()).willReturn(dealQualifierList);

        try {
            buyGetSameListDealModelValidateInterceptor.onValidate(mockBuyGetDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.QUALIFIER_REQUIRES_QUANTITY);
        }
    }

    @Test
    public void testOnValidateWithBelowZeroAmountOnQualifier() {
        final BuyGetSameListDealModel mockBuyGetDealModel = Mockito.mock(BuyGetSameListDealModel.class);
        BDDMockito.given(mockBuyGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.FIXEDDOLLAREACH);

        final DealQualifierModel mockDealQualifier = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifier.getMinQty()).willReturn(Integer.valueOf(-3));

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        dealQualifierList.add(mockDealQualifier);

        BDDMockito.given(mockBuyGetDealModel.getQualifierList()).willReturn(dealQualifierList);

        try {
            buyGetSameListDealModelValidateInterceptor.onValidate(mockBuyGetDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.QUALIFIER_REQUIRES_QUANTITY);
        }
    }

    @Test
    public void testOnValidateDealDisabled() throws InterceptorException {
        final BuyGetSameListDealModel mockBuyGetDealModel = Mockito.mock(BuyGetSameListDealModel.class);
        BDDMockito.given(mockBuyGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.FIXEDDOLLAREACH);
        BDDMockito.given(mockBuyGetDealModel.getEnabled()).willReturn(Boolean.FALSE);

        buyGetSameListDealModelValidateInterceptor.onValidate(mockBuyGetDealModel, null);
    }

    @Test
    public void testOnValidate() throws InterceptorException {
        final BuyGetSameListDealModel mockBuyGetDealModel = Mockito.mock(BuyGetSameListDealModel.class);
        BDDMockito.given(mockBuyGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.FIXEDDOLLAREACH);
        BDDMockito.given(mockBuyGetDealModel.getEnabled()).willReturn(Boolean.TRUE);

        final DealQualifierModel mockDealQualifier = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifier.getMinQty()).willReturn(Integer.valueOf(3));

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        dealQualifierList.add(mockDealQualifier);

        BDDMockito.given(mockBuyGetDealModel.getQualifierList()).willReturn(dealQualifierList);

        buyGetSameListDealModelValidateInterceptor.onValidate(mockBuyGetDealModel, null);
    }
}

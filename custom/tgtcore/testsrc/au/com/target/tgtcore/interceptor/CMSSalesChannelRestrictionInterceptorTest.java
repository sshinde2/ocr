/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtwebcore.model.cms2.restrictions.CMSSalesChannelRestrictionModel;


/**
 * Unit test for {@link CMSSalesChannelRestrictionInterceptor}
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CMSSalesChannelRestrictionInterceptorTest {

    @InjectMocks
    private final CMSSalesChannelRestrictionInterceptor interceptor = new CMSSalesChannelRestrictionInterceptor();
    private final CMSSalesChannelRestrictionModel restrictionModel = new CMSSalesChannelRestrictionModel();
    private final Collection<SalesApplication> salesApplications = new ArrayList<>();
    @Mock
    private InterceptorContext ctx;
    @Mock
    private CatalogVersionModel catalogVersion;

    @Test
    public void testValidateSingleSalesChannelForRestriction() throws InterceptorException {
        salesApplications.add(SalesApplication.MOBILEAPP);
        restrictionModel.setSalesApplications(salesApplications);
        interceptor.onValidate(restrictionModel, ctx);
    }

    @Test(expected = InterceptorException.class)
    public void testValidateEmptySalesChannelForRestriction() throws InterceptorException {
        restrictionModel.setSalesApplications(CollectionUtils.EMPTY_COLLECTION);
        interceptor.onValidate(restrictionModel, ctx);
    }

    @Test
    public void testValidateMultipleSalesChannelForRestriction() throws InterceptorException {
        salesApplications.add(SalesApplication.KIOSK);
        restrictionModel.setSalesApplications(salesApplications);
        interceptor.onValidate(restrictionModel, ctx);
    }

    @Test(expected = InterceptorException.class)
    public void testValidateNullSalesChannelForRestriction() throws InterceptorException {
        restrictionModel.setSalesApplications(null);
        interceptor.onValidate(restrictionModel, ctx);
    }

}

/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.CurrencyConversionFactorsModel;


/**
 * @author Vivek
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CurrencyConversionFactorsPrepareInterceptorTest {

    @Mock
    private InterceptorContext context;

    @Spy
    @InjectMocks
    private final CurrencyConversionFactorsPrepareInterceptor prepareInterceptor = new CurrencyConversionFactorsPrepareInterceptor();

    @Test
    public void testNewModel() throws InterceptorException {
        final CurrencyConversionFactorsModel currencyConversionFactor = Mockito
                .mock(CurrencyConversionFactorsModel.class);
        Mockito.when(currencyConversionFactor.getCurrencyConversionFactor()).thenReturn(Double.valueOf(1.90099));
        Mockito.when(Boolean.valueOf(context.isNew(currencyConversionFactor))).thenReturn(Boolean.TRUE);

        prepareInterceptor.onPrepare(currencyConversionFactor, context);

        Mockito.verify(prepareInterceptor, Mockito.times(1)).logNewConversionFactor(currencyConversionFactor);
        Mockito.verify(prepareInterceptor, Mockito.never()).logUpdateConversionFactor(currencyConversionFactor);
    }

    @Test
    public void testUpdateModel() throws InterceptorException {
        final CurrencyConversionFactorsModel currencyConversionFactor = Mockito
                .mock(CurrencyConversionFactorsModel.class);
        Mockito.when(currencyConversionFactor.getCurrencyConversionFactor()).thenReturn(Double.valueOf(1.90099));
        Mockito.when(Boolean.valueOf(context.isNew(currencyConversionFactor))).thenReturn(Boolean.FALSE);
        Mockito.when(Boolean.valueOf(
                context.isModified(currencyConversionFactor, CurrencyConversionFactorsModel.CURRENCYCONVERSIONFACTOR)))
                .thenReturn(Boolean.TRUE);

        prepareInterceptor.onPrepare(currencyConversionFactor, context);

        Mockito.verify(prepareInterceptor, Mockito.never()).logNewConversionFactor(currencyConversionFactor);
        Mockito.verify(prepareInterceptor, Mockito.times(1)).logUpdateConversionFactor(currencyConversionFactor);
    }

    @Test
    public void testNullCheck() throws InterceptorException {
        final CurrencyConversionFactorsModel currencyConversionFactor = Mockito
                .mock(CurrencyConversionFactorsModel.class);
        Mockito.when(currencyConversionFactor.getCurrencyConversionFactor()).thenReturn(null);

        prepareInterceptor.onPrepare(currencyConversionFactor, context);

        Mockito.verify(prepareInterceptor, Mockito.never()).logNewConversionFactor(currencyConversionFactor);
        Mockito.verify(prepareInterceptor, Mockito.never()).logUpdateConversionFactor(currencyConversionFactor);
    }
}

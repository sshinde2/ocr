package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.DealBreakPointModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.interceptor.DealBreakPointModelValidateInterceptor.ErrorMessages;


@UnitTest
public class DealBreakPointModelValidateInterceptorTest {
    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";

    private final DealBreakPointModelValidateInterceptor dealBreakPointModelValidateInterceptor = new DealBreakPointModelValidateInterceptor();

    @Test
    public void testOnValidateWithNullModel() throws InterceptorException {
        dealBreakPointModelValidateInterceptor.onValidate(null, null);
    }

    @Test
    public void testOnValidateWithIncorrectModelType() throws InterceptorException {
        final DealQualifierModel mockModel = Mockito.mock(DealQualifierModel.class);

        dealBreakPointModelValidateInterceptor.onValidate(mockModel, null);

        Mockito.verifyZeroInteractions(mockModel);
    }

    @Test
    public void testOnValidateWithQtyTooLow() {
        final DealBreakPointModel mockModel = Mockito.mock(DealBreakPointModel.class);
        BDDMockito.given(mockModel.getQty()).willReturn(Integer.valueOf(1));

        try {
            dealBreakPointModelValidateInterceptor.onValidate(mockModel, null);
            Assert.fail("Expected InterceptorException to be thrown");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(EXCEPTION_MESSAGE_PREFIX + ErrorMessages.QTY_TOO_LOW);
        }
    }

    @Test
    public void testOnValidateWithPriceBelowZero() {
        final DealBreakPointModel mockModel = Mockito.mock(DealBreakPointModel.class);
        BDDMockito.given(mockModel.getQty()).willReturn(Integer.valueOf(2));
        BDDMockito.given(mockModel.getUnitPrice()).willReturn(Double.valueOf("-1"));

        try {
            dealBreakPointModelValidateInterceptor.onValidate(mockModel, null);
            Assert.fail("Expected InterceptorException to be thrown");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.UNIT_PRICE_TOO_LOW);
        }
    }

    @Test
    public void testOnValidateWithPriceEqualToZero() {
        final DealBreakPointModel mockModel = Mockito.mock(DealBreakPointModel.class);
        BDDMockito.given(mockModel.getQty()).willReturn(Integer.valueOf(2));
        BDDMockito.given(mockModel.getUnitPrice()).willReturn(Double.valueOf("0"));

        try {
            dealBreakPointModelValidateInterceptor.onValidate(mockModel, null);
            Assert.fail("Expected InterceptorException to be thrown");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.UNIT_PRICE_TOO_LOW);
        }
    }

    @Test
    public void testOnValidate() throws InterceptorException {
        final DealBreakPointModel mockModel = Mockito.mock(DealBreakPointModel.class);
        BDDMockito.given(mockModel.getQty()).willReturn(Integer.valueOf(2));
        BDDMockito.given(mockModel.getUnitPrice()).willReturn(Double.valueOf("15"));

        dealBreakPointModelValidateInterceptor.onValidate(mockModel, null);
    }
}

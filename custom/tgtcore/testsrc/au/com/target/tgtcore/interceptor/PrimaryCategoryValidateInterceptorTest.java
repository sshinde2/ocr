/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants.Catalog;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PrimaryCategoryValidateInterceptorTest {

    private PrimaryCategoryValidateInterceptor interceptor;

    @Mock
    private InterceptorContext ctx;

    @Mock
    private CatalogVersionModel catalogVersion;

    @Before
    public void setup() {
        interceptor = new PrimaryCategoryValidateInterceptor();
    }

    @Test(expected = InterceptorException.class)
    public void testValidate() throws InterceptorException {
        final TargetProductModel targetProduct = mock(TargetProductModel.class);
        final TargetProductCategoryModel testCategory = mock(TargetProductCategoryModel.class);

        final Collection<CategoryModel> superCategories = new ArrayList<>();
        final CategoryModel superCategory = mock(CategoryModel.class);
        superCategories.add(superCategory);

        given(targetProduct.getSupercategories()).willReturn(superCategories);
        given(targetProduct.getPrimarySuperCategory()).willReturn(testCategory);
        given(targetProduct.getCatalogVersion()).willReturn(catalogVersion);
        given(catalogVersion.getVersion()).willReturn(Catalog.OFFLINE_VERSION);

        interceptor.onValidate(targetProduct, ctx);
    }

    @Test(expected = InterceptorException.class)
    public void testValidateEmptySuperCategories() throws InterceptorException {
        final TargetProductModel targetProduct = mock(TargetProductModel.class);
        final TargetProductCategoryModel primaryCategory = mock(TargetProductCategoryModel.class);
        given(targetProduct.getPrimarySuperCategory()).willReturn(primaryCategory);
        given(targetProduct.getSupercategories()).willReturn(Collections.EMPTY_LIST);
        given(targetProduct.getCatalogVersion()).willReturn(catalogVersion);
        given(catalogVersion.getVersion()).willReturn(Catalog.OFFLINE_VERSION);

        interceptor.onValidate(targetProduct, ctx);
    }

    @Test
    public void testValidatePrimaryCategoryIsEmpty() throws InterceptorException {
        final TargetProductModel targetProduct = mock(TargetProductModel.class);
        given(targetProduct.getCatalogVersion()).willReturn(catalogVersion);
        given(catalogVersion.getVersion()).willReturn(Catalog.OFFLINE_VERSION);

        interceptor.onValidate(targetProduct, ctx);
        verify(targetProduct, never()).getSupercategories();
    }

    @Test
    public void testSkipsExecutingForOnlineCatalog() throws InterceptorException {
        final TargetProductModel targetProduct = mock(TargetProductModel.class);
        given(targetProduct.getCatalogVersion()).willReturn(catalogVersion);
        given(catalogVersion.getVersion()).willReturn(Catalog.ONLINE_VERSION);

        interceptor.onValidate(targetProduct, ctx);
        verify(targetProduct, never()).getPrimarySuperCategory();
    }
}

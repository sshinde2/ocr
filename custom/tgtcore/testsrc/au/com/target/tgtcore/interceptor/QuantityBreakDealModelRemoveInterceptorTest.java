package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.promotions.model.DealBreakPointModel;
import de.hybris.platform.promotions.model.QuantityBreakDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetDealCategoryModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class QuantityBreakDealModelRemoveInterceptorTest {

    //CHECKSTYLE:OFF

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @Spy
    private final QuantityBreakDealModelRemoveInterceptor dealModelRemoveInterceptor = new QuantityBreakDealModelRemoveInterceptor();

    @Mock
    private ModelService modelService;

    @Mock
    private InterceptorContext interceptorContext;

    @Mock
    private QuantityBreakDealModel dealModel;

    @Before
    public void setUp() {
        BDDMockito.willReturn(modelService).given(dealModelRemoveInterceptor).getModelService();
    }

    @Test
    public void testOnRemoveForNullMode() throws InterceptorException {
        dealModelRemoveInterceptor.onRemove(null, interceptorContext);
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testOnRemoveForOtherModel() throws InterceptorException {
        dealModelRemoveInterceptor.onRemove(new Object(), interceptorContext);
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testOnRemoveWithNoCategoriesAndBreakpoints() throws InterceptorException {

        BDDMockito.given(dealModel.getBreakPoints()).willReturn(Collections.EMPTY_LIST);
        BDDMockito.given(dealModel.getCategories()).willReturn(Collections.EMPTY_LIST);

        dealModelRemoveInterceptor.onRemove(dealModel, interceptorContext);
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testOnRemoveWithCategoryNoBreakPoints() throws InterceptorException {

        final CategoryModel dealCategory = Mockito.mock(TargetDealCategoryModel.class);

        BDDMockito.given(dealModel.getBreakPoints()).willReturn(Collections.EMPTY_LIST);
        BDDMockito.given(dealModel.getCategories()).willReturn(Collections.singletonList(dealCategory));

        final ArgumentCaptor<Collection> argumentCaptor = ArgumentCaptor.forClass(Collection.class);

        dealModelRemoveInterceptor.onRemove(dealModel, interceptorContext);
        Mockito.verify(modelService).removeAll(argumentCaptor.capture());

        Assert.assertEquals(1, argumentCaptor.getValue().size());
        Assert.assertTrue(argumentCaptor.getValue().contains(dealCategory));
    }

    @Test
    public void testOnRemoveWithBreakPointsNoCategory() throws InterceptorException {

        final DealBreakPointModel breakPointModel = Mockito.mock(DealBreakPointModel.class);

        BDDMockito.given(dealModel.getBreakPoints()).willReturn(Collections.singletonList(breakPointModel));
        BDDMockito.given(dealModel.getCategories()).willReturn(Collections.EMPTY_LIST);

        final ArgumentCaptor<Collection> argumentCaptor = ArgumentCaptor.forClass(Collection.class);

        dealModelRemoveInterceptor.onRemove(dealModel, interceptorContext);
        Mockito.verify(modelService).removeAll(argumentCaptor.capture());

        Assert.assertEquals(1, argumentCaptor.getValue().size());
        Assert.assertTrue(argumentCaptor.getValue().contains(breakPointModel));
    }

    @Test
    public void testOnRemoveWithBreakPointsAndCategory() throws InterceptorException {

        final CategoryModel dealCategory = Mockito.mock(TargetDealCategoryModel.class);
        final DealBreakPointModel breakPointModel = Mockito.mock(DealBreakPointModel.class);

        BDDMockito.given(dealModel.getBreakPoints()).willReturn(Collections.singletonList(breakPointModel));
        BDDMockito.given(dealModel.getCategories()).willReturn(Collections.singletonList(dealCategory));

        final ArgumentCaptor<Collection> argumentCaptor = ArgumentCaptor.forClass(Collection.class);

        dealModelRemoveInterceptor.onRemove(dealModel, interceptorContext);
        Mockito.verify(modelService).removeAll(argumentCaptor.capture());

        Assert.assertEquals(2, argumentCaptor.getValue().size());
        Assert.assertTrue(argumentCaptor.getValue().contains(dealCategory));
        Assert.assertTrue(argumentCaptor.getValue().contains(breakPointModel));
    }

    @Test
    public void testOnRemoveWithMultipleBreakpointsAndCategories() throws InterceptorException {

        final CategoryModel dealCategory1 = Mockito.mock(TargetDealCategoryModel.class);
        final CategoryModel dealCategory2 = Mockito.mock(TargetDealCategoryModel.class);

        final DealBreakPointModel breakPointModel1 = Mockito.mock(DealBreakPointModel.class);
        final DealBreakPointModel breakPointModel2 = Mockito.mock(DealBreakPointModel.class);


        BDDMockito.given(dealModel.getBreakPoints()).willReturn(Arrays.asList(breakPointModel1, breakPointModel2));
        BDDMockito.given(dealModel.getCategories()).willReturn(Arrays.asList(dealCategory1, dealCategory2));

        final ArgumentCaptor<Collection> argumentCaptor = ArgumentCaptor.forClass(Collection.class);

        dealModelRemoveInterceptor.onRemove(dealModel, interceptorContext);
        Mockito.verify(modelService).removeAll(argumentCaptor.capture());

        Assert.assertEquals(4, argumentCaptor.getValue().size());
        Assert.assertTrue(argumentCaptor.getValue().contains(dealCategory1));
        Assert.assertTrue(argumentCaptor.getValue().contains(dealCategory2));
        Assert.assertTrue(argumentCaptor.getValue().contains(breakPointModel1));
        Assert.assertTrue(argumentCaptor.getValue().contains(breakPointModel2));
    }

}

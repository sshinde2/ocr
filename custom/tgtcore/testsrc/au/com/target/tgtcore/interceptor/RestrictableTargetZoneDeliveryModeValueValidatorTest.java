/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneModel;
import de.hybris.platform.order.ZoneDeliveryModeService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel;


/**
 * Test for {@link RestrictableTargetZoneDeliveryModeValueValidator}.
 */
public class RestrictableTargetZoneDeliveryModeValueValidatorTest extends ServicelayerTransactionalTest {

    @Resource
    private RestrictableTargetZoneDeliveryModeValueValidator restrictableTargetZoneDeliveryModeValueValidator;

    @Resource
    private ModelService modelService;

    @Resource
    private ZoneDeliveryModeService zoneDeliveryModeService;

    @Resource
    private CommonI18NService commonI18NService;

    @Resource
    private ConfigurationService configurationService;

    private ZoneDeliveryModeModel dhlZoneDeliveryMode;
    private CurrencyModel eurCurrency;
    private ZoneModel deZone;

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON
    /**
     * Creates the core data, and necessary data for delivery modes.
     */
    @Before
    public void setUp() throws Exception {
        createCoreData();
        importCsv("/tgtcore/test/testRestrictableDeliveryMode.impex", "windows-1252");
        dhlZoneDeliveryMode = (ZoneDeliveryModeModel)zoneDeliveryModeService.getDeliveryModeForCode("dhl");
        eurCurrency = commonI18NService.getCurrency("EUR");
        deZone = zoneDeliveryModeService.getZoneForCode("de");
    }

    @Test(expected = InterceptorException.class)
    public void testZoneDeliveryModeValueValidatorDuplicate() throws Exception {
        final RestrictableTargetZoneDeliveryModeValueModel valueModel =
                createZoneDeliveryModeValue(eurCurrency, 0.0, 5.0, deZone, dhlZoneDeliveryMode, 6);

        restrictableTargetZoneDeliveryModeValueValidator.onValidate(valueModel, null);
    }

    @Test
    public void testZoneDeliveryModeValueValidatorNonDuplicates() throws Exception {
        RestrictableTargetZoneDeliveryModeValueModel valueModel =
                createZoneDeliveryModeValue(eurCurrency, 0.0, 5.0, deZone, dhlZoneDeliveryMode, 5);
        restrictableTargetZoneDeliveryModeValueValidator.onValidate(valueModel, null);


        valueModel =
                createZoneDeliveryModeValue(eurCurrency, 10.0, 5.0, deZone, dhlZoneDeliveryMode, 6);
        restrictableTargetZoneDeliveryModeValueValidator.onValidate(valueModel, null);
    }

    /**
     * Test when promotion sticker message's length is the maximum value
     * 
     * @throws InterceptorException
     */
    @Test
    public void testOnValidateWhenStickerMessageLengthIsAllowed() throws InterceptorException {
        final RestrictableTargetZoneDeliveryModeValueModel valueModel =
                createZoneDeliveryModeValue(eurCurrency, 0.0, 5.0, deZone, dhlZoneDeliveryMode, 5);
        valueModel.setPromotionalStickerMessage(generateMessage(configurationService.getConfiguration().getInt(
                "tgtcore.restrictabledeliveryvalue.stickerlength.allowed")));
        restrictableTargetZoneDeliveryModeValueValidator.onValidate(valueModel, null);
    }

    /**
     * Test when promotion sticker message's length is greater than maximum value
     * 
     * @throws InterceptorException
     */
    @Test
    public void testOnValidateWhenStickerMessageLengthIsNotAllowed() throws InterceptorException {
        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage("Promotional sticker message's length is greater than 25");
        final RestrictableTargetZoneDeliveryModeValueModel valueModel =
                createZoneDeliveryModeValue(eurCurrency, 0.0, 5.0, deZone, dhlZoneDeliveryMode, 5);
        valueModel.setPromotionalStickerMessage(generateMessage(configurationService.getConfiguration().getInt(
                "tgtcore.restrictabledeliveryvalue.stickerlength.allowed")) + 1);
        restrictableTargetZoneDeliveryModeValueValidator.onValidate(valueModel, null);
    }

    /**
     * Method to generate a string of expected length
     * 
     * @param length
     * @return message of the a defined length
     */
    private String generateMessage(final int length) {
        return StringUtils.repeat("1", length);
    }

    private RestrictableTargetZoneDeliveryModeValueModel createZoneDeliveryModeValue(
            final CurrencyModel currency, final double min, final double value,
            final ZoneModel zone, final ZoneDeliveryModeModel zoneDeliveryMode, final int priority) {
        final RestrictableTargetZoneDeliveryModeValueModel zoneDeliveryModeValue =
                modelService.create(RestrictableTargetZoneDeliveryModeValueModel.class);
        zoneDeliveryModeValue.setCurrency(currency);
        zoneDeliveryModeValue.setMinimum(Double.valueOf(min));
        zoneDeliveryModeValue.setValue(Double.valueOf(value));
        zoneDeliveryModeValue.setZone(zone);
        zoneDeliveryModeValue.setDeliveryMode(zoneDeliveryMode);
        zoneDeliveryModeValue.setPriority(Integer.valueOf(priority));

        return zoneDeliveryModeValue;
    }
}

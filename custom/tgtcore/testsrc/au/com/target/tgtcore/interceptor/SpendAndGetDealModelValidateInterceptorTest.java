package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.promotions.model.SpendAndGetDealModel;
import de.hybris.platform.promotions.model.SpendAndSaveDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.ArrayList;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.enums.DealRewardTypeEnum;
import au.com.target.tgtcore.interceptor.SpendAndGetDealModelValidateInterceptor.ErrorMessages;


@UnitTest
public class SpendAndGetDealModelValidateInterceptorTest {

    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";

    private List<DealRewardTypeEnum> validDealTypes;

    private final SpendAndGetDealModelValidateInterceptor spendAndGetDealModelValidateInterceptor = new SpendAndGetDealModelValidateInterceptor();

    @Before
    public void setUp() {
        validDealTypes = new ArrayList<>();
        validDealTypes.add(DealRewardTypeEnum.DOLLAROFFEACH);
        validDealTypes.add(DealRewardTypeEnum.FIXEDDOLLAREACH);
        validDealTypes.add(DealRewardTypeEnum.PERCENTOFFEACH);

        spendAndGetDealModelValidateInterceptor.setValidDealTypes(validDealTypes);
    }

    @Test
    public void testOnValidateWithNullModel() throws InterceptorException {
        spendAndGetDealModelValidateInterceptor.onValidate(null, null);
    }

    @Test
    public void testOnValidateWithWrongModelType() throws InterceptorException {
        final SpendAndSaveDealModel mockModel = Mockito.mock(SpendAndSaveDealModel.class);

        spendAndGetDealModelValidateInterceptor.onValidate(mockModel, null);

        Mockito.verifyZeroInteractions(mockModel);
    }

    @Test
    public void testOnValidateWithInvalidDealType() {
        final SpendAndGetDealModel mockSpendAndGetDealModel = Mockito.mock(SpendAndGetDealModel.class);
        BDDMockito.given(mockSpendAndGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.FIXEDDOLLARTOTAL);

        try {
            spendAndGetDealModelValidateInterceptor.onValidate(mockSpendAndGetDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).startsWith(
                    EXCEPTION_MESSAGE_PREFIX + String.format(ErrorMessages.INVALID_DEAL_REWARD_TYPE, ""));
        }
    }

    @Test
    public void testOnValidateWithDealEnabledNotEnoughQualifiers() {
        final SpendAndGetDealModel mockSpendAndGetDealModel = Mockito.mock(SpendAndGetDealModel.class);
        BDDMockito.given(mockSpendAndGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.DOLLAROFFEACH);
        BDDMockito.given(mockSpendAndGetDealModel.getEnabled()).willReturn(Boolean.TRUE);

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        BDDMockito.given(mockSpendAndGetDealModel.getQualifierList()).willReturn(dealQualifierList);

        try {
            spendAndGetDealModelValidateInterceptor.onValidate(mockSpendAndGetDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.NOT_ENOUGH_QUALIFIERS);
        }
    }

    @Test
    public void testOnValidateWithTooManyQualifiers() {
        final SpendAndGetDealModel mockSpendAndGetDealModel = Mockito.mock(SpendAndGetDealModel.class);
        BDDMockito.given(mockSpendAndGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.DOLLAROFFEACH);

        final DealQualifierModel mockDealQualifier1 = Mockito.mock(DealQualifierModel.class);
        final DealQualifierModel mockDealQualifier2 = Mockito.mock(DealQualifierModel.class);

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        dealQualifierList.add(mockDealQualifier1);
        dealQualifierList.add(mockDealQualifier2);

        BDDMockito.given(mockSpendAndGetDealModel.getQualifierList()).willReturn(dealQualifierList);

        try {
            spendAndGetDealModelValidateInterceptor.onValidate(mockSpendAndGetDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.TOO_MANY_QUALIFIERS);
        }
    }

    @Test
    public void testOnValidateWithNoAmountOnQualifier() {
        final SpendAndGetDealModel mockSpendAndGetDealModel = Mockito.mock(SpendAndGetDealModel.class);
        BDDMockito.given(mockSpendAndGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.DOLLAROFFEACH);

        final DealQualifierModel mockDealQualifier = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifier.getMinAmt()).willReturn(null);

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        dealQualifierList.add(mockDealQualifier);

        BDDMockito.given(mockSpendAndGetDealModel.getQualifierList()).willReturn(dealQualifierList);

        try {
            spendAndGetDealModelValidateInterceptor.onValidate(mockSpendAndGetDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.QUALIFIER_REQUIRES_AMOUNT);
        }
    }

    @Test
    public void testOnValidateWithMinAmtZeroOnQualifier() {
        final SpendAndGetDealModel mockSpendAndGetDealModel = Mockito.mock(SpendAndGetDealModel.class);
        BDDMockito.given(mockSpendAndGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.DOLLAROFFEACH);

        final DealQualifierModel mockDealQualifier = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifier.getMinAmt()).willReturn(Double.valueOf(0d));

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        dealQualifierList.add(mockDealQualifier);

        BDDMockito.given(mockSpendAndGetDealModel.getQualifierList()).willReturn(dealQualifierList);

        try {
            spendAndGetDealModelValidateInterceptor.onValidate(mockSpendAndGetDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.QUALIFIER_REQUIRES_AMOUNT);
        }
    }

    @Test
    public void testOnValidateWithBelowZeroQuantityOnQualifier() {
        final SpendAndGetDealModel mockSpendAndGetDealModel = Mockito.mock(SpendAndGetDealModel.class);
        BDDMockito.given(mockSpendAndGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.DOLLAROFFEACH);

        final DealQualifierModel mockDealQualifier = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifier.getMinQty()).willReturn(Integer.valueOf(-3));

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        dealQualifierList.add(mockDealQualifier);

        BDDMockito.given(mockSpendAndGetDealModel.getQualifierList()).willReturn(dealQualifierList);

        try {
            spendAndGetDealModelValidateInterceptor.onValidate(mockSpendAndGetDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.QUALIFIER_REQUIRES_AMOUNT);
        }
    }

    @Test
    public void testOnValidateDealDisabled() throws InterceptorException {
        final SpendAndGetDealModel mockSpendAndGetDealModel = Mockito.mock(SpendAndGetDealModel.class);
        BDDMockito.given(mockSpendAndGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.DOLLAROFFEACH);
        BDDMockito.given(mockSpendAndGetDealModel.getEnabled()).willReturn(Boolean.FALSE);

        spendAndGetDealModelValidateInterceptor.onValidate(mockSpendAndGetDealModel, null);
    }

    @Test
    public void testOnValidate() throws InterceptorException {
        final SpendAndGetDealModel mockSpendAndGetDealModel = Mockito.mock(SpendAndGetDealModel.class);
        BDDMockito.given(mockSpendAndGetDealModel.getRewardType()).willReturn(DealRewardTypeEnum.DOLLAROFFEACH);
        BDDMockito.given(mockSpendAndGetDealModel.getEnabled()).willReturn(Boolean.TRUE);

        final DealQualifierModel mockDealQualifier = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifier.getMinAmt()).willReturn(Double.valueOf(1d));

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        dealQualifierList.add(mockDealQualifier);

        BDDMockito.given(mockSpendAndGetDealModel.getQualifierList()).willReturn(dealQualifierList);

        spendAndGetDealModelValidateInterceptor.onValidate(mockSpendAndGetDealModel, null);
    }
}

package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.BuyGetDealModel;
import de.hybris.platform.promotions.model.DealQualifierModel;
import de.hybris.platform.promotions.model.SpendAndSaveDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.ArrayList;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.enums.DealRewardTypeEnum;
import au.com.target.tgtcore.interceptor.SpendAndSaveDealModelValidateInterceptor.ErrorMessages;


@UnitTest
public class SpendAndSaveDealModelValidateInterceptorTest {

    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";

    private List<DealRewardTypeEnum> validDealTypes;

    private final SpendAndSaveDealModelValidateInterceptor spendAndSaveDealModelValidateInterceptor = new SpendAndSaveDealModelValidateInterceptor();

    @Before
    public void setUp() {
        validDealTypes = new ArrayList<>();
        validDealTypes.add(DealRewardTypeEnum.DOLLAROFFTOTAL);
        validDealTypes.add(DealRewardTypeEnum.PERCENTOFFEACH);

        spendAndSaveDealModelValidateInterceptor.setValidDealTypes(validDealTypes);
    }

    @Test
    public void testOnValidateWithNullModel() throws InterceptorException {
        spendAndSaveDealModelValidateInterceptor.onValidate(null, null);
    }

    public void testOnValidateWithWrongModelType() throws InterceptorException {
        final BuyGetDealModel mockModel = Mockito.mock(BuyGetDealModel.class);

        spendAndSaveDealModelValidateInterceptor.onValidate(mockModel, null);

        Mockito.verifyZeroInteractions(mockModel);
    }

    @Test
    public void testOnValidateWithInvalidDealType() {
        final SpendAndSaveDealModel mockSpendAndSaveDealModel = Mockito.mock(SpendAndSaveDealModel.class);
        BDDMockito.given(mockSpendAndSaveDealModel.getRewardType()).willReturn(DealRewardTypeEnum.FIXEDDOLLARTOTAL);

        try {
            spendAndSaveDealModelValidateInterceptor.onValidate(mockSpendAndSaveDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).startsWith(
                    EXCEPTION_MESSAGE_PREFIX + String.format(ErrorMessages.INVALID_DEAL_REWARD_TYPE, ""));
        }
    }

    @Test
    public void testOnValidateWithDealEnabledNotEnoughQualifiers() {
        final SpendAndSaveDealModel mockSpendAndSaveDealModel = Mockito.mock(SpendAndSaveDealModel.class);
        BDDMockito.given(mockSpendAndSaveDealModel.getRewardType()).willReturn(DealRewardTypeEnum.DOLLAROFFTOTAL);
        BDDMockito.given(mockSpendAndSaveDealModel.getEnabled()).willReturn(Boolean.TRUE);

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        BDDMockito.given(mockSpendAndSaveDealModel.getQualifierList()).willReturn(dealQualifierList);

        try {
            spendAndSaveDealModelValidateInterceptor.onValidate(mockSpendAndSaveDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.NOT_ENOUGH_QUALIFIERS);
        }
    }

    @Test
    public void testOnValidateWithTooManyQualifiers() {
        final SpendAndSaveDealModel mockSpendAndSaveDealModel = Mockito.mock(SpendAndSaveDealModel.class);
        BDDMockito.given(mockSpendAndSaveDealModel.getRewardType()).willReturn(DealRewardTypeEnum.DOLLAROFFTOTAL);

        final DealQualifierModel mockDealQualifier1 = Mockito.mock(DealQualifierModel.class);
        final DealQualifierModel mockDealQualifier2 = Mockito.mock(DealQualifierModel.class);

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        dealQualifierList.add(mockDealQualifier1);
        dealQualifierList.add(mockDealQualifier2);

        BDDMockito.given(mockSpendAndSaveDealModel.getQualifierList()).willReturn(dealQualifierList);

        try {
            spendAndSaveDealModelValidateInterceptor.onValidate(mockSpendAndSaveDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.TOO_MANY_QUALIFIERS);
        }
    }

    @Test
    public void testOnValidateWithNoQuantityOnQualifier() {
        final SpendAndSaveDealModel mockSpendAndSaveDealModel = Mockito.mock(SpendAndSaveDealModel.class);
        BDDMockito.given(mockSpendAndSaveDealModel.getRewardType()).willReturn(DealRewardTypeEnum.DOLLAROFFTOTAL);

        final DealQualifierModel mockDealQualifier = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifier.getMinAmt()).willReturn(null);

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        dealQualifierList.add(mockDealQualifier);

        BDDMockito.given(mockSpendAndSaveDealModel.getQualifierList()).willReturn(dealQualifierList);

        try {
            spendAndSaveDealModelValidateInterceptor.onValidate(mockSpendAndSaveDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.QUALIFIER_REQUIRES_AMOUNT);
        }
    }

    @Test
    public void testOnValidateWithZeroQuantityOnQualifier() {
        final SpendAndSaveDealModel mockSpendAndSaveDealModel = Mockito.mock(SpendAndSaveDealModel.class);
        BDDMockito.given(mockSpendAndSaveDealModel.getRewardType()).willReturn(DealRewardTypeEnum.DOLLAROFFTOTAL);

        final DealQualifierModel mockDealQualifier = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifier.getMinAmt()).willReturn(Double.valueOf(0d));

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        dealQualifierList.add(mockDealQualifier);

        BDDMockito.given(mockSpendAndSaveDealModel.getQualifierList()).willReturn(dealQualifierList);

        try {
            spendAndSaveDealModelValidateInterceptor.onValidate(mockSpendAndSaveDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.QUALIFIER_REQUIRES_AMOUNT);
        }
    }

    @Test
    public void testOnValidateWithBelowZeroQuantityOnQualifier() {
        final SpendAndSaveDealModel mockSpendAndSaveDealModel = Mockito.mock(SpendAndSaveDealModel.class);
        BDDMockito.given(mockSpendAndSaveDealModel.getRewardType()).willReturn(DealRewardTypeEnum.DOLLAROFFTOTAL);

        final DealQualifierModel mockDealQualifier = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifier.getMinAmt()).willReturn(Double.valueOf(-3d));

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        dealQualifierList.add(mockDealQualifier);

        BDDMockito.given(mockSpendAndSaveDealModel.getQualifierList()).willReturn(dealQualifierList);

        try {
            spendAndSaveDealModelValidateInterceptor.onValidate(mockSpendAndSaveDealModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.QUALIFIER_REQUIRES_AMOUNT);
        }
    }

    @Test
    public void testOnValidateDealDisabled() throws InterceptorException {
        final SpendAndSaveDealModel mockSpendAndSaveDealModel = Mockito.mock(SpendAndSaveDealModel.class);
        BDDMockito.given(mockSpendAndSaveDealModel.getRewardType()).willReturn(DealRewardTypeEnum.DOLLAROFFTOTAL);
        BDDMockito.given(mockSpendAndSaveDealModel.getEnabled()).willReturn(Boolean.FALSE);

        spendAndSaveDealModelValidateInterceptor.onValidate(mockSpendAndSaveDealModel, null);
    }

    @Test
    public void testOnValidate() throws InterceptorException {
        final SpendAndSaveDealModel mockSpendAndSaveDealModel = Mockito.mock(SpendAndSaveDealModel.class);
        BDDMockito.given(mockSpendAndSaveDealModel.getRewardType()).willReturn(DealRewardTypeEnum.DOLLAROFFTOTAL);
        BDDMockito.given(mockSpendAndSaveDealModel.getEnabled()).willReturn(Boolean.TRUE);

        final DealQualifierModel mockDealQualifier = Mockito.mock(DealQualifierModel.class);
        BDDMockito.given(mockDealQualifier.getMinAmt()).willReturn(Double.valueOf(1d));

        final List<DealQualifierModel> dealQualifierList = new ArrayList<>();
        dealQualifierList.add(mockDealQualifier);

        BDDMockito.given(mockSpendAndSaveDealModel.getQualifierList()).willReturn(dealQualifierList);

        spendAndSaveDealModelValidateInterceptor.onValidate(mockSpendAndSaveDealModel, null);
    }
}

/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetAllProductCategoryZDMVRestrictionModel;


/**
 * @author Umesh
 * 
 */
@UnitTest
public class TargetAllProductCategoryZDMVRestrictionValidatorTest {

    private TargetAllProductCategoryZDMVRestrictionValidator validator;

    private List<ProductModel> products;

    private List<CategoryModel> categories;

    @Mock
    private TargetAllProductCategoryZDMVRestrictionModel restrictionModel;

    @Mock
    private ProductModel product;

    @Mock
    private CategoryModel category;

    @Mock
    private ProductTypeModel productType;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        validator = new TargetAllProductCategoryZDMVRestrictionValidator();
        products = new ArrayList<>();
        categories = new ArrayList<>();
    }

    @Test(expected = InterceptorException.class)
    public void testEmptyProductsAndCategories() throws InterceptorException {
        given(restrictionModel.getIncludedProducts()).willReturn(null);
        given(restrictionModel.getIncludedCategories()).willReturn(null);

        validator.onValidate(restrictionModel, null);
    }

    @Test
    public void testSomeProductsAndEmptyCategories() throws InterceptorException {
        products.add(product);

        given(restrictionModel.getIncludedProducts()).willReturn(products);
        given(restrictionModel.getIncludedCategories()).willReturn(null);

        validator.onValidate(restrictionModel, null);
    }

    @Test
    public void testEmptyProductsAndSomeCategories() throws InterceptorException {
        categories.add(category);

        given(restrictionModel.getIncludedProducts()).willReturn(null);
        given(restrictionModel.getIncludedCategories()).willReturn(categories);

        validator.onValidate(restrictionModel, null);
    }

    @Test
    public void testProductsAndCategories() throws InterceptorException {
        products.add(product);
        categories.add(category);

        given(restrictionModel.getIncludedProducts()).willReturn(products);
        given(restrictionModel.getIncludedCategories()).willReturn(categories);

        validator.onValidate(restrictionModel, null);
    }

    @Test(expected = InterceptorException.class)
    public void testEmptyProductsAndCategoriesAndProductType() throws InterceptorException {
        given(restrictionModel.getIncludedProducts()).willReturn(null);
        given(restrictionModel.getIncludedCategories()).willReturn(null);
        given(restrictionModel.getProductType()).willReturn(null);
        validator.onValidate(restrictionModel, null);
    }

    @Test
    public void testProductType() throws InterceptorException {
        given(restrictionModel.getProductType()).willReturn(productType);
        validator.onValidate(restrictionModel, null);
    }
}

/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;


/**
 * @author pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetColourVariantProductPrepareInterceptorTest {

    @InjectMocks
    private final TargetColourVariantProductPrepareInterceptor prepareInterceptor = new TargetColourVariantProductPrepareInterceptor();

    private TargetColourVariantProductModel colourVariantProduct;

    @Before
    public void setUp() throws Exception {
        colourVariantProduct = new TargetColourVariantProductModel();
        prepareInterceptor.setMaxNewLowerPriceDisplayTime(6);
    }

    @Test
    public void testOnPrepareWhenNLPStartDateDoesntExist() throws InterceptorException {
        prepareInterceptor.onPrepare(colourVariantProduct, null);
        assertNull(colourVariantProduct.getNewLowerPriceEndDate());
    }

    @Test
    public void testOnPrepareWhenNLPStartDateExistsAndDateIsMissing() throws InterceptorException {
        final Date currentDate = new Date();
        colourVariantProduct.setNewLowerPriceStartDate(currentDate);

        prepareInterceptor.onPrepare(colourVariantProduct, null);
        assertNotNull(colourVariantProduct.getNewLowerPriceEndDate());
        assertEquals(DateUtils.addMonths(currentDate, 6), colourVariantProduct.getNewLowerPriceEndDate());
    }

    @Test
    public void testOnPrepareWhenNLPStartDateExistsAndEndDateIsGreaterThanAllowed() throws InterceptorException {
        final Date currentDate = new Date();
        colourVariantProduct.setNewLowerPriceStartDate(currentDate);
        colourVariantProduct.setNewLowerPriceEndDate(DateUtils.addMonths(currentDate, 12));

        prepareInterceptor.onPrepare(colourVariantProduct, null);
        assertNotNull(colourVariantProduct.getNewLowerPriceEndDate());
        assertEquals(DateUtils.addMonths(currentDate, 6), colourVariantProduct.getNewLowerPriceEndDate());
    }

    @Test
    public void testOnPrepareWhenNLPStartDateExistsAndEndDateIsAllowedValue() throws InterceptorException {
        final Date currentDate = new Date();
        colourVariantProduct.setNewLowerPriceStartDate(currentDate);
        colourVariantProduct.setNewLowerPriceEndDate(DateUtils.addMonths(currentDate, 5));

        prepareInterceptor.onPrepare(colourVariantProduct, null);
        assertNotNull(colourVariantProduct.getNewLowerPriceEndDate());
        assertEquals(DateUtils.addMonths(currentDate, 5), colourVariantProduct.getNewLowerPriceEndDate());
    }

}

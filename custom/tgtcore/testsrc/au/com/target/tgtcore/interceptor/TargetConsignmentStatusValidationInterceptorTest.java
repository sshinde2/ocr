/**
 *
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Test case to check interceptor to validate ConsignmentStatus
 *
 * @author jjayawa1
 *
 */
@UnitTest
public class TargetConsignmentStatusValidationInterceptorTest {

    private final TargetConsignmentStatusValidationInterceptor consignmentStatusInterceptor = new TargetConsignmentStatusValidationInterceptor();

    @Mock
    private InterceptorContext interceptorContext;

    @Mock
    private ConsignmentModel consignment;

    private List<ConsignmentStatus> invalidConsignmentStatuses;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        invalidConsignmentStatuses = new ArrayList<>();
        invalidConsignmentStatuses.add(ConsignmentStatus.WAITING);
        invalidConsignmentStatuses.add(ConsignmentStatus.PICKPACK);
        invalidConsignmentStatuses.add(ConsignmentStatus.READY);
        consignmentStatusInterceptor.setInvalidConsignmentStatuses(invalidConsignmentStatuses);
    }

    @Test(expected = InterceptorException.class)
    public void testInvlidConsignmentStatus() throws InterceptorException {
        BDDMockito.given(consignment.getStatus()).willReturn(ConsignmentStatus.WAITING);
        consignmentStatusInterceptor.onValidate(consignment, interceptorContext);
    }

    @Test
    public void testNullValueList() throws InterceptorException {
        boolean testFailed = false;

        try {
            consignmentStatusInterceptor.setInvalidConsignmentStatuses(null);
            consignmentStatusInterceptor.onValidate(consignment, interceptorContext);
        }
        catch (final NullPointerException e) {
            testFailed = true;
            Assert.assertTrue("Null list is not handled", false);
        }

        if (!testFailed) {
            Assert.assertTrue("Null list is handled", true);
        }
    }

    @Test
    public void testWithValidConsignmentStatuses() throws InterceptorException {
        boolean testFailed = false;

        try {
            BDDMockito.given(consignment.getStatus()).willReturn(ConsignmentStatus.CREATED);
            consignmentStatusInterceptor.onValidate(consignment, interceptorContext);
        }
        catch (final InterceptorException e) {
            testFailed = true;
            Assert.assertTrue("A valid ConsignmentStatus caused an InterceptorException",
                    false);
        }

        if (!testFailed) {
            Assert.assertTrue("A valid ConsignmentStatus was passed in", true);
        }
    }
}

/**
 *
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;


/**
 * Test case to check interceptor to validate CreditCardType
 *
 * @author jjayawa1
 *
 */
@UnitTest
public class TargetCreditCardTypeInterceptorTest {
    private final TargetCreditCardTypeInterceptor creditCardTypeInterceptor = new TargetCreditCardTypeInterceptor();

    @Mock
    private InterceptorContext interceptorContext;

    @Mock
    private CreditCardPaymentInfoModel creditCardPaymentInfo;

    @Mock
    private PinPadPaymentInfoModel pinPadPaymentInfo;

    private List<CreditCardType> invalidCards;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        invalidCards = new ArrayList<>();
        invalidCards.add(CreditCardType.MAESTRO);
        invalidCards.add(CreditCardType.MASTERCARD_EUROCARD);
        invalidCards.add(CreditCardType.SWITCH);
        creditCardTypeInterceptor.setInvalidCreditCardTypes(invalidCards);
    }

    @Test(expected = InterceptorException.class)
    public void testInvalidCardInCreditCardPaymentInfo() throws InterceptorException {
        BDDMockito.given(creditCardPaymentInfo.getType()).willReturn(CreditCardType.MAESTRO);
        creditCardTypeInterceptor.onValidate(creditCardPaymentInfo, interceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testInvalidPinPadPaymentInfo() throws InterceptorException {
        BDDMockito.given(pinPadPaymentInfo.getCardType()).willReturn(CreditCardType.SWITCH);
        creditCardTypeInterceptor.onValidate(pinPadPaymentInfo, interceptorContext);
    }

    @Test
    public void testNullValueListForCreditCardPayment() throws InterceptorException {
        boolean testFailed = false;

        try {
            creditCardTypeInterceptor.setInvalidCreditCardTypes(null);
            creditCardTypeInterceptor.onValidate(creditCardPaymentInfo, interceptorContext);
        }
        catch (final NullPointerException e) {
            testFailed = true;
            Assert.assertTrue("Null list is not handled", false);
        }

        if (!testFailed) {
            Assert.assertTrue("Null list is handled", true);
        }
    }

    @Test
    public void testNullValueListForPinPadPayment() throws InterceptorException {
        boolean testFailed = false;

        try {
            creditCardTypeInterceptor.setInvalidCreditCardTypes(null);
            creditCardTypeInterceptor.onValidate(pinPadPaymentInfo, interceptorContext);
        }
        catch (final NullPointerException e) {
            testFailed = true;
            Assert.assertTrue("Null list is not handled", false);
        }

        if (!testFailed) {
            Assert.assertTrue("Null list is handled", true);
        }
    }

    @Test
    public void testWithValidCreditCardsForCreditCardPaymentInfo() throws InterceptorException {
        boolean testFailed = false;

        try {
            BDDMockito.given(creditCardPaymentInfo.getType()).willReturn(CreditCardType.VISA);
            creditCardTypeInterceptor.onValidate(creditCardPaymentInfo, interceptorContext);
        }
        catch (final InterceptorException e) {
            testFailed = true;
            Assert.assertTrue("A valid Credit card caused an InterceptorException",
                    false);
        }

        if (!testFailed) {
            Assert.assertTrue("A valid ConsignmentStatus was passed in", true);
        }
    }

    @Test
    public void testWithValidCreditCardsForPinPadPaymentInfo() throws InterceptorException {
        boolean testFailed = false;

        try {
            BDDMockito.given(pinPadPaymentInfo.getCardType()).willReturn(CreditCardType.VISA);
            creditCardTypeInterceptor.onValidate(pinPadPaymentInfo, interceptorContext);
        }
        catch (final InterceptorException e) {
            testFailed = true;
            Assert.assertTrue("A valid Credit card caused an InterceptorException",
                    false);
        }

        if (!testFailed) {
            Assert.assertTrue("A valid ConsignmentStatus was passed in", true);
        }
    }
}

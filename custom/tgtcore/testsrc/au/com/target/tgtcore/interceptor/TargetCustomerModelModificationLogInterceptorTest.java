package au.com.target.tgtcore.interceptor;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.MapAssert.entry;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.RETURNS_MOCKS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.model.ItemModelContext;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import au.com.target.tgtcore.customer.TargetCustomerModelModificationLogEvent;
import au.com.target.tgtcore.model.TargetCustomerModel;


@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest({ JaloSession.class, PropertyUtils.class })
public class TargetCustomerModelModificationLogInterceptorTest {

    @InjectMocks
    private final TargetCustomerModelModificationLogInterceptor targetCustomerModelModificationLogInterceptor = new TargetCustomerModelModificationLogInterceptor();

    @Mock
    private EventService eventService;

    @Mock
    private TargetCustomerModel targetCustomerModel;

    @Mock
    private ItemModelContext itemModelContext;

    @Mock
    private InterceptorContext interceptorContext;

    private User user;

    private Set<String> dirtyAttributes;

    private Set<String> attributesToLog;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        dirtyAttributes = new HashSet<>();
        dirtyAttributes.add("firstname");
        dirtyAttributes.add("uid");

        attributesToLog = new HashSet<>();
        attributesToLog.add("firstname");
        attributesToLog.add("uid");

        targetCustomerModelModificationLogInterceptor.setAttributesToLog(attributesToLog);
        given(targetCustomerModel.getItemModelContext()).willReturn(itemModelContext);

        given(itemModelContext.getOriginalValue("firstname")).willReturn("John");
        given(PropertyUtils.getProperty(targetCustomerModel, "firstname")).willReturn("Johnn");

        given(itemModelContext.getOriginalValue("uid")).willReturn("John@gmail.com");
        given(PropertyUtils.getProperty(targetCustomerModel, "uid")).willReturn("Johnn@gmail.com");
        given(itemModelContext.getPK()).willReturn(PK.fromLong(9999l));

        user = mock(User.class);
        final JaloSession jaloSession = mock(JaloSession.class);
        PowerMockito.mockStatic(JaloSession.class);
        PowerMockito.when(JaloSession.getCurrentSession()).thenReturn(jaloSession);
        PowerMockito.when(jaloSession.getUser()).thenReturn(user);

    }

    @Test
    public void testOnValidateWithValidDirtAttributes()
            throws InterceptorException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {

        given(itemModelContext.getDirtyAttributes()).willReturn(dirtyAttributes);
        willReturn(Boolean.TRUE).given(itemModelContext).isLoaded("firstname");
        willReturn(Boolean.TRUE).given(itemModelContext).isLoaded("uid");

        targetCustomerModelModificationLogInterceptor.onValidate(targetCustomerModel, interceptorContext);
        final ArgumentCaptor<TargetCustomerModelModificationLogEvent> argumentCaptor = ArgumentCaptor
                .forClass(TargetCustomerModelModificationLogEvent.class);

        verify(eventService).publishEvent(argumentCaptor.capture());
        assertThat(argumentCaptor.getAllValues()).hasSize(1);

        final List<TargetCustomerModelModificationLogEvent> events = argumentCaptor.getAllValues();
        assertThat(events).hasSize(1);
        assertThat(events).onProperty("previousValueMap").hasSize(1);
        assertThat(events).onProperty("newValueMap").hasSize(1);

        assertThat(events.get(0).getPreviousValueMap()).includes(entry("uid", "John@gmail.com"),
                entry("firstname", "John"));
        assertThat(events.get(0).getNewValueMap()).includes(entry("uid", "Johnn@gmail.com"),
                entry("firstname", "Johnn"));
        assertThat(events.get(0).getItemPK()).isEqualTo(PK.fromLong(9999l));
        assertThat(events.get(0).getUser()).isEqualTo(user);
    }



    @Test
    public void testOnValidateOneAttributeNotLoaded()
            throws InterceptorException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {


        given(itemModelContext.getDirtyAttributes()).willReturn(dirtyAttributes);
        willReturn(Boolean.FALSE).given(itemModelContext).isLoaded("firstname");
        willReturn(Boolean.TRUE).given(itemModelContext).isLoaded("uid");

        targetCustomerModelModificationLogInterceptor.onValidate(targetCustomerModel, interceptorContext);
        final ArgumentCaptor<TargetCustomerModelModificationLogEvent> argumentCaptor = ArgumentCaptor
                .forClass(TargetCustomerModelModificationLogEvent.class);

        verify(eventService).publishEvent(argumentCaptor.capture());
        assertThat(argumentCaptor.getAllValues()).hasSize(1);

        final List<TargetCustomerModelModificationLogEvent> events = argumentCaptor.getAllValues();
        assertThat(events).hasSize(1);
        assertThat(events).onProperty("previousValueMap").hasSize(1);
        assertThat(events).onProperty("newValueMap").hasSize(1);

        assertThat(events.get(0).getPreviousValueMap()).includes(entry("uid", "John@gmail.com"));
        assertThat(events.get(0).getNewValueMap()).includes(entry("uid", "Johnn@gmail.com"));
        assertThat(events.get(0).getItemPK()).isEqualTo(PK.fromLong(9999l));
        assertThat(events.get(0).getUser()).isEqualTo(user);

    }


    @Test
    public void testOnValidateEmptyDirtyAttributes()
            throws InterceptorException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {

        given(itemModelContext.getDirtyAttributes()).willReturn(Collections.EMPTY_SET);

        targetCustomerModelModificationLogInterceptor.onValidate(targetCustomerModel, interceptorContext);
        final ArgumentCaptor<TargetCustomerModelModificationLogEvent> argumentCaptor = ArgumentCaptor
                .forClass(TargetCustomerModelModificationLogEvent.class);

        verify(eventService, never()).publishEvent(argumentCaptor.capture());

    }

    @Test
    public void testOnValidateNoLoadedAttributes()
            throws InterceptorException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {

        given(itemModelContext.getDirtyAttributes()).willReturn(dirtyAttributes);
        willReturn(Boolean.FALSE).given(itemModelContext).isLoaded("firstname");
        willReturn(Boolean.FALSE).given(itemModelContext).isLoaded("uid");

        targetCustomerModelModificationLogInterceptor.onValidate(targetCustomerModel, interceptorContext);
        final ArgumentCaptor<TargetCustomerModelModificationLogEvent> argumentCaptor = ArgumentCaptor
                .forClass(TargetCustomerModelModificationLogEvent.class);

        verify(eventService, never()).publishEvent(argumentCaptor.capture());

    }

    @Test
    public void testOnEmptyNewValueMap()
            throws Exception {

        given(itemModelContext.getDirtyAttributes()).willReturn(dirtyAttributes);
        willReturn(Boolean.TRUE).given(itemModelContext).isLoaded("firstname");
        willReturn(Boolean.FALSE).given(itemModelContext).isLoaded("uid");

        PowerMockito.mockStatic(PropertyUtils.class, RETURNS_MOCKS);
        PowerMockito.doThrow(new IllegalAccessException()).when(PropertyUtils.class, "getProperty", targetCustomerModel,
                "firstname");

        targetCustomerModelModificationLogInterceptor.onValidate(targetCustomerModel, interceptorContext);
        final ArgumentCaptor<TargetCustomerModelModificationLogEvent> argumentCaptor = ArgumentCaptor
                .forClass(TargetCustomerModelModificationLogEvent.class);

        verify(eventService, never()).publishEvent(argumentCaptor.capture());

    }


}

/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.i18n.L10NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.order.TargetCommerceCheckoutService;


/**
 * Unit test for {@link TargetCustomerValidateInterceptor}
 * 
 */
@UnitTest
public class TargetCustomerValidateInterceptorTest {

    private static final String FIRST_NAME = "First Name";
    private static final String LAST_NAME = "Last Name";
    private static final String PHONE_NUMBER = "Phone Number";
    private static final String EMAIL_ID = "Email ID";
    private static final String MOBILE_NUMBER = "Mobile Number";
    private static final String FLYBUYS_CODE = "FlyBuys code";
    private static final String NULL_MESSAGE = "cannot be blank";
    private static final String INVALID_MESSAGE = "must be valid";

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException exception = ExpectedException.none();
    //CHECKSTYLE:ON

    private TargetCustomerValidateInterceptor targetCustomerValidateInterceptor;

    @Mock
    private L10NService l10NService;

    @Mock
    private InterceptorContext context;

    @Mock
    private TargetCommerceCheckoutService targetCommerceCheckoutService;

    /**
     * @throws java.lang.Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        targetCustomerValidateInterceptor = new TargetCustomerValidateInterceptor();

        BDDMockito.when(l10NService.getLocalizedString("type.targetcustomer.firstname.name")).thenReturn(FIRST_NAME);
        BDDMockito.when(l10NService.getLocalizedString("type.targetcustomer.lastname.name")).thenReturn(LAST_NAME);
        BDDMockito.when(l10NService.getLocalizedString("type.targetcustomer.phonenumber.name")).thenReturn(
                PHONE_NUMBER);
        BDDMockito.when(l10NService.getLocalizedString("type.targetcustomer.uid.name")).thenReturn(EMAIL_ID);
        BDDMockito.when(l10NService.getLocalizedString("type.targetcustomer.mobilenumber.name"))
                .thenReturn(MOBILE_NUMBER);
        BDDMockito.when(l10NService.getLocalizedString("type.targetcustomer.flybuyscode.name"))
                .thenReturn(FLYBUYS_CODE);

        targetCustomerValidateInterceptor.setL10NService(l10NService);
        targetCustomerValidateInterceptor.setTargetCommerceCheckoutService(targetCommerceCheckoutService);
    }

    @Test
    public void testOnValidateMandatoryFirstname() throws InterceptorException {
        final TargetCustomerModel model = BDDMockito.mock(TargetCustomerModel.class);

        BDDMockito.given(model.getFirstname()).willReturn("");

        exception.expect(InterceptorException.class);
        exception.expectMessage(FIRST_NAME);
        exception.expectMessage(NULL_MESSAGE);
        targetCustomerValidateInterceptor.onValidate(model, context);
    }

    @Test
    public void testOnValidateInvalidLengthFirstname() throws InterceptorException {
        final TargetCustomerModel model = BDDMockito.mock(TargetCustomerModel.class);

        BDDMockito.given(model.getFirstname()).willReturn("A");
        BDDMockito.given(model.getLastname()).willReturn("notnull");
        BDDMockito.given(model.getContactEmail()).willReturn("notnull");
        BDDMockito.given(model.getPhoneNumber()).willReturn("123456789");

        exception.expect(InterceptorException.class);
        exception.expectMessage(FIRST_NAME);
        exception.expectMessage(INVALID_MESSAGE);
        targetCustomerValidateInterceptor.onValidate(model, context);
    }

    @Test
    public void testOnValidateMandatoryLastname() throws InterceptorException {
        final TargetCustomerModel model = BDDMockito.mock(TargetCustomerModel.class);

        BDDMockito.given(model.getFirstname()).willReturn("firstname");
        BDDMockito.given(model.getLastname()).willReturn("");

        exception.expect(InterceptorException.class);
        exception.expectMessage(LAST_NAME);
        exception.expectMessage(NULL_MESSAGE);
        targetCustomerValidateInterceptor.onValidate(model, context);
    }

    @Test
    public void testOnValidateInvalidLengthLastname() throws InterceptorException {
        final TargetCustomerModel model = BDDMockito.mock(TargetCustomerModel.class);

        BDDMockito.given(model.getFirstname()).willReturn("firstname");
        BDDMockito.given(model.getLastname()).willReturn("L");
        BDDMockito.given(model.getContactEmail()).willReturn("notnull");
        BDDMockito.given(model.getPhoneNumber()).willReturn("132456789");

        exception.expect(InterceptorException.class);
        exception.expectMessage(LAST_NAME);
        exception.expectMessage(INVALID_MESSAGE);
        targetCustomerValidateInterceptor.onValidate(model, context);
    }

    @Test
    public void testOnValidateMandatoryEmail() throws InterceptorException {
        final TargetCustomerModel model = BDDMockito.mock(TargetCustomerModel.class);

        BDDMockito.given(model.getFirstname()).willReturn("firstname");
        BDDMockito.given(model.getLastname()).willReturn("lastname");
        BDDMockito.given(model.getContactEmail()).willReturn("");

        exception.expect(InterceptorException.class);
        exception.expectMessage(EMAIL_ID);
        exception.expectMessage(NULL_MESSAGE);
        targetCustomerValidateInterceptor.onValidate(model, context);
    }

    @Test
    public void testOnValidateInvalidEmail() throws InterceptorException {
        final TargetCustomerModel model = BDDMockito.mock(TargetCustomerModel.class);

        BDDMockito.given(model.getFirstname()).willReturn("firstname");
        BDDMockito.given(model.getLastname()).willReturn("lastname");
        BDDMockito.given(model.getPhoneNumber()).willReturn("123456789");
        BDDMockito.given(model.getContactEmail()).willReturn("very.(@");

        exception.expect(InterceptorException.class);
        exception.expectMessage(EMAIL_ID);
        exception.expectMessage(INVALID_MESSAGE);
        targetCustomerValidateInterceptor.onValidate(model, context);
    }

    @Test
    public void testOnValidateInvalidPhoneNumber() throws InterceptorException {
        final TargetCustomerModel model = BDDMockito.mock(TargetCustomerModel.class);

        BDDMockito.given(model.getFirstname()).willReturn("firstname");
        BDDMockito.given(model.getLastname()).willReturn("lastname");
        BDDMockito.given(model.getContactEmail()).willReturn("example@email.com");
        BDDMockito.given(model.getPhoneNumber()).willReturn("12");

        exception.expect(InterceptorException.class);
        exception.expectMessage(PHONE_NUMBER);
        exception.expectMessage(INVALID_MESSAGE);
        targetCustomerValidateInterceptor.onValidate(model, context);
    }

    @Test
    public void testOnValidateInvalidMobileNumber() throws InterceptorException {
        final TargetCustomerModel model = BDDMockito.mock(TargetCustomerModel.class);

        BDDMockito.given(model.getFirstname()).willReturn("firstname");
        BDDMockito.given(model.getLastname()).willReturn("lastname");
        BDDMockito.given(model.getContactEmail()).willReturn("example@email.com");
        BDDMockito.given(model.getPhoneNumber()).willReturn("123456789");
        BDDMockito.given(model.getMobileNumber()).willReturn("onetwothree");

        exception.expect(InterceptorException.class);
        exception.expectMessage(MOBILE_NUMBER);
        exception.expectMessage(INVALID_MESSAGE);
        targetCustomerValidateInterceptor.onValidate(model, context);
    }

    @Test
    public void testOnValidateInvalidFlyBuys() throws InterceptorException {
        final TargetCustomerModel model = BDDMockito.mock(TargetCustomerModel.class);

        BDDMockito.when(Boolean.valueOf(targetCommerceCheckoutService.isFlybuysNumberValid(BDDMockito.anyString())))
                .thenReturn(Boolean.FALSE);

        BDDMockito.given(model.getFirstname()).willReturn("firstname");
        BDDMockito.given(model.getLastname()).willReturn("lastname");
        BDDMockito.given(model.getContactEmail()).willReturn("example@email.com");
        BDDMockito.given(model.getPhoneNumber()).willReturn("123456789");
        BDDMockito.given(model.getMobileNumber()).willReturn("0412345678");
        BDDMockito.given(model.getFlyBuysCode()).willReturn("flybuys");

        exception.expect(InterceptorException.class);
        exception.expectMessage(FLYBUYS_CODE);
        exception.expectMessage(INVALID_MESSAGE);
        targetCustomerValidateInterceptor.onValidate(model, context);
    }

}

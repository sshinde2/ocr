/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetMaxDimensionZDMVRestrictionModel;


/**
 * @author pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetMaxDimensionZDMVRestrictionValidatorTest {

    @InjectMocks
    private final TargetMaxDimensionZDMVRestrictionValidator validator = new TargetMaxDimensionZDMVRestrictionValidator();

    @Mock
    private InterceptorContext ctx;

    @Mock
    private TargetMaxDimensionZDMVRestrictionModel model;

    @Test
    public void testWhenMaxDimensionIsSetOne() throws InterceptorException {
        when(model.getMaxDimension()).thenReturn(Double.valueOf(1.0));
        validator.onValidate(model, ctx);
        assertTrue(true);
    }

    @Test
    public void testWhenMaxDimensionIsZero() throws InterceptorException {
        when(model.getMaxDimension()).thenReturn(Double.valueOf(0.0));
        validator.onValidate(model, ctx);
        assertTrue(true);
    }

    @Test(expected = InterceptorException.class)
    public void testWhenMaxDimensionIsNull() throws InterceptorException {
        when(model.getMaxDimension()).thenReturn(null);
        validator.onValidate(model, ctx);
    }

    @Test(expected = InterceptorException.class)
    public void testWhenMaxDimensionIsNegative() throws InterceptorException {
        when(model.getMaxDimension()).thenReturn(Double.valueOf(-0.1));
        validator.onValidate(model, ctx);
    }

}

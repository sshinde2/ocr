/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetMaxTotalDeadWeightZDMVRestrictionModel;


/**
 * @author pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetMaxTotalDeadWeightZDMVRestrictionValidatorTest {

    @InjectMocks
    private final TargetMaxTotalDeadWeightZDMVRestrictionValidator validator = new TargetMaxTotalDeadWeightZDMVRestrictionValidator();

    @Mock
    private InterceptorContext ctx;

    @Mock
    private TargetMaxTotalDeadWeightZDMVRestrictionModel restrictionModel;

    @Test
    public void testOnValidateWhenMaxDeadWeightIsZero() throws InterceptorException {
        when(restrictionModel.getMaxTotalDeadWeight()).thenReturn(Double.valueOf(0.0));
        validator.onValidate(restrictionModel, ctx);
        assertTrue(true);
    }

    @Test
    public void testOnValidateWhenMaxDeadWeightIsOne() throws InterceptorException {
        when(restrictionModel.getMaxTotalDeadWeight()).thenReturn(Double.valueOf(1.0));
        validator.onValidate(restrictionModel, ctx);
        assertTrue(true);
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidateWhenMaxDeadWeightIsNegative() throws InterceptorException {
        when(restrictionModel.getMaxTotalDeadWeight()).thenReturn(Double.valueOf(-0.1));
        validator.onValidate(restrictionModel, ctx);
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidateWhenMaxDeadWeightWhenWeightIsNull() throws InterceptorException {
        when(restrictionModel.getMaxTotalDeadWeight()).thenReturn(null);
        validator.onValidate(restrictionModel, ctx);
    }

}

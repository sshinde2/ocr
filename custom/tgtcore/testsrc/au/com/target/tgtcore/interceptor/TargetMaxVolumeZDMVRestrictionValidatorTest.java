/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetMaxVolumeZDMVRestrictionModel;


/**
 * @author pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetMaxVolumeZDMVRestrictionValidatorTest {

    @InjectMocks
    private final TargetMaxVolumeZDMVRestrictionValidator validator = new TargetMaxVolumeZDMVRestrictionValidator();

    @Mock
    private InterceptorContext ctx;

    @Mock
    private TargetMaxVolumeZDMVRestrictionModel restrictionModel;

    @Test
    public void testOnValidateWhenMaxVolumeIsZero() throws InterceptorException {
        when(restrictionModel.getMaxVolume()).thenReturn(Double.valueOf(0.0));
        validator.onValidate(restrictionModel, ctx);
        assertTrue(true);
    }

    @Test
    public void testOnValidateWhenMaxVolumeIsPositive() throws InterceptorException {
        when(restrictionModel.getMaxVolume()).thenReturn(Double.valueOf(0.01));
        validator.onValidate(restrictionModel, ctx);
        assertTrue(true);
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidateWhenMaxVolumeIsNegative() throws InterceptorException {
        when(restrictionModel.getMaxVolume()).thenReturn(Double.valueOf(-0.1));
        validator.onValidate(restrictionModel, ctx);
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidateWhenMaxVolumeIsNull() throws InterceptorException {
        when(restrictionModel.getMaxVolume()).thenReturn(null);
        validator.onValidate(restrictionModel, ctx);
    }
}

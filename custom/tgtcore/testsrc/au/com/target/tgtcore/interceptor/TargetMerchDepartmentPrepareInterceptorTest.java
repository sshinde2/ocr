package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetMerchDepartmentModel;


/**
 * Unit test for {@link TargetMerchDepartmentModel}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetMerchDepartmentPrepareInterceptorTest {

    private final TargetMerchDepartmentPrepareInterceptor departmentPrepareInterceptor = new TargetMerchDepartmentPrepareInterceptor();

    @Mock
    private InterceptorContext interceptorContext;

    // Just to prove the method will not throw any exception for nulls
    @Test
    public void testOnPrepareForNull() throws InterceptorException {
        departmentPrepareInterceptor.onPrepare(null, interceptorContext);
    }

    @Test
    public void testOnPrepareForInvalidModels() throws InterceptorException {

        final ItemModel itemModel = Mockito.mock(ItemModel.class);
        departmentPrepareInterceptor.onPrepare(itemModel, interceptorContext);

        Mockito.verifyZeroInteractions(itemModel);
    }

    @Test
    public void testOnPrepareForNullCode() throws InterceptorException {

        final TargetMerchDepartmentModel departmentModel = Mockito.mock(TargetMerchDepartmentModel.class);
        Mockito.when(departmentModel.getCode()).thenReturn(null);

        departmentPrepareInterceptor.onPrepare(departmentModel, interceptorContext);

        Mockito.verify(departmentModel, Mockito.never()).setCode(Mockito.anyString());
    }

    @Test
    public void testOnPrepareForBlankCode() throws InterceptorException {

        final TargetMerchDepartmentModel departmentModel = Mockito.mock(TargetMerchDepartmentModel.class);
        Mockito.when(departmentModel.getCode()).thenReturn("");

        departmentPrepareInterceptor.onPrepare(departmentModel, interceptorContext);

        Mockito.verify(departmentModel, Mockito.never()).setCode(Mockito.anyString());
    }

    @Test
    public void testOnPrepareForCodeNotStartingWithZero() throws InterceptorException {

        final TargetMerchDepartmentModel departmentModel = Mockito.mock(TargetMerchDepartmentModel.class);
        Mockito.when(departmentModel.getCode()).thenReturn("100");

        departmentPrepareInterceptor.onPrepare(departmentModel, interceptorContext);

        Mockito.verify(departmentModel, Mockito.never()).setCode(Mockito.anyString());
    }

    @Test
    public void testOnPrepareForCodeStartingWithZero() throws InterceptorException {

        final TargetMerchDepartmentModel departmentModel = Mockito.mock(TargetMerchDepartmentModel.class);
        Mockito.when(departmentModel.getCode()).thenReturn("010");

        departmentPrepareInterceptor.onPrepare(departmentModel, interceptorContext);

        Mockito.verify(departmentModel).setCode("10");
    }

    @Test
    public void testOnPrepareForCodeStartingWithMultipleZeroes() throws InterceptorException {

        final TargetMerchDepartmentModel departmentModel = Mockito.mock(TargetMerchDepartmentModel.class);
        Mockito.when(departmentModel.getCode()).thenReturn("001");

        departmentPrepareInterceptor.onPrepare(departmentModel, interceptorContext);

        Mockito.verify(departmentModel).setCode("1");
    }

    @Test
    public void testOnPrepareForCodeZero() throws InterceptorException {

        final TargetMerchDepartmentModel departmentModel = Mockito.mock(TargetMerchDepartmentModel.class);
        Mockito.when(departmentModel.getCode()).thenReturn("0");

        departmentPrepareInterceptor.onPrepare(departmentModel, interceptorContext);

        Mockito.verify(departmentModel).setCode("0");
    }

    @Test
    public void testOnPrepareForCodeMultipleZeroes() throws InterceptorException {

        final TargetMerchDepartmentModel departmentModel = Mockito.mock(TargetMerchDepartmentModel.class);
        Mockito.when(departmentModel.getCode()).thenReturn("000");

        departmentPrepareInterceptor.onPrepare(departmentModel, interceptorContext);

        Mockito.verify(departmentModel).setCode("0");
    }


}

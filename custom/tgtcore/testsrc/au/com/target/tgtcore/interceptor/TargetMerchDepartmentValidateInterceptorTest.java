package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.interceptor.TargetMerchDepartmentValidateInterceptor.ErrorMessages;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;


/**
 * Unit test for {@link TargetMerchDepartmentValidateInterceptor}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetMerchDepartmentValidateInterceptorTest {

    // CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    private final TargetMerchDepartmentValidateInterceptor departmentValidateInterceptor = new TargetMerchDepartmentValidateInterceptor();

    @Mock
    private InterceptorContext interceptorContext;

    // Just to prove the method will not throw any exception for nulls
    @Test
    public void testOnValidateForNull() throws InterceptorException {
        departmentValidateInterceptor.onValidate(null, interceptorContext);
    }

    @Test
    public void testOnValidateForInvalidModels() throws InterceptorException {

        final ItemModel itemModel = Mockito.mock(ItemModel.class);
        departmentValidateInterceptor.onValidate(itemModel, interceptorContext);

        Mockito.verifyZeroInteractions(itemModel);
    }

    @Test
    public void testOnValidateForNullCode() throws InterceptorException {

        final TargetMerchDepartmentModel departmentModel = Mockito.mock(TargetMerchDepartmentModel.class);
        Mockito.when(departmentModel.getCode()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage(ErrorMessages.CODE_NULL_EMPTY);

        departmentValidateInterceptor.onValidate(departmentModel, interceptorContext);
    }

    @Test
    public void testOnValidateForBlankCode() throws InterceptorException {

        final TargetMerchDepartmentModel departmentModel = Mockito.mock(TargetMerchDepartmentModel.class);
        Mockito.when(departmentModel.getCode()).thenReturn("");

        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage(ErrorMessages.CODE_NULL_EMPTY);

        departmentValidateInterceptor.onValidate(departmentModel, interceptorContext);
    }

    @Test
    public void testOnValidateForNonNumericCode() throws InterceptorException {

        final TargetMerchDepartmentModel departmentModel = Mockito.mock(TargetMerchDepartmentModel.class);
        Mockito.when(departmentModel.getCode()).thenReturn("abc123");

        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage(ErrorMessages.CODE_NOT_NUMBER);

        departmentValidateInterceptor.onValidate(departmentModel, interceptorContext);
    }

    @Test
    public void testOnValidateForNegativeNumberCode() throws InterceptorException {

        final TargetMerchDepartmentModel departmentModel = Mockito.mock(TargetMerchDepartmentModel.class);
        Mockito.when(departmentModel.getCode()).thenReturn("-123");

        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage(ErrorMessages.CODE_NOT_NUMBER);

        departmentValidateInterceptor.onValidate(departmentModel, interceptorContext);
    }

    @Test
    public void testOnValidateForCodeWithDecimals() throws InterceptorException {

        final TargetMerchDepartmentModel departmentModel = Mockito.mock(TargetMerchDepartmentModel.class);
        Mockito.when(departmentModel.getCode()).thenReturn("123.45");

        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage(ErrorMessages.CODE_NOT_NUMBER);

        departmentValidateInterceptor.onValidate(departmentModel, interceptorContext);
    }

    @Test
    public void testOnValidateForLongCode() throws InterceptorException {

        final TargetMerchDepartmentModel departmentModel = Mockito.mock(TargetMerchDepartmentModel.class);
        Mockito.when(departmentModel.getCode()).thenReturn("123456");

        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage(ErrorMessages.CODE_TOO_LONG);

        departmentValidateInterceptor.onValidate(departmentModel, interceptorContext);
    }

    @Test
    public void testOnValidateForZeroCode() throws InterceptorException {

        final TargetMerchDepartmentModel departmentModel = Mockito.mock(TargetMerchDepartmentModel.class);
        Mockito.when(departmentModel.getCode()).thenReturn("0");

        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage(ErrorMessages.CODE_ZERO_NEGATIVE);

        departmentValidateInterceptor.onValidate(departmentModel, interceptorContext);
    }

    @Test
    public void testOnValidateForCodeMultipleZeroes() throws InterceptorException {

        final TargetMerchDepartmentModel departmentModel = Mockito.mock(TargetMerchDepartmentModel.class);
        Mockito.when(departmentModel.getCode()).thenReturn("00000");

        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage(ErrorMessages.CODE_ZERO_NEGATIVE);

        departmentValidateInterceptor.onValidate(departmentModel, interceptorContext);
    }

}

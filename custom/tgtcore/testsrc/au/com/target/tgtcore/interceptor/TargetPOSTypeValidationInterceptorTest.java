/**
 *
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Test case for TargetPOSTypeValidationInterceptor
 *
 * @author jjayawa1
 *
 */
@UnitTest
public class TargetPOSTypeValidationInterceptorTest {

    private final TargetPOSTypeValidationInterceptor posTypeInterceptor = new TargetPOSTypeValidationInterceptor();

    @Mock
    private InterceptorContext interceptorContext;

    @Mock
    private PointOfServiceModel pointOfService;

    private List<PointOfServiceTypeEnum> invalidPointOfServiceTypes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        invalidPointOfServiceTypes = new ArrayList<>();
        invalidPointOfServiceTypes.add(PointOfServiceTypeEnum.POS);
        invalidPointOfServiceTypes.add(PointOfServiceTypeEnum.STORE);
        posTypeInterceptor.setInvalidPointOfServiceTypes(invalidPointOfServiceTypes);
    }

    @Test(expected = InterceptorException.class)
    public void testInvlidPOSType() throws InterceptorException {
        BDDMockito.given(pointOfService.getType()).willReturn(PointOfServiceTypeEnum.POS);
        posTypeInterceptor.onValidate(pointOfService, interceptorContext);
    }

    @Test
    public void testNullValueList() throws InterceptorException {
        boolean testFailed = false;
        try {
            posTypeInterceptor.setInvalidPointOfServiceTypes(null);
            posTypeInterceptor.onValidate(pointOfService, interceptorContext);
        }
        catch (final NullPointerException e) {
            testFailed = true;
            Assert.assertTrue("Null list is not handled", false);
        }

        if (!testFailed) {
            Assert.assertTrue("Null list is handled", true);
        }
    }

    @Test
    public void testWithValidPOSTypeStatusTarget() throws InterceptorException {
        boolean testFailed = false;

        try {
            BDDMockito.given(pointOfService.getType()).willReturn(PointOfServiceTypeEnum.TARGET);
            posTypeInterceptor.onValidate(pointOfService, interceptorContext);
        }
        catch (final InterceptorException e) {
            testFailed = true;
            Assert.assertTrue("A valid PointOfServiceTypeEnum caused an InterceptorException",
                    false);
        }

        if (!testFailed) {
            Assert.assertTrue("A valid PointOfServiceTypeEnum was passed in", true);
        }
    }

    @Test
    public void testWithValidPOSTypeStatusWarehouse() throws InterceptorException {
        boolean testFailed = false;

        try {
            BDDMockito.given(pointOfService.getType()).willReturn(PointOfServiceTypeEnum.WAREHOUSE);
            posTypeInterceptor.onValidate(pointOfService, interceptorContext);
        }
        catch (final InterceptorException e) {
            testFailed = true;
            Assert.assertTrue("A valid PointOfServiceTypeEnum caused an InterceptorException",
                    false);
        }

        if (!testFailed) {
            Assert.assertTrue("A valid PointOfServiceTypeEnum was passed in", true);
        }
    }
}

/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.model.TargetAddressModel;


/**
 * Test for {@link TargetPhoneNumberValidateInterceptor}
 * 
 */
@UnitTest
public class TargetPhoneNumberValidateInterceptorTest {

    private TargetPhoneNumberValidateInterceptor interceptor;

    @Before
    public void setUp() {
        interceptor = new TargetPhoneNumberValidateInterceptor();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidationRuleShortNumber() throws Exception {
        final TargetAddressModel model = new TargetAddressModel();
        model.setPhone2(null);
        model.setPhone1("1");
        interceptor.onValidate(model, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidationRulesLongNumber() throws Exception {
        final TargetAddressModel model = new TargetAddressModel();
        model.setPhone2(null);
        model.setPhone1("1234567890123456789012345678");
        interceptor.onValidate(model, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidationRulesLongNumberWithPlusSign() throws Exception {
        final TargetAddressModel model = new TargetAddressModel();
        model.setPhone2(null);
        model.setPhone1("+1234567890123456");
        interceptor.onValidate(model, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidationRulesShortNumber() throws Exception {
        final TargetAddressModel model = new TargetAddressModel();
        model.setPhone2(null);
        model.setPhone1("1234567");
        interceptor.onValidate(model, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNonDigit() throws Exception {
        final TargetAddressModel model = new TargetAddressModel();
        model.setPhone2(null);
        model.setPhone1("test");
        interceptor.onValidate(model, null);
    }

    @Test
    public void testNormalFlowWithPlusSign() throws Exception {
        final TargetAddressModel model = new TargetAddressModel();
        model.setPhone2(null);
        model.setPhone1("+012345678912345");
        interceptor.onValidate(model, null);
    }

    @Test
    public void testNormalFlowWithoutPlusSign() throws Exception {
        final TargetAddressModel model = new TargetAddressModel();
        model.setPhone2(null);
        model.setPhone1("0123456789");
        interceptor.onValidate(model, null);
    }
}

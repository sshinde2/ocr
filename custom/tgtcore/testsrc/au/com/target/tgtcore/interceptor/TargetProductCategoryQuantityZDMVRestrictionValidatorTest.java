/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.TargetProductCategoryQuantityZDMVRestrictionModel;


/**
 * @author Rahul
 * 
 */
@UnitTest
public class TargetProductCategoryQuantityZDMVRestrictionValidatorTest {

    private TargetProductCategoryQuantityZDMVRestrictionValidator validator;

    private List<ProductModel> products;

    private List<CategoryModel> categories;

    @Mock
    private TargetProductCategoryQuantityZDMVRestrictionModel restrictionModel;

    @Mock
    private ProductModel product;

    @Mock
    private CategoryModel category;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        validator = new TargetProductCategoryQuantityZDMVRestrictionValidator();
        products = new ArrayList<>();
        categories = new ArrayList<>();
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidateQuantityWithZero() throws InterceptorException {
        products.add(product);
        given(restrictionModel.getIncludedProducts()).willReturn(products);
        given(restrictionModel.getIncludedCategories()).willReturn(null);
        given(restrictionModel.getQuantity()).willReturn(new Integer(0));
        validator.onValidate(restrictionModel, null);
    }

    @Test(expected = InterceptorException.class)
    public void testEmptyProductsAndCategories() throws InterceptorException {
        given(restrictionModel.getIncludedProducts()).willReturn(null);
        given(restrictionModel.getIncludedCategories()).willReturn(null);

        validator.onValidate(restrictionModel, null);
    }

    @Test
    public void testSomeProductsAndEmptyCategories() throws InterceptorException {
        products.add(product);

        given(restrictionModel.getIncludedProducts()).willReturn(products);
        given(restrictionModel.getIncludedCategories()).willReturn(null);
        given(restrictionModel.getQuantity()).willReturn(new Integer(2));
        validator.onValidate(restrictionModel, null);
    }

    @Test
    public void testEmptyProductsAndSomeCategories() throws InterceptorException {
        categories.add(category);

        given(restrictionModel.getIncludedProducts()).willReturn(null);
        given(restrictionModel.getIncludedCategories()).willReturn(categories);
        given(restrictionModel.getQuantity()).willReturn(new Integer(2));
        validator.onValidate(restrictionModel, null);
    }

    @Test
    public void testProductsAndCategories() throws InterceptorException {
        products.add(product);
        categories.add(category);

        given(restrictionModel.getIncludedProducts()).willReturn(products);
        given(restrictionModel.getIncludedCategories()).willReturn(categories);
        given(restrictionModel.getQuantity()).willReturn(new Integer(2));
        validator.onValidate(restrictionModel, null);
    }
}

/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetProductModel;


/**
 * Unit test for {@link TargetProductEanPrepareInterceptor}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductEanPrepareInterceptorTest {

    private final TargetProductEanPrepareInterceptor interceptor = new TargetProductEanPrepareInterceptor();

    @Mock
    private InterceptorContext interceptorContext;

    @Test
    public void testPrepareEanForNullEan() throws InterceptorException {
        final TargetProductModel model = new TargetProductModel();
        model.setEan(null);
        interceptor.onPrepare(model, interceptorContext);
        Assert.assertEquals("", model.getEan());
    }

    @Test
    public void testPrepareEanForEmptyEan() throws InterceptorException {
        final TargetProductModel model = new TargetProductModel();
        model.setEan("");
        interceptor.onPrepare(model, interceptorContext);
        Assert.assertEquals("", model.getEan());
    }

    @Test
    public void testTargetProductEanPrepareInterceptor() throws InterceptorException {
        final TargetProductModel model = new TargetProductModel();
        model.setEan(" 1234 ");
        interceptor.onPrepare(model, interceptorContext);
        Assert.assertEquals("00001234", model.getEan());

        model.setEan(" 123 456  ");
        interceptor.onPrepare(model, interceptorContext);
        Assert.assertEquals("0123 456", model.getEan());

        model.setEan("  123 456 789 ");
        interceptor.onPrepare(model, interceptorContext);
        Assert.assertEquals("00123 456 789", model.getEan());

        model.setEan("      123456     ");
        interceptor.onPrepare(model, interceptorContext);
        Assert.assertEquals("00123456", model.getEan());

        model.setEan("      12345678     ");
        interceptor.onPrepare(model, interceptorContext);
        Assert.assertEquals("12345678", model.getEan());

        model.setEan("      1234567890     ");
        interceptor.onPrepare(model, interceptorContext);
        Assert.assertEquals("0001234567890", model.getEan());

        model.setEan("1234567890123");
        interceptor.onPrepare(model, interceptorContext);
        Assert.assertEquals("1234567890123", model.getEan());

        model.setEan("      123456789012345     ");
        interceptor.onPrepare(model, interceptorContext);
        Assert.assertEquals("123456789012345", model.getEan());

        model.setEan("123456789012345");
        interceptor.onPrepare(model, interceptorContext);
        Assert.assertEquals("123456789012345", model.getEan());

        model.setEan("012345    435");
        interceptor.onPrepare(model, interceptorContext);
        Assert.assertEquals("012345    435", model.getEan());

    }
}

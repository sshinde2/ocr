/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.model.TargetProductModel;


/**
 * @author Vivek
 *
 */
@UnitTest
public class TargetProductEanValidateInterceptorTest {

    private TargetProductEanValidateInterceptor interceptor;

    @Before
    public void setUp() {
        interceptor = new TargetProductEanValidateInterceptor();
    }

    @Test(expected = InterceptorException.class)
    public void testValidationNonNumeric() throws Exception {
        final TargetProductModel model = new TargetProductModel();
        model.setEan("test");
        interceptor.onValidate(model, null);
    }

    @Test(expected = InterceptorException.class)
    public void testValidationNumericWithWhiteSpace() throws Exception {
        final TargetProductModel model = new TargetProductModel();
        model.setEan("9300675 001427");
        interceptor.onValidate(model, null);
    }

    @Test(expected = InterceptorException.class)
    public void testValidationLengthGreaterThan13() throws Exception {
        final TargetProductModel model = new TargetProductModel();
        model.setEan("93006750001427");
        interceptor.onValidate(model, null);
    }

    @Test
    public void testNormalEanWithCorrectChecksum() throws Exception {
        final TargetProductModel model = new TargetProductModel();
        model.setEan("9300675001427");
        interceptor.onValidate(model, null);
    }

    @Test(expected = InterceptorException.class)
    public void testNormalEanWithWrongChecksum() throws Exception {
        final TargetProductModel model = new TargetProductModel();
        model.setEan("9300675001428");
        interceptor.onValidate(model, null);
    }

    @Test
    public void testShortEan() throws Exception {
        final TargetProductModel model = new TargetProductModel();
        model.setEan("55");
        interceptor.onValidate(model, null);
    }

    @Test
    public void testEmptyEan() throws Exception {
        final TargetProductModel model = new TargetProductModel();
        model.setEan(StringUtils.EMPTY);
        interceptor.onValidate(model, null);
    }

}

/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * Unit Test for {@link TargetProductValidateInterceptor}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductValidateInterceptorTest {

    private static final String MANDATORY_HOME_DELIVERY_CODE = "mhd";
    private static final String CNC_CODE = "click-and-collect";

    @Mock
    private InterceptorContext interceptorContext;

    @Mock
    private TargetProductModel targetProduct;

    @Mock
    private ProductTypeModel type;

    @Mock
    private DeliveryModeModel cncMode;


    @InjectMocks
    private final TargetProductValidateInterceptor targetProductValidateInterceptor = new TargetProductValidateInterceptor();

    @Before
    public void setup() {
        targetProductValidateInterceptor.setClickAndCollectCode(CNC_CODE);
        targetProductValidateInterceptor.setMandatoryHomeDeliveryCode(MANDATORY_HOME_DELIVERY_CODE);

        // product type
        BDDMockito.given(targetProduct.getProductType()).willReturn(type);

        // Has CNC delivery mode
        BDDMockito.given(targetProduct.getDeliveryModes()).willReturn(Collections.singleton(cncMode));
        BDDMockito.given(cncMode.getCode()).willReturn(CNC_CODE);
    }

    @Test
    public void testInterceptorOk() throws InterceptorException {

        BDDMockito.given(type.getCode()).willReturn("bulky");
        targetProductValidateInterceptor.onValidate(targetProduct, interceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testInterceptorNotOk() throws InterceptorException {

        BDDMockito.given(type.getCode()).willReturn(MANDATORY_HOME_DELIVERY_CODE);
        targetProductValidateInterceptor.onValidate(targetProduct, interceptorContext);
    }

    @Test
    public void testInterceptorNotBase() throws InterceptorException {
        final AbstractTargetVariantProductModel variant = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(variant.getProductType()).willReturn(type);
        BDDMockito.given(variant.getDeliveryModes()).willReturn(Collections.singleton(cncMode));

        targetProductValidateInterceptor.onValidate(variant, interceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testTargetProductValidateInterceptorWithBulkyBoardProductTooManyVariants() throws InterceptorException {
        final TargetColourVariantProductModel mockTargetColourVariantProduct1 = mock(TargetColourVariantProductModel.class);
        final TargetColourVariantProductModel mockTargetColourVariantProduct2 = mock(TargetColourVariantProductModel.class);

        final List<VariantProductModel> targetColourVariantList = new ArrayList<>();
        targetColourVariantList.add(mockTargetColourVariantProduct1);
        targetColourVariantList.add(mockTargetColourVariantProduct2);

        given(targetProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);
        given(targetProduct.getVariants()).willReturn(targetColourVariantList);

        targetProductValidateInterceptor.onValidate(targetProduct, interceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testTargetProductValidateInterceptorWithBulkyBoardProductWithSizeVariants() throws InterceptorException {
        final TargetSizeVariantProductModel mockTargetSizeVariantProduct = mock(TargetSizeVariantProductModel.class);

        final List<VariantProductModel> targetSizeVariantList = new ArrayList<>();
        targetSizeVariantList.add(mockTargetSizeVariantProduct);

        final TargetColourVariantProductModel mockTargetColourVariantProduct = mock(TargetColourVariantProductModel.class);
        given(mockTargetColourVariantProduct.getVariants()).willReturn(targetSizeVariantList);

        final List<VariantProductModel> targetColourVariantList = new ArrayList<>();
        targetColourVariantList.add(mockTargetColourVariantProduct);

        given(targetProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);
        given(targetProduct.getVariants()).willReturn(targetColourVariantList);

        targetProductValidateInterceptor.onValidate(targetProduct, interceptorContext);
    }
}

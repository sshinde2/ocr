/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.TargetSalesChannelZDMVRestrictionModel;


/**
 * @author Nandini
 * 
 */
@UnitTest
public class TargetSalesChannelZDMVRestrictionValidateInterceptorTest {

    private TargetSalesChannelZDMVRestrictionValidateInterceptor salesValidateInterceptor;

    @Mock
    private TargetSalesChannelZDMVRestrictionModel salesChannelModel;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        salesValidateInterceptor = new TargetSalesChannelZDMVRestrictionValidateInterceptor();
    }

    @Test(expected = InterceptorException.class)
    public void testValidateNullSalesChannel() throws InterceptorException {
        given(salesChannelModel.getSalesApplication()).willReturn(null);
        salesValidateInterceptor.onValidate(salesChannelModel, null);
        Assert.fail();
    }

    @Test
    public void testValidateSalesChannel() throws InterceptorException {
        final SalesApplication salesApplication = Mockito.mock(SalesApplication.class);
        final Collection<SalesApplication> salesApplications = new ArrayList<>();
        salesApplications.add(salesApplication);
        given(salesChannelModel.getSalesApplication()).willReturn(salesApplications);
        salesValidateInterceptor.onValidate(salesChannelModel, null);
    }

}

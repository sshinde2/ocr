package au.com.target.tgtcore.interceptor;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.variants.model.VariantProductModel;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


@UnitTest
public class TargetSizeVariantProductValidateInterceptorTest {

    @Mock
    private InterceptorContext mockInterceptorContext;

    @InjectMocks
    private final TargetSizeVariantProductValidateInterceptor targetSizeVariantProductValidateInterceptor = new TargetSizeVariantProductValidateInterceptor();

    @Test
    public void testOnValidateWithModel() throws InterceptorException {
        // This test just ensures that no errors occur when a null model is passed in
        targetSizeVariantProductValidateInterceptor.onValidate(null, mockInterceptorContext);
    }

    @Test
    public void testOnValidateWithIncorrectModelType() throws InterceptorException {
        // This test just ensures that no errors occur when the wrong model type is passed in
        final VariantProductModel mockVariant = mock(VariantProductModel.class);

        targetSizeVariantProductValidateInterceptor.onValidate(mockVariant, mockInterceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidateWithBulkyBoardProduct() throws InterceptorException {
        final TargetSizeVariantProductModel mockTargetSizeVariant = mock(TargetSizeVariantProductModel.class);
        given(mockTargetSizeVariant.getBulkyBoardProduct()).willReturn(Boolean.TRUE);

        targetSizeVariantProductValidateInterceptor.onValidate(mockTargetSizeVariant, mockInterceptorContext);
    }

    @Test
    public void testOnValidateWithNonBulkyBoardProduct() throws InterceptorException {
        final TargetSizeVariantProductModel mockTargetSizeVariant = mock(TargetSizeVariantProductModel.class);
        given(mockTargetSizeVariant.getBulkyBoardProduct()).willReturn(Boolean.FALSE);

        targetSizeVariantProductValidateInterceptor.onValidate(mockTargetSizeVariant, mockInterceptorContext);
    }
}

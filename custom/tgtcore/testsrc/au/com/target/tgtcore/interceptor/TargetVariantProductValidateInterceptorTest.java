/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;


@UnitTest
public class TargetVariantProductValidateInterceptorTest {
    private TargetVariantProductValidateInterceptor tvpInterceptor;

    @Mock
    private InterceptorContext context;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        tvpInterceptor = new TargetVariantProductValidateInterceptor();
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidateNegativeDepartmentValue() throws InterceptorException {
        final AbstractTargetVariantProductModel variant = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(variant.getDepartment()).willReturn(Integer.valueOf("-1"));
        tvpInterceptor.onValidate(variant, context);
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidateTargetVariantHasDeliveryModes() throws InterceptorException {
        final AbstractTargetVariantProductModel variant = Mockito.mock(AbstractTargetVariantProductModel.class);
        final Set<DeliveryModeModel> prodDeliveryModes = new HashSet<>();
        final TargetZoneDeliveryModeModel deliveryModeModel = Mockito.mock(TargetZoneDeliveryModeModel.class);
        prodDeliveryModes.add(deliveryModeModel);
        BDDMockito.given(variant.getDeliveryModes()).willReturn(prodDeliveryModes);
        tvpInterceptor.onValidate(variant, context);
    }
}

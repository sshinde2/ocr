/**
 * 
 */
package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.order.DeliveryModeService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.Collection;
import java.util.HashSet;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test class for TargetZoneDeliveryModeValidateInterceptor
 * 
 * @author Pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetZoneDeliveryModeValidateInterceptorTest {

    @InjectMocks
    private final TargetZoneDeliveryModeValidateInterceptor targetZoneDeliveryModeValidateInterceptor = new TargetZoneDeliveryModeValidateInterceptor();

    @Mock
    private InterceptorContext ctx;

    @Mock
    private DeliveryModeService deliveryModeService;

    @Mock
    private TargetZoneDeliveryModeModel cncDeliveryMode;

    @Mock
    private TargetZoneDeliveryModeModel homeDeliveryMode;

    @Mock
    private TargetZoneDeliveryModeModel expressDeliveryMode;

    private Collection<DeliveryModeModel> deliveryModes;

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Before
    public void setUp() {
        deliveryModes = new HashSet<>();
        deliveryModes.add(expressDeliveryMode);
        deliveryModes.add(homeDeliveryMode);
        deliveryModes.add(cncDeliveryMode);
        Mockito.when(deliveryModeService.getAllDeliveryModes()).thenReturn(deliveryModes);
        Mockito.when(expressDeliveryMode.getDeliveryPromotionRank()).thenReturn(Integer.valueOf(1));
        Mockito.when(homeDeliveryMode.getDeliveryPromotionRank()).thenReturn(Integer.valueOf(2));
        Mockito.when(cncDeliveryMode.getDeliveryPromotionRank()).thenReturn(Integer.valueOf(3));
        Mockito.when(expressDeliveryMode.getCode()).thenReturn("express-delivery");
        Mockito.when(homeDeliveryMode.getCode()).thenReturn("home-delivery");
        Mockito.when(cncDeliveryMode.getCode()).thenReturn("click-n-collect");
    }

    @Test
    public void testOnValidateWhenPromotionRankToBeSavedIsUnique() {
        try {
            targetZoneDeliveryModeValidateInterceptor.onValidate(expressDeliveryMode, ctx);
            Assert.assertTrue("InterceptorException is thrown", true);
        }
        catch (final InterceptorException ie) {
            Assert.assertFalse("InterceptorException is not thrown", true);
        }
    }

    @Test
    public void testOnValidateWhenPromotionRankToBeSavedIsNotUnique() throws InterceptorException {
        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage("Delivery promotion rank is not unique");
        Mockito.when(expressDeliveryMode.getDeliveryPromotionRank()).thenReturn(Integer.valueOf(3));
        targetZoneDeliveryModeValidateInterceptor.onValidate(expressDeliveryMode, ctx);
    }

    @Test
    public void testOnValidateWhenPromotionRankToBeSavedIsZero() throws InterceptorException {
        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage("Delivery promotion rank cannot be less than or equal to 0");
        Mockito.when(expressDeliveryMode.getDeliveryPromotionRank()).thenReturn(Integer.valueOf(0));
        targetZoneDeliveryModeValidateInterceptor.onValidate(expressDeliveryMode, ctx);
    }

    @Test
    public void testOnValidateWhenPromotionRankToBeSavedIsNegative() throws InterceptorException {
        expectedException.expect(InterceptorException.class);
        expectedException.expectMessage("Delivery promotion rank cannot be less than or equal to 0");
        Mockito.when(expressDeliveryMode.getDeliveryPromotionRank()).thenReturn(Integer.valueOf(-1));
        targetZoneDeliveryModeValidateInterceptor.onValidate(expressDeliveryMode, ctx);
    }

    @Test
    public void testOnValidateWhenDeliveryModeIsNotATargetZoneDeliveryMode() {
        try {
            targetZoneDeliveryModeValidateInterceptor.onValidate(Mockito.mock(DeliveryModeModel.class), ctx);
            Assert.assertTrue("InterceptorException is thrown", true);
        }
        catch (final InterceptorException ie) {
            Assert.assertFalse("InterceptorException is not thrown", true);
        }
    }

    @Test
    public void testOnValidateWhenNoTargetZoneDeliveryModeExist() {
        Mockito.when(deliveryModeService.getAllDeliveryModes()).thenReturn(null);
        try {
            targetZoneDeliveryModeValidateInterceptor.onValidate(expressDeliveryMode, ctx);
            Assert.assertTrue("InterceptorException is thrown", true);
        }
        catch (final InterceptorException ie) {
            Assert.assertFalse("InterceptorException is not thrown", true);
        }
    }

    @Test
    public void testOnValidateWhenOneTargetZoneDeliveryModeExist() {
        deliveryModes = new HashSet<>();
        deliveryModes.add(homeDeliveryMode);
        try {
            targetZoneDeliveryModeValidateInterceptor.onValidate(expressDeliveryMode, ctx);
            Assert.assertTrue("InterceptorException is thrown", true);
        }
        catch (final InterceptorException ie) {
            Assert.assertFalse("InterceptorException is not thrown", true);
        }
    }

}

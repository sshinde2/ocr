package au.com.target.tgtcore.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.voucher.model.VoucherModel;

import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.interceptor.VoucherModelValidateInterceptor.ErrorMessages;


@UnitTest
public class VoucherModelValidateInterceptorTest {

    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";

    private final VoucherModelValidateInterceptor voucherModelValidateInterceptor = new VoucherModelValidateInterceptor();

    @Test
    public void testOnValidateWithNullModel() throws InterceptorException {
        voucherModelValidateInterceptor.onValidate(null, null);
    }

    @Test
    public void testOnValidateWithWrongModelType() throws InterceptorException {
        final ItemModel mockModel = Mockito.mock(ItemModel.class);

        voucherModelValidateInterceptor.onValidate(mockModel, null);

        Mockito.verifyZeroInteractions(mockModel);
    }

    @Test
    public void testOnValidateWithCodeWithLowercaseCharacters() {
        final VoucherModel mockVoucher = Mockito.mock(VoucherModel.class);
        BDDMockito.given(mockVoucher.getCode()).willReturn("AbC");

        try {
            voucherModelValidateInterceptor.onValidate(mockVoucher, null);
            Assert.fail("Expected InterceptorException to be thrown");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.PROMOTION_PATTERN_INVALID);
        }
    }

    @Test
    public void testOnValidateWithCodeWithSymbols() {
        final VoucherModel mockVoucher = Mockito.mock(VoucherModel.class);
        BDDMockito.given(mockVoucher.getCode()).willReturn("AB?");

        try {
            voucherModelValidateInterceptor.onValidate(mockVoucher, null);
            Assert.fail("Expected InterceptorException to be thrown");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.PROMOTION_PATTERN_INVALID);
        }
    }

    @Test
    public void testOnValidateWithCodeWithLettersOnly() throws InterceptorException {
        final VoucherModel mockVoucher = Mockito.mock(VoucherModel.class);
        BDDMockito.given(mockVoucher.getCode()).willReturn("ABC");

        voucherModelValidateInterceptor.onValidate(mockVoucher, null);
    }

    @Test
    public void testOnValidateWithCodeWithNumbersOnly() throws InterceptorException {
        final VoucherModel mockVoucher = Mockito.mock(VoucherModel.class);
        BDDMockito.given(mockVoucher.getCode()).willReturn("123");

        voucherModelValidateInterceptor.onValidate(mockVoucher, null);
    }

    @Test
    public void testOnValidateWithCodeWithLettersAndNumbers() throws InterceptorException {
        final VoucherModel mockVoucher = Mockito.mock(VoucherModel.class);
        BDDMockito.given(mockVoucher.getCode()).willReturn("AB1");

        voucherModelValidateInterceptor.onValidate(mockVoucher, null);
    }
}

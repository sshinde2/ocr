/**
 * 
 */
package au.com.target.tgtcore.jalo;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.jalo.order.Cart;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * @author siddharam
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("deprecation")
public class TargetMaxTotalDeadWeightZDMVRestrictionTest {

    @Mock
    private TargetMaxTotalDeadWeightZDMVRestriction targetMaxTotalDeadWeightZDMVRestriction;

    @Mock
    private Product product;

    @Mock
    private Cart cart;

    @Mock
    private TargetColourVariantProductModel productModel;

    @Mock
    private CartModel cartModel;

    private final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
    @Mock
    private TargetProductDimensionsModel targetProductDimensions;

    @Mock
    private ModelService modelService;

    @Before
    public void setUp() {
        given(Boolean.valueOf(targetMaxTotalDeadWeightZDMVRestriction.evaluate(product))).willCallRealMethod();
        given(Boolean.valueOf(targetMaxTotalDeadWeightZDMVRestriction.evaluate(cartModel))).willCallRealMethod();
        given(targetMaxTotalDeadWeightZDMVRestriction.getMaxTotalDeadWeight()).willReturn(Double.valueOf(5));
        given(targetMaxTotalDeadWeightZDMVRestriction.getModelService()).willReturn(modelService);
        given(modelService.get(any(Product.class))).willReturn(productModel);
        given(productModel.getProductPackageDimensions()).willReturn(targetProductDimensions);
    }

    @Test
    public void testEvaluateProductWeightWeightWhenHasVariants() {
        final Collection<VariantProductModel> variants = new HashSet<>();
        final TargetSizeVariantProductModel firstVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetSizeVariantProductModel secondVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetProductDimensionsModel firstDimension = mock(TargetProductDimensionsModel.class);
        final TargetProductDimensionsModel secondDimension = mock(TargetProductDimensionsModel.class);
        given(firstVariantProductModel.getProductPackageDimensions()).willReturn(firstDimension);
        given(firstDimension.getWeight()).willReturn(Double.valueOf(2));

        given(secondVariantProductModel.getProductPackageDimensions()).willReturn(secondDimension);
        given(secondDimension.getWeight()).willReturn(Double.valueOf(3));

        variants.add(firstVariantProductModel);
        variants.add(secondVariantProductModel);
        given(productModel.getVariants()).willReturn(variants);
        assertThat(targetMaxTotalDeadWeightZDMVRestriction.evaluate(product)).isTrue();
    }

    @Test
    public void testEvaluateProductWeightWeightWhenHasOneVariantWeightIsExceeding() {
        final Collection<VariantProductModel> variants = new HashSet<>();
        final TargetSizeVariantProductModel firstVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetSizeVariantProductModel secondVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetProductDimensionsModel firstDimension = mock(TargetProductDimensionsModel.class);
        final TargetProductDimensionsModel secondDimension = mock(TargetProductDimensionsModel.class);
        given(firstVariantProductModel.getProductPackageDimensions()).willReturn(firstDimension);
        given(firstDimension.getWeight()).willReturn(Double.valueOf(6));

        given(secondVariantProductModel.getProductPackageDimensions()).willReturn(secondDimension);
        given(secondDimension.getWeight()).willReturn(Double.valueOf(3));

        variants.add(firstVariantProductModel);
        variants.add(secondVariantProductModel);
        given(productModel.getVariants()).willReturn(variants);
        assertThat(targetMaxTotalDeadWeightZDMVRestriction.evaluate(product)).isFalse();
    }

    @Test
    public void testEvaluateProductWeightWeightWhenHasOneVariantHasNoDimension() {
        final Collection<VariantProductModel> variants = new HashSet<>();
        final TargetSizeVariantProductModel firstVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetSizeVariantProductModel secondVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetProductDimensionsModel firstDimension = mock(TargetProductDimensionsModel.class);
        given(firstVariantProductModel.getProductPackageDimensions()).willReturn(firstDimension);
        given(firstDimension.getWeight()).willReturn(Double.valueOf(2));

        given(secondVariantProductModel.getProductPackageDimensions()).willReturn(null);

        variants.add(firstVariantProductModel);
        variants.add(secondVariantProductModel);
        given(productModel.getVariants()).willReturn(variants);
        assertThat(targetMaxTotalDeadWeightZDMVRestriction.evaluate(product)).isFalse();
    }

    @Test
    public void testEvaluateProductWeightWeightWhenHasOneVariantWeightIsNull() {
        final Collection<VariantProductModel> variants = new HashSet<>();
        final TargetSizeVariantProductModel firstVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetSizeVariantProductModel secondVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetProductDimensionsModel firstDimension = mock(TargetProductDimensionsModel.class);
        final TargetProductDimensionsModel secondDimension = mock(TargetProductDimensionsModel.class);
        given(firstVariantProductModel.getProductPackageDimensions()).willReturn(firstDimension);
        given(firstDimension.getWeight()).willReturn(Double.valueOf(2));

        given(secondVariantProductModel.getProductPackageDimensions()).willReturn(secondDimension);
        given(secondDimension.getWeight()).willReturn(null);

        variants.add(firstVariantProductModel);
        variants.add(secondVariantProductModel);
        given(productModel.getVariants()).willReturn(variants);
        assertThat(targetMaxTotalDeadWeightZDMVRestriction.evaluate(product)).isFalse();
    }

    @Test
    public void testEvaluateProductWeightWeightWhenHasOneVariantWeightIsZero() {
        final Collection<VariantProductModel> variants = new HashSet<>();
        final TargetSizeVariantProductModel firstVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetSizeVariantProductModel secondVariantProductModel = mock(TargetSizeVariantProductModel.class);
        final TargetProductDimensionsModel firstDimension = mock(TargetProductDimensionsModel.class);
        final TargetProductDimensionsModel secondDimension = mock(TargetProductDimensionsModel.class);
        given(firstVariantProductModel.getProductPackageDimensions()).willReturn(firstDimension);
        given(firstDimension.getWeight()).willReturn(Double.valueOf(3));

        given(secondVariantProductModel.getProductPackageDimensions()).willReturn(secondDimension);
        given(secondDimension.getWeight()).willReturn(Double.valueOf(0));

        variants.add(firstVariantProductModel);
        variants.add(secondVariantProductModel);
        given(productModel.getVariants()).willReturn(variants);
        assertThat(targetMaxTotalDeadWeightZDMVRestriction.evaluate(product)).isTrue();
    }

    @Test
    public void testEvaluateProductWeightWeight() {
        given(targetProductDimensions.getWeight()).willReturn(Double.valueOf(4));
        assertThat(targetMaxTotalDeadWeightZDMVRestriction.evaluate(product)).isTrue();
    }

    @Test
    public void testEvaluateProductWeightWeightWhenExceeding() {
        given(targetProductDimensions.getWeight()).willReturn(Double.valueOf(6));
        assertThat(targetMaxTotalDeadWeightZDMVRestriction.evaluate(product)).isFalse();
    }

    @Test
    public void testEvaluateProductWeightWeightWhenNoDimension() {
        given(productModel.getProductPackageDimensions()).willReturn(null);
        assertThat(targetMaxTotalDeadWeightZDMVRestriction.evaluate(product)).isFalse();
    }

    @Test
    public void testEvaluateProductWeightWeightWhenNullWeight() {
        given(targetProductDimensions.getWeight()).willReturn(null);
        assertThat(targetMaxTotalDeadWeightZDMVRestriction.evaluate(product)).isFalse();
    }

    @Test
    public void testEvaluateProductWeightWeightWhenNegativeWeight() {
        given(targetProductDimensions.getWeight()).willReturn(Double.valueOf(-6));
        assertThat(targetMaxTotalDeadWeightZDMVRestriction.evaluate(product)).isFalse();
    }


    @Test
    public void testEvaluateCartWeight() {
        final TargetProductDimensionsModel secondDimension = setUpEntries();
        given(secondDimension.getWeight()).willReturn(Double.valueOf(1));
        assertThat(targetMaxTotalDeadWeightZDMVRestriction.evaluate(cartModel)).isTrue();
    }

    @Test
    public void testEvaluateMixedCartWeight() {
        final TargetProductDimensionsModel secondDimension = setUpEntries();
        given(secondDimension.getWeight()).willReturn(Double.valueOf(1));
        assertThat(targetMaxTotalDeadWeightZDMVRestriction.evaluate(cartModel)).isTrue();
    }

    @Test
    public void testEvaluateCartWeightExceeding() {
        final TargetProductDimensionsModel secondDimension = setUpEntries();
        given(secondDimension.getWeight()).willReturn(Double.valueOf(8));
        assertThat(targetMaxTotalDeadWeightZDMVRestriction.evaluate(cartModel)).isFalse();
    }

    private TargetProductDimensionsModel setUpEntries() {
        final AbstractOrderEntryModel firstEntry = mock(OrderEntryModel.class);
        final AbstractOrderEntryModel secondEntry = mock(OrderEntryModel.class);
        final TargetSizeVariantProductModel firstProduct = mock(TargetSizeVariantProductModel.class);
        final TargetProductDimensionsModel firstDimension = mock(TargetProductDimensionsModel.class);
        final TargetSizeVariantProductModel secondProduct = mock(TargetSizeVariantProductModel.class);
        final TargetProductDimensionsModel secondDimension = mock(TargetProductDimensionsModel.class);
        final TargetProductModel baseProduct = mock(TargetProductModel.class);
        final ProductTypeModel productType = mock(ProductTypeModel.class);
        given(productType.getCode()).willReturn(ProductType.DIGITAL);
        given(baseProduct.getProductType()).willReturn(productType);
        given(firstEntry.getProduct()).willReturn(baseProduct);
        orderEntries.add(firstEntry);
        orderEntries.add(secondEntry);
        given(cartModel.getEntries()).willReturn(orderEntries);
        given(firstEntry.getProduct())
                .willReturn(firstProduct);
        given(firstEntry.getQuantity())
                .willReturn(Long.valueOf(2));
        given(firstProduct.getProductPackageDimensions()).willReturn(firstDimension);
        given(firstDimension.getWeight()).willReturn(Double.valueOf(1.5));

        given(secondEntry.getProduct())
                .willReturn(secondProduct);
        given(secondEntry.getQuantity())
                .willReturn(Long.valueOf(1));
        given(secondProduct.getProductPackageDimensions()).willReturn(secondDimension);
        return secondDimension;
    }

}
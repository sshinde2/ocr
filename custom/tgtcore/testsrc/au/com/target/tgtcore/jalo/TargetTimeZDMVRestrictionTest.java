/**
 * 
 */
package au.com.target.tgtcore.jalo;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * @author Rahul
 * 
 */
@UnitTest
public class TargetTimeZDMVRestrictionTest {

    @Mock
    private TargetTimeZDMVRestriction targetTimeZDMVRestriction;

    @Mock
    private CartModel cart;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        given(cart.getDate()).willReturn(new Date());
        given(Boolean.valueOf(targetTimeZDMVRestriction.evaluate(cart))).willCallRealMethod();
    }

    @Test
    public void testEvaluateEmptyDates() {
        given(targetTimeZDMVRestriction.getActiveFrom()).willReturn(null);
        given(targetTimeZDMVRestriction.getActiveUntil()).willReturn(null);

        assertThat(targetTimeZDMVRestriction.evaluate(cart)).isTrue();
    }

    @Test
    public void testEvaluateInvalidDates() {
        given(targetTimeZDMVRestriction.getActiveFrom()).willReturn(DateUtils.addDays(new Date(), -1));
        given(targetTimeZDMVRestriction.getActiveUntil()).willReturn(DateUtils.addHours(new Date(), -1));

        assertThat(targetTimeZDMVRestriction.evaluate(cart)).isFalse();
    }

    @Test
    public void testEvaluateValidDates() {
        given(targetTimeZDMVRestriction.getActiveFrom()).willReturn(DateUtils.addDays(new Date(), -1));
        given(targetTimeZDMVRestriction.getActiveUntil()).willReturn(DateUtils.addHours(new Date(), 1));

        assertThat(targetTimeZDMVRestriction.evaluate(cart)).isTrue();
    }
}

/**
 * 
 */
package au.com.target.tgtcore.jobs;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.internal.model.MaintenanceCleanupJobModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import au.com.target.tgtcore.model.TargetDynamicMaintenanceCleanupJobModel;
import bsh.EvalError;
import bsh.Interpreter;
import bsh.NameSpace;


/**
 * @author siddharam
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetDynamicMaintenanceJobPerformableTest {

    @InjectMocks
    private final TargetDynamicMaintenanceJobPerformable targetDynamicMaintenanceJobPerformable = new MyTargetDynamicMaintenanceJobPerformable();

    private class MyTargetDynamicMaintenanceJobPerformable extends TargetDynamicMaintenanceJobPerformable {

        @Override
        protected Interpreter getInterpreter() {
            return interpreter;
        }

        @Override
        protected ApplicationContext getApplicationContext() {
            return applicationContext;
        }
    }

    @Mock
    private ApplicationContext applicationContext;

    @Mock
    private Interpreter interpreter;

    @Mock
    private NameSpace nameSpace;

    @Mock
    private CronJobModel cronJobModel;

    @Mock
    private TargetDynamicMaintenanceCleanupJobModel dynamicMaintenanceCleanupJobModel;

    @Mock
    private MaintenanceCleanupJobModel maintenanceCleanupJobModel;

    @Mock
    private FlexibleSearchQuery flexibleSearchQuery;

    @Mock
    private ModelService modelService;

    private List<ItemModel> elements;

    private ItemModel item;
    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    //CHECKSTYLE:ON
    @Test
    public void testGetFetchQueryWithNullScript() {
        exception.expect(IllegalArgumentException.class);
        exception
                .expectMessage("Long search code attribute value is missing for TargetDynamicMaintenanceCleanupJobModel. This is mandatory");
        Mockito.when(cronJobModel.getJob()).thenReturn(dynamicMaintenanceCleanupJobModel);
        Mockito.when(dynamicMaintenanceCleanupJobModel.getLongSearchScript()).thenReturn(null);
        targetDynamicMaintenanceJobPerformable.getFetchQuery(cronJobModel);
    }

    @Test
    public void testGetFetchQueryWhenNotInstanceOf() {
        exception.expect(IllegalStateException.class);
        exception
                .expectMessage("The job must be a TargetDynamicMaintenanceCleanupJobModel. Cannot execute the beanshell search");
        Mockito.when(dynamicMaintenanceCleanupJobModel.getLongSearchScript()).thenReturn(null);
        targetDynamicMaintenanceJobPerformable.getFetchQuery(cronJobModel);
    }

    @Test
    public void testGetFetchQueryWithSearcScript() throws EvalError {
        setUpData();
        final FlexibleSearchQuery fsq = targetDynamicMaintenanceJobPerformable.getFetchQuery(cronJobModel);
        Assert.assertNotNull(fsq);
    }

    /**
     * @throws EvalError
     */
    protected void setUpData() throws EvalError {
        Mockito.when(cronJobModel.getJob()).thenReturn(dynamicMaintenanceCleanupJobModel);
        Mockito.when(dynamicMaintenanceCleanupJobModel.getLongSearchScript()).thenReturn("This is Query");
        Mockito.when(interpreter.eval(Mockito.anyString())).thenReturn(flexibleSearchQuery);
        Mockito.when(interpreter.getNameSpace()).thenReturn(nameSpace);
        elements = new ArrayList<>();
        item = new ItemModel();
        elements.add(item);
    }

}

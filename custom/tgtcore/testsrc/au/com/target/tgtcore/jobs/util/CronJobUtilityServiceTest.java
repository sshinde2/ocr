/**
 * 
 */
package au.com.target.tgtcore.jobs.util;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.cronjob.model.JobModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.jobs.exceptions.CronJobAbortException;


/**
 * @author siddharam
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CronJobUtilityServiceTest {

    @InjectMocks
    private final CronJobUtilityService utilityService = new CronJobUtilityService();

    @Mock
    private CronJobService cronJobService;

    @Mock
    private ModelService modelService;

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    //CHECKSTYLE:ON


    @Before
    public void setUp() {
        //
    }

    @Test
    public void testGetRunningSyncJob() {
        final List<CronJobModel> cronJobsList = new ArrayList<>();
        final CronJobModel cronjob = Mockito.mock(CronJobModel.class);
        cronJobsList.add(cronjob);
        final JobModel job = Mockito.mock(JobModel.class);
        when(cronjob.getJob()).thenReturn(job);
        when(Boolean.valueOf(cronJobService.isRunning(cronjob))).thenReturn(Boolean.TRUE);
        when(job.getCode()).thenReturn("StepSync_targetProductCatalog");
        when(cronJobService.getRunningOrRestartedCronJobs()).thenReturn(cronJobsList);
        final CronJobModel result = utilityService.getRunningSyncJob();
        Assert.assertNotNull(result);
        Assert.assertEquals(result, cronjob);
    }

    @Test
    public void testGetRunningSyncJobWhenNotRunning() {
        final List<CronJobModel> cronJobsList = new ArrayList<>();
        final CronJobModel cronjob = Mockito.mock(CronJobModel.class);
        cronJobsList.add(cronjob);
        final JobModel job = Mockito.mock(JobModel.class);
        when(cronjob.getJob()).thenReturn(job);
        when(Boolean.valueOf(cronJobService.isRunning(cronjob))).thenReturn(Boolean.TRUE);
        when(cronJobService.getRunningOrRestartedCronJobs()).thenReturn(cronJobsList);
        final CronJobModel result = utilityService.getRunningSyncJob();
        Assert.assertNull(result);
    }

    @Test
    public void testWaitForSyncCronJobToFinish() throws InterruptedException, CronJobAbortException {
        final CronJobModel runningSyncJobModel = Mockito.mock(CronJobModel.class);
        final CronJobModel currentCronJob = Mockito.mock(CronJobModel.class);
        final JobModel job = Mockito.mock(JobModel.class);
        when(runningSyncJobModel.getJob()).thenReturn(job);
        Mockito.when(Boolean.valueOf(cronJobService.isRunning(runningSyncJobModel))).thenReturn(Boolean.TRUE,
                Boolean.FALSE);
        when(job.getCode()).thenReturn("StepSync_targetProductCatalog");
        utilityService.waitForSyncCronJobToFinish(currentCronJob, runningSyncJobModel, 60);
        Mockito.verify(runningSyncJobModel).getJob();
        Mockito.verify(job).getCode();
    }

    @Test
    public void testWaitForSyncCronJobToFinishWhenNotRunning() throws InterruptedException, CronJobAbortException {
        final CronJobModel runningSyncJobModel = Mockito.mock(CronJobModel.class);
        final CronJobModel currentCronJob = Mockito.mock(CronJobModel.class);
        final JobModel job = Mockito.mock(JobModel.class);
        when(runningSyncJobModel.getJob()).thenReturn(job);
        Mockito.when(Boolean.valueOf(cronJobService.isRunning(runningSyncJobModel))).thenReturn(
                Boolean.FALSE);
        when(job.getCode()).thenReturn("StepSync_targetProductCatalog");
        utilityService.waitForSyncCronJobToFinish(currentCronJob, runningSyncJobModel, 60);
        Mockito.verifyNoMoreInteractions(runningSyncJobModel);
    }

    @Test
    public void testCheckIfCurrentJobIsAborted() throws CronJobAbortException {
        exception.expect(CronJobAbortException.class);
        final CronJobModel currentCronJob = Mockito.mock(CronJobModel.class);
        when(currentCronJob.getRequestAbort()).thenReturn(Boolean.TRUE);
        utilityService.checkIfCurrentJobIsAborted(currentCronJob);
    }

}

package au.com.target.tgtcore.model.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.featureswitch.dao.TargetFeatureSwitchDao;
import au.com.target.tgtcore.model.TargetFeatureSwitchModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFeatureSwitchModelInterceptorTest {

    @InjectMocks
    private final TargetFeatureSwitchModelInterceptor interceptor = new TargetFeatureSwitchModelInterceptor();

    @Mock
    private TargetFeatureSwitchDao targetFeatureSwitchDao;

    @Mock
    private InterceptorContext ctx;

    private TargetFeatureSwitchModel model;

    @Mock
    private TargetFeatureSwitchModel targetFeatureSwitchModel;

    private List<List<String>> requireAtLeastOneLists;

    @Test
    public void testOnValidateWhenBothDisabled() {

        setUpMocks();
        model.setName("payment.tns");
        model.setEnabled(Boolean.FALSE);
        Mockito.doReturn(Boolean.FALSE).when(targetFeatureSwitchModel).getEnabled();
        Mockito.doReturn("payment.paypal").when(targetFeatureSwitchModel).getName();
        boolean isValidationSuccessful = false;
        try {
            interceptor.onValidate(model, ctx);
            isValidationSuccessful = true;
        }
        catch (final InterceptorException ie) {
            isValidationSuccessful = false;
        }
        Assert.assertFalse("Failed", isValidationSuccessful);
    }

    @Test
    public void testOnValidateWhenOneIsEnabled() {
        setUpMocks();
        model.setName("payment.tns");
        model.setEnabled(Boolean.TRUE);
        Mockito.doReturn(Boolean.FALSE).when(targetFeatureSwitchModel).getEnabled();
        Mockito.doReturn("payment.paypal").when(targetFeatureSwitchModel).getName();
        boolean isValidationSuccessful = false;
        try {
            interceptor.onValidate(model, ctx);
            isValidationSuccessful = true;
        }
        catch (final InterceptorException ie) {
            isValidationSuccessful = false;
        }
        Assert.assertTrue("Success", isValidationSuccessful);
    }

    @Test
    public void testOnValidateWhenOtherFeature() {
        setUpMocks();
        model.setName("Other");
        model.setEnabled(Boolean.TRUE);
        Mockito.doReturn(Boolean.FALSE).when(targetFeatureSwitchModel).getEnabled();
        Mockito.doReturn("payment.paypal").when(targetFeatureSwitchModel).getName();
        boolean isValidationSuccessful = false;
        try {
            interceptor.onValidate(model, ctx);
            isValidationSuccessful = true;
        }
        catch (final InterceptorException ie) {
            isValidationSuccessful = false;
        }
        Assert.assertTrue("Success", isValidationSuccessful);
    }


    @Test
    public void testOnValidateWhenFeatureExistInMultipleList() {
        setUpMocks();
        requireAtLeastOneLists.add(Arrays.asList("payment.tns", "payment.paypal"));
        requireAtLeastOneLists.add(Arrays.asList("payment.tns", "other"));
        model.setName("payment.tns");
        model.setEnabled(Boolean.FALSE);
        Mockito.doReturn(Boolean.TRUE).when(targetFeatureSwitchModel).getEnabled();
        Mockito.doReturn("other").when(targetFeatureSwitchModel).getName();
        boolean isValidationSuccessful = false;
        try {
            interceptor.onValidate(model, ctx);
            isValidationSuccessful = true;
        }
        catch (final InterceptorException ie) {
            isValidationSuccessful = false;
        }
        Assert.assertTrue("Success", isValidationSuccessful);
    }

    @Test
    public void testOnValidateWhenFeatureExistInMultipleListAndDisabled() {
        setUpMocks();
        requireAtLeastOneLists.add(Arrays.asList("payment.tns", "payment.paypal"));
        requireAtLeastOneLists.add(Arrays.asList("payment.tns", "other"));
        model.setName("payment.tns");
        model.setEnabled(Boolean.FALSE);
        Mockito.doReturn(Boolean.FALSE).when(targetFeatureSwitchModel).getEnabled();
        Mockito.doReturn("other").when(targetFeatureSwitchModel).getName();
        boolean isValidationSuccessful = false;
        try {
            interceptor.onValidate(model, ctx);
            isValidationSuccessful = true;
        }
        catch (final InterceptorException ie) {
            isValidationSuccessful = false;
        }
        Assert.assertFalse("Failed", isValidationSuccessful);
    }


    protected void setUpMocks() {
        requireAtLeastOneLists = new ArrayList<List<String>>();
        requireAtLeastOneLists.add(Arrays.asList("payment.tns", "payment.paypal"));
        interceptor.setRequireAtLeastOneLists(requireAtLeastOneLists);
        model = new TargetFeatureSwitchModel();
        Mockito.doReturn(targetFeatureSwitchModel).when(targetFeatureSwitchDao)
                .getFeatureByName(Mockito.anyString());
    }

}

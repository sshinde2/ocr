/**
 * 
 */
package au.com.target.tgtcore.order.impl;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.OrderModificationEntryStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;


/**
 * Unit test for {@link FindOrderRefundedShippingStrategyImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FindOrderRefundedShippingStrategyTest {

    @Mock
    private OrderModel order;

    private final FindOrderRefundedShippingStrategyImpl strategy = new FindOrderRefundedShippingStrategyImpl();

    /**
     * Verifies that refunded shipping amount is calculated from order modification entries.
     */
    @Test
    public void testGetRefundedShippingAmount() {
        final OrderModificationRecordModel parentRecord = mock(OrderModificationRecordModel.class);
        final OrderModificationRecordEntryModel record1 = mock(OrderCancelRecordEntryModel.class);
        final OrderModificationRecordEntryModel record2 = mock(OrderCancelRecordEntryModel.class);
        when(parentRecord.getModificationRecordEntries()).thenReturn(ImmutableList.of(record1, record2));
        when(record1.getRefundedShippingAmount()).thenReturn(Double.valueOf(9d));
        when(record1.getStatus()).thenReturn(OrderModificationEntryStatus.SUCCESSFULL);
        when(record2.getRefundedShippingAmount()).thenReturn(Double.valueOf(1d));
        when(record2.getStatus()).thenReturn(OrderModificationEntryStatus.FAILED);

        when(order.getInitialDeliveryCost()).thenReturn(Double.valueOf(15d));
        when(order.getModificationRecords()).thenReturn(ImmutableSet.of(parentRecord));

        assertEquals(9d, strategy.getRefundedShippingAmount(order), .001d);
    }

}

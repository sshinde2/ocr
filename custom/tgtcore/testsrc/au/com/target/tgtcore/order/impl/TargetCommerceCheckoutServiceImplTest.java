package au.com.target.tgtcore.order.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.OrderService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.jalo.PromotionsManager.AutoApplyMode;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.AddressService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.store.services.BaseStoreService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.cart.TargetCartService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.fluent.FluentStockService;
import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.model.StoreEmployeeModel;
import au.com.target.tgtcore.model.TMDiscountProductPromotionModel;
import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtcore.order.FluentOrderService;
import au.com.target.tgtcore.order.TargetDiscountService;
import au.com.target.tgtcore.order.UserCheckoutPreferencesService;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;
import au.com.target.tgtpayment.commands.result.TargetCardPaymentResult;
import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.exceptions.PaymentException;
import au.com.target.tgtpayment.service.PaymentsInProgressService;
import au.com.target.tgtpayment.service.TargetPaymentService;


/**
 * 
 * Unit Test for {@link TargetCommerceCheckoutServiceImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCommerceCheckoutServiceImplTest {

    private static final String VALID_FLYBUYS_NUMBER = "6008943218616910";

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException exception = ExpectedException.none();
    //CHECKSTYLE:ON

    @Mock
    private ModelService modelService;

    @Mock
    private CommerceCartService commerceCartService;

    @InjectMocks
    @Spy
    private final TargetCommerceCheckoutServiceImpl commerceCheckoutService = new TargetCommerceCheckoutServiceImpl();

    @Mock
    private UserService userService;

    @Mock
    private AddressService addressService;

    @Mock
    private TargetPointOfServiceService targetPointOfServiceService;

    @Mock
    private TargetDiscountService targetDiscountService;

    @Mock
    private TargetPointOfServiceModel pointOfService;

    @Mock
    private TargetPaymentService targetPaymentService;

    @Mock
    private PaymentsInProgressService paymentsInProgressService;

    @Mock
    private TargetStockService targetStockService;

    @Mock
    private WarehouseService warehouseService;

    @Mock
    private TargetCartService targetCartService;

    @Mock
    private FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    @Mock
    private PaymentTransactionEntryModel paymentTransactionEntryModel;

    @Mock
    private PaymentTransactionModel paymentTransactionModel;

    @Mock
    private AddressModel addressModel;

    @Mock
    private ProductModel productModel;

    @Mock
    private TitleModel titleModel;

    @Mock
    private DeliveryModeModel deliveryMode;

    @Mock
    private CartModel cart;

    @Mock
    private TargetCustomerModel customer;

    @Mock
    private AbstractOrderEntryModel cartEntryModel;

    @Mock
    private PaymentInfoModel paymentInfoModel;

    @Mock
    private CurrencyModel currencyModel;

    @Mock
    private WarehouseModel warehouseModel;

    @Mock
    private TargetVoucherService targetVoucherService;

    @Mock
    private OrderService orderService;

    @Mock
    private OrderModel order;

    @Mock
    private PromotionsService promotionsService;

    @Mock
    private CalculationService calculationService;

    @Mock
    private UserCheckoutPreferencesService userCheckoutPreferencesService;

    @Mock
    private TargetSalesApplicationConfigService salesApplicationConfigService;

    @Mock
    private FluentStockService fluentStockService;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Mock
    private CommerceCheckoutParameter checkoutParam;

    @Mock
    private FluentOrderService fluentOrderService;


    @Before
    public void setUp() {
        willReturn(Boolean.TRUE).given(commerceCartService).calculateCart(any(CommerceCartParameter.class));
        given(userService.getTitleForCode("mr")).willReturn(titleModel);

        given(warehouseService.getDefWarehouse()).willReturn(Collections.singletonList(warehouseModel));

        given(productModel.getName()).willReturn("test");
        given(productModel.getCode()).willReturn("P12345");
        given(cartEntryModel.getProduct()).willReturn(productModel);
        given(cartEntryModel.getQuantity()).willReturn(Long.valueOf(10L));
        given(cart.getEntries()).willReturn(Collections.singletonList(cartEntryModel));
        given(cart.getCurrency()).willReturn(currencyModel);
        given(cart.getTotalPrice()).willReturn(Double.valueOf(10));
        given(cart.getPaymentInfo()).willReturn(paymentInfoModel);
        given(cart.getDeliveryMode()).willReturn(deliveryMode);
        given(deliveryMode.getCode()).willReturn("click-and-collect");

        given(targetPaymentService.capture(any(CartModel.class), any(PaymentInfoModel.class),
                any(BigDecimal.class), any(CurrencyModel.class),
                any(PaymentCaptureType.class)))
                .willReturn(paymentTransactionEntryModel);

        commerceCheckoutService.setFlybuysLen(16);
        commerceCheckoutService.setFlybuysPrefix("6008");
    }

    @Test
    public void testFillCart4ClickAndCollect() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        given(targetPointOfServiceService.getPOSByStoreNumber(Integer.valueOf(1)))
                .willReturn(pointOfService);
        given(pointOfService.getAddress()).willReturn(addressModel);

        final AddressModel clonedAddress = mock(AddressModel.class);
        given(addressService.cloneAddressForOwner(addressModel, cart))
                .willReturn(clonedAddress);

        final boolean returnValue = commerceCheckoutService.fillCart4ClickAndCollect(cart, 1, "mr", "first", "last",
                "12345678", deliveryMode);

        assertThat(returnValue).isTrue();
        verify(clonedAddress).setTitle(titleModel);
        verify(clonedAddress).setFirstname("first");
        verify(clonedAddress).setLastname("last");
        verify(clonedAddress).setPhone1("12345678");
        verify(modelService).save(cart);
        verify(commerceCartService).calculateCart(any(CommerceCartParameter.class));
    }

    @Test
    public void testFillCart4ClickAndCollectError() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        given(targetPointOfServiceService.getPOSByStoreNumber(Integer.valueOf(1)))
                .willThrow(new TargetUnknownIdentifierException(""));
        given(pointOfService.getAddress()).willReturn(addressModel);

        final AddressModel clonedAddress = mock(AddressModel.class);
        final TitleModel title = mock(TitleModel.class);
        given(addressService.cloneAddressForOwner(addressModel, cart))
                .willReturn(clonedAddress);

        commerceCheckoutService.fillCart4ClickAndCollectWithoutRecalculate(cart, Integer.valueOf(1), title, "first",
                "last",
                "12345678", deliveryMode);

        verifyZeroInteractions(addressService, modelService, clonedAddress);
    }

    @Test
    public void testFillCart4ClickAndCollectError2() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        given(targetPointOfServiceService.getPOSByStoreNumber(Integer.valueOf(1)))
                .willThrow(new TargetAmbiguousIdentifierException(""));
        given(pointOfService.getAddress()).willReturn(addressModel);

        final AddressModel clonedAddress = mock(AddressModel.class);
        final TitleModel title = mock(TitleModel.class);
        given(addressService.cloneAddressForOwner(addressModel, cart))
                .willReturn(clonedAddress);

        commerceCheckoutService.fillCart4ClickAndCollectWithoutRecalculate(cart, Integer.valueOf(1), title, "first",
                "last",
                "12345678", deliveryMode);

        verifyZeroInteractions(addressService, modelService, clonedAddress);
    }

    @Test
    public void testFillCart4ClickAndCollectEmptyTitle() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        given(targetPointOfServiceService.getPOSByStoreNumber(Integer.valueOf(1)))
                .willReturn(pointOfService);
        given(pointOfService.getAddress()).willReturn(addressModel);

        final AddressModel clonedAddress = mock(AddressModel.class);
        given(addressService.cloneAddressForOwner(addressModel, cart))
                .willReturn(clonedAddress);

        final boolean returnValue = commerceCheckoutService.fillCart4ClickAndCollect(cart, 1, "", "first", "last",
                "12345678", deliveryMode);

        assertThat(returnValue).isTrue();

        verify(clonedAddress).setTitle(null);
        verify(clonedAddress).setFirstname("first");
        verify(clonedAddress).setLastname("last");
        verify(clonedAddress).setPhone1("12345678");
        verify(modelService).save(cart);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testFillCart4ClickAndCollectInvalidParam() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        commerceCheckoutService.fillCart4ClickAndCollect(null, 1, "mr", "first", "last",
                "12345678", deliveryMode);
    }

    @Test
    public void testValidFlybuysNumber() {
        given(cart.getFlyBuysCode()).willReturn(VALID_FLYBUYS_NUMBER);
        final boolean result = commerceCheckoutService.isFlybuysNumberValid(cart);

        assertThat(result).isTrue();
    }

    @Test
    public void testInvalidStartsWithFlybuysNumber() {
        given(cart.getFlyBuysCode()).willReturn("9300675001427");
        final boolean result = commerceCheckoutService.isFlybuysNumberValid(cart);

        assertThat(result).isFalse();
    }

    @Test
    public void testInvalidLengthFlybuysNumber() {
        given(cart.getFlyBuysCode()).willReturn("00159159");
        final boolean result = commerceCheckoutService.isFlybuysNumberValid(cart);

        assertThat(result).isFalse();
    }

    @Test
    public void testsetTMDCardNumberNullCart() {
        exception.expect(IllegalArgumentException.class);
        commerceCheckoutService.setTMDCardNumber(null, "123");
    }

    @Test
    public void testsetTMDCardNumberNullCardNumber() {
        final CartModel checkoutCart = mock(CartModel.class);

        final boolean result = commerceCheckoutService.setTMDCardNumber(checkoutCart, null);

        assertThat(result).isFalse();

        verifyZeroInteractions(checkoutCart);
        verifyZeroInteractions(modelService);
        verifyZeroInteractions(targetDiscountService);
        verifyZeroInteractions(commerceCartService);
        verifyZeroInteractions(targetCartService);
    }

    @Test
    public void testsetTMDCardNumberBlankCardNumber() {
        final CartModel checkoutCart = mock(CartModel.class);

        final boolean result = commerceCheckoutService.setTMDCardNumber(checkoutCart, "");

        assertThat(result).isFalse();

        verifyZeroInteractions(checkoutCart);
        verifyZeroInteractions(targetDiscountService);
        verifyZeroInteractions(modelService);
        verifyZeroInteractions(commerceCartService);
        verifyZeroInteractions(targetCartService);
    }

    @Test
    public void testsetTMDCardNumberForHotCard() {
        final CartModel checkoutCart = mock(CartModel.class);

        willReturn(Boolean.FALSE).given(targetDiscountService).isValidTMDCard(anyString());

        final boolean result = commerceCheckoutService.setTMDCardNumber(checkoutCart, "00000000");

        assertThat(result).isFalse();

        verify(targetDiscountService).isValidTMDCard("00000000");

        verifyZeroInteractions(checkoutCart);
        verifyZeroInteractions(modelService);
        verifyZeroInteractions(commerceCartService);
        verifyZeroInteractions(targetCartService);
    }

    @Test
    public void testsetTMDCardNumberValidCardNumber() {
        final String cardNumber = "12345678";
        final CartModel checkoutCart = mock(CartModel.class);
        final CartModel masterCart = mock(CartModel.class);

        willReturn(Boolean.TRUE).given(targetDiscountService).isValidTMDCard(cardNumber);
        given(targetCartService.getSessionCart()).willReturn(masterCart);
        willReturn(Boolean.TRUE).given(targetDiscountService).isTMDiscountAppliedToOrder(checkoutCart);

        final boolean result = commerceCheckoutService.setTMDCardNumber(checkoutCart, cardNumber);

        assertThat(result).isTrue();

        // check apply on checkout cart
        verify(checkoutCart).setTmdCardNumber(cardNumber);
        verify(modelService).save(checkoutCart);
        verify(commerceCartService).calculateCart(any(CommerceCartParameter.class));
    }

    @Test
    public void testsetTMDCardNumberValidCardNumberNoAppliedTMDPromotions() {
        final String cardNumber = "12345678";
        final CartModel checkoutCart = mock(CartModel.class);
        final CartModel masterCart = mock(CartModel.class);

        willReturn(Boolean.TRUE).given(targetDiscountService).isValidTMDCard(cardNumber);
        given(targetCartService.getSessionCart()).willReturn(masterCart);
        willReturn(Boolean.FALSE).given(targetDiscountService).isTMDiscountAppliedToOrder(checkoutCart);

        final boolean result = commerceCheckoutService.setTMDCardNumber(checkoutCart, cardNumber);

        assertThat(result).isFalse();

        // check apply on checkout cart
        verify(checkoutCart).setTmdCardNumber(cardNumber);
        verify(modelService).save(checkoutCart);
        verify(commerceCartService).calculateCart(any(CommerceCartParameter.class));
    }

    @Test
    public void testBeforePlaceOrderForNullCart() throws InsufficientStockLevelException {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("CartModel cannot be null");
        commerceCheckoutService.beforePlaceOrder(null, null);
    }

    @Test
    public void testBeforePlaceOrderWithNullPaymentInfo() throws InsufficientStockLevelException {
        given(cart.getPaymentInfo()).willReturn(null);
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("PaymentInfo cannot be null");
        commerceCheckoutService.beforePlaceOrder(cart, null);
    }

    @Test
    public void testBeforePlaceOrderForGuest() throws InsufficientStockLevelException {
        final String firstName = "John";
        final String lastName = "Smith";

        final TitleModel mockTitleForDeliverytAddress = mock(TitleModel.class);

        final TargetAddressModel mockDeliverytAddress = mock(TargetAddressModel.class);
        given(mockDeliverytAddress.getTitle()).willReturn(mockTitleForDeliverytAddress);
        given(mockDeliverytAddress.getFirstname()).willReturn(firstName);
        given(mockDeliverytAddress.getLastname()).willReturn(lastName);

        given(cart.getPaymentInfo()).willReturn(paymentInfoModel);
        given(cart.getUser()).willReturn(customer);
        given(customer.getType()).willReturn(CustomerType.GUEST);
        given(cart.getDeliveryAddress()).willReturn(mockDeliverytAddress);

        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(cart)).willReturn(
                new Double(10d));

        final boolean successful = commerceCheckoutService.beforePlaceOrder(cart, null);
        assertThat(successful).isTrue();

        verify(targetStockService)
                .reserve(productModel, 10, "ReserveStock : cart=null, quantity=10 for product=test");
        verify(targetPaymentService).capture(cart, paymentInfoModel, BigDecimal.valueOf(10.0), currencyModel,
                PaymentCaptureType.PLACEORDER);

        verify(customer).setTitle(mockTitleForDeliverytAddress);
        verify(customer).setFirstname(firstName);
        verify(customer).setLastname(lastName);
        verify(customer).setName(firstName + " " + lastName);
        verify(modelService).save(customer);
    }

    @Test
    public void testBeforePlaceOrderWithNullCartTotal() throws InsufficientStockLevelException {
        given(cart.getPaymentInfo()).willReturn(paymentInfoModel);
        given(cart.getTotalPrice()).willReturn(null);
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Cart total cannot be null");
        commerceCheckoutService.beforePlaceOrder(cart, null);
    }

    @Test(expected = InsufficientStockLevelException.class)
    public void testBeforePlaceOrderWithInsufficientStock() throws InsufficientStockLevelException {
        willThrow(new InsufficientStockLevelException("test")).given(targetStockService).reserve(
                any(ProductModel.class),
                anyInt(), anyString());
        commerceCheckoutService.beforePlaceOrder(cart, null);
    }

    @Test(expected = FluentOrderException.class)
    public void testBeforePlaceOrderWithFluentException() throws InsufficientStockLevelException {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        willThrow(new FluentOrderException()).given(fluentStockService).lookupPlaceOrderAts(anyList(), anyString());
        commerceCheckoutService.beforePlaceOrder(cart, null);
    }

    @Test
    public void testBeforePlaceOrderCallsCaptureMethodWithoutTransaction() throws InsufficientStockLevelException {
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(cart)).willReturn(new Double(10d));

        final boolean successful = commerceCheckoutService.beforePlaceOrder(cart, null);
        assertThat(successful).isTrue();

        verify(targetStockService).reserve(productModel, 10, "ReserveStock : cart=null, quantity=10 for product=test");
        verify(targetPaymentService).capture(cart, paymentInfoModel, BigDecimal.valueOf(10.0), currencyModel,
                PaymentCaptureType.PLACEORDER);
        verify(targetPaymentService, never()).capture(cart, paymentInfoModel, BigDecimal.valueOf(10.0),
                currencyModel,
                PaymentCaptureType.PLACEORDER, paymentTransactionModel);
    }

    @Test
    public void testBeforePlaceOrderCallsCaptureMethodWithTransaction() throws InsufficientStockLevelException {
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(cart)).willReturn(
                new Double(10d));

        final boolean successful = commerceCheckoutService.beforePlaceOrder(cart, paymentTransactionModel);
        assertThat(successful).isTrue();

        verify(targetStockService).reserve(productModel, 10, "ReserveStock : cart=null, quantity=10 for product=test");
        verify(targetPaymentService, never()).capture(cart, paymentInfoModel, BigDecimal.valueOf(10.0),
                currencyModel,
                PaymentCaptureType.PLACEORDER);
        verify(targetPaymentService).capture(cart, paymentInfoModel, BigDecimal.valueOf(10.0),
                currencyModel,
                PaymentCaptureType.PLACEORDER, paymentTransactionModel);
        verify(paymentTransactionModel).setIsCaptureSuccessful(Boolean.TRUE);
        verify(modelService).save(paymentTransactionModel);
        verify(modelService).refresh(paymentTransactionModel);
    }

    @Test
    public void testBeforePlaceOrderForFullAmountCapture() throws InsufficientStockLevelException {
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(cart)).willReturn(
                new Double(10d));

        final boolean successful = commerceCheckoutService.beforePlaceOrder(cart, null);
        assertThat(successful).isTrue();

        verify(targetStockService)
                .reserve(productModel, 10, "ReserveStock : cart=null, quantity=10 for product=test");
        verify(targetPaymentService).capture(cart, paymentInfoModel, BigDecimal.valueOf(10.0), currencyModel,
                PaymentCaptureType.PLACEORDER);
    }

    @Test
    public void testBeforePlaceOrderForFullAmountCaptureWithPreCreatedTransaction()
            throws InsufficientStockLevelException {
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(cart)).willReturn(
                new Double(10d));

        final boolean successful = commerceCheckoutService.beforePlaceOrder(cart, paymentTransactionModel);
        assertThat(successful).isTrue();

        verify(targetStockService).reserve(productModel, 10,
                "ReserveStock : cart=null, quantity=10 for product=test");
        verify(targetPaymentService).capture(cart, paymentInfoModel, BigDecimal.valueOf(10.0), currencyModel,
                PaymentCaptureType.PLACEORDER, paymentTransactionModel);
    }

    @Test
    public void testBeforePlaceOrderForPartialAmountCapture() throws InsufficientStockLevelException {
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(cart)).willReturn(
                new Double(8d));
        willReturn(Boolean.TRUE).given(paymentsInProgressService).removeInProgressPayment(cart);
        final boolean successful = commerceCheckoutService.beforePlaceOrder(cart, null);
        assertThat(successful).isFalse();

        verify(targetStockService)
                .reserve(productModel, 10, "ReserveStock : cart=null, quantity=10 for product=test");
        verify(targetPaymentService).capture(cart, paymentInfoModel, BigDecimal.valueOf(10.0), currencyModel,
                PaymentCaptureType.PLACEORDER);
        verify(targetStockService).release(productModel, 10, "ReleaseStock : cart=null, quantity=10 for product=test");
        verify(paymentsInProgressService).removeInProgressPayment(cart);
    }

    @Test
    public void testBeforePlaceOrderForPartialAmountCaptureWithPreCreatedTransaction()
            throws InsufficientStockLevelException {
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(cart)).willReturn(Double.valueOf(8d));
        willReturn(Boolean.TRUE).given(paymentsInProgressService).removeInProgressPayment(cart);
        final boolean successful = commerceCheckoutService.beforePlaceOrder(cart, paymentTransactionModel);
        assertThat(successful).isFalse();

        verify(targetStockService)
                .reserve(productModel, 10, "ReserveStock : cart=null, quantity=10 for product=test");
        verify(targetPaymentService).capture(cart, paymentInfoModel, BigDecimal.valueOf(10.0), currencyModel,
                PaymentCaptureType.PLACEORDER, paymentTransactionModel);
        verify(targetStockService).release(productModel, 10, "ReleaseStock : cart=null, quantity=10 for product=test");
        verify(paymentsInProgressService).removeInProgressPayment(cart);
        verify(paymentTransactionModel).setIsCaptureSuccessful(Boolean.FALSE);
    }

    @Test
    public void testBeforePlaceOrderForZeroAmountCapture() throws InsufficientStockLevelException {
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(cart)).willReturn(Double.valueOf(0d));
        willReturn(Boolean.TRUE).given(paymentsInProgressService).removeInProgressPayment(cart);
        final boolean successful = commerceCheckoutService.beforePlaceOrder(cart, null);
        assertThat(successful).isFalse();

        verify(targetStockService)
                .reserve(productModel, 10, "ReserveStock : cart=null, quantity=10 for product=test");
        verify(targetPaymentService).capture(cart, paymentInfoModel, BigDecimal.valueOf(10.0), currencyModel,
                PaymentCaptureType.PLACEORDER);
        verify(targetStockService).release(productModel, 10, "ReleaseStock : cart=null, quantity=10 for product=test");
        verify(paymentsInProgressService).removeInProgressPayment(cart);
    }

    @Test
    public void testBeforePlaceOrderForZeroAmountCaptureWithPreCreatedTransactionEntry()
            throws InsufficientStockLevelException {
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(cart)).willReturn(Double.valueOf(0d));
        willReturn(Boolean.TRUE).given(paymentsInProgressService).removeInProgressPayment(cart);
        final boolean successful = commerceCheckoutService.beforePlaceOrder(cart, paymentTransactionModel);
        assertThat(successful).isFalse();

        verify(targetStockService)
                .reserve(productModel, 10, "ReserveStock : cart=null, quantity=10 for product=test");
        verify(targetPaymentService).capture(cart, paymentInfoModel, BigDecimal.valueOf(10.0), currencyModel,
                PaymentCaptureType.PLACEORDER, paymentTransactionModel);
        verify(targetStockService).release(productModel, 10, "ReleaseStock : cart=null, quantity=10 for product=test");
        verify(paymentsInProgressService).removeInProgressPayment(cart);
        verify(paymentTransactionModel).setIsCaptureSuccessful(Boolean.FALSE);
    }

    @Test
    public void testAfterPlaceOrderForNullCart() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("CartModel cannot be null");
        commerceCheckoutService.afterPlaceOrder(null, null);
    }

    @Test
    public void testAfterPlaceOrderForNullOrder() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("OrderModel cannot be null");
        commerceCheckoutService.afterPlaceOrder(cart, null);
    }

    @Test
    public void testAfterPlaceOrderWithNoFlybuysOnOrder() {
        final OrderModel orderModel = mock(OrderModel.class);
        given(orderModel.getPaymentInfo()).willReturn(paymentInfoModel);
        given(orderModel.getUser()).willReturn(customer);

        commerceCheckoutService.afterPlaceOrder(cart, orderModel);

        verify(customer, never()).setFlyBuysCode(any(String.class));
        verify(modelService, never()).save(any(TargetCustomerModel.class));
        verify(paymentsInProgressService).removeInProgressPayment(cart);
        verify(targetCartService).removeSessionCart();
        verify(modelService).refresh(orderModel);
    }

    @Test
    public void testAfterPlaceOrderForGuestWithNoPaymentAddress() {
        final OrderModel orderModel = mock(OrderModel.class);
        given(orderModel.getPaymentInfo()).willReturn(paymentInfoModel);
        given(customer.getType()).willReturn(CustomerType.GUEST);
        given(orderModel.getUser()).willReturn(customer);

        commerceCheckoutService.afterPlaceOrder(cart, orderModel);

        verify(paymentsInProgressService).removeInProgressPayment(cart);
        verify(targetCartService).removeSessionCart();
        verify(modelService).refresh(orderModel);
        verify(customer, never()).setFlyBuysCode(any(String.class));
        verify(customer, never()).setTitle(any(TitleModel.class));
        verify(customer, never()).setFirstname(any(String.class));
        verify(customer, never()).setLastname(any(String.class));
        verify(customer, never()).setName(any(String.class));
        verify(modelService, never()).save(any(TargetCustomerModel.class));
    }

    @Test
    public void testAfterPlaceOrderForGuestWithFlybuys() {

        final OrderModel orderModel = mock(OrderModel.class);
        given(orderModel.getPaymentInfo()).willReturn(paymentInfoModel);
        given(customer.getType()).willReturn(CustomerType.GUEST);
        given(orderModel.getUser()).willReturn(customer);
        given(orderModel.getFlyBuysCode()).willReturn("myFlybuysCode");

        commerceCheckoutService.afterPlaceOrder(cart, orderModel);

        verify(paymentsInProgressService).removeInProgressPayment(cart);
        verify(targetCartService).removeSessionCart();
        verify(modelService).refresh(orderModel);
        verify(customer, never()).setFlyBuysCode("myFlybuysCode");
        verify(modelService, never()).save(customer);
    }

    @Test
    public void testAfterPlaceOrderForRegisteredUserWithFlybuys() {
        final OrderModel orderModel = mock(OrderModel.class);
        given(orderModel.getPaymentInfo()).willReturn(paymentInfoModel);
        given(orderModel.getUser()).willReturn(customer);
        given(orderModel.getFlyBuysCode()).willReturn("myFlybuysCode");

        commerceCheckoutService.afterPlaceOrder(cart, orderModel);

        verify(paymentsInProgressService).removeInProgressPayment(cart);
        verify(targetCartService).removeSessionCart();
        verify(modelService).refresh(orderModel);
        verify(customer).setFlyBuysCode("myFlybuysCode");
        verify(modelService).save(customer);
    }

    @Test
    public void testSetFlybuysNumberForNullCart() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("CartModel cannot be null");
        commerceCheckoutService.setFlybuysNumber(null, null);
    }

    @Test
    public void testSetFlybuysNumberForNullNumber() {
        assertThat(commerceCheckoutService.setFlybuysNumber(cart, null)).isTrue();
    }

    @Test
    public void testSetFlybuysNumberForInvalidNumber() {
        final boolean result = commerceCheckoutService.setFlybuysNumber(cart, "0000");
        assertThat(result).isFalse();

        verifyZeroInteractions(cart);
    }

    @Test
    public void testSetFlybuysNumberForvalidNumber() {
        final boolean result = commerceCheckoutService.setFlybuysNumber(cart, VALID_FLYBUYS_NUMBER);
        assertThat(result).isTrue();

        verify(cart).setFlyBuysCode(VALID_FLYBUYS_NUMBER);
        verify(modelService).save(cart);
    }

    @Test
    public void testSetPaymentSucceeded() {
        final OrderModel orderModel = mock(OrderModel.class);
        final PaymentInfoModel orderPaymentInfo = mock(PaymentInfoModel.class);
        given(orderModel.getPaymentInfo()).willReturn(orderPaymentInfo);
        commerceCheckoutService.afterPlaceOrder(cart, orderModel);

        verify(paymentInfoModel).setIsPaymentSucceeded(Boolean.TRUE);
        verify(orderPaymentInfo).setIsPaymentSucceeded(Boolean.TRUE);
        verify(modelService).saveAll(paymentInfoModel, orderPaymentInfo);
    }

    @Test
    public void testIsCaptureOrderSuccessfulNoAmountCaptured() {
        given(cart.getTotalPrice()).willReturn(Double.valueOf(12.34d));
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(cart)).willReturn(new Double(0d));

        final boolean result = commerceCheckoutService.isCaptureOrderSuccessful(cart);
        assertThat(result).isFalse();
    }

    @Test
    public void testIsCaptureOrderSuccessfulLessAmountCaptured() {
        given(cart.getTotalPrice()).willReturn(Double.valueOf(12.34d));
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(cart)).willReturn(
                new Double(12.33d));

        final boolean result = commerceCheckoutService.isCaptureOrderSuccessful(cart);
        assertThat(result).isFalse();
    }

    @Test
    public void testIsCaptureOrderSuccessfulMoreAmountCaptured() {
        given(cart.getTotalPrice()).willReturn(Double.valueOf(12.34d));
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(cart)).willReturn(
                new Double(12.35d));

        final boolean result = commerceCheckoutService.isCaptureOrderSuccessful(cart);
        assertThat(result).isFalse();
    }

    @Test
    public void testIsCaptureOrderSuccessful() {
        given(cart.getTotalPrice()).willReturn(Double.valueOf(12.34d));
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(cart)).willReturn(
                new Double(12.34d));

        final boolean result = commerceCheckoutService.isCaptureOrderSuccessful(cart);
        verify(modelService).refresh(cart);
        assertThat(result).isTrue();
    }

    @Test
    public void testIsCapturedOrderSuccessfulWithDifferentNumberOfZeroFractionDigits() {
        //two decimal points with 0 in both
        given(cart.getTotalPrice()).willReturn(Double.valueOf(12.00d));
        //no decimal points
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(cart)).willReturn(
                new Double(12));

        final boolean result = commerceCheckoutService.isCaptureOrderSuccessful(cart);
        assertThat(result).isTrue();
    }

    @Test
    public void handlePaymentFailureNullCart() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("CartModel cannot be null");

        commerceCheckoutService.handlePaymentFailure(null);
    }

    @Test
    public void handlePaymentFailure() {
        willReturn(Boolean.TRUE).given(paymentsInProgressService).removeInProgressPayment(cart);
        commerceCheckoutService.handlePaymentFailure(cart);
        verify(targetPaymentService).refundLastCaptureTransaction(cart);
        verify(targetStockService).release(productModel, 10, "ReleaseStock : cart=null, quantity=10 for product=test");
    }

    @Test
    public void testSetKioskStoreNumberNullCart() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Cart model cannot be null");

        commerceCheckoutService.setKioskStoreNumber(null, null);

        verifyZeroInteractions(modelService);
    }

    @Test
    public void testSetKioskStoreNumberNullKioskStoreNumber() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Kiosk Store number cannot be null or empty");

        commerceCheckoutService.setKioskStoreNumber(cart, null);

        verifyZeroInteractions(modelService);
    }

    @Test
    public void testSetKioskStoreNumberEmptyKioskStoreNumber() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Kiosk Store number cannot be null or empty");

        commerceCheckoutService.setKioskStoreNumber(cart, "");

        verifyZeroInteractions(modelService);
    }

    @Test
    public void testSetKioskStoreNumber() {

        final boolean result = commerceCheckoutService.setKioskStoreNumber(cart, "5001");

        assertThat(result).isTrue();

        verify(cart).setKioskStore("5001");
        verify(modelService).save(cart);
    }

    @Test
    public void testSetStoreAssistedNullCart() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Cart model cannot be null");

        commerceCheckoutService.setKioskStoreNumber(null, null);

        verifyZeroInteractions(modelService);
    }

    @Test
    public void testSetStoreAssistedNullEmployee() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Store Employee cannot be null");

        commerceCheckoutService.setStoreAssisted(cart, null);

        verifyZeroInteractions(modelService);
    }

    @Test
    public void testSetStoreAssisted() {
        final StoreEmployeeModel storeEmployee = mock(StoreEmployeeModel.class);
        final TargetPointOfServiceModel pos = mock(TargetPointOfServiceModel.class);
        given(pos.getStoreNumber()).willReturn(Integer.valueOf(5098));
        given(storeEmployee.getStore()).willReturn(pos);
        final boolean result = commerceCheckoutService.setStoreAssisted(cart, storeEmployee);

        assertThat(result).isTrue();

        verify(cart).setKioskStore("5098");
        verify(cart).setAssistedBy(storeEmployee);
        verify(modelService).save(cart);
    }

    @Test
    public void testCheckFlybuysNumDiscountSimilar() {

        final FlybuysDiscountModel fbDiscountModel = mock(FlybuysDiscountModel.class);
        final String dummyFlybuysNumber = "123456789";

        given(targetVoucherService.getFlybuysDiscountForOrder(cart)).willReturn(fbDiscountModel);
        given(cart.getFlyBuysCode()).willReturn(dummyFlybuysNumber);

        assertThat(commerceCheckoutService.checkFlybuysNumDiscount(cart, dummyFlybuysNumber)).isTrue();
    }

    @Test
    public void testCheckFlybuysNumDiscountNonfbDiscount() {
        final String dummyFlybuysNumber = "7789943218616910";
        assertThat(commerceCheckoutService.checkFlybuysNumDiscount(cart, dummyFlybuysNumber)).isTrue();
    }

    @Test
    public void testCheckFlybuysNumDiscountDissimilar() {

        final String dummyFlybuysNumber = "899843218616910";

        final FlybuysDiscountModel fbDiscountModel = mock(FlybuysDiscountModel.class);
        given(targetVoucherService.getFlybuysDiscountForOrder(cart)).willReturn(fbDiscountModel);
        assertThat(commerceCheckoutService.checkFlybuysNumDiscount(cart, dummyFlybuysNumber)).isFalse();

    }

    @Test
    public void testCheckFlybuysNumDiscountNullFBNum() {

        final FlybuysDiscountModel fbDiscountModel = mock(FlybuysDiscountModel.class);
        final String dummyFlybuysNumber = "897678765";

        given(targetVoucherService.getFlybuysDiscountForOrder(cart)).willReturn(fbDiscountModel);
        given(cart.getFlyBuysCode()).willReturn(dummyFlybuysNumber);

        assertThat(commerceCheckoutService.checkFlybuysNumDiscount(cart, dummyFlybuysNumber)).isTrue();
    }

    @Test
    public void testPlaceOrderWithTMDAndFalseOrderCalculated() throws InvalidCartException, CalculationException {
        final BaseSiteService baseSiteService = mock(BaseSiteService.class);
        final BaseStoreService baseStoreService = mock(BaseStoreService.class);
        final PromotionResultModel promotionResult = mock(PromotionResultModel.class);
        final Set<PromotionResultModel> promotionResults = new HashSet<>();
        promotionResults.add(promotionResult);
        final List<PromotionGroupModel> promotionGroupModel = new ArrayList<>();
        promotionGroupModel.add(null);
        final TMDiscountProductPromotionModel tmDiscount = mock(TMDiscountProductPromotionModel.class);
        final Date date = new Date();

        commerceCheckoutService.setOrderService(orderService);
        commerceCheckoutService.setBaseSiteService(baseSiteService);
        commerceCheckoutService.setBaseStoreService(baseStoreService);

        given(paymentInfoModel.getBillingAddress()).willReturn(addressModel);
        given(order.getPaymentInfo()).willReturn(paymentInfoModel);
        given(order.getAllPromotionResults()).willReturn(promotionResults);
        given(order.getDate()).willReturn(date);
        given(orderService.createOrderFromCart(cart)).willReturn(order);
        willReturn(Boolean.TRUE).given(cart).getCalculated();
        given(cart.getPaymentInfo()).willReturn(paymentInfoModel);
        given(cart.getUser()).willReturn(customer);
        given(promotionResult.getPromotion()).willReturn(tmDiscount);

        willReturn(Boolean.FALSE).given(order).getCalculated();

        final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
        checkoutParameter.setCart(cart);
        checkoutParameter.setSalesApplication(SalesApplication.WEB);
        final CommerceOrderResult commerceOrderResult = commerceCheckoutService.placeOrder(checkoutParameter);

        assertThat(commerceOrderResult).isNotNull();

        verify(order).setCalculated(Boolean.TRUE);
        verify(promotionsService).updatePromotions(promotionGroupModel, order, true,
                AutoApplyMode.APPLY_ALL,
                AutoApplyMode.APPLY_ALL, date);
    }

    @Test
    public void testPlaceOrderWithTMDAndTrueOrderCalculated() throws InvalidCartException, CalculationException {
        final BaseSiteService baseSiteService = mock(BaseSiteService.class);
        final BaseStoreService baseStoreService = mock(BaseStoreService.class);
        final PromotionResultModel promotionResult = mock(PromotionResultModel.class);
        final Set<PromotionResultModel> promotionResults = new HashSet<>();
        promotionResults.add(promotionResult);
        final List<PromotionGroupModel> promotionGroupModel = new ArrayList<>();
        promotionGroupModel.add(null);
        final TMDiscountProductPromotionModel tmDiscount = mock(TMDiscountProductPromotionModel.class);
        final Date date = new Date();

        commerceCheckoutService.setOrderService(orderService);
        commerceCheckoutService.setBaseSiteService(baseSiteService);
        commerceCheckoutService.setBaseStoreService(baseStoreService);

        given(paymentInfoModel.getBillingAddress()).willReturn(addressModel);
        given(order.getPaymentInfo()).willReturn(paymentInfoModel);
        given(order.getAllPromotionResults()).willReturn(promotionResults);
        given(order.getDate()).willReturn(date);
        given(orderService.createOrderFromCart(cart)).willReturn(order);
        willReturn(Boolean.TRUE).given(cart).getCalculated();
        given(cart.getPaymentInfo()).willReturn(paymentInfoModel);
        given(cart.getUser()).willReturn(customer);
        given(promotionResult.getPromotion()).willReturn(tmDiscount);

        willReturn(Boolean.TRUE).given(order).getCalculated();

        final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
        checkoutParameter.setCart(cart);
        checkoutParameter.setSalesApplication(SalesApplication.WEB);
        final CommerceOrderResult commerceOrderResult = commerceCheckoutService.placeOrder(checkoutParameter);

        assertThat(commerceOrderResult).isNotNull();
        verify(order, never()).setCalculated(Boolean.TRUE);
        verify(promotionsService).updatePromotions(promotionGroupModel, order, true,
                AutoApplyMode.APPLY_ALL,
                AutoApplyMode.APPLY_ALL, date);
    }

    @Test
    public void testPlaceOrderPartnerOrder() throws InvalidCartException, CalculationException {
        final BaseSiteService baseSiteService = mock(BaseSiteService.class);
        final BaseStoreService baseStoreService = mock(BaseStoreService.class);
        final Date date = new Date();

        commerceCheckoutService.setOrderService(orderService);
        commerceCheckoutService.setBaseSiteService(baseSiteService);
        commerceCheckoutService.setBaseStoreService(baseStoreService);

        given(paymentInfoModel.getBillingAddress()).willReturn(addressModel);
        given(order.getPaymentInfo()).willReturn(paymentInfoModel);
        given(order.getDate()).willReturn(date);
        given(orderService.createOrderFromCart(cart)).willReturn(order);
        willReturn(Boolean.TRUE).given(cart).getCalculated();
        given(cart.getPaymentInfo()).willReturn(paymentInfoModel);
        given(cart.getUser()).willReturn(customer);
        willReturn(Boolean.FALSE).given(order).getCalculated();
        given(Boolean.valueOf(salesApplicationConfigService.isPartnerChannel(SalesApplication.EBAY)))
                .willReturn(Boolean.TRUE);
        final AddressModel alonedAddress = mock(AddressModel.class);
        given(modelService.clone(addressModel)).willReturn(alonedAddress);

        final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
        checkoutParameter.setCart(cart);
        checkoutParameter.setSalesApplication(SalesApplication.EBAY);
        final CommerceOrderResult commerceOrderResult = commerceCheckoutService.placeOrder(checkoutParameter);
        verify(modelService).clone(addressModel);
        verify(order).setPaymentAddress(alonedAddress);
        verify(paymentInfoModel).setBillingAddress(alonedAddress);

        assertThat(commerceOrderResult).isNotNull();
        verifyZeroInteractions(promotionsService);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemovePaymentInfoWithNullCart() {
        commerceCheckoutService.removePaymentInfo(null);
    }

    @Test
    public void testRemovePaymentInfo() {
        final CartModel mockCart = mock(CartModel.class);

        commerceCheckoutService.removePaymentInfo(mockCart);

        final InOrder verifyOrder = inOrder(mockCart, modelService);
        verifyOrder.verify(mockCart).setPaymentInfo(null);
        verifyOrder.verify(modelService).save(mockCart);
        verifyOrder.verify(modelService).refresh(mockCart);
    }

    @Test
    public void testNormalizeEntryNumbers() {
        final List<AbstractOrderEntryModel> entries = createUnNormalizeEntries();
        final CartModel cartModel = new CartModel();
        cartModel.setEntries(entries);
        commerceCheckoutService.normalizeEntryNumbers(cartModel);
        willDoNothing().given(modelService).save(any(Object.class));

        assertThat(cartModel.getEntries().get(0).getEntryNumber()).isEqualTo(Integer.valueOf(0));
        assertThat(cartModel.getEntries().get(1).getEntryNumber()).isEqualTo(Integer.valueOf(1));
        assertThat(cartModel.getEntries().get(2).getEntryNumber()).isEqualTo(Integer.valueOf(2));

        assertThat(cartModel.getEntries().get(0).getQuantity()).isEqualTo(Long.valueOf(2));
        assertThat(cartModel.getEntries().get(1).getQuantity()).isEqualTo(Long.valueOf(5));
        assertThat(cartModel.getEntries().get(2).getQuantity()).isEqualTo(Long.valueOf(7));

    }

    @Test
    public void testNormalizeEntryNumbersWithModelSaveException() {
        final List<AbstractOrderEntryModel> entries = createUnNormalizeEntries();
        final ModelSavingException modelSavingException = mock(ModelSavingException.class);
        final AbstractOrderEntryModel abstractOrderEntryModel = entries.get(0);
        final CartModel cartModel = new CartModel();
        cartModel.setEntries(entries);
        willThrow(modelSavingException).given(modelService).save(abstractOrderEntryModel);
        commerceCheckoutService.normalizeEntryNumbers(cartModel);
        verify(modelService).saveAll(any(List.class));

        assertThat(abstractOrderEntryModel.getEntryNumber()).isEqualTo(Integer.valueOf(0));
        assertThat(cartModel.getEntries().get(1).getEntryNumber()).isEqualTo(Integer.valueOf(1));
        assertThat(cartModel.getEntries().get(2).getEntryNumber()).isEqualTo(Integer.valueOf(2));

        assertThat(abstractOrderEntryModel.getQuantity()).isEqualTo(Long.valueOf(2));
        assertThat(cartModel.getEntries().get(1).getQuantity()).isEqualTo(Long.valueOf(5));
        assertThat(cartModel.getEntries().get(2).getQuantity()).isEqualTo(Long.valueOf(7));

    }

    @Test
    public void testVerifyNormalizeEntryNumbersWhenEmptyEntries() {
        final CartModel cartModel = new CartModel();
        cartModel.setEntries(Collections.EMPTY_LIST);
        commerceCheckoutService.normalizeEntryNumbers(cartModel);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testVerifyNormalizeEntryNumbersWhenEntriesNull() {
        final CartModel cartModel = new CartModel();
        commerceCheckoutService.normalizeEntryNumbers(cartModel);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testCapturePaymentNonIpg() {
        final CartModel cartModel = mock(CartModel.class);
        final Double amount = new Double("10");
        given(cartModel.getTotalPrice()).willReturn(amount);

        commerceCheckoutService.capturePayment(cartModel, null);

        verify(targetPaymentService).capture(any(CartModel.class),
                any(PaymentInfoModel.class),
                any(BigDecimal.class), any(CurrencyModel.class), any(PaymentCaptureType.class));
    }

    @Test
    public void testCapturePaymentIpg() {
        final CartModel cartModel = mock(CartModel.class);
        final Double amount = new Double("10");
        given(cartModel.getTotalPrice()).willReturn(amount);

        commerceCheckoutService.capturePayment(cartModel, paymentTransactionModel);

        verify(targetPaymentService).capture(any(CartModel.class),
                any(PaymentInfoModel.class),
                any(BigDecimal.class), any(CurrencyModel.class), any(PaymentCaptureType.class),
                any(PaymentTransactionModel.class));
    }

    @Test(expected = PaymentException.class)
    public void testFailedToCapturePaymentIpg() {
        final CartModel cartModel = mock(CartModel.class);
        final Double amount = new Double("10");
        given(cartModel.getTotalPrice()).willReturn(amount);
        willThrow(new PaymentException("test")).given(targetPaymentService).capture(any(CartModel.class),
                any(PaymentInfoModel.class), any(BigDecimal.class), any(CurrencyModel.class),
                any(PaymentCaptureType.class), any(PaymentTransactionModel.class));

        commerceCheckoutService.capturePayment(cartModel, paymentTransactionModel);

        verify(targetPaymentService).capture(any(CartModel.class), any(PaymentInfoModel.class), any(BigDecimal.class),
                any(CurrencyModel.class), any(PaymentCaptureType.class), any(PaymentTransactionModel.class));
        verify(paymentsInProgressService).removeInProgressPayment(cartModel);
    }

    private List<AbstractOrderEntryModel> createUnNormalizeEntries() {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final AbstractOrderEntryModel firstEntry = new AbstractOrderEntryModel();
        firstEntry.setEntryNumber(Integer.valueOf(1));
        firstEntry.setQuantity(Long.valueOf(2));
        entries.add(firstEntry);
        final AbstractOrderEntryModel secondEntry = new AbstractOrderEntryModel();
        secondEntry.setEntryNumber(Integer.valueOf(1));
        secondEntry.setQuantity(Long.valueOf(5));
        entries.add(secondEntry);
        final AbstractOrderEntryModel thirdEntry = new AbstractOrderEntryModel();
        thirdEntry.setEntryNumber(null);
        thirdEntry.setQuantity(Long.valueOf(7));
        entries.add(thirdEntry);
        return entries;
    }

    @Test
    public void testHasGiftCardUsedInPaymentWhenCardResultIsNull() {
        final List<TargetCardResult> cardResults = null;
        final boolean hasGiftCardUsed = commerceCheckoutService.hasGiftCardUsedInPayment(cardResults);
        assertThat(hasGiftCardUsed).isFalse();
    }

    @Test
    public void testHasGiftCardUsedInPaymentWhenCardResultIsEmpty() {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        final boolean hasGiftCardUsed = commerceCheckoutService.hasGiftCardUsedInPayment(cardResults);
        assertThat(hasGiftCardUsed).isFalse();
    }

    @Test
    public void testHasGiftCardUsedInPaymentWhenGiftCardsAreNull() {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        final TargetCardResult card = new TargetCardResult();
        cardResults.add(card);


        willReturn(null).given(commerceCheckoutService).getGiftCards(cardResults);

        final boolean hasGiftCardUsed = commerceCheckoutService.hasGiftCardUsedInPayment(cardResults);
        assertThat(hasGiftCardUsed).isFalse();
    }

    @Test
    public void testHasGiftCardUsedInPaymentWhenGiftCardsAreEmpty() {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        final TargetCardResult card = new TargetCardResult();
        cardResults.add(card);

        willReturn(Collections.emptyList()).given(commerceCheckoutService).getGiftCards(cardResults);

        final boolean hasGiftCardUsed = commerceCheckoutService.hasGiftCardUsedInPayment(cardResults);
        assertThat(hasGiftCardUsed).isFalse();
    }

    @Test
    public void testHasGiftCardUsedInPaymentWhenOneGiftCardAvailableInGetGiftCards() {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        final TargetCardResult card = new TargetCardResult();
        cardResults.add(card);


        final List<TargetCardResult> giftCards = new ArrayList<>();
        final TargetCardResult giftCard = new TargetCardResult();
        giftCards.add(giftCard);

        willReturn(giftCards).given(commerceCheckoutService).getGiftCards(cardResults);

        final boolean hasGiftCardUsed = commerceCheckoutService.hasGiftCardUsedInPayment(cardResults);
        assertThat(hasGiftCardUsed).isTrue();
    }

    @Test
    public void testIsOrderTotalMatchesIpgSubmittedTotalWithNullCardResult() {
        final BigDecimal orderTotal = new BigDecimal("19.90");
        willReturn(orderTotal).given(commerceCheckoutService).getAmountForPaymentCapture(cart);


        final List<TargetCardResult> cardResults = null;
        final boolean isOrderTotalMatchesWithPaidTotal = commerceCheckoutService
                .isOrderTotalMatchesIpgSubmittedTotal(cart, cardResults);

        assertThat(isOrderTotalMatchesWithPaidTotal).isFalse();
    }

    @Test
    public void testIsOrderTotalMatchesIpgSubmittedTotalWithEmptyCardResult() {
        final BigDecimal orderTotal = new BigDecimal("19.90");
        willReturn(orderTotal).given(commerceCheckoutService).getAmountForPaymentCapture(cart);

        final List<TargetCardResult> cardResults = new ArrayList<>();
        final boolean isOrderTotalMatchesWithPaidTotal = commerceCheckoutService
                .isOrderTotalMatchesIpgSubmittedTotal(cart, cardResults);

        assertThat(isOrderTotalMatchesWithPaidTotal).isFalse();
    }

    @Test
    public void testIsOrderTotalMatchesIpgSubmittedTotalWithOneCardResulHavingNullPaymentResult() {
        final BigDecimal orderTotal = new BigDecimal("19.90");
        willReturn(orderTotal).given(commerceCheckoutService).getAmountForPaymentCapture(cart);

        final List<TargetCardResult> cardResults = new ArrayList<>();
        final TargetCardResult card = new TargetCardResult();
        cardResults.add(card);

        final boolean isOrderTotalMatchesWithPaidTotal = commerceCheckoutService
                .isOrderTotalMatchesIpgSubmittedTotal(cart, cardResults);

        assertThat(isOrderTotalMatchesWithPaidTotal).isFalse();
    }

    @Test
    public void testIsOrderTotalMatchesIpgSubmittedTotalWithOneCardResulHavingPaymentResultOfNullAmount() {
        final BigDecimal orderTotal = new BigDecimal("19.90");
        willReturn(orderTotal).given(commerceCheckoutService).getAmountForPaymentCapture(cart);

        final List<TargetCardResult> cardResults = new ArrayList<>();
        final TargetCardResult card = new TargetCardResult();

        final TargetCardPaymentResult targetCardPaymentResult = new TargetCardPaymentResult();
        card.setTargetCardPaymentResult(targetCardPaymentResult);
        cardResults.add(card);

        final boolean isOrderTotalMatchesWithPaidTotal = commerceCheckoutService
                .isOrderTotalMatchesIpgSubmittedTotal(cart, cardResults);

        assertThat(isOrderTotalMatchesWithPaidTotal).isFalse();
    }

    @Test
    public void testIsOrderTotalMatchesIpgSubmittedTotalWithOneCardResulHavingPaymentResultOfLowerAmount() {
        final BigDecimal orderTotal = new BigDecimal("19.90");
        willReturn(orderTotal).given(commerceCheckoutService).getAmountForPaymentCapture(cart);

        final List<TargetCardResult> cardResults = new ArrayList<>();
        final TargetCardResult card = new TargetCardResult();

        final TargetCardPaymentResult targetCardPaymentResult = new TargetCardPaymentResult();
        targetCardPaymentResult.setAmount(new BigDecimal("10.00"));
        card.setTargetCardPaymentResult(targetCardPaymentResult);
        cardResults.add(card);

        final boolean isOrderTotalMatchesWithPaidTotal = commerceCheckoutService
                .isOrderTotalMatchesIpgSubmittedTotal(cart, cardResults);

        assertThat(isOrderTotalMatchesWithPaidTotal).isFalse();
    }

    @Test
    public void testIsOrderTotalMatchesIpgSubmittedTotalWithOneCardResulHavingPaymentResultOfHigherAmount() {
        final BigDecimal orderTotal = new BigDecimal("19.90");
        willReturn(orderTotal).given(commerceCheckoutService).getAmountForPaymentCapture(cart);

        final List<TargetCardResult> cardResults = new ArrayList<>();
        final TargetCardResult card = new TargetCardResult();

        final TargetCardPaymentResult targetCardPaymentResult = new TargetCardPaymentResult();
        targetCardPaymentResult.setAmount(new BigDecimal("20.00"));
        card.setTargetCardPaymentResult(targetCardPaymentResult);
        cardResults.add(card);

        final boolean isOrderTotalMatchesWithPaidTotal = commerceCheckoutService
                .isOrderTotalMatchesIpgSubmittedTotal(cart, cardResults);

        assertThat(isOrderTotalMatchesWithPaidTotal).isFalse();
    }

    @Test
    public void testIsOrderTotalMatchesIpgSubmittedTotalWithOneCardResulHavingPaymentResultOfEqualAmount() {
        final BigDecimal orderTotal = new BigDecimal("19.90");
        willReturn(orderTotal).given(commerceCheckoutService).getAmountForPaymentCapture(cart);

        final List<TargetCardResult> cardResults = new ArrayList<>();
        final TargetCardResult card = new TargetCardResult();

        final TargetCardPaymentResult targetCardPaymentResult = new TargetCardPaymentResult();
        targetCardPaymentResult.setAmount(new BigDecimal("19.90"));
        card.setTargetCardPaymentResult(targetCardPaymentResult);
        cardResults.add(card);

        final boolean isOrderTotalMatchesWithPaidTotal = commerceCheckoutService
                .isOrderTotalMatchesIpgSubmittedTotal(cart, cardResults);

        assertThat(isOrderTotalMatchesWithPaidTotal).isTrue();
    }

    @Test
    public void testIsOrderTotalMatchesIpgSubmittedTotalWithTwoCardResulHavingPaymentResultOfLowerAmount() {
        final BigDecimal orderTotal = new BigDecimal("19.90");
        willReturn(orderTotal).given(commerceCheckoutService).getAmountForPaymentCapture(cart);

        final List<TargetCardResult> cardResults = new ArrayList<>();

        final TargetCardResult card1 = new TargetCardResult();
        final TargetCardPaymentResult targetCardPaymentResult1 = new TargetCardPaymentResult();
        targetCardPaymentResult1.setAmount(new BigDecimal("8.90"));
        card1.setTargetCardPaymentResult(targetCardPaymentResult1);
        cardResults.add(card1);

        final TargetCardResult card2 = new TargetCardResult();
        final TargetCardPaymentResult targetCardPaymentResult2 = new TargetCardPaymentResult();
        targetCardPaymentResult2.setAmount(new BigDecimal("8.90"));
        card2.setTargetCardPaymentResult(targetCardPaymentResult2);
        cardResults.add(card2);

        final boolean isOrderTotalMatchesWithPaidTotal = commerceCheckoutService
                .isOrderTotalMatchesIpgSubmittedTotal(cart, cardResults);

        assertThat(isOrderTotalMatchesWithPaidTotal).isFalse();
    }

    @Test
    public void testIsOrderTotalMatchesIpgSubmittedTotalWithTwoCardResulHavingPaymentResultOfHigherAmount() {
        final BigDecimal orderTotal = new BigDecimal("19.90");
        willReturn(orderTotal).given(commerceCheckoutService).getAmountForPaymentCapture(cart);

        final List<TargetCardResult> cardResults = new ArrayList<>();

        final TargetCardResult card1 = new TargetCardResult();
        final TargetCardPaymentResult targetCardPaymentResult1 = new TargetCardPaymentResult();
        targetCardPaymentResult1.setAmount(new BigDecimal("11.90"));
        card1.setTargetCardPaymentResult(targetCardPaymentResult1);
        cardResults.add(card1);

        final TargetCardResult card2 = new TargetCardResult();
        final TargetCardPaymentResult targetCardPaymentResult2 = new TargetCardPaymentResult();
        targetCardPaymentResult2.setAmount(new BigDecimal("8.90"));
        card2.setTargetCardPaymentResult(targetCardPaymentResult2);
        cardResults.add(card2);

        final boolean isOrderTotalMatchesWithPaidTotal = commerceCheckoutService
                .isOrderTotalMatchesIpgSubmittedTotal(cart, cardResults);

        assertThat(isOrderTotalMatchesWithPaidTotal).isFalse();
    }

    @Test
    public void testIsOrderTotalMatchesIpgSubmittedTotalWithTwoCardResulHavingPaymentResultOfEqualAmount() {
        final BigDecimal orderTotal = new BigDecimal("19.90");
        willReturn(orderTotal).given(commerceCheckoutService).getAmountForPaymentCapture(cart);

        final List<TargetCardResult> cardResults = new ArrayList<>();

        final TargetCardResult card1 = new TargetCardResult();
        final TargetCardPaymentResult targetCardPaymentResult1 = new TargetCardPaymentResult();
        targetCardPaymentResult1.setAmount(new BigDecimal("11.90"));
        card1.setTargetCardPaymentResult(targetCardPaymentResult1);
        cardResults.add(card1);

        final TargetCardResult card2 = new TargetCardResult();
        final TargetCardPaymentResult targetCardPaymentResult2 = new TargetCardPaymentResult();
        targetCardPaymentResult2.setAmount(new BigDecimal("8.00"));
        card2.setTargetCardPaymentResult(targetCardPaymentResult2);
        cardResults.add(card2);

        final boolean isOrderTotalMatchesWithPaidTotal = commerceCheckoutService
                .isOrderTotalMatchesIpgSubmittedTotal(cart, cardResults);

        assertThat(isOrderTotalMatchesWithPaidTotal).isTrue();
    }

    @Test
    public void testIsSplitPaymentWithNullCardResults() {
        final List<TargetCardResult> cardResults = null;
        final boolean isSplitPayment = commerceCheckoutService.isSplitPayment(cardResults);
        assertThat(isSplitPayment).isFalse();
    }

    @Test
    public void testIsSplitPaymentWithEmptyCardResults() {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        final boolean isSplitPayment = commerceCheckoutService.isSplitPayment(cardResults);
        assertThat(isSplitPayment).isFalse();
    }

    @Test
    public void testIsSplitPaymentWithOneCardInCardResults() {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        final TargetCardResult card = new TargetCardResult();
        cardResults.add(card);

        final boolean isSplitPayment = commerceCheckoutService.isSplitPayment(cardResults);
        assertThat(isSplitPayment).isFalse();

    }

    @Test
    public void testIsSplitPaymentWithTwoCardsInCardResults() {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        final TargetCardResult card1 = new TargetCardResult();
        cardResults.add(card1);

        final TargetCardResult card2 = new TargetCardResult();
        cardResults.add(card2);

        final boolean isSplitPayment = commerceCheckoutService.isSplitPayment(cardResults);
        assertThat(isSplitPayment).isTrue();
    }

    @Test
    public void testShouldPaymentBeReversedWhenSplitPaymentAndNoGiftCardInPaymentWhenOrderTotalMatches() {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        willReturn(Boolean.TRUE).given(commerceCheckoutService).isSplitPayment(cardResults);
        willReturn(Boolean.FALSE).given(commerceCheckoutService).hasGiftCardUsedInPayment(cardResults);
        willReturn(Boolean.TRUE).given(commerceCheckoutService).isOrderTotalMatchesIpgSubmittedTotal(cart, cardResults);

        final boolean shouldPaymentBeReversed = commerceCheckoutService.shouldPaymentBeReversed(cart, cardResults);
        assertThat(shouldPaymentBeReversed).isFalse();
    }

    @Test
    public void testShouldPaymentBeReversedWhenNotSplitPaymentAndGiftCardInPaymentWhenOrderTotalMatches() {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        willReturn(Boolean.FALSE).given(commerceCheckoutService).isSplitPayment(cardResults);
        willReturn(Boolean.TRUE).given(commerceCheckoutService).hasGiftCardUsedInPayment(cardResults);
        willReturn(Boolean.TRUE).given(commerceCheckoutService).isOrderTotalMatchesIpgSubmittedTotal(cart, cardResults);

        final boolean shouldPaymentBeReversed = commerceCheckoutService.shouldPaymentBeReversed(cart, cardResults);
        assertThat(shouldPaymentBeReversed).isFalse();
    }

    @Test
    public void testShouldPaymentBeReversedWhenSplitPaymentAndGiftCardInPaymentWhenOrderTotalMatches() {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        willReturn(Boolean.TRUE).given(commerceCheckoutService).isSplitPayment(cardResults);
        willReturn(Boolean.TRUE).given(commerceCheckoutService).hasGiftCardUsedInPayment(cardResults);
        willReturn(Boolean.TRUE).given(commerceCheckoutService).isOrderTotalMatchesIpgSubmittedTotal(cart, cardResults);

        final boolean shouldPaymentBeReversed = commerceCheckoutService.shouldPaymentBeReversed(cart, cardResults);
        assertThat(shouldPaymentBeReversed).isFalse();
    }

    @Test
    public void testShouldPaymentBeReversedWhenSplitPaymentAndNotGiftCardInPaymentWhenOrderTotalDontMatch() {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        willReturn(Boolean.TRUE).given(commerceCheckoutService).isSplitPayment(cardResults);
        willReturn(Boolean.FALSE).given(commerceCheckoutService).hasGiftCardUsedInPayment(cardResults);
        willReturn(Boolean.FALSE).given(commerceCheckoutService).isOrderTotalMatchesIpgSubmittedTotal(cart,
                cardResults);

        final boolean shouldPaymentBeReversed = commerceCheckoutService.shouldPaymentBeReversed(cart, cardResults);
        assertThat(shouldPaymentBeReversed).isTrue();
    }

    @Test
    public void testShouldPaymentBeReversedWhenNotSplitPaymentAndGiftCardInPaymentWhenOrderTotalDontMatch() {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        willReturn(Boolean.FALSE).given(commerceCheckoutService).isSplitPayment(cardResults);
        willReturn(Boolean.TRUE).given(commerceCheckoutService).hasGiftCardUsedInPayment(cardResults);
        willReturn(Boolean.FALSE).given(commerceCheckoutService).isOrderTotalMatchesIpgSubmittedTotal(cart,
                cardResults);

        final boolean shouldPaymentBeReversed = commerceCheckoutService.shouldPaymentBeReversed(cart, cardResults);
        assertThat(shouldPaymentBeReversed).isTrue();
    }

    @Test
    public void testShouldPaymentBeReversedWhenSplitPaymentAndGiftCardInPaymentWhenOrderTotalDontMatch() {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        willReturn(Boolean.TRUE).given(commerceCheckoutService).isSplitPayment(cardResults);
        willReturn(Boolean.TRUE).given(commerceCheckoutService).hasGiftCardUsedInPayment(cardResults);
        willReturn(Boolean.FALSE).given(commerceCheckoutService).isOrderTotalMatchesIpgSubmittedTotal(cart,
                cardResults);

        final boolean shouldPaymentBeReversed = commerceCheckoutService.shouldPaymentBeReversed(cart, cardResults);
        assertThat(shouldPaymentBeReversed).isTrue();
    }

    @Test
    public void testShouldPaymentBeReversedWhenNotSplitPaymentAndNoGiftCardInPayment() {
        final List<TargetCardResult> cardResults = new ArrayList<>();
        willReturn(Boolean.FALSE).given(commerceCheckoutService).isSplitPayment(cardResults);
        willReturn(Boolean.FALSE).given(commerceCheckoutService).hasGiftCardUsedInPayment(cardResults);
        final boolean shouldPaymentBeReversed = commerceCheckoutService.shouldPaymentBeReversed(cart, cardResults);

        assertThat(shouldPaymentBeReversed).isFalse();
    }

    @Test
    public void testFillCart4ClickAndCollectWithoutRecalculate() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        given(targetPointOfServiceService.getPOSByStoreNumber(Integer.valueOf(1)))
                .willReturn(pointOfService);
        given(pointOfService.getAddress()).willReturn(addressModel);

        final AddressModel clonedAddress = mock(AddressModel.class);
        final TitleModel title = mock(TitleModel.class);
        given(addressService.cloneAddressForOwner(addressModel, cart))
                .willReturn(clonedAddress);

        commerceCheckoutService.fillCart4ClickAndCollectWithoutRecalculate(cart, Integer.valueOf(1), title, "first",
                "last",
                "12345678", deliveryMode);

        verify(clonedAddress).setTitle(title);
        verify(clonedAddress).setFirstname("first");
        verify(clonedAddress).setLastname("last");
        verify(clonedAddress).setPhone1("12345678");
        verify(cart).setDeliveryMode(deliveryMode);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemoveFlybuysNumberWithNullCartModel() {
        commerceCheckoutService.removeFlybuysNumber(null);
    }

    @Test
    public void testRemoveFlybuysNumber() {
        final CartModel mockCart = mock(CartModel.class);
        commerceCheckoutService.removeFlybuysNumber(mockCart);
        final InOrder verifyOrder = inOrder(mockCart, modelService);
        verifyOrder.verify(mockCart).setFlyBuysCode(null);
        verifyOrder.verify(modelService).save(mockCart);
        verifyOrder.verify(modelService).refresh(mockCart);
    }

    @Test
    public void testAmountForCapturePreOrder() {
        willReturn(Boolean.TRUE).given(cart).getContainsPreOrderItems();
        given(cart.getPreOrderDepositAmount()).willReturn(Double.valueOf(10d));

        final BigDecimal amountForPaymentCapture = commerceCheckoutService.getAmountForPaymentCapture(cart);
        assertThat(amountForPaymentCapture).isEqualTo(new BigDecimal("10.0"));
    }

    @Test
    public void testAmountForCapturePreOrderDepositAmountNull() {
        willReturn(Boolean.TRUE).given(cart).getContainsPreOrderItems();
        given(cart.getPreOrderDepositAmount()).willReturn(null);

        final BigDecimal amountForPaymentCapture = commerceCheckoutService.getAmountForPaymentCapture(cart);
        assertThat(amountForPaymentCapture).isNull();
    }

    @Test
    public void testIsCaptureOrderSuccessfullPreOrder() {
        given(cart.getPreOrderDepositAmount()).willReturn(Double.valueOf(12.34d));
        willReturn(Boolean.TRUE).given(cart).getContainsPreOrderItems();
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(cart)).willReturn(Double.valueOf(12.34d));

        final boolean result = commerceCheckoutService.isCaptureOrderSuccessful(cart);
        assertThat(result).isTrue();
    }

    @Test
    public void testIsCaptureOrderFailedPreOrder() {
        given(cart.getPreOrderDepositAmount()).willReturn(Double.valueOf(12.34d));
        willReturn(Boolean.TRUE).given(cart).getContainsPreOrderItems();
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(cart)).willReturn(Double.valueOf(0));

        final boolean result = commerceCheckoutService.isCaptureOrderSuccessful(cart);
        assertThat(result).isFalse();
    }

    @Test
    public void testFluentCreateOrderSuccess() throws InvalidCartException {

        final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
        checkoutParameter.setCart(cart);
        checkoutParameter.setSalesApplication(SalesApplication.WEB);
        final BaseSiteService baseSiteService = mock(BaseSiteService.class);
        final BaseStoreService baseStoreService = mock(BaseStoreService.class);

        commerceCheckoutService.setOrderService(orderService);
        commerceCheckoutService.setBaseSiteService(baseSiteService);
        commerceCheckoutService.setBaseStoreService(baseStoreService);

        given(paymentInfoModel.getBillingAddress()).willReturn(addressModel);
        given(order.getPaymentInfo()).willReturn(paymentInfoModel);
        given(orderService.createOrderFromCart(cart)).willReturn(order);
        willReturn(Boolean.TRUE).given(cart).getCalculated();
        given(cart.getPaymentInfo()).willReturn(paymentInfoModel);
        given(cart.getUser()).willReturn(customer);
        willReturn(Boolean.FALSE).given(order).getCalculated();
        final AddressModel alonedAddress = mock(AddressModel.class);
        given(modelService.clone(addressModel)).willReturn(alonedAddress);

        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)))
                .willReturn(Boolean.TRUE);
        given(fluentOrderService.createOrder(order)).willReturn(String.valueOf(1));

        final CommerceOrderResult commerceOrderResult = commerceCheckoutService.placeOrder(checkoutParameter);
        assertThat(commerceOrderResult).isNotNull();
        verify(modelService, times(2)).save(order);
        verify(modelService).refresh(order);
        verify(orderService).submitOrder(order);

    }

    @Test
    public void testFluentCreateOrderException() throws InvalidCartException {
        final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
        checkoutParameter.setCart(cart);
        checkoutParameter.setSalesApplication(SalesApplication.WEB);
        final BaseSiteService baseSiteService = mock(BaseSiteService.class);
        final BaseStoreService baseStoreService = mock(BaseStoreService.class);

        commerceCheckoutService.setOrderService(orderService);
        commerceCheckoutService.setBaseSiteService(baseSiteService);
        commerceCheckoutService.setBaseStoreService(baseStoreService);

        given(paymentInfoModel.getBillingAddress()).willReturn(addressModel);
        given(order.getPaymentInfo()).willReturn(paymentInfoModel);
        given(orderService.createOrderFromCart(cart)).willReturn(order);
        willReturn(Boolean.TRUE).given(cart).getCalculated();
        given(cart.getPaymentInfo()).willReturn(paymentInfoModel);
        given(cart.getUser()).willReturn(customer);
        willReturn(Boolean.FALSE).given(order).getCalculated();
        final AddressModel alonedAddress = mock(AddressModel.class);
        given(modelService.clone(addressModel)).willReturn(alonedAddress);

        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)))
                .willReturn(Boolean.TRUE);
        given(fluentOrderService.createOrder(order)).willThrow(new FluentOrderException());

        final CommerceOrderResult commerceOrderResult = commerceCheckoutService.placeOrder(checkoutParameter);
        assertThat(commerceOrderResult).isNotNull();
        verify(modelService).save(order);
        verify(modelService).refresh(order);
        verify(orderService).submitOrder(order);

    }

    @Test
    public void testReserveStockForCart() throws InsufficientStockLevelException {
        commerceCheckoutService.reserveStockForCart(cart);
        verify(fluentStockService, never()).lookupPlaceOrderAts(anyList(), anyString());
    }

    @Test
    public void testReserveStockForCartWithFluent() throws InsufficientStockLevelException {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService).isFeatureEnabled(
                TgtCoreConstants.FeatureSwitch.FLUENT);
        final Map<String, Integer> stockData = mock(Map.class);
        given(fluentStockService.lookupPlaceOrderAts(Arrays.asList("P12345"), "CC")).willReturn(stockData);
        when(stockData.get(anyString())).thenReturn(Integer.valueOf(20));

        commerceCheckoutService.reserveStockForCart(cart);
        verify(targetStockService, never()).reserve(any(ProductModel.class), anyInt(), anyString());
    }

    @Test(expected = InsufficientStockLevelException.class)
    public void testReserveStockForCartWithFluentInsufficientStock() throws InsufficientStockLevelException {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService).isFeatureEnabled(
                TgtCoreConstants.FeatureSwitch.FLUENT);
        final Map<String, Integer> stockData = mock(Map.class);
        when(stockData.get(anyString())).thenReturn(Integer.valueOf(5));
        given(fluentStockService.lookupPlaceOrderAts(Arrays.asList("P12345"), "CC")).willReturn(stockData);

        commerceCheckoutService.reserveStockForCart(cart);
    }

    @Test(expected = FluentOrderException.class)
    public void testReserveStockForCartWithFluentException() throws InsufficientStockLevelException {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService).isFeatureEnabled(
                TgtCoreConstants.FeatureSwitch.FLUENT);
        final Map<String, Integer> stockData = mock(Map.class);
        when(stockData.get(anyString())).thenReturn(Integer.valueOf(5));
        willThrow(new FluentOrderException()).given(fluentStockService).lookupPlaceOrderAts(Arrays.asList("P12345"),
                "CC");
        commerceCheckoutService.reserveStockForCart(cart);
    }

    @Test
    public void testGetFluentDeliveryMode() {
        given(cart.getDeliveryMode().getCode()).willReturn("digital-gift-card");
        final String deliveryMode = commerceCheckoutService.getFluentDeliveryMode(cart);
        assertThat(deliveryMode).isEqualTo("HD");
    }

    @Test
    public void testGetFluentDeliveryModeNull() {
        given(cart.getDeliveryMode()).willReturn(null);
        final String deliveryMode = commerceCheckoutService.getFluentDeliveryMode(cart);
        assertThat(deliveryMode).isNull();
    }

    @Test
    public void testGetFluentDeliveryModeWithPreOrderItems() {
        given(cart.getContainsPreOrderItems()).willReturn(Boolean.TRUE);
        given(cart.getDeliveryMode().getCode()).willReturn(null);
        final String deliveryMode = commerceCheckoutService.getFluentDeliveryMode(cart);
        assertThat(deliveryMode).isEqualTo(TgtCoreConstants.DELIVERY_TYPE.PO);
    }

    @Test
    public void testGetFluentDeliveryModeExpressDelivery() {
        given(cart.getDeliveryMode().getCode()).willReturn("express-delivery");
        final String deliveryMode = commerceCheckoutService.getFluentDeliveryMode(cart);
        assertThat(deliveryMode).isEqualTo(TgtCoreConstants.DELIVERY_TYPE.ED);
    }
}

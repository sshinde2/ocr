/**
 * 
 */
package au.com.target.tgtcore.order.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.delivery.TargetDeliveryService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetDeliveryAddressStrategyImplTest {

    @InjectMocks
    private final TargetDeliveryAddressStrategyImpl deliveryStrategy = new TargetDeliveryAddressStrategyImpl();

    @Mock
    private TargetDeliveryService targetDeliveryService;

    @Mock
    private CartModel cartModel;

    @Mock
    private AddressModel addressModel;

    @Mock
    private DeliveryModeModel deliveryMode;

    @Before
    public void setUp() {
        given(cartModel.getDeliveryMode()).willReturn(deliveryMode);
    }

    @Test
    public void testIsInValidDeliveryAddressWithNullAddress() {
        assertThat(deliveryStrategy.isValidDeliveryAddress(cartModel, null)).isTrue();
    }

    @Test
    public void testIsValidDeliveryAddressWithValidAddress() {
        given(Boolean.valueOf(targetDeliveryService.isDeliveryModePostCodeCombinationValid(deliveryMode,
                addressModel, cartModel))).willReturn(Boolean.TRUE);
        assertThat(deliveryStrategy.isValidDeliveryAddress(cartModel, addressModel)).isTrue();
    }

    @Test
    public void testIsValidDeliveryAddressWithInValidAddress() {
        given(Boolean.valueOf(targetDeliveryService.isDeliveryModePostCodeCombinationValid(deliveryMode,
                addressModel, cartModel))).willReturn(Boolean.valueOf(false));
        assertThat(deliveryStrategy.isValidDeliveryAddress(cartModel, addressModel)).isFalse();
    }
}

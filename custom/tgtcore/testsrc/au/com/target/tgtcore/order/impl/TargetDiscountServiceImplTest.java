package au.com.target.tgtcore.order.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.order.daos.DiscountDao;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.jalo.ProductPromotion;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.voucher.VoucherService;
import de.hybris.platform.voucher.model.PromotionVoucherModel;
import de.hybris.platform.voucher.model.VoucherModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.jalo.TMDiscountPromotion;
import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.order.data.AppliedFlybuysDiscountData;
import au.com.target.tgtcore.order.data.AppliedVoucherData;
import au.com.target.tgtpayment.strategy.FindVoucherValueStrategy;


/**
 * Unit test for {@link TargetDiscountServiceImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetDiscountServiceImplTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @Mock
    private PromotionsService promotionsService;

    @Mock
    private VoucherService voucherService;

    @Mock
    private FindVoucherValueStrategy findVoucherValueStrategy;

    @InjectMocks
    private final TargetDiscountServiceImpl targetDiscountServiceImpl = new TargetDiscountServiceImpl();

    @Mock
    private ModelService mockModelService;

    @Mock
    private DiscountModel mockDiscountModel;

    @Mock
    private OrderModel orderModel;

    @Mock
    private DiscountDao mockDiscountDao;

    @Mock
    private SessionService sessionService;


    // Voucher test data
    private Double voucherValue1;
    private String voucherCode1;
    private DiscountModel voucher1;
    private Double voucherValue2;
    private Double voucherPercentValue2;
    private String voucherCode2;
    private DiscountModel voucher2;

    // Flybuys test data
    private DiscountModel flybuysDiscountModel;
    private Integer points;
    private String redeemCode;
    private String confirmationCode;

    @Before
    public void setUp() {

        given(mockModelService.create(DiscountModel.class)).willReturn(mockDiscountModel);

        // Setup vouchers

        // A dollar off voucher
        voucherValue1 = Double.valueOf(5.5);
        voucherCode1 = "ABC1";
        voucher1 = Mockito.mock(PromotionVoucherModel.class);
        given(findVoucherValueStrategy.getDiscountValueAppliedValue((VoucherModel)voucher1, orderModel))
                .willReturn(voucherValue1);
        given(voucher1.getCode()).willReturn(voucherCode1);
        given(voucher1.getAbsolute()).willReturn(Boolean.TRUE);
        given(voucher1.getValue()).willReturn(voucherValue1);

        // A percent off voucher
        voucherValue2 = Double.valueOf(3.5);
        voucherCode2 = "ABC2";
        voucherPercentValue2 = Double.valueOf(15);
        voucher2 = Mockito.mock(PromotionVoucherModel.class);
        given(findVoucherValueStrategy.getDiscountValueAppliedValue((VoucherModel)voucher2, orderModel))
                .willReturn(voucherValue2);
        given(voucher2.getCode()).willReturn(voucherCode2);
        given(voucher2.getAbsolute()).willReturn(Boolean.FALSE);
        given(voucher2.getValue()).willReturn(voucherPercentValue2);

        // setup flybuys voucher
        flybuysDiscountModel = Mockito.mock(FlybuysDiscountModel.class);
        points = Integer.valueOf(1000);
        redeemCode = "testRedeem";
        confirmationCode = "testConfirmation";
        given(flybuysDiscountModel.getValue()).willReturn(voucherValue1);
        given(((FlybuysDiscountModel)flybuysDiscountModel).getFlybuysCardNumber()).willReturn(voucherCode1);
        given(((FlybuysDiscountModel)flybuysDiscountModel).getRedeemCode()).willReturn(redeemCode);
        given(((FlybuysDiscountModel)flybuysDiscountModel).getConfirmationCode()).willReturn(confirmationCode);
        given(orderModel.getFlybuysPointsConsumed()).willReturn(points);
    }

    @Test
    public void testCreateGlobalDiscountForNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Ordermodel cannot not be null");

        targetDiscountServiceImpl.createGlobalDiscountForOrder(null, 0d, null);
    }

    @Test
    public void testCreateGlobalDiscountForZeroDiscountValue() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Discount value cannot be less than or equal to zero.");

        final OrderModel mockOrder = Mockito.mock(OrderModel.class);

        targetDiscountServiceImpl.createGlobalDiscountForOrder(mockOrder, 0d, null);
    }

    @Test
    public void testCreateGlobalDiscountForSubZeroDiscountValue() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Discount value cannot be less than or equal to zero.");

        final OrderModel mockOrder = Mockito.mock(OrderModel.class);

        targetDiscountServiceImpl.createGlobalDiscountForOrder(mockOrder, -1.5d, null);
    }

    @Test
    public void testCreateGlobalDiscountForOrderWithoutDiscounts() {

        final CurrencyModel mockCurrency = Mockito.mock(CurrencyModel.class);

        final OrderModel mockOrder = Mockito.mock(OrderModel.class);
        given(mockOrder.getCode()).willReturn("12345678");
        given(mockOrder.getCurrency()).willReturn(mockCurrency);
        given(mockOrder.getDiscounts()).willReturn(Collections.EMPTY_LIST);

        final ArgumentCaptor<Collection> argumentCaptor = ArgumentCaptor.forClass(Collection.class);

        targetDiscountServiceImpl.createGlobalDiscountForOrder(mockOrder, 1d, null);

        verify(mockModelService).create(DiscountModel.class);
        verify(mockDiscountModel).setCode("12345678-GD-1");
        verify(mockDiscountModel).setGlobal(Boolean.TRUE);
        verify(mockDiscountModel).setName(null);
        verify(mockDiscountModel).setCurrency(mockCurrency);
        verify(mockDiscountModel).setValue(Double.valueOf(1d));
        verify(mockDiscountModel).setOrders(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue().size()).isEqualTo(1);
        assertThat(argumentCaptor.getValue().contains(mockOrder)).isTrue();

        verify(mockModelService).save(mockDiscountModel);
        verify(mockModelService).refresh(mockOrder);
    }

    @Test
    public void testCreateGlobalDiscountForOrderWithDiscounts() {

        final CurrencyModel mockCurrency = Mockito.mock(CurrencyModel.class);
        final DiscountModel mockDiscount1 = Mockito.mock(DiscountModel.class);
        final DiscountModel mockDiscount2 = Mockito.mock(DiscountModel.class);

        final OrderModel mockOrder = Mockito.mock(OrderModel.class);
        given(mockOrder.getCode()).willReturn("12345678");
        given(mockOrder.getCurrency()).willReturn(mockCurrency);

        final List<DiscountModel> discounts = new ArrayList<>();
        discounts.add(mockDiscount1);
        discounts.add(mockDiscount2);
        given(mockOrder.getDiscounts()).willReturn(discounts);

        final ArgumentCaptor<Collection> argumentCaptor = ArgumentCaptor.forClass(Collection.class);

        targetDiscountServiceImpl.createGlobalDiscountForOrder(mockOrder, 1d, null);

        verify(mockModelService).create(DiscountModel.class);
        verify(mockDiscountModel).setCode("12345678-GD-3");
        verify(mockDiscountModel).setGlobal(Boolean.TRUE);
        verify(mockDiscountModel).setName(null);
        verify(mockDiscountModel).setCurrency(mockCurrency);
        verify(mockDiscountModel).setValue(Double.valueOf(1d));
        verify(mockDiscountModel).setOrders(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue().size()).isEqualTo(1);
        assertThat(argumentCaptor.getValue().contains(mockOrder)).isTrue();

        verify(mockModelService).save(mockDiscountModel);
        verify(mockModelService).refresh(mockOrder);
    }

    @Test
    public void testGetVoucherDiscountGivenNullVouchers() {

        given(voucherService.getAppliedVouchers(orderModel)).willReturn(null);

        final BigDecimal total = targetDiscountServiceImpl.getVoucherDiscount(orderModel);
        assertThat(Double.valueOf(total.doubleValue())).isEqualTo(Double.valueOf(0));
    }

    @Test
    public void testGetVoucherDiscountGivenNoVouchers() {

        given(voucherService.getAppliedVouchers(orderModel)).willReturn(Collections.EMPTY_LIST);

        final BigDecimal total = targetDiscountServiceImpl.getVoucherDiscount(orderModel);
        assertThat(Double.valueOf(total.doubleValue())).isEqualTo(Double.valueOf(0));
    }

    @Test
    public void testGetVoucherDiscountGivenNonVoucherDiscount() {

        final DiscountModel discount = Mockito.mock(DiscountModel.class);
        given(voucherService.getAppliedVouchers(orderModel)).willReturn(Collections.singletonList(discount));

        final BigDecimal total = targetDiscountServiceImpl.getVoucherDiscount(orderModel);
        assertThat(Double.valueOf(total.doubleValue())).isEqualTo(Double.valueOf(0));

    }

    @Test
    public void testGetVoucherDiscountGivenOneVoucher() {

        given(voucherService.getAppliedVouchers(orderModel)).willReturn(Collections.singletonList(voucher1));

        final BigDecimal total = targetDiscountServiceImpl.getVoucherDiscount(orderModel);
        assertThat(Double.valueOf(total.doubleValue())).isEqualTo(voucherValue1);
    }

    @Test
    public void testGetVoucherDiscountGivenTwoVouchers() {

        final Collection<DiscountModel> listVouchers = new ArrayList<>();
        listVouchers.add(voucher1);
        listVouchers.add(voucher2);

        given(voucherService.getAppliedVouchers(orderModel)).willReturn(listVouchers);

        final BigDecimal total = targetDiscountServiceImpl.getVoucherDiscount(orderModel);
        assertThat(Double.valueOf(total.doubleValue()))
                .isEqualTo(voucherValue1.doubleValue() + voucherValue2.doubleValue());

    }

    @Test
    public void testGetAppliedVouchersAndFlybuysDataGivenNoVouchersAndFlybuys() {

        given(voucherService.getAppliedVouchers(orderModel)).willReturn(Collections.EMPTY_LIST);

        final Collection<AppliedVoucherData> data = targetDiscountServiceImpl
                .getAppliedVouchersAndFlybuysData(orderModel);

        assertThat(data).isEmpty();
    }

    @Test
    public void testGetAppliedVouchersAndFlybuysDataGivenOneVoucher() {

        given(voucherService.getAppliedVouchers(orderModel)).willReturn(Collections.singletonList(voucher1));

        final Collection<AppliedVoucherData> dataList = targetDiscountServiceImpl
                .getAppliedVouchersAndFlybuysData(orderModel);

        assertThat(dataList.size()).isEqualTo(1);

        final AppliedVoucherData data1 = dataList.iterator().next();
        checkDollarVoucher(data1);
    }

    @Test
    public void testGetAppliedVouchersAndFlybuysDataGivenOneFlybuy() {

        given(voucherService.getAppliedVouchers(orderModel)).willReturn(
                Collections.singletonList(flybuysDiscountModel));

        final Collection<AppliedVoucherData> dataList = targetDiscountServiceImpl
                .getAppliedVouchersAndFlybuysData(orderModel);

        assertThat(dataList.size()).isEqualTo(1);

        final AppliedVoucherData data = dataList.iterator().next();

        assertThat(data).isInstanceOf(AppliedFlybuysDiscountData.class);

        checkFlybuy((AppliedFlybuysDiscountData)data);
    }

    @Test
    public void testGetAppliedVouchersAndFlybuysDataGivenTwoVouchers() {

        final Collection<DiscountModel> listVouchers = new ArrayList<>();
        listVouchers.add(voucher1);
        listVouchers.add(voucher2);
        given(voucherService.getAppliedVouchers(orderModel)).willReturn(listVouchers);

        final Collection<AppliedVoucherData> dataList = targetDiscountServiceImpl
                .getAppliedVouchersAndFlybuysData(orderModel);
        assertThat(dataList.size()).isEqualTo(2);

        final Iterator<AppliedVoucherData> it = dataList.iterator();
        final AppliedVoucherData data1 = it.next();
        checkDollarVoucher(data1);

        final AppliedVoucherData data2 = it.next();
        checkPercentVoucher(data2);
    }

    @Test
    public void testGetTotalDiscount() {
        mockTMDDiscount();
        targetDiscountServiceImpl.calculateTotalTMDDiscount(orderModel);
        Mockito.verify(orderModel).setTotalTMDDiscount(Double.valueOf(1D));
    }

    @Test
    public void testIsTMDiscountAppliedToOrderForAppliedPromotion() {
        given(orderModel.getTotalTMDDiscount()).willReturn(Double.valueOf(2D));
        assertThat(targetDiscountServiceImpl.isTMDiscountAppliedToOrder(orderModel)).isTrue();

    }

    @Test
    public void testIsTMDiscountAppliedToOrderForNoAppliedPromotion() {
        given(orderModel.getTotalTMDDiscount()).willReturn(Double.valueOf(0));
        assertThat(targetDiscountServiceImpl.isTMDiscountAppliedToOrder(orderModel)).isFalse();
    }

    private void mockTMDDiscount() {
        final SessionContext sessioncontext = mock(SessionContext.class);
        final AbstractOrder abstractOrder = mock(AbstractOrder.class);
        final PromotionResult promotionResultNotTMD = Mockito.mock(PromotionResult.class);
        final PromotionResult promotionResultTMD = Mockito.mock(PromotionResult.class);
        final PromotionOrderResults promotionResults = new PromotionOrderResults(sessioncontext, abstractOrder,
                Arrays.asList(promotionResultNotTMD, promotionResultTMD), 2.1);


        final ProductPromotion productPromotion = Mockito.mock(ProductPromotion.class);
        final JaloSession jaloSession = mock(JaloSession.class);
        given(promotionResultNotTMD.getSession()).willReturn(jaloSession);
        given(promotionResultNotTMD.getPromotion()).willReturn(productPromotion);
        given(Boolean.valueOf(promotionResultNotTMD.isApplied())).willReturn(Boolean.TRUE);
        given(Double.valueOf(promotionResultNotTMD.getTotalDiscount())).willReturn(Double.valueOf(2D));

        given(promotionResultTMD.getSession()).willReturn(jaloSession);
        given(Boolean.valueOf(promotionResultTMD.isApplied())).willReturn(Boolean.TRUE);
        final ProductPromotion tmdPromotion = Mockito.mock(TMDiscountPromotion.class);
        given(promotionResultTMD.getPromotion()).willReturn(tmdPromotion);
        given(Double.valueOf(promotionResultTMD.getTotalDiscount())).willReturn(Double.valueOf(1D));
        given(promotionsService.getPromotionResults(orderModel)).willReturn(promotionResults);

    }

    @Test
    public void getTotalDiscountWhenAlreadyCalculated() {

        given(orderModel.getTotalTMDDiscount()).willReturn(Double.valueOf(2D));
        final double discount = targetDiscountServiceImpl.getTotalTMDiscount(orderModel);
        assertThat(discount).isEqualTo(2D);
    }

    private void checkDollarVoucher(final AppliedVoucherData data1) {

        assertThat(data1.getVoucherCode()).contains(voucherCode1);
        assertThat(data1.isAbsolute()).isTrue();
        assertThat(data1.getAppliedValue()).isEqualTo(voucherValue1);
    }

    private void checkPercentVoucher(final AppliedVoucherData data2) {

        assertThat(data2.getVoucherCode()).contains(voucherCode2);
        assertThat(data2.isAbsolute()).isFalse();
        assertThat(data2.getAppliedValue()).isEqualTo(voucherValue2);
        assertThat(data2.getPercent()).isEqualTo(voucherPercentValue2);

    }

    private void checkFlybuy(final AppliedFlybuysDiscountData data) {

        assertThat(data.getVoucherCode()).contains(voucherCode1);
        assertThat(data.getAppliedValue()).isEqualTo(voucherValue1);
        assertThat(data.getPoints()).isEqualTo(points);
        assertThat(data.getRedeemCode()).contains(redeemCode);
        assertThat(data.getConfirmationCode()).contains(confirmationCode);

    }
}
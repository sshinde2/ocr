/**
 * 
 */
package au.com.target.tgtcore.order.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.util.PriceValue;

import java.util.Collections;

import org.fest.assertions.Delta;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.delivery.dto.DeliveryCostDto;
import au.com.target.tgtcore.delivery.dto.DeliveryCostEnumType;
import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.model.TargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.order.FindOrderRefundedShippingStrategy;


/**
 * 
 */
@SuppressWarnings("boxing")
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFindDeliveryCostStrategyTest {

    @InjectMocks
    private final TargetFindDeliveryCostStrategyImpl strategy = new TargetFindDeliveryCostStrategyImpl();

    @Mock
    private CurrencyModel currency;

    @Mock
    private AbstractOrderModel cart;

    @Mock
    private OrderModel order;

    @Mock
    private OrderCancelRequest orderCancelRequest;

    @Mock
    private TargetDeliveryService targetDeliveryService;

    @Mock
    private TargetZoneDeliveryModeValueModel targetZoneDeliveryModeValueModel;

    @Mock
    private FindOrderRefundedShippingStrategy findOrderRefundedShippingStrategy;

    @Mock
    private ConsignmentModel consignment;

    @Mock
    private TargetZoneDeliveryModeModel deliveryMode;

    @Mock
    private ConsignmentEntryModel consignmentEntry;

    @Mock
    private AbstractOrderEntryModel orderEntry;

    @Before
    public void prepare() {

        given(currency.getIsocode()).willReturn("AUD");

        given(cart.getDeliveryMode()).willReturn(deliveryMode);
        given(order.getDeliveryMode()).willReturn(deliveryMode);
        given(cart.getCurrency()).willReturn(currency);
        given(order.getCurrency()).willReturn(currency);
        given(cart.getNet()).willReturn(Boolean.FALSE);
        given(order.getNet()).willReturn(Boolean.FALSE);

        given(cart.getZoneDeliveryModeValue()).willReturn(targetZoneDeliveryModeValueModel);
        given(order.getZoneDeliveryModeValue()).willReturn(targetZoneDeliveryModeValueModel);
        given(order.getEntries()).willReturn(Collections.singletonList(orderEntry));
        given(orderEntry.getQuantity()).willReturn(Long.valueOf(1));
    }

    @Test
    public void testGetDeliveryCostCart() throws Exception {
        final DeliveryCostDto cost = new DeliveryCostDto();
        cost.setDeliveryCost(1.23);
        given(
                targetDeliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel, cart))
                .willReturn(cost);
        final PriceValue priceValue = strategy.getDeliveryCost(cart);

        assertThat(priceValue.getValue()).isEqualTo(1.23);
    }

    @Test
    public void testGetDeliveryCostCartNoDeliveryMode() throws Exception {
        final DeliveryCostDto cost = new DeliveryCostDto();
        cost.setDeliveryCost(0d);
        given(cart.getZoneDeliveryModeValue()).willReturn(null);
        given(
                targetDeliveryService.getDeliveryCostForDeliveryType(null, cart))
                .willReturn(cost);
        final PriceValue priceValue = strategy.getDeliveryCost(cart);

        assertThat(priceValue.getValue()).isEqualTo(0d);
    }

    @Test
    public void testGetDeliveryCostDto() {
        final DeliveryCostDto cost = new DeliveryCostDto();
        cost.setDeliveryCost(1d);
        given(
                targetDeliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel, cart))
                .willReturn(cost);
        final DeliveryCostDto result = strategy.getDeliveryCostDto(cart);
        assertThat(result).isEqualTo(cost);

    }

    @Test
    public void testResetShipsterAttibutesWithNormalDelivery() {
        final CartModel mockCartModel = mock(CartModel.class);
        final DeliveryCostDto deliveryCostDto = new DeliveryCostDto();
        deliveryCostDto.setType(DeliveryCostEnumType.NORMAL);
        strategy.resetShipsterAttibutes(mockCartModel, deliveryCostDto);
        verify(mockCartModel).setAusPostDeliveryClubFreeDelivery(null);
        verify(mockCartModel, Mockito.times(2)).setOriginalDeliveryCost(0d);
        verify(mockCartModel).setAusPostDeliveryClubFreeDelivery(Boolean.FALSE);
    }

    @Test
    public void testResetShipsterAttibutesWithShipsterDelivery() {
        final CartModel mockCartModel = mock(CartModel.class);
        final DeliveryCostDto deliveryCostDto = new DeliveryCostDto();
        deliveryCostDto.setType(DeliveryCostEnumType.SHIPSTERFREEDELIVERY);
        deliveryCostDto.setOriginalDeliveryCost(5d);
        strategy.resetShipsterAttibutes(mockCartModel, deliveryCostDto);
        verify(mockCartModel).setAusPostDeliveryClubFreeDelivery(null);
        verify(mockCartModel).setOriginalDeliveryCost(0d);
        verify(mockCartModel).setAusPostDeliveryClubFreeDelivery(Boolean.TRUE);
        verify(mockCartModel).setOriginalDeliveryCost(5d);
    }

    /**
     * Verifies that for orders that are already placed and possibly refunded we recalculate delivery cost based on
     * initial value.
     */
    @Test
    public void testGetDeliveryCostOrder() {
        final double initialDeliveryCost = 9d;
        final double refundedDeliveryCost = 4d;
        given(order.getInitialDeliveryCost()).willReturn(initialDeliveryCost);
        given(findOrderRefundedShippingStrategy.getRefundedShippingAmount(order)).willReturn(refundedDeliveryCost);
        assertThat(strategy.getDeliveryCost(order).getValue()).isEqualTo(initialDeliveryCost - refundedDeliveryCost,
                Delta.delta(.001d));

    }

    /**
     * Verifies that order without initial delivery cost will fall back to default calculation strategy.
     */
    @Test
    public void testGetOrderWithoutInitialDeliveryCost() {
        final double flatRate = 9d;
        final DeliveryCostDto cost = new DeliveryCostDto();
        cost.setDeliveryCost(flatRate);
        given(order.getInitialDeliveryCost()).willReturn(null);
        given(targetDeliveryService.getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel, order))
                .willReturn(cost);
        assertThat(strategy.getDeliveryCost(order).getValue()).isEqualTo(flatRate, Delta.delta(.001d));
    }

    @Test
    public void testDeliveryCostOutstanding() {
        final double initialDeliveryCost = 9d;
        final double refundedDeliveryCost = 4d;
        given(order.getInitialDeliveryCost()).willReturn(initialDeliveryCost);
        given(findOrderRefundedShippingStrategy.getRefundedShippingAmount(order)).willReturn(refundedDeliveryCost);

        final double cost = strategy.deliveryCostOutstandingForOrder(order);
        assertThat(cost).isEqualTo(initialDeliveryCost - refundedDeliveryCost, Delta.delta(.001d));
    }

    @Test
    public void testDeliveryCostOutstandingNullInitial() {
        final double refundedDeliveryCost = 4d;
        given(order.getInitialDeliveryCost()).willReturn(null);
        given(findOrderRefundedShippingStrategy.getRefundedShippingAmount(order)).willReturn(refundedDeliveryCost);

        final double cost = strategy.deliveryCostOutstandingForOrder(order);
        assertThat(cost).isEqualTo(0d, Delta.delta(.001d));
    }

    @Test
    public void testResetDeliveryValue() {
        final TargetZoneDeliveryModeModel mockMode = mock(TargetZoneDeliveryModeModel.class);
        final TargetZoneDeliveryModeValueModel mockValue = mock(TargetZoneDeliveryModeValueModel.class);
        final CartModel mockCart = Mockito.mock(CartModel.class);
        given(targetDeliveryService.getZoneDeliveryModeValueForAbstractOrderAndMode(
                any(TargetZoneDeliveryModeModel.class), any(CartModel.class))).willReturn(mockValue);
        given(mockCart.getDeliveryMode()).willReturn(mockMode);

        strategy.resetDeliveryValue(mockCart);
        verify(mockCart).setZoneDeliveryModeValue(mockValue);
    }

    @Test
    public void testResetDeliveryValueNotTargetMode() {
        final ZoneDeliveryModeModel mockMode = mock(ZoneDeliveryModeModel.class);
        final TargetZoneDeliveryModeValueModel mockValue = mock(TargetZoneDeliveryModeValueModel.class);
        final CartModel mockCart = Mockito.mock(CartModel.class);
        given(targetDeliveryService.getZoneDeliveryModeValueForAbstractOrderAndMode(
                any(TargetZoneDeliveryModeModel.class), any(CartModel.class))).willReturn(mockValue);
        given(mockCart.getDeliveryMode()).willReturn(mockMode);

        strategy.resetDeliveryValue(mockCart);
        verify(mockCart, times(0)).setZoneDeliveryModeValue(mockValue);
    }

    @Test
    public void testResetDeliveryValueNotTargetModeWhenNoPostCodeFound() {
        final TargetZoneDeliveryModeModel mockMode = mock(TargetZoneDeliveryModeModel.class);
        final CartModel mockCart = Mockito.mock(CartModel.class);
        given(targetDeliveryService.getZoneDeliveryModeValueForAbstractOrderAndMode(
                any(TargetZoneDeliveryModeModel.class), any(CartModel.class))).willThrow(
                new TargetNoPostCodeException("No post code found"));
        given(mockCart.getDeliveryMode()).willReturn(mockMode);
        strategy.resetDeliveryValue(mockCart);
        assertThat(mockCart.getZoneDeliveryModeValue()).isNull();
    }

}
/**
 * 
 */
package au.com.target.tgtcore.order.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.strategies.CreateOrderFromCartStrategy;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.order.dao.TargetOrderDao;
import au.com.target.tgtcore.order.strategies.ReplaceOrderDenialStrategy;
import au.com.target.tgtcore.util.OrderHistoryHelper;
import junit.framework.Assert;


/**
 * @author asingh78
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetOrderServiceImplTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @Mock
    private TargetOrderDao targetOrderDao;

    @Mock
    private CreateOrderFromCartStrategy createOrderFromCartStrategy;

    @Mock
    private TargetFindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    @Mock
    private OrderHistoryHelper orderHistoryHelper;

    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;

    @Mock
    private ModelService modelService;

    @Spy
    @InjectMocks
    private final TargetOrderServiceImpl targetOrderService = new TargetOrderServiceImpl();

    @Mock
    private OrderModel order;

    @Mock
    private CartModel cart;

    @Mock
    private UserModel userModel;

    /**
     * Initializes test case before run.
     */
    @Before
    public void setUp() {
        doReturn(Boolean.TRUE).when(targetOrderService).isUpdateDeliveryAddressPossible(any(OrderModel.class));
        doReturn(Boolean.TRUE).when(targetOrderService).isValidDeliveryAddress(any(OrderModel.class),
                any(AddressModel.class));
    }

    /**
     * Verifies that order service delegates search call to DAO.
     */
    @Test
    public void testFindOrderModelForOrderId() {
        final String orderId = "00000000";
        targetOrderService.findOrderModelForOrderId(orderId);
        verify(targetOrderDao).findOrderModelForOrderId(orderId);
    }

    @Test
    public void testUpdateDeliveryAddress() {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final AddressModel addressModel = Mockito.mock(AddressModel.class);
        BDDMockito.doReturn(targetBusinessProcessService).when(targetOrderService).getTargetBusinessProcessService();
        Assert.assertTrue(targetOrderService.updateDeliveryAddress(orderModel, addressModel));
        Mockito.verify(orderHistoryHelper)
                .createHistorySnapshot(orderModel, addressModel, TargetOrderServiceImpl.ADDRESS_UPDATE_NOTE);
        Mockito.verify(orderModel).setDeliveryAddress(addressModel);
        Mockito.verify(targetBusinessProcessService).startPOSAddressChangeProcess(orderModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateDeliveryAddressNullOrder() {
        final AddressModel addressModel = Mockito.mock(AddressModel.class);
        Assert.assertTrue(targetOrderService.updateDeliveryAddress(null, addressModel));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateDeliveryAddressNullAddress() {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        Assert.assertTrue(targetOrderService.updateDeliveryAddress(orderModel, null));
    }


    /**
     * Verifies that initial delivery cost is set.
     * 
     * @throws InvalidCartException
     *             if cart validation fails
     */
    @Test
    public void testSetInitialDeliveryCost() throws InvalidCartException {
        final Double deliveryCost = Double.valueOf(1d);
        when(order.getDeliveryCost()).thenReturn(deliveryCost);
        when(createOrderFromCartStrategy.createOrderFromCart(cart)).thenReturn(order);

        targetOrderService.createOrderFromCart(cart);

        verify(order).setInitialDeliveryCost(deliveryCost);
    }

    /**
     * verify whether IsReplacePossible() works
     */
    @SuppressWarnings("boxing")
    @Test
    public void testIsReplacePossible() {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final ReplaceOrderDenialStrategy replaceOrderDenialStrategy = Mockito.mock(ReplaceOrderDenialStrategy.class);
        final List<ReplaceOrderDenialStrategy> replaceOrderDenialStrategies = new ArrayList<>();
        when(replaceOrderDenialStrategy.isDenied(orderModel)).thenReturn(false);
        targetOrderService.setReplaceOrderDenialStrategies(replaceOrderDenialStrategies);
        final boolean replacePossible = targetOrderService.isReplacePossible(order);
        Assert.assertTrue(replacePossible);
    }

    /**
     * Verifies that order service delegates search call to DAO.
     */
    @Test
    public void testFindOrderforPartnerOrder() {
        final String eBayOrderNumber = "123456";
        final String salesChannel = "eBay";
        targetOrderService.findOrdersForPartnerOrderIdAndSalesChannel(eBayOrderNumber, salesChannel);
        verify(targetOrderDao).findOrdersForPartnerOrderIdAndSalesChannel(eBayOrderNumber, salesChannel);
    }

    @Test
    public void testFindLatestOrderForUserWhenOrderExists() {
        Mockito.when(targetOrderDao.findLatestOrderForUser(userModel)).thenReturn(order);
        Assert.assertEquals(order, targetOrderService.findLatestOrderForUser(userModel));
    }

    @Test
    public void testFindLatestOrderForUserWhenOrderDoesntExists() {
        Mockito.when(targetOrderDao.findLatestOrderForUser(userModel)).thenReturn(null);
        Assert.assertNull(targetOrderService.findLatestOrderForUser(userModel));
    }

    @Test
    public void testGetUniqueKey() {
        final String uniqueKey = targetOrderService.getUniqueKey();
        Assert.assertNotNull(uniqueKey);
    }

    @Test
    public void testCreateUniqueKeyForPaymentInitiation() {
        final String uniqueKey = "213564987987";
        final CartModel cartModel = Mockito.mock(CartModel.class);
        BDDMockito.given(targetOrderService.getUniqueKey()).willReturn(uniqueKey);
        targetOrderService.createUniqueKeyForPaymentInitiation(cartModel);
        BDDMockito.verify(cartModel).setUniqueKeyForPayment(uniqueKey);
    }

    @Test
    public void testUpdateOwner() {
        targetOrderService.updateOwner(order, userModel);
        verify(modelService).refresh(order);
        verify(order).setUser(userModel);
        verify(modelService).save(order);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateOwnerWithoutOrder() {
        targetOrderService.updateOwner(null, userModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateOwnerWithoutUser() {
        targetOrderService.updateOwner(order, null);
    }

    @Test
    public void testFindOrderByFluentId() {
        final OrderModel orderModel = mock(OrderModel.class);
        willReturn(orderModel).given(targetOrderDao).findOrderByFluentId("fluentId");
        targetOrderService.findOrderByFluentId("fluentId");
    }

    @Test
    public void testFindPendingPreOrdersForProduct() {
        final OrderModel orderModel = mock(OrderModel.class);
        willReturn(Arrays.asList(orderModel)).given(targetOrderDao).findPendingPreOrdersForProduct("productCode");
        assertThat(targetOrderService.findPendingPreOrdersForProduct("productCode")).containsExactly(orderModel);
    }
}
/**
 * 
 */
package au.com.target.tgtcore.order.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.order.dao.TargetOrderDao;
import au.com.target.tgtcore.product.data.ProductDisplayType;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;


/**
 * @author gsing236
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TargetPreOrderServiceImplTest {

    @InjectMocks
    @Spy
    private final TargetPreOrderServiceImpl service = new TargetPreOrderServiceImpl();

    @Mock
    private CartModel cart;

    @Mock
    private TargetSharedConfigService sharedConfigService;

    @Mock
    private TargetColourVariantProductModel targetColourVariant;

    @Mock
    private TargetSizeVariantProductModel sizeVariant;

    @Mock
    private TargetSalesApplicationService targetSalesApplicationService;

    @Mock
    private TargetOrderDao targetOrderDao;

    private Date monday;
    private Date lastSecondOfTuesday;
    private Date thursday;
    private Date nextMonday;

    private Calendar calendar;

    @Before
    public void setup() {

        final SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        try {
            monday = dateformat.parse("2018-01-01T05:00:00.000");
            lastSecondOfTuesday = dateformat.parse("2018-01-02T23:59:59.999");
            thursday = dateformat.parse("2018-01-04T10:15:00.000");
            nextMonday = dateformat.parse("2018-01-08T23:59:59.999");
        }
        catch (final ParseException e) {
            //Ignore. Unreachable.
        }

    }



    @Test
    public void testDoesCartHavePreOrderProductTrue() {
        final TargetSizeVariantProductModel productOne = createSellableProduct(true);
        final TargetSizeVariantProductModel productTwo = createSellableProduct(false);

        mockCartEntries(productTwo, productOne);

        assertThat(service.doesCartHavePreOrderProduct(cart)).isTrue();

    }

    @Test
    public void testDoesCartDoesNotHavePreOrderProduct() {
        final TargetSizeVariantProductModel productOne = createSellableProduct(false);
        final TargetSizeVariantProductModel productTwo = createSellableProduct(false);

        mockCartEntries(productTwo, productOne);

        assertThat(service.doesCartHavePreOrderProduct(cart)).isFalse();

    }

    @Test
    public void testDoesCartDoesHavePreOrderProductEmptyCart() {

        given(cart.getEntries()).willReturn(Collections.EMPTY_LIST);
        assertThat(service.doesCartHavePreOrderProduct(cart)).isFalse();

    }

    @Test
    public void testDoesCartPreOrderProductNullCart() {

        assertThat(service.doesCartHavePreOrderProduct(null)).isFalse();

    }

    @Test
    public void testUpdatePreOrderDetailsPreOrderCart() {

        willReturn(Double.valueOf(10d)).given(sharedConfigService).getDouble(
                TgtCoreConstants.Config.PREODER_DEPOSIT_AMOUNT, 10);

        final TargetColourVariantProductModel targetColourVariantProductModel = createSellableProductForColourVariant(
                true);

        mockCartEntriesWithPreOrderItems(targetColourVariantProductModel);

        service.updatePreOrderDetails(cart);

        verify(cart).setContainsPreOrderItems(Boolean.TRUE);
        verify(cart).setPreOrderDepositAmount(Double.valueOf(10d));
    }

    @Test
    public void testUpdatePreOrderDetailsNonPreOrderCart() {

        final CartModel cartModel = new CartModel();

        willReturn(Boolean.FALSE).given(service).doesCartHavePreOrderProduct(cartModel);

        service.updatePreOrderDetails(cartModel);

        verifyZeroInteractions(sharedConfigService);
        assertThat(cartModel.getContainsPreOrderItems()).isFalse();
        assertThat(cartModel.getPreOrderDepositAmount()).isEqualTo(Double.valueOf(0d));
        assertThat(cartModel.getNormalSaleStartDateTime()).isNull();
    }

    @Test
    public void testgetProductDisplayTypePreviewProduct() {
        given(targetColourVariant.getPreview()).willReturn(Boolean.TRUE);
        final ProductDisplayType displayType = service.getProductDisplayType(targetColourVariant);
        assertThat(displayType).isEqualTo(ProductDisplayType.COMING_SOON);
    }


    /**
     * Set embargo date two months before the current month display type AVAILABLE_FOR_SALE
     */
    @Test
    public void testgetProductDisplayTypeNotEmbargoed() {
        given(targetColourVariant.getPreview()).willReturn(Boolean.FALSE);
        calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) - 2, calendar.get(Calendar.DATE));
        final Date embargoDate = new Date(calendar.getTimeInMillis());
        given(targetColourVariant.getNormalSaleStartDateTime()).willReturn(embargoDate);
        final ProductDisplayType displayType = service.getProductDisplayType(targetColourVariant);
        assertThat(displayType).isEqualTo(ProductDisplayType.AVAILABLE_FOR_SALE);
    }

    /**
     * Set embargo date two months before the current month display type AVAILABLE_FOR_SALE
     */
    @Test
    public void testgetProductDisplayTypeNotEmbargoedKioskMode() {
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.KIOSK);
        given(targetColourVariant.getPreview()).willReturn(Boolean.FALSE);
        calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) - 2, calendar.get(Calendar.DATE));
        final Date embargoDate = new Date(calendar.getTimeInMillis());
        given(targetColourVariant.getNormalSaleStartDateTime()).willReturn(embargoDate);
        final ProductDisplayType displayType = service.getProductDisplayType(targetColourVariant);
        assertThat(displayType).isEqualTo(ProductDisplayType.AVAILABLE_FOR_SALE);
    }

    /**
     * Set embargo date two months after the current month,no start and end date - display type COMING_SOON
     */
    @Test
    public void testgetProductDisplayTypeEmbargoed() {
        given(targetColourVariant.getPreview()).willReturn(Boolean.FALSE);
        calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 2, calendar.get(Calendar.DATE));
        final Date embargoDate = new Date(calendar.getTimeInMillis());
        given(targetColourVariant.getNormalSaleStartDateTime()).willReturn(embargoDate);
        final ProductDisplayType displayType = service.getProductDisplayType(targetColourVariant);
        assertThat(displayType).isEqualTo(ProductDisplayType.COMING_SOON);
    }

    /**
     * Set embargo date two months after the current month,no start and end date - display type COMING_SOON(And size
     * variant passed to the method)
     */
    @Test
    public void testgetProductDisplayTypeEmbargoedSizeVariant() {
        given(targetColourVariant.getPreview()).willReturn(Boolean.FALSE);
        given(sizeVariant.getBaseProduct()).willReturn(targetColourVariant);
        calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 2, calendar.get(Calendar.DATE));
        final Date embargoDate = new Date(calendar.getTimeInMillis());
        given(targetColourVariant.getNormalSaleStartDateTime()).willReturn(embargoDate);
        final ProductDisplayType displayType = service.getProductDisplayType(sizeVariant);
        assertThat(displayType).isEqualTo(ProductDisplayType.COMING_SOON);
    }


    /**
     * Set embargo date two months after the current month,curent date between start and end date - display type
     * PREORDER_AVAILABLE
     */
    @Test
    public void testgetProductDisplayTypeEmbargoedBetweenStartAndEnd() {
        given(targetColourVariant.getPreview()).willReturn(Boolean.FALSE);
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 3, calendar.get(Calendar.DATE));
        final Date embargoDate = new Date(calendar.getTimeInMillis());
        final Calendar start = Calendar.getInstance();
        final Calendar end = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), start.get(Calendar.MONTH) - 1, start.get(Calendar.DATE));
        final Date startDate = new Date(start.getTimeInMillis());
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 1, end.get(Calendar.DATE));
        final Date endDate = new Date(end.getTimeInMillis());
        given(targetColourVariant.getNormalSaleStartDateTime()).willReturn(embargoDate);
        given(targetColourVariant.getPreOrderStartDateTime()).willReturn(startDate);
        given(targetColourVariant.getPreOrderEndDateTime()).willReturn(endDate);
        final ProductDisplayType displayType = service.getProductDisplayType(targetColourVariant);
        assertThat(displayType).isEqualTo(ProductDisplayType.PREORDER_AVAILABLE);
    }



    /**
     * Set embargo date two months after the current month,curent date between start and end date - display type And
     * Sales channel is Kiosk , COMING_SOON
     */
    @Test
    public void testgetProductDisplayTypeEmbargoedBetweenStartAndEndKiosk() {
        given(targetColourVariant.getPreview()).willReturn(Boolean.FALSE);
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.KIOSK);
        calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 3, calendar.get(Calendar.DATE));
        final Date embargoDate = new Date(calendar.getTimeInMillis());
        final Calendar start = Calendar.getInstance();
        final Calendar end = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), start.get(Calendar.MONTH) - 1, start.get(Calendar.DATE));
        final Date startDate = new Date(start.getTimeInMillis());
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 1, end.get(Calendar.DATE));
        final Date endDate = new Date(end.getTimeInMillis());
        given(targetColourVariant.getNormalSaleStartDateTime()).willReturn(embargoDate);
        given(targetColourVariant.getPreOrderStartDateTime()).willReturn(startDate);
        given(targetColourVariant.getPreOrderEndDateTime()).willReturn(endDate);
        final ProductDisplayType displayType = service.getProductDisplayType(targetColourVariant);
        assertThat(displayType).isEqualTo(ProductDisplayType.COMING_SOON);
    }

    /**
     * Set embargo date two months after the current month,curent date after end date - display type COMING SOON
     */
    @Test
    public void testgetProductDisplayTypeEmbargoedAfterEndDate() {
        given(targetColourVariant.getPreview()).willReturn(Boolean.FALSE);
        calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 3, calendar.get(Calendar.DATE));
        final Date embargoDate = new Date(calendar.getTimeInMillis());
        final Calendar start = Calendar.getInstance();
        final Calendar end = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), start.get(Calendar.MONTH) - 3, start.get(Calendar.DATE));
        final Date startDate = new Date(start.getTimeInMillis());
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) - 1, end.get(Calendar.DATE));
        final Date endDate = new Date(end.getTimeInMillis());
        given(targetColourVariant.getNormalSaleStartDateTime()).willReturn(embargoDate);
        given(targetColourVariant.getPreOrderStartDateTime()).willReturn(startDate);
        given(targetColourVariant.getPreOrderEndDateTime()).willReturn(endDate);
        final ProductDisplayType displayType = service.getProductDisplayType(targetColourVariant);
        assertThat(displayType).isEqualTo(ProductDisplayType.COMING_SOON);
    }

    /**
     * Method to verify normal sale start date for colour variant.
     */
    @Test
    public void testUpdatePreOrderDetailsForPreOrderCartWithDateForColourVariant() {

        willReturn(Double.valueOf(10d)).given(sharedConfigService).getDouble(
                TgtCoreConstants.Config.PREODER_DEPOSIT_AMOUNT, 10);
        final TargetColourVariantProductModel targetColourVariantProductModel = createSellableProductForColourVariant(
                true);
        mockCartEntriesWithPreOrderItems(targetColourVariantProductModel);
        service.updatePreOrderDetails(cart);

        verify(cart).setNormalSaleStartDateTime(targetColourVariantProductModel.getNormalSaleStartDateTime());
    }

    /**
     * Method to verify normal sales start date(none) for colour variant.
     */
    @Test
    public void testUpdatePreOrderDetailsForPreOrderCartWithoutDateForColourVariant() {

        final TargetColourVariantProductModel productOne = createSellableProductForColourVariantWithoutReleaseDate(
                true);
        mockCartEntriesWithPreOrderItems(productOne);
        service.updatePreOrderDetails(cart);

        verify(cart).setNormalSaleStartDateTime(null);
    }

    /**
     * Method to verify normal sale start date for size variant.
     */
    @Test
    public void testUpdatePreOrderDetailsForPreOrderCartWithDateForSizeVariant() {

        willReturn(Double.valueOf(10d)).given(sharedConfigService).getDouble(
                TgtCoreConstants.Config.PREODER_DEPOSIT_AMOUNT, 10);

        final Calendar embargoDate = Calendar.getInstance();
        embargoDate.set(embargoDate.get(Calendar.YEAR), embargoDate.get(Calendar.MONTH) + 2, 27, 0, 0, 0);

        final TargetColourVariantProductModel targetColourVariantProductModel = mock(
                TargetColourVariantProductModel.class);
        targetColourVariantProductModel.setNormalSaleStartDateTime(embargoDate.getTime());

        final TargetSizeVariantProductModel productOne = createSellableProduct(
                true);
        mockCartEntries(productOne, null);
        service.updatePreOrderDetails(cart);

        verify(cart).setNormalSaleStartDateTime(targetColourVariantProductModel.getNormalSaleStartDateTime());
    }

    /**
     * Method to verify normal sales start date(none) for size variant.
     */
    @Test
    public void testUpdatePreOrderDetailsForPreOrderCartWithoutDateForSizeVariant() {

        final TargetSizeVariantProductModel productOne = createSellableProduct(true);
        final TargetSizeVariantProductModel productTwo = createSellableProduct(false);
        mockCartEntries(productTwo, productOne);
        service.updatePreOrderDetails(cart);
        verify(cart).setNormalSaleStartDateTime(null);
    }

    @Test
    public void testgetParkedPreOrdersForNormalSalesStartDateOverWeekDay() {

        final List<OrderModel> emptyList = Collections.EMPTY_LIST;
        given(targetOrderDao.findOrdersBeforeNormalSalesStartDate(lastSecondOfTuesday, OrderStatus.PARKED))
                .willReturn(emptyList);
        final List<OrderModel> parkedPreOrders = service.getParkedPreOrdersForReleaseDate(monday, 1);
        assertThat(parkedPreOrders.isEmpty()).isTrue();
        verify(targetOrderDao).findOrdersBeforeNormalSalesStartDate(lastSecondOfTuesday, OrderStatus.PARKED);

    }


    @Test
    public void testgetParkedPreOrdersForNormalSalesStartDateOnWeekend() {

        final List<OrderModel> emptyList = Collections.EMPTY_LIST;
        given(targetOrderDao.findOrdersBeforeNormalSalesStartDate(nextMonday, OrderStatus.PARKED))
                .willReturn(emptyList);
        final List<OrderModel> parkedPreOrders = service.getParkedPreOrdersForReleaseDate(thursday, 2);
        assertThat(parkedPreOrders.isEmpty()).isTrue();
        verify(targetOrderDao).findOrdersBeforeNormalSalesStartDate(nextMonday, OrderStatus.PARKED);

    }

    private TargetSizeVariantProductModel createSellableProduct(final boolean isPreOrderProduct) {
        final TargetSizeVariantProductModel size = new TargetSizeVariantProductModel();

        final TargetColourVariantProductModel colour = new TargetColourVariantProductModel();

        if (isPreOrderProduct) {
            final Calendar end = Calendar.getInstance();
            end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 2, 22, 0, 0, 0);

            final Calendar start = Calendar.getInstance();
            start.set(start.get(Calendar.YEAR), start.get(Calendar.MONTH) - 2, 22, 0, 0, 0);

            colour.setPreOrderEndDateTime(end.getTime());
            colour.setPreOrderStartDateTime(start.getTime());
        }
        size.setBaseProduct(colour);
        return size;

    }

    /**
     * Method to mock cart entries
     * 
     * @param entryOneProduct
     * @param entryTwoProduct
     */
    private void mockCartEntries(final TargetSizeVariantProductModel entryOneProduct,
            final TargetSizeVariantProductModel entryTwoProduct) {
        final List<AbstractOrderEntryModel> cartEntries = new ArrayList<>();
        if (entryOneProduct != null) {
            final AbstractOrderEntryModel cartEntryOne = mock(AbstractOrderEntryModel.class);
            cartEntries.add(cartEntryOne);
            given(cartEntryOne.getProduct()).willReturn(entryOneProduct);
        }
        if (entryTwoProduct != null) {
            final AbstractOrderEntryModel cartEntryTwo = mock(AbstractOrderEntryModel.class);
            cartEntries.add(cartEntryTwo);
            given(cartEntryTwo.getProduct()).willReturn(entryTwoProduct);
        }
        given(cart.getEntries()).willReturn(cartEntries);
    }

    /**
     * Method to mock cart entries with Pre-order items
     * 
     * @param targetColourVariantProductModel
     */
    private void mockCartEntriesWithPreOrderItems(
            final TargetColourVariantProductModel targetColourVariantProductModel) {
        final List<AbstractOrderEntryModel> cartEntries = new ArrayList<>();
        if (targetColourVariantProductModel != null) {
            final AbstractOrderEntryModel abstractOrderEntryModel = mock(AbstractOrderEntryModel.class);
            cartEntries.add(abstractOrderEntryModel);
            given(abstractOrderEntryModel.getProduct()).willReturn(targetColourVariantProductModel);
        }
        given(cart.getEntries()).willReturn(cartEntries);
    }

    /**
     * Method to create sellable colour variant product for pre-order
     * 
     * @param isPreOrderProduct
     * @return TargetColourVariantProductModel
     */
    private TargetColourVariantProductModel createSellableProductForColourVariant(
            final boolean isPreOrderProduct) {

        final TargetColourVariantProductModel colour = new TargetColourVariantProductModel();

        if (isPreOrderProduct) {
            final Calendar end = Calendar.getInstance();
            end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 2, 22, 0, 0, 0);
            final Calendar start = Calendar.getInstance();
            start.set(start.get(Calendar.YEAR), start.get(Calendar.MONTH) - 2, 22, 0, 0, 0);
            colour.setPreOrderEndDateTime(end.getTime());
            colour.setPreOrderStartDateTime(start.getTime());
            final Calendar embargoDate = Calendar.getInstance();
            embargoDate.set(embargoDate.get(Calendar.YEAR), embargoDate.get(Calendar.MONTH) + 2, 27, 0, 0, 0);
            colour.setNormalSaleStartDateTime(embargoDate.getTime());
        }
        return colour;

    }

    /**
     * Method to create sellable colour variant product for pre-order
     * 
     * @param isPreOrderProduct
     * @return TargetColourVariantProductModel
     */
    private TargetColourVariantProductModel createSellableProductForColourVariantWithoutReleaseDate(
            final boolean isPreOrderProduct) {

        final TargetColourVariantProductModel colour = new TargetColourVariantProductModel();

        if (isPreOrderProduct) {
            final Calendar end = Calendar.getInstance();
            end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 2, 22, 0, 0, 0);
            final Calendar start = Calendar.getInstance();
            start.set(start.get(Calendar.YEAR), start.get(Calendar.MONTH) - 2, 22, 0, 0, 0);
            colour.setPreOrderEndDateTime(end.getTime());
            colour.setPreOrderStartDateTime(start.getTime());
        }
        return colour;

    }



}

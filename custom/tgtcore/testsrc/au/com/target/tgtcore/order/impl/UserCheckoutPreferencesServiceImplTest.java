/**
 * 
 */
package au.com.target.tgtcore.order.impl;

import static org.mockito.Mockito.atLeast;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.StubLocaleProvider;
import de.hybris.platform.servicelayer.internal.model.impl.LocaleProvider;
import de.hybris.platform.servicelayer.model.ItemModelContextImpl;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.SavedCncStoreDetailsModel;
import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.dao.TargetPointOfServiceDao;


/**
 * @author ajit
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UserCheckoutPreferencesServiceImplTest {

    @InjectMocks
    private final UserCheckoutPreferencesServiceImpl userCheckoutPreferencesServiceImpl = new UserCheckoutPreferencesServiceImpl();

    @Mock
    private ModelService modelService;
    @Mock
    private TargetPointOfServiceDao targetPointOfServiceDao;

    /**
     * Test after place order for registered with CNC delivery mode selected.
     *
     * @throws TargetUnknownIdentifierException
     *             the target unknown identifier exception
     * @throws TargetAmbiguousIdentifierException
     *             the target ambiguous identifier exception
     */
    @Test
    public void testAfterPlaceOrderForRegisteredWithCNC() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final TargetCustomerModel customer = new TargetCustomerModel();
        BDDMockito.given(orderModel.getUser()).willReturn(customer);
        BDDMockito.given(orderModel.getFlyBuysCode()).willReturn("myFlybuysCode");
        final TargetZoneDeliveryModeModel deliveryModel = new TargetZoneDeliveryModeModel();
        deliveryModel.setIsDeliveryToStore(Boolean.valueOf(true));
        BDDMockito.given(orderModel.getDeliveryMode()).willReturn(deliveryModel);
        final TargetAddressModel address = new TargetAddressModel();
        final TitleModel title = new TitleModel();
        final LocaleProvider localeProvider = new StubLocaleProvider(Locale.ENGLISH);
        final ItemModelContextImpl itemModelContext = (ItemModelContextImpl)title.getItemModelContext();
        itemModelContext.setLocaleProvider(localeProvider);
        final TargetPointOfServiceModel targetPOS = new TargetPointOfServiceModel();
        targetPOS.setStoreNumber(Integer.valueOf(7126));
        title.setName("Mr");
        address.setFirstname("testFirstNamme");
        address.setLastname("testLastNamme");
        address.setPhone1("1234567898");
        address.setTitle(title);
        BDDMockito.given(orderModel.getDeliveryAddress()).willReturn(address);
        BDDMockito.given(modelService.create(SavedCncStoreDetailsModel.class)).willReturn(
                new SavedCncStoreDetailsModel());
        BDDMockito.given(targetPointOfServiceDao.getPOSByStoreNumber(Integer.valueOf(Mockito.anyInt()))).willReturn(
                targetPOS);

        userCheckoutPreferencesServiceImpl.saveUserPreferences(orderModel);

        BDDMockito.verify(modelService, atLeast(2)).save(customer);
    }

    /**
     * Test after place order for registered with out CNC delivery mode.
     */
    @Test
    public void testAfterPlaceOrderForRegisteredWithEXDelivery() {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final TargetCustomerModel customer = new TargetCustomerModel();
        BDDMockito.given(orderModel.getUser()).willReturn(customer);
        final TargetZoneDeliveryModeModel deliveryModel = new TargetZoneDeliveryModeModel();
        deliveryModel.setIsDeliveryToStore(Boolean.valueOf(false));
        BDDMockito.given(orderModel.getDeliveryMode()).willReturn(deliveryModel);
        userCheckoutPreferencesServiceImpl.saveUserPreferences(orderModel);

        BDDMockito.verify(modelService, atLeast(1)).save(customer);
    }

    /**
     * Test save CNC store details for customer when cnc is selected but user type Guest
     */
    @Test
    public void testSaveUserPreferencesWhenCNCIsSelected() {
        final OrderModel orderModel = new OrderModel();
        final TargetCustomerModel userModel = new TargetCustomerModel();
        userModel.setType(CustomerType.GUEST);
        orderModel.setUser(userModel);
        userCheckoutPreferencesServiceImpl.saveUserPreferences(orderModel);
        BDDMockito.verify(modelService, atLeast(0)).save(userModel);
    }

    /**
     * Test save cnc store details for customer when CNC is not selected logged in user.
     */
    @Test
    public void testSaveUserPreferencesForCustomerWhenCNCIsNotSelectedLoggedInUser() {
        final OrderModel orderModel = new OrderModel();
        final TargetCustomerModel userModel = new TargetCustomerModel();
        orderModel.setUser(userModel);
        final TargetZoneDeliveryModeModel deliveryModel = new TargetZoneDeliveryModeModel();
        deliveryModel.setIsDeliveryToStore(Boolean.valueOf(false));
        orderModel.setDeliveryMode(deliveryModel);
        userCheckoutPreferencesServiceImpl.saveUserPreferences(orderModel);
        BDDMockito.verify(modelService, atLeast(1)).save(userModel);
    }

    /**
     * Test save cnc store details for customer when cnc is selected logged in user.
     * 
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    @Test
    public void testsaveUserPreferencesForCustomerWhenCNCIsSelectedLoggedInUser()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final OrderModel orderModel = new OrderModel();
        final TargetCustomerModel userModel = new TargetCustomerModel();
        orderModel.setUser(userModel);
        final TargetZoneDeliveryModeModel deliveryModel = new TargetZoneDeliveryModeModel();
        deliveryModel.setIsDeliveryToStore(Boolean.valueOf(true));
        orderModel.setDeliveryMode(deliveryModel);

        BDDMockito.given(modelService.create(SavedCncStoreDetailsModel.class)).willReturn(
                new SavedCncStoreDetailsModel());
        final TargetAddressModel address = new TargetAddressModel();
        final TitleModel title = new TitleModel();
        final LocaleProvider localeProvider = new StubLocaleProvider(Locale.ENGLISH);
        final ItemModelContextImpl itemModelContext = (ItemModelContextImpl)title.getItemModelContext();
        itemModelContext.setLocaleProvider(localeProvider);
        final TargetPointOfServiceModel targetPOS = new TargetPointOfServiceModel();
        title.setName("Mr");
        targetPOS.setStoreNumber(Integer.valueOf(7126));
        address.setFirstname("testFirstNamme");
        address.setLastname("testLastNamme");
        address.setPhone1("1234567898");
        address.setTitle(title);
        orderModel.setDeliveryAddress(address);
        BDDMockito.given(targetPointOfServiceDao.getPOSByStoreNumber(Integer.valueOf(Mockito.anyInt()))).willReturn(
                targetPOS);

        userCheckoutPreferencesServiceImpl.saveUserPreferences(orderModel);
        BDDMockito.verify(modelService, atLeast(2)).save(userModel);
    }

    @Test
    public void testsaveUserPreferencesForCustomerWhenCNCIsSelectedLoggedInUserWithoutTitle()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final OrderModel orderModel = new OrderModel();
        final TargetCustomerModel userModel = new TargetCustomerModel();
        orderModel.setUser(userModel);
        final TargetZoneDeliveryModeModel deliveryModel = new TargetZoneDeliveryModeModel();
        deliveryModel.setIsDeliveryToStore(Boolean.valueOf(true));
        orderModel.setDeliveryMode(deliveryModel);
        final SavedCncStoreDetailsModel savedCncStoreDetailsModel = Mockito.mock(SavedCncStoreDetailsModel.class);
        BDDMockito.given(modelService.create(SavedCncStoreDetailsModel.class)).willReturn(
                savedCncStoreDetailsModel);
        final TargetAddressModel address = new TargetAddressModel();
        final TitleModel title = new TitleModel();
        final LocaleProvider localeProvider = new StubLocaleProvider(Locale.ENGLISH);
        final ItemModelContextImpl itemModelContext = (ItemModelContextImpl)title.getItemModelContext();
        itemModelContext.setLocaleProvider(localeProvider);
        final TargetPointOfServiceModel targetPOS = new TargetPointOfServiceModel();
        title.setName("Mr");
        targetPOS.setStoreNumber(Integer.valueOf(7126));
        address.setFirstname("testFirstNamme");
        address.setLastname("testLastNamme");
        address.setPhone1("1234567898");
        orderModel.setDeliveryAddress(address);
        BDDMockito.given(targetPointOfServiceDao.getPOSByStoreNumber(Integer.valueOf(Mockito.anyInt()))).willReturn(
                targetPOS);

        userCheckoutPreferencesServiceImpl.saveUserPreferences(orderModel);
        BDDMockito.verify(modelService, atLeast(2)).save(userModel);
        BDDMockito.verify(savedCncStoreDetailsModel, Mockito.times(0)).setTitle(Mockito.anyString());
    }

}

/**
 * 
 */
package au.com.target.tgtcore.orderEntry.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.orderEntry.dao.TargetOrderEntryDao;

import com.google.common.collect.ImmutableList;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetOrderEntryServiceImplTest {

    @Mock
    private TargetOrderEntryDao targetOrderEntryDao;

    @InjectMocks
    private final TargetOrderEntryServiceImpl targetOrderEntryService = new TargetOrderEntryServiceImpl();


    @Test
    public void testFindOrderEntryMissingProductModel() {
        final List<OrderEntryModel> testList = new ArrayList<>();
        given(targetOrderEntryDao.findOrderEntryMissingProductModel()).willReturn(testList);
        final List<OrderEntryModel> resultList = targetOrderEntryService.findOrderEntryMissingProductModel();
        assertThat(testList).isEqualTo(resultList);
    }

    @Test
    public void testGetAllProductCodeFromOrderEntries() {
        final OrderModel order = mock(OrderModel.class);
        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final ProductModel p1 = mock(ProductModel.class);
        given(p1.getCode()).willReturn("p1");
        given(entry1.getProduct()).willReturn(p1);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final ProductModel p2 = mock(ProductModel.class);
        given(p2.getCode()).willReturn("p2");
        given(entry2.getProduct()).willReturn(p2);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);
        final ProductModel p3 = mock(ProductModel.class);
        given(p3.getCode()).willReturn("p3");
        given(entry3.getProduct()).willReturn(p3);

        given(order.getEntries()).willReturn(ImmutableList.of(entry1, entry2, entry3));

        final List<String> resultList = targetOrderEntryService.getAllProductCodeFromOrderEntries(order);
        assertThat(resultList).containsExactly("p1", "p2", "p3");
    }

    @Test
    public void testGetOrderEntryByProductCode() {
        final OrderModel order = mock(OrderModel.class);
        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final ProductModel p1 = mock(ProductModel.class);
        given(p1.getCode()).willReturn("p1");
        given(entry1.getProduct()).willReturn(p1);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final ProductModel p2 = mock(ProductModel.class);
        given(p2.getCode()).willReturn("p2");
        given(entry2.getProduct()).willReturn(p2);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);
        final ProductModel p3 = mock(ProductModel.class);
        given(p3.getCode()).willReturn("p3");
        given(entry3.getProduct()).willReturn(p3);

        given(order.getEntries()).willReturn(ImmutableList.of(entry1, entry2, entry3));

        final AbstractOrderEntryModel result = targetOrderEntryService.getOrderEntryByProductCode(order, "p1");
        assertThat(result).isEqualTo(entry1);
    }
}

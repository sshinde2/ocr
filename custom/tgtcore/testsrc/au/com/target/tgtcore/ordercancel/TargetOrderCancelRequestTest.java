/**
 * 
 */
package au.com.target.tgtcore.ordercancel;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


@UnitTest
public class TargetOrderCancelRequestTest {

    private final String notes = "testNotes";

    private OrderModel order;
    private AbstractOrderEntryModel orderEntry1;
    private AbstractOrderEntryModel orderEntry2;
    private List<OrderCancelEntry> orderCancelEntries;

    private TargetOrderCancelRequest cancelRequest;

    @Before
    public void setup() {
        // assuming order contains two entries, one with 0 qty and other with 2 qty
        order = new OrderModel();

        orderEntry1 = new AbstractOrderEntryModel();
        orderEntry1.setQuantity(Long.valueOf(0L));
        orderEntry1.setOrder(order);

        orderEntry2 = new AbstractOrderEntryModel();
        orderEntry2.setQuantity(Long.valueOf(2L));
        orderEntry2.setOrder(order);

        order.setEntries(Arrays.asList(orderEntry1, orderEntry2));

        orderCancelEntries = new ArrayList<>();
    }

    @Test
    public void testIsPartialCancelTrue() {
        // partially cancelling the non-zero entry
        final OrderCancelEntry cancelEntry = new OrderCancelEntry(orderEntry2, 1L);
        orderCancelEntries.add(cancelEntry);
        createCancelRequest();

        Assert.assertTrue(cancelRequest.isPartialCancel());
    }

    @Test
    public void testIsPartialCancelFalse() {
        // fully cancelling the non-zero entry
        orderCancelEntries.add(new OrderCancelEntry(orderEntry2, 2L));
        createCancelRequest();

        Assert.assertFalse(cancelRequest.isPartialCancel());
    }

    private void createCancelRequest() {
        cancelRequest = new TargetOrderCancelRequest(order, orderCancelEntries,
                notes);
    }

}

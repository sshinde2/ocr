package au.com.target.tgtcore.ordercancel.denialstrategies;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelDenialReason;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FlybuysDiscCancelDenialStrategyTest {

    @Mock
    private OrderCancelDenialReason mockReason;

    @Mock
    private TargetVoucherService targetVoucherService;

    @InjectMocks
    private final FlybuysDiscCancelDenialStrategy flybuysDiscCancelDenialStrategy = new FlybuysDiscCancelDenialStrategy();

    @Test
    public void testGetCancelDenialReasonWithNoFlybuysDiscount() throws Exception {
        final OrderModel mockOrder = mock(OrderModel.class);
        given(targetVoucherService.getFlybuysDiscountForOrder(mockOrder)).willReturn(null);

        final OrderCancelDenialReason orderCancelDenialReason = flybuysDiscCancelDenialStrategy
                .getCancelDenialReason(null, mockOrder, null, false, false);
        assertThat(orderCancelDenialReason).isNull();
    }

    @Test
    public void testGetCancelDenialReasonWithFlybuysDiscount() throws Exception {
        final OrderModel mockOrder = mock(OrderModel.class);
        final FlybuysDiscountModel mockFbDisc = mock(FlybuysDiscountModel.class);
        given(targetVoucherService.getFlybuysDiscountForOrder(mockOrder)).willReturn(mockFbDisc);

        final OrderCancelDenialReason orderCancelDenialReason = flybuysDiscCancelDenialStrategy
                .getCancelDenialReason(null, mockOrder, null, false, false);
        assertThat(orderCancelDenialReason).isEqualTo(mockReason);
    }
}

/**
 * 
 */
package au.com.target.tgtcore.ordercancel.denialstrategies;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.ordercancel.OrderCancelDenialReason;
import de.hybris.platform.ticket.model.CsAgentGroupModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableSet;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;


/**
 * @author fkhan4
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCSUserGroupCancelDenialStrategyTest {

    @Mock
    private OrderCancelDenialReason mockReason;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private PrincipalModel principalModel;

    @InjectMocks
    private final TargetCSUserGroupCancelDenialStrategy strategy = new TargetCSUserGroupCancelDenialStrategy();


    @Test
    public void testGetCancelDenialReasontWithCancellationAllowedWhenFluentOn() {
        final CsAgentGroupModel csAgentGroupModel = mock(CsAgentGroupModel.class);
        given(csAgentGroupModel.getCancellationAllowed()).willReturn(Boolean.TRUE);
        given(principalModel.getGroups()).willReturn(ImmutableSet.of((PrincipalGroupModel)csAgentGroupModel));
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);

        final OrderCancelDenialReason orderCancelDenialReason = strategy
                .getCancelDenialReason(null, null, principalModel, false, false);
        assertThat(orderCancelDenialReason).isNull();
    }

    @Test
    public void testGetCancelDenialReasontWithCancellationNoAllowedWhenFluentOn() {
        final CsAgentGroupModel csAgentGroupModel = mock(CsAgentGroupModel.class);
        given(csAgentGroupModel.getCancellationAllowed()).willReturn(Boolean.FALSE);
        given(principalModel.getGroups()).willReturn(ImmutableSet.of((PrincipalGroupModel)csAgentGroupModel));
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        final OrderCancelDenialReason orderCancelDenialReason = strategy
                .getCancelDenialReason(null, null, principalModel, false, false);
        assertThat(orderCancelDenialReason).isEqualTo(mockReason);
    }

    @Test
    public void testGetCancelDenialReasontWithCancellationAllowedWhenFluentOff() {
        willReturn(Boolean.FALSE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        final OrderCancelDenialReason orderCancelDenialReason = strategy
                .getCancelDenialReason(null, null, principalModel, false, false);
        assertThat(orderCancelDenialReason).isNull();
    }


}

/**
 * 
 */
package au.com.target.tgtcore.ordercancel.denialstrategies;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelDenialReason;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;


/**
 * @author Nandini
 *
 *         Test classes for SalesChannelCancelDenialStrategy
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetSalesChannelCancelDenialStrategyTest {

    @Mock
    private OrderCancelDenialReason mockReason;

    @Mock
    private OrderModel order;

    @Mock
    private TargetSalesApplicationConfigService salesApplicationConfigService;

    @InjectMocks
    private final TargetSalesChannelCancelDenialStrategy strategy = new TargetSalesChannelCancelDenialStrategy();

    @Test
    public void testGetCancelDenialReasontWithNullSalesApplication() {
        BDDMockito.given(order.getSalesApplication()).willReturn(null);

        final OrderCancelDenialReason orderCancelDenialReason = strategy
                .getCancelDenialReason(null, order, null, false, false);
        assertThat(orderCancelDenialReason).isNull();
    }

    @Test
    public void testGetCancelDenialReasontWitSalesApplicationNullConfig() {
        BDDMockito.given(order.getSalesApplication()).willReturn(SalesApplication.TRADEME);

        final OrderCancelDenialReason orderCancelDenialReason = strategy
                .getCancelDenialReason(null, order, null, false, false);
        assertThat(orderCancelDenialReason).isNull();
    }

    @Test
    public void testGetCancelDenialReasonWhenDenyCancellationFalse() {
        BDDMockito.given(order.getSalesApplication()).willReturn(SalesApplication.TRADEME);
        BDDMockito.given(
                Boolean.valueOf(salesApplicationConfigService.isDenyCancellation(SalesApplication.TRADEME)))
                .willReturn(
                        Boolean.FALSE);

        final OrderCancelDenialReason orderCancelDenialReason = strategy
                .getCancelDenialReason(null, order, null, false, false);
        assertThat(orderCancelDenialReason).isNull();
    }

    @Test
    public void testGetCancelDenialReasonWhenDenyCancellationTrue() {
        BDDMockito.given(order.getSalesApplication()).willReturn(SalesApplication.TRADEME);
        BDDMockito.given(
                Boolean.valueOf(salesApplicationConfigService.isDenyCancellation(SalesApplication.TRADEME)))
                .willReturn(
                        Boolean.TRUE);
        final OrderCancelDenialReason orderCancelDenialReason = strategy
                .getCancelDenialReason(null, order, null, false, false);
        assertThat(orderCancelDenialReason).isEqualTo(mockReason);
    }

}
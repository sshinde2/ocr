/**
 * 
 */
package au.com.target.tgtcore.ordercancel.discountstrategy.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.model.AbstractPromotionActionModel;
import de.hybris.platform.promotions.model.PromotionNullActionModel;
import de.hybris.platform.promotions.model.PromotionOrderEntryAdjustActionModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.model.TargetPromotionOrderEntryConsumedModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.ordercancel.discountstrategy.FindConsumedEntryStrategy;


/**
 * Unit test for {@link PromotionResultCorrectionStrategyImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PromotionResultCorrectionStrategyTest {

    private static final String DUMMY_ACTIONID = "actionId";
    private static final String DUMMY_ORDERCODE = "100";
    private static final String DUMMY_PRODUCTCODE = "prodA";

    @Mock
    private ModelService modelService;

    @Mock
    private FindConsumedEntryStrategy findConsumedEntryStrategy;

    @InjectMocks
    private final PromotionResultCorrectionStrategyImpl strategy = new PromotionResultCorrectionStrategyImpl();

    @Mock
    private AbstractOrderModel order;

    @Mock
    private PromotionResultModel promotionResult;

    @Mock
    private PromotionOrderEntryAdjustActionModel foundAdjustAction;

    @Mock
    private AbstractOrderEntryModel orderEntry;

    @Mock
    private TargetPromotionOrderEntryConsumedModel foundConsumedEntry;


    @Before
    public void setup() {

        // relate order to orderEntry
        Mockito.when(orderEntry.getOrder()).thenReturn(order);
        Mockito.when(order.getCode()).thenReturn(DUMMY_ORDERCODE);

        // Dummy product
        final ProductModel product = Mockito.mock(ProductModel.class);
        Mockito.when(orderEntry.getProduct()).thenReturn(product);
        Mockito.when(product.getCode()).thenReturn(DUMMY_PRODUCTCODE);

    }

    @Test
    public void testGetAdjustActionForOrderGivenNullPromotionResults() {

        Mockito.when(order.getAllPromotionResults()).thenReturn(null);
        assertThat(strategy.getAdjustActionForOrder(DUMMY_ACTIONID, order)).isNull();
    }

    @Test
    public void testGetAdjustActionForOrderGivenEmptyPromotionResults() {

        Mockito.when(order.getAllPromotionResults()).thenReturn(Collections.EMPTY_SET);
        assertThat(strategy.getAdjustActionForOrder(DUMMY_ACTIONID, order)).isNull();
    }

    @Test
    public void testGetAdjustActionForOrderGivenAdjustActionNotFound() {

        Mockito.when(order.getAllPromotionResults()).thenReturn(Collections.singleton(promotionResult));

        final PromotionResultCorrectionStrategyImpl spyStrategy = Mockito.spy(strategy);

        Mockito.doReturn(null).when(spyStrategy)
                .getAdjustActionForPromotionResult(DUMMY_ACTIONID, promotionResult);

        assertThat(spyStrategy.getAdjustActionForOrder(DUMMY_ACTIONID, order)).isNull();
    }

    @Test
    public void testGetAdjustActionForOrderGivenAdjustActionFound() {

        Mockito.when(order.getAllPromotionResults()).thenReturn(Collections.singleton(promotionResult));

        final PromotionResultCorrectionStrategyImpl spyStrategy = Mockito.spy(strategy);

        Mockito.doReturn(foundAdjustAction).when(spyStrategy)
                .getAdjustActionForPromotionResult(DUMMY_ACTIONID, promotionResult);

        final PromotionOrderEntryAdjustActionModel adjustAction = spyStrategy.getAdjustActionForOrder(DUMMY_ACTIONID,
                order);
        assertThat(adjustAction).isNotNull();
        assertThat(adjustAction).isEqualTo(foundAdjustAction);
    }


    @Test
    public void testGetAdjustActionForPromotionResultGivenNullActions() {

        Mockito.when(promotionResult.getActions()).thenReturn(null);
        assertThat(strategy.getAdjustActionForPromotionResult(DUMMY_ACTIONID, promotionResult)).isNull();
    }

    @Test
    public void testGetAdjustActionForPromotionResultGivenEmptyActions() {

        Mockito.when(promotionResult.getActions()).thenReturn(Collections.EMPTY_SET);
        assertThat(strategy.getAdjustActionForPromotionResult(DUMMY_ACTIONID, promotionResult)).isNull();
    }

    @Test
    public void testGetAdjustActionForPromotionResultGivenAdjustActionNotFound() {

        // Given a list of actions not containing our test one
        final PromotionOrderEntryAdjustActionModel differentAdjustAction = Mockito
                .mock(PromotionOrderEntryAdjustActionModel.class);
        Mockito.when(differentAdjustAction.getGuid()).thenReturn("not a match");
        Mockito.when(promotionResult.getActions()).thenReturn(getActionList(differentAdjustAction));

        assertThat(strategy.getAdjustActionForPromotionResult(DUMMY_ACTIONID, promotionResult)).isNull();
    }

    private Collection<AbstractPromotionActionModel> getActionList(final AbstractPromotionActionModel fromAction) {

        final Collection<AbstractPromotionActionModel> mockActions = new ArrayList<>();
        mockActions.add(fromAction);
        return mockActions;
    }


    @Test
    public void testGetAdjustActionForPromotionResultGivenAdjustActionWrongType() {

        // Given a list of actions containing our test one but wrong type
        final PromotionNullActionModel differentAdjustAction = Mockito
                .mock(PromotionNullActionModel.class);
        Mockito.when(differentAdjustAction.getGuid()).thenReturn("matched action");
        Mockito.when(promotionResult.getActions()).thenReturn(getActionList(differentAdjustAction));

        final PromotionOrderEntryAdjustActionModel retAction = strategy.getAdjustActionForPromotionResult(
                "matched action", promotionResult);
        assertThat(retAction).isNull();
    }

    @Test
    public void testGetAdjustActionForPromotionResultGivenAdjustActionFound() {

        // Given a list of actions containing our test one
        Mockito.when(foundAdjustAction.getGuid()).thenReturn("matched action");
        Mockito.when(promotionResult.getActions()).thenReturn(getActionList(foundAdjustAction));

        final PromotionOrderEntryAdjustActionModel retAction = strategy.getAdjustActionForPromotionResult(
                "matched action", promotionResult);
        assertThat(retAction).isNotNull();
        assertThat(retAction).isEqualTo(foundAdjustAction);
    }

    @Test
    public void testCorrectPromotionResultRecordsGivenNegativeQty() {

        final PromotionResultCorrectionStrategyImpl spyStrategy = Mockito.spy(strategy);
        spyStrategy.correctPromotionResultRecords(DUMMY_ACTIONID, orderEntry, 10, -1, true);

        Mockito.verify(spyStrategy, Mockito.never()).getAdjustActionForOrder(Mockito.anyString(),
                Mockito.any(AbstractOrderModel.class));
    }

    @Test
    public void testCorrectPromotionResultRecordsGivenActionNotFound() {

        final PromotionResultCorrectionStrategyImpl spyStrategy = Mockito.spy(strategy);

        Mockito.doReturn(null).when(spyStrategy).getAdjustActionForOrder(DUMMY_ACTIONID, order);

        spyStrategy.correctPromotionResultRecords(DUMMY_ACTIONID, orderEntry, 10, 1, true);

        Mockito.verify(findConsumedEntryStrategy, Mockito.never()).findConsumedEntry(promotionResult, orderEntry);
    }

    @Test
    public void testCorrectPromotionResultRecordsGivenNullDealResult() {

        final PromotionResultCorrectionStrategyImpl spyStrategy = Mockito.spy(strategy);

        Mockito.doReturn(foundAdjustAction).when(spyStrategy).getAdjustActionForOrder(DUMMY_ACTIONID, order);

        spyStrategy.correctPromotionResultRecords(DUMMY_ACTIONID, orderEntry, 10, 1, true);

        Mockito.verify(findConsumedEntryStrategy, Mockito.never()).findConsumedEntry(promotionResult, orderEntry);
    }

    @Test
    public void testCorrectPromotionResultRecordsGivenNoConsumedEntry() {

        final PromotionResultCorrectionStrategyImpl spyStrategy = Mockito.spy(strategy);

        Mockito.doReturn(foundAdjustAction).when(spyStrategy).getAdjustActionForOrder(DUMMY_ACTIONID, order);
        Mockito.when(foundAdjustAction.getPromotionResult()).thenReturn(promotionResult);
        Mockito.doReturn(null).when(findConsumedEntryStrategy).findConsumedEntry(promotionResult, orderEntry);

        spyStrategy.correctPromotionResultRecords(DUMMY_ACTIONID, orderEntry, 10, 1, true);

        Mockito.verify(findConsumedEntryStrategy).findConsumedEntry(promotionResult, orderEntry);
    }

    @Test
    public void testCorrectPromotionResultRecordsGivenQtyZero() {

        final PromotionResultCorrectionStrategyImpl spyStrategy = Mockito.spy(strategy);

        Mockito.doReturn(foundAdjustAction).when(spyStrategy).getAdjustActionForOrder(DUMMY_ACTIONID, order);
        Mockito.when(foundAdjustAction.getPromotionResult()).thenReturn(promotionResult);
        Mockito.doReturn(foundConsumedEntry).when(findConsumedEntryStrategy)
                .findConsumedEntry(promotionResult, orderEntry);

        spyStrategy.correctPromotionResultRecords(DUMMY_ACTIONID, orderEntry, 10, 0, true);

        // Expect records to be removed
        Mockito.verify(modelService).remove(foundAdjustAction);
        Mockito.verify(modelService).remove(foundConsumedEntry);
    }

    @Test
    public void testCorrectPromotionResultRecordsGivenQtyZeroNoUpdateConsumed() {

        final PromotionResultCorrectionStrategyImpl spyStrategy = Mockito.spy(strategy);

        Mockito.doReturn(foundAdjustAction).when(spyStrategy).getAdjustActionForOrder(DUMMY_ACTIONID, order);
        Mockito.when(foundAdjustAction.getPromotionResult()).thenReturn(promotionResult);
        Mockito.doReturn(foundConsumedEntry).when(findConsumedEntryStrategy)
                .findConsumedEntry(promotionResult, orderEntry);

        spyStrategy.correctPromotionResultRecords(DUMMY_ACTIONID, orderEntry, 10, 0, false);

        // Expect only adjust record to be removed
        Mockito.verify(modelService).remove(foundAdjustAction);
        Mockito.verify(modelService, Mockito.never()).remove(foundConsumedEntry);
    }

    @Test
    public void testCorrectPromotionResultRecordsGivenPositiveQty() {

        final PromotionResultCorrectionStrategyImpl spyStrategy = Mockito.spy(strategy);

        Mockito.doReturn(foundAdjustAction).when(spyStrategy).getAdjustActionForOrder(DUMMY_ACTIONID, order);
        Mockito.when(foundAdjustAction.getPromotionResult()).thenReturn(promotionResult);
        Mockito.doReturn(foundConsumedEntry).when(findConsumedEntryStrategy)
                .findConsumedEntry(promotionResult, orderEntry);

        spyStrategy.correctPromotionResultRecords(DUMMY_ACTIONID, orderEntry, 10, 2, true);

        // Expect records to be updated as follows
        Mockito.verify(foundAdjustAction).setOrderEntryQuantity(Long.valueOf(2));
        Mockito.verify(foundAdjustAction).setAmount(Double.valueOf(0 - 20));

        Mockito.verify(foundConsumedEntry).setQuantity(Long.valueOf(2));

        Mockito.verify(modelService).save(foundAdjustAction);
        Mockito.verify(modelService).save(foundConsumedEntry);
    }

    @Test
    public void testCorrectPromotionResultRecordsGivenPositiveQtyNoUpdateConsumed() {

        final PromotionResultCorrectionStrategyImpl spyStrategy = Mockito.spy(strategy);

        Mockito.doReturn(foundAdjustAction).when(spyStrategy).getAdjustActionForOrder(DUMMY_ACTIONID, order);
        Mockito.when(foundAdjustAction.getPromotionResult()).thenReturn(promotionResult);
        Mockito.doReturn(foundConsumedEntry).when(findConsumedEntryStrategy)
                .findConsumedEntry(promotionResult, orderEntry);

        spyStrategy.correctPromotionResultRecords(DUMMY_ACTIONID, orderEntry, 10, 2, false);

        // Expect only action records to be updated as follows
        Mockito.verify(foundAdjustAction).setOrderEntryQuantity(Long.valueOf(2));
        Mockito.verify(foundAdjustAction).setAmount(Double.valueOf(0 - 20));
        Mockito.verify(modelService).save(foundAdjustAction);

        Mockito.verify(foundConsumedEntry, Mockito.never()).setQuantity(Long.valueOf(2));

        Mockito.verify(modelService, Mockito.never()).save(foundConsumedEntry);
    }


}

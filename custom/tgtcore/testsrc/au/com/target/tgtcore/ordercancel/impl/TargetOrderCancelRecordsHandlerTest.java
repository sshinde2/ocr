package au.com.target.tgtcore.ordercancel.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.util.OrderHandlerUtil;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetOrderCancelRecordsHandlerTest {

    @InjectMocks
    private final TargetOrderCancelRecordsHandler orderCancelHandler = new TargetOrderCancelRecordsHandler();

    @Mock
    private OrderModel orderModel;

    @Mock
    private OrderModel versionOrderModel;

    @Mock
    private OrderHandlerUtil orderHandlerUtil;

    @Test
    public void testVerfiyCreateSnaphot() throws Exception {
        orderCancelHandler.createSnaphot(orderModel,
                versionOrderModel, StringUtils.EMPTY, null);
        Mockito.verify(orderHandlerUtil).createOrderSnapshot(orderModel, versionOrderModel, StringUtils.EMPTY, null);
    }

}

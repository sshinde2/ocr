package au.com.target.tgtcore.ordercancel.impl.executors;

import static au.com.target.tgtbusproc.util.OrderProcessParameterHelper.CancelTypeEnum.COCKPIT_CANCEL;
import static de.hybris.platform.payment.dto.TransactionStatus.REJECTED;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.math.BigDecimal.ZERO;
import static java.math.RoundingMode.CEILING;
import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willAnswer;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRecordsHandler;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.flybuys.service.FlybuysDiscountService;
import au.com.target.tgtcore.order.FluentOrderService;
import au.com.target.tgtcore.order.impl.TargetFindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtcore.ordercancel.TargetCancelService;
import au.com.target.tgtcore.ordercancel.TargetOrderCancelRequest;
import au.com.target.tgtcore.ordercancel.TargetRefundPaymentService;
import au.com.target.tgtcore.ordercancel.data.CancelRequestResponse;
import au.com.target.tgtcore.ordercancel.data.IpgNewRefundInfoDTO;
import au.com.target.tgtcore.ordercancel.data.PaymentTransactionEntryDTO;
import au.com.target.tgtcore.ordercancel.data.RefundInfoDTO;
import au.com.target.tgtcore.ordercancel.discountstrategy.VoucherCorrectionStrategy;
import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;


/**
 * Unit test for ({@link TargetCancelRequestExecutor}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCancelRequestExecutorTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @InjectMocks
    private final TargetCancelRequestExecutor cancelRequestExecutor = new TargetCancelRequestExecutor();

    @Mock
    private TargetFindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetCancelService targetCancelService;

    @Mock
    private TargetRefundPaymentService targetRefundPaymentService;

    @Mock
    private TargetBusinessProcessService businessProcessService;

    @Mock
    private OrderCancelRecordsHandler orderCancelRecordsHandler;

    @Mock
    private TransactionTemplate transactionTemplate;

    @Mock
    private TargetOrderCancelRequest orderCancelRequest;

    @Mock
    private OrderCancelRecordEntryModel orderCancelRecordEntryModel;

    @Mock
    private OrderModel orderModel;

    @Mock
    private PrincipalModel requestor;

    @Mock
    private CreditCardPaymentInfoModel newPayment;

    @Mock
    private VoucherCorrectionStrategy mockVoucherCorrectionStrategy;

    @Mock
    private FlybuysDiscountService flybuysDiscountService;

    @Mock
    private TargetTicketBusinessService targetTicketBusinessService;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private FluentOrderService fluentOrderService;

    private final String refund_customer_failed_notify_cs_cockpit_user = "The order has been cancelled and a "
            + "CS Ticket has been raised. Please ensure the customer is refunded.";

    final String order_cancel_partial_refund_failed = "Order cancel or partial cancel refund failed";

    @Before
    public void setUp() {
        given(orderCancelRequest.getOrder()).willReturn(orderModel);
        given(orderModel.getTotalPrice()).willReturn(Double.valueOf(6));
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(orderModel))
                .willReturn(Double.valueOf(10));
        given(newPayment.getSubscriptionId()).willReturn("123");
    }


    @Test
    public void testProcessFullCancelRequest() throws OrderCancelException {

        final CancelRequestResponse response = new CancelRequestResponse();

        willReturn(orderCancelRecordEntryModel).given(orderCancelRecordsHandler).createRecordEntry(orderCancelRequest,
                requestor);

        final List<PaymentTransactionEntryModel> paymentTransactionEntries = new ArrayList<>();
        given(targetRefundPaymentService.performFollowOnRefund(eq(orderModel), any(BigDecimal.class)))
                .willReturn(paymentTransactionEntries);

        cancelRequestExecutor.processInternalCancelRequest(orderCancelRequest, requestor, response, null);

        assertThat(response.getRefundAmount()).isEqualTo(BigDecimal.valueOf(4).setScale(2));
        verify(targetCancelService).modifyOrderAccordingToRequest(orderCancelRequest, orderModel);

        verify(modelService).refresh(orderModel);

        verify(targetCancelService).willCancellationIncreaseOrderValue(orderModel);

        verify(targetCancelService).releaseStock(orderCancelRequest);

        verify(targetCancelService).updateOrderStatus(orderCancelRecordEntryModel, orderModel);

        verify(targetCancelService).updateRecordEntry(orderCancelRequest);

        verify(targetRefundPaymentService).performFollowOnRefund(orderModel, BigDecimal.valueOf(4).setScale(2));

        verify(mockVoucherCorrectionStrategy).correctAppliedVouchers(orderModel);

        verify(flybuysDiscountService).setFlybuysRefundDetailsInModificationEntry(
                orderCancelRecordEntryModel,
                orderModel, orderModel.getTotalPrice());

        assertThat(response.getFollowOnRefundPaymentList()).isEqualTo(paymentTransactionEntries);
    }

    @Test
    public void testProcessFullCancelRequestForFluentOrder() throws OrderCancelException {

        willReturn("fluentId").given(orderModel).getFluentId();

        final CancelRequestResponse response = new CancelRequestResponse();

        willReturn(orderCancelRecordEntryModel).given(orderCancelRecordsHandler).createRecordEntry(orderCancelRequest,
                requestor);

        final List<PaymentTransactionEntryModel> paymentTransactionEntries = new ArrayList<>();
        given(targetRefundPaymentService.performFollowOnRefund(eq(orderModel), any(BigDecimal.class)))
                .willReturn(paymentTransactionEntries);

        cancelRequestExecutor.processInternalCancelRequest(orderCancelRequest, requestor, response, null);

        assertThat(response.getRefundAmount()).isEqualTo(BigDecimal.valueOf(4).setScale(2));
        verify(targetCancelService).modifyOrderAccordingToRequest(orderCancelRequest, orderModel);

        verify(modelService).refresh(orderModel);

        verify(targetCancelService).willCancellationIncreaseOrderValue(orderModel);

        verify(targetCancelService, never()).releaseStock(orderCancelRequest);

        verify(targetCancelService).updateOrderStatus(orderCancelRecordEntryModel, orderModel);

        verify(targetCancelService).updateRecordEntry(orderCancelRequest);

        verify(targetRefundPaymentService).performFollowOnRefund(orderModel, BigDecimal.valueOf(4).setScale(2));

        verify(mockVoucherCorrectionStrategy).correctAppliedVouchers(orderModel);

        verify(flybuysDiscountService).setFlybuysRefundDetailsInModificationEntry(
                orderCancelRecordEntryModel, orderModel, orderModel.getTotalPrice());

        assertThat(response.getFollowOnRefundPaymentList()).isEqualTo(paymentTransactionEntries);
    }

    @Test
    public void testProcessFullCancelRequestNewPayment() throws OrderCancelException {

        final CancelRequestResponse response = new CancelRequestResponse();

        willReturn(orderCancelRecordEntryModel).given(orderCancelRecordsHandler).createRecordEntry(orderCancelRequest,
                requestor);

        cancelRequestExecutor.processInternalCancelRequest(orderCancelRequest, requestor, response, newPayment);

        assertThat(response.getRefundAmount()).isEqualTo(BigDecimal.valueOf(4).setScale(2));
        verify(targetRefundPaymentService).performStandaloneRefund(orderModel,
                BigDecimal.valueOf(4).setScale(2), newPayment);
        verify(mockVoucherCorrectionStrategy).correctAppliedVouchers(orderModel);

        verify(flybuysDiscountService).setFlybuysRefundDetailsInModificationEntry(orderCancelRecordEntryModel,
                orderModel, orderModel.getTotalPrice());
    }

    @Test
    public void testProcessFullCancelRequestWithNewPayment() throws OrderCancelException {

        final CancelRequestResponse response = new CancelRequestResponse();
        final TargetOrderCancelRequest cancelRequest = new TargetOrderCancelRequest(orderModel);
        willReturn(orderCancelRecordEntryModel).given(orderCancelRecordsHandler).createRecordEntry(cancelRequest,
                requestor);
        cancelRequestExecutor.processInternalCancelRequest(cancelRequest, requestor, response, newPayment);
        verify(targetRefundPaymentService).performStandaloneRefund(orderModel,
                BigDecimal.valueOf(4).setScale(2), newPayment);
    }

    @Test
    public void testProcessFullCancelRequestWithIpgManualRefund() throws OrderCancelException {

        final CancelRequestResponse response = new CancelRequestResponse();
        final TargetOrderCancelRequest cancelRequest = new TargetOrderCancelRequest(orderModel);
        final IpgNewRefundInfoDTO ipgNewRefundInfoDTO = mock(IpgNewRefundInfoDTO.class);
        cancelRequest.setIpgNewRefundInfoDTO(ipgNewRefundInfoDTO);
        willReturn(orderCancelRecordEntryModel).given(orderCancelRecordsHandler).createRecordEntry(cancelRequest,
                requestor);

        cancelRequestExecutor.processInternalCancelRequest(cancelRequest, requestor, response, null);
        verify(targetRefundPaymentService).performIpgManualRefund(orderModel,
                BigDecimal.valueOf(4).setScale(2), ipgNewRefundInfoDTO);
    }

    @Test
    public void testProcessFullCancelRequestWithNormalRefundFollowOn() throws OrderCancelException {

        final CancelRequestResponse response = new CancelRequestResponse();
        final TargetOrderCancelRequest cancelRequest = new TargetOrderCancelRequest(orderModel);
        willReturn(orderCancelRecordEntryModel).given(orderCancelRecordsHandler).createRecordEntry(cancelRequest,
                requestor);

        cancelRequestExecutor.processInternalCancelRequest(cancelRequest, requestor, response, null);
        verify(targetRefundPaymentService).performFollowOnRefund(orderModel, BigDecimal.valueOf(4).setScale(2));
    }


    @Test
    public void testProcessCancelRequest() throws OrderCancelException {

        willReturn(TRUE).given(targetCancelService).isExtractCancelToWarehouseRequired(orderModel);
        willReturn(FALSE).given(orderCancelRequest).isPartialCancel();
        final BigDecimal refundAmountRemaining = ZERO;

        final RefundInfoDTO refundInfo = new RefundInfoDTO();
        refundInfo.setRefundAmountRemaining(refundAmountRemaining);

        given(targetRefundPaymentService.createRefundedInfo(anyList(), any(BigDecimal.class))).willReturn(refundInfo);

        cancelRequestExecutor.processCancelRequest(orderCancelRequest, requestor);

        verify(businessProcessService).startOrderCancelProcess(orderModel, null,
                null, COCKPIT_CANCEL, true, null, refundAmountRemaining);
    }

    @Test
    public void testProcessCancelRequestWithPartialRefundSuccess() throws OrderCancelException {

        final BigDecimal refundAmountRemaining = BigDecimal.valueOf(15.0d);
        final RefundInfoDTO refundInfo = new RefundInfoDTO();
        final String refundedInfoMessage = "test error";
        refundInfo.setMessage(refundedInfoMessage);
        refundInfo.setRefundAmountRemaining(refundAmountRemaining);

        willReturn(TRUE).given(targetCancelService).isExtractCancelToWarehouseRequired(orderModel);
        willReturn(FALSE).given(orderCancelRequest).isPartialCancel();
        willReturn(refundInfo).given(targetRefundPaymentService).createRefundedInfo(anyList(), any(BigDecimal.class));

        try {
            cancelRequestExecutor.processCancelRequest(orderCancelRequest, requestor);
            fail();
        }
        catch (final OrderCancelException e) {
            assertThat(true).isTrue();
        }

        verify(businessProcessService).startOrderCancelProcess(orderModel, null,
                null, COCKPIT_CANCEL, true, null, refundAmountRemaining);

        verify(targetTicketBusinessService).createCsAgentTicket(order_cancel_partial_refund_failed,
                order_cancel_partial_refund_failed,
                " Order cancel is partial failed due to failure to refund | Refund information: test error",
                orderModel, "refundfailures-csagentgroup");
    }

    @Test
    public void testProcessCancelRequestWithRefundSuccess() throws OrderCancelException {

        final RefundInfoDTO refundInfo = new RefundInfoDTO();

        willReturn(TRUE).given(targetCancelService).isExtractCancelToWarehouseRequired(orderModel);
        willReturn(FALSE).given(orderCancelRequest).isPartialCancel();
        willReturn(refundInfo).given(targetRefundPaymentService).createRefundedInfo(anyList(), any(BigDecimal.class));

        cancelRequestExecutor.processCancelRequest(orderCancelRequest, requestor);

        verify(businessProcessService).startOrderCancelProcess(orderModel, null,
                null, COCKPIT_CANCEL, true, null, null);
    }

    @Test
    public void testProcessCancelRequestRefundFailureFluentSwitchOff() throws OrderCancelException {

        final List<PaymentTransactionEntryModel> entryList = new ArrayList<>();
        final List<PaymentTransactionEntryDTO> dtoList = new ArrayList<>();
        final PaymentTransactionEntryDTO dto = new PaymentTransactionEntryDTO();
        final RefundInfoDTO refundInfo = new RefundInfoDTO();
        final String refundedInfoMessage = "System failed to refund automatically, refund amount=50 need to be refunded manually.The order has been cancelled and a CS Ticket has been raised. Please ensure the customer is refunded.";
        final BigDecimal refundAmountRemaining = BigDecimal.valueOf(50.0d);

        dto.setTransactionStatus(REJECTED.toString());
        dto.setTransactionStatusDetails(refund_customer_failed_notify_cs_cockpit_user);
        dtoList.add(dto);

        refundInfo.setMessage(refundedInfoMessage);
        refundInfo.setRefundAmountRemaining(refundAmountRemaining);

        willReturn(refundInfo).given(targetRefundPaymentService).createRefundedInfo(anyList(), any(BigDecimal.class));
        willReturn(orderCancelRecordEntryModel).given(orderCancelRecordsHandler).createRecordEntry(
                orderCancelRequest,
                requestor);

        willReturn(entryList).given(targetRefundPaymentService).performFollowOnRefund(eq(orderModel),
                any(BigDecimal.class));

        given(targetRefundPaymentService.convertPaymentTransactionEntries(entryList)).willReturn(dtoList);
        given(orderModel.getTotalPrice()).willReturn(Double.valueOf(6.0d));

        willReturn(FALSE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        final TransactionStatus status = mock(TransactionStatus.class);

        willAnswer(
                new Answer<OrderCancelException>() {

                    @Override
                    public OrderCancelException answer(final InvocationOnMock invocation) {
                        final TransactionCallback<OrderCancelException> tc = (TransactionCallback<OrderCancelException>)invocation
                                .getArguments()[0];
                        return tc.doInTransaction(status);
                    }

                }).given(transactionTemplate).execute(any(TransactionCallback.class));

        verify(flybuysDiscountService, times(0)).setFlybuysRefundDetailsInModificationEntry(
                orderCancelRecordEntryModel,
                orderModel, orderModel.getTotalPrice());

        OrderCancelException oce = null;

        try {
            cancelRequestExecutor.processCancelRequest(orderCancelRequest,
                    requestor);
        }
        catch (final OrderCancelException e) {
            oce = e;
        }

        assertThat(oce).isNotNull();
        assertThat(oce.getMessage()).contains(refund_customer_failed_notify_cs_cockpit_user);
        verify(mockVoucherCorrectionStrategy).correctAppliedVouchers(orderModel);

        verify(flybuysDiscountService).setFlybuysRefundDetailsInModificationEntry(orderCancelRecordEntryModel,
                orderModel, orderModel.getTotalPrice());

        verify(businessProcessService, never()).startOrderCancelProcess(orderModel, orderCancelRecordEntryModel,
                new BigDecimal(4).setScale(2, CEILING), COCKPIT_CANCEL, false,
                new ArrayList(), new BigDecimal(50).setScale(1, CEILING));

        verify(targetTicketBusinessService, never()).createCsAgentTicket(anyString(), anyString(),
                anyString(), any(OrderModel.class));

        verify(status, times(1)).setRollbackOnly();
    }


    @Test
    public void testProcessCancelRequestRefundFailureFluentSwitchOn() throws OrderCancelException {

        final List<PaymentTransactionEntryModel> entryList = new ArrayList<>();
        final List<PaymentTransactionEntryDTO> dtoList = new ArrayList<>();
        final PaymentTransactionEntryDTO dto = new PaymentTransactionEntryDTO();
        dto.setTransactionStatus(REJECTED.toString());
        dto.setTransactionStatusDetails(refund_customer_failed_notify_cs_cockpit_user);
        dtoList.add(dto);

        final RefundInfoDTO refundInfo = new RefundInfoDTO();
        final String refundedInfoMessage = "System failed to refund automatically, refund amount=50 need to be refunded manually.The order has been cancelled and a CS Ticket has been raised. Please ensure the customer is refunded.";
        final BigDecimal refundAmountRemaining = BigDecimal.valueOf(50.0d);
        refundInfo.setMessage(refundedInfoMessage);
        refundInfo.setRefundAmountRemaining(refundAmountRemaining);
        willReturn(refundInfo).given(targetRefundPaymentService).createRefundedInfo(anyList(), any(BigDecimal.class));
        willReturn(orderCancelRecordEntryModel).given(orderCancelRecordsHandler).createRecordEntry(
                orderCancelRequest,
                requestor);
        willReturn(entryList).given(targetRefundPaymentService).performFollowOnRefund(eq(orderModel),
                any(BigDecimal.class));

        given(targetRefundPaymentService.convertPaymentTransactionEntries(entryList)).willReturn(dtoList);

        willReturn(TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        given(orderModel.getFluentId()).willReturn("123");
        final TransactionStatus status = mock(TransactionStatus.class);

        willAnswer(
                new Answer<OrderCancelException>() {

                    @Override
                    public OrderCancelException answer(final InvocationOnMock invocation) {
                        final TransactionCallback<OrderCancelException> tc = (TransactionCallback<OrderCancelException>)invocation
                                .getArguments()[0];
                        return tc.doInTransaction(status);
                    }

                }).given(transactionTemplate).execute(any(TransactionCallback.class));
        verify(flybuysDiscountService, times(0)).setFlybuysRefundDetailsInModificationEntry(
                orderCancelRecordEntryModel,
                orderModel, orderModel.getTotalPrice());

        OrderCancelException oce = null;

        try {
            cancelRequestExecutor.processCancelRequest(orderCancelRequest,
                    requestor);
        }
        catch (final OrderCancelException e) {
            oce = e;
        }

        assertThat(oce).isNotNull();
        assertThat(oce.getMessage()).contains(refund_customer_failed_notify_cs_cockpit_user);
        verify(mockVoucherCorrectionStrategy).correctAppliedVouchers(orderModel);
        given(orderModel.getTotalPrice()).willReturn(Double.valueOf(6.0d));

        verify(flybuysDiscountService).setFlybuysRefundDetailsInModificationEntry(orderCancelRecordEntryModel,
                orderModel, orderModel.getTotalPrice());

        verify(status, never()).setRollbackOnly();

        verify(businessProcessService).startOrderCancelProcess(orderModel, orderCancelRecordEntryModel,
                new BigDecimal(4).setScale(2, CEILING), COCKPIT_CANCEL, false,
                new ArrayList(), new BigDecimal(50).setScale(1, CEILING));

        verify(targetTicketBusinessService).createCsAgentTicket(order_cancel_partial_refund_failed,
                order_cancel_partial_refund_failed,
                " Order cancel is partial failed due to failure to refund | Refund information: System failed to refund automatically, refund amount=50 need to be refunded manually.The order has been cancelled and a CS Ticket has been raised. Please ensure the customer is refunded.",
                orderModel, "refundfailures-csagentgroup");
    }

    /**
     * 1. Fluent Switch is ON <br>
     * 2. SendCancelEventToFluent throws an exception - Fluent Sync fails! <br>
     * Should throw exception on Send Cancel Event to Fluent
     *
     * @throws OrderCancelException
     */
    @Test
    public void testProcessCancelRequestFluentSwitchOnSendCancelEventToFluentFails() throws OrderCancelException {

        final TransactionStatus status = mock(TransactionStatus.class);
        final CancelRequestResponse response = mock(CancelRequestResponse.class);
        final RefundInfoDTO refundInfo = new RefundInfoDTO();

        willReturn(refundInfo).given(targetRefundPaymentService).createRefundedInfo(anyList(), any(BigDecimal.class));
        willReturn(orderCancelRecordEntryModel).given(orderCancelRecordsHandler).createRecordEntry(
                orderCancelRequest,
                requestor);
        willReturn(TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        given(orderModel.getFluentId()).willReturn("123");
        given(fluentOrderService.customerCancelOrder(orderModel)).willReturn("Failed cancel order event Fluent");
        given(response.getRefundFailureMessage()).willReturn("Failed");

        willAnswer(
                new Answer<OrderCancelException>() {

                    @Override
                    public OrderCancelException answer(final InvocationOnMock invocation) {
                        final TransactionCallback<OrderCancelException> tc = (TransactionCallback<OrderCancelException>)invocation
                                .getArguments()[0];
                        return tc.doInTransaction(status);
                    }

                }).given(transactionTemplate).execute(any(TransactionCallback.class));


        OrderCancelException oce = null;

        try {
            cancelRequestExecutor.processCancelRequest(orderCancelRequest,
                    requestor);
        }
        catch (final OrderCancelException e) {
            oce = e;
        }

        assertThat(oce).isNotNull();
        assertThat(oce.getMessage()).contains("Failed cancel order event Fluent");

        verify(businessProcessService, never()).startOrderCancelProcess(orderModel, orderCancelRecordEntryModel,
                new BigDecimal(4).setScale(2, CEILING), COCKPIT_CANCEL, false,
                new ArrayList(), new BigDecimal(50).setScale(1, CEILING));

        verify(targetTicketBusinessService, never()).createCsAgentTicket(order_cancel_partial_refund_failed,
                order_cancel_partial_refund_failed,
                " Order cancel is partial failed due to failure to refund | Refund information: System failed to refund automatically, refund amount=50 need to be refunded manually.The order has been cancelled and a CS Ticket has been raised. Please ensure the customer is refunded.",
                orderModel, "refundfailures-csagentgroup");
        verify(status, never()).setRollbackOnly();
    }

    @Test
    public void testProcessCancelRequestOrderValueIncrease() {
        final TransactionStatus status = mock(TransactionStatus.class);
        final RefundInfoDTO refundInfo = new RefundInfoDTO();
        final String refundedInfoMessage = "test error";
        refundInfo.setMessage(refundedInfoMessage);

        final BigDecimal refundAmountRemaining = BigDecimal.valueOf(50.0d);
        refundInfo.setRefundAmountRemaining(refundAmountRemaining);

        given(targetRefundPaymentService.createRefundedInfo(anyList(), any(BigDecimal.class))).willReturn(refundInfo);

        willAnswer(
                new Answer<OrderCancelException>() {

                    @Override
                    public OrderCancelException answer(final InvocationOnMock invocation) {
                        final TransactionCallback<OrderCancelException> tc = (TransactionCallback<OrderCancelException>)invocation
                                .getArguments()[0];
                        return tc.doInTransaction(status);
                    }

                }).given(transactionTemplate).execute(any(TransactionCallback.class));
        willReturn(TRUE).given(targetCancelService).willCancellationIncreaseOrderValue(orderModel);
        verify(flybuysDiscountService, times(0)).setFlybuysRefundDetailsInModificationEntry(
                orderCancelRecordEntryModel,
                orderModel, orderModel.getTotalPrice());

        OrderCancelException oce = null;

        try {
            cancelRequestExecutor.processCancelRequest(orderCancelRequest, requestor);
        }
        catch (final OrderCancelException e) {
            oce = e;
        }

        assertThat(oce).isNotNull();
        assertThat(oce.getOrderCode()).isEqualTo(orderModel.getCode());
        assertThat(oce.getMessage()).isNotEmpty();
    }

    @Test
    public void testSendCancelEnventToFluentWithFluentFeatureOn() {
        willReturn(TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        given(orderModel.getFluentId()).willReturn("123");
        given(fluentOrderService.customerCancelOrder(orderModel)).willReturn("Message");
        OrderCancelException oce = null;
        try {
            cancelRequestExecutor.sendCancelEventToFluent(orderModel);
        }
        catch (final OrderCancelException e) {
            oce = e;
        }
        assertThat(oce).isNotNull();
        assertThat(oce.getMessage()).contains("Message");
    }

    @Test
    public void testSendCancelEnventToFluentWithFluentFeatureOff() {
        willReturn(FALSE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        OrderCancelException oce = null;
        try {
            cancelRequestExecutor.sendCancelEventToFluent(orderModel);
        }
        catch (final OrderCancelException e) {
            oce = e;
        }
        assertThat(oce).isNull();
    }

    @Test
    public void testSendCancelEnventToFluentWithFluentFeatureOnForOldOrder() {
        willReturn(TRUE).given(targetFeatureSwitchService).isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        given(orderModel.getFluentId()).willReturn("");
        given(fluentOrderService.customerCancelOrder(orderModel)).willReturn("Message");
        OrderCancelException oce = null;
        try {
            cancelRequestExecutor.sendCancelEventToFluent(orderModel);
        }
        catch (final OrderCancelException e) {
            oce = e;
        }
        assertThat(oce).isNull();
    }
}

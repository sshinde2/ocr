/**
 * 
 */
package au.com.target.tgtcore.ordercancel.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelRecordsHandler;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.exceptions.OrderCancelRecordsHandlerException;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;

import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.ordercancel.TargetOrderCancelRequest;


/**
 * Unit test for {@link OrderCancelHelper}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderCancelHelperTest {

    private static final String NOTES = "NOTES";
    private static final CancelReason REASON = CancelReason.CUSTOMERREQUEST;

    @Mock
    private OrderCancelRecordsHandler orderCancelRecordsHandler;

    @InjectMocks
    private final OrderCancelHelper orderCancelHelper = new OrderCancelHelper();

    @Mock
    private OrderModel order;

    @Mock
    private OrderCancelRecordEntryModel orderCancelRequestEntry;

    @Mock
    private AbstractOrderEntryModel orderEntry;


    @Before
    public void setup() throws OrderCancelRecordsHandlerException {

        BDDMockito.given(orderCancelRecordsHandler.createRecordEntry(Mockito.any(OrderCancelRequest.class)))
                .willReturn(orderCancelRequestEntry);

        // Set up order
        BDDMockito.given(order.getEntries()).willReturn(Collections.singletonList(orderEntry));
        BDDMockito.given(orderEntry.getQuantity()).willReturn(Long.valueOf(2l));
        BDDMockito.given(orderEntry.getOrder()).willReturn(order);
        BDDMockito.given(order.getInitialDeliveryCost()).willReturn(Double.valueOf(2));
    }

    @Test
    public void testCreateRecord() throws OrderCancelRecordsHandlerException {

        final TargetOrderCancelRequest orderCancelRequest = orderCancelHelper.createOrderFullCancelRecord(order, NOTES,
                REASON);

        Assert.assertNotNull(orderCancelRequest);
        Assert.assertEquals(NOTES, orderCancelRequest.getNotes());
        Assert.assertEquals(REASON, orderCancelRequest.getCancelReason());
        Assert.assertEquals(Double.valueOf(2), orderCancelRequest.getShippingAmountToRefund());
    }

    @Test
    public void test() throws OrderCancelRecordsHandlerException {

        final OrderCancelRecordEntryModel ret = orderCancelHelper
                .createOrderFullCancelRecordEntry(new TargetOrderCancelRequest(order));

        Assert.assertNotNull(ret);
        Assert.assertEquals(orderCancelRequestEntry, ret);
    }

    @Test
    public void testGetCancelEntries() {

        final List<OrderCancelEntry> listEntries = orderCancelHelper.getCancelEntriesForFullOrder(order, NOTES, REASON);

        Assert.assertNotNull(listEntries);
        Assert.assertEquals(orderEntry, listEntries.get(0).getOrderEntry());
        Assert.assertEquals(2, listEntries.get(0).getCancelQuantity());
        Assert.assertEquals(REASON, listEntries.get(0).getCancelReason());
        Assert.assertEquals(NOTES, listEntries.get(0).getNotes());
    }
}

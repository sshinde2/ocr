/**
 * 
 */
package au.com.target.tgtcore.orderreturn.impl;

import static junit.framework.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.OrderModificationEntryStatus;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordercancel.exceptions.OrderCancelDaoException;
import de.hybris.platform.orderhistory.OrderHistoryService;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.refund.model.OrderRefundRecordEntryModel;
import de.hybris.platform.returns.OrderReturnRecordsHandlerException;
import de.hybris.platform.returns.dao.OrderReturnDao;
import de.hybris.platform.returns.model.OrderEntryReturnRecordEntryModel;
import de.hybris.platform.returns.model.OrderReturnRecordEntryModel;
import de.hybris.platform.returns.model.OrderReturnRecordModel;
import de.hybris.platform.returns.model.RefundEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.util.OrderHandlerUtil;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetOrderReturnRecordHandlerImplTest {

    @InjectMocks
    @Spy
    private final TargetOrderReturnRecordHandlerImpl handler = new TargetOrderReturnRecordHandlerImpl();

    @Mock
    private ModelService modelService;

    @Mock
    private OrderModel order;

    @Mock
    private OrderReturnRecordModel returnRecord;

    @Mock
    private OrderHistoryEntryModel snapshot;

    @Mock
    private UserService userService;

    @Mock
    private UserModel principal;

    @Mock
    private OrderHistoryService orderHistoryService;

    @Mock
    private PromotionsService promotionsService;

    @Mock
    private OrderReturnDao orderReturnDao;

    @Mock
    private OrderHistoryEntryModel orderHistoryEntryModel;

    @Mock
    private OrderHandlerUtil orderHandlerUtil;

    private final List<RefundEntryModel> refunds = new ArrayList<>();

    @Before
    public void setup() {
        final OrderModel mockOrder = Mockito.mock(OrderModel.class);
        final List<AbstractOrderEntryModel> entries = Mockito.mock(List.class);
        final OrderEntryModel entry1 = Mockito.mock(OrderEntryModel.class);
        final OrderEntryModel entry2 = Mockito.mock(OrderEntryModel.class);
        BDDMockito.given(snapshot.getOrder()).willReturn(mockOrder);
        BDDMockito.given(snapshot.getPreviousOrderVersion()).willReturn(mockOrder);
        BDDMockito.given(mockOrder.getEntries()).willReturn(entries);
        BDDMockito.given(entries.get(1)).willReturn(entry1);
        BDDMockito.given(entries.get(2)).willReturn(entry2);

        when(order.getCode()).thenReturn("1");
    }

    @Test
    public void testCreateRefundRecordEntry() throws OrderReturnRecordsHandlerException {
        final OrderRefundRecordEntryModel recordEntry = new OrderRefundRecordEntryModel();


        final OrderEntryReturnRecordEntryModel orderEntryRefundEntry = Mockito
                .mock(OrderEntryReturnRecordEntryModel.class);
        BDDMockito.given(modelService.create(OrderRefundRecordEntryModel.class)).willReturn(recordEntry);
        BDDMockito.given(modelService.create(OrderEntryReturnRecordEntryModel.class)).willReturn(orderEntryRefundEntry);

        final OrderReturnRecordEntryModel returnRecordEntry = handler.createRefundRecordEntry(order, returnRecord,
                snapshot,
                refunds, principal);

        Assert.assertEquals(OrderModificationEntryStatus.SUCCESSFULL, returnRecordEntry.getStatus());
        Assert.assertEquals(0, returnRecordEntry.getOrderEntriesModificationEntries().size());

    }

    @Test
    public void testFillEmployeeFieldInHistoryEntryModel() throws OrderReturnRecordsHandlerException {
        final RefundEntryModel refundEntryModel = mock(RefundEntryModel.class);
        final List<RefundEntryModel> myRefunds = new ArrayList<>();
        myRefunds.add(refundEntryModel);

        final OrderModel mySnapshot = mock(OrderModel.class);
        when(orderHistoryService.createHistorySnapshot(order)).thenReturn(mySnapshot);

        final OrderHistoryEntryModel historyEntry = mock(OrderHistoryEntryModel.class);
        when(historyEntry.getOrder()).thenReturn(order);

        final OrderModel prevOrder = mock(OrderModel.class);
        when(prevOrder.getVersionID()).thenReturn("1");
        when(historyEntry.getPreviousOrderVersion()).thenReturn(prevOrder);
        when(modelService.create(OrderHistoryEntryModel.class)).thenReturn(historyEntry);

        final OrderReturnRecordModel orderReturnRecordModel = mock(OrderReturnRecordModel.class);
        when(orderReturnDao.getOrderReturnRecord(order)).thenReturn(orderReturnRecordModel);

        final EmployeeModel employee = new EmployeeModel();
        when(userService.getCurrentUser()).thenReturn(employee);

        Mockito.doReturn(new OrderReturnRecordEntryModel()).when(
                handler).createRefundRecordEntry((OrderModel)Mockito.anyObject(),
                (OrderReturnRecordModel)Mockito.anyObject(),
                (OrderHistoryEntryModel)Mockito.anyObject(),
                (List<RefundEntryModel>)Mockito.anyList(), (UserModel)Mockito.anyObject());

        Mockito.doReturn(historyEntry).when(orderHandlerUtil)
                .createOrderSnapshot(order, "description", null);

        handler.createRefundEntry(order, myRefunds, "description");

        verify(historyEntry).setEmployee(employee);

    }

    @Test(expected = IllegalStateException.class)
    public void testThrowExceptionIfOrderReturnInProgress() throws OrderReturnRecordsHandlerException {
        final RefundEntryModel refundEntryModel = mock(RefundEntryModel.class);
        final List<RefundEntryModel> myRefunds = new ArrayList<>();
        myRefunds.add(refundEntryModel);

        final OrderReturnRecordModel orderReturnRecordModel = new OrderReturnRecordModel();
        orderReturnRecordModel.setInProgress(true);
        when(orderReturnDao.getOrderReturnRecord(order)).thenReturn(orderReturnRecordModel);

        handler.createRefundEntry(order, myRefunds, "description");
        fail("method have to throw IllegalStateException");
    }

    @Test(expected = OrderReturnRecordsHandlerException.class)
    public void testHandlingExceptionIfOrderReturnDaoThrowException() throws OrderReturnRecordsHandlerException {
        final RefundEntryModel refundEntryModel = mock(RefundEntryModel.class);
        final List<RefundEntryModel> myRefunds = new ArrayList<>();
        myRefunds.add(refundEntryModel);

        doThrow(new OrderCancelDaoException(order.getCode(), "Only one return record allowed"))
                .when(orderReturnDao).getOrderReturnRecord(order);

        handler.createRefundEntry(order, myRefunds, "description");
        fail("method have to throw OrderReturnRecordsHandlerException");
    }

    @Ignore
    @Test
    public void testCreateRefundRecordMultiEntries() throws OrderReturnRecordsHandlerException {
        final OrderRefundRecordEntryModel recordEntry = new OrderRefundRecordEntryModel();

        final OrderEntryReturnRecordEntryModel orderEntryRefundEntry = Mockito
                .mock(OrderEntryReturnRecordEntryModel.class);
        BDDMockito.given(modelService.create(OrderRefundRecordEntryModel.class)).willReturn(recordEntry);
        BDDMockito.given(modelService.create(OrderEntryReturnRecordEntryModel.class)).willReturn(orderEntryRefundEntry);

        final List<RefundEntryModel> myRefunds = new ArrayList<>();
        final RefundEntryModel entry1 = new RefundEntryModel();
        entry1.setExpectedQuantity(Long.valueOf(1l));
        final OrderEntryModel aem1 = Mockito.mock(OrderEntryModel.class);
        BDDMockito.given(aem1.getPk()).willReturn(PK.fromLong(1234l));
        BDDMockito.given(aem1.getEntryNumber()).willReturn(Integer.valueOf(1));
        entry1.setOrderEntry(aem1);

        final RefundEntryModel entry2 = new RefundEntryModel();
        entry2.setExpectedQuantity(Long.valueOf(1l));
        final OrderEntryModel aem2 = Mockito.mock(OrderEntryModel.class);
        BDDMockito.given(aem2.getPk()).willReturn(PK.fromLong(1234l));
        BDDMockito.given(aem2.getEntryNumber()).willReturn(Integer.valueOf(2));
        entry2.setOrderEntry(aem2);

        myRefunds.add(entry1);
        myRefunds.add(entry2);

        final OrderReturnRecordEntryModel returnRecordEntry = handler.createRefundRecordEntry(order, returnRecord,
                snapshot,
                myRefunds, principal);

        Assert.assertEquals(OrderModificationEntryStatus.SUCCESSFULL, returnRecordEntry.getStatus());
        Assert.assertEquals(2, returnRecordEntry.getOrderEntriesModificationEntries().size());

    }

    @Test
    public void testCreateSnaphot() {
        handler.createSnaphot(order, StringUtils.EMPTY);
        Mockito.verify(orderHandlerUtil).createOrderSnapshot(order, StringUtils.EMPTY, null);
    }

}

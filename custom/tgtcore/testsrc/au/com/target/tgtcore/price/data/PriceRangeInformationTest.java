/**
 * 
 */
package au.com.target.tgtcore.price.data;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Test;


@UnitTest
public class PriceRangeInformationTest {

    private static final Double PRICE_ONE = new Double(1);
    private static final Double PRICE_TWO = new Double(2);

    private static final Double PRICE_NEARLY_TWO = new Double(2.0001);

    @Test
    public void testRangeWithEqualSellPrices()
    {
        final PriceRangeInformation info = new PriceRangeInformation(PRICE_ONE, PRICE_ONE, null, null);
        Assert.assertFalse(info.sellPriceIsRange());
    }

    @Test
    public void testRangeWithNearlyEqualSellPrices()
    {
        final PriceRangeInformation info = new PriceRangeInformation(PRICE_TWO, PRICE_NEARLY_TWO, null, null);
        Assert.assertFalse(info.sellPriceIsRange());
    }

    @Test
    public void testRangeWithUnequalSellPrices()
    {
        final PriceRangeInformation info = new PriceRangeInformation(PRICE_ONE, PRICE_TWO, null, null);
        Assert.assertTrue(info.sellPriceIsRange());
    }

    @Test
    public void testRangeWithEqualWasPrices()
    {
        final PriceRangeInformation info = new PriceRangeInformation(PRICE_ONE, PRICE_ONE, PRICE_TWO, PRICE_TWO);
        Assert.assertFalse(info.wasPriceIsRange());
    }

    @Test
    public void testRangeWithUnequalWasPrices()
    {
        final PriceRangeInformation info = new PriceRangeInformation(PRICE_ONE, PRICE_TWO, PRICE_ONE, PRICE_TWO);
        Assert.assertTrue(info.wasPriceIsRange());
    }

    @Test
    public void testRangeWithNullWasPrices()
    {
        PriceRangeInformation info = new PriceRangeInformation(PRICE_ONE, PRICE_ONE, null, null);
        Assert.assertFalse(info.wasPriceIsRange());

        info = new PriceRangeInformation(PRICE_ONE, PRICE_ONE, PRICE_ONE, null);
        Assert.assertFalse(info.wasPriceIsRange());

        info = new PriceRangeInformation(PRICE_ONE, PRICE_ONE, null, PRICE_ONE);
        Assert.assertFalse(info.wasPriceIsRange());

    }

    @Test
    public void testRangeWithNearlyEqualWasPrices()
    {
        final PriceRangeInformation info = new PriceRangeInformation(PRICE_TWO, PRICE_TWO, PRICE_TWO, PRICE_NEARLY_TWO);
        Assert.assertFalse(info.wasPriceIsRange());
    }

}

/**
 * 
 */
package au.com.target.tgtcore.price.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.ordersplitting.daos.WarehouseDao;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.PriceService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.jalo.TargetPriceRow;
import au.com.target.tgtcore.model.TargetPreviousPermanentPriceModel;
import au.com.target.tgtcore.model.TargetPriceRowModel;
import au.com.target.tgtcore.price.daos.TargetPreviousPermanentPriceDao;
import au.com.target.tgtcore.price.data.PriceRangeInformation;
import au.com.target.tgtcore.stock.TargetStockService;
import junit.framework.Assert;


@UnitTest
public class TargetCommercePriceServiceUnitTest {

    private static final String PRICE_ROW_TYPECODE = PriceRowModel._TYPECODE.toLowerCase();
    private static final String PRICE_ROW_WASPRICE = TargetPriceRowModel.WASPRICE.toLowerCase();
    private static final String PROMO_EVENT = TargetPriceRowModel.PROMOEVENT.toLowerCase();

    private static final String CURRENT_PERM_PRICE_AGE = "currentPermPriceMaxAge";
    private static final String PREVIOUS_PERM_PRICE_AGE = "previousPermPriceMinAge";

    private static final String CURRENCY_ISO = "AUD";
    private static final boolean TEST_NET = false;

    private static final String PRODUCT_CODE = "100001";
    private static final String VAR1_CODE = "100002";
    private static final String VAR2_CODE = "100003";
    private static final String VAR3_CODE = "100004";

    private static final Double PRICE_HALF = new Double(0.5);
    private static final Double PRICE_ONE = new Double(1);
    private static final Double PRICE_TWO = new Double(2);
    private static final Double PRICE_THREE = new Double(3);
    private static final Double PRICE_SIX = new Double(6);
    private static final Double PRICE_SEVEN = new Double(7);
    private static final Double PRICE_TEN = new Double(10);

    // Services and daos to inject 
    @Mock
    private WarehouseDao warehouseDao;
    @Mock
    private TargetStockService targetStockService;
    @Mock
    private PriceService priceService;
    @Mock
    private TargetPreviousPermanentPriceDao targetPreviousPermanentPriceDao;
    @Mock
    private CommonI18NService commonI18NService;
    @Mock
    private TargetSharedConfigService targetSharedConfigService;

    // Service being tested
    @InjectMocks
    private final TargetCommercePriceServiceImpl tgtPriceService = new TargetCommercePriceServiceImpl();

    // Product and variants
    @Mock
    private ProductModel product;
    @Mock
    private VariantProductModel var1;
    @Mock
    private VariantProductModel var2;
    @Mock
    private VariantProductModel var3;

    // Selection of PriceInformation and associated PriceRows
    @Mock
    private PriceInformation priceInfoOne;
    @Mock
    private PriceInformation priceInfoTwo;
    @Mock
    private PriceInformation priceInfoThree;
    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    // Can't use mock annotation for PriceRow since it has a final hashCode method
    // See https://code.google.com/p/mockito/issues/detail?id=327
    private final TargetPriceRow priceRowWasNull = mock(TargetPriceRow.class);
    private final TargetPriceRow priceRowWasOne = mock(TargetPriceRow.class);
    private final TargetPriceRow priceRowWasSix = mock(TargetPriceRow.class);
    private final TargetPriceRow priceRowWasSeven = mock(TargetPriceRow.class);
    private final TargetPriceRow priceRowWasHalf = mock(TargetPriceRow.class);

    //priceRows for test was price with promoFlag
    private final TargetPriceRow priceRowWithPromoEventFalse = mock(TargetPriceRow.class);
    @Mock
    private List<WarehouseModel> warehouseList;


    @Before
    public void setUp() throws JaloInvalidParameterException, JaloSecurityException {
        initMocks(this);

        given(product.getCode()).willReturn(PRODUCT_CODE);
        given(var1.getCode()).willReturn(VAR1_CODE);
        given(var2.getCode()).willReturn(VAR2_CODE);
        given(var3.getCode()).willReturn(VAR3_CODE);

        given(commonI18NService.getCurrency(TgtCoreConstants.AUSTRALIAN_DOLLARS)).willReturn(new CurrencyModel());
        given(targetSharedConfigService.getConfigByCode(CURRENT_PERM_PRICE_AGE)).willReturn("13");
        given(targetSharedConfigService.getConfigByCode(PREVIOUS_PERM_PRICE_AGE)).willReturn("14");

        // A price with value = 1 
        final PriceValue onePrice = new PriceValue(CURRENCY_ISO, PRICE_ONE.doubleValue(), TEST_NET);
        given(priceInfoOne.getPriceValue()).willReturn(onePrice);

        // A price with value = 2
        final PriceValue twoPrice = new PriceValue(CURRENCY_ISO, PRICE_TWO.doubleValue(), TEST_NET);
        given(priceInfoTwo.getPriceValue()).willReturn(twoPrice);

        // A price with value = 3
        final PriceValue threePrice = new PriceValue(CURRENCY_ISO, PRICE_THREE.doubleValue(), TEST_NET);
        given(priceInfoThree.getPriceValue()).willReturn(threePrice);

        // rows for was prices
        given(priceRowWasNull.getAttribute(PRICE_ROW_WASPRICE)).willReturn(null);
        given(priceRowWasOne.getAttribute(PRICE_ROW_WASPRICE)).willReturn(PRICE_ONE);
        given(priceRowWasSix.getAttribute(PRICE_ROW_WASPRICE)).willReturn(PRICE_SIX);
        given(priceRowWasSeven.getAttribute(PRICE_ROW_WASPRICE)).willReturn(PRICE_SEVEN);
        given(priceRowWasHalf.getAttribute(PRICE_ROW_WASPRICE)).willReturn(PRICE_HALF);

        //rows for promoEvent
        given(priceRowWasNull.getAttribute(PROMO_EVENT)).willReturn(Boolean.TRUE);
        given(priceRowWasOne.getAttribute(PROMO_EVENT)).willReturn(Boolean.TRUE);
        given(priceRowWasSix.getAttribute(PROMO_EVENT)).willReturn(Boolean.TRUE);
        given(priceRowWasSeven.getAttribute(PROMO_EVENT)).willReturn(Boolean.TRUE);
        given(priceRowWasHalf.getAttribute(PROMO_EVENT)).willReturn(Boolean.TRUE);


        given(priceRowWithPromoEventFalse.getAttribute(PROMO_EVENT)).willReturn(Boolean.FALSE);

        // Set up null was price for priceInfos
        given(priceInfoOne.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWasNull);
        given(priceInfoTwo.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWasNull);
        given(priceInfoThree.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWasNull);

        // Set up a warehouse list
        given(warehouseDao.getWarehouses(any(String.class))).willReturn(warehouseList);
    }

    /**
     * Test getting the web price returns price as before (sanity check)
     */
    @Test
    public void testWebPrice() {
        setupPriceServiceForProduct();

        final PriceInformation info = tgtPriceService.getWebPriceForProduct(product);
        assertThat(new Double(info.getPriceValue().getValue())).as("Web price should be 1").isEqualTo(PRICE_ONE);
    }


    /**
     * Test price range for a product with no variants
     */
    @Test
    public void testPriceRangeInfoForProductWithNoVariants() {
        setupPriceServiceForProduct();

        final PriceRangeInformation rangeInfo = tgtPriceService.getPriceRangeInfoForProduct(product);

        // Check there will be both a low and a high and they will be equal to the supplied reference value
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("PriceRangeInformation from price is null").isNotNull();
        assertThat(rangeInfo.getToPrice()).as("PriceRangeInformation to price is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("From price should be 1").isEqualTo(PRICE_ONE);
        assertThat(rangeInfo.getToPrice()).as("To price should be 1").isEqualTo(PRICE_ONE);
    }

    /**
     * Test out of stock flag is set for a product with no variants which is out of stock
     */
    @Test
    public void testPriceRangeInfoForProductWithNoVariantsSoldOut() {
        setupPriceServiceForProduct();

        final PriceRangeInformation rangeInfo = tgtPriceService.getPriceRangeInfoForProduct(product);

        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("From price should be 1").isEqualTo(PRICE_ONE);
        assertThat(rangeInfo.getToPrice()).as("To price should be 1").isEqualTo(PRICE_ONE);
    }

    /**
     * Test price range for a product with variants which are all sold out
     */
    @Test
    public void testPriceRangeInfoForVariantsAllSoldOut() {
        setupVariantsAndPrices(priceInfoOne, priceInfoOne, null);

        final PriceRangeInformation rangeInfo = tgtPriceService.getPriceRangeInfoForProduct(product);

        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("From price should be 1").isEqualTo(PRICE_ONE);
        assertThat(rangeInfo.getToPrice()).as("To price should be 1").isEqualTo(PRICE_ONE);
    }

    /**
     * Test price range for a product with variants which have the same price
     */
    @Test
    public void testPriceRangeInfoForVariantsWithSamePrice() {
        setupVariantsAndPrices(priceInfoOne, priceInfoOne, priceInfoOne);

        final PriceRangeInformation rangeInfo = tgtPriceService.getPriceRangeInfoForProduct(product);

        // Check there will be both a low and a high and they will be equal to the reference value
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("PriceRangeInformation from price is null").isNotNull();
        assertThat(rangeInfo.getToPrice()).as("PriceRangeInformation to price is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("From price should be 1").isEqualTo(PRICE_ONE);
        assertThat(rangeInfo.getToPrice()).as("To price should be 1").isEqualTo(PRICE_ONE);
    }

    /**
     * Test price range for a product with variants which have different prices
     */
    @Test
    public void testPriceRangeInfoForVariantsWithDifferentPrices() {
        setupVariantsAndPrices(priceInfoOne, priceInfoTwo, null);

        final PriceRangeInformation rangeInfo = tgtPriceService.getPriceRangeInfoForProduct(product);

        // Check there will be both a low and a high and they are different and in the right order
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("PriceRangeInformation from price is null").isNotNull();
        assertThat(rangeInfo.getToPrice()).as("PriceRangeInformation to price is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("From price should be 1").isEqualTo(PRICE_ONE);
        assertThat(rangeInfo.getToPrice()).as("To price should be 2").isEqualTo(PRICE_TWO);
        Assert.assertTrue("From should be less than to price",
                rangeInfo.getFromPrice().doubleValue() < rangeInfo.getToPrice().doubleValue());
    }

    /**
     * Test price range for a product with two variants which have different prices, <br/>
     * One is sold out
     */
    //Ignore boxing warnings for mocking
    @SuppressWarnings("boxing")
    @Test
    public void testPriceRangeInfoForTwoVariantsWithDifferentPricesOneSoldOut() {
        setupVariantsAndPrices(priceInfoOne, priceInfoTwo, null);

        final PriceRangeInformation rangeInfo = tgtPriceService.getPriceRangeInfoForProduct(product);

        // Check there will be both a low and a high and they are for var2
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("PriceRangeInformation from price is null").isNotNull();
        assertThat(rangeInfo.getToPrice()).as("PriceRangeInformation to price is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("From price should be 1").isEqualTo(PRICE_ONE);
        assertThat(rangeInfo.getToPrice()).as("To price should be 2").isEqualTo(PRICE_TWO);
    }

    /**
     * Test price range for a product with three variants which have different prices, <br/>
     * One is sold out
     */
    // Ignore boxing warnings for mocking
    @SuppressWarnings("boxing")
    @Test
    public void testPriceRangeInfoForThreeVariantsWithDifferentPricesOneSoldOut() {
        setupVariantsAndPrices(priceInfoOne, priceInfoTwo, priceInfoThree);

        final PriceRangeInformation rangeInfo = tgtPriceService.getPriceRangeInfoForProduct(product);

        // Check there will be both a low and a high and they are for var2, var3
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("PriceRangeInformation from price is null").isNotNull();
        assertThat(rangeInfo.getToPrice()).as("PriceRangeInformation to price is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("From price should be 1").isEqualTo(PRICE_ONE);
        assertThat(rangeInfo.getToPrice()).as("To price should be 3").isEqualTo(PRICE_THREE);
    }

    /**
     * Test was price range for a product with no variants and no was
     */
    @Test
    public void testWasPriceRangeInfoForProductWithNoVariantsAndNoWas() {
        setupPriceServiceForProduct();

        final PriceRangeInformation rangeInfo = tgtPriceService.getPriceRangeInfoForProduct(product);

        // Check there is no was price data
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromWasPrice()).as("PriceRangeInformation from was price should be null").isNull();
        assertThat(rangeInfo.getToWasPrice()).as("PriceRangeInformation to was price should be null").isNull();
    }

    /**
     * Test was price range for a product with no variants, with a was price
     */
    @Test
    public void testWasPriceRangeInfoForProductWithNoVariantsAndWithWas() throws JaloInvalidParameterException {
        setupPriceServiceForProduct();

        // Set up to have was price = 6 for the product price
        given(priceInfoOne.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWasSix);

        final PriceRangeInformation rangeInfo = tgtPriceService.getPriceRangeInfoForProduct(product);

        // Check there is a was price, with low and high equal to reference value
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromWasPrice()).as("PriceRangeInformation from was price should not be null")
                .isNotNull();
        assertThat(rangeInfo.getToWasPrice()).as("PriceRangeInformation to was price should not be null").isNotNull();
        assertThat(rangeInfo.getFromWasPrice()).as("From was price should be 6").isEqualTo(PRICE_SIX);
        assertThat(rangeInfo.getToWasPrice()).as("To was price should be 6").isEqualTo(PRICE_SIX);
    }

    /**
     * Test was price range for a product with variants which have the same price
     */
    @Test
    public void testWasPriceRangeInfoForVariantsWithSamePrice() {
        setupVariantsAndPrices(priceInfoOne, priceInfoOne, null);

        // Set up to have was price = 6
        given(priceInfoOne.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWasSix);

        final PriceRangeInformation rangeInfo = tgtPriceService.getPriceRangeInfoForProduct(product);

        // Check there will be both a low and a high and they will be equal to the supplied reference value
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromWasPrice()).as("PriceRangeInformation from was price should not be null")
                .isNotNull();
        assertThat(rangeInfo.getToWasPrice()).as("PriceRangeInformation to was price should not be null").isNotNull();
        assertThat(rangeInfo.getFromWasPrice()).as("From was price should be 6").isEqualTo(PRICE_SIX);
        assertThat(rangeInfo.getToWasPrice()).as("To was price should be 6").isEqualTo(PRICE_SIX);
    }

    /**
     * Test was price range for a product with variants which have different prices
     */
    @Test
    public void testWasPriceRangeInfoForVariantsWithDifferentPrices() {
        setupVariantsAndPrices(priceInfoOne, priceInfoTwo, null);

        // Set up to have was prices 
        given(priceInfoOne.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWasSix);
        given(priceInfoTwo.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWasSeven);

        final PriceRangeInformation rangeInfo = tgtPriceService.getPriceRangeInfoForProduct(product);

        // Check there will be both a low and a high and they are different and in the right order
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromWasPrice()).as("PriceRangeInformation from was price should not be null")
                .isNotNull();
        assertThat(rangeInfo.getToWasPrice()).as("PriceRangeInformation to was price should not be null").isNotNull();
        assertThat(rangeInfo.getFromWasPrice()).as("From was price should be 6").isEqualTo(PRICE_SIX);
        assertThat(rangeInfo.getToWasPrice()).as("To was price should be 7").isEqualTo(PRICE_SEVEN);
        assertThat(rangeInfo.getFromWasPrice().doubleValue() < rangeInfo.getToWasPrice().doubleValue())
                .as("From was should be less than to price").isTrue();
    }

    /**
     * Test was price range for a product with variants, one has a null was price <br/>
     * so all was prices should be null'd
     */
    @Test
    public void testWasPriceRangeInfoForVariantsWithANullWasPrice() {
        setupVariantsAndPrices(priceInfoOne, priceInfoTwo, null);

        // Set up to have was prices, one of them is null
        given(priceInfoOne.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWasSix);
        given(priceInfoTwo.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWasNull);

        final PriceRangeInformation rangeInfo = tgtPriceService.getPriceRangeInfoForProduct(product);

        // Only show was price when all variants have a valid non-null was price 
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromWasPrice()).as("PriceRangeInformation from was price should be null").isNull();
        assertThat(rangeInfo.getToWasPrice()).as("PriceRangeInformation to was price should be null").isNull();
    }

    /**
     * Test was price range for a product with variants, one has a was price less than the sell price <br/>
     * so all was prices should be null'd
     */
    @Test
    public void testWasPriceRangeInfoForVariantsWithAnInvalidWasPrice() {
        setupVariantsAndPrices(priceInfoTwo, priceInfoThree, null);

        // Set up to have was prices, one of them has was less than the sell
        given(priceInfoTwo.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWasSix);
        given(priceInfoThree.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWasOne);

        final PriceRangeInformation rangeInfo = tgtPriceService.getPriceRangeInfoForProduct(product);

        // Only show was price when all variants have a valid was price (less than sell price)
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromWasPrice()).as("PriceRangeInformation from was price should be null").isNull();
        assertThat(rangeInfo.getToWasPrice()).as("PriceRangeInformation to was price should be null").isNull();
    }

    /**
     * Test was price range for a product with no variants and a was price less than the sell price
     */
    @Test
    public void testWasPriceRangeInfoForProductWithNoVariantsAndWithInvalidWas() throws JaloInvalidParameterException {
        setupPriceServiceForProduct();

        // Set up to have was price = 0.5 for the product price
        given(priceInfoOne.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWasHalf);

        final PriceRangeInformation rangeInfo = tgtPriceService.getPriceRangeInfoForProduct(product);

        // Check there is a was price, with low and high equal to reference value
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromWasPrice()).as("PriceRangeInformation from was price should be null").isNull();
        assertThat(rangeInfo.getToWasPrice()).as("PriceRangeInformation to was price should be null").isNull();
    }

    @Test
    public void testWasPriceRangeInfoForProductWithPromoEventFalseValidWasPrice() throws JaloInvalidParameterException {
        setupPriceServiceForProduct();

        // Set up to have was price = 10 for the product price
        given(priceInfoOne.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWithPromoEventFalse);
        final TargetPreviousPermanentPriceModel targetPreviousPermanentPriceModel = new TargetPreviousPermanentPriceModel();
        targetPreviousPermanentPriceModel.setPrice(PRICE_TEN);
        given(targetPreviousPermanentPriceDao.findValidProductPreviousPrice(Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(), Mockito.any(CurrencyModel.class))).willReturn(
                targetPreviousPermanentPriceModel);
        final PriceRangeInformation rangeInfo = tgtPriceService.getPriceRangeInfoForProduct(product);

        // Check there is a was price, with low and high equal to reference value
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromWasPrice()).as("PriceRangeInformation from was price should not be null")
                .isNotNull();
        assertThat(rangeInfo.getToWasPrice()).as("PriceRangeInformation to was price should not be null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("current price should be 1").isEqualTo(PRICE_ONE);
        assertThat(rangeInfo.getToPrice()).as("current price should be 1").isEqualTo(PRICE_ONE);
        assertThat(rangeInfo.getFromWasPrice()).as("From was price should be 10").isEqualTo(PRICE_TEN);
        assertThat(rangeInfo.getToWasPrice()).as("To was price should be 10").isEqualTo(PRICE_TEN);
    }


    @Test
    public void testWasPriceRangeInfoForProductWithPromoEventFalseInvalidWasPrice()
            throws JaloInvalidParameterException {
        setupPriceServiceForProduct();
        //was price less than current price
        // Set up to have was price = 0.5 for the product price, current price=1
        given(priceInfoOne.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWithPromoEventFalse);
        final TargetPreviousPermanentPriceModel targetPreviousPermanentPriceModel = new TargetPreviousPermanentPriceModel();
        targetPreviousPermanentPriceModel.setPrice(PRICE_HALF);
        given(targetPreviousPermanentPriceDao.findValidProductPreviousPrice(Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(), Mockito.any(CurrencyModel.class))).willReturn(
                targetPreviousPermanentPriceModel);
        final PriceRangeInformation rangeInfo = tgtPriceService.getPriceRangeInfoForProduct(product);

        // Check there is a was price, with low and high equal to reference value
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("current price should be 1").isEqualTo(PRICE_ONE);
        assertThat(rangeInfo.getToPrice()).as("current price should be 1").isEqualTo(PRICE_ONE);
        assertThat(rangeInfo.getFromWasPrice()).as("PriceRangeInformation from was price should be null").isNull();
        assertThat(rangeInfo.getToWasPrice()).as("PriceRangeInformation to was price should be null").isNull();
    }

    @Test
    public void testWasPriceRangeInfoForProductWithPromoEventFalseInvalidWasPriceFromDao()
            throws JaloInvalidParameterException {
        setupPriceServiceForProduct();
        // return null was price from Dao
        // current price=1
        given(priceInfoOne.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWithPromoEventFalse);
        given(targetPreviousPermanentPriceDao.findValidProductPreviousPrice(Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(), Mockito.any(CurrencyModel.class))).willReturn(
                null);
        final PriceRangeInformation rangeInfo = tgtPriceService.getPriceRangeInfoForProduct(product);

        // Check there is a was price, with low and high equal to reference value
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("current price should be 1").isEqualTo(PRICE_ONE);
        assertThat(rangeInfo.getToPrice()).as("current price should be 1").isEqualTo(PRICE_ONE);
        assertThat(rangeInfo.getFromWasPrice()).as("PriceRangeInformation from was price should be null").isNull();
        assertThat(rangeInfo.getToWasPrice()).as("PriceRangeInformation to was price should be null").isNull();
    }

    @Test
    public void testWasPriceRangeInfoForVariantsWithPromoEventFalseValidWasPriceFromDao()
            throws JaloInvalidParameterException {
        setupVariantsAndPrices(priceInfoOne, priceInfoTwo, priceInfoThree);
        given(priceInfoOne.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWithPromoEventFalse);
        given(priceInfoTwo.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWithPromoEventFalse);
        given(priceInfoThree.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWithPromoEventFalse);
        final TargetCommercePriceServiceImpl spyTgtPriceService = spy(tgtPriceService);
        willReturn(PRICE_SEVEN).given(spyTgtPriceService)
                .getWasPriceFromTargetPreviousPermanentPrice(VAR1_CODE);
        willReturn(PRICE_SIX).given(spyTgtPriceService).getWasPriceFromTargetPreviousPermanentPrice(VAR2_CODE);
        willReturn(PRICE_TEN).given(spyTgtPriceService).getWasPriceFromTargetPreviousPermanentPrice(VAR3_CODE);

        final PriceRangeInformation rangeInfo = spyTgtPriceService.getPriceRangeInfoForProduct(product);

        // Check there is a was price, with low and high equal to reference value
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("current price should be 1").isEqualTo(PRICE_ONE);
        assertThat(rangeInfo.getToPrice()).as("current price should be 3").isEqualTo(PRICE_THREE);
        assertThat(rangeInfo.getFromWasPrice()).as("From was price should be 6").isEqualTo(PRICE_SIX);
        assertThat(rangeInfo.getToWasPrice()).as("To was price should be 10").isEqualTo(PRICE_TEN);
    }

    @Test
    public void testWasPriceRangeInfoForVariantsWithPromoEventFalseInvalidWasPriceFromDao()
            throws JaloInvalidParameterException {
        //three variant product with one product wasprice from prevPrice higher than current price make was price null 
        setupVariantsAndPrices(priceInfoOne, priceInfoTwo, priceInfoThree);
        given(priceInfoOne.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWithPromoEventFalse);
        given(priceInfoTwo.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWithPromoEventFalse);
        given(priceInfoThree.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWithPromoEventFalse);
        final TargetCommercePriceServiceImpl spyTgtPriceService = spy(tgtPriceService);
        willReturn(PRICE_SEVEN).given(spyTgtPriceService)
                .getWasPriceFromTargetPreviousPermanentPrice(VAR1_CODE);
        willReturn(PRICE_SIX).given(spyTgtPriceService).getWasPriceFromTargetPreviousPermanentPrice(VAR2_CODE);
        willReturn(PRICE_ONE).given(spyTgtPriceService).getWasPriceFromTargetPreviousPermanentPrice(VAR3_CODE);

        final PriceRangeInformation rangeInfo = spyTgtPriceService.getPriceRangeInfoForProduct(product);

        // Check there is a was price, with low and high equal to reference value
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("current price should be 1").isEqualTo(PRICE_ONE);
        assertThat(rangeInfo.getToPrice()).as("current price should be 3").isEqualTo(PRICE_THREE);
        assertThat(rangeInfo.getFromWasPrice()).as("PriceRangeInformation from was price should be null").isNull();
        assertThat(rangeInfo.getToWasPrice()).as("PriceRangeInformation to was price should be null").isNull();
    }

    @Test
    public void testWasPriceRangeInfoForVariantsWithMixPromoEventValidWasPrice()
            throws JaloInvalidParameterException {
        //three variant product one promoEvent=true, other two=false, all have the was price 
        setupVariantsAndPrices(priceInfoOne, priceInfoTwo, priceInfoThree);
        given(priceInfoOne.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWithPromoEventFalse);
        given(priceInfoTwo.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWithPromoEventFalse);
        given(priceInfoThree.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWasSix);
        final TargetCommercePriceServiceImpl spyTgtPriceService = spy(tgtPriceService);
        willReturn(PRICE_SEVEN).given(spyTgtPriceService)
                .getWasPriceFromTargetPreviousPermanentPrice(VAR1_CODE);
        willReturn(PRICE_TEN).given(spyTgtPriceService).getWasPriceFromTargetPreviousPermanentPrice(VAR2_CODE);

        final PriceRangeInformation rangeInfo = spyTgtPriceService.getPriceRangeInfoForProduct(product);

        // Check there is a was price, with low and high equal to reference value
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("current price should be 1").isEqualTo(PRICE_ONE);
        assertThat(rangeInfo.getToPrice()).as("current price should be 3").isEqualTo(PRICE_THREE);
        assertThat(rangeInfo.getFromWasPrice()).as("From was price should be 6").isEqualTo(PRICE_SIX);
        assertThat(rangeInfo.getToWasPrice()).as("To was price should be 10").isEqualTo(PRICE_TEN);
        verify(spyTgtPriceService).getHighestVariantPrice(product);
        verify(spyTgtPriceService).getLowestVariantWasPrice(product);
        verify(spyTgtPriceService).getHighestVariantWasPrice(product);

    }

    @Test
    public void testWasPriceRangeInfoForVariantsWithMixPromoEventInvalidWasPriceFromPriceRow()
            throws JaloInvalidParameterException {
        //three variant product one promoEvent=true invalid was price, other two=false valid was price,
        setupVariantsAndPrices(priceInfoOne, priceInfoTwo, priceInfoThree);
        given(priceInfoOne.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWithPromoEventFalse);
        given(priceInfoTwo.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWithPromoEventFalse);
        //invalid was price
        given(priceInfoThree.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWasHalf);
        final TargetCommercePriceServiceImpl spyTgtPriceService = spy(tgtPriceService);
        willReturn(PRICE_SEVEN).given(spyTgtPriceService)
                .getWasPriceFromTargetPreviousPermanentPrice(VAR1_CODE);
        willReturn(PRICE_TEN).given(spyTgtPriceService).getWasPriceFromTargetPreviousPermanentPrice(VAR2_CODE);

        final PriceRangeInformation rangeInfo = spyTgtPriceService.getPriceRangeInfoForProduct(product);

        // Check there is a was price, with low and high equal to reference value
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("current price should be 1").isEqualTo(PRICE_ONE);
        assertThat(rangeInfo.getToPrice()).as("current price should be 3").isEqualTo(PRICE_THREE);
        assertThat(rangeInfo.getFromWasPrice()).as("PriceRangeInformation from was price should be null").isNull();
        assertThat(rangeInfo.getToWasPrice()).as("PriceRangeInformation to was price should be null").isNull();
    }

    @Test
    public void testWasPriceRangeInfoForVariantsWithMixPromoEventInvalidWasPriceFromPrevPermPriceRow()
            throws JaloInvalidParameterException {
        //three variant product 1.promoEvent=true valid was price, 2. promoEvent=false valid was price, 3.promoEvent=false Invalid was price
        setupVariantsAndPrices(priceInfoOne, priceInfoTwo, priceInfoThree);
        given(priceInfoOne.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWithPromoEventFalse);
        given(priceInfoTwo.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWithPromoEventFalse);
        given(priceInfoThree.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWasSix);
        final TargetCommercePriceServiceImpl spyTgtPriceService = spy(tgtPriceService);
        willReturn(PRICE_SEVEN).given(spyTgtPriceService)
                .getWasPriceFromTargetPreviousPermanentPrice(VAR1_CODE);
        //invalid was price
        willReturn(PRICE_ONE).given(spyTgtPriceService).getWasPriceFromTargetPreviousPermanentPrice(VAR2_CODE);

        final PriceRangeInformation rangeInfo = spyTgtPriceService.getPriceRangeInfoForProduct(product);

        // Check there is a was price, with low and high equal to reference value
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("current price should be 1").isEqualTo(PRICE_ONE);
        assertThat(rangeInfo.getToPrice()).as("current price should be 3").isEqualTo(PRICE_THREE);
        assertThat(rangeInfo.getFromWasPrice()).as("PriceRangeInformation from was price should be null").isNull();
        assertThat(rangeInfo.getToWasPrice()).as("PriceRangeInformation to was price should be null").isNull();
    }

    @Test
    public void testWasPriceRangeInfoForVariantsWithOptimiseFeatureSwitchOn()
            throws JaloInvalidParameterException {
        //three variant product one promoEvent=true, other two=false, all have the was price 
        setupVariantsAndPrices(priceInfoOne, priceInfoTwo, priceInfoThree);
        given(priceInfoOne.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWithPromoEventFalse);
        given(priceInfoTwo.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWithPromoEventFalse);
        given(priceInfoThree.getQualifierValue(PRICE_ROW_TYPECODE)).willReturn(priceRowWasSix);
        final TargetCommercePriceServiceImpl spyTgtPriceService = spy(tgtPriceService);
        willReturn(PRICE_SEVEN).given(spyTgtPriceService)
                .getWasPriceFromTargetPreviousPermanentPrice(VAR1_CODE);
        willReturn(PRICE_TEN).given(spyTgtPriceService).getWasPriceFromTargetPreviousPermanentPrice(VAR2_CODE);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.PDP_OPTIMISATION);

        final PriceRangeInformation rangeInfo = spyTgtPriceService.getPriceRangeInfoForProduct(product);

        // Check there is a was price, with low and high equal to reference value
        assertThat(rangeInfo).as("PriceRangeInformation is null").isNotNull();
        assertThat(rangeInfo.getFromPrice()).as("current price should be 1").isEqualTo(PRICE_ONE);
        assertThat(rangeInfo.getToPrice()).as("current price should be 3").isEqualTo(PRICE_THREE);
        assertThat(rangeInfo.getFromWasPrice()).as("From was price should be 6").isEqualTo(PRICE_SIX);
        assertThat(rangeInfo.getToWasPrice()).as("To was price should be 10").isEqualTo(PRICE_TEN);
        verify(spyTgtPriceService, never()).getHighestVariantPrice(product);
        verify(spyTgtPriceService, never()).getLowestVariantWasPrice(product);
        verify(spyTgtPriceService, never()).getHighestVariantWasPrice(product);

    }

    /**
     * Price service will return a single PriceInformation priceInfoOne
     */
    private void setupPriceServiceForProduct() {
        final List<PriceInformation> listPriceInfo = new ArrayList<>();
        given(priceService.getPriceInformationsForProduct(product)).willReturn(listPriceInfo);
        listPriceInfo.add(priceInfoOne);
    }

    /**
     * Set up product with variants and price service to return prices for the variants
     * 
     * @param price1
     *            the price for var1
     * @param price2
     *            the price for var2
     */
    private void setupVariantsAndPrices(final PriceInformation price1,
            final PriceInformation price2,
            final PriceInformation price3) {
        // Product has list of 2 or 3 variants
        final List<VariantProductModel> listVars = new LinkedList<>();
        listVars.add(var1);
        listVars.add(var2);
        given(product.getVariants()).willReturn(listVars);

        // Set up price lists for variant products
        final List<PriceInformation> var1ListPriceInfo = new ArrayList<>();
        final List<PriceInformation> var2ListPriceInfo = new ArrayList<>();
        given(priceService.getPriceInformationsForProduct(var1)).willReturn(var1ListPriceInfo);
        given(priceService.getPriceInformationsForProduct(var2)).willReturn(var2ListPriceInfo);

        // Set prices for variants
        var1ListPriceInfo.add(price1);
        var2ListPriceInfo.add(price2);

        if (price3 != null) {
            listVars.add(var3);

            final List<PriceInformation> var3ListPriceInfo = new ArrayList<>();
            given(priceService.getPriceInformationsForProduct(var3)).willReturn(var3ListPriceInfo);

            var3ListPriceInfo.add(price3);
        }
    }
}

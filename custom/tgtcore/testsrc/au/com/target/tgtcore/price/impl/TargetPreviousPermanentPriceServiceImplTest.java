/**
 * 
 */
package au.com.target.tgtcore.price.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetPreviousPermanentPriceModel;
import au.com.target.tgtcore.price.daos.TargetPreviousPermanentPriceDao;
import au.com.target.tgtcore.product.TargetProductService;


/**
 * @author knemalik
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPreviousPermanentPriceServiceImplTest {

    private static final String VARIANT_CODE = "P1001";

    private static final DateTime NOW = DateTime.now();

    @Mock
    private TargetPreviousPermanentPriceDao targetPreviousPermanentPriceDao;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetPreviousPermanentPriceModel targetPreviousPermanentPriceModel;

    @Mock
    private CatalogVersionService catalogversionService;

    @Mock
    private CommonI18NService commonI18NService;
    @Mock
    private TargetProductService productService;

    @Mock
    private CurrencyModel currency;

    private Double price;

    @Mock
    private CatalogVersionModel catalogVersionModel;

    @Mock
    private ProductModel productModel;

    @Mock
    private TargetSharedConfigService mockTargetSharedConfigService;

    @InjectMocks
    private final TargetPreviousPermanentPriceServiceImpl targetPreviousPermanentPriceServiceImpl = new TargetPreviousPermanentPriceServiceImpl();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        BDDMockito.given(modelService.create(TargetPreviousPermanentPriceModel.class)).willReturn(
                targetPreviousPermanentPriceModel);
        BDDMockito.given(commonI18NService.getCurrency(TgtCoreConstants.AUSTRALIAN_DOLLARS)).willReturn(currency);
        BDDMockito.given(catalogversionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION)).willReturn(catalogVersionModel);
        BDDMockito.given(productService.getProductForCode(catalogVersionModel, VARIANT_CODE)).willReturn(productModel);
        price = Double.valueOf(10.0);

        given(mockTargetSharedConfigService.getConfigByCode(TgtCoreConstants.Config.PREVIOUS_PERM_PRICE_MIN_AGE))
                .willReturn("14");
        given(mockTargetSharedConfigService.getConfigByCode(TgtCoreConstants.Config.CURRENT_PERM_PRICE_MAX_AGE))
                .willReturn("14");

        targetPreviousPermanentPriceServiceImpl.setCleanUpBatchSize(100);
    }

    @Test
    public void testUpdatePreviousPermanentPriceEndDate() {
        final String mockProductCode = "11009989";
        BDDMockito.given(targetPreviousPermanentPriceDao.findProductWithNullEndDate(mockProductCode)).willReturn(
                java.util.Collections.singletonList(targetPreviousPermanentPriceModel));
        targetPreviousPermanentPriceServiceImpl.updatePreviousPermanentPriceEndDate(mockProductCode);
        verify(targetPreviousPermanentPriceDao).findProductWithNullEndDate(mockProductCode);
        verify(targetPreviousPermanentPriceModel).setEndDate(Mockito.any(Date.class));
        verify(modelService).save(targetPreviousPermanentPriceModel);
    }

    @Test
    public void testUpdatePreviousPermanentPriceEndDateWithProductCodeNull() {
        final String mockProductCode = "11009989";

        BDDMockito.given(targetPreviousPermanentPriceDao.findProductWithNullEndDate(mockProductCode)).willReturn(
                null);
        targetPreviousPermanentPriceServiceImpl.updatePreviousPermanentPriceEndDate(mockProductCode);
        verify(targetPreviousPermanentPriceModel, Mockito.never()).setEndDate(Mockito.any(Date.class));
        verify(modelService, Mockito.never()).save(targetPreviousPermanentPriceModel);

    }

    @Test
    public void testGetPreviousPermanentPriceWithNullEndDate() {
        final String mockVariantCode = "11009989";
        targetPreviousPermanentPriceServiceImpl.getPreviousPermanentPriceWithNullEndDate(mockVariantCode);
        verify(targetPreviousPermanentPriceDao).findProductWithNullEndDate(mockVariantCode);

    }

    @Test
    public void testGetPreviousPermanentPriceWithNullWithProductCodeNull() {
        final String mockProductCode = "11009989";
        targetPreviousPermanentPriceServiceImpl.setModelService(modelService);
        BDDMockito.given(targetPreviousPermanentPriceDao.findProductWithNullEndDate(mockProductCode)).willReturn(
                null);
        final TargetPreviousPermanentPriceModel productCodeWithNullEndDate = targetPreviousPermanentPriceServiceImpl
                .getPreviousPermanentPriceWithNullEndDate(mockProductCode);
        Assert.assertNull(productCodeWithNullEndDate);

    }

    @Test
    public void testcreateNewPreviousPermanentPrice() {

        targetPreviousPermanentPriceServiceImpl.createNewPreviousPermanentPrice("P1002", price, currency);
        Mockito.verify(modelService).create(TargetPreviousPermanentPriceModel.class);
        Mockito.verify(modelService).save(targetPreviousPermanentPriceModel);
    }

    /**
     * Testing the scenario,promoevent false and permanentPrice doesn't exist
     */
    @Test
    public void testcreateOrUpdateWithNoPrevPermPriceAndNotPromoEvent() {

        BDDMockito
                .given(targetPreviousPermanentPriceServiceImpl.getPreviousPermanentPriceWithNullEndDate(VARIANT_CODE))
                .willReturn(null);
        BDDMockito.given(productModel.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        targetPreviousPermanentPriceServiceImpl.createOrUpdatePermanentPrice(VARIANT_CODE, price, false);
        Mockito.verify(modelService).create(TargetPreviousPermanentPriceModel.class);
        Mockito.verify(modelService).save(targetPreviousPermanentPriceModel);
    }


    /**
     * Testing the scenario,promo event false and prev perm price exists
     */
    @Test
    public void testCreateorUpdateWithPrevPermAndNotPromoEvent() {
        BDDMockito.when(targetPreviousPermanentPriceDao.findProductWithNullEndDate(VARIANT_CODE)).thenReturn(
                Collections.singletonList(targetPreviousPermanentPriceModel));
        BDDMockito.given(productModel.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        targetPreviousPermanentPriceServiceImpl.createOrUpdatePermanentPrice(VARIANT_CODE, Double.valueOf(8.0), false);
        Mockito.verify(modelService, Mockito.times(2)).save(targetPreviousPermanentPriceModel);
        Mockito.verify(modelService).create(TargetPreviousPermanentPriceModel.class);
    }

    /**
     * Testing the scenario,promo event false and prev perm price exists,same price
     */
    @Test
    public void testCreateorUpdateWithPrevPermAndNotPromoEventSamePrice() {
        BDDMockito.when(targetPreviousPermanentPriceDao.findProductWithNullEndDate(VARIANT_CODE)).thenReturn(
                Collections.singletonList(targetPreviousPermanentPriceModel));
        BDDMockito.given(targetPreviousPermanentPriceModel.getPrice()).willReturn(Double.valueOf(10.0));
        BDDMockito.given(productModel.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        targetPreviousPermanentPriceServiceImpl.createOrUpdatePermanentPrice(VARIANT_CODE, price, false);
        Mockito.verify(modelService, Mockito.never()).save(targetPreviousPermanentPriceModel);
        Mockito.verify(modelService, Mockito.never()).create(TargetPreviousPermanentPriceModel.class);
    }

    /**
     * Testing the scenario,promo event true and prev perm price exists
     */
    @Test
    public void testCreateorUpdateWithPrevPermAndPromoEvent() {
        BDDMockito.when(targetPreviousPermanentPriceDao.findProductWithNullEndDate(VARIANT_CODE)).thenReturn(
                Collections.singletonList(targetPreviousPermanentPriceModel));
        BDDMockito.given(productModel.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        targetPreviousPermanentPriceServiceImpl.createOrUpdatePermanentPrice(VARIANT_CODE, price, true);
        Mockito.verify(modelService).save(targetPreviousPermanentPriceModel);
        Mockito.verify(modelService, Mockito.never()).create(TargetPreviousPermanentPriceModel.class);
    }

    /**
     * Testing the scenario,promo event true and no prev perm exists
     */

    @Test
    public void testCreateorUpdateWithNoPrevPermAndPromoEvent() {
        BDDMockito.when(targetPreviousPermanentPriceDao.findProductWithNullEndDate(VARIANT_CODE)).thenReturn(
                null);
        BDDMockito.given(productModel.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        targetPreviousPermanentPriceServiceImpl.createOrUpdatePermanentPrice(VARIANT_CODE, price, true);
        Mockito.verify(modelService, Mockito.never()).save(targetPreviousPermanentPriceModel);
        Mockito.verify(modelService, Mockito.never()).create(TargetPreviousPermanentPriceModel.class);
    }

    /**
     * Testing the scenario product unapproved
     */
    @Test
    public void testCreateorUpdateWithNoProductUnapproved() {
        BDDMockito.when(targetPreviousPermanentPriceDao.findProductWithNullEndDate(VARIANT_CODE)).thenReturn(
                null);
        BDDMockito.given(productModel.getApprovalStatus()).willReturn(ArticleApprovalStatus.UNAPPROVED);
        targetPreviousPermanentPriceServiceImpl.createOrUpdatePermanentPrice(VARIANT_CODE, price, true);
        Mockito.verify(modelService, Mockito.never()).save(targetPreviousPermanentPriceModel);
        Mockito.verify(modelService, Mockito.never()).create(TargetPreviousPermanentPriceModel.class);
    }

    /**
     * Testing the scenario product check
     */
    @Test
    public void testCreateorUpdateWithNoProductCheck() {
        BDDMockito.when(targetPreviousPermanentPriceDao.findProductWithNullEndDate(VARIANT_CODE)).thenReturn(
                null);
        BDDMockito.given(productModel.getApprovalStatus()).willReturn(ArticleApprovalStatus.CHECK);
        targetPreviousPermanentPriceServiceImpl.createOrUpdatePermanentPrice(VARIANT_CODE, price, true);
        Mockito.verify(modelService, Mockito.never()).save(targetPreviousPermanentPriceModel);
        Mockito.verify(modelService, Mockito.never()).create(TargetPreviousPermanentPriceModel.class);
    }

    @Test
    public void testCleanupPreviousPermanentPriceRecordsWithRecords() {
        final TargetPreviousPermanentPriceModel mockTargetPreviousPermanentPriceModelStillValid = createMockTargetPreviousPermanentPriceModel(
                "00000001", NOW.minusDays(30).toDate(), NOW.minusDays(10).toDate(), Double.valueOf(10.0));
        final TargetPreviousPermanentPriceModel mockTargetPreviousPermanentPriceModelTooOld = createMockTargetPreviousPermanentPriceModel(
                "00000002", NOW.minusDays(100).toDate(), NOW.minusDays(15).toDate(), Double.valueOf(10.0));
        final TargetPreviousPermanentPriceModel mockTargetPreviousPermanentPriceModelNotLongEnough = createMockTargetPreviousPermanentPriceModel(
                "00000003", NOW.minusDays(13).toDate(), NOW.minusDays(1).toDate(), Double.valueOf(10.0));

        final List<TargetPreviousPermanentPriceModel> prevPermPrices = new ArrayList<>();
        prevPermPrices.add(mockTargetPreviousPermanentPriceModelStillValid);
        prevPermPrices.add(mockTargetPreviousPermanentPriceModelTooOld);
        prevPermPrices.add(mockTargetPreviousPermanentPriceModelNotLongEnough);

        given(targetPreviousPermanentPriceDao.findAllRemovableTargetPreviousPermanentPriceRecords(14, 14, 100))
                .willReturn(prevPermPrices);

        final boolean result = targetPreviousPermanentPriceServiceImpl.cleanupPreviousPermanentPriceRecords();

        assertThat(result).isTrue();
        verify(modelService).removeAll(prevPermPrices);
    }

    @Test
    public void testCleanupPreviousPermanentPriceRecordsWithNoRecords() {
        final List<TargetPreviousPermanentPriceModel> prevPermPrices = new ArrayList<>();

        given(targetPreviousPermanentPriceDao.findAllRemovableTargetPreviousPermanentPriceRecords(14, 14, 100))
                .willReturn(prevPermPrices);

        final boolean result = targetPreviousPermanentPriceServiceImpl.cleanupPreviousPermanentPriceRecords();

        assertThat(result).isFalse();
        verifyZeroInteractions(modelService);
    }

    private TargetPreviousPermanentPriceModel createMockTargetPreviousPermanentPriceModel(final String variantCode,
            final Date startDate, final Date endDate, final Double itemPrice) {
        final TargetPreviousPermanentPriceModel mockTargetPreviousPermanentPriceModel = mock(TargetPreviousPermanentPriceModel.class);

        given(mockTargetPreviousPermanentPriceModel.getVariantCode()).willReturn(variantCode);
        given(mockTargetPreviousPermanentPriceModel.getStartDate()).willReturn(startDate);
        given(mockTargetPreviousPermanentPriceModel.getEndDate()).willReturn(endDate);
        given(mockTargetPreviousPermanentPriceModel.getPrice()).willReturn(itemPrice);
        given(mockTargetPreviousPermanentPriceModel.getCurrency()).willReturn(currency);

        return mockTargetPreviousPermanentPriceModel;
    }
}

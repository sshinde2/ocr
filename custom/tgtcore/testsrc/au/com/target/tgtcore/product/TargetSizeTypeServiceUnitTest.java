/**
 * 
 */
package au.com.target.tgtcore.product;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtcore.product.dao.TargetSizeTypeDao;
import au.com.target.tgtcore.product.impl.TargetSizeTypeServiceImpl;


/**
 * @author nandini
 * 
 */
@UnitTest
public class TargetSizeTypeServiceUnitTest {

    private TargetSizeTypeServiceImpl sizeTypeService;

    @Mock
    private SizeTypeModel sizeType;

    @Mock
    private TargetSizeTypeDao sizeTypeDao;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        sizeTypeService = new TargetSizeTypeServiceImpl();
        sizeTypeService.setSizeTypeDao(sizeTypeDao);

        given(sizeType.getCode()).willReturn("testcode");
        given(sizeType.getName()).willReturn("testname");
        given(sizeType.getIsDefault()).willReturn(Boolean.TRUE);
        given(sizeType.getIsSizeChartDisplay()).willReturn(Boolean.TRUE);
    }

    @Test
    public void testGetSizeTypeForCodeNull() {
        given(sizeTypeDao.getSizeTypeByCode(null)).willReturn(null);

        final SizeTypeModel size = sizeTypeService.findSizeTypeForCode(null);

        verify(sizeTypeDao).getSizeTypeByCode(null);
        Assert.assertNull(size);
    }

    @Test
    public void testGetSizeTypeForCodeValid()
    {
        given(sizeTypeDao.getSizeTypeByCode("testcode")).willReturn(sizeType);

        final SizeTypeModel size = sizeTypeService.findSizeTypeForCode("testcode");

        verify(sizeTypeDao).getSizeTypeByCode("testcode");
        Assert.assertNotNull(size);
        Assert.assertEquals("testcode", size.getCode());
        Assert.assertEquals("testname", size.getName());
        Assert.assertTrue(size.getIsDefault().booleanValue());
        Assert.assertTrue(size.getIsSizeChartDisplay().booleanValue());
    }

    @Test
    public void testGetSizeTypeForCodeInvalid()
    {
        given(sizeTypeDao.getSizeTypeByCode("invalidCode")).willReturn(null);

        final SizeTypeModel size = sizeTypeService.findSizeTypeForCode("invalidCode");

        verify(sizeTypeDao).getSizeTypeByCode("invalidCode");
        Assert.assertNull(size);
    }

    @Test
    public void testGetDefaultSizeTypeNull() {
        given(sizeTypeDao.getDefaultSizeType()).willReturn(null);

        final SizeTypeModel size = sizeTypeService.getDefaultSizeType();

        verify(sizeTypeDao).getDefaultSizeType();
        Assert.assertNull(size);
    }

    @Test
    public void testGetDefaultSizeTypeValid() {
        given(sizeTypeDao.getDefaultSizeType()).willReturn(sizeType);

        final SizeTypeModel size = sizeTypeService.getDefaultSizeType();

        verify(sizeTypeDao).getDefaultSizeType();
        Assert.assertNotNull(size);
        Assert.assertEquals("testcode", size.getCode());
        Assert.assertEquals("testname", size.getName());
        Assert.assertTrue(size.getIsDefault().booleanValue());
        Assert.assertTrue(size.getIsSizeChartDisplay().booleanValue());
    }

}

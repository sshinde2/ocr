/**
 * 
 */
package au.com.target.tgtcore.product.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.util.StandardDateRange;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.TargetPriceRowModel;
import au.com.target.tgtcore.product.data.TargetPriceRowData;


@UnitTest
public class PriceUpdateRequiredStrategyTest {

    private TargetPriceRowModel prm1;
    private TargetPriceRowModel prm2;

    private TargetPriceRowData tprd1;
    private TargetPriceRowData tprd2;

    private final Collection<PriceRowModel> currentPrices = new ArrayList<>();

    private final List<TargetPriceRowData> futurePrices = new ArrayList<>();

    private final PriceUpdateRequiredStrategy priceUpdateRequiredStrategy = new PriceUpdateRequiredStrategy();

    private StandardDateRange dateRange = null;
    private StandardDateRange dateRange2 = null;
    private StandardDateRange dateRange3 = null;
    private StandardDateRange dateRange4 = null;
    private StandardDateRange dateRange5 = null;
    private StandardDateRange dateRange6 = null;

    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);

        Calendar cal = Calendar.getInstance();
        final Date now = cal.getTime();

        cal.add(Calendar.DATE, -1);
        final Date yesterday = cal.getTime();

        cal = Calendar.getInstance();
        cal.add(Calendar.WEEK_OF_YEAR, 1);
        final Date nextWeek = cal.getTime();
        cal.add(Calendar.WEEK_OF_YEAR, 1);
        final Date inTwoWeeks = cal.getTime();

        cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);
        final Date nextMonth = cal.getTime();
        cal.add(Calendar.DATE, 1);
        final Date nextMonthAndDay = cal.getTime();


        dateRange = new StandardDateRange(now, nextWeek);
        dateRange2 = new StandardDateRange(nextWeek, nextMonth);
        dateRange3 = new StandardDateRange(yesterday, nextWeek);
        dateRange4 = new StandardDateRange(now, inTwoWeeks);
        dateRange5 = new StandardDateRange(now, nextMonth);
        dateRange6 = new StandardDateRange(now, nextMonthAndDay);

        // Create some future prices
        tprd1 = new TargetPriceRowData();
        tprd1.setSellPrice(10);
        tprd1.setWasPrice(11);
        tprd1.setDateRange(dateRange);

        tprd2 = new TargetPriceRowData();
        tprd2.setSellPrice(10);
        tprd2.setWasPrice(11);
        tprd2.setDateRange(dateRange);

        prm1 = mock(TargetPriceRowModel.class);
        prm2 = mock(TargetPriceRowModel.class);
    }

    @Test
    public void testWithNullCurrent() {
        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(null, 1, 2, false, null);
        Assert.assertTrue(bUpdate);
    }

    @Test
    public void testWithCurrentReducedFutures() {
        // Current has one future
        currentPrices.add(prm1);
        currentPrices.add(prm2);

        // Test with null future supplied
        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, 1, 2, false, null);
        Assert.assertTrue(bUpdate);
    }

    @Test
    public void testWithCurrentIncreasedFutures() {
        // Current has one future
        currentPrices.add(prm1);
        currentPrices.add(prm2);

        // Two futures supplied
        futurePrices.add(tprd1);
        futurePrices.add(tprd2);

        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, 1, 2, false,
                futurePrices);
        Assert.assertTrue(bUpdate);
    }

    @Test
    public void testWithNullCurrentEffectivePrice() {

        currentPrices.add(prm1);
        given(prm1.getPrice()).willReturn(null);

        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, 1, 2, false,
                futurePrices);
        Assert.assertTrue(bUpdate);
    }

    @Test
    public void testWithDifferentEffectivePrice() {

        currentPrices.add(prm1);
        given(prm1.getPrice()).willReturn(new Double(1));

        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, 1.5, 1.5, false, null);
        Assert.assertTrue(bUpdate);
    }

    @Test
    public void testWithDifferentEffectiveWasPriceNull() {

        currentPrices.add(prm1);
        given(prm1.getPrice()).willReturn(new Double(1));
        given(prm1.getWasPrice()).willReturn(null);

        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, 1, 2, false, null);
        Assert.assertTrue(bUpdate);
    }

    @Test
    public void testWithNoChangeForEffectiveWasPriceNull() {

        currentPrices.add(prm1);
        given(prm1.getPrice()).willReturn(new Double(1));
        given(prm1.getWasPrice()).willReturn(null);

        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, 1, 1, false, null);
        Assert.assertFalse(bUpdate);
    }

    @Test
    public void testWithDifferentEffectiveWasPriceNotNull() {

        currentPrices.add(prm1);
        given(prm1.getPrice()).willReturn(new Double(1));
        given(prm1.getWasPrice()).willReturn(new Double(2));

        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, 1, 2.5, false, null);
        Assert.assertTrue(bUpdate);
    }

    @Test
    public void testWithDifferentPromoEventFlag() {

        currentPrices.add(prm1);
        given(prm1.getPrice()).willReturn(new Double(1));
        given(prm1.getWasPrice()).willReturn(new Double(2.5));
        given(prm1.getPromoEvent()).willReturn(Boolean.FALSE);

        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, 1, 2.5, true, null);
        Assert.assertTrue(bUpdate);
    }

    @Test
    public void testWithNoDifferentPromoEventFlag() {

        currentPrices.add(prm1);
        given(prm1.getPrice()).willReturn(new Double(1));
        given(prm1.getWasPrice()).willReturn(new Double(2.5));
        given(prm1.getPromoEvent()).willReturn(Boolean.FALSE);

        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, 1, 2.5, false, null);
        Assert.assertFalse(bUpdate);
    }

    @Test
    public void testWithNullPromoEventFlag() {

        currentPrices.add(prm1);
        given(prm1.getPrice()).willReturn(new Double(1));
        given(prm1.getWasPrice()).willReturn(new Double(2.5));
        given(prm1.getPromoEvent()).willReturn(null);

        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, 1, 2.5, false, null);
        Assert.assertTrue(bUpdate);
    }

    @Test
    public void testWithNoChangeForEffectivePrices() {

        currentPrices.add(prm1);
        given(prm1.getPrice()).willReturn(new Double(1));
        given(prm1.getWasPrice()).willReturn(new Double(2));

        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, 1, 2, false, null);
        Assert.assertFalse(bUpdate);
    }

    @Test
    public void testWithFuturePriceCurrentPriceNull() {

        currentPrices.add(prm1);
        currentPrices.add(prm2);
        given(prm1.getPrice()).willReturn(new Double(1));
        given(prm1.getWasPrice()).willReturn(new Double(2));
        given(prm2.getPrice()).willReturn(null);

        futurePrices.add(tprd1);

        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, 1, 2, false,
                futurePrices);
        Assert.assertTrue(bUpdate);
    }

    @Test
    public void testWithFuturePriceCurrentRangeNull() {

        currentPrices.add(prm1);
        currentPrices.add(prm2);
        given(prm1.getPrice()).willReturn(new Double(1));
        given(prm1.getWasPrice()).willReturn(new Double(2));
        given(prm2.getPrice()).willReturn(new Double(1.5));
        given(prm2.getDateRange()).willReturn(null);

        futurePrices.add(tprd1);

        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, 1, 2, false,
                futurePrices);
        Assert.assertTrue(bUpdate);
    }

    @Test
    public void testWithFuturePriceAllMatching() {

        currentPrices.add(prm1);
        currentPrices.add(prm2);
        given(prm1.getPrice()).willReturn(new Double(1));
        given(prm1.getWasPrice()).willReturn(new Double(2));
        given(prm2.getPrice()).willReturn(new Double(10));
        given(prm2.getWasPrice()).willReturn(new Double(11));
        given(prm2.getDateRange()).willReturn(dateRange);

        futurePrices.add(tprd1);

        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, 1, 2, false,
                futurePrices);
        Assert.assertFalse(bUpdate);
    }

    @Test
    public void testWithFuturePriceDifferentPrice() {

        currentPrices.add(prm1);
        currentPrices.add(prm2);
        given(prm1.getPrice()).willReturn(new Double(1));
        given(prm1.getWasPrice()).willReturn(new Double(2));
        given(prm2.getPrice()).willReturn(new Double(9));
        given(prm2.getWasPrice()).willReturn(new Double(11));
        given(prm2.getDateRange()).willReturn(dateRange);

        futurePrices.add(tprd1);

        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, 1, 2, false,
                futurePrices);
        Assert.assertTrue(bUpdate);
    }

    @Test
    public void testWithFuturePriceDifferentStartDate() {

        currentPrices.add(prm1);
        currentPrices.add(prm2);
        given(prm1.getPrice()).willReturn(new Double(1));
        given(prm1.getWasPrice()).willReturn(new Double(2));
        given(prm2.getPrice()).willReturn(new Double(10));
        given(prm2.getWasPrice()).willReturn(new Double(11));
        given(prm2.getDateRange()).willReturn(dateRange2);

        futurePrices.add(tprd1);

        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, 1, 2, false,
                futurePrices);
        Assert.assertTrue(bUpdate);
    }

    @Test
    public void testWithFuturePriceDifferentStartDateAlreadyStarted() {

        // If already started then difference doesn't matter
        currentPrices.add(prm1);
        currentPrices.add(prm2);
        given(prm1.getPrice()).willReturn(new Double(1));
        given(prm1.getWasPrice()).willReturn(new Double(2));
        given(prm2.getPrice()).willReturn(new Double(10));
        given(prm2.getWasPrice()).willReturn(new Double(11));
        given(prm2.getDateRange()).willReturn(dateRange3);

        futurePrices.add(tprd1);

        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, 1, 2, false,
                futurePrices);
        Assert.assertFalse(bUpdate);
    }

    @Test
    public void testWithFuturePriceDifferentEndDateSoon() {

        // If end date is soon then difference matters
        currentPrices.add(prm1);
        currentPrices.add(prm2);
        given(prm1.getPrice()).willReturn(new Double(1));
        given(prm1.getWasPrice()).willReturn(new Double(2));
        given(prm2.getPrice()).willReturn(new Double(10));
        given(prm2.getWasPrice()).willReturn(new Double(11));
        given(prm2.getDateRange()).willReturn(dateRange4);

        futurePrices.add(tprd1);

        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, 1, 2, false,
                futurePrices);
        Assert.assertTrue(bUpdate);
    }

    @Test
    public void testWithFuturePriceDifferentEndDateDistant() {

        // If end date is distant then difference matters not
        prm2.setDateRange(dateRange5);
        currentPrices.add(prm1);
        currentPrices.add(prm2);
        given(prm1.getPrice()).willReturn(new Double(1));
        given(prm1.getWasPrice()).willReturn(new Double(2));
        given(prm2.getPrice()).willReturn(new Double(10));
        given(prm2.getWasPrice()).willReturn(new Double(11));
        given(prm2.getDateRange()).willReturn(dateRange6);

        futurePrices.add(tprd1);

        final boolean bUpdate = priceUpdateRequiredStrategy.isPriceUpdateRequired(currentPrices, 1, 2, false,
                futurePrices);
        Assert.assertFalse(bUpdate);
    }


}

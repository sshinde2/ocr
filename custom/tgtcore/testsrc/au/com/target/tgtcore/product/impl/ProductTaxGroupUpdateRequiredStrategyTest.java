/**
 * 
 */
package au.com.target.tgtcore.product.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.enums.ProductTaxGroup;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


@UnitTest
public class ProductTaxGroupUpdateRequiredStrategyTest {


    @InjectMocks
    private final ProductTaxGroupUpdateRequiredStrategy taxGroupUpdateRequiredStrategy = new ProductTaxGroupUpdateRequiredStrategy();

    @Mock
    private ProductModel product;

    @Mock
    private ProductTaxGroup ptg;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        taxGroupUpdateRequiredStrategy.setPtgPrefix("aus-gst-");
    }

    @Test
    public void testWithNullPtg() {

        // If the product currently has a null PTG then it should be updated
        given(product.getEurope1PriceFactory_PTG()).willReturn(null);
        Assert.assertTrue(taxGroupUpdateRequiredStrategy.isTaxGroupUpdateRequired(product, 1000));
    }

    @Test
    public void testWithNullCode() {

        // If the product currently has a null PTG code then it should be updated
        given(product.getEurope1PriceFactory_PTG()).willReturn(ptg);
        given(ptg.getCode()).willReturn(null);
        Assert.assertTrue(taxGroupUpdateRequiredStrategy.isTaxGroupUpdateRequired(product, 1000));
    }

    @Test
    public void testWithNoMatch() {

        // If the product currently has a PTG code for 1000 then it should be updated if supplied rate is different
        given(product.getEurope1PriceFactory_PTG()).willReturn(ptg);
        given(ptg.getCode()).willReturn("aus-gst-1000");

        Assert.assertTrue(taxGroupUpdateRequiredStrategy.isTaxGroupUpdateRequired(product, 1001));
        Assert.assertFalse(taxGroupUpdateRequiredStrategy.isTaxGroupUpdateRequired(product, 1000));
    }


}

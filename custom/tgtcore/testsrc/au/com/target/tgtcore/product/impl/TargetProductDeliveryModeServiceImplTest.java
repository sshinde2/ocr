/**
 * 
 */
package au.com.target.tgtcore.product.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.order.ZoneDeliveryModeService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.util.TargetProductMockHelper;


/**
 * @author pthoma20
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductDeliveryModeServiceImplTest {
    @Mock
    private CategoryService categoryService;


    @Mock
    private ZoneDeliveryModeService zoneDeliveryModeService;

    @InjectMocks
    private final TargetProductDeliveryModeServiceImpl targetProductDeliveryModeService = new TargetProductDeliveryModeServiceImpl();

    private final CatalogVersionModel catalogVersionModel = Mockito.mock(CatalogVersionModel.class);

    private TargetProductMockHelper targetProductMockHelper;



    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        targetProductMockHelper = new TargetProductMockHelper(catalogVersionModel);
        when(zoneDeliveryModeService.getDeliveryModeForCode("express-delivery")).thenReturn(
                targetProductMockHelper.createDeliveryModeModel("express-delivery"));


    }


    /**
     * Test Positive Scenario - is express delivery eligible and is not bulky
     */
    @Test
    public void testPositiveExpressDeliveryScenario() {


        //Test positive scenario - Is eligible for Express delivery and is not bulky
        final TargetProductModel targetProductModel = targetProductMockHelper.createTargetProduct(
                new Integer(457),
                "DM1", 0, 2,
                null,
                Boolean.TRUE,
                Boolean.FALSE);

        final TargetMerchDepartmentModel targetMerchDepModel = targetProductMockHelper
                .createDepartmentCategoryModel("457",
                        Boolean.TRUE);

        when(categoryService.getCategoryForCode(catalogVersionModel, "457")).thenReturn(
                targetMerchDepModel);


        Assert.assertTrue(targetProductDeliveryModeService
                .processExpressDeliveryModeUpdation(targetProductModel, targetMerchDepModel));


    }



    /**
     * Test negative scenario - Is 'not' eligible for Express delivery and is not bulky
     */
    @Test
    public void testNegativeExpressEligiblityScenario() {



        final TargetProductModel targetProductModel = targetProductMockHelper.createTargetProduct(
                new Integer(457),
                "DM1", 0, 2,
                null,
                Boolean.TRUE,
                Boolean.FALSE);

        final TargetMerchDepartmentModel targetMerchDepModel = targetProductMockHelper
                .createDepartmentCategoryModel("457",
                        Boolean.FALSE);

        when(categoryService.getCategoryForCode(catalogVersionModel, "457")).thenReturn(
                targetMerchDepModel);


        Assert.assertFalse(targetProductDeliveryModeService
                .processExpressDeliveryModeUpdation(targetProductModel, targetMerchDepModel));


    }

    /**
     * Test negative scenario - Is eligible for Express delivery and is 'bulky'
     */
    @Test
    public void testNegativeBulkyScenario() {

        //Test positive scenario - Is eligible for Express delivery and is not bulky
        final TargetProductModel targetProductModel = targetProductMockHelper.createTargetProduct(
                new Integer(457),
                "DM1", 0, 2,
                null,
                Boolean.TRUE,
                Boolean.TRUE);

        final TargetMerchDepartmentModel targetMerchDepModel = targetProductMockHelper
                .createDepartmentCategoryModel("457",
                        Boolean.TRUE);

        when(categoryService.getCategoryForCode(catalogVersionModel, "457")).thenReturn(
                targetMerchDepModel);


        Assert.assertFalse(targetProductDeliveryModeService
                .processExpressDeliveryModeUpdation(targetProductModel, targetMerchDepModel));


    }
}

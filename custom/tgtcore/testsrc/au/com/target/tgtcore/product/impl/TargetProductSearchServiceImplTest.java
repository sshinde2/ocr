package au.com.target.tgtcore.product.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.fluent.FluentStockService;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.price.TargetCommercePriceService;
import au.com.target.tgtcore.product.dao.TargetProductSearchServiceDao;
import au.com.target.tgtcore.stock.TargetStockService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductSearchServiceImplTest {

    private static final Logger LOG = Logger.getLogger(TargetProductSearchServiceImpl.class);

    private static final String PRODUCT_CODE = "P1040_blue_S";
    private static final String SWATCH_CODE = "black";

    @Mock
    private TargetProductSearchServiceDao targetProductSearchServiceDao;

    @Mock
    private CatalogVersionService catalogVersionService;

    @Mock
    private TargetCommercePriceService targetCommercePriceService;

    @Mock
    private TargetStockService targetStockService;

    @Mock
    private FluentStockService fluentStockService;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private CatalogVersionModel catalogVersion;

    @InjectMocks
    @Spy
    private final TargetProductSearchServiceImpl targetProductSearchServiceImpl = new TargetProductSearchServiceImpl();

    @Test
    public void testTgtCSSearchCustomerTest() {
        final List resList = getTargetSizeVariantProductModels();
        given(targetProductSearchServiceImpl.getTargetSizeVariantProductModelByCode(BDDMockito.anyString()))
                .willReturn(resList);
        try {
            final List<TargetSizeVariantProductModel> targetSizes = targetProductSearchServiceImpl
                    .getTargetSizeVariantProductModelByCode("P1040_blue_S");
            assertThat(targetSizes).isNotNull();
            for (final TargetSizeVariantProductModel targetSize : targetSizes) {
                assertThat(targetSize.getCode()).isEqualTo("P1040_blue_S");
            }
        }
        catch (final Exception e) {
            LOG.error("Exception while test get ", e);
        }

    }

    /**
     * Verifies that product service uses staged product catalog version and
     * {@link TargetProductSearchServiceDao#getByBaseProductAndSwatch(String, String, CatalogVersionModel)} to find
     * variants by base product and swatch code.
     */
    @Test
    public void testGetByBaseProductAndSwatch() {
        given(catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION))
                        .willReturn(catalogVersion);
        targetProductSearchServiceImpl.getColourVariantByBaseProductAndSwatch(PRODUCT_CODE, SWATCH_CODE);
        verify(targetProductSearchServiceDao).getByBaseProductAndSwatch(PRODUCT_CODE, SWATCH_CODE, catalogVersion);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsProductAvailableToSell() {
        targetProductSearchServiceImpl.isProductAvailableToSell(null);
    }

    @Test
    public void testIsProductAvailableToSellSizeButNoVariants() {
        final TargetSizeVariantProductModel product = new TargetSizeVariantProductModel();

        willReturn(Boolean.TRUE).given(targetProductSearchServiceImpl)
                .doesProductHaveCorrectStatusPriceAndStock(product);
        // one sellable variant is purchasable
        assertThat(targetProductSearchServiceImpl.isProductAvailableToSell(product)).isTrue();

        willReturn(Boolean.FALSE).given(targetProductSearchServiceImpl)
                .doesProductHaveCorrectStatusPriceAndStock(product);
        // no sellable variant is purchasable
        assertThat(targetProductSearchServiceImpl.isProductAvailableToSell(product)).isFalse();
    }

    @Test
    public void testIsProductAvailableToSellColourButNoVariants() {
        final TargetColourVariantProductModel product = new TargetColourVariantProductModel();

        willReturn(Boolean.TRUE).given(targetProductSearchServiceImpl)
                .doesProductHaveCorrectStatusPriceAndStock(product);
        // one sellable variant is purchasable
        assertThat(targetProductSearchServiceImpl.isProductAvailableToSell(product)).isTrue();

        willReturn(Boolean.FALSE).given(targetProductSearchServiceImpl)
                .doesProductHaveCorrectStatusPriceAndStock(product);
        // no sellable variant is purchasable
        assertThat(targetProductSearchServiceImpl.isProductAvailableToSell(product)).isFalse();
    }

    @Test
    public void testIsProductAvailableToSellColourWithSizeVariants() {
        final TargetColourVariantProductModel product = new TargetColourVariantProductModel();
        final TargetSizeVariantProductModel sizeProduct1 = new TargetSizeVariantProductModel();
        final TargetSizeVariantProductModel sizeProduct2 = new TargetSizeVariantProductModel();

        final List<VariantProductModel> variants = new ArrayList<>();
        variants.add(sizeProduct1);
        variants.add(sizeProduct2);
        product.setVariants(variants);

        willReturn(Boolean.FALSE).given(targetProductSearchServiceImpl)
                .doesProductHaveCorrectStatusPriceAndStock(sizeProduct1);
        willReturn(Boolean.TRUE).given(targetProductSearchServiceImpl)
                .doesProductHaveCorrectStatusPriceAndStock(sizeProduct2);
        // one sellable variant is purchasable
        assertThat(targetProductSearchServiceImpl.isProductAvailableToSell(product)).isTrue();

        willReturn(Boolean.FALSE).given(targetProductSearchServiceImpl)
                .doesProductHaveCorrectStatusPriceAndStock(sizeProduct2);
        // no sellable variant is purchasable
        assertThat(targetProductSearchServiceImpl.isProductAvailableToSell(product)).isFalse();
    }

    @Test
    public void testIsProductAvailableToSellBaseWithColourVariants() {
        final TargetProductModel product = new TargetProductModel();
        final TargetColourVariantProductModel colourProduct1 = new TargetColourVariantProductModel();
        final TargetColourVariantProductModel colourProduct2 = new TargetColourVariantProductModel();

        final List<VariantProductModel> variants = new ArrayList<>();
        variants.add(colourProduct1);
        variants.add(colourProduct2);
        product.setVariants(variants);

        willReturn(Boolean.FALSE).given(targetProductSearchServiceImpl)
                .doesProductHaveCorrectStatusPriceAndStock(colourProduct1);
        willReturn(Boolean.TRUE).given(targetProductSearchServiceImpl)
                .doesProductHaveCorrectStatusPriceAndStock(colourProduct2);
        // one sellable variant is purchasable
        assertThat(targetProductSearchServiceImpl.isProductAvailableToSell(product)).isTrue();

        willReturn(Boolean.FALSE).given(targetProductSearchServiceImpl)
                .doesProductHaveCorrectStatusPriceAndStock(colourProduct2);
        // no sellable variant is purchasable
        assertThat(targetProductSearchServiceImpl.isProductAvailableToSell(product)).isFalse();
    }

    @Test
    public void testIsProductAvailableToSellBaseWithColourAndSizeVariants() {
        final TargetProductModel product = new TargetProductModel();
        final TargetColourVariantProductModel colourProduct1 = new TargetColourVariantProductModel();
        final TargetColourVariantProductModel colourProduct2 = new TargetColourVariantProductModel();
        final TargetSizeVariantProductModel sizeProduct1 = new TargetSizeVariantProductModel();
        final TargetSizeVariantProductModel sizeProduct2 = new TargetSizeVariantProductModel();

        final List<VariantProductModel> colourVariants = new ArrayList<>();
        colourVariants.add(colourProduct1);
        colourVariants.add(colourProduct2);
        final List<VariantProductModel> sizeVariants = new ArrayList<>();
        sizeVariants.add(sizeProduct1);
        sizeVariants.add(sizeProduct2);

        product.setVariants(colourVariants);
        colourProduct2.setVariants(sizeVariants);

        willReturn(Boolean.FALSE).given(targetProductSearchServiceImpl)
                .doesProductHaveCorrectStatusPriceAndStock(colourProduct1);
        willReturn(Boolean.FALSE).given(targetProductSearchServiceImpl)
                .doesProductHaveCorrectStatusPriceAndStock(sizeProduct1);
        willReturn(Boolean.TRUE).given(targetProductSearchServiceImpl)
                .doesProductHaveCorrectStatusPriceAndStock(sizeProduct2);
        // one sellable variant is purchasable
        Assert.assertTrue(targetProductSearchServiceImpl.isProductAvailableToSell(product));

        willReturn(Boolean.FALSE).given(targetProductSearchServiceImpl)
                .doesProductHaveCorrectStatusPriceAndStock(sizeProduct2);
        // no sellable variant is purchasable
        assertThat(targetProductSearchServiceImpl.isProductAvailableToSell(product)).isFalse();
    }

    @Test
    public void testDoesProductHaveCorrectStatusPriceAndStock() throws TargetUnknownIdentifierException {
        final ProductModel productModel = new ProductModel();
        productModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);

        willReturn(setupPriceInfo()).given(targetCommercePriceService)
                .getWebPriceForProduct(productModel);
        willReturn(StockLevelStatus.INSTOCK).given(targetStockService)
                .getProductStatus(productModel);

        assertThat(targetProductSearchServiceImpl.doesProductHaveCorrectStatusPriceAndStock(productModel))
                .isTrue();
        verifyNoMoreInteractions(fluentStockService);
    }

    @Test
    public void testDoesDisplayProductHaveCorrectStatusPriceAndStock() throws TargetUnknownIdentifierException {
        final TargetColourVariantProductModel productModel = new TargetColourVariantProductModel();
        productModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
        productModel.setDisplayOnly(Boolean.TRUE);

        willReturn(setupPriceInfo()).given(targetCommercePriceService)
                .getWebPriceForProduct(productModel);
        willReturn(StockLevelStatus.OUTOFSTOCK).given(targetStockService)
                .getProductStatus(productModel);

        assertThat(targetProductSearchServiceImpl.doesProductHaveCorrectStatusPriceAndStock(productModel))
                .isTrue();
        verifyNoMoreInteractions(fluentStockService);
    }

    private PriceInformation setupPriceInfo() {
        return new PriceInformation(new PriceValue("AUD", 10d, true));
    }

    @Test
    public void testDoesProductHaveCorrectStatusPriceAndStockNotApproved() throws TargetUnknownIdentifierException {
        final ProductModel productModel = new ProductModel();
        productModel.setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);

        assertThat(targetProductSearchServiceImpl.doesProductHaveCorrectStatusPriceAndStock(productModel))
                .isFalse();
    }

    @Test
    public void testDoesProductHaveCorrectStatusPriceAndStockNoPrice() throws TargetUnknownIdentifierException {
        final ProductModel productModel = new ProductModel();
        productModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);

        willReturn(null).given(targetCommercePriceService)
                .getWebPriceForProduct(productModel);

        assertThat(targetProductSearchServiceImpl.doesProductHaveCorrectStatusPriceAndStock(productModel))
                .isFalse();
        verifyNoMoreInteractions(fluentStockService);
    }

    @Test
    public void testDoesProductHaveCorrectStatusPriceAndStockNoStock() throws TargetUnknownIdentifierException {
        final ProductModel productModel = new ProductModel();
        productModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);

        willReturn(setupPriceInfo()).given(targetCommercePriceService)
                .getWebPriceForProduct(productModel);
        willReturn(null).given(targetStockService).getProductStatus(productModel);

        assertThat(targetProductSearchServiceImpl.doesProductHaveCorrectStatusPriceAndStock(productModel))
                .isFalse();
        verifyNoMoreInteractions(fluentStockService);
    }

    @Test
    public void testDoesProductHaveCorrectStatusPriceAndStockOutOfStock() throws TargetUnknownIdentifierException {
        final ProductModel productModel = new ProductModel();
        productModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);

        willReturn(setupPriceInfo()).given(targetCommercePriceService)
                .getWebPriceForProduct(productModel);
        willReturn(StockLevelStatus.OUTOFSTOCK).given(targetStockService)
                .getProductStatus(productModel);

        assertThat(targetProductSearchServiceImpl.doesProductHaveCorrectStatusPriceAndStock(productModel))
                .isFalse();
        verifyNoMoreInteractions(fluentStockService);
    }

    @Test
    public void testDoesProductHaveCorrectStatusPriceAndStockUsingFluent() throws TargetUnknownIdentifierException {
        final ProductModel productModel = new ProductModel();
        productModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);

        willReturn(setupPriceInfo()).given(targetCommercePriceService)
                .getWebPriceForProduct(productModel);
        willReturn(StockLevelStatus.INSTOCK).given(fluentStockService)
                .getProductOnlineStockStatus(productModel.getCode());
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService).isFeatureEnabled("fluent");

        assertThat(targetProductSearchServiceImpl.doesProductHaveCorrectStatusPriceAndStock(productModel))
                .isTrue();
        verifyNoMoreInteractions(targetStockService);
    }

    protected List<TargetSizeVariantProductModel> getTargetSizeVariantProductModels() {
        final List<TargetSizeVariantProductModel> resList = new ArrayList<>();
        resList.add(createResultEntry("P1040_blue_S"));
        return resList;
    }

    protected TargetSizeVariantProductModel createResultEntry(final String PK) {
        final TargetSizeVariantProductModel item = new TargetSizeVariantProductModel();
        item.setCode(PK);
        return item;
    }

    /**
     * Verifies that product search service returns a list of TargetProductModels and also validates the data returned.
     */
    @Test
    public void testGetAllTargetProducts() {
        final List resList = getTargetProductModels();
        given(targetProductSearchServiceImpl.getAllTargetProductsWithNoDepartment(
                TgtCoreConstants.Catalog.OFFLINE_VERSION, 1, 500)).willReturn(resList);
        final List<TargetProductModel> targetProducts = targetProductSearchServiceImpl
                .getAllTargetProductsWithNoDepartment(TgtCoreConstants.Catalog.OFFLINE_VERSION, 1, 500);
        assertThat(targetProducts).isNotNull();
        for (final TargetProductModel targetProduct : targetProducts) {
            assertThat(targetProduct.getCode()).isEqualTo("CP1040");
        }

    }

    protected List<TargetProductModel> getTargetProductModels() {
        final List<TargetProductModel> resList = new ArrayList<>();
        resList.add(createTargetProductResult("CP1040"));
        return resList;
    }

    protected TargetProductModel createTargetProductResult(final String PK) {
        final TargetProductModel tgtPdtModel = new TargetProductModel();
        tgtPdtModel.setCode(PK);
        return tgtPdtModel;
    }

}
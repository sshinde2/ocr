package au.com.target.tgtcore.product.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.product.TargetProductSearchService;
import au.com.target.tgtcore.product.dao.TargetProductDao;
import au.com.target.tgtcore.stock.TargetStockService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductServiceImplTest {

    private static final String TEST_EAN = "testEan";

    @Mock
    private CatalogVersionService mockCatalogVersionService;

    @Mock
    private TargetProductDao mockTargetProductDao;

    @Mock
    private CatalogVersionModel mockStagedCatalogVersionModel;

    @Mock
    private CatalogVersionModel mockOnlineCatalogVersionModel;

    @Mock
    private TargetProductSearchService targetProductSearchService;

    @Mock
    private AbstractTargetVariantProductModel mockTargetVariantProduct;

    @Mock
    private TargetColourVariantProductModel mockColourVariantProduct;

    @Mock
    private TargetSizeVariantProductModel mockSizeVariantProduct;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private TargetStockService mockTargetStockService;

    @InjectMocks
    @Spy
    private final TargetProductServiceImpl targetProductServiceImpl = new TargetProductServiceImpl();

    @Before
    public void setUp() {
        given(mockCatalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION)).willReturn(mockStagedCatalogVersionModel);
        given(mockCatalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.ONLINE_VERSION)).willReturn(mockOnlineCatalogVersionModel);
    }

    @Test
    public void testGetBulkyBoardProducts() throws Exception {
        given(mockTargetVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockColourVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockSizeVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);

        final VariantProductModel mockVariantProduct = mock(VariantProductModel.class);
        given(mockVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);

        final List<VariantProductModel> productVariants = new ArrayList<>();
        productVariants.add(mockTargetVariantProduct);
        productVariants.add(mockColourVariantProduct);
        productVariants.add(mockSizeVariantProduct);
        productVariants.add(mockVariantProduct);

        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);
        given(mockTargetProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockTargetProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);
        given(mockTargetProduct.getVariants()).willReturn(productVariants);

        final List<TargetProductModel> targetProducts = new ArrayList<>();
        targetProducts.add(mockTargetProduct);

        given(mockTargetProductDao.findBulkyBoardProducts(mockStagedCatalogVersionModel)).willReturn(targetProducts);

        final List<AbstractTargetVariantProductModel> result = targetProductServiceImpl.getBulkyBoardProducts();

        assertThat(result).isNotEmpty().containsOnly(mockTargetVariantProduct, mockColourVariantProduct,
                mockSizeVariantProduct);
    }

    @Test
    public void testGetBulkyBoardProductsWithNoProducts() {
        given(mockTargetProductDao.findBulkyBoardProducts(mockStagedCatalogVersionModel)).willReturn(null);

        final List<AbstractTargetVariantProductModel> result = targetProductServiceImpl.getBulkyBoardProducts();

        assertThat(result).isNotNull().isEmpty();
    }

    @Test
    public void testGetBulkyBoardProductsWithProductNoVariants() {
        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);
        given(mockTargetProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);
        given(mockTargetProduct.getVariants()).willReturn(null);

        final List<TargetProductModel> targetProducts = new ArrayList<>();
        targetProducts.add(mockTargetProduct);

        given(mockTargetProductDao.findBulkyBoardProducts(mockStagedCatalogVersionModel)).willReturn(targetProducts);

        final List<AbstractTargetVariantProductModel> result = targetProductServiceImpl.getBulkyBoardProducts();

        assertThat(result).isNotNull().isEmpty();
    }

    @Test
    public void testGetBulkyBoardProductsWithNotApprovedBaseProduct() throws Exception {
        given(mockTargetVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockColourVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockSizeVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);

        final List<VariantProductModel> productVariants = new ArrayList<>();
        productVariants.add(mockTargetVariantProduct);
        productVariants.add(mockColourVariantProduct);
        productVariants.add(mockSizeVariantProduct);

        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);
        given(mockTargetProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.UNAPPROVED);
        given(mockTargetProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);
        given(mockTargetProduct.getVariants()).willReturn(productVariants);

        final List<TargetProductModel> targetProducts = new ArrayList<>();
        targetProducts.add(mockTargetProduct);

        given(mockTargetProductDao.findBulkyBoardProducts(mockStagedCatalogVersionModel)).willReturn(targetProducts);

        final List<AbstractTargetVariantProductModel> result = targetProductServiceImpl.getBulkyBoardProducts();

        assertThat(result).isNotNull().isEmpty();
    }

    @Test
    public void testGetBulkyBoardProductsWithSomeNotApprovedVariants() throws Exception {
        given(mockTargetVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockColourVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.UNAPPROVED);
        given(mockSizeVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.CHECK);

        final List<VariantProductModel> productVariants = new ArrayList<>();
        productVariants.add(mockTargetVariantProduct);
        productVariants.add(mockColourVariantProduct);
        productVariants.add(mockSizeVariantProduct);

        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);
        given(mockTargetProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockTargetProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);
        given(mockTargetProduct.getVariants()).willReturn(productVariants);

        final List<TargetProductModel> targetProducts = new ArrayList<>();
        targetProducts.add(mockTargetProduct);

        given(mockTargetProductDao.findBulkyBoardProducts(mockStagedCatalogVersionModel)).willReturn(targetProducts);

        final List<AbstractTargetVariantProductModel> result = targetProductServiceImpl.getBulkyBoardProducts();

        assertThat(result).isNotEmpty().containsOnly(mockTargetVariantProduct);
    }

    @Test
    public void testGetBulkyBoardProductsWithBaseProductWithOfflineDateInPast() throws Exception {
        given(mockTargetVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockColourVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockSizeVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);

        final List<VariantProductModel> productVariants = new ArrayList<>();
        productVariants.add(mockTargetVariantProduct);
        productVariants.add(mockColourVariantProduct);
        productVariants.add(mockSizeVariantProduct);

        final DateTime yesterday = DateTime.now().minusDays(1);

        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);
        given(mockTargetProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockTargetProduct.getOfflineDate()).willReturn(yesterday.toDate());
        given(mockTargetProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);
        given(mockTargetProduct.getVariants()).willReturn(productVariants);

        final List<TargetProductModel> targetProducts = new ArrayList<>();
        targetProducts.add(mockTargetProduct);

        given(mockTargetProductDao.findBulkyBoardProducts(mockStagedCatalogVersionModel)).willReturn(targetProducts);

        final List<AbstractTargetVariantProductModel> result = targetProductServiceImpl.getBulkyBoardProducts();

        assertThat(result).isNotNull().isEmpty();
    }

    @Test
    public void testGetBulkyBoardProductsWithBaseProductWithOnlineDateInFuture() throws Exception {
        given(mockTargetVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockColourVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockSizeVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);

        final List<VariantProductModel> productVariants = new ArrayList<>();
        productVariants.add(mockTargetVariantProduct);
        productVariants.add(mockColourVariantProduct);
        productVariants.add(mockSizeVariantProduct);

        final DateTime tomorrow = DateTime.now().plusDays(1);

        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);
        given(mockTargetProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockTargetProduct.getOnlineDate()).willReturn(tomorrow.toDate());
        given(mockTargetProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);
        given(mockTargetProduct.getVariants()).willReturn(productVariants);

        final List<TargetProductModel> targetProducts = new ArrayList<>();
        targetProducts.add(mockTargetProduct);

        given(mockTargetProductDao.findBulkyBoardProducts(mockStagedCatalogVersionModel)).willReturn(targetProducts);

        final List<AbstractTargetVariantProductModel> result = targetProductServiceImpl.getBulkyBoardProducts();

        assertThat(result).isNotNull().isEmpty();
    }

    @Test
    public void testGetBulkyBoardProductsWithProductVariantsWithOfflineDateInPast() throws Exception {
        final DateTime yesterday = DateTime.now().minusDays(1);

        given(mockTargetVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockTargetVariantProduct.getOfflineDate()).willReturn(yesterday.toDate());
        given(mockColourVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockColourVariantProduct.getOfflineDate()).willReturn(yesterday.toDate());
        given(mockSizeVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockSizeVariantProduct.getOfflineDate()).willReturn(yesterday.toDate());

        final List<VariantProductModel> productVariants = new ArrayList<>();
        productVariants.add(mockTargetVariantProduct);
        productVariants.add(mockColourVariantProduct);
        productVariants.add(mockSizeVariantProduct);

        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);
        given(mockTargetProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockTargetProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);
        given(mockTargetProduct.getVariants()).willReturn(productVariants);

        final List<TargetProductModel> targetProducts = new ArrayList<>();
        targetProducts.add(mockTargetProduct);

        given(mockTargetProductDao.findBulkyBoardProducts(mockStagedCatalogVersionModel)).willReturn(targetProducts);

        final List<AbstractTargetVariantProductModel> result = targetProductServiceImpl.getBulkyBoardProducts();

        assertThat(result).isNotNull().isEmpty();
    }

    @Test
    public void testGetBulkyBoardProductsWithProductVariantsWithOnlineDateInFuture() throws Exception {
        final DateTime tomorrow = DateTime.now().plusDays(1);

        given(mockTargetVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockTargetVariantProduct.getOnlineDate()).willReturn(tomorrow.toDate());
        given(mockColourVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockColourVariantProduct.getOnlineDate()).willReturn(tomorrow.toDate());
        given(mockSizeVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockSizeVariantProduct.getOnlineDate()).willReturn(tomorrow.toDate());

        final List<VariantProductModel> productVariants = new ArrayList<>();
        productVariants.add(mockTargetVariantProduct);
        productVariants.add(mockColourVariantProduct);
        productVariants.add(mockSizeVariantProduct);

        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);
        given(mockTargetProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockTargetProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);
        given(mockTargetProduct.getVariants()).willReturn(productVariants);

        final List<TargetProductModel> targetProducts = new ArrayList<>();
        targetProducts.add(mockTargetProduct);

        given(mockTargetProductDao.findBulkyBoardProducts(mockStagedCatalogVersionModel)).willReturn(targetProducts);

        final List<AbstractTargetVariantProductModel> result = targetProductServiceImpl.getBulkyBoardProducts();

        assertThat(result).isNotNull().isEmpty();
    }

    @Test
    public void testGetBulkyBoardProductsWithBaseProductWithOfflineDateInFuture() throws Exception {
        given(mockTargetVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockColourVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockSizeVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);

        final List<VariantProductModel> productVariants = new ArrayList<>();
        productVariants.add(mockTargetVariantProduct);
        productVariants.add(mockColourVariantProduct);
        productVariants.add(mockSizeVariantProduct);

        final DateTime tomorrow = DateTime.now().plusDays(1);

        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);
        given(mockTargetProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockTargetProduct.getOfflineDate()).willReturn(tomorrow.toDate());
        given(mockTargetProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);
        given(mockTargetProduct.getVariants()).willReturn(productVariants);

        final List<TargetProductModel> targetProducts = new ArrayList<>();
        targetProducts.add(mockTargetProduct);

        given(mockTargetProductDao.findBulkyBoardProducts(mockStagedCatalogVersionModel)).willReturn(targetProducts);

        final List<AbstractTargetVariantProductModel> result = targetProductServiceImpl.getBulkyBoardProducts();

        assertThat(result).isNotEmpty().containsOnly(mockTargetVariantProduct, mockColourVariantProduct,
                mockSizeVariantProduct);
    }

    @Test
    public void testGetBulkyBoardProductsWithBaseProductWithOnlineDateInPast() throws Exception {
        given(mockTargetVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockColourVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockSizeVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);

        final List<VariantProductModel> productVariants = new ArrayList<>();
        productVariants.add(mockTargetVariantProduct);
        productVariants.add(mockColourVariantProduct);
        productVariants.add(mockSizeVariantProduct);

        final DateTime yesterday = DateTime.now().minusDays(1);

        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);
        given(mockTargetProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockTargetProduct.getOnlineDate()).willReturn(yesterday.toDate());
        given(mockTargetProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);
        given(mockTargetProduct.getVariants()).willReturn(productVariants);

        final List<TargetProductModel> targetProducts = new ArrayList<>();
        targetProducts.add(mockTargetProduct);

        given(mockTargetProductDao.findBulkyBoardProducts(mockStagedCatalogVersionModel)).willReturn(targetProducts);

        final List<AbstractTargetVariantProductModel> result = targetProductServiceImpl.getBulkyBoardProducts();

        assertThat(result).isNotEmpty().containsOnly(mockTargetVariantProduct, mockColourVariantProduct,
                mockSizeVariantProduct);
    }

    @Test
    public void testGetBulkyBoardProductsWithProductVariantsWithOfflineDateInFuture() throws Exception {
        final DateTime tomorrow = DateTime.now().plusDays(1);

        given(mockTargetVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockTargetVariantProduct.getOfflineDate()).willReturn(tomorrow.toDate());
        given(mockColourVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockColourVariantProduct.getOfflineDate()).willReturn(tomorrow.toDate());
        given(mockSizeVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockSizeVariantProduct.getOfflineDate()).willReturn(tomorrow.toDate());

        final List<VariantProductModel> productVariants = new ArrayList<>();
        productVariants.add(mockTargetVariantProduct);
        productVariants.add(mockColourVariantProduct);
        productVariants.add(mockSizeVariantProduct);

        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);
        given(mockTargetProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockTargetProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);
        given(mockTargetProduct.getVariants()).willReturn(productVariants);

        final List<TargetProductModel> targetProducts = new ArrayList<>();
        targetProducts.add(mockTargetProduct);

        given(mockTargetProductDao.findBulkyBoardProducts(mockStagedCatalogVersionModel)).willReturn(targetProducts);

        final List<AbstractTargetVariantProductModel> result = targetProductServiceImpl.getBulkyBoardProducts();

        assertThat(result).isNotEmpty().containsOnly(mockTargetVariantProduct, mockColourVariantProduct,
                mockSizeVariantProduct);
    }

    @Test
    public void testGetBulkyBoardProductsWithProductVariantsWithOnlineDateInPast() throws Exception {
        final DateTime yesterday = DateTime.now().minusDays(1);

        given(mockTargetVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockTargetVariantProduct.getOnlineDate()).willReturn(yesterday.toDate());
        given(mockColourVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockColourVariantProduct.getOnlineDate()).willReturn(yesterday.toDate());
        given(mockSizeVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockSizeVariantProduct.getOnlineDate()).willReturn(yesterday.toDate());

        final List<VariantProductModel> productVariants = new ArrayList<>();
        productVariants.add(mockTargetVariantProduct);
        productVariants.add(mockColourVariantProduct);
        productVariants.add(mockSizeVariantProduct);

        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);
        given(mockTargetProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(mockTargetProduct.getBulkyBoardProduct()).willReturn(Boolean.TRUE);
        given(mockTargetProduct.getVariants()).willReturn(productVariants);

        final List<TargetProductModel> targetProducts = new ArrayList<>();
        targetProducts.add(mockTargetProduct);

        given(mockTargetProductDao.findBulkyBoardProducts(mockStagedCatalogVersionModel)).willReturn(targetProducts);

        final List<AbstractTargetVariantProductModel> result = targetProductServiceImpl.getBulkyBoardProducts();

        assertThat(result).isNotEmpty().containsOnly(mockTargetVariantProduct, mockColourVariantProduct,
                mockSizeVariantProduct);
    }

    @Test
    public void testGetProductsForMerchDeptCodeNullmerchCode() {
        final TargetMerchDepartmentModel merchModel = null;
        final List<TargetColourVariantProductModel> result = targetProductServiceImpl.getProductsForMerchDeptCode(
                merchModel, 1, 10);
        assertThat(result).isEmpty();
    }

    @Test
    public void testGetProductsForMerchDeptCode() {
        final TargetMerchDepartmentModel merchModel = mock(TargetMerchDepartmentModel.class);
        final TargetColourVariantProductModel productModel = mock(TargetColourVariantProductModel.class);
        final List<TargetColourVariantProductModel> targetProducts = new ArrayList<>();
        targetProducts.add(productModel);
        given(mockTargetProductDao.findProductByMerchDepartment(merchModel, 1, 10)).willReturn(targetProducts);
        final List<TargetColourVariantProductModel> result = targetProductServiceImpl.getProductsForMerchDeptCode(
                merchModel, 1, 10);
        assertThat(result).isNotEmpty().containsOnly(productModel);
    }

    @Test
    public void testSetOnlineDateIfApplicableInvalidProduct() throws Exception {
        given(Boolean.valueOf(targetProductSearchService.isProductAvailableToSell(mockColourVariantProduct)))
                .willReturn(Boolean.FALSE);

        targetProductServiceImpl.setOnlineDateIfApplicable(mockColourVariantProduct);

        verify(mockColourVariantProduct, never()).setOnlineDate(any(Date.class));
        verify(modelService, never()).save(mockColourVariantProduct);
    }

    @Test
    public void testSetOnlineDateIfApplicableValidProductWithNoVariantsFeatureOff() throws Exception {
        willReturn(Boolean.FALSE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.NEWIN_ONLINEFROM);

        targetProductServiceImpl.setOnlineDateIfApplicable(mockColourVariantProduct);

        verify(mockColourVariantProduct, never()).setOnlineDate(any(Date.class));
        verify(modelService, never()).save(mockColourVariantProduct);
    }

    @Test
    public void testSetOnlineDateIfApplicableValidProductWithNoVariantsAndNullDate() throws Exception {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.NEWIN_ONLINEFROM);
        given(Boolean.valueOf(targetProductSearchService.isProductAvailableToSell(mockColourVariantProduct)))
                .willReturn(Boolean.TRUE);
        given(mockColourVariantProduct.getOnlineDate()).willReturn(null);

        targetProductServiceImpl.setOnlineDateIfApplicable(mockColourVariantProduct);

        verify(mockColourVariantProduct).setOnlineDate(any(Date.class));
        verify(modelService).save(mockColourVariantProduct);
    }

    @Test
    public void testSetOnlineDateIfApplicableValidProductWithNoVariantsDateAlreadySet() throws Exception {
        given(Boolean.valueOf(targetProductSearchService.isProductAvailableToSell(mockColourVariantProduct)))
                .willReturn(Boolean.TRUE);
        given(mockColourVariantProduct.getOnlineDate()).willReturn(new Date());

        targetProductServiceImpl.setOnlineDateIfApplicable(mockColourVariantProduct);

        verify(mockColourVariantProduct, never()).setOnlineDate(any(Date.class));
        verify(modelService, never()).save(mockColourVariantProduct);
    }

    @Test
    public void testSetOnlineDateIfApplicableValidProductWithSizeVariantsAndNullDate() throws Exception {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.NEWIN_ONLINEFROM);
        given(Boolean.valueOf(targetProductSearchService.isProductAvailableToSell(mockColourVariantProduct)))
                .willReturn(Boolean.TRUE);
        final List<VariantProductModel> sizeVariants = new ArrayList<>();
        sizeVariants.add(mockSizeVariantProduct);
        given(mockColourVariantProduct.getVariants()).willReturn(sizeVariants);
        given(mockColourVariantProduct.getOnlineDate()).willReturn(null);
        given(mockSizeVariantProduct.getOnlineDate()).willReturn(null);

        targetProductServiceImpl.setOnlineDateIfApplicable(mockColourVariantProduct);

        verify(mockColourVariantProduct).setOnlineDate(any(Date.class));
        verify(mockSizeVariantProduct).setOnlineDate(any(Date.class));
        verify(modelService).save(mockColourVariantProduct);
        verify(modelService).save(mockSizeVariantProduct);
    }

    @Test
    public void testSetOnlineDateIfApplicableValidProductWithSizeVariantsDateAlreadySet() throws Exception {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.NEWIN_ONLINEFROM);
        given(Boolean.valueOf(targetProductSearchService.isProductAvailableToSell(mockColourVariantProduct)))
                .willReturn(Boolean.TRUE);
        final List<VariantProductModel> sizeVariants = new ArrayList<>();
        sizeVariants.add(mockSizeVariantProduct);
        given(mockColourVariantProduct.getVariants()).willReturn(sizeVariants);
        given(mockColourVariantProduct.getOnlineDate()).willReturn(new Date());
        given(mockSizeVariantProduct.getOnlineDate()).willReturn(new Date());

        targetProductServiceImpl.setOnlineDateIfApplicable(mockColourVariantProduct);

        verify(mockColourVariantProduct, never()).setOnlineDate(any(Date.class));
        verify(mockSizeVariantProduct, never()).setOnlineDate(any(Date.class));
        verify(modelService, never()).save(mockColourVariantProduct);
        verify(modelService, never()).save(mockSizeVariantProduct);
    }

    @Test
    public void testSetOnlineDateIfApplicableValidSVProduct() throws Exception {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.NEWIN_ONLINEFROM);
        given(Boolean.valueOf(targetProductSearchService.isProductAvailableToSell(mockColourVariantProduct)))
                .willReturn(Boolean.TRUE);
        final List<VariantProductModel> sizeVariants = new ArrayList<>();

        final TargetSizeVariantProductModel mockSizeVariantProduct1 = mock(TargetSizeVariantProductModel.class);
        final TargetSizeVariantProductModel mockSizeVariantProduct2 = mock(TargetSizeVariantProductModel.class);
        sizeVariants.add(mockSizeVariantProduct);
        sizeVariants.add(mockSizeVariantProduct1);
        sizeVariants.add(mockSizeVariantProduct2);

        given(mockColourVariantProduct.getVariants()).willReturn(sizeVariants);
        given(mockSizeVariantProduct.getBaseProduct()).willReturn(mockColourVariantProduct);

        targetProductServiceImpl.setOnlineDateIfApplicable(mockSizeVariantProduct);

        verify(mockColourVariantProduct).setOnlineDate(any(Date.class));
        verify(mockSizeVariantProduct).setOnlineDate(any(Date.class));
        verify(mockSizeVariantProduct1).setOnlineDate(any(Date.class));
        verify(mockSizeVariantProduct2).setOnlineDate(any(Date.class));
        verify(modelService).save(mockColourVariantProduct);
        verify(modelService).save(mockSizeVariantProduct);
        verify(modelService).save(mockSizeVariantProduct1);
        verify(modelService).save(mockSizeVariantProduct2);
    }

    @Test(expected = NullPointerException.class)
    public void testGetProductByEanProductListNull()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(mockTargetProductDao.findProductByEan(TEST_EAN)).willReturn(null);
        final ProductModel product = targetProductServiceImpl.getProductByEan(TEST_EAN);
        assertThat(product).isNull();
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testGetProductByEanProductListEmpty()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(mockTargetProductDao.findProductByEan(TEST_EAN)).willReturn(Collections.EMPTY_LIST);
        final ProductModel product = targetProductServiceImpl.getProductByEan(TEST_EAN);
        assertThat(product).isNull();
    }

    @Test(expected = TargetAmbiguousIdentifierException.class)
    public void testGetProductByEanProductListContainingMultipleProducts()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final ProductModel product1 = new ProductModel();
        final ProductModel product2 = new ProductModel();
        final List<ProductModel> products = new ArrayList<>();
        products.add(product1);
        products.add(product2);

        given(mockTargetProductDao.findProductByEan(TEST_EAN)).willReturn(products);
        final ProductModel product = targetProductServiceImpl.getProductByEan(TEST_EAN);
        assertThat(product).isNull();
    }

    @Test
    public void testGetProductByEanProductListContainingOneProduct()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final ProductModel product1 = new ProductModel();
        product1.setCode("testProductCode");
        final List<ProductModel> products = new ArrayList<>();
        products.add(product1);

        given(mockTargetProductDao.findProductByEan(TEST_EAN)).willReturn(products);
        final ProductModel product = targetProductServiceImpl.getProductByEan(TEST_EAN);
        assertThat(product).isNotNull();
        assertThat(product.getCode()).isEqualTo("testProductCode");
    }

    @Test
    public void testIsDropShipProductWithNoStockLevels() {
        final TargetProductModel mockProduct = mock(TargetProductModel.class);

        assertThat(targetProductServiceImpl.isDropShipProduct(mockProduct)).isFalse();
    }

    @Test
    public void testIsDropShipProductDropShipWarehouse() {
        final WarehouseModel mockWarehouse = mock(WarehouseModel.class);
        willReturn(Boolean.TRUE).given(mockWarehouse).isDropShipWarehouse();

        final StockLevelModel mockStockLevel = mock(StockLevelModel.class);
        given(mockStockLevel.getWarehouse()).willReturn(mockWarehouse);

        final TargetProductModel mockProduct = mock(TargetProductModel.class);

        given(mockTargetStockService.getAllStockLevels(mockProduct)).willReturn(Collections.singleton(mockStockLevel));

        assertThat(targetProductServiceImpl.isDropShipProduct(mockProduct)).isTrue();
    }

    @Test
    public void testIsDropShipProductNotDropShipWarehouse() {
        final WarehouseModel mockWarehouse = mock(WarehouseModel.class);
        willReturn(Boolean.FALSE).given(mockWarehouse).isDropShipWarehouse();

        final StockLevelModel mockStockLevel = mock(StockLevelModel.class);
        given(mockStockLevel.getWarehouse()).willReturn(mockWarehouse);

        final TargetProductModel mockProduct = mock(TargetProductModel.class);

        given(mockTargetStockService.getAllStockLevels(mockProduct)).willReturn(Collections.singleton(mockStockLevel));

        assertThat(targetProductServiceImpl.isDropShipProduct(mockProduct)).isFalse();
    }

    @Test
    public void testSetOnlineDateForApplicableProducts() {
        willReturn(Arrays.asList("P1000_blue", "P2000_red")).given(mockTargetProductDao)
                .findProductsEligibleForNewOnlineDate();

        final TargetColourVariantProductModel mockProduct1 = mock(TargetColourVariantProductModel.class);
        final TargetColourVariantProductModel mockProductOnline1 = mock(TargetColourVariantProductModel.class);
        final TargetColourVariantProductModel mockProduct2 = mock(TargetColourVariantProductModel.class);
        willReturn(mockProduct1).given(targetProductServiceImpl).getProductForCode(mockStagedCatalogVersionModel,
                "P1000_blue");
        willReturn(mockProductOnline1).given(targetProductServiceImpl).getProductForCode(mockOnlineCatalogVersionModel,
                "P1000_blue");
        willReturn(mockProduct2).given(targetProductServiceImpl).getProductForCode(mockStagedCatalogVersionModel,
                "P2000_red");
        willThrow(new UnknownIdentifierException("")).given(targetProductServiceImpl).getProductForCode(
                mockOnlineCatalogVersionModel, "P2000_red");
        willDoNothing().given(targetProductServiceImpl)
                .setOnlineDateToColourVariant(any(TargetColourVariantProductModel.class));

        targetProductServiceImpl.setOnlineDateForApplicableProducts();
        verify(targetProductServiceImpl).setOnlineDateToColourVariant(mockProduct1);
        verify(targetProductServiceImpl).setOnlineDateToColourVariant(mockProductOnline1);
        verify(targetProductServiceImpl).setOnlineDateToColourVariant(mockProduct2);
        verify(targetProductServiceImpl, never())
                .setOnlineDateToColourVariantIfApplicable(any(TargetColourVariantProductModel.class));
    }
}

/**
 * 
 */
package au.com.target.tgtcore.refund.returablechecks;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Sets;


/**
 * @author gsing236
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TargetConsignmentBasedReturnableCheckTest {

    @Mock
    private OrderModel order;

    @Mock
    private AbstractOrderEntryModel orderEntryModel;

    @InjectMocks
    private final TargetConsignmentBasedReturnableCheck returnableCheck = new TargetConsignmentBasedReturnableCheck();

    @Before
    public void setUp() {
        given(orderEntryModel.getQuantity()).willReturn(Long.valueOf(5l));
    }

    @Test
    public void testPerformWhenReturnQuantityIsZero() {
        final boolean result = returnableCheck.perform(order, orderEntryModel, 0);
        assertThat(result).isFalse();
    }

    @Test
    public void testPerformOrderEntryQuantityLessThanReturnQuantity() {
        given(orderEntryModel.getQuantity()).willReturn(Long.valueOf(2l));
        final boolean result = returnableCheck.perform(order, orderEntryModel, 5);
        assertThat(result).isFalse();
    }

    @Test
    public void testPerformWhenOrderStatusCompleteButNoConsignments() {
        given(order.getConsignments()).willReturn(Collections.EMPTY_SET);
        given(order.getStatus()).willReturn(OrderStatus.COMPLETED);
        final boolean result = returnableCheck.perform(order, orderEntryModel, 5);
        assertThat(result).isFalse();
    }

    @Test
    public void testPerformWhenNoConsignmentsAreShipped() {

        mockData(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, ConsignmentStatus.CONFIRMED_BY_WAREHOUSE,
                OrderStatus.INPROGRESS);
        final boolean result = returnableCheck.perform(order, orderEntryModel, 11);
        assertThat(result).isFalse();
    }

    @Test
    public void testPerformWhenNoConsignmentsAreShippedButOrderStatusMarkedComplete() {

        mockData(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, ConsignmentStatus.CONFIRMED_BY_WAREHOUSE,
                OrderStatus.COMPLETED);
        final boolean result = returnableCheck.perform(order, orderEntryModel, 11);
        assertThat(result).isFalse();
    }

    @Test
    public void testPerformWhenFewConsignmentsAreShipped() {

        mockData(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, ConsignmentStatus.SHIPPED, OrderStatus.INPROGRESS);
        final boolean result = returnableCheck.perform(order, orderEntryModel, 11);
        assertThat(result).isFalse();
    }

    @Test
    public void testPerformWhenFewConsignmentsAreCancelled() {

        mockData(ConsignmentStatus.CANCELLED, ConsignmentStatus.SHIPPED, OrderStatus.COMPLETED);
        final boolean result = returnableCheck.perform(order, orderEntryModel, 8);
        assertThat(result).isTrue();
    }


    @Test
    public void testPerformSuccessWhenAllConsignmentsShipped() {

        mockData(ConsignmentStatus.SHIPPED, ConsignmentStatus.SHIPPED, OrderStatus.COMPLETED);
        final boolean result = returnableCheck.perform(order, orderEntryModel, 11);
        assertThat(result).isTrue();
    }

    @Test
    public void testPerformWhenShippedQuantityIsNull() {

        mockData(ConsignmentStatus.SHIPPED, ConsignmentStatus.SHIPPED, OrderStatus.COMPLETED);
        final ConsignmentModel consignment = order.getConsignments().iterator().next();
        final ConsignmentEntryModel entry = consignment.getConsignmentEntries().iterator().next();
        given(entry.getShippedQuantity()).willReturn(null);
        final boolean result = returnableCheck.perform(order, orderEntryModel, 11);
        assertThat(result).isFalse();
    }

    /**
     * 
     */
    private void mockData(final ConsignmentStatus consignment1Status, final ConsignmentStatus consignment2Status,
            final OrderStatus orderStatus) {

        given(orderEntryModel.getQuantity()).willReturn(Long.valueOf(11l));

        final ConsignmentEntryModel entryModel1 = mock(ConsignmentEntryModel.class);
        final ConsignmentEntryModel entryModel2 = mock(ConsignmentEntryModel.class);

        given(entryModel1.getOrderEntry()).willReturn(orderEntryModel);
        given(entryModel1.getShippedQuantity()).willReturn(Long.valueOf(3l));
        given(entryModel2.getOrderEntry()).willReturn(orderEntryModel);
        given(entryModel2.getShippedQuantity()).willReturn(Long.valueOf(8l));

        final ConsignmentModel consignment1 = mock(ConsignmentModel.class);
        final ConsignmentModel consignment2 = mock(ConsignmentModel.class);

        given(consignment1.getConsignmentEntries()).willReturn(Collections.singleton(entryModel1));
        given(consignment2.getConsignmentEntries()).willReturn(Collections.singleton(entryModel2));
        given(consignment1.getStatus()).willReturn(consignment1Status);
        given(consignment2.getStatus()).willReturn(consignment2Status);
        given(order.getConsignments()).willReturn(Sets.newHashSet(consignment1, consignment2));
        given(order.getStatus()).willReturn(orderStatus);
    }

}

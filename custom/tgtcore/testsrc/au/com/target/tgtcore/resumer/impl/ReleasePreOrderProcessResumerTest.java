/**
 * 
 */
package au.com.target.tgtcore.resumer.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtpayment.service.PaymentsInProgressService;


/**
 * @author gsing236
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ReleasePreOrderProcessResumerTest {

    @InjectMocks
    @Spy
    private final ReleasePreOrderProcessResumer resumer = new ReleasePreOrderProcessResumer();

    @Mock
    private PaymentsInProgressService paymentsInProgressService;

    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;

    @Mock
    private OrderModel orderModel;

    @Mock
    private PaymentTransactionEntryModel ptem;

    @Before
    public void setUp() {
        doReturn(targetBusinessProcessService).when(resumer).getTargetBusinessProcessService();
    }

    @Test
    public void testWhenOrderStatusIsParked() {
        given(orderModel.getStatus()).willReturn(OrderStatus.PARKED);

        resumer.resumeProcess(orderModel, ptem);

        verify(orderModel).getStatus();
        verifyZeroInteractions(paymentsInProgressService);
        verify(targetBusinessProcessService).startFluentReleasePreOrderProcess(orderModel);

    }

    @Test
    public void testWhenOrderStatusIsNotParked() {
        given(orderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);

        resumer.resumeProcess(orderModel, ptem);

        verify(orderModel).getStatus();
        verify(paymentsInProgressService).removeInProgressPayment(orderModel);
        verifyZeroInteractions(targetBusinessProcessService);

    }
}

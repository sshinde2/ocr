/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtcore.setup;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.setup.SetupImpexService;
import de.hybris.platform.commerceservices.setup.SetupSyncJobService;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.validation.services.ValidationService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


@UnitTest
public class CoreSystemSetupUnitTest {
    private CoreSystemSetup setup;

    @Mock
    private SystemSetupContext ctx;

    @Mock
    private ValidationService validation;

    @Mock
    private SetupImpexService setupImpexService;

    @Mock
    private SetupSyncJobService setupSyncJobService;

    @org.junit.Before
    public void prepare() {
        MockitoAnnotations.initMocks(this);
        setup = Mockito.spy(new CoreSystemSetup());

        Mockito.doReturn(Collections.EMPTY_LIST).when(setup).getExtensionNames();
        Mockito.doReturn(setupImpexService).when(setup).getSetupImpexService();
        Mockito.doReturn(setupSyncJobService).when(setup).getSetupSyncJobService();

        Mockito.doReturn("core").when(ctx).getExtensionName();
        Mockito.doReturn(validation).when(setup).getBeanForName("validationService");
        Mockito.when(ctx.getProcess()).thenReturn(Process.INIT);
        simulateBooleanParamater(CoreSystemSetup.IMPORT_ACCESS_RIGHTS, false);
        simulateBooleanParamater(CoreSystemSetup.IMPORT_SITES, true);

    }


    @Test
    public void testImportSitesNoSync() //NOPMD
    {
        testImportSitesSync(false);
    }

    @Test
    public void testImportAllDeliveryMode() {
        final List<String> extentions = new ArrayList<>();
        extentions.add("core");
        simulateBooleanParamater(CoreSystemSetup.IMPORT_ACCESS_RIGHTS, false);
        simulateBooleanParamater(CoreSystemSetup.IMPORT_SITES, false);
        simulateBooleanParamater(CoreSystemSetup.IMPORT_SYNC_CATALOGS, false);
        when(ctx.getProcess()).thenReturn(Process.UPDATE);
        when(setup.getExtensionNames()).thenReturn(extentions);

        setup.createProjectData(ctx);
    }

    @Test
    public void testImportDefaultDeliveryMode() {
        final List<String> extentions = new ArrayList<>();
        extentions.add("core");
        extentions.add("tgtinitialdata");
        simulateBooleanParamater(CoreSystemSetup.IMPORT_ACCESS_RIGHTS, false);
        simulateBooleanParamater(CoreSystemSetup.IMPORT_SITES, false);
        simulateBooleanParamater(CoreSystemSetup.IMPORT_SYNC_CATALOGS, false);
        when(ctx.getProcess()).thenReturn(Process.UPDATE);
        when(setup.getExtensionNames()).thenReturn(extentions);

        setup.createProjectData(ctx);
    }

    private void testImportSitesSync(final boolean syncAll) {
        final InOrder order = Mockito.inOrder(setupImpexService, setupSyncJobService, validation);

        simulateBooleanParamater(CoreSystemSetup.IMPORT_SYNC_CATALOGS, syncAll);


        setup.createProjectData(ctx);


        verifySyncProductImport(order, CoreSystemSetup.TARGET, syncAll);

        Mockito.verify(validation).reloadValidationEngine();
    }

    private void verifySyncProductImport(final InOrder order, final String catalogName, final boolean sync) {

        if (sync) {
            order.verify(setupSyncJobService).executeCatalogSyncJob(catalogName + "ProductCatalog");
        }

    }

    private void simulateBooleanParamater(final String key, final boolean value) {
        BDDMockito.given(ctx.getParameter("core_" + key)).willReturn(value ? "yes" : "no");
    }
}

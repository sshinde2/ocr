/**
 *
 */
package au.com.target.tgtcore.sms.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.email.CMSEmailPageService;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageTemplateModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.acceleratorservices.process.email.context.EmailContextFactory;
import de.hybris.platform.acceleratorservices.process.strategies.ProcessContextResolutionStrategy;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.commons.renderer.RendererService;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import java.io.StringWriter;
import java.text.MessageFormat;
import java.util.TimeZone;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import au.com.target.tgtbusproc.exceptions.BusinessProcessSmsException;
import au.com.target.tgtbusproc.process.email.context.impl.TargetEmailContextFactory;
import au.com.target.tgtbusproc.sms.data.SendSmsToStoreData;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.postcode.service.TargetPostCodeService;
import au.com.target.tgtcore.sms.client.TargetSendSmsClient;
import au.com.target.tgtcore.sms.dto.TargetSendSmsResponseDto;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetSendSmsServiceImplTest {

    private static final String SMS_MESSAGE_TO_STORE_FOR_OPEN_ORDERS = "The OFC for store {0} has {1} open orders awaiting action";

    @InjectMocks
    private final TargetSendSmsServiceImpl targetSendSmsServiceImpl = new TargetSendSmsServiceImpl() {

        /* (non-Javadoc)
         * @see au.com.target.tgtcore.sms.impl.TargetSendSmsServiceImpl#getEmailContextFactory()
         */
        @Override
        public EmailContextFactory getEmailContextFactory() {
            return emailContextFactory;
        }

    };

    @Mock
    private RendererService rendererService;
    @Mock
    private TargetEmailContextFactory emailContextFactory;
    @Mock
    private ProcessContextResolutionStrategy contextResolutionStrategy;
    @Mock
    private CMSEmailPageService cmsEmailPageService;
    @Mock
    private CatalogVersionModel contentCatalogVersion;
    @Mock
    private TargetPostCodeService targetPostCodeService;
    @Mock
    private TargetSendSmsClient targetSendSmsClient;
    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;
    @Mock
    private SendSmsToStoreData storedata;
    @Mock
    private TargetPointOfServiceService targetPointOfServiceService;
    @Mock
    private TargetPointOfServiceModel pos;
    private final Integer storeId = Integer.valueOf(100);


    @Before
    public void setup() {

        BDDMockito.given(contextResolutionStrategy.getContentCatalogVersion(
                Mockito.any(BusinessProcessModel.class))).willReturn(contentCatalogVersion);
        targetSendSmsServiceImpl
                .setSmsMessageToStoreForOpenOrders(SMS_MESSAGE_TO_STORE_FOR_OPEN_ORDERS);
    }

    @Test
    public void testGenerateSmsMessageFromVelocityTemplate() throws BusinessProcessSmsException {

        final EmailPageModel emailPageModel = new EmailPageModel();
        final EmailPageTemplateModel emailPageTemplateModel = new EmailPageTemplateModel();
        final RendererTemplateModel smsTemplate = new RendererTemplateModel();
        emailPageTemplateModel.setHtmlTemplate(smsTemplate);
        emailPageModel.setMasterTemplate(emailPageTemplateModel);

        final AbstractEmailContext smsContext = new TargetTestEmailContext();
        BDDMockito.given(cmsEmailPageService.getEmailPageForFrontendTemplate(Mockito.anyString(),
                Mockito.any(CatalogVersionModel.class))).willReturn(emailPageModel);
        BDDMockito.given(
                emailContextFactory.create(Mockito.any(BusinessProcessModel.class), Mockito.any(EmailPageModel.class),
                        Mockito.any(RendererTemplateModel.class)))
                .willReturn(smsContext);

        BDDMockito.doAnswer(new Answer<Void>() {

            @Override
            public Void answer(final InvocationOnMock invocation) throws Exception {
                final StringWriter outputValue = (StringWriter)invocation.getArguments()[2];
                outputValue.append("test");
                return null;
            }
        }).when(rendererService).render(Mockito.any(RendererTemplateModel.class), Mockito.any(Object.class),
                Mockito.any(StringWriter.class));

        final String message = targetSendSmsServiceImpl.generateSmsMessageFromVelocityTemplate(
                new BusinessProcessModel(), "test");

        Assert.assertEquals(message, "test");
    }


    @Test(expected = BusinessProcessSmsException.class)
    public void testGenerateSmsMessageFromVelocityTemplateWithNullEmailPageModel() throws BusinessProcessSmsException {

        final EmailPageModel emailPageModel = null;

        BDDMockito.given(cmsEmailPageService.getEmailPageForFrontendTemplate(Mockito.anyString(),
                Mockito.any(CatalogVersionModel.class))).willReturn(emailPageModel);


        targetSendSmsServiceImpl.generateSmsMessageFromVelocityTemplate(
                new BusinessProcessModel(), "test");
    }

    @Test(expected = BusinessProcessSmsException.class)
    public void testGenerateSmsMessageFromVelocityTemplateWithNullEmailPageTemplateModel()
            throws BusinessProcessSmsException {

        final EmailPageModel emailPageModel = new EmailPageModel();
        final EmailPageTemplateModel emailPageTemplateModel = null;
        emailPageModel.setMasterTemplate(emailPageTemplateModel);

        BDDMockito.given(cmsEmailPageService.getEmailPageForFrontendTemplate(Mockito.anyString(),
                Mockito.any(CatalogVersionModel.class))).willReturn(emailPageModel);


        targetSendSmsServiceImpl.generateSmsMessageFromVelocityTemplate(
                new BusinessProcessModel(), "test");
    }

    @Test(expected = BusinessProcessSmsException.class)
    public void testGenerateSmsMessageFromVelocityTemplateWithNullRendererTemplateModel()
            throws BusinessProcessSmsException {

        final EmailPageModel emailPageModel = new EmailPageModel();
        final EmailPageTemplateModel emailPageTemplateModel = new EmailPageTemplateModel();
        final RendererTemplateModel smsTemplate = null;
        emailPageTemplateModel.setHtmlTemplate(smsTemplate);
        emailPageModel.setMasterTemplate(emailPageTemplateModel);

        BDDMockito.given(cmsEmailPageService.getEmailPageForFrontendTemplate(Mockito.anyString(),
                Mockito.any(CatalogVersionModel.class))).willReturn(emailPageModel);

        targetSendSmsServiceImpl.generateSmsMessageFromVelocityTemplate(
                new BusinessProcessModel(), "test");
    }

    @Test(expected = BusinessProcessSmsException.class)
    public void testGenerateSmsMessageFromVelocityTemplateWithNullSmsContext()
            throws BusinessProcessSmsException {

        final EmailPageModel emailPageModel = new EmailPageModel();
        final EmailPageTemplateModel emailPageTemplateModel = new EmailPageTemplateModel();
        final RendererTemplateModel smsTemplate = new RendererTemplateModel();
        emailPageTemplateModel.setHtmlTemplate(smsTemplate);
        emailPageModel.setMasterTemplate(emailPageTemplateModel);

        final AbstractEmailContext smsContext = null;
        BDDMockito.given(cmsEmailPageService.getEmailPageForFrontendTemplate(Mockito.anyString(),
                Mockito.any(CatalogVersionModel.class))).willReturn(emailPageModel);
        BDDMockito.given(
                emailContextFactory.create(Mockito.any(BusinessProcessModel.class), Mockito.any(EmailPageModel.class),
                        Mockito.any(RendererTemplateModel.class)))
                .willReturn(smsContext);


        targetSendSmsServiceImpl.generateSmsMessageFromVelocityTemplate(
                new BusinessProcessModel(), "test");
    }

    @Test
    public void testSendSmsForCncNotificationVaildMobileNumberVicTimeZone() throws BusinessProcessSmsException {
        final OrderProcessModel process = new OrderProcessModel();
        final OrderModel order = new OrderModel();
        final AddressModel address = new AddressModel();
        address.setPhone1("+61412345678");
        order.setDeliveryAddress(address);
        process.setOrder(order);
        generateSmsMessage();
        final PostCodeModel postCodeModel = new PostCodeModel();
        postCodeModel.setTimeZone(TimeZone.getTimeZone("Australia/Victoria"));
        BDDMockito.given(targetPostCodeService.getPostCode(Mockito.anyString())).willReturn(postCodeModel);
        final TargetSendSmsResponseDto response = new TargetSendSmsResponseDto();
        BDDMockito.given(
                targetSendSmsClient.sendSmsNotification("61412345678", "test message", "+1000")).willReturn(response);
        final TargetSendSmsResponseDto result;
        result = targetSendSmsServiceImpl.sendSmsForCncNotification(process,
                "testTemplate");
        Assert.assertEquals(response, result);
    }

    @Test
    public void testSendSmsForCncNotificationInvaildMobileNumberVicTimeZone() {
        final OrderProcessModel process = new OrderProcessModel();
        final OrderModel order = new OrderModel();
        final AddressModel address = new AddressModel();
        address.setPhone1("+61312345678");
        order.setDeliveryAddress(address);
        process.setOrder(order);
        generateSmsMessage();
        final PostCodeModel postCodeModel = new PostCodeModel();
        postCodeModel.setTimeZone(TimeZone.getTimeZone("Australia/Victoria"));
        BDDMockito.given(targetPostCodeService.getPostCode(Mockito.anyString())).willReturn(postCodeModel);
        final TargetSendSmsResponseDto response = new TargetSendSmsResponseDto();
        BDDMockito.given(
                targetSendSmsClient.sendSmsNotification("61412345678", "test message", "+1000")).willReturn(response);

        try {
            targetSendSmsServiceImpl.sendSmsForCncNotification(process,
                    "testTemplate");
        }
        catch (final BusinessProcessSmsException e) {
            Assert.assertEquals(
                    "Failed to get mobile number or invalid mobile number from process " + process.getCode(),
                    e.getMessage());
        }
    }

    @Test
    public void testSendSmsForCncNotificationValidMobileNumberDefaultTimeZone() throws BusinessProcessSmsException {
        final OrderProcessModel process = new OrderProcessModel();
        final OrderModel order = new OrderModel();
        final AddressModel address = new AddressModel();
        address.setPhone1("+61412345678");
        order.setDeliveryAddress(address);
        process.setOrder(order);
        generateSmsMessage();
        final PostCodeModel postCodeModel = null;
        BDDMockito.given(targetPostCodeService.getPostCode(Mockito.anyString())).willReturn(postCodeModel);
        final TargetSendSmsResponseDto response = new TargetSendSmsResponseDto();
        BDDMockito.given(
                targetSendSmsClient.sendSmsNotification("61412345678", "test message", "+1000")).willReturn(response);
        final TargetSendSmsResponseDto result;
        result = targetSendSmsServiceImpl.sendSmsForCncNotification(process,
                "testTemplate");
        Assert.assertEquals(response, result);
    }

    @Test
    public void testSendSmsToStoreForOpenOrdersWithVaildMobileNumberVicTimeZone() throws Exception {
        final String storeNumber = storeId.toString();
        final BusinessProcessModel process = new BusinessProcessModel();
        final PostCodeModel postCodeModel = new PostCodeModel();
        postCodeModel.setTimeZone(TimeZone.getTimeZone("Australia/Victoria"));

        BDDMockito.given(targetPostCodeService.getPostCode(Mockito.anyString())).willReturn(postCodeModel);
        final TargetSendSmsResponseDto response = new TargetSendSmsResponseDto();

        BDDMockito.given(storedata.getMobileNumber()).willReturn("+61412345678");
        BDDMockito.given(storedata.getStoreNumber()).willReturn(storeNumber);
        BDDMockito.given(orderProcessParameterHelper.getSendSmsToStoreData(process)).willReturn(storedata);
        BDDMockito.given(targetPointOfServiceService.getPOSByStoreNumber(storeId)).willReturn(pos);
        BDDMockito.given(
                targetSendSmsClient.sendSmsNotification("61412345678",
                        getFormatttedMessageForOpenOrders(storeNumber, "0"), "+1000"))
                .willReturn(response);

        final TargetSendSmsResponseDto result = targetSendSmsServiceImpl.sendSmsToStoreForOpenOrders(process);
        Assert.assertEquals(response, result);
    }

    @Test(expected = BusinessProcessSmsException.class)
    public void testSendSmsToStoreForOpenOrdersWithInvaildMobileNumberVicTimeZone() throws Exception {
        final BusinessProcessModel process = new BusinessProcessModel();
        final PostCodeModel postCodeModel = new PostCodeModel();
        postCodeModel.setTimeZone(TimeZone.getTimeZone("Australia/Victoria"));
        BDDMockito.given(targetPostCodeService.getPostCode(Mockito.anyString())).willReturn(postCodeModel);
        BDDMockito.given(orderProcessParameterHelper.getSendSmsToStoreData(process)).willReturn(storedata);

        targetSendSmsServiceImpl.sendSmsToStoreForOpenOrders(process);
        Assert.fail();
    }

    @Test
    public void testSendSmsToStoreForOpenOrdersWithValidMobileNumberDefaultTimeZone()
            throws Exception {
        final String storeNumber = storeId.toString();
        final BusinessProcessModel process = new BusinessProcessModel();
        final PostCodeModel postCodeModel = null;
        BDDMockito.given(targetPostCodeService.getPostCode(Mockito.anyString())).willReturn(postCodeModel);
        final TargetSendSmsResponseDto response = new TargetSendSmsResponseDto();

        BDDMockito.given(storedata.getMobileNumber()).willReturn("+61412345678");
        BDDMockito.given(storedata.getStoreNumber()).willReturn(storeNumber);
        BDDMockito.given(orderProcessParameterHelper.getSendSmsToStoreData(process)).willReturn(storedata);
        BDDMockito.given(targetPointOfServiceService.getPOSByStoreNumber(storeId)).willReturn(pos);
        BDDMockito.given(
                targetSendSmsClient.sendSmsNotification("61412345678",
                        getFormatttedMessageForOpenOrders(storeNumber, "0"), "+1000"))
                .willReturn(response);

        final TargetSendSmsResponseDto result = targetSendSmsServiceImpl.sendSmsToStoreForOpenOrders(process);
        Assert.assertEquals(response, result);
    }

    private String getFormatttedMessageForOpenOrders(final String storeNumber, final String openOrders) {
        final Object[] msgArgs = { storeNumber, openOrders };
        final MessageFormat mf = new MessageFormat(SMS_MESSAGE_TO_STORE_FOR_OPEN_ORDERS);
        return mf.format(msgArgs);
    }

    public void generateSmsMessage() {
        final EmailPageModel emailPageModel = new EmailPageModel();
        final EmailPageTemplateModel emailPageTemplateModel = new EmailPageTemplateModel();
        final RendererTemplateModel smsTemplate = new RendererTemplateModel();
        emailPageTemplateModel.setHtmlTemplate(smsTemplate);
        emailPageModel.setMasterTemplate(emailPageTemplateModel);

        final AbstractEmailContext smsContext = new TargetTestEmailContext();
        BDDMockito.given(cmsEmailPageService.getEmailPageForFrontendTemplate(Mockito.anyString(),
                Mockito.any(CatalogVersionModel.class))).willReturn(emailPageModel);
        BDDMockito.given(
                emailContextFactory.create(Mockito.any(BusinessProcessModel.class), Mockito.any(EmailPageModel.class),
                        Mockito.any(RendererTemplateModel.class)))
                .willReturn(smsContext);

        BDDMockito.doAnswer(new Answer<Void>() {

            @Override
            public Void answer(final InvocationOnMock invocation) throws Exception {
                final StringWriter outputValue = (StringWriter)invocation.getArguments()[2];
                outputValue.append("test message");
                return null;
            }
        }).when(rendererService).render(Mockito.any(RendererTemplateModel.class), Mockito.any(Object.class),
                Mockito.any(StringWriter.class));
    }

    class TargetTestEmailContext extends AbstractEmailContext {
        @Override
        public void init(final BusinessProcessModel businessProcessModel, final EmailPageModel emailPageModel) {
            super.init(businessProcessModel, emailPageModel);
        }

        @Override
        protected BaseSiteModel getSite(final BusinessProcessModel businessProcessModel) {
            return ((StoreFrontCustomerProcessModel)businessProcessModel).getSite();
        }

        @Override
        protected CustomerModel getCustomer(final BusinessProcessModel businessProcessModel) {
            return ((StoreFrontCustomerProcessModel)businessProcessModel).getCustomer();
        }

        /* (non-Javadoc)
         * @see de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#getEmailLanguage(de.hybris.platform.processengine.model.BusinessProcessModel)
         */
        @Override
        protected LanguageModel getEmailLanguage(final BusinessProcessModel businessProcessModel) {
            return new LanguageModel();
        }
    }

}

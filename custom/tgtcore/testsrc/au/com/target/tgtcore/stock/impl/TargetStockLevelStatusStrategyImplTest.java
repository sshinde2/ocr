/**
 * 
 */
package au.com.target.tgtcore.stock.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.willReturn;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.config.TargetSharedConfigService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetStockLevelStatusStrategyImplTest {

    @InjectMocks
    private final TargetStockLevelStatusStrategyImpl targetStockLevelStatusStrategy = new TargetStockLevelStatusStrategyImpl();
    @Mock
    private TargetSharedConfigService targetSharedConfigService;

    @Before
    public void setUp() {
        willReturn(Integer.valueOf(8)).given(targetSharedConfigService).getInt(null, 8);
        willReturn(Integer.valueOf(15)).given(targetSharedConfigService).getInt("15islow", 8);
        willReturn(Integer.valueOf(0)).given(targetSharedConfigService).getInt(null, 0);
        willReturn(Integer.valueOf(1)).given(targetSharedConfigService).getInt("noStock", 0);
        targetStockLevelStatusStrategy.setLowStockThresholdConfigKey(null);
        targetStockLevelStatusStrategy.setNoStockThresholdConfigKey(null);
    }

    @Test
    public void testDefaultGetStockLevel() {
        assertThat(targetStockLevelStatusStrategy.checkStatus("10")).isEqualTo(StockLevelStatus.INSTOCK);
        assertThat(targetStockLevelStatusStrategy.checkStatus("5")).isEqualTo(StockLevelStatus.LOWSTOCK);
        assertThat(targetStockLevelStatusStrategy.checkStatus("1")).isEqualTo(StockLevelStatus.LOWSTOCK);
        assertThat(targetStockLevelStatusStrategy.checkStatus("0")).isEqualTo(StockLevelStatus.OUTOFSTOCK);
        assertThat(targetStockLevelStatusStrategy.checkStatus("NA")).isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    @Test
    public void testDefaultGetStockLevelLowStockWithConfigKey() {
        targetStockLevelStatusStrategy.setLowStockThresholdConfigKey("15islow");
        targetStockLevelStatusStrategy.setNoStockThresholdConfigKey("noStock");
        assertThat(targetStockLevelStatusStrategy.checkStatus("20")).isEqualTo(StockLevelStatus.INSTOCK);
        assertThat(targetStockLevelStatusStrategy.checkStatus("15")).isEqualTo(StockLevelStatus.LOWSTOCK);
        assertThat(targetStockLevelStatusStrategy.checkStatus("2")).isEqualTo(StockLevelStatus.LOWSTOCK);
        assertThat(targetStockLevelStatusStrategy.checkStatus("1")).isEqualTo(StockLevelStatus.OUTOFSTOCK);
        assertThat(targetStockLevelStatusStrategy.checkStatus("0")).isEqualTo(StockLevelStatus.OUTOFSTOCK);
        assertThat(targetStockLevelStatusStrategy.checkStatus("NA")).isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    @Test
    public void testGetStockLevelLowStockWithBuffer() {
        targetStockLevelStatusStrategy.setBuffer(5);
        assertThat(targetStockLevelStatusStrategy.checkStatus("0")).isEqualTo(StockLevelStatus.OUTOFSTOCK);
        assertThat(targetStockLevelStatusStrategy.checkStatus("5")).isEqualTo(StockLevelStatus.OUTOFSTOCK);
        assertThat(targetStockLevelStatusStrategy.checkStatus("13")).isEqualTo(StockLevelStatus.LOWSTOCK);
        assertThat(targetStockLevelStatusStrategy.checkStatus("14")).isEqualTo(StockLevelStatus.INSTOCK);
    }

}

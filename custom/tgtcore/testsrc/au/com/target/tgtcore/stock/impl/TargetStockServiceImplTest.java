/**
 * 
 */
package au.com.target.tgtcore.stock.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollection;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.basecommerce.enums.StockLevelUpdateType;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.stock.exception.StockLevelNotFoundException;
import de.hybris.platform.stock.model.StockLevelHistoryEntryModel;
import de.hybris.platform.stock.strategy.StockLevelProductStrategy;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.ListUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.stock.TargetStockLevelDao;
import au.com.target.tgtcore.stock.strategies.impl.PreOrderStockLevelStatusStrategy;
import au.com.target.tgtcore.warehouse.dao.TargetWarehouseDao;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;


/**
 * @author dcwillia
 * 
 */
@UnitTest
public class TargetStockServiceImplTest {
    protected class MyTargetStockService extends TargetStockServiceImpl {
        // Hybris is not running in the unit test, these methods require hybris to run, so just do nothing for now.
        // We do not need to test these methods as they are exact copies from the hybris DefaultStockService.

        @Override
        public void clearCacheForItem(final StockLevelModel stockLevel) {
            // do nothing
        }

        @Override
        public void reserve(final ProductModel productModel, final WarehouseModel warehouseModel,
                final int amount, final String comment) throws InsufficientStockLevelException {
            // do nothing
        }

        @Override
        public void release(final ProductModel productModel, final WarehouseModel warehouseModel,
                final int amount, final String comment) {
            // do nothing
        }
    }

    @Spy
    private final TargetStockServiceImpl targetStockService = new MyTargetStockService();

    @Mock
    private TargetStockLevelDao targetStockLevelDao;

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private StockLevelProductStrategy stockLevelProductStrategy;

    @Mock
    private TargetWarehouseDao targetWarehouseDao;

    @Mock
    private TargetColourVariantProductModel colourVariant;

    @Mock
    private StockService stockService;

    @Mock
    private ProductModel product;

    @Mock
    private WarehouseModel warehouse;

    @Mock
    private StockLevelModel stockLevelMock;

    @Mock
    private AbstractOrderEntryModel orderEntry;

    @Mock
    private TargetColourVariantProductModel preOrderProduct;

    @Mock
    private ModelService mockModelService;

    @Mock
    private StockLevelHistoryEntryModel stockLevelHistoryEntryModel;

    @Mock
    private TargetSharedConfigService targetSharedConfigService;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private PreOrderStockLevelStatusStrategy preOrderStockLevelStatusStrategy;

    @Mock
    private TargetSizeVariantProductModel targetSizeVariantPreOrderProduct;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        targetStockService.setTargetStockLevelDao(targetStockLevelDao);
        targetStockService.setStockLevelProductStrategy(stockLevelProductStrategy);
        targetStockService.setTargetWarehouseDao(targetWarehouseDao);
        targetStockService.setTargetWarehouseService(targetWarehouseService);
        targetStockService.setTargetFeatureSwitchService(targetFeatureSwitchService);
        targetStockService.setTargetSharedConfigService(targetSharedConfigService);
        targetStockService.setPreOrderStockLevelStatusStrategy(preOrderStockLevelStatusStrategy);
        given(targetStockLevelDao.findStockLevel(anyString(), (WarehouseModel)any()))
                .willReturn(stockLevelMock);
        given(stockLevelProductStrategy.convert(product)).willReturn("testProduct");
        given(stockLevelProductStrategy.convert(preOrderProduct)).willReturn("testPreOrderProduct");

        final Calendar start = Calendar.getInstance();
        start.set(2018, Calendar.FEBRUARY, 15, 0, 0, 0);

        final Calendar end = Calendar.getInstance();
        end.set(2050, Calendar.APRIL, 22, 0, 0, 0);

        given(preOrderProduct.getPreOrderStartDateTime()).willReturn(start.getTime());
        given(preOrderProduct.getPreOrderEndDateTime()).willReturn(end.getTime());


        // Default not force in stock
        doReturn(InStockStatus.NOTSPECIFIED).when(targetStockService).getInStockStatus(product, warehouse);
        targetStockService.setModelService(mockModelService);
        given(mockModelService.create(StockLevelHistoryEntryModel.class)).willReturn(stockLevelHistoryEntryModel);
    }

    @Test
    public void testUpdateStockLevel() {
        final String comment = "stockupdate";

        targetStockService.updateStockLevel(product, warehouse, 1, comment);

        verify(targetStockLevelDao).updateActualAmount(stockLevelMock, 1);
        verify(targetStockService).clearCacheForItem(stockLevelMock);
        verify(targetStockService).createStockLevelHistoryEntry(stockLevelMock,
                StockLevelUpdateType.WAREHOUSE, 0, comment);
    }

    @Test(expected = StockLevelNotFoundException.class)
    public void testUpdateStockLevelNull() {
        final String comment = "stockupdate";
        given(targetStockLevelDao.findStockLevel(anyString(), (WarehouseModel)any()))
                .willReturn(null);
        targetStockService.updateStockLevel(product, warehouse, 1, comment);
        verify(targetStockLevelDao, times(0)).updateActualAmount(stockLevelMock, 1);
        verify(targetStockService, times(0)).clearCacheForItem(stockLevelMock);
        verify(targetStockService, times(0)).createStockLevelHistoryEntry(stockLevelMock,
                StockLevelUpdateType.WAREHOUSE, 0, comment);
    }

    @Test
    public void testUpdateNegativeStockLevel() {
        final String comment = "stockupdate";
        targetStockService.updateStockLevel(product, warehouse, -9, comment);

        verify(targetStockLevelDao).updateActualAmount(stockLevelMock, 0);
        verify(targetStockService).clearCacheForItem(stockLevelMock);
        verify(targetStockService).createStockLevelHistoryEntry(stockLevelMock,
                StockLevelUpdateType.WAREHOUSE, 0, comment);
    }

    @Test
    public void testAdjustActualAmountHappyCase() {
        final String comment = "happy, happy, joy, joy";

        targetStockService.adjustActualAmount(product, warehouse, 1, comment);

        verify(targetStockLevelDao).adjustActualAmount(stockLevelMock, 1);
        verify(targetStockService).clearCacheForItem(stockLevelMock);
        verify(targetStockService).createStockLevelHistoryEntry(stockLevelMock,
                StockLevelUpdateType.WAREHOUSE, 0, comment);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAdjustActualAmountNullProduct() {
        targetStockService.adjustActualAmount(null, warehouse, 0, "some comment");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAdjustActualAmountNullWarehouse() {
        targetStockService.adjustActualAmount(product, null, 0, "some comment");
    }

    @Test
    public void testAdjustActualAmountNullComment() {
        targetStockService.adjustActualAmount(product, warehouse, 2, null);

        verify(targetStockLevelDao).adjustActualAmount(stockLevelMock, 2);
        verify(targetStockService).clearCacheForItem(stockLevelMock);
        verify(targetStockService).createStockLevelHistoryEntry(stockLevelMock,
                StockLevelUpdateType.WAREHOUSE, 0, null);
    }


    @Test
    public void testTransferReserveToActualAmountHappyCase() {
        final String comment = "happy, happy, joy, joy";

        targetStockService.transferReserveToActualAmount(product, warehouse, 3, comment);

        verify(targetStockLevelDao).transferReserveToActualAmount(stockLevelMock, 3);
        verify(targetStockService).clearCacheForItem(stockLevelMock);
        verify(targetStockService).createStockLevelHistoryEntry(stockLevelMock,
                StockLevelUpdateType.WAREHOUSE, 0, comment);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTransferReserveToActualAmountProduct() {
        targetStockService.transferReserveToActualAmount(null, warehouse, 0, "some comment");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTransferReserveToActualAmountWarehouse() {
        targetStockService.transferReserveToActualAmount(product, null, 0, "some comment");
    }

    @Test
    public void testTransferReserveToActualAmountComment() {
        targetStockService.transferReserveToActualAmount(product, warehouse, 4, null);

        verify(targetStockLevelDao).transferReserveToActualAmount(stockLevelMock, 4);
        verify(targetStockService).clearCacheForItem(stockLevelMock);
        verify(targetStockService).createStockLevelHistoryEntry(stockLevelMock,
                StockLevelUpdateType.WAREHOUSE, 0, null);
    }

    @Test
    public void testCheckAndGetStockLevel() {
        assertThat(targetStockService.checkAndGetStockLevel(product, warehouse)).isNotNull();
    }

    @Test(expected = StockLevelNotFoundException.class)
    public void testCheckAndGetNullStockLevel() {
        given(targetStockLevelDao.findStockLevel(anyString(), (WarehouseModel)any()))
                .willReturn(null);
        targetStockService.checkAndGetStockLevel(product, warehouse);
    }

    @Test
    public void testforceReserve() {
        targetStockService.forceReserve(product, warehouse, 4, null);

        verify(targetStockLevelDao).forceReserve(stockLevelMock, 4);
        verify(targetStockService).clearCacheForItem(stockLevelMock);
        verify(targetStockService).createStockLevelHistoryEntry(stockLevelMock,
                StockLevelUpdateType.CUSTOMER_RESERVE, 0, null);
    }

    @Test
    public void testIsOrderEntryInStockAtWarehouseForStockAvailable() {
        doReturn(Integer.valueOf(5)).when(targetStockService).getStockLevelAmount(product, warehouse);
        given(orderEntry.getProduct()).willReturn(product);
        given(orderEntry.getQuantity()).willReturn(Long.valueOf(5));

        assertThat(targetStockService.isOrderEntryInStockAtWarehouse(orderEntry, warehouse)).isTrue();
    }

    @Test
    public void testIsOrderEntryInStockAtWarehouseForStockUnAvailable() {
        doReturn(Integer.valueOf(5)).when(targetStockService).getStockLevelAmount(product, warehouse);
        given(orderEntry.getProduct()).willReturn(product);
        given(orderEntry.getQuantity()).willReturn(Long.valueOf(6));

        assertThat(targetStockService.isOrderEntryInStockAtWarehouse(orderEntry, warehouse)).isFalse();
    }

    @Test
    public void testIsOrderEntryInStockAtWarehouseForNoStock() {
        doThrow(new StockLevelNotFoundException(null)).when(targetStockService)
                .getStockLevelAmount(product, warehouse);
        given(orderEntry.getProduct()).willReturn(product);
        given(orderEntry.getQuantity()).willReturn(Long.valueOf(5));

        assertThat(targetStockService.isOrderEntryInStockAtWarehouse(orderEntry, warehouse)).isFalse();
    }

    @Test
    public void testIsOrderEntryInStockAtWarehouseForStockZero() {
        doReturn(Integer.valueOf(0)).when(targetStockService).getStockLevelAmount(product, warehouse);
        given(orderEntry.getProduct()).willReturn(product);
        given(orderEntry.getQuantity()).willReturn(Long.valueOf(5));

        assertThat(targetStockService.isOrderEntryInStockAtWarehouse(orderEntry, warehouse)).isFalse();
    }

    @Test
    public void testIsOrderEntryInStockAtWarehouseForForceInStock() {
        doReturn(InStockStatus.FORCEINSTOCK).when(targetStockService).getInStockStatus(product, warehouse);
        doReturn(Integer.valueOf(0)).when(targetStockService).getStockLevelAmount(product, warehouse);
        given(orderEntry.getProduct()).willReturn(product);
        given(orderEntry.getQuantity()).willReturn(Long.valueOf(5));

        assertThat(targetStockService.isOrderEntryInStockAtWarehouse(orderEntry, warehouse)).isTrue();
    }


    @Test(expected = IllegalArgumentException.class)
    public void testIsOrderEntryInStockAtWarehouseForNullOrderEntry() {
        targetStockService.isOrderEntryInStockAtWarehouse(null, warehouse);
    }

    @Test
    public void testIsOrderEntryInStockAtWarehouseForNullProductInEntry() {
        given(orderEntry.getProduct()).willReturn(null);
        assertThat(targetStockService.isOrderEntryInStockAtWarehouse(orderEntry, warehouse)).isFalse();
    }

    @Test
    public void testIsOrderEntryInStockAtWarehouseForNullQuantityInEntry() {
        given(orderEntry.getQuantity()).willReturn(null);
        assertThat(targetStockService.isOrderEntryInStockAtWarehouse(orderEntry, warehouse)).isFalse();
    }

    @Test
    public void testTransferReserveAmount() throws InsufficientStockLevelException {
        final WarehouseModel warehouseTo = mock(WarehouseModel.class);
        final WarehouseModel warehouseFrom = mock(WarehouseModel.class);
        given(warehouseFrom.getName()).willReturn("Fastline");
        given(warehouseTo.getName()).willReturn("testNSW");

        targetStockService.transferReserveAmount(product, 3, warehouseFrom, warehouseTo, "testCons");

        verify(targetStockService).release(product, warehouseFrom, 3,
                "Releasing stock from Fastline for consignment : testCons");
        verify(targetStockService).reserve(product, warehouseTo, 3,
                "Reserving stock in testNSW for consignment : testCons");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetProductStatusNullProduct() {
        targetStockService.getProductStatus(null);
    }

    @Test
    public void testGetProductStatus() {

        willReturn(Collections.singletonList(warehouse)).given(targetWarehouseDao)
                .getSurfaceStockOnlineWarehouses();
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON);
        willReturn(Integer.valueOf(10)).given(targetStockService)
                .calculateTotalActualAmount(Mockito.anyCollection());

        willReturn(StockLevelStatus.INSTOCK).given(targetStockService)
                .getProductStatus(product, warehouse);
        final StockLevelStatus result = targetStockService.getProductStatus(product);

        assertThat(result).isEqualTo(StockLevelStatus.INSTOCK);
    }

    @Test
    public void testGetProductOutOfStockStatus() {

        willReturn(Collections.singletonList(warehouse)).given(targetWarehouseDao)

        .getSurfaceStockOnlineWarehouses();
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON);
        targetStockService.setDefaultLowStockThreshold(5);
        willReturn(Integer.valueOf(0)).given(targetStockService)
                .calculateTotalActualAmount(Mockito.anyCollection());

        willReturn(StockLevelStatus.OUTOFSTOCK).given(targetStockService)
                .getProductStatus(product, warehouse);
        final StockLevelStatus result = targetStockService.getProductStatus(product);

        assertThat(result).isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    @Test
    public void testGetProductStatusNoWarehouses() {

        willReturn(Collections.EMPTY_LIST).given(targetWarehouseDao)
                .getSurfaceStockOnlineWarehouses();

        final StockLevelStatus result = targetStockService.getProductStatus(product);
        assertThat(result).isEqualTo(StockLevelStatus.OUTOFSTOCK);

    }

    @Test
    public void testGetProductStatusTwoWarehousesBothIn() {

        final List<WarehouseModel> warehouses = getTwoWarehouses(StockLevelStatus.INSTOCK, StockLevelStatus.INSTOCK);
        willReturn(warehouses).given(targetWarehouseDao).getSurfaceStockOnlineWarehouses();

        final StockLevelStatus result = targetStockService.getProductStatus(product);

        assertThat(result).isEqualTo(StockLevelStatus.INSTOCK);

    }

    @Test
    public void testGetProductStatusTwoWarehousesBothOut() {

        final List<WarehouseModel> warehouses = getTwoWarehouses(StockLevelStatus.OUTOFSTOCK,
                StockLevelStatus.OUTOFSTOCK);
        willReturn(warehouses).given(targetWarehouseDao).getSurfaceStockOnlineWarehouses();
        final StockLevelStatus result = targetStockService.getProductStatus(product);

        assertThat(result).isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    @Test
    public void testGetProductStatusTwoWarehousesInOut() {

        final List<WarehouseModel> warehouses = getTwoWarehouses(StockLevelStatus.INSTOCK, StockLevelStatus.OUTOFSTOCK);
        willReturn(warehouses).given(targetWarehouseDao).getSurfaceStockOnlineWarehouses();

        final StockLevelStatus result = targetStockService.getProductStatus(product);

        assertThat(result).isEqualTo(StockLevelStatus.INSTOCK);
    }

    @Test
    public void testGetProductStatusTwoWarehousesOutIn() {

        final List<WarehouseModel> warehouses = getTwoWarehouses(StockLevelStatus.OUTOFSTOCK, StockLevelStatus.INSTOCK);
        willReturn(warehouses).given(targetWarehouseDao).getSurfaceStockOnlineWarehouses();

        final StockLevelStatus result = targetStockService.getProductStatus(product);

        assertThat(result).isEqualTo(StockLevelStatus.INSTOCK);
    }

    @Test
    public void testGetProductStatusTwoWarehousesLowIn() {

        final List<WarehouseModel> warehouses = getTwoWarehouses(StockLevelStatus.LOWSTOCK, StockLevelStatus.INSTOCK);
        willReturn(warehouses).given(targetWarehouseDao).getSurfaceStockOnlineWarehouses();

        final StockLevelStatus result = targetStockService.getProductStatus(product);

        assertThat(result).isEqualTo(StockLevelStatus.INSTOCK);
    }

    @Test
    public void testGetProductStatusTwoWarehousesLowOut() {

        final List<WarehouseModel> warehouses = getTwoWarehouses(StockLevelStatus.LOWSTOCK,
                StockLevelStatus.OUTOFSTOCK);
        willReturn(warehouses).given(targetWarehouseDao).getSurfaceStockOnlineWarehouses();

        final StockLevelStatus result = targetStockService.getProductStatus(product);

        assertThat(result).isEqualTo(StockLevelStatus.LOWSTOCK);
    }

    private List<WarehouseModel> getTwoWarehouses(final StockLevelStatus status1, final StockLevelStatus status2) {

        final WarehouseModel warehouseA = mock(WarehouseModel.class);
        final WarehouseModel warehouseB = mock(WarehouseModel.class);

        willReturn(status1).given(targetStockService).getProductStatus(product, warehouseA);
        willReturn(status2).given(targetStockService).getProductStatus(product, warehouseB);

        final StockLevelModel preOrderStockMockWarehouseA = mock(StockLevelModel.class);
        final StockLevelModel preOrderStockMockWarehouseB = mock(StockLevelModel.class);
        willReturn(preOrderStockMockWarehouseA).given(targetStockLevelDao).findStockLevel("testPreOrderProduct",
                warehouseA);
        willReturn(preOrderStockMockWarehouseB).given(targetStockLevelDao).findStockLevel("testPreOrderProduct",
                warehouseB);

        willReturn(status1).given(preOrderStockLevelStatusStrategy).checkStatus(preOrderStockMockWarehouseA);
        willReturn(status2).given(preOrderStockLevelStatusStrategy).checkStatus(preOrderStockMockWarehouseB);

        given(targetStockLevelDao.findStockLevel("testPreOrderProduct", warehouseA))
                .willReturn(preOrderStockMockWarehouseA);
        given(targetStockLevelDao.findStockLevel("testPreOrderProduct", warehouseB))
                .willReturn(preOrderStockMockWarehouseB);

        final List<WarehouseModel> warehouses = new ArrayList<>();
        warehouses.add(warehouseA);
        warehouses.add(warehouseB);
        return warehouses;
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetStockLevelAmountNullProduct() {
        targetStockService.getStockLevelAmountFromOnlineWarehouses(null);
    }

    @Test
    public void testGetStockLevelAmount() {

        willReturn(Collections.singletonList(warehouse)).given(targetWarehouseDao)
                .getSurfaceStockOnlineWarehouses();

        willReturn(Collections.singletonList(stockLevelMock)).given(targetStockService)
                .getStockLevels(eq(product), anyList());

        willReturn(Integer.valueOf(10)).given(targetStockService)
                .calculateTotalActualAmount(anyCollection());

        final int result = targetStockService.getStockLevelAmountFromOnlineWarehouses(product);

        assertThat(result).isEqualTo(10);
    }

    @Test(expected = InsufficientStockLevelException.class)
    public void testReserveWhenNoStockAtAll() throws InsufficientStockLevelException {
        given(targetStockService.getAllStockLevels(any(ProductModel.class))).willReturn(
                new ArrayList<StockLevelModel>());
        targetStockService.reserve(product, 1, "Test Prod");
    }

    @Test
    public void testReserveWhenNeedAdjustmentFalse() throws InsufficientStockLevelException {
        final StockLevelModel stockLevel = mock(StockLevelModel.class);
        final WarehouseModel productWarehouse = mock(WarehouseModel.class);
        given(productWarehouse.getCode()).willReturn("Incomm");
        given(stockLevel.getWarehouse()).willReturn(productWarehouse);
        final Collection<StockLevelModel> stockLevels = new ArrayList<>();
        stockLevels.add(stockLevel);
        given(targetStockService.getAllStockLevels(product)).willReturn(
                stockLevels);
        given(Boolean.valueOf(targetWarehouseService.isStockAdjustmentRequired(productWarehouse)))
                .willReturn(
                        Boolean.FALSE);
        targetStockService.reserve(product, 1, "Test Prod");
        verify(targetStockService, times(0)).reserve(any(ProductModel.class),
                any(WarehouseModel.class), anyInt(),
                any(String.class));
    }

    @Test
    public void testReserveWhenNeedAdjustmentTrue() throws InsufficientStockLevelException {
        final StockLevelModel stockLevel = new StockLevelModel();
        final WarehouseModel productWarehouse = new WarehouseModel();
        productWarehouse.setCode("Cnp");
        productWarehouse.setNeedStockAdjustment(true);
        stockLevel.setWarehouse(productWarehouse);
        final Collection<StockLevelModel> stockLevels = new ArrayList<>();
        stockLevels.add(stockLevel);
        given(Boolean.valueOf(targetWarehouseService.isStockAdjustmentRequired(productWarehouse)))
                .willReturn(
                        Boolean.TRUE);
        given(targetStockService.getAllStockLevels(product)).willReturn(
                stockLevels);
        given(targetStockLevelDao.reserve(stockLevel, 1, false)).willReturn(Integer.valueOf(1));
        targetStockService.reserve(product, 1, "Test Prod");
        verify(stockLevelHistoryEntryModel).setReserved(1);
        verify(stockLevelHistoryEntryModel).setComment("Test Prod");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReserveZeroQuantity() throws InsufficientStockLevelException {
        targetStockService.reserve(product, 0, "Test Prod");
    }

    @Test
    public void testPreOrderReserveWhenNeedAdjustmentTrue() throws InsufficientStockLevelException {
        final StockLevelModel stockLevel = mock(StockLevelModel.class);
        final WarehouseModel productWarehouse = mock(WarehouseModel.class);
        given(productWarehouse.getCode()).willReturn("fastline");
        given(stockLevel.getWarehouse()).willReturn(productWarehouse);
        willReturn(Integer.valueOf(1)).given(stockLevel).getReserved();
        willReturn(Integer.valueOf(100)).given(stockLevel).getMaxPreOrder();
        given(Boolean.valueOf(targetWarehouseService.isStockAdjustmentRequired(productWarehouse)))
                .willReturn(
                        Boolean.TRUE);
        final Collection<StockLevelModel> stockLevels = new ArrayList<>();
        stockLevels.add(stockLevel);
        given(targetStockService.getAllStockLevels(preOrderProduct)).willReturn(stockLevels);
        given(targetStockLevelDao.reserve(stockLevel, 2, true)).willReturn(Integer.valueOf(2));
        targetStockService.reserve(preOrderProduct, 2, "Test PreOrder Product");
        verify(stockLevelHistoryEntryModel).setReserved(1);
        verify(stockLevelHistoryEntryModel).setComment("Test PreOrder Product");
        verify(stockLevelHistoryEntryModel).setPreOrderQuantity(100);
    }

    @Test
    public void testReleaseWhenNoStockAtAll() throws InsufficientStockLevelException {
        given(targetStockService.getAllStockLevels(any(ProductModel.class))).willReturn(
                null);
        targetStockService.release(product, 1, "Test Prod");
        verify(targetStockService, times(0)).release(any(ProductModel.class),
                any(WarehouseModel.class), anyInt(),
                any(String.class));
    }

    @Test
    public void testReleasePreOrderWhenNoStockAtAll() throws InsufficientStockLevelException {
        given(targetStockService.getAllStockLevels(preOrderProduct)).willReturn(
                null);
        targetStockService.release(preOrderProduct, 1, "Test Pre-Prod");
        verify(targetStockLevelDao, never()).release(null, 1, true);
    }

    @Test
    public void testReleaseWhenNeedStockAdjustmentFalse() throws InsufficientStockLevelException {
        final StockLevelModel stockLevel = new StockLevelModel();
        final WarehouseModel productWarehouse = new WarehouseModel();
        productWarehouse.setCode("Incomm");
        productWarehouse.setNeedStockAdjustment(false);
        stockLevel.setWarehouse(productWarehouse);
        final Collection<StockLevelModel> stockLevels = new ArrayList<>();
        stockLevels.add(stockLevel);
        given(targetStockService.getAllStockLevels(product)).willReturn(
                stockLevels);
        given(Boolean.valueOf(targetWarehouseService.isStockAdjustmentRequired(productWarehouse)))
                .willReturn(
                        Boolean.FALSE);
        targetStockService.release(product, 1, "Test Prod");
        verify(targetStockService, times(0)).release(any(ProductModel.class),
                any(WarehouseModel.class), anyInt(),
                any(String.class));
    }

    @Test
    public void testReleasePreOrderWhenNeedStockAdjustmentFalse() throws InsufficientStockLevelException {
        final StockLevelModel stockLevel = mock(StockLevelModel.class);
        final WarehouseModel productWarehouse = mock(WarehouseModel.class);
        given(productWarehouse.getCode()).willReturn("Incomm");
        willReturn(Boolean.FALSE).given(productWarehouse).isNeedStockAdjustment();
        given(stockLevel.getWarehouse()).willReturn(productWarehouse);
        final Collection<StockLevelModel> stockLevels = new ArrayList<>();
        stockLevels.add(stockLevel);
        given(targetStockService.getAllStockLevels(product)).willReturn(
                stockLevels);
        given(Boolean.valueOf(targetWarehouseService.isStockAdjustmentRequired(productWarehouse)))
                .willReturn(
                        Boolean.FALSE);
        targetStockService.release(preOrderProduct, 1, "Test Prod");
        verify(targetStockLevelDao, never()).release(stockLevel, 1, false);
    }

    @Test
    public void testReleaseWhenNeedStockAdjustmentTrue() throws InsufficientStockLevelException {
        final StockLevelModel stockLevel = mock(StockLevelModel.class);
        final WarehouseModel productWarehouse = mock(WarehouseModel.class);
        given(productWarehouse.getCode()).willReturn("Fastline");
        willReturn(Boolean.TRUE).given(productWarehouse).isNeedStockAdjustment();
        given(stockLevel.getWarehouse()).willReturn(productWarehouse);
        final Collection<StockLevelModel> stockLevels = new ArrayList<>();
        stockLevels.add(stockLevel);
        given(targetStockService.getAllStockLevels(product)).willReturn(
                stockLevels);
        given(Boolean.valueOf(targetWarehouseService.isStockAdjustmentRequired(productWarehouse)))
                .willReturn(
                        Boolean.TRUE);
        targetStockService.release(product, 1, "Test Prod");

        verify(targetStockLevelDao).release(stockLevel, 1, false);
    }

    @Test
    public void testReleasePreOrderWhenNeedStockAdjustmentTrue() throws InsufficientStockLevelException {
        final StockLevelModel stockLevel = mock(StockLevelModel.class);
        final WarehouseModel productWarehouse = mock(WarehouseModel.class);
        given(productWarehouse.getCode()).willReturn("Fastline");
        willReturn(Boolean.TRUE).given(productWarehouse).isNeedStockAdjustment();
        given(stockLevel.getWarehouse()).willReturn(productWarehouse);
        final Collection<StockLevelModel> stockLevels = new ArrayList<>();
        stockLevels.add(stockLevel);
        given(targetStockService.getAllStockLevels(preOrderProduct)).willReturn(
                stockLevels);
        given(Boolean.valueOf(targetWarehouseService.isStockAdjustmentRequired(productWarehouse)))
                .willReturn(
                        Boolean.TRUE);
        targetStockService.release(preOrderProduct, 1, "Test Pre-order Prod");
        verify(targetStockLevelDao).release(stockLevel, 1, true);

    }

    @Test
    public void testGetStockLevelAmountNoWarehouses() {

        willReturn(Collections.EMPTY_LIST).given(targetWarehouseDao)
                .getSurfaceStockOnlineWarehouses();

        final int result = targetStockService.getStockLevelAmountFromOnlineWarehouses(product);

        assertThat(result).isEqualTo(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetStockLevelAndStatusAmountFromOnlineWarehousesNullProduct() {
        targetStockService.getStockLevelAndStatusAmount(null);
        verifyZeroInteractions(targetWarehouseDao);
    }

    @Test
    public void testGetStockLevelAndStatusAmountFromOnlineWarehousesEmptyStock() {
        given(targetWarehouseDao.getSurfaceStockOnlineWarehouses()).willReturn(ListUtils.EMPTY_LIST);
        final StockData stockInWarehouse = targetStockService
                .getStockLevelAndStatusAmount(product);

        assertThat(StockLevelStatus.OUTOFSTOCK).isEqualTo(stockInWarehouse.getStockLevelStatus());
        assertThat(stockInWarehouse.getStockLevel().longValue()).isEqualTo(0);


        verify(targetWarehouseDao).getSurfaceStockOnlineWarehouses();
        verifyNoMoreInteractions(targetWarehouseDao);
    }

    @Test
    public void testGetStockLevelAndStatusAmountFromOnlineWarehousesSingleWarehouse() {
        given(targetWarehouseDao.getSurfaceStockOnlineWarehouses()).willReturn(
                Collections.singletonList(warehouse));
        willReturn(Integer.valueOf(10)).given(targetStockService)
                .calculateTotalActualAmount(anyCollection());
        willReturn(StockLevelStatus.INSTOCK).given(targetStockService)
                .getProductStatus(product, warehouse);
        final StockData stockInWarehouse = targetStockService
                .getStockLevelAndStatusAmount(product);

        assertThat(StockLevelStatus.INSTOCK).isEqualTo(stockInWarehouse.getStockLevelStatus());
        assertThat(stockInWarehouse.getStockLevel().longValue()).isEqualTo(10L);


        verify(targetWarehouseDao).getSurfaceStockOnlineWarehouses();
        verifyNoMoreInteractions(targetWarehouseDao);
    }

    @Test
    public void getStockForProductsInWarehouseTestWhenStockReturned() {
        final Map<String, Integer> stockLevelMap = mock(HashMap.class);
        final List<String> itemCodes = mock(ArrayList.class);
        given(targetStockLevelDao.findStockForProductCodesAndWarehouse(itemCodes, null))
                .willReturn(stockLevelMap);
        assertThat(targetStockService.getStockForProductsInWarehouse(itemCodes, null))
                .isEqualTo(stockLevelMap);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetStockLevelAmountPreOrderNullProduct() {
        targetStockService.getStockLevelAmountFromPreOrderWarehouses(null);
    }

    @Test
    public void testGetStockLevelAmountPreOrderWarehouseWhenEmptyWarehouses() {

        given(targetWarehouseDao
                .getPreOrderWarehouses()).willReturn(Collections.EMPTY_LIST);

        final int result = targetStockService.getStockLevelAmountFromPreOrderWarehouses(preOrderProduct);

        verify(targetWarehouseDao).getPreOrderWarehouses();
        assertThat(result).isEqualTo(0);
    }

    @Test
    public void testGetStockLevelAmountPreOrderWarehouseForNonPreOrderProduct() {

        given(targetWarehouseDao
                .getPreOrderWarehouses()).willReturn(Collections.singletonList(warehouse));

        final Calendar start = Calendar.getInstance();
        start.set(2018, Calendar.FEBRUARY, 15, 0, 0, 0);

        final Calendar end = Calendar.getInstance();
        end.set(2018, Calendar.MARCH, 13, 0, 0, 0);
        given(preOrderProduct.getPreOrderStartDateTime()).willReturn(start.getTime());
        given(preOrderProduct.getPreOrderEndDateTime()).willReturn(end.getTime());

        final int result = targetStockService.getStockLevelAmountFromPreOrderWarehouses(preOrderProduct);

        assertThat(result).isEqualTo(0);
    }

    @Test
    public void testGetStockLevelAmountPreOrderWarehouseSuccess() {

        final List<WarehouseModel> warehouseList = Collections.singletonList(warehouse);
        given(targetWarehouseDao
                .getPreOrderWarehouses()).willReturn(warehouseList);

        willReturn(new Integer(500)).given(stockLevelMock).getMaxPreOrder();
        willReturn(new Integer(450)).given(stockLevelMock).getPreOrder();
        willReturn(Collections.singletonList(stockLevelMock)).given(targetStockService)
                .getStockLevels(eq(preOrderProduct), eq(warehouseList));

        final int result = targetStockService.getStockLevelAmountFromPreOrderWarehouses(preOrderProduct);

        assertThat(result).isEqualTo(50);
    }

    @Test
    public void testGetStockLevelAmountFromOnlineWarehousesWithEmbargoedProduct() {
        final Calendar embargoDate = Calendar.getInstance();
        embargoDate.set(embargoDate.get(Calendar.YEAR), embargoDate.get(Calendar.MONTH) + 2, 22, 0, 0, 0);
        given(preOrderProduct.getNormalSaleStartDateTime()).willReturn(embargoDate.getTime());

        final int result = targetStockService.getStockLevelAmountFromOnlineWarehouses(preOrderProduct);

        verify(targetWarehouseDao, never()).getSurfaceStockOnlineWarehouses();
        assertThat(result).isEqualTo(0);
    }

    @Test
    public void testGetStockLevelFromOnlineWarehousesWithEmbargoedProduct() {
        final Calendar embargoDate = Calendar.getInstance();
        embargoDate.set(embargoDate.get(Calendar.YEAR), embargoDate.get(Calendar.MONTH) + 2, 22, 0, 0, 0);
        given(preOrderProduct.getNormalSaleStartDateTime()).willReturn(embargoDate.getTime());

        final StockData stockData = targetStockService
                .getStockLevelAndStatusAmount(preOrderProduct);

        verify(targetWarehouseDao, never()).getSurfaceStockOnlineWarehouses();
        assertThat(stockData).isNotNull();
        assertThat(stockData.getStockLevel()).isEqualTo(0L);
    }

    @Test
    public void testCreateStockLevelHistoryPreOrderProduct() {

        final Calendar start = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), Calendar.FEBRUARY, 15, 0, 0, 0);

        final Calendar end = Calendar.getInstance();
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 2, 22, 0, 0, 0);

        given(preOrderProduct.getPreOrderEndDateTime()).willReturn(end.getTime());
        given(preOrderProduct.getPreOrderStartDateTime()).willReturn(start.getTime());

        given(stockLevelMock.getProduct()).willReturn(preOrderProduct);
        willReturn(Integer.valueOf(5)).given(stockLevelMock).getReserved();

        final StockLevelHistoryEntryModel historyEntry = mock(StockLevelHistoryEntryModel.class);
        given(mockModelService.create(StockLevelHistoryEntryModel.class)).willReturn(historyEntry);
        targetStockService.createStockLevelHistoryEntry(stockLevelMock,
                StockLevelUpdateType.WAREHOUSE, 1,
                "PreOrder reserve", true);

        verify(stockLevelMock, never()).getPreOrder();
        verify(historyEntry).setReservedPreOrderQuantity(1);
        verify(historyEntry).setReserved(5);
        verify(historyEntry).setComment("PreOrder reserve");
    }

    @Test
    public void testCreateStockLevelHistoryNormalProduct() {

        given(stockLevelMock.getProduct()).willReturn(product);
        willReturn(Integer.valueOf(5)).given(stockLevelMock).getReserved();

        final StockLevelHistoryEntryModel historyEntry = mock(StockLevelHistoryEntryModel.class);
        given(mockModelService.create(StockLevelHistoryEntryModel.class)).willReturn(historyEntry);
        targetStockService.createStockLevelHistoryEntry(stockLevelMock,
                StockLevelUpdateType.WAREHOUSE, 10,
                "Normal reserve");

        verify(stockLevelMock, never()).getReserved();
        verify(historyEntry).setReservedPreOrderQuantity(0);
        verify(historyEntry).setReserved(10);
        verify(historyEntry).setComment("Normal reserve");
    }

    @Test
    public void testReserveForStockReservationWarehouse() throws InsufficientStockLevelException {
        final StockLevelModel stockLevel = new StockLevelModel();
        final WarehouseModel productWarehouse = new WarehouseModel();
        productWarehouse.setCode("StockReservationWarehouse");
        productWarehouse.setNeedStockAdjustment(true);
        stockLevel.setWarehouse(productWarehouse);
        final Collection<StockLevelModel> stockLevels = new ArrayList<>();
        stockLevels.add(stockLevel);
        willReturn(Boolean.TRUE).given(targetWarehouseService).isStockAdjustmentRequired(productWarehouse);
        given(targetStockService.getAllStockLevels(product)).willReturn(
                stockLevels);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService).isFeatureEnabled("fastlineFalcon");
        targetStockService.reserve(product, 1, "Test Prod");
    }

    @Test
    public void testUpdateQuantityFulfilledByFastlineWarehouse() throws InterruptedException {
        Thread.sleep(50);
        targetStockService.updateQuantityFulfilledByWarehouse(7, warehouse);
        verify(targetStockService, times(1)).updateQuantityFulfilledByWarehouse(anyInt(), any(WarehouseModel.class));
    }

    @Test
    public void testGetStockLevelForOnlineWarehouseAmountWhenFastlineDialOn() {

        willReturn(Collections.singletonList(warehouse)).given(targetWarehouseDao)
                .getSurfaceStockOnlineWarehouses();

        willReturn(Collections.singletonList(stockLevelMock)).given(targetStockService)
                .getStockLevels(Mockito.eq(product), Mockito.anyList());

        willReturn(Integer.valueOf(10)).given(targetStockService)
                .calculateTotalActualAmount(Mockito.anyCollection());

        willReturn(Integer.valueOf(6)).given(targetSharedConfigService)
                .getInt(TgtCoreConstants.Config.MAX_QTY_FASTLINE_FULFILLED_PER_DAY, 100);

        willReturn(warehouse).given(targetWarehouseService).getDefaultOnlineWarehouse();
        willReturn(Integer.valueOf(2)).given(warehouse).getTotalQtyFulfilledAtFastlineToday();
        willReturn(Integer.valueOf(5)).given(stockLevelMock).getAvailable();

        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON);

        final int result = targetStockService.getStockLevelAmountFromOnlineWarehouses(product);

        assertThat(10).isEqualTo(result);
    }

    @Test
    public void testGetStockLevelForOnlineWarehouseAmountWhenFastlineDialOff() {

        willReturn(Collections.singletonList(warehouse)).given(targetWarehouseDao)
                .getSurfaceStockOnlineWarehouses();

        willReturn(Collections.singletonList(stockLevelMock)).given(targetStockService)
                .getStockLevels(Mockito.eq(product), Mockito.anyList());

        willReturn(Integer.valueOf(10)).given(targetStockService)
                .calculateTotalActualAmount(Mockito.anyCollection());

        willReturn(Integer.valueOf(6)).given(targetSharedConfigService)
                .getInt(TgtCoreConstants.Config.MAX_QTY_FASTLINE_FULFILLED_PER_DAY, 100);

        willReturn(warehouse).given(targetWarehouseService).getDefaultOnlineWarehouse();
        willReturn(Integer.valueOf(2)).given(warehouse).getTotalQtyFulfilledAtFastlineToday();

        willReturn(Boolean.FALSE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON);

        final int result = targetStockService.getStockLevelAmountFromOnlineWarehouses(product);

        assertThat(10).isEqualTo(result);
    }

    @Test
    public void testPreOrderProductStatusNoWarehouse() {
        final StockLevelStatus status = targetStockService.getPreOrderProductStatus(preOrderProduct,
                Collections.EMPTY_LIST);

        assertThat(status).isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    @Test
    public void testPreOrderStockStatusTwoWarehousesInStock() {
        final List<WarehouseModel> warehouses = getTwoWarehouses(StockLevelStatus.INSTOCK, StockLevelStatus.INSTOCK);

        final StockLevelStatus status = targetStockService.getPreOrderProductStatus(preOrderProduct, warehouses);

        assertThat(status).isEqualTo(StockLevelStatus.INSTOCK);
    }

    @Test
    public void testPreOrderStockStatusOneWarehousesInStock() {

        given(preOrderStockLevelStatusStrategy.checkStatus(stockLevelMock)).willReturn(StockLevelStatus.INSTOCK);
        final StockLevelStatus status = targetStockService.getPreOrderProductStatus(preOrderProduct,
                Collections.singletonList(warehouse));

        assertThat(status).isEqualTo(StockLevelStatus.INSTOCK);
    }

    @Test
    public void testPreOrderStockStatusTwoWarehousesOutOfStock() {
        final List<WarehouseModel> warehouses = getTwoWarehouses(StockLevelStatus.OUTOFSTOCK,
                StockLevelStatus.OUTOFSTOCK);

        final StockLevelStatus status = targetStockService.getPreOrderProductStatus(preOrderProduct, warehouses);

        assertThat(status).isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    @Test
    public void testPreOrderStockStatusTwoWarehousesLowStock() {
        final List<WarehouseModel> warehouses = getTwoWarehouses(StockLevelStatus.LOWSTOCK,
                StockLevelStatus.LOWSTOCK);

        final StockLevelStatus status = targetStockService.getPreOrderProductStatus(preOrderProduct, warehouses);

        assertThat(status).isEqualTo(StockLevelStatus.INSTOCK);
    }

    @Test
    public void testPreOrderStockStatusTwoWarehousesOneLowStockAndOtherInStock() {
        final List<WarehouseModel> warehouses = getTwoWarehouses(StockLevelStatus.INSTOCK,
                StockLevelStatus.LOWSTOCK);

        final StockLevelStatus status = targetStockService.getPreOrderProductStatus(preOrderProduct, warehouses);

        assertThat(status).isEqualTo(StockLevelStatus.INSTOCK);
    }

    @Test
    public void testPreOrderStockStatusTwoWarehousesOneLowStockAndOtherOutOfStock() {
        final List<WarehouseModel> warehouses = getTwoWarehouses(StockLevelStatus.LOWSTOCK,
                StockLevelStatus.OUTOFSTOCK);

        final StockLevelStatus status = targetStockService.getPreOrderProductStatus(preOrderProduct, warehouses);

        assertThat(status).isEqualTo(StockLevelStatus.INSTOCK);
    }

    /**
     * Method will throw an exception if the product is null while retrieving pre-order product's stock level status
     * from warehouses.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetProductStatusFromPreOrderWarehousesForNullProduct() {
        targetStockService.getProductStatus(null);
    }

    /**
     * Method to verify INSTOCK level status for the pre-order product.
     */
    @Test
    public void testGetProductStatusFromPreOrderWarehousesWithInstockLevelStatus() {
        willReturn(Collections.singletonList(warehouse)).given(targetWarehouseDao).getPreOrderWarehouses();
        willReturn(StockLevelStatus.INSTOCK).given(preOrderStockLevelStatusStrategy).checkStatus(stockLevelMock);
        final StockLevelStatus stockLevelStatus = targetStockService
                .getProductStatus(preOrderProduct);
        assertThat(stockLevelStatus).isEqualTo(StockLevelStatus.INSTOCK);
    }

    /**
     * Method to verify OUTOFSTOCK level status for the pre-order product.
     */
    @Test
    public void testGetProductStatusFromPreOrderWarehousesWithOutOfStockLevelStatus() {

        willReturn(Collections.singletonList(warehouse)).given(targetWarehouseDao).getPreOrderWarehouses();
        willReturn(StockLevelStatus.OUTOFSTOCK).given(preOrderStockLevelStatusStrategy).checkStatus(stockLevelMock);
        final StockLevelStatus stockLevelStatus = targetStockService
                .getProductStatus(preOrderProduct);
        assertThat(stockLevelStatus).isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    /**
     * Method to verify OUTOFSTOCK level status for the pre-order product. This test case will be executed in case of
     * absence of warehouses.
     */
    @Test
    public void testGetProductStatusFromPreOrderWarehousesWithNoWarehouses() {
        willReturn(Collections.EMPTY_LIST).given(targetWarehouseDao).getPreOrderWarehouses();
        final StockLevelStatus stockLevelStatus = targetStockService
                .getProductStatus(preOrderProduct);
        assertThat(stockLevelStatus).isEqualTo(StockLevelStatus.OUTOFSTOCK);

    }

    /**
     * Method to verify INSTOCK level status for the pre-order product. This test case will check the presence of
     * products in two warehouses(both warehouses has pre-order products stock available).
     */
    @Test
    public void testGetProductStatusFromPreOrderWarehousesWithTwoWarehousesBothInStock() {
        final List<WarehouseModel> warehouses = getTwoWarehouses(StockLevelStatus.INSTOCK, StockLevelStatus.INSTOCK);
        final StockLevelStatus stockLevelStatus = retriveStockLevelStatusForPreOrderProduct(warehouses);
        assertThat(stockLevelStatus).isEqualTo(StockLevelStatus.INSTOCK);

    }

    /**
     * Method to verify OUTOFSTOCK level status for the pre-order product. This test case will check the presence of
     * products in two warehouses(both warehouses does not have pre-order product stocks).
     */
    @Test
    public void testGetProductStatusFromPreOrderWarehousesWithBothOutStock() {
        final List<WarehouseModel> warehouses = getTwoWarehouses(StockLevelStatus.OUTOFSTOCK,
                StockLevelStatus.OUTOFSTOCK);
        final StockLevelStatus stockLevelStatus = retriveStockLevelStatusForPreOrderProduct(warehouses);
        assertThat(stockLevelStatus).isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    /**
     * Method to verify INSTOCK level status for the pre-order product. This test case will check the presence of
     * products in two warehouses(one warehouse has stocks but other one does not have any stocks).
     */
    @Test
    public void testGetProductStatusFromPreOrderWarehousesWithInOutStock() {
        final List<WarehouseModel> warehouses = getTwoWarehouses(StockLevelStatus.INSTOCK, StockLevelStatus.OUTOFSTOCK);
        final StockLevelStatus stockLevelStatus = retriveStockLevelStatusForPreOrderProduct(warehouses);
        assertThat(stockLevelStatus).isEqualTo(StockLevelStatus.INSTOCK);
    }

    /**
     * Method to verify INSTOCK level status for the pre-order product. This test case will check the presence of
     * products in two warehouses(one warehouse have no stocks but other one have stocks).
     */
    @Test
    public void testGetProductStatusFromPreOrderWarehousesWithOutInStock() {
        final List<WarehouseModel> warehouses = getTwoWarehouses(StockLevelStatus.OUTOFSTOCK, StockLevelStatus.INSTOCK);
        final StockLevelStatus stockLevelStatus = retriveStockLevelStatusForPreOrderProduct(warehouses);
        assertThat(stockLevelStatus).isEqualTo(StockLevelStatus.INSTOCK);
    }

    /**
     * Method will throw an exception(if the product is null) while retrieving product's stock level status from
     * warehouses.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetProductStatusForNullProduct() {
        targetStockService.getProductStatus(null);
    }

    /**
     * Method to verify INSTOCK product status of colour variant pre-order product.
     */
    @Test
    public void testGetProductStatusForPreOrderColourVariantProductWithInStockStatus() {
        willReturn(StockLevelStatus.INSTOCK).given(targetStockService)
                .getProductStatus(preOrderProduct);
        final StockLevelStatus stockLevelStatus = targetStockService.getProductStatus(preOrderProduct);
        assertThat(stockLevelStatus).isEqualTo(StockLevelStatus.INSTOCK);
    }

    /**
     * Method to verify OUTOFSTOCK product status of colour variant pre-order product.
     */
    @Test
    public void testGetProductStatusForPreOrderColourVariantProductWithOutOfStockStatus() {
        willReturn(StockLevelStatus.OUTOFSTOCK).given(targetStockService)
                .getProductStatus(preOrderProduct);
        final StockLevelStatus stockLevelStatus = targetStockService.getProductStatus(preOrderProduct);
        assertThat(stockLevelStatus).isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    /**
     * Method to verify INSTOCK product status of size variant pre-order product.
     */
    @Test
    public void testGetProductStatusForPreOrderSizeVariantProductWithInStockStatus() {
        willReturn(preOrderProduct).given(targetSizeVariantPreOrderProduct).getBaseProduct();
        willReturn(Collections.singletonList(warehouse)).given(targetWarehouseDao).getPreOrderWarehouses();
        willReturn(StockLevelStatus.INSTOCK).given(preOrderStockLevelStatusStrategy).checkStatus(stockLevelMock);
        final StockLevelStatus stockLevelStatus = targetStockService.getProductStatus(targetSizeVariantPreOrderProduct);
        assertThat(stockLevelStatus).isEqualTo(StockLevelStatus.INSTOCK);
    }

    /**
     * Method to verify OUTOFSTOCK product status of size variant pre-order product.
     */
    @Test
    public void testGetProductStatusForPreOrderSizeVariantProductWithOutOfStockStatus() {
        willReturn(preOrderProduct).given(targetSizeVariantPreOrderProduct).getBaseProduct();
        willReturn(Collections.singletonList(warehouse)).given(targetWarehouseDao).getPreOrderWarehouses();
        willReturn(StockLevelStatus.OUTOFSTOCK).given(preOrderStockLevelStatusStrategy).checkStatus(stockLevelMock);
        final StockLevelStatus stockLevelStatus = targetStockService.getProductStatus(targetSizeVariantPreOrderProduct);
        assertThat(stockLevelStatus).isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    /**
     * Method to verify INSTOCK product status for normal product.
     */
    @Test
    public void testGetProductStatusForNormalProductWithInStockStatus() {
        willReturn(StockLevelStatus.INSTOCK).given(targetStockService)
                .getProductStatus(product);
        final StockLevelStatus stockLevelStatus = targetStockService.getProductStatus(product);
        assertThat(stockLevelStatus).isEqualTo(StockLevelStatus.INSTOCK);
    }

    /**
     * Method to verify OUTOFSTOCK product status for normal product.
     */
    @Test
    public void testGetProductStatusForNormalProductWithOutOfStockStatus() {
        willReturn(StockLevelStatus.OUTOFSTOCK).given(targetStockService)
                .getProductStatus(product);
        final StockLevelStatus stockLevelStatus = targetStockService.getProductStatus(product);
        assertThat(stockLevelStatus).isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    /**
     * Method to verify LOWSTOCK product status for normal product.
     */
    @Test
    public void testGetProductStatusForNormalProductWithLowStockStatus() {
        willReturn(StockLevelStatus.LOWSTOCK).given(targetStockService)
                .getProductStatus(product);
        final StockLevelStatus stockLevelStatus = targetStockService.getProductStatus(product);
        assertThat(stockLevelStatus).isEqualTo(StockLevelStatus.LOWSTOCK);
    }

    /**
     * @param warehouses
     * @return stock level status
     */
    private StockLevelStatus retriveStockLevelStatusForPreOrderProduct(final List<WarehouseModel> warehouses) {
        willReturn(warehouses).given(targetWarehouseDao).getPreOrderWarehouses();
        final StockLevelStatus stockLevelStatus = targetStockService
                .getProductStatus(preOrderProduct);
        return stockLevelStatus;
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetStockLevelAndStatusAmountFromPreOrderWarehousesNullProduct() {
        targetStockService.getStockLevelAndStatusAmount(null);
        verifyZeroInteractions(targetWarehouseDao);
    }

    @Test
    public void testGetStockLevelAndStatusAmountFromPreOrderWarehousesEmptyStock() {
        given(targetWarehouseDao.getPreOrderWarehouses()).willReturn(ListUtils.EMPTY_LIST);
        final StockData stockInWarehouse = targetStockService
                .getStockLevelAndStatusAmount(preOrderProduct);

        assertThat(StockLevelStatus.OUTOFSTOCK).isEqualTo(stockInWarehouse.getStockLevelStatus());
        assertThat(stockInWarehouse.getStockLevel().longValue()).isEqualTo(0);


        verify(targetWarehouseDao).getPreOrderWarehouses();
        verifyNoMoreInteractions(targetWarehouseDao);
    }

    @Test
    public void testGetStockLevelAndStatusAmountFromPreOrderWarehouses() {
        final List<WarehouseModel> warehouses = Collections.singletonList(warehouse);
        given(targetWarehouseDao.getPreOrderWarehouses()).willReturn(
                warehouses);

        final StockLevelModel stockModel = mock(StockLevelModel.class);
        willReturn(Integer.valueOf(50)).given(stockModel).getMaxPreOrder();
        willReturn(Integer.valueOf(30)).given(stockModel).getPreOrder();
        given(targetStockService.getStockLevels(preOrderProduct, warehouses))
                .willReturn(Collections.singletonList(stockModel));

        given(preOrderStockLevelStatusStrategy.checkStatus(stockLevelMock)).willReturn(StockLevelStatus.INSTOCK);

        final StockData stockInWarehouse = targetStockService
                .getStockLevelAndStatusAmount(preOrderProduct);

        assertThat(StockLevelStatus.INSTOCK).isEqualTo(stockInWarehouse.getStockLevelStatus());
        assertThat(stockInWarehouse.getStockLevel().longValue()).isEqualTo(20L);


        verify(targetWarehouseDao).getPreOrderWarehouses();
        verifyNoMoreInteractions(targetWarehouseDao);

    }

    @Test
    public void testGetStockLevelAmountWithStockLevel() {
        willReturn(Integer.valueOf(200)).given(stockLevelMock).getAvailable();
        willReturn(Integer.valueOf(10)).given(stockLevelMock).getReserved();
        given(targetStockLevelDao.findStockLevel(anyString(), (WarehouseModel)any()))
                .willReturn(stockLevelMock);
        final int amount = targetStockService.getStockLevelAmount(product, warehouse);
        assertThat(amount).isNotNull().isEqualTo(190);
    }

    @Test
    public void testGetStockLevelAmountWithNoStockLevel() {
        given(targetStockLevelDao.findStockLevel(anyString(), (WarehouseModel)any()))
                .willReturn(null);
        final int amount = targetStockService.getStockLevelAmount(product, warehouse);
        assertThat(amount).isNotNull().isEqualTo(0);
    }
}

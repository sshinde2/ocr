/**
 * 
 */
package au.com.target.tgtcore.stock.strategies.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.ordersplitting.model.StockLevelModel;

import org.junit.Test;


/**
 * @author gsing236
 *
 */
public class PreOrderAvailabilityCalculationStrategyTest {

    private final PreOrderAvailabilityCalculationStrategy strategy = new PreOrderAvailabilityCalculationStrategy();

    @Test
    public void testGetAvailableToSellQuantity() {
        final StockLevelModel stock = new StockLevelModel();
        stock.setMaxPreOrder(100);
        stock.setPreOrder(20);

        final long availableToSellQuantity = strategy.getAvailableToSellQuantity(stock);
        assertThat(availableToSellQuantity).isEqualTo(80);
    }

    @Test
    public void testGetAvailableToSellQuantityZero() {
        final StockLevelModel stock = new StockLevelModel();
        stock.setMaxPreOrder(100);
        stock.setPreOrder(100);

        final long availableToSellQuantity = strategy.getAvailableToSellQuantity(stock);
        assertThat(availableToSellQuantity).isEqualTo(0);
    }

    @Test
    public void testGetAvailableToSellQuantityNegative() {
        final StockLevelModel stock = new StockLevelModel();
        stock.setMaxPreOrder(100);
        stock.setPreOrder(120);

        final long availableToSellQuantity = strategy.getAvailableToSellQuantity(stock);
        assertThat(availableToSellQuantity).isEqualTo(-20);
    }
}

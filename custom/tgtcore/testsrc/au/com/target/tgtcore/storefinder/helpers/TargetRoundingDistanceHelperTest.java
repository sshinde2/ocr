package au.com.target.tgtcore.storefinder.helpers;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.DistanceUnit;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;


@UnitTest
public class TargetRoundingDistanceHelperTest {

    private final BigDecimal decimalLimit = BigDecimal.valueOf(100d);

    private final TargetRoundingDistanceHelper targetRoundingDistanceHelper = new TargetRoundingDistanceHelper();

    @Before
    public void setUp() {
        targetRoundingDistanceHelper.setDecimalLimit(decimalLimit);
    }

    @Test
    public void testGetDistanceWithNoDistanceUnit() {
        final Double result = targetRoundingDistanceHelper.getDistance(null, 5.34d);

        assertThat(result).isEqualTo(5.34d);
    }

    @Test
    public void testGetDistanceWithDistanceInKmUnderLimit() {
        final Double result = targetRoundingDistanceHelper.getDistance(DistanceUnit.KM, 5.34d);

        assertThat(result).isEqualTo(5.34d);
    }

    @Test
    public void testGetDistanceWithDistanceInKmEqualToLimit() {
        final Double result = targetRoundingDistanceHelper.getDistance(DistanceUnit.KM, 100d);

        assertThat(result).isEqualTo(100d);
    }

    @Test
    public void testGetDistanceWithDistanceInKmOverLimit() {
        final Double result = targetRoundingDistanceHelper.getDistance(DistanceUnit.KM, 11345.74d);

        assertThat(result).isEqualTo(11346d);
    }

    @Test
    public void testGetDistanceWithDistanceInKmUnderLimitAndDistanceUnitInMiles() {
        final Double result = targetRoundingDistanceHelper.getDistance(DistanceUnit.MILES, 5.34d);

        assertThat(result).isEqualTo(3.3181157999999997d);
    }

    @Test
    public void testGetDistanceWithDistanceInKmEqualToLimitAndDistanceUnitInMiles() {
        final Double result = targetRoundingDistanceHelper.getDistance(DistanceUnit.MILES, 100d);

        assertThat(result).isEqualTo(62.137d);
    }

    @Test
    public void testGetDistanceWithDistanceInKmOverLimitAndDistanceUnitInMiles() {
        final Double result = targetRoundingDistanceHelper.getDistance(DistanceUnit.MILES, 11345.74d);

        assertThat(result).isEqualTo(7050d);
    }
}

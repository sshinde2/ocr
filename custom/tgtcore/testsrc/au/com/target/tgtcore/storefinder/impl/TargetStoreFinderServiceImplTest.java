/**
 *
 */
package au.com.target.tgtcore.storefinder.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.search.dao.PagedGenericDao;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.commerceservices.storefinder.impl.DefaultStoreFinderService;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.GeoWebServiceWrapper;
import de.hybris.platform.storelocator.PointOfServiceDao;
import de.hybris.platform.storelocator.data.AddressData;
import de.hybris.platform.storelocator.exception.GeoServiceWrapperException;
import de.hybris.platform.storelocator.impl.DefaultGPS;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.impl.TargetGPS;
import au.com.target.tgtcore.storelocator.pos.dao.TargetPointOfServiceDao;


/**
 * @author Pradeep a s
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetStoreFinderServiceImplTest {

    private static final int FIRST_PAGE = 0;

    private static final String LOCATION_TEXT = "Sydney";

    @Spy
    @InjectMocks
    private final TargetStoreFinderServiceImpl service = new TargetStoreFinderServiceImpl();

    @Mock
    private BaseStoreModel baseStoreModel;

    @Mock
    private GeoWebServiceWrapper geoWebServiceWrapper;

    @Mock
    private PointOfServiceDao pointOfServiceDao;

    @Mock
    private PagedGenericDao<PointOfServiceModel> pointOfServicePagedGenericDao;

    @Mock
    private GenericDao<PointOfServiceModel> pointOfServiceGenericDao;

    @Mock
    private TargetPointOfServiceDao targetPointOfServiceDao;

    @Mock
    private PageableData mockPageableData;

    @Mock
    private DefaultStoreFinderService defaultStoreFinderService;

    @Test
    public void testGenerateGeoAddressForSearchQueryInvalidLocationText()
    {
        final AddressData addressData = service.generateGeoAddressForSearchQuery(baseStoreModel, StringUtils.EMPTY);
        assertThat(addressData).isNotNull();
        assertThat(addressData.getCountryCode()).isNull();
        assertThat(addressData.getCity()).isEmpty();
    }

    @Test
    public void testGenerateGeoAddressForSearchQueryWithEmptyDeliveryCountries()
    {
        given(baseStoreModel.getDeliveryCountries()).willReturn(Collections.EMPTY_LIST);
        final AddressData addressData = service.generateGeoAddressForSearchQuery(baseStoreModel, "2014");
        assertThat(addressData).isNotNull();
        assertThat(addressData.getCountryCode()).isNull();
        assertThat(addressData.getCity()).isEqualTo("2014");
    }


    @Test
    public void testGenerateGeoAddressForSearchQueryWithValidCountries()
    {
        final Collection<CountryModel> deliveryCountries = new ArrayList<>();
        final CountryModel country = mock(CountryModel.class);
        deliveryCountries.add(country);
        given(baseStoreModel.getDeliveryCountries()).willReturn(deliveryCountries);
        given(country.getName()).willReturn("Australia");
        final AddressData addressData = service.generateGeoAddressForSearchQuery(baseStoreModel, LOCATION_TEXT);
        assertThat(addressData.getCountryCode()).isNotNull();
        assertThat(addressData.getCountryCode()).isEqualTo("Australia");
        assertThat(addressData.getCity()).isEqualTo(LOCATION_TEXT);
    }


    @Test
    public void testDoSearchForValidMaxRadius()
    {
        final PageableData pageData = preparePageMetaData(FIRST_PAGE, 5);
        final GeoPoint geoPoint = prepareGeoPoint();
        final StoreFinderSearchPageData storeFinderSearchPageData = service.doSearch(baseStoreModel, LOCATION_TEXT,
                geoPoint, pageData, Double.valueOf(500.0));
        verify(targetPointOfServiceDao, Mockito.times(0)).getAllPOSByTypes(baseStoreModel);
        assertThat(storeFinderSearchPageData).isNotNull();
    }

    @Test
    public void testDoSearchForNullMaxRadius()
    {
        final PageableData pageData = preparePageMetaData(FIRST_PAGE, 5);
        final GeoPoint geoPoint = prepareGeoPoint();
        final StoreFinderSearchPageData storeFinderSearchPageData = service.doSearch(baseStoreModel, LOCATION_TEXT,
                geoPoint, pageData,
                null);
        verify(targetPointOfServiceDao).getAllPOSByTypes(baseStoreModel);
        assertThat(storeFinderSearchPageData).isNotNull();
    }

    @Test
    public void testDoSearchForValidPOS()
    {
        final PageableData pageData = preparePageMetaData(FIRST_PAGE, 5);
        final GeoPoint geoPoint = prepareGeoPoint();
        final List allPOS = new ArrayList<>();
        final TargetPointOfServiceModel targetPointOfService = new TargetPointOfServiceModel();
        targetPointOfService.setLatitude(Double.valueOf(-33.546));
        targetPointOfService.setLongitude(Double.valueOf(55.335));
        final AddressModel address = new AddressModel();
        final CountryModel country = new CountryModel();
        country.setIsocode(TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);
        address.setCountry(country);
        targetPointOfService.setAddress(address);
        allPOS.add(targetPointOfService);
        given(
                targetPointOfServiceDao.getAllPOSByTypes(any(BaseStoreModel.class)))
                .willReturn(allPOS);
        final StoreFinderSearchPageData storeFinderSearchPageData = service.doSearch(baseStoreModel, LOCATION_TEXT,
                geoPoint, pageData,
                null);
        verify(targetPointOfServiceDao).getAllPOSByTypes(any(BaseStoreModel.class));
        assertThat(storeFinderSearchPageData).isNotNull();
    }

    @Test
    public void testDoSearchByLocation() {
        final PageableData pageData = preparePageMetaData(FIRST_PAGE, 5);
        given(geoWebServiceWrapper.geocodeAddress(any(AddressData.class))).willReturn(
                new DefaultGPS(-20, -20));

        final StoreFinderSearchPageData storeFinderSearchPageData = service.doSearchByLocation(baseStoreModel,
                LOCATION_TEXT, pageData, getPointOfServiceList());
        assertThat(storeFinderSearchPageData.getResults()).hasSize(5);
    }

    @Test
    public void testDoSearchByLocationWithEmptyLocationText() {
        final PageableData pageData = preparePageMetaData(FIRST_PAGE, 5);
        given(geoWebServiceWrapper.geocodeAddress(any(AddressData.class))).willReturn(
                new DefaultGPS(-20, -20));

        final StoreFinderSearchPageData storeFinderSearchPageData = service.doSearchByLocation(baseStoreModel,
                StringUtils.EMPTY, pageData, getPointOfServiceList());
        assertThat(storeFinderSearchPageData.getResults()).hasSize(0);
    }

    @Test
    public void testDoSearchByLocationWithFailedToResolveLocation() {
        final PageableData pageData = preparePageMetaData(FIRST_PAGE, 5);
        given(geoWebServiceWrapper.geocodeAddress(any(AddressData.class))).willThrow(
                new GeoServiceWrapperException());

        final StoreFinderSearchPageData storeFinderSearchPageData = service.doSearchByLocation(baseStoreModel,
                StringUtils.EMPTY, pageData, getPointOfServiceList());
        assertThat(storeFinderSearchPageData.getResults()).hasSize(0);
    }

    @Test
    public void testDoSearchByLocationWithoutPOS() {
        final PageableData pageData = preparePageMetaData(FIRST_PAGE, 5);
        final TargetGPS targetGPS = new TargetGPS(-20, -20);
        targetGPS.setPostcode("3220");
        targetGPS.setLocality("North Geelong");
        targetGPS.setState("VIC");
        given(geoWebServiceWrapper.geocodeAddress(any(AddressData.class))).willReturn(
                targetGPS);

        final StoreFinderSearchPageData storeFinderSearchPageData = service.doSearchByLocation(baseStoreModel,
                LOCATION_TEXT, pageData);
        assertThat(storeFinderSearchPageData.getLocationText()).isEqualTo("North Geelong 3220, VIC");
    }

    @Test
    public void testDoSearchByLocationWithoutPOSFor0872() {
        final PageableData pageData = preparePageMetaData(FIRST_PAGE, 5);
        final TargetGPS targetGPS = new TargetGPS(-20, -20);
        targetGPS.setPostcode("0872");
        targetGPS.setLocality("");
        targetGPS.setState("NT");
        given(geoWebServiceWrapper.geocodeAddress(any(AddressData.class))).willReturn(
                targetGPS);

        final StoreFinderSearchPageData storeFinderSearchPageData = service.doSearchByLocation(baseStoreModel,
                LOCATION_TEXT, pageData);
        assertThat(storeFinderSearchPageData.getLocationText()).isEqualTo("0872, NT");
    }

    @Test
    public void testDoSearchByLocationWithoutPOSForXXX() {
        final PageableData pageData = preparePageMetaData(FIRST_PAGE, 5);
        final TargetGPS targetGPS = new TargetGPS(-20, -20);
        given(geoWebServiceWrapper.geocodeAddress(any(AddressData.class))).willReturn(
                targetGPS);

        final StoreFinderSearchPageData storeFinderSearchPageData = service.doSearchByLocation(baseStoreModel,
                "XXX", pageData);
        assertThat(storeFinderSearchPageData.getLocationText()).isEqualTo("XXX");
    }

    /**
     * This method verify list of stores returned which have the facilities to avail click and collect delivery modes.
     * Here in this scenario, result set will get fetched.
     */
    @Test
    public void testDoSearchNearestClickAndCollectLocationsWithStoreNumberInputParameter() {
        final PageableData pageableData = preparePageMetaData(FIRST_PAGE, 1);
        final GeoPoint geoPoint = prepareGeoPoint();
        given(targetPointOfServiceDao.getAllOpenCncPOS()).willReturn(getPointOfServiceList());
        final StoreFinderSearchPageData storeFinderSearchPageData = service
                .doSearchNearestClickAndCollectLocation(null, geoPoint, pageableData);
        assertThat(storeFinderSearchPageData.getResults()).isNotEmpty();
        assertThat(storeFinderSearchPageData.getResults()).hasSize(1);
    }

    /**
     * This method verify list of stores returned which have the facilities to avail click and collect delivery modes.
     * Here in this scenario, no results get fetched.
     */
    @Test
    public void testDoSearchNearestClickAndCollectLocationsWithStoreNumberInputParameterFetchNoResults() {
        final PageableData pageableData = preparePageMetaData(FIRST_PAGE, 1);
        final GeoPoint geoPoint = prepareGeoPoint();
        given(targetPointOfServiceDao.getAllOpenCncPOS()).willReturn(null);
        final StoreFinderSearchPageData storeFinderSearchPageData = service
                .doSearchNearestClickAndCollectLocation(null, geoPoint, pageableData);
        assertThat(storeFinderSearchPageData.getResults()).isEmpty();
    }

    /**
     * Method to verify number Point of service data returned for click and collect delivery location. Here we can only
     * expect one store details.
     */
    @Test
    public void testDoSearchClickAndCollectStoresByLocationWithLocationInputParameter() {
        final PageableData pageData = preparePageMetaData(FIRST_PAGE, 1);
        given(geoWebServiceWrapper.geocodeAddress(any(AddressData.class))).willReturn(
                new TargetGPS(-20, -20));
        given(targetPointOfServiceDao.getAllOpenCncPOS()).willReturn(getPointOfServiceList());
        final StoreFinderSearchPageData storeFinderSearchPageData = service.doSearchClickAndCollectStoresByLocation(
                baseStoreModel,
                LOCATION_TEXT, pageData);
        assertThat(storeFinderSearchPageData.getResults()).isNotEmpty();
        assertThat(storeFinderSearchPageData.getResults()).hasSize(1);
    }

    /**
     * Method to verify number Point of service data returned for click and collect delivery location. Here no store
     * details will fetched.
     */
    @Test
    public void testDoSearchClickAndCollectStoresByLocationWithLocationParameterFetchNoResults() {
        final PageableData pageData = preparePageMetaData(FIRST_PAGE, 0);
        given(geoWebServiceWrapper.geocodeAddress(any(AddressData.class))).willReturn(
                new TargetGPS(-20, -20));
        final StoreFinderSearchPageData storeFinderSearchPageData = service.doSearchClickAndCollectStoresByLocation(
                baseStoreModel,
                LOCATION_TEXT, pageData);
        assertThat(storeFinderSearchPageData.getResults()).isEmpty();
    }

    /**
     * @return geoPoint
     */
    private GeoPoint prepareGeoPoint() {
        final GeoPoint geoPoint = new GeoPoint();
        geoPoint.setLatitude(-33.546);
        geoPoint.setLongitude(55.335);
        return geoPoint;
    }

    private PageableData preparePageMetaData(final int start, final int size)
    {
        final PageableData pageData = new PageableData();
        pageData.setCurrentPage(start);
        pageData.setPageSize(size);
        return pageData;
    }

    private List getPointOfServiceList() {
        final List targetPointOfServiceList = new ArrayList();
        for (int i = 0; i < 10; i++) {
            final TargetPointOfServiceModel model = new TargetPointOfServiceModel();
            model.setLatitude(Double.valueOf(i * 10));
            model.setLongitude(Double.valueOf(i * 10));
            targetPointOfServiceList.add(model);
        }
        return targetPointOfServiceList;
    }


}

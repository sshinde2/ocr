package au.com.target.tgtcore.storelocator;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.storelocator.GPS;
import de.hybris.platform.storelocator.GeolocationResponseParser;
import de.hybris.platform.storelocator.data.AddressData;
import de.hybris.platform.storelocator.data.MapLocationData;
import de.hybris.platform.storelocator.data.RouteData;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.storelocator.data.TargetMapLocationData;
import au.com.target.tgtcore.storelocator.impl.TargetGPS;
import au.com.target.tgtcore.storelocator.route.TargetGeolocationDirectionsUrlBuilder;


/**
 * Test suite for {@link TargetGoogleMapTools}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetGoogleMapToolsTest {

    private static final String BASE_URL = "http://maps.googleapis.com/maps/api/geocode";

    @Mock
    private TargetGeolocationDirectionsUrlBuilder directionsUrlBuilder;
    @Mock
    private GeolocationResponseParser<MapLocationData> addressLocationParser;
    @Mock
    private GeolocationResponseParser<RouteData> routeDataParser;

    @Mock
    private MapLocationData mapLocation;

    @Mock
    private TargetMapLocationData targetMapLocation;
    @Mock
    private AddressData address;

    @Spy
    @InjectMocks
    private final TargetGoogleMapTools tools = new TargetGoogleMapTools();

    private final String postcode = "3030";
    private final double latitude = -38.758366;
    private final double longitude = 143.662719;
    private final String state = "VIC";
    private final String locality = "Point Cook";

    /**
     * Initializes test case before run.
     */
    @Before
    public void setUp() {
        tools.setBaseUrl(BASE_URL);
        tools.setDirectionBaseUrl("https://maps.google.com");
        when(directionsUrlBuilder.getAddressWebServiceUrl(BASE_URL, address, null)).thenReturn(BASE_URL);
    }

    /**
     * Verifies that {@link TargetGoogleMapTools#geocodeAddress(AddressData)} will make a call to Google API and return
     * latitude and longitude.
     */
    @Test
    public void testGeocodeAddress() {
        when(mapLocation.getLatitude()).thenReturn(String.valueOf(latitude));
        when(mapLocation.getLongitude()).thenReturn(String.valueOf(longitude));

        doReturn(mapLocation).when(tools).getResponse(BASE_URL, addressLocationParser);

        final GPS gps = tools.geocodeAddress(address);
        assertEquals(latitude, gps.getDecimalLatitude(), .000001d);
        assertEquals(longitude, gps.getDecimalLongitude(), .000001d);
    }



    /**
     * Verifies that {@link TargetGoogleMapTools#geocodeAddress(AddressData)} will make a call to Google API and return
     * latitude and longitude.
     */
    @Test
    public void testGeocodeAddressWithLocationText() {
        when(targetMapLocation.getLatitude()).thenReturn(String.valueOf(latitude));
        when(targetMapLocation.getLongitude()).thenReturn(String.valueOf(longitude));
        when(targetMapLocation.getPostcode()).thenReturn(postcode);
        when(targetMapLocation.getLocality()).thenReturn(locality);
        when(targetMapLocation.getState()).thenReturn(state);

        doReturn(targetMapLocation).when(tools).getResponse(BASE_URL, addressLocationParser);

        final TargetGPS gps = (TargetGPS)tools.geocodeAddress(address);
        assertEquals(latitude, gps.getDecimalLatitude(), .000001d);
        assertEquals(longitude, gps.getDecimalLongitude(), .000001d);
        assertEquals(postcode, gps.getPostcode());
        assertEquals(locality, gps.getLocality());
        assertEquals(state, gps.getState());
    }

    @Test
    public void testDirectionUrl() {
        when(directionsUrlBuilder.getDirectionUrl("https://maps.google.com", "current location", "destination"))
                .thenReturn("url");
        Assertions.assertThat(tools.getDirectionUrl("current location", "destination")).isEqualTo("url");
    }
}

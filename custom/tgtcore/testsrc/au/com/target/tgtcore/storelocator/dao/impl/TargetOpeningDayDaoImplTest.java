package au.com.target.tgtcore.storelocator.dao.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.MapAssert.entry;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.model.TargetOpeningDayModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetOpeningDayDaoImplTest {
    @Mock
    private FlexibleSearchService mockFlexibleSearchService;

    @InjectMocks
    private final TargetOpeningDayDaoImpl targetOpeningDayDaoImpl = new TargetOpeningDayDaoImpl();

    @Test
    public void testFindTargetOpeningDayByScheduleAndId() throws Exception {
        final OpeningScheduleModel mockOpeningSchedule = mock(OpeningScheduleModel.class);
        final String id = "5090-1212312312313";

        final TargetOpeningDayModel mockTargetOpeningDay = mock(TargetOpeningDayModel.class);

        final List<TargetOpeningDayModel> targetOpeningDays = new ArrayList<>();
        targetOpeningDays.add(mockTargetOpeningDay);

        final SearchResult mockSearchResult = mock(SearchResult.class);
        given(mockSearchResult.getResult()).willReturn(targetOpeningDays);

        final ArgumentCaptor<FlexibleSearchQuery> searchQueryCaptor = ArgumentCaptor
                .forClass(FlexibleSearchQuery.class);
        given(mockFlexibleSearchService.search(searchQueryCaptor.capture())).willReturn(mockSearchResult);

        final TargetOpeningDayModel result = targetOpeningDayDaoImpl.findTargetOpeningDayByScheduleAndId(
                mockOpeningSchedule, id);

        assertThat(result).isEqualTo(mockTargetOpeningDay);

        final FlexibleSearchQuery capturedQuery = searchQueryCaptor.getValue();
        final Map<String, Object> queryParameters = capturedQuery.getQueryParameters();

        assertThat(queryParameters).hasSize(2).includes(entry(TargetOpeningDayModel.TARGETOPENINGDAYID, id))
                .includes(entry(TargetOpeningDayModel.OPENINGSCHEDULE, mockOpeningSchedule));
    }

    @Test
    public void testFindTargetOpeningDayByScheduleAndIdWithTooManyResults() throws Exception {
        final OpeningScheduleModel mockOpeningSchedule = mock(OpeningScheduleModel.class);
        final String id = "5090-1212312312313";

        final TargetOpeningDayModel mockTargetOpeningDay = mock(TargetOpeningDayModel.class);
        final TargetOpeningDayModel mockTargetOpeningDay2 = mock(TargetOpeningDayModel.class);

        final List<TargetOpeningDayModel> targetOpeningDays = new ArrayList<>();
        targetOpeningDays.add(mockTargetOpeningDay);
        targetOpeningDays.add(mockTargetOpeningDay2);

        final SearchResult mockSearchResult = mock(SearchResult.class);
        given(mockSearchResult.getResult()).willReturn(targetOpeningDays);

        final ArgumentCaptor<FlexibleSearchQuery> searchQueryCaptor = ArgumentCaptor
                .forClass(FlexibleSearchQuery.class);
        given(mockFlexibleSearchService.search(searchQueryCaptor.capture())).willReturn(mockSearchResult);

        TargetOpeningDayModel result = null;
        try {
            result = targetOpeningDayDaoImpl.findTargetOpeningDayByScheduleAndId(
                    mockOpeningSchedule, id);
            Assert.fail("Expected findTargetOpeningDayByScheduleAndId to throw TargetAmbiguousIdentifierException");
        }
        catch (final TargetAmbiguousIdentifierException ex) {
            assertThat(result).isNull();

            final FlexibleSearchQuery capturedQuery = searchQueryCaptor.getValue();
            final Map<String, Object> queryParameters = capturedQuery.getQueryParameters();

            assertThat(queryParameters).hasSize(2).includes(entry(TargetOpeningDayModel.TARGETOPENINGDAYID, id))
                    .includes(entry(TargetOpeningDayModel.OPENINGSCHEDULE, mockOpeningSchedule));
        }
    }

    @Test
    public void testFindTargetOpeningDayByScheduleAndIdWithNoResults() throws Exception {
        final OpeningScheduleModel mockOpeningSchedule = mock(OpeningScheduleModel.class);
        final String id = "5090-1212312312313";

        final List<TargetOpeningDayModel> targetOpeningDays = new ArrayList<>();

        final SearchResult mockSearchResult = mock(SearchResult.class);
        given(mockSearchResult.getResult()).willReturn(targetOpeningDays);

        final ArgumentCaptor<FlexibleSearchQuery> searchQueryCaptor = ArgumentCaptor
                .forClass(FlexibleSearchQuery.class);
        given(mockFlexibleSearchService.search(searchQueryCaptor.capture())).willReturn(mockSearchResult);

        final TargetOpeningDayModel result = targetOpeningDayDaoImpl.findTargetOpeningDayByScheduleAndId(
                mockOpeningSchedule, id);

        assertThat(result).isNull();

        final FlexibleSearchQuery capturedQuery = searchQueryCaptor.getValue();
        final Map<String, Object> queryParameters = capturedQuery.getQueryParameters();

        assertThat(queryParameters).hasSize(2).includes(entry(TargetOpeningDayModel.TARGETOPENINGDAYID, id))
                .includes(entry(TargetOpeningDayModel.OPENINGSCHEDULE, mockOpeningSchedule));
    }
}

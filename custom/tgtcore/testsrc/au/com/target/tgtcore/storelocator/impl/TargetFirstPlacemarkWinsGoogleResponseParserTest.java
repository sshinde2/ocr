/**
 * 
 */
package au.com.target.tgtcore.storelocator.impl;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.storelocator.exception.GeoDocumentParsingException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.client.ClientHttpResponse;

import au.com.target.tgtcore.storelocator.data.TargetMapLocationData;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFirstPlacemarkWinsGoogleResponseParserTest {
    private final TargetFirstPlacemarkWinsGoogleResponseParser parser = new TargetFirstPlacemarkWinsGoogleResponseParser();
    @Mock
    private ClientHttpResponse response;

    @Test(expected = GeoDocumentParsingException.class)
    public void testParseInvalidXmlResponse() throws IOException {
        final InputStream stream = new ByteArrayInputStream("text".getBytes());
        given(response.getBody()).willReturn(stream);
        parser.extractData(response);
    }

    @Test
    public void testParseInvalidResponse() throws IOException {
        final InputStream stream = new ByteArrayInputStream("<test></test>".getBytes());
        given(response.getBody()).willReturn(stream);
        final TargetMapLocationData mapLocationData = (TargetMapLocationData)parser.extractData(response);
        Assertions.assertThat(mapLocationData.getLatitude()).isEmpty();
        Assertions.assertThat(mapLocationData.getLongitude()).isEmpty();
        Assertions.assertThat(mapLocationData.getLocality()).isEmpty();
        Assertions.assertThat(mapLocationData.getPostcode()).isEmpty();
        Assertions.assertThat(mapLocationData.getState()).isEmpty();
    }
}

/**
 *
 */
package au.com.target.tgtcore.storelocator.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.model.TargetOpeningDayModel;
import au.com.target.tgtcore.storelocator.TargetOpeningDayService;
import au.com.target.tgtcore.storelocator.dao.TargetOpeningDayDao;


/**
 * @author Pradeep
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetOpeningDayServiceImplTest {

    private static final String TARGET_OPENING_DAY_ID = "7004-23-05-1970";

    @InjectMocks
    private final TargetOpeningDayService targetOpeningDayService = new TargetOpeningDayServiceImpl();

    @Mock
    private TargetOpeningDayDao mockTargetOpeningDayDao;

    @Test(expected = IllegalArgumentException.class)
    public void testgetTargetOpeningDayIdwithEmptyStoreNo() {
        targetOpeningDayService.getTargetOpeningDayId(StringUtils.EMPTY, new Date());
    }

    @Test(expected = NullPointerException.class)
    public void testgetTargetOpeningDayIdwithNullDate() {
        targetOpeningDayService.getTargetOpeningDayId("7004", null);
    }

    @Test
    public void testgetTargetOpeningDayId() {
        final Calendar cal = Calendar.getInstance();
        cal.set(1970, Calendar.MAY, 23);
        final String targetOpeningDayID = targetOpeningDayService.getTargetOpeningDayId("7004", cal.getTime());
        Assert.assertTrue(StringUtils.isNotEmpty(targetOpeningDayID));
        Assert.assertEquals(targetOpeningDayID, TARGET_OPENING_DAY_ID);
    }

    @Test(expected = NullPointerException.class)
    public void testGetTargetOpeningDayWithNullSchedule() throws TargetAmbiguousIdentifierException {
        targetOpeningDayService.getTargetOpeningDay(null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testGetTargetOpeningDayWithNullId() throws TargetAmbiguousIdentifierException {
        final OpeningScheduleModel mockOpeningSchedule = mock(OpeningScheduleModel.class);

        targetOpeningDayService.getTargetOpeningDay(mockOpeningSchedule, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetTargetOpeningDayWithBlankId() throws TargetAmbiguousIdentifierException {
        final OpeningScheduleModel mockOpeningSchedule = mock(OpeningScheduleModel.class);

        targetOpeningDayService.getTargetOpeningDay(mockOpeningSchedule, "");
    }

    @Test
    public void testGetTargetOpeningDay() throws TargetAmbiguousIdentifierException {
        final OpeningScheduleModel mockOpeningSchedule = mock(OpeningScheduleModel.class);
        final String id = "5891-12341243333";

        final TargetOpeningDayModel mockTargetOpeningDay = mock(TargetOpeningDayModel.class);
        given(mockTargetOpeningDayDao.findTargetOpeningDayByScheduleAndId(mockOpeningSchedule, id)).willReturn(
                mockTargetOpeningDay);

        final TargetOpeningDayModel result = targetOpeningDayService.getTargetOpeningDay(mockOpeningSchedule, id);
        assertThat(result).isEqualTo(mockTargetOpeningDay);
    }

    @Test(expected = TargetAmbiguousIdentifierException.class)
    public void testGetTargetOpeningDayWithTooManyResults() throws TargetAmbiguousIdentifierException {
        final OpeningScheduleModel mockOpeningSchedule = mock(OpeningScheduleModel.class);
        final String id = "5891-12341243333";

        given(mockTargetOpeningDayDao.findTargetOpeningDayByScheduleAndId(mockOpeningSchedule, id)).willThrow(
                new TargetAmbiguousIdentifierException("Too many results!"));

        targetOpeningDayService.getTargetOpeningDay(mockOpeningSchedule, id);
    }
}

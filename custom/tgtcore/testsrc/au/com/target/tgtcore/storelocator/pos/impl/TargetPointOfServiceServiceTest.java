/**
 * 
 */
package au.com.target.tgtcore.storelocator.pos.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Table;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.dao.TargetPointOfServiceDao;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.model.NTLFulfilmentCapabilitiesModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPointOfServiceServiceTest {

    @Mock
    private TargetPointOfServiceDao posDao;

    @Mock
    private CartModel cartModel;

    @Mock
    private AbstractTargetVariantProductModel productModel;

    @Mock
    private AbstractTargetVariantProductModel productModelDigital;

    @Mock
    private ProductTypeModel productTypeModel;

    @Mock
    private CartEntryModel targetCartEntryModel;

    @Mock
    private CartEntryModel targetCartEntryModelDigital;

    @Mock
    private TargetPointOfServiceModel targetPointOfServiceModel;

    @Mock
    private FlexibleSearchService flexibleSearchService;

    @Mock
    private AddressModel addressModel;

    @Mock
    private RegionModel region;

    @Mock
    private NTLFulfilmentCapabilitiesModel ntlModel;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Mock
    private SessionService sessionService;

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @InjectMocks
    private final TargetPointOfServiceServiceImpl posService = new TargetPointOfServiceServiceImpl();

    private final List<AbstractOrderEntryModel> entries = new ArrayList<>();

    @Before
    public void setup() {
        posService.setTargetPointOfServiceDao(posDao);
        entries.add(targetCartEntryModel);
        given(cartModel.getEntries()).willReturn(entries);
        given(targetCartEntryModel.getProduct()).willReturn(productModel);
        given(productModel.getProductType()).willReturn(productTypeModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetPOSByStoreNumberNull() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        posService.getPOSByStoreNumber(null);
    }

    @Test
    public void testGetPOSByStoreNumber() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final TargetPointOfServiceModel model = new TargetPointOfServiceModel();
        model.setStoreNumber(Integer.valueOf(123));
        given(posDao.getPOSByStoreNumber(Integer.valueOf(Mockito.anyInt()))).willReturn(model);

        final TargetPointOfServiceModel returnModel = posService.getPOSByStoreNumber(Integer.valueOf(123));

        assertThat(returnModel).isNotNull();
        assertThat(returnModel.getStoreNumber()).isEqualTo(Integer.valueOf(123));
    }


    @Test
    public void testBuildStateAndStoreMapNewState() {
        final List<TargetPointOfServiceModel> pos = new ArrayList<>();
        pos.add(targetPointOfServiceModel);
        given(targetPointOfServiceModel.getAddress()).willReturn(addressModel);
        given(addressModel.getDistrict()).willReturn("Vic");

        final Map<String, Set<TargetPointOfServiceModel>> resultMap = posService.buildStateAndStoreMap(pos);

        assertThat(resultMap.size()).isEqualTo(1);

        final Set<Entry<String, Set<TargetPointOfServiceModel>>> entrySet = resultMap.entrySet();
        assertThat(entrySet.iterator().next().getKey()).isEqualTo("Vic");
    }

    @Test
    public void testBuildStateAndStoreMapExistingState() {
        final List<TargetPointOfServiceModel> pos = new ArrayList<>();
        final TargetPointOfServiceModel mock2 = mock(TargetPointOfServiceModel.class);
        pos.add(targetPointOfServiceModel);
        pos.add(mock2);
        given(targetPointOfServiceModel.getAddress()).willReturn(addressModel);
        given(targetPointOfServiceModel.getStoreNumber()).willReturn(Integer.valueOf(15));
        given(mock2.getAddress()).willReturn(addressModel);
        given(mock2.getStoreNumber()).willReturn(Integer.valueOf(18));
        given(addressModel.getDistrict()).willReturn("Vic");

        final Map<String, Set<TargetPointOfServiceModel>> resultMap = posService.buildStateAndStoreMap(pos);

        assertThat(resultMap.size()).isEqualTo(1);

        final Set<Entry<String, Set<TargetPointOfServiceModel>>> entrySet = resultMap.entrySet();
        assertThat(entrySet.iterator().next().getKey()).isEqualTo("Vic");

        assertThat(entrySet.iterator().next().getValue().size()).isEqualTo(2);
    }

    @Test
    public void testBuildStateAndStoreMapSort() {
        final List<TargetPointOfServiceModel> pos = new ArrayList<>();
        final TargetPointOfServiceModel pos1 = mock(TargetPointOfServiceModel.class);
        final TargetPointOfServiceModel pos2 = mock(TargetPointOfServiceModel.class);
        final TargetPointOfServiceModel pos3 = mock(TargetPointOfServiceModel.class);
        pos.add(pos1);
        pos.add(pos2);
        pos.add(pos3);

        final AddressModel addr1 = mock(AddressModel.class);
        final AddressModel addr2 = mock(AddressModel.class);
        final AddressModel addr3 = mock(AddressModel.class);
        given(pos1.getAddress()).willReturn(addr1);
        given(pos2.getAddress()).willReturn(addr2);
        given(pos3.getAddress()).willReturn(addr3);

        given(pos1.getName()).willReturn("c");
        given(pos2.getName()).willReturn("b");
        given(pos3.getName()).willReturn("a");

        // All stores are in vic
        given(addr1.getDistrict()).willReturn("Vic");
        given(addr2.getDistrict()).willReturn("Vic");
        given(addr3.getDistrict()).willReturn("Vic");

        // Various suburbs
        given(addr1.getTown()).willReturn("Albury");
        given(addr2.getTown()).willReturn("Geelong");
        given(addr3.getTown()).willReturn("Albury");

        // Various store numbers
        given(pos1.getStoreNumber()).willReturn(Integer.valueOf(21));
        given(pos2.getStoreNumber()).willReturn(Integer.valueOf(64));
        given(pos3.getStoreNumber()).willReturn(Integer.valueOf(100));

        final Map<String, Set<TargetPointOfServiceModel>> resultMap = posService.buildStateAndStoreMap(pos);

        assertThat(resultMap.size()).isEqualTo(1);

        final Set<Entry<String, Set<TargetPointOfServiceModel>>> entrySet = resultMap.entrySet();
        assertThat(entrySet.iterator().next().getKey()).isEqualTo("Vic");
        assertThat(entrySet.iterator().next().getValue().size()).isEqualTo(3);

        final Iterator<TargetPointOfServiceModel> itPos = entrySet.iterator().next().getValue().iterator();
        String lastName = null;
        while (itPos.hasNext()) {
            final String newName = itPos.next().getName();
            if (lastName != null) {
                assertThat(lastName.compareTo(newName)).as("Stores not in asc order of name").isLessThanOrEqualTo(0);
            }
            lastName = newName;
        }
    }

    @Test
    public void testStoresForCart() {
        final List<TargetPointOfServiceModel> pos = new ArrayList<>();
        pos.add(targetPointOfServiceModel);
        final Set<ProductTypeModel> pr = new HashSet<>();
        pr.add(productTypeModel);
        given(targetPointOfServiceModel.getProductTypes()).willReturn(pr);
        given(flexibleSearchService.getModelsByExample(Mockito.any(TargetPointOfServiceModel.class)))
                .willReturn(pos);
        given(Boolean.valueOf(targetDeliveryModeHelper.doesProductHavePhysicalDeliveryMode(productModel)))
                .willReturn(
                        Boolean.TRUE);

        posService.getStoresForCart(cartModel);

        assertThat(pos.size()).isEqualTo(1);
    }

    @Test
    public void testRemoveStoresForCart() {
        final List<TargetPointOfServiceModel> pos = new ArrayList<>();
        pos.add(targetPointOfServiceModel);
        given(flexibleSearchService.getModelsByExample(Mockito.any(TargetPointOfServiceModel.class)))
                .willReturn(pos);
        given(Boolean.valueOf(targetDeliveryModeHelper.doesProductHavePhysicalDeliveryMode(productModel)))
                .willReturn(
                        Boolean.TRUE);
        final List<TargetPointOfServiceModel> result = posService.getStoresForCart(cartModel);

        // The original list won't be changed
        assertThat(pos.size()).isEqualTo(1);
        // The result list should contain nothing
        assertThat(result.size()).isEqualTo(0);
    }

    @Test
    public void testGetAllStateAndStores() {
        final List<TargetPointOfServiceModel> pos = new ArrayList<>();
        pos.add(targetPointOfServiceModel);
        final Set<ProductTypeModel> productTypes = new HashSet<>();
        productTypes.add(productTypeModel);
        given(targetPointOfServiceModel.getProductTypes()).willReturn(productTypes);
        given(flexibleSearchService.getModelsByExample(Mockito.any(TargetPointOfServiceModel.class)))
                .willReturn(pos);
        final Map<String, Set<TargetPointOfServiceModel>> pointOfServiceMap = new TreeMap<String, Set<TargetPointOfServiceModel>>();
        final TargetPointOfServiceServiceImpl posServiceSpy = spy(posService);
        given(posServiceSpy.buildStateAndStoreMap(Mockito.anyListOf(TargetPointOfServiceModel.class)))
                .willReturn(pointOfServiceMap);
        final Map<String, Set<TargetPointOfServiceModel>> resultMap = posServiceSpy.getAllStateAndStores();

        assertThat(pointOfServiceMap).isEqualTo(resultMap);
    }


    @Test
    public void testGetAllStateAndStoresWithDigitalProduct() {
        entries.add(targetCartEntryModelDigital);
        given(targetCartEntryModelDigital.getProduct()).willReturn(productModelDigital);
        final List<TargetPointOfServiceModel> pos = new ArrayList<>();
        pos.add(targetPointOfServiceModel);
        final Set<ProductTypeModel> pr = new HashSet<>();
        pr.add(productTypeModel);
        given(targetPointOfServiceModel.getProductTypes()).willReturn(pr);
        given(flexibleSearchService.getModelsByExample(Mockito.any(TargetPointOfServiceModel.class)))
                .willReturn(pos);
        given(Boolean.valueOf(targetDeliveryModeHelper.doesProductHavePhysicalDeliveryMode(productModel)))
                .willReturn(
                        Boolean.TRUE);
        given(
                Boolean.valueOf(targetDeliveryModeHelper.doesProductHavePhysicalDeliveryMode(productModelDigital)))
                        .willReturn(
                                Boolean.FALSE);


        posService.getStoresForCart(cartModel);

        assertThat(pos.size()).isEqualTo(1);
    }

    @Test
    public void testGetAllOpenStores() {
        posService.getAllOpenStores();
        verify(posDao).getAllOpenPOS();
    }

    @Test
    public void testIsSupportAllProductTypesInCartWithEmptyProductTypesInPointOfService() {
        given(Boolean.valueOf(targetDeliveryModeHelper.doesProductHavePhysicalDeliveryMode(productModel)))
                .willReturn(
                        Boolean.TRUE);
        final Set<ProductTypeModel> pr = new HashSet<>();
        given(targetPointOfServiceModel.getProductTypes()).willReturn(pr);
        final boolean result = posService.isSupportAllProductTypesInCart(targetPointOfServiceModel, cartModel);
        assertThat(result).isFalse();
    }

    @Test
    public void testIsSupportAllProductTypesInCart() {
        given(Boolean.valueOf(targetDeliveryModeHelper.doesProductHavePhysicalDeliveryMode(productModel)))
                .willReturn(
                        Boolean.TRUE);
        final Set<ProductTypeModel> pr = new HashSet<>();
        pr.add(productTypeModel);
        given(targetPointOfServiceModel.getProductTypes()).willReturn(pr);
        final boolean result = posService.isSupportAllProductTypesInCart(targetPointOfServiceModel, cartModel);
        assertThat(result).isTrue();
    }

    @Test
    public void testIsSupportAllProductTypesInCartWithNotMatching() {
        final CartEntryModel cartEntryModel1 = mock(CartEntryModel.class);
        final AbstractTargetVariantProductModel productModel1 = mock(AbstractTargetVariantProductModel.class);
        given(cartEntryModel1.getProduct()).willReturn(productModel1);
        final ProductTypeModel productTypeModel1 = mock(ProductTypeModel.class);
        given(productModel1.getProductType()).willReturn(productTypeModel1);
        entries.add(cartEntryModel1);
        given(Boolean.valueOf(targetDeliveryModeHelper.doesProductHavePhysicalDeliveryMode(productModel)))
                .willReturn(
                        Boolean.TRUE);
        given(Boolean.valueOf(targetDeliveryModeHelper.doesProductHavePhysicalDeliveryMode(productModel1)))
                .willReturn(
                        Boolean.TRUE);
        final Set<ProductTypeModel> pr = new HashSet<>();
        pr.add(productTypeModel);
        given(targetPointOfServiceModel.getProductTypes()).willReturn(pr);
        final boolean result = posService.isSupportAllProductTypesInCart(targetPointOfServiceModel, cartModel);
        assertThat(result).isFalse();
    }

    @Test
    public void testGetFulfilmentPendingQuantity() {
        given(sessionService.executeInLocalView(any(SessionExecutionBody.class))).willReturn(Integer.valueOf(5));
        final int pendingQuantity = posService.getFulfilmentPendingQuantity(Integer.valueOf(5432), productModel);
        assertThat(pendingQuantity).isEqualTo(5);
    }

    @Test
    public void testValidateStoreNumberNumberNotExists()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final TargetUnknownIdentifierException exception = mock(TargetUnknownIdentifierException.class);
        given(posDao.getPOSByStoreNumber(Integer.valueOf(12323121))).willThrow(exception);
        assertThat(posService.validateStoreNumber(Integer.valueOf(12323121))).isFalse();
    }

    @Test
    public void testValidateStoreNumberNumberIsDuplicate()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final TargetAmbiguousIdentifierException exception = mock(TargetAmbiguousIdentifierException.class);
        given(posDao.getPOSByStoreNumber(Integer.valueOf(12323121))).willThrow(exception);
        assertThat(posService.validateStoreNumber(Integer.valueOf(12323121))).isFalse();
    }

    @Test
    public void testValidateStoreNumberNumberIsValid()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(posDao.getPOSByStoreNumber(Integer.valueOf(12323121))).willReturn(targetPointOfServiceModel);
        assertThat(posService.validateStoreNumber(Integer.valueOf(12323121))).isTrue();
    }

    @Test
    public void testGetWarehouseByStoreNumber() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetPointOfServiceModel model = new TargetPointOfServiceModel();
        model.setStoreNumber(Integer.valueOf(123));
        given(posDao.getPOSByStoreNumber(Integer.valueOf(Mockito.anyInt()))).willReturn(model);
        final WarehouseModel warehouse = mock(WarehouseModel.class);
        given(targetWarehouseService.getWarehouseForPointOfService(model)).willReturn(warehouse);

        final WarehouseModel result = posService.getWarehouseByStoreNumber(Integer.valueOf(123));
        assertThat(result).isEqualTo(warehouse);
    }

    @Test
    public void testGetTposOfDefaultWarehouse() {
        final WarehouseModel warehouse = mock(WarehouseModel.class);
        given(targetWarehouseService.getDefaultOnlineWarehouse()).willReturn(warehouse);
        final TargetPointOfServiceModel tpos = mock(TargetPointOfServiceModel.class);
        given(sessionService.executeInLocalView(any(SessionExecutionBody.class))).willReturn(tpos);
        final TargetPointOfServiceModel result = posService.getTposOfDefaultWarehouse();
        assertThat(result).isEqualTo(tpos);
    }

    @Test
    public void testGetFulfilmentPendingQuantityByProductCodeForStoresWithNull() {
        final List<String> storeNumbers = new ArrayList<>();
        final ProductModel prod1 = mock(ProductModel.class);
        final List<ProductModel> products = Arrays.asList(prod1);
        given(posDao.getFulfilmentPendingQuantityByProductCodeForStores(storeNumbers, products)).willReturn(null);
        final Table<Integer, String, Long> sohTable = posService
                .getFulfilmentPendingQuantityByProductCodeForStores(storeNumbers, products);
        assertThat(sohTable).isNotNull();
        assertThat(sohTable.isEmpty()).isTrue();
    }

    @Test
    public void testGetFulfilmentPendingQuantityByProductCodeForStores() {
        final List<String> storeNumbers = new ArrayList<>();
        final ProductModel prod1 = mock(ProductModel.class);
        given(prod1.getCode()).willReturn("P1000");
        final ProductModel prod2 = mock(ProductModel.class);
        given(prod2.getCode()).willReturn("P2000");
        final List<ProductModel> products = Arrays.asList(prod1, prod2);

        final List<List> searchResult = new ArrayList<>();
        final List<Object> data1 = new ArrayList<>();
        final List<Object> data2 = new ArrayList<>();
        final Integer storeNumber1 = Integer.valueOf(7032);
        final Long soh1 = Long.valueOf(10);
        final Integer storeNumber2 = Integer.valueOf(7126);
        final Long soh2 = Long.valueOf(15);
        data1.add(storeNumber1);
        data1.add(prod1);
        data1.add(soh1);
        data2.add(storeNumber2);
        data2.add(prod2);
        data2.add(soh2);

        searchResult.add(data1);
        searchResult.add(data2);

        given(posDao.getFulfilmentPendingQuantityByProductCodeForStores(storeNumbers, products))
                .willReturn(searchResult);
        final Table<Integer, String, Long> sohTable = posService
                .getFulfilmentPendingQuantityByProductCodeForStores(storeNumbers, products);
        assertThat(sohTable).isNotNull();
        assertThat(sohTable.get(storeNumber1, prod1.getCode())).isEqualTo(soh1);
        assertThat(sohTable.get(storeNumber2, prod2.getCode())).isEqualTo(soh2);
    }
}
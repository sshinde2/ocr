package au.com.target.tgtcore.storelocator.route.impl;

import static junit.framework.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import org.junit.Before;
import org.junit.Test;


/**
 * Test suite for {@link GoogleUrlSigner}.
 */
@UnitTest
public class GoogleUrlSignerTest {

    private static final String CLIENT_ID = "target-australia";
    private static final String SIGNATURE = "vNIXE0xscrmjlyV-12Nj_BvUPaw=";

    private final GoogleUrlSigner signer = new GoogleUrlSigner();


    /**
     * Initializes test cases before run.
     *
     * @throws NoSuchAlgorithmException
     *             if HmacSHA1 algorithm is not supported
     * @throws InvalidKeyException
     *             if provided signature is invalid
     */
    @Before
    public void setUp() throws InvalidKeyException, NoSuchAlgorithmException {
        signer.setClientId(CLIENT_ID);
        signer.setPrivateKey(SIGNATURE);
        signer.afterPropertiesSet();
    }

    /**
     * Verifies that signature is properly generated and appended to the URL as a result of
     * {@link GoogleUrlSigner#sign(String)} invocation.
     */
    @Test
    public void testSignatureAppended() {
        final String result = signer.sign("/maps/api/geocode/json?address=New+York&sensor=false");
        assertEquals("/maps/api/geocode/json?address=New+York&sensor=false"
                + "&client=target-australia&signature=e0pi2aooUigRTq_Arqr_aTCZy4w=", result);
    }

}

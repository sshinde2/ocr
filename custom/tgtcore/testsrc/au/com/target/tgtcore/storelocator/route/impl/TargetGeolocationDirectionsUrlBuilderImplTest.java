package au.com.target.tgtcore.storelocator.route.impl;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.storelocator.data.AddressData;
import de.hybris.platform.storelocator.impl.DefaultGPS;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import au.com.target.tgtcore.storelocator.route.TargetGeolocationDirectionsUrlBuilder;

import com.google.common.collect.ImmutableMap;


/**
 * Test suite for {@link TargetGeolocationDirectionsUrlBuilderImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetGeolocationDirectionsUrlBuilderImplTest {

    private static final String BASE_URL = "http://maps.googleapis.com/maps/api/geocode";
    private static final String SIGNER_SUFFIX = "&client=target&signature=cryptic_string";

    @Mock
    private GoogleUrlSigner googleUrlSigner;

    @InjectMocks
    private final TargetGeolocationDirectionsUrlBuilder urlBuilder = new TargetGeolocationDirectionsUrlBuilderImpl();


    /**
     * Initializes test cases before run.
     */
    @Before
    public void setUp() {
        when(googleUrlSigner.sign(anyString())).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(final InvocationOnMock invocation) {
                final String url = (String)invocation.getArguments()[0];
                return url + SIGNER_SUFFIX;
            }
        });
    }

    /**
     * Verifies that URL is properly composed for Google's "get directions" API using GPS coordinates.
     */
    @Test
    public void testWebServiceUrlForGetDirectionsAPIWithGPS() {
        final String url = urlBuilder.getWebServiceUrl(BASE_URL,
                new DefaultGPS(-38.16047628099622, 144.3548583984375),
                new DefaultGPS(-38.75836693561278, 143.6627197265625), ImmutableMap.of());
        assertEquals(composeExpectedUrl("/api/directions/xml?origin=-38.160476,144.354858"
                + "&destination=-38.758367,143.66272&sensor=true&mode=driving"), url);
    }

    /**
     * Verifies that URL is properly composed for Google's "get directions" API using addresses.
     */
    @Test
    public void testWebServiceUrlForGetDirectionsAPIWithAddress() {
        final String url = urlBuilder.getWebServiceUrl(BASE_URL,
                new AddressData("Main Street", "123", "zip", "City", "Country"),
                new AddressData("zip2"), ImmutableMap.of());
        assertEquals(composeExpectedUrl("/api/directions/xml?origin=123%20Main%20Street%20City%20zip%20Country"
                + "&destination=zip2&sensor=true&mode=driving"), url);
    }

    /**
     * Verifies that URL is properly composed for Google's "get position" API using single address.
     */
    @Test
    public void testAddressWebServiceUrl() {
        final String url = urlBuilder.getAddressWebServiceUrl(BASE_URL,
                new AddressData("The Store", "Street 2", "33", "zip", "New York", "US"), ImmutableMap.of());
        assertEquals(composeExpectedUrl("xml?address=33%20Street%202%20New%20York%20zip%20US&sensor=true"), url);
    }

    /**
     * Verifies that special characters in parameter values are encoded.
     */
    @Test
    public void testAddressWebServiceUrlWithSpecialChars() {
        final String url = urlBuilder.getAddressWebServiceUrl(BASE_URL,
                new AddressData("W&-=,:^.+"), ImmutableMap.of());
        assertEquals(composeExpectedUrl("xml?address=W%26-%3D,:%5E.%2B&sensor=true"), url);
    }

    @Test
    public void testDirectionUrlWithSpaces() {
        final String url = urlBuilder.getDirectionUrl("https://maps.google.com", "Current Location",
                "Target Point Cook Australia");
        assertEquals(url, "https://maps.google.com?saddr=Current+Location&daddr=Target+Point+Cook+Australia");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDirectionUrlWithNullSourceLocation() {
        urlBuilder.getDirectionUrl("https://maps.google.com", null,
                "Target Point Cook Australia");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDirectionUrlWithNullDestination() {
        urlBuilder.getDirectionUrl("https://maps.google.com", "Current Location",
                null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDirectionUrlWithNullBaseUrl() {
        urlBuilder.getDirectionUrl(null, "Current Location",
                "Target Point Cook Australia");
    }

    /**
     * Returns the absolute URL composed from static prefix and suffix, and {@code expectedPart}.
     *
     * @param expectedPart
     *            the expected part of URL
     * @return the absolute URL
     */
    private String composeExpectedUrl(final String expectedPart) {
        return BASE_URL + expectedPart + SIGNER_SUFFIX;
    }
}

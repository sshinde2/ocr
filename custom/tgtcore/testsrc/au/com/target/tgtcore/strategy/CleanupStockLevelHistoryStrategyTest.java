/**
 * 
 */
package au.com.target.tgtcore.strategy;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.stock.model.StockLevelHistoryEntryModel;

import org.junit.Assert;
import org.junit.Test;


/**
 * @author smishra1
 *
 */
@UnitTest
public class CleanupStockLevelHistoryStrategyTest {

    private final CleanupStockLevelHistoryStrategy strategy = new CleanupStockLevelHistoryStrategy();
    private final Integer batchSize = new Integer(10000);
    private final String expectedQuery = "SELECT TOP " + batchSize + " {PK} "
            + "FROM { " + StockLevelHistoryEntryModel._TYPECODE
            + "} WHERE {creationtime} < ?age";

    @Test(expected = IllegalArgumentException.class)
    public void testAgeNull() {
        strategy.setRecordAge(null);
        strategy.createFetchQuery(new CronJobModel());
    }

    @Test
    public void testQueryEqualsExpected() {
        strategy.setRecordAge(new Integer(15));
        strategy.setBatchSize(batchSize);
        Assert.assertEquals(expectedQuery, strategy.createFetchQuery(new CronJobModel()).getQuery());
    }
}

/**
 * 
 */
package au.com.target.tgtcore.ticket.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ticket.model.CsAgentGroupModel;
import de.hybris.platform.ticket.model.CsTicketModel;
import de.hybris.platform.ticket.service.TicketService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;


/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetTicketBusinessServiceTest {

    @Mock
    private SalesApplication salesApplication;

    @Mock
    private CsTicketModel ticketModel;

    @Mock
    private OrderModel order;

    @Mock
    private CsAgentGroupModel tradeMeAgent;

    @Mock
    private CsAgentGroupModel eBayAgent;

    @Mock
    private CsAgentGroupModel refundFailureAgents;

    @Mock
    private TicketService ticketService;

    @InjectMocks
    private final TargetTicketBusinessServiceImpl targetTicketService = new TargetTicketBusinessServiceImpl();
    private final List<CsAgentGroupModel> agentList = new ArrayList<>();

    @Before
    public void setup() {
        Mockito.when(ticketModel.getOrder()).thenReturn(order);
        Mockito.when(tradeMeAgent.getUid()).thenReturn(TgtCoreConstants.CsAgentGroup.TRADE_ME_CS_AGENT_GROUP);
        Mockito.when(eBayAgent.getUid()).thenReturn(TgtCoreConstants.CsAgentGroup.EBAY_CS_AGENT_GROUP);
        Mockito.when(refundFailureAgents.getUid()).thenReturn(
                TgtCoreConstants.CsAgentGroup.REFUND_FAILED_CS_AGENT_GROUP);
        agentList.add(tradeMeAgent);
        agentList.add(eBayAgent);
        agentList.add(refundFailureAgents);
        Mockito.when(ticketService.getAgentGroups()).thenReturn(agentList);
    }

    @Test
    public void testEbayTickets() {
        targetTicketService.assignCSAgentGroupToTicket(SalesApplication.EBAY, ticketModel, null);
        Mockito.verify(ticketModel).setAssignedGroup(eBayAgent);
        Mockito.verifyNoMoreInteractions(ticketModel);
    }

    @Test
    public void testTradeMeTickets() {
        targetTicketService.assignCSAgentGroupToTicket(SalesApplication.TRADEME, ticketModel, null);
        Mockito.verify(ticketModel).setAssignedGroup(tradeMeAgent);
        Mockito.verifyNoMoreInteractions(ticketModel);
    }

    //The group that is sent takes high priority irrespective of the saleschannel
    @Test
    public void testRefundFailedEbay() {
        targetTicketService.assignCSAgentGroupToTicket(SalesApplication.EBAY, ticketModel,
                TgtCoreConstants.CsAgentGroup.REFUND_FAILED_CS_AGENT_GROUP);
        Mockito.verify(ticketModel).setAssignedGroup(refundFailureAgents);
        Mockito.verifyNoMoreInteractions(ticketModel);
    }

    @Test
    public void testRefundFailedForWeb() {
        targetTicketService.assignCSAgentGroupToTicket(SalesApplication.WEB, ticketModel,
                TgtCoreConstants.CsAgentGroup.REFUND_FAILED_CS_AGENT_GROUP);
        Mockito.verify(ticketModel).setAssignedGroup(refundFailureAgents);
        Mockito.verifyNoMoreInteractions(ticketModel);
    }

    @Test
    public void testRefundFailedWithNoSalesApplication() {
        targetTicketService.assignCSAgentGroupToTicket(null, ticketModel,
                TgtCoreConstants.CsAgentGroup.REFUND_FAILED_CS_AGENT_GROUP);
        Mockito.verify(ticketModel).setAssignedGroup(refundFailureAgents);
        Mockito.verifyNoMoreInteractions(ticketModel);
    }

    @Test
    public void testRefundFailedWithNoSalesApplicationNoAgentGroup() {
        targetTicketService.assignCSAgentGroupToTicket(null, ticketModel, null);
        Mockito.verify(ticketModel, Mockito.never()).setAssignedGroup(Mockito.any(CsAgentGroupModel.class));
        Mockito.verifyNoMoreInteractions(ticketModel);
    }
}

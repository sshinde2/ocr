package au.com.target.tgtcore.user.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetAddressModel;


/**
 * Unit test for {@link TargetAddressService}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetAddressServiceTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @InjectMocks
    private final TargetAddressService addressService = new TargetAddressService();

    @Mock
    private ModelService modelService;

    @Mock
    private TargetAddressModel targetAddressModel;

    @Before
    public void setUp() {
        BDDMockito.given(modelService.create(TargetAddressModel.class)).willReturn(targetAddressModel);
    }

    @Test
    public void createAddressForOwnerForNull() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Parameter owner can not be null");
        addressService.createAddressForOwner(null);
    }

    @Test
    public void createAddressForOwner() {
        final ItemModel item = Mockito.mock(ItemModel.class);

        addressService.createAddressForOwner(item);

        Mockito.verify(modelService).create(TargetAddressModel.class);
        Mockito.verify(targetAddressModel).setOwner(item);
    }

}

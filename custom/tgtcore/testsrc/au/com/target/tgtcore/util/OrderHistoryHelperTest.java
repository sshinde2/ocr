/**
 * 
 */
package au.com.target.tgtcore.util;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.orderhistory.OrderHistoryService;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AddressModificationRecordEntryModel;
import au.com.target.tgtcore.model.AddressModificationRecordModel;


/**
 * Test suite for {@link OrderHistoryHelper}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderHistoryHelperTest {

    @Mock
    private UserService userService;
    @Mock
    private ModelService modelService;
    @Mock
    private OrderHistoryService orderHistoryService;

    @InjectMocks
    private final OrderHistoryHelper orderHistoryHelper = new OrderHistoryHelper();

    @Test
    public void testMakeDeliveryAddresChange() {
        final OrderModel order = mock(OrderModel.class);
        final AddressModel newAddress = mock(AddressModel.class);

        final OrderHistoryEntryModel history = mock(OrderHistoryEntryModel.class);
        when(history.getOrder()).thenReturn(order);
        final OrderModel version = mock(OrderModel.class);
        when(history.getPreviousOrderVersion()).thenReturn(version);
        when((OrderHistoryEntryModel)modelService.create(OrderHistoryEntryModel.class)).thenReturn(history);

        final AddressModificationRecordModel addressModifModel = mock(AddressModificationRecordModel.class);
        when((AddressModificationRecordModel)modelService.create(AddressModificationRecordModel.class))
                .thenReturn(addressModifModel);

        final AddressModificationRecordEntryModel amrem = mock(AddressModificationRecordEntryModel.class);
        when(
                (AddressModificationRecordEntryModel)modelService.create(AddressModificationRecordEntryModel.class))
                .thenReturn(amrem);

        when(orderHistoryService.createHistorySnapshot(order)).thenReturn(version);

        orderHistoryHelper.createHistorySnapshot(order, newAddress, "description");

        verify(orderHistoryService).createHistorySnapshot(order);
        verify(orderHistoryService).saveHistorySnapshot(version);
        verify(modelService).save(addressModifModel);
        verify(modelService).save(history);
    }

}

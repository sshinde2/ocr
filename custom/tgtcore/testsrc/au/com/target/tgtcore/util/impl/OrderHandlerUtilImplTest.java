/**
 * 
 */
package au.com.target.tgtcore.util.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.orderhistory.OrderHistoryService;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.util.OrderHandlerUtil;


/**
 * Class to test OrderHandlerUtil class
 * 
 * @author Pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderHandlerUtilImplTest {

    @InjectMocks
    private final OrderHandlerUtil orderHandlerUtil = new OrderHandlerUtilImpl();

    @Mock
    private PromotionsService promotionsService;

    @Mock
    private OrderHistoryService orderHistoryService;

    @Mock
    private ModelService modelService;

    @Mock
    private OrderHistoryEntryModel orderHistoryEntryModel;

    @Mock
    private OrderModel orderModel;

    @Mock
    private OrderModel versionOrderModel;

    @Mock
    private EmployeeModel employeeRequestor;

    @Mock
    private PrincipalModel principalRequestor;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        Mockito.doReturn(orderHistoryEntryModel).when(modelService).create(OrderHistoryEntryModel.class);
    }

    /**
     * Method to test createOrderSnapshot method when order version doesn't exist and requestor is not an employee
     */
    @Test
    public void verifyCreateOrderSnapshotWithoutVersionOrderAndUserAsRequestor() {
        Mockito.doReturn(versionOrderModel).when(orderHistoryService).createHistorySnapshot(orderModel);
        orderHandlerUtil.createOrderSnapshot(orderModel, StringUtils.EMPTY, principalRequestor);

        Mockito.verify(orderHistoryService).createHistorySnapshot(orderModel);
        Mockito.verify(orderHistoryService).saveHistorySnapshot(versionOrderModel);
        Mockito.verify(modelService).create(OrderHistoryEntryModel.class);
        verifyInteractionsWithOrderHistoryMock();
        Mockito.verify(modelService).save(orderHistoryEntryModel);
        Mockito.verify(promotionsService).transferPromotionsToOrder(orderModel, versionOrderModel, false);
        verifyNoMoreInterationsWithMocks();
    }

    /**
     * Method to test createOrderSnapshot when order version is already created and requestor is an employee
     */
    @Test
    public void verifyCreateOrderSnapshotWithVersionOrderAndEmployeeRequestor() {
        orderHandlerUtil.createOrderSnapshot(orderModel, versionOrderModel, StringUtils.EMPTY, employeeRequestor);

        Mockito.verify(orderHistoryService).saveHistorySnapshot(versionOrderModel);
        Mockito.verify(modelService).create(OrderHistoryEntryModel.class);
        verifyInteractionsWithOrderHistoryMock();
        Mockito.verify(orderHistoryEntryModel).setEmployee(employeeRequestor);
        Mockito.verify(modelService).save(orderHistoryEntryModel);
        Mockito.verify(promotionsService).transferPromotionsToOrder(orderModel, versionOrderModel, false);
        verifyNoMoreInterationsWithMocks();
    }

    /**
     * Method to test createOrderSnapshot when order version doesn't exist and requestor is missing
     */
    @Test
    public void verifyCreateOrderSnapshotWithoutVersionOrderAndRequestorIsMissing() {
        Mockito.doReturn(versionOrderModel).when(orderHistoryService).createHistorySnapshot(orderModel);
        orderHandlerUtil.createOrderSnapshot(orderModel, StringUtils.EMPTY, null);

        Mockito.verify(orderHistoryService).createHistorySnapshot(orderModel);
        Mockito.verify(orderHistoryService).saveHistorySnapshot(versionOrderModel);
        Mockito.verify(modelService).create(OrderHistoryEntryModel.class);
        verifyInteractionsWithOrderHistoryMock();
        Mockito.verify(modelService).save(orderHistoryEntryModel);
        Mockito.verify(promotionsService).transferPromotionsToOrder(orderModel, versionOrderModel, false);
        verifyNoMoreInterationsWithMocks();
    }

    /**
     * Method to verify interactions made with orderHistoryEntryModel mock
     */
    private void verifyInteractionsWithOrderHistoryMock() {
        Mockito.verify(orderHistoryEntryModel).setOrder(orderModel);
        Mockito.verify(orderHistoryEntryModel).setPreviousOrderVersion(versionOrderModel);
        Mockito.verify(orderHistoryEntryModel).setDescription(StringUtils.EMPTY);
        Mockito.verify(orderHistoryEntryModel).setTimestamp(Mockito.isA(Date.class));
    }


    /**
     * Method to verify no more interactions are made with the mocks
     */
    private void verifyNoMoreInterationsWithMocks() {
        Mockito.verifyNoMoreInteractions(orderHistoryEntryModel);
        Mockito.verifyNoMoreInteractions(orderHistoryService);
        Mockito.verifyNoMoreInteractions(modelService);
        Mockito.verifyNoMoreInteractions(promotionsService);
    }

}

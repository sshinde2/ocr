package au.com.target.tgtcore.valuepack.impl;

import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.valuepack.TargetValuePackService;
import au.com.target.tgtcore.valuepack.dao.TargetValuePackDao;

/**
 * Test suite for {@link TargetValuePackServiceImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetValuePackServiceImplTest {

    private static final String SKU = "12345";

    @Mock
    private TargetValuePackDao valuePackDao;

    @InjectMocks
    private TargetValuePackService valuePackService = new TargetValuePackServiceImpl();

    /**
     * Verifies that service layer object properly delegates the execution flow to DAO layer.
     */
    @Test
    public void testGetByChildSku() {
        valuePackService.getByChildSku(SKU);
        verify(valuePackDao).getByChildSku(SKU);
    }
}

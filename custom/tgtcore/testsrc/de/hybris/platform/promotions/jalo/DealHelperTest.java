package de.hybris.platform.promotions.jalo;

import de.hybris.bootstrap.annotations.UnitTest;

import junit.framework.Assert;

import org.junit.Test;


/**
 * Stolen direct from POS team TgtMathTest class
 */
@UnitTest
public class DealHelperTest {
    //*************************************************************************
    @Test
    public void testCalcDiscTruncated() throws Exception
    {
        Assert.assertEquals(1234, DealHelper.calcDiscTruncated(12340, 1000));

        // test the rounding is the correct direction
        Assert.assertEquals(1, DealHelper.calcDiscTruncated(10, 1000));
        Assert.assertEquals(2, DealHelper.calcDiscTruncated(11, 1000));
        Assert.assertEquals(2, DealHelper.calcDiscTruncated(12, 1000));
        Assert.assertEquals(2, DealHelper.calcDiscTruncated(13, 1000));
        Assert.assertEquals(2, DealHelper.calcDiscTruncated(14, 1000));
        Assert.assertEquals(2, DealHelper.calcDiscTruncated(15, 1000));
        Assert.assertEquals(2, DealHelper.calcDiscTruncated(16, 1000));
        Assert.assertEquals(2, DealHelper.calcDiscTruncated(17, 1000));
        Assert.assertEquals(2, DealHelper.calcDiscTruncated(18, 1000));
        Assert.assertEquals(2, DealHelper.calcDiscTruncated(19, 1000));
        Assert.assertEquals(2, DealHelper.calcDiscTruncated(20, 1000));

        Assert.assertEquals(2, DealHelper.calcDiscTruncated(199, 100));
        Assert.assertEquals(2, DealHelper.calcDiscTruncated(200, 100));
        Assert.assertEquals(3, DealHelper.calcDiscTruncated(201, 100));

        // test with big numbers
        Assert.assertEquals(2134567890, DealHelper.calcDiscTruncated(2134567890, 10000));
        Assert.assertEquals(2147483647, DealHelper.calcDiscTruncated(2147483647, 10000));

        // all the same tests with negatives
        // note value should now go down not up, ie sales and refunds produce same result
        Assert.assertEquals(1234, DealHelper.calcDiscTruncated(12340, 1000));

        // test the rounding is the correct direction
        Assert.assertEquals(-1, DealHelper.calcDiscTruncated(-10, 1000));
        Assert.assertEquals(-2, DealHelper.calcDiscTruncated(-11, 1000));
        Assert.assertEquals(-2, DealHelper.calcDiscTruncated(-12, 1000));
        Assert.assertEquals(-2, DealHelper.calcDiscTruncated(-13, 1000));
        Assert.assertEquals(-2, DealHelper.calcDiscTruncated(-14, 1000));
        Assert.assertEquals(-2, DealHelper.calcDiscTruncated(-15, 1000));
        Assert.assertEquals(-2, DealHelper.calcDiscTruncated(-16, 1000));
        Assert.assertEquals(-2, DealHelper.calcDiscTruncated(-17, 1000));
        Assert.assertEquals(-2, DealHelper.calcDiscTruncated(-18, 1000));
        Assert.assertEquals(-2, DealHelper.calcDiscTruncated(-19, 1000));
        Assert.assertEquals(-2, DealHelper.calcDiscTruncated(-20, 1000));

        Assert.assertEquals(-2, DealHelper.calcDiscTruncated(-199, 100));
        Assert.assertEquals(-2, DealHelper.calcDiscTruncated(-200, 100));
        Assert.assertEquals(-3, DealHelper.calcDiscTruncated(-201, 100));

        // test with big numbers
        Assert.assertEquals(-2134567890, DealHelper.calcDiscTruncated(-2134567890, 10000));
        Assert.assertEquals(-2147483647, DealHelper.calcDiscTruncated(-2147483647, 10000));
    }

    //*************************************************************************
    @Test
    public void testApportionDiscount() throws Exception
    {
        // test with just one item
        DealHelperDiscountableInterface[] list = new DealHelperDiscountableAdaptor[] {
                new DealHelperDiscountableAdaptor(100)
        };

        // simple discount
        DealHelper.apportionDiscount(list, 20);
        Assert.assertEquals(20, list[0].getDiscAmt());
        // no discount
        DealHelper.apportionDiscount(list, 0);
        Assert.assertEquals(0, list[0].getDiscAmt());
        // 100% discount
        DealHelper.apportionDiscount(list, 100);
        Assert.assertEquals(100, list[0].getDiscAmt());
        // > 100% discount
        DealHelper.apportionDiscount(list, 101);
        Assert.assertEquals(100, list[0].getDiscAmt()); // cannot go negative

        // test with empty list
        list = new DealHelperDiscountableAdaptor[0];

        DealHelper.apportionDiscount(list, 20);
        DealHelper.apportionDiscount(list, 0);
        DealHelper.apportionDiscount(list, 100);
        DealHelper.apportionDiscount(list, 101);


        // test with mulitple items
        list = new DealHelperDiscountableAdaptor[] {
                new DealHelperDiscountableAdaptor(100),
                new DealHelperDiscountableAdaptor(100)
        };

        // simple discount
        DealHelper.apportionDiscount(list, 20);
        Assert.assertEquals(10, list[0].getDiscAmt());
        Assert.assertEquals(10, list[1].getDiscAmt());

        // no discount
        DealHelper.apportionDiscount(list, 0);
        Assert.assertEquals(0, list[0].getDiscAmt());
        Assert.assertEquals(0, list[1].getDiscAmt());

        // 100% discount
        DealHelper.apportionDiscount(list, 200);
        Assert.assertEquals(100, list[0].getDiscAmt());
        Assert.assertEquals(100, list[1].getDiscAmt());

        // > 100% discount
        DealHelper.apportionDiscount(list, 250);
        Assert.assertEquals(100, list[0].getDiscAmt()); // cannot go negative
        Assert.assertEquals(100, list[1].getDiscAmt());

        // does not evenly divide
        DealHelper.apportionDiscount(list, 23);
        if (list[0].getDiscAmt() == 12) {
            Assert.assertEquals(12, list[0].getDiscAmt());
            Assert.assertEquals(11, list[1].getDiscAmt());
        }
        else {
            Assert.assertEquals(11, list[0].getDiscAmt());
            Assert.assertEquals(12, list[1].getDiscAmt());
        }

    }
}

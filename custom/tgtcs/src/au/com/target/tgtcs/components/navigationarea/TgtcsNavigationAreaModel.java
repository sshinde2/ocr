/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtcs.components.navigationarea;

import de.hybris.platform.cockpit.components.navigationarea.DefaultNavigationAreaModel;
import de.hybris.platform.cockpit.session.impl.AbstractUINavigationArea;

import au.com.target.tgtcs.session.impl.TgtcsNavigationArea;


/**
 * Tgtcs navigation area model.
 */
public class TgtcsNavigationAreaModel extends DefaultNavigationAreaModel
{
    public TgtcsNavigationAreaModel()
    {
        super();
    }

    public TgtcsNavigationAreaModel(final AbstractUINavigationArea area)
    {
        super(area);
    }

    @Override
    public TgtcsNavigationArea getNavigationArea()
    {
        return (TgtcsNavigationArea) super.getNavigationArea();
    }
}

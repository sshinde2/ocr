package au.com.target.tgtcs.components.orderdetail.controllers;

import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.cscockpit.widgets.controllers.impl.DefaultCheckoutController;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zk.ui.Execution;

import au.com.target.tgtcore.ordercancel.data.IpgNewRefundInfoDTO;
import au.com.target.tgtcs.facade.TgtCsCardPaymentFacade;


/**
 * The Class TgtCsCardPaymentController.
 */
public class TgtCsCardPaymentController extends DefaultCheckoutController {

    private TgtCsCardPaymentFacade tgtCsCardPaymentFacade;

    /**
     * Handle refund payment form response.
     * 
     * @param execution
     *            the execution
     * @return the list
     */
    public List<String> handleRefundPaymentFormResponse(final Execution execution) {
        return tgtCsCardPaymentFacade.handleRefundPaymentFormResponse(execution);
    }

    /**
     * Handle cancel payment form response.
     * 
     * @param execution
     *            the execution
     * @return the list
     */
    public List<String> handleCancelPaymentFormResponse(final Execution execution) {
        return tgtCsCardPaymentFacade.handleCancelPaymentFormResponse(execution);
    }

    /**
     * clear any old existing payment sessions
     */
    public void clearOldPayment() {
        tgtCsCardPaymentFacade.clearOldPayment();
    }

    /**
     * get unauthorized and unpaied amount
     * 
     * @return the amount in double format
     */

    /**
     * Gets the new payment.
     * 
     * @return the new payment
     */
    public PaymentInfoModel getNewPayment() {
        return tgtCsCardPaymentFacade.getNewPayment();
    }

    /**
     * Get ipg new refund info dto
     * 
     * @return IpgNewRefundInfoDTO
     */
    public IpgNewRefundInfoDTO getIpgNewRefundInfoDTO() {
        return tgtCsCardPaymentFacade.getIpgNewRefundInfoDTO();
    }

    /**
     * @param tgtCsCardPaymentFacade
     *            the tgtCsCardPaymentFacade to set
     */
    @Required
    public void setTgtCsCardPaymentFacade(final TgtCsCardPaymentFacade tgtCsCardPaymentFacade) {
        this.tgtCsCardPaymentFacade = tgtCsCardPaymentFacade;
    }

}
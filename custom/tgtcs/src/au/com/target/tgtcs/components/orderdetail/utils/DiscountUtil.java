/**
 * 
 */
package au.com.target.tgtcs.components.orderdetail.utils;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.util.DiscountValue;


/**
 * @author Nandini
 * 
 */
public class DiscountUtil {
    /**
     * 
     */
    private DiscountUtil() {
        super();
    }

    /**
     * Return discount about from discountValues
     * 
     * @param orderEntryModel
     *            the orderEntryModel
     * @return amount of discount
     */
    public static double getDiscount(final AbstractOrderEntryModel orderEntryModel) {
        double result = 0D;
        for (final DiscountValue dv : orderEntryModel.getDiscountValues()) {
            result = result + dv.getAppliedValue();
        }
        return result;
    }
}

/**
 * 
 */
package au.com.target.tgtcs.components.orderdetail.utils;

import de.hybris.platform.core.model.product.ProductModel;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * @author Nandini
 * 
 */
public class ProductUtil {
    private ProductUtil() {
        super();
    }

    /**
     * Returns the product colour
     * 
     * @param product
     *            the product
     * @return the product colour
     */
    public static String getProductColour(final ProductModel product)
    {
        String colourName = StringUtils.EMPTY;
        if (product instanceof TargetColourVariantProductModel) {
            colourName = ((TargetColourVariantProductModel)product).getColourName();

        }
        else if (product instanceof TargetSizeVariantProductModel) {
            final ProductModel baseProduct = ((TargetSizeVariantProductModel)product).getBaseProduct();
            if (baseProduct instanceof TargetColourVariantProductModel) {
                colourName = ((TargetColourVariantProductModel)baseProduct).getColourName();
            }
        }
        return colourName != null ? colourName : StringUtils.EMPTY;
    }

    /**
     * Returns the product size
     * 
     * @param product
     *            the product
     * @return the product size
     */
    public static String getProductSize(final ProductModel product)
    {
        String size = StringUtils.EMPTY;
        if (product instanceof TargetSizeVariantProductModel) {
            size = ((TargetSizeVariantProductModel)product).getSize();
        }

        return size != null ? size : StringUtils.EMPTY;
    }
}

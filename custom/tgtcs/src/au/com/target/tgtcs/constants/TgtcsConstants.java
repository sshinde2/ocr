/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 * 
 */
package au.com.target.tgtcs.constants;



/**
 * Global class for all tgtcs constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class TgtcsConstants extends GeneratedTgtcsConstants {
    public static final String QUALIFIER_ADDRESS_FIRSTNAME = "Address.firstname";
    public static final String QUALIFIER_ADDRESS_LASTNAME = "Address.lastname";
    public static final String QUALIFIER_ADDRESS_LINE1 = "Address.line1";
    public static final String QUALIFIER_ADDRESS_LINE2 = "Address.line2";
    public static final String QUALIFIER_ADDRESS_TOWN = "Address.town";
    public static final String QUALIFIER_ADDRESS_DISTRICT = "Address.district";
    public static final String QUALIFIER_ADDRESS_POSTALCODE = "Address.postalcode";
    public static final String QUALIFIER_ADDRESS_COUNTRY = "Address.country";
    public static final String QUALIFIER_TARGETADDRESS_ADDRESSVALIDATED = "TargetAddress.addressValidated";
    public static final String QUALIFIIER_ITEM_OWNER = "Item.owner";
    public static final String QUALIFIIER_TARGET_CUSTOMER_PHONENUMBER = "TargetCustomer.phoneNumber";
    public static final String QUALIFIER_CUSTOMER_TITLE = "Customer.title";
    public static final String NAME_SEPARATOR = " ";
    public static final String ENTERED_ADDRESS_RADIO_BUTTON_KEY_POSTFIX = "enteredAddressRadioButton";
    public static final String SELECT_BUTTON_KEY_POSTFIX = "selectButton";
    public static final String QUALIFIER_ADDRESS_PHONENUMBER1 = "Address.phone1";

    // cockpit event parameters constants
    public static final String EVENT_CLASS_KEY = "eventClass";
    public static final String NEW_DELIVERY_MODE_KEY = "newDeliveryMode";
    public static final String ORDER_KEY = "order";

    // handle address details validation
    public static final String PHONE_LENGTH_ERROR_MESSAGE = "tgtcscockpit.checkout.address.validphonenumber";
    public static final String INVALID_ADDRESS = "tgtcscockpit.checkout.invalid.address";
    public static final String FAILED_PLACE_ORDER = "tgtcscockpit.checkout.failed.order";
    public static final String ERROR_MESSAGE_CODE = "tgtcscockpit.orderdetail.addNewPayment.window.open.error";

    //payment form css
    public static final String ALL_COLUMN_PADDING_BOTTOM = "padding-bottom:0;";
    public static final String TEXT_EDITOR_HEIGHT_MAX = "100%";
    public static final String LABEL_WIDTH_CSS = "9em, none";
    public static final String EACH_COLUMN_HEIGHT_CSS = "height:35px;";
    public static final String PAYMENT_FORM_WIDTH = "750px";
    public static final String LABEL_WIDTH = "150px";
    public static final String CARD_HOLDER_FULL_NAME_WIDTH = "300px";
    public static final String SECURITY_CODE_WIDTH = "30px";
    public static final String ALL_COLUMN_WIDTH_CSS = "100%";
    public static final String TEXT_EDITOR_WIDTH = "40px";
    public static final int TEXT_EDITOR_MAX_LENGTH = 10;

    public static final String STREET_ADDRESS_WIDTH = "500px";
    public static final String POSTAL_CODE_LABEL_WIDTH = "50px";
    public static final String PHONE_NUMBER_LABEL_WIDTH = "80px";
    public static final String BUTTON_STYLE = "cursor: pointer;"
            + "font-family: Arial,Helvetica,sans-serif;"
            + "background-color: rgb(32, 43, 74);"
            + "color: rgb(255, 255, 255);"
            + "background-image: url(\"images/button_back.gif\");"
            + "background-repeat: repeat-x;"
            + "border: 0px none;";

    //payemnt card and TNS
    public static final String GATEWAY_CARD_EXPIRY_DATE_YEAR = "gatewayCardExpiryDateYear";
    public static final String GATEWAY_CARD_EXPIRY_DATE_MONTH = "gatewayCardExpiryDateMonth";
    public static final String HOSTED_PAYMENT_FORM_CONTAINER = "hostedPaymentFormContainer";
    public static final String ADD_NEW_PAYMENT = "Add New Payment";
    public static final String AMOUNT = "amount";
    public static final String AMOUNT_MUST_NOT_BE_ZERO = "amount must not be zero";
    public static final String AMOUNT_REQUIRED = "amount is required";

    //payment form elements
    public static final String FIRST_NAME = "firstName";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String STATE = "state";
    public static final String COUNTRY = "country";
    public static final String POSTAL_CODE = "postalCode";
    public static final String CITY = "city";
    public static final String STREET2 = "street2";
    public static final String STREET1 = "street1";
    public static final String LAST_NAME = "lastName";
    public static final String IPG_RECEIPT_NUMBER = "receiptNo";

    //payment form elements label
    public static final String NAME_ON_CARD = "nameOnCard";
    public static final String CARD_TYPE_LABEL = "Card type *";
    public static final String CARD_TYPE = "cardType";
    public static final String CARD_NUMBER_LABEL = "Card Number *";
    public static final String GATEWAY_CARD_NUMBER = "gatewayCardNumber";
    public static final String CARD_EXPIRY_MONTH_LABEL = "Card Expiry Month *";
    public static final String CARD_EXPIRY_YEAR_LABEL = "Card Expiry Year *";
    public static final String CARD_SECURITY_CODE_LABEL = "Card Security Code *";
    public static final String GATEWAY_CARD_SECURITY_CODE = "gatewayCardSecurityCode";
    public static final String CARD_HOLDER_FULL_NAME = "Card holder full name*";
    public static final String EDITOR_WIDGET_EDITOR_LABEL = "editorWidgetEditorLabel";
    public static final String TEXT = "text";
    public static final String PHONE_NUMBER_LABEL = "Phone number";
    public static final String COUNTRY_LABEL = "Country *";
    public static final String POSTAL_CODE_LABEL = "Postal code *";
    public static final String STATE_LABEL = "State *";
    public static final String CITY_SUBURB = "City/Suburb *";
    public static final String STREET_ADDRESS_2 = "Street Address 2";
    public static final String STREET_ADDRESS = "Street Address *";
    public static final String LAST_NAME_LABEL = "Last name *";
    public static final String FIRST_NAME_LABEL = "First name *";
    public static final String IPG_RECEIPT_NUMBER_LABEL = "Receipt No";
    public static final String REFUND_AMOUNT = "Refund Amount";
    public static final String REFUNDABLE_AMOUNT = "Refundable Amount";
    public static final String MANUAL_REFUND_AMOUNT = "Manual Refund Amount";

    //payment form properties
    public static final String EDITOR_WIDGET_EDITOR = "editorWidgetEditor";
    public static final String TEXT_EDITOR = "textEditor";
    public static final String NAME = "name";
    public static final String HIDDEN = "hidden";
    public static final String TYPE = "type";
    public static final String SUBMIT = "submit";
    public static final String PAYMENT_FORM = "paymentForm";
    public static final String ACTION = "action";

    public static final String PAYMENT_TYPE = "paymentType";
    public static final String CREDITCARD = "creditcard";
    public static final String PAYMENT_SESSION_ID = "paymentSessionId";
    public static final String TNSACTION = "tnsaction";
    public static final String GATEWAY_RETURN_URL = "gatewayReturnURL";
    public static final String ORDER_ID = "orderID";
    public static final String PAYMENT_FORM_METHOD = "method";
    public static final String PAYMENT_FORM_METHOD_TYPE = "post";
    public static final String MYIFRAME = "myiframe";
    public static final String TARGET = "target";

    public static final String GATEWAY_CARD_SCHEME = "gatewayCardScheme";
    public static final String GATEWAY_FORM_RESPONSE = "gatewayFormResponse";

    public static final String DINERS_CLUB = "diners_club";
    public static final String SPACE_SEPARATOR = " ";
    public static final String TNS_ERROR_INVALID = "tgtcscockpit.payment.validation.tns.invalid";
    public static final String TNS_FIELD_REQUIRED = "tgtcscockpit.payment.validation.tns.req";
    public static final String POSTAL_CODE_REQUIRED = "tgtcscockpit.payment.validation.postal.req";
    public static final String COUNTRY_REQUIRED = "tgtcscockpit.payment.validation.country.req";
    public static final String STATE_REQUIRED = "tgtcscockpit.payment.validation.state.req";
    public static final String CITY_SUBURB_REQUIRED = "tgtcscockpit.payment.validation.city.req";
    public static final String STREET_REQUIRED = "tgtcscockpit.payment.validation.street.req";
    public static final String LAST_NAME_REQUIRED = "tgtcscockpit.payment.validation.lastname.req";
    public static final String FIRST_NAME_REQUIRED = "tgtcscockpit.payment.validation.firstname.req";
    public static final String CARD_TYPE_REQUIRED = "tgtcscockpit.payment.validation.cardtype.req";
    public static final String CARD_HOLDER_FULL_NAME_REQUIRED = "tgtcscockpit.payment.validation.cardholdername.req";
    public static final String TNS_ERROR_TYPE_FIELD_REQUIRED = "1~";
    public static final String TNS_ERROR_TYPE_FIELD_INVALID = "2~";
    public static final String TNS_ERROR_TYPE_FIELD_ERROR = "3~";
    public static final String TNS_ERROR_TYPE_SYSTEM_ERROR = "4~";
    public static final String TNS_FIELD_ERRORS = "tgtcscockpit.payment.validation.tns.field.error";
    public static final String CARD_SECURITY_CODE_REQUIRED = "tgtcscockpit.payment.validation.cardsecuritycode.req";
    public static final String CARD_SECURITY_CODE = "tgtcscockpit.payment.label.cardsecuritycode";
    public static final String CARD_EXPIRY_YEAR = "tgtcscockpit.payment.label.cardexpyear";
    public static final String CARD_EXPIRY_MONTH = "tgtcscockpit.payment.label.cardexpmonth";
    public static final String CARD_NUMBER = "tgtcscockpit.payment.label.cardnumber";
    public static final String MISSING_GATEWAY_FORM_RESPONSE = "tgtcscockpit.payment.error.missing.gateway.response";
    public static final String SESSION_KEY_HAS_EXPIRED_GETTING_NEW_ONE = "tgtcscockpit.payment.validation.session.expired";
    public static final String TNS_SYSTEM_ERROR = "tgtcscockpit.payment.validation.tns.system.error";
    public static final String PIPE_SEPARATOR = "|";
    public static final int EDITOR_MAX_LENGTH = 20;
    public static final int MAX_LENGTH_TEXTFIELD_STREET = 40;
    public static final int MAX_LENGTH_TEXTFIELD = 200;
    public static final String SUCCESS = "success";
    public static final String FAILURE = "failure";
    public static final String IPG_TRANSACTION_RESULT_KEY = "QUERY_TRANSACTION_RESULT";
    public static final String IS_B2B_ORDER_ATTR = "isB2BOrder";
    public static final String MANUAL_REFUND_ERROR_TITLE = "tgtcscockpit.widget.orderdetail.popup.csChangeOrder.failed";

}

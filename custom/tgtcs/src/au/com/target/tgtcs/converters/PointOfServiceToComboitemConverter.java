/**
 * 
 */
package au.com.target.tgtcs.converters;

import de.hybris.platform.converters.impl.AbstractConverter;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import org.zkoss.zul.Comboitem;


/**
 * @author Pavel_Yakushevich
 * 
 */
public class PointOfServiceToComboitemConverter extends AbstractConverter<PointOfServiceModel, Comboitem> {

    public static final String BEAN_NAME = "targetPOStoComboitemConverter";


    @Override
    public void populate(final PointOfServiceModel source, final Comboitem target) {
        final AddressModel posAddress = source.getAddress();
        final String posLabel = String.format("%s, %s, %s", posAddress.getDistrict(), posAddress.getTown(),
                posAddress.getStreetname());
        target.setLabel(posLabel);
        target.setValue(source);
    }

}

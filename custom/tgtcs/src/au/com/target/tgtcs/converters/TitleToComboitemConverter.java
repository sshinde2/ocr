/**
 * 
 */
package au.com.target.tgtcs.converters;

import de.hybris.platform.converters.impl.AbstractConverter;
import de.hybris.platform.core.model.user.TitleModel;

import org.zkoss.zul.Comboitem;


/**
 * @author Pavel_Yakushevich
 * 
 */
public class TitleToComboitemConverter extends AbstractConverter<TitleModel, Comboitem> {
    public static final String BEAN_NAME = "targetTitletoComboitemConverter";


    @Override
    public void populate(final TitleModel source, final Comboitem target) {
        target.setLabel(source.getName());
        target.setValue(source);
    }

}

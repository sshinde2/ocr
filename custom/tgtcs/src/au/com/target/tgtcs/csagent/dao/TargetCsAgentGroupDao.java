/**
 * 
 */
package au.com.target.tgtcs.csagent.dao;

import de.hybris.platform.ticket.model.CsAgentGroupModel;

import java.util.List;


/**
 * @author mjanarth
 *
 */
public interface TargetCsAgentGroupDao {

    /**
     * 
     * @return List
     */
    List<CsAgentGroupModel> findAllCsAgentGroup();
}

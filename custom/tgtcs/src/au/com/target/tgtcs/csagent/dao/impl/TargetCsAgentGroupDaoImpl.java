/**
 * 
 */
package au.com.target.tgtcs.csagent.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.ticket.model.CsAgentGroupModel;

import java.util.List;

import au.com.target.tgtcs.csagent.dao.TargetCsAgentGroupDao;


/**
 * @author mjanarth
 *
 */
public class TargetCsAgentGroupDaoImpl extends DefaultGenericDao<CsAgentGroupModel> implements TargetCsAgentGroupDao {



    private static final String CSAGENTGROUP_QUERY = "SELECT {" + CsAgentGroupModel.PK + "} FROM {"
            + CsAgentGroupModel._TYPECODE + "}";

    /**
     * 
     */
    public TargetCsAgentGroupDaoImpl() {
        super(CsAgentGroupModel._TYPECODE);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcs.csagent.dao.TargetCsAgentGroupDao#findAllCsAgentGroup()
     */
    @Override
    public List<CsAgentGroupModel> findAllCsAgentGroup() {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(CSAGENTGROUP_QUERY);
        final SearchResult<CsAgentGroupModel> result = getFlexibleSearchService().search(query);
        return result.getResult();
    }


}

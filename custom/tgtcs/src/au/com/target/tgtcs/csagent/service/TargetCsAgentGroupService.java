/**
 * 
 */
package au.com.target.tgtcs.csagent.service;

import java.util.List;


/**
 * @author mjanarth
 *
 */
public interface TargetCsAgentGroupService {

    /**
     * Returns all the uids of the csAgentGroups
     * 
     * @return List
     */
    List<String> getAllCustomerAgentGroupUid();

}

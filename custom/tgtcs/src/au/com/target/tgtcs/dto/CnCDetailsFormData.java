/**
 * 
 */
package au.com.target.tgtcs.dto;

import de.hybris.platform.core.model.user.TitleModel;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;


/**
 * @author Pavel_Yakushevich
 * 
 */
public class CnCDetailsFormData {

    private String state;
    private TargetPointOfServiceModel store;
    private String firstName;
    private String lastName;
    private String phone;
    private TitleModel title;

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(final String state) {
        this.state = state;
    }

    /**
     * @return the store
     */
    public TargetPointOfServiceModel getStore() {
        return store;
    }

    /**
     * @param store
     *            the store to set
     */
    public void setStore(final TargetPointOfServiceModel store) {
        this.store = store;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     *            the phone to set
     */
    public void setPhone(final String phone) {
        this.phone = phone;
    }

    /**
     * @return the title
     */
    public TitleModel getTitle() {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(final TitleModel title) {
        this.title = title;
    }
}

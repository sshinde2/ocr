/**
 * 
 */
package au.com.target.tgtcs.dto;

/**
 * Document media details
 */
public class DocumentMediaDTO
{

    private String url;
    private String mime;

    /**
     * @return the url
     */
    public String getURL() {
        return url;
    }

    /**
     * @param url
     *            the url to set
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    /**
     * @return the mime
     */
    public String getMime() {
        return mime;
    }

    /**
     * @param mime
     *            the mime to set
     */
    public void setMime(final String mime) {
        this.mime = mime;
    }


}

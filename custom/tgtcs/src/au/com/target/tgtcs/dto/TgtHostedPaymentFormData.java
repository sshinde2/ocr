/**
 * 
 */
package au.com.target.tgtcs.dto;

import de.hybris.platform.cockpit.wizards.Message;

import java.util.Collections;
import java.util.List;


/**
 * @author xiaofanlu
 * 
 */
public class TgtHostedPaymentFormData {
    private String amount;
    private String sessionKey;

    private String email;
    private String phoneNumber;

    private String firstName;
    private String lastName;
    private String streetAddress;
    private String streetAddress2;
    private String city;
    private String state;
    private String postalCode;
    private String country;

    private String cardHolderFullName;
    private String cardType;
    private String cardScheme;
    private String cardNumber;
    private String cardExpiryMonth;
    private String cardExpiryYear;
    private String cardSecurityCode;

    private String gatewayFormResponse;
    private List<Message> messages;



    public String getAmount() {
        return amount;
    }


    public void setAmount(final String amount) {
        this.amount = amount;
    }


    public String getSessionKey() {
        return sessionKey;
    }


    public void setSessionKey(final String sessionKey) {
        this.sessionKey = sessionKey;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(final String email) {
        this.email = email;
    }


    public String getPhoneNumber() {
        return phoneNumber;
    }


    public void setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    public String getCardHolderFullName() {
        return cardHolderFullName;
    }


    public void setCardHolderFullName(final String cardHolderFullName) {
        this.cardHolderFullName = cardHolderFullName;
    }


    public String getFirstName() {
        return firstName;
    }


    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }


    public String getStreetAddress() {
        return streetAddress;
    }


    public void setStreetAddress(final String streetAddress) {
        this.streetAddress = streetAddress;
    }


    public String getStreetAddress2() {
        return streetAddress2;
    }


    public void setStreetAddress2(final String streetAddress2) {
        this.streetAddress2 = streetAddress2;
    }


    public String getCity() {
        return city;
    }


    public void setCity(final String city) {
        this.city = city;
    }


    public String getState() {
        return state;
    }


    public void setState(final String state) {
        this.state = state;
    }


    public String getPostalCode() {
        return postalCode;
    }


    public void setPostalCode(final String postalCode) {
        this.postalCode = postalCode;
    }


    public String getCountry() {
        return country;
    }


    public void setCountry(final String country) {
        this.country = country;
    }


    public String getCardType() {
        return cardType;
    }


    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }


    public String getCardScheme() {
        return cardScheme;
    }


    public void setCardScheme(final String cardScheme) {
        this.cardScheme = cardScheme;
    }


    public String getCardNumber() {
        return cardNumber;
    }


    public void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }


    public String getCardExpiryMonth() {
        return cardExpiryMonth;
    }


    public void setCardExpiryMonth(final String cardExpiryMonth) {
        this.cardExpiryMonth = cardExpiryMonth;
    }


    public String getCardExpiryYear() {
        return cardExpiryYear;
    }


    public void setCardExpiryYear(final String cardExpiryYear) {
        this.cardExpiryYear = cardExpiryYear;
    }


    public String getCardSecurityCode() {
        return cardSecurityCode;
    }


    public void setCardSecurityCode(final String cardSecurityCode) {
        this.cardSecurityCode = cardSecurityCode;
    }


    public String getGatewayFormResponse() {
        return gatewayFormResponse;
    }


    public void setGatewayFormResponse(final String gatewayFormResponse) {
        this.gatewayFormResponse = gatewayFormResponse;
    }

    public List<Message> getMessages() {
        return messages == null ? Collections.EMPTY_LIST : Collections.unmodifiableList(messages);
    }


    public void setMessages(final List<Message> messages) {
        this.messages = messages;
    }
}

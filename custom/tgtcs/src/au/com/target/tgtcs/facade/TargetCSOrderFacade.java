/**
 * 
 */
package au.com.target.tgtcs.facade;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;


/**
 * @author Pavel_Yakushevich
 * 
 */
public interface TargetCSOrderFacade {

    /**
     * Checks if <code>order</code> is available for <code>deliveryMode</code>. Returns <code>true</code>, if specified
     * <code>order</code> is available for specified <code>deliveryMode</code>, <code>false</code> otherwise.
     * 
     * @param order
     *            the {@link AbstractOrderModel} to check the availability for
     * @param deliveryMode
     *            the {@link DeliveryModeModel} to check the availability for
     * @return boolean <code>true</code>, if specified <code>order</code> is available for specified
     *         <code>deliveryMode</code>, <code>false</code> otherwise
     */
    boolean isOrderAvailableForDeliveryMode(AbstractOrderModel order, DeliveryModeModel deliveryMode);

    /**
     * Checks if <code>entry</code> is available for <code>deliveryMode</code>. Returns <code>true</code>, if specified
     * <code>entry</code> is available for specified <code>deliveryMode</code>, <code>false</code> otherwise.
     * 
     * @param entry
     *            the {@link AbstractOrderEntryModel} to check the availability for
     * @param deliveryMode
     *            the {@link DeliveryModeModel} to check the availability for
     * @return boolean <code>true</code>, if specified <code>entry</code> is available for specified
     *         <code>deliveryMode</code>, <code>false</code> otherwise
     */
    boolean isOrderEntryAvailableForDeliveryMode(AbstractOrderEntryModel entry, DeliveryModeModel deliveryMode);

    /**
     * Changes <code>order</code> delivery mode to specified one.
     * 
     * @param order
     *            the {@link CartModel} to change attributes on
     * @param deliveryMode
     *            the {@link DeliveryModeModel} to be used as new delivery mode for the order.
     */
    void changeDeliveryMode(CartModel order, DeliveryModeModel deliveryMode);
}

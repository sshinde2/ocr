/**
 * 
 */
package au.com.target.tgtcs.facade.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.cart.TargetCartService;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcs.facade.TargetCSOrderFacade;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;


/**
 * @author Pavel_Yakushevich
 * 
 */
public class TargetCSOrderFacadeImpl implements TargetCSOrderFacade {

    private ModelService modelService;

    private TargetDeliveryModeHelper deliveryModeHelper;

    private TargetCommerceCartService commerceCartService;

    private TargetCartService cartService;

    /**
     * {@inheritDoc} If order has no entries, it is considered unavailable for any delivery mode. If it has any entries,
     * its availability for specified delivery mode is defined by simultaneous availability of its entries for said
     * delivery mode.
     */
    @Override
    public boolean isOrderAvailableForDeliveryMode(final AbstractOrderModel order,
            final DeliveryModeModel deliveryMode) {
        if (order.getEntries().size() == 0) {
            return false;
        }

        boolean available = true;

        for (final AbstractOrderEntryModel entry : order.getEntries()) {
            available &= isOrderEntryAvailableForDeliveryMode(entry, deliveryMode);
        }

        return available;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isOrderEntryAvailableForDeliveryMode(final AbstractOrderEntryModel entry,
            final DeliveryModeModel deliveryMode) {
        return getDeliveryModeHelper().isProductAvailableForDeliveryMode(entry.getProduct(), deliveryMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void changeDeliveryMode(final CartModel order, final DeliveryModeModel deliveryMode) {
        order.setDeliveryAddress(null);
        getCartService().setDeliveryMode(order, deliveryMode);
        final CommerceCartParameter cartParameter = new CommerceCartParameter();
        cartParameter.setCart(order);
        getCommerceCartService().calculateCart(cartParameter);
        getModelService().save(order);
    }

    /**
     * @return the modelService
     */
    public ModelService getModelService() {
        return modelService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @return the deliveryModeHelper
     */
    public TargetDeliveryModeHelper getDeliveryModeHelper() {
        return deliveryModeHelper;
    }

    /**
     * @param deliveryModeHelper
     *            the deliveryModeHelper to set
     */
    @Required
    public void setDeliveryModeHelper(final TargetDeliveryModeHelper deliveryModeHelper) {
        this.deliveryModeHelper = deliveryModeHelper;
    }

    /**
     * @return the commerceCartService
     */
    public TargetCommerceCartService getCommerceCartService() {
        return commerceCartService;
    }

    /**
     * @param commerceCartService
     *            the commerceCartService to set
     */
    @Required
    public void setCommerceCartService(final TargetCommerceCartService commerceCartService) {
        this.commerceCartService = commerceCartService;
    }

    /**
     * @return the cartService
     */
    public TargetCartService getCartService() {
        return cartService;
    }

    /**
     * @param cartService
     *            the cartService to set
     */
    @Required
    public void setCartService(final TargetCartService cartService) {
        this.cartService = cartService;
    }


}

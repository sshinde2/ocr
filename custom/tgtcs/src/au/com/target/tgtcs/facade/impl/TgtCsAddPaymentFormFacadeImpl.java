/**
 * 
 */
package au.com.target.tgtcs.facade.impl;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.cscockpit.services.payment.CsCardPaymentService;
import de.hybris.platform.cscockpit.services.payment.strategy.impl.DefaultCsOrderUnauthorizedTotalStrategy;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Form;
import org.zkoss.zhtml.Input;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Div;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtcs.facade.TgtCsAddPaymentFormFacade;
import au.com.target.tgtcs.facade.TgtCsCardPaymentFacade;
import au.com.target.tgtcs.util.CountryComparator;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.dto.HostedSessionTokenRequest;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.service.TargetPaymentService;


/**
 * @author umesh
 * 
 */
public class TgtCsAddPaymentFormFacadeImpl implements TgtCsAddPaymentFormFacade {

    public static final int PAYMENT_OPERATION_TYPE_TOKENIZE = 0;
    public static final int PAYMENT_OPERATION_TYPE_CAPTURE = 1;
    private static final String ADD_NEW_PAYMENT_REFUND_FORM_ZUL = "/addNewPaymentRefundForm.zul";
    private static final String ADD_NEW_PAYMENT_CANCEL_FORM_ZUL = "/addNewPaymentCancelForm.zul";
    private static final String PAYMENT_TYPE_VALUE = "0";
    private String tgtcsbaseurl;
    private String tnsbaseurl;
    private CsCardPaymentService csCardPaymentService;
    private TgtCsCardPaymentFacade tgtCsCardPaymentFacade;
    private TargetPaymentService targetPaymentService;
    private PaymentModeService paymentModeService;
    private CommonI18NService commonI18NService;
    private DefaultCsOrderUnauthorizedTotalStrategy defaultCsOrderUnauthorizedTotalStrategy;

    @Override
    public void loadFormContent(final OrderModel order, final Window window) {
        //clear any possible old token when the payment dialog loads.
        tgtCsCardPaymentFacade.clearOldPayment();
        createForm(window);
        fillWindowContent(order, window);
    }

    @Override
    public void loadIpgFormContent(final OrderModel order, final Window window) {
        //clear any possible old token when the payment dialog loads.
        tgtCsCardPaymentFacade.clearOldPayment();
        final Div formcontainer = (Div)window.getFellow(TgtcsConstants.HOSTED_PAYMENT_FORM_CONTAINER);
        final Form form = new Form();
        form.setId(TgtcsConstants.PAYMENT_FORM);
        form.setParent(formcontainer);

        createFormDoubleBox(form, TgtcsConstants.REFUND_AMOUNT, TgtcsConstants.AMOUNT, TgtcsConstants.LABEL_WIDTH,
                TgtcsConstants.EDITOR_MAX_LENGTH,
                true);
        createFormTextBox(form, TgtcsConstants.IPG_RECEIPT_NUMBER_LABEL,
                TgtcsConstants.IPG_RECEIPT_NUMBER, TgtcsConstants.LABEL_WIDTH, TgtcsConstants.MAX_LENGTH_TEXTFIELD);

        createIpgFormButton(form, TgtcsConstants.ADD_NEW_PAYMENT);

        window.setWidth(TgtcsConstants.PAYMENT_FORM_WIDTH);

    }

    /**
     * Fill window content.
     */
    private void fillWindowContent(final OrderModel order, final Window window) {
        fillUpCardTypes(order, window);
        fillUpCountries(window);
    }

    /**
     * Fill up countries.
     * 
     * @param window
     *            the window
     */
    protected void fillUpCountries(final Window window) {
        final Combobox countrycombo = (Combobox)window.getFellow(TgtcsConstants.COUNTRY);
        final List<CountryData> countries = getDeliveryCountries();
        if (CollectionUtils.isNotEmpty(countries)) {
            for (final CountryData item : countries) {
                final Comboitem comboItem = new Comboitem();
                countrycombo.appendChild(comboItem);
                comboItem.setValue(item.getIsocode());
                comboItem.setLabel(item.getName());
            }
        }
    }

    /**
     * Gets the delivery countries.
     * 
     * @return the delivery countries
     */
    protected List<CountryData> getDeliveryCountries() {
        final List<CountryData> result = new ArrayList<>();
        final List<CountryModel> deliveryCountries = sortCountries(commonI18NService.getAllCountries());
        if (CollectionUtils.isNotEmpty(deliveryCountries)) {
            for (final CountryModel country : deliveryCountries) {
                final CountryData countryData = new CountryData();
                countryData.setIsocode(country.getIsocode());
                countryData.setName(country.getName());
                result.add(countryData);
            }
        }
        return result;
    }

    /**
     * Sort countries.
     * 
     * @param countries
     *            the countries
     * @return the list
     */
    private List<CountryModel> sortCountries(final Collection<CountryModel> countries) {
        if (CollectionUtils.isNotEmpty(countries)) {
            final List<CountryModel> result = new ArrayList<CountryModel>(countries);
            Collections.sort(result, CountryComparator.INSTANCE);
            return result;
        }
        return null;
    }

    /**
     * Fill up card type and countries.
     */
    private void fillUpCardTypes(final OrderModel order, final Window window) {
        final Combobox cardtypecombo = (Combobox)window.getFellow(TgtcsConstants.CARD_TYPE);
        final List<CreditCardType> types = getAvailableCardTypes(order);
        if (CollectionUtils.isNotEmpty(types)) {
            for (final CreditCardType item : types) {
                final Comboitem comboItem = new Comboitem();
                cardtypecombo.appendChild(comboItem);
                comboItem.setValue(item.getCode());
                comboItem.setLabel(item.name());
            }
        }
    }

    /**
     * return a list of available credit card types
     * 
     * @return list of available card types
     */
    protected List<CreditCardType> getAvailableCardTypes(final OrderModel order) {
        return csCardPaymentService.getSupportedCardSchemes(order);
    }

    /**
     * Creates add payment form
     */
    private void createForm(final Window window) {
        final Div formcontainer = (Div)window.getFellow(TgtcsConstants.HOSTED_PAYMENT_FORM_CONTAINER);
        final Form form = new Form();
        form.setId(TgtcsConstants.PAYMENT_FORM);
        form.setParent(formcontainer);
        createFormDoubleBox(form, null, TgtcsConstants.AMOUNT, null, TgtcsConstants.EDITOR_MAX_LENGTH, false);
        cardDetails(form);
        billingAddress(form);
        returnURL(form);
        createFormButton(form, TgtcsConstants.ACTION, TgtcsConstants.ADD_NEW_PAYMENT);
        createFormHiddenField(form, TgtcsConstants.PAYMENT_TYPE);
        window.setWidth(TgtcsConstants.PAYMENT_FORM_WIDTH);
    }

    /**
     * Creates the form button.
     * 
     * @param parent
     *            the parent
     * @param fieldName
     *            the field name
     * @param fieldValue
     *            the field value
     */
    private void createFormButton(final AbstractComponent parent, final String fieldName, final String fieldValue) {
        final Input button = new Input();
        button.setDynamicProperty(TgtcsConstants.TYPE, TgtcsConstants.SUBMIT);
        button.setDynamicProperty(TgtcsConstants.NAME, fieldName);
        button.setStyle(TgtcsConstants.BUTTON_STYLE);
        button.setValue(fieldValue);
        parent.appendChild(button);
    }

    private void createIpgFormButton(final AbstractComponent parent, final String label) {
        final Button button = new Button(label);
        button.addEventListener(Events.ON_CLICK, new EventListener() {

            @Override
            public void onEvent(final Event paramEvent) throws Exception {
                try {
                    tgtCsCardPaymentFacade.createPaymentInfoModelForIpg(parent);
                    Messagebox
                            .show(
                                    "Succeed to add the new payment, please close the add new payment option window and finish the rest of the process",
                                    "Add new payment successfully",
                                    Messagebox.OK,
                                    Messagebox.INFORMATION);
                }
                catch (final Exception e) {
                    Messagebox.show(
                            e.getMessage()
                                    + ((e.getCause() == null) ? "" : new StringBuilder(" - ").append(
                                            e.getCause().getMessage())
                                            .toString()),
                            Labels.getLabel(TgtcsConstants.MANUAL_REFUND_ERROR_TITLE), 1, "z-msgbox z-msgbox-error");
                }

            }
        });
        parent.appendChild(button);
    }

    /**
     * Creates the form hidden field.
     * 
     * @param parent
     *            the parent
     * @param fieldName
     *            the field name
     */
    private void createFormHiddenField(final AbstractComponent parent, final String fieldName) {
        final Input hiddenInput = new Input();
        hiddenInput.setDynamicProperty(TgtcsConstants.TYPE, TgtcsConstants.HIDDEN);
        hiddenInput.setDynamicProperty(TgtcsConstants.NAME, fieldName);
        hiddenInput.setId(fieldName);
        parent.appendChild(hiddenInput);
    }

    /**
     * Billing address.
     * 
     * @param form
     *            the form
     */
    private void billingAddress(final Form form) {
        createFormTextBox(form, TgtcsConstants.FIRST_NAME_LABEL, TgtcsConstants.FIRST_NAME,
                TgtcsConstants.LABEL_WIDTH, TgtcsConstants.MAX_LENGTH_TEXTFIELD);
        createFormTextBox(form, TgtcsConstants.LAST_NAME_LABEL, TgtcsConstants.LAST_NAME,
                TgtcsConstants.LABEL_WIDTH, TgtcsConstants.MAX_LENGTH_TEXTFIELD);
        createFormTextBox(form, TgtcsConstants.STREET_ADDRESS, TgtcsConstants.STREET1,
                TgtcsConstants.STREET_ADDRESS_WIDTH, TgtcsConstants.MAX_LENGTH_TEXTFIELD_STREET);
        createFormTextBox(form, TgtcsConstants.STREET_ADDRESS_2, TgtcsConstants.STREET2,
                TgtcsConstants.STREET_ADDRESS_WIDTH, TgtcsConstants.MAX_LENGTH_TEXTFIELD_STREET);
        createFormTextBox(form, TgtcsConstants.CITY_SUBURB, TgtcsConstants.CITY,
                TgtcsConstants.LABEL_WIDTH, TgtcsConstants.MAX_LENGTH_TEXTFIELD);
        createFormTextBox(form, TgtcsConstants.STATE_LABEL, TgtcsConstants.STATE,
                TgtcsConstants.LABEL_WIDTH, TgtcsConstants.MAX_LENGTH_TEXTFIELD);
        createFormTextBox(form, TgtcsConstants.POSTAL_CODE_LABEL, TgtcsConstants.POSTAL_CODE,
                TgtcsConstants.POSTAL_CODE_LABEL_WIDTH, TgtcsConstants.MAX_LENGTH_TEXTFIELD);
        createFormSelect(form, TgtcsConstants.COUNTRY_LABEL, TgtcsConstants.COUNTRY);
        createFormTextBox(form, TgtcsConstants.PHONE_NUMBER_LABEL, TgtcsConstants.PHONE_NUMBER,
                TgtcsConstants.PHONE_NUMBER_LABEL_WIDTH, TgtcsConstants.MAX_LENGTH_TEXTFIELD);
    }

    /**
     * Card details.
     * 
     * @param form
     *            the form
     */
    private void cardDetails(final Form form) {
        createFormTextBox(form, TgtcsConstants.CARD_HOLDER_FULL_NAME, TgtcsConstants.NAME_ON_CARD,
                TgtcsConstants.CARD_HOLDER_FULL_NAME_WIDTH, TgtcsConstants.MAX_LENGTH_TEXTFIELD);
        createFormSelect(form, TgtcsConstants.CARD_TYPE_LABEL, TgtcsConstants.CARD_TYPE);
        createFormTextBox(form, TgtcsConstants.CARD_NUMBER_LABEL, TgtcsConstants.GATEWAY_CARD_NUMBER,
                TgtcsConstants.LABEL_WIDTH, TgtcsConstants.MAX_LENGTH_TEXTFIELD);
        createFormIntBox(form, TgtcsConstants.CARD_EXPIRY_MONTH_LABEL,
                TgtcsConstants.GATEWAY_CARD_EXPIRY_DATE_MONTH);
        createFormIntBox(form, TgtcsConstants.CARD_EXPIRY_YEAR_LABEL,
                TgtcsConstants.GATEWAY_CARD_EXPIRY_DATE_YEAR);
        createFormTextBox(form, TgtcsConstants.CARD_SECURITY_CODE_LABEL,
                TgtcsConstants.GATEWAY_CARD_SECURITY_CODE,
                TgtcsConstants.SECURITY_CODE_WIDTH, TgtcsConstants.MAX_LENGTH_TEXTFIELD);
    }

    /**
     * Creates the form int box.
     * 
     * @param parent
     *            the parent
     * @param labelText
     *            the label text
     * @param fieldName
     *            the field name
     */
    private void createFormIntBox(final AbstractComponent parent, final String labelText, final String fieldName) {
        final Div div = new Div();
        div.setSclass(TgtcsConstants.EDITOR_WIDGET_EDITOR);
        div.setStyle(TgtcsConstants.ALL_COLUMN_PADDING_BOTTOM);
        div.setHeight(TgtcsConstants.ALL_COLUMN_WIDTH_CSS);
        div.setWidth(TgtcsConstants.ALL_COLUMN_WIDTH_CSS);
        parent.appendChild(div);

        final Hbox hbox = getNewHBox();
        hbox.setWidth(TgtcsConstants.ALL_COLUMN_WIDTH_CSS);
        hbox.setWidths(TgtcsConstants.LABEL_WIDTH_CSS);
        hbox.setStyle(TgtcsConstants.EACH_COLUMN_HEIGHT_CSS);
        div.appendChild(hbox);

        final Label label = new Label();
        label.setSclass(TgtcsConstants.EDITOR_WIDGET_EDITOR_LABEL);
        label.setValue(labelText);
        hbox.appendChild(label);

        final Intbox intbox = new Intbox();
        intbox.setSclass(TgtcsConstants.TEXT_EDITOR);
        intbox.setName(fieldName);
        intbox.setId(fieldName);
        intbox.setWidth(TgtcsConstants.TEXT_EDITOR_WIDTH);
        intbox.setMaxlength(TgtcsConstants.TEXT_EDITOR_MAX_LENGTH);

        hbox.appendChild(intbox);
    }

    /**
     * Creates the form text box.
     * 
     * @param parent
     *            the parent
     * @param labelText
     *            the label text
     * @param fieldName
     *            the field name
     * @param width
     *            the width
     */
    private void createFormTextBox(final AbstractComponent parent, final String labelText, final String fieldName,
            final String width, final int maxLength) {
        final Div div = new Div();
        div.setSclass(TgtcsConstants.EDITOR_WIDGET_EDITOR);
        div.setStyle(TgtcsConstants.ALL_COLUMN_PADDING_BOTTOM);
        div.setHeight(TgtcsConstants.ALL_COLUMN_WIDTH_CSS);
        div.setWidth(TgtcsConstants.ALL_COLUMN_WIDTH_CSS);
        parent.appendChild(div);

        final Hbox hbox = getNewHBox();
        hbox.setWidth(TgtcsConstants.ALL_COLUMN_WIDTH_CSS);
        hbox.setWidths(TgtcsConstants.LABEL_WIDTH_CSS);
        hbox.setStyle(TgtcsConstants.EACH_COLUMN_HEIGHT_CSS);
        div.appendChild(hbox);

        final Label label = new Label();
        label.setSclass(TgtcsConstants.EDITOR_WIDGET_EDITOR_LABEL);
        label.setValue(labelText);
        hbox.appendChild(label);

        final Textbox textbox = new Textbox();
        textbox.setSclass(TgtcsConstants.TEXT_EDITOR);
        textbox.setType(TgtcsConstants.TEXT);
        textbox.setWidth(width);
        textbox.setName(fieldName);
        textbox.setId(fieldName);
        textbox.setMaxlength(maxLength);
        hbox.appendChild(textbox);
    }

    /**
     * Creates the form select.
     * 
     * @param parent
     *            the parent
     * @param labelText
     *            the label text
     * @param fieldName
     *            the field name
     */
    private void createFormSelect(final AbstractComponent parent, final String labelText, final String fieldName) {
        final Div div = new Div();
        div.setSclass(TgtcsConstants.EDITOR_WIDGET_EDITOR);
        div.setStyle(TgtcsConstants.ALL_COLUMN_PADDING_BOTTOM);
        div.setHeight(TgtcsConstants.ALL_COLUMN_WIDTH_CSS);
        div.setWidth(TgtcsConstants.ALL_COLUMN_WIDTH_CSS);
        parent.appendChild(div);

        final Hbox hbox = getNewHBox();
        hbox.setWidth(TgtcsConstants.ALL_COLUMN_WIDTH_CSS);
        hbox.setWidths(TgtcsConstants.LABEL_WIDTH_CSS);

        hbox.setStyle(TgtcsConstants.EACH_COLUMN_HEIGHT_CSS);
        div.appendChild(hbox);

        final Label label = new Label();
        label.setSclass(TgtcsConstants.EDITOR_WIDGET_EDITOR_LABEL);
        label.setValue(labelText);
        hbox.appendChild(label);

        final Combobox select = new Combobox();
        select.setId(fieldName);
        select.setName(fieldName);
        hbox.appendChild(select);
    }

    /**
     * Creates the form double box.
     * 
     * @param parent
     *            the parent
     * @param fieldName
     *            the field name
     */
    protected void createFormDoubleBox(final AbstractComponent parent, final String labelText, final String fieldName,
            final String width,
            final int maxLength,
            final boolean visible) {
        final Div div = new Div();
        div.setSclass(TgtcsConstants.EDITOR_WIDGET_EDITOR);
        div.setStyle(TgtcsConstants.ALL_COLUMN_PADDING_BOTTOM);
        div.setHeight(TgtcsConstants.TEXT_EDITOR_HEIGHT_MAX);
        div.setWidth(TgtcsConstants.TEXT_EDITOR_HEIGHT_MAX);
        parent.appendChild(div);

        final Hbox hbox = getNewHBox();
        hbox.setWidth(TgtcsConstants.TEXT_EDITOR_HEIGHT_MAX);
        hbox.setWidths(TgtcsConstants.LABEL_WIDTH_CSS);
        hbox.setStyle(TgtcsConstants.EACH_COLUMN_HEIGHT_CSS);
        div.appendChild(hbox);

        if (StringUtils.isNotEmpty(labelText)) {
            final Label label = new Label();
            label.setSclass(TgtcsConstants.EDITOR_WIDGET_EDITOR_LABEL);
            label.setValue(labelText);
            hbox.appendChild(label);
        }
        final Doublebox doublebox = new Doublebox();
        doublebox.setSclass(TgtcsConstants.TEXT_EDITOR);
        doublebox.setName(fieldName);
        doublebox.setId(fieldName);
        doublebox.setWidth(width);
        doublebox.setMaxlength(maxLength);
        doublebox.setVisible(visible);

        hbox.appendChild(doublebox);
    }

    /**
     * Gets the new h box.
     * 
     * @return the new h box
     */
    protected Hbox getNewHBox() {
        return new Hbox();
    }

    @Override
    public void populateRefundForm(final OrderModel order, final Window window) {
        populatePaymentFields(order, window, ADD_NEW_PAYMENT_REFUND_FORM_ZUL, null);
    }

    @Override
    public void populateCancelForm(final OrderModel order, final Window window) {
        final double amount = getSuggestedAmountForPaymentOption(order);
        final Doublebox amountbox = (Doublebox)window.getFellow(TgtcsConstants.AMOUNT);
        amountbox.setValue(amount);

        populatePaymentFields(order, window, ADD_NEW_PAYMENT_CANCEL_FORM_ZUL, PAYMENT_TYPE_VALUE);
    }

    private void populatePaymentFields(final OrderModel order, final Window window,
            final String zulFileName, final String paymentTypeString) {
        ((Intbox)window.getFellow(TgtcsConstants.GATEWAY_CARD_EXPIRY_DATE_MONTH)).setValue(Integer.valueOf(
                java.util.Calendar
                        .getInstance()
                        .get(
                                java.util.Calendar.MONTH)
                        + 1));
        ((Intbox)window.getFellow(TgtcsConstants.GATEWAY_CARD_EXPIRY_DATE_YEAR)).setValue(Integer.valueOf(
                java.util.Calendar.getInstance()
                        .get(
                                java.util.Calendar.YEAR)));
        final AddressModel addressModel = order.getPaymentAddress();
        if (addressModel != null) {
            ((Textbox)window.getFellow(TgtcsConstants.FIRST_NAME)).setValue(addressModel.getFirstname());
            ((Textbox)window.getFellow(TgtcsConstants.LAST_NAME)).setValue(addressModel.getLastname());
            ((Textbox)window.getFellow(TgtcsConstants.STREET1)).setValue(addressModel.getLine1());
            ((Textbox)window.getFellow(TgtcsConstants.STREET2)).setValue(addressModel.getLine2());
            ((Textbox)window.getFellow(TgtcsConstants.CITY)).setValue(addressModel.getTown());
            ((Textbox)window.getFellow(TgtcsConstants.POSTAL_CODE)).setValue(addressModel.getPostalcode());
            if (addressModel.getCountry() != null) {
                ((Combobox)window.getFellow(TgtcsConstants.COUNTRY)).setValue(addressModel.getCountry().getIsocode());
            }
            ((Textbox)window.getFellow(TgtcsConstants.PHONE_NUMBER)).setValue(addressModel.getPhone1());
            ((Textbox)window.getFellow(TgtcsConstants.STATE)).setValue(addressModel.getDistrict());
        }
        //set tns information
        final HostedSessionTokenRequest hostedSessionTokenRequest = new HostedSessionTokenRequest();
        hostedSessionTokenRequest.setAmount(BigDecimal.ZERO);
        hostedSessionTokenRequest.setCancelUrl(null);
        hostedSessionTokenRequest.setOrderModel(order);
        hostedSessionTokenRequest.setPaymentCaptureType(PaymentCaptureType.PLACEORDER);
        hostedSessionTokenRequest.setPaymentMode(paymentModeService
                .getPaymentModeForCode(TgtcsConstants.CREDITCARD));
        hostedSessionTokenRequest.setReturnUrl(null);

        final TargetCreateSubscriptionResult targetCreateSubscriptionResult = targetPaymentService
                .getHostedSessionToken(hostedSessionTokenRequest);
        final String paymentSessionToken = targetCreateSubscriptionResult != null
                ? targetCreateSubscriptionResult.getRequestToken() : StringUtils.EMPTY;

        ((Input)window.getFellow(TgtcsConstants.PAYMENT_SESSION_ID)).setValue(paymentSessionToken);
        ((Input)window.getFellow(TgtcsConstants.TNSACTION)).setValue(tnsbaseurl + paymentSessionToken);
        ((Input)window.getFellow(TgtcsConstants.GATEWAY_RETURN_URL))
                .setValue(tgtcsbaseurl + zulFileName);
        ((Input)window.getFellow(TgtcsConstants.ORDER_ID)).setValue(order.getCode());

        if (null != paymentTypeString) {
            ((Input)window.getFellow(TgtcsConstants.PAYMENT_TYPE)).setValue(String.valueOf(PAYMENT_TYPE_VALUE));
        }

        final Form form = (Form)window.getFellow(TgtcsConstants.PAYMENT_FORM);
        form.setDynamicProperty(TgtcsConstants.ACTION, tnsbaseurl + paymentSessionToken);
        form.setDynamicProperty(TgtcsConstants.PAYMENT_FORM_METHOD, TgtcsConstants.PAYMENT_FORM_METHOD_TYPE);
        form.setDynamicProperty(TgtcsConstants.TARGET, TgtcsConstants.MYIFRAME);
    }

    /**
     * 
     * @param order
     * @return suggested amount for payment option
     */
    private double getSuggestedAmountForPaymentOption(final OrderModel order) {
        return defaultCsOrderUnauthorizedTotalStrategy.getUnauthorizedTotal(order);
    }

    /**
     * Return url.
     * 
     * @param form
     *            the form
     */
    private void returnURL(final Form form) {
        createFormHiddenField(form, TgtcsConstants.GATEWAY_RETURN_URL);
        createFormHiddenField(form, TgtcsConstants.PAYMENT_SESSION_ID);
        createFormHiddenField(form, TgtcsConstants.TNSACTION);
        createFormHiddenField(form, TgtcsConstants.ORDER_ID);
    }

    /**
     * @param csCardPaymentService
     *            the csCardPaymentService to set
     */
    @Required
    public void setCsCardPaymentService(final CsCardPaymentService csCardPaymentService) {
        this.csCardPaymentService = csCardPaymentService;
    }

    /**
     * @param targetPaymentService
     *            the targetPaymentService to set
     */
    @Required
    public void setTargetPaymentService(final TargetPaymentService targetPaymentService) {
        this.targetPaymentService = targetPaymentService;
    }

    /**
     * @param paymentModeService
     *            the paymentModeService to set
     */
    @Required
    public void setPaymentModeService(final PaymentModeService paymentModeService) {
        this.paymentModeService = paymentModeService;
    }

    /**
     * @param tgtcsbaseurl
     *            the tgtcsbaseurl to set
     */
    @Required
    public void setTgtcsbaseurl(final String tgtcsbaseurl) {
        this.tgtcsbaseurl = tgtcsbaseurl;
    }

    /**
     * @param tnsbaseurl
     *            the tnsbaseurl to set
     */
    @Required
    public void setTnsbaseurl(final String tnsbaseurl) {
        this.tnsbaseurl = tnsbaseurl;
    }

    /**
     * @param tgtCsCardPaymentFacade
     *            the tgtCsCardPaymentFacade to set
     */
    @Required
    public void setTgtCsCardPaymentFacade(final TgtCsCardPaymentFacade tgtCsCardPaymentFacade) {
        this.tgtCsCardPaymentFacade = tgtCsCardPaymentFacade;
    }

    /**
     * @param commonI18NService
     *            the commonI18NService to set
     */
    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    /**
     * @param defaultCsOrderUnauthorizedTotalStrategy
     *            the new default cs order unauthorized total strategy
     */
    @Required
    public void setDefaultCsOrderUnauthorizedTotalStrategy(
            final DefaultCsOrderUnauthorizedTotalStrategy defaultCsOrderUnauthorizedTotalStrategy) {
        this.defaultCsOrderUnauthorizedTotalStrategy = defaultCsOrderUnauthorizedTotalStrategy;
    }

}

/**
 * 
 */
package au.com.target.tgtcs.listeners;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.zkoss.spring.SpringUtil;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.api.Combobox;

import au.com.target.tgtcs.util.TargetCsCockpitUtil;
import au.com.target.tgtcs.widgets.controllers.impl.TargetCnCStoreSelectController;


/**
 * This EventListener, when triggered by {@link SelectEvent}, changes dataset in <code>addressCombobox</code> to match
 * selected District. The selected District is retrieved from the SelectEvent that triggered this listener.
 * 
 * @author Pavel_Yakushevich
 * 
 */
public class StateChangeEventListener implements EventListener {

    private Combobox addressCombobox;

    private TargetCnCStoreSelectController controller;

    /**
     * Constructor
     * 
     * @param combobox
     *            the store address combobox
     */
    public StateChangeEventListener(final Combobox combobox) {
        setAddressCombobox(combobox);
        wireBeans();
    }

    /**
     * Performs {@link SpringUtil}-powered bean wiring.
     */
    protected void wireBeans() {
        setCheckoutController((TargetCnCStoreSelectController)SpringUtil.getBean("targetCnCStoreSelectController",
                TargetCnCStoreSelectController.class));
    }

    @Override
    public void onEvent(final Event event) throws Exception {
        if (event instanceof SelectEvent) {
            final Collection<Comboitem> selectedItems = ((SelectEvent)event).getSelectedItems();

            Validate.isTrue(selectedItems.size() == 1, "Expected 1 selected item, found ", selectedItems.size());

            final Comboitem item = selectedItems.iterator().next();
            final Object value = item != null ? item.getValue() : null;

            if (value instanceof String) {
                // fetch new items
                final List<Comboitem> itemsForState = new ArrayList<Comboitem>(getCheckoutController()
                        .getStoresForState((String)value));
                // deselect current item
                getAddressCombobox().setSelectedIndex(-1);
                // remove children
                TargetCsCockpitUtil.removeChildren(getAddressCombobox());
                // add new children
                TargetCsCockpitUtil.addAllChildren(getAddressCombobox(), itemsForState);
            }
        }
    }

    /**
     * @return the addressCombobox
     */
    public Combobox getAddressCombobox() {
        return addressCombobox;
    }

    /**
     * @param addressCombobox
     *            the addressCombobox to set
     */
    public void setAddressCombobox(final Combobox addressCombobox) {
        this.addressCombobox = addressCombobox;
    }

    /**
     * @return the controller
     */
    public TargetCnCStoreSelectController getCheckoutController() {
        return controller;
    }

    /**
     * @param selectController
     *            the controller to set
     */
    public void setCheckoutController(final TargetCnCStoreSelectController selectController) {
        this.controller = selectController;
    }

}

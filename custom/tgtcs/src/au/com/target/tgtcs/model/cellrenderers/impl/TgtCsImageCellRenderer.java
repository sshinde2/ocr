/**
 *
 */
package au.com.target.tgtcs.model.cellrenderers.impl;

import de.hybris.platform.cockpit.model.listview.CellRenderer;
import de.hybris.platform.cockpit.model.listview.TableModel;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.cscockpit.model.cellrenderers.impl.ImageCellRenderer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;


/**
 * @author Pradeep
 *
 */
public class TgtCsImageCellRenderer implements CellRenderer {

    private static final String CS_IMAGE_CELL = "csImageCell";

    private static final String DUMMY_IMAGE = "cockpit/images/dummyUrl";
    private static final Logger LOG = Logger.getLogger(TgtCsImageCellRenderer.class);


    @Override
    public void render(final TableModel model, final int colIndex, final int rowIndex, final Component parent)
    {
        if ((model == null) || (parent == null))
        {
            throw new IllegalArgumentException("Model or parent can not be null");
        }

        try
        {
            final Object value = model.getValueAt(colIndex, rowIndex);

            final Div div = new Div();
            div.setParent(parent);
            div.setSclass(CS_IMAGE_CELL);

            String imgSrc = DUMMY_IMAGE;
            if (value instanceof TypedObject)
            {
                final Object object = ((TypedObject)value).getObject();
                if (object instanceof MediaModel)
                {
                    imgSrc = ((MediaModel)object).getInternalURL();
                }
                else
                {
                    LOG.warn("Could not render cell. Reason: Item not a media.");
                }
            }
            else if (value instanceof String)
            {
                imgSrc = (String)value;
            }
            else
            {
                LOG.warn("Could not render cell. Reason: Value not a TypedObject.");
            }

            String displayImgUrl = DUMMY_IMAGE;
            if (StringUtils.isNotBlank(imgSrc))
            {
                //  ~ is appended to prevent url getting appended with extension name
                displayImgUrl = '~' + imgSrc;
            }

            final Image img = new Image(displayImgUrl);
            img.setParent(div);
        }
        catch (final IllegalArgumentException ex)
        {
            LOG.warn("Could not render cell using [" + ImageCellRenderer.class.getSimpleName() + "]", ex);
        }

    }

}

package au.com.target.tgtcs.model.editor.impl;

import de.hybris.platform.cockpit.model.editor.EditorListener;
import de.hybris.platform.cockpit.model.editor.impl.DefaultTextUIEditor;

import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.impl.InputElement;


public class TargetMandatoryTextUIEditor extends DefaultTextUIEditor {
    private static final String MESSAGE_UNABLE_TO_UPDATE_ADDRESS = "tgtcscockpit.unableToUpdateAddress";

    class OnBlurEventListener implements EventListener {
        private final InputElement editor;
        private final EditorListener listener;
        private final HtmlBasedComponent container;

        public OnBlurEventListener(final InputElement editor, final EditorListener listener,
                final HtmlBasedComponent container) {
            super();
            this.editor = editor;
            this.listener = listener;
            this.container = container;
        }

        @Override
        public void onEvent(final Event arg0)
                throws Exception
        {
            // check mandatory field
            if (editor.getRawText() == null || editor.getRawText().isEmpty()) {
                Messagebox.show(Labels.getLabel("required_attribute_missing") + ": "
                        + editor.getAttribute("editorType").toString(),
                        Labels.getLabel(MESSAGE_UNABLE_TO_UPDATE_ADDRESS),
                        Messagebox.OK,
                        Messagebox.ERROR);
                return;
            }
            final boolean valueUnchanged;
            if (editor.getRawValue() != null)
            {
                valueUnchanged = editor.getRawValue().equals(TargetMandatoryTextUIEditor.this.getValue());
            }
            else
            {
                valueUnchanged = TargetMandatoryTextUIEditor.this.getValue() == null;
            }
            if (!valueUnchanged)
            {
                if ((editor.getRawValue() == null) || (StringUtils.isEmpty(editor.getRawValue().toString())))
                {
                    TargetMandatoryTextUIEditor.this.setValue(null);
                }
                else
                {
                    TargetMandatoryTextUIEditor.this.setValue(editor.getRawValue());
                }
                TargetMandatoryTextUIEditor.this.fireValueChanged(listener);
            }
            ((CancelButtonContainer)container).showButton(false);
        }
    }

    @Override
    public HtmlBasedComponent createViewComponent(final Object initialValue,
            final Map<String, ? extends Object> parameters,
            final EditorListener listener) {
        final HtmlBasedComponent container = super.createViewComponent(
                initialValue, parameters, listener);
        if (isEditable()) {
            final InputElement editor = setUpNewEditor(container, parameters);
            editor.addEventListener("onBlur", new OnBlurEventListener(editor, listener, container));
        }
        return container;
    }

    private InputElement setUpNewEditor(final HtmlBasedComponent container,
            final Map<String, ? extends Object> parameters) {
        final InputElement editor = (InputElement)((CancelButtonContainer)container)
                .getContent();
        editor.setAttribute("editorType", parameters.get("attributeQualifier"));
        // remove default listeners: onBlur
        final Iterator<EventListener> listeners = editor
                .getListenerIterator("onBlur");
        listeners.next();
        listeners.remove();
        return editor;
    }
}

package au.com.target.tgtcs.model.editor.impl;

import de.hybris.platform.cockpit.model.editor.EditorListener;
import de.hybris.platform.cockpit.model.editor.impl.DefaultTextUIEditor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.core.Registry;
import de.hybris.platform.cscockpit.widgets.renderers.utils.PopupWidgetHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.impl.InputElement;

import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtcs.widgets.controllers.AddressCustomerController;
import au.com.target.tgtcs.widgets.impl.TgtcsListboxWidget;
import au.com.target.tgtcs.widgets.renderers.impl.ConfirmUpdateAddressWidgetRenderer;
import au.com.target.tgtverifyaddr.data.AddressData;


public class TgtcsTextUIEditor extends DefaultTextUIEditor {

    private static final String CONFIRM_UPDATE_ADDRESS_WIDGET_CONFIG = "csConfirmUpdateAddressWidgetConfig";
    private static final String CSS_CONFIRM_UPDATE_ADDRESS_POPUP = "csConfirmAddressWidget";
    private static final String CONFIRM_UPDATE_ADDRESS_POPUP_TITLE_KEY = "cscockpit.widget.address.update.confirmPopup";
    private static final String MESSAGE_UNABLE_TO_UPDATE_ADDRESS = "tgtcscockpit.unableToUpdateAddress";

    class OnEditEventListener implements EventListener {
        private final InputElement editor;
        private final EditorListener listener;

        public OnEditEventListener(final InputElement editor, final EditorListener listener) {
            super();
            this.editor = editor;
            this.listener = listener;
        }

        @Override
        public void onEvent(final Event event) throws Exception {
            TgtcsTextUIEditor.this.inEditMode = true;
            final boolean valueUnchanged;
            if (editor.getRawValue() != null) {
                valueUnchanged = editor.getRawValue().equals(
                        TgtcsTextUIEditor.this.getValue());
            }
            else {
                valueUnchanged = TgtcsTextUIEditor.this.getValue() == null;
            }
            if (!valueUnchanged) {
                if ((editor.getRawValue() == null)
                        || (StringUtils.isEmpty(editor.getRawValue()
                                .toString()))) {
                    TgtcsTextUIEditor.this.setValue(null);
                }
                else {
                    TgtcsTextUIEditor.this.setValue(editor
                            .getRawValue());
                }
                TgtcsTextUIEditor.this.fireValueChanged(listener);
            }
            TgtcsTextUIEditor.this.inEditMode = false;
        }
    }


    class OnBlurEventListener implements EventListener {
        private final InputElement editor;
        private final EditorListener listener;
        private final Map<String, ? extends Object> parameters;
        private final HtmlBasedComponent container;

        public OnBlurEventListener(final InputElement editor, final EditorListener listener,
                final Map<String, ? extends Object> parameters, final HtmlBasedComponent container) {
            super();
            this.editor = editor;
            this.listener = listener;
            this.parameters = parameters;
            this.container = container;
        }

        @Override
        public void onEvent(final Event event) throws Exception {
            // check mandatory field
            if (!checkMandatoryField()) {
                return;
            }
            final boolean valueUnchanged;
            if (editor.getRawValue() != null) {
                valueUnchanged = editor.getRawValue().equals(
                        TgtcsTextUIEditor.this.getValue());
            }
            else {
                valueUnchanged = TgtcsTextUIEditor.this.getValue() == null;
            }
            if (!valueUnchanged) {
                final InputElement elementLine, elementCity, elementState, elementPostalCode;
                final InputElement element = (InputElement)event.getTarget();
                final String id = element.getId();
                final int index = id.indexOf('_');
                final String row = id.substring(index + 1);
                if (id.indexOf(TgtcsConstants.QUALIFIER_ADDRESS_LINE1) >= 0
                        || id.indexOf(TgtcsConstants.QUALIFIER_ADDRESS_LINE2) >= 0) {
                    elementLine = element;
                }
                else {
                    elementLine = (InputElement)element
                            .getFellow(TgtcsConstants.QUALIFIER_ADDRESS_LINE1 + "_"
                                    + row);
                }
                if (id.indexOf(TgtcsConstants.QUALIFIER_ADDRESS_TOWN) < 0) {
                    elementCity = (InputElement)element
                            .getFellow(TgtcsConstants.QUALIFIER_ADDRESS_TOWN + "_"
                                    + row);
                }
                else {
                    elementCity = element;
                }

                if (id.indexOf(TgtcsConstants.QUALIFIER_ADDRESS_DISTRICT) < 0) {
                    elementState = (InputElement)element
                            .getFellow(TgtcsConstants.QUALIFIER_ADDRESS_DISTRICT + "_"
                                    + row);
                }
                else {
                    elementState = element;
                }

                if (id.indexOf(TgtcsConstants.QUALIFIER_ADDRESS_POSTALCODE) < 0) {
                    elementPostalCode = (InputElement)element
                            .getFellow(TgtcsConstants.QUALIFIER_ADDRESS_POSTALCODE
                                    + "_" + row);
                }
                else {
                    elementPostalCode = element;
                }

                final String addressString = populateAddressString(elementLine,
                        elementCity, elementState, elementPostalCode);
                final List<AddressData> checkResult = ((AddressCustomerController)Registry
                        .getApplicationContext().getBean(
                                "csCustomerController"))
                                        .checkValidAddress(addressString);

                // get item object
                final TypedObject item = (TypedObject)parameters
                        .get("parentObject");
                final TargetAddressModel addressModel = (TargetAddressModel)item
                        .getObject();
                if (checkResult.isEmpty()
                        || (checkResult.size() == 1 && checkResult
                                .get(0).getPartialAddress().equals(""))) {
                    saveAddressWhenNoMatchQAS(addressModel);
                }
                else {
                    processAddressWhenHaveMatchQAS(elementLine, elementCity, elementState, elementPostalCode, element,
                            checkResult, addressModel);
                }
            }
            ((CancelButtonContainer)container).showButton(false);
        }

        private String populateAddressString(final InputElement elementLine1, final InputElement elementCity,
                final InputElement elementState, final InputElement elementPostalCode) {
            final StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(elementLine1.getRawValue());
            stringBuilder.append(", ");
            stringBuilder.append(elementCity.getRawValue());
            stringBuilder.append(", ");
            stringBuilder.append(elementState.getRawValue());
            stringBuilder.append(", ");
            stringBuilder.append(elementPostalCode.getRawValue());
            return stringBuilder.toString();
        }

        private void processAddressWhenHaveMatchQAS(final InputElement elementLine, final InputElement elementCity,
                final InputElement elementState, final InputElement elementPostalCode, final InputElement element,
                final List<AddressData> checkResult, final TargetAddressModel addressModel) {
            final ConfirmUpdateAddressWidgetRenderer beanRenderer = (ConfirmUpdateAddressWidgetRenderer)Registry
                    .getApplicationContext()
                    .getBean(
                            "csConfirmUpdateAddressWidgetRenderer");
            beanRenderer.setPotentialAddresses(checkResult);
            beanRenderer.setMainEditor(element);
            beanRenderer.setAddressModel(addressModel);
            final List<InputElement> editors = new ArrayList<>();
            editors.add(elementLine);
            editors.add(elementCity);
            editors.add(elementState);
            editors.add(elementPostalCode);
            beanRenderer.setEditors(editors);
            ((PopupWidgetHelper)Registry
                    .getApplicationContext().getBean(
                            "popupWidgetHelper")).createPopupWidget(
                                    (TgtcsListboxWidget)parameters
                                            .get("eventSource"),
                                    CONFIRM_UPDATE_ADDRESS_WIDGET_CONFIG,
                                    "csConfirmUpdateAddressWidget-popup",
                                    CSS_CONFIRM_UPDATE_ADDRESS_POPUP,
                                    Labels.getLabel(CONFIRM_UPDATE_ADDRESS_POPUP_TITLE_KEY));
        }

        private void saveAddressWhenNoMatchQAS(final TargetAddressModel addressModel) {
            if ((editor.getRawValue() == null)
                    || (StringUtils.isEmpty(editor
                            .getRawValue().toString()))) {
                TgtcsTextUIEditor.this.setValue(null);
            }
            else {
                TgtcsTextUIEditor.this.setValue(editor
                        .getRawValue());
            }
            TgtcsTextUIEditor.this.fireValueChanged(listener);
            addressModel.setAddressValidated(Boolean.FALSE);
            UISessionUtils.getCurrentSession()
                    .getModelService().save(addressModel);
        }

        private boolean checkMandatoryField() throws InterruptedException {
            final boolean isLine2 = editor.getId().indexOf(TgtcsConstants.QUALIFIER_ADDRESS_LINE2) >= 0 ? true : false;
            if (!isLine2) {
                if (editor.getRawText() == null || editor.getRawText().isEmpty()) {
                    Messagebox.show(Labels.getLabel("required_attribute_missing") + ": "
                            + editor.getAttribute("editorType").toString(),
                            Labels.getLabel(MESSAGE_UNABLE_TO_UPDATE_ADDRESS),
                            Messagebox.OK,
                            Messagebox.ERROR);
                    return false;
                }
            }
            return true;
        }
    }

    @Override
    public HtmlBasedComponent createViewComponent(final Object initialValue,
            final Map<String, ? extends Object> parameters,
            final EditorListener listener) {
        final HtmlBasedComponent container = super.createViewComponent(
                initialValue, parameters, listener);
        if (isEditable()) {
            final InputElement editor = setUpNewEditor(container, initialValue, parameters);
            editor.addEventListener("onBlur", new OnBlurEventListener(editor, listener, parameters, container));
            editor.addEventListener("onEdit", new OnEditEventListener(editor, listener));
        }
        return container;
    }

    private InputElement setUpNewEditor(final HtmlBasedComponent container,
            final Object initialValue,
            final Map<String, ? extends Object> parameters) {
        final InputElement editor = (InputElement)((CancelButtonContainer)container)
                .getContent();
        // set unique id for editor
        final TgtcsListboxWidget listboxWidget = (TgtcsListboxWidget)parameters
                .get("eventSource");
        int currentItemIndex = listboxWidget.getCurrentItemIndex();
        editor.setId((String)parameters.get("attributeQualifier") + "_"
                + currentItemIndex);
        editor.setAttribute("editorType", parameters.get("attributeQualifier"));
        if (((String)parameters.get("attributeQualifier"))
                .equals(TgtcsConstants.QUALIFIER_ADDRESS_POSTALCODE)) {
            listboxWidget.setCurrentItemIndex(++currentItemIndex);
        }
        // remove default listeners: onBlur, onChange, onOk
        Iterator<EventListener> listeners = editor
                .getListenerIterator("onBlur");
        listeners.next();
        listeners.remove();
        listeners = editor.getListenerIterator("onChange");
        listeners.next();
        listeners.remove();
        listeners = editor.getListenerIterator("onOK");
        listeners.next();
        listeners.remove();
        // set Value for ui editor
        setValue(initialValue);
        return editor;
    }
}

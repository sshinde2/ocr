package au.com.target.tgtcs.model.referenceeditor.simple.impl;

import de.hybris.platform.cockpit.model.referenceeditor.simple.impl.DefaultSimpleReferenceUIEditor;


public class TargetSimpleReferenceUIEditor extends DefaultSimpleReferenceUIEditor {

    @Override
    public boolean isEditable()
    {
        return false;
    }

}

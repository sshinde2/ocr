package au.com.target.tgtcs.service.address.impl;

import de.hybris.platform.cockpit.session.UISessionUtils;

import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcs.service.address.TargetAddressService;


public class TargetAddressServiceImpl implements TargetAddressService {

    @Override
    public void setAddressValidated(final TargetAddressModel addressModel, final Boolean validated) {
        addressModel.setAddressValidated(validated);
        UISessionUtils.getCurrentSession().getModelService().save(addressModel);
    }
}

/**
 * 
 */
package au.com.target.tgtcs.service.checkout.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.cscockpit.exceptions.ResourceMessage;
import de.hybris.platform.cscockpit.exceptions.ValidationException;
import de.hybris.platform.cscockpit.services.checkout.impl.DefaultCsCheckoutService;
import de.hybris.platform.cscockpit.utils.SafeUnbox;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtcore.order.TargetCommerceCheckoutService;
import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtcs.facade.TargetCSOrderFacade;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.service.TargetPaymentService;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;



/**
 * Target Checkout Service for the cs cockpit
 * 
 * @author xiaofanlu
 * 
 */
public class TargetCsCheckoutService extends DefaultCsCheckoutService {
    private static final Logger LOG = Logger.getLogger(TargetCsCheckoutService.class);
    private static final String PAYMENT_ERROR_MSG = "payment Capture transaction failure";
    private static final String PAYMENT_ERROR_FAILURE_DUETO_MISSMATCH_TOTALS = "Payment Capture transaction failure due to total amount missmatch";
    private static final String PAYMENT_ERROR_FAILURE_DUETO_NO_PAYMENT_DETAILS = "Payment Capture transaction failure due to no valid payment details";
    private static final String NO_SUFFICIENT_STOCK_MSG = "no sufficient stock:";
    private static final String HAS_PAYMENT_ALREADY = "Cart already has a payment but trying to place order again! Code=";

    private CommerceCartService commerceCartService;
    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;
    private TargetCSOrderFacade orderFacade;

    private TargetPaymentService targetPaymentService;

    private FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    /**
     * {@inheritDoc} Target customized cs cockpit checkout validation
     */
    @Override
    protected void validateCartForCheckout(final CartModel cart) throws ValidationException
    {
        final List errorMessages = new ArrayList();

        if (cart == null)
        {
            errorMessages.add(new ResourceMessage("placeOrder.validation.invalidCart"));
        }
        else
        {
            final DeliveryModeModel deliveryMode = cart.getDeliveryMode();

            if (cart.getEntries().isEmpty())
            {
                errorMessages.add(new ResourceMessage("placeOrder.validation.noItems"));
            }
            else if (SafeUnbox.toDouble(cart.getTotalPrice()) <= 0.0D)
            {
                errorMessages.add(new ResourceMessage("placeOrder.validation.invalidTotal"));
            }

            if ((isDeliveryAddressRequired()) && (cart.getDeliveryAddress() == null))
            {
                errorMessages.add(new ResourceMessage("placeOrder.validation.noShippingAddress"));
            }

            if ((isDeliveryModeRequired()) && (deliveryMode == null))
            {
                errorMessages.add(new ResourceMessage("placeOrder.validation.noDeliveryMode"));
            }

            if ((isPaymentAddressRequired()) && (cart.getPaymentAddress() == null))
            {
                errorMessages.add(new ResourceMessage("placeOrder.validation.noPaymentAddress"));
            }

            if (cart.getPaymentInfo() == null
                    || !cart.getPaymentInfo().getUser().getUid().equals(cart.getUser().getUid()))
            {
                errorMessages.add(new ResourceMessage("placeOrder.validation.needPaymentOption"));
            }

            if (!getOrderFacade().isOrderAvailableForDeliveryMode(cart, deliveryMode))
            {
                errorMessages.add(new ResourceMessage("cscockpit.placeOrder.validation.invalidDeliveryMode"));
            }
        }

        if (errorMessages.isEmpty()) {
            return;
        }
        throw new ValidationException(errorMessages);
    }

    /**
     * {@inheritDoc} customized cs cockpit checkout so that payment is captured before place the order
     */
    @Override
    public OrderModel doCheckout(final CartModel cart)
            throws ValidationException
    {
        validateCartForCheckout(cart);
        //check if the cart has captured value already
        if (hasPaidAmount(cart)) {
            LOG.error("Cart already has a payment but trying to place order again! Code=" + cart.getCode());
            throwValidationException(HAS_PAYMENT_ALREADY);
        }

        final PaymentInfoModel currentPaymentInfo = cart.getPaymentInfo();
        PaymentTransactionModel paymentTransactionModel = null;
        if (currentPaymentInfo instanceof IpgPaymentInfoModel)
        {

            final TargetQueryTransactionDetailsResult queryTransactionDetailsResult = getSessionService().getAttribute(
                    TgtcsConstants.IPG_TRANSACTION_RESULT_KEY);
            if (queryTransactionDetailsResult != null && queryTransactionDetailsResult.isSuccess()) {

                // In case of split payment or single payment with gift card, check whether
                // order total amount matches total amount submitted from iFrame. If they
                // don't match, reverse payments and exit order flow with exception.

                final boolean shouldReversePayments = ((TargetCommerceCheckoutService)getCommerceCheckoutService())
                        .shouldPaymentBeReversed(cart,
                                queryTransactionDetailsResult.getCardResults());

                if (shouldReversePayments) {
                    targetPaymentService.reversePayment(cart,
                            ((TargetCommerceCheckoutService)getCommerceCheckoutService())
                                    .getGiftCards(queryTransactionDetailsResult.getCardResults()));

                    throwValidationException(PAYMENT_ERROR_FAILURE_DUETO_MISSMATCH_TOTALS);
                }

                paymentTransactionModel = targetPaymentService
                        .createTransactionWithQueryResult(cart, queryTransactionDetailsResult);
            }
            else {
                throwValidationException(PAYMENT_ERROR_FAILURE_DUETO_NO_PAYMENT_DETAILS);
            }
        }


        OrderModel order = null;
        try {
            if (!((TargetCommerceCheckoutService)getCommerceCheckoutService()).beforePlaceOrder(cart,
                    paymentTransactionModel)) {
                throwValidationException(PAYMENT_ERROR_MSG);
            }
        }
        catch (final InsufficientStockLevelException ex) {
            if (paymentTransactionModel != null) {
                getModelService().remove(paymentTransactionModel);
            }
            final ResourceMessage message = new ResourceMessage(NO_SUFFICIENT_STOCK_MSG + ex.getMessage());
            final List<ResourceMessage> messages = new ArrayList<>();
            messages.add(message);
            throw new ValidationException(messages, ex);
        }

        try
        {
            //put the payment info back because the beforePlaceOrder will remove it from the cart.
            cart.setPaymentInfo(currentPaymentInfo);
            //check capture order result
            if (!((TargetCommerceCheckoutService)getCommerceCheckoutService()).isCaptureOrderSuccessful(cart)) {
                ((TargetCommerceCheckoutService)getCommerceCheckoutService()).handlePaymentFailure(cart);
                throwValidationException(PAYMENT_ERROR_MSG);
            }

            final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
            checkoutParameter.setCart(cart);
            
            if (BooleanUtils.isTrue((Boolean)getSessionService().getAttribute(TgtcsConstants.IS_B2B_ORDER_ATTR))) {
                checkoutParameter.setSalesApplication(SalesApplication.B2B);
            }
            else {
                checkoutParameter.setSalesApplication(SalesApplication.CALLCENTER);
            }
            CommerceOrderResult orderResult = new CommerceOrderResult();
            orderResult = getCommerceCheckoutService().placeOrder(checkoutParameter);
            order = orderResult.getOrder();
            order.setPaymentInfo(currentPaymentInfo);
            ((TargetCommerceCheckoutService)getCommerceCheckoutService()).afterPlaceOrder(cart, order);

        }
        catch (final InvalidCartException localInvalidCartException)
        {
            LOG.warn("Cart [" + cart + "] is invalid and cannot be used to place an order");
        }
        catch (final Exception ex) {
            // Something else has gone wrong
            LOG.error("Exception when placing order: " + (order != null ? order.getCode() : StringUtils.EMPTY), ex);
        }

        if (order != null)
        {
            getModelService().remove(cart);
        }
        return order;
    }

    private void throwValidationException(final String msg) throws ValidationException {
        final ResourceMessage message = new ResourceMessage(msg);
        final List<ResourceMessage> messages = new ArrayList<>();
        messages.add(message);
        throw new ValidationException(messages);
    }

    /**
     * check if the cart has captured value already.
     * 
     * @param cart
     * @return true if there is any amount captured successfully.
     */
    private boolean hasPaidAmount(final CartModel cart) {
        return findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(cart).doubleValue() > 0d;
    }

    /**
     * @return the commerceCartService
     */
    public CommerceCartService getCommerceCartService() {
        return commerceCartService;
    }

    /**
     * @param commerceCartService
     *            the commerceCartService to set
     */
    @Required
    public void setCommerceCartService(final CommerceCartService commerceCartService) {
        this.commerceCartService = commerceCartService;
    }

    /**
     * @return the targetPurchaseOptionHelper
     */
    public TargetPurchaseOptionHelper getTargetPurchaseOptionHelper() {
        return targetPurchaseOptionHelper;
    }

    /**
     * @param targetPurchaseOptionHelper
     *            the targetPurchaseOptionHelper to set
     */
    @Required
    public void setTargetPurchaseOptionHelper(final TargetPurchaseOptionHelper targetPurchaseOptionHelper) {
        this.targetPurchaseOptionHelper = targetPurchaseOptionHelper;
    }

    /**
     * @return the orderFacade
     */
    public TargetCSOrderFacade getOrderFacade() {
        return orderFacade;
    }

    /**
     * @param orderFacade
     *            the orderFacade to set
     */
    public void setOrderFacade(final TargetCSOrderFacade orderFacade) {
        this.orderFacade = orderFacade;
    }


    /**
     * @param targetPaymentService
     *            the targetPaymentService to set
     */
    @Required
    public void setTargetPaymentService(final TargetPaymentService targetPaymentService) {
        this.targetPaymentService = targetPaymentService;
    }

    /**
     * @return the findOrderTotalPaymentMadeStrategy
     */
    protected FindOrderTotalPaymentMadeStrategy getFindOrderTotalPaymentMadeStrategy() {
        return findOrderTotalPaymentMadeStrategy;
    }

    /**
     * @param findOrderTotalPaymentMadeStrategy
     *            the findOrderTotalPaymentMadeStrategy to set
     */
    @Required
    public void setFindOrderTotalPaymentMadeStrategy(
            final FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy) {
        this.findOrderTotalPaymentMadeStrategy = findOrderTotalPaymentMadeStrategy;
    }



}

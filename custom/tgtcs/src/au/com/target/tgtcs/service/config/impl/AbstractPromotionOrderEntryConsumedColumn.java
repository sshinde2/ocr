package au.com.target.tgtcs.service.config.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cscockpit.model.promotions.wrappers.AbstractItemModelReflectionWrapper;
import de.hybris.platform.cscockpit.model.promotions.wrappers.WrappedPromotionOrderEntryConsumedModel;
import de.hybris.platform.cscockpit.services.config.impl.AbstractSimpleCustomColumnConfiguration;
import de.hybris.platform.cscockpit.services.promotions.CsPromotionService;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;

import java.util.Locale;

import org.zkoss.spring.SpringUtil;


/**
 * 
 * Abstract custom column configuration for {@link PromotionOrderEntryConsumedModel}
 * 
 */
public abstract class AbstractPromotionOrderEntryConsumedColumn extends AbstractSimpleCustomColumnConfiguration {

    private static final String CS_PROMOTION_SERVICE_BEAN = "csPromotionService";
    private CsPromotionService csPromotionService = null;
    private TypeService typeService;

    @Override
    protected Object getItemValue(final ItemModel item, final Locale locale) throws ValueHandlerException {
        if (!(item instanceof PromotionOrderEntryConsumedModel)
                || !(getCsPromotionService().isPromotionServiceAvailable()))
        {
            return null;
        }
        final TypedObject poecTypedObject = getTypeService().wrapItem(item);
        final AbstractItemModelReflectionWrapper wrapper = getCsPromotionService().unwrapPromotionTypedObject(
                poecTypedObject);
        if (!(wrapper instanceof WrappedPromotionOrderEntryConsumedModel))
        {
            return null;
        }

        final WrappedPromotionOrderEntryConsumedModel consumedOrderEntry = (WrappedPromotionOrderEntryConsumedModel)wrapper;
        return getPromotionOrderEntryConsumedValue(consumedOrderEntry);

    }

    public abstract Object getPromotionOrderEntryConsumedValue(
            WrappedPromotionOrderEntryConsumedModel consumedOrderEntry);

    protected CsPromotionService getCsPromotionService()
    {
        if (csPromotionService == null)
        {
            csPromotionService = (CsPromotionService)SpringUtil.getBean(CS_PROMOTION_SERVICE_BEAN,
                    CsPromotionService.class);
        }
        return csPromotionService;
    }

    protected TypeService getTypeService()
    {
        if (typeService == null) {
            typeService = UISessionUtils.getCurrentSession().getTypeService();
        }
        return typeService;
    }

}

package au.com.target.tgtcs.service.config.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cscockpit.services.config.impl.AbstractProductCustomColumn;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Locale;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * @author Dell
 * 
 */
public class ColourProductCustomColumn extends AbstractProductCustomColumn {

    /* (non-Javadoc)
     * @see de.hybris.platform.cscockpit.services.config.impl.AbstractProductCustomColumn#getProductValue(de.hybris.platform.core.model.product.ProductModel, java.util.Locale)
     */
    @Override
    protected String getProductValue(final ProductModel product, final Locale locale) {
        if (product instanceof TargetColourVariantProductModel) {
            return ((TargetColourVariantProductModel)product).getColourName();
        }
        else if (product instanceof TargetSizeVariantProductModel) {
            if (((VariantProductModel)product).getBaseProduct() instanceof TargetColourVariantProductModel) {
                return ((TargetColourVariantProductModel)((VariantProductModel)product).getBaseProduct())
                        .getColourName();
            }
        }
        return "";
    }

}

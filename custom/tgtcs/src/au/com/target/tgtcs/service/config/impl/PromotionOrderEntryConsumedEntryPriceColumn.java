package au.com.target.tgtcs.service.config.impl;

import de.hybris.platform.cscockpit.model.promotions.wrappers.WrappedPromotionOrderEntryConsumedModel;
//CHECKSTYLE:OFF
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;


//CHECKSTYLE:ON


/**
 * Custom column configuration to render the total entry price for {@link PromotionOrderEntryConsumedModel}
 */
public class PromotionOrderEntryConsumedEntryPriceColumn extends AbstractPromotionOrderEntryConsumedColumn {

    @Override
    public Object getPromotionOrderEntryConsumedValue(final WrappedPromotionOrderEntryConsumedModel consumedOrderEntry) {
        return Double.valueOf(consumedOrderEntry.getEntryPrice());
    }

}

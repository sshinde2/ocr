/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtcs.service.config.impl;

import de.hybris.platform.cockpit.services.label.impl.PrincipalModelLabelProvider;
import de.hybris.platform.core.model.security.PrincipalModel;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcs.constants.TgtcsConstants;


/**
 * 
 * The provider to create logic for target customer name field.
 * 
 */
public class TargetPrincipalLabelProvider extends PrincipalModelLabelProvider {

    /* 
     * Get item label
     * Return customer uid or customer name if item is customer
     * Return customer full name if item is TargetCustomer
     * 
     * @see de.hybris.platform.cockpit.services.label.impl.PrincipalModelLabelProvider#getItemLabel(de.hybris.platform.core.model.security.PrincipalModel) //NOPMD
     * 
     */
    @Override
    protected String getItemLabel(final PrincipalModel item) {

        if (item != null) {
            String label = null;
            if (item instanceof TargetCustomerModel) {
                final TargetCustomerModel customer = (TargetCustomerModel)item;
                label = customer.getFirstname() + TgtcsConstants.NAME_SEPARATOR + customer.getLastname();
            }
            else {
                label = item.getName();
            }
            return (StringUtils.isBlank(label) ? item.getUid() : label);
        }
        return null;
    }

}

/**
 *
 */
package au.com.target.tgtcs.service.fraud;

import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.model.order.OrderModel;

/**
 * @author mjanarth
 *
 */
public interface TargetCsFraudService {
    /**
     * Add the fraud report to the current order
     * @param orderModel
     * @param fraudStatus
     * @param note
     */
    void addFraudReportToOrder(final OrderModel orderModel, final FraudStatus fraudStatus, final String note);
}

/**
 *
 */
package au.com.target.tgtcs.service.fraud.impl;

import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.fraud.model.FraudReportModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.user.UserService;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcs.service.fraud.TargetCsFraudService;
import au.com.target.tgtfraud.strategy.TargetFraudReportCreationStrategy;

/**
 * @author mjanarth
 *
 */
public class TargetCsFraudServiceImpl  extends AbstractBusinessService implements TargetCsFraudService {


    private UserService userService;
    private TargetFraudReportCreationStrategy targetFraudReportCreationStrategy;

    @Override
    public void addFraudReportToOrder(final OrderModel orderModel, final FraudStatus fraudStatus, final String note) {
        Assert.notNull(orderModel, "orderModel may not be null");
        Assert.notNull(fraudStatus, "fraudStatus may not be null");
        final UserModel currentUser = userService.getCurrentUser();
        final FraudServiceResponse response = new FraudServiceResponse(note, currentUser.getUid());
        final FraudReportModel fraudReport = targetFraudReportCreationStrategy.createFraudReport(response, orderModel, fraudStatus);
        getModelService().save(fraudReport);
        getModelService().refresh(orderModel);
    }

    /**
     * @param userService the userService to set
     */
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @param targetFraudReportCreationStrategy the targetFraudReportCreationStrategy to set
     */
    @Required
    public void setTargetFraudReportCreationStrategy(final TargetFraudReportCreationStrategy targetFraudReportCreationStrategy) {
        this.targetFraudReportCreationStrategy = targetFraudReportCreationStrategy;
    }

}

package au.com.target.tgtcs.service.meta.impl;

import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.services.meta.impl.DefaultPropertyService;

import au.com.target.tgtcs.constants.TgtcsConstants;


public class TargetPropertyService extends DefaultPropertyService {

    @Override
    public boolean isMandatory(final PropertyDescriptor propertyDescriptor, final boolean creationMode) {
        if (propertyDescriptor.getQualifier().equals(TgtcsConstants.QUALIFIER_ADDRESS_FIRSTNAME)
                || propertyDescriptor.getQualifier().equals(TgtcsConstants.QUALIFIER_ADDRESS_LASTNAME)
                || propertyDescriptor.getQualifier().equals(TgtcsConstants.QUALIFIER_ADDRESS_TOWN)
                || propertyDescriptor.getQualifier().equals(TgtcsConstants.QUALIFIER_ADDRESS_DISTRICT)
                || propertyDescriptor.getQualifier().equals(TgtcsConstants.QUALIFIER_ADDRESS_POSTALCODE)
                || propertyDescriptor.getQualifier().equals(TgtcsConstants.QUALIFIER_ADDRESS_LINE1)
                || propertyDescriptor.getQualifier().equals(TgtcsConstants.QUALIFIIER_TARGET_CUSTOMER_PHONENUMBER)
                || propertyDescriptor.getQualifier().equals(TgtcsConstants.QUALIFIER_CUSTOMER_TITLE)
                || propertyDescriptor.getQualifier().equals(TgtcsConstants.QUALIFIER_ADDRESS_PHONENUMBER1)) {
            return true;
        }
        else {
            return super.isMandatory(propertyDescriptor, creationMode);
        }
    }
}

package au.com.target.tgtcs.service.payment.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.cscockpit.exceptions.ResourceMessage;
import de.hybris.platform.cscockpit.services.payment.impl.DefaultCsCardPaymentService;
import de.hybris.platform.payment.dto.CardInfo;

import java.util.List;

import org.apache.commons.lang.StringUtils;


public class TargetCsCardPaymentService extends DefaultCsCardPaymentService {
    @Override
    protected void validateBillingInfo(final AbstractOrderModel order, final CardInfo cardInfo,
            final List<ResourceMessage> errorMessages)
    {
        if (StringUtils.isBlank(cardInfo.getBillingInfo().getFirstName()))
        {
            errorMessages.add(new ResourceMessage("paymentOption.billingInfo.validation.noFirstName"));
        }
        if (StringUtils.isBlank(cardInfo.getBillingInfo().getLastName()))
        {
            errorMessages.add(new ResourceMessage("paymentOption.billingInfo.validation.noLastName"));
        }
        if (StringUtils.isBlank(cardInfo.getBillingInfo().getStreet1()))
        {
            errorMessages.add(new ResourceMessage("paymentOption.billingInfo.validation.noStreet1"));
        }
        if (StringUtils.isBlank(cardInfo.getBillingInfo().getCity()))
        {
            errorMessages.add(new ResourceMessage("paymentOption.billingInfo.validation.noCity"));
        }
        if (StringUtils.isBlank(cardInfo.getBillingInfo().getCountry()))
        {
            errorMessages.add(new ResourceMessage("paymentOption.billingInfo.validation.noCountry"));
        }
        if (StringUtils.isBlank(cardInfo.getBillingInfo().getState()))
        {
            errorMessages.add(new ResourceMessage("paymentOption.billingInfo.validation.noState"));
        }
        if (StringUtils.isBlank(cardInfo.getBillingInfo().getPostalCode()))
        {
            errorMessages.add(new ResourceMessage("paymentOption.billingInfo.validation.noPostalCode"));
        }
    }

}

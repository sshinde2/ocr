/**
 * 
 */
package au.com.target.tgtcs.services.config.impl;

import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.services.config.impl.AbstractSimpleCustomColumnConfiguration;

import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationContext;

import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;


/**
 * @author bhuang3
 * 
 */
public class TgtCsOrderDeliveryAddressStoreInfoColumn extends AbstractSimpleCustomColumnConfiguration {

    private static final String TARGET_STORE = "Target Store";
    private static final String DASH = " - ";

    @Override
    protected Object getItemValue(final ItemModel item, final Locale locale) throws ValueHandlerException {
        if (item instanceof OrderModel) {
            final Integer storeNumber = ((OrderModel)item).getCncStoreNumber();
            if (storeNumber != null) {
                final TargetStoreLocatorFacade targetStoreLocatorFacade = (TargetStoreLocatorFacade)getCtx()
                        .getBean("targetStoreLocatorFacade");
                final TargetPointOfServiceData selectedStore = targetStoreLocatorFacade.getPointOfService(storeNumber);
                return TARGET_STORE + DASH + selectedStore.getName() + DASH + storeNumber.toString();
            }
        }
        return StringUtils.EMPTY;
    }

    /**
     * @return the ctx
     */
    public ApplicationContext getCtx() {
        return de.hybris.platform.core.Registry.getApplicationContext();
    }




}

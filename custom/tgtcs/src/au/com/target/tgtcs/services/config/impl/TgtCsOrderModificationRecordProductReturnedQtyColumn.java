/**
 * 
 */
package au.com.target.tgtcs.services.config.impl;

import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cscockpit.services.config.impl.AbstractSimpleCustomColumnConfiguration;
import de.hybris.platform.returns.model.OrderEntryReturnRecordEntryModel;

import java.util.Locale;

import org.apache.commons.lang.StringUtils;


/**
 * @author umesh
 * 
 */
public class TgtCsOrderModificationRecordProductReturnedQtyColumn extends AbstractSimpleCustomColumnConfiguration {


    @Override
    protected Object getItemValue(final ItemModel itemmodel, final Locale locale) throws ValueHandlerException {
        if (itemmodel instanceof OrderEntryReturnRecordEntryModel)
        {
            final OrderEntryReturnRecordEntryModel returnEntry = (OrderEntryReturnRecordEntryModel)itemmodel;
            return returnEntry.getReturnedQuantity().toString();
        }
        return StringUtils.EMPTY;
    }
}

/**
 *
 */
package au.com.target.tgtcs.services.config.impl;

import de.hybris.platform.cockpit.model.listview.CellRenderer;
import de.hybris.platform.cscockpit.services.config.impl.ProductThumbnailColumn;

import au.com.target.tgtcs.model.cellrenderers.impl.TgtCsImageCellRenderer;


/**
 * @author Pradeep
 *
 */
public class TgtCsProductThumbnailColumn extends ProductThumbnailColumn {

    @Override
    public CellRenderer getCellRenderer()
    {
        return new TgtCsImageCellRenderer();
    }

}

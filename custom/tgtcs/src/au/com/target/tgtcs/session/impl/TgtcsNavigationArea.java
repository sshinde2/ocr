/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtcs.session.impl;

import de.hybris.platform.cockpit.components.sectionpanel.SectionPanelModel;
import de.hybris.platform.cockpit.session.impl.BaseUICockpitNavigationArea;

import au.com.target.tgtcs.components.navigationarea.TgtcsNavigationAreaModel;


/**
 * Tgtcs navigation area.
 */
public class TgtcsNavigationArea extends BaseUICockpitNavigationArea
{
    @Override
    public SectionPanelModel getSectionModel()
    {
        if (super.getSectionModel() == null)
        {
            final TgtcsNavigationAreaModel model = new TgtcsNavigationAreaModel(this);
            model.initialize();
            super.setSectionModel(model);
        }
        return super.getSectionModel();
    }
}

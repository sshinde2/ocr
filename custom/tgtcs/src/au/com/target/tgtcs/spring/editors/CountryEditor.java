/**
 * 
 */
package au.com.target.tgtcs.spring.editors;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.i18n.daos.CountryDao;

import java.beans.PropertyEditorSupport;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;


/**
 * @author Dell
 * 
 */
public class CountryEditor extends PropertyEditorSupport {

    private CountryDao countryDao;

    public CountryDao getCountryDao() {
        return countryDao;
    }

    @Required
    public void setCountryDao(final CountryDao countryDao) {
        this.countryDao = countryDao;
    }


    @Override
    public void setAsText(final String text) {
        final List<CountryModel> countries = countryDao.findCountriesByCode(text);
        if (!countries.isEmpty()) {
            setValue(countries.get(0));
        }
    }
}

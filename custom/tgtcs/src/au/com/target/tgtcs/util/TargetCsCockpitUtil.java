package au.com.target.tgtcs.util;

import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.util.TypeTools;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.springframework.util.StringUtils;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;

import au.com.target.tgtcore.refund.TargetOrderRefundException;
import au.com.target.tgtcs.constants.TgtcsConstants;


public final class TargetCsCockpitUtil {
    private static TypeService cockpitTypeService;
    private static ModelService modelService;

    /**
     * Private constructor - to prevent accidental invocation.
     */
    private TargetCsCockpitUtil() {
        //prevent construction
    }

    /**
     * @param cockpitTypeService
     *            the cockpitTypeService to set
     */
    public static void setCockpitTypeService(final TypeService cockpitTypeService) {
        TargetCsCockpitUtil.cockpitTypeService = cockpitTypeService;
    }


    /**
     * @param modelService
     *            the modelService to set
     */
    public static void setModelService(final ModelService modelService) {
        TargetCsCockpitUtil.modelService = modelService;
    }


    public static boolean checkMandatoryFields(final ObjectValueContainer container,
            final String configurationTypeCode,
            final StringBuilder msgBuilder) {
        boolean result = true;
        final Set<PropertyDescriptor> omitteds = TypeTools.getOmittedProperties(container, cockpitTypeService
                .getObjectTemplate(configurationTypeCode), true);
        if (!omitteds.isEmpty()) {
            final ItemModel newItemModel = (ItemModel)modelService.create(
                    cockpitTypeService.getObjectTemplate(configurationTypeCode).getBaseType().getCode());
            for (final PropertyDescriptor propertyDescriptor : omitteds) {
                if (!propertyDescriptor.getQualifier().equals(TgtcsConstants.QUALIFIIER_ITEM_OWNER)) {
                    final String attrCode = cockpitTypeService.getAttributeCodeFromPropertyQualifier(
                            propertyDescriptor.getQualifier());
                    if ((attrCode != null)
                            && (modelService.getAttributeValue(newItemModel, attrCode) == null)) {
                        msgBuilder.append(Labels.getLabel("required_attribute_missing")).append(": '")
                                .append(propertyDescriptor.getQualifier())
                                .append("' \n");
                        if (result) {
                            result = false;
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * A utility method to add all <code>children</code> {@link Component}s to specified <code>parent</code>
     * {@link Component}.
     * 
     * @param parent
     *            the parent {@link Component} to add <code>children</code> to
     * @param children
     *            the {@List} of child {@link Component}s to add to the <code>parent</code>
     */
    public static void addAllChildren(final Component parent, final List<? extends Component> children)
    {
        for (final Component child : children)
        {
            parent.appendChild(child);
        }
    }

    /**
     * A utility method to add all <code>children</code> {@link Component}s to specified <code>parent</code>
     * {@link Component}.
     * 
     * @param parent
     *            the parent {@link Component} to add
     * @param children
     *            the array of child {@link Component}s to add to the <code>parent</code>
     */
    public static void addAllChildren(final Component parent, final Component... children)
    {
        addAllChildren(parent, Arrays.asList(children));
    }

    /**
     * A utility method to detach all child components from specified <code>parent</code> {@link Component}.
     * 
     * @param parent
     *            the {@link Component} to detach children from.
     */
    public static void removeChildren(final Component parent)
    {
        parent.getChildren().clear();
    }

    /**
     * Validate the manual refund input
     * 
     * @param amount
     * @param receiptNo
     */
    public static void validateManualRefundInput(final Double amount, final String receiptNo) {
        if (amount == null || amount.compareTo(Double.valueOf(0d)) <= 0) {
            throw new TargetOrderRefundException("input refund amount is not valid");
        }
        if (StringUtils.isEmpty(receiptNo)) {
            throw new TargetOrderRefundException("input receipt No cannot be empty");
        }
        if (receiptNo.length() > 17) {
            throw new TargetOrderRefundException("The length of the receipt no cannot excceed 17 characters");
        }
    }

}

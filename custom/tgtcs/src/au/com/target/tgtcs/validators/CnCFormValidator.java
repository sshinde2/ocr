/**
 * 
 */
package au.com.target.tgtcs.validators;


import static au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel.FIRSTNAME_ITEM_ID;
import static au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel.LASTNAME_ITEM_ID;
import static au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel.PHONE_ITEM_ID;
import static au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel.STATE_ITEM_ID;
import static au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel.STORE_ITEM_ID;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import au.com.target.tgtcs.dto.CnCDetailsFormData;
import au.com.target.tgtutility.util.TargetValidationCommon.Phone;


/**
 * @author Pavel_Yakushevich
 * 
 */
public class CnCFormValidator implements Validator {

    private static final String ERROR_KEY_FORMAT =
            "tgtcscockpit.widget.checkout.clickAndCollectStore.validation.%s.empty";

    private static final String PHONE_LENGTH_ERROR_KEY =
            "tgtcscockpit.widget.checkout.clickAndCollectStore.validation.phone.size";

    @Override
    public boolean supports(final Class<?> class_) {
        return class_ != null && CnCDetailsFormData.class.isAssignableFrom(class_);
    }

    @Override
    public void validate(final Object object, final Errors errors) {
        if (object != null && supports(object.getClass())) {
            final CnCDetailsFormData formData = (CnCDetailsFormData)object;

            // mandatory fields : first name, last name, phone, state, store
            if (StringUtils.isEmpty(formData.getFirstName())) {
                errors.rejectValue(FIRSTNAME_ITEM_ID, String.format(ERROR_KEY_FORMAT, FIRSTNAME_ITEM_ID));
            }

            if (StringUtils.isEmpty(formData.getLastName())) {
                errors.rejectValue(LASTNAME_ITEM_ID, String.format(ERROR_KEY_FORMAT, LASTNAME_ITEM_ID));
            }

            if (StringUtils.isEmpty(formData.getState())) {
                errors.rejectValue(STATE_ITEM_ID, String.format(ERROR_KEY_FORMAT, STATE_ITEM_ID));
            }

            if (formData.getStore() == null) {
                errors.rejectValue(STORE_ITEM_ID, String.format(ERROR_KEY_FORMAT, STORE_ITEM_ID));
            }
            if (formData.getPhone() == null) {
                errors.rejectValue(PHONE_ITEM_ID, String.format(ERROR_KEY_FORMAT, PHONE_ITEM_ID));
            }
            else {
                validatePhone(formData.getPhone(), errors);
            }
        }
    }

    private void validatePhone(final String phone, final Errors errors) {
        final String sanitizedPhone = Phone.CLEAN_REGEX.matcher(phone).replaceAll(StringUtils.EMPTY);

        if (StringUtils.isEmpty(sanitizedPhone)) {
            errors.rejectValue(PHONE_ITEM_ID, String.format(ERROR_KEY_FORMAT, PHONE_ITEM_ID));
            return;
        }

        if (sanitizedPhone.length() < Phone.MIN_SIZE || sanitizedPhone.length() > Phone.MAX_SIZE) {
            errors.rejectValue(PHONE_ITEM_ID, String.format(PHONE_LENGTH_ERROR_KEY));
        }
    }
}

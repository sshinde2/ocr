/**
 * 
 */
package au.com.target.tgtcs.widgets;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.renderers.details.impl.OrderDetailsListRenderer;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import java.util.Date;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zul.Div;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;


/**
 * @author rmcalave
 * 
 */
public class TargetOrderDetailsListRenderer extends OrderDetailsListRenderer {

    private TargetConsignmentService targetConsignmentService;

    /* (non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.renderers.details.impl.OrderDetailsListRenderer#populateKnownDetails(java.lang.Object, de.hybris.platform.cockpit.model.meta.TypedObject, de.hybris.platform.cockpit.widgets.Widget, org.zkoss.zul.Div)
     */
    @Override
    protected void populateKnownDetails(final Object context, final TypedObject item, final Widget widget,
            final Div detailContainer) {
        super.populateKnownDetails(context, item, widget, detailContainer);

        if (item != null && item.getObject() instanceof OrderModel) {
            final OrderModel orderModel = (OrderModel)item.getObject();
            //added TMD card number
            renderRow(createTitleLabel(LabelUtils.getLabel(widget, "teamMemberDiscountNumber")),
                    createValueLabel(orderModel.getTmdCardNumber() != null ? orderModel.getTmdCardNumber() : ""),
                    detailContainer);

            final boolean shipsterMember = orderModel.getAusPostClubMember() != null
                    ? orderModel.getAusPostClubMember().booleanValue() : false;

            renderRow(createTitleLabel(LabelUtils.getLabel(widget, "shipsterClubMember")),
                    createValueLabel(shipsterMember ? "Yes" : "No"),
                    detailContainer);

            if (shipsterMember) {
                final boolean shipsterFreeDelivery = orderModel.getAusPostDeliveryClubFreeDelivery() != null
                        ? orderModel.getAusPostDeliveryClubFreeDelivery().booleanValue() : false;

                renderRow(createTitleLabel(LabelUtils.getLabel(widget, "shipsterFreeDelivery")),
                        createValueLabel(shipsterFreeDelivery ? "Applied" : "Not Applied"),
                        detailContainer);
            }

            if (orderModel.getDeliveryMode() instanceof TargetZoneDeliveryModeModel) {
                final TargetZoneDeliveryModeModel orderDeliveryMode = (TargetZoneDeliveryModeModel)orderModel
                        .getDeliveryMode();

                if (BooleanUtils.isTrue(orderDeliveryMode.getIsDeliveryToStore())) {
                    final Date cncLastNotification = orderModel.getCncLastNotification();
                    final TargetConsignmentModel consignmentModel = targetConsignmentService
                            .getActiveDeliverToStoreConsignmentForOrder(orderModel);

                    Date cncReadyForPickupDate = null;
                    Date cncPickedupDate = null;
                    Date cncReturnedToFloorDate = null;
                    if (consignmentModel != null) {
                        cncReadyForPickupDate = consignmentModel.getReadyForPickUpDate();
                        cncPickedupDate = consignmentModel.getPickedUpDate();
                        cncReturnedToFloorDate = consignmentModel.getReturnedToFloorDate();
                    }
                    renderRow(
                            createTitleLabel(LabelUtils.getLabel(widget, "cncLastNotification")),
                            createValueLabel(cncLastNotification != null ? LabelUtils.getLabel(widget,
                                    "cncNotificationDateFormat", cncLastNotification) : LabelUtils.getLabel(widget,
                                            "cncNotification.notSent")),
                            detailContainer);

                    renderRow(
                            createTitleLabel(LabelUtils.getLabel(widget, "cncReadyForPickupDate")),
                            createValueLabel(cncReadyForPickupDate != null ? LabelUtils.getLabel(widget,
                                    "cncNotificationDateFormat", cncReadyForPickupDate) : LabelUtils.getLabel(widget,
                                            "cncNotification.notSent")),
                            detailContainer);

                    renderRow(
                            createTitleLabel(LabelUtils.getLabel(widget, "cncPickedupDate")),
                            createValueLabel(cncPickedupDate != null ? LabelUtils.getLabel(widget,
                                    "cncNotificationDateFormat", cncPickedupDate) : LabelUtils.getLabel(widget,
                                            "cncNotification.notSent")),
                            detailContainer);

                    renderRow(
                            createTitleLabel(LabelUtils.getLabel(widget, "cncReturnedToFloorDate")),
                            createValueLabel(cncReturnedToFloorDate != null ? LabelUtils.getLabel(widget,
                                    "cncNotificationDateFormat", cncReturnedToFloorDate) : LabelUtils.getLabel(widget,
                                            "cncNotification.notSent")),
                            detailContainer);
                }
            }
        }
    }

    /**
     * @param targetConsignmentService
     *            the targetConsignmentService to set
     */
    @Required
    public void setTargetConsignmentService(final TargetConsignmentService targetConsignmentService) {
        this.targetConsignmentService = targetConsignmentService;
    }
}

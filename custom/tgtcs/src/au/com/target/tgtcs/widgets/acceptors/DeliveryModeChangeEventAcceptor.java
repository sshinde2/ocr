/**
 * 
 */
package au.com.target.tgtcs.widgets.acceptors;

import static org.zkoss.zul.Messagebox.CANCEL;
import static org.zkoss.zul.Messagebox.NONE;
import static org.zkoss.zul.Messagebox.OK;

import de.hybris.platform.cockpit.events.CockpitEvent;
import de.hybris.platform.cockpit.events.CockpitEventAcceptor;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cockpit.widgets.impl.DefaultWidget;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.cscockpit.utils.TypeUtils;
import de.hybris.platform.cscockpit.widgets.renderers.utils.PopupWidgetHelper;

import org.apache.log4j.Logger;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcs.facade.TargetCSOrderFacade;
import au.com.target.tgtcs.widgets.controllers.TargetBasketController;
import au.com.target.tgtcs.widgets.controllers.TargetCheckoutController;
import au.com.target.tgtcs.widgets.controllers.impl.TargetCnCStoreSelectController;
import au.com.target.tgtcs.widgets.events.DeliveryModeChangeEvent;
import au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel;



/**
 * @author Pavel_Yakushevich
 * 
 */
public class DeliveryModeChangeEventAcceptor implements CockpitEventAcceptor {
    public static final String BEAN_ID = "targetDeliveryModeChangeEventAcceptor";

    private static final Logger LOG = Logger.getLogger(DeliveryModeChangeEventAcceptor.class);


    private static final String DELIVERY_MODE_INVALID_TITLE_KEY = "tgtcscockpit.widget.checkout.shippingAddress.error.invalidDeliveryMode.title";

    private static final String DELIVERY_MODE_INVALID_MESSAGE_KEY = "tgtcscockpit.widget.checkout.shippingAddress.error.invalidDeliveryMode.message";

    private TargetCSOrderFacade orderFacade;

    private TargetBasketController controller;

    private PopupWidgetHelper popupWidgetHelper;

    private TargetDeliveryModeHelper deliveryModeHelper;

    private Widget eventSource;

    private Widget<SelectCnCStoreWidgetModel, TargetCnCStoreSelectController> cncStoreDetailsWidget;


    @Override
    public void onCockpitEvent(final CockpitEvent event) {
        if (event instanceof DeliveryModeChangeEvent) {
            final DeliveryModeChangeEvent deliveryChangeEvent = (DeliveryModeChangeEvent)event;

            CartModel cartModel = null;
            DeliveryModeModel deliveryModeModel = null;
            String purchaseCode = null;

            try {
                cartModel = TypeUtils.unwrapItem(deliveryChangeEvent.getOrder(), CartModel.class);
                deliveryModeModel = TypeUtils.unwrapItem(deliveryChangeEvent.getNewDeliveryMode(),
                        DeliveryModeModel.class);
                eventSource = (Widget)event.getSource();
                purchaseCode = ((TargetBasketController)eventSource.getWidgetController())
                        .getPurchaseOptionModel().getCode().toLowerCase();
            }
            catch (final IllegalArgumentException e) {
                // typed objects carry not the models we were expecting.
                // do nothing
                LOG.error("Unexpected wrapped item in TypedObject", e);
                return;
            }

            final boolean isStoreDeliveryMode = getDeliveryModeHelper().isDeliveryModeStoreDelivery(deliveryModeModel);

            if (getOrderFacade().isOrderAvailableForDeliveryMode(cartModel, deliveryModeModel) && isStoreDeliveryMode) {
                // all is OK, show the 'select CnC details' popup
                createAndShowCnCDetailsPopup(purchaseCode);
                getCncStoreDetailsWidget().getWidgetModel().setSelectedDeliveryMode(deliveryModeModel);
            }
            else if (isStoreDeliveryMode) {
                // show error popup
                int buttonPressed = -1;
                try {
                    buttonPressed = Messagebox
                            .show(Labels
                                    .getLabel(DELIVERY_MODE_INVALID_MESSAGE_KEY),
                                    Labels.getLabel(DELIVERY_MODE_INVALID_TITLE_KEY),
                                    OK | CANCEL, NONE);
                }
                catch (final InterruptedException e) {
                    LOG.error("Was interrupted displaying error popup", e);
                    Thread.currentThread().interrupt();
                    return;
                }

                switch (buttonPressed) {
                    case CANCEL:
                        // user decided to change delivery mode anyway - show the 'select CnC details' popup
                        createAndShowCnCDetailsPopup(purchaseCode);
                        getCncStoreDetailsWidget().getWidgetModel().setSelectedDeliveryMode(deliveryModeModel);

                        return;
                    case OK:
                        // user decided not to change delivery mode after all
                        updateWidgetView(getDeliveryAddressWidget());
                        break;
                    case -1:
                    default:
                        // unexpected answer. log it and do nothing.
                        LOG.warn(String.format("Popup returned code %d, expected one of [%d,%d]",
                                Integer.valueOf(buttonPressed), Integer.valueOf(OK),
                                Integer.valueOf(CANCEL)));
                        updateWidgetView(getDeliveryAddressWidget());
                        break;
                }
            }
            else {
                // change the delivery mode
                getOrderFacade()
                        .changeDeliveryMode(
                                ((TargetCheckoutController)getController().getCheckoutController())
                                        .getCheckoutCartByMasterCart(),
                                deliveryModeModel);

                updateWidgetView(getDeliveryAddressWidget());
                getController().dispatchEvent(null, event.getSource(), null);
            }

        }
    }

    /**
     * Creates and shows popup that asks user for Click and Collect details.
     * 
     */
    protected void createAndShowCnCDetailsPopup(final String purchaseCode) {
        final String widgetCode = "selectClickAndCollectStoreWidgetConfig" + purchaseCode + "-Popup";

        final Window window = getPopupWidgetHelper().createPopupWidget(getDeliveryAddressWidget().getRoot(),
                "selectClickAndCollectStoreWidgetConfig" + purchaseCode,
                widgetCode, "selectClickAndCollectStorePopup",
                Labels.getLabel("tgtcscockpit.widget.checkout.clickAndCollectStore"),
                new EventListener() {
                    @Override
                    public void onEvent(final Event event) throws Exception {
                        updateWidgetView(getDeliveryAddressWidget());
                    }
                });

        cncStoreDetailsWidget = findWidgetForCode(widgetCode, window);
    }

    private Widget findWidgetForCode(final String code, final Component parent) {
        for (final Object child : parent.getChildren()) {
            if (child instanceof Widget && code.equals(((Widget)child).getWidgetCode())) {
                return (Widget)child;
            }
            else {
                return findWidgetForCode(code, (Component)child);
            }
        }
        return null;
    }


    private void updateWidgetView(final Widget widget) {
        if (getDeliveryAddressWidget() instanceof DefaultWidget) {
            getDeliveryAddressWidget().getContent().getChildren().clear();
            ((DefaultWidget)widget).update();
        }
    }

    /**
     * 
     * @return the deliveryAddressWidget
     */
    public Widget getDeliveryAddressWidget() {
        return eventSource;
    }

    /**
     * 
     * @return the PopupWidgetHelper
     */
    protected PopupWidgetHelper getPopupWidgetHelper() {
        return this.popupWidgetHelper;
    }

    /**
     * 
     * @param popupWidgetHelper
     *            the PopupWidgetHelper to set
     */
    public void setPopupWidgetHelper(final PopupWidgetHelper popupWidgetHelper) {
        this.popupWidgetHelper = popupWidgetHelper;
    }

    /**
     * @return the controller
     */
    public TargetBasketController getController() {
        return controller;
    }

    /**
     * @param controller
     *            the controller to set
     */
    public void setController(final TargetBasketController controller) {
        this.controller = controller;
    }


    /**
     * @return the orderFacade
     */
    public TargetCSOrderFacade getOrderFacade() {
        return orderFacade;
    }

    /**
     * @param orderFacade
     *            the orderFacade to set
     */
    public void setOrderFacade(final TargetCSOrderFacade orderFacade) {
        this.orderFacade = orderFacade;
    }

    /**
     * @return the deliveryModeHelper
     */
    public TargetDeliveryModeHelper getDeliveryModeHelper() {
        return deliveryModeHelper;
    }

    /**
     * @param deliveryModeHelper
     *            the deliveryModeHelper to set
     */
    public void setDeliveryModeHelper(final TargetDeliveryModeHelper deliveryModeHelper) {
        this.deliveryModeHelper = deliveryModeHelper;
    }

    /**
     * @return the cncStoreDetailsWidget
     */
    protected Widget<SelectCnCStoreWidgetModel, TargetCnCStoreSelectController> getCncStoreDetailsWidget() {
        return cncStoreDetailsWidget;
    }
}

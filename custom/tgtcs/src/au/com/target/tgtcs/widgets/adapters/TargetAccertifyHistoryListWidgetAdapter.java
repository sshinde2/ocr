/**
 * 
 */
package au.com.target.tgtcs.widgets.adapters;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.widgets.adapters.AbstractInitialisingWidgetAdapter;
import de.hybris.platform.cscockpit.widgets.controllers.OrderController;
import de.hybris.platform.fraud.model.FraudReportModel;

import java.util.List;
import java.util.Set;

import au.com.target.tgtcs.widgets.models.impl.TargetAccertifyReportListWidgetModel;


/**
 * @author umesh
 * 
 */
public class TargetAccertifyHistoryListWidgetAdapter extends AbstractInitialisingWidgetAdapter {

    @Override
    protected boolean updateModel() {
        boolean changed = false;
        if (getWidgetController() instanceof OrderController) {
            final OrderController orderController = ((OrderController)getWidgetController());
            final TypedObject orderObject = orderController.getCurrentOrder();
            if (orderObject != null) {
                final Object obj = orderObject.getObject();
                if (obj != null) {
                    final OrderModel orderModel = (OrderModel)obj;
                    final Set<FraudReportModel> frauds = orderModel.getFraudReports();
                    final List fraudList = getCockpitTypeService().wrapItems(frauds);
                    changed |= ((TargetAccertifyReportListWidgetModel)getWidgetModel()).setItems(fraudList);
                }
            }
        }
        return changed;
    }
}

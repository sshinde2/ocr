package au.com.target.tgtcs.widgets.adapters;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.cscockpit.widgets.adapters.CartAddressAdapter;

import java.util.Collection;
import java.util.List;

import au.com.target.tgtcs.widgets.controllers.TargetBasketController;
import au.com.target.tgtcs.widgets.controllers.TargetCheckoutController;


public abstract class TargetCartAddressAdapter extends CartAddressAdapter {
    @Override
    protected boolean updateModel()
    {
        boolean changed = false;

        final CartModel cartModel = getCartModel();
        final Collection<AddressModel> addresses = getAvailableAddressesForCart(cartModel);
        final List<TypedObject> addressObjects = getCockpitTypeService().wrapItems(addresses);
        changed |= getWidgetModel().setItems(addressObjects);

        changed |= getWidgetModel().setCart(getCockpitTypeService().wrapItem(cartModel));

        return changed;
    }

    /**
     * @return checkout cart model
     */
    protected CartModel getCartModel() {
        final CartModel cartModel = ((TargetCheckoutController)((TargetBasketController)getWidgetController())
                .getCheckoutController()).getCheckoutCartByMasterCart();
        return cartModel;
    }


}

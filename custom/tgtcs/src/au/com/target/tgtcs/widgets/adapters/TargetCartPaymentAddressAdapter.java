package au.com.target.tgtcs.widgets.adapters;

import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Required;


public class TargetCartPaymentAddressAdapter extends TargetCartAddressAdapter {
    private CustomerAccountService customerAccountService;

    protected CustomerAccountService getCustomerAccountService()
    {
        return customerAccountService;
    }

    /**
     * @param customerAccountService
     */
    @Required
    public void setCustomerAccountService(final CustomerAccountService customerAccountService)
    {
        this.customerAccountService = customerAccountService;
    }

    @Override
    protected Collection<AddressModel> getAvailableAddressesForCart(final CartModel cart)
    {
        return getCustomerAccountService().getAddressBookEntries((CustomerModel)cart.getUser());
    }
}

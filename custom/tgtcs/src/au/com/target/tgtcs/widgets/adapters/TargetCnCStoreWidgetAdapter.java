/**
 * 
 */
package au.com.target.tgtcs.widgets.adapters;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.cscockpit.widgets.adapters.AbstractInitialisingWidgetAdapter;

import au.com.target.tgtcs.widgets.controllers.impl.TargetCnCStoreSelectController;
import au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel;


/**
 * @author Pavel_Yakushevich
 * 
 */
public class TargetCnCStoreWidgetAdapter extends
        AbstractInitialisingWidgetAdapter<SelectCnCStoreWidgetModel, TargetCnCStoreSelectController> {

    @Override
    protected boolean updateModel() {
        boolean changed = false;

        changed |= getWidgetModel().setCart(getWidgetController().getBasketController().getCart());

        initialize();

        return changed;
    }

    /**
     * Performs initialization of widget model with new customer values
     */
    protected void initialize() {
        final TargetCnCStoreSelectController controller = getWidgetController();
        final AddressModel address = controller.getBasketController().getMasterCart().getDeliveryAddress();
        final SelectCnCStoreWidgetModel widgetModel = getWidgetModel();
        if (address != null) {
            widgetModel.setFirstName(address.getFirstname());
            widgetModel.setLastName(address.getLastname());
            widgetModel.setPhone(address.getPhone1());
            widgetModel.setState(address.getDistrict());
            widgetModel.setTitle(address.getTitle());
        }

        controller.initialize();
    }
}

package au.com.target.tgtcs.widgets.binding;

import de.hybris.platform.cockpit.widgets.models.WidgetModel;


public abstract class AbstractTgtcsBinder<M extends WidgetModel> {

    private final M widgetModel;

    /**
     * Constructor
     * 
     * @param model
     *            the WidgetModel to keep components in sync with
     */
    protected AbstractTgtcsBinder(final M model) {
        widgetModel = model;
    }

    /**
     * Loads all data from model to UI components
     */
    public abstract void loadAll();

    /**
     * Saves all data from UI components to model
     */
    public abstract void saveAll();

    /**
     * @return the widgetModel
     */
    public M getWidgetModel() {
        return widgetModel;
    }

}

/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers;

import de.hybris.platform.cockpit.widgets.controllers.WidgetController;



/**
 * @author Dell
 * 
 */
public interface EmailCustomerController extends WidgetController {
    public void sendResetPasswordEmail();

    public void sendUidChangedEmail(final String oldUid);
}

/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.cscockpit.widgets.controllers.BasketController;
import de.hybris.platform.cscockpit.widgets.controllers.CallContextController;

import java.util.List;

import org.zkoss.zul.Listbox;

import au.com.target.tgtcs.widgets.controllers.impl.TargetCnCStoreSelectController;
import au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;


/**
 * 
 * 
 */
public interface TargetBasketController extends BasketController {

    /**
     * update cart entry quantity when user clicks on Update Cart button
     * 
     * @param cartEntry
     *            the cart entry to update
     * @param quantity
     *            the new quantity
     * @param oldQuantity
     *            the old quantity
     * @throws CommerceCartModificationException
     *             if old quantity is null or 0
     */
    void setQuantityToSku(final TypedObject cartEntry, final long quantity, Long oldQuantity)
            throws CommerceCartModificationException;

    /**
     * Gets current customer's persisted master cart. Any manipulation in cart should use this function.
     * 
     * @return CartModel
     */
    CartModel getMasterCart();

    /**
     * @return CallContextController
     */
    CallContextController returnCallContextController();

    /**
     * Set the address select component
     * 
     * @param addressSelect
     *            address select Listbox
     */
    void setAddressSelectComponent(Listbox addressSelect);

    /**
     * @return address select component
     */
    Listbox getAddressSelectComponent();

    /**
     * 
     * @param widget
     *            the WidgetModel that was source for CnC selection event
     */
    boolean handleCnCModeSelection(Widget<SelectCnCStoreWidgetModel, TargetCnCStoreSelectController> widget);

    /**
     * according to cartModel.cockpitFreeDelivery value, change the delivery cost and recalcuate the order
     * 
     * @param cartModel
     *            cart Model
     */
    void flipFreeDelivery(final CartModel cartModel);

    /**
     * Returns the purchase option model appropriate for this controller.
     * 
     * @return the purchase option model
     */
    PurchaseOptionModel getPurchaseOptionModel();

    /**
     * To remove the existing delivery mode from the cart.
     * 
     * @param cartModel
     *            the purchase option model
     */
    void removeDeliveryMode(CartModel cartModel);

    /**
     * Gets the available delivery modes.
     * 
     * @param orderModel
     *            the order model
     * @return the available delivery modes
     */
    List<TypedObject> getAvailableDeliveryModes(final AbstractOrderModel orderModel);

}

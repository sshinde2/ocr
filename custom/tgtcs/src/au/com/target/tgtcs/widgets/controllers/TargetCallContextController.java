package au.com.target.tgtcs.widgets.controllers;

import de.hybris.platform.cscockpit.widgets.controllers.CallContextController;


public interface TargetCallContextController extends CallContextController {
    /**
     * @return the isAddAddressInCheckoutTab
     */
    public boolean isAddAddressInCheckoutTab();

    /**
     * @param isAddAddressInCheckoutTab
     *            the isAddAddressInCheckoutTab to set
     */
    public void setAddAddressInCheckoutTab(final boolean isAddAddressInCheckoutTab);

    /**
     * @return targetBasketController
     */
    public TargetBasketController returnBasketController();
}

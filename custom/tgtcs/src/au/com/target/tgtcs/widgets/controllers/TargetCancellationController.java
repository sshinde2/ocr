package au.com.target.tgtcs.widgets.controllers;

import de.hybris.platform.cscockpit.widgets.controllers.CancellationController;

/**
 * Target extension to {@link CancellationController}.
 */
public interface TargetCancellationController extends CancellationController {

    /**
     * Returns the maximum allowed shipping cost amount to refund for current order.
     *
     * @return the available shipping cost amount refund
     */
    double getMaximumShippingAmountToRefund();
}

/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers;

import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.cscockpit.widgets.controllers.CardPaymentController;


public interface TargetCardPaymentController extends CardPaymentController {

    /**
     * @return the paymentInfoModel
     */
    public PaymentInfoModel getCurrentPaymentInfoModel();

}

/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.cscockpit.widgets.controllers.CustomerCreateController;


/**
 * @author tuy.vo
 * 
 */
public interface TargetCustomerCreateController extends CustomerCreateController {

    @Override
    public abstract TypedObject createNewCustomer(ObjectValueContainer paramObjectValueContainer, String paramString)
            throws DuplicateUidException, ValueHandlerException;

}

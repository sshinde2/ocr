/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers;

import de.hybris.platform.core.model.order.OrderModel;

import java.math.BigDecimal;

import au.com.target.tgtcore.ordercancel.data.IpgNewRefundInfoDTO;


/**
 * @author bhuang3
 *
 */
public interface TargetManualRefundWidgetController {

    /**
     * get current order
     * 
     * @return orderModel
     */
    OrderModel getSelectedOrder();

    /**
     * find the refund amount for the order
     * 
     * @param order
     * @return BigDecimal refund amount
     */
    BigDecimal findRefundAmount(OrderModel order);

    /**
     * perform ipg manual refund
     * 
     * @param ipgNewRefundInfoDTO
     */
    void performIpgManualRefund(IpgNewRefundInfoDTO ipgNewRefundInfoDTO);

}

/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers;

import de.hybris.platform.cscockpit.widgets.controllers.OrderManagementActionsWidgetController;


/**
 * @author mjanarth
 * 
 */
public interface TargetOrderManagementActionsWidgetController extends OrderManagementActionsWidgetController {
    /**
     * Is hold order possible
     * 
     * @return true if hold order is possible
     */
    boolean isHoldOrderPossible();

    /**
     * Is approve or reject order fraud possible
     * 
     * @return true if Order Fraud Review is possible
     */
    boolean isOrderFraudReviewPossible();

    /**
     * Is manual refund possible
     * 
     * 
     * @return true if manual is possible
     */
    boolean isManualRefundPossible();


}

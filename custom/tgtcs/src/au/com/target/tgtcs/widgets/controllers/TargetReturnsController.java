package au.com.target.tgtcs.widgets.controllers;

import de.hybris.platform.cscockpit.widgets.controllers.ReturnsController;

/**
 * Target extension to {@link ReturnsController}.
 */
public interface TargetReturnsController extends ReturnsController {

    /**
     * Returns the maximum allowed shipping cost amount to refund for current order.
     *
     * @return the available shipping cost amount refund
     */
    double getMaximumShippingAmountToRefund();
}

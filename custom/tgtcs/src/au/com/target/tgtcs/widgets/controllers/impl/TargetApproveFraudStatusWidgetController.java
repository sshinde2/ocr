/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers.impl;

import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;


/**
 * @author mjanarth
 * 
 */
public class TargetApproveFraudStatusWidgetController extends TargetAbstractFraudStatusWidgetController {

    private static final Logger LOG = Logger.getLogger(TargetApproveFraudStatusWidgetController.class);

    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderModel changeOrder(final String note) {
        final OrderModel orderModel = getSelectedOrder();
        if (OrderStatus.REVIEW.equals(orderModel.getStatus())
                || OrderStatus.REVIEW_ON_HOLD.equals(orderModel.getStatus())) {
            orderModel.setStatus(OrderStatus.CREATED);
            getTargetCsFraudService().addFraudReportToOrder(orderModel, FraudStatus.OK, note);
            startAcceptOrderProcess(orderModel);
        }
        else {
            LOG.info("Order could not be APPROVED as the current status is " + orderModel.getStatus().toString());
        }
        return orderModel;
    }

    /**
     * @param orderModel
     */
    private void startAcceptOrderProcess(final OrderModel orderModel) {
        if (targetFeatureSwitchFacade.isFluentEnabled() && StringUtils.isNotEmpty(orderModel.getFluentId())) {
            startAcceptOrderProcessWithFluent(orderModel);
        }
        else {
            if (BooleanUtils.isTrue(orderModel.getContainsPreOrderItems())) {
                getTargetBusinessProcessService().startPreOrderAcceptProcess(orderModel);
            }
            else {
                getTargetBusinessProcessService().startOrderAcceptProcess(orderModel);
            }
        }
    }

    /**
     * This method decides which business process to call based on three scenarios an order can go to review. 1. After
     * capturing initial deposit for a preorder - containsPreOrderItems: true and normalSalesStartDateTime is present 2.
     * After final payment is captured for a preorder - containsPreOrderItems: false and normalSalesStartDateTime is
     * present 3. After payment is captured for a normal order - containsPreOrderItems: false and
     * normalSalesStartDateTime is null
     * 
     * @param orderModel
     */
    private void startAcceptOrderProcessWithFluent(final OrderModel orderModel) {
        if (BooleanUtils.isTrue(orderModel.getContainsPreOrderItems())) {
            getTargetBusinessProcessService().startFluentAcceptPreOrderProcess(orderModel);
        }
        else if (orderModel.getNormalSaleStartDateTime() != null) {
            getTargetBusinessProcessService().startFluentPreOrderReleaseAcceptProcess(orderModel);
        }
        else {
            getTargetBusinessProcessService().startFluentOrderAcceptProcess(orderModel);
        }
    }

    /**
     * @param targetFeatureSwitchFacade
     *            the targetFeatureSwitchFacade to set
     */
    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }
}

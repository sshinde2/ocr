package au.com.target.tgtcs.widgets.controllers.impl;

import static au.com.target.tgtcs.constants.TgtcsConstants.EVENT_CLASS_KEY;
import static au.com.target.tgtcs.constants.TgtcsConstants.NEW_DELIVERY_MODE_KEY;
import static au.com.target.tgtcs.constants.TgtcsConstants.ORDER_KEY;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cockpit.widgets.browsers.WidgetBrowserModel;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.cscockpit.exceptions.ResourceMessage;
import de.hybris.platform.cscockpit.exceptions.ValidationException;
import de.hybris.platform.cscockpit.utils.SafeUnbox;
import de.hybris.platform.cscockpit.utils.TypeUtils;
import de.hybris.platform.cscockpit.widgets.controllers.CallContextController;
import de.hybris.platform.cscockpit.widgets.controllers.impl.DefaultBasketController;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.util.localization.Localization;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.MessageSource;
import org.springframework.validation.DirectFieldBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.zkoss.spring.SpringUtil;
import org.zkoss.util.resource.Labels;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcs.widgets.acceptors.DeliveryModeChangeEventAcceptor;
import au.com.target.tgtcs.widgets.controllers.TargetBasketController;
import au.com.target.tgtcs.widgets.controllers.TargetCheckoutController;
import au.com.target.tgtcs.widgets.controllers.strategies.TargetBasketStrategy;
import au.com.target.tgtcs.widgets.events.DeliveryModeChangeEvent;
import au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.order.TargetLaybyCommerceCheckoutService;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


/**
 * 
 */
public class TargetBasketControllerImpl extends DefaultBasketController implements TargetBasketController {

    private static final String NOT_ENOUGH_STOCK_OF_PRODUCT_MESSAGE_KEY = "tgtcscockpit.addToCart.not_enough_stock_of_product";

    private static final Logger LOG = Logger.getLogger(TargetBasketController.class);

    private static final String VALID_NUMBER_MESSAGE = "tgtcscockpit.widget.cart.number.valid";

    private static final String DELIVERY_ADDRESS_MESSAGE_KEY = "validation.message.delivery.address";

    private TargetCommerceCartService targetCommerceCartService;

    private I18NService i18nService;

    private MessageSource messageSource;

    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;

    private TargetLaybyCommerceCheckoutService targetLaybyCommerceCheckoutService;

    private Listbox addressSelect;

    private TargetBasketStrategy targetBasketStrategy;

    private Validator clickAndCollectFormValidator;

    private TargetDeliveryService targetDeliveryService;

    private TargetDeliveryModeHelper targetDeliveryModeHelper;


    @Override
    public PurchaseOptionModel getPurchaseOptionModel() {
        return getTargetPurchaseOptionHelper().getCurrentPurchaseOptionModel();
    }

    @Override
    public CartModel getMasterCart() {
        TypedObject customer = getCallContextController().getCurrentCustomer();
        if (customer == null) {
            customer = getCallContextController().getAnonymousCustomer();
            return TypeUtils.unwrapItem(getTargetBasketStrategy().getCart(customer,
                    getCallContextController().getAnonymousCartCode()), CartModel.class);
        }
        final CustomerModel customerModel = TypeUtils.unwrapItem(customer, CustomerModel.class);

        return getTargetBasketStrategy().getPersistedMasterCart(customerModel);
    }

    /**
     * Return current customer session cart.
     * 
     * @return wrapped cart object
     */
    @Override
    public TypedObject getCart() {
        TypedObject customer = getCallContextController().getCurrentCustomer();
        if (customer == null) {
            customer = getCallContextController().getAnonymousCustomer();
        }

        if (getCallContextController().isAnonymousCustomer(customer)) {
            final String cartCode = getCallContextController().getAnonymousCartCode();
            //master cart will be created if it is null
            return getTargetBasketStrategy().loadCart(customer, cartCode);
        }
        else {
            final CustomerModel customerModel = TypeUtils.unwrapItem(customer, CustomerModel.class);

            CartModel cart = getTargetBasketStrategy().getCart(customerModel);
            if (cart == null) {
                cart = getTargetBasketStrategy().createCart(customerModel);
            }
            return getCockpitTypeService().wrapItem(cart);
        }
    }



    /* (non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.controllers.impl.DefaultBasketController#getCartModel()
     */
    @Override
    protected CartModel getCartModel() {
        final TypedObject cart = getCart();
        if (cart == null) {
            return null;
        }
        return (TypeUtils.unwrapItem(cart, CartModel.class));

    }

    @Override
    public void setQuantityToSku(final TypedObject cartEntry, final long quantity,
            final Long oldQuantity)
            throws CommerceCartModificationException {

        long newQty = quantity;
        final CartEntryModel entry = TypeUtils.unwrapItem(cartEntry, CartEntryModel.class);
        if (newQty < 0) {
            if (oldQuantity != null && oldQuantity.longValue() > 0L) {
                newQty = oldQuantity.longValue();
            }
            else {
                final String message = messageSource.getMessage(VALID_NUMBER_MESSAGE, new Object[] {},
                        i18nService.getCurrentLocale());
                throw new CommerceCartModificationException(message);
            }
        }

        final CartModel cart = getMasterCart();
        final ProductModel product = entry.getProduct();

        try {
            final CommerceCartModification commerceCartModification;
            commerceCartModification = targetCommerceCartService.updateEntry(cart, product, newQty);

            if (commerceCartModification != null
                    && !CommerceCartModificationStatus.SUCCESS.equals(commerceCartModification.getStatusCode())) {

                final String message = commerceCartModification.getStatusCode();
                throw new CommerceCartModificationException(message);
            }
            calculateCartInContext(getMasterCart());
        }
        catch (final CommerceCartModificationException e) {
            final String message = e.getMessage();
            throw new CommerceCartModificationException(message, e);
        }
        catch (final Exception e) {
            LOG.error("Set cart quantity error", e);
        }
    }

    @Override
    public CartSummary getCartSummary() {
        final CartModel cartModel = TypeUtils.unwrapItem(getCart(), CartModel.class);
        if (cartModel != null) {
            final DefaultCartSummary cartSummary = new DefaultCartSummary();
            try {
                cartSummary.setNet(SafeUnbox.toBoolean(cartModel.getNet()));

                // Format the total values to currency specific strings

                final NumberFormat currencyFormatter = (NumberFormat)getSessionService().executeInLocalView(
                        new SessionExecutionBody() {

                            @Override
                            public Object execute() {
                                getCommonI18NService().setCurrentCurrency(cartModel.getCurrency());
                                return getFormatFactory().createCurrencyFormat();
                            }
                        });

                cartSummary.setSubtotal(currencyFormatter.format(SafeUnbox.toDouble(cartModel.getSubtotal())));
                cartSummary
                        .setTotalDiscounts(currencyFormatter.format(SafeUnbox.toDouble(cartModel.getTotalDiscounts())));
                cartSummary.setTotalTax(currencyFormatter.format(SafeUnbox.toDouble(cartModel.getTotalTax())));
                cartSummary.setTotalPrice(currencyFormatter.format(SafeUnbox.toDouble(cartModel.getTotalPrice())));
            }
            catch (final Exception e) {
                LOG.error("Failed to build cart summary", e);
            }
            return cartSummary;
        }
        return null;
    }

    @Override
    public void addToCart(final TypedObject product, final long quantity) throws IllegalArgumentException {
        final ProductModel productModel = TypeUtils.unwrapItem(product, ProductModel.class);
        final CartModel cart = getMasterCart();
        final PurchaseOptionConfigModel config = getTargetPurchaseOptionHelper().getCurrentPurchaseOptionConfigModel();
        if (config == null) {
            throw new IllegalArgumentException("Master cart is not supported this purchase option");
        }

        // Calculate the cart in the context of the user that owns the cart
        final ImpersonationContext context = createImpersonationContext(cart);

        getImpersonationService().executeInContext(context,
                new ImpersonationService.Executor<Object, ImpersonationService.Nothing>() {
                    @Override
                    public Object execute() {
                        try {
                            // Add the product and quantify specified - default behaviour is to add it to an exiting
                            // order entry or to append a new cart entry
                            final CommerceCartModification cartModification = ((TargetCommerceCartService)getCommerceCartService())
                                    .addToCart(cart, productModel, quantity, null);
                            if (cartModification.getQuantityAdded() > 0
                                    && cartModification.getStatusCode()
                                            .equals(CommerceCartModificationStatus.LOW_STOCK)) {
                                try {
                                    Messagebox.show(Localization
                                            .getLocalizedString(NOT_ENOUGH_STOCK_OF_PRODUCT_MESSAGE_KEY),
                                            Labels.getLabel("tgtcscockpit.addToCart"), Messagebox.OK,
                                            Messagebox.INFORMATION);
                                }
                                catch (final InterruptedException e) {
                                    LOG.error("System exception: ", e);
                                    Thread.currentThread().interrupt();
                                }
                            }
                        }
                        catch (final CommerceCartModificationException e) {
                            LOG.error("Exception calculating cart [" + cart.getCode() + "]", e);
                        }
                        return null;
                    }
                });
    }

    @Override
    public void triggerCheckout() throws ValidationException {
        final CartModel cartModel = getMasterCart();
        // Calculate the cart before going into checkout
        calculateCartInContext(cartModel);

        // Validate the cart is ready for checkout
        validateBasketReadyForCheckout(cartModel);

        setPaymentAddressIfAvailable(cartModel);

        // create checkout tab and switch to it
        final WidgetBrowserModel checkoutBrowser = getCheckoutBrowser();

        getWidgetHelper().openAndFocusBrowser(checkoutBrowser);
        getWidgetHelper().focusWidget(checkoutBrowser.getBrowserCode(), getCheckoutBrowserDefaultWidgetCode());
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.controllers.impl.DefaultBasketController#getAvailableDeliveryModes()
     */
    @Override
    public List<TypedObject> getAvailableDeliveryModes(final AbstractOrderModel orderModel) {
        final List<DeliveryModeModel> filteredDeliveryModeModels = new ArrayList<>();
        final Collection<TargetZoneDeliveryModeModel> allDeliveryModeModels = targetDeliveryService
                .getAllApplicableDeliveryModesForOrder(orderModel, SalesApplication.CALLCENTER);
        for (final DeliveryModeModel deliveryModeModel : allDeliveryModeModels) {
            if (targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(
                    ((TargetCheckoutController)getCheckoutController()).getCheckoutCartByMasterCart(),
                    deliveryModeModel)) {
                filteredDeliveryModeModels.add(deliveryModeModel);
            }
        }
        return getCockpitTypeService().wrapItems(filteredDeliveryModeModels);
    }

    @Override
    public void setDeliveryAddress(final TypedObject address) {
        final CartModel cartModel = ((TargetCheckoutController)getCheckoutController()).getCheckoutCartByMasterCart();
        final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
        checkoutParameter.setCart(cartModel);
        checkoutParameter.setAddress(TypeUtils.unwrapItem(address, AddressModel.class));
        checkoutParameter.setIsDeliveryAddress(true);

        if (!getCommerceCheckoutService().setDeliveryAddress(checkoutParameter)) {
            try {
                Messagebox.show(Localization.getLocalizedString(DELIVERY_ADDRESS_MESSAGE_KEY),
                        Labels.getLabel("tgtcscockpit.widget.checkout.deliveryAddress"), Messagebox.OK,
                        Messagebox.ERROR);
            }
            catch (final Exception e) {
                LOG.error("Error setting cart delivery address", e);
            }
        }
        if (cartModel.getDeliveryMode() == null) {
            setDeliveryModeIfAvailable(cartModel);
        }

        calculateCartInContext(cartModel);
    }

    @Override
    public void removeDeliveryMode(final CartModel cartModel) {
        final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
        checkoutParameter.setCart(cartModel);
        targetLaybyCommerceCheckoutService.removeDeliveryMode(checkoutParameter);
    }

    @Override
    public void setPaymentAddress(final TypedObject address) {
        final CartModel cartModel = ((TargetCheckoutController)getCheckoutController()).getCheckoutCartByMasterCart();
        cartModel.setPaymentAddress(TypeUtils.unwrapItem(address, AddressModel.class));
        getModelService().save(cartModel);
        calculateCartInContext(cartModel);
    }

    @Override
    protected void validateBasketReadyForCheckout(final CartModel cart) throws ValidationException {

        final List errorMessages = new ArrayList();

        if (getUserService().getAnonymousUser().equals(cart.getUser())) {
            errorMessages.add(new ResourceMessage("checkoutAction.notCustomerSelected.error"));
            throw new ValidationException(errorMessages);
        }

        super.validateBasketReadyForCheckout(cart);
    }

    @Override
    public boolean handleCnCModeSelection(
            final Widget<SelectCnCStoreWidgetModel, TargetCnCStoreSelectController> widget) {
        final SelectCnCStoreWidgetModel widgetModel = widget.getWidgetModel();
        final CartModel order = getMasterCart();

        final DeliveryModeModel newDeliveryMode = widgetModel.getSelectedDeliveryMode();

        final TargetPointOfServiceModel pos = widgetModel.getStore();

        // perform form validation - if validation is OK, change delivery mode and address
        final Errors errors = new DirectFieldBindingResult(widgetModel.getCncFormData(), "formData");
        getClickAndCollectFormValidator().validate(widgetModel.getCncFormData(), errors);

        if (errors.hasErrors()) {
            // show errors
            widget.getWidgetController().displayValidationErrors(errors, widget);
            return false;
        }

        final TitleModel titleModel = widgetModel.getTitle();

        final String title = titleModel == null ? null : titleModel.getCode();
        final String firstName = widgetModel.getFirstName();
        final String lastName = widgetModel.getLastName();
        final String phoneNumber = widgetModel.getPhone();

        final boolean result = getTargetLaybyCommerceCheckoutService().fillCart4ClickAndCollect(order,
                pos.getStoreNumber().intValue(),
                title,
                firstName,
                lastName,
                phoneNumber, newDeliveryMode);

        getCheckoutController().dispatchEvent(null, widget, null);
        this.dispatchEvent(null, widget, null);

        return result;
    }


    @Override
    public void flipFreeDelivery(final CartModel cartModel) {
        final AbstractTargetZoneDeliveryModeValueModel deliveryValue = cartModel.getZoneDeliveryModeValue();

        final double fee = getTargetDeliveryService().getDeliveryCostForDeliveryType(deliveryValue,
                cartModel).getDeliveryCost();
        cartModel.setDeliveryCost(Double.valueOf(fee));
        getModelService().save(cartModel);
        calculateCartInContext(cartModel);

    }

    /**
     * Constructs and dispatches instance of WidgetEvent.
     * 
     * @param context
     *            the context of event
     * @param source
     *            the source of the event
     * @param data
     *            event parameters
     */
    @Override
    public void dispatchEvent(final String context, final Object source, final Map<String, Object> data) {
        if (data != null && DeliveryModeChangeEvent.class.equals(data.get(EVENT_CLASS_KEY))) {
            final TypedObject newDeliveryMode = (TypedObject)data.get(NEW_DELIVERY_MODE_KEY);
            final TypedObject order = (TypedObject)data.get(ORDER_KEY);

            final DeliveryModeChangeEvent event = new DeliveryModeChangeEvent(source, context, newDeliveryMode,
                    order);

            dispatchEvent(CONTROLLER_CONTEXT, event);
            return;
        }

        super.dispatchEvent(context, source, data);
    }

    /**
     * Spring init-method
     */
    public void initialize() {
        // retrieving acceptor beans from context
        final DeliveryModeChangeEventAcceptor deliveryChangeEventAcceptor = (DeliveryModeChangeEventAcceptor)SpringUtil
                .getBean(
                        DeliveryModeChangeEventAcceptor.BEAN_ID,
                        DeliveryModeChangeEventAcceptor.class);

        // avoiding circular spring dependency
        deliveryChangeEventAcceptor.setController(this);

        //adding beans to acceptor map
        addCockpitEventAcceptor(CONTROLLER_CONTEXT, deliveryChangeEventAcceptor);
    }

    /**
     * @return the clickAndCollectFormValidator
     */
    protected Validator getClickAndCollectFormValidator() {
        return clickAndCollectFormValidator;
    }

    /**
     * @param clickAndCollectFormValidator
     *            the clickAndCollectFormValidator to set
     */
    @Required
    public void setClickAndCollectFormValidator(final Validator clickAndCollectFormValidator) {
        this.clickAndCollectFormValidator = clickAndCollectFormValidator;
    }

    /**
     * @return the targetBasketStrategy
     */
    protected TargetBasketStrategy getTargetBasketStrategy() {
        return targetBasketStrategy;
    }

    /**
     * @param targetBasketStrategy
     *            the targetBasketStrategy to set
     */
    @Required
    public void setTargetBasketStrategy(final TargetBasketStrategy targetBasketStrategy) {
        this.targetBasketStrategy = targetBasketStrategy;
    }

    /**
     * @return the targetLaybyCommerceCheckoutService
     */
    protected TargetLaybyCommerceCheckoutService getTargetLaybyCommerceCheckoutService() {
        return targetLaybyCommerceCheckoutService;
    }

    /**
     * @param targetLaybyCommerceCheckoutService
     *            the targetLaybyCommerceCheckoutService to set
     */
    @Required
    public void setTargetLaybyCommerceCheckoutService(
            final TargetLaybyCommerceCheckoutService targetLaybyCommerceCheckoutService) {
        this.targetLaybyCommerceCheckoutService = targetLaybyCommerceCheckoutService;
    }

    /**
     * @param targetCommerceCartService
     *            the commerceCartService to set
     */
    @Required
    public void setTargetCommerceCartService(final TargetCommerceCartService targetCommerceCartService) {
        this.targetCommerceCartService = targetCommerceCartService;
    }

    @Override
    public CallContextController returnCallContextController() {
        return getCallContextController();
    }

    @Override
    public void setAddressSelectComponent(final Listbox varAddressSelect) {
        this.addressSelect = varAddressSelect;
    }

    @Override
    public Listbox getAddressSelectComponent() {
        return addressSelect;
    }

    /**
     * @param i18nService
     *            the i18nService to set
     */
    @Required
    public void setI18nService(final I18NService i18nService) {
        this.i18nService = i18nService;
    }

    /**
     * @param messageSource
     *            the messageSource to set
     */
    @Required
    public void setMessageSource(final MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /**
     * @return the targetPurchaseOptionHelper
     */
    protected TargetPurchaseOptionHelper getTargetPurchaseOptionHelper() {
        return targetPurchaseOptionHelper;
    }

    /**
     * @param targetPurchaseOptionHelper
     *            the targetPurchaseOptionHelper to set
     */
    @Required
    public void setTargetPurchaseOptionHelper(final TargetPurchaseOptionHelper targetPurchaseOptionHelper) {
        this.targetPurchaseOptionHelper = targetPurchaseOptionHelper;
    }

    /**
     * @return the targetDeliveryService
     */
    protected TargetDeliveryService getTargetDeliveryService() {
        return targetDeliveryService;
    }

    /**
     * @param targetDeliveryService
     *            the targetDeliveryService to set
     */
    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        super.setDeliveryService(targetDeliveryService);
        this.targetDeliveryService = targetDeliveryService;
    }

    /**
     * @param targetDeliveryModeHelper
     *            the targetDeliveryModeHelper to set
     */
    @Required
    public void setTargetDeliveryModeHelper(final TargetDeliveryModeHelper targetDeliveryModeHelper) {
        this.targetDeliveryModeHelper = targetDeliveryModeHelper;
    }

}

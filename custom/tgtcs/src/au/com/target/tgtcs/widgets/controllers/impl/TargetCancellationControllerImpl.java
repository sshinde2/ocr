package au.com.target.tgtcs.widgets.controllers.impl;

import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.cscockpit.exceptions.ResourceMessage;
import de.hybris.platform.cscockpit.exceptions.ValidationException;
import de.hybris.platform.cscockpit.utils.SafeUnbox;
import de.hybris.platform.cscockpit.widgets.controllers.impl.DefaultCancellationController;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRequest;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.order.FindOrderRefundedShippingStrategy;
import au.com.target.tgtcore.ordercancel.TargetOrderCancelRequest;
import au.com.target.tgtcore.ordercancel.data.IpgNewRefundInfoDTO;
import au.com.target.tgtcore.ordercancel.enums.RequestOrigin;
import au.com.target.tgtcs.components.orderdetail.controllers.TgtCsCardPaymentController;
import au.com.target.tgtcs.widgets.controllers.TargetCancellationController;

import com.google.common.collect.ImmutableList;


/**
 * Default implementation of {@link TargetCancellationController}.
 */
public class TargetCancellationControllerImpl extends DefaultCancellationController
        implements TargetCancellationController {

    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.00");

    private static final String CANCEL_ENTRY_VALIDATION_KEY = "cancelRecordEntry.validation.cancelRequestQuantity";

    private static final String MAX_CANCEL_REQUEST_QTY_VALIDATION_KEY = CANCEL_ENTRY_VALIDATION_KEY + ".gtMaxQty";

    private static final String NEGATIVE_CANCEL_REQUEST_QTY_VALIDATION_KEY = CANCEL_ENTRY_VALIDATION_KEY + ".negative";

    private static final String ZERO_CANCEL_REQUEST_QTY_VALIDATION_KEY = CANCEL_ENTRY_VALIDATION_KEY + ".zeroQty";

    private static final String ORDER_CANCEL_ENTRY_CANCEL_QUANTITY = "OrderCancelEntry.cancelQuantity";

    private static final String ORDER_CANCEL_ENTRY_CANCEL_REASON = "OrderCancelEntry.cancelReason";

    private static final String ENTRY = "Entry ";

    private static final String MISSING_REASON = ": Missing Reason";

    private static final String PRODUCT_NOT_CANCELABLE_KEY = "cancelRecordEntry.validation.product.notCancelable";

    private static final String NONE_SELECTED_VALIDATION_KEY = "cancellationRequest.validation.noneSelected";

    private FindOrderRefundedShippingStrategy findOrderRefundedShippingStrategy;

    private TgtCsCardPaymentController tgtCsCardPaymentController;

    /**
     * Creates Target extension to order cancel request that includes all delivery cost to refund.
     * 
     * @param order
     *            the order to create cancel request for
     * @param cancelRequest
     *            the submitted cancel request form
     * @return the order cancel request
     */
    @Override
    protected OrderCancelRequest buildCancelRequest(final OrderModel order, final ObjectValueContainer cancelRequest) {
        final String notes =
                (String)getPropertyValue(cancelRequest, "OrderCancelRequest.notes").getCurrentValue();
        final CancelReason reason =
                (CancelReason)getPropertyValue(cancelRequest, "OrderCancelRequest.cancelReason").getCurrentValue();

        final CreditCardPaymentInfoModel newPayment = getNewPayment(order);
        final IpgNewRefundInfoDTO ipgNewRefundInfoDTO = tgtCsCardPaymentController.getIpgNewRefundInfoDTO();

        if (order != null) {
            final TargetOrderCancelRequest orderCancelRequest = new TargetOrderCancelRequest(order);
            orderCancelRequest.setCancelReason(reason);
            orderCancelRequest.setNotes(notes);
            orderCancelRequest.setShippingAmountToRefund(Double.valueOf(getMaximumShippingAmountToRefund()));
            orderCancelRequest.setNewPayment(newPayment);
            orderCancelRequest.setIpgNewRefundInfoDTO(ipgNewRefundInfoDTO);
            orderCancelRequest.setRequestOrigin(RequestOrigin.CSCOCKPIT);
            return orderCancelRequest;
        }

        return null;
    }

    private CreditCardPaymentInfoModel getNewPayment(final OrderModel order) {
        CreditCardPaymentInfoModel newPayment = (CreditCardPaymentInfoModel)
                tgtCsCardPaymentController.getNewPayment();
        //check whether this payment is for this user
        if (newPayment != null) {
            if (newPayment.getCode() != null) {
                final String[] codeInfo = newPayment.getCode().split("_");
                if (codeInfo.length != 2 || !codeInfo[0].equalsIgnoreCase(order.getUser().getUid())
                        || !newPayment.getUser().getUid().equals(order.getUser().getUid())
                        || newPayment.getSubscriptionId() == null) {
                    newPayment = null;
                }
            }
        }
        return newPayment;
    }

    /**
     * Creates Target extension of order cancel request object based on provided {@code order} and submitted cancel
     * request form {@code cancelRequest}.
     * 
     * @param order
     *            the order to create cancel request for
     * @param entriesToCancel
     *            the entries to cancel
     * @param cancelRequest
     *            the submitted cancel request form
     * @return the order cancel request
     */
    @Override
    protected OrderCancelRequest buildCancelRequest(final OrderModel order,
            final Map<TypedObject, CancelEntryDetails> entriesToCancel, final ObjectValueContainer cancelRequest) {
        Double shippingAmountToRefund =
                (Double)getPropertyValue(cancelRequest, "OrderCancelRequest.shippingAmountToRefund").getCurrentValue();

        if (shippingAmountToRefund == null) {
            shippingAmountToRefund = Double.valueOf(0d);
        }

        final CreditCardPaymentInfoModel newPayment = getNewPayment(order);
        final IpgNewRefundInfoDTO ipgNewRefundInfoDTO = tgtCsCardPaymentController.getIpgNewRefundInfoDTO();

        if (order != null && entriesToCancel != null && !entriesToCancel.isEmpty()) {
            final List<OrderCancelEntry> orderCancelEntries = new ArrayList<>();

            for (final TypedObject orderEntry : entriesToCancel.keySet()) {
                if (orderEntry.getObject() instanceof OrderEntryModel) {
                    final OrderEntryModel entryModel = (OrderEntryModel)orderEntry.getObject();
                    final CancelEntryDetails cancelEntry = entriesToCancel.get(orderEntry);
                    final OrderCancelEntry orderCancelEntry = new OrderCancelEntry(entryModel,
                            cancelEntry.getQuantity(), cancelEntry.getNotes(), cancelEntry.getReason());
                    orderCancelEntries.add(orderCancelEntry);
                }
            }

            final TargetOrderCancelRequest orderCancelRequest = new TargetOrderCancelRequest(order, orderCancelEntries);
            orderCancelRequest.setShippingAmountToRefund(shippingAmountToRefund);
            orderCancelRequest.setNewPayment(newPayment);
            orderCancelRequest.setIpgNewRefundInfoDTO(ipgNewRefundInfoDTO);
            return orderCancelRequest;
        }

        return null;
    }

    @Override
    public double getMaximumShippingAmountToRefund() {
        final TypedObject typedObject = getOrder();
        if (typedObject != null && typedObject.getObject() instanceof OrderModel) {
            final OrderModel order = (OrderModel)typedObject.getObject();
            final Double paidObj = order.getInitialDeliveryCost();
            final double paid = paidObj != null ? paidObj.doubleValue() : 0d;
            final double alreadyRefunded = findOrderRefundedShippingStrategy.getRefundedShippingAmount(order);
            return Math.max(paid - alreadyRefunded, 0d);
        }
        return 0;
    }

    /**
     * this method not only check the cancel qty, but also make sure there is a cancel reason for each cancelled item.
     * 
     * @param orderModel
     *            the ordermodels
     * @param cancelableOrderEntries
     *            the cancelableOrderEntries
     * @param orderEntryCancelRecords
     *            the orderEntryCancelRecords
     * @throws ValidationException
     *             the exception
     * @return a boolean to say whether the validation is good or not
     * 
     */
    @Override
    protected boolean validateCreateCancellationRequest(final OrderModel orderModel,
            final Map<TypedObject, Long> cancelableOrderEntries,
            final List<ObjectValueContainer> orderEntryCancelRecords)
            throws ValidationException
    {
        long okCount = 0L;
        final List<ResourceMessage> errorMessages = new ArrayList<>();

        for (final ObjectValueContainer ovc : orderEntryCancelRecords)
        {
            final List<ResourceMessage> entryErrorMessages = new ArrayList<>();
            boolean entryProcessed = false;

            final TypedObject orderEntry = (TypedObject)ovc.getObject();
            final int entryNumber = SafeUnbox.toInt(((AbstractOrderEntryModel)orderEntry.getObject()).getEntryNumber());
            if (cancelableOrderEntries.containsKey(orderEntry))
            {
                final ObjectValueContainer.ObjectValueHolder cancelRequestQuantity = getPropertyValue(ovc,
                        ORDER_CANCEL_ENTRY_CANCEL_QUANTITY);

                if ((cancelRequestQuantity != null) && (cancelRequestQuantity.getCurrentValue() instanceof Long))
                {
                    final long cancelRequestQuantityValue = SafeUnbox.toLong((Long)cancelRequestQuantity
                            .getCurrentValue());
                    if (cancelRequestQuantityValue == 0L)
                    {
                        entryErrorMessages.add(
                                new ResourceMessage(ZERO_CANCEL_REQUEST_QTY_VALIDATION_KEY,
                                        Arrays.asList(new Integer[] {
                                                Integer.valueOf(entryNumber) })));
                    }

                    if (cancelRequestQuantityValue < 0L)
                    {
                        entryErrorMessages.add(
                                new ResourceMessage(NEGATIVE_CANCEL_REQUEST_QTY_VALIDATION_KEY,
                                        Arrays.asList(new Integer[] {
                                                Integer.valueOf(entryNumber) })));
                    }

                    if ((cancelRequestQuantityValue > 0L)
                            && (cancelRequestQuantityValue > SafeUnbox.toLong(cancelableOrderEntries.get(orderEntry))))
                    {
                        entryErrorMessages.add(
                                new ResourceMessage(MAX_CANCEL_REQUEST_QTY_VALIDATION_KEY,
                                        Arrays.asList(new Integer[] {
                                                Integer.valueOf(entryNumber) })));
                    }

                    //check whether the entry has a reason specified
                    final ObjectValueContainer.ObjectValueHolder cancelReasonHolder = getPropertyValue(ovc,
                            ORDER_CANCEL_ENTRY_CANCEL_REASON);
                    if (cancelReasonHolder == null || cancelReasonHolder.getCurrentValue() == null)
                    {
                        final String msg = ENTRY + entryNumber + MISSING_REASON;
                        entryErrorMessages.add(
                                new ResourceMessage(msg));
                    }

                    entryProcessed = true;
                }// end of if ((cancelRequestQuantity != null) ...

            } //end of if (cancelableOrderEntries.containsKey(orderEntry))
            else
            {
                entryErrorMessages.add(
                        new ResourceMessage(PRODUCT_NOT_CANCELABLE_KEY,
                                Arrays.asList(new Integer[] {
                                        Integer.valueOf(entryNumber) })));
            }

            if (!(entryErrorMessages.isEmpty()))
            {
                errorMessages.addAll(entryErrorMessages);
            }

            if ((!(entryProcessed)) || (!(entryErrorMessages.isEmpty()))) {
                continue;
            }
            okCount += 1L;
        } // end of for

        if ((okCount == 0L) && (errorMessages.isEmpty()))
        {
            errorMessages.add(new ResourceMessage(NONE_SELECTED_VALIDATION_KEY));
        }

        if (!(errorMessages.isEmpty()))
        {
            throw new ValidationException(errorMessages);
        }

        return (okCount > 0L);
    }

    @Override
    public TypedObject createPartialOrderCancellationRequest(final List<ObjectValueContainer> orderEntryCancelRecords,
            final ObjectValueContainer cancelRequest) throws OrderCancelException, ValidationException {

        final Double shippingAmountToRefundObj =
                (Double)getPropertyValue(cancelRequest, "OrderCancelRequest.shippingAmountToRefund").getCurrentValue();
        final double maxShippingAmountToRefund = getMaximumShippingAmountToRefund();
        if (shippingAmountToRefundObj != null) {
            final double shippingAmountToRefund = shippingAmountToRefundObj.doubleValue();
            if (shippingAmountToRefund < 0 || shippingAmountToRefund > maxShippingAmountToRefund) {
                if (maxShippingAmountToRefund > 0) {
                    throw new ValidationException(ImmutableList.of(
                            new ResourceMessage("tgtcscockpit.cancelRecord.validation.shippingAmountInvalid",
                                    ImmutableList.of(DECIMAL_FORMAT.format(maxShippingAmountToRefund)))));
                }
                else {
                    throw new ValidationException(ImmutableList.of(
                            new ResourceMessage("tgtcscockpit.cancelRecord.validation.shippingAmountAlreadyRefunded")));
                }
            }
        }

        return super.createPartialOrderCancellationRequest(orderEntryCancelRecords, cancelRequest);
    }

    /**
     * @param findOrderRefundedShippingStrategy
     *            the findOrderRefundedShippingStrategy to set
     */
    public void setFindOrderRefundedShippingStrategy(
            final FindOrderRefundedShippingStrategy findOrderRefundedShippingStrategy) {
        this.findOrderRefundedShippingStrategy = findOrderRefundedShippingStrategy;
    }

    /**
     * @param tgtCsCardPaymentController
     *            the tgtCsCardPaymentController to set
     */
    @Required
    public void setTgtCsCardPaymentController(final TgtCsCardPaymentController tgtCsCardPaymentController) {
        this.tgtCsCardPaymentController = tgtCsCardPaymentController;
    }


}

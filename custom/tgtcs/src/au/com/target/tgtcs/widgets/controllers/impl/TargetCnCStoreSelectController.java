/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers.impl;

import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.cscockpit.utils.TypeUtils;
import de.hybris.platform.cscockpit.widgets.controllers.impl.AbstractCsWidgetController;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.validation.Errors;
import org.zkoss.zul.Comboitem;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtcs.widgets.controllers.TargetBasketController;
import au.com.target.tgtcs.widgets.controllers.TargetCheckoutController;
import au.com.target.tgtcs.widgets.renderers.impl.SelectCnCStoreWidgetRenderer;


/**
 * @author Pavel_Yakushevich
 * 
 */
public class TargetCnCStoreSelectController extends AbstractCsWidgetController {

    private Converter<PointOfServiceModel, Comboitem> targetPOStoComboitemConverter;

    private Converter<TitleModel, Comboitem> targetTitletoComboitemConverter;

    private TargetBasketController basketController;

    private TargetPointOfServiceService pointOfServiceService;

    private Map<String, Set<TargetPointOfServiceModel>> stateToStoreMap;

    private SelectCnCStoreWidgetRenderer renderer;

    private UserService userService;

    @Override
    public void dispatchEvent(final String paramString, final Object paramObject, final Map<String, Object> paramMap) {
        getBasketController().dispatchEvent(paramString, paramObject, paramMap);
    }

    /**
     * 
     * @param errors
     * @param widget
     */
    public void displayValidationErrors(final Errors errors, final Widget widget) {
        getRenderer().displayErrors(errors, widget);
    }

    /**
     * 
     * @return states
     */
    public Collection<Comboitem> getStates() {
        final Collection<Comboitem> states = new ArrayList<>();
        for (final String state : stateToStoreMap.keySet()) {
            final Comboitem item = new Comboitem(state);
            item.setValue(state);
            states.add(item);
        }
        return states;
    }

    /**
     * 
     * @param state
     *            the code of the state to fetch stores for
     * @return stores for given state
     */
    public Collection<Comboitem> getStoresForState(final String state) {
        final Collection<TargetPointOfServiceModel> stores = getStoresForStateInternal(state);
        return processStores(stores);
    }

    /**
     * Utility method to convert a collection of {@link TargetPointOfServiceModel} to a collection of {@link Comboitem}
     * with {@link #getPOStoComboitemConverter()}.
     * 
     * @param stores
     *            the Collection of {@link TargetPointOfServiceModel}
     * @return processed stores
     */
    protected Collection<Comboitem> processStores(final Collection<TargetPointOfServiceModel> stores) {
        return Converters.convertAll(stores, getPOStoComboitemConverter());
    }

    /**
     * 
     * @param state
     *            the state isocode
     * @return stores for given state
     */
    protected Collection<TargetPointOfServiceModel> getStoresForStateInternal(final String state) {
        Collection<TargetPointOfServiceModel> allStores = null;
        final CartModel cart = ((TargetCheckoutController)this.getBasketController().getCheckoutController())
                .getCheckoutCartByMasterCart();
        final Map<String, Set<TargetPointOfServiceModel>> stsm = getPointOfServiceService().getStateAndStoresForCart(
                cart);
        if (state == null) {
            // add all stores from all states
            allStores = new ArrayList<>();
            for (final Collection<TargetPointOfServiceModel> storesForState : stsm.values()) {
                allStores.addAll(storesForState);
            }
        }
        else {
            allStores = stsm.get(state);
        }

        return allStores == null ? Collections.EMPTY_LIST : allStores;
    }

    /**
     * All distinct user titles in database(e.g. Mr, Mrs, Dr, etc)
     * 
     * @return all distinct user titles, converted to {@link Comboitem} Collection
     */
    public Collection<Comboitem> getAllTitles() {
        return Converters.convertAll(getUserService().getAllTitles(), getTitletoComboitemConverter());
    }

    /**
     * Spring init method. Constructs states-to-stores map.
     */
    @PostConstruct
    public void initialize() {
        final CartModel cart = TypeUtils.unwrapItem(getBasketController().getCart(), CartModel.class);
        stateToStoreMap = getPointOfServiceService().getStateAndStoresForCart(cart);
    }

    /**
     * @return the userService
     */
    protected UserService getUserService() {
        return userService;
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @return the pointOfServiceService
     */
    protected TargetPointOfServiceService getPointOfServiceService() {
        return pointOfServiceService;
    }

    /**
     * @param pointOfServiceService
     *            the pointOfServiceService to set
     */
    @Required
    public void setPointOfServiceService(final TargetPointOfServiceService pointOfServiceService) {
        this.pointOfServiceService = pointOfServiceService;
    }

    /**
     * @return the basketController
     */
    public TargetBasketController getBasketController() {
        return basketController;
    }

    /**
     * @param basketController
     *            the basketController to set
     */
    @Required
    public void setBasketController(final TargetBasketController basketController) {
        this.basketController = basketController;
    }

    /**
     * @return the targetPOStoComboitemConverter
     */
    protected Converter<PointOfServiceModel, Comboitem> getPOStoComboitemConverter() {
        return targetPOStoComboitemConverter;
    }

    /**
     * @param POStoComboitemConverter
     *            the targetPOStoComboitemConverter to set
     */
    @Required
    public void setPOStoComboitemConverter(
            final Converter<PointOfServiceModel, Comboitem> POStoComboitemConverter) {
        this.targetPOStoComboitemConverter = POStoComboitemConverter;
    }

    /**
     * @return the targetTitletoComboitemConverter
     */
    protected Converter<TitleModel, Comboitem> getTitletoComboitemConverter() {
        return targetTitletoComboitemConverter;
    }

    /**
     * @param targetTitletoComboitemConverter_
     *            the targetTitletoComboitemConverter to set
     */
    @Required
    public void setTitletoComboitemConverter(
            final Converter<TitleModel, Comboitem> targetTitletoComboitemConverter_) {
        this.targetTitletoComboitemConverter = targetTitletoComboitemConverter_;
    }

    /**
     * @return the renderer
     */
    protected SelectCnCStoreWidgetRenderer getRenderer() {
        return renderer;
    }

    /**
     * @param renderer
     *            the renderer to set
     */
    @Required
    public void setRenderer(final SelectCnCStoreWidgetRenderer renderer) {
        this.renderer = renderer;
    }

}

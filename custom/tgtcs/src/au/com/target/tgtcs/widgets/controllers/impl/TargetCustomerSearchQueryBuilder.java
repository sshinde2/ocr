package au.com.target.tgtcs.widgets.controllers.impl;

import de.hybris.platform.cscockpit.services.search.generic.query.DefaultCustomerSearchQueryBuilder;
import de.hybris.platform.cscockpit.services.search.impl.DefaultCsTextSearchCommand;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;


/**
 * 
 * The query builder for customer searching
 * 
 */
public class TargetCustomerSearchQueryBuilder extends DefaultCustomerSearchQueryBuilder {

    private static final String SPACE = " ";

    /* 
     * Customer searching by first name, last name, uid, fly buys code fields.
     * If key word is in firstname + " " + lastname format, we are searching by firstname and lastname. 
     * If there is not a space, we are searching by uid or flybuys code or firstname or lastname.
     * If keyword is not in nummeric format, we don't search by flybuys code 
     * 
     * @see de.hybris.platform.cscockpit.services.search.generic.query.DefaultCustomerSearchQueryBuilder#buildFlexibleSearchQuery(de.hybris.platform.cscockpit.services.search.impl.DefaultCsTextSearchCommand) //NOPMD
     */
    @Override
    protected FlexibleSearchQuery buildFlexibleSearchQuery(final DefaultCsTextSearchCommand command) {

        //handle special characters
        final String keyword = handleKeyword(command.getText(TextField.Name));
        final String nameCriteria = getNameCriteria(keyword);

        final StringBuilder query = new StringBuilder(1000);
        final Map<String, Object> params = new HashMap<String, Object>();

        query.append("SELECT {c:pk}, {c:firstname}, {c:lastname}, {c:uid} ");
        query.append("FROM {TargetCustomer AS c } ");

        if (StringUtils.isNotBlank(keyword)) {

            if (StringUtils.isNotBlank(nameCriteria)) {
                query.append(" WHERE");
                query.append(nameCriteria);

                final String[] name = StringUtils.split(keyword, SPACE);
                params.put("firstname", name[0] + '%');
                params.put("lastname", name[1] + '%');
            }
            else {
                query.append(" WHERE {c:uid} LIKE (?keyword)");
                query.append(" OR {c:firstname} LIKE (?keyword)");
                query.append(" OR {c:lastname} LIKE (?keyword)");

                if (StringUtils.isNumeric(keyword)) {
                    query.append(" OR {c:flybuyscode} LIKE (?keyword)");
                }
                params.put("keyword", keyword + '%');
            }
        }

        query.append(" ORDER BY {c:firstname}, {c:lastname} ");

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());

        searchQuery.addQueryParameters(params);

        return searchQuery;
    }

    /**
     * Build search query when keyword is first name + last name format
     * 
     * @param keyword
     *            keyword
     * @return String
     */
    private String getNameCriteria(final String keyword) {
        if (StringUtils.contains(keyword, SPACE)) {
            final StringBuilder criteria = new StringBuilder(200);
            criteria.append(" (");
            criteria.append(" {c:firstname} LIKE (?firstname)");
            criteria.append(" AND {c:lastname} LIKE (?lastname)");
            criteria.append(" ) ");
            return criteria.toString();
        }
        return "";
    }

    /**
     * Remove % wild card in keyword, escape other special characters
     * 
     * @param keyword
     *            keyword
     * @return String
     */
    private String handleKeyword(final String keyword) {
        String result = StringUtils.trimToEmpty(keyword);
        result = StringUtils.remove(result, "%");
        return result;
    }
}

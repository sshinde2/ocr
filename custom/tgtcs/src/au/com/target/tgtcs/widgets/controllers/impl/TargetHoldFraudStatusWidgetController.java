/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers.impl;

import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import org.apache.log4j.Logger;


/**
 * @author mjanarth
 * 
 */
public class TargetHoldFraudStatusWidgetController extends TargetAbstractFraudStatusWidgetController {
    private static final Logger LOG = Logger.getLogger(TargetHoldFraudStatusWidgetController.class);

    @Override
    public OrderModel changeOrder(final String note) {
        final OrderModel orderModel = getSelectedOrder();
        if (OrderStatus.REVIEW.equals(orderModel.getStatus())) {
            getTargetCsFraudService().addFraudReportToOrder(orderModel, FraudStatus.CHECK, note);
            orderModel.setStatus(OrderStatus.REVIEW_ON_HOLD);
            getModelService().save(orderModel);
            getModelService().refresh(orderModel);
        }
        else {
            LOG.info("Order can not be on HOLD as the current status is " + orderModel.getStatus().toString());
        }
        return orderModel;
    }

}

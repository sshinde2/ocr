/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers.impl;

import de.hybris.platform.cockpit.widgets.controllers.impl.AbstractWidgetController;
import de.hybris.platform.cockpit.widgets.events.WidgetEvent;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.widgets.controllers.CallContextController;
import de.hybris.platform.cscockpit.widgets.controllers.OrderManagementActionsWidgetController;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtcore.ordercancel.TargetRefundPaymentService;
import au.com.target.tgtcore.ordercancel.data.IpgNewRefundInfoDTO;
import au.com.target.tgtcs.widgets.controllers.TargetManualRefundWidgetController;
import au.com.target.tgtpayment.util.PriceCalculator;


/**
 * @author bhuang3
 *
 */
public class TargetManualRefundWidgetControllerImpl extends AbstractWidgetController implements
        TargetManualRefundWidgetController {

    private CallContextController callContextController;

    private FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    private TargetRefundPaymentService targetRefundPaymentService;

    private OrderManagementActionsWidgetController orderManagementActionsWidgetController;

    @Override
    public void dispatchEvent(final String context, final Object source, final Map<String, Object> data) {
        dispatchEvent("csCtx", new WidgetEvent(source, context));
        getOrderManagementActionsWidgetController().dispatchEvent(null, source, data);
    }

    /**
     *
     * @return OrderModel
     */
    @Override
    public OrderModel getSelectedOrder() {
        if (null != getCallContextController().getCurrentOrder()) {
            return (OrderModel)getCallContextController().getCurrentOrder().getObject();
        }

        return null;
    }

    @Override
    public BigDecimal findRefundAmount(final OrderModel order) {
        BigDecimal refundAmount = BigDecimal.ZERO;
        if (order != null) {
            final Double paidSoFar = findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(order);
            refundAmount = PriceCalculator.subtract(order.getTotalPrice(), paidSoFar);
        }
        return refundAmount;
    }

    @Override
    public void performIpgManualRefund(final IpgNewRefundInfoDTO ipgNewRefundInfoDTO) {
        final OrderModel orderModel = getSelectedOrder();
        final BigDecimal refundAmount = findRefundAmount(orderModel);
        targetRefundPaymentService.performIpgManualRefund(orderModel, refundAmount, ipgNewRefundInfoDTO);
    }

    /**
     * @return the callContextController
     */
    public CallContextController getCallContextController() {
        return callContextController;
    }

    /**
     * @param callContextController
     *            the callContextController to set
     */
    @Required
    public void setCallContextController(final CallContextController callContextController) {
        this.callContextController = callContextController;
    }

    /**
     * @param findOrderTotalPaymentMadeStrategy
     *            the findOrderTotalPaymentMadeStrategy to set
     */
    @Required
    public void setFindOrderTotalPaymentMadeStrategy(
            final FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy) {
        this.findOrderTotalPaymentMadeStrategy = findOrderTotalPaymentMadeStrategy;
    }

    /**
     * @param targetRefundPaymentService
     *            the targetRefundPaymentService to set
     */
    @Required
    public void setTargetRefundPaymentService(final TargetRefundPaymentService targetRefundPaymentService) {
        this.targetRefundPaymentService = targetRefundPaymentService;
    }

    /**
     * @return the orderManagementActionsWidgetController
     */
    protected OrderManagementActionsWidgetController getOrderManagementActionsWidgetController() {
        return orderManagementActionsWidgetController;
    }

    /**
     * @param orderManagementActionsWidgetController
     *            the orderManagementActionsWidgetController to set
     */
    @Required
    public void setOrderManagementActionsWidgetController(
            final OrderManagementActionsWidgetController orderManagementActionsWidgetController) {
        this.orderManagementActionsWidgetController = orderManagementActionsWidgetController;
    }



}

/**
 *
 */
package au.com.target.tgtcs.widgets.controllers.impl;



import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.widgets.controllers.impl.DefaultOrderManagementActionsWidgetController;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtcs.widgets.controllers.TargetOrderManagementActionsWidgetController;
import au.com.target.tgtfraud.TargetFraudService;
import au.com.target.tgtpayment.util.PriceCalculator;


/**
 * @author mjanarth
 *
 */
public class TargetOrderManagementActionsWidgetControllerImpl extends DefaultOrderManagementActionsWidgetController
        implements
        TargetOrderManagementActionsWidgetController {

    private static final Logger LOG = Logger.getLogger(TargetOrderManagementActionsWidgetControllerImpl.class);
    private TargetFraudService targetFraudService;
    private FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;
    private List<SalesApplication> disableManualRefundForSalesApplications;

    @Override
    public boolean isHoldOrderPossible() {
        final TypedObject order = getOrder();
        if (order != null && order.getObject() instanceof OrderModel
                && StringUtils.isBlank(((OrderModel)order.getObject()).getVersionID())) {
            try {
                final OrderModel orderModel = (OrderModel)order.getObject();

                final boolean isOrderFraudReviewable = targetFraudService.isOrderReviewable(orderModel);
                return isOrderFraudReviewable && !(OrderStatus.REVIEW_ON_HOLD.equals(orderModel.getStatus()));
            }
            catch (final Exception e) {
                LOG.error("failed to work out if hold order is possible", e);
            }
        }
        return false;
    }

    @Override
    public boolean isOrderFraudReviewPossible() {
        final TypedObject order = getOrder();
        if (order != null && order.getObject() instanceof OrderModel
                && StringUtils.isBlank(((OrderModel)order.getObject()).getVersionID())) {
            try {
                final OrderModel orderModel = (OrderModel)order.getObject();

                return targetFraudService.isOrderReviewable(orderModel);
            }
            catch (final Exception e) {
                LOG.error("failed to work out if order fraud review is possible", e);
            }
        }
        return false;
    }

    @Override
    public boolean isManualRefundPossible() {
        BigDecimal refundAmount = BigDecimal.ZERO;
        final TypedObject order = getOrder();

        if (null != order && order.getObject() instanceof OrderModel) {
            final OrderModel orderModel = (OrderModel)order.getObject();
            if (null != orderModel
                    && disableManualRefundForSalesApplications.contains(orderModel.getSalesApplication())) {
                return false;
            }
            if (null != orderModel && StringUtils.isBlank(((OrderModel)order.getObject()).getVersionID())) {
                final Double paidSoFar = findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(orderModel);
                refundAmount = PriceCalculator.subtract(orderModel.getTotalPrice(), paidSoFar);
            }
            if (refundAmount.compareTo(BigDecimal.ZERO) > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param targetFraudService
     *            the targetFraudService to set
     */
    @Required
    public void setTargetFraudService(final TargetFraudService targetFraudService) {
        this.targetFraudService = targetFraudService;
    }

    /**
     * @param findOrderTotalPaymentMadeStrategy
     *            the findOrderTotalPaymentMadeStrategy to set
     */
    @Required
    public void setFindOrderTotalPaymentMadeStrategy(
            final FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy) {
        this.findOrderTotalPaymentMadeStrategy = findOrderTotalPaymentMadeStrategy;
    }

    /**
     * @param disableManualRefundForSalesApplications
     *            the disableManualRefundForSalesApplications to set
     */
    @Required
    public void setDisableManualRefundForSalesApplications(
            final List<SalesApplication> disableManualRefundForSalesApplications) {
        this.disableManualRefundForSalesApplications = disableManualRefundForSalesApplications;
    }
}

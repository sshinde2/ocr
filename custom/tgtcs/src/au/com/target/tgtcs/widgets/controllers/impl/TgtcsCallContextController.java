package au.com.target.tgtcs.widgets.controllers.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.model.search.impl.ResultObjectWrapper;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer.ObjectValueHolder;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.cscockpit.utils.TypeUtils;
import de.hybris.platform.cscockpit.widgets.controllers.impl.DefaultCallContextController;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.impl.InputElement;

import au.com.target.tgtcore.customer.TargetCustomerAccountService;
import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtcs.widgets.controllers.AddressCustomerController;
import au.com.target.tgtcs.widgets.controllers.EmailCustomerController;
import au.com.target.tgtcs.widgets.controllers.TargetBasketController;
import au.com.target.tgtcs.widgets.controllers.TargetCallContextController;
import au.com.target.tgtverifyaddr.TargetAddressVerificationService;
import au.com.target.tgtverifyaddr.data.AddressData;
import au.com.target.tgtverifyaddr.data.CountryEnum;
import au.com.target.tgtverifyaddr.exception.NoMatchFoundException;
import au.com.target.tgtverifyaddr.exception.RequestTimeOutException;
import au.com.target.tgtverifyaddr.exception.ServiceNotAvailableException;
import au.com.target.tgtverifyaddr.exception.TooManyMatchesFoundException;



public class TgtcsCallContextController extends DefaultCallContextController implements TargetCallContextController,
        EmailCustomerController,
        AddressCustomerController {

    private static final Logger LOG = Logger.getLogger(TgtcsCallContextController.class);

    private TargetAddressVerificationService targetAddressVerificationService;

    private boolean addAddressInCheckoutTab;

    /* (non-Javadoc)
     * @see au.com.target.tgtcs.widgets.controllers.EmailCustomerController#sendResetPasswordEmail()
     */
    @Override
    public void sendResetPasswordEmail() {
        //send reset password mail
        final TypedObject customer = getCurrentCustomer();
        if ((customer != null) && ((customer.getObject() instanceof CustomerModel))) {
            getCustomerAccountService().forgottenPassword((CustomerModel)customer.getObject());
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcs.widgets.controllers.AddressCustomerController#checkValidAddress(java.lang.String)
     */
    @Override
    public List<AddressData> checkValidAddress(final ObjectValueContainer addressContainer) {
        // create address string for passing validate address service
        final StringBuilder builderAddress = new StringBuilder();
        final Set<ObjectValueHolder> allValues = addressContainer.getAllValues();
        final String[] arrAddressDetail = new String[] { "", "", "", "", "" };
        for (final ObjectValueHolder ovh : allValues) {
            final String qualifier = ovh.getPropertyDescriptor().getQualifier();
            final Object currentValue = ovh.getCurrentValue();
            if (qualifier.equals(TgtcsConstants.QUALIFIER_ADDRESS_LINE1)) {
                arrAddressDetail[0] = currentValue == null ? "" : currentValue.toString();
                continue;
            }
            if (qualifier.equals(TgtcsConstants.QUALIFIER_ADDRESS_TOWN)) {
                arrAddressDetail[1] = currentValue == null ? "" : currentValue.toString();
                continue;
            }
            if (qualifier.equals(TgtcsConstants.QUALIFIER_ADDRESS_DISTRICT)) {
                arrAddressDetail[2] = currentValue == null ? "" : currentValue.toString();
                continue;
            }
            if (qualifier.equals(TgtcsConstants.QUALIFIER_ADDRESS_POSTALCODE)) {
                arrAddressDetail[3] = currentValue == null ? "" : currentValue.toString();
                continue;
            }
            if (qualifier.equals(TgtcsConstants.QUALIFIER_ADDRESS_COUNTRY)) {
                CountryModel currentCountry = null;
                if (ovh.getCurrentValue() instanceof CountryModel) {
                    currentCountry = (CountryModel)ovh.getCurrentValue();
                }
                else if (ovh.getCurrentValue() instanceof ResultObjectWrapper) {
                    final ResultObjectWrapper wrapper = (ResultObjectWrapper)ovh.getCurrentValue();
                    currentCountry = (CountryModel)wrapper.getObject();
                }
                else if (ovh.getCurrentValue() instanceof TypedObject) {
                    final TypedObject wrapper = (TypedObject)ovh.getCurrentValue();
                    currentCountry = (CountryModel)wrapper.getObject();
                }
                arrAddressDetail[4] = currentCountry == null ? "" : currentCountry.getName();
                continue;
            }
        }
        builderAddress.append(arrAddressDetail[0]);
        builderAddress.append(", ");
        builderAddress.append(arrAddressDetail[1]);
        builderAddress.append(", ");
        builderAddress.append(arrAddressDetail[2]);
        builderAddress.append(", ");
        builderAddress.append(arrAddressDetail[3]);
        builderAddress.append(", ");
        builderAddress.append(arrAddressDetail[4]);
        // call QAS service to validate address
        try {
            return targetAddressVerificationService.verifyAddress(builderAddress.toString(), CountryEnum.AUSTRALIA);
        }
        catch (final ServiceNotAvailableException e) {
            LOG.debug("QAS Service exception: ", e);
        }
        catch (final RequestTimeOutException e) {
            LOG.debug("QAS Service exception: ", e);
        }
        catch (final NoMatchFoundException e) {
            LOG.debug("QAS Service exception: ", e);
        }
        catch (final TooManyMatchesFoundException e) {
            LOG.debug("QAS Service exception: ", e);
        }
        return new ArrayList<>();
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtcs.widgets.controllers.AddressCustomerController#updateAddressByQASFormattedAddress(de.hybris.platform.cockpit.services.values.ObjectValueContainer, java.lang.String)
     */
    @Override
    public void updateAddressByQASFormattedAddress(final ObjectValueContainer addressContainer,
            final String addressString) {
        ObjectValueHolder ovhLine1 = null;
        ObjectValueHolder ovhCity = null;
        ObjectValueHolder ovhState = null;
        ObjectValueHolder ovhPostCode = null;
        ObjectValueHolder ovhAddressValidated = null;

        for (final ObjectValueHolder ovh : addressContainer.getAllValues()) {
            switch (ovh.getPropertyDescriptor().getQualifier()) {
                case TgtcsConstants.QUALIFIER_ADDRESS_LINE1:
                    ovhLine1 = ovh;
                    break;
                case TgtcsConstants.QUALIFIER_ADDRESS_TOWN:
                    ovhCity = ovh;
                    break;
                case TgtcsConstants.QUALIFIER_ADDRESS_DISTRICT:
                    ovhState = ovh;
                    break;
                case TgtcsConstants.QUALIFIER_ADDRESS_POSTALCODE:
                    ovhPostCode = ovh;
                    break;
                case TgtcsConstants.QUALIFIER_TARGETADDRESS_ADDRESSVALIDATED:
                    ovhAddressValidated = ovh;
                    break;
                default:
                    break;
            }
        }
        final int index = addressString.lastIndexOf(',');
        if (ovhLine1 != null) {
            final String line1 = addressString.substring(0, index);
            ovhLine1.setLocalValue(line1);
            ovhLine1.setModified(true);
        }
        if (ovhCity != null && ovhState != null && ovhPostCode != null) {
            final String cityStatePostcode = addressString.substring(index + 1).trim();
            final String[] arrCityStatePostcode = cityStatePostcode.split("\\s\\s");
            final String city = arrCityStatePostcode[0];
            final String state = arrCityStatePostcode[1];
            final String postcode = arrCityStatePostcode[2];
            ovhCity.setLocalValue(city);
            ovhCity.setModified(true);
            ovhState.setLocalValue(state);
            ovhState.setModified(true);
            ovhPostCode.setLocalValue(postcode);
            ovhPostCode.setModified(true);
        }
        if (ovhAddressValidated != null) {
            ovhAddressValidated.setLocalValue(Boolean.TRUE);
            ovhAddressValidated.setModified(true);
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcs.widgets.controllers.AddressCustomerController#checkValidAddress(java.lang.String)
     */
    @Override
    public List<AddressData> checkValidAddress(final String addressString) {
        // call QAS service to validate address
        try {
            return targetAddressVerificationService.verifyAddress(addressString, CountryEnum.AUSTRALIA);
        }
        catch (final ServiceNotAvailableException | RequestTimeOutException | NoMatchFoundException
                | TooManyMatchesFoundException e) {
            LOG.debug("QAS Service exception: ", e);
        }
        return new ArrayList<>();
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcs.widgets.controllers.AddressCustomerController#updateAddressByQASFormattedAddress(java.util.List, java.lang.String)
     */
    @Override
    public void updateAddressByQASFormattedAddress(final List<InputElement> editors, final String addressString) {
        final int index = addressString.lastIndexOf(',');
        final InputElement elementLine = editors.get(0);
        final String line1 = addressString.substring(0, index);
        elementLine.setRawValue(line1);
        Events.postEvent(new Event("onEdit", elementLine));
        final String cityStatePostcode = addressString.substring(index + 1).trim();
        final String[] arrCityStatePostcode = cityStatePostcode.split("\\s\\s");
        final String city = arrCityStatePostcode[0];
        final String state = arrCityStatePostcode[1];
        final String postcode = arrCityStatePostcode[2];
        final InputElement elementCity = editors.get(1);
        elementCity.setRawValue(city);
        Events.postEvent(new Event("onEdit", elementCity));
        final InputElement elementState = editors.get(2);
        elementState.setRawValue(state);
        Events.postEvent(new Event("onEdit", elementState));
        final InputElement elementPostalCode = editors.get(3);
        elementPostalCode.setRawValue(postcode);
        Events.postEvent(new Event("onEdit", elementPostalCode));
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcs.widgets.controllers.TargetCallContextController#isAddAddressInCheckoutTab()
     */
    @Override
    public boolean isAddAddressInCheckoutTab() {
        return addAddressInCheckoutTab;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcs.widgets.controllers.TargetCallContextController#setAddAddressInCheckoutTab(boolean)
     */
    @Override
    public void setAddAddressInCheckoutTab(final boolean addAddressInCheckoutTab) {
        this.addAddressInCheckoutTab = addAddressInCheckoutTab;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcs.widgets.controllers.TargetCallContextController#returnBasketController()
     */
    @Override
    public TargetBasketController returnBasketController() {
        return (TargetBasketController)getBasketController();
    }

    @Override
    protected void transferAnonymousCartToCustomer(final TypedObject customer) {
        if (getCurrentCustomer() == null) {
            final CartModel cartModel = returnBasketController().getMasterCart();

            final TypedObject cartObject = getCockpitTypeService().wrapItem(cartModel);

            if (isAnonymousCustomerCart(cartObject)) {
                final String cardCode = getAnonymousCartCode();
                if (CollectionUtils.isNotEmpty(cartModel.getEntries())) {
                    getBasketController().transferAnonymousCartToCustomer(cardCode, customer);
                }
            }
        }
    }

    @Override
    public void sendUidChangedEmail(final String oldUid) {
        final CustomerModel customer = TypeUtils.unwrapItem(getCurrentCustomer(), CustomerModel.class);
        ((TargetCustomerAccountService)getCustomerAccountService()).createAndPublishUpdateCustomerEmailAddrEventForCS(
                oldUid,
                customer);
    }

    /**
     * @param targetAddressVerificationService
     *            the targetAddressVerificationService to set
     */
    @Required
    public void setTargetAddressVerificationService(
            final TargetAddressVerificationService targetAddressVerificationService) {
        this.targetAddressVerificationService = targetAddressVerificationService;
    }
}

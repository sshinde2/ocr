package au.com.target.tgtcs.widgets.controllers.strategies.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.cscockpit.utils.TypeUtils;
import de.hybris.platform.cscockpit.widgets.controllers.strategies.impl.DefaultBasketStrategy;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcs.widgets.controllers.strategies.TargetBasketStrategy;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.purchase.PurchaseOptionConfigService;
import au.com.target.tgtlayby.purchase.PurchaseOptionService;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


/**
 * Strategy for loading and creating carts
 */
public class TargetBasketStrategyImpl extends DefaultBasketStrategy implements TargetBasketStrategy {

    private TargetCommerceCartService targetCommerceCartService;

    private ConfigurationService configurationService;

    private TargetLaybyCartService targetLaybyCartService;

    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;

    private String purchaseOptionCode;

    private PurchaseOptionService purchaseOptionService;

    private PurchaseOptionConfigService purchaseOptionConfigService;

    @Override
    public CartModel createCart(final CustomerModel customer) {
        CartModel cart = getPersistedMasterCart(customer);

        // creating new cart if there is no carts for given customer
        if (cart == null) {
            final TypedObject customerObj = getCockpitTypeService().wrapItem(customer);
            final TypedObject cartObj = createCart(customerObj);
            cart = TypeUtils.unwrapItem(cartObj, CartModel.class);
        }

        return cart;
    }

    @Override
    public TypedObject loadCart(final TypedObject customer, final String cartCode) {
        if (customer != null) {
            TypedObject typedObjectCart = getCart(customer, cartCode);
            if (typedObjectCart == null) {
                typedObjectCart = createCart(customer, cartCode);
            }
            final CartModel masterCart = TypeUtils.unwrapItem(typedObjectCart, CartModel.class);
            return getCockpitTypeService().wrapItem(masterCart);
        }
        return null;
    }

    /**
     * Get customer checkout card by master card
     * 
     * @param customer
     */
    @Override
    public CartModel getCart(final CustomerModel customer) {

        final CartModel masterCart = getPersistedMasterCart(customer);
        if (masterCart == null) {
            return null;
        }
        final PurchaseOptionConfigModel config = purchaseOptionConfigService.getCurrentPurchaseOptionConfig();
        masterCart.setPurchaseOptionConfig(config);
        getModelService().save(masterCart);
        return masterCart;
    }


    /**
     * get master cart by customer
     * 
     * @param customer
     * @return CartModel
     */
    @Override
    public CartModel getPersistedMasterCart(final CustomerModel customer) {
        getModelService().refresh(customer);
        return targetLaybyCartService.getMostRecentCartForCustomer(customer);
    }

    @Override
    public TypedObject getCart(final TypedObject customer, final String cartCode)
    {
        if (customer != null)
        {
            final CustomerModel customerModel = TypeUtils.unwrapItem(customer, CustomerModel.class);
            final CartModel cartModel = getCart(customerModel, cartCode);
            if (cartModel != null)
            {
                return getCockpitTypeService().wrapItem(cartModel);
            }
        }
        return null;
    }

    /**
     * @return the targetCommerceCartService
     */
    public TargetCommerceCartService getTargetCommerceCartService() {
        return targetCommerceCartService;
    }

    /**
     * @param targetCommerceCartService
     *            the targetCommerceCartService to set
     */
    public void setTargetCommerceCartService(final TargetCommerceCartService targetCommerceCartService) {
        this.targetCommerceCartService = targetCommerceCartService;
    }

    /**
     * @return the configurationService
     */
    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    /**
     * @param configurationService
     *            the configurationService to set
     */
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    /**
     * @param targetLaybyCartService
     *            the targetLaybyCartService to set
     */
    @Required
    public void setTargetLaybyCartService(final TargetLaybyCartService targetLaybyCartService) {
        this.targetLaybyCartService = targetLaybyCartService;
    }

    /**
     * @param targetPurchaseOptionHelper
     *            the targetPurchaseOptionHelper to set
     */
    @Required
    public void setTargetPurchaseOptionHelper(final TargetPurchaseOptionHelper targetPurchaseOptionHelper) {
        this.targetPurchaseOptionHelper = targetPurchaseOptionHelper;
    }

    /**
     * @return the targetPurchaseOptionHelper
     */
    public TargetPurchaseOptionHelper getTargetPurchaseOptionHelper() {
        return targetPurchaseOptionHelper;
    }

    @Override
    public String getPurchaseOptionCode() {
        return purchaseOptionCode;
    }

    /**
     * @param purchaseOptionCode
     *            the purchaseOptionCode to set
     */
    @Required
    public void setPurchaseOptionCode(final String purchaseOptionCode) {
        this.purchaseOptionCode = purchaseOptionCode;
    }

    /**
     * @return the purchaseOptionService
     */
    public PurchaseOptionService getPurchaseOptionService() {
        return purchaseOptionService;
    }

    /**
     * @param purchaseOptionService
     *            the purchaseOptionService to set
     */
    @Required
    public void setPurchaseOptionService(final PurchaseOptionService purchaseOptionService) {
        this.purchaseOptionService = purchaseOptionService;
    }

    /**
     * @return the purchaseOptionConfigService
     */
    public PurchaseOptionConfigService getPurchaseOptionConfigService() {
        return purchaseOptionConfigService;
    }

    /**
     * @param purchaseOptionConfigService
     *            the purchaseOptionConfigService to set
     */
    @Required
    public void setPurchaseOptionConfigService(final PurchaseOptionConfigService purchaseOptionConfigService) {
        this.purchaseOptionConfigService = purchaseOptionConfigService;
    }

}

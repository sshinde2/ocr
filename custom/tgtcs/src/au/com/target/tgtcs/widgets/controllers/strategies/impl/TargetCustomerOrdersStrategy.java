/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers.strategies.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.cscockpit.widgets.controllers.strategies.impl.DefaultCustomerOrdersStrategy;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Assert;


/**
 * Target Customer Order Strategy
 * 
 */
public class TargetCustomerOrdersStrategy extends DefaultCustomerOrdersStrategy {

    @Override
    public Collection<OrderModel> getCustomerOrders(final CustomerModel customer) {
        final Collection<OrderModel> orders = super.getCustomerOrders(customer);
        Assert.assertTrue(orders instanceof List);
        Collections.sort((List)orders, new Comparator<OrderModel>() {
            @Override
            public int compare(final OrderModel o1, final OrderModel o2) {
                if (o1.getCode() == null) {
                    return 1;
                }
                if (o2.getCode() == null) {
                    return -1;
                }
                return -o1.getCode().compareTo(o2.getCode());

            }
        });
        return orders;
    }



}

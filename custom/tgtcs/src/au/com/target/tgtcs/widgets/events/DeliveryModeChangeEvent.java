package au.com.target.tgtcs.widgets.events;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.widgets.events.WidgetEvent;


public class DeliveryModeChangeEvent extends WidgetEvent {

    private final TypedObject newDeliveryMode;
    private final TypedObject order;

    /**
     * Constructor
     * 
     * @param source
     *            the source of the event
     * @param context
     *            the context of the event
     * @param newDeliveryMode
     *            the new DeliveryMode for the order
     * @param order
     *            the order the DeliveryMode was changed for
     */
    public DeliveryModeChangeEvent(final Object source, final String context,
            final TypedObject newDeliveryMode, final TypedObject order) {
        super(source, context);
        this.newDeliveryMode = newDeliveryMode;
        this.order = order;
    }

    /**
     * @return the newDeliveryMode
     */
    public TypedObject getNewDeliveryMode() {
        return newDeliveryMode;
    }

    /**
     * @return the order
     */
    public TypedObject getOrder() {
        return order;
    }
}

package au.com.target.tgtcs.widgets.events;

import de.hybris.platform.cockpit.widgets.events.WidgetEvent;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;

import java.util.List;


public class HighlightItemsEvent extends WidgetEvent {

    private final List<OrderEntryData> offendingItems;

    /**
     * 
     * @param source
     *            the source of the event
     * @param context
     *            the context of the event
     * @param offendingItems
     *            the order items to be highlighted
     */
    public HighlightItemsEvent(final Object source, final String context,
            final List<OrderEntryData> offendingItems) {
        super(source, context);
        this.offendingItems = offendingItems;
    }

    /**
     * @return the offendingItems
     */
    public List<OrderEntryData> getOffendingItems() {
        return offendingItems;
    }

}

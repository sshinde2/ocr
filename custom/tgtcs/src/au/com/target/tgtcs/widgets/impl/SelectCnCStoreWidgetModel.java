package au.com.target.tgtcs.widgets.impl;

import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.cscockpit.widgets.models.impl.CheckoutCartWidgetModel;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcs.dto.CnCDetailsFormData;


public class SelectCnCStoreWidgetModel extends CheckoutCartWidgetModel {

    public static final String TITLE_ITEM_ID = "title";
    public static final String FIRSTNAME_ITEM_ID = "firstName";
    public static final String LASTNAME_ITEM_ID = "lastName";
    public static final String PHONE_ITEM_ID = "phone";
    public static final String STATE_ITEM_ID = "state";
    public static final String STORE_ITEM_ID = "store";

    private DeliveryModeModel selectedDeliveryMode;

    private final CnCDetailsFormData cncFormData = new CnCDetailsFormData();

    /**
     * @return the cncFormData
     */
    public CnCDetailsFormData getCncFormData() {
        return cncFormData;
    }

    /**
     * @return the selectedDeliveryMode
     */
    public DeliveryModeModel getSelectedDeliveryMode() {
        return selectedDeliveryMode;
    }

    /**
     * @param selectedDeliveryMode
     *            the selectedDeliveryMode to set
     */
    public void setSelectedDeliveryMode(final DeliveryModeModel selectedDeliveryMode) {
        this.selectedDeliveryMode = selectedDeliveryMode;
    }

    /**
     * @return the state
     */
    public String getState() {
        return cncFormData.getState();
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(final String state) {
        cncFormData.setState(state);
    }

    /**
     * @return the store
     */
    public TargetPointOfServiceModel getStore() {
        return cncFormData.getStore();
    }

    /**
     * @param store
     *            the store to set
     */
    public void setStore(final TargetPointOfServiceModel store) {
        cncFormData.setStore(store);
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return cncFormData.getFirstName();
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        cncFormData.setFirstName(firstName);
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return cncFormData.getLastName();
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        cncFormData.setLastName(lastName);
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return cncFormData.getPhone();
    }

    /**
     * @param phone
     *            the phone to set
     */
    public void setPhone(final String phone) {
        cncFormData.setPhone(phone);
    }

    /**
     * @return the title
     */
    public TitleModel getTitle() {
        return cncFormData.getTitle();
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(final TitleModel title) {
        cncFormData.setTitle(title);
    }
}

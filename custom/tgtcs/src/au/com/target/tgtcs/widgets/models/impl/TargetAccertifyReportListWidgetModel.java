/**
 * 
 */
package au.com.target.tgtcs.widgets.models.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.widgets.models.impl.DefaultListWidgetModel;

import org.springframework.util.ObjectUtils;


/**
 * @author umesh
 * 
 */
public class TargetAccertifyReportListWidgetModel extends DefaultListWidgetModel<TypedObject> {
    private TypedObject order;

    /**
     * @return TypedObject
     */
    public TypedObject getOrder()
    {
        return this.order;
    }

    /**
     * @param orderObj
     * @return boolean
     */
    public boolean setOrder(final TypedObject orderObj)
    {
        boolean changed = false;
        if (!(ObjectUtils.nullSafeEquals(this.order, orderObj)))
        {
            changed = true;
            this.order = orderObj;
        }
        return changed;
    }
}

package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.events.CockpitEvent;
import de.hybris.platform.cockpit.events.CockpitEventAcceptor;
import de.hybris.platform.cockpit.events.impl.ItemChangedEvent;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.config.EditorConfiguration;
import de.hybris.platform.cockpit.services.config.EditorRowConfiguration;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.widgets.InputWidget;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cockpit.widgets.controllers.WidgetController;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.cscockpit.utils.CockpitUiConfigLoader;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.utils.TypeUtils;
import de.hybris.platform.cscockpit.widgets.controllers.CustomerController;
import de.hybris.platform.cscockpit.widgets.controllers.impl.DefaultCallContextController;
import de.hybris.platform.cscockpit.widgets.models.impl.CustomerItemWidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.impl.CustomerDetailsEditWidgetRenderer;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcs.widgets.controllers.EmailCustomerController;


/**
 * Customer details edit renderer that supports editing customer attributes
 */
public class AcceleratorCustomerDetailsEditWidgetRenderer extends CustomerDetailsEditWidgetRenderer
{
    private static final String EMAIL_FIELD = "email";

    private static final Logger LOG = Logger.getLogger(AcceleratorCustomerDetailsEditWidgetRenderer.class);
    private final List<String> excludedFields = Arrays.asList("SMS", "SUBSCRIBE TO EMAIL", "COMPETITION");
    private final List<String> nameFields = Arrays.asList("FIRSTNAME", "LASTNAME");

    private CustomerNameStrategy customerNameStrategy;

    private final CockpitEventAcceptor onCustomerChanged = new CockpitEventAcceptor() {

        @Override
        public void onCockpitEvent(final CockpitEvent event) {
            if (!(event.getSource() instanceof Widget)) {
                return;
            }
            final WidgetController controller = ((Widget)event.getSource()).getWidgetController();
            if (event instanceof ItemChangedEvent && controller instanceof CustomerController) {
                final ItemChangedEvent changeEvent = (ItemChangedEvent)event;
                for (final PropertyDescriptor descriptor : changeEvent.getProperties()) {
                    if (isPropertyIncludedInNames(descriptor.getQualifier())) {
                        final TargetCustomerModel customer = ((TargetCustomerModel)(
                                ((CustomerController)controller)
                                        .getCurrentCustomer().getObject()));
                        customer.setName(customerNameStrategy.getName(customer.getFirstname(), customer.getLastname()));
                        UISessionUtils.getCurrentSession().getModelService().save(customer);
                    }
                }
            }
            return;
        }
    };

    @PostConstruct
    public void initIt() throws Exception {
        UISessionUtils.getCurrentSession().getCurrentPerspective().addCockpitEventAcceptor(onCustomerChanged);
    }

    @PreDestroy
    public void cleanUp() throws Exception {
        UISessionUtils.getCurrentSession().getCurrentPerspective().removeCockpitEventAcceptor(onCustomerChanged);
    }


    /**
     * @return the customerNameStrategy
     */
    public CustomerNameStrategy getCustomerNameStrategy() {
        return customerNameStrategy;
    }

    /**
     * @param customerNameStrategy
     *            the customerNameStrategy to set
     */
    @Required
    public void setCustomerNameStrategy(final CustomerNameStrategy customerNameStrategy) {
        this.customerNameStrategy = customerNameStrategy;
    }

    /*(non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.renderers.impl.CustomerDetailsEditWidgetRenderer#createContentInternal
     * (de.hybris.platform.cockpit.widgets.InputWidget, org.zkoss.zk.ui.api.HtmlBasedComponent)
     */
    @Override
    protected HtmlBasedComponent createContentInternal(
            final InputWidget<CustomerItemWidgetModel, CustomerController> widget,
            final HtmlBasedComponent rootContainer) {

        final Div content = new Div();
        final CustomerItemWidgetModel widgetModel = widget.getWidgetModel();
        if (widgetModel != null) {
            final TypedObject customer = widgetModel.getCustomer();
            if (customer != null && customer.getObject() instanceof TargetCustomerModel) {
                loadAndCreateTargetEditors(widget, content, customer);
                renderFooter(content, widget);
            }
            else {
                if (customer != null && customer.getObject() instanceof CustomerModel) {
                    super.loadAndCreateEditors(widget, content, customer);
                    renderFooter(content, widget);
                }
                else {
                    content.appendChild(new Label(LabelUtils.getLabel(widget, "noCustomerSelected")));
                }
            }
        }
        return content;
    }

    /**
     * generate edit customer widget
     * 
     * @param widget
     *            edit customer widget
     * @param parent
     *            html component
     * @param item
     *            customer item
     */
    private void loadAndCreateTargetEditors(final InputWidget<CustomerItemWidgetModel, CustomerController> widget,
            final HtmlBasedComponent parent, final TypedObject item) {
        loadAndCreateTargetEditors(widget, parent, item, getEditorConfigurationCode());
    }

    /**
     * generate edit customer widget
     * 
     * @param widget
     *            edit customer widget
     * @param parent
     *            html component
     * @param item
     *            customer item
     * @param configurationCode
     *            editor configuration code that defined in spring
     */
    private void loadAndCreateTargetEditors(final InputWidget<CustomerItemWidgetModel, CustomerController> widget,
            final HtmlBasedComponent parent, final TypedObject item,
            final String configurationCode) {
        final EditorConfiguration editorConf = CockpitUiConfigLoader.getEditorConfiguration(
                UISessionUtils.getCurrentSession(),
                configurationCode, item.getType().getCode());

        final List<EditorRowConfiguration> editors = getPropertyEditorHelper()
                .getAllEditorRowConfigurations(editorConf);

        if ((editors == null) || (editors.isEmpty())) {
            LOG.error("No editor widget configuration entries found.");
        }
        else {
            for (final EditorRowConfiguration editor : editors) {
                final String propertyName = editor.getPropertyDescriptor().getName();
                if (isPropertyIncluded(propertyName)) {
                    final Div editorDiv = new Div();
                    editorDiv.setSclass("editorWidgetEditor");
                    renderEditor(editorDiv, editor, item, editor.getPropertyDescriptor(), widget);
                    if (isCustomerEmailProperty(propertyName)) {
                        final Component textBox = getTextBox(editorDiv);
                        if (textBox != null) {
                            final Iterator listenerItr = textBox.getListenerIterator("onBlur");
                            while (listenerItr.hasNext()) {
                                textBox.removeEventListener("onBlur", (EventListener)listenerItr.next());
                            }
                            textBox.addEventListener("onChange", new ChangeCustomerUIDEventListener(widget));
                        }
                    }
                    if (CollectionUtils.isNotEmpty(editorDiv.getChildren())) {
                        parent.appendChild(editorDiv);
                    }
                }
            }
        }
    }

    private Component getTextBox(final Component parent) {
        if (!(parent.getChildren().isEmpty())) {
            final Component hbox = (Component)parent.getChildren().get(0);
            if (hbox.getChildren().size() > 1) {
                return ((Component)hbox.getChildren().get(1)).getFirstChild();
            }
        }
        return null;
    }

    /**
     * Returns true if propertyName is not in the list of excluded fields
     * 
     * @param propertyName
     *            the property to check
     * @return true if the property is not excluded
     */
    private boolean isPropertyIncluded(final String propertyName) {
        for (final String excludedField : excludedFields) {
            if (propertyName.toUpperCase().contains(excludedField)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns true if propertyName is in the list of nameFields
     * 
     * @param propertyName
     *            the property to check
     * @return true if the property is include
     */
    private boolean isPropertyIncludedInNames(final String propertyName) {
        Assert.notNull(propertyName, "propertyName is required; it must not be null");
        for (final String includedField : nameFields) {
            if (propertyName.toUpperCase().contains(includedField)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns true if propertyName contains "email"
     * 
     * @param propertyName
     *            the property to check
     * @return true if propertyName contains "email"
     */
    private boolean isCustomerEmailProperty(final String propertyName) {
        Assert.notNull(propertyName, "propertyName is required; it must not be null");
        return propertyName.toLowerCase().contains(EMAIL_FIELD);
    }


    private class ChangeCustomerUIDEventListener implements EventListener {

        private final InputWidget<CustomerItemWidgetModel, CustomerController> widget;

        public ChangeCustomerUIDEventListener(final InputWidget<CustomerItemWidgetModel, CustomerController> widget) {
            this.widget = widget;
        }

        @Override
        public void onEvent(final Event event) throws Exception {
            final Textbox textbox = (Textbox)event.getTarget();
            final DefaultCallContextController callContextController = (DefaultCallContextController)widget
                    .getWidgetController();
            final CustomerModel customer = TypeUtils.unwrapItem(callContextController.getCurrentCustomer(),
                    CustomerModel.class);
            final String newUid = textbox.getValue();
            final String oldUid = customer.getUid();
            if (oldUid.equals(newUid)) {
                return;
            }
            customer.setUid(newUid);
            try {
                UISessionUtils.getCurrentSession().getModelService().save(customer);
            }
            catch (final ModelSavingException e) {
                final String message = e.getCause().getMessage();
                Messagebox.show(message,
                        LabelUtils.getLabel(widget, "unableToSaveItem", new Object[0]),
                        1, "z-msgbox z-msgbox-error");
                return;
            }

            ((EmailCustomerController)callContextController).sendUidChangedEmail(oldUid);
        }

    }
}

package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.renderers.impl.AbstractCsWidgetRenderer;

import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;


/**
 * @author Dell
 * 
 */
public class ConfirmResetPasswordEmailWidgetRenderer extends AbstractCsWidgetRenderer<Widget> {

    protected static final String SUCCESS_LABEL_KEY_POSTFIX = "successLabel";

    /* (non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.renderers.impl.AbstractCsWidgetRenderer#createContentInternal(de.hybris.platform.cockpit.widgets.Widget, org.zkoss.zk.ui.api.HtmlBasedComponent)
     */
    @Override
    protected HtmlBasedComponent createContentInternal(final Widget widget,
            final HtmlBasedComponent paramHtmlBasedComponent) {
        final Div content = new Div();
        content.appendChild(new Label(LabelUtils.getLabel(widget, SUCCESS_LABEL_KEY_POSTFIX)));
        return content;
    }

}

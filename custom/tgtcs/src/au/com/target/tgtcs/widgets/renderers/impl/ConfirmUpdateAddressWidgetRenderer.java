package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.renderers.impl.AbstractCsWidgetRenderer;
import de.hybris.platform.cscockpit.widgets.renderers.utils.PopupWidgetHelper;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.impl.InputElement;

import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtcs.service.address.TargetAddressService;
import au.com.target.tgtcs.widgets.controllers.AddressCustomerController;
import au.com.target.tgtverifyaddr.data.AddressData;


/**
 * @author Dell
 * 
 */
public class ConfirmUpdateAddressWidgetRenderer extends AbstractCsWidgetRenderer {

    private List<AddressData> addresses;
    private PopupWidgetHelper popupWidgetHelper;
    private List<InputElement> editors;
    private InputElement mainEditor;
    private TargetAddressModel addressModel;
    private TargetAddressService targetAddressService;

    /**
     * @return the popupWidgetHelper
     */
    public PopupWidgetHelper getPopupWidgetHelper() {
        return popupWidgetHelper;
    }

    /**
     * @param popupWidgetHelper
     *            the popupWidgetHelper to set
     */
    @Required
    public void setPopupWidgetHelper(final PopupWidgetHelper popupWidgetHelper) {
        this.popupWidgetHelper = popupWidgetHelper;
    }

    /**
     * @return the targetAddressService
     */
    public TargetAddressService getTargetAddressService() {
        return targetAddressService;
    }

    /**
     * @param targetAddressService
     *            the targetAddressService to set
     */
    @Required
    public void setTargetAddressService(final TargetAddressService targetAddressService) {
        this.targetAddressService = targetAddressService;
    }

    /**
     * @param potentialAddresses
     *            the potential addresses to set
     */
    public void setPotentialAddresses(final List<AddressData> potentialAddresses) {
        this.addresses = potentialAddresses;
    }

    /**
     * @param editors
     *            the editors to set
     */
    public void setEditors(final List<InputElement> editors) {
        this.editors = editors;
    }

    /**
     * @param mainEditor
     *            the mainEditor to set
     */
    public void setMainEditor(final InputElement mainEditor) {
        this.mainEditor = mainEditor;
    }

    /**
     * @param addressModel
     *            the addressModel to set
     */
    public void setAddressModel(final TargetAddressModel addressModel) {
        this.addressModel = addressModel;
    }

    /* (non-Javadoc)
    * @see de.hybris.platform.cscockpit.widgets.renderers.impl.AbstractCsWidgetRenderer#createContentInternal(de.hybris.platform.cockpit.widgets.Widget, org.zkoss.zk.ui.api.HtmlBasedComponent)
    */
    @Override
    protected HtmlBasedComponent createContentInternal(final Widget widget, final HtmlBasedComponent rootContainer) {
        final Div content = new Div();
        final Radiogroup radiogroupAddress = new Radiogroup();
        for (final AddressData data : addresses) {
            final Radio radio = new Radio(data.getPartialAddress());
            radio.setParent(radiogroupAddress);
        }
        final Radio radio = new Radio(LabelUtils.getLabel(widget,
                TgtcsConstants.ENTERED_ADDRESS_RADIO_BUTTON_KEY_POSTFIX));
        radio.setParent(radiogroupAddress);
        radiogroupAddress.setSelectedIndex(0);
        content.appendChild(radiogroupAddress);
        final Button buttonSelect = new Button(LabelUtils.getLabel(widget, TgtcsConstants.SELECT_BUTTON_KEY_POSTFIX));
        buttonSelect.addEventListener("onClick", new EventListener()
        {
            @Override
            public void onEvent(final Event event) throws Exception
            {
                ConfirmUpdateAddressWidgetRenderer.this.handleSelectAddressClickEvent(widget, event);
            }
        });
        content.appendChild(buttonSelect);
        return content;
    }

    protected void handleSelectAddressClickEvent(final Widget widget, final Event event) throws Exception {
        // get selected address
        final Radiogroup radiogroupAddress = (Radiogroup)event.getTarget().getPreviousSibling();
        final String addressString = radiogroupAddress.getSelectedItem().getLabel();
        final String enteredAddressLabel = LabelUtils
                .getLabel(widget, TgtcsConstants.ENTERED_ADDRESS_RADIO_BUTTON_KEY_POSTFIX);
        if (!addressString.equals(enteredAddressLabel)) {
            ((AddressCustomerController)widget.getWidgetController()).updateAddressByQASFormattedAddress(editors,
                    addressString);
            targetAddressService.setAddressValidated(addressModel, Boolean.TRUE);
        }
        else {
            Events.postEvent(new Event("onEdit", mainEditor));
            targetAddressService.setAddressValidated(addressModel, Boolean.FALSE);
        }
        getPopupWidgetHelper().dismissCurrentPopup();
    }
}

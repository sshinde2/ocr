package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cockpit.widgets.models.MapWidgetModel;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.renderers.impl.AbstractCsWidgetRenderer;
import de.hybris.platform.cscockpit.widgets.renderers.utils.PopupWidgetHelper;

import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcs.widgets.controllers.EmailCustomerController;
import au.com.target.tgtcs.widgets.controllers.impl.TgtcsCallContextController;


/**
 * @author Dell
 * 
 */
public class ResetPasswordWidgetRenderer extends
        AbstractCsWidgetRenderer<Widget<MapWidgetModel<String, String>, EmailCustomerController>> {

    protected static final String RESET_BUTTON_KEY_POSTFIX = "resetButton";
    protected static final String CONFIRM_POPUP_KEY_POSTFIX = "confirmPopup";

    private PopupWidgetHelper popupWidgetHelper;

    protected PopupWidgetHelper getPopupWidgetHelper()
    {
        return this.popupWidgetHelper;
    }

    @Required
    public void setPopupWidgetHelper(final PopupWidgetHelper popupWidgetHelper)
    {
        this.popupWidgetHelper = popupWidgetHelper;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.renderers.impl.AbstractCsWidgetRenderer#createContentInternal(de.hybris.platform.cockpit.widgets.Widget, org.zkoss.zk.ui.api.HtmlBasedComponent)
     */
    @Override
    protected HtmlBasedComponent createContentInternal(final Widget widget,
            final HtmlBasedComponent paramHtmlBasedComponent) {
        final Div content = new Div();
        final Button resetPasswordButton = new Button(LabelUtils.getLabel(widget, RESET_BUTTON_KEY_POSTFIX));
        content.appendChild(resetPasswordButton);
        resetPasswordButton.setDisabled(isGuestUser(widget));
        resetPasswordButton.addEventListener("onClick", new EventListener()
        {
            @Override
            public void onEvent(final Event event) throws Exception
            {
                ResetPasswordWidgetRenderer.this.handleResetPasswordClickEvent(widget, event);
            }
        });
        return content;
    }

    private boolean isGuestUser(final Widget widget) {
        final TgtcsCallContextController widgetController = (TgtcsCallContextController)widget.getWidgetController();
        final TypedObject currentCustomer = widgetController.getCurrentCustomer();
        if (currentCustomer == null) {
            return true;
        }
        final TargetCustomerModel targetCutomerModel = (TargetCustomerModel)currentCustomer.getObject();
        if (targetCutomerModel == null) {
            return true;
        }
        return CustomerType.GUEST.equals(targetCutomerModel.getType());
    }

    /**
     * @param widget
     * @param event
     */
    protected void handleResetPasswordClickEvent(final Widget widget, final Event event) {
        ((EmailCustomerController)widget.getWidgetController()).sendResetPasswordEmail();
        // display confirm dialog
        this.popupWidgetHelper.createPopupWidget(widget, "csConfirmResetPasswordEmailWidgetConfig",
                "csConfirmResetPasswordEmailWidgetConfig-Popup", "csConfirmResetPasswordEmailPopup",
                LabelUtils.getLabel(widget, CONFIRM_POPUP_KEY_POSTFIX));
    }
}

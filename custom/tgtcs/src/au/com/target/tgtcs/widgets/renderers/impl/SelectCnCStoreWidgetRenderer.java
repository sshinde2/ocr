package au.com.target.tgtcs.widgets.renderers.impl;

import static au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel.FIRSTNAME_ITEM_ID;
import static au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel.LASTNAME_ITEM_ID;
import static au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel.PHONE_ITEM_ID;
import static au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel.STATE_ITEM_ID;
import static au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel.STORE_ITEM_ID;
import static au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel.TITLE_ITEM_ID;

import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cockpit.widgets.impl.DefaultWidget;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.renderers.impl.AbstractCsWidgetRenderer;
import de.hybris.platform.cscockpit.widgets.renderers.utils.PopupWidgetHelper;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Column;
import org.zkoss.zul.Columns;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;

import au.com.target.tgtcs.listeners.StateChangeEventListener;
import au.com.target.tgtcs.listeners.StoreChangeEventListener;
import au.com.target.tgtcs.util.TargetCsCockpitUtil;
import au.com.target.tgtcs.widgets.binding.AbstractTgtcsBinder;
import au.com.target.tgtcs.widgets.binding.CnCPopupDataBinder;
import au.com.target.tgtcs.widgets.controllers.impl.TargetCnCStoreSelectController;
import au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel;


public class SelectCnCStoreWidgetRenderer extends
        AbstractCsWidgetRenderer<DefaultWidget<SelectCnCStoreWidgetModel, TargetCnCStoreSelectController>> {
    public static final String CONFIRM_BUTTON_ID = "confirm";

    /**
     * Convention used: error label IDs are produced by concatenating input field ID and <code>ERROR_ID_POSTFIX</code>
     */
    public static final String ERROR_ID_POSTFIX = "_error";

    public static final String FIRSTNAME_ERROR_ID = FIRSTNAME_ITEM_ID + ERROR_ID_POSTFIX;
    public static final String LASTNAME_ERROR_ID = LASTNAME_ITEM_ID + ERROR_ID_POSTFIX;
    public static final String PHONE_ERROR_ID = PHONE_ITEM_ID + ERROR_ID_POSTFIX;
    public static final String STATE_ERROR_ID = STATE_ITEM_ID + ERROR_ID_POSTFIX;
    public static final String STORE_ERROR_ID = STORE_ITEM_ID + ERROR_ID_POSTFIX;
    public static final String ERROR_CSS_CLASS = "error";
    public static final String SUBHEADING_CSS_CLASS = "subheading";

    private static final String[] ERROR_LABEL_IDS = { FIRSTNAME_ERROR_ID, LASTNAME_ERROR_ID, PHONE_ERROR_ID,
            STATE_ERROR_ID, STORE_ERROR_ID };

    private static final String CUSTOMER_DETAILS_LABEL_KEY = "customerDetails";
    private static final String TITLE_LABEL_KEY = "title";
    private static final String FIRSTNAME_LABEL_KEY = "firstName";
    private static final String LASTNAME_LABEL_KEY = "lastName";
    private static final String PHONE_LABEL_KEY = "phone";
    private static final String STORE_DETAILS_LABEL_KEY = "storeDetails";
    private static final String STATE_LABEL_KEY = "state";
    private static final String ADDRESS_LABEL_KEY = "address";
    private static final String CONFIRM_BUTTON_KEY = "confirm";

    private AbstractTgtcsBinder cncDataBinder;

    private PopupWidgetHelper popupWidgetHelper;

    @Override
    protected HtmlBasedComponent createContentInternal(
            final DefaultWidget<SelectCnCStoreWidgetModel, TargetCnCStoreSelectController> widget,
            final HtmlBasedComponent rootContainer) {
        final Div container = new Div();
        final Grid grid = constructGrid();
        final Rows rows = new Rows();

        container.appendChild(grid);
        grid.appendChild(rows);
        Row row = new Row();

        addLabel(null, widget, CUSTOMER_DETAILS_LABEL_KEY, SUBHEADING_CSS_CLASS, row, true);

        rows.appendChild(row);
        row = new Row();

        addLabel(null, widget, TITLE_LABEL_KEY, null, row, true);
        final Combobox titleComboBox = addCombobox(row, TITLE_ITEM_ID);

        rows.appendChild(row);
        row = new Row();

        addLabel(FIRSTNAME_ERROR_ID, widget, null, ERROR_CSS_CLASS, row, false);

        rows.appendChild(row);
        row = new Row();

        addLabel(null, widget, FIRSTNAME_LABEL_KEY, null, row, true);
        final Textbox firstNameText = addTextBox(StringUtils.EMPTY, row, FIRSTNAME_ITEM_ID);

        rows.appendChild(row);
        row = new Row();

        addLabel(LASTNAME_ERROR_ID, widget, null, ERROR_CSS_CLASS, row, false);

        rows.appendChild(row);
        row = new Row();

        addLabel(null, widget, LASTNAME_LABEL_KEY, null, row, true);
        final Textbox lastNameText = addTextBox(StringUtils.EMPTY, row, LASTNAME_ITEM_ID);

        rows.appendChild(row);
        row = new Row();

        addLabel(PHONE_ERROR_ID, widget, null, ERROR_CSS_CLASS, row, false);

        rows.appendChild(row);
        row = new Row();

        addLabel(null, widget, PHONE_LABEL_KEY, null, row, true);
        final Textbox phoneText = addTextBox(StringUtils.EMPTY, row, PHONE_ITEM_ID);

        rows.appendChild(row);
        row = new Row();

        addLabel(null, widget, STORE_DETAILS_LABEL_KEY, SUBHEADING_CSS_CLASS, row, true);
        rows.appendChild(row);


        row = new Row();

        addLabel(STATE_ERROR_ID, widget, null, ERROR_CSS_CLASS, row, false);

        rows.appendChild(row);
        row = new Row();

        addLabel(null, widget, STATE_LABEL_KEY, null, row, true);

        final Combobox stateComboBox = addCombobox(row, STATE_ITEM_ID);


        rows.appendChild(row);
        row = new Row();

        addLabel(STORE_ERROR_ID, widget, null, ERROR_CSS_CLASS, row, false);

        rows.appendChild(row);
        row = new Row();

        addLabel(null, widget, ADDRESS_LABEL_KEY, null, row, true);
        final Combobox storeAddressComboBox = addCombobox(row, STORE_ITEM_ID);

        rows.appendChild(row);
        row = new Row();

        populateTitleCombobox(titleComboBox, widget.getWidgetController().getAllTitles().toArray(new Comboitem[0]));
        populateStateCombobox(stateComboBox, widget);
        populateStoreCombobox(storeAddressComboBox, widget);

        rows.appendChild(row);

        final Button confirmButton = addConfirmButton(container, LabelUtils.getLabel(widget, CONFIRM_BUTTON_KEY));
        addConfirmButtonListener(confirmButton, widget);

        storeAddressComboBox.addEventListener(Events.ON_SELECT, new StoreChangeEventListener(stateComboBox));
        stateComboBox.addEventListener(Events.ON_SELECT, new StateChangeEventListener(storeAddressComboBox));

        cncDataBinder = new CnCPopupDataBinder(widget.getWidgetModel(), titleComboBox, firstNameText, lastNameText,
                phoneText,
                stateComboBox,
                storeAddressComboBox
                );

        hideErrorLabels(widget);

        cncDataBinder.loadAll();

        return container;
    }

    /**
     * Displays error messages on a widget. The messages are displayed on {@link Label} components with specific ID
     * attached to specified widget.
     * 
     * @param errors
     *            the {@link Errors} to display
     * @param widget
     *            the {@link Widget} to display errors on
     */
    public void displayErrors(final Errors errors, final Widget widget) {
        // first - hide previous errors
        hideErrorLabels(widget);

        // then display new ones
        for (final FieldError error : errors.getFieldErrors())
        {
            displayError(error.getField() + ERROR_ID_POSTFIX, Labels.getLabel(error.getCode()), widget);
        }
    }

    private void displayError(final String componentID, final String text, final Widget widget) {
        final Component component = widget.getFellowIfAny(componentID);
        if (component != null && component instanceof Label)
        {
            component.setVisible(true);
            ((Label)component).setValue(text);
        }
    }

    private void hideErrorLabels(final Widget widget) {
        for (final String id : ERROR_LABEL_IDS)
        {
            final Component errorLabel = widget.getFellowIfAny(id);
            if (errorLabel != null)
            {
                errorLabel.setVisible(false);
            }
        }
    }

    private boolean handleCnCConfirmation(
            final Widget<SelectCnCStoreWidgetModel, TargetCnCStoreSelectController> widget) {
        return widget.getWidgetController().getBasketController().handleCnCModeSelection(widget);
    }

    private void addConfirmButtonListener(final Button button,
            final Widget<SelectCnCStoreWidgetModel, TargetCnCStoreSelectController> widget) {
        button.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(final Event event) throws Exception {
                cncDataBinder.saveAll();
                if (handleCnCConfirmation(widget))
                {
                    getPopupWidgetHelper().dismissCurrentPopup();
                }
            }
        });
    }

    private Button addConfirmButton(final Component parent, final String label) {
        final Button confirmbutton = new Button(label);
        return addComponent(confirmbutton, parent, CONFIRM_BUTTON_ID, null, true);
    }


    private void populateStateCombobox(final Combobox comboBox,
            final Widget<SelectCnCStoreWidgetModel, TargetCnCStoreSelectController> widget) {
        Comboitem[] states = new Comboitem[0];
        states = widget.getWidgetController().getStates().toArray(states);
        populateCombobox(comboBox, states);
    }

    private void populateStoreCombobox(final Combobox comboBox,
            final Widget<SelectCnCStoreWidgetModel, TargetCnCStoreSelectController> widget) {
        Comboitem[] stores = new Comboitem[0];
        stores = widget.getWidgetController().getStoresForState(null).toArray(stores);
        populateCombobox(comboBox, stores);
    }

    private void populateTitleCombobox(final Combobox comboBox, final Comboitem... items) {
        comboBox.setId(TITLE_ITEM_ID);
        populateCombobox(comboBox, items);
    }

    private void populateCombobox(final Combobox comboBox, final Comboitem... items) {
        TargetCsCockpitUtil.addAllChildren(comboBox, items);
        comboBox.setSelectedItem(null);
    }

    /**
     * 
     * @param id
     *            the id of the new Label
     * @param widget
     *            the widget that provides context for this Label. Used in localization.
     * @param key
     *            the key of the message to display on the label
     * @param sclass
     *            the CSS class to apply to the label
     * @param parent
     *            the Component to attach this label to
     * @param visible
     *            true, if this label is visible, false otherwise
     * @return constructed Label
     */
    private Label addLabel(final String id,
            final Widget<SelectCnCStoreWidgetModel, TargetCnCStoreSelectController> widget,
            final String key, final String sclass, final Component parent, final boolean visible) {
        final String text = key == null ? StringUtils.EMPTY : LabelUtils.getLabel(widget, key,
                ArrayUtils.EMPTY_OBJECT_ARRAY);
        return addComponent(new Label(text), parent, id, sclass, visible);
    }

    private Textbox addTextBox(final String text, final Component parent, final String id) {
        final Textbox textbox = new Textbox(text);
        return addComponent(textbox, parent, id, null, true);
    }

    private Combobox addCombobox(final Component parent, final String id) {
        final Combobox combobox = new Combobox();
        return addComponent(combobox, parent, id, null, true);
    }

    private <T extends HtmlBasedComponent> T addComponent(final T component, final Component parent, final String id,
            final String cssClass,
            final boolean visible) {
        component.setParent(parent);
        component.setSclass(cssClass);
        component.setVisible(visible);
        if (id != null) {
            component.setId(id);
        }
        return component;
    }

    /**
     * Constructs grid with two columns, 50% wide each.
     * 
     * @return Grid
     */
    private Grid constructGrid() {
        final Grid grid = new Grid();
        final Columns columns = new Columns();

        columns.appendChild(new Column(null, null, "50%"));
        columns.appendChild(new Column(null, null, "50%"));
        grid.appendChild(columns);

        return grid;
    }

    /**
     * @return the popupWidgetHelper
     */
    public PopupWidgetHelper getPopupWidgetHelper() {
        return popupWidgetHelper;
    }

    /**
     * @param popupWidgetHelper
     *            the popupWidgetHelper to set
     */
    @Required
    public void setPopupWidgetHelper(final PopupWidgetHelper popupWidgetHelper) {
        this.popupWidgetHelper = popupWidgetHelper;
    }

}

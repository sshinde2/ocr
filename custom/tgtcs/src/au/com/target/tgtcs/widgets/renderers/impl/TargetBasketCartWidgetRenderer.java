/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.components.StyledDiv;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.util.UITools;
import de.hybris.platform.cockpit.widgets.ListboxWidget;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.cscockpit.exceptions.ValidationException;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.utils.SafeUnbox;
import de.hybris.platform.cscockpit.widgets.controllers.BasketController;
import de.hybris.platform.cscockpit.widgets.models.impl.BasketCartWidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.impl.BasketCartWidgetRenderer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Longbox;
import org.zkoss.zul.Messagebox;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcs.widgets.controllers.TargetBasketController;


/**
 * cart widget
 * 
 */
public class TargetBasketCartWidgetRenderer extends BasketCartWidgetRenderer {

    private static final Logger LOG = Logger.getLogger(TargetBasketCartWidgetRenderer.class);


    /* (non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.renderers.impl.BasketCartWidgetRenderer#createCartEntryQtyChangeEventListener(de.hybris.platform.cockpit.widgets.ListboxWidget, de.hybris.platform.cockpit.model.meta.TypedObject, org.zkoss.zul.Longbox, java.lang.Long)//NOPMD
     */
    @Override
    protected EventListener createCartEntryQtyChangeEventListener(
            final ListboxWidget<BasketCartWidgetModel, BasketController> widget, final TypedObject item,
            final Longbox qtyInput,
            final Long oldQuantity) {

        return new TargetCartEntryQtyChangeEventListener(widget, item, qtyInput, oldQuantity);

    }

    private class TargetCartEntryQtyChangeEventListener implements EventListener {
        private final transient ListboxWidget<BasketCartWidgetModel, BasketController> widget;
        private final TypedObject item;
        private final Longbox qtyInput;
        private final Long oldQuantity;

        /**
         * Change quantity of cart entry event listener
         * 
         * @param widget
         *            the basket widget
         * @param item
         *            the entry item
         * @param qtyInput
         *            the quantity input
         * @param oldQuantity
         *            the old quantity
         */
        public TargetCartEntryQtyChangeEventListener(
                final ListboxWidget<BasketCartWidgetModel, BasketController> widget, final TypedObject item,
                final Longbox qtyInput,
                final Long oldQuantity) {
            this.widget = widget;
            this.qtyInput = qtyInput;
            this.item = item;
            this.oldQuantity = oldQuantity;
        }

        @Override
        public void onEvent(final Event event) throws InterruptedException {

            handleTargetCartEntryQtyChangeEvent(this.widget, event, this.item, this.qtyInput, this.oldQuantity);
        }
    }

    /**
     * 
     * @param widget
     *            the basket widget
     * @param event
     *            the event
     * @param item
     *            the entry item
     * @param qtyInput
     *            the quantity input
     * @param oldQuantity
     *            the old quantity
     * @throws InterruptedException
     *             the exception
     */
    private void handleTargetCartEntryQtyChangeEvent(
            final ListboxWidget<BasketCartWidgetModel, BasketController> widget,
            final Event event, final TypedObject item, final Longbox qtyInput, final Long oldQuantity)
                    throws InterruptedException {

        final Long qty = qtyInput.getValue();
        if (qty != null) {
            if ((!UITools.isFromOtherDesktop(event.getTarget())) && (item.getObject() != null)) {
                if ((item.getObject() instanceof AbstractOrderEntryModel)) {
                    final CartEntryModel entry = (CartEntryModel)item.getObject();

                    final long totalEntryQuantity;
                    if (oldQuantity == null) {
                        totalEntryQuantity = SafeUnbox.toLong(qty);
                    }
                    else {
                        totalEntryQuantity = SafeUnbox.toLong(entry.getQuantity())
                                + (SafeUnbox.toLong(qty) - SafeUnbox.toLong(oldQuantity));
                    }

                    final TargetBasketController controller = (TargetBasketController)widget.getWidgetController();

                    try {
                        controller.setQuantityToSku(item, totalEntryQuantity, oldQuantity);
                    }
                    catch (final CommerceCartModificationException e) {
                        Messagebox.show(e.getMessage(), LabelUtils.getLabel(widget, "unableToSaveQty"),
                                Messagebox.OK, Messagebox.ERROR);
                        LOG.error("unable to save item");
                    }

                    widget.getWidgetController().dispatchEvent(null, widget, null);
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.renderers.impl.BasketCartWidgetRenderer#createCaption(de.hybris.platform.cockpit.widgets.ListboxWidget)//NOPMD
     */
    @Override
    public HtmlBasedComponent createCaption(
            final ListboxWidget<BasketCartWidgetModel, BasketController> widget) {
        final Div caption = new StyledDiv();
        caption.setSclass(CSS_WIDGET_CAPTION);

        CartModel cartModel = null;
        if (widget.getWidgetModel().getCart() != null) {
            cartModel = (CartModel)widget.getWidgetModel().getCart().getObject();
        }

        String cartCode = null;
        String customerName = null;
        if (cartModel != null) {
            cartCode = cartModel.getCode();
            if (cartModel.getUser() != null) {
                customerName = cartModel.getUser().getName();
            }
            if (StringUtils.isBlank(customerName) && cartModel.getUser() instanceof TargetCustomerModel) {
                final TargetCustomerModel targetCustomer = (TargetCustomerModel)cartModel.getUser();
                customerName = targetCustomer.getFirstname() + " " + targetCustomer.getLastname();
            }
        }
        cartCode = StringUtils.defaultIfEmpty(cartCode, LabelUtils.getLabel(widget, "empty"));
        customerName = StringUtils.defaultIfEmpty(customerName, LabelUtils.getLabel(widget, "anonymous"));
        final Label captionLabel = new Label(
                LabelUtils.getLabel(widget, widget.getWidgetCode() + ".title", customerName, cartCode));
        captionLabel.setParent(caption);
        return caption;
    }

    @Override
    protected HtmlBasedComponent createContentInternal(
            final ListboxWidget<BasketCartWidgetModel, BasketController> widget,
            final HtmlBasedComponent rootContainer) {
        // reload the cart before displaying to avoid caching issues with concurrent updates
        if (widget.getWidgetController().getCart() != null) {
            widget.getWidgetModel().setItems(widget.getWidgetController().getPromotionalCartLineItems());
            widget.getWidgetModel().setCart(widget.getWidgetController().getCart());
            widget.getWidgetModel().setCartCurrency(widget.getWidgetController().getCartCurrency());
        }
        else {
            widget.setWidgetModel(null);
        }
        return super.createContentInternal(widget, rootContainer);
    }

    /*
     * (non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.renderers.impl.BasketCartWidgetRenderer#handleCheckoutClickEvent(de.hybris.platform.cockpit.widgets.ListboxWidget, org.zkoss.zk.ui.event.Event)//NOPMD
     */
    @Override
    protected void handleCheckoutClickEvent(final ListboxWidget<BasketCartWidgetModel, BasketController> widget,
            final Event event)
                    throws Exception {

        try {
            widget.getWidgetController().triggerCheckout();
        }
        catch (final ValidationException ex) {
            Messagebox.show(ex.getMessage(), LabelUtils.getLabel(widget, "failedToCheckout"), Messagebox.OK,
                    Messagebox.ERROR);
        }

        widget.getWidgetController().dispatchEvent(null, widget, null);
    }
}

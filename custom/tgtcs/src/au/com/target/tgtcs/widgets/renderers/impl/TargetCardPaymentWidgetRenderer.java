package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.config.EditorRowConfiguration;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.cscockpit.widgets.impl.CardPaymentWidget;
import de.hybris.platform.cscockpit.widgets.renderers.impl.CardPaymentWidgetRenderer;
import de.hybris.platform.cscockpit.widgets.renderers.utils.edit.BasicPropertyDescriptor;
import de.hybris.platform.cscockpit.widgets.renderers.utils.edit.ComposedTypeReferenceHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zul.Div;

import au.com.target.tgtcs.widgets.controllers.TargetCardPaymentController;
import au.com.target.tgtcs.widgets.controllers.TargetCheckoutController;
import au.com.target.tgtcs.widgets.renderers.utils.edit.TargetPojoPropertyRendererUtil;


public class TargetCardPaymentWidgetRenderer extends CardPaymentWidgetRenderer {

    private static boolean editPayment = false;

    /**
     * @return the isEditPayment
     */
    public static boolean isEditPayment() {
        return editPayment;
    }

    /**
     * @param editPayment
     *            the isEditPayment to set
     */
    public static void setEditPayment(final boolean editPayment) {
        TargetCardPaymentWidgetRenderer.editPayment = editPayment;
    }

    public class UnEditablePropertyDescriptor extends BasicPropertyDescriptor {
        public UnEditablePropertyDescriptor(final String qualifier, final String editorType) {
            super(qualifier, editorType);
        }

        @Override
        public boolean isWritable()
        {
            return false;
        }
    }

    @Override
    protected List<PropertyDescriptor> getProperties(final CardPaymentWidget widget) {
        if (widget.getProperties() == null)
        {
            final List<PropertyDescriptor> properties = new ArrayList<>();

            final UnEditablePropertyDescriptor pdAmount = new UnEditablePropertyDescriptor("PaymentOption.amount",
                    "DECIMAL");
            properties.add(pdAmount);
            final BasicPropertyDescriptor pdCardHolderName = new BasicPropertyDescriptor(
                    "PaymentOption.CardInfo.cardHolderFullName", "TEXT");
            pdCardHolderName.setOccurrence(PropertyDescriptor.Occurrence.REQUIRED);
            properties.add(pdCardHolderName);
            final BasicPropertyDescriptor spdCartType = new BasicPropertyDescriptor("PaymentOption.CardInfo.cardType",
                    "ENUM");
            spdCartType.setAvailableValues(widget.getWidgetController().getAvailableCartTypes());
            spdCartType.setOccurrence(PropertyDescriptor.Occurrence.REQUIRED);
            properties.add(spdCartType);
            final BasicPropertyDescriptor pdCardNumber = new BasicPropertyDescriptor(
                    "PaymentOption.CardInfo.cardNumber", "TEXT");
            pdCardNumber.setOccurrence(PropertyDescriptor.Occurrence.REQUIRED);
            properties.add(pdCardNumber);
            final BasicPropertyDescriptor pdExpirationMonth = new BasicPropertyDescriptor(
                    "PaymentOption.CardInfo.expirationMonth", "INTEGER");
            pdExpirationMonth.setOccurrence(PropertyDescriptor.Occurrence.REQUIRED);
            properties.add(pdExpirationMonth);
            final BasicPropertyDescriptor pdExpirationYear = new BasicPropertyDescriptor(
                    "PaymentOption.CardInfo.expirationYear", "INTEGER");
            pdExpirationYear.setOccurrence(PropertyDescriptor.Occurrence.REQUIRED);
            properties.add(pdExpirationYear);
            final BasicPropertyDescriptor pdCardSecurityCode = new BasicPropertyDescriptor(
                    "PaymentOption.CardInfo.cv2Number", "TEXT");
            pdCardSecurityCode.setOccurrence(PropertyDescriptor.Occurrence.REQUIRED);
            properties.add(pdCardSecurityCode);

            final BasicPropertyDescriptor pdFirstName = new BasicPropertyDescriptor(
                    "PaymentOption.CardInfo.BillingInfo.firstName", "TEXT");
            pdFirstName.setOccurrence(PropertyDescriptor.Occurrence.REQUIRED);
            properties.add(pdFirstName);
            final BasicPropertyDescriptor pdLastName = new BasicPropertyDescriptor(
                    "PaymentOption.CardInfo.BillingInfo.lastName", "TEXT");
            pdLastName.setOccurrence(PropertyDescriptor.Occurrence.REQUIRED);
            properties.add(pdLastName);
            final BasicPropertyDescriptor pdStreet = new BasicPropertyDescriptor(
                    "PaymentOption.CardInfo.BillingInfo.street1", "TEXT");
            pdStreet.setOccurrence(PropertyDescriptor.Occurrence.REQUIRED);
            properties.add(pdStreet);
            properties.add(new BasicPropertyDescriptor("PaymentOption.CardInfo.BillingInfo.street2", "TEXT"));
            final BasicPropertyDescriptor pdCity = new BasicPropertyDescriptor(
                    "PaymentOption.CardInfo.BillingInfo.city", "TEXT");
            pdCity.setOccurrence(PropertyDescriptor.Occurrence.REQUIRED);
            properties.add(pdCity);
            final BasicPropertyDescriptor pdState = new BasicPropertyDescriptor(
                    "PaymentOption.CardInfo.BillingInfo.state", "TEXT");
            pdState.setOccurrence(PropertyDescriptor.Occurrence.REQUIRED);
            properties.add(pdState);
            final BasicPropertyDescriptor pdPostalCode = new BasicPropertyDescriptor(
                    "PaymentOption.CardInfo.BillingInfo.postalCode", "TEXT");
            pdPostalCode.setOccurrence(PropertyDescriptor.Occurrence.REQUIRED);
            properties.add(pdPostalCode);
            final BasicPropertyDescriptor bpdCountry = new BasicPropertyDescriptor(
                    "Address.country",
                    "REFERENCE");
            bpdCountry.setOccurrence(PropertyDescriptor.Occurrence.REQUIRED);
            bpdCountry.setReferenceComposedTypeCode("country");
            bpdCountry.addComposedTypeReferenceHelper(new ComposedTypeReferenceHelper<CountryModel>()
            {
                @Override
                public CountryModel getComposedTypeObject(final String code)
                {
                    return getCommonI18NService().getCountry(code);
                }

                @Override
                public String getCode(final TypedObject object)
                {
                    return object == null ? null : ((CountryModel)object.getObject()).getIsocode();
                }
            });
            properties.add(bpdCountry);
            properties.add(new BasicPropertyDescriptor("PaymentOption.CardInfo.BillingInfo.phoneNumber", "TEXT"));
            widget.setProperties(properties);
        }

        return widget.getProperties();
    }

    @Override
    protected void populateAmountAndBillingAddressFromCart(final CardPaymentWidget widget,
            final ObjectValueContainer currentObjectValues, final List<PropertyDescriptor> propertyDescriptors)
    {
        final Map<String, Object> values = new HashMap<String, Object>();

        values.put("PaymentOption.amount",
                Double.valueOf(widget.getWidgetController().getSuggestedAmountForPaymentOption()));

        final TypedObject defaultPaymentAddress = widget.getWidgetController().getDefaultPaymentAddress();
        if (defaultPaymentAddress != null)
        {
            if (defaultPaymentAddress.getObject() instanceof AddressModel)
            {
                final AddressModel addressModel = (AddressModel)defaultPaymentAddress.getObject();
                values.put("PaymentOption.CardInfo.BillingInfo.firstName", addressModel.getFirstname());
                values.put("PaymentOption.CardInfo.BillingInfo.lastName", addressModel.getLastname());
                values.put("PaymentOption.CardInfo.BillingInfo.street1", addressModel.getLine1());
                values.put("PaymentOption.CardInfo.BillingInfo.street2", addressModel.getLine2());
                values.put("PaymentOption.CardInfo.BillingInfo.city", addressModel.getTown());
                values.put("PaymentOption.CardInfo.BillingInfo.postalCode", addressModel.getPostalcode());
                values.put("Address.country", addressModel.getCountry());
                values.put("PaymentOption.CardInfo.BillingInfo.phoneNumber", addressModel.getPhone1());
                values.put("PaymentOption.CardInfo.BillingInfo.state", addressModel.getDistrict());
            }
        }

        if (editPayment)
        {
            final CreditCardPaymentInfoModel paymentInfoModel = (CreditCardPaymentInfoModel)((TargetCardPaymentController)widget
                    .getWidgetController()).getCurrentPaymentInfoModel();
            if (paymentInfoModel != null)
            {
                //the amount should be the previous amount from the "add payment"
                final CartModel cart = (((TargetCheckoutController)widget
                        .getWidgetController()).getCheckoutCartByMasterCart());
                values.put("PaymentOption.amount",
                        cart.getTotalPrice());
                values.put("PaymentOption.CardInfo.cardNumber", paymentInfoModel.getNumber());
                values.put("PaymentOption.CardInfo.cardType", paymentInfoModel.getType());
                values.put("PaymentOption.CardInfo.cardHolderFullName", paymentInfoModel.getCcOwner());
                values.put("PaymentOption.CardInfo.cv2Number", "xxx");
                final String toYear = paymentInfoModel.getValidToYear();
                if (toYear != null && toYear.length() > 0)
                {
                    values.put("PaymentOption.CardInfo.expirationYear", Integer.valueOf(toYear));
                }
                final String toMonth = paymentInfoModel.getValidToMonth();
                if (toMonth != null && toMonth.length() > 0)
                {
                    values.put("PaymentOption.CardInfo.expirationMonth", Integer.valueOf(toMonth));
                }

                final AddressModel billingAddress = paymentInfoModel.getBillingAddress();
                if (billingAddress != null)
                {
                    values.put("PaymentOption.CardInfo.BillingInfo.firstName", billingAddress.getFirstname());
                    values.put("PaymentOption.CardInfo.BillingInfo.lastName", billingAddress.getLastname());
                    values.put("PaymentOption.CardInfo.BillingInfo.street1", billingAddress.getLine1());
                    values.put("PaymentOption.CardInfo.BillingInfo.street2", billingAddress.getLine2());
                    values.put("PaymentOption.CardInfo.BillingInfo.city", billingAddress.getTown());
                    values.put("PaymentOption.CardInfo.BillingInfo.postalCode", billingAddress.getPostalcode());
                    values.put("Address.country", billingAddress.getCountry());
                    values.put("PaymentOption.CardInfo.BillingInfo.state", billingAddress.getDistrict());
                }

            }
        }

        for (final PropertyDescriptor propertyDescriptor : propertyDescriptors)
        {
            if (values.containsKey(propertyDescriptor.getQualifier()))
            {
                final ObjectValueContainer.ObjectValueHolder holder = currentObjectValues.getValue(propertyDescriptor,
                        null);
                if (holder != null)
                {
                    holder.update(values.get(propertyDescriptor.getQualifier()));
                    holder.unmodify();
                }
            }
        }
    }

    @Override
    protected ObjectValueContainer loadAndCreateEditors(final CardPaymentWidget widget, final HtmlBasedComponent content)
            throws ValueHandlerException
    {
        final List<PropertyDescriptor> propertyDescriptors = getProperties(widget);
        if (propertyDescriptors != null && !propertyDescriptors.isEmpty())
        {
            final ObjectValueContainer currentObjectValues = buildObjectValueContainer(null, propertyDescriptors,
                    Collections.<String> emptySet());
            populateAmountAndBillingAddressFromCart(widget, currentObjectValues, propertyDescriptors);

            final Map<PropertyDescriptor, EditorRowConfiguration> editorMapping = getPropertyEditorRowConfigurations(widget);
            if (editorMapping != null && !editorMapping.isEmpty())
            {
                for (final PropertyDescriptor propDescriptor : propertyDescriptors)
                {
                    final Div editorDiv = new Div();
                    editorDiv.setSclass(CSS_EDITOR_WIDGET_EDITOR);

                    TargetPojoPropertyRendererUtil.renderEditor(editorDiv, propDescriptor,
                            editorMapping.get(propDescriptor), null,
                            currentObjectValues, widget, true);

                    content.appendChild(editorDiv);
                }

                return currentObjectValues;
            }
        }

        return null;
    }

    /*@Override
    protected void handleCreateItemEvent(final CardPaymentWidget widget, final Event event,
            final ObjectValueContainer valueContainer) throws Exception {
        super.handleCreateItemEvent(widget, event, valueContainer);
    }*/

    /*private void checkMandatoryFields(final ObjectValueContainer valueContainer, final StringBuilder builder) {
        final Set<ObjectValueHolder> objectValueHandlers = valueContainer.getAllValues();
        for (final ObjectValueHolder objectValueHandler : objectValueHandlers) {
            final PropertyDescriptor propertyDescriptor = objectValueHandler.getPropertyDescriptor();
            if (propertyDescriptor.getOccurence().equals(Occurrence.REQUIRED)) {
                Object currentValue = objectValueHandler.getCurrentValue();
                if (((BasicPropertyDescriptor)propertyDescriptor).getComposedTypeReferenceHelper() != null)
                {
                    currentValue = ((BasicPropertyDescriptor)propertyDescriptor).getComposedTypeReferenceHelper()
                            .getCode(
                                    (TypedObject)currentValue);
                }
                if (currentValue == null || StringUtils.isEmpty(currentValue.toString())) {
                    builder.append(Labels.getLabel("required_attribute_missing")).append(": '")
                            .append(propertyDescriptor.getQualifier())
                            .append("' \n");
                }
            }
        }
    }
    */
}

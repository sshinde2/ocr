/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.widgets.ListboxWidget;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.utils.SafeUnbox;
import de.hybris.platform.cscockpit.utils.TypeUtils;
import de.hybris.platform.cscockpit.widgets.controllers.BasketController;
import de.hybris.platform.cscockpit.widgets.controllers.CheckoutController;
import de.hybris.platform.cscockpit.widgets.models.impl.CheckoutCartWidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.impl.CheckoutCartWidgetRenderer;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;

import java.text.NumberFormat;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Longbox;
import org.zkoss.zul.Messagebox;

import au.com.target.tgtcs.facade.TargetCSOrderFacade;
import au.com.target.tgtcs.widgets.controllers.TargetBasketController;


/**
 * @author Roman_Skrypal, Pavel_Yakushevich
 * 
 */
public class TargetCheckoutCartWidgetRenderer extends CheckoutCartWidgetRenderer {

    private static final Logger LOG = Logger.getLogger(TargetCheckoutCartWidgetRenderer.class);

    private TargetCSOrderFacade orderFacade;

    @Override
    protected HtmlBasedComponent createCartEntryInformationBlock(
            final ListboxWidget<CheckoutCartWidgetModel, CheckoutController> widget, final TypedObject cartEntry) {
        final Div container = new Div();

        final CartEntryModel cartEntryModel = TypeUtils.unwrapItem(cartEntry, CartEntryModel.class);

        DeliveryModeModel deliveryMode = cartEntryModel.getDeliveryMode();
        if (deliveryMode == null) {
            deliveryMode = cartEntryModel.getOrder().getDeliveryMode();
        }

        final Div cartEntryPropertiesContainer = new Div();
        cartEntryPropertiesContainer.setSclass("csProperties");
        cartEntryPropertiesContainer.setParent(container);

        final Div cartEntryTotalsContainer = new Div();
        cartEntryTotalsContainer.setSclass("csEntryTotals");
        cartEntryTotalsContainer.setParent(cartEntryPropertiesContainer);

        final CurrencyModel cartCurrencyModel = ((AbstractOrderModel)widget.getWidgetModel().getCart().getObject())
                .getCurrency();

        final NumberFormat currencyInstance = (NumberFormat)getSessionService().executeInLocalView(
                new SessionExecutionBody() {
                    @Override
                    public Object execute() {
                        TargetCheckoutCartWidgetRenderer.this.getCommonI18NService().setCurrentCurrency(
                                cartCurrencyModel);
                        return TargetCheckoutCartWidgetRenderer.this.getFormatFactory().createCurrencyFormat();
                    }
                });

        final Div priceContainer = new Div();
        priceContainer.setParent(cartEntryTotalsContainer);
        final Double basePriceValue = cartEntryModel.getBasePrice();
        final String basePriceString = basePriceValue != null ? currencyInstance.format(basePriceValue) : "";
        priceContainer.appendChild(new Label(LabelUtils.getLabel(widget, "basePrice", new Object[0])));
        priceContainer.appendChild(new Label(basePriceString));

        final Double totalPriceValue = cartEntryModel.getTotalPrice();
        final String totalPriceString = totalPriceValue != null ? currencyInstance.format(totalPriceValue) : "";
        priceContainer.appendChild(new Label(LabelUtils.getLabel(widget, "totalPrice", new Object[0])));
        priceContainer.appendChild(new Label(totalPriceString));

        final Long qty = cartEntryModel.getQuantity();
        final Longbox quantityInput = new Longbox();
        if (qty != null) {
            quantityInput.setValue(qty);
        }
        quantityInput.setWidth("33px");
        priceContainer.appendChild(new Label(LabelUtils.getLabel(widget, "qty", new Object[0])));
        priceContainer.appendChild(quantityInput);

        final Div actionContainer = new Div();
        actionContainer.setSclass("csCartLineActions");
        actionContainer.setParent(cartEntryPropertiesContainer);

        final boolean available = deliveryMode != null
                && getOrderFacade().isOrderEntryAvailableForDeliveryMode(cartEntryModel, deliveryMode);

        if (!available) {
            final Label removeItem = new Label(LabelUtils.getLabel(widget, "removeThisItem"));
            removeItem.setSclass("tgtcsRemoveItem");
            removeItem.setParent(actionContainer);
        }

        final String rowMessage = getRowActionMessage(cartEntry);
        if (rowMessage != null) {
            final Label rowMessageLabel = new Label(rowMessage);
            rowMessageLabel.setSclass("csCartEntryMessage");
            actionContainer.appendChild(rowMessageLabel);
        }

        final Button changeCartEntryQtyButton = new Button(LabelUtils.getLabel(widget, "changeCartEntryQtyBtn",
                new Object[0]));
        changeCartEntryQtyButton.setParent(actionContainer);
        changeCartEntryQtyButton.setSclass("btngreen");

        final EventListener basketListener = createCartEntryQtyChangeEventListener(widget, cartEntry, quantityInput);
        changeCartEntryQtyButton.addEventListener("onClick", basketListener);

        final Button splitCartEntryButton = new Button(LabelUtils.getLabel(widget, "splitCartEntryBtn", new Object[0]));
        splitCartEntryButton.setParent(actionContainer);
        splitCartEntryButton.setSclass("btngreen");
        splitCartEntryButton.setDisabled(SafeUnbox.toLong(qty, 0L) < 2L);
        splitCartEntryButton.addEventListener("onClick", createSplitCartEntryEventListener(widget, cartEntry));

        final List columns = getColumnConfigurations();
        getPropertyRendererHelper().buildPropertyValuesFromColumnConfigs(cartEntry, columns,
                cartEntryPropertiesContainer);

        return container;
    }

    @Override
    protected void handleCartEntryQtyChangeEvent(
            final ListboxWidget<CheckoutCartWidgetModel, CheckoutController> widget,
            final Event event, final TypedObject item, final Longbox qtyInput) {

        final Long qty = qtyInput.getValue();
        if ((qty == null) || (item.getObject() == null)) {
            return;
        }
        final TargetBasketController basketController = (TargetBasketController)widget.getWidgetController()
                .getBasketController();

        try {
            basketController.setQuantityToSku(item, qty.longValue(), null);
        }
        catch (final CommerceCartModificationException e) {
            try {
                Messagebox.show(e.getMessage(), LabelUtils.getLabel(widget, "unableToSaveQty"),
                        Messagebox.OK, Messagebox.ERROR);
            }
            catch (final InterruptedException ex) {
                LOG.warn(ex.getMessage(), ex);
                Thread.currentThread().interrupt();
            }
        }
        basketController.dispatchEvent(null, widget, null);
    }

    @Override
    protected HtmlBasedComponent createContentInternal(
            final ListboxWidget<CheckoutCartWidgetModel, CheckoutController> widget,
            final HtmlBasedComponent rootContainer) {
        final BasketController basketController = widget.getWidgetController().getBasketController();
        final CheckoutCartWidgetModel widgetModel = widget.getWidgetModel();
        // reload the cart before displaying to avoid caching issues with concurrent updates
        widgetModel.setItems(basketController.getEntries());
        widgetModel.setCart(basketController.getCart());
        return super.createContentInternal(widget, rootContainer);
    }

    /**
     * @return the orderFacade
     */
    public TargetCSOrderFacade getOrderFacade() {
        return orderFacade;
    }

    /**
     * @param orderFacade
     *            the orderFacade to set
     */
    @Required
    public void setOrderFacade(final TargetCSOrderFacade orderFacade) {
        this.orderFacade = orderFacade;
    }
}

package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.util.UITools;
import de.hybris.platform.cockpit.widgets.impl.DefaultListboxWidget;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.cscockpit.exceptions.ValidationException;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.controllers.CardPaymentController;
import de.hybris.platform.cscockpit.widgets.controllers.CheckoutController;
import de.hybris.platform.cscockpit.widgets.models.impl.CheckoutPaymentWidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.impl.CheckoutPaymentWidgetRenderer;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;

import java.text.NumberFormat;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import au.com.target.tgtcs.widgets.controllers.TargetCardPaymentController;
import au.com.target.tgtcs.widgets.controllers.TargetCheckoutController;
import au.com.target.tgtcs.widgets.controllers.impl.TgtcsAddPaymentWindowController;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;


public class TargetCheckoutPaymentWidgetRenderer extends CheckoutPaymentWidgetRenderer {
    private static final String COCKPIT_ID_CHECKOUT_EDITPAYMENT_BUTTON = "Checkout_Payment_EditPayment_button";
    private static final String VIEW_PAGE_PATH = "tgtcshostedPaymentForm.zul";

    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Override
    protected HtmlBasedComponent createContentInternal(
            final DefaultListboxWidget<CheckoutPaymentWidgetModel, CheckoutController> widget,
            final HtmlBasedComponent rootContainer) {
        final Div container = new Div();
        container.setSclass(CSS_CHECKOUT_PAYMENT_CONTAINER);

        final Div actionPaymentOptionContainer = new Div();
        actionPaymentOptionContainer.setParent(container);

        final CurrencyModel cartCurrencyModel = ((TargetCheckoutController)widget.getWidgetController())
                .getCheckoutCartByMasterCart().getCurrency();
        final NumberFormat currencyInstance = (NumberFormat)getSessionService().executeInLocalView(
                new SessionExecutionBody()
                {
                    @Override
                    public Object execute()
                    {
                        getCommonI18NService().setCurrentCurrency(cartCurrencyModel);
                        return getFormatFactory().createCurrencyFormat();
                    }
                });

        final String paymentRequiredLabel = LabelUtils.getLabel(widget, "paymentRequired",
                currencyInstance.format(widget.getWidgetController().getSuggestedAmountForPaymentOption()));
        final Label remainder = new Label(paymentRequiredLabel);
        remainder.setParent(actionPaymentOptionContainer);

        final Div actionPaymentOptionDiv = new Div();
        actionPaymentOptionDiv.setParent(actionPaymentOptionContainer);

        final Button addNewPaymentOptionButton = new Button(LabelUtils.getLabel(widget, "addCreditDetailsBtn"));
        addNewPaymentOptionButton.setParent(actionPaymentOptionDiv);
        if (UISessionUtils.getCurrentSession().isUsingTestIDs())
        {
            UITools.applyTestID(addNewPaymentOptionButton, COCKPIT_ID_CHECKOUT_ADDPAYMENT_BUTTON);
        }
        addNewPaymentOptionButton.addEventListener(Events.ON_CLICK, new EventListener()
        {
            @Override
            public void onEvent(final Event event) throws Exception
            {
                handlePaymentButtonClickEvent(widget, event, container, IpgPaymentTemplateType.CREDITCARDSINGLE);
            }
        });

        final Button editPaymentOptionButton = new Button(LabelUtils.getLabel(widget, "editBtn"));
        editPaymentOptionButton.setParent(actionPaymentOptionDiv);
        if (UISessionUtils.getCurrentSession().isUsingTestIDs())
        {
            UITools.applyTestID(editPaymentOptionButton, COCKPIT_ID_CHECKOUT_EDITPAYMENT_BUTTON);
        }
        editPaymentOptionButton.addEventListener(Events.ON_CLICK, new EventListener()
        {
            @Override
            public void onEvent(final Event event) throws Exception
            {
                handlePaymentButtonClickEvent(widget, event, container, IpgPaymentTemplateType.CREDITCARDSINGLE);
            }
        });

        if (targetFeatureSwitchFacade.isFeatureEnabled("payment.ipg.giftcard.cscockpit")) {
            final Button addGiftcardMultiplePaymentButton = new Button(LabelUtils.getLabel(widget, "addGiftcardBtn"));
            addGiftcardMultiplePaymentButton.setParent(actionPaymentOptionDiv);
            if (UISessionUtils.getCurrentSession().isUsingTestIDs())
            {
                UITools.applyTestID(addGiftcardMultiplePaymentButton, COCKPIT_ID_CHECKOUT_ADDPAYMENT_BUTTON);
            }
            addGiftcardMultiplePaymentButton.addEventListener(Events.ON_CLICK, new EventListener()
            {
                @Override
                public void onEvent(final Event event) throws Exception
                {
                    handlePaymentButtonClickEvent(widget, event, container, IpgPaymentTemplateType.GIFTCARDMULTIPLE);
                }
            });
        }

        final PaymentInfoModel paymentInfo = ((TargetCardPaymentController)widget.getWidgetController())
                .getCurrentPaymentInfoModel();
        final CartModel cartModel = ((TargetCheckoutController)widget.getWidgetController())
                .getCheckoutCartByMasterCart();

        if (paymentInfo == null
                || !StringUtils.equals(paymentInfo.getUser().getUid(), cartModel.getUser().getUid())
                || (paymentInfo instanceof CreditCardPaymentInfoModel && ((CreditCardPaymentInfoModel)paymentInfo)
                        .getSubscriptionId() == null)) {
            // Add new payment is always disabled to be compliant.
            addNewPaymentOptionButton.setDisabled(true);
            editPaymentOptionButton.setDisabled(true);
        }
        else {
            addNewPaymentOptionButton.setDisabled(true);
            editPaymentOptionButton.setDisabled(false);
        }

        return container;
    }

    /**
     * handle edit payment event
     * 
     * @param widget
     * @param event
     * @param container
     * @throws Exception
     */
    protected void handleOpenEditPaymentOptionClickEvent(
            final DefaultListboxWidget<CheckoutPaymentWidgetModel, CheckoutController> widget, final Event event,
            final Div container) throws Exception {
        try
        {
            widget.getWidgetController().canCreatePayments();
            TargetCardPaymentWidgetRenderer.setEditPayment(true);
            getPopupWidgetHelper().createPopupWidget(container, "csCheckoutCardPaymentWidgetConfig",
                    "csCheckoutCardPaymentWidgetConfig-Popup", CSS_CHECKOUT_PAYMENT_POPUP,
                    LabelUtils.getLabel(widget, "popup.paymentOptionCreateTitle"));
        }
        catch (final ValidationException ex)
        {
            Messagebox.show(ex.getMessage(), LabelUtils.getLabel(widget, "failedToCreatePayment"), Messagebox.OK,
                    Messagebox.ERROR);
        }
    }

    @Override
    protected void handleOpenNewPaymentOptionClickEvent(
            final DefaultListboxWidget<CheckoutPaymentWidgetModel, CheckoutController> widget, final Event event,
            final Div container)
            throws Exception
    {
        try
        {
            widget.getWidgetController().canCreatePayments();
            TargetCardPaymentWidgetRenderer.setEditPayment(false);
            getPopupWidgetHelper().createPopupWidget(container, "csCheckoutCardPaymentWidgetConfig",
                    "csCheckoutCardPaymentWidgetConfig-Popup", CSS_CHECKOUT_PAYMENT_POPUP,
                    LabelUtils.getLabel(widget, "popup.paymentOptionCreateTitle"));
        }
        catch (final ValidationException ex)
        {
            Messagebox.show(ex.getMessage(), LabelUtils.getLabel(widget, "failedToCreatePayment"), Messagebox.OK,
                    Messagebox.ERROR);
        }
    }

    /**
     * handel add/edit payment button click event
     * 
     * @param widget
     * @param event
     * @param container
     * @throws Exception
     */
    protected void handlePaymentButtonClickEvent(
            final DefaultListboxWidget<CheckoutPaymentWidgetModel, CheckoutController> widget, final Event event,
            final Div container, final IpgPaymentTemplateType ipgPaymentTemplateType)
            throws Exception
    {
        try
        {
            widget.getWidgetController().canCreatePayments();
            final HashMap<String, Object> params = new HashMap<String, Object>();
            final Window window;

            window = (Window)Executions.createComponents(VIEW_PAGE_PATH, null, params);
            window.addEventListener(Events.ON_CLOSE, new EventListener() {
                @Override
                public void onEvent(final Event event) throws Exception {
                    widget.getWidgetModel().notifyListeners();
                    getPopupWidgetHelper().dismissCurrentPopup();
                    ((CardPaymentController)widget.getWidgetController()).dispatchEvent(null, widget, null);
                    ((CardPaymentController)widget.getWidgetController()).getCustomerController().dispatchEvent(null,
                            widget, null);
                }

            });

            final PaymentInfoModel paymentInfo = ((TargetCardPaymentController)widget.getWidgetController())
                    .getCurrentPaymentInfoModel();

            final TgtcsAddPaymentWindowController controller = new TgtcsAddPaymentWindowController(window,
                    ((TargetCheckoutController)widget.getWidgetController())
                            .getCheckoutCartByMasterCart(),
                    TgtcsAddPaymentWindowController.PAYMENT_OPERATION_TYPE_TOKENIZE, paymentInfo,
                    ipgPaymentTemplateType);
            controller.refreshWindow();
            try {
                window.doModal();
            }
            catch (final Exception e) {
                Messagebox.show(e.getMessage(), LabelUtils.getLabel(widget, "failedToCreatePayment"), Messagebox.OK,
                        Messagebox.ERROR);
            }
        }
        catch (final ValidationException ex)
        {
            Messagebox.show(ex.getMessage(), LabelUtils.getLabel(widget, "failedToCreatePayment"), Messagebox.OK,
                    Messagebox.ERROR);
        }
    }

    /**
     * @return the targetFeatureSwitchFacade
     */
    protected TargetFeatureSwitchFacade getTargetFeatureSwitchFacade() {
        return targetFeatureSwitchFacade;
    }

    /**
     * @param targetFeatureSwitchFacade
     *            the targetFeatureSwitchFacade to set
     */
    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }


}

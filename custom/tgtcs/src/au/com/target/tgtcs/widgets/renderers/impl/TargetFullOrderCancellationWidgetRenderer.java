/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.widgets.InputWidget;
import de.hybris.platform.cockpit.widgets.models.impl.DefaultListWidgetModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.exceptions.ValidationException;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.controllers.CancellationController;
import de.hybris.platform.cscockpit.widgets.renderers.impl.FullOrderCancellationWidgetRenderer;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import au.com.target.tgtcs.components.orderdetail.controllers.AddPaymentWindowController;
import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtcs.facade.TgtCsCardPaymentFacade;
import au.com.target.tgtcs.widgets.controllers.impl.TargetCancellationControllerImpl;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;


/**
 * @author Nandini
 * 
 */
public class TargetFullOrderCancellationWidgetRenderer extends FullOrderCancellationWidgetRenderer {
    private static final Logger LOG = Logger.getLogger(TargetFullOrderCancellationWidgetRenderer.class);
    private static final String ADD_PAYMENT_BUTTON = "addPaymentButton";
    private static final String TGTCS_HOSTED_PAYMENT_FORM_ZUL = "tgtcs/hostedPaymentForm.zul";
    private AddPaymentWindowController addPaymentWindowController;
    private TgtCsCardPaymentFacade tgtCsCardPaymentFacade;
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Override
    protected HtmlBasedComponent createContentInternal(
            final InputWidget<DefaultListWidgetModel<OrderCancelRequest>, CancellationController> widget,
            final HtmlBasedComponent rootContainer) {

        tgtCsCardPaymentFacade.clearOldPayment();
        final Div container = new Div();
        container.setSclass("partialOrderCancellationWidget");

        final Div orderCancelRequestBox = new Div();
        orderCancelRequestBox.setParent(container);
        final ObjectValueContainer orderCancelRequestObjectValueContainer = createOrderCancelRequestEditors(
                widget, orderCancelRequestBox);

        final Div addPaymentButtonBox = new Div();
        addPaymentButtonBox.setParent(container);
        addPaymentButtonBox.setSclass("addPaymentButtonBox");

        final Button attemptAddPaymentButton = new Button(LabelUtils.getLabel(
                widget, ADD_PAYMENT_BUTTON, new Object[0]));
        attemptAddPaymentButton.setParent(addPaymentButtonBox);
        attemptAddPaymentButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(final Event event) throws InterruptedException {
                TargetFullOrderCancellationWidgetRenderer.this.handleRefundPaymentPopupEvent(widget);
            }
        });

        final Div cancellationOrderButtonBox = new Div();
        cancellationOrderButtonBox.setParent(container);
        cancellationOrderButtonBox.setSclass("cancellationOrderButtonBox");

        final Button attemptCancellationButton = new Button(LabelUtils.getLabel(
                widget, "cancelOrderButton", new Object[0]));
        attemptCancellationButton.setParent(cancellationOrderButtonBox);
        attemptCancellationButton.addEventListener(
                "onClick",
                createConfirmBeforeCompletingRequestEventListener(
                        widget,
                        createAttemptCancellationEventListener(widget,
                                orderCancelRequestObjectValueContainer)));
        return container;
    }

    /**
     * @param widget
     * @throws InterruptedException
     */
    protected void handleRefundPaymentPopupEvent(
            final InputWidget<DefaultListWidgetModel<OrderCancelRequest>, CancellationController> widget)
                    throws InterruptedException {
        if (widget != null) {
            final CancellationController controller = widget.getWidgetController();
            if (controller != null && controller instanceof TargetCancellationControllerImpl) {
                final TargetCancellationControllerImpl controllerImpl = (TargetCancellationControllerImpl)controller;
                final TypedObject typedObject = controllerImpl.getOrder();
                if (typedObject != null) {
                    final Object object = typedObject.getObject();
                    if (object != null && object instanceof OrderModel) {
                        final OrderModel order = (OrderModel)object;
                        final Window window = getPaymentWindow(order);
                        try {
                            window.doModal();
                        }
                        catch (final Exception e) {
                            LOG.error(Labels.getLabel(TgtcsConstants.ERROR_MESSAGE_CODE), e);
                            Messagebox.show(e.getMessage(),
                                    Labels.getLabel(TgtcsConstants.ERROR_MESSAGE_CODE, new Object[0]), 1,
                                    "z-msgbox z-msgbox-error");
                        }
                    }
                }
            }
        }
    }



    /* (non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.renderers.impl.FullOrderCancellationWidgetRenderer#handleAttemptCancellationEvent(de.hybris.platform.cockpit.widgets.InputWidget, org.zkoss.zk.ui.event.Event, de.hybris.platform.cockpit.services.values.ObjectValueContainer)
     */
    @Override
    protected void handleAttemptCancellationEvent(
            final InputWidget<DefaultListWidgetModel<OrderCancelRequest>, CancellationController> widget,
            final Event event,
            final ObjectValueContainer orderCancelRequestObjectValueContainer) throws Exception {

        if ("onOK".equals(event.getName())) {

            Assert.notNull(widget, "Widget cannot be null");
            Assert.notNull(widget.getWidgetController(), "WidgetController cannot be null");

            try {

                final TypedObject cancellationRequest = widget.getWidgetController()
                        .createOrderCancellationRequest(orderCancelRequestObjectValueContainer);
                if (cancellationRequest != null) {
                    this.getPopupWidgetHelper().dismissCurrentPopup();
                    final OrderCancelRecordEntryModel orderCancelRecordEntryModel = (OrderCancelRecordEntryModel)cancellationRequest
                            .getObject();
                    Messagebox.show(
                            LabelUtils.getLabel(widget, "cancellationNumber",
                                    new Object[] { orderCancelRecordEntryModel.getCode() }),
                            LabelUtils.getLabel(widget, "cancellationNumberTitle", new Object[0]), 1,
                            "z-msgbox z-msgbox-information");
                    widget.getWidgetController().dispatchEvent(null, widget,
                            null);
                }
                else {
                    Messagebox.show(LabelUtils.getLabel(widget, "errorCreatingRequest", new Object[0]),
                            LabelUtils.getLabel(widget, "failed", new Object[0]), 1, "z-msgbox z-msgbox-error");
                }
            }
            catch (final OrderCancelException orderCancelException) {
                showMessageBox(widget, orderCancelException, "failed");
                widget.getWidgetController().dispatchEvent(null, widget,
                        null);
            }
            catch (final ValidationException validationException) {
                showMessageBox(widget, validationException, "failedToValidate");
            }
        }

    }

    /**
     * @param widget
     * @param exception
     * @throws InterruptedException
     */
    private void showMessageBox(
            final InputWidget<DefaultListWidgetModel<OrderCancelRequest>, CancellationController> widget,
            final Exception exception, final String componentName) throws InterruptedException {
        Messagebox.show(
                exception.getMessage()
                        + (exception.getCause() == null ? "" : " - " + exception.getCause().getMessage()),
                getLabelFailed(widget, componentName, new Object[0]), 1, "z-msgbox z-msgbox-error");
    }

    protected String getLabelFailed(
            final InputWidget<DefaultListWidgetModel<OrderCancelRequest>, CancellationController> widget,
            final String componentName, final Object object) {

        return LabelUtils.getLabel(widget, componentName, object);
    }

    /**
     * Gets the payment window.
     * 
     * @param orderModel
     *            the order model
     * @return the payment window
     */
    protected Window getPaymentWindow(final OrderModel orderModel) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        final Window window = (Window)Executions.createComponents(TGTCS_HOSTED_PAYMENT_FORM_ZUL, null,
                params);
        if (targetFeatureSwitchFacade.isFeatureEnabled("payment.ipg.cscockpit")) {
            addPaymentWindowController.populateCancelPaymentFormForIpg(orderModel, window);
        }
        else {
            addPaymentWindowController.populateCancelPaymentForm(orderModel, window);
        }
        return window;
    }

    /**
     * @param addPaymentWindowController
     *            the addPaymentWindowController to set
     */
    @Required
    public void setAddPaymentWindowController(final AddPaymentWindowController addPaymentWindowController) {
        this.addPaymentWindowController = addPaymentWindowController;
    }

    /**
     * @param tgtCsCardPaymentFacade
     */
    @Required
    public void setTgtCsCardPaymentFacade(final TgtCsCardPaymentFacade tgtCsCardPaymentFacade) {
        this.tgtCsCardPaymentFacade = tgtCsCardPaymentFacade;
    }

    /**
     * @param targetFeatureSwitchFacade
     *            the targetFeatureSwitchFacade to set
     */
    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }


}

/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.config.ColumnConfiguration;
import de.hybris.platform.cockpit.services.config.ColumnGroupConfiguration;
import de.hybris.platform.cockpit.services.config.ListViewConfiguration;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.cscockpit.utils.CockpitUiConfigLoader;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.utils.ObjectGetValueUtils;
import de.hybris.platform.cscockpit.widgets.controllers.OrderController;
import de.hybris.platform.cscockpit.widgets.models.impl.OrderItemWidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.impl.OrderDetailsAddressWidgetRenderer;

import java.util.List;

import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;


/**
 * @author bhuang3
 * 
 */
public class TargetOrderDetailsAddressWidgetRenderer extends OrderDetailsAddressWidgetRenderer {

    protected static final String STYLE_FONT_BOLD = "font-weight: bolder;";

    @Override
    protected HtmlBasedComponent createContentInternal(final Widget<OrderItemWidgetModel, OrderController> widget,
            final HtmlBasedComponent rootContainer)
    {
        final Div content = new Div();
        content.setSclass(CSS_ORDER_DETAILS_ADDRESS);

        final String addressConfigurationCode = getConfigurationCode();

        final TypedObject order = widget.getWidgetModel().getOrder();
        if (order != null)
        {
            final OrderModel orderModel = (OrderModel)order.getObject();
            final AddressModel address = (isBilling() ? orderModel.getPaymentAddress() : orderModel
                    .getDeliveryAddress());

            if (address != null)
            {
                final TypedObject item = getCockpitTypeService().wrapItem(address);

                final ListViewConfiguration template = UISessionUtils
                        .getCurrentSession()
                        .getUiConfigurationService()
                        .getComponentConfiguration(getCockpitTypeService().getObjectTemplate(item.getType().getCode()),
                                addressConfigurationCode, ListViewConfiguration.class);

                buildRows(item, orderModel, template.getRootColumnGroupConfiguration(), content);
            }
        }
        else
        {
            final Label dummyLabel = new Label(LabelUtils.getLabel(widget, "noOrderSelected"));
            dummyLabel.setParent(content);
        }
        return content;
    }


    /**
     * build rows for every line in the xml
     * 
     * @param item
     * @param orderModel
     * @param col
     * @param parentContainer
     */
    protected void buildRows(final TypedObject item, final OrderModel orderModel, final ColumnGroupConfiguration col,
            final HtmlBasedComponent parentContainer)
    {
        final List<? extends ColumnGroupConfiguration> lines = col.getColumnGroupConfigurations();
        if (lines != null && !lines.isEmpty())
        {
            for (final ColumnGroupConfiguration line : lines)
            {
                final Div lineContainer = new Div();
                lineContainer.setParent(parentContainer);
                lineContainer.setSclass(CSS_WIDGET_ADDRESS_LINE);
                if (orderModel.getDeliveryMode().getCode().equals("click-and-collect")
                        && line.getName().equals("storeInfo")) {
                    buildStoreRow(getCockpitTypeService().wrapItem(orderModel),
                            CockpitUiConfigLoader.getDirectVisibleColumnConfigurations(line), lineContainer);
                }
                else {
                    buildRow(item, CockpitUiConfigLoader.getDirectVisibleColumnConfigurations(line), lineContainer);
                }
            }
        }
    }

    protected void buildStoreRow(final TypedObject item, final List<ColumnConfiguration> columns,
            final HtmlBasedComponent parentContainer)
    {
        if (columns != null && !columns.isEmpty())
        {
            for (final ColumnConfiguration col : columns)
            {
                final String value = ObjectGetValueUtils.getValue(col.getValueHandler(), item);
                if (value != null && !value.isEmpty() && !value.trim().isEmpty())
                {
                    final Label label = new Label(value + " ");
                    label.setSclass(CSS_LIST_VALUE);
                    label.setStyle(STYLE_FONT_BOLD);
                    parentContainer.appendChild(label);
                }
            }
        }
    }


}

/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.controllers.OrderController;
import de.hybris.platform.cscockpit.widgets.models.impl.OrderItemWidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.impl.OrderDetailsOrderPaymentWidgetRenderer;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;

import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.service.TargetPaymentService;



/**
 * @author mjanarth
 *
 */
public class TargetOrderDetailsOrderPaymentWidgetRenderer extends OrderDetailsOrderPaymentWidgetRenderer {

    private TargetPaymentService targetPaymentService;

    @Override
    protected HtmlBasedComponent createContentInternal(final Widget<OrderItemWidgetModel, OrderController> widget,
            final HtmlBasedComponent rootContainer)
    {
        final TypedObject order = widget.getWidgetModel().getOrder();
        TypedObject paymentInfo = null;
        if (order.getObject() instanceof OrderModel)
        {
            final OrderModel orderModel = (OrderModel)order.getObject();
            final PaymentInfoModel paymentInfoModel = orderModel.getPaymentInfo();
            if (paymentInfoModel != null)
            {
                if (paymentInfoModel instanceof IpgPaymentInfoModel)
                {
                    final IpgPaymentInfoModel ipgInfo = (IpgPaymentInfoModel)paymentInfoModel;

                    if (IpgPaymentTemplateType.CREDITCARDSINGLE.equals(ipgInfo.getIpgPaymentTemplateType())) {
                        final AddressModel billingAddress = paymentInfoModel.getBillingAddress();

                        final List<PaymentInfoModel> successfulPayments = targetPaymentService
                                .getSuccessfulPayments(orderModel);

                        if (CollectionUtils.isNotEmpty(successfulPayments)) {
                            // only get first one for now. Will have to retrieve all when split payment is implemented
                            final PaymentInfoModel creditCardInfo = successfulPayments.get(0);

                            creditCardInfo.setBillingAddress(billingAddress);
                            if (creditCardInfo instanceof IpgCreditCardPaymentInfoModel) {
                                paymentInfo = getCockpitTypeService().wrapItem(creditCardInfo);
                            }
                        }
                    }
                    else {
                        return new Div();
                    }
                }
                else {
                    paymentInfo = getCockpitTypeService().wrapItem(paymentInfoModel);

                }
                final HtmlBasedComponent detailContent = getDetailRenderer().createContent(null, paymentInfo, widget);
                return ((detailContent == null) ? new Div() : detailContent);

            }

        }
        final Div content = new Div();
        content.setSclass("csOrderDetailsPayment");
        final Label dummyLabel = new Label(LabelUtils.getLabel(widget, "noEntry", new Object[0]));
        dummyLabel.setParent(content);

        return content;
    }

    /**
     * @param targetPaymentService
     *            the targetPaymentService to set
     */
    public void setTargetPaymentService(final TargetPaymentService targetPaymentService) {
        this.targetPaymentService = targetPaymentService;
    }


}

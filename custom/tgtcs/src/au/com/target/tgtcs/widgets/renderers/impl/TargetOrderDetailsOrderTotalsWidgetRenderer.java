/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.controllers.OrderController;
import de.hybris.platform.cscockpit.widgets.models.impl.OrderItemWidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.impl.OrderDetailsOrderTotalsWidgetRenderer;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;

import java.text.NumberFormat;

import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zul.Div;

import au.com.target.tgtcore.voucher.service.TargetVoucherService;


/**
 * Target customized order detail order total widget render
 * 
 */
public class TargetOrderDetailsOrderTotalsWidgetRenderer extends OrderDetailsOrderTotalsWidgetRenderer {

    private TargetVoucherService targetVoucherService;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void renderOrderDetail(final Widget<OrderItemWidgetModel, OrderController> widget,
            final TypedObject order, final HtmlBasedComponent parent)
    {
        final Div container = new Div();
        container.setSclass("csOrderTotals");

        if ((order != null) && (order.getObject() instanceof AbstractOrderModel))
        {
            final AbstractOrderModel abstractOrderModel = (AbstractOrderModel)order.getObject();

            final CurrencyModel cartCurrencyModel = abstractOrderModel.getCurrency();
            final NumberFormat currencyInstance = (NumberFormat)getSessionService().executeInLocalView(
                    new SessionExecutionBody()
                    {
                        @Override
                        public Object execute()
                        {
                            getCommonI18NService().setCurrentCurrency(cartCurrencyModel);
                            return getFormatFactory().createCurrencyFormat();
                        }
                    });
            final Double subtotal = abstractOrderModel.getSubtotal();
            renderRow(subtotal, LabelUtils.getLabel(widget, "subtotal", new Object[0]), currencyInstance, container);

            final Double taxes = abstractOrderModel.getTotalTax();
            renderRow(taxes, LabelUtils.getLabel(widget, "taxes", new Object[0]), currencyInstance, container);

            final Double paymentCosts = abstractOrderModel.getPaymentCost();
            renderRow(paymentCosts, LabelUtils.getLabel(widget, "paymentCosts", new Object[0]), currencyInstance,
                    container);

            final Double deliveryCosts = abstractOrderModel.getDeliveryCost();
            renderRow(deliveryCosts, LabelUtils.getLabel(widget, "deliveryCosts", new Object[0]), currencyInstance,
                    container);

            final Double discounts = abstractOrderModel.getTotalDiscounts();
            String discountLabel = "discounts";
            if (null != targetVoucherService
                    .getFlybuysDiscountForOrder(abstractOrderModel) && isFlybuysPointsConsumed(abstractOrderModel)) {
                discountLabel = "flybuys.discounts";
            }
            renderRow(discounts, LabelUtils.getLabel(widget, discountLabel, new Object[0]), currencyInstance,
                    container);

            final Double totalPrice = abstractOrderModel.getTotalPrice();
            renderRow(totalPrice, LabelUtils.getLabel(widget, "totalPrice", new Object[0]), currencyInstance, container);
        }

        container.setParent(parent);
    }

    /**
     * Gets the flybuys discount value.
     * 
     * @param abstractOrderModel
     *            the abstract order model
     * @return the flybuys discount value
     */
    protected boolean isFlybuysPointsConsumed(final AbstractOrderModel abstractOrderModel) {
        final Integer flybuysPointsConsumed = abstractOrderModel.getFlybuysPointsConsumed();
        return null != flybuysPointsConsumed
                && flybuysPointsConsumed.intValue() > 0;
    }

    /**
     * @param targetVoucherService
     *            the targetVoucherService to set
     */
    @Required
    public void setTargetVoucherService(final TargetVoucherService targetVoucherService) {
        this.targetVoucherService = targetVoucherService;
    }
}

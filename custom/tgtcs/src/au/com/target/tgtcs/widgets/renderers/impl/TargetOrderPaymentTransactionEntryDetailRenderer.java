/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.cscockpit.widgets.renderers.details.impl.OrderPaymentTransactionEntryDetailRenderer;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;


/**
 * @author mjanarth
 *
 */
public class TargetOrderPaymentTransactionEntryDetailRenderer extends OrderPaymentTransactionEntryDetailRenderer {

    @Override
    protected TypedObject getPaymentInfo(final TypedObject transactionEntry)
    {
        if (transactionEntry != null)
        {
            final PaymentTransactionEntryModel entry = (PaymentTransactionEntryModel)transactionEntry.getObject();
            final PaymentTransactionModel paymentTransactionModel = entry.getPaymentTransaction();
            final PaymentInfoModel ipgPaymentInfo = entry.getIpgPaymentInfo();
            if (paymentTransactionModel != null) {
                if (ipgPaymentInfo == null) {
                    return getCockpitTypeService().wrapItem(paymentTransactionModel.getInfo());
                }
                else {
                    ipgPaymentInfo.setBillingAddress(paymentTransactionModel.getInfo().getBillingAddress());
                    return getCockpitTypeService().wrapItem(ipgPaymentInfo);
                }
            }
        }
        return null;
    }
}

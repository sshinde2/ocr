package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.widgets.InputWidget;
import de.hybris.platform.cockpit.widgets.models.impl.DefaultListWidgetModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.controllers.CancellationController;
import de.hybris.platform.cscockpit.widgets.renderers.impl.PartialOrderCancellationWidgetRenderer;
import de.hybris.platform.cscockpit.widgets.renderers.utils.edit.BasicPropertyDescriptor;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import au.com.target.tgtcs.components.orderdetail.controllers.AddPaymentWindowController;
import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtcs.facade.TgtCsCardPaymentFacade;
import au.com.target.tgtcs.widgets.controllers.TargetCancellationController;
import au.com.target.tgtcs.widgets.controllers.impl.TargetCancellationControllerImpl;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;

import com.google.common.collect.ImmutableMap;


/**
 * Target extension to {@link PartialOrderCancellationWidgetRenderer}.
 */
public class TargetPartialOrderCancellationWidgetRenderer extends PartialOrderCancellationWidgetRenderer {
    private static final Logger LOG = Logger.getLogger(TargetPartialOrderCancellationWidgetRenderer.class);
    private static final String ADD_PAYMENT_BUTTON = "addPaymentButton";
    private static final String SHIPPING_AMOUNT_FIELD_CSS_CLASS = "tgtcs_input_tip";
    private static final String SHIPPING_AMOUNT_FIELD_QUALIFIER = "OrderCancelRequest.shippingAmountToRefund";

    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.00");
    private static final String TGTCS_HOSTED_PAYMENT_FORM_ZUL = "tgtcs/hostedPaymentForm.zul";
    private AddPaymentWindowController addPaymentWindowController;
    private TgtCsCardPaymentFacade tgtCsCardPaymentFacade;
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    /**
     * Removes "cancel reason" and "notes" fields from dialog box and adds new "shipping amount to refund" field to
     * dialog box.
     */
    @PostConstruct
    public void initialize() {
        final List<PropertyDescriptor> orderCancelRequestProperties = super.getOrderCancelRequestProperties();
        orderCancelRequestProperties.clear();
        orderCancelRequestProperties.add(
                new BasicPropertyDescriptor(SHIPPING_AMOUNT_FIELD_QUALIFIER, "DECIMAL"));
        setDefaultPropertyValuesMap(ImmutableMap.of(SHIPPING_AMOUNT_FIELD_QUALIFIER, (Object)Double.valueOf(0d)));
    }

    @Override
    protected ObjectValueContainer createOrderCancelRequestEditors(
            final InputWidget<DefaultListWidgetModel<TypedObject>, CancellationController> widget,
            final HtmlBasedComponent content) {
        final ObjectValueContainer result = super.createOrderCancelRequestEditors(widget, content);

        final TargetCancellationController controller = (TargetCancellationController)widget.getWidgetController();
        final double maxShippingToRefund = controller.getMaximumShippingAmountToRefund();
        final String formattedAmount = DECIMAL_FORMAT.format(maxShippingToRefund);

        final Label label = new Label(LabelUtils.getLabel(widget, "maxAllowed", formattedAmount));
        label.setSclass(SHIPPING_AMOUNT_FIELD_CSS_CLASS);
        // get last table row -> first and the only one div container -> input div container (which is last)
        label.setParent(content.getLastChild().getFirstChild().getLastChild());

        return result;
    }

    @Override
    protected org.zkoss.zk.ui.api.HtmlBasedComponent createContentInternal(
            final InputWidget<DefaultListWidgetModel<TypedObject>, CancellationController> widget,
            final org.zkoss.zk.ui.api.HtmlBasedComponent rootContainer) {

        tgtCsCardPaymentFacade.clearOldPayment();
        final Div mainContainer = (Div)super.createContentInternal(widget, rootContainer);

        final Button addPaymentButton = new Button(LabelUtils.getLabel(widget, ADD_PAYMENT_BUTTON, new Object[0]));
        addPaymentButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(final Event event) throws InterruptedException
            {
                TargetPartialOrderCancellationWidgetRenderer.this.handleRefundPaymentPopupEvent(widget);
            }
        });

        addPaymentButton.setParent(mainContainer.getFirstChild().getLastChild());
        return mainContainer;
    }

    /**
     * @param widget
     */
    protected void handleRefundPaymentPopupEvent(
            final InputWidget<DefaultListWidgetModel<TypedObject>, CancellationController> widget)
            throws InterruptedException {
        if (widget != null) {
            final CancellationController controller = widget.getWidgetController();
            if (controller != null && controller instanceof TargetCancellationControllerImpl) {
                final TargetCancellationControllerImpl controllerImpl = (TargetCancellationControllerImpl)controller;
                final TypedObject typedObject = controllerImpl.getOrder();
                if (typedObject != null) {
                    final Object object = typedObject.getObject();
                    if (object != null && object instanceof OrderModel) {
                        final OrderModel order = (OrderModel)object;
                        final Window window = getPaymentWindow(order);
                        try {
                            window.doModal();
                        }
                        catch (final Exception e) {
                            LOG.error(Labels.getLabel(TgtcsConstants.ERROR_MESSAGE_CODE), e);
                            Messagebox.show(e.getMessage(),
                                    Labels.getLabel(TgtcsConstants.ERROR_MESSAGE_CODE, new Object[0]), 1,
                                    "z-msgbox z-msgbox-error");
                        }
                    }
                }
            }
        }
    }

    /**
     * Gets the payment window.
     * 
     * @param orderModel
     *            the order model
     * @return the payment window
     */
    protected Window getPaymentWindow(final OrderModel orderModel) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        final Window window = (Window)Executions.createComponents(TGTCS_HOSTED_PAYMENT_FORM_ZUL, null,
                params);
        if (targetFeatureSwitchFacade.isFeatureEnabled("payment.ipg.cscockpit")) {
            addPaymentWindowController.populateCancelPaymentFormForIpg(orderModel, window);
        }
        else {
            addPaymentWindowController.populateCancelPaymentForm(orderModel, window);
        }
        return window;
    }

    /**
     * @param addPaymentWindowController
     *            the addPaymentWindowController to set
     */
    @Required
    public void setAddPaymentWindowController(final AddPaymentWindowController addPaymentWindowController) {
        this.addPaymentWindowController = addPaymentWindowController;
    }

    /**
     * @param tgtCsCardPaymentFacade
     *            the tgtCsCardPaymentFacade to set
     */
    @Required
    public void setTgtCsCardPaymentFacade(final TgtCsCardPaymentFacade tgtCsCardPaymentFacade) {
        this.tgtCsCardPaymentFacade = tgtCsCardPaymentFacade;
    }

    /**
     * @param targetFeatureSwitchFacade
     *            the targetFeatureSwitchFacade to set
     */
    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }



}

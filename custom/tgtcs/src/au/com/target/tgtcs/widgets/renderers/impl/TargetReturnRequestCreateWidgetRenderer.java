package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.widgets.InputWidget;
import de.hybris.platform.cockpit.widgets.models.impl.DefaultListWidgetModel;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.controllers.ReturnsController;
import de.hybris.platform.cscockpit.widgets.renderers.impl.ReturnRequestCreateWidgetRenderer;
import de.hybris.platform.cscockpit.widgets.renderers.utils.edit.BasicPropertyDescriptor;
import de.hybris.platform.cscockpit.widgets.renderers.utils.edit.BasicPropertyEditorRowConfiguration;
import de.hybris.platform.cscockpit.widgets.renderers.utils.edit.PojoPropertyRendererUtil;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;

import au.com.target.tgtcs.widgets.controllers.impl.TargetReturnsControllerImpl;

import com.google.common.collect.ImmutableMap;


/**
 * Target extension to {@link ReturnRequestCreateWidgetRenderer}.
 */
public class TargetReturnRequestCreateWidgetRenderer extends ReturnRequestCreateWidgetRenderer {

    private static final String SHIPPING_AMOUNT_FIELD_CSS_CLASS = "tgtcs_input_tip";
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.00");

    private static final String REFUND_VIEW = "RefundEntry";
    private static final String SHIP_AMOUNT_FIELD = "ReturnRequest.shippingAmountToRefund";

    private ObjectValueContainer form;

    /**
     * Sets the default value for "Delivery Cost" field.
     */
    @PostConstruct
    public void initialize() {
        setDefaultPropertyValuesMap(ImmutableMap.of(SHIP_AMOUNT_FIELD, (Object)Double.valueOf(0d)));
    }

    /**
     * Returns return / refund request properties.
     * 
     * @return the list of request property descriptors
     */
    protected List<PropertyDescriptor> getRequestPropertyDescriptors() {
        final List<PropertyDescriptor> formProperties = new ArrayList<>();
        formProperties.add(new BasicPropertyDescriptor(SHIP_AMOUNT_FIELD, "DECIMAL"));
        return formProperties;
    }

    @Override
    protected void renderListbox(final Listbox listBox,
            final InputWidget<DefaultListWidgetModel<TypedObject>, ReturnsController> widget) {
        super.renderListbox(listBox, widget);

        if (REFUND_VIEW.equalsIgnoreCase(getListConfigurationType())) {

            final List<PropertyDescriptor> propertyDescriptors = getRequestPropertyDescriptors();
            form = buildObjectValueContainer(null, propertyDescriptors, Collections.<String> emptySet());

            for (final PropertyDescriptor propertyDescriptor : propertyDescriptors) {
                final Div editorDiv = new Div();
                editorDiv.setSclass(CSS_EDITOR_WIDGET_EDITOR);

                PojoPropertyRendererUtil.renderEditor(editorDiv, propertyDescriptor,
                        new BasicPropertyEditorRowConfiguration(true, true, null, propertyDescriptor), null,
                        form, widget, true);

                listBox.getParent().getParent().appendChild(editorDiv);

                if (SHIP_AMOUNT_FIELD.equals(propertyDescriptor.getQualifier())) {
                    final TargetReturnsControllerImpl controller = (TargetReturnsControllerImpl)
                            widget.getWidgetController();
                    final double maxShippingToRefund = controller.getMaximumShippingAmountToRefund();
                    final String formattedAmount = DECIMAL_FORMAT.format(maxShippingToRefund);

                    final Label label = new Label(LabelUtils.getLabel(widget, "maxAllowed", formattedAmount));
                    label.setSclass(SHIPPING_AMOUNT_FIELD_CSS_CLASS);
                    editorDiv.getFirstChild().getLastChild().appendChild(label);
                }
            }
        }
    }

    @Override
    protected void handleReturnRequestCreateEvent(
            final InputWidget<DefaultListWidgetModel<TypedObject>, ReturnsController> widget, final Event event,
            final List<ObjectValueContainer> returnObjectValueContainers) throws Exception {
        final TargetReturnsControllerImpl controller = (TargetReturnsControllerImpl)widget.getWidgetController();
        controller.setAdditionalRequestParams(form);

        super.handleReturnRequestCreateEvent(widget, event, returnObjectValueContainers);
    }
}

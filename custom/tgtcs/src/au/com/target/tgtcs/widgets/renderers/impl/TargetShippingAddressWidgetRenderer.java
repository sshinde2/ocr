package au.com.target.tgtcs.widgets.renderers.impl;

import static au.com.target.tgtcs.constants.TgtcsConstants.EVENT_CLASS_KEY;
import static au.com.target.tgtcs.constants.TgtcsConstants.NEW_DELIVERY_MODE_KEY;
import static au.com.target.tgtcs.constants.TgtcsConstants.ORDER_KEY;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.util.TypeTools;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.utils.TypeUtils;
import de.hybris.platform.cscockpit.widgets.controllers.BasketController;
import de.hybris.platform.cscockpit.widgets.models.impl.ShippingCartAddressWidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.impl.ShippingAddressWidgetRenderer;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.ObjectUtils;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Br;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.ComponentNotFoundException;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtcs.widgets.controllers.TargetBasketController;
import au.com.target.tgtcs.widgets.controllers.TargetCallContextController;
import au.com.target.tgtcs.widgets.controllers.TargetCheckoutController;
import au.com.target.tgtcs.widgets.controllers.impl.TargetBasketControllersContainer;
import au.com.target.tgtcs.widgets.events.DeliveryModeChangeEvent;
import au.com.target.tgtutility.util.PhoneValidationUtils;
import au.com.target.tgtutility.util.TargetValidationCommon.Phone;


public class TargetShippingAddressWidgetRenderer extends ShippingAddressWidgetRenderer {

    public static final String DELIVERY_MODE_DROPDOWN_ID = "deliveryModeSelect";
    public static final String ADDRESS_DROPDOWN_ID = "deliveryAddressSelect";
    public static final String FREE_DELIVERY_CHECKBOX_ID = "freeDeliveryCheckbox";
    public static final String DEFAULT_STRATEGY = "default";

    private static final Logger LOG = Logger.getLogger(TargetShippingAddressWidgetRenderer.class);
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    private TypeService typeService;

    private ModelService modelService;

    private Listbox addressSelectComponent;
    private TargetBasketControllersContainer targetBasketControllersContainer;


    @Override
    protected HtmlBasedComponent createContentInternal(
            final Widget<ShippingCartAddressWidgetModel, BasketController> widget,
            final HtmlBasedComponent rootContainer) {
        CartModel cartModel = getCartModel(widget);

        final Div content = new Div();
        renderDeliveryModeBlock(widget, content);

        if (CollectionUtils.isEmpty(cartModel.getEntries())) {
            LOG.warn(
                    "Cart entries have reduced to zero, which would have prevented the 'Delivery Address' panel from displaying.");
        }

        // Refresh the cart to ensure that the cart entries are available. Some gremlin removes them deep inside renderDeliveryModeBlock.
        cartModel = getCartModel(widget);

        if (cartModel != null && CollectionUtils.isNotEmpty(cartModel.getEntries())) {
            final DeliveryModeModel deliveryModeModel = cartModel.getDeliveryMode();
            if (deliveryModeModel != null
                    && !getTargetDeliveryModeHelper().isDeliveryModeStoreDelivery(deliveryModeModel)) {
                final AddressModel deliveryAddress = cartModel.getDeliveryAddress();
                final TypedObject currentAddressObject = getCockpitTypeService().wrapItem(deliveryAddress);
                final HtmlBasedComponent addressSelectContent = createAddressSelect(widget, currentAddressObject);
                addressSelectComponent = (Listbox)addressSelectContent.getFirstChild().getFirstChild();
                content.appendChild(addressSelectContent);
            }
            renderFreeDelivery(widget, content);

            return content;
        }

        return null;
    }

    @Override
    protected void renderDeliveryModeBlock(final Widget<ShippingCartAddressWidgetModel, BasketController> widget,
            final HtmlBasedComponent parent) {
        final Div deliveryModeContainer = new Div();

        deliveryModeContainer.setSclass(CSS_DELIVERY_MODE_CONTAINER);
        deliveryModeContainer.setParent(parent);

        final CartModel cartModel = getCartModel(widget);
        if (cartModel != null) {
            DeliveryModeModel deliveryModeModel = cartModel.getDeliveryMode();

            final Label deliveryModeLabel = new Label(LabelUtils.getLabel(widget, "deliveryTypes"));
            deliveryModeLabel.setParent(deliveryModeContainer);

            final Listbox deliveryModeDropdown = getOrCreateComponentForID(DELIVERY_MODE_DROPDOWN_ID, widget,
                    new Listbox());
            deliveryModeDropdown.getChildren().clear();
            deliveryModeDropdown.setSelectedIndex(-1);
            deliveryModeDropdown.setMold("select");
            deliveryModeDropdown.setParent(deliveryModeContainer);
            removeListeners(deliveryModeDropdown, Events.ON_SELECT);
            deliveryModeDropdown.addEventListener(Events.ON_SELECT,
                    createDeliveryModeChangeEventListener(widget, getCockpitTypeService().wrapItem(cartModel)));
            final List<TypedObject> deliveryModes = ((TargetBasketController)widget.getWidgetController())
                    .getAvailableDeliveryModes(cartModel);
            final List<String> deliveryModeList = new ArrayList<>();
            if (deliveryModes != null && !deliveryModes.isEmpty()) {
                for (final TypedObject t : deliveryModes) {
                    if (t != null) {
                        final String deliveryModeText = TypeTools.getValueAsString(getLabelService(), t);

                        final Listitem deliveryModeItem = new Listitem(deliveryModeText, t);
                        deliveryModeItem.setParent(deliveryModeDropdown);
                        if (ObjectUtils.nullSafeEquals(deliveryModeModel, t.getObject())) {
                            deliveryModeItem.setSelected(true);
                        }
                        deliveryModeList.add(((TargetZoneDeliveryModeModel)t.getObject()).getCode());
                    }

                }
                //resetting delivery mode if it is not applicable to the cart.
                if (deliveryModeModel != null && !deliveryModeList.contains(deliveryModeModel.getCode())) {
                    deliveryModeModel = null;
                    final TargetBasketController controller = (TargetBasketController)widget.getWidgetController();
                    controller.removeDeliveryMode(cartModel);
                }

                // showing no delivery mode to be selected in the renderer so that agent can select the desired delivery mode - show dummy entry
                final Listitem notSelectedItem = new Listitem(LabelUtils.getLabel(widget, "dummyListEntry"), null);
                notSelectedItem.setParent(deliveryModeDropdown);
                if (deliveryModeDropdown.getSelectedItem() == null) {
                    notSelectedItem.setSelected(true);
                }
            }
            else {
                final Listitem emptyItem = new Listitem(LabelUtils.getLabel(widget, "deliveryTypeNoneAvailable"), null);
                emptyItem.setParent(deliveryModeDropdown);
                emptyItem.setSelected(true);
                deliveryModeDropdown.setDisabled(true);
            }

            deliveryModeContainer.appendChild(new Br());
            if (deliveryModeModel != null
                    && getTargetDeliveryModeHelper().isDeliveryModeStoreDelivery(deliveryModeModel)
                    && cartModel.getDeliveryAddress() != null) {
                final DeliveryModeModel deliveryModeModelForWrapping = deliveryModeModel;
                final AddressModel address = cartModel.getDeliveryAddress();
                // show selected store address
                final String storeAddress = String.format("%s: %s, %s, %s, %s, %s",
                        LabelUtils.getLabel(widget, "selectedStore"),
                        address.getTown(), address.getStreetname(),
                        address.getDistrict(), address.getPostalcode(),
                        address.getCountry().getName());
                final Label storeAddressLabel = new Label(storeAddress);
                storeAddressLabel.setParent(deliveryModeContainer);
                // show change store button
                final Button button = new Button();
                button.setLabel(LabelUtils.getLabel(widget, "changeStore"));
                button.setParent(deliveryModeContainer);
                button.addEventListener(Events.ON_CLICK,
                        new EventListener() {
                            @Override
                            public void onEvent(final Event event) throws Exception {
                                final TypedObject order = getTypeService().wrapItem(cartModel);
                                final TypedObject deliveryMode = getTypeService()
                                        .wrapItem(deliveryModeModelForWrapping);
                                fireDeliveryModeChangeEvent(deliveryMode, order, widget);
                            }
                        });
            }
        }
    }

    @Override
    protected void handleAddAddressClickEvent(final Widget widget, final Event event, final Div container) {
        super.handleAddAddressClickEvent(widget, event, container);
        for (final TargetBasketController controller : getTargetBasketControllersContainer().getBasketControllers()) {
            controller.setAddressSelectComponent(null);
        }
        final TargetBasketController basketController = (TargetBasketController)widget.getWidgetController();
        final TargetCallContextController callContextController = (TargetCallContextController)basketController
                .returnCallContextController();
        callContextController.setAddAddressInCheckoutTab(true);
        basketController.setAddressSelectComponent(addressSelectComponent);
    }

    @Override
    protected void handleDeliveryModeChangeEvent(
            final Widget<ShippingCartAddressWidgetModel, BasketController> widget,
            final SelectEvent selectEvent, final TypedObject cart) {
        final Object selectedItem = getSelectedItem(selectEvent);
        if ((selectedItem instanceof TypedObject)) {
            fireDeliveryModeChangeEvent((TypedObject)selectedItem, cart, widget);
        }
    }

    private <T extends HtmlBasedComponent> T getOrCreateComponentForID(final String id, final Widget widget,
            final T returnCandidate) {
        try {
            return (T)widget.getContent().getFellow(id);
        }
        catch (final ComponentNotFoundException e) {
            returnCandidate.setId(id);
            return returnCandidate;
        }
        catch (final NullPointerException e) {
            returnCandidate.setId(id);
            return returnCandidate;
        }

    }

    private void removeListeners(final Component component, final String event) {
        final Iterator<EventListener> iterator = component.getListenerIterator(event);
        EventListener listener;

        while (iterator.hasNext()) {
            listener = iterator.next();
            component.removeEventListener(event, listener);
        }
    }

    private void fireDeliveryModeChangeEvent(final TypedObject deliveryMode, final TypedObject order,
            final Widget<ShippingCartAddressWidgetModel, BasketController> widget) {
        final BasketController controller = widget.getWidgetController();

        final Map<String, Object> eventParameters = new HashMap<String, Object>();
        eventParameters.put(NEW_DELIVERY_MODE_KEY, deliveryMode);
        eventParameters.put(ORDER_KEY, order);
        eventParameters.put(EVENT_CLASS_KEY, DeliveryModeChangeEvent.class);

        controller.dispatchEvent(null, widget, eventParameters);
        controller.getCheckoutController().dispatchEvent(null, widget, eventParameters);
    }

    /**
     * @param widget
     *            the CheckoutController widget
     * @return checkout cart model
     */
    protected CartModel getCartModel(final Widget widget) {
        final TargetBasketController controller = (TargetBasketController)widget.getWidgetController();
        final CartModel cart = ((TargetCheckoutController)controller.getCheckoutController())
                .getCheckoutCartByMasterCart();
        getModelService().refresh(cart);
        return cart;
    }

    /**
     * Forces refresh of selected delivery mode.
     * 
     * @param widget
     *            the widget
     */
    public void updateSelectedDeliveryMode(final Widget widget) {
        final DeliveryModeModel deliveryMode = getCartModel(widget).getDeliveryMode();
        final Listbox deliveryModeDropdown = (Listbox)widget.getContent().getFellow(DELIVERY_MODE_DROPDOWN_ID);
        final List<Listitem> items = deliveryModeDropdown.getChildren();
        for (int i = 0; i < items.size(); i++) {
            if (deliveryMode.equals(TypeUtils.unwrapItem((TypedObject)items.get(i).getValue(), ItemModel.class))) {
                items.get(i).setSelected(true);
                return;
            }
        }
        // if nothing matched - deselect current item.
        deliveryModeDropdown.setSelectedIndex(-1);
    }

    @Override
    protected void handleSelectAddressEvent(final Widget widget, final SelectEvent selectEvent) {
        final Object selectedItem = getSelectedItem(selectEvent);
        if (!(selectedItem instanceof TypedObject)) {
            return;
        }

        final TypedObject address = (TypedObject)selectedItem;
        final AddressModel addressmodel = TypeUtils.unwrapItem(address, AddressModel.class);

        if ((StringUtils.isNotBlank(addressmodel.getPhone1()) && PhoneValidationUtils
                .validatePhoneNumber(addressmodel.getPhone1()))
                || (StringUtils.isNotBlank(addressmodel.getPhone2()) && PhoneValidationUtils
                        .validatePhoneNumber(addressmodel.getPhone2()))) {
            setSelectedAddress(widget, (TypedObject)selectedItem);
            final TargetBasketController basketController = (TargetBasketController)widget
                    .getWidgetController();
            basketController.returnCallContextController().dispatchEvent(null, this, null);
            for (final TargetBasketController controller : getTargetBasketControllersContainer()
                    .getBasketControllers()) {
                controller.dispatchEvent(null, this, null);
            }
        }
        else {
            try {
                LOG.info(Labels.getLabel(TgtcsConstants.PHONE_LENGTH_ERROR_MESSAGE,
                        new Object[] { Integer.valueOf(Phone.MIN_SIZE), Integer.valueOf(Phone.MAX_SIZE) })
                        + addressmodel.getPhone1());
                Messagebox.show(
                        Labels.getLabel(TgtcsConstants.PHONE_LENGTH_ERROR_MESSAGE,
                                new Object[] { Integer.valueOf(Phone.MIN_SIZE), Integer.valueOf(Phone.MAX_SIZE) }),
                        Labels.getLabel(TgtcsConstants.INVALID_ADDRESS), 1, "z-msgbox z-msgbox-error");
                addressSelectComponent.setSelectedIndex(-1);
            }
            catch (final InterruptedException e) {
                LOG.error("Failed to display message box to user", e);
                Thread.currentThread().interrupt();
            }
        }
    }

    /**
     * @return the targetBasketControllersContainer
     */
    public TargetBasketControllersContainer getTargetBasketControllersContainer() {
        return targetBasketControllersContainer;
    }

    /**
     * @param targetBasketControllersContainer
     *            the targetBasketControllersContainer to set
     */
    @Required
    public void setTargetBasketControllersContainer(
            final TargetBasketControllersContainer targetBasketControllersContainer) {
        this.targetBasketControllersContainer = targetBasketControllersContainer;
    }

    /**
     * render the free delivery checkbox
     * 
     * @param widget
     * @param parent
     */
    private void renderFreeDelivery(final Widget widget,
            final HtmlBasedComponent parent) {
        final Div freeDeliveryCheckboxContainer = new Div();
        freeDeliveryCheckboxContainer.setParent(parent);
        freeDeliveryCheckboxContainer.setSclass(CSS_DELIVERY_MODE_CONTAINER);

        final Checkbox freeDeliveryCheckbox = new Checkbox(LabelUtils.getLabel(widget, "freeDelivery"));
        freeDeliveryCheckbox.setParent(freeDeliveryCheckboxContainer);
        final CartModel cartModel = getCartModel(widget);
        freeDeliveryCheckbox.setChecked(cartModel.getCockpitFreeDelivery().booleanValue());

        freeDeliveryCheckbox.addEventListener(Events.ON_CHECK, new EventListener() {

            @Override
            public void onEvent(final Event event) throws Exception {
                //pass freeDeliveryCheckbox.isChecked() to the service to recalcuate the order.
                cartModel.setCockpitFreeDelivery(Boolean.valueOf(freeDeliveryCheckbox.isChecked()));

                ((TargetBasketController)widget.getWidgetController()).flipFreeDelivery(cartModel);
                //also update the total widget and payment widget
                widget.getWidgetModel().notifyListeners();
                widget.getWidgetController().dispatchEvent(null, widget, null);
            }
        });
    }

    /**
     * @return the targetDeliveryModeHelper
     */
    public TargetDeliveryModeHelper getTargetDeliveryModeHelper() {
        return targetDeliveryModeHelper;
    }

    /**
     * @param targetDeliveryModeHelper
     *            the targetDeliveryModeHelper to set
     */
    @Required
    public void setTargetDeliveryModeHelper(final TargetDeliveryModeHelper targetDeliveryModeHelper) {
        this.targetDeliveryModeHelper = targetDeliveryModeHelper;
    }

    /**
     * @return the typeService
     */
    public TypeService getTypeService() {
        return typeService;
    }

    /**
     * @param typeService
     *            the typeService to set
     */
    @Required
    public void setTypeService(final TypeService typeService) {
        this.typeService = typeService;
    }

    /**
     * @return the modelService
     */
    public ModelService getModelService() {
        return modelService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

}
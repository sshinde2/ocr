/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.widgets.ListboxWidget;
import de.hybris.platform.cscockpit.services.search.generic.query.DefaultTicketPoolSearchQueryBuilder;
import de.hybris.platform.cscockpit.services.search.generic.query.DefaultTicketPoolSearchQueryBuilder.SearchGroup;
import de.hybris.platform.cscockpit.services.search.impl.DefaultCsTextSearchCommand;
import de.hybris.platform.cscockpit.utils.CssUtils;
import de.hybris.platform.cscockpit.widgets.controllers.search.SearchCommandController;
import de.hybris.platform.cscockpit.widgets.models.impl.TextSearchWidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.impl.TicketPoolWidgetRenderer;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zul.Div;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;

import au.com.target.tgtcs.csagent.service.TargetCsAgentGroupService;
import au.com.target.tgtcs.search.services.query.TargetTicketPoolSearchQueryBuilder;
import au.com.target.tgtcs.search.services.query.TargetTicketPoolSearchQueryBuilder.DynamicSearchGroup;


/**
 * @author mjanarth
 *
 */
public class TargetTicketPoolWidgetRenderer extends TicketPoolWidgetRenderer {

    private TargetCsAgentGroupService targetCsAgentGroupService;

    @Override
    protected HtmlBasedComponent createSearchPane(
            final ListboxWidget<TextSearchWidgetModel, SearchCommandController<DefaultCsTextSearchCommand>> widget) {
        final Div searchPane = new Div();

        final Listbox searchAgentListbox = createSearchAgentField(widget, searchPane);
        final Listbox searchGroupListbox = createSearchGroupField(widget, searchPane);
        final Listbox searchStatusListbox = createSearchStatusField(widget, searchPane);
        attachSearchEventListener(widget, new TargetSearchGroupEventListener(widget, searchAgentListbox,
                searchGroupListbox,
                searchStatusListbox), new AbstractComponent[] { searchAgentListbox, searchGroupListbox,
                searchStatusListbox });
        return searchPane;
    }

    @Override
    protected Listbox createSearchGroupField(
            final ListboxWidget<TextSearchWidgetModel, SearchCommandController<DefaultCsTextSearchCommand>> widget,
            final Div parent) {
        final List<String> csAgentGrps = targetCsAgentGroupService.getAllCustomerAgentGroupUid();
        final DefaultCsTextSearchCommand searchCommand = widget.getWidgetModel().getSearchCommand();
        final Listbox listbox = createSearchDropdownField(widget, parent, "searchGroupText",
                DefaultTicketPoolSearchQueryBuilder.SearchGroup.class,
                CssUtils.combine(new String[] { "csTicketPoolSearchBlock", "csTicketPoolSearchGroup" }), null);
        final String selectCustomerGroup = searchCommand.getText(DynamicSearchGroup.SearchText);
        if (CollectionUtils.isNotEmpty(csAgentGrps)) {
            String csGroup = null;
            for (int i = 0; i < csAgentGrps.size(); i++) {
                csGroup = csAgentGrps.get(i);
                listbox.appendItem(csGroup, csGroup);
            }
        }
        if (StringUtils.isNotEmpty(selectCustomerGroup)) {
            setSelectedIndexForCustomGroup(listbox, selectCustomerGroup);
        }
        return listbox;
    }

    /**
     * 
     * @param listBox
     * @param customerGroup
     */
    protected void setSelectedIndexForCustomGroup(final Listbox listBox, final String customerGroup) {
        int selectedIndex = 0;
        if (StringUtils.isNotEmpty(customerGroup)) {
            for (int i = 0; i < listBox.getItemCount(); i++) {
                if (customerGroup.equalsIgnoreCase(listBox.getItemAtIndex(i).getLabel())) {
                    selectedIndex = i;
                    listBox.setSelectedIndex(selectedIndex);
                    break;
                }
            }
        }
    }


    protected class TargetSearchGroupEventListener extends SearchEventListener {

        private final transient Listbox searchAgentListbox;
        private final transient Listbox searchGroupListbox;
        private final transient Listbox searchStatusListbox;

        /**
         * @param widget
         * @param searchAgentListbox
         * @param searchGroupListbox
         * @param searchStatusListbox
         */
        public TargetSearchGroupEventListener(
                final ListboxWidget<TextSearchWidgetModel, SearchCommandController<DefaultCsTextSearchCommand>> widget,
                final Listbox searchAgentListbox, final Listbox searchGroupListbox, final Listbox searchStatusListbox) {
            super(widget, searchAgentListbox, searchGroupListbox, searchStatusListbox);
            this.searchAgentListbox = searchAgentListbox;
            this.searchGroupListbox = searchGroupListbox;
            this.searchStatusListbox = searchStatusListbox;
        }

        @Override
        protected void fillSearchCommand(final DefaultCsTextSearchCommand command) {
            command.addFlag(getSelectedValue(DefaultTicketPoolSearchQueryBuilder.SearchAgent.class, searchAgentListbox));
            command.addFlag(getSelectedValue(DefaultTicketPoolSearchQueryBuilder.SearchStatus.class,
                    searchStatusListbox));

            setSelectedSearchGroupValue(DefaultTicketPoolSearchQueryBuilder.SearchGroup.class,
                    searchGroupListbox, command);

        }

        /**
         * 
         * @param enumType
         * @param listbox
         * @param command
         */
        protected void setSelectedSearchGroupValue(final Class<SearchGroup> enumType, final Listbox listbox,
                final DefaultCsTextSearchCommand command) {
            if (listbox != null) {
                final Listitem selectedItem = listbox.getSelectedItem();
                if (selectedItem != null) {
                    final String valueText = (String)selectedItem.getValue();
                    if ((valueText != null) && (!valueText.isEmpty())) {
                        try {
                            command.addFlag(Enum.valueOf(enumType, valueText));
                            return;
                        }
                        catch (final IllegalArgumentException ex) {
                            command.setText(TargetTicketPoolSearchQueryBuilder.DynamicSearchGroup.SearchText, valueText);
                            return;
                        }
                    }
                }
            }
            command.addFlag(getDefaultValue(getWidget(), enumType));
        }
    }

    /**
     * @param targetCsAgentGroupService
     *            the targetCsAgentGroupService to set
     */
    @Required
    public void setTargetCsAgentGroupService(final TargetCsAgentGroupService targetCsAgentGroupService) {
        this.targetCsAgentGroupService = targetCsAgentGroupService;
    }

    /**
     * @return the targetCsAgentGroupService
     */
    public TargetCsAgentGroupService getTargetCsAgentGroupService() {
        return targetCsAgentGroupService;
    }

}

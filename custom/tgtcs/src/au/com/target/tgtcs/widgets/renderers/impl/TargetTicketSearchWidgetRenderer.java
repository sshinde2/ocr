/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.util.UITools;
import de.hybris.platform.cockpit.widgets.ListboxWidget;
import de.hybris.platform.cscockpit.services.search.impl.DefaultCsTextSearchCommand;
import de.hybris.platform.cscockpit.widgets.controllers.search.SearchCommandController;
import de.hybris.platform.cscockpit.widgets.models.impl.TextSearchWidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.impl.TicketSearchWidgetRenderer;
import de.hybris.platform.util.localization.Localization;

import org.apache.commons.lang.StringUtils;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.impl.InputElement;


/**
 * 
 *
 */
public class TargetTicketSearchWidgetRenderer extends TicketSearchWidgetRenderer {

    protected static final String SEARCH_LABEL_I18N_KEY = "tgtcscockpit.widget.ticket.search.label";

    /* (non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.renderers.impl.TicketSearchWidgetRenderer#createSearchPane(de.hybris.platform.cockpit.widgets.ListboxWidget)
     */
    @Override
    protected HtmlBasedComponent createSearchPane(
            final ListboxWidget<TextSearchWidgetModel, SearchCommandController<DefaultCsTextSearchCommand>> widget) {
        final Div searchPane = new Div();

        final Textbox searchTextInput = createTicketSearchField(widget, searchPane);
        final Button searchBtn = createSearchButton(widget, searchPane);

        attachSearchEventListener(widget, createSearchEventListener(widget, searchTextInput), new AbstractComponent[] {
                searchTextInput,
                searchBtn });

        return searchPane;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.cscockpit.widgets.renderers.impl.AbstractSearcherWidgetRenderer#createSearchTextField(de.hybris.platform.cockpit.widgets.ListboxWidget, org.zkoss.zul.Div, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    protected Textbox createSearchTextField(
            final ListboxWidget<TextSearchWidgetModel, SearchCommandController<DefaultCsTextSearchCommand>> widget,
            final Div parent, final String labelKey, final String currentValue, final String testId) {
        final Label fieldLabel = new Label(Localization.getLocalizedString(SEARCH_LABEL_I18N_KEY));

        final Textbox fieldTextBox = new Textbox(currentValue != null ? currentValue : StringUtils.EMPTY);
        if ((UISessionUtils.getCurrentSession().isUsingTestIDs()) && (testId != null)) {
            UITools.applyTestID(fieldTextBox, testId);
        }

        parent.appendChild(fieldLabel);
        parent.appendChild(fieldTextBox);

        return fieldTextBox;
    }


    /**
     * create search event listener
     * 
     * @param widget
     *            search widget
     * @param searchInput
     *            search input control
     */
    protected EventListener createSearchEventListener(
            final ListboxWidget<TextSearchWidgetModel, SearchCommandController<DefaultCsTextSearchCommand>> widget,
            final InputElement searchInput) {
        return new TargetSearchEventListener(widget, searchInput, null);
    }


    /**
     * search event listener, paging listener on ticket searching popup
     */
    protected class TargetSearchEventListener extends SearchEventListener {

        /**
         * @param widget
         * @param inputComponent
         * @param searchTypeCheckbox
         */
        public TargetSearchEventListener(
                final ListboxWidget<TextSearchWidgetModel, SearchCommandController<DefaultCsTextSearchCommand>> widget,
                final InputElement inputComponent, final Checkbox searchTypeCheckbox) {
            super(widget, inputComponent, searchTypeCheckbox);
        }

    }

}

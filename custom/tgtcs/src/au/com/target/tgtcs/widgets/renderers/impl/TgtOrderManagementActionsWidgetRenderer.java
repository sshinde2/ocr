package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cockpit.widgets.models.impl.DefaultItemWidgetModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.widgets.controllers.OrderManagementActionsWidgetController;
import de.hybris.platform.cscockpit.widgets.renderers.impl.OrderManagementActionsWidgetRenderer;
import de.hybris.platform.ordercancel.CancelDecision;
import de.hybris.platform.ordercancel.OrderCancelService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.returns.ReturnService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.ApplicationContext;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zul.Div;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.refund.TargetRefundService;
import au.com.target.tgtcore.util.ProductUtil;
import au.com.target.tgtcs.widgets.controllers.TargetOrderManagementActionsWidgetController;


public class TgtOrderManagementActionsWidgetRenderer extends OrderManagementActionsWidgetRenderer {


    private static final Logger LOG = Logger.getLogger(TgtOrderManagementActionsWidgetRenderer.class);
    private static final String CANCELLATION_ERROR_MESSAGE = "failed to work out if cancellation or order is possible";
    private static final String ORDER_CANCEL_SERVICE = "orderCancelService";
    private static final String USER_SERVICE_BEAN_NAME = "userService";
    private static final String RETURN_SERVICE = "returnService";
    private static final String APPROVE_ORDER_POPUP_LABEL = "label.approveOrder";

    private static final String REJECT_ORDER_POPUP_LABEL = "label.rejectOrder";

    private static final String HOLD_FRAUD_REVIEW_ORDER_POPUP_LABEL = "label.holdOrder";

    private static final String MANUAL_REFUND_POPUP_LABEL = "label.manualRefund";

    private static final String REFUND_SERVICE = "refundService";

    private final UserService userService;
    private final ApplicationContext ctx;
    private final ReturnService returnService;

    private final TargetRefundService targetRefundService;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private List<ConsignmentStatus> cancelDeniedConsignmentStates;

    public TgtOrderManagementActionsWidgetRenderer() {
        this.ctx = de.hybris.platform.core.Registry.getApplicationContext();
        this.userService = (UserService)ctx.getBean(USER_SERVICE_BEAN_NAME);
        this.returnService = ((ReturnService)ctx.getBean(RETURN_SERVICE));
        this.targetRefundService = ((TargetRefundService)ctx.getBean(REFUND_SERVICE));
    }





    /**
     * Order management is disabled since is done by custom OMS
     * 
     * @param widget
     * @param rootContainer
     * @return HtmlBasedComponent
     */
    @Override
    protected HtmlBasedComponent createContentInternal(
            final Widget<DefaultItemWidgetModel, OrderManagementActionsWidgetController> widget,
            final HtmlBasedComponent rootContainer) {
        TargetOrderManagementActionsWidgetController widgetController = null;
        final Div component = new Div();
        component.setSclass("orderManagementActionsWidget");
        final OrderManagementActionsWidgetController orderManagementActionsWidgetController = widget
                .getWidgetController();
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.CSCOCKPIT_CANCEL_BUTTON)) {
            createButton(widget, component, "csCancelPartialOrder", "csPartialOrderCancellationWidgetConfig",
                    "csPartialOrderCancellationWidgetConfig-Popup", "csPartialCancelPopup",
                    "popup.partialCancellationRequestCreate",
                    !isPartialCancelPossible(getOrder(widget)));
        }
        else {
            createButton(widget, component, "csCancelPartialOrder", "csPartialOrderCancellationWidgetConfig",
                    "csPartialOrderCancellationWidgetConfig-Popup", "csPartialCancelPopup",
                    "popup.partialCancellationRequestCreate",
                    true);
        }

        createButton(widget, component, "csCancelWholeOrder", "csFullOrderCancellationWidgetConfig",
                "csFullOrderCancel-Popup",
                "csFullCancelPopup", "popup.fullCancellationRequestCreate",
                !isFullCancelPossible(getOrder(widget)));
        createButton(widget, component, "csRefundOrder", "csRefundRequestCreateWidgetConfig",
                "csRefundRequestCreateWidget-Popup",
                "csReturnRequestCreateWidget", "popup.refundRequestCreate", !isRefundAllowed(getOrder(widget)));

        if (orderManagementActionsWidgetController instanceof TargetOrderManagementActionsWidgetController) {
            widgetController = (TargetOrderManagementActionsWidgetController)orderManagementActionsWidgetController;


            createButton(widget, component, HOLD_FRAUD_REVIEW_ORDER_POPUP_LABEL,
                    "csHoldFraudStatusWidgetConfig",
                    "csHoldFraudStatusWidgetConfig-Popup", "csFraudStatusPopup",
                    "popup.csFraudStatus", isHoldOrderButtonDisable(widgetController));

            createButton(widget, component, APPROVE_ORDER_POPUP_LABEL,
                    "csApproveFraudStatusWidgetConfig",
                    "csApproveFraudStatusWidgetConfig-Popup", "csFraudStatusPopup",
                    "popup.csFraudStatus", isReviewOrderButtonDisable(widgetController));

            createButton(widget, component, REJECT_ORDER_POPUP_LABEL,
                    "csRejectFraudStatusWidgetConfig",
                    "csRejectFraudStatusWidgetConfig-Popup", "csFraudStatusPopup",
                    "popup.csFraudStatus", isReviewOrderButtonDisable(widgetController));

            createButton(widget, component, MANUAL_REFUND_POPUP_LABEL,
                    "csManualRefundWidgetConfig",
                    "csManualRefundWidgetConfig-Popup", "csFraudStatusPopup",
                    "popup.manualRefund", isManualRefundButtonDisable(widgetController));


        }
        return component;
    }


    protected OrderModel getOrder(final Widget<DefaultItemWidgetModel, OrderManagementActionsWidgetController> widget) {
        final OrderManagementActionsWidgetController orderManagementWidget = widget.getWidgetController();
        final TypedObject order = orderManagementWidget.getOrder();
        if (order != null && order.getObject() instanceof OrderModel) {
            return (OrderModel)order.getObject();
        }
        return null;
    }

    /**
     * 
     * @param orderModel
     *            the order
     * @return isPartialCancelPossible
     */
    protected boolean isPartialCancelPossible(final OrderModel orderModel) {
        if (orderModel != null && StringUtils.isBlank(orderModel.getVersionID())) {
            try {
                final CancelDecision cancelable = ((OrderCancelService)ctx.getBean(ORDER_CANCEL_SERVICE))
                        .isCancelPossible(orderModel, userService.getCurrentUser(), true, true);
                return cancelable != null && cancelable.isAllowed();
            }
            catch (final Exception e) {
                LOG.error(CANCELLATION_ERROR_MESSAGE, e);
            }
        }
        return false;
    }

    /**
     * 
     * @param orderModel
     *            the order
     * @return isFullCancelPossible
     */
    protected boolean isFullCancelPossible(final OrderModel orderModel) {
        if ((orderModel != null) && (StringUtils.isBlank(orderModel.getVersionID()))) {
            try {

                if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)
                        && CollectionUtils.isNotEmpty(orderModel.getConsignments())
                        && !evaluateCancelButtonEnableDisableSituation(orderModel.getConsignments())) {
                    return false;
                }
                else if (targetFeatureSwitchService
                        .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.CSCOCKPIT_FULL_CANCEL_BUTTON)
                        && CollectionUtils.isNotEmpty(orderModel.getConsignments())
                        && !validateConsignmentStatus(orderModel.getConsignments())) {
                    return false;
                }
                else {
                    final CancelDecision cancelable = ((OrderCancelService)ctx
                            .getBean(ORDER_CANCEL_SERVICE)).isCancelPossible(orderModel,
                                    userService.getCurrentUser(), false, false);
                    return ((cancelable != null) && (cancelable.isAllowed()));
                }
            }
            catch (final Exception e) {
                LOG.error(
                        CANCELLATION_ERROR_MESSAGE,
                        e);
            }
        }
        return false;
    }

    /**
     * 
     * @param consignments
     * @return false or true based on consignments type
     */
    private boolean evaluateCancelButtonEnableDisableSituation(final Set<ConsignmentModel> consignments) {
        if (anyConsignmentHasPhysicalOrDigitalProduct(consignments)) {
            return false;
        }
        else if (anyConsignmentInDenialStatus(consignments)) {
            return false;
        }
        return true;
    }

    /**
     * @param consignments
     * @return return false if any consignment is picked
     */
    private boolean anyConsignmentInDenialStatus(final Set<ConsignmentModel> consignments) {
        for (final ConsignmentModel conModel : consignments) {
            if (!conModel.getStatus().equals(ConsignmentStatus.CANCELLED)
                    && cancelDeniedConsignmentStates.contains(conModel.getStatus())) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param consignments
     * @return false if any consignment has physical or digital gift card
     */
    private boolean anyConsignmentHasPhysicalOrDigitalProduct(final Set<ConsignmentModel> consignments) {
        for (final ConsignmentModel conModel : consignments) {
            for (final ConsignmentEntryModel conEntry : conModel.getConsignmentEntries()) {
                if (null != conEntry.getOrderEntry()
                        && (ProductUtil.isProductTypeDigital(conEntry.getOrderEntry().getProduct())
                                || ProductUtil.isProductTypePhysicalGiftcard(conEntry.getOrderEntry().getProduct()))) {
                    return true;
                }
            }

        }
        return false;
    }

    /**
     * @param consignments
     * @return false if there are more than one consignment not in cancelled status
     */
    private boolean validateConsignmentStatus(final Set<ConsignmentModel> consignments) {
        int counter = 0;
        for (final ConsignmentModel conModel : consignments) {
            if (!conModel.getStatus().equals(ConsignmentStatus.CANCELLED)) {
                counter += 1;
            }
        }
        return counter > 1 ? false : true;
    }

    /**
     * Checks if is refund allowed.
     * 
     * @param order
     *            the order
     * @return true, if is refund allowed
     */
    protected boolean isRefundAllowed(final OrderModel order) {
        if (order != null) {
            final Map<AbstractOrderEntryModel, Long> returnableOrderEntries = returnService
                    .getAllReturnableEntries(order);
            return (((returnableOrderEntries != null) && !returnableOrderEntries.isEmpty())
                    || isDeliverCostRefundable(order));
        }
        return false;
    }

    /**
     * @param order
     * @return boolean
     */
    private boolean isDeliverCostRefundable(final OrderModel order) {
        return targetRefundService.isDeliveryCostRefundable(order);
    }

    /**
     * Returns false if the hold order is true
     * 
     * @return boolean
     */
    protected boolean isHoldOrderButtonDisable(final TargetOrderManagementActionsWidgetController controller) {
        return !controller.isHoldOrderPossible();
    }

    /**
     * Returns true if the review order is false
     * 
     * @return boolean
     */
    protected boolean isReviewOrderButtonDisable(final TargetOrderManagementActionsWidgetController controller) {
        return !controller.isOrderFraudReviewPossible();
    }

    /**
     * Returns true if the refundable amount is bigger than zero
     * 
     * @param controller
     * @return boolean
     */
    protected boolean isManualRefundButtonDisable(final TargetOrderManagementActionsWidgetController controller) {
        return !controller.isManualRefundPossible();
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * @param cancelDeniedConsignmentStates
     *            the cancelDeniedConsignmentStates to set
     */
    @Required
    public void setCancelDeniedConsignmentStates(final List<ConsignmentStatus> cancelDeniedConsignmentStates) {
        this.cancelDeniedConsignmentStates = cancelDeniedConsignmentStates;
    }

}

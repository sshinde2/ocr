/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.model.meta.ObjectType;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.config.ColumnConfiguration;
import de.hybris.platform.cockpit.services.config.EditorConfiguration;
import de.hybris.platform.cockpit.services.config.EditorRowConfiguration;
import de.hybris.platform.cockpit.services.config.impl.PropertyColumnConfiguration;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer.ObjectValueHolder;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.widgets.InputWidget;
import de.hybris.platform.cockpit.widgets.models.ListWidgetModel;
import de.hybris.platform.cockpit.widgets.models.impl.DefaultListWidgetModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.utils.CockpitUiConfigLoader;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.utils.comparators.TypedObjectOrderEntryNumberComparator;
import de.hybris.platform.cscockpit.widgets.controllers.ReturnsController;
import de.hybris.platform.cscockpit.widgets.renderers.impl.ReturnRequestCreateWidgetRenderer;
import de.hybris.platform.cscockpit.widgets.renderers.utils.edit.BasicPropertyDescriptor;
import de.hybris.platform.cscockpit.widgets.renderers.utils.edit.BasicPropertyEditorRowConfiguration;
import de.hybris.platform.cscockpit.widgets.renderers.utils.edit.PojoPropertyRendererUtil;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import au.com.target.tgtcs.components.orderdetail.controllers.AddPaymentWindowController;
import au.com.target.tgtcs.components.orderdetail.utils.DiscountUtil;
import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtcs.facade.TgtCsCardPaymentFacade;
import au.com.target.tgtcs.widgets.controllers.impl.TargetReturnsControllerImpl;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;


/**
 * @author Nandini
 * 
 */
public class TgtReturnRequestCreateWidgetRenderer extends ReturnRequestCreateWidgetRenderer {
    private static final String WIDTH_MAX_QTY = "55px";
    private static final String WIDTH_PRODUCT_INFO = "250px";
    private static final String WIDTH_ENTRY_NUMBER = "20px";
    private static final String WIDTH_QUANTITY = "110px";
    private static final String WIDTH_REASON = "170px";
    private static final String WIDTH_ACTION = "170px";
    private static final String WIDTH_DISCOUNT = "60px";
    private static final String WIDTH_BASE_PRICE = "60px";
    private static final String WIDTH_NOTES = "70px";
    private static final String FAILED_TO_RENDER_RETURN_ENTRIES_LIST = "failed to render return entries list";
    private static final String MAX_ALLOWED = "maxAllowed";
    private static final String LISTBOX_ROW_ITEM = "listbox-row-item";
    private static final String DECIMAL = "DECIMAL";
    private static final String CS_CANT_RETURN = "csCantReturn";
    private static final String CANT_RETURN = "cantReturn";
    private static final String CREATE_BUTTON = "createButton";
    private static final String CS_RETURN_REQUEST_ACTIONS = "csReturnRequestActions";
    private static final String CS_WIDGET_LISTBOX = "csWidgetListbox";
    private static final String CS_LISTBOX_CONTAINER = "csListboxContainer";
    private static final String DISCOUNT_FIELD = "discount";
    private static final String MAX_QTY_FIELD = "maxQty";
    private static final String BASE_PRICE_FIELD = "basePrice";
    private static final String PRODUCT_INFO_FILED = "productInfo";
    private static final String ENTRY_NUMBER_FIELD = "entryNumber";

    private static final String REASON_CODE_COLUMN_KEY = "reasonCode.label";
    private static final String ACTION_COLUMN_KEY = "action.label";
    private static final String NOTES_COLUMN_KEY = "notes.label";
    private static final String QUANTITY_COLUMN_KEY = "quantity.label";

    private static final String SHIPPING_AMOUNT_FIELD_CSS_CLASS = "tgtcs_input_tip";
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.00");

    private static final String REFUND_VIEW = "RefundEntry";
    private static final String SHIP_AMOUNT_FIELD = "ReturnRequest.shippingAmountToRefund";
    private static final String EXPECTED_QTY_PROPERTY_QUALIFIER = "ReturnEntry.expectedQuantity";
    private static final String ADD_NEW_PAYMENT_BUTTON = "addNewPaymentButton";
    private static final String TGTCS_HOSTED_PAYMENT_FORM_ZUL = "tgtcs/hostedPaymentForm.zul";
    private static final String EDITOR_WIDGET_EDITOR_CSSCLASS = "editorWidgetEditor";
    private static final Logger LOG = Logger.getLogger(TgtReturnRequestCreateWidgetRenderer.class);

    private ObjectValueContainer form;
    private List<ObjectValueContainer> returnObjectValueContainers;
    private AddPaymentWindowController addPaymentWindowController;
    private TgtCsCardPaymentFacade tgtCsCardPaymentFacade;
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Override
    protected HtmlBasedComponent createContentInternal(
            final InputWidget<DefaultListWidgetModel<TypedObject>, ReturnsController> widget,
            final HtmlBasedComponent rootContainer) {

        final Div content = new Div();

        this.returnObjectValueContainers = new ArrayList();
        tgtCsCardPaymentFacade.clearOldPayment();
        final ReturnsController widgetController = widget.getWidgetController();

        if (widgetController != null && widgetController.canReturn()) {
            final Div returnableContainer = new Div();
            returnableContainer.setSclass(CS_LISTBOX_CONTAINER);
            returnableContainer.setParent(content);

            final Listbox listBox = new Listbox();
            listBox.setParent(returnableContainer);
            listBox.setVflex(false);
            listBox.setFixedLayout(true);
            listBox.setSclass(CS_WIDGET_LISTBOX);

            renderListbox(listBox, widget);

            final Div requestCreationContent = new Div();
            requestCreationContent.setSclass(CS_RETURN_REQUEST_ACTIONS);
            requestCreationContent.setParent(content);
            final Button paymentButton = new Button(LabelUtils.getLabel(widget, ADD_NEW_PAYMENT_BUTTON, new Object[0]));
            paymentButton.setParent(requestCreationContent);
            paymentButton.addEventListener(Events.ON_CLICK, new EventListener() {
                @Override
                public void onEvent(final Event event) throws InterruptedException
                {
                    TgtReturnRequestCreateWidgetRenderer.this.handleRefundPaymentPopupeEvent(widget);
                }
            });
            final Button createButton = new Button(LabelUtils.getLabel(widget, CREATE_BUTTON, new Object[0]));
            createButton.setParent(requestCreationContent);
            createButton.addEventListener(Events.ON_CLICK,
                    createReturnRequestCreateEventListener(widget, this.returnObjectValueContainers));
        }
        else {
            final Label dummyLabel = new Label(LabelUtils.getLabel(widget, CANT_RETURN, new Object[0]));
            dummyLabel.setSclass(CS_CANT_RETURN);
            dummyLabel.setParent(content);
        }

        return content;
    }

    /**
     * Handle refund payment popupe event.
     * 
     * @param widget
     *            the widget
     * @throws InterruptedException
     *             the interrupted exception
     */
    protected void handleRefundPaymentPopupeEvent(
            final InputWidget<DefaultListWidgetModel<TypedObject>, ReturnsController> widget)
            throws InterruptedException
    {
        if (widget != null) {
            final ReturnsController controller = widget.getWidgetController();
            if (controller != null && controller instanceof TargetReturnsControllerImpl) {
                final TargetReturnsControllerImpl controllerImpl = (TargetReturnsControllerImpl)controller;
                final TypedObject typedObject = controllerImpl.getCurrentOrder();
                if (typedObject != null) {
                    final Object object = typedObject.getObject();
                    if (object != null && object instanceof OrderModel) {
                        final OrderModel order = (OrderModel)object;
                        final Window window = getPaymentWindow(order);
                        try {
                            window.doModal();
                        }
                        catch (final Exception e) {
                            LOG.error(Labels.getLabel(TgtcsConstants.ERROR_MESSAGE_CODE), e);
                            Messagebox.show(e.getMessage(),
                                    Labels.getLabel(TgtcsConstants.ERROR_MESSAGE_CODE, new Object[0]), 1,
                                    "z-msgbox z-msgbox-error");
                        }
                    }
                }
            }
        }
    }


    /**
     * Gets the payment window.
     * 
     * @param orderModel
     *            the order model
     * @return the payment window
     */
    protected Window getPaymentWindow(final OrderModel orderModel) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        final Window window = (Window)Executions.createComponents(TGTCS_HOSTED_PAYMENT_FORM_ZUL, null,
                params);
        if (targetFeatureSwitchFacade.isFeatureEnabled("payment.ipg.cscockpit")) {
            addPaymentWindowController.populateIpgRefundPaymentForm(orderModel, window);
        }
        else {
            addPaymentWindowController.populateRefundPaymentForm(orderModel, window);
        }
        return window;
    }

    /**
     * Returns return / refund request properties.
     * 
     * @return the list of request property descriptors
     */
    private List<PropertyDescriptor> getRequestPropertyDescriptors() {
        final List<PropertyDescriptor> formProperties = new ArrayList<>();
        formProperties.add(new BasicPropertyDescriptor(SHIP_AMOUNT_FIELD, DECIMAL));
        return formProperties;
    }

    @Override
    protected void renderListbox(
            final Listbox listBox,
            final InputWidget<DefaultListWidgetModel<TypedObject>, ReturnsController> widget) {

        final ListWidgetModel<?> widgetModel = widget.getWidgetModel();

        if (widgetModel == null) {
            return;
        }
        if (!widget.getWidgetController().canReturn()) {
            return;
        }
        final Map<TypedObject, ?> returnableOrderEntries = widget.getWidgetController()
                .getReturnableOrderEntries();


        try {
            final List<ColumnConfiguration> columns = getColumnConfigurations(getListConfigurationCode(),
                    getListConfigurationType());
            if (!(CollectionUtils.isNotEmpty(columns))) {
                return;
            }
            final Listhead headRow = new Listhead();
            headRow.setParent(listBox);
            if (returnableOrderEntries != null && !returnableOrderEntries.isEmpty()) {
                prepareFixedColums(widget, headRow);
            }
            final EditorConfiguration editorConf = CockpitUiConfigLoader.getEditorConfiguration(
                    UISessionUtils.getCurrentSession(), getListEditorConfigurationCode(), getListConfigurationType());
            final List<EditorRowConfiguration> editorMapping = getPropertyEditorHelper().getAllEditorRowConfigurations(
                    editorConf);
            final Set<TypedObject> set = returnableOrderEntries.keySet();
            final List<TypedObject> orderEntries = new ArrayList(set);
            Collections.sort(orderEntries, TypedObjectOrderEntryNumberComparator.INSTANCE);

            for (final TypedObject item : orderEntries) {
                final Listitem row = new Listitem();
                row.setSclass(LISTBOX_ROW_ITEM);
                row.setParent(listBox);
                fillFixedColumns(returnableOrderEntries, item, row);
                final TypedObject returnEntryObject = null;
                final ObjectType returnEntryType = getCockpitTypeService().getObjectType(getListConfigurationType());
                final ObjectValueContainer returnEntryValueContainer = buildReturnEntryValueContainer(item,
                        returnEntryType,
                        returnEntryType.getPropertyDescriptors(), getSystemService().getAvailableLanguageIsos());
                this.returnObjectValueContainers.add(returnEntryValueContainer);
                for (final ColumnConfiguration column : columns) {
                    if (!(column instanceof PropertyColumnConfiguration)) {
                        continue;
                    }
                    final PropertyColumnConfiguration col = (PropertyColumnConfiguration)column;

                    final PropertyDescriptor propertyDescriptor = col.getPropertyDescriptor();

                    if (isExpectedQtyPropertyDescriptor(propertyDescriptor)) {
                        final ObjectValueHolder objectValueHolder = returnEntryValueContainer.getValue(
                                propertyDescriptor, null);
                        objectValueHolder.setLocalValue(null);
                    }

                    final EditorRowConfiguration editorRowConfiguration = getPropertyEditorHelper()
                            .findConfigurationByPropertyDescriptor(editorMapping, propertyDescriptor);
                    if (editorRowConfiguration == null) {
                        continue;
                    }
                    final Listcell cell = new Listcell();
                    cell.setParent(row);
                    final Div editorDiv = new Div();
                    editorDiv.setParent(cell);
                    editorDiv.setSclass(EDITOR_WIDGET_EDITOR_CSSCLASS);
                    renderEditor(editorDiv, editorRowConfiguration, returnEntryObject, returnEntryValueContainer,
                            propertyDescriptor, widget);
                }
            }
            if (REFUND_VIEW.equalsIgnoreCase(getListConfigurationType())) {
                final List<PropertyDescriptor> propertyDescriptors = getRequestPropertyDescriptors();
                form = buildObjectValueContainer(null, propertyDescriptors, Collections.<String> emptySet());

                for (final PropertyDescriptor propertyDescriptor : propertyDescriptors) {
                    final Div editorDiv = new Div();
                    editorDiv.setSclass(CSS_EDITOR_WIDGET_EDITOR);

                    PojoPropertyRendererUtil.renderEditor(editorDiv, propertyDescriptor,
                            new BasicPropertyEditorRowConfiguration(true, true, null, propertyDescriptor), null,
                            form, widget, true);

                    listBox.getParent().getParent().appendChild(editorDiv);

                    if (SHIP_AMOUNT_FIELD.equals(propertyDescriptor.getQualifier())) {
                        final TargetReturnsControllerImpl controller = (TargetReturnsControllerImpl)
                                widget.getWidgetController();
                        final double maxShippingToRefund = controller.getMaximumShippingAmountToRefund();
                        final String formattedAmount = DECIMAL_FORMAT.format(maxShippingToRefund);

                        final Label label = new Label(LabelUtils.getLabel(widget, MAX_ALLOWED, formattedAmount));
                        label.setSclass(SHIPPING_AMOUNT_FIELD_CSS_CLASS);
                        editorDiv.getFirstChild().getLastChild().appendChild(label);
                    }
                }
            }
        }
        catch (final Exception e) {
            LOG.error(FAILED_TO_RENDER_RETURN_ENTRIES_LIST, e);
        }
    }

    /**
     * Checks if is expected qty property descriptor.
     * 
     * @param propertyDescriptor
     *            the property descriptor
     * @return true, if is expected qty property descriptor
     */
    protected boolean isExpectedQtyPropertyDescriptor(final PropertyDescriptor propertyDescriptor) {
        if (propertyDescriptor == null) {
            return false;
        }
        else {
            return propertyDescriptor.getQualifier().equals(EXPECTED_QTY_PROPERTY_QUALIFIER);
        }
    }

    /**
     * Fill fixed columns
     * 
     * @param returnableOrderEntries
     *            the returnableOrderEntries
     * @param item
     *            the item
     * @param row
     *            the row
     */
    protected void fillFixedColumns(final Map<TypedObject, ?> returnableOrderEntries, final TypedObject item,
            final Listitem row) {
        final Listcell entryNoCell = new Listcell();
        entryNoCell.setParent(row);
        final Div entryNoDiv = new Div();
        entryNoDiv.setParent(entryNoCell);
        entryNoDiv.setSclass(EDITOR_WIDGET_EDITOR_CSSCLASS);
        final Label entryNoLabel = new Label(String.valueOf(((AbstractOrderEntryModel)item.getObject())
                .getEntryNumber()));
        entryNoLabel.setParent(entryNoDiv);

        final Listcell productCell = new Listcell();
        productCell.setParent(row);
        final Div productDiv = new Div();
        productDiv.setParent(productCell);
        productDiv.setSclass(EDITOR_WIDGET_EDITOR_CSSCLASS);
        final List<ColumnConfiguration> columnsOrderEntry = getColumnConfigurations(
                getProductInfoConfigurationCode(), item.getType().getCode());
        getPropertyRendererHelper().buildPropertyValuesFromColumnConfigs(item, columnsOrderEntry, productDiv);

        final Listcell basePriceCell = new Listcell();
        basePriceCell.setParent(row);
        final Div basePriceDiv = new Div();
        basePriceDiv.setParent(basePriceCell);
        basePriceDiv.setSclass(EDITOR_WIDGET_EDITOR_CSSCLASS);
        final Label basePriceLabel = new Label(String.valueOf(((AbstractOrderEntryModel)item.getObject())
                .getBasePrice()));
        basePriceLabel.setParent(basePriceDiv);

        final Listcell maxQtyCell = new Listcell();
        maxQtyCell.setParent(row);
        final Div maxQtyDiv = new Div();
        maxQtyDiv.setParent(maxQtyCell);
        maxQtyDiv.setSclass(EDITOR_WIDGET_EDITOR_CSSCLASS);
        final Label maxQtyLabel = new Label(String.valueOf(returnableOrderEntries.get(item)));
        maxQtyLabel.setParent(maxQtyDiv);


        final Listcell discountCell = new Listcell();
        discountCell.setParent(row);
        final Div discountDiv = new Div();
        discountDiv.setParent(discountCell);
        discountDiv.setSclass(EDITOR_WIDGET_EDITOR_CSSCLASS);
        final Label discountLabel = new Label(String.valueOf(DiscountUtil
                .getDiscount((AbstractOrderEntryModel)item.getObject())));
        discountLabel.setParent(discountDiv);
    }

    /**
     * 
     * @param widget
     *            the widget
     * @param headRow
     *            the headRow
     */
    protected void prepareFixedColums(final InputWidget<DefaultListWidgetModel<TypedObject>, ReturnsController> widget,
            final Listhead headRow) {
        final Listheader colEntryNoHeader = new Listheader(
                LabelUtils.getLabel(widget, ENTRY_NUMBER_FIELD, new Object[0]));
        colEntryNoHeader.setWidth(WIDTH_ENTRY_NUMBER);
        colEntryNoHeader.setParent(headRow);

        final Listheader colProductHeader = new Listheader(
                LabelUtils.getLabel(widget, PRODUCT_INFO_FILED, new Object[0]));
        colProductHeader.setWidth(WIDTH_PRODUCT_INFO);
        colProductHeader.setParent(headRow);

        final Listheader colBasePriceHeader = new Listheader(
                LabelUtils.getLabel(widget, BASE_PRICE_FIELD, new Object[0]));
        colBasePriceHeader.setWidth(WIDTH_BASE_PRICE);
        colBasePriceHeader.setParent(headRow);

        final Listheader colMaxQtyHeader = new Listheader(LabelUtils.getLabel(widget, MAX_QTY_FIELD, new Object[0]));
        colMaxQtyHeader.setWidth(WIDTH_MAX_QTY);
        colMaxQtyHeader.setParent(headRow);

        final Listheader colDiscountHeader = new Listheader(LabelUtils.getLabel(widget, DISCOUNT_FIELD,
                new Object[0]));
        colDiscountHeader.setWidth(WIDTH_DISCOUNT);
        colDiscountHeader.setParent(headRow);

        final Listheader colQuantityHeader = new Listheader(LabelUtils.getLabel(widget, QUANTITY_COLUMN_KEY,
                new Object[0]));
        colQuantityHeader.setWidth(WIDTH_QUANTITY);
        colQuantityHeader.setParent(headRow);

        final Listheader colReasonHeader = new Listheader(LabelUtils.getLabel(widget, REASON_CODE_COLUMN_KEY,
                new Object[0]));
        colReasonHeader.setWidth(WIDTH_REASON);
        colReasonHeader.setParent(headRow);

        final Listheader colActionHeader = new Listheader(LabelUtils.getLabel(widget, ACTION_COLUMN_KEY, new Object[0]));
        colActionHeader.setWidth(WIDTH_ACTION);
        colActionHeader.setParent(headRow);

        final Listheader colNotesHeader = new Listheader(LabelUtils.getLabel(widget, NOTES_COLUMN_KEY, new Object[0]));
        colNotesHeader.setWidth(WIDTH_NOTES);
        colNotesHeader.setParent(headRow);
    }

    @Override
    protected void handleReturnRequestCreateEvent(
            final InputWidget<DefaultListWidgetModel<TypedObject>, ReturnsController> widget, final Event event,
            final List<ObjectValueContainer> returnValueContainers) throws Exception {
        final TargetReturnsControllerImpl controller = (TargetReturnsControllerImpl)widget.getWidgetController();
        controller.setAdditionalRequestParams(form);

        super.handleReturnRequestCreateEvent(widget, event, returnValueContainers);
    }

    /**
     * @param addPaymentWindowController
     *            the addPaymentWindowController to set
     */
    @Required
    public void setAddPaymentWindowController(final AddPaymentWindowController addPaymentWindowController) {
        this.addPaymentWindowController = addPaymentWindowController;
    }

    /**
     * @param tgtCsCardPaymentFacade
     *            the tgtCsCardPaymentFacade to set
     */
    @Required
    public void setTgtCsCardPaymentFacade(final TgtCsCardPaymentFacade tgtCsCardPaymentFacade) {
        this.tgtCsCardPaymentFacade = tgtCsCardPaymentFacade;
    }

    /**
     * @param targetFeatureSwitchFacade
     *            the targetFeatureSwitchFacade to set
     */
    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }

}

package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cockpit.widgets.models.WidgetModel;
import de.hybris.platform.commons.model.DocumentModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.utils.TypeUtils;
import de.hybris.platform.cscockpit.widgets.controllers.CallContextController;
import de.hybris.platform.cscockpit.widgets.renderers.impl.AbstractCsWidgetRenderer;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import au.com.target.tgtcore.taxinvoice.TaxInvoiceException;
import au.com.target.tgtcore.taxinvoice.TaxInvoiceService;
import au.com.target.tgtcs.dto.DocumentMediaDTO;
import au.com.target.tgtcs.widgets.listener.ResendTaxInvoiceEventListener;


public class TgtTaxInvoiceWidgetRenderer extends
        AbstractCsWidgetRenderer<Widget<WidgetModel, CallContextController>> {

    private static final Logger LOG = Logger.getLogger(TgtTaxInvoiceWidgetRenderer.class);

    private static final String EMAIL_INPUT_ID = "emailId";
    private static final String PADDING_3PX = "padding : 3px;";
    private static final String DAFAUTL_WIDTH = "200px";
    private static final String RESEND_INVOICE_BUTTON_POSTFIX = "button.resend";
    private static final String PRINT_INVOICE_BUTTON_POSTFIX = "button.printInvoice";
    private static final String PRINT_WINDOW_TEMPLATE_PATH = "tgtcs/printWindow.zul";

    private TaxInvoiceService taxInvoiceService;
    private int taxInvoiceMaxDisplayTime;

    /**
     * set invoice max display time
     * 
     * @param taxInvoiceMaxDisplayTime
     *            an int
     */
    @Required
    public void setTaxInvoiceMaxDisplayTime(final int taxInvoiceMaxDisplayTime) {
        this.taxInvoiceMaxDisplayTime = taxInvoiceMaxDisplayTime;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected HtmlBasedComponent createContentInternal(
            final Widget<WidgetModel, CallContextController> widget,
            final HtmlBasedComponent parent) {
        final Hbox hbox = new Hbox();

        final Textbox emailAddress = new Textbox();
        emailAddress.setId(EMAIL_INPUT_ID);
        emailAddress.setValue(getDefaultEmail(widget));
        emailAddress.setWidth(DAFAUTL_WIDTH);
        emailAddress.setStyle(PADDING_3PX);
        hbox.appendChild(emailAddress);

        final Button resendTaxInvoice = new Button(LabelUtils.getLabel(widget, RESEND_INVOICE_BUTTON_POSTFIX));
        resendTaxInvoice.addEventListener(Events.ON_CLICK, new ResendTaxInvoiceEventListener(getSelectedOrder(widget),
                emailAddress));
        final boolean invoiceAllowed = (getTaxInvoiceService().isTaxInvoiceResendAllowed(getSelectedOrder(widget)));
        resendTaxInvoice.setDisabled(!invoiceAllowed);
        hbox.appendChild(resendTaxInvoice);

        final Button printInvoiceBtn = new Button(LabelUtils.getLabel(widget, PRINT_INVOICE_BUTTON_POSTFIX));
        printInvoiceBtn.setDisabled(!invoiceAllowed);
        hbox.appendChild(printInvoiceBtn);

        if (invoiceAllowed) {
            final Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, -taxInvoiceMaxDisplayTime);
            final Map<String, DocumentMediaDTO> map = new HashMap<String, DocumentMediaDTO>();

            try {
                final DocumentModel invoiceDocument = getTaxInvoice(widget);
                if (invoiceDocument != null && invoiceDocument.getCreationtime().compareTo(calendar.getTime()) >= 0) {
                    final DocumentMediaDTO dto = new DocumentMediaDTO();
                    dto.setMime(invoiceDocument.getMime());
                    dto.setUrl(invoiceDocument.getURL2());
                    map.put("model", dto);
                }
                else {
                    printInvoiceBtn.setDisabled(true);
                }
            }
            catch (final Exception ex) {
                printInvoiceBtn.setDisabled(true);
            }
            printInvoiceBtn.addEventListener(Events.ON_CLICK, new EventListener() {

                @Override
                public void onEvent(final Event paramEvent) throws Exception {
                    final Window dialog = (Window)Executions.createComponents(PRINT_WINDOW_TEMPLATE_PATH,
                            null, map);
                    dialog.doModal();
                    paramEvent.stopPropagation();
                }
            });
        }

        return hbox;
    }


    /**
     * Gets the tax invoice if exists else generate tax invoice.
     * 
     * @param widget
     *            the widget
     * @return the tax invoice
     */
    private DocumentModel getTaxInvoice(final Widget<WidgetModel, CallContextController> widget) {
        final OrderModel orderModel = getSelectedOrder(widget);
        if (orderModel == null) {
            return null;
        }
        final DocumentModel documentModel = taxInvoiceService.getTaxInvoiceForOrder(orderModel);
        if (documentModel != null) {
            return documentModel;
        }
        try {
            final MediaModel mediaModel = taxInvoiceService.generateTaxInvoiceForOrder(orderModel);
            if (mediaModel != null) {
                return (DocumentModel)mediaModel;
            }
        }
        catch (final TaxInvoiceException e) {
            LOG.error("Error while generating tax invoice for order:" + orderModel.getCode());
        }
        return null;
    }

    /**
     * 
     * @return selected order
     */
    private OrderModel getSelectedOrder(final Widget<WidgetModel, CallContextController> widget) {
        return TypeUtils.unwrapItem(widget.getWidgetController().getCurrentOrder(), OrderModel.class);
    }

    /**
     * 
     * @return customer's email
     */
    private String getDefaultEmail(final Widget<WidgetModel, CallContextController> widget) {
        final OrderModel order = getSelectedOrder(widget);
        if (order == null) {
            return StringUtils.EMPTY;
        }
        else {
            final UserModel user = order.getUser();
            if (user instanceof CustomerModel) {
                return ((CustomerModel)user).getContactEmail();
            }
            else {
                return StringUtils.EMPTY;
            }
        }
    }

    /**
     * @return the taxInvoiceService
     */
    public TaxInvoiceService getTaxInvoiceService() {
        return taxInvoiceService;
    }

    /**
     * @param taxInvoiceService
     *            the taxInvoiceService to set
     */
    @Required
    public void setTaxInvoiceService(final TaxInvoiceService taxInvoiceService) {
        this.taxInvoiceService = taxInvoiceService;
    }
}
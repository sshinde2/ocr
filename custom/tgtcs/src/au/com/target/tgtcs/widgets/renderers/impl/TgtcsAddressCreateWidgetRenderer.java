package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.platform.cockpit.model.meta.ObjectType;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.model.search.impl.ResultObjectWrapper;
import de.hybris.platform.cockpit.services.config.EditorConfiguration;
import de.hybris.platform.cockpit.services.config.EditorRowConfiguration;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer.ObjectValueHolder;
import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.cockpit.widgets.InputWidget;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.controllers.CustomerController;
import de.hybris.platform.cscockpit.widgets.models.impl.DefaultMasterDetailListWidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.impl.AddressCreateWidgetRenderer;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtcs.util.TargetCsCockpitUtil;
import au.com.target.tgtcs.widgets.controllers.AddressCustomerController;
import au.com.target.tgtverifyaddr.data.AddressData;
import au.com.target.tgtverifyaddr.data.CountryEnum;


public class TgtcsAddressCreateWidgetRenderer extends AddressCreateWidgetRenderer {
    private static final Logger LOG = Logger.getLogger(TgtcsAddressCreateWidgetRenderer.class);
    private static final String CONFIRM_ADDRESS_WIDGET_CONFIG = "csConfirmAddressWidgetConfig";
    private static final String CSS_CONFIRM_ADDRESS_POPUP = "csConfirmAddressWidget";
    private static final String CONFIRM_ADDRESS_POPUP_TITLE_CODE = "confirmPopup";

    @Override
    protected void handleCreateClickEvent(
            final InputWidget<DefaultMasterDetailListWidgetModel<TypedObject>, CustomerController> widget,
            final Event event,
            final ObjectValueContainer container, final String configurationTypeCode) throws Exception {
        // check mandatory fields
        final StringBuilder msgBuilder = new StringBuilder();
        final boolean checkResult = TargetCsCockpitUtil.checkMandatoryFields(container, configurationTypeCode,
                msgBuilder);
        if (!checkResult) {
            Messagebox.show(msgBuilder.toString(), LabelUtils.getLabel(widget, "unableToCreateAddress"), Messagebox.OK,
                    Messagebox.ERROR);
            return;
        }
        // validate international address
        if (isInternationalAddress(container)) {
            Messagebox.show(LabelUtils.getLabel(widget, "noInternationalAddressAllowed"),
                    LabelUtils.getLabel(widget, "unableToCreateAddress"),
                    Messagebox.OK,
                    Messagebox.ERROR);
            return;
        }
        // validate address       
        final List<AddressData> returnAddresses = ((AddressCustomerController)widget.getWidgetController())
                .checkValidAddress(container);

        // display address select popup
        final Window parent = getPopupWidgetHelper().getCurrentPopup();
        final ConfirmNewAddressWidgetRenderer beanRenderer = (ConfirmNewAddressWidgetRenderer)Registry
                .getApplicationContext().getBean(
                        "csConfirmAddressWidgetRenderer");
        beanRenderer.setPotentialAddresses(returnAddresses);
        beanRenderer.setInputtedAddressContainer(container);
        beanRenderer.setConfigurationTypeCode(configurationTypeCode);
        getPopupWidgetHelper().createPopupWidget(parent.getParent(), CONFIRM_ADDRESS_WIDGET_CONFIG,
                "csConfirmAddressWidget-popup", CSS_CONFIRM_ADDRESS_POPUP,
                LabelUtils.getLabel(widget, CONFIRM_ADDRESS_POPUP_TITLE_CODE));
        Events.postEvent(new Event(Events.ON_CLOSE, parent));
    }


    /**
     * a private method to check whether it's an international address or not
     * 
     * @param addressContainer
     *            address object value container
     * @return a boolean
     */
    private boolean isInternationalAddress(final ObjectValueContainer addressContainer) {
        final Set<ObjectValueHolder> allValues = addressContainer.getAllValues();
        for (final ObjectValueHolder ovh : allValues) {
            final String qualifier = ovh.getPropertyDescriptor().getQualifier();
            if (qualifier.equals(TgtcsConstants.QUALIFIER_ADDRESS_COUNTRY)) {
                CountryModel currentValue = null;
                if (ovh.getCurrentValue() instanceof CountryModel) {
                    currentValue = (CountryModel)ovh.getCurrentValue();
                }
                else if (ovh.getCurrentValue() instanceof ResultObjectWrapper) {
                    final ResultObjectWrapper wrapper = (ResultObjectWrapper)ovh.getCurrentValue();
                    currentValue = (CountryModel)wrapper.getObject();
                }
                else if (ovh.getCurrentValue() instanceof TypedObject) {
                    final TypedObject wrapper = (TypedObject)ovh.getCurrentValue();
                    currentValue = (CountryModel)wrapper.getObject();
                }

                return !(currentValue == null
                || currentValue.getName().equalsIgnoreCase(CountryEnum.AUSTRALIA.name()));

            }
            else {
                continue;
            }
        }
        return false;

    }


    @Override
    protected ObjectValueContainer loadAndCreateEditors(
            final InputWidget<DefaultMasterDetailListWidgetModel<TypedObject>, CustomerController> widget,
            final HtmlBasedComponent content,
            final String configurationTypeCode, final String configurationCode,
            final Set<String> autoFilledPropertyQualifiers)
            throws ValueHandlerException
    {
        final ObjectType type = getCockpitTypeService().getObjectType(configurationTypeCode);
        final List<PropertyDescriptor> propertyDescriptors = getPropertyEditorHelper()
                .getRelevantPropertyDescriptors(configurationTypeCode, configurationCode);

        final ObjectValueContainer currentObjectValues = buildObjectValueContainer(type, propertyDescriptors,
                getPropertyEditorHelper()
                        .getRelevantLanguages());

        final EditorConfiguration customerConfiguration = getPropertyEditorHelper().getEditorConfiguration(
                configurationTypeCode,
                configurationCode);

        final List editorMapping = getPropertyEditorHelper().getAllEditorRowConfigurations(
                customerConfiguration);
        if ((editorMapping == null) || (editorMapping.isEmpty()))
        {
            LOG.warn("No editor widget configuration entries found.");
        }
        else
        {

            for (final Iterator localIterator1 = editorMapping.iterator(); localIterator1.hasNext();) {
                final EditorRowConfiguration editor = (EditorRowConfiguration)localIterator1.next();

                final PropertyDescriptor propertyDescriptor = editor.getPropertyDescriptor();

                final boolean forceNotMandatory = (autoFilledPropertyQualifiers != null) &&
                        (autoFilledPropertyQualifiers.contains(propertyDescriptor.getQualifier()));

                final Div editorDiv = new Div();
                editorDiv.setSclass("editorWidgetEditor");

                if (!propertyDescriptor.getQualifier().equals(TgtcsConstants.QUALIFIER_ADDRESS_COUNTRY))
                {
                    if (propertyDescriptor.isLocalized())
                    {
                        for (final String langIso : getPropertyEditorHelper().getRelevantLanguages())
                        {
                            getPropertyEditorHelper().renderEditor(editorDiv, editor, currentObjectValues,
                                    propertyDescriptor, widget,
                                    langIso, forceNotMandatory);
                        }

                    }
                    else {
                        getPropertyEditorHelper().renderEditor(editorDiv, editor, currentObjectValues,
                                propertyDescriptor,
                                widget, null,
                                forceNotMandatory);
                    }
                }
                else {
                    final Hbox hbox = new Hbox();
                    hbox.setParent(editorDiv);
                    hbox.setWidth("96%");
                    hbox.setWidths("9em, none");
                    final Label propLabel = new Label(propertyDescriptor.getName());
                    propLabel.setParent(hbox);
                    propLabel.setSclass("editorWidgetEditorLabel");

                    final Label countryLabel = new Label(CountryEnum.AUSTRALIA.name());
                    countryLabel.setSclass("editorWidgetEditorLabel");
                    hbox.appendChild(countryLabel);

                }

                if (!(editorDiv.getChildren().isEmpty()))
                {
                    content.appendChild(editorDiv);
                }
            }
            return currentObjectValues;
        }
        return null;
    }
}

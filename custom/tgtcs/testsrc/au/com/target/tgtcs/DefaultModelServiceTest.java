/**
 * 
 */
package au.com.target.tgtcs;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.internal.model.ModelContext;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import de.hybris.platform.servicelayer.session.SessionService;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.transaction.support.TransactionTemplate;

import au.com.target.tgtcore.model.TargetCustomerModel;


@UnitTest
public class DefaultModelServiceTest
{
    private static final String TEST_USER_UID = "testUid";
    private static final String TEST_USER_NAME = "testName";
    private static final String TEST_FLYBUYS_CODE = "123456789";

    private DefaultModelService modelService;

    @Mock
    private SessionService sessionService;

    @Resource
    private SessionService sessionServiceResource;

    @Mock
    private TransactionTemplate transactionTemplate;
    @Mock
    private ModelContext modelContext;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFlybuysCodeSaving() throws Exception {

        final TargetCustomerModel customer = Mockito.mock(TargetCustomerModel.class);
        BDDMockito.given(customer.getUid()).willReturn(TEST_USER_UID);
        BDDMockito.given(customer.getName()).willReturn(TEST_USER_NAME);
        BDDMockito.given(customer.getFlyBuysCode()).willReturn(TEST_FLYBUYS_CODE);

        BDDMockito.given(sessionService.getAttribute(DefaultModelService.ENABLE_TRANSACTIONAL_SAVES)).willReturn(
                Boolean.TRUE);

        modelService = new DefaultModelService() {
            @Override
            public SessionService lookupSessionService() {
                return sessionService;
            }

        };
        modelService.setTransactionTemplate(transactionTemplate);
        modelService.save(customer);
    }
}

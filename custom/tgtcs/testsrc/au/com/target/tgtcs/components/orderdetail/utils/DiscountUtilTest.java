/**
 * 
 */
package au.com.target.tgtcs.components.orderdetail.utils;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.util.DiscountValue;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;


/**
 * @author Nandini
 * 
 */
@UnitTest
@RunWith(PowerMockRunner.class)
public class DiscountUtilTest {

    @Mock
    private AbstractOrderEntryModel abstractOrder;

    @Mock
    private DiscountValue disValue1;
    @Mock
    private DiscountValue disValue2;

    @SuppressWarnings("boxing")
    @Test
    public void testGetDiscount() {
        final List<DiscountValue> discountValues = new ArrayList<>();
        given(disValue1.getAppliedValue()).willReturn(12.0d);
        given(disValue2.getAppliedValue()).willReturn(12.0d);
        discountValues.add(disValue1);
        discountValues.add(disValue2);
        given(abstractOrder.getDiscountValues()).willReturn(discountValues);

        Assert.assertEquals(24.0d, DiscountUtil.getDiscount(abstractOrder));
    }
}

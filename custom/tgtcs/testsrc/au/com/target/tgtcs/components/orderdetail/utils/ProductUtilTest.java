/**
 * 
 */
package au.com.target.tgtcs.components.orderdetail.utils;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mockito.Mockito;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * @author Nandini
 * 
 */
@UnitTest
public class ProductUtilTest {
    private static final String EXECTED_PRODUCT_SIZE = "Exected product size";
    private static final String EXECTED_PRODUCT_COLOUR = "Exected product colour";
    private static final String SIZE = "size";
    private static final String COLOUR = "colour";
    private static final String EXPECTING_NO_RESULT = "Expecting no result";

    @Test
    public void testGetProductColorEmpty() {
        final String result = ProductUtil.getProductColour(null);
        assertEquals(EXPECTING_NO_RESULT, StringUtils.EMPTY, result);
    }

    @Test
    public void testGetProductColourForColourVariant() {
        final TargetColourVariantProductModel variant = mock(TargetColourVariantProductModel.class);

        given(variant.getColourName()).willReturn(COLOUR);

        assertEquals(EXECTED_PRODUCT_COLOUR, COLOUR, ProductUtil.getProductColour(variant));
        Mockito.verify(variant).getColourName();
        Mockito.verifyNoMoreInteractions(variant);
    }

    @Test
    public void testGetProductColourForColourVariantWillreturnNull() {
        final TargetColourVariantProductModel variant = mock(TargetColourVariantProductModel.class);

        given(variant.getColourName()).willReturn(null);

        assertEquals(EXECTED_PRODUCT_COLOUR, StringUtils.EMPTY, ProductUtil.getProductColour(variant));
        Mockito.verify(variant).getColourName();
        Mockito.verifyNoMoreInteractions(variant);
    }

    @Test
    public void testGetProductColourForSizeVariant() {
        final TargetSizeVariantProductModel variant = mock(TargetSizeVariantProductModel.class);
        final TargetColourVariantProductModel colourvariant = mock(TargetColourVariantProductModel.class);

        given(variant.getBaseProduct()).willReturn(colourvariant);
        given(colourvariant.getColourName()).willReturn(COLOUR);

        assertEquals(EXECTED_PRODUCT_COLOUR, COLOUR, ProductUtil.getProductColour(variant));
        Mockito.verify(colourvariant).getColourName();
        Mockito.verifyNoMoreInteractions(colourvariant);
        Mockito.verify(variant).getBaseProduct();
        Mockito.verifyNoMoreInteractions(variant);
    }

    @Test
    public void testProductColourForProductOfTypeProductModel() {
        final String result = ProductUtil.getProductColour(new ProductModel());
        assertEquals(EXECTED_PRODUCT_COLOUR, StringUtils.EMPTY, result);
    }

    @Test
    public void testGetProductSizeForNull() {

        final String result = ProductUtil.getProductSize(null);
        assertEquals(EXPECTING_NO_RESULT, StringUtils.EMPTY, result);
    }

    @Test
    public void testGetProductSizeVariant() {
        final TargetSizeVariantProductModel variant = mock(TargetSizeVariantProductModel.class);

        given(variant.getSize()).willReturn(SIZE);

        assertEquals(EXECTED_PRODUCT_SIZE, SIZE, ProductUtil.getProductSize(variant));
    }

    @Test
    public void testGetProductSizeVariantWillReturnNull() {
        final TargetSizeVariantProductModel variant = mock(TargetSizeVariantProductModel.class);

        given(variant.getSize()).willReturn(null);

        assertEquals(EXECTED_PRODUCT_SIZE, StringUtils.EMPTY, ProductUtil.getProductSize(variant));
    }

    @Test
    public void testGetProductSizeForColourVariant() {
        final TargetColourVariantProductModel variant = mock(TargetColourVariantProductModel.class);

        assertEquals(EXECTED_PRODUCT_SIZE, StringUtils.EMPTY, ProductUtil.getProductSize(variant));
        Mockito.verifyNoMoreInteractions(variant);
    }

}

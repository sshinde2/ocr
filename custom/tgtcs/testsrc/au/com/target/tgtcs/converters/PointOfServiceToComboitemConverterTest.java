package au.com.target.tgtcs.converters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import org.junit.Test;
import org.zkoss.zul.Comboitem;


@UnitTest
public class PointOfServiceToComboitemConverterTest {

    private final PointOfServiceToComboitemConverter testInstance = new PointOfServiceToComboitemConverter();

    @Test
    public void testPointOfServiceToComboitemConverterPopulate() {
        final PointOfServiceModel pointOfService = mock(PointOfServiceModel.class);
        final AddressModel mockAddress = mock(AddressModel.class);
        when(mockAddress.getDistrict()).thenReturn("d");
        when(mockAddress.getTown()).thenReturn("t");
        when(mockAddress.getStreetname()).thenReturn("str");
        when(pointOfService.getAddress()).thenReturn(mockAddress);

        final AddressModel posAddress = pointOfService.getAddress();
        final String addresLabel = String.format("%s, %s, %s", posAddress.getDistrict(), posAddress.getTown(),
                posAddress.getStreetname());
        final Comboitem item = new Comboitem();
        testInstance.populate(pointOfService, item);
        assertSame(pointOfService, item.getValue());
        assertEquals(addresLabel, item.getLabel());
    }

}

package au.com.target.tgtcs.converters;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.TitleModel;

import org.junit.Test;
import org.zkoss.zul.Comboitem;


@UnitTest
public class TitleToComboitemConverterTest {

    private final TitleToComboitemConverter testInstance = new TitleToComboitemConverter();

    @Test
    public void testTitleToComboitemConverter()
    {
        final TitleModel mockTitle = mock(TitleModel.class);
        final String title = "testTitleName";

        when(mockTitle.getName()).thenReturn(title);
        testInstance.setTargetClass(Comboitem.class);

        final Comboitem converted = testInstance.convert(mockTitle);
        assertEquals("Unexpected comboitem value", mockTitle, converted.getValue());
        assertEquals(String.format("Expected comboitem label : [%s], actual : [%s]", title, converted.getLabel()),
                title, converted.getLabel());
    }
}

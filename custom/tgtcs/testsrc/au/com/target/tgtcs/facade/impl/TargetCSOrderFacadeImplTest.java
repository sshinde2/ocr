package au.com.target.tgtcs.facade.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;

import com.google.common.collect.ImmutableList;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCSOrderFacadeImplTest {

    @Mock
    private TargetDeliveryModeHelper deliveryModeHelper;
    @Mock
    private ModelService modelService;
    @Mock
    private AbstractOrderModel order;
    @Mock
    private DeliveryModeModel deliveryMode;
    @Mock
    private AddressModel deliveryAddress;
    @Mock
    private AbstractOrderEntryModel orderEntry;
    @Mock
    private ProductModel product;
    @InjectMocks
    private final TargetCSOrderFacadeImpl testInstance = new TargetCSOrderFacadeImpl();

    @Before
    public void setUp() throws Exception {
        when(orderEntry.getProduct()).thenReturn(product);
    }



    @Test
    public void testIsOrderAvailableForDeliveryMode() {
        final List list = ImmutableList.of(orderEntry);
        when(order.getEntries()).thenReturn(list);
        when(Boolean.valueOf(deliveryModeHelper.isDeliveryModeStoreDelivery(deliveryMode))).thenReturn(
                Boolean.TRUE);
        when(Boolean.valueOf(deliveryModeHelper.isProductAvailableForDeliveryMode(product, deliveryMode))).thenReturn(
                Boolean.TRUE);

        final boolean result = testInstance.isOrderAvailableForDeliveryMode(order, deliveryMode);
        Assert.assertTrue(result);
    }

    @Test
    public void testIsOrderEntryAvailableForDeliveryModeStoreDelivery() {
        when(Boolean.valueOf(deliveryModeHelper.isDeliveryModeStoreDelivery(deliveryMode))).thenReturn(
                Boolean.TRUE);
        when(Boolean.valueOf(deliveryModeHelper.isProductAvailableForDeliveryMode(product, deliveryMode))).thenReturn(
                Boolean.TRUE);

        testInstance.isOrderEntryAvailableForDeliveryMode(orderEntry, deliveryMode);
        verify(deliveryModeHelper).isProductAvailableForDeliveryMode(product, deliveryMode);
    }

    @Test
    public void testIsOrderEntryAvailableForDeliveryModeNotStoreDelivery() {
        when(Boolean.valueOf(deliveryModeHelper.isDeliveryModeStoreDelivery(deliveryMode))).thenReturn(
                Boolean.TRUE);
        testInstance.isOrderEntryAvailableForDeliveryMode(orderEntry, deliveryMode);
        verify(deliveryModeHelper).isProductAvailableForDeliveryMode(product, deliveryMode);
    }

}

package au.com.target.tgtcs.facade.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.wizards.Message;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.cscockpit.services.payment.CsCardPaymentService;
import de.hybris.platform.cscockpit.services.payment.strategy.impl.DefaultCsOrderUnauthorizedTotalStrategy;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.payment.dto.BillingInfo;
import de.hybris.platform.payment.dto.CardInfo;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.zkoss.spring.SpringUtil;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Form;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.order.TargetCommerceCheckoutService;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.refund.impl.TargetRefundServiceImpl;
import au.com.target.tgtcs.components.orderdetail.dto.TargetHostedPaymentFormData;
import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtpayment.dto.HostedSessionTokenRequest;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.service.TargetPaymentService;


/**
 * @author umesh
 * 
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Registry.class, SpringUtil.class, Labels.class })
@PowerMockIgnore("javax.management.*")
public class TgtCsCardPaymentFacadeImplTest {
    private static final String NEW_CARD_ADDED_MESSAGE = "tgtcscockpit.payment.newCard.added";
    private static final String FAILED_TO_GENERATE_TOKEN = "tgtcscockpit.payment.error.failedToGenerateToken";
    private static final String NEW_PAYMENT_ADDED_MESSAGE = "tgtcscockpit.newPayment.added";
    private static final String SAVED_TOKEN = "1sc7s6hs";

    @Mock
    private TargetPaymentService targetPaymentService;
    @Mock
    private PaymentModeService paymentModeService;
    @Mock
    private TargetCommerceCheckoutService targetCommerceCheckoutService;
    @Mock
    private TargetOrderService targetOrderService;
    @Mock
    private ModelService modelService;
    @Mock
    private CsCardPaymentService csCardPaymentService;
    @Mock
    private OrderModel orderModel;
    @Mock
    private CommonI18NService commonI18NService;
    @Mock
    private Configuration configuration;
    @Mock
    private TargetRefundServiceImpl refundService;
    @Mock
    private ReturnRequestModel request;
    @Mock
    private DefaultCsOrderUnauthorizedTotalStrategy defaultCsOrderUnauthorizedTotalStrategy;
    @Mock
    private TargetHostedPaymentFormData data;
    @Mock
    private Execution execution;
    @Mock
    private CreditCardPaymentInfoModel cardPaymentInfo;
    @InjectMocks
    private final TgtCsCardPaymentFacadeImpl facade = new TgtCsCardPaymentFacadeImpl();

    private final TgtCsCardPaymentFacadeImpl spyCsCardPaymentFacadeImpl = Mockito.spy(new TgtCsCardPaymentFacadeImpl());

    @Before
    public void setup() {
        final String acceptableCards = "AMEX|MASTER|DINERS|VISA";
        try {
            PowerMockito.mockStatic(Registry.class, Mockito.RETURNS_MOCKS);
            PowerMockito.mockStatic(SpringUtil.class, Mockito.RETURNS_MOCKS);
            PowerMockito.mockStatic(Labels.class, Mockito.RETURNS_MOCKS);
            MockitoAnnotations.initMocks(this);

            final UserModel userModel = mock(UserModel.class);
            final TargetAddressModel targetAddressModel = mock(TargetAddressModel.class);
            final MessageSource messageSource = mock(MessageSource.class);
            final I18NService i18Service = mock(I18NService.class);
            targetOrderService = mock(TargetOrderService.class);
            final CurrencyModel currencyModel = mock(CurrencyModel.class);
            final int round = 2;

            spyCsCardPaymentFacadeImpl.setCommonI18NService(commonI18NService);
            spyCsCardPaymentFacadeImpl.setTargetOrderService(targetOrderService);
            spyCsCardPaymentFacadeImpl
                    .setDefaultCsOrderUnauthorizedTotalStrategy(defaultCsOrderUnauthorizedTotalStrategy);

            given(Labels.getLabel(NEW_CARD_ADDED_MESSAGE)).willReturn("success");
            given(Labels.getLabel(NEW_PAYMENT_ADDED_MESSAGE)).willReturn("successfulPayment");
            given(Labels.getLabel(FAILED_TO_GENERATE_TOKEN)).willReturn("fail");
            given(orderModel.getCurrency()).willReturn(currencyModel);
            given(orderModel.getUser()).willReturn(userModel);
            given(userModel.getUid()).willReturn("test");
            given(currencyModel.getDigits()).willReturn(new Integer(round));
            given(targetOrderService.findOrderModelForOrderId(Mockito.anyString())).willReturn(orderModel);
            given(Double.valueOf(commonI18NService.roundCurrency(Mockito.anyDouble(), Mockito.anyInt()))).willReturn(
                    new Double("100"));
            given(modelService.create(CreditCardPaymentInfoModel.class)).willReturn(cardPaymentInfo);
            given(modelService.create(TargetAddressModel.class)).willReturn(targetAddressModel);
            given(SpringUtil.getBean("messageSource")).willReturn(messageSource);
            given(SpringUtil.getBean("i18nService")).willReturn(i18Service);
            given(configuration.getString("tgtcs.acceptable.creditcard.list")).willReturn(acceptableCards);
            given(cardPaymentInfo.getSubscriptionId()).willReturn(SAVED_TOKEN);
            facade.setCommonI18NService(commonI18NService);
            facade.setModelService(modelService);
            facade.setAcceptableCards(acceptableCards);
            facade.setTargetOrderService(targetOrderService);
        }
        catch (final Exception e) {
            fail("Failure setting up TgtCsCardPaymentFacadeImplTest");
        }
    }

    @Test
    public void testCreateBillingInfoWithNull() {
        final BillingInfo billingInfo = facade.createBillingInfo(execution);
        Assert.assertEquals(null, billingInfo.getFirstName());
        Assert.assertEquals(null, billingInfo.getPhoneNumber());
    }

    @Test
    public void testCreateBillingInfoWithValues() {
        final String testName = "testName";
        final String testPhone = "987456321";

        given(execution.getParameter(TgtcsConstants.FIRST_NAME)).willReturn(testName);
        given(execution.getParameter(TgtcsConstants.PHONE_NUMBER)).willReturn(testPhone);

        final BillingInfo billingInfo = facade.createBillingInfo(execution);
        Assert.assertEquals(testName, billingInfo.getFirstName());
        Assert.assertEquals(testPhone, billingInfo.getPhoneNumber());
    }

    @Test
    public void testCreateTargetHostedPaymentFormDataWithEmptyValues() {
        data = facade.createTargetHostedPaymentFormData(execution);
        Assert.assertEquals(null, data.getCardHolderFullName());
        Assert.assertEquals(null, data.getPostalCode());
    }

    @Test
    public void testCreateTargetHostedPaymentFormDataWithValues() {
        final String testNameOnCard = "abc";
        final String testPostal = "1233";

        given(execution.getParameter(TgtcsConstants.NAME_ON_CARD)).willReturn(testNameOnCard);
        given(execution.getParameter(TgtcsConstants.POSTAL_CODE)).willReturn(testPostal);

        data = facade.createTargetHostedPaymentFormData(execution);
        Assert.assertEquals(testNameOnCard, data.getCardHolderFullName());
        Assert.assertEquals(testPostal, data.getPostalCode());
    }

    @Test
    public void testCreateCardInfoWithNoValues() {
        given(execution.getParameter(TgtcsConstants.GATEWAY_CARD_SCHEME)).willReturn("MASTERCARD");
        final CardInfo cardInfo = facade.createCardInfo(execution);
        Assert.assertEquals(null, cardInfo.getCardHolderFullName());
        Assert.assertEquals(null, cardInfo.getCardNumber());
        Assert.assertEquals(null, cardInfo.getCv2Number());
        Assert.assertEquals(null, cardInfo.getExpirationMonth());
        Assert.assertEquals(null, cardInfo.getExpirationYear());
    }

    @Test
    public void testCreateCardInfoWithValues() {
        final String cardHolderFullName = "TEST";
        final String cardNumber = "5123456789012346";
        final String expirationMonth = "05";
        final String expirationYear = "2017";
        final String cv2Number = "123";

        given(execution.getParameter(TgtcsConstants.NAME_ON_CARD)).willReturn(cardHolderFullName);
        given(execution.getParameter(TgtcsConstants.GATEWAY_CARD_NUMBER)).willReturn(cardNumber);
        given(execution.getParameter(TgtcsConstants.GATEWAY_CARD_EXPIRY_DATE_MONTH)).willReturn(expirationMonth);
        given(execution.getParameter(TgtcsConstants.GATEWAY_CARD_EXPIRY_DATE_YEAR)).willReturn(expirationYear);
        given(execution.getParameter(TgtcsConstants.GATEWAY_CARD_SECURITY_CODE)).willReturn(cv2Number);
        given(execution.getParameter(TgtcsConstants.GATEWAY_CARD_SCHEME)).willReturn("MASTERCARD");
        final CardInfo cardInfo = facade.createCardInfo(execution);
        Assert.assertEquals(cardHolderFullName, cardInfo.getCardHolderFullName());
        Assert.assertEquals(cardNumber, cardInfo.getCardNumber());
        Assert.assertEquals(cv2Number, cardInfo.getCv2Number());
        Assert.assertEquals(Integer.valueOf(expirationMonth), cardInfo.getExpirationMonth());
        Assert.assertEquals(Integer.valueOf(expirationYear), cardInfo.getExpirationYear());
    }

    @Test
    public void testCreateCreditCardPaymentInfoModelWithNoCardInfo() {
        given(execution.getParameter(TgtcsConstants.ORDER_ID)).willReturn("1565");
        cardPaymentInfo = facade.createCreditCardPaymentInfoModel(null,
                "1sc7s6hs", execution);
        Assert.assertNotNull(cardPaymentInfo);
        Assert.assertEquals(SAVED_TOKEN, cardPaymentInfo.getSubscriptionId());
        Assert.assertEquals(null, cardPaymentInfo.getNumber());
    }

    @Test
    public void testCreateCreditCardPaymentInfoModel() {
        final CardInfo cardInfo = createCardInfo();
        given(execution.getParameter(TgtcsConstants.ORDER_ID)).willReturn("1565");
        cardPaymentInfo = facade.createCreditCardPaymentInfoModel(cardInfo,
                "1sc7s6hs", execution);
        Assert.assertNotNull(cardInfo);
        Assert.assertEquals(SAVED_TOKEN, cardPaymentInfo.getSubscriptionId());
        Assert.assertNotNull(cardInfo.getCardNumber());
    }

    @Test
    public void testCleanupTNSFieldValue() {
        Assert.assertEquals("5123456789012346", facade.cleanupTNSFieldValue("5123456789012346"));
        Assert.assertEquals("5123456789012346", facade.cleanupTNSFieldValue("1~5123456789012346"));
        Assert.assertEquals("5123456789012346", facade.cleanupTNSFieldValue("2~5123456789012346"));
        Assert.assertEquals("5123456789012346", facade.cleanupTNSFieldValue("3~5123456789012346"));
    }

    @Test
    public void testGetCreditCardType() {
        Assert.assertEquals(CreditCardType.MASTER, facade.getCreditCardType("MASTERCARD"));
        Assert.assertEquals(CreditCardType.AMEX, facade.getCreditCardType("AMEX"));
        Assert.assertEquals(CreditCardType.DINERS, facade.getCreditCardType("DINERS_CLUB"));
        Assert.assertEquals(CreditCardType.VISA, facade.getCreditCardType("VISA"));
        Assert.assertEquals(null, facade.getCreditCardType("BANK"));
    }

    @Test
    public void testHandleRefundPaymentFormResponseExucutionNull() {
        final List<String> result = facade.handleRefundPaymentFormResponse(null);
        assertNotNull(result);
        assertEquals(0, result.size());
    }

    @Test
    public void testHandleRefundPaymentFormResponseValidationError() {
        final Desktop desktop = mock(Desktop.class);
        given(Labels.getLabel(TgtcsConstants.FIRST_NAME_REQUIRED)).willReturn("FirstName_Req");
        given(Labels.getLabel(TgtcsConstants.LAST_NAME_REQUIRED)).willReturn("LastName_Req");
        given(execution.getDesktop()).willReturn(desktop);
        given(execution.getDesktop()).willReturn(desktop);
        given(execution.getParameter("paymentType")).willReturn("1");
        given(execution.getParameter("amount")).willReturn("100");
        given(execution.getParameter("nameOnCard")).willReturn("Test test");
        given(execution.getParameter("cardType")).willReturn("MASTERCARD");
        given(execution.getParameter("gatewayCardNumber")).willReturn("1");
        given(execution.getParameter("street1")).willReturn("street1");
        given(execution.getParameter("street2")).willReturn("street2");
        given(execution.getParameter("city")).willReturn("city");
        given(execution.getParameter("postalCode")).willReturn("postalcode");
        given(execution.getParameter("country")).willReturn("country");
        given(execution.getParameter("state")).willReturn("state");
        given(execution.getParameter("phoneNumber")).willReturn("phone");
        given(execution.getParameter("gatewayFormResponse")).willReturn("1");
        given(targetPaymentService.tokenize(Mockito.anyString(), Mockito.any(PaymentModeModel.class))).willReturn(
                "token");
        final List<String> result = facade.handleRefundPaymentFormResponse(execution);
        assertNotNull(result);
        assertEquals(3, result.size());
        assertEquals("FirstName_Req", result.get(0));
        assertEquals("LastName_Req", result.get(1));
        assertEquals("Card Type Unsupported", result.get(2));
    }

    @Test
    public void testHandleRefundPaymentFormResponseFAILURE() {
        final Desktop desktop = mock(Desktop.class);
        given(execution.getDesktop()).willReturn(desktop);
        given(execution.getDesktop()).willReturn(desktop);
        given(execution.getParameter("paymentType")).willReturn("1");
        given(execution.getParameter("amount")).willReturn("100");
        given(execution.getParameter("nameOnCard")).willReturn("Test test");
        given(execution.getParameter("cardType")).willReturn("Visa");
        given(execution.getParameter("gatewayCardScheme")).willReturn("MASTERCARD");
        given(execution.getParameter("gatewayCardNumber")).willReturn("1");
        given(execution.getParameter("gatewayCardExpiryDateMonth")).willReturn("1");
        given(execution.getParameter("gatewayCardExpiryDateYear")).willReturn("1");
        given(execution.getParameter("gatewayCardSecurityCode")).willReturn("1");
        given(execution.getParameter("firstName")).willReturn("Firstname");
        given(execution.getParameter("lastName")).willReturn("Lastname");
        given(execution.getParameter("street1")).willReturn("street1");
        given(execution.getParameter("street2")).willReturn("street2");
        given(execution.getParameter("city")).willReturn("city");
        given(execution.getParameter("postalCode")).willReturn("postalcode");
        given(execution.getParameter("country")).willReturn("country");
        given(execution.getParameter("state")).willReturn("state");
        given(execution.getParameter("phoneNumber")).willReturn("phone");
        given(execution.getParameter("gatewayFormResponse")).willReturn("1");
        given(
                targetPaymentService.tokenize(Mockito.anyString(),
                        paymentModeService.getPaymentModeForCode(TgtcsConstants.CREDITCARD)))
                .willReturn(null);
        final List<String> result = facade.handleRefundPaymentFormResponse(execution);
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("fail", result.get(0));
    }

    @Test
    public void testHandleRefundPaymentFormResponseSUCCESS() {
        final Desktop desktop = mock(Desktop.class);
        given(execution.getDesktop()).willReturn(desktop);
        given(execution.getDesktop()).willReturn(desktop);
        given(execution.getParameter("paymentType")).willReturn("1");
        given(execution.getParameter("amount")).willReturn("100");
        given(execution.getParameter("nameOnCard")).willReturn("Test test");
        given(execution.getParameter("cardType")).willReturn("Visa");
        given(execution.getParameter("gatewayCardScheme")).willReturn("MASTERCARD");
        given(execution.getParameter("gatewayCardNumber")).willReturn("1");
        given(execution.getParameter("gatewayCardExpiryDateMonth")).willReturn("1");
        given(execution.getParameter("gatewayCardExpiryDateYear")).willReturn("1");
        given(execution.getParameter("gatewayCardSecurityCode")).willReturn("1");
        given(execution.getParameter("firstName")).willReturn("Firstname");
        given(execution.getParameter("lastName")).willReturn("Lastname");
        given(execution.getParameter("street1")).willReturn("street1");
        given(execution.getParameter("street2")).willReturn("street2");
        given(execution.getParameter("city")).willReturn("city");
        given(execution.getParameter("postalCode")).willReturn("postalcode");
        given(execution.getParameter("country")).willReturn("country");
        given(execution.getParameter("state")).willReturn("state");
        given(execution.getParameter("phoneNumber")).willReturn("phone");
        given(execution.getParameter("gatewayFormResponse")).willReturn("1");
        given(targetPaymentService.tokenize(Mockito.anyString(), Mockito.any(PaymentModeModel.class))).willReturn(
                "token");
        final List<String> result = facade.handleRefundPaymentFormResponse(execution);
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("success", result.get(0));
    }

    @Test
    public void testHandleCancelPaymentFormResponseExecutionNull() {
        final List<String> result = facade.handleCancelPaymentFormResponse(null);
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("failure", result.get(0));
    }

    @Test
    public void testValidate() {

        spyCsCardPaymentFacadeImpl.validate(data, 0, execution);
        assertEquals(0, data.getMessages().size());
    }


    @SuppressWarnings("boxing")
    @Test
    public void testValidateAmountDifferencePossitive() {

        given(spyCsCardPaymentFacadeImpl.getSuggestedAmountForPaymentOption(orderModel)).willReturn(3.0);
        given(commonI18NService.roundCurrency(1.00, orderModel.getCurrency().getDigits().intValue())).willReturn(3.0);
        given(spyCsCardPaymentFacadeImpl.getSuggestedAmountForPaymentOption(orderModel)).willReturn(1.0);
        final List<Message> messages = new ArrayList<>();
        spyCsCardPaymentFacadeImpl.validateAmount(1, messages, execution);
        Assert.assertTrue("There should be a Messege", messages.size() >= 0);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testValidateDifferenceNegetive() {
        given(spyCsCardPaymentFacadeImpl.getSuggestedAmountForPaymentOption(orderModel)).willReturn(1.0);
        given(commonI18NService.roundCurrency(1.00, orderModel.getCurrency().getDigits().intValue())).willReturn(1.0);
        given(spyCsCardPaymentFacadeImpl.getSuggestedAmountForPaymentOption(orderModel)).willReturn(3.0);
        final List<Message> messages = new ArrayList<>();
        spyCsCardPaymentFacadeImpl.validateAmount(1, messages, execution);
        Assert.assertTrue("There should not be any Error Messege", messages.size() <= 0);
    }

    @Test
    public void testValidateBillingAddress()
    {
        final List<Message> messages = new ArrayList<>();
        spyCsCardPaymentFacadeImpl.validateBillingAddress(data, messages);
        Assert.assertTrue(messages.size() > 0);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testHandleCancelPaymentFormResponseSuccess() {
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);

        given(execution.getParameter("paymentType")).willReturn("0");
        given(execution.getParameter("amount")).willReturn("100");
        given(execution.getParameter("nameOnCard")).willReturn("Test test");
        given(execution.getParameter("cardType")).willReturn("Visa");
        given(execution.getParameter("gatewayCardScheme")).willReturn("1");
        given(execution.getParameter("gatewayCardNumber")).willReturn("1");
        given(execution.getParameter("gatewayCardExpiryDateMonth")).willReturn("1");
        given(execution.getParameter("gatewayCardExpiryDateYear")).willReturn("1");
        given(execution.getParameter("gatewayCardSecurityCode")).willReturn("1");
        given(execution.getParameter("firstName")).willReturn("Firstname");
        given(execution.getParameter("lastName")).willReturn("Lastname");
        given(execution.getParameter("street1")).willReturn("street1");
        given(execution.getParameter("street2")).willReturn("street2");
        given(execution.getParameter("city")).willReturn("city");
        given(execution.getParameter("postalCode")).willReturn("postalcode");
        given(execution.getParameter("country")).willReturn("country");
        given(execution.getParameter("state")).willReturn("state");
        given(execution.getParameter("phoneNumber")).willReturn("phone");
        given(execution.getParameter("gatewayFormResponse")).willReturn("1");
        given(spyCsCardPaymentFacadeImpl.getSuggestedAmountForPaymentOption(orderModel)).willReturn(1.0);
        given(defaultCsOrderUnauthorizedTotalStrategy.getUnauthorizedTotal(orderModel)).willReturn(10.0d);
        given(targetPaymentService.tokenize(Mockito.anyString(), Mockito.any(PaymentModeModel.class))).willReturn(
                "token");
        given(targetPaymentService.capture(Mockito.any(AbstractOrderModel.class), Mockito.any(PaymentInfoModel.class),
                Mockito.any(BigDecimal.class), Mockito.any(CurrencyModel.class), Mockito.any(PaymentCaptureType.class)))
                .willReturn(paymentTransactionEntryModel);
        given(commonI18NService.roundCurrency(Mockito.anyDouble(), Mockito.anyInt())).willReturn(5.00);

        final List<String> result = facade.handleCancelPaymentFormResponse(execution);

        assertNotNull("Expecting a result", result);
        assertEquals("Expected size result", 2, result.size());
        assertEquals("success", result.get(0));
        assertEquals("successfulPayment", result.get(1));
    }

    @Test
    public void testMakeRefundWithCardPaymentInfoModel() {
        final CreditCardPaymentInfoModel paymentInfo = Mockito.mock(CreditCardPaymentInfoModel.class);
        facade.setCardPaymentInfoModel(paymentInfo);
        facade.processRefund(request, orderModel);
        verify(refundService).apply(request, orderModel, paymentInfo, null);
    }

    private CardInfo createCardInfo() {
        final String cardHolderFullName = "TEST";
        final String cardNumber = "5123456789012346";
        final String expirationMonth = "05";
        final String expirationYear = "2017";
        final String cv2Number = "123";
        final CardInfo cardInfo = new CardInfo();
        cardInfo.setCardHolderFullName(cardHolderFullName);
        cardInfo.setCardType(CreditCardType.MASTER);
        cardInfo.setCardNumber(cardNumber);
        cardInfo.setExpirationMonth(Integer.valueOf(expirationMonth));
        cardInfo.setExpirationYear(Integer.valueOf(expirationYear));
        cardInfo.setCv2Number(cv2Number);
        cardInfo.setBillingInfo(createBillingInfo());
        return cardInfo;
    }

    private BillingInfo createBillingInfo() {
        final BillingInfo billingInfo = new BillingInfo();
        billingInfo.setFirstName("test");
        billingInfo.setLastName("test");
        billingInfo.setStreet1("street1");
        return billingInfo;
    }

    @Test
    public void testCreateHostedSessionTokenRequest() {
        final CartModel cartModel = mock(CartModel.class);
        final PaymentModeModel paymentModeModel = mock(PaymentModeModel.class);
        final HostedSessionTokenRequest actualTokenrequest = facade.createHostedSessionTokenRequest(cartModel,
                BigDecimal.valueOf(10.0), paymentModeModel, "URL", "URL",
                PaymentCaptureType.PLACEORDER, "UNIQUE", IpgPaymentTemplateType.CREDITCARDSINGLE);
        assertEquals(BigDecimal.valueOf(10.0), actualTokenrequest.getAmount());
        assertEquals("URL", actualTokenrequest.getCancelUrl());
        assertEquals("URL", actualTokenrequest.getReturnUrl());
        assertEquals(PaymentCaptureType.PLACEORDER, actualTokenrequest.getPaymentCaptureType());
        assertEquals("UNIQUE", actualTokenrequest.getSessionId());
        assertEquals(paymentModeModel, actualTokenrequest.getPaymentMode());
        assertEquals(cartModel, actualTokenrequest.getOrderModel());
        assertEquals(IpgPaymentTemplateType.CREDITCARDSINGLE, actualTokenrequest.getIpgPaymentTemplateType());
    }

    @Test
    public void testCreatePaymentInfoModelForIpg() {
        final Double amount = Double.valueOf(11.01d);
        final String testReceiptNo = "testReceipt";
        final Form component = Mockito.mock(Form.class);
        final Doublebox doublebox = Mockito.mock(Doublebox.class);
        given(doublebox.getValue()).willReturn(amount);
        final Textbox textbox = Mockito.mock(Textbox.class);
        given(textbox.getValue()).willReturn(testReceiptNo);
        given(component.getFellow(TgtcsConstants.AMOUNT)).willReturn(doublebox);
        given(component.getFellow(TgtcsConstants.IPG_RECEIPT_NUMBER)).willReturn(textbox);
        facade.createPaymentInfoModelForIpg(component);
        assertEquals(amount, facade.getIpgNewRefundInfoDTO().getAmount());
        assertEquals(testReceiptNo, facade.getIpgNewRefundInfoDTO().getReceiptNo());
    }

    @Test
    public void testCreatePaymentInfoModelForIpgWithoutForm() {
        final AbstractComponent component = Mockito.mock(Window.class);
        facade.createPaymentInfoModelForIpg(component);
        assertEquals(null, facade.getIpgNewRefundInfoDTO());
    }
}
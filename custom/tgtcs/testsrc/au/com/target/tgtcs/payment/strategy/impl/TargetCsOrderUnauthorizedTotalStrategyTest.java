/**
 * 
 */
package au.com.target.tgtcs.payment.strategy.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;


/**
 * 
 * Unit Test for {@link TargetCsOrderUnauthorizedTotalStrategy}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCsOrderUnauthorizedTotalStrategyTest {

    private static final double BIG_PART_PAY = 15D;
    private static final int PART_PAY = 5;
    private static final double TOTAL_PRICE = 10D;

    @Mock
    private OrderModel order;
    @Mock
    private CartModel cart;
    @Mock
    private TargetCsOrderUnauthorizedTotalStrategy testInstance;

    @Before
    public void setUp() {
        doCallRealMethod().when(testInstance).getUnauthorizedTotal(order);
        doCallRealMethod().when(testInstance).getUnauthorizedTotal(cart);
        when(order.getTotalPrice()).thenReturn(Double.valueOf(TOTAL_PRICE));
        when(cart.getTotalPrice()).thenReturn(Double.valueOf(TOTAL_PRICE));
    }

    @Test
    public void testGetUnauthorizedTotalForOrder() {
        when(testInstance.getTransactionViableAuthAmountOrCapturedAmount(order)).thenReturn(BigDecimal.valueOf(0));
        final double unauthorizetotal = testInstance.getUnauthorizedTotal(order);

        Assert.assertTrue(unauthorizetotal == 10D);
        Mockito.verify(order, atMost(0)).getAmountCurrentPayment();
    }


    @Test
    public void testGetUnauthorizedTotalForCartWithNotAllowedPaymentDues() {
        when(testInstance.getTransactionViableAuthAmountOrCapturedAmount(cart)).thenReturn(BigDecimal.valueOf(0));
        when(cart.getAmountCurrentPayment()).thenReturn(Double.valueOf(PART_PAY));
        final PurchaseOptionConfigModel purchaseCopnfig = mock(PurchaseOptionConfigModel.class);
        when(purchaseCopnfig.getAllowPaymentDues()).thenReturn(Boolean.FALSE);
        when(cart.getPurchaseOptionConfig()).thenReturn(purchaseCopnfig);
        final double unauthorizetotal = testInstance.getUnauthorizedTotal(cart);

        assertTrue(unauthorizetotal == TOTAL_PRICE);
        verify(order, atMost(0)).getAmountCurrentPayment();
    }

    @Test
    public void testGetUnauthorizedTotalForCartWithAllowedPaymentDues() {
        when(testInstance.getTransactionViableAuthAmountOrCapturedAmount(cart)).thenReturn(BigDecimal.valueOf(0));
        when(cart.getAmountCurrentPayment()).thenReturn(Double.valueOf(PART_PAY));
        final PurchaseOptionConfigModel purchaseCopnfig = mock(PurchaseOptionConfigModel.class);
        when(purchaseCopnfig.getAllowPaymentDues()).thenReturn(Boolean.TRUE);
        when(cart.getPurchaseOptionConfig()).thenReturn(purchaseCopnfig);
        final double unauthorizetotal = testInstance.getUnauthorizedTotal(cart);

        assertTrue(unauthorizetotal == PART_PAY);
        verify(cart, atLeast(1)).getAmountCurrentPayment();
    }

    @Test
    public void testGetUnauthorizedTotalForCartWhenCurrentPaymentAmountMorThenTotalPrice() {
        when(testInstance.getTransactionViableAuthAmountOrCapturedAmount(cart)).thenReturn(BigDecimal.valueOf(0));
        when(cart.getAmountCurrentPayment()).thenReturn(Double.valueOf(BIG_PART_PAY));
        final PurchaseOptionConfigModel purchaseCopnfig = mock(PurchaseOptionConfigModel.class);
        when(purchaseCopnfig.getAllowPaymentDues()).thenReturn(Boolean.TRUE);
        when(cart.getPurchaseOptionConfig()).thenReturn(purchaseCopnfig);
        final double unauthorizetotal = testInstance.getUnauthorizedTotal(cart);

        assertTrue(unauthorizetotal == TOTAL_PRICE);
        verify(cart, atLeast(1)).getAmountCurrentPayment();
    }



}

package au.com.target.tgtcs.service.config.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


@UnitTest
public class ColourProductCustomColumnTest {
    private ColourProductCustomColumn customColumn;

    @Before
    public void prepare()
    {
        MockitoAnnotations.initMocks(this);
        customColumn = new ColourProductCustomColumn();
    }

    @Test
    public void testColourColumnMatch() {
        final TargetColourVariantProductModel product = mock(TargetColourVariantProductModel.class);
        when(product.getColourName()).thenReturn("Red");
        final String result = customColumn.getProductValue(product, null);
        Assert.assertEquals("Red", result);
    }

    @Test
    public void testColourColumnNotMatch() {
        final TargetSizeVariantProductModel product = mock(TargetSizeVariantProductModel.class);
        final String result = customColumn.getProductValue(product, null);
        Assert.assertEquals("", result);
    }

}

package au.com.target.tgtcs.service.config.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;

import java.util.Locale;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Unit test for {@link PromotionOrderEntryConsumedEntryPriceColumn}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PromotionOrderEntryConsumedEntryPriceColumnTest extends PromotionOrderEntryConsumedColumnBaseTest {

    @Before
    public void setup() {
        final PromotionOrderEntryConsumedEntryPriceColumn poecColumn = new PromotionOrderEntryConsumedEntryPriceColumn();
        spy = Mockito.spy(poecColumn);

        BDDMockito.doReturn(mockTypeService).when(spy).getTypeService();
        BDDMockito.doReturn(mockCsPromotionService).when(spy).getCsPromotionService();

        BDDMockito.given(mockTypeService.wrapItem(Mockito.any(PromotionOrderEntryConsumedModel.class))).willReturn(
                poecTypedObject);
    }

    @Test
    public void testGetItemValue() throws ValueHandlerException {

        final PromotionOrderEntryConsumedModel poecModel = Mockito.mock(PromotionOrderEntryConsumedModel.class);

        BDDMockito.given(Boolean.valueOf(mockCsPromotionService.isPromotionServiceAvailable())).willReturn(
                Boolean.TRUE);

        BDDMockito.given(mockCsPromotionService.unwrapPromotionTypedObject(poecTypedObject)).willReturn(
                mockWrappedPromotionOrderEntryConsumedModel);

        BDDMockito.given(Double.valueOf(mockWrappedPromotionOrderEntryConsumedModel.getEntryPrice()))
                .willReturn(Double.valueOf(9.99d));

        final Object result = spy.getItemValue(poecModel, Locale.ENGLISH);

        Assert.assertEquals(Double.valueOf(9.99d), result);

        Mockito.verify(mockCsPromotionService).isPromotionServiceAvailable();
        Mockito.verify(mockTypeService).wrapItem(poecModel);
        Mockito.verify(mockCsPromotionService).unwrapPromotionTypedObject(poecTypedObject);

    }

}

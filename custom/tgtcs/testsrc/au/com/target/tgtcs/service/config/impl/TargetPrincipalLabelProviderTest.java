/**
 * 
 */
package au.com.target.tgtcs.service.config.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.user.CustomerModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;

import au.com.target.tgtcore.model.TargetCustomerModel;


/**
 * Unit Test for {@link TargetPrincipalLabelProvider}
 * 
 */
@UnitTest
public class TargetPrincipalLabelProviderTest {

    private TargetPrincipalLabelProvider provider;

    /**
     * @throws java.lang.Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        provider = new TargetPrincipalLabelProvider();
    }

    /**
     * Get name data by concat first name and last name case
     */
    @Test
    public void testGetItemLabel() {
        final TargetCustomerModel customer = BDDMockito.mock(TargetCustomerModel.class);
        BDDMockito.when(customer.getFirstname()).thenReturn("Firstname");
        BDDMockito.when(customer.getLastname()).thenReturn("Lastname");
        final String rs = provider.getItemLabel(customer);
        Assert.assertTrue("Not equal", "Firstname Lastname".equals(rs));
    }

    /**
     * Name is null case
     */
    @Test
    public void testGetItemLabelNull() {
        final String rs = provider.getItemLabel(null);
        Assert.assertTrue("Not null", rs == null);
    }

    /**
     * Get name data by concatenating first name and last name case
     */
    @Test
    public void testGetItemLabelFromUID() {
        final PrincipalModel customer = BDDMockito.mock(PrincipalModel.class);
        BDDMockito.when(customer.getUid()).thenReturn("email@mail.com");
        final String rs = provider.getItemLabel(customer);
        Assert.assertTrue("Not equal email", "email@mail.com".equals(rs));
    }

    /**
     * Get name case
     */
    @Test
    public void testGetItemLabelFromName() {
        final CustomerModel customer = BDDMockito.mock(CustomerModel.class);
        BDDMockito.when(customer.getName()).thenReturn("Name");
        final String rs = provider.getItemLabel(customer);
        Assert.assertTrue("Not equal Name", "Name".equals(rs));
    }
}

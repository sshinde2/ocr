package au.com.target.tgtcs.service.meta.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcs.constants.TgtcsConstants;


@UnitTest
public class TargetPropertyServiceTest {
    private TargetPropertyService propertyService;
    @Mock
    private PropertyDescriptor propertyDescriptor;

    @Before
    public void prepare()
    {
        MockitoAnnotations.initMocks(this);
        propertyService = new TargetPropertyService();
    }

    @Test
    public void testIsMandatoryCase1() {
        when(propertyDescriptor.getQualifier()).thenReturn(TgtcsConstants.QUALIFIER_ADDRESS_LINE1);
        final boolean result = propertyService.isMandatory(propertyDescriptor, true);
        Assert.assertTrue(result);
    }

    @Test
    public void testIsMandatoryCase2() {
        when(propertyDescriptor.getQualifier()).thenReturn(TgtcsConstants.QUALIFIER_ADDRESS_POSTALCODE);
        final boolean result = propertyService.isMandatory(propertyDescriptor, true);
        Assert.assertTrue(result);
    }

    @Test
    public void testIsMandatoryCustomerTitle() {
        when(propertyDescriptor.getQualifier()).thenReturn(TgtcsConstants.QUALIFIER_CUSTOMER_TITLE);
        final boolean result = propertyService.isMandatory(propertyDescriptor, true);
        Assert.assertTrue(result);
    }

    @Test
    public void testIsMandatoryCustomerPhoneNumber() {
        when(propertyDescriptor.getQualifier()).thenReturn(TgtcsConstants.QUALIFIER_ADDRESS_PHONENUMBER1);
        final boolean result = propertyService.isMandatory(propertyDescriptor, true);
        Assert.assertTrue(result);
    }
}

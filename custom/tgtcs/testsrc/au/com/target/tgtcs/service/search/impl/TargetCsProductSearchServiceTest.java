package au.com.target.tgtcs.service.search.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.commerceservices.search.ProductSearchService;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commerceservices.search.pagedata.SortData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cscockpit.model.data.DataObject;
import de.hybris.platform.cscockpit.services.search.CsFacetSearchResult;
import de.hybris.platform.cscockpit.services.search.Pageable;
import de.hybris.platform.cscockpit.services.search.SearchException;
import de.hybris.platform.cscockpit.services.search.impl.DefaultCsTextFacetSearchCommand;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.site.BaseSiteService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.product.TargetProductSearchService;


@UnitTest
public class TargetCsProductSearchServiceTest {

    private static final Logger LOG = Logger.getLogger(TargetCsProductSearchServiceTest.class);


    @Mock
    private FlexibleSearchService flexibleSearchService;

    @Mock
    private SearchResult searchResult;
    @Mock
    private ProductSearchService<ProductModel, SearchResultValueData, ProductSearchPageData<ProductModel, SearchResultValueData>> productSearchService;

    @Mock
    private BaseSiteService baseSiteService;

    @Mock
    private TypeService typeService;

    @Mock
    private ProductService productService;

    @Mock
    private CatalogVersionService catalogVersionService;

    @Mock
    private ImpersonationService impersonationService;

    @Mock
    private TargetProductSearchService targetProductSearchService;

    @Mock
    private ConfigurationService configurationService;


    private TargetCsProductSearchService<ProductModel> targetCsProductSearchService;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        targetCsProductSearchService = new TargetCsProductSearchService<>();
        targetCsProductSearchService.setCatalogVersionService(catalogVersionService);
        targetCsProductSearchService.setProductService(productService);
        targetCsProductSearchService.setTypeService(typeService);
        targetCsProductSearchService.setProductSearchService(productSearchService);
        targetCsProductSearchService.setImpersonationService(impersonationService);
        targetCsProductSearchService.setTargetProductSearchService(targetProductSearchService);
        targetCsProductSearchService.setConfigurationService(configurationService);

        BDDMockito
                .given(impersonationService.executeInContext(
                        (ImpersonationContext)Mockito.any(),
                        (ImpersonationService.Executor<CsFacetSearchResult<DefaultCsTextFacetSearchCommand, ProductModel>, SearchException>)Mockito
                                .any()))
                .willAnswer(new Answer<CsFacetSearchResult<DefaultCsTextFacetSearchCommand, ProductModel>>() {
                    /* (non-Javadoc)
                      * @see org.mockito.stubbing.Answer#answer(org.mockito.invocation.InvocationOnMock)
                      */
                    //CHECKSTYLE:OFF
                    @Override
                    public CsFacetSearchResult<DefaultCsTextFacetSearchCommand, ProductModel> answer(
                            final InvocationOnMock invocation) throws Throwable {
                        final ImpersonationService.Executor<CsFacetSearchResult<DefaultCsTextFacetSearchCommand, ProductModel>, SearchException> executor = (ImpersonationService.Executor<CsFacetSearchResult<DefaultCsTextFacetSearchCommand, ProductModel>, SearchException>)invocation
                                .getArguments()[1];
                        return executor.execute();
                    }
                    //CHECKSTYLE:ON
                });


    }

    @Test
    public void testTgtCSSearchCustomerTest() throws SearchException {
        final Collection<CatalogVersionModel> versionModels = new ArrayList<>();
        final CatalogVersionModel sampleModel = new CatalogVersionModel();
        versionModels.add(sampleModel);
        BDDMockito.given(catalogVersionService.getSessionCatalogVersions()).willReturn(versionModels);

        final ProductModel productModel = new ProductModel();
        BDDMockito.given(productService.getProductForCode("P1040_blue_S")).willReturn(productModel);
        final Configuration configuration = Mockito.mock(Configuration.class);
        BDDMockito.given(configurationService.getConfiguration()).willReturn(configuration);
        BDDMockito.given(new Integer(configurationService.getConfiguration().getInt("tgtcs.sellable-variant", 100)))
                .willReturn(new Integer(100));

        final List<SearchResultValueData> resList = getSearchResultValueDatas();
        final DefaultCsTextFacetSearchCommand facetSearchCommand = new DefaultCsTextFacetSearchCommand();
        final CatalogModel catalogModel = new CatalogModel();
        catalogModel.setActiveCatalogVersion(sampleModel);
        facetSearchCommand.setText("Shoes");
        facetSearchCommand.setCatalog(catalogModel);

        final Pageable pageable = Mockito.mock(Pageable.class);
        BDDMockito.given(new Integer(pageable.getPageNumber())).willReturn(new Integer(0));
        BDDMockito.given(new Integer(pageable.getPageSize())).willReturn(new Integer(15));

        final ProductSearchPageData<ProductModel, SearchResultValueData> productSearchPageData = new ProductSearchPageData<ProductModel, SearchResultValueData>();
        final PaginationData pageableData = new PaginationData();
        final List<SortData> sortDatas = new ArrayList<>();
        pageableData.setCurrentPage(pageable.getPageNumber());
        pageableData.setPageSize(pageable.getPageSize());
        pageableData.setTotalNumberOfResults(10);

        productSearchPageData.setPagination(pageableData);
        productSearchPageData.setResults(resList);
        productSearchPageData.setSorts(sortDatas);

        try {
            final CsFacetSearchResult<DefaultCsTextFacetSearchCommand, ProductModel> facetSearchResult = targetCsProductSearchService
                    .search(facetSearchCommand, pageable);
            final List<DataObject<ProductModel>> resultList = facetSearchResult.getResult();
            Assert.assertEquals(0, resultList.size());
        }
        catch (final Exception e) {
            LOG.error("Exception while test get facet search result", e);
        }

    }

    protected List<SearchResultValueData> getSearchResultValueDatas() {
        final List<SearchResultValueData> searchResultValueDatas = new ArrayList<>();
        final SearchResultValueData searchResultValueData = new SearchResultValueData();
        final Map<String, Object> valueDatas = new HashMap<String, Object>();
        valueDatas.put("code", "P1040_blue_S");
        searchResultValueData.setValues(valueDatas);
        searchResultValueDatas.add(searchResultValueData);
        return searchResultValueDatas;
    }

    protected TargetSizeVariantProductModel createResultEntry(final String PK) {
        final TargetSizeVariantProductModel item = new TargetSizeVariantProductModel();
        item.setCode(PK);
        item.setBaseProduct(new ProductModel());
        return item;
    }
}
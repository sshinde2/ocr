/**
 * 
 */
package au.com.target.tgtcs.services.config.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;


/**
 * @author Nandini
 * 
 */
@UnitTest
public class ProductColourColumnTest {
    private static final String PRODUCT_COLOUR = "Black";

    private final ProductColourColumn testInstance = new ProductColourColumn();

    @Test
    public void testGetProductValue() {
        final TargetColourVariantProductModel product = mock(TargetColourVariantProductModel.class);
        when(product.getColourName()).thenReturn(PRODUCT_COLOUR);
        final String colour = testInstance.getProductValue(product, null);
        Assert.assertTrue(colour.equals(PRODUCT_COLOUR));
    }

    @Test
    public void testGetProductValueWithEmptyColour() {
        final TargetColourVariantProductModel product = mock(TargetColourVariantProductModel.class);
        when(product.getColourName()).thenReturn(StringUtils.EMPTY);
        final String colour = testInstance.getProductValue(product, null);
        Assert.assertTrue(colour.equals(StringUtils.EMPTY));
    }

    @Test
    public void testGetProductValueWithNullProduct() {
        final String colour = testInstance.getProductValue(null, null);
        Assert.assertTrue(colour.equals(StringUtils.EMPTY));
    }
}

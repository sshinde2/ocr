/**
 * 
 */
package au.com.target.tgtcs.services.config.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * @author Nandini
 * 
 */
@UnitTest
public class ProductSizeColumnTest {
    private static final String PRODUCT_SIZE = "L";
    private final ProductSizeColumn testInstance = new ProductSizeColumn();

    @Test
    public void testGetProductValue() {
        final TargetSizeVariantProductModel product = mock(TargetSizeVariantProductModel.class);
        when(product.getSize()).thenReturn(PRODUCT_SIZE);
        final String size = testInstance.getProductValue(product, null);
        Assert.assertTrue(size.equals(PRODUCT_SIZE));
    }

    @Test
    public void testGetProductValueWithNullSize() {
        final TargetSizeVariantProductModel product = mock(TargetSizeVariantProductModel.class);
        when(product.getSize()).thenReturn(StringUtils.EMPTY);
        final String size = testInstance.getProductValue(product, null);
        Assert.assertTrue(size.equals(StringUtils.EMPTY));
    }

    @Test
    public void testGetProductValueWithNullProduct() {
        final String size = testInstance.getProductValue(null, null);
        Assert.assertTrue(size.equals(StringUtils.EMPTY));
    }
}

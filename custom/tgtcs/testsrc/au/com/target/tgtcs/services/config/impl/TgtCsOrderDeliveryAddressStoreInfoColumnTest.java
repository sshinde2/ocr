/**
 * 
 */
package au.com.target.tgtcs.services.config.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;


/**
 * @author bhuang3
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TgtCsOrderDeliveryAddressStoreInfoColumnTest {

    @Spy
    private final TgtCsOrderDeliveryAddressStoreInfoColumn testInstance = new TgtCsOrderDeliveryAddressStoreInfoColumn();

    @Mock
    private ApplicationContext ctx;

    @Mock
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Test
    public void testGetItemValueWithOrderModel() throws ValueHandlerException {
        final OrderModel orderModel = new OrderModel();
        final TargetPointOfServiceData selectedStore = new TargetPointOfServiceData();
        selectedStore.setName("TestStoreName");
        orderModel.setCncStoreNumber(Integer.valueOf(1000));
        when(testInstance.getCtx()).thenReturn(ctx);
        when(ctx.getBean(Mockito.anyString())).thenReturn(targetStoreLocatorFacade);
        when(targetStoreLocatorFacade.getPointOfService(Integer.valueOf(1000))).thenReturn(selectedStore);
        final String result = (String)testInstance.getItemValue(orderModel, null);
        Assert.assertEquals("Target Store - TestStoreName - 1000", result);
    }

    @Test
    public void testGetItemValueWithoutOrderModel() throws ValueHandlerException {
        final AddressModel addressModel = new AddressModel();
        final String result = (String)testInstance.getItemValue(addressModel, null);
        Assert.assertEquals(StringUtils.EMPTY, result);
    }

}

/**
 * 
 */
package au.com.target.tgtcs.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.LinkedList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Div;

import au.com.target.tgtcore.refund.TargetOrderRefundException;


/**
 * @author Pavel_Yakushevich
 * 
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest(TargetCsCockpitUtil.class)
public class TargetCsCockpitUtilTest
{

    private Component parent;
    private List<Component> children;

    @Before
    public void setUp()
    {
        parent = new Div();
        parent.setId("parent");

        children = new LinkedList<>();

        for (int i = 0; i < 5; i++)
        {
            final Div child = new Div();
            child.setId("child " + i);
            children.add(child);
        }
    }

    @Test
    public void testAddComponents()
    {
        final int childrenBefore = parent.getChildren().size();

        TargetCsCockpitUtil.addAllChildren(parent, children);

        final Integer expected = Integer.valueOf(childrenBefore + children.size());
        final Integer actual = Integer.valueOf(parent.getChildren().size());

        assertEquals(String.format("Expected children: [%d], actual: [%d]", expected, actual), expected, actual);
    }

    @Test(expected = NullPointerException.class)
    public void testAddNullComponent()
    {
        // LinkedList permits null elements

        children.add(null);
        final int childrenBefore = parent.getChildren().size();

        TargetCsCockpitUtil.addAllChildren(parent, children);

        final Integer expected = Integer.valueOf(childrenBefore + children.size());
        final Integer actual = Integer.valueOf(parent.getChildren().size());

        assertEquals(String.format("Expected children: [%d], actual: [%d]", expected, actual), expected, actual);
    }

    @Test
    public void testRemoveComponents()
    {
        parent.getChildren().addAll(children);

        assertTrue(parent.getChildren().size() > 0);

        TargetCsCockpitUtil.removeChildren(parent);

        final Integer expected = Integer.valueOf(0);
        final Integer actual = Integer.valueOf(parent.getChildren().size());

        assertEquals(String.format("Expected children: [%d], actual: [%d]", expected, actual), expected, actual);
    }

    @Test
    public void testValidateManualRefundInput()
    {
        final Double amount = Double.valueOf(123.55);
        final String receiptNo = "8fe6874f4b8ba";
        try {
            TargetCsCockpitUtil.validateManualRefundInput(amount, receiptNo);
        }
        catch (final TargetOrderRefundException e) {
            Assert.fail("should not throw TargetOrderRefundException");
        }
    }

    @Test
    public void testValidateManualRefundInputWithRefundAmountNotValid()
    {
        final Double amount = Double.valueOf(-123.55);
        final String receiptNo = "8fe6874f4b8ba";
        try {
            TargetCsCockpitUtil.validateManualRefundInput(amount, receiptNo);
            Assert.fail("should throw TargetOrderRefundException");
        }
        catch (final TargetOrderRefundException e) {
            assertEquals("input refund amount is not valid", e.getMessage());
        }
    }

    @Test
    public void testValidateManualRefundInputWithInputReceiptNoEmpty()
    {
        final Double amount = Double.valueOf(123.55);
        final String receiptNo = "";
        try {
            TargetCsCockpitUtil.validateManualRefundInput(amount, receiptNo);
            Assert.fail("should throw TargetOrderRefundException");
        }
        catch (final TargetOrderRefundException e) {
            assertEquals("input receipt No cannot be empty", e.getMessage());
        }
    }

    @Test
    public void testValidateManualRefundInputWithLengthOfInputReceiptNoExceed17()
    {
        final Double amount = Double.valueOf(123.55);
        final String receiptNo = "123456789012345678";
        try {
            TargetCsCockpitUtil.validateManualRefundInput(amount, receiptNo);
            Assert.fail("should throw TargetOrderRefundException");
        }
        catch (final TargetOrderRefundException e) {
            assertEquals("The length of the receipt no cannot excceed 17 characters", e.getMessage());
        }
    }
}

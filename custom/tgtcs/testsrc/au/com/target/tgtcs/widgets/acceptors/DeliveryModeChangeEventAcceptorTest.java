package au.com.target.tgtcs.widgets.acceptors;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.cscockpit.widgets.renderers.utils.PopupWidgetHelper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Window;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcs.facade.TargetCSOrderFacade;
import au.com.target.tgtcs.widgets.controllers.TargetBasketController;
import au.com.target.tgtcs.widgets.events.DeliveryModeChangeEvent;
import au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;

import com.google.common.collect.ImmutableList;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DeliveryModeChangeEventAcceptorTest {

    @Mock
    private PopupWidgetHelper popupWidgetHelper;

    @Mock
    private Widget deliveryAddressWidget;

    @Mock
    private TargetDeliveryModeHelper deliveryModeHelper;

    @Mock
    private TargetCSOrderFacade orderFacade;
    @Mock
    private TargetBasketController basketController;
    @Mock
    private PurchaseOptionModel purchaseOptionModel;

    @InjectMocks
    @Spy
    private final DeliveryModeChangeEventAcceptor testInstance = new DeliveryModeChangeEventAcceptor();

    @Mock
    private DeliveryModeModel deliveryMode;

    @Mock
    private SelectCnCStoreWidgetModel widgetModel;

    @Mock
    private Widget widget;

    @Test
    public void testOnCockpitEvent() {
        final DeliveryModeChangeEvent event = mock(DeliveryModeChangeEvent.class);
        final TypedObject object = mock(TypedObject.class);
        final CartModel cart = mock(CartModel.class);
        when(object.getObject()).thenReturn(cart);
        when(event.getOrder()).thenReturn(object);
        when(event.getSource()).thenReturn(widget);

        final TypedObject wrappedMode = mock(TypedObject.class);
        when(wrappedMode.getObject()).thenReturn(deliveryMode);
        when(event.getNewDeliveryMode()).thenReturn(wrappedMode);
        when(testInstance.getPopupWidgetHelper()).thenReturn(popupWidgetHelper);

        when(Boolean.valueOf(deliveryModeHelper.isDeliveryModeStoreDelivery(deliveryMode)))
                .thenReturn(Boolean.TRUE);
        when(Boolean.valueOf(orderFacade.isOrderAvailableForDeliveryMode(cart, deliveryMode))).thenReturn(
                Boolean.TRUE);
        when(purchaseOptionModel.getCode()).thenReturn("buynow");
        when(basketController.getPurchaseOptionModel()).thenReturn(purchaseOptionModel);
        when(widget.getWidgetModel()).thenReturn(widgetModel);
        when(widget.getWidgetController()).thenReturn(basketController);
        when(testInstance.getCncStoreDetailsWidget()).thenReturn(widget);

        doNothing().when(testInstance).createAndShowCnCDetailsPopup(Mockito.anyString());
        doNothing().when(widgetModel).setSelectedDeliveryMode(deliveryMode);

        testInstance.onCockpitEvent(event);
        verify(event).getSource();
        verify(testInstance).createAndShowCnCDetailsPopup(Mockito.anyString());
        verify(widgetModel).setSelectedDeliveryMode(deliveryMode);
    }

    @Test
    public void testCreateAndShowCnCDetailsPopup() {
        final String widgetCode = "selectClickAndCollectStoreWidgetConfig-Popup";
        final Component root = mock(Component.class);
        final Window window = mock(Window.class);

        when(widget.getWidgetCode()).thenReturn(widgetCode);
        when(widget.getWidgetModel()).thenReturn(widgetModel);
        when(window.getChildren()).thenReturn(ImmutableList.of(widget));
        when(deliveryAddressWidget.getRoot()).thenReturn(root);
        when(testInstance.getDeliveryAddressWidget()).thenReturn(deliveryAddressWidget);

        when(popupWidgetHelper.createPopupWidget(
                Mockito.eq(root),
                Mockito.eq("selectClickAndCollectStoreWidgetConfig"),
                Mockito.eq(widgetCode),
                Mockito.eq("selectClickAndCollectStorePopup"),
                Mockito.anyString(),
                (EventListener)Mockito.anyObject())).thenReturn(window);

        testInstance.createAndShowCnCDetailsPopup("");

        verify(popupWidgetHelper).createPopupWidget(Mockito.eq(root),
                Mockito.eq("selectClickAndCollectStoreWidgetConfig"),
                Mockito.eq(widgetCode),
                Mockito.eq("selectClickAndCollectStorePopup"),
                Mockito.anyString(),
                (EventListener)Mockito.anyObject());
    }
}
/**
 * 
 */
package au.com.target.tgtcs.widgets.adapters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.cscockpit.widgets.models.impl.CartAddressWidgetModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.exception.TargetNoPostCodeException;


/**
 * @author pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCartShippingAddressAdapterTest {

    @InjectMocks
    private final TargetCartShippingAddressAdapter shippingAddressAdapter = new TargetCartShippingAddressAdapter();

    @Mock
    private TargetDeliveryService targetDeliveryService;

    @Mock
    private CartAddressWidgetModel widgetModel;

    @Mock
    private CartModel cart;

    private final List<AddressModel> addresses = new ArrayList<>();

    @Mock
    private TypeService typeService;

    @Mock
    private List<TypedObject> addressObjects;

    @Mock
    private TypedObject cartObject;

    @Before
    public void setUp() throws Exception {
        final AddressModel addressOneForUser = Mockito.mock(AddressModel.class);
        final AddressModel addressTwoForUser = Mockito.mock(AddressModel.class);
        addresses.add(addressOneForUser);
        addresses.add(addressTwoForUser);
        final DeliveryModeModel deliveryMode = Mockito.mock(DeliveryModeModel.class);
        Mockito.when(cart.getDeliveryMode()).thenReturn(deliveryMode);
        Mockito.when(shippingAddressAdapter.getAvailableAddressesForCart(cart)).thenReturn(addresses);
        Mockito.when(
                Boolean.valueOf(targetDeliveryService.isDeliveryModePostCodeCombinationValid(deliveryMode,
                        addressOneForUser, cart))).thenReturn(Boolean.TRUE);
        Mockito.when(
                Boolean.valueOf(targetDeliveryService.isDeliveryModePostCodeCombinationValid(deliveryMode,
                        addressTwoForUser, cart))).thenThrow(new TargetNoPostCodeException("No Postcode"));
        Mockito.when(typeService.wrapItems(addresses)).thenReturn(addressObjects);
        Mockito.when(typeService.wrapItem(cart)).thenReturn(cartObject);
    }

    /**
     * Test case to test scenario where postcode exception is thrown for one of the addresses, even though exception is
     * thrown address is added to be viewed in cscockpit.
     */
    @Test
    public void testUpdateValidShippingAddressWhenTargetNoPostCodeExceptionIsThrown() {
        Assert.assertFalse(shippingAddressAdapter.updateValidShippingAddress(cart));
        Mockito.verify(widgetModel).setItems(addressObjects);
        Mockito.verify(widgetModel).setCart(cartObject);
    }

}

package au.com.target.tgtcs.widgets.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.services.meta.impl.DefaultTypeService;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.strategies.ModifiableChecker;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.cscockpit.services.checkout.CsCheckoutService;
import de.hybris.platform.cscockpit.widgets.controllers.CallContextController;
import de.hybris.platform.cscockpit.widgets.controllers.CheckoutController;
import de.hybris.platform.cscockpit.widgets.controllers.strategies.BasketStrategy;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.stock.StockService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcs.dto.CnCDetailsFormData;
import au.com.target.tgtcs.validators.CnCFormValidator;
import au.com.target.tgtcs.widgets.controllers.impl.TargetBasketControllerImpl;
import au.com.target.tgtcs.widgets.controllers.impl.TargetCnCStoreSelectController;
import au.com.target.tgtcs.widgets.controllers.strategies.TargetBasketStrategy;
import au.com.target.tgtcs.widgets.impl.SelectCnCStoreWidgetModel;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.order.TargetLaybyCommerceCheckoutService;
import au.com.target.tgtlayby.purchase.PurchaseOptionConfigService;
import au.com.target.tgtlayby.purchase.PurchaseOptionService;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


/**
 * Unit Test for {@link TargetCommerceCartService}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetBasketControllerImplTest {

    private static final long MAX_QUANTITY = 99L;
    private static final Long OLD_QUANTITY = new Long(5L);
    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Mock
    private ProductService productService;

    @Mock
    private TargetLaybyCartService targetLaybyCartService;

    @Mock
    private TimeService timeService;

    @Mock
    private ModelService modelService;

    @Mock
    private CalculationService calculationService;

    @Mock
    private PromotionsService promotionsService;

    @Mock
    private ModifiableChecker<AbstractOrderEntryModel> entryOrderChecker;

    @Mock
    private Map<String, String> configProvider;

    @Mock
    private Map<String, String> localeProvider;

    @Mock
    private ProductModel product;

    @Mock
    private TypedObject typedObject;


    private CartModel cart;

    @Mock
    private PurchaseOptionModel purchaseOption;


    private CartEntryModel cartEntry;

    @Mock
    private UnitModel unitModel;

    @Mock
    private StockService stockService;

    @Mock
    private WarehouseService warehouseService;

    @Mock
    private PurchaseOptionConfigService purchaseOptionConfigService;

    @Mock
    private PurchaseOptionService purchaseOptionService;

    @Mock
    private PurchaseOptionConfigModel purchaseOptionConfigModel;

    @Mock
    private WarehouseModel warehouseModel;

    @Mock
    private StockLevelModel stockLevelModel;

    @Mock
    private TargetStockService targetStockService;

    @Mock
    private AddressModel addressModel;

    @Mock
    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;

    @Mock
    private CustomerModel customerModel;

    @Mock
    private TypedObject customer;

    @Mock
    private TypeService cockpitTypeService;

    @Mock
    private CallContextController callContextController;

    @Mock
    private TargetBasketStrategy targetBasketStrategy;

    @Mock
    private BasketStrategy basketStrategy;

    @Mock
    private SessionService sessionService;

    @Mock
    private CheckoutController checkoutController;

    @Mock
    private TargetDeliveryService targetDeliveryService;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;


    // Implementation for set quantity testing
    private final TargetBasketControllerImpl targetBasketControllerImpl = new TargetBasketControllerImpl() {

        // To avoid initializing all platform,  override some methods with providing by mock data
        @Override
        public TypedObject getCart() {
            return typedObject;
        }

        @Override
        public CartModel getMasterCart() {
            return cart;
        }

        @Override
        protected CartModel getCartModel() {
            return cart;
        }

        @Override
        protected void calculateCartInContext(final CartModel cartModel) {
            // Do nothing because this method invoke other services
            // which we do not need to care in this test case
        }

        /* (non-Javadoc)
         * @see de.hybris.platform.cscockpit.widgets.controllers.impl.DefaultBasketController#lookupCheckoutController()
         */
        @Override
        protected CheckoutController lookupCheckoutController() {
            return checkoutController;
        }



    };

    // Implementation for get master cart testing
    private final TargetBasketControllerImpl targetBasketControllerForMasterCart = new TargetBasketControllerImpl() {

        // To avoid initializing all platform,  override some methods with providing by mock data
        @Override
        public TypedObject getCart() {
            return typedObject;
        }

        @Override
        protected CartModel getCartModel() {
            return cart;
        }

        @Override
        protected void calculateCartInContext(final CartModel cartModel) {
            // Do nothing because this method invoke other services
            // which we do not need to care in this test case
        }
    };
    // Implementation for get cart testing
    private final TargetBasketControllerImpl targetBasketControllerImplForGetCart = new TargetBasketControllerImpl() {

        @Override
        protected CartModel getCartModel() {
            return cart;
        }

        @Override
        protected void calculateCartInContext(final CartModel cartModel) {
            // Do nothing because this method invoke other services
            // which we do not need to care in this test case
        }
    };

    private void setupForCart() {
        targetBasketControllerForMasterCart.setCallContextController(callContextController);
        targetBasketControllerForMasterCart.setTargetBasketStrategy(targetBasketStrategy);
        targetBasketControllerImplForGetCart.setCallContextController(callContextController);
        targetBasketControllerImplForGetCart.setBasketStrategy(basketStrategy);
        targetBasketControllerImplForGetCart.setCockpitTypeService(cockpitTypeService);
        targetBasketControllerImplForGetCart.setTargetBasketStrategy(targetBasketStrategy);
        targetBasketControllerForMasterCart.setSessionService(sessionService);
    }

    @SuppressWarnings("boxing")
    @Before
    public void setup() throws Exception {
        setupForCart();
        cart = new CartModel();
        cartEntry = new CartEntryModel();
        cartEntry.setOrder(cart);
        cartEntry.setProduct(product);
        cartEntry.setQuantity(OLD_QUANTITY);
        cartEntry.setInfo("test entry");
        cart.setPaymentAddress(addressModel);

        BDDMockito.given(purchaseOptionConfigService
                .getActivePurchaseOptionConfigByPurchaseOption(purchaseOption)).willReturn(purchaseOptionConfigModel);
        BDDMockito.given(purchaseOptionConfigModel.getWarehouse()).willReturn(warehouseModel);
        BDDMockito.given(stockService.getStockLevel(product, warehouseModel)).willReturn(stockLevelModel);
        BDDMockito.given(stockLevelModel.getAvailable()).willReturn(99);

        BDDMockito.given(targetBasketStrategy.getPurchaseOptionCode()).willReturn(StringUtils.EMPTY);
        BDDMockito.given(targetPurchaseOptionHelper.getPurchaseOptionModel(StringUtils.EMPTY))
                .willReturn(purchaseOption);

        BDDMockito.given(typedObject.getObject()).willReturn(cartEntry);

        final MessageSource messageSource = Mockito.mock(MessageSource.class);
        final TargetCommerceCartService targetCommerceCartService = Mockito
                .mock(TargetCommerceCartService.class);
        BDDMockito
                .when(targetCommerceCartService.updateEntry((CartModel)Mockito.anyObject(),
                        (ProductModel)Mockito.anyObject(), Mockito.anyLong()))
                .thenAnswer(new Answer<CommerceCartModification>() {
                    /* (non-Javadoc)
                      * @see org.mockito.stubbing.Answer#answer(org.mockito.invocation.InvocationOnMock)
                      */
                    @Override
                    public CommerceCartModification answer(final InvocationOnMock invocation) {
                        final CartModel cartModel = (CartModel)invocation.getArguments()[0];
                        final List<AbstractOrderEntryModel> entryList = cartModel.getEntries();
                        final CartEntryModel cartEntry1 = (CartEntryModel)entryList.get(0);
                        final Long newQuantity = (Long)invocation.getArguments()[2];
                        cartEntry1.setQuantity(newQuantity);
                        // remove
                        if (newQuantity.longValue() == 0) {
                            entryList.remove(0);
                            cartModel.setEntries(entryList);
                        }
                        if (newQuantity.longValue() > MAX_QUANTITY) {
                            if (stockLevelModel.getAvailable() <= MAX_QUANTITY) {
                                cartEntry1.setQuantity(Long.valueOf(String.valueOf(stockLevelModel.getAvailable())));
                            }
                            else {
                                cartEntry1.setQuantity(Long.valueOf(MAX_QUANTITY));
                            }
                        }

                        final CommerceCartModification rs = new CommerceCartModification();
                        rs.setStatusCode("success");
                        return rs;
                    }
                });

        final I18NService i18nService = Mockito.mock(I18NService.class);
        targetBasketControllerImpl.setTargetCommerceCartService(targetCommerceCartService);
        targetBasketControllerImpl.setMessageSource(messageSource);
        targetBasketControllerImpl.setI18nService(i18nService);
        targetBasketControllerImpl.setTargetPurchaseOptionHelper(targetPurchaseOptionHelper);
        targetBasketControllerImpl.setTargetBasketStrategy(targetBasketStrategy);

        BDDMockito.given(targetLaybyCartService.getEntryForProduct(cart, product))
                .willReturn(cartEntry);

        BDDMockito.given(entryOrderChecker.canModify(cartEntry)).willReturn(Boolean.TRUE);

        final List<AbstractOrderEntryModel> cartEntries = new ArrayList<>();
        cartEntries.add(cartEntry);
        cart.setEntries(cartEntries);
    }

    @Test
    public void testSetQuantity() throws Exception {
        final long newQuantity = 10L;
        targetBasketControllerImpl.setQuantityToSku(typedObject, newQuantity, OLD_QUANTITY);
        Assert.assertTrue("Quantity not updated " + cartEntry.getQuantity(), newQuantity == cartEntry.getQuantity()
                .longValue());

        targetBasketControllerImpl.setQuantityToSku(typedObject, newQuantity + 1, OLD_QUANTITY);
        Assert.assertTrue("Quantity not updated " + cartEntry.getQuantity(),
                (newQuantity + 1) == cartEntry.getQuantity().longValue());
    }

    @Test
    public void testSetLowerZeroNumber() throws Exception {
        final long newQuantity = -1L;
        targetBasketControllerImpl.setQuantityToSku(typedObject, newQuantity, OLD_QUANTITY);

        Assert.assertTrue("Not equal current quantity", OLD_QUANTITY.longValue() == cartEntry.getQuantity().longValue());
    }

    @Test
    public void testSetOverMaxNumber() throws Exception {
        final long newQuantity = 1000L;
        targetBasketControllerImpl.setQuantityToSku(typedObject, newQuantity, OLD_QUANTITY);
        final int stockAvailable = stockLevelModel.getAvailable();
        Assert.assertTrue("Not equal current quantity", stockAvailable == cartEntry.getQuantity().longValue());
    }

    @Test
    public void testAvailableStockUnderMaxNumber() throws Exception {
        final long newQuantity = 1000L;
        final int availableNumer = 50;
        BDDMockito.given(Integer.valueOf(stockLevelModel.getAvailable())).willReturn(Integer.valueOf(availableNumer));
        targetBasketControllerImpl.setQuantityToSku(typedObject, newQuantity, OLD_QUANTITY);
        Assert.assertTrue("Not equal available number of stock level quantity",
                availableNumer == cartEntry.getQuantity().longValue());
    }

    @Test
    public void testAvailableStockOverMaxNumber() throws Exception {
        final long newQuantity = 1000L;
        final int availableNumer = 500;
        BDDMockito.given(Integer.valueOf(stockLevelModel.getAvailable())).willReturn(Integer.valueOf(availableNumer));
        targetBasketControllerImpl.setQuantityToSku(typedObject, newQuantity, OLD_QUANTITY);
        Assert.assertTrue("Not equal max quantity", MAX_QUANTITY == cartEntry.getQuantity().longValue());
    }

    @Test
    public void testGetMasterCart() {
        final String cartCode = "00001";
        targetBasketControllerForMasterCart.setCallContextController(callContextController);
        targetBasketControllerForMasterCart.setTargetBasketStrategy(targetBasketStrategy);

        final CartModel cartModel = new CartModel();
        cartModel.setCode(cartCode);

        final TypedObject cartObject = Mockito.mock(TypedObject.class);
        BDDMockito.given(cartObject.getObject()).willReturn(cartModel);

        BDDMockito.given(customerModel.getName()).willReturn("ABC");
        BDDMockito.given(customer.getObject()).willReturn(customerModel);
        //anonymous
        BDDMockito.given(callContextController.getAnonymousCustomer()).willReturn(customer);
        BDDMockito.given(callContextController.getCurrentCustomer()).willReturn(null);
        BDDMockito.given(callContextController.getAnonymousCartCode()).willReturn("code");

        BDDMockito.given(targetBasketStrategy.getPersistedMasterCart(customerModel)).willReturn(cartModel);
        BDDMockito.given(targetBasketStrategy.getCart(customer, "code")).willReturn(cartObject);

        final CartModel rs = targetBasketControllerForMasterCart.getMasterCart();
        Assert.assertEquals(rs.getCode(), cartCode);
        //customer
        BDDMockito.given(callContextController.getCurrentCustomer()).willReturn(customer);

        final CartModel rs2 = targetBasketControllerForMasterCart.getMasterCart();
        Assert.assertEquals(rs2.getCode(), cartCode);
    }

    @Test
    public void testAnonymousGetCart() {
        final String cartCode = "00001";
        BDDMockito.given(customerModel.getName()).willReturn("ABC");
        BDDMockito.given(customer.getObject()).willReturn(customerModel);
        BDDMockito.given(callContextController.getCurrentCustomer()).willReturn(customer);
        BDDMockito.given(callContextController.getAnonymousCustomer()).willReturn(customer);
        BDDMockito.given(Boolean.valueOf(callContextController.isAnonymousCustomer(customer))).willReturn(Boolean.TRUE);
        BDDMockito.given(callContextController.getAnonymousCartCode()).willReturn(cartCode);

        final CartModel cartModel = new CartModel();
        cartModel.setCode(cartCode);
        final TypedObject cartObj = Mockito.mock(TypedObject.class);
        BDDMockito.given(customer.getObject()).willReturn(customerModel);
        cart.setCode(cartCode);
        BDDMockito.given(cartObj.getObject()).willReturn(cart);

        BDDMockito.given(targetBasketStrategy.getPersistedMasterCart(customerModel)).willReturn(cartModel);
        BDDMockito.given(basketStrategy.getCart(customer, cartCode))
                .willReturn(cartObj);
        //mock strategy
        BDDMockito.given(targetBasketStrategy.loadCart(customer, cartCode)).willReturn(cartObj);

        final TypedObject rs = targetBasketControllerImplForGetCart.getCart();

        final CartModel rsCartModel = (CartModel)rs.getObject();
        Assert.assertEquals(rsCartModel.getCode(), cartCode);
    }

    @Test
    public void testCustomerGetCartBuyNowCartNotNull() {
        final String cartCode = "00001";
        BDDMockito.given(customerModel.getName()).willReturn("ABC");
        BDDMockito.given(customer.getObject()).willReturn(customerModel);
        BDDMockito.given(callContextController.getCurrentCustomer()).willReturn(customer);
        BDDMockito.given(callContextController.getAnonymousCustomer()).willReturn(customer);
        BDDMockito.given(Boolean.valueOf(callContextController.isAnonymousCustomer(customer)))
                .willReturn(Boolean.FALSE);
        BDDMockito.given(callContextController.getAnonymousCartCode()).willReturn(cartCode);

        final CartModel cartModel = new CartModel();
        cartModel.setCode(cartCode);
        final TypedObject cartObj = Mockito.mock(TypedObject.class);
        BDDMockito.given(customer.getObject()).willReturn(customerModel);
        BDDMockito.given(cartObj.getObject()).willReturn(cartModel);

        BDDMockito.given(targetBasketStrategy.getCart(customerModel)).willReturn(cartModel);
        BDDMockito.given(basketStrategy.getCart(customer, cartCode))
                .willReturn(cartObj);
        BDDMockito.given(cockpitTypeService.wrapItem(cartModel)).willReturn(cartObj);

        final TypedObject rs = targetBasketControllerImplForGetCart.getCart();

        final CartModel rsCartModel = (CartModel)rs.getObject();
        Assert.assertEquals(rsCartModel.getCode(), cartCode);
    }

    @Test
    public void testCustomerGetCartBuyNowCartIsNull() {
        final String cartCode = "00001";
        BDDMockito.given(customerModel.getName()).willReturn("ABC");
        BDDMockito.given(customer.getObject()).willReturn(customerModel);
        BDDMockito.given(callContextController.getCurrentCustomer()).willReturn(customer);
        BDDMockito.given(callContextController.getAnonymousCustomer()).willReturn(customer);
        BDDMockito.given(Boolean.valueOf(callContextController.isAnonymousCustomer(customer)))
                .willReturn(Boolean.FALSE);
        BDDMockito.given(callContextController.getAnonymousCartCode()).willReturn(cartCode);

        final CartModel cartModel = new CartModel();
        cartModel.setCode(cartCode);
        final TypedObject cartObj = Mockito.mock(TypedObject.class);
        BDDMockito.given(customer.getObject()).willReturn(customerModel);
        BDDMockito.given(cartObj.getObject()).willReturn(cartModel);

        BDDMockito.given(targetBasketStrategy.getCart(customerModel)).willReturn(null);
        BDDMockito.given(targetBasketStrategy.createCart(customerModel)).willReturn(cartModel);
        BDDMockito.given(cockpitTypeService.wrapItem(cartModel)).willReturn(cartObj);

        final TypedObject rs = targetBasketControllerImplForGetCart.getCart();

        final CartModel rsCartModel = (CartModel)rs.getObject();
        Assert.assertEquals(rsCartModel.getCode(), cartCode);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetAvailableDeliveryModes() {
        final TargetCheckoutController targetCheckoutController = mock(TargetCheckoutController.class);
        final TargetBasketControllerImpl mytargetBasketControllerImpl = new TargetBasketControllerImpl() {
            @Override
            public CheckoutController getCheckoutController() {
                return targetCheckoutController;
            }
        };
        final CartModel cartModel = mock(CartModel.class);
        when(targetCheckoutController.getCheckoutCartByMasterCart()).thenReturn(cartModel);
        final CsCheckoutService csCheckoutService = mock(CsCheckoutService.class);
        final Collection<TargetZoneDeliveryModeModel> deliveryModeModels = new ArrayList<>();
        final TargetZoneDeliveryModeModel clickAndCollectDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        when(clickAndCollectDeliveryModeModel.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);
        final TargetZoneDeliveryModeModel homeDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        when(homeDeliveryModeModel.getIsDeliveryToStore()).thenReturn(Boolean.FALSE);
        Mockito.when(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(
                cartModel,
                homeDeliveryModeModel)).thenReturn(Boolean.TRUE);
        Mockito.when(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(
                cartModel,
                clickAndCollectDeliveryModeModel)).thenReturn(Boolean.TRUE);
        deliveryModeModels.add(clickAndCollectDeliveryModeModel);
        deliveryModeModels.add(homeDeliveryModeModel);
        Mockito.when(
                targetDeliveryService.getAllApplicableDeliveryModesForOrder(cartModel, SalesApplication.CALLCENTER))
                .thenReturn(
                        deliveryModeModels);

        mytargetBasketControllerImpl.setTargetDeliveryModeHelper(targetDeliveryModeHelper);
        mytargetBasketControllerImpl.setTargetDeliveryService(targetDeliveryService);
        mytargetBasketControllerImpl.setCsCheckoutService(csCheckoutService);
        final TypeService typeService = new DefaultTypeService() {
            @Override
            public List<TypedObject> wrapItems(final Collection<? extends Object> itemObjects) {
                final List<TypedObject> list = new ArrayList<>();
                for (final Object object : itemObjects) {
                    final TypedObject myTypedObject = mock(TypedObject.class);
                    when(myTypedObject.getObject()).thenReturn(object);
                    list.add(myTypedObject);
                }
                return list;
            }
        };
        mytargetBasketControllerImpl.setCockpitTypeService(typeService);
        final List<TypedObject> result = mytargetBasketControllerImpl.getAvailableDeliveryModes(cartModel);
        Assert.assertEquals(result.size(), 2);

        Assert.assertTrue(isObjectInCollection(result, clickAndCollectDeliveryModeModel));
        Assert.assertTrue(isObjectInCollection(result, homeDeliveryModeModel));
    }

    @Test
    public void testHandleCnCModeSelection() {
        final SelectCnCStoreWidgetModel widgetModel = mock(SelectCnCStoreWidgetModel.class);
        final Widget widget = mock(Widget.class);
        final Validator validator = mock(CnCFormValidator.class);
        final TargetLaybyCommerceCheckoutService checkoutService = mock(TargetLaybyCommerceCheckoutService.class);
        final TargetPointOfServiceModel posModel = mock(TargetPointOfServiceModel.class);
        when(posModel.getStoreNumber()).thenReturn(Integer.valueOf(1));
        when(widgetModel.getStore()).thenReturn(posModel);
        when(widget.getWidgetModel()).thenReturn(widgetModel);
        Mockito.doNothing().when(validator).validate(Mockito.anyObject(), (Errors)Mockito.anyObject());

        targetBasketControllerImpl.setClickAndCollectFormValidator(validator);
        targetBasketControllerImpl.setTargetLaybyCommerceCheckoutService(checkoutService);

        targetBasketControllerImpl.handleCnCModeSelection(widget);
        Mockito.verify(checkoutService).fillCart4ClickAndCollect((CartModel)Mockito.anyObject(), Mockito.anyInt(),
                Mockito.anyString(),
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), (DeliveryModeModel)Mockito.anyObject());
        Mockito.verify(checkoutController).dispatchEvent(null, widget, null);

    }

    @Test
    public void testHandleCnCModeSelectionShowErrors() {
        final SelectCnCStoreWidgetModel widgetModel = mock(SelectCnCStoreWidgetModel.class);
        final TargetCnCStoreSelectController widgetController = mock(TargetCnCStoreSelectController.class);
        final Widget widget = mock(Widget.class);
        final Validator validator = mock(CnCFormValidator.class);
        final TargetPointOfServiceModel posModel = mock(TargetPointOfServiceModel.class);
        final CnCDetailsFormData formData = mock(CnCDetailsFormData.class);
        final TargetLaybyCommerceCheckoutService checkoutService = mock(TargetLaybyCommerceCheckoutService.class);

        when(posModel.getStoreNumber()).thenReturn(Integer.valueOf(1));
        when(widgetModel.getStore()).thenReturn(posModel);
        when(widgetModel.getCncFormData()).thenReturn(formData);
        when(widget.getWidgetModel()).thenReturn(widgetModel);
        when(widget.getWidgetController()).thenReturn(widgetController);

        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) {
                final Object[] args = invocation.getArguments();
                ((Errors)args[1]).rejectValue("firstName", "message");
                return null;
            }
        }).when(validator).validate(Mockito.anyObject(), (Errors)Mockito.anyObject());

        targetBasketControllerImpl.setClickAndCollectFormValidator(validator);
        targetBasketControllerImpl.setTargetLaybyCommerceCheckoutService(checkoutService);

        targetBasketControllerImpl.handleCnCModeSelection(widget);
        Mockito.verify(widgetController).displayValidationErrors((Errors)Mockito.anyObject(), Mockito.eq(widget));

    }

    private boolean isObjectInCollection(final List<TypedObject> items, final Object object) {
        for (final TypedObject item : items) {
            if (object.equals(item.getObject())) {
                return true;
            }
        }
        return false;
    }

}

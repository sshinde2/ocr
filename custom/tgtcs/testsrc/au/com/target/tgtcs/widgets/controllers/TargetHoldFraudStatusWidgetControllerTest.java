/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers;

import static org.mockito.Mockito.doReturn;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import au.com.target.tgtcs.service.fraud.impl.TargetCsFraudServiceImpl;
import au.com.target.tgtcs.widgets.controllers.impl.TargetHoldFraudStatusWidgetController;


/**
 * @author mjanarth
 * 
 */

@UnitTest
public class TargetHoldFraudStatusWidgetControllerTest {

    @Mock
    private TargetCsFraudServiceImpl targetFraudService;
    @Mock
    private ModelService modelService;
    @Mock
    private OrderModel orderModel;
    @Spy
    @InjectMocks
    private final TargetHoldFraudStatusWidgetController controller = new TargetHoldFraudStatusWidgetController();

    @Before
    public void prepare() {
        MockitoAnnotations.initMocks(this);
        doReturn(orderModel).when(controller).getSelectedOrder();
    }

    @Test
    public void testChangeOrderWithOrderStatusReview() {
        BDDMockito.when(controller.getSelectedOrder()).thenReturn(orderModel);
        BDDMockito.when(orderModel.getStatus()).thenReturn(OrderStatus.REVIEW);
        controller.changeOrder("test");
        Mockito.verify(modelService, Mockito.times(1)).refresh(orderModel);
        Mockito.verify(modelService, Mockito.times(1)).save(orderModel);
        Mockito.verify(targetFraudService, Mockito.atLeastOnce()).addFraudReportToOrder(orderModel, FraudStatus.CHECK,
                "test");
    }

    @Test
    public void testChangeOrderWithOrderStatusReviewHold() {
        BDDMockito.when(controller.getSelectedOrder()).thenReturn(orderModel);
        BDDMockito.when(orderModel.getStatus()).thenReturn(OrderStatus.REVIEW_ON_HOLD);
        controller.changeOrder("test");
        Mockito.verify(modelService, Mockito.never()).refresh(orderModel);
        Mockito.verify(modelService, Mockito.never()).save(orderModel);
        Mockito.verify(targetFraudService, Mockito.never()).addFraudReportToOrder(orderModel, FraudStatus.CHECK,
                "test");
    }

    @Test
    public void testChangeOrderWithOrderStatusApproved() {
        BDDMockito.when(controller.getSelectedOrder()).thenReturn(orderModel);
        BDDMockito.when(orderModel.getStatus()).thenReturn(OrderStatus.CREATED);
        controller.changeOrder("test");
        Mockito.verify(modelService, Mockito.never()).refresh(orderModel);
        Mockito.verify(modelService, Mockito.never()).save(orderModel);
        Mockito.verify(targetFraudService, Mockito.never()).addFraudReportToOrder(orderModel, FraudStatus.CHECK,
                "test");
    }

    @Test
    public void testChangeOrderWithOrderStatusRejected() {
        BDDMockito.when(controller.getSelectedOrder()).thenReturn(orderModel);
        BDDMockito.when(orderModel.getStatus()).thenReturn(OrderStatus.REJECTED);
        controller.changeOrder("test");
        Mockito.verify(modelService, Mockito.never()).refresh(orderModel);
        Mockito.verify(modelService, Mockito.never()).save(orderModel);
        Mockito.verify(targetFraudService, Mockito.never()).addFraudReportToOrder(orderModel, FraudStatus.CHECK,
                "test");
    }

}

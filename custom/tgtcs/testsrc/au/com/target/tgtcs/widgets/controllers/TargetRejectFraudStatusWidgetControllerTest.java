/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers;

import static org.mockito.Mockito.doReturn;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.exceptions.OrderCancelRecordsHandlerException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import au.com.target.tgtcs.service.fraud.impl.TargetCsFraudServiceImpl;
import au.com.target.tgtcs.widgets.controllers.impl.TargetRejectFraudStatusWidgetController;
import au.com.target.tgtfraud.ordercancel.RejectCancelService;


/**
 * @author mjanarth
 * 
 */
@UnitTest
public class TargetRejectFraudStatusWidgetControllerTest {
    @Mock
    private TargetCsFraudServiceImpl targetFraudService;
    @Mock
    private RejectCancelService rejectService;
    @Mock
    private OrderModel orderModel;
    @Spy
    @InjectMocks
    private final TargetRejectFraudStatusWidgetController controller = new TargetRejectFraudStatusWidgetController();

    @Before
    public void prepare() {
        MockitoAnnotations.initMocks(this);
        doReturn(orderModel).when(controller).getSelectedOrder();
    }

    @Test
    public void testChangeOrderWithOrderStatusReview() throws OrderCancelRecordsHandlerException, OrderCancelException {
        BDDMockito.when(controller.getSelectedOrder()).thenReturn(orderModel);
        BDDMockito.when(orderModel.getStatus()).thenReturn(OrderStatus.REVIEW);
        controller.changeOrder("test");
        Mockito.verify(rejectService, Mockito.times(1)).startRejectCancelProcess(orderModel);
        Mockito.verify(targetFraudService, Mockito.atLeastOnce()).addFraudReportToOrder(orderModel, FraudStatus.FRAUD,
                "test");
    }

    @Test
    public void testChangeOrderWithOrderStatusReviewOnHold() throws OrderCancelRecordsHandlerException,
            OrderCancelException {
        BDDMockito.when(controller.getSelectedOrder()).thenReturn(orderModel);
        BDDMockito.when(orderModel.getStatus()).thenReturn(OrderStatus.REVIEW_ON_HOLD);
        controller.changeOrder("test");
        Mockito.verify(rejectService, Mockito.times(1)).startRejectCancelProcess(orderModel);
        Mockito.verify(targetFraudService, Mockito.atLeastOnce()).addFraudReportToOrder(orderModel, FraudStatus.FRAUD,
                "test");
    }

    @Test
    public void testChangeOrderWithOrderStatusApproved() throws OrderCancelRecordsHandlerException,
            OrderCancelException {
        BDDMockito.when(controller.getSelectedOrder()).thenReturn(orderModel);
        BDDMockito.when(orderModel.getStatus()).thenReturn(OrderStatus.CREATED);
        controller.changeOrder("test");
        Mockito.verify(rejectService, Mockito.never()).startRejectCancelProcess(orderModel);
        Mockito.verify(targetFraudService, Mockito.never()).addFraudReportToOrder(orderModel, FraudStatus.FRAUD,
                "test");
    }

    @Test
    public void testChangeOrderWithOrderStatusRejected() throws OrderCancelRecordsHandlerException,
            OrderCancelException {
        BDDMockito.when(controller.getSelectedOrder()).thenReturn(orderModel);
        BDDMockito.when(orderModel.getStatus()).thenReturn(OrderStatus.REJECTED);
        controller.changeOrder("test");
        Mockito.verify(rejectService, Mockito.never()).startRejectCancelProcess(orderModel);
        Mockito.verify(targetFraudService, Mockito.never()).addFraudReportToOrder(orderModel, FraudStatus.FRAUD,
                "test");
    }
}

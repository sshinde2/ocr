/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.verification.VerificationMode;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcs.service.fraud.impl.TargetCsFraudServiceImpl;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;


/**
 * @author mjanarth
 * 
 */
@UnitTest
public class TargetApproveFraudStatusWidgetControllerTest {

    @Mock
    private TargetCsFraudServiceImpl targetFraudService;
    @Mock
    private TargetBusinessProcessService businessService;
    @Mock
    private OrderModel orderModel;
    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Spy
    @InjectMocks
    private final TargetApproveFraudStatusWidgetController controller = new TargetApproveFraudStatusWidgetController();

    @Before
    public void prepare() {
        MockitoAnnotations.initMocks(this);
        doReturn(orderModel).when(controller).getSelectedOrder();
    }

    @Test
    public void testChangeOrderWhenStatusIsReview() {
        given(controller.getSelectedOrder()).willReturn(orderModel);
        given(orderModel.getStatus()).willReturn(OrderStatus.REVIEW);
        controller.changeOrder("test");
        verify(businessService).startOrderAcceptProcess(orderModel);
        verify(targetFraudService, atLeastOnce()).addFraudReportToOrder(orderModel, FraudStatus.OK,
                "test");
    }

    @Test
    public void testChangeOrderWhenStatusIsReviewOnHold() {
        given(controller.getSelectedOrder()).willReturn(orderModel);
        given(orderModel.getStatus()).willReturn(OrderStatus.REVIEW_ON_HOLD);
        controller.changeOrder("test");
        verify(businessService).startOrderAcceptProcess(orderModel);
        verify(targetFraudService, atLeastOnce()).addFraudReportToOrder(orderModel, FraudStatus.OK,
                "test");
    }

    @Test
    public void testChangeOrderWhenStatusIsCreated() {
        given(controller.getSelectedOrder()).willReturn(orderModel);
        given(orderModel.getStatus()).willReturn(OrderStatus.CREATED);
        controller.changeOrder("test");
        verify(businessService, never()).startOrderAcceptProcess(orderModel);
        verify(targetFraudService, never()).addFraudReportToOrder(orderModel, FraudStatus.OK,
                "test");
    }

    @Test
    public void testChangeOrderWhenStatusIsRejected() {
        given(controller.getSelectedOrder()).willReturn(orderModel);
        given(orderModel.getStatus()).willReturn(OrderStatus.REJECTED);
        controller.changeOrder("test");
        verify(businessService, never()).startOrderAcceptProcess(orderModel);
        verify(targetFraudService, never()).addFraudReportToOrder(orderModel, FraudStatus.OK,
                "test");
    }

    @Test
    public void testChangeOrderWhenFeatureSwitchOn() {
        given(controller.getSelectedOrder()).willReturn(orderModel);
        given(orderModel.getStatus()).willReturn(OrderStatus.REVIEW);
        given(orderModel.getFluentId()).willReturn("123");
        given(Boolean.valueOf(targetFeatureSwitchFacade.isFluentEnabled())).willReturn(Boolean.TRUE);
        controller.changeOrder("test");
        verify(businessService).startFluentOrderAcceptProcess(orderModel);
        verifyNoMoreInteractions(businessService);
    }

    /**
     * Method to verify execution of Accept Pre-Order Business Process. Here Pre-order order status is REVIEW.
     */
    @Test
    public void testChangeOrderWhenPreOrderStatusIsReview() {

        verifyExecutionOfBusinessProcesses(OrderStatus.REVIEW, Boolean.TRUE, never(), atLeastOnce(),
                times(1), "testPreOrder");
    }

    /**
     * Method to verify execution of Accept Pre-Order Business Process. Here Pre-order order status is REJECTED. Ideally
     * none of the business processes will get called when order status other than REVIEW and/or REVIEW_ON_HOLD.
     */
    @Test
    public void testChangeOrderWhenPreOrderStatusOtherThanReview() {

        verifyExecutionOfBusinessProcesses(OrderStatus.REJECTED, Boolean.TRUE, never(), never(),
                never(), "testPreOrder");
    }

    /**
     * Method to verify execution of Accept Pre-Order Business Process. Here order status is REVIEW. Ideally accept
     * pre-order business process will never get called when order is not pre-order item.
     */
    @Test
    public void testChangeOrderWhenOrderStatusIsReviewAndItemIsNotPreOrderItem() {

        verifyExecutionOfBusinessProcesses(OrderStatus.REVIEW, Boolean.FALSE, times(1), atLeastOnce(),
                never(), "testOrder");
    }

    @Test
    public void testChangeOrderWhenFluentFeatureSwitchOnForPreOrder() {
        given(orderModel.getContainsPreOrderItems()).willReturn(Boolean.TRUE);
        given(orderModel.getFluentId()).willReturn("123");
        given(Boolean.valueOf(targetFeatureSwitchFacade.isFluentEnabled())).willReturn(Boolean.TRUE);
        given(orderModel.getStatus()).willReturn(OrderStatus.REVIEW);
        controller.changeOrder("testFluentPreOrder");
        verify(businessService).startFluentAcceptPreOrderProcess(orderModel);
        verifyNoMoreInteractions(businessService);
    }

    @Test
    public void testChangeOrderWhenFluentFeatureSwitchOffForPreOrder() {
        given(orderModel.getContainsPreOrderItems()).willReturn(Boolean.TRUE);
        given(Boolean.valueOf(targetFeatureSwitchFacade.isFluentEnabled())).willReturn(Boolean.FALSE);
        given(orderModel.getStatus()).willReturn(OrderStatus.REVIEW);
        controller.changeOrder("testFluentPreOrder");
        verify(businessService).startPreOrderAcceptProcess(orderModel);
        verifyNoMoreInteractions(businessService);
    }


    @Test
    public void testChangeOrderWhenFluentFeatureSwitchOnForPreOrderRelease() {
        given(orderModel.getContainsPreOrderItems()).willReturn(Boolean.FALSE);
        given(orderModel.getFluentId()).willReturn("123");
        given(Boolean.valueOf(targetFeatureSwitchFacade.isFluentEnabled())).willReturn(Boolean.TRUE);
        given(orderModel.getStatus()).willReturn(OrderStatus.REVIEW);
        final Date salesDateMock = mock(Date.class);
        given(orderModel.getNormalSaleStartDateTime()).willReturn(salesDateMock);

        controller.changeOrder("testFluentPreOrder");
        verify(businessService).startFluentPreOrderReleaseAcceptProcess(orderModel);
        verifyNoMoreInteractions(businessService);
    }

    /**
     * Method to verify execution of business processes based on order status(Review / Reject etc) and order
     * type(pre-order or normal product)
     * 
     * @param orderStatus
     * @param isPreOrderItems
     * @param startOrderAcceptProcessMode
     * @param targetCsFraudServiceMode
     * @param startPreOrderAcceptProcessMode
     */
    private void verifyExecutionOfBusinessProcesses(final OrderStatus orderStatus, final Boolean isPreOrderItems,
            final VerificationMode startOrderAcceptProcessMode, final VerificationMode targetCsFraudServiceMode,
            final VerificationMode startPreOrderAcceptProcessMode, final String note) {

        given(controller.getSelectedOrder()).willReturn(orderModel);
        given(orderModel.getStatus()).willReturn(orderStatus);
        given(orderModel.getContainsPreOrderItems()).willReturn(isPreOrderItems);

        controller.changeOrder(note);

        verify(businessService, startOrderAcceptProcessMode).startOrderAcceptProcess(orderModel);

        verify(targetFraudService, targetCsFraudServiceMode).addFraudReportToOrder(orderModel, FraudStatus.OK,
                note);
        verify(businessService, startPreOrderAcceptProcessMode).startPreOrderAcceptProcess(orderModel);
    }
}

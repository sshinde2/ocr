/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.cscockpit.exceptions.ValidationException;
import de.hybris.platform.cscockpit.services.checkout.CsCheckoutService;
import de.hybris.platform.cscockpit.services.payment.CsCardPaymentService;
import de.hybris.platform.cscockpit.widgets.controllers.CallContextController;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.interceptor.Interceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.validation.interceptors.ValidationInterceptor;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.zkoss.spring.SpringUtil;
import org.zkoss.zk.ui.Execution;

import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcs.widgets.controllers.TargetBasketController;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.order.TargetLaybyCommerceCheckoutService;
import au.com.target.tgtpayment.commands.request.TargetQueryTransactionDetailsRequest;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.methods.TargetIpgPaymentMethod;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.service.PaymentsInProgressService;
import au.com.target.tgtpayment.service.TargetPaymentService;


/**
 * 
 * Unit Test for {@link TargetCheckoutControllerImpl}
 * 
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Registry.class, SpringUtil.class })
@PowerMockIgnore("javax.management.*")
public class TargetCheckoutControllerImplTest {

    private static final String SESSION_ID = "SessionId";

    private static final String SST = "SST";

    private CartModel masterCartModel;

    @Mock
    private PurchaseOptionModel purchaseOption;
    @Mock
    private TargetBasketController targetBasketController;
    @Mock
    private TargetLaybyCartService targetLaybyCartService;
    @Mock
    private TargetLaybyCommerceCheckoutService targetLaybyCommerceCheckoutService;
    @Mock
    private DefaultModelService modelService;
    @Mock
    private CallContextController callContextController;
    @Mock
    private TargetPaymentService targetPaymentService;
    @Mock
    private CommonI18NService commonI18NService;
    @Mock
    private CsCheckoutService csCheckoutService;
    @Mock
    private CsCardPaymentService csCardPaymentService;
    @Mock
    private PaymentModeService paymentModeService;
    @Mock
    private PaymentsInProgressService paymentsInProgressService;
    @Mock
    private ImpersonationService impersonationService;
    @Mock
    private CreditCardPaymentInfoModel cardPaymentInfo;
    @Mock
    private SessionService sessionService;
    @Mock
    private TargetIpgPaymentMethod paymentMethod;

    private final TargetCheckoutControllerImpl targetCheckoutControllerImpl = new TargetCheckoutControllerImpl() {
        /*
         * (non-Javadoc)
         * @see de.hybris.platform.cscockpit.widgets.controllers.impl.AbstractCsWidgetController#getModelService()
         */
        @Override
        public ModelService getModelService() {
            return modelService;
        }

    };

    private final TargetCheckoutControllerImpl targetControllerImpl = new TargetCheckoutControllerImpl() {
        @Override
        public void saveFlyBuysCode(final String flyBuysCode) throws ValueHandlerException {
            // Do nothing because do not need to care in this case
        }
    };

    /**
     * @throws java.lang.Exception
     *             exception
     */
    @SuppressWarnings("boxing")
    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(Registry.class, Mockito.RETURNS_MOCKS);
        PowerMockito.mockStatic(SpringUtil.class, Mockito.RETURNS_MOCKS);
        MockitoAnnotations.initMocks(this);
        masterCartModel = new CartModel();

        final I18NService i18Service = mock(I18NService.class);
        final TargetAddressModel targetAddressModel = mock(TargetAddressModel.class);
        final MessageSource messageSource = mock(MessageSource.class);
        final CurrencyModel currencyModel = mock(CurrencyModel.class);

        final UserModel userModel = mock(UserModel.class);

        given(userModel.getUid()).willReturn("test");
        final int round = 2;
        given(currencyModel.getDigits()).willReturn(Integer.valueOf(round));

        given(csCheckoutService.getUnauthorizedTotal(masterCartModel)).willReturn(
                new Double("100"));
        given(targetPaymentService.tokenize(Mockito.anyString(), Mockito.any(PaymentModeModel.class))).willReturn(
                "token");
        given(modelService.create(CreditCardPaymentInfoModel.class)).willReturn(cardPaymentInfo);
        given(modelService.create(TargetAddressModel.class)).willReturn(targetAddressModel);
        given(SpringUtil.getBean("messageSource")).willReturn(messageSource);
        given(SpringUtil.getBean("i18nService")).willReturn(i18Service);

        BDDMockito.when(targetBasketController.getPurchaseOptionModel()).thenReturn(purchaseOption);
        BDDMockito.when(targetBasketController.getMasterCart()).thenReturn(masterCartModel);

        masterCartModel.setUser(userModel);
        masterCartModel.setCurrency(currencyModel);

        targetCheckoutControllerImpl.setBasketController(targetBasketController);
        targetCheckoutControllerImpl.setCommonI18NService(commonI18NService);
        targetCheckoutControllerImpl.setModelService(modelService);
        targetCheckoutControllerImpl.setCsCardPaymentService(csCardPaymentService);
        targetCheckoutControllerImpl.setCsCheckoutService(csCheckoutService);
        targetCheckoutControllerImpl.setTargetPaymentService(targetPaymentService);
        targetCheckoutControllerImpl.setPaymentModeService(paymentModeService);
        targetCheckoutControllerImpl.setImpersonationService(impersonationService);
        targetCheckoutControllerImpl.setTargetLaybyCartService(targetLaybyCartService);
        targetCheckoutControllerImpl.setTargetLaybyCommerceCheckoutService(targetLaybyCommerceCheckoutService);

        targetControllerImpl.setBasketController(targetBasketController);
        targetControllerImpl.setTargetLaybyCartService(targetLaybyCartService);
        targetControllerImpl.setTargetLaybyCommerceCheckoutService(targetLaybyCommerceCheckoutService);

    }

    /**
     * Save valid fly buys code successfully
     * 
     * @throws ValueHandlerException
     *             valueHandlerException
     */
    @Test
    public void testSaveValidFlyBuysCode() throws ValueHandlerException {
        final String flybuy = "2793218616917";
        targetCheckoutControllerImpl.setModelService(modelService);
        // mock valid fly buys code

        BDDMockito.given(
                Boolean.valueOf(targetLaybyCommerceCheckoutService.setFlybuysNumber(masterCartModel, flybuy)))
                .willReturn(Boolean.TRUE);

        targetCheckoutControllerImpl.saveFlyBuysCode("2793218616917");
        Mockito.verify(targetLaybyCommerceCheckoutService).setFlybuysNumber(masterCartModel, flybuy);
    }

    /**
     * Error saving case
     * 
     * @throws ValueHandlerException
     *             valueHandlerException
     */
    @Test(expected = ValueHandlerException.class)
    public void testSaveFlyBuysCodeFail() throws ValueHandlerException {

        final InterceptorException interceptorException = new InterceptorException("Error message");
        final Interceptor interceptor = new ValidationInterceptor();
        interceptorException.setInterceptor(interceptor);
        final Throwable exception = new ModelSavingException("Save Error!", interceptorException);
        Mockito.doThrow(exception).when(modelService).save(masterCartModel);

        targetCheckoutControllerImpl.saveFlyBuysCode("FlyBuysCode");

        Assert.assertTrue("Not match exception", "Error message".equals(exception.getMessage()));
    }

    /**
     * Save invalid code case. Checkout cart code will be set empty
     * 
     * @throws ValueHandlerException
     *             valueHandlerException
     */
    @Test
    public void testInvalidFlyBuysCode() throws ValueHandlerException {
        final String flybuy = "InvalidCode";
        // mock invalid fly buys code
        BDDMockito.given(
                Boolean.valueOf(targetLaybyCommerceCheckoutService.setFlybuysNumber(masterCartModel, flybuy)))
                .willReturn(Boolean.FALSE);
        try {
            targetCheckoutControllerImpl.saveFlyBuysCode(flybuy);
            Assert.fail("method must throw exception");
        }
        catch (final ValueHandlerException ex) {
            Assert.assertTrue(("Invalid flybuys number " + "InvalidCode").equals(ex.getMessage()));
        }
    }

    /**
     * Get default fly buy code from cart
     * 
     * @throws ValueHandlerException
     *             valueHandlerException
     */
    @Test
    public void testPreLoadFlyBuysCodeFromCart() throws ValueHandlerException {
        masterCartModel.setFlyBuysCode("2793218616917");
        final String flyBuysCode = targetCheckoutControllerImpl.preLoadFlyBuysCode();
        Assert.assertTrue("2793218616917".equals(flyBuysCode));
    }

    /**
     * Get default fly buy code from customer. Invalid code return empty case
     * 
     * @throws ValueHandlerException
     *             valueHandlerException
     */
    @Test
    public void testPreLoadFlyBuysCodeInValidCustomerCode() throws ValueHandlerException {
        // Use implementation mock saveFlyBuysCode method
        final TargetCustomerModel customerModel = new TargetCustomerModel();
        customerModel.setFlyBuysCode("InvalidCode");
        final TypedObject customerObject = BDDMockito.mock(TypedObject.class);

        BDDMockito.given(customerObject.getObject()).willReturn(customerModel);
        targetControllerImpl.setCallContextController(callContextController);
        BDDMockito.given(callContextController.getCurrentCustomer()).willReturn(customerObject);

        // mock valid fly buys code
        BDDMockito.given(Boolean.valueOf(targetLaybyCommerceCheckoutService.isFlybuysNumberValid(masterCartModel)))
                .willReturn(Boolean.FALSE);

        masterCartModel.setFlyBuysCode(null);

        final String flyBuysCode = targetControllerImpl.preLoadFlyBuysCode();
        Assert.assertTrue("".equals(flyBuysCode));
    }

    /**
     * Get default fly buy code from valid code of customer
     * 
     * @throws ValueHandlerException
     *             valueHandlerException
     */
    @Test
    public void testPreLoadFlyBuysCodeFromValidCodeCustomerCode() throws ValueHandlerException {
        // Use implementation mock saveFlyBuysCode method
        final TargetCustomerModel customerModel = new TargetCustomerModel();
        customerModel.setFlyBuysCode("2793218616917");
        final TypedObject customerObject = BDDMockito.mock(TypedObject.class);

        BDDMockito.given(customerObject.getObject()).willReturn(customerModel);
        targetControllerImpl.setCallContextController(callContextController);
        BDDMockito.given(callContextController.getCurrentCustomer()).willReturn(customerObject);

        // mock valid fly buys code
        BDDMockito.given(Boolean.valueOf(targetLaybyCommerceCheckoutService.isFlybuysNumberValid(masterCartModel)))
                .willReturn(Boolean.TRUE);

        masterCartModel.setFlyBuysCode(null);

        final String flyBuysCode = targetControllerImpl.preLoadFlyBuysCode();
        Assert.assertTrue("2793218616917".equals(flyBuysCode));
    }

    /**
     * 
     */
    @SuppressWarnings("boxing")
    @Test
    public void testhandlePaymentFormResponseSuccess() {
        final Execution execution = mock(Execution.class);
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);

        given(execution.getParameter("paymentType")).willReturn("0");
        given(execution.getParameter("amount")).willReturn("100");
        given(execution.getParameter("nameOnCard")).willReturn("Test test");
        given(execution.getParameter("cardType")).willReturn("Visa");
        given(execution.getParameter("gatewayCardScheme")).willReturn("1");
        given(execution.getParameter("gatewayCardNumber")).willReturn("1");
        given(execution.getParameter("gatewayCardExpiryDateMonth")).willReturn("1");
        given(execution.getParameter("gatewayCardExpiryDateYear")).willReturn("1");
        given(execution.getParameter("gatewayCardSecurityCode")).willReturn("1");
        given(execution.getParameter("firstName")).willReturn("Firstname");
        given(execution.getParameter("lastName")).willReturn("Lastname");
        given(execution.getParameter("street1")).willReturn("street1");
        given(execution.getParameter("street2")).willReturn("street2");
        given(execution.getParameter("city")).willReturn("city");
        given(execution.getParameter("postalCode")).willReturn("postalcode");
        given(execution.getParameter("country")).willReturn("country");
        given(execution.getParameter("state")).willReturn("state");
        given(execution.getParameter("phoneNumber")).willReturn("phone");
        given(execution.getParameter("gatewayFormResponse")).willReturn("1");
        given(targetPaymentService.capture(Mockito.any(AbstractOrderModel.class), Mockito.any(PaymentInfoModel.class),
                Mockito.any(BigDecimal.class), Mockito.any(CurrencyModel.class), Mockito.any(PaymentCaptureType.class)))
                .willReturn(paymentTransactionEntryModel);
        given(commonI18NService.roundCurrency(Mockito.anyDouble(), Mockito.anyInt())).willReturn(100.00);

        final List<String> result = targetCheckoutControllerImpl.handlePaymentFormResponse(execution);

        assertNotNull("Expecting a result", result);
        assertEquals("Expected size result", 2, result.size());
        final Iterator<String> it = result.iterator();
        assertEquals("Expected value", "success", it.next());
        assertTrue("expected true", it.next().startsWith("New payment option is added successfully."));
    }

    /**
     * 
     */
    @SuppressWarnings("boxing")
    @Test
    public void testhandlePaymentFormResponseError() {
        final Execution execution = mock(Execution.class);
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);

        given(execution.getParameter("paymentType")).willReturn("1");
        given(execution.getParameter("amount")).willReturn("100");
        given(execution.getParameter("nameOnCard")).willReturn("Test test");
        given(execution.getParameter("cardType")).willReturn("Visa");
        given(execution.getParameter("gatewayCardScheme")).willReturn("1");
        given(execution.getParameter("gatewayCardNumber")).willReturn("1");
        given(execution.getParameter("gatewayCardExpiryDateMonth")).willReturn("1");
        given(execution.getParameter("gatewayCardExpiryDateYear")).willReturn("1");
        given(execution.getParameter("gatewayCardSecurityCode")).willReturn("1");
        given(execution.getParameter("firstName")).willReturn("Firstname");
        given(execution.getParameter("lastName")).willReturn("Lastname");
        given(execution.getParameter("street1")).willReturn("street1");
        given(execution.getParameter("street2")).willReturn("street2");
        given(execution.getParameter("city")).willReturn("city");
        given(execution.getParameter("postalCode")).willReturn("postalcode");
        given(execution.getParameter("country")).willReturn("country");
        given(execution.getParameter("state")).willReturn("state");
        given(execution.getParameter("phoneNumber")).willReturn("phone");
        given(execution.getParameter("gatewayFormResponse")).willReturn("1");
        given(targetPaymentService.capture(Mockito.any(AbstractOrderModel.class), Mockito.any(PaymentInfoModel.class),
                Mockito.any(BigDecimal.class), Mockito.any(CurrencyModel.class), Mockito.any(PaymentCaptureType.class)))
                .willReturn(paymentTransactionEntryModel);
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(TransactionStatus.ERROR.toString());
        given(commonI18NService.roundCurrency(Mockito.anyDouble(), Mockito.anyInt())).willReturn(100.00);

        final List<String> result = targetCheckoutControllerImpl.handlePaymentFormResponse(execution);

        assertNotNull("Expecting a result", result);
        assertEquals("Expected size result", 2, result.size());
        final Iterator<String> it = result.iterator();
        assertEquals("Expected value", "failure", it.next());
        assertEquals("Expected value", "Failed to capture, Error happens with transaction", it.next());
    }


    /**
     * 
     */
    @SuppressWarnings("boxing")
    @Test
    public void testhandlePaymentFormResponseReview() {
        final Execution execution = mock(Execution.class);
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);

        given(execution.getParameter("paymentType")).willReturn("1");
        given(execution.getParameter("amount")).willReturn("100");
        given(execution.getParameter("nameOnCard")).willReturn("Test test");
        given(execution.getParameter("cardType")).willReturn("Visa");
        given(execution.getParameter("gatewayCardScheme")).willReturn("1");
        given(execution.getParameter("gatewayCardNumber")).willReturn("1");
        given(execution.getParameter("gatewayCardExpiryDateMonth")).willReturn("1");
        given(execution.getParameter("gatewayCardExpiryDateYear")).willReturn("1");
        given(execution.getParameter("gatewayCardSecurityCode")).willReturn("1");
        given(execution.getParameter("firstName")).willReturn("Firstname");
        given(execution.getParameter("lastName")).willReturn("Lastname");
        given(execution.getParameter("street1")).willReturn("street1");
        given(execution.getParameter("street2")).willReturn("street2");
        given(execution.getParameter("city")).willReturn("city");
        given(execution.getParameter("postalCode")).willReturn("postalcode");
        given(execution.getParameter("country")).willReturn("country");
        given(execution.getParameter("state")).willReturn("state");
        given(execution.getParameter("phoneNumber")).willReturn("phone");
        given(execution.getParameter("gatewayFormResponse")).willReturn("1");
        given(targetPaymentService.capture(Mockito.any(AbstractOrderModel.class), Mockito.any(PaymentInfoModel.class),
                Mockito.any(BigDecimal.class), Mockito.any(CurrencyModel.class), Mockito.any(PaymentCaptureType.class)))
                .willReturn(paymentTransactionEntryModel);
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(TransactionStatus.REVIEW.toString());
        given(commonI18NService.roundCurrency(Mockito.anyDouble(), Mockito.anyInt())).willReturn(100.00);

        final List<String> result = targetCheckoutControllerImpl.handlePaymentFormResponse(execution);

        assertNotNull("Expecting a result", result);
        assertEquals("Expected size result", 2, result.size());
        final Iterator<String> it = result.iterator();
        assertEquals("Expected value", "success", it.next());
        assertEquals("Expected value", "transaction went though and it is under Review status now", it.next());
    }

    /**
     * 
     */
    @SuppressWarnings("boxing")
    @Test
    public void testhandlePaymentFormResponseReject() {
        final Execution execution = mock(Execution.class);
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);

        given(execution.getParameter("paymentType")).willReturn("1");
        given(execution.getParameter("amount")).willReturn("100");
        given(execution.getParameter("nameOnCard")).willReturn("Test test");
        given(execution.getParameter("cardType")).willReturn("Visa");
        given(execution.getParameter("gatewayCardScheme")).willReturn("1");
        given(execution.getParameter("gatewayCardNumber")).willReturn("1");
        given(execution.getParameter("gatewayCardExpiryDateMonth")).willReturn("1");
        given(execution.getParameter("gatewayCardExpiryDateYear")).willReturn("1");
        given(execution.getParameter("gatewayCardSecurityCode")).willReturn("1");
        given(execution.getParameter("firstName")).willReturn("Firstname");
        given(execution.getParameter("lastName")).willReturn("Lastname");
        given(execution.getParameter("street1")).willReturn("street1");
        given(execution.getParameter("street2")).willReturn("street2");
        given(execution.getParameter("city")).willReturn("city");
        given(execution.getParameter("postalCode")).willReturn("postalcode");
        given(execution.getParameter("country")).willReturn("country");
        given(execution.getParameter("state")).willReturn("state");
        given(execution.getParameter("phoneNumber")).willReturn("phone");
        given(execution.getParameter("gatewayFormResponse")).willReturn("1");
        given(targetPaymentService.capture(Mockito.any(AbstractOrderModel.class), Mockito.any(PaymentInfoModel.class),
                Mockito.any(BigDecimal.class), Mockito.any(CurrencyModel.class), Mockito.any(PaymentCaptureType.class)))
                .willReturn(paymentTransactionEntryModel);
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(TransactionStatus.REJECTED.toString());
        given(commonI18NService.roundCurrency(Mockito.anyDouble(), Mockito.anyInt())).willReturn(100.00);

        final List<String> result = targetCheckoutControllerImpl.handlePaymentFormResponse(execution);

        assertNotNull("Expecting a result", result);
        assertEquals("Expected size result", 2, result.size());
        final Iterator<String> it = result.iterator();
        assertEquals("Expected value", "failure", it.next());
        assertEquals("Expected value", "Failed to capture, transaction is rejected by TNS payment provider", it.next());
    }

    /**
     * 
     */
    @SuppressWarnings("boxing")
    @Test
    public void testhandlePaymentFormTokenize() {
        final Execution execution = mock(Execution.class);
        final PaymentTransactionEntryModel paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);

        given(execution.getParameter("paymentType")).willReturn("0");
        given(execution.getParameter("amount")).willReturn("100");
        given(execution.getParameter("nameOnCard")).willReturn("Test test");
        given(execution.getParameter("cardType")).willReturn("Visa");
        given(execution.getParameter("gatewayCardScheme")).willReturn("1");
        given(execution.getParameter("gatewayCardNumber")).willReturn("1");
        given(execution.getParameter("gatewayCardExpiryDateMonth")).willReturn("1");
        given(execution.getParameter("gatewayCardExpiryDateYear")).willReturn("1");
        given(execution.getParameter("gatewayCardSecurityCode")).willReturn("1");
        given(execution.getParameter("firstName")).willReturn("Firstname");
        given(execution.getParameter("lastName")).willReturn("Lastname");
        given(execution.getParameter("street1")).willReturn("street1");
        given(execution.getParameter("street2")).willReturn("street2");
        given(execution.getParameter("city")).willReturn("city");
        given(execution.getParameter("postalCode")).willReturn("postalcode");
        given(execution.getParameter("country")).willReturn("country");
        given(execution.getParameter("state")).willReturn("state");
        given(execution.getParameter("phoneNumber")).willReturn("phone");
        given(execution.getParameter("gatewayFormResponse")).willReturn("1");
        given(targetPaymentService.capture(Mockito.any(AbstractOrderModel.class), Mockito.any(PaymentInfoModel.class),
                Mockito.any(BigDecimal.class), Mockito.any(CurrencyModel.class), Mockito.any(PaymentCaptureType.class)))
                .willReturn(paymentTransactionEntryModel);
        given(commonI18NService.roundCurrency(Mockito.anyDouble(), Mockito.anyInt())).willReturn(100.00);

        final List<String> result = targetCheckoutControllerImpl.handlePaymentFormResponse(execution);

        assertNotNull("Expecting a result", result);
        assertEquals("Expected size result", 2, result.size());
        final Iterator<String> it = result.iterator();
        assertEquals("Expected value", "success", it.next());
        assertTrue("expected result", it.next().startsWith("New payment option is added successfully"));
    }

    /**
     * 
     */
    @SuppressWarnings("boxing")
    @Test
    public void testhandlePaymentFormNullResponse() {
        final Execution execution = mock(Execution.class);
        final PaymentTransactionEntryModel paymentTransactionEntryModel = null;

        given(execution.getParameter("paymentType")).willReturn("1");
        given(execution.getParameter("amount")).willReturn("100");
        given(execution.getParameter("nameOnCard")).willReturn("Test test");
        given(execution.getParameter("cardType")).willReturn("Visa");
        given(execution.getParameter("gatewayCardScheme")).willReturn("1");
        given(execution.getParameter("gatewayCardNumber")).willReturn("1");
        given(execution.getParameter("gatewayCardExpiryDateMonth")).willReturn("1");
        given(execution.getParameter("gatewayCardExpiryDateYear")).willReturn("1");
        given(execution.getParameter("gatewayCardSecurityCode")).willReturn("1");
        given(execution.getParameter("firstName")).willReturn("Firstname");
        given(execution.getParameter("lastName")).willReturn("Lastname");
        given(execution.getParameter("street1")).willReturn("street1");
        given(execution.getParameter("street2")).willReturn("street2");
        given(execution.getParameter("city")).willReturn("city");
        given(execution.getParameter("postalCode")).willReturn("postalcode");
        given(execution.getParameter("country")).willReturn("country");
        given(execution.getParameter("state")).willReturn("state");
        given(execution.getParameter("phoneNumber")).willReturn("phone");
        given(execution.getParameter("gatewayFormResponse")).willReturn("1");
        given(targetPaymentService.capture(Mockito.any(AbstractOrderModel.class), Mockito.any(PaymentInfoModel.class),
                Mockito.any(BigDecimal.class), Mockito.any(CurrencyModel.class), Mockito.any(PaymentCaptureType.class)))
                .willReturn(paymentTransactionEntryModel);
        given(commonI18NService.roundCurrency(Mockito.anyDouble(), Mockito.anyInt())).willReturn(100.00);

        final List<String> result = targetCheckoutControllerImpl.handlePaymentFormResponse(execution);

        assertNotNull("Expecting a result", result);
        assertEquals("Expected size result", 2, result.size());
        final Iterator<String> it = result.iterator();
        assertEquals("Expected value", "failure", it.next());
        assertEquals("Expected value", "payment failed: paymentTransactionModelEntry from payment service is null",
                it.next());
    }

    /**
     * Place order code validation
     * 
     * @throws ValidationException
     *             a validation exception
     * 
     */
    @Test
    public void testFailPlaceOrder() throws ValidationException {

        final AddressModel shippingAddress = BDDMockito.mock(AddressModel.class);
        final AddressModel paymentAddress = BDDMockito.mock(AddressModel.class);
        final DeliveryModeModel deliveryMode = BDDMockito.mock(DeliveryModeModel.class);

        final TargetCheckoutControllerImpl myImpl = BDDMockito.mock(TargetCheckoutControllerImpl.class);

        masterCartModel.setDeliveryAddress(shippingAddress);
        Assert.assertNotNull(masterCartModel.getDeliveryAddress());

        masterCartModel.setPaymentAddress(paymentAddress);
        Assert.assertNotNull(masterCartModel.getPaymentAddress());

        masterCartModel.setDeliveryMode(deliveryMode);
        Assert.assertNotNull(masterCartModel.getDeliveryMode());

        final OrderModel orderModel = (OrderModel)myImpl.placeOrder();
        // orderModel will be null as the cartModel in myImpl is null
        Assert.assertNull(orderModel);
    }

    @Test
    public void testPlaceOrderWithoutDeletingMasterCartEntries() throws ValidationException {
        given(
                impersonationService.executeInContext((ImpersonationContext)Mockito.any(),
                        (ImpersonationService.Executor<OrderModel, ValidationException>)Mockito.any())).willReturn(
                null);
        targetCheckoutControllerImpl.placeOrder();
        Mockito.verify(targetLaybyCartService, Mockito.times(0)).removeSessionCart();
    }

    @Test
    public void testPlaceOrderWithDeletingMasterCartEntries() throws ValidationException {
        final OrderModel orderModel = new OrderModel();
        given(
                impersonationService.executeInContext((ImpersonationContext)Mockito.any(),
                        (ImpersonationService.Executor<OrderModel, ValidationException>)Mockito.any())).willReturn(
                orderModel);
        targetCheckoutControllerImpl.placeOrder();
        Mockito.verify(targetLaybyCartService, Mockito.times(1)).removeSessionCart();
    }

    @Test
    public void testPlaceOrderWithIpgPaymentInfoModel() throws ValidationException {
        final IpgPaymentInfoModel ipgPaymentInfo = mock(IpgPaymentInfoModel.class);
        final UserModel user = new UserModel();
        user.setUid("uid");
        given(ipgPaymentInfo.getUser()).willReturn(user);
        masterCartModel.setUser(user);
        targetCheckoutControllerImpl.setCurrentPaymentInfo(ipgPaymentInfo);
        targetCheckoutControllerImpl.setCurrentPaymentModeCode("ipg");
        final PaymentModeModel paymentMode = mock(PaymentModeModel.class);
        given(paymentModeService.getPaymentModeForCode("ipg")).willReturn(paymentMode);
        final OrderModel orderModel = new OrderModel();
        given(
                impersonationService.executeInContext((ImpersonationContext)Mockito.any(),
                        (ImpersonationService.Executor<OrderModel, ValidationException>)Mockito.any())).willReturn(
                orderModel);
        targetCheckoutControllerImpl.placeOrder();
        Mockito.verify(paymentModeService).getPaymentModeForCode("ipg");
        Mockito.verify(targetLaybyCommerceCheckoutService).setPaymentMode(masterCartModel, paymentMode);
    }

    @Test
    public void testHandleIPGPaymentResponseSuccess() {
        final Execution execution = mock(Execution.class);
        targetCheckoutControllerImpl.setPaymentMethod(paymentMethod);
        targetCheckoutControllerImpl.setSessionService(sessionService);
        final AddressModel paymentAddress = new AddressModel();
        masterCartModel.setPaymentAddress(paymentAddress);
        given(execution.getParameter(SESSION_ID)).willReturn("asfsf-asfafaf-hdbhg-kdgkjdf");
        given(execution.getParameter(SST)).willReturn("asdasdd");
        final TargetQueryTransactionDetailsResult transactionResult = new TargetQueryTransactionDetailsResult();
        transactionResult.setSuccess(true);
        given(paymentMethod.queryTransactionDetails(Mockito.any(TargetQueryTransactionDetailsRequest.class)))
                .willReturn(transactionResult);
        final List<String> result = targetCheckoutControllerImpl.handleIPGPaymentFormResponse(execution);
        assertNotNull("expecting PaymentInfo", masterCartModel.getPaymentInfo());
        assertEquals("Expected size result", 1, result.size());
        assertTrue("success Result Expected", result.get(0).equals("success : Card payment reserved successfully"));
        assertTrue("asfsf-asfafaf-hdbhg-kdgkjdf".equals(((IpgPaymentInfoModel)masterCartModel.getPaymentInfo())
                .getSessionId()));
        assertEquals(paymentAddress, ((IpgPaymentInfoModel)masterCartModel.getPaymentInfo()).getBillingAddress());
        assertEquals(IpgPaymentTemplateType.CREDITCARDSINGLE,
                ((IpgPaymentInfoModel)masterCartModel.getPaymentInfo()).getIpgPaymentTemplateType());
    }

    @Test
    public void testHandleIPGPaymentResponseFailure() {
        final Execution execution = mock(Execution.class);
        targetCheckoutControllerImpl.setPaymentMethod(paymentMethod);
        targetCheckoutControllerImpl.setSessionService(sessionService);
        given(execution.getParameter(SESSION_ID)).willReturn("asfsf-asfafaf-hdbhg-kdgkjdf");
        given(execution.getParameter(SST)).willReturn("asdasdd");
        final TargetQueryTransactionDetailsResult transactionResult = new TargetQueryTransactionDetailsResult();
        transactionResult.setSuccess(false);
        given(paymentMethod.queryTransactionDetails(Mockito.any(TargetQueryTransactionDetailsRequest.class)))
                .willReturn(transactionResult);
        final List<String> result = targetCheckoutControllerImpl.handleIPGPaymentFormResponse(execution);
        assertNull("expecting Null PaymentInfo", masterCartModel.getPaymentInfo());
        assertEquals("Expected size result", 2, result.size());
        assertTrue("Failure Result Expected", result.get(0).equals("failure"));
    }

    @Test
    public void testHandleIPGPaymentResponseForException() {
        final Execution execution = mock(Execution.class);
        targetCheckoutControllerImpl.setPaymentMethod(paymentMethod);
        targetCheckoutControllerImpl.setSessionService(sessionService);
        given(execution.getParameter(SESSION_ID)).willReturn("asfsf-asfafaf-hdbhg-kdgkjdf");
        given(execution.getParameter(SST)).willReturn("asdasdd");
        final TargetQueryTransactionDetailsResult transactionResult = new TargetQueryTransactionDetailsResult();
        transactionResult.setSuccess(false);
        given(paymentMethod.queryTransactionDetails(Mockito.any(TargetQueryTransactionDetailsRequest.class)))
                .willThrow(new RuntimeException());
        final List<String> result = targetCheckoutControllerImpl.handleIPGPaymentFormResponse(execution);
        assertNull("expecting Null PaymentInfo", masterCartModel.getPaymentInfo());
        assertEquals("Expected size result", 2, result.size());
        assertTrue("Failure Result Expected", result.get(0).equals("failure"));
    }
}

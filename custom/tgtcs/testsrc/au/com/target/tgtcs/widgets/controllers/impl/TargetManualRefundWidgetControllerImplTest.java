/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers.impl;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.widgets.controllers.CallContextController;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtcore.ordercancel.TargetRefundPaymentService;
import au.com.target.tgtcore.ordercancel.data.IpgNewRefundInfoDTO;


/**
 * @author bhuang3
 *
 */
@UnitTest
public class TargetManualRefundWidgetControllerImplTest {

    @Mock
    private CallContextController callContextController;

    @Mock
    private FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    @Mock
    private TargetRefundPaymentService targetRefundPaymentService;

    @Mock
    private OrderModel orderModel;

    @Spy
    @InjectMocks
    private final TargetManualRefundWidgetControllerImpl controller = new TargetManualRefundWidgetControllerImpl();

    @Before
    public void prepare() {
        MockitoAnnotations.initMocks(this);
        doReturn(orderModel).when(controller).getSelectedOrder();
    }

    @Test
    public void testFindRefundAmount() {
        final Double paidSoFar = Double.valueOf(10.01d);
        final Double totalPrice = Double.valueOf(8.22d);
        when(orderModel.getTotalPrice()).thenReturn(totalPrice);
        when(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(orderModel)).thenReturn(paidSoFar);
        final BigDecimal result = controller.findRefundAmount(orderModel);
        assertEquals(
                BigDecimal.valueOf(paidSoFar.doubleValue() - totalPrice.doubleValue()).setScale(2,
                        RoundingMode.HALF_UP), result);
    }

    @Test
    public void testPerformIpgManualRefund() {
        final IpgNewRefundInfoDTO ipgNewRefundInfoDTO = Mockito.mock(IpgNewRefundInfoDTO.class);
        final BigDecimal refundAmount = Mockito.mock(BigDecimal.class);
        doReturn(refundAmount).when(controller).findRefundAmount(orderModel);
        controller.performIpgManualRefund(ipgNewRefundInfoDTO);
        Mockito.verify(targetRefundPaymentService)
                .performIpgManualRefund(orderModel, refundAmount, ipgNewRefundInfoDTO);
    }

}

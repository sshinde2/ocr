/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers.impl;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ReturnStatus;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.model.meta.impl.DefaultTypedObject;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.exceptions.ValidationException;
import de.hybris.platform.cscockpit.widgets.controllers.OrderManagementActionsWidgetController;
import de.hybris.platform.refund.RefundService;
import de.hybris.platform.returns.ReturnService;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcs.facade.TgtCsCardPaymentFacade;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetReturnsControllerTest {

    private static final Double OLD_ORDER_TOTAL_PRICE = Double.valueOf(100d);
    private static final Double NEW_ORDER_TOTAL_PRICE = Double.valueOf(80d);
    private static final Double SHIPPING_AMOUNT_TO_REFUND = Double.valueOf(14d);

    @InjectMocks
    @Spy
    private final TargetReturnsControllerImpl controller = new TargetReturnsControllerImpl();

    @Mock
    private ModelService modelService;

    @Mock
    private TypeService cockpitTypeService;

    @Mock
    private ReturnService returnService;

    @Mock
    private OrderManagementActionsWidgetController orderManagementActionsWidgetController;

    @Mock
    private ObjectValueContainer objectValueContainer;

    @Mock
    private OrderModel order;

    @Mock
    private OrderModel refundOrderPreview;

    @Mock
    private RefundService refundService;

    @Mock
    private TgtCsCardPaymentFacade tgtCsCardPaymentFacade;
    @Mock
    private ReturnRequestModel returnRequestModel;

    @Mock
    private Map returnableOrderEntries;

    @Mock
    private AbstractOrderEntryModel orderEntryModel;

    @Mock
    private DefaultTypedObject defaultTypedObject;

    @Mock
    private ObjectValueContainer.ObjectValueHolder objectValueHolder;


    private final List<ObjectValueContainer> objectValues = new ArrayList<>();



    @Before
    public void setup() {
        final TypedObject typedObject = mock(TypedObject.class);
        when(typedObject.getObject()).thenReturn(order);
        doReturn(typedObject).when(controller).getCurrentOrder();
        when(order.getEntries()).thenReturn(Collections.EMPTY_LIST);

        final TypedObject typedObjectForPreviewOrder = mock(TypedObject.class);
        when(typedObjectForPreviewOrder.getObject()).thenReturn(refundOrderPreview);
        doReturn(typedObjectForPreviewOrder).when(controller).getRefundOrderPreview();
        when(refundOrderPreview.getEntries()).thenReturn(Collections.EMPTY_LIST);
        when(refundOrderPreview.getOriginalVersion()).thenReturn(order);
    }

    @Test
    public void testCreateRefundRequest() {
        final ReturnRequestModel refundRequest = new ReturnRequestModel();
        final List<ReturnEntryModel> entries = new ArrayList<>();
        final ReturnEntryModel entryModel = new ReturnEntryModel();
        entries.add(entryModel);
        refundRequest.setReturnEntries(entries);

        given(returnService.createReturnRequest(order)).willReturn(refundRequest);
        given(returnService.createRMA(refundRequest)).willThrow(new RuntimeException());

        controller.createRefundRequest();

        Assert.assertEquals(ReturnStatus.CANCELED, entryModel.getStatus());
    }

    @Test
    public void testFillingRefundedAmount() {
        final ReturnRequestModel refundOrderPreviewRequest = new ReturnRequestModel();

        final PropertyDescriptor descriptor =
                mockPropertyDescriptorValue("ReturnRequest.shippingAmountToRefund", SHIPPING_AMOUNT_TO_REFUND);
        when(objectValueContainer.getPropertyDescriptors()).thenReturn(ImmutableSet.of(descriptor));
        controller.setAdditionalRequestParams(objectValueContainer);

        given(returnService.createReturnRequest(refundOrderPreview)).willReturn(refundOrderPreviewRequest);

        given(refundService.createRefundOrderPreview(order)).willReturn(refundOrderPreview);
        //for initialization refundDetailsList fields
        controller.createRefundOrderPreview(Collections.EMPTY_LIST);

        final ReturnRequestModel refundRequest = new ReturnRequestModel();
        given(order.getTotalPrice()).willReturn(OLD_ORDER_TOTAL_PRICE);
        given(refundOrderPreview.getTotalPrice()).willReturn(NEW_ORDER_TOTAL_PRICE);
        given(returnService.createReturnRequest(order)).willReturn(refundRequest);
        given(returnService.createRMA(refundRequest)).willReturn(StringUtils.EMPTY);

        controller.createRefundRequest();

        final double actualRefundedAmount = OLD_ORDER_TOTAL_PRICE.doubleValue() - NEW_ORDER_TOTAL_PRICE.doubleValue()
                + SHIPPING_AMOUNT_TO_REFUND.doubleValue();
        assertEquals(refundRequest.getRefundedAmount(), Double.valueOf(actualRefundedAmount));
    }

    @Test
    public void testFillingRefundedAmountWithNotNullOriginalVersion() {
        final ReturnRequestModel refundRequest = new ReturnRequestModel();
        controller.setRefundedAmount(refundOrderPreview, refundRequest);
        assertTrue(refundRequest.getRefundedAmount() == null);
    }

    @Test
    public void testFillingRefundedAmountWithNullOriginalVersion() {
        final ReturnRequestModel refundRequest = new ReturnRequestModel();

        refundRequest.setShippingAmountToRefund(SHIPPING_AMOUNT_TO_REFUND);
        given(order.getTotalPrice()).willReturn(OLD_ORDER_TOTAL_PRICE);
        given(refundOrderPreview.getTotalPrice()).willReturn(NEW_ORDER_TOTAL_PRICE);

        controller.setRefundedAmount(order, refundRequest);

        assertTrue(refundRequest.getRefundedAmount().doubleValue() > 0d);
    }

    @Test
    public void testFillingRefundedAmountWithDifferentValues() {
        final ReturnRequestModel refundRequest = new ReturnRequestModel();

        refundRequest.setShippingAmountToRefund(Double.valueOf(0d));
        given(order.getTotalPrice()).willReturn(Double.valueOf(15d));
        given(refundOrderPreview.getTotalPrice()).willReturn(Double.valueOf(10d));

        controller.setRefundedAmount(order, refundRequest);

        assertEquals(refundRequest.getRefundedAmount(), Double.valueOf(5d));

        refundRequest.setShippingAmountToRefund(Double.valueOf(5d));
        controller.setRefundedAmount(order, refundRequest);

        assertEquals(refundRequest.getRefundedAmount(), Double.valueOf(10d));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFillingRefundedAmountWithIncorrectData() {
        final ReturnRequestModel refundRequest = new ReturnRequestModel();
        given(refundOrderPreview.getTotalPrice()).willReturn(null);
        controller.setRefundedAmount(order, refundRequest);
    }

    /**
     * Verifies that controller validates submitted shipping amount.
     */
    @Test
    public void testShippingAmountValidation() {

        mockShippingAmount(SHIPPING_AMOUNT_TO_REFUND);

        try {
            controller.setAdditionalRequestParams(objectValueContainer);
            controller.validateCreateRefundRequest(ImmutableList.<ObjectValueContainer> of());
            fail();
        }
        catch (final ValidationException e) {
            assertEquals(1, e.getResourceMessages().size());
        }
    }

    @Test
    public void testMakeRefund() {
        controller.makeRefund(returnRequestModel, order);
        Mockito.verify(tgtCsCardPaymentFacade).processRefund(returnRequestModel, order);
    }

    @Test
    public void testValidateCreateRequest() throws ValidationException {
        mockShippingAmount(Double.valueOf(8d));
        final PropertyDescriptor descriptor1 =
                mockPropertyDescriptorValue("ReturnEntry.expectedQuantity", Long.valueOf(1));
        final PropertyDescriptor descriptor2 =
                mockPropertyDescriptorValue("ReturnRequest.shippingAmountToRefund", Double.valueOf(8d));
        final PropertyDescriptor descriptor3 =
                mockPropertyDescriptorValue("RefundEntry.reason", "Refunded");
        final PropertyDescriptor descriptor4 =
                mockPropertyDescriptorValue("ReturnEntry.action", "Refunded");
        final HashSet hs = new HashSet();
        hs.add(descriptor1);
        hs.add(descriptor2);
        hs.add(descriptor3);
        hs.add(descriptor4);
        when(objectValueContainer.getPropertyDescriptors()).thenReturn(
                hs);
        Assert.assertTrue(controller.validateCreateRefundRequest(objectValues));
    }

    @Test
    public void testValidateCreateRequestForZeroQauntity() throws ValidationException {
        mockShippingAmount(Double.valueOf(8d));
        final PropertyDescriptor descriptor =
                mockPropertyDescriptorValue("ReturnEntry.expectedQuantity", Long.valueOf(0));
        when(objectValueContainer.getPropertyDescriptors()).thenReturn(ImmutableSet.of(descriptor));
        try {
            controller.validateCreateRefundRequest(objectValues);
        }
        catch (final ValidationException e) {
            assertEquals(1, e.getResourceMessages().size());
        }
    }

    @Test
    public void testValidateCreateRequestForMoreThanAvailableQauntity() throws ValidationException {
        mockShippingAmount(Double.valueOf(8d));
        final PropertyDescriptor descriptor =
                mockPropertyDescriptorValue("ReturnEntry.expectedQuantity", Long.valueOf(5));

        doReturn(Long.valueOf(2)).when(orderEntryModel).getQuantity();
        when(objectValueContainer.getPropertyDescriptors()).thenReturn(ImmutableSet.of(descriptor));
        try {
            controller.validateCreateRefundRequest(objectValues);
        }
        catch (final ValidationException e) {
            assertEquals(3, e.getResourceMessages().size());
        }
    }


    @Test
    public void testValidateCreateRequestForNullReason() throws ValidationException {
        mockShippingAmount(Double.valueOf(8d));
        final PropertyDescriptor descriptor =
                mockPropertyDescriptorValue("ReturnEntry.expectedQuantity", Long.valueOf(1));
        when(objectValueContainer.getPropertyDescriptors()).thenReturn(ImmutableSet.of(descriptor));
        try {
            controller.validateCreateRefundRequest(objectValues);
        }
        catch (final ValidationException e) {
            assertEquals(2, e.getResourceMessages().size());
        }
    }

    @Test
    public void testValidateCreateRequestForNullAction() throws ValidationException {
        mockShippingAmount(Double.valueOf(8d));
        final PropertyDescriptor descriptor1 =
                mockPropertyDescriptorValue("ReturnEntry.expectedQuantity", Long.valueOf(1));
        final PropertyDescriptor descriptor2 =
                mockPropertyDescriptorValue("RefundEntry.reason", "Refunded");
        final HashSet hs = new HashSet();
        hs.add(descriptor1);
        hs.add(descriptor2);
        when(objectValueContainer.getPropertyDescriptors()).thenReturn(
                hs);
        try {
            controller.validateCreateRefundRequest(objectValues);
        }
        catch (final ValidationException e) {
            assertEquals(1, e.getResourceMessages().size());
        }
    }

    protected void mockShippingAmount(final Double shippingAmountToRefund) {
        final Double availableShipToRefund = Double.valueOf(9d);
        final PropertyDescriptor descriptor =
                mockPropertyDescriptorValue("ReturnRequest.shippingAmountToRefund", shippingAmountToRefund);
        doReturn(availableShipToRefund).when(controller).getMaximumShippingAmountToRefund();
        when(objectValueContainer.getPropertyDescriptors()).thenReturn(ImmutableSet.of(descriptor));

        doReturn(returnableOrderEntries).when(controller).getReturnableOrderEntries();
        objectValues.add(objectValueContainer);
        doReturn(defaultTypedObject).when(objectValueContainer).getObject();
        doReturn(orderEntryModel).when(defaultTypedObject).getObject();
        doReturn(Integer.valueOf(1)).when(orderEntryModel).getEntryNumber();
        doReturn(Boolean.TRUE).when(returnableOrderEntries).containsKey(Mockito.any());
        doReturn(Long.valueOf(2)).when(returnableOrderEntries).get(Mockito.any());
        controller.setAdditionalRequestParams(objectValueContainer);
    }

    private PropertyDescriptor mockPropertyDescriptorValue(final String name, final Object value) {
        final PropertyDescriptor descriptor = mock(PropertyDescriptor.class);
        final ObjectValueContainer.ObjectValueHolder valueHolder = mock(ObjectValueContainer.ObjectValueHolder.class);
        when(descriptor.getQualifier()).thenReturn(name);
        when(objectValueContainer.getValue(eq(descriptor), anyString())).thenReturn(valueHolder);
        when(valueHolder.getCurrentValue()).thenReturn(value);
        return descriptor;
    }
}

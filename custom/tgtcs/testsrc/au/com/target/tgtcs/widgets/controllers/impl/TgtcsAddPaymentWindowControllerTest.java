/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.math.BigDecimal;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Window;

import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcs.facade.TgtCsCardPaymentFacade;
import au.com.target.tgtcs.widgets.controllers.TargetCheckoutController;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.dto.HostedSessionTokenRequest;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.service.TargetPaymentService;
import junit.framework.Assert;


/**
 * @author pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TgtcsAddPaymentWindowControllerTest {

    private static final String IPG_PAYMENT_FEATURE = "payment.ipg.cscockpit";
    private static final String IPG = "ipg";
    private static final String GIFTCARD = "giftcard";
    private static final String IPG_BASE_URL = "tgtpaymentprovider.ipg.ipgSessionUrl";
    private static final String IPG_PAYMENT_FORM_RESULT_ZUL = "/tgtcshostedIpgPaymentForm.zul";

    @Mock
    private Window window;

    @Mock
    private CartModel cart;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private TargetOrderService targetOrderService;

    @Mock
    private PaymentModeService paymentModeService;

    @Mock
    private TargetPaymentService targetPaymentService;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private Iframe iFrame;

    @Mock
    private Configuration configuration;

    @Mock
    private TgtcsAddPaymentWindowController controller;

    @Mock
    private PaymentModeModel paymentModeModel;

    @Mock
    private OrderModel orderModel;

    @Mock
    private TargetCheckoutFacade targetCheckoutFacade;

    @Mock
    private TgtCsCardPaymentFacade tgtCsCardPaymentFacade;

    @Mock
    private TargetCheckoutController targetCheckoutController;

    @Mock
    private TargetCreateSubscriptionResult targetCreateSubscriptionResult;

    private final Div formcontainer = new Div();

    @Before
    public void setUp() throws Exception {
        Mockito.doCallRealMethod().when(controller).refreshWindow();
        mockControllersVariables();
        Mockito.when(controller.getOrder()).thenReturn(cart);
        Mockito.when(controller.getTargetCheckoutController()).thenReturn(targetCheckoutController);
        Mockito.when(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled(IPG_PAYMENT_FEATURE))).thenReturn(
                Boolean.TRUE);
        Mockito.when(window.getFellow("hostedPaymentFormContainer")).thenReturn(formcontainer);
        Mockito.when(configurationService.getConfiguration()).thenReturn(configuration);
        Mockito.when(cart.getTotalPrice()).thenReturn(Double.valueOf(10));
        Mockito.when(configuration.getString("tgtcscockpit.baseurl")).thenReturn("baseUrl");
        Mockito.when(paymentModeService.getPaymentModeForCode(IPG)).thenReturn(paymentModeModel);
        Mockito.when(targetOrderService.createUniqueKeyForPaymentInitiation(cart)).thenReturn(
                "UNIQUE");
        mockSessionTokenCreation(IpgPaymentTemplateType.CREDITCARDSINGLE);
        Mockito.when(configuration.getString(IPG_BASE_URL)).thenReturn("testUrl");
    }

    @Test
    public void testRefreshWindowWhenIpgPaymentIsEnabledAndAbleToCreateFrameUrl() {
        Mockito.when(controller.getFrameUrl()).thenReturn("testFrameUrl");
        controller.refreshWindow();
        Mockito.verify(iFrame).setId("ipgIFramePaymentId");
        Mockito.verify(iFrame).setName("ipgIFrame");
        Mockito.verify(iFrame).setWidth("600px");
        Mockito.verify(iFrame).setHeight("600px");
        Mockito.verify(iFrame).setParent(formcontainer);
        Mockito.verify(iFrame).setSrc("testFrameUrl");
    }

    @Test
    public void testRefreshWindowWhenIpgPaymentIsEnabledAndUnAbleToCreateFrameUrl() {
        Mockito.when(controller.getFrameUrl()).thenReturn(StringUtils.EMPTY);
        controller.refreshWindow();
        Mockito.verifyZeroInteractions(iFrame);
    }

    @Test
    public void testRefreshWindowWhenIpgPaymentIsDisabled() {
        Mockito.when(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled(IPG_PAYMENT_FEATURE))).thenReturn(
                Boolean.FALSE);
        Mockito.doNothing().when(controller).fillWindowContent();
        controller.refreshWindow();
        Mockito.verify(controller).createForm();
        Mockito.verify(controller).fillWindowContent();
    }

    @Test
    public void testRefreshWindowWhenIpgCreditcardPayment() {
        final Iframe iframe = new Iframe();
        Mockito.when(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled(IPG_PAYMENT_FEATURE))).thenReturn(
                Boolean.TRUE);
        Mockito.doReturn("testUrl").when(controller).getFrameUrl();
        Mockito.doReturn(iframe).when(controller).createIframe();
        Mockito.when(controller.getIpgPaymentTemplateType()).thenReturn(IpgPaymentTemplateType.CREDITCARDSINGLE);
        controller.refreshWindow();
        Assert.assertEquals("720px", iframe.getHeight());
        Assert.assertEquals("620px", iframe.getWidth());
    }

    @Test
    public void testRefreshWindowWhenIpgMultiplecardPayment() {
        final Iframe iframe = new Iframe();
        Mockito.when(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled(IPG_PAYMENT_FEATURE))).thenReturn(
                Boolean.TRUE);
        Mockito.doReturn("testUrl").when(controller).getFrameUrl();
        Mockito.doReturn(iframe).when(controller).createIframe();
        Mockito.when(controller.getIpgPaymentTemplateType()).thenReturn(IpgPaymentTemplateType.GIFTCARDMULTIPLE);
        controller.refreshWindow();
        Assert.assertEquals("745px", iframe.getHeight());
        Assert.assertEquals("620px", iframe.getWidth());
    }

    @Test
    public void testGetFrameUrl() {
        Mockito.when(controller.getIpgPaymentTemplateType()).thenReturn(IpgPaymentTemplateType.CREDITCARDSINGLE);
        final String expectedUrl = "testUrl?sessionId=UNIQUE&SST=UNIQUESST";
        Mockito.when(targetCheckoutFacade.createIpgUrl("UNIQUE", "UNIQUESST")).thenReturn(expectedUrl);
        Mockito.when(controller.getFrameUrl()).thenCallRealMethod();
        Assert.assertEquals(expectedUrl, controller.getFrameUrl());
        Mockito.verify(targetCheckoutController).setCurrentPaymentModeCode(IPG);
    }

    @Test
    public void testGetFrameUrlWithGiftcardMultiple() {
        mockSessionTokenCreation(IpgPaymentTemplateType.GIFTCARDMULTIPLE);
        Mockito.when(controller.getIpgPaymentTemplateType()).thenReturn(IpgPaymentTemplateType.GIFTCARDMULTIPLE);
        final String expectedUrl = "testUrl?sessionId=UNIQUE&SST=UNIQUESST";
        Mockito.when(targetCheckoutFacade.createIpgUrl("UNIQUE", "UNIQUESST")).thenReturn(expectedUrl);
        Mockito.when(controller.getFrameUrl()).thenCallRealMethod();
        Assert.assertEquals(expectedUrl, controller.getFrameUrl());
        Mockito.verify(targetCheckoutController).setCurrentPaymentModeCode(GIFTCARD);
    }

    @Test
    public void testGetFrameUrlWhenInstanceIsNotACartModel() {
        Mockito.when(controller.getOrder()).thenReturn(orderModel);
        final String expectedUrl = StringUtils.EMPTY;
        Mockito.when(controller.getFrameUrl()).thenCallRealMethod();
        Assert.assertEquals(expectedUrl, controller.getFrameUrl());
    }

    /**
     * Method to mock controllers getters to return mock objects
     */
    private void mockControllersVariables() {
        Mockito.when(controller.getTargetFeatureSwitchService()).thenReturn(targetFeatureSwitchService);
        Mockito.when(controller.getPaymentModeService()).thenReturn(paymentModeService);
        Mockito.when(controller.getTargetOrderService()).thenReturn(targetOrderService);
        Mockito.when(controller.getTargetPaymentService()).thenReturn(targetPaymentService);
        Mockito.when(controller.getConfigurationService()).thenReturn(configurationService);
        Mockito.when(controller.getWindow()).thenReturn(window);
        Mockito.when(controller.createIframe()).thenReturn(iFrame);
        Mockito.when(controller.getTgtCsCardPaymentFacade()).thenReturn(tgtCsCardPaymentFacade);
        Mockito.when(controller.getTargetCheckoutFacade()).thenReturn(targetCheckoutFacade);
    }

    /**
     * Method to mock session token creation
     */
    private void mockSessionTokenCreation(final IpgPaymentTemplateType ipgPaymentTemplateType) {
        final String url = "baseUrl" + IPG_PAYMENT_FORM_RESULT_ZUL;
        final HostedSessionTokenRequest hostedSessionTokenRequest = new HostedSessionTokenRequest();
        hostedSessionTokenRequest.setAmount(BigDecimal.valueOf(10));
        hostedSessionTokenRequest.setCancelUrl(url);
        hostedSessionTokenRequest.setOrderModel(cart);
        hostedSessionTokenRequest.setPaymentCaptureType(PaymentCaptureType.PLACEORDER);
        hostedSessionTokenRequest.setPaymentMode(paymentModeModel);
        hostedSessionTokenRequest.setReturnUrl(url);
        hostedSessionTokenRequest.setSessionId("UNIQUE");
        Mockito.when(
                tgtCsCardPaymentFacade.createHostedSessionTokenRequest(cart,
                        BigDecimal.valueOf(cart.getTotalPrice().doubleValue()), paymentModeModel, url, url,
                        PaymentCaptureType.PLACEORDER, "UNIQUE", ipgPaymentTemplateType))
                .thenReturn(
                        hostedSessionTokenRequest);
        Mockito.when(targetCreateSubscriptionResult.getRequestToken()).thenReturn("UNIQUESST");
        Mockito.when(targetPaymentService.getHostedSessionToken(Mockito.refEq(hostedSessionTokenRequest)))
                .thenReturn(targetCreateSubscriptionResult);
    }
}

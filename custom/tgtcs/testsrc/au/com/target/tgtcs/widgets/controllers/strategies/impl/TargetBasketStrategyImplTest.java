/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers.strategies.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.purchase.PurchaseOptionConfigService;
import au.com.target.tgtlayby.purchase.PurchaseOptionService;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetBasketStrategyImplTest {

    private static final String BUY_NOW_CODE = "buynow";
    private static final String PRODUCT_BUY_BY_BUYNOW = "Sample product A - buy by buynow option";
    private static ThreadLocal<TypedObject> currentSessionMockCart = new ThreadLocal<>();
    private static ThreadLocal<TypedObject> currentSessionMockCart2 = new ThreadLocal<>();
    private static ThreadLocal<TypedObject> currentSessionMockCart3 = new ThreadLocal<>();
    private static ThreadLocal<TypedObject> currentSessionMockCartNull = new ThreadLocal<>();

    private final TargetBasketStrategyImpl targetBasketStrategy = new TargetBasketStrategyImpl() {
        @Override
        public TypedObject createCart(final TypedObject customer) {
            if (currentSessionMockCart.get() != null) {
                return currentSessionMockCart.get();
            }
            return super.createCart(customer);
        }

        @Override
        public TypedObject createCart(final TypedObject customer, final String cartCode) {
            if (currentSessionMockCart3.get() != null) {
                return currentSessionMockCart3.get();
            }
            return super.createCart(customer, cartCode);
        }

        //mock super.getCart method
        @Override
        public TypedObject getCart(final TypedObject customer, final String cartCode)
        {
            if (currentSessionMockCartNull.get() == null) {
                return null;
            }
            if (currentSessionMockCart2.get() != null) {
                return currentSessionMockCart2.get();
            }
            return super.getCart(customer, cartCode);
        }
    };

    @Mock
    private TargetLaybyCartService targetLaybyCartService;

    @Mock
    private TargetCommerceCartService targetCommerceCartService;

    @Mock
    private Configuration configuration;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;

    @Mock
    private TypeService cockpitTypeService;

    @Mock
    private ModelService modelService;

    @Mock
    private PurchaseOptionService purchaseOptionService;

    @Mock
    private PurchaseOptionConfigService purchaseOptionConfigService;

    private final CustomerModel customerModel = new CustomerModel();

    @Before
    public void setup() throws Exception {
        final PurchaseOptionModel buynowPurchaseOption = new PurchaseOptionModel();

        buynowPurchaseOption.setCode(BUY_NOW_CODE);

        final TargetProductModel productA = new TargetProductModel();
        productA.setCode(PRODUCT_BUY_BY_BUYNOW);

        final CartModel masterCart = new CartModel();
        masterCart.setCode("Master");
        final CartModel memoryCart = new CartModel();
        memoryCart.setCode("InMemory");

        final CartModel checkoutCart = new CartModel();
        checkoutCart.setCode("checkoutCart");

        final CartEntryModel buynowEntry = new CartEntryModel();
        buynowEntry.setProduct(productA);

        final List<AbstractOrderEntryModel> masterCartEntries = new ArrayList<>();
        masterCartEntries.add(buynowEntry);
        masterCart.setEntries(masterCartEntries);

        final List<AbstractOrderEntryModel> memoryCartEntries = new ArrayList<>();
        memoryCartEntries.add(buynowEntry);
        memoryCart.setEntries(memoryCartEntries);

        final List<AbstractOrderEntryModel> checkoutCartEntries = new ArrayList<>();
        checkoutCartEntries.add(buynowEntry);
        checkoutCart.setEntries(memoryCartEntries);

        targetBasketStrategy.setTargetLaybyCartService(targetLaybyCartService);
        targetBasketStrategy.setConfigurationService(configurationService);
        targetBasketStrategy.setTargetCommerceCartService(targetCommerceCartService);
        targetBasketStrategy.setTargetPurchaseOptionHelper(targetPurchaseOptionHelper);
        targetBasketStrategy.setCockpitTypeService(cockpitTypeService);
        targetBasketStrategy.setModelService(modelService);
        targetBasketStrategy.setPurchaseOptionService(purchaseOptionService);
        targetBasketStrategy.setPurchaseOptionConfigService(purchaseOptionConfigService);
        final KeyGenerator keyGenerator = Mockito.mock(KeyGenerator.class);
        targetBasketStrategy.setKeyGenerator(keyGenerator);
        when(keyGenerator.generate()).thenReturn("mockId");
        when(configurationService.getConfiguration()).thenReturn(configuration);
        when(targetLaybyCartService.getMostRecentCartForCustomer((CustomerModel)Mockito.anyObject()))
                .thenReturn(masterCart);
        when(configuration.getString(Mockito.anyString(), Mockito.anyString())).thenAnswer(new Answer<String>() {
            /* (non-Javadoc)
             * @see org.mockito.stubbing.Answer#answer(org.mockito.invocation.InvocationOnMock)
             */
            @Override
            public String answer(final InvocationOnMock invocation) {
                //always return default value
                return (String)invocation.getArguments()[1];
            }
        });

        when(targetPurchaseOptionHelper.getPurchaseOptionModel(BUY_NOW_CODE)).thenReturn(buynowPurchaseOption);
    }

    @Test
    public void testGetMasterCart() {
        final CartModel cartModel = targetBasketStrategy.getPersistedMasterCart(customerModel);
        Assert.assertNotNull(cartModel);
        Assert.assertEquals(cartModel.getEntries().size(), 1);
    }

}
/**
 * 
 */
package au.com.target.tgtcs.widgets.controllers.strategies.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * 
 * Unit Test for {@link TargetCustomerOrdersStrategy}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCustomerOrdersStrategyTest {

    private final TargetCustomerOrdersStrategy testInstance = new TargetCustomerOrdersStrategy();
    @Mock
    private CustomerModel customer;


    @Test
    public void testGetCustomerOrderOrderedDesc() {
        final List<OrderModel> customerOrders = new ArrayList<OrderModel>(2);
        final OrderModel order1 = mock(OrderModel.class);
        when(order1.getCode()).thenReturn("00000100");
        final OrderModel order2 = mock(OrderModel.class);
        when(order2.getCode()).thenReturn("00000200");
        customerOrders.add(order1);
        customerOrders.add(order2);
        when(customer.getOrders()).thenReturn(customerOrders);


        final Collection<OrderModel> orders = testInstance.getCustomerOrders(customer);
        Assert.assertTrue(orders.size() == 2);
        Assert.assertTrue("big number must be in top", orders.iterator().next().getCode().equals("00000200"));
    }

    @Test
    public void testGetCustomerOrderOrderedDescWhenOrderCodeIsNull() {
        final List<OrderModel> customerOrders = new ArrayList<OrderModel>(2);
        final OrderModel order1 = mock(OrderModel.class);
        when(order1.getCode()).thenReturn("00000100");
        final OrderModel order2 = mock(OrderModel.class);
        when(order2.getCode()).thenReturn(null);
        final OrderModel order3 = mock(OrderModel.class);
        when(order3.getCode()).thenReturn(null);
        customerOrders.add(order1);
        customerOrders.add(order2);
        customerOrders.add(order3);
        when(customer.getOrders()).thenReturn(customerOrders);


        final Collection<OrderModel> orders = testInstance.getCustomerOrders(customer);
        Assert.assertTrue(orders.size() == 3);
        Assert.assertTrue("order code with null must be in down", orders.iterator().next().getCode().equals("00000100"));
    }

}

/**
 * 
 */
package au.com.target.tgtcs.widgets.listener;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.util.mail.MailUtils;

import org.apache.commons.mail.EmailException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.zkoss.util.resource.Labels;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.impl.InputElement;

import au.com.target.tgtbusproc.TargetBusinessProcessService;


/**
 * 
 * Unit Test for {@link ResendTaxInvoiceEventListener}
 * 
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Messagebox.class, Labels.class, MailUtils.class })
public class ResendTaxInvoiceEventListenerTest {

    private static final String VALID_TEST_EMAIL = "email@example.com";
    private static final String INVALID_TEST_EMAIL = "emailexample.com";
    @Mock
    private InputElement emailAddressInput;
    @Mock
    private OrderModel order;
    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;
    private ResendTaxInvoiceEventListener testInstance;


    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        initMocks(this);
        mockStatic(Labels.class);
        mockStatic(Messagebox.class);
        mockStatic(MailUtils.class);
        testInstance = new ResendTaxInvoiceEventListener(order, emailAddressInput);
        testInstance.setTargetBusinessProcessService(targetBusinessProcessService);

        PowerMockito.when(Labels.getLabel(ResendTaxInvoiceEventListener.TAX_INVOICE_SUCCESS_SENDING_KEY,
                new Object[] { VALID_TEST_EMAIL })).thenReturn("success");
        PowerMockito.when(Labels.getLabel(ResendTaxInvoiceEventListener.CONFIRM_POPUP_TITLE_KEY)).thenReturn("title");
        PowerMockito.doThrow(new EmailException()).when(MailUtils.class);
        MailUtils.validateEmailAddress(INVALID_TEST_EMAIL, "customer email");
    }

    @Test
    public void testHandleSendTaxInvoice() throws Exception {
        testInstance.handleSendTaxInvoice(VALID_TEST_EMAIL);
        Mockito.verify(targetBusinessProcessService).startResendTaxInvoiceProcess(order, VALID_TEST_EMAIL);
        PowerMockito.verifyStatic();
        Labels.getLabel(ResendTaxInvoiceEventListener.TAX_INVOICE_SUCCESS_SENDING_KEY,
                new Object[] { VALID_TEST_EMAIL });
    }

    @Test
    public void testHandleSendTaxInvoiceWithInvalidEmail() throws Exception {
        testInstance.handleSendTaxInvoice(INVALID_TEST_EMAIL);
        Mockito.verify(targetBusinessProcessService, Mockito.atMost(0)).startResendTaxInvoiceProcess(order,
                VALID_TEST_EMAIL);
        PowerMockito.verifyStatic();
        Labels.getLabel(ResendTaxInvoiceEventListener.INVALID_EMAIL_MESSAGE_KEY);
    }

    @Test
    public void testHandleSendTaxInvoiceWhenStartResendTaxInvoiceProcessThrowsException() throws Exception {
        final RuntimeException exception = Mockito.mock(RuntimeException.class);
        Mockito.when(exception.getMessage()).thenReturn("ex message");
        Mockito.when(targetBusinessProcessService.startResendTaxInvoiceProcess(order,
                VALID_TEST_EMAIL)).thenThrow(exception);
        testInstance.handleSendTaxInvoice(VALID_TEST_EMAIL);
        Mockito.verify(exception).getMessage();

    }

}

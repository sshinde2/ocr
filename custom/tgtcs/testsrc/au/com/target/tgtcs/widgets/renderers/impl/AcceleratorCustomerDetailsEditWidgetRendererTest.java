/**
 * 
 */

package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.widgets.InputWidget;
import de.hybris.platform.cscockpit.widgets.controllers.CustomerController;
import de.hybris.platform.cscockpit.widgets.models.impl.CustomerItemWidgetModel;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.zkoss.zk.ui.api.HtmlBasedComponent;

import au.com.target.tgtcore.model.TargetCustomerModel;


/**
 * 
 * Unit Test for {@link AcceleratorCustomerDetailsEditWidgetRenderer}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AcceleratorCustomerDetailsEditWidgetRendererTest {
    @Mock
    private CustomerItemWidgetModel customerItemWidgetModel;
    @Mock
    private HtmlBasedComponent targetCommerceCheckoutService;
    @Mock
    private TargetCustomerModel targetCustomerModel;

    /**
     * Retrieve fields to be displayed on Customer Detail Edit page Fields to be displayed do not include SMS, Subscribe
     * and Competition
     * 
     * 
     */
    @Test
    public void testCreateContentInternal() {
        final AcceleratorCustomerDetailsEditWidgetRenderer acceleratorCustomerDetailsEditWidgetRenderer =
                new AcceleratorCustomerDetailsEditWidgetRenderer();
        final InputWidget<CustomerItemWidgetModel, CustomerController> inputWidget =
                BDDMockito.mock(InputWidget.class);
        final HtmlBasedComponent rootContainer = BDDMockito.mock(HtmlBasedComponent.class);
        final HtmlBasedComponent component =
                acceleratorCustomerDetailsEditWidgetRenderer.createContentInternal(inputWidget, rootContainer);
        Assert.assertNotNull(component);
    }
}

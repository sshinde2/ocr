/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cockpit.widgets.models.WidgetModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.widgets.renderers.utils.PopupWidgetHelper;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.zkoss.zk.ui.api.HtmlBasedComponent;

import au.com.target.tgtcs.widgets.controllers.impl.TargetManualRefundWidgetControllerImpl;


/**
 * @author bhuang3
 *
 */
@UnitTest
public class TargetCsManualRefundWidgetRendererTest {

    @Mock
    private PopupWidgetHelper popupWidgetHelper;
    @Mock
    private ModelService modelService;
    @Mock
    private TypeService typeService;
    @Mock
    private Widget<WidgetModel, TargetManualRefundWidgetControllerImpl> widget;
    @Mock
    private TargetManualRefundWidgetControllerImpl controller;
    @Mock
    private OrderModel orderModel;
    @Spy
    @InjectMocks
    private final TargetCsManualRefundWidgetRenderer renderer = new TargetCsManualRefundWidgetRenderer();

    @Before
    public void prepare() {
        MockitoAnnotations.initMocks(this);
        BDDMockito.given(widget.getWidgetTitle()).willReturn("title");
        BDDMockito.given(widget.getWidgetController()).willReturn(controller);
        Mockito.when(controller.getSelectedOrder()).thenReturn(orderModel);
    }

    @Test
    public void testCreateCaption() {
        final HtmlBasedComponent component = renderer.createCaption(widget);
        Assert.assertNotNull(component);
    }

    @Test
    public void testCreateContent() {
        final BigDecimal refundAmount = BigDecimal.valueOf(10d);
        Mockito.when(controller.findRefundAmount(orderModel)).thenReturn(refundAmount);
        final HtmlBasedComponent component = renderer.createContent(widget);
        Assert.assertNotNull(component);
    }

}

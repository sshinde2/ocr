/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.widgets.InputWidget;
import de.hybris.platform.cockpit.widgets.models.impl.DefaultListWidgetModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.widgets.controllers.CancellationController;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import au.com.target.tgtcs.components.orderdetail.controllers.AddPaymentWindowController;
import au.com.target.tgtcs.facade.impl.TgtCsCardPaymentFacadeImpl;
import au.com.target.tgtcs.widgets.controllers.impl.TargetCancellationControllerImpl;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;


/**
 * @author Nandini
 * 
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest(Messagebox.class)
public class TargetFullOrderCancellationWidgetRendererTest {

    @InjectMocks
    private final TargetFullOrderCancellationWidgetRenderer orderCancellationWidgetRenderer = new TargetFullOrderCancellationWidgetRenderer();

    @Mock
    private TgtCsCardPaymentFacadeImpl tgtCsCardPaymentFacade;
    @Mock
    private AddPaymentWindowController addPaymentWindowController;
    @Mock
    private Window window;
    @Mock
    private CancellationController controller;
    @Mock
    private OrderModel orderModel;
    @Mock
    private InputWidget<DefaultListWidgetModel<OrderCancelRequest>, CancellationController> inputWidget;
    @Mock
    private DefaultListWidgetModel defaultListWidgetModel;
    @Mock
    private TargetCancellationControllerImpl cancellationController;
    @Mock
    private TypedObject typedObject;
    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Mock
    private InputWidget<DefaultListWidgetModel<OrderCancelRequest>, CancellationController> widget;

    @Mock
    private ObjectValueContainer objectValueContainer;

    @Mock
    private CancellationController widgetController;

    @Before
    public void setUp() {
        orderCancellationWidgetRenderer.setAddPaymentWindowController(addPaymentWindowController);
        given(inputWidget.getWidgetController()).willReturn(controller);
        given(inputWidget.getWidgetModel()).willReturn(defaultListWidgetModel);
        given(controller.getOrder()).willReturn(typedObject);
        given(typedObject.getObject()).willReturn(orderModel);
    }

    @Test
    public void testHandleRefundPaymentPopupEventWidgetNull() throws InterruptedException {
        orderCancellationWidgetRenderer.handleRefundPaymentPopupEvent(null);
        verify(addPaymentWindowController, Mockito.never()).populateCancelPaymentForm(orderModel,
                window);
    }

    @Test
    public void testHandleRefundPaymentPopupEventWidgetDifferentController() throws InterruptedException {
        orderCancellationWidgetRenderer.handleRefundPaymentPopupEvent(inputWidget);
        verify(addPaymentWindowController, Mockito.never()).populateCancelPaymentForm(orderModel,
                window);
    }

    @Test
    public void testHandleRefundPaymentPopupEventWidgetNoTypedObject() throws InterruptedException {
        given(cancellationController.getOrder()).willReturn(null);
        orderCancellationWidgetRenderer.handleRefundPaymentPopupEvent(inputWidget);
        verify(addPaymentWindowController, Mockito.never()).populateCancelPaymentForm(orderModel,
                window);
    }

    @Test
    public void testHandleRefundPaymentPopupEventWidgetWithNoOrder() throws InterruptedException {
        given(typedObject.getObject()).willReturn(null);
        orderCancellationWidgetRenderer.handleRefundPaymentPopupEvent(inputWidget);
        verify(addPaymentWindowController, Mockito.never()).populateCancelPaymentForm(orderModel,
                window);
    }

    @Test(expected = NullPointerException.class)
    public void testCreateContentInternalWindowNull() {
        given(orderCancellationWidgetRenderer.getPaymentWindow(orderModel)).willReturn(window);
        final HtmlBasedComponent component = orderCancellationWidgetRenderer.createContentInternal(inputWidget,
                null);
        assertThat(component).isNotNull();
        verify(addPaymentWindowController, never()).populateCancelPaymentForm(orderModel, window);
        fail("Exception in payment window");
        assertThat("Exception in payment window").isEqualTo("Exception in payment window");

    }

    @Test
    public void testHandleAttemptCancellationEventThrowsOrderCancelException() throws Exception {
        final Event event = new Event("onOK", null);
        final OrderCancelException cancelException = new OrderCancelException("test error");

        willReturn(widgetController).given(widget).getWidgetController();
        willThrow(cancelException).given(widgetController)
                .createOrderCancellationRequest(objectValueContainer);

        mockStatic(Messagebox.class);

        orderCancellationWidgetRenderer.handleAttemptCancellationEvent(widget, event, objectValueContainer);

        verify(widgetController).dispatchEvent(null, widget, null);

    }
}
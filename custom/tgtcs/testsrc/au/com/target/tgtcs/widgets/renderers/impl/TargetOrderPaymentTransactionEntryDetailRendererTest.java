/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.core.model.order.payment.IpgGiftCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;



/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetOrderPaymentTransactionEntryDetailRendererTest {


    @InjectMocks
    private final TargetOrderPaymentTransactionEntryDetailRenderer renderer = new TargetOrderPaymentTransactionEntryDetailRenderer();

    @Mock
    private TypedObject typedObject;

    @Mock
    private PaymentTransactionEntryModel paymentTransactionEntry;

    @Mock
    private PaymentTransactionModel paymentTransactionModel;

    @Mock
    private IpgPaymentInfoModel ipgPaymentInfo;

    @Mock
    private IpgCreditCardPaymentInfoModel creditCardInfo;

    @Mock
    private IpgGiftCardPaymentInfoModel giftCardInfo;

    @Mock
    private TypeService typeService;

    @Mock
    private AddressModel addressModel;

    @Mock
    private PaymentInfoModel paymentInfo;

    @Before
    public void setup() {
        renderer.setCockpitTypeService(typeService);
    }

    @Test
    public void testGetPaymentInfoWithNullEntry() {
        Assert.assertNull(renderer.getPaymentInfo(null));
        Mockito.verifyZeroInteractions(typeService);
    }

    @Test
    public void testGetPaymentInfoWithNullTrans() {
        when(typedObject.getObject()).thenReturn(paymentTransactionEntry);
        when(paymentTransactionEntry.getPaymentTransaction()).thenReturn(null);
        renderer.getPaymentInfo(typedObject);
        Mockito.verifyZeroInteractions(typeService);
    }

    @Test
    public void testGetPaymentInfoWithNonIPG() {
        when(typedObject.getObject()).thenReturn(paymentTransactionEntry);
        when(paymentTransactionEntry.getPaymentTransaction()).thenReturn(paymentTransactionModel);
        when(paymentTransactionModel.getInfo()).thenReturn(paymentInfo);
        renderer.getPaymentInfo(typedObject);
        Mockito.verify(typeService).wrapItem(paymentInfo);
    }

    @Test
    public void testGetPaymentInfoWithIPGCreditCard() {
        when(typedObject.getObject()).thenReturn(paymentTransactionEntry);
        when(paymentTransactionEntry.getPaymentTransaction()).thenReturn(paymentTransactionModel);
        when(paymentTransactionEntry.getIpgPaymentInfo()).thenReturn(creditCardInfo);
        when(paymentTransactionModel.getInfo()).thenReturn(ipgPaymentInfo);
        when(ipgPaymentInfo.getBillingAddress()).thenReturn(addressModel);
        when(paymentTransactionModel.getEntries()).thenReturn(Collections.singletonList(paymentTransactionEntry));
        renderer.getPaymentInfo(typedObject);
        Mockito.verify(typeService).wrapItem(creditCardInfo);
        Mockito.verify(creditCardInfo).setBillingAddress(addressModel);
    }

    @Test
    public void testGetPaymentInfoWithIPGGiftcard() {
        when(typedObject.getObject()).thenReturn(paymentTransactionEntry);
        when(paymentTransactionEntry.getPaymentTransaction()).thenReturn(paymentTransactionModel);
        when(paymentTransactionEntry.getIpgPaymentInfo()).thenReturn(giftCardInfo);
        when(paymentTransactionModel.getInfo()).thenReturn(ipgPaymentInfo);
        when(ipgPaymentInfo.getBillingAddress()).thenReturn(addressModel);
        when(paymentTransactionModel.getEntries()).thenReturn(Collections.singletonList(paymentTransactionEntry));
        renderer.getPaymentInfo(typedObject);
        Mockito.verify(typeService).wrapItem(giftCardInfo);
        Mockito.verify(giftCardInfo).setBillingAddress(addressModel);
    }


}

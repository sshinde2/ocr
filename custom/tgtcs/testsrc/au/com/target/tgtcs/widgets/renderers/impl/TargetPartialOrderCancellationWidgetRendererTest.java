/**
 * 
 */
package au.com.target.tgtcs.widgets.renderers.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.widgets.InputWidget;
import de.hybris.platform.cockpit.widgets.models.impl.DefaultListWidgetModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.widgets.controllers.CancellationController;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zul.Div;
import org.zkoss.zul.Window;

import au.com.target.tgtcs.components.orderdetail.controllers.AddPaymentWindowController;
import au.com.target.tgtcs.facade.TgtCsCardPaymentFacade;
import au.com.target.tgtcs.widgets.controllers.impl.TargetCancellationControllerImpl;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;


/**
 * @author Nandini
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPartialOrderCancellationWidgetRendererTest {
    private static final String EXPECTING_A_DIV = "Expecting a div";
    private static final String EXPECTING_A_RESULT = "Expecting a result";

    @InjectMocks
    private final TargetPartialOrderCancellationWidgetRenderer partialOrderCancellationWidgetRenderer =
            new TargetPartialOrderCancellationWidgetRenderer();

    @Mock
    private AddPaymentWindowController addPaymentWindowController;
    @Mock
    private Window window;
    @Mock
    private OrderModel orderModel;
    @Mock
    private InputWidget<DefaultListWidgetModel<TypedObject>, CancellationController> inputWidget;
    @Mock
    private DefaultListWidgetModel defaultListWidgetModel;
    @Mock
    private TargetCancellationControllerImpl cancellationController;
    @Mock
    private TypedObject typedObject;
    @Mock
    private HtmlBasedComponent rootContainer;
    @Mock
    private TgtCsCardPaymentFacade tgtCsCardPaymentFacade;
    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Test
    public void testcreateContentInternal() {
        partialOrderCancellationWidgetRenderer.setAddPaymentWindowController(addPaymentWindowController);
        partialOrderCancellationWidgetRenderer.setTgtCsCardPaymentFacade(tgtCsCardPaymentFacade);
        given(inputWidget.getWidgetController()).willReturn(cancellationController);
        given(inputWidget.getWidgetModel()).willReturn(defaultListWidgetModel);
        given(cancellationController.getOrder()).willReturn(typedObject);
        given(typedObject.getObject()).willReturn(orderModel);


        final HtmlBasedComponent result = partialOrderCancellationWidgetRenderer.createContentInternal(inputWidget,
                rootContainer);

        assertNotNull(EXPECTING_A_RESULT, result);
        assertEquals(EXPECTING_A_DIV, Div.class, result.getClass());
        Mockito.verify(tgtCsCardPaymentFacade).clearOldPayment();

    }

    @Test
    public void testHandleRefundPaymentPopupEventWidgetNull() throws InterruptedException {
        partialOrderCancellationWidgetRenderer.handleRefundPaymentPopupEvent(null);
        Mockito.verify(addPaymentWindowController, Mockito.never()).populateCancelPaymentForm(orderModel,
                window);
    }

    @Test
    public void testHandleRefundPaymentPopupEventWidgetDifferentController() throws InterruptedException {
        partialOrderCancellationWidgetRenderer.handleRefundPaymentPopupEvent(inputWidget);
        Mockito.verify(addPaymentWindowController, Mockito.never()).populateCancelPaymentForm(orderModel,
                window);
    }

    @Test
    public void testHandleRefundPaymentPopupEventWidgetNoTypedObject() throws InterruptedException {
        given(cancellationController.getOrder()).willReturn(null);
        partialOrderCancellationWidgetRenderer.handleRefundPaymentPopupEvent(inputWidget);
        Mockito.verify(addPaymentWindowController, Mockito.never()).populateCancelPaymentForm(orderModel,
                window);
    }

    @Test
    public void testHandleRefundPaymentPopupEventWidgetWithNoOrder() throws InterruptedException {
        given(typedObject.getObject()).willReturn(null);
        partialOrderCancellationWidgetRenderer.handleRefundPaymentPopupEvent(inputWidget);
        Mockito.verify(addPaymentWindowController, Mockito.never()).populateCancelPaymentForm(orderModel,
                window);
    }

}

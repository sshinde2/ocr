/**
 * 
 */
package au.com.target.tgtebay.actions;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;
import au.com.target.tgtebay.logger.PartnerStatusUpdateLogger;
import au.com.target.tgtwebmethods.ca.TargetCaShippingService;


/**
 * @author pratik
 *
 */
public class UpdatePartnerWithReadyForPickupStatusAction extends RetryActionWithSpringConfig<OrderProcessModel> {

    private static final PartnerStatusUpdateLogger LOG = new PartnerStatusUpdateLogger(
            UpdatePartnerWithReadyForPickupStatusAction.class);

    private TargetCaShippingService targetCaShippingService;

    public enum Transition {
        OK;

        public static Set<String> getStringValues() {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values()) {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    protected String executeInternal(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process, "Business process can't be null.");
        final OrderModel orderModel = process.getOrder();
        Assert.notNull(orderModel, "OrderModel can't be null.");

        final SalesApplication salesApp = orderModel.getSalesApplication();
        final String hybrisOrderNumber = orderModel.getCode();
        final String partnerOrderNumber = ObjectUtils.toString(orderModel.getEBayOrderNumber());

        if (getOrderProcessParameterHelper().isPartnerUpdateRequired(process)) {
            LOG.logOrderStatusUpdateEntry(PartnerStatusUpdateLogger.LOG_READYFORPICKUP, salesApp, hybrisOrderNumber,
                    partnerOrderNumber);

            if (targetCaShippingService.submitCncOrderStatusForReadyForPickupUpdate(orderModel)) {
                LOG.logOrderStatusUpdateSuccess(PartnerStatusUpdateLogger.LOG_READYFORPICKUP, salesApp,
                        hybrisOrderNumber, partnerOrderNumber);
            }
            else {
                LOG.logOrderStatusUpdateFailure(PartnerStatusUpdateLogger.LOG_READYFORPICKUP, salesApp,
                        hybrisOrderNumber, partnerOrderNumber);
                throw new RetryLaterException();
            }
        }
        else {
            LOG.logOrderStatusUpdateSkip(PartnerStatusUpdateLogger.LOG_READYFORPICKUP, salesApp, hybrisOrderNumber,
                    partnerOrderNumber);
        }

        return Transition.OK.toString();
    }

    /**
     * Log retry exceeded information
     */
    @Override
    protected void logRetryExceeded(final OrderProcessModel process) {
        final OrderModel orderModel = process.getOrder();

        LOG.logRetryExceededError(PartnerStatusUpdateLogger.LOG_READYFORPICKUP, orderModel.getSalesApplication(),
                orderModel.getCode(), ObjectUtils.toString(orderModel.getEBayOrderNumber()));
    }

    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }

    /**
     * @param targetCaShippingService
     *            the targetCaShippingService to set
     */
    @Required
    public void setTargetCaShippingService(final TargetCaShippingService targetCaShippingService) {
        this.targetCaShippingService = targetCaShippingService;
    }

}

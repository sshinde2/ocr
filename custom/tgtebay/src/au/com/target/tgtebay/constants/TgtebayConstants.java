/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtebay.constants;

/**
 * Global class for all Tgtebay constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class TgtebayConstants extends GeneratedTgtebayConstants {

    public static final String EXTENSIONNAME = "tgtebay";
    public static final String EBAY_PAYPAL_TOKEN = "EBAY_BLANK_PAYPAL_TOKEN";
    public static final String EBAY_PAYPAL_PAYERID = "EBAY_PAYERID";
    public static final String TRADEME_PAYNOW_PAYERID = "TRADEME_PAYERID";
    public static final String GUEST_NAME = "Guest";
    public static final Double DOUBLE_ZERO = Double.valueOf(0.0);
    public static final Long LONG_ZERO = Long.valueOf(0);
    public static final String TOTAL_DISCOUNTS = "Total Discounts";
    public static final String TOTAL_PRICE = "Total Price";
    public static final String TOTAL_TAX = "Total Tax";
    public static final String BASE_PRICE = "Base Price";
    public static final String ORDER_ENTRY_QUANTITY = "OrderEntry Quantity";
    public static final String DELIVERY_COST = "Delivery Cost";
    public static final String NOT_APPLICABLE = "N/A";
    public static final String CURRENCY_CODE_AUD = "AUD";
    public static final String CURRENCY_CODE_NZD = "NZD";

    private TgtebayConstants() {
        //empty to avoid instantiating this constant class
    }

    // implement here constants used by this extension
}

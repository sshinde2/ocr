/**
 * 
 */
package au.com.target.tgtebay.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author mjanarth
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class EbayTargetOrderDTO {

    @XmlElement
    private String createdDate;
    @XmlElement
    private String deliveryCost;
    @XmlElement
    private String subTotal;
    @XmlElement
    private String totalDiscounts;
    @XmlElement
    private String totalPrice;
    @XmlElement
    private String totalTax;
    @XmlElement
    private String orderNumber;
    @XmlElement
    private String clientOrderId;
    @XmlElement
    private String buyerIpAddress;

    // references
    @XmlElement
    private EbayTargetAddressDTO deliveryAddress;
    @XmlElement
    private EbayTargetAddressDTO paymentAddress;

    // reference list
    @XmlElement
    private EbayTargetOrderEntriesDTO ebayTargetOrderEntriesDTO;

    @XmlElement
    private String status;
    @XmlElement
    private EbayTargetPayInfoDTO paymentInfo;

    @XmlElement
    private String eBayOrderNumber;
    @XmlElement
    private String name;
    @XmlElement
    private String buyerEmail;
    @XmlElement
    private String buyerIP;
    @XmlElement
    private String salesChannel;
    @XmlElement
    private String currencyCode;

    /**
     * @return the deliveryCost
     */
    public String getDeliveryCost() {
        return deliveryCost;
    }

    /**
     * @param deliveryCost
     *            the deliveryCost to set
     */
    public void setDeliveryCost(final String deliveryCost) {
        this.deliveryCost = deliveryCost;
    }

    /**
     * @return the subTotal
     */
    public String getSubTotal() {
        return subTotal;
    }

    /**
     * @param subTotal
     *            the subTotal to set
     */
    public void setSubTotal(final String subTotal) {
        this.subTotal = subTotal;
    }

    /**
     * @return the totalDiscounts
     */
    public String getTotalDiscounts() {
        return totalDiscounts;
    }

    /**
     * @param totalDiscounts
     *            the totalDiscounts to set
     */
    public void setTotalDiscounts(final String totalDiscounts) {
        this.totalDiscounts = totalDiscounts;
    }

    /**
     * @return the totalPrice
     */
    public String getTotalPrice() {
        return totalPrice;
    }

    /**
     * @param totalPrice
     *            the totalPrice to set
     */
    public void setTotalPrice(final String totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * @return the totalTax
     */
    public String getTotalTax() {
        return totalTax;
    }

    /**
     * @param totalTax
     *            the totalTax to set
     */
    public void setTotalTax(final String totalTax) {
        this.totalTax = totalTax;
    }

    /**
     * @return the deliveryAddress
     */
    public EbayTargetAddressDTO getDeliveryAddress() {
        return deliveryAddress;
    }

    /**
     * @param deliveryAddress
     *            the deliveryAddress to set
     */
    public void setDeliveryAddress(final EbayTargetAddressDTO deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    /**
     * @return the paymentAddress
     */
    public EbayTargetAddressDTO getPaymentAddress() {
        return paymentAddress;
    }

    /**
     * @param paymentAddress
     *            the paymentAddress to set
     */
    public void setPaymentAddress(final EbayTargetAddressDTO paymentAddress) {
        this.paymentAddress = paymentAddress;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * @return the paymentInfo
     */
    public EbayTargetPayInfoDTO getPaymentInfo() {
        return paymentInfo;
    }

    /**
     * @param paymentInfo
     *            the paymentInfo to set
     */
    public void setPaymentInfo(final EbayTargetPayInfoDTO paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    /**
     * @return the eBayOrderNumber
     */
    public String geteBayOrderNumber() {
        return eBayOrderNumber;
    }

    /**
     * @param eBayOrderNumber
     *            the eBayOrderNumber to set
     */
    public void seteBayOrderNumber(final String eBayOrderNumber) {
        this.eBayOrderNumber = eBayOrderNumber;
    }

    /**
     * @return the orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * @param orderNumber
     *            the orderNumber to set
     */
    public void setOrderNumber(final String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the buyerEmail
     */
    public String getBuyerEmail() {
        return buyerEmail;
    }

    /**
     * @param buyerEmail
     *            the buyerEmail to set
     */
    public void setBuyerEmail(final String buyerEmail) {
        this.buyerEmail = buyerEmail;
    }

    /**
     * @return the createdDate
     */
    public String getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate
     *            the createdDate to set
     */
    public void setCreatedDate(final String createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the buyerIP
     */
    public String getBuyerIP() {
        return buyerIP;
    }

    /**
     * @param buyerIP
     *            the buyerIP to set
     */
    public void setBuyerIP(final String buyerIP) {
        this.buyerIP = buyerIP;
    }

    /**
     * @return the clientOrderId
     */
    public String getClientOrderId() {
        return clientOrderId;
    }

    /**
     * @param clientOrderId
     *            the clientOrderId to set
     */
    public void setClientOrderId(final String clientOrderId) {
        this.clientOrderId = clientOrderId;
    }

    /**
     * @return the ebayTargetOrderEntriesDTO
     */
    public EbayTargetOrderEntriesDTO getEbayTargetOrderEntriesDTO() {
        return ebayTargetOrderEntriesDTO;
    }

    /**
     * @param ebayTargetOrderEntriesDTO
     *            the ebayTargetOrderEntriesDTO to set
     */
    public void setEbayTargetOrderEntriesDTO(final EbayTargetOrderEntriesDTO ebayTargetOrderEntriesDTO) {
        this.ebayTargetOrderEntriesDTO = ebayTargetOrderEntriesDTO;
    }

    /**
     * @return the buyerIpAddress
     */
    public String getBuyerIpAddress() {
        return buyerIpAddress;
    }

    /**
     * @param buyerIpAddress
     *            the buyerIpAddress to set
     */
    public void setBuyerIpAddress(final String buyerIpAddress) {
        this.buyerIpAddress = buyerIpAddress;
    }

    /**
     * @return the salesChannel
     */
    public String getSalesChannel() {
        return salesChannel;
    }

    /**
     * @param salesChannel
     *            the salesChannel to set
     */
    public void setSalesChannel(final String salesChannel) {
        this.salesChannel = salesChannel;
    }

    /**
     * @return the currencyCode
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * @param currencyCode
     *            the currencyCode to set
     */
    public void setCurrencyCode(final String currencyCode) {
        this.currencyCode = currencyCode;
    }

}

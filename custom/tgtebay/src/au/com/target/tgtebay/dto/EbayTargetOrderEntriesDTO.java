/**
 * 
 */
package au.com.target.tgtebay.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author jjayawa1
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class EbayTargetOrderEntriesDTO {

    @XmlElement
    private List<EbayTargetOrderEntryDTO> entries;

    /**
     * @return the entries
     */
    public List<EbayTargetOrderEntryDTO> getEntries() {
        return entries;
    }

    /**
     * @param entries
     *            the entries to set
     */
    public void setEntries(final List<EbayTargetOrderEntryDTO> entries) {
        this.entries = entries;
    }

}

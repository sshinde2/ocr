/**
 * 
 */
package au.com.target.tgtebay.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;



/**
 * @author jjayawa1
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class EbayTargetOrderResponseDTO {

    @XmlElement
    private String orderNumber;

    @XmlElement
    private String orderStatus;

    /**
     * @return the orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * @return the orderStatus
     */
    public String getOrderStatus() {
        return orderStatus;
    }

    /**
     * @param orderNumber
     *            the orderNumber to set
     */
    public void setOrderNumber(final String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * @param orderStatus
     *            the orderStatus to set
     */
    public void setOrderStatus(final String orderStatus) {
        this.orderStatus = orderStatus;
    }
}

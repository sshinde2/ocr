/**
 * 
 */
package au.com.target.tgtebay.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author jjayawa1
 * 
 */
@XmlRootElement(name = "integration-ebayOrderResponses")
@XmlAccessorType(XmlAccessType.FIELD)
public class EbayTargetOrderResponsesDTO {

    private List<EbayTargetOrderResponseDTO> orderResponses;

    /**
     * @return the orderResponses
     */
    public List<EbayTargetOrderResponseDTO> getOrderResponses() {
        return orderResponses;
    }

    /**
     * @param orderResponses
     *            the orderResponses to set
     */
    public void setOrderResponses(final List<EbayTargetOrderResponseDTO> orderResponses) {
        this.orderResponses = orderResponses;
    }

}

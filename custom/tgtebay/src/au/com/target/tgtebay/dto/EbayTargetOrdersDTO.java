/**
 * 
 */
package au.com.target.tgtebay.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author mjanarth
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "eBayOrders")
public class EbayTargetOrdersDTO {

    @XmlElement
    private List<EbayTargetOrderDTO> eBayOrders;

    /**
     * @return the eBayOrders
     */
    public List<EbayTargetOrderDTO> geteBayOrders() {
        return eBayOrders;
    }

    /**
     * @param eBayOrders
     *            the eBayOrders to set
     */
    public void seteBayOrders(final List<EbayTargetOrderDTO> eBayOrders) {
        this.eBayOrders = eBayOrders;
    }

}

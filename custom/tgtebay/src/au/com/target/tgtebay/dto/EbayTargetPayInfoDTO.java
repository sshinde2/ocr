/**
 * 
 */
package au.com.target.tgtebay.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author mjanarth
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class EbayTargetPayInfoDTO {

    @XmlElement
    private String payerId;
    @XmlElement
    private String emailId;
    @XmlElement
    private String token;
    @XmlElement
    private String paymentType;
    @XmlElement
    private String receiptNumber;
    @XmlElement
    private String transactionId;
    @XmlElement
    private String plannedAmount;
    @XmlElement
    private String paypalAccountId;

    /**
     * @return the payerId
     */
    public String getPayerId() {
        return payerId;
    }

    /**
     * @param payerId
     *            the payerId to set
     */
    public void setPayerId(final String payerId) {
        this.payerId = payerId;
    }

    /**
     * @return the emailId
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * @param emailId
     *            the emailId to set
     */
    public void setEmailId(final String emailId) {
        this.emailId = emailId;
    }


    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token
     *            the token to set
     */
    public void setToken(final String token) {
        this.token = token;
    }

    /**
     * @return the paymentType
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * @param paymentType
     *            the paymentType to set
     */
    public void setPaymentType(final String paymentType) {
        this.paymentType = paymentType;
    }

    /**
     * @return the receiptNumber
     */
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * @param receiptNumber
     *            the receiptNumber to set
     */
    public void setReceiptNumber(final String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    /**
     * @return the transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * @param transactionId
     *            the transactionId to set
     */
    public void setTransactionId(final String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * @return the plannedAmount
     */
    public String getPlannedAmount() {
        return plannedAmount;
    }

    /**
     * @param plannedAmount
     *            the plannedAmount to set
     */
    public void setPlannedAmount(final String plannedAmount) {
        this.plannedAmount = plannedAmount;
    }

    /**
     * @return the paypalAccountId
     */
    public String getPaypalAccountId() {
        return paypalAccountId;
    }

    /**
     * @param paypalAccountId
     *            the paypalAccountId to set
     */
    public void setPaypalAccountId(final String paypalAccountId) {
        this.paypalAccountId = paypalAccountId;
    }

}

/**
 * 
 */
package au.com.target.tgtebay.logger;

import de.hybris.platform.commerceservices.enums.SalesApplication;

import java.text.MessageFormat;

import org.apache.log4j.Logger;


/**
 * @author ragarwa3
 *
 */
public class PartnerStatusUpdateLogger {

    public static final String LOG_READYFORPICKUP = "ReadyForPickup";
    public static final String LOG_PICKEDUP = "Pickedup";
    public static final String LOG_SHIPPED = "Shipped";

    private static final String LOGGING_PREFIX = "PartnerOrderStatusUpdate: ";

    private Logger log = null;

    public PartnerStatusUpdateLogger(final Class classname) {
        log = Logger.getLogger(classname);
    }

    public void logOrderStatusUpdateEntry(final String status, final SalesApplication salesApp,
            final String hybrisOrderNumber, final String partnerOrderNumber) {

        logOrderStatusUpdate("Start", status, salesApp, hybrisOrderNumber, partnerOrderNumber);
    }

    public void logOrderStatusUpdateSuccess(final String status, final SalesApplication salesApplication,
            final String hybrisOrderNumber, final String partnerOrderNumber) {

        logOrderStatusUpdate("Success", status, salesApplication, hybrisOrderNumber, partnerOrderNumber);
    }

    public void logOrderStatusUpdateFailure(final String status, final SalesApplication salesApplication,
            final String hybrisOrderNumber, final String partnerOrderNumber) {

        logOrderStatusUpdate("Failure", status, salesApplication, hybrisOrderNumber, partnerOrderNumber);
    }

    public void logOrderStatusUpdateSkip(final String status, final SalesApplication salesApplication,
            final String hybrisOrderNumber, final String partnerOrderNumber) {

        logOrderStatusUpdate("Skip", status, salesApplication, hybrisOrderNumber, partnerOrderNumber);
    }

    public void logRetryExceededError(final String status, final SalesApplication salesApplication,
            final String hybrisOrderNumber, final String partnerOrderNumber) {
        log.error(getLogMessage("RETRYEXCEEDED", status, salesApplication, hybrisOrderNumber, partnerOrderNumber));
    }

    private void logOrderStatusUpdate(final String state, final String status, final SalesApplication salesApp,
            final String hybrisOrderNumber, final String partnerOrderNumber) {

        log.info(getLogMessage(state, status, salesApp, hybrisOrderNumber, partnerOrderNumber));
    }

    private String getLogMessage(final String state, final String status, final SalesApplication salesApp,
            final String hybrisOrderNumber, final String partnerOrderNumber) {
        return LOGGING_PREFIX + MessageFormat.format(
                "state={0}, orderStatus={1}, salesApplication={2}, hybrisOrderNumber={3}, partnerOrderNumber={4}",
                state, status, salesApp, hybrisOrderNumber, partnerOrderNumber);
    }

}

/**
 * 
 */
package au.com.target.tgtebay.order;

import de.hybris.platform.core.model.order.OrderModel;

import au.com.target.tgtebay.dto.EbayTargetOrderDTO;
import au.com.target.tgtebay.exceptions.ImportOrderException;


/**
 * @author mjanarth
 * 
 */
public interface TargetPartnerOrderService {


    /**
     * 
     * @param order
     * @return orderId
     * @throws ImportOrderException
     */
    String createPartnerOrders(EbayTargetOrderDTO order)
            throws ImportOrderException;


    /**
     * This method force reserve the stock for partner order
     * 
     * @param orderModel
     * 
     */
    void reserveStockForPartnerOrder(OrderModel orderModel);
}

package au.com.target.tgtebay.order.impl;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.europe1.constants.Europe1Constants;
import de.hybris.platform.europe1.enums.UserTaxGroup;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.currency.conversion.service.CurrencyConversionFactorService;
import au.com.target.tgtcore.customer.TargetCustomerAccountService;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.CurrencyConversionFactorsModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.order.TargetCommerceCheckoutService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtebay.constants.TgtebayConstants;
import au.com.target.tgtebay.dto.EbayTargetOrderDTO;
import au.com.target.tgtebay.dto.EbayTargetOrderEntryDTO;
import au.com.target.tgtebay.exceptions.ImportOrderException;
import au.com.target.tgtebay.order.TargetPartnerOrderService;
import au.com.target.tgtebay.util.TargetPartnerOrderFieldPopulatorHelper;
import au.com.target.tgtebay.util.TargetPartnerOrderFieldValidator;
import au.com.target.tgtlayby.purchase.PurchaseOptionConfigService;
import au.com.target.tgtutility.util.TargetDateUtil;


/**
 * @author mjanarth
 * 
 */
public class TargetPartnerOrderServiceImpl extends AbstractBusinessService
        implements TargetPartnerOrderService {

    private static final Logger LOG = Logger.getLogger(TargetPartnerOrderServiceImpl.class);

    private static final String ERROR_MESSAGE_CREATE_ORDER_FAILED = "Failed creating order for partner partnerOrderNumber={0}, salesApplication={1}";

    private static final String ERROR_MESSAGE_RESERVATION_ORDER_FAILED = "Failed reserving the stock for partner Order={0}, productCode={1}";

    private static final String ERROR_MESSAGE_DUPLICATE_ORDER_FAILED = "Duplicate uid while creating customer model for partner order partnerOrderNumber={0}, salesApplication={1}";

    private static final String PARTNER_ORDER_IMPORT = "PartnerOrderImport: ";

    private TargetCustomerAccountService targetCustomerAccountService;

    private TargetDeliveryService targetDeliveryService;

    private PaymentModeService paymentModeService;

    private KeyGenerator orderCodeGenerator;

    private KeyGenerator customerIDGenerator;

    private TargetPartnerOrderFieldPopulatorHelper targetPartnerOrderFieldPopulatorHelper;

    private PurchaseOptionConfigService purchaseOptionConfigService;

    private TargetCommerceCheckoutService targetCommerceCheckoutService;

    private TargetStockService targetStockService;

    private TargetWarehouseService targetWarehouseService;

    private TargetPartnerOrderFieldValidator targetPartnerOrderFieldValidator;

    private BaseStoreService baseStoreService;

    private Map<String, String> paymentTypeToPaymentMethodMapping;

    private CurrencyConversionFactorService currencyConversionFactorService;

    private Map<String, Boolean> fulfillmentTypeToDeliveryToStoreMapping;

    private Map<String, Boolean> shippingClassToShortDeliveryTimeMapping;

    private TargetFeatureSwitchService targetFeatureSwitchService;


    /**
     * This method create orders from cart and kick off the business process in hybris which are successfully created in
     * partner
     * 
     * @param order
     * @return orderId
     * @throws ImportOrderException
     * 
     */
    @Override
    public String createPartnerOrders(final EbayTargetOrderDTO order)
            throws ImportOrderException {

        if (!targetPartnerOrderFieldValidator.isOrderDataValid(order)) {
            return null;
        }

        final CartModel cartModel = getModelService().create(CartModel.class);
        final String ebayOrderNumber = order.geteBayOrderNumber();
        final String salesChannel = order.getSalesChannel();
        cartModel.setPurchaseOptionConfig(purchaseOptionConfigService.getCurrentPurchaseOptionConfig());

        final String cartOrderNumber = generateCartOrderNumber();
        String orderNumber = null;
        cartModel.setDate(new Date());
        try {
            // create guest customer for partner order
            TargetCustomerModel customerModel = null;
            //Log a warning message if the email address is not valid
            if ((StringUtils.isNotEmpty(order.getBuyerEmail()))
                    && !(targetPartnerOrderFieldValidator.validateEmail(order.getBuyerEmail()))) {
                LOG.warn(MessageFormat.format(PARTNER_ORDER_IMPORT
                        + "Invalid email Address while creating Partner order, email={0}, partnerOrderNumber={1}",
                        order.getBuyerEmail(), ebayOrderNumber));
            }
            customerModel = targetCustomerAccountService
                    .registerGuestForAnonymousCheckout(StringUtils.trim(order.getBuyerEmail()),
                            TgtebayConstants.GUEST_NAME);
            customerModel.setCustomerID(String.valueOf(customerIDGenerator.generate()));
            customerModel.setEbayCustomer(Boolean.TRUE);
            cartModel.setUser(customerModel);
            cartModel.setCode(cartOrderNumber);

            final SalesApplication salesApplication = getSalesChannelFromDto(salesChannel);

            // create shipping and billing address
            if (order.getDeliveryAddress() != null) {
                populateAddressInCustomer(order, customerModel);
            }
            populateOrderDeliveryDetails(cartModel, order, salesApplication, customerModel);

            if (StringUtils.isNotEmpty(order.geteBayOrderNumber())) {
                cartModel.setEBayOrderNumber(Integer.valueOf(
                        order.geteBayOrderNumber()));
            }
            cartModel.setNet(Boolean.FALSE);
            cartModel.setCalculated(Boolean.TRUE);
            cartModel.setDeliveryCost(targetPartnerOrderFieldValidator.parseDouble(order.getDeliveryCost(),
                    TgtebayConstants.DELIVERY_COST,
                    ebayOrderNumber));

            // create order entries
            if (order.getEbayTargetOrderEntriesDTO().getEntries() != null) {
                cartModel.setEntries(targetPartnerOrderFieldPopulatorHelper
                        .populateOrderEntries(order.getEbayTargetOrderEntriesDTO().getEntries(), cartModel));
            }
            cartModel.setCurrency(targetPartnerOrderFieldPopulatorHelper.getCurrencyByCode(order.getCurrencyCode()));
            cartModel.setTotalDiscounts(targetPartnerOrderFieldValidator.parseDouble(order.getTotalDiscounts(),
                    TgtebayConstants.TOTAL_DISCOUNTS,
                    ebayOrderNumber));
            cartModel.setTotalPrice(targetPartnerOrderFieldValidator.parseDouble(order.getTotalPrice(),
                    TgtebayConstants.TOTAL_PRICE,
                    ebayOrderNumber));

            setPaymentInfo(order, cartModel, customerModel);

            //set the Tax group in session for tax calculation
            initializeSessionTaxGroup();
            final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
            checkoutParameter.setCart(cartModel);
            checkoutParameter.setSalesApplication(salesApplication);

            final CommerceOrderResult orderResult = targetCommerceCheckoutService
                    .placeOrder(checkoutParameter);
            final OrderModel orderModel = orderResult.getOrder();

            if (orderModel != null) {
                orderNumber = orderModel.getCode();
                // populating order's date from received createdDate and currency conversion factor
                setDateAndCurrencyConverionFactor(order, orderModel);
            }
            return orderNumber;
        }
        catch (final DuplicateUidException ex) {
            throw new ImportOrderException(MessageFormat.format(ERROR_MESSAGE_DUPLICATE_ORDER_FAILED, ebayOrderNumber,
                    salesChannel), ex);
        }
        catch (final TargetUnknownIdentifierException ex) {
            throw new ImportOrderException(MessageFormat.format(ERROR_MESSAGE_CREATE_ORDER_FAILED, ebayOrderNumber,
                    salesChannel), ex);
        }
        catch (final TargetAmbiguousIdentifierException ex) {
            throw new ImportOrderException(MessageFormat.format(ERROR_MESSAGE_CREATE_ORDER_FAILED, ebayOrderNumber,
                    salesChannel), ex);
        }
        catch (final InvalidCartException ex) {
            throw new ImportOrderException(MessageFormat.format(ERROR_MESSAGE_CREATE_ORDER_FAILED, ebayOrderNumber,
                    salesChannel), ex);
        }
        catch (final IllegalArgumentException ex) {
            throw new ImportOrderException(MessageFormat.format(ERROR_MESSAGE_CREATE_ORDER_FAILED, ebayOrderNumber,
                    salesChannel), ex);
        }
        catch (final ModelSavingException ex) {
            throw new ImportOrderException(MessageFormat.format(ERROR_MESSAGE_CREATE_ORDER_FAILED, ebayOrderNumber,
                    salesChannel), ex);
        }
    }

    /**
     * Generate cart order number.
     *
     * @return the string
     */
    protected String generateCartOrderNumber() {
        return String.valueOf(orderCodeGenerator.generate());
    }

    /**
     * Populate delivery mode and address details based on fulfillment type
     * 
     * @param cartModel
     * @param order
     * @param salesChannel
     * @param customerModel
     * @throws ImportOrderException
     */
    protected void populateOrderDeliveryDetails(final CartModel cartModel, final EbayTargetOrderDTO order,
            final SalesApplication salesChannel, final TargetCustomerModel customerModel) throws ImportOrderException,
            TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final Collection<TargetZoneDeliveryModeModel> deliveryModes = targetDeliveryService
                .getAllCurrentlyActiveDeliveryModes(salesChannel);
        if (CollectionUtils.isEmpty(deliveryModes)) {
            throw new ImportOrderException("No deliveryModes available for given sales channel="
                    + salesChannel);
        }
        final EbayTargetOrderEntryDTO orderEntry = order.getEbayTargetOrderEntriesDTO().getEntries().get(0);
        final String fulfillmentType = orderEntry.getFulfillmentType();

        if (StringUtils.isNotEmpty(fulfillmentType) &&
                BooleanUtils.isTrue(fulfillmentTypeToDeliveryToStoreMapping
                        .get(fulfillmentType))) {

            if (StringUtils.isEmpty(orderEntry.getCncStoreNumber())) {
                throw new ImportOrderException(
                        "Store number is empty for partner cnc order=" + order.geteBayOrderNumber());
            }

            final Integer storeNumber = NumberUtils.createInteger(orderEntry.getCncStoreNumber());
            final AddressModel customerAddress = customerModel.getDefaultShipmentAddress();
            targetCommerceCheckoutService.fillCart4ClickAndCollectWithoutRecalculate(cartModel, storeNumber,
                    customerAddress.getTitle(), customerAddress.getFirstname(), customerAddress.getLastname(),
                    customerAddress.getPhone1(), getCNCDeliveryMode(deliveryModes));

            if (null == cartModel.getCncStoreNumber()) {
                throw new ImportOrderException("No store found with storeNumber=" + storeNumber
                        + " for partner cnc order=" + order.geteBayOrderNumber());
            }
        }
        else {
            String shippingClass = null;
            if (null != order.getDeliveryAddress()) {
                shippingClass = order.getDeliveryAddress().getShippingClass();
            }

            TargetZoneDeliveryModeModel deliveryMode = null;

            if (StringUtils.isNotBlank(shippingClass)
                    && BooleanUtils.isTrue(shippingClassToShortDeliveryTimeMapping.get(shippingClass))) {
                deliveryMode = getExpressDeliveryMode(deliveryModes);
                if (null == deliveryMode) {
                    LOG.error(MessageFormat
                            .format(PARTNER_ORDER_IMPORT
                                    + "No Active Express Delivery Mode found for given shipping class, shippingClass={0}, partnerOrderNumber={1}",
                                    shippingClass, order.geteBayOrderNumber()));

                }
            }

            if (null == deliveryMode) {
                deliveryMode = getHomeDeliveryMode(deliveryModes);
            }

            cartModel.setDeliveryMode(deliveryMode);
            cartModel.setDeliveryAddress(customerModel.getDefaultShipmentAddress());
        }

    }



    /**
     * Retrieve Express Delivery Mode
     * 
     * @param deliveryModes
     * @return TargetZoneDeliveryModeModel
     */
    private TargetZoneDeliveryModeModel getExpressDeliveryMode(
            final Collection<TargetZoneDeliveryModeModel> deliveryModes) {
        TargetZoneDeliveryModeModel delMode = null;
        for (final TargetZoneDeliveryModeModel deliveryMode : deliveryModes) {
            if (BooleanUtils.isTrue(deliveryMode.getIsShortDeliveryTime())) {
                delMode = deliveryMode;
            }
        }
        return delMode;
    }

    /**
     * Retrieve HM Delivery Mode
     * 
     * @param deliveryModes
     * @return TargetZoneDeliveryModeModel
     */
    private TargetZoneDeliveryModeModel getHomeDeliveryMode(
            final Collection<TargetZoneDeliveryModeModel> deliveryModes) {
        TargetZoneDeliveryModeModel delMode = null;
        for (final TargetZoneDeliveryModeModel deliveryMode : deliveryModes) {
            if (BooleanUtils.isNotTrue(deliveryMode.getIsDeliveryToStore())
                    && BooleanUtils.isNotTrue(deliveryMode.getIsShortDeliveryTime())) {
                delMode = deliveryMode;
            }
        }
        return delMode;
    }

    /**
     * Retrieve CNC Delivery Mode
     * 
     * @param deliveryModes
     */
    private TargetZoneDeliveryModeModel getCNCDeliveryMode(
            final Collection<TargetZoneDeliveryModeModel> deliveryModes) {
        TargetZoneDeliveryModeModel delMode = null;
        for (final TargetZoneDeliveryModeModel deliveryMode : deliveryModes) {
            if (BooleanUtils.isTrue(deliveryMode.getIsDeliveryToStore())) {
                delMode = deliveryMode;
            }
        }
        return delMode;
    }


    /**
     * Gets the sales channel from dto.
     * 
     * @param salesChannelString
     *
     * @return the sales channel from dto
     */
    protected SalesApplication getSalesChannelFromDto(final String salesChannelString) {

        // null/blank sales channel will be treated as eBay
        if (StringUtils.isBlank(salesChannelString)) {
            return SalesApplication.EBAY;
        }
        else {
            return SalesApplication.valueOf(salesChannelString);
        }
    }

    private void setPaymentInfo(final EbayTargetOrderDTO order, final CartModel cartModel,
            final TargetCustomerModel customerModel) {

        if (null != order.getPaymentInfo()) {
            final PaymentModeModel paymentMode = getPaymentMode(order.getPaymentInfo().getPaymentType());
            cartModel.setPaymentMode(paymentMode);

            if (null != paymentMode) {
                final PaymentInfoModel paymentInfoModel = targetPartnerOrderFieldPopulatorHelper
                        .populatePaymentInfo(paymentMode.getCode(), order.getPaymentInfo(),
                                customerModel);

                paymentInfoModel.setCode((customerModel.getUid()
                        + TgtCoreConstants.Seperators.UNDERSCORE + UUID
                        .randomUUID()));
                cartModel.setPaymentInfo(paymentInfoModel);

                cartModel.setPaymentAddress(cartModel.getPaymentInfo()
                        .getBillingAddress());
                cartModel.getPaymentAddress().setOwner(paymentInfoModel);

                cartModel.setPaymentTransactions(targetPartnerOrderFieldPopulatorHelper.populatePaymentTransaction(
                        order, cartModel, paymentInfoModel, customerModel.getUid()));
            }
            else {
                LOG.error(
                        MessageFormat
                                .format(PARTNER_ORDER_IMPORT
                                        + "No payment mode found for given payment type, paymentType={0}, partnerOrderNumber={1}",
                                        order.getPaymentInfo().getPaymentType(), order.geteBayOrderNumber()));
            }
        }
    }

    private void populateAddressInCustomer(final EbayTargetOrderDTO order, final TargetCustomerModel customerModel) {
        final AddressModel shippingAddress = targetPartnerOrderFieldPopulatorHelper
                .populateAddressModel(order.getDeliveryAddress(), order.geteBayOrderNumber());
        final ArrayList<AddressModel> addresses = new ArrayList<>();

        shippingAddress.setOwner(customerModel);
        shippingAddress.setShippingAddress(Boolean.TRUE);
        shippingAddress.setBillingAddress(Boolean.FALSE);
        customerModel.setDefaultShipmentAddress(shippingAddress);
        addresses.add(shippingAddress);
        customerModel.setFirstname(shippingAddress.getFirstname());
        customerModel.setLastname(shippingAddress.getLastname());
        customerModel.setAddresses(addresses);

        if (!(targetPartnerOrderFieldValidator.isEmptyAddress(order.getPaymentAddress()))) {
            final AddressModel billingAddress = targetPartnerOrderFieldPopulatorHelper
                    .populateAddressModel(order.getPaymentAddress(), order.geteBayOrderNumber());
            billingAddress.setBillingAddress(Boolean.TRUE);
            billingAddress.setShippingAddress(Boolean.FALSE);
            addresses.add(billingAddress);
            customerModel.setDefaultPaymentAddress(billingAddress);
        }
        //if the billing address is not provided set shipping address as default payment address
        else {
            customerModel.setDefaultPaymentAddress(shippingAddress);
        }

    }

    /**
     * Method to populate order's date and currency conversion factor
     * 
     * @param order
     * @param orderModel
     */
    private void setDateAndCurrencyConverionFactor(final EbayTargetOrderDTO order, final OrderModel orderModel) {
        final Date orderCreationDate = TargetDateUtil.getStringAsDate(order.getCreatedDate());
        if (null != orderCreationDate) {
            orderModel.setDate(orderCreationDate);
            final SalesApplication salesApplication = orderModel.getSalesApplication();
            if (SalesApplication.TRADEME.equals(salesApplication)) {
                final CurrencyConversionFactorsModel conversionModel = currencyConversionFactorService
                        .findLatestCurrencyFactor(orderModel);
                if (null != conversionModel) {
                    orderModel.setCurrencyConversionFactor(conversionModel.getCurrencyConversionFactor());
                }
            }
            getModelService().save(orderModel);
        }
    }


    /**
     * 
     * @param orderModel
     */
    @Override
    public void reserveStockForPartnerOrder(final OrderModel orderModel) {

        final List<AbstractOrderEntryModel> orderEntryModelList = orderModel.getEntries();

        if (CollectionUtils.isNotEmpty(orderEntryModelList)) {
            for (final AbstractOrderEntryModel orderEntry : orderEntryModelList) {
                final ProductModel product = orderEntry.getProduct();
                final Long am = orderEntry.getQuantity();
                final int amount = am.intValue();
                reserveStock(product, amount, orderModel.getCode());
            }
        }
    }

    private void reserveStock(final ProductModel product, final int amount, final String orderCode) {
        final String reservationMessage = "We have reserved " + amount + " of " + product.getName()
                + " from partner order number is " + orderCode;
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)) {
            try {
                targetStockService.reserve(product, amount, reservationMessage);
            }
            catch (final InsufficientStockLevelException e) {
                LOG.error(MessageFormat.format(ERROR_MESSAGE_CREATE_ORDER_FAILED
                        + ERROR_MESSAGE_RESERVATION_ORDER_FAILED, orderCode, product.getCode()), e);
            }
        }
        else {
            targetStockService.forceReserve(product, targetWarehouseService.getDefaultOnlineWarehouse(), amount,
                    reservationMessage);
        }
    }

    private PaymentModeModel getPaymentMode(final String paymentType) {
        final PaymentModeModel paymentMode = paymentModeService
                .getPaymentModeForCode(paymentTypeToPaymentMethodMapping.get(paymentType));
        return paymentMode;
    }

    private void initializeSessionTaxGroup() {
        final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
        if (currentBaseStore != null) {
            final UserTaxGroup taxGroup = currentBaseStore.getTaxGroup();
            if (taxGroup != null) {
                getSessionService().setAttribute(Europe1Constants.PARAMS.UTG, taxGroup);
            }
        }
    }

    /**
     * @param paymentModeService
     *            the paymentModeService to set
     */
    @Required
    public void setPaymentModeService(final PaymentModeService paymentModeService) {
        this.paymentModeService = paymentModeService;
    }

    /**
     * @param orderCodeGenerator
     *            the orderCodeGenerator to set
     */
    @Required
    public void setOrderCodeGenerator(final KeyGenerator orderCodeGenerator) {
        this.orderCodeGenerator = orderCodeGenerator;
    }

    /**
     * @param customerIDGenerator
     *            the customerIDGenerator to set
     */
    @Required
    public void setCustomerIDGenerator(final KeyGenerator customerIDGenerator) {
        this.customerIDGenerator = customerIDGenerator;
    }


    /**
     * @param purchaseOptionConfigService
     *            the purchaseOptionConfigService to set
     */
    @Required
    public void setPurchaseOptionConfigService(final PurchaseOptionConfigService purchaseOptionConfigService) {
        this.purchaseOptionConfigService = purchaseOptionConfigService;
    }

    /**
     * @param targetStockService
     *            the targetStockService to set
     */
    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }

    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */
    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }

    /**
     * 
     * @param targetCustomerAccountService
     */
    @Required
    public void setTargetCustomerAccountService(
            final TargetCustomerAccountService targetCustomerAccountService) {
        this.targetCustomerAccountService = targetCustomerAccountService;
    }

    /**
     * @param targetCommerceCheckoutService
     *            the targetCommerceCheckoutService to set
     */
    @Required
    public void setTargetCommerceCheckoutService(final TargetCommerceCheckoutService targetCommerceCheckoutService) {
        this.targetCommerceCheckoutService = targetCommerceCheckoutService;
    }

    /**
     * @param targetDeliveryService
     *            the targetDeliveryService to set
     */
    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        this.targetDeliveryService = targetDeliveryService;
    }

    /**
     * @param baseStoreService
     *            the baseStoreService to set
     */
    @Required
    public void setBaseStoreService(final BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    /**
     * @param paymentTypeToPaymentMethodMapping
     *            the paymentTypeToPaymentMethodMapping to set
     */
    @Required
    public void setPaymentTypeToPaymentMethodMapping(final Map<String, String> paymentTypeToPaymentMethodMapping) {
        this.paymentTypeToPaymentMethodMapping = paymentTypeToPaymentMethodMapping;
    }

    /**
     * @param currencyConversionFactorService
     *            the currencyConversionFactorService to set
     */
    @Required
    public void setCurrencyConversionFactorService(
            final CurrencyConversionFactorService currencyConversionFactorService) {
        this.currencyConversionFactorService = currencyConversionFactorService;
    }

    /**
     * @param fulfillmentTypeToDeliveryToStoreMapping
     *            the fulfillmentTypeToDeliveryToStoreMapping to set
     */
    @Required
    public void setFulfillmentTypeToDeliveryToStoreMapping(
            final Map<String, Boolean> fulfillmentTypeToDeliveryToStoreMapping) {
        this.fulfillmentTypeToDeliveryToStoreMapping = fulfillmentTypeToDeliveryToStoreMapping;
    }

    /**
     * @param shippingClassToShortDeliveryTimeMapping
     *            the shippingClassToShortDeliveryTimeMapping to set
     */
    @Required
    public void setShippingClassToShortDeliveryTimeMapping(
            final Map<String, Boolean> shippingClassToShortDeliveryTimeMapping) {
        this.shippingClassToShortDeliveryTimeMapping = shippingClassToShortDeliveryTimeMapping;
    }

    /**
     * @param targetPartnerOrderFieldPopulatorHelper
     *            the targetPartnerOrderFieldPopulatorHelper to set
     */
    @Required
    public void setTargetPartnerOrderFieldPopulatorHelper(
            final TargetPartnerOrderFieldPopulatorHelper targetPartnerOrderFieldPopulatorHelper) {
        this.targetPartnerOrderFieldPopulatorHelper = targetPartnerOrderFieldPopulatorHelper;
    }

    /**
     * @param targetPartnerOrderFieldValidator
     *            the targetPartnerOrderFieldValidator to set
     */
    @Required
    public void setTargetPartnerOrderFieldValidator(
            final TargetPartnerOrderFieldValidator targetPartnerOrderFieldValidator) {
        this.targetPartnerOrderFieldValidator = targetPartnerOrderFieldValidator;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

}

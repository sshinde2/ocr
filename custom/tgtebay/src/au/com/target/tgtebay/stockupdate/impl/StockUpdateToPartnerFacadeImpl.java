/**
 * 
 */
package au.com.target.tgtebay.stockupdate.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.stock.exception.StockLevelNotFoundException;

import java.text.MessageFormat;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.stockupdate.TargetInventoryService;
import au.com.target.tgtebay.stockupdate.StockUpdateToPartnerFacade;


/**
 * @author bhuang3
 * 
 */
public class StockUpdateToPartnerFacadeImpl implements StockUpdateToPartnerFacade {

    private static final Logger LOG = Logger.getLogger(StockUpdateToPartnerFacadeImpl.class);
    private static final String ERR_STOCKUPDATE_PRODUCT_AVAILABLE = "Product was not found product={0}";
    private static final String ERR_STOCKLEVEL_PRODUCT = "could not get stock level";

    private ProductService productService;
    private TargetInventoryService targetInventoryService;
    private TargetStockService targetStockService;

    @Override
    public boolean updateStock(final String productCode, final int quantity) {
        ProductModel productModel = null;
        try {
            productModel = productService.getProductForCode(productCode);
        }
        catch (final UnknownIdentifierException e) {
            LOG.error(MessageFormat.format(ERR_STOCKUPDATE_PRODUCT_AVAILABLE, productCode));
            return false;
        }
        if (isProductAvailableForPartner(productModel)) {
            return targetInventoryService.updateInventoryItemQuantity(productCode, quantity);
        }
        return false;
    }

    @Override
    public boolean updateAvailableStock(final String productCode) {

        ProductModel productModel = null;
        try {
            productModel = productService.getProductForCode(productCode);
        }
        catch (final UnknownIdentifierException e) {
            LOG.error(MessageFormat.format(ERR_STOCKUPDATE_PRODUCT_AVAILABLE, productCode));
            return false;
        }
        if (isProductAvailableForPartner(productModel)) {
            int availableStock = 0;
            try {
                LOG.info("Updating third party of stock level for the product=" + productCode);
                availableStock = targetStockService.getStockLevelAmountFromOnlineWarehouses(productModel);
            }
            catch (final StockLevelNotFoundException e) {
                LOG.warn(ERR_STOCKLEVEL_PRODUCT, e);
                return false;
            }
            return updateStock(productCode, availableStock);
        }
        return false;

    }

    private boolean isProductAvailableForPartner(final ProductModel productModel) {
        if (productModel instanceof TargetColourVariantProductModel
                || productModel instanceof TargetSizeVariantProductModel) {
            final AbstractTargetVariantProductModel abstractProductModel = (AbstractTargetVariantProductModel)productModel;
            if (abstractProductModel.getAvailableOnEbay() != null
                    && abstractProductModel.getAvailableOnEbay().booleanValue()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param productService
     *            the productService to set
     */
    @Required
    public void setProductService(final ProductService productService) {
        this.productService = productService;
    }

    /**
     * @param targetInventoryService
     *            the targetInventoryService to set
     */
    @Required
    public void setTargetInventoryService(final TargetInventoryService targetInventoryService) {
        this.targetInventoryService = targetInventoryService;
    }

    /**
     * @param targetStockService
     *            the targetStockService to set
     */
    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }

}

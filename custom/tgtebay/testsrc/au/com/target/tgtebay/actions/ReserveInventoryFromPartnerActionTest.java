/**
 * 
 */
package au.com.target.tgtebay.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Sets;

import au.com.target.tgtcore.model.SalesApplicationConfigModel;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import au.com.target.tgtebay.order.TargetPartnerOrderService;


/**
 * @author Trinadh
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ReserveInventoryFromPartnerActionTest {

    @Mock
    private OrderProcessModel orderProcess;

    @Mock
    private OrderModel order;

    @Mock
    private TargetPartnerOrderService targetPartnerOrderService;

    @Mock
    private TargetSalesApplicationConfigService salesApplicationConfigService;

    @InjectMocks
    private final ReserveInventoryFromPartnerAction reserveInventoryFromPartnerAction = new ReserveInventoryFromPartnerAction();

    private final SalesApplicationConfigModel ebaySalesApplicationConfigModel = new SalesApplicationConfigModel();

    private final TargetZoneDeliveryModeModel ebayHomeDeliveryMode = new TargetZoneDeliveryModeModel();

    private final TargetZoneDeliveryModeModel ebaycncDeliveryMode = new TargetZoneDeliveryModeModel();

    @Before
    public void setUp() {
        Mockito.when(orderProcess.getOrder()).thenReturn(order);

        ebaySalesApplicationConfigModel.setSalesApplication(SalesApplication.EBAY);

        ebaySalesApplicationConfigModel.setSkipPartnerInventoryUpdateForDelModes(Sets.newHashSet(ebayHomeDeliveryMode));
        Mockito.when(salesApplicationConfigService.getConfigForSalesApplication(SalesApplication.EBAY))
                .thenReturn(ebaySalesApplicationConfigModel);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteActionNullOrderProcess() throws Exception {
        reserveInventoryFromPartnerAction.executeAction(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteActionNullOrder() throws Exception {
        Mockito.when(orderProcess.getOrder()).thenReturn(null);
        reserveInventoryFromPartnerAction.executeAction(orderProcess);
    }

    @Test
    public void testExecuteActionReserveStock() throws Exception {

        final SalesApplication salesApp = SalesApplication.EBAY;
        Mockito.when(order.getSalesApplication()).thenReturn(salesApp);
        Mockito.doReturn(Boolean.TRUE).when(salesApplicationConfigService)
                .isReserveStockOnAcceptOrder(salesApp);
        Mockito.when(order.getDeliveryMode()).thenReturn(ebayHomeDeliveryMode);
        reserveInventoryFromPartnerAction.executeAction(orderProcess);

        Mockito.verify(targetPartnerOrderService).reserveStockForPartnerOrder(order);
        Mockito.verify(order, Mockito.never()).getEntries();
    }

    @Test
    public void testExecuteActionNotReserveStock() throws Exception {

        final SalesApplication salesApp = SalesApplication.WEB;
        Mockito.when(order.getSalesApplication()).thenReturn(salesApp);
        Mockito.doReturn(Boolean.FALSE).when(salesApplicationConfigService)
                .isReserveStockOnAcceptOrder(salesApp);

        reserveInventoryFromPartnerAction.executeAction(orderProcess);

        Mockito.verifyZeroInteractions(targetPartnerOrderService);
    }

    @Test
    public void testExecuteForEbayCNC() throws Exception {

        final SalesApplication salesApp = SalesApplication.EBAY;
        Mockito.when(order.getSalesApplication()).thenReturn(salesApp);
        Mockito.doReturn(Boolean.TRUE).when(salesApplicationConfigService)
                .isReserveStockOnAcceptOrder(salesApp);
        Mockito.when(order.getDeliveryMode()).thenReturn(ebaycncDeliveryMode);

        reserveInventoryFromPartnerAction.executeAction(orderProcess);

        Mockito.verify(targetPartnerOrderService).reserveStockForPartnerOrder(order);
    }

}

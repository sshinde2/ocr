/**
 * 
 */
package au.com.target.tgtebay.actions;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.SetUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Sets;

import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.SalesApplicationConfigModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.stockupdate.TargetInventoryService;
import au.com.target.tgtebay.order.TargetPartnerOrderService;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.service.FluentStockLookupService;


/**
 * @author Trinadh
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UpdateInventoryToPartnerActionTest {

    @Mock
    private OrderProcessModel orderProcess;

    @Mock
    private OrderModel order;

    @Mock
    private TargetPartnerOrderService targetPartnerOrderService;

    @Mock
    private TargetStockService targetStockService;

    @Mock
    private TargetInventoryService targetCaInventoryService;

    @Mock
    private TargetSalesApplicationConfigService salesApplicationConfigService;

    @Mock
    private FluentStockLookupService fluentStockLookupService;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @InjectMocks
    @Spy
    private final UpdateInventoryToPartnerAction updateInventoryToPartnerAction = new UpdateInventoryToPartnerAction();

    @Mock
    private SalesApplicationConfigModel ebaySalesApplicationConfigModel;

    @Mock
    private TargetZoneDeliveryModeModel ebayHomeDeliveryMode;


    @Before
    public void setUp() {
        given(orderProcess.getOrder()).willReturn(order);

        given(order.getSalesApplication()).willReturn(SalesApplication.EBAY);
        given(order.getDeliveryMode()).willReturn(ebayHomeDeliveryMode);

        given(ebaySalesApplicationConfigModel.getSalesApplication()).willReturn(SalesApplication.EBAY);
        given(ebaySalesApplicationConfigModel.getSkipPartnerInventoryUpdateForDelModes())
                .willReturn(Sets.newHashSet(ebayHomeDeliveryMode));
        given(salesApplicationConfigService.getConfigForSalesApplication(SalesApplication.EBAY))
                .willReturn(ebaySalesApplicationConfigModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteNullOrderProcess() throws Exception {
        updateInventoryToPartnerAction.executeInternal(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteNullOrder() throws Exception {
        given(orderProcess.getOrder()).willReturn(null);
        updateInventoryToPartnerAction.executeInternal(orderProcess);
    }

    @Test
    public void testExecuteNullSalesAppConfig() throws Exception {

        willReturn(null).given(salesApplicationConfigService)
                .getConfigForSalesApplication(SalesApplication.EBAY);

        assertThat(updateInventoryToPartnerAction.executeInternal(orderProcess))
                .isEqualTo(UpdateInventoryToPartnerAction.Transition.OK.toString());

        verify(order).getEntries();
        verifyZeroInteractions(targetStockService);
        verifyZeroInteractions(targetCaInventoryService);
    }

    @Test
    public void testExecuteSalesAppConfigWithEmptyDelModes() throws Exception {
        given(ebaySalesApplicationConfigModel.getSkipPartnerInventoryUpdateForDelModes())
                .willReturn(SetUtils.EMPTY_SET);

        assertThat(updateInventoryToPartnerAction.executeInternal(orderProcess))
                .isEqualTo(UpdateInventoryToPartnerAction.Transition.OK.toString());

        verify(order).getEntries();
        verifyZeroInteractions(targetStockService);
        verifyZeroInteractions(targetCaInventoryService);
    }

    @Test
    public void testExecuteSalesAppConfigWithNonSkipDelMode() throws Exception {

        given(order.getDeliveryMode()).willReturn(new DeliveryModeModel());

        assertThat(updateInventoryToPartnerAction.executeInternal(orderProcess))
                .isEqualTo(UpdateInventoryToPartnerAction.Transition.OK.toString());

        verify(order).getEntries();
        verifyZeroInteractions(targetStockService);
        verifyZeroInteractions(targetCaInventoryService);
    }

    @Test
    public void testExecuteSalesAppConfigWithSkipDelMode() throws Exception {

        assertThat(updateInventoryToPartnerAction.executeInternal(orderProcess))
                .isEqualTo(UpdateInventoryToPartnerAction.Transition.OK.toString());

        verify(order, never()).getEntries();
        verifyZeroInteractions(targetStockService);
        verifyZeroInteractions(targetCaInventoryService);
    }

    @Test(expected = RetryLaterException.class)
    public void testExecuteServiceFailure() throws Exception {

        final TargetSizeVariantProductModel product = new TargetSizeVariantProductModel();
        product.setAvailableOnEbay(Boolean.TRUE);
        product.setCode("12345678");

        final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();
        entry.setProduct(product);

        given(order.getEntries()).willReturn(Collections.singletonList(entry));
        given(order.getDeliveryMode()).willReturn(new DeliveryModeModel());

        willReturn(Integer.valueOf(10)).given(targetStockService)
                .getStockLevelAmountFromOnlineWarehouses(product);
        willReturn(Boolean.FALSE).given(targetCaInventoryService).updateInventoryItemQuantity("12345678", 10);

        updateInventoryToPartnerAction.executeInternal(orderProcess);
    }

    @Test
    public void testExecute() throws Exception {
        given(ebaySalesApplicationConfigModel.getSkipPartnerInventoryUpdateForDelModes())
                .willReturn(SetUtils.EMPTY_SET);
        willReturn("Order01").given(order).getCode();
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries.add(mock(AbstractOrderEntryModel.class));
        willReturn(orderEntries).given(order).getEntries();
        final Map<String, Integer> stockMap = mock(Map.class);
        willReturn(stockMap).given(updateInventoryToPartnerAction).getStockMap(orderEntries);

        final String transition = updateInventoryToPartnerAction.executeInternal(orderProcess);
        verify(updateInventoryToPartnerAction).logInventoryUpdate(stockMap, "Order01");
        assertThat(transition).isEqualTo("OK");
        verify(updateInventoryToPartnerAction, never()).getStockMapUsingFluent(Mockito.anyList());
    }

    @Test
    public void testExecuteSkipUpdate() throws Exception {
        willReturn("Order01").given(order).getCode();
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries.add(mock(AbstractOrderEntryModel.class));
        willReturn(orderEntries).given(order).getEntries();
        final Map<String, Integer> stockMap = mock(Map.class);
        willReturn(stockMap).given(updateInventoryToPartnerAction).getStockMap(orderEntries);

        final String transition = updateInventoryToPartnerAction.executeInternal(orderProcess);
        verify(updateInventoryToPartnerAction, never()).logInventoryUpdate(stockMap, "Order01");
        assertThat(transition).isEqualTo("OK");
        verify(updateInventoryToPartnerAction, never()).getStockMapUsingFluent(Mockito.anyList());
    }

    @Test
    public void testExecuteUsingFluent() throws Exception {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService).isFeatureEnabled("fluentStockLookup");
        given(ebaySalesApplicationConfigModel.getSkipPartnerInventoryUpdateForDelModes())
                .willReturn(SetUtils.EMPTY_SET);
        willReturn("Order01").given(order).getCode();
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries.add(mock(AbstractOrderEntryModel.class));
        willReturn(orderEntries).given(order).getEntries();
        final Map<String, Integer> stockMap = mock(Map.class);
        willReturn(stockMap).given(updateInventoryToPartnerAction).getStockMapUsingFluent(orderEntries);

        final String transition = updateInventoryToPartnerAction.executeInternal(orderProcess);
        verify(updateInventoryToPartnerAction).logInventoryUpdate(stockMap, "Order01");
        assertThat(transition).isEqualTo("OK");
        verify(updateInventoryToPartnerAction, never()).getStockMap(Mockito.anyList());
    }

    @Test
    public void testGetStockMap() {
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        final AbstractOrderEntryModel entry = mock(AbstractOrderEntryModel.class);
        final AbstractTargetVariantProductModel product = mock(AbstractTargetVariantProductModel.class);
        given(product.getCode()).willReturn("P01");
        given(product.getAvailableOnEbay()).willReturn(Boolean.TRUE);
        given(entry.getProduct()).willReturn(product);
        orderEntries.add(entry);
        willReturn(orderEntries).given(order).getEntries();
        willReturn(Integer.valueOf(10)).given(targetStockService).getStockLevelAmountFromOnlineWarehouses(product);

        final Map<String, Integer> stockMap = updateInventoryToPartnerAction.getStockMap(orderEntries);
        assertThat(stockMap).isNotEmpty().hasSize(1);
        assertThat(stockMap.keySet()).contains("P01");
        assertThat(stockMap.values()).contains(Integer.valueOf(10));
    }

    @Test
    public void testGetStockMapEmpty() {
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        final AbstractOrderEntryModel entry = mock(AbstractOrderEntryModel.class);
        final AbstractTargetVariantProductModel product = mock(AbstractTargetVariantProductModel.class);
        given(product.getCode()).willReturn("P01");
        given(entry.getProduct()).willReturn(product);
        orderEntries.add(entry);
        willReturn(orderEntries).given(order).getEntries();
        willReturn(Integer.valueOf(10)).given(targetStockService).getStockLevelAmountFromOnlineWarehouses(product);

        final Map<String, Integer> stockMap = updateInventoryToPartnerAction.getStockMap(orderEntries);
        assertThat(stockMap).isEmpty();
    }

    @Test
    public void testGetStockMapUsingFluent() throws FluentClientException {
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        final AbstractOrderEntryModel entry = mock(AbstractOrderEntryModel.class);
        final AbstractTargetVariantProductModel product = mock(AbstractTargetVariantProductModel.class);
        given(product.getCode()).willReturn("P01");
        given(product.getAvailableOnEbay()).willReturn(Boolean.TRUE);
        given(entry.getProduct()).willReturn(product);
        orderEntries.add(entry);
        willReturn(orderEntries).given(order).getEntries();
        final ArgumentCaptor<Collection> skuCodesCaptor = ArgumentCaptor.forClass(Collection.class);
        final Map<String, Integer> expectedStockMap = new HashMap<>();
        expectedStockMap.put("P01", Integer.valueOf(10));
        willReturn(expectedStockMap).given(fluentStockLookupService).lookupStock(skuCodesCaptor.capture());

        final Map<String, Integer> stockMap = updateInventoryToPartnerAction.getStockMapUsingFluent(orderEntries);
        assertThat(stockMap).isEqualTo(expectedStockMap);
        assertThat(skuCodesCaptor.getValue()).contains("P01");
    }

    @Test
    public void testGetStockMapUsingFluentEmpty() throws FluentClientException {
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        final AbstractOrderEntryModel entry = mock(AbstractOrderEntryModel.class);
        final AbstractTargetVariantProductModel product = mock(AbstractTargetVariantProductModel.class);
        given(product.getCode()).willReturn("P01");
        given(entry.getProduct()).willReturn(product);
        orderEntries.add(entry);
        willReturn(orderEntries).given(order).getEntries();
        final ArgumentCaptor<Collection> skuCodesCaptor = ArgumentCaptor.forClass(Collection.class);
        final Map<String, Integer> expectedStockMap = new HashMap<>();
        willReturn(expectedStockMap).given(fluentStockLookupService).lookupStock(skuCodesCaptor.capture());

        final Map<String, Integer> stockMap = updateInventoryToPartnerAction.getStockMapUsingFluent(orderEntries);
        assertThat(stockMap).isEqualTo(expectedStockMap);
        assertThat(skuCodesCaptor.getValue()).isEmpty();
    }
}

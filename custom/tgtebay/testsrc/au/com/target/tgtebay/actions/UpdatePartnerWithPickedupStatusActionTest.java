/**
 * 
 */
package au.com.target.tgtebay.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtwebmethods.ca.TargetCaShippingService;


/**
 * @author pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UpdatePartnerWithPickedupStatusActionTest {

    @InjectMocks
    private final UpdatePartnerWithPickedupStatusAction action = new UpdatePartnerWithPickedupStatusAction();

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private TargetCaShippingService targetCaShippingService;

    @Mock
    private OrderProcessModel orderProcessModel;

    @Mock
    private OrderModel orderModel;

    @Before
    public void setUp() {
        Mockito.when(orderProcessModel.getOrder()).thenReturn(orderModel);
        Mockito.when(Boolean.valueOf(orderProcessParameterHelper.isPartnerUpdateRequired(orderProcessModel)))
                .thenReturn(Boolean.TRUE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteActionWhenOrderModelIsNull() throws RetryLaterException, Exception {
        Mockito.when(orderProcessModel.getOrder()).thenReturn(null);
        action.executeInternal(orderProcessModel);
    }

    @Test
    public void testExecuteActionWhenPartnerIsUpdated() throws RetryLaterException, Exception {
        Mockito.when(Boolean
                .valueOf(targetCaShippingService.submitCncOrderStatusForPickedupUpdate(orderProcessModel.getOrder())))
                .thenReturn(Boolean.TRUE);
        final String result = action.executeInternal(orderProcessModel);
        Assert.assertEquals("OK", result);
    }


    @Test
    public void testExecuteActionWhenPartnerIsNotUpdated() throws RetryLaterException, Exception {
        Mockito.when(Boolean.valueOf(orderProcessParameterHelper.isPartnerUpdateRequired(orderProcessModel)))
                .thenReturn(Boolean.FALSE);
        action.executeInternal(orderProcessModel);

        Mockito.verifyZeroInteractions(targetCaShippingService);
    }

    @Test(expected = RetryLaterException.class)
    public void testExecuteServiceFailure() throws Exception {

        Mockito.when(Boolean
                .valueOf(targetCaShippingService.submitCncOrderStatusForPickedupUpdate(orderProcessModel.getOrder())))
                .thenReturn(Boolean.FALSE);
        action.executeInternal(orderProcessModel);
    }

}

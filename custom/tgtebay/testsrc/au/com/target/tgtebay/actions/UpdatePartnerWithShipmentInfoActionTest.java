/**
 * 
 */
package au.com.target.tgtebay.actions;

import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import au.com.target.tgtwebmethods.ca.TargetCaShippingService;


/**
 * Test class for UpdatePartnerWithShipmentInfoAction
 * 
 * @author siddharam
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UpdatePartnerWithShipmentInfoActionTest {

    @InjectMocks
    private final UpdatePartnerWithShipmentInfoAction updatePartnerWithShipmentInfoAction = new UpdatePartnerWithShipmentInfoAction();

    @Mock
    private OrderProcessModel orderProcess;

    @Mock
    private OrderModel order;

    @Mock
    private TargetZoneDeliveryModeModel deliveryMode;

    @Mock
    private TargetCaShippingService targetCaShippingService;

    @Mock
    private TargetSalesApplicationConfigService targetSalesApplicationConfigService;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Before
    public void setUp() {
        when(orderProcess.getOrder()).thenReturn(order);
        when(order.getDeliveryMode()).thenReturn(deliveryMode);
        when(Boolean.valueOf(targetSalesApplicationConfigService.isPartnerChannel(SalesApplication.EBAY)))
                .thenReturn(Boolean.TRUE);
    }

    @Test
    public void testShippingNotificationSentForEbayNonCNC() throws RetryLaterException, Exception {
        when(order.getSalesApplication()).thenReturn(SalesApplication.EBAY);
        when(deliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.FALSE);
        when(Boolean
                .valueOf(targetCaShippingService.submitDeliveryOrderStatusForCompleteUpdate(orderProcess.getOrder())))
                .thenReturn(Boolean.TRUE);
        updatePartnerWithShipmentInfoAction.executeInternal(orderProcess);
        verify(targetCaShippingService).submitDeliveryOrderStatusForCompleteUpdate(order);
    }

    @Test
    public void testShippingNotificationSentForEbayWithFeatureSwitchOn() throws RetryLaterException, Exception {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        when(order.getSalesApplication()).thenReturn(SalesApplication.EBAY);
        when(deliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.FALSE);
        when(Boolean
                .valueOf(targetCaShippingService.submitDeliveryOrderStatusForCompleteUpdate(orderProcess.getOrder())))
                .thenReturn(Boolean.TRUE);
        when(Boolean
                .valueOf(targetCaShippingService.submitDeliveryConsignmentStatus(null)))
                .thenReturn(Boolean.TRUE);
        updatePartnerWithShipmentInfoAction.executeInternal(orderProcess);
        verify(targetCaShippingService).submitDeliveryConsignmentStatus(null);
    }

    @Test
    public void testShippingNotificationSentForEbayCNC() throws RetryLaterException, Exception {
        when(order.getSalesApplication()).thenReturn(SalesApplication.EBAY);
        when(deliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);
        updatePartnerWithShipmentInfoAction.executeInternal(orderProcess);
        Mockito.verifyZeroInteractions(targetCaShippingService);
    }

    @Test
    public void testShippingNotificationSentForNonPartner() throws RetryLaterException, Exception {
        when(order.getSalesApplication()).thenReturn(SalesApplication.WEB);
        when(deliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.FALSE);
        updatePartnerWithShipmentInfoAction.executeInternal(orderProcess);
        Mockito.verifyZeroInteractions(targetCaShippingService);
    }

    @Test
    public void testShippingNotificationSentForNonPartnerCNC() throws RetryLaterException, Exception {
        when(order.getSalesApplication()).thenReturn(SalesApplication.WEB);
        when(deliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);
        updatePartnerWithShipmentInfoAction.executeInternal(orderProcess);
        Mockito.verifyZeroInteractions(targetCaShippingService);
    }

    @Test(expected = RetryLaterException.class)
    public void testExecuteServiceFailure() throws Exception {
        when(order.getSalesApplication()).thenReturn(SalesApplication.EBAY);
        when(deliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.FALSE);
        when(Boolean
                .valueOf(targetCaShippingService.submitDeliveryOrderStatusForCompleteUpdate(orderProcess.getOrder())))
                .thenReturn(Boolean.FALSE);
        updatePartnerWithShipmentInfoAction.executeInternal(orderProcess);
    }
}

/**
 * 
 */
package au.com.target.tgtebay.jobs;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.dao.TargetConsignmentDao;



/**
 * @author Pradeep
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AutoUpdatePartnerPickedupJobTest {

    @Mock
    private TargetConsignmentDao targetConsignmentDao;

    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private final AutoUpdatePartnerPickedupJob autoUpdatePartnerPickedupJob = new AutoUpdatePartnerPickedupJob();

    private final CronJobModel cronJob = new CronJobModel();

    /**
     * Test method for
     * {@link au.com.target.tgtebay.jobs.AutoUpdatePartnerPickedupJob#perform(de.hybris.platform.cronjob.model.CronJobModel)}
     * .
     */
    @Test
    public void testPerformCronJobModel() {
        final TargetConsignmentModel consignmentModelOne = new TargetConsignmentModel();
        final TargetConsignmentModel consignmentModelTwo = new TargetConsignmentModel();
        final OrderModel orderModel = new OrderModel();
        orderModel.setCode("order1");
        consignmentModelOne.setOrder(orderModel);
        final OrderModel orderModelTwo = new OrderModel();
        orderModelTwo.setCode("order2");
        consignmentModelTwo.setOrder(orderModelTwo);
        final List<TargetConsignmentModel> consignmentList = new ArrayList<>();
        consignmentList.add(consignmentModelOne);
        consignmentList.add(consignmentModelTwo);
        when(targetConsignmentDao.getConsignmentsForAutoUpdatePartnerPickedup(Mockito.anyString(), Mockito.anyInt()))
                .thenReturn(consignmentList);
        Mockito.doNothing().when(modelService).save(Mockito.anyObject());
        autoUpdatePartnerPickedupJob.perform(cronJob);
        Mockito.verify(modelService, Mockito.atLeast(2)).save(Mockito.anyObject());
    }

    @Test
    public void testPerformCronJobModelWithNoResult() {
        when(targetConsignmentDao.getConsignmentsForAutoUpdatePartnerPickedup(Mockito.anyString(), Mockito.anyInt()))
                .thenReturn(null);
        Mockito.doNothing().when(modelService).save(Mockito.anyObject());
        autoUpdatePartnerPickedupJob.perform(cronJob);
        Mockito.verifyZeroInteractions(modelService);
    }
}

/**
 * 
 */
package au.com.target.tgtebay.stockupdate.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.stockupdate.TargetInventoryService;


/**
 * @author bhuang3
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StockUpdateToPartnerFacadeTest {

    private static final String PRODUCT_CODE = "w123456";
    @Mock
    private ProductService productService;
    @Mock
    private TargetInventoryService targetInventoryService;
    @InjectMocks
    private final StockUpdateToPartnerFacadeImpl stockUpdateOnEbayFacadeImpl = new StockUpdateToPartnerFacadeImpl();

    @SuppressWarnings("boxing")
    @Test
    public void testUpdateStock() {
        final AbstractTargetVariantProductModel colourVariantProductModel = new TargetColourVariantProductModel();
        colourVariantProductModel.setAvailableOnEbay(Boolean.TRUE);
        BDDMockito.given(productService.getProductForCode(PRODUCT_CODE)).willReturn(colourVariantProductModel);
        BDDMockito.given(targetInventoryService.updateInventoryItemQuantity(PRODUCT_CODE, 100)).willReturn(
                Boolean.TRUE);
        final boolean result = stockUpdateOnEbayFacadeImpl.updateStock(PRODUCT_CODE, 100);
        Assert.assertTrue(result);
    }

    @Test
    public void testUpdateStockWithNoProductFound() {
        final AbstractTargetVariantProductModel colourVariantProductModel = new TargetColourVariantProductModel();
        colourVariantProductModel.setAvailableOnEbay(Boolean.TRUE);
        BDDMockito.given(productService.getProductForCode(PRODUCT_CODE)).willThrow(new UnknownIdentifierException(""));
        final boolean result = stockUpdateOnEbayFacadeImpl.updateStock(PRODUCT_CODE, 100);
        Assert.assertTrue(!result);
    }

    @Test
    public void testUpdateStockWithNotVariantProduct() {
        final ProductModel productModel = new ProductModel();
        BDDMockito.given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        final boolean result = stockUpdateOnEbayFacadeImpl.updateStock(PRODUCT_CODE, 100);
        Assert.assertTrue(!result);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testUpdateStockWithItemNotAvailableOnEbay() {
        final AbstractTargetVariantProductModel colourVariantProductModel = new TargetColourVariantProductModel();
        colourVariantProductModel.setAvailableOnEbay(Boolean.FALSE);
        BDDMockito.given(productService.getProductForCode(PRODUCT_CODE)).willReturn(colourVariantProductModel);
        BDDMockito.given(targetInventoryService.updateInventoryItemQuantity(PRODUCT_CODE, 100)).willReturn(
                Boolean.TRUE);
        final boolean result = stockUpdateOnEbayFacadeImpl.updateStock(PRODUCT_CODE, 100);
        Assert.assertTrue(!result);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testUpdateStockWithAvailableOnEbayNull() {
        final AbstractTargetVariantProductModel colourVariantProductModel = new TargetColourVariantProductModel();
        colourVariantProductModel.setAvailableOnEbay(null);
        BDDMockito.given(productService.getProductForCode(PRODUCT_CODE)).willReturn(colourVariantProductModel);
        BDDMockito.given(targetInventoryService.updateInventoryItemQuantity(PRODUCT_CODE, 100)).willReturn(
                Boolean.TRUE);

        final boolean result = stockUpdateOnEbayFacadeImpl.updateStock(PRODUCT_CODE, 100);
        Assert.assertFalse(result);
    }
}

/**
 * 
 */
package au.com.target.tgtebay.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.SalesApplicationConfigModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import au.com.target.tgtebay.constants.TgtebayConstants;
import au.com.target.tgtebay.dto.EbayTargetAddressDTO;
import au.com.target.tgtebay.dto.EbayTargetOrderDTO;
import au.com.target.tgtebay.dto.EbayTargetOrderEntriesDTO;
import au.com.target.tgtebay.dto.EbayTargetOrderEntryDTO;
import au.com.target.tgtebay.dto.EbayTargetPayInfoDTO;


/**
 * @author mjanarth
 * 
 */
@UnitTest
public class TargetPartnerOrderFieldValidatorTest {

    private static final String SALES_APPLICATION_EBAY = "eBay";

    private static final String FULFILLMENT_TYPE_PICKUP = "Pickup";
    private static final String FULFILLMENT_TYPE_SHIP = "Ship";
    private static final String FULFILLMENT_TYPE_PICKUP_NOT_ACCEPTED = "DUMMY";

    private static final String CNC_STORE_NUMBER = "1234";
    private static final String ORDER_NUMBER = "12345678";

    @Mock
    private EbayTargetOrderDTO orderDTO;

    @Mock
    private EbayTargetAddressDTO addressDTO;

    @Mock
    private EbayTargetPayInfoDTO paymentDTO;

    @Mock
    private TargetOrderService targetOrderService;

    @Mock
    private TargetSalesApplicationConfigService salesApplicationConfigService;

    @Mock
    private EbayTargetOrderEntriesDTO orderEntriesDTO;

    private List<EbayTargetOrderEntryDTO> orderEntries;

    private final Map<String, Boolean> fulfillmentTypeToDeliveryToStoreMapping = new HashMap<String, Boolean>();

    @InjectMocks
    private final TargetPartnerOrderFieldValidator fieldValidation = new TargetPartnerOrderFieldValidator();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        BDDMockito.given(orderDTO.geteBayOrderNumber()).willReturn("123456");
        BDDMockito.given(orderDTO.getDeliveryAddress()).willReturn(addressDTO);
        BDDMockito.given(addressDTO.getCountry()).willReturn(TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);
        BDDMockito.given(orderDTO.getPaymentInfo()).willReturn(paymentDTO);
        BDDMockito.given(paymentDTO.getPaymentType()).willReturn("PayPal");

        final SalesApplicationConfigModel eBayConfig = new SalesApplicationConfigModel();
        eBayConfig.setSalesApplication(SalesApplication.EBAY);
        eBayConfig.setIsPartnerChannel(true);
        final SalesApplicationConfigModel tradeMeConfig = new SalesApplicationConfigModel();
        tradeMeConfig.setSalesApplication(SalesApplication.TRADEME);
        tradeMeConfig.setIsPartnerChannel(true);
        BDDMockito.given(salesApplicationConfigService.getAllPartnerSalesApplicationConfigs())
                .willReturn(Arrays.asList(eBayConfig, tradeMeConfig));

        final Set<String> validCurrencies = new HashSet();
        validCurrencies.add("AUD");
        fieldValidation.setValidCurrencies(validCurrencies);

        final Set<String> validCountries = new HashSet();
        validCountries.add("AU");
        validCountries.add("NZ");
        fieldValidation.setValidCountries(validCountries);

        final Map<String, String> paymentTypeToPaymentMethodMapping = new HashMap();
        paymentTypeToPaymentMethodMapping.put("PayPal", "paypal");
        paymentTypeToPaymentMethodMapping.put("TradeMe", "paynow");
        fieldValidation.setPaymentTypeToPaymentMethodMapping(paymentTypeToPaymentMethodMapping);

        fulfillmentTypeToDeliveryToStoreMapping.put(FULFILLMENT_TYPE_PICKUP, Boolean.TRUE);
        fulfillmentTypeToDeliveryToStoreMapping.put(FULFILLMENT_TYPE_SHIP, Boolean.FALSE);
        fieldValidation.setFulfillmentTypeToDeliveryToStoreMapping(fulfillmentTypeToDeliveryToStoreMapping);

        orderEntries = new ArrayList<>();
        orderEntries.add(createEbayOrderEntry(null, null));
        BDDMockito.given(orderDTO.getEbayTargetOrderEntriesDTO()).willReturn(orderEntriesDTO);
        BDDMockito.given(orderEntriesDTO.getEntries()).willReturn(orderEntries);
    }

    @Test
    public void testEmailAddress() {

        final String emailAddress = "madhu@test.com";
        final boolean valid = fieldValidation.validateEmail(emailAddress);
        org.junit.Assert.assertTrue(valid);
    }

    @Test
    public void testEmailAddress1() {

        final String emailAddress = "madhu@.com";
        final boolean valid = fieldValidation.validateEmail(emailAddress);
        Assert.assertFalse(valid);

    }

    @Test
    public void testinvalidPhone() {
        final String phoneNumber = "12345678945612388";
        final boolean valid = fieldValidation.validatePhoneNumber(phoneNumber,
                TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);
        Assert.assertFalse(valid);
    }

    @Test
    public void testvalidPhone() {
        final String phoneNumber = "+61452355456";
        final boolean valid = fieldValidation.validatePhoneNumber(phoneNumber,
                TgtCoreConstants.COUNTRY_ISO_CODE_NEWZEALAND);
        Assert.assertTrue(valid);
    }

    @Test
    public void testvalidPhone1() {
        final String phoneNumber = "+1452355456";
        final boolean valid = fieldValidation.validatePhoneNumber(phoneNumber,
                TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);
        Assert.assertTrue(valid);
    }

    @Test
    public void testinvalidPhone1() {
        final String phoneNumber = "+145254";
        final boolean valid = fieldValidation.validatePhoneNumber(phoneNumber,
                TgtCoreConstants.COUNTRY_ISO_CODE_NEWZEALAND);
        Assert.assertFalse(valid);
    }

    @Test
    public void testinvalidPhone2() {
        final String phoneNumber = "+145254wwwwwww5";
        final boolean valid = fieldValidation.validatePhoneNumber(phoneNumber,
                TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);
        Assert.assertFalse(valid);
    }

    @Test
    public void testvalidPhone2() {
        final String phoneNumber = "+0452-456-789";
        final boolean valid = fieldValidation.validatePhoneNumber(phoneNumber,
                TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);
        Assert.assertTrue(valid);
    }

    @Test
    public void testEmptyPhoneNumber() {
        final boolean valid = fieldValidation.validatePhoneNumber(StringUtils.EMPTY,
                TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);
        Assert.assertTrue(valid);
    }

    @Test
    public void testAUMinPhoneLength() {
        final String phoneNumber = "123456789";
        final boolean valid = fieldValidation.validatePhoneNumber(phoneNumber,
                TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);
        Assert.assertTrue(valid);
    }

    @Test
    public void testNZMinPhoneLength() {
        final String phoneNumber = "12345678";
        final boolean valid = fieldValidation.validatePhoneNumber(phoneNumber,
                TgtCoreConstants.COUNTRY_ISO_CODE_NEWZEALAND);
        Assert.assertTrue(valid);
    }


    @Test
    public void testInvalidPhoneLength() {
        final String phoneNumber = "1234567";
        final boolean valid = fieldValidation.validatePhoneNumber(phoneNumber,
                TgtCoreConstants.COUNTRY_ISO_CODE_NEWZEALAND);
        Assert.assertFalse(valid);
    }

    @Test
    public void testMaxPhoneLength() {
        final String phoneNumber = "1234567890123456";
        final boolean valid = fieldValidation.validatePhoneNumber(phoneNumber,
                TgtCoreConstants.COUNTRY_ISO_CODE_NEWZEALAND);
        Assert.assertTrue(valid);
    }

    @Test
    public void testisEmptyAddress() {
        final EbayTargetAddressDTO address = new EbayTargetAddressDTO();
        final boolean valid = fieldValidation.isEmptyAddress(address);
        Assert.assertTrue(valid);
    }

    @Test
    public void testisNotEmptyAddress() {
        final EbayTargetAddressDTO address = new EbayTargetAddressDTO();
        address.setStreetname("thompson rd");
        final boolean valid = fieldValidation.isEmptyAddress(address);
        Assert.assertFalse(valid);
    }

    @Test
    public void testInavlidParseDouble() {
        final String doubleVal = "aaaaa";
        final Double testVal = fieldValidation.parseDouble(doubleVal, "Fieldname", "123456");
        Assert.assertEquals(testVal, TgtebayConstants.DOUBLE_ZERO);

    }

    @Test
    public void testInavlidParseDouble1() {
        final String doubleVal = null;
        final Double testVal = fieldValidation.parseDouble(doubleVal, "Fieldname", "123456");
        Assert.assertEquals(testVal, TgtebayConstants.DOUBLE_ZERO);

    }

    @Test
    public void testvalidParseDouble() {
        final String doubleVal = "50";
        final Double testVal = fieldValidation.parseDouble(doubleVal, "Fieldname", "123456");
        Assert.assertEquals(testVal, new Double(50));

    }

    @Test
    public void testInavlidParseLong() {
        final String longVal = "aaaaa";
        final Long testVal = fieldValidation.parseLong(longVal, "Fieldname", "123456");
        Assert.assertEquals(testVal, TgtebayConstants.LONG_ZERO);

    }

    @Test
    public void testInavlidParseLong1() {
        final String longVal = null;
        final Long testVal = fieldValidation.parseLong(longVal, "Fieldname", "123456");
        Assert.assertEquals(testVal, TgtebayConstants.LONG_ZERO);

    }

    @Test
    public void testvalidParseLong() {
        final String longVal = "50";
        final Long testVal = fieldValidation.parseLong(longVal, "Fieldname", "123456");
        Assert.assertEquals(testVal, Long.valueOf(50));
    }

    @Test
    public void testIsOrderDataValidEmptySalesChannel() {
        Assert.assertTrue(fieldValidation.isOrderDataValid(orderDTO));
    }

    @Test
    public void testIsOrderDataValidForSalesChannelWithPaymentModeMapping() {
        BDDMockito.given(paymentDTO.getPaymentType()).willReturn("TradeMe");
        Assert.assertTrue(fieldValidation.isOrderDataValid(orderDTO));
    }

    @Test
    public void testIsOrderDataValidForSalesChannelWithoutPaymentModeMapping() {
        BDDMockito.given(paymentDTO.getPaymentType()).willReturn("InValid");
        Assert.assertFalse(fieldValidation.isOrderDataValid(orderDTO));
    }

    @Test
    public void testIsOrderDataValidInvalidSalesChannel() {
        Mockito.when(orderDTO.getSalesChannel()).thenReturn("InValid");
        Assert.assertFalse(fieldValidation.isOrderDataValid(orderDTO));
    }

    @Test
    public void testIsOrderDataValidEmptyCurrency() {
        Assert.assertTrue(fieldValidation.isOrderDataValid(orderDTO));
    }

    @Test
    public void testIsOrderDataValidWithValidCurrency() {
        Mockito.when(orderDTO.getCurrencyCode()).thenReturn("AUD");
        Assert.assertTrue(fieldValidation.isOrderDataValid(orderDTO));
    }

    @Test
    public void testIsOrderDataValidWithInvalidCurrency() {
        Mockito.when(orderDTO.getCurrencyCode()).thenReturn("USD");
        Assert.assertFalse(fieldValidation.isOrderDataValid(orderDTO));
    }

    @Test
    public void testIsOrderDataValidWithNullAddress() {
        Mockito.when(orderDTO.getDeliveryAddress()).thenReturn(null);
        Assert.assertFalse(fieldValidation.isOrderDataValid(orderDTO));
    }

    @Test
    public void testIsOrderDataValidWithNullCountry() {
        Mockito.when(addressDTO.getCountry()).thenReturn(null);
        Assert.assertFalse(fieldValidation.isOrderDataValid(orderDTO));
    }

    @Test
    public void testIsOrderDataValidWithValidCountryAU() {
        Mockito.when(addressDTO.getCountry()).thenReturn("AU");
        Assert.assertTrue(fieldValidation.isOrderDataValid(orderDTO));
    }

    @Test
    public void testIsOrderDataValidWithValidCountryNZ() {
        Mockito.when(addressDTO.getCountry()).thenReturn("NZ");
        Assert.assertTrue(fieldValidation.isOrderDataValid(orderDTO));
    }

    @Test
    public void testIsOrderDataValidWithInvalidCountry() {
        Mockito.when(addressDTO.getCountry()).thenReturn("US");
        Assert.assertFalse(fieldValidation.isOrderDataValid(orderDTO));
    }

    @Test
    public void testValidatePartnerOrderNumberNullEbayOrderNumber() {
        Assert.assertFalse(fieldValidation.validatePartnerOrderNumber(null,
                SALES_APPLICATION_EBAY));
    }

    @Test
    public void testValidatePartnerOrderNumberNullOrders() {
        Mockito.when(targetOrderService.findOrdersForPartnerOrderIdAndSalesChannel("123456",
                SALES_APPLICATION_EBAY)).thenReturn(null);
        Assert.assertTrue(fieldValidation.validatePartnerOrderNumber("123456",
                SALES_APPLICATION_EBAY));
    }

    @Test
    public void testValidatePartnerOrderNumberOneOrder() {
        final OrderModel mockOrder = Mockito.mock(OrderModel.class);
        Mockito.when(targetOrderService.findOrdersForPartnerOrderIdAndSalesChannel("123456",
                SALES_APPLICATION_EBAY))
                .thenReturn(Collections.singletonList(mockOrder));
        Assert.assertFalse(fieldValidation.validatePartnerOrderNumber("123456",
                SALES_APPLICATION_EBAY));
    }

    @Test
    public void testIsValidateOrderDataValidWhenEntriesAreMissing() {
        BDDMockito.given(orderEntriesDTO.getEntries()).willReturn(Collections.EMPTY_LIST);

        Assert.assertFalse(fieldValidation.isOrderDataValid(orderDTO));
    }

    @Test
    public void testIsValidateOrderDataValidWhenEntriesAreNull() {
        BDDMockito.given(orderDTO.getEbayTargetOrderEntriesDTO()).willReturn(null);

        Assert.assertFalse(fieldValidation.isOrderDataValid(orderDTO));
    }

    @Test
    public void testIsValidateOrderDataValidWhenEntriesAreCorrect() {
        Assert.assertTrue(fieldValidation.isOrderDataValid(orderDTO));
    }

    @Test
    public void testIsValidateOrderDataValidWhenEntriesAreInCorrect() {
        clearExistingOrderEntries();
        orderEntries.add(createEbayOrderEntry(FULFILLMENT_TYPE_PICKUP_NOT_ACCEPTED, CNC_STORE_NUMBER));

        Assert.assertFalse(fieldValidation.isOrderDataValid(orderDTO));
    }

    @Test
    public void testValidateEntryForFulfilDataWhenFulFillTypeAndCnCNumberExistWithCorrectValues() {
        clearExistingOrderEntries();
        orderEntries.add(createEbayOrderEntry(FULFILLMENT_TYPE_PICKUP, CNC_STORE_NUMBER));

        Assert.assertTrue(fieldValidation.validatePartnerOrderEntriesForFulfillmentType(orderEntriesDTO, ORDER_NUMBER,
                SALES_APPLICATION_EBAY));
    }

    @Test
    public void testValidateEntryForFulfilDataWhenFulfillTypeAndCnCNumberExistWithIncorrectFulfillmentType() {
        clearExistingOrderEntries();
        orderEntries.add(createEbayOrderEntry(FULFILLMENT_TYPE_PICKUP_NOT_ACCEPTED, CNC_STORE_NUMBER));

        Assert.assertFalse(fieldValidation.validatePartnerOrderEntriesForFulfillmentType(orderEntriesDTO, ORDER_NUMBER,
                SALES_APPLICATION_EBAY));
    }

    @Test
    public void testValidateEntryForFulfilDataWhenCnCNumberExistButFulfillmentTypeMissing() {
        clearExistingOrderEntries();
        orderEntries.add(createEbayOrderEntry(null, CNC_STORE_NUMBER));

        Assert.assertTrue(fieldValidation.validatePartnerOrderEntriesForFulfillmentType(orderEntriesDTO, ORDER_NUMBER,
                SALES_APPLICATION_EBAY));
    }

    @Test
    public void testValidateEntryForFulfilDataWhenFulfillTypeExistAndCNCStoreNumberMissing() {
        clearExistingOrderEntries();
        orderEntries.add(createEbayOrderEntry(FULFILLMENT_TYPE_PICKUP, null));

        Assert.assertTrue(fieldValidation.validatePartnerOrderEntriesForFulfillmentType(orderEntriesDTO, ORDER_NUMBER,
                SALES_APPLICATION_EBAY));
    }

    @Test
    public void testValidateEntryForFulfilDataWhenFulfillTypeAndCnCNumberBothMissing() {
        clearExistingOrderEntries();
        orderEntries.add(createEbayOrderEntry(null, null));

        Assert.assertTrue(fieldValidation.validatePartnerOrderEntriesForFulfillmentType(orderEntriesDTO, ORDER_NUMBER,
                SALES_APPLICATION_EBAY));
    }

    @Test
    public void testValidateEntryForFufilDataWhenFulfillTypeAndCnCExistForMultipleEntries() {
        clearExistingOrderEntries();
        orderEntries.add(createEbayOrderEntry(FULFILLMENT_TYPE_PICKUP, CNC_STORE_NUMBER));
        orderEntries.add(createEbayOrderEntry(FULFILLMENT_TYPE_PICKUP, CNC_STORE_NUMBER));

        Assert.assertTrue(fieldValidation.validatePartnerOrderEntriesForFulfillmentType(orderEntriesDTO, ORDER_NUMBER,
                SALES_APPLICATION_EBAY));
    }

    @Test
    public void testValidateEntryForFufilDataWhenFulfillTypeAndCnCMissingForMultipleEntries() {
        clearExistingOrderEntries();
        orderEntries.add(createEbayOrderEntry(null, null));
        orderEntries.add(createEbayOrderEntry(null, null));
        BDDMockito.given(orderEntriesDTO.getEntries()).willReturn(orderEntries);

        Assert.assertTrue(fieldValidation.validatePartnerOrderEntriesForFulfillmentType(orderEntriesDTO, ORDER_NUMBER,
                SALES_APPLICATION_EBAY));
    }

    @Test
    public void testValidateEntryForFufilDataWhenFulfillTypeAndCnCExistForMultipleEntriesWithIncorrectFulfillType() {
        clearExistingOrderEntries();
        orderEntries.add(createEbayOrderEntry(FULFILLMENT_TYPE_PICKUP, CNC_STORE_NUMBER));
        orderEntries.add(createEbayOrderEntry(FULFILLMENT_TYPE_PICKUP_NOT_ACCEPTED, CNC_STORE_NUMBER));

        Assert.assertFalse(fieldValidation.validatePartnerOrderEntriesForFulfillmentType(orderEntriesDTO, ORDER_NUMBER,
                SALES_APPLICATION_EBAY));
    }

    @Test
    public void testValidateEntryForFufilDataWhenFulfillTypeMissingAndCnCExistForMultipleEntries() {
        clearExistingOrderEntries();
        orderEntries.add(createEbayOrderEntry(null, CNC_STORE_NUMBER));
        orderEntries.add(createEbayOrderEntry(FULFILLMENT_TYPE_PICKUP, CNC_STORE_NUMBER));

        Assert.assertFalse(fieldValidation.validatePartnerOrderEntriesForFulfillmentType(orderEntriesDTO, ORDER_NUMBER,
                SALES_APPLICATION_EBAY));
    }

    @Test
    public void testValidateEntryForFufilDataWhenFulfillTypeExistAndCnCStoreMissingForMultipleEntries() {
        clearExistingOrderEntries();
        orderEntries.add(createEbayOrderEntry(FULFILLMENT_TYPE_PICKUP, CNC_STORE_NUMBER));
        orderEntries.add(createEbayOrderEntry(FULFILLMENT_TYPE_PICKUP, null));

        Assert.assertFalse(fieldValidation.validatePartnerOrderEntriesForFulfillmentType(orderEntriesDTO, ORDER_NUMBER,
                SALES_APPLICATION_EBAY));
    }

    @Test
    public void testValidateEntryForFufilDataWhenFulfillTypeShipAndCnCStoreMissingForMultipleEntries() {
        clearExistingOrderEntries();
        orderEntries.add(createEbayOrderEntry(FULFILLMENT_TYPE_SHIP, CNC_STORE_NUMBER));
        orderEntries.add(createEbayOrderEntry(FULFILLMENT_TYPE_SHIP, null));

        Assert.assertTrue(fieldValidation.validatePartnerOrderEntriesForFulfillmentType(orderEntriesDTO, ORDER_NUMBER,
                SALES_APPLICATION_EBAY));
    }

    @Test
    public void testValidateEntryForFufilDataWhenFulfillTypeAndCnCStoreMissingForMultipleEntries() {
        clearExistingOrderEntries();
        orderEntries.add(createEbayOrderEntry(null, null));
        orderEntries.add(createEbayOrderEntry(null, null));

        Assert.assertTrue(fieldValidation.validatePartnerOrderEntriesForFulfillmentType(orderEntriesDTO, ORDER_NUMBER,
                SALES_APPLICATION_EBAY));
    }

    private EbayTargetOrderEntryDTO createEbayOrderEntry(final String fulfillmentType, final String cncStoreNumber) {
        final EbayTargetOrderEntryDTO firstOrderEntry = Mockito.mock(EbayTargetOrderEntryDTO.class);
        BDDMockito.given(firstOrderEntry.getFulfillmentType()).willReturn(fulfillmentType);
        BDDMockito.given(firstOrderEntry.getCncStoreNumber()).willReturn(cncStoreNumber);
        return firstOrderEntry;
    }

    private void clearExistingOrderEntries() {
        orderEntries.clear();
    }

}

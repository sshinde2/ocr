/**
 * 
 */
package au.com.target.endeca.infront.cache;

import au.com.target.endeca.infront.data.EndecaDimensions;
import au.com.target.endeca.infront.exception.TargetEndecaException;

import com.endeca.navigation.ENEQueryException;


/**
 * @author smudumba
 * 
 */
public interface EndecaDimensionCacheService {

    /**
     * Method returns EndecaDimensions
     * 
     * @return EndecaDimensions
     * @throws TargetEndecaException
     */
    EndecaDimensions getDimensions() throws TargetEndecaException;


    /**
     * Method to get dimension id for refinement
     * 
     * @param dimName
     * @param refinementName
     * @return String
     * @throws ENEQueryException
     * @throws TargetEndecaException
     */
    String getDimension(final String dimName, final String refinementName) throws ENEQueryException,
            TargetEndecaException;

    /*
     *  Method to invalidate EndecaDimensions Cache
     */
    Boolean invalidateEndecaDimensionsCache();

}

/**
 * 
 */
package au.com.target.endeca.infront.cache;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.endeca.navigation.ENEQueryException;

import au.com.target.endeca.infront.data.EndecaDimensions;
import au.com.target.endeca.infront.exception.TargetEndecaException;



/**
 * @author smudumba
 * 
 */
public class EndecaDimensionCacheServiceImpl implements EndecaDimensionCacheService {


    protected static final Logger LOG = Logger.getLogger(EndecaDimensionCacheServiceImpl.class);

    private TargetEndecaDimensionCache targetEndecaDimensionCache;



    /**
     * Method returns EndecaDimensions
     * 
     * @return EndecaDimensions
     * @throws TargetEndecaException
     */
    @Override
    public EndecaDimensions getDimensions() throws TargetEndecaException {

        EndecaDimensions enDims = null;

        try {
            enDims = targetEndecaDimensionCache.getDimensionCache();

        }
        catch (final Exception e) {
            throw new TargetEndecaException("Error getting Endeca Dimensions" + e);
        }
        return enDims;
    }


    /**
     * Method to get dimension id for refinement
     * 
     * @param dimName
     * @param refinementName
     * @return String
     * @throws ENEQueryException
     * @throws TargetEndecaException
     */
    @Override
    public String getDimension(final String dimName, final String refinementName) throws ENEQueryException,
            TargetEndecaException {
        String result = StringUtils.EMPTY;
        final EndecaDimensions dimensions = this.getDimensions();
        if (null != dimensions.getDimensions()) {
            if (dimensions.getDimensions().containsKey(dimName.toLowerCase())
                    && dimensions.getDimensions().get(dimName.toLowerCase())
                            .containsKey(refinementName.toLowerCase())) {
                result = dimensions.getDimensions().get(dimName.toLowerCase()).get(refinementName.toLowerCase());
            }

        }

        return result;
    }



    /*
     *  Method to invalidate EndecaDimensions Cache
     */
    @Override
    public Boolean invalidateEndecaDimensionsCache() {
        try {
            targetEndecaDimensionCache.refreshCache();
        }
        catch (final ENEQueryException | TargetEndecaException e) {
            LOG.error("TGT-ENDECA: Error while clearing cache", e);
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }


    /**
     * @param targetEndecaDimensionCache
     *            the targetEndecaDimensionCache to set
     */
    @Required
    public void setTargetEndecaDimensionCache(final TargetEndecaDimensionCache targetEndecaDimensionCache) {
        this.targetEndecaDimensionCache = targetEndecaDimensionCache;
    }




}

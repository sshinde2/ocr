/**
 * 
 */
package au.com.target.endeca.infront.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.endeca.navigation.ENEQueryException;

import au.com.target.endeca.infront.data.EndecaDimensions;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.service.EndecaDimensionService;



/**
 * @author pthoma20
 * 
 */
public class TargetEndecaDimensionCache {
    protected static Map<String, EndecaDimensions> cache;

    private static final Logger LOG = Logger.getLogger(TargetEndecaDimensionCache.class);

    private static final String ENDECA_CACHE_KEY = "EndecaDimensionCache";

    private EndecaDimensionService endecaDimensionService;

    static {
        cache = new ConcurrentHashMap<String, EndecaDimensions>();
    }

    /**
     * This method will fetch the endeca dimensions from cache. If it is not found in cache will call the
     * initializeCacheDataMethod
     * 
     * 
     * @return Endeca Dimensions from cache.
     * @throws ENEQueryException
     * @throws TargetEndecaException
     */
    public EndecaDimensions getDimensionCache() throws ENEQueryException, TargetEndecaException {

        if (isCacheEmpty()) {
            /*99% of the time cache will not be stale and will not enter this loop.
             Hence making the getDimensionsCache is not necessary but fetching should be synched.*/
            LOG.info("TGT-ENDECA :getDimensionCache() -- Cache Is Stale..");
            initializeCacheData();
        }
        return cache.get(ENDECA_CACHE_KEY);
    }

    private synchronized void initializeCacheData() throws ENEQueryException, TargetEndecaException {
        if (!isCacheEmpty()) {
            /*Considering 5 threads comes to get dimension and find that the cache is stale, it will all try and initialize one by one.
            It is only required for one thread to do the initialization so checking again the staleness inside will prevent 
            other threads from doing initialize.*/
            LOG.info("TGT-ENDECA :initializeCacheData() -- Cache Has been refreshed by another thread");
            return;
        }
        LOG.info("TGT-ENDECA :initializeCacheData() -- Going to get Data For Endeca Cache.");
        final EndecaDimensions endecaDimensions = endecaDimensionService.getDimensions();
        cache.put(ENDECA_CACHE_KEY, endecaDimensions);
    }

    private boolean isCacheEmpty() {
        if (null != cache && null != cache.get(ENDECA_CACHE_KEY)) {
            return false;
        }
        return true;
    }

    /**
     * This method is invoked after the baseline Update runs to get its cache refreshed.
     * 
     * @throws ENEQueryException
     * @throws TargetEndecaException
     */
    public void refreshCache() throws ENEQueryException, TargetEndecaException {
        /*when a refresh cache request comes in after a baseline - the server will fetch the dimensions and then replace the cache 
         * In the mean while the front end will still be served with the old cache. 
         */
        LOG.info("TGT-ENDECA :refreshCache() -- Fetching details for Cache");
        final EndecaDimensions endecaDimensions = endecaDimensionService.getDimensions();
        cache.put(ENDECA_CACHE_KEY, endecaDimensions);
        LOG.info("TGT-ENDECA :refreshed the cache()");
    }

    /**
     * @param endecaDimensionService
     *            the endecaDimensionService to set
     */
    @Required
    public void setEndecaDimensionService(final EndecaDimensionService endecaDimensionService) {
        this.endecaDimensionService = endecaDimensionService;
    }
}

/**
 * 
 */
package au.com.target.endeca.infront.cartridge;

import java.util.Comparator;

import org.apache.commons.lang.builder.CompareToBuilder;

import com.endeca.infront.cartridge.RefinementMenuConfig;
import com.endeca.infront.cartridge.model.Refinement;

import au.com.target.endeca.infront.constants.EndecaConstants;


/**
 * Comparator for sorting refinements as per <code>cartridgeConfig</code>. It handles boosted and buried records
 * correctly and the sort options: default, static and dynRank.
 * 
 * @author mgazal
 *
 */
public class RefinementComparator implements Comparator<Refinement> {

    private final RefinementMenuConfig cartridgeConfig;

    /**
     * @param cartridgeConfig
     */
    public RefinementComparator(final RefinementMenuConfig cartridgeConfig) {
        super();
        this.cartridgeConfig = cartridgeConfig;
    }

    @Override
    public int compare(final Refinement o1, final Refinement o2) {
        final CompareToBuilder compareToBuilder = new CompareToBuilder();
        final String o1Sort = o1.getProperties().get("DGraph.Strata");
        final String o2Sort = o2.getProperties().get("DGraph.Strata");

        compareToBuilder.append(Long.valueOf(o2Sort != null ? o2Sort : "0"),
                Long.valueOf(o1Sort != null ? o1Sort : "0"));

        if (cartridgeConfig.getSort().equals(EndecaConstants.RefinementMenuConfig.SORT_FREQUENCY)) {
            compareToBuilder.append(o2.getCount(), o1.getCount());
        }
        compareToBuilder.append(o1.getLabel(), o2.getLabel());

        return compareToBuilder.toComparison();
    }


}

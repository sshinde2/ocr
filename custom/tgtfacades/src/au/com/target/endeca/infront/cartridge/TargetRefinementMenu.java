/**
 * 
 */
package au.com.target.endeca.infront.cartridge;

import com.endeca.infront.cartridge.RefinementMenu;


/**
 * @author pvarghe2
 *
 */
public class TargetRefinementMenu extends RefinementMenu {

    private static final String SEO_FOLLOW_ENABLED = "seoFollowEnabled";

    /**
     * SEO enabled flag is transformed depending on some checks, but we need a placeholder to get the actual setting of
     * SEO follow up in Endeca and hence a new variable. I know, it should have been the other way around, but... ( that
     * would involve existing impl change including Endeca)
     */
    private static final String ACTUAL_FOLLOWUP = "actualFollowup";

    public TargetRefinementMenu(final TargetRefinementMenuConfig config) {
        super(config);
    }

    public boolean isSeoFollowEnabled() {
        return getBooleanProperty(SEO_FOLLOW_ENABLED, false);
    }

    public void setSeoFollowEnabled(final boolean seoFollowEnable) {
        put(SEO_FOLLOW_ENABLED, Boolean.valueOf(seoFollowEnable));
    }

    public boolean isActualFollowup() {
        return getBooleanProperty(ACTUAL_FOLLOWUP, false);
    }

    public void setActualFollowup(final boolean actualFollowup) {
        put(ACTUAL_FOLLOWUP, Boolean.valueOf(actualFollowup));
    }

}

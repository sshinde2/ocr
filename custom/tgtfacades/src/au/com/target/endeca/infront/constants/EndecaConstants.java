/**
 * 
 */
package au.com.target.endeca.infront.constants;

/**
 * @author Paul
 * 
 */
public interface EndecaConstants {

    String ENDECA_QUERY_STRING = "Endeca-QueryString";

    String QUERY_PARAM_SEPERATOR_AMP = "&";

    String QUERY_PARAM_SEPERATOR_PLUS = "+";

    String QUERY_PARAM_JOIN = "Z";

    String QUERY_PARAM_DOUBLE_PIPE = "||";

    String QUERY_PARAM_SINGLE_PIPE = "|";

    String ENCODED_QUERY_PARAM_SINGLE_PIPE = "%7C";

    String QUERY_PARAM_SEPERATOR_QNMRK = "?";

    String ENDECA_NAVIGATION_STATE_SEPERATOR = "/_/";

    String OFFSET = "offset";

    String RECORDS_PER_PAGE = "recordsPerPage";

    String WHY_MATCH_ENABLED = "whyMatchEnabled";

    String WHY_RANK_ENABLED = "whyRankEnabled";

    String SORT_OPTION = "sortOption";

    String REL_RANK_MATCH_MODE = "relRankMatchMode";

    String MATCH = "match";

    String DIMENSION_SEARCH_RESULTS = "dimensionSearchResults";

    String DIMENSION_SEARCH_GROUPS = "dimensionSearchGroups";

    String TO_TARGET_URL = "TARGET";

    String TO_ENDECA_URL = "ENDECA";

    String SORT_TERM_KEY = "Ns";

    String PAGE_SIZE_KEY = "Nrpp";

    String CODE_LITERAL = "UTF-8";

    String INVALID_ENDECA_SEARCH_LITERALS = "[<>]";

    String ENDECA_DIMENSIONS = "EndecaDimensions";

    int ENDECA_DIMENSIONS_DEPTH_VALUE = 1;

    String ENDECA_N_VALUE = "999";

    String ENDECA_N_0 = "0";

    String ENDECA_NP_1 = "1";

    String ENDECA_NP_2 = "2";

    String SEARCH_FIELD_PRODUCT_CODE = "productCode";

    String SEARCH_FIELD_CV_PRODUCT_CODE_STRING = "colourVariantCode";

    String SEARCH_FIELD_RECOMMENDED_PRODUCTS = "recommendedProduct";

    String MATCH_MODE_ANY = "matchany";

    String MATCH_MODE_ALL = "matchall";

    String LISTING_VIEW_AS_KEY = "viewAs=";

    String ENDECA_PAGE_DEFAULT_TEMPLATE = "No=%7Boffset%7D";

    String ENDECA_DATE_FORMAT = "MMM dd yyyy hh:mma";

    String ENDECA_CONCATENATE_SYMBOL = "@@";

    String ENDECA_CONCATENATE_SYMBOL_RECOMMENDATIONS = "@";

    String ENDECA_BOOLEAN_VALUE = "1";

    String WS_URL_CATEGORY = "category=";
    String WS_URL_BRAND = "brand=";

    public interface EndecaPageUris {

        String ENDECA_BASE_PAGES_URI = "/pages";

        String ENDECA_DIMENSION_SEARCH = "/services/dimensionsearch";

        String ENDECA_CATEGORY_PAGE_URI = "/pages/category";

        String ENDECA_BRAND_PAGE_URI = "/pages/brand";

        String ENDECA_SEARCH_PAGE_URI = "/pages/search";

        String ENDECA_PRODUCT_PAGE_URI = "/pages/product";

        String ENDECA_CART_PAGE_URI = "/pages/cart";

        String ENDECA_DEAL_QUALIFIER_CATEGORY_PAGE_URI = "/pages/dealQualifierCategory";

        String ENDECA_DEAL_REWARD_CATEGORY_PAGE_URI = "/pages/dealRewardCategory";

        String ENDECA_AUTO_SUGGEST_COMPONENT_URI = "/autoSuggest";

        String ENDECA_PRODUCTS_SEARCH_URI = "/guidedsearchservice";

        String ENDECA_AUTOSUGGEST_SEARCH_CONTENT_PATH = "/content/AutoSuggestPanel";

        String ENDECA_AUTOSUGGEST_SEARCH_TEMPLATE_TYPE = "AutoSuggestPanel";

    }

    public interface EndecaFilterQuery {

        String ENDECA_FILTER_KEY_VALUE_SEPERATOR = ":";

        String ENDECA_MULTIPLE_FILTER_VALUES_SEPERATOR = ",";

        String ENDECA_NAV_STATE_TERM = "N=";

        String ENDECA_NAV_STATE_QUERY_PARAM_TERM = "?N=";

        String ENDECA_SEARCH_TERM = "Ntt=";

        String ENDECA_SEARCH_FIELD = "Ntk=";

        String ENDECA_SEARCH_MATCH_MODE = "Ntx=mode+";

        String ENDECA_SEARCH_PRODUCT_CODES = "productCodes=";

        String ENDECA_SORT_TERM = "Ns=";

        String ENDECA_NO_OF_RECORDS = "Nrpp=";

        String ENDECA_RECORDS_OFFSET = "No=";

        String ENDECA_OR_FILTER_KEY = "OR";

        String ENDECA_AND_FILTER_KEY = "AND";

        String ENDECA_OR_FILTER_START = "(";

        String ENDECA_OR_FILTER_END = ")";

        String ENDECA_RECORD_FILTERS = "Nr=";

    }

    public interface EndecaRecordSpecificFields {
        String ENDECA_CATEGORY = "category";
        String ENDECA_BRAND = "brand";
        String ENDECA_DEAL_QUALIFIER_CATEGORY = "dealQualifierCategory";
        String ENDECA_DEAL_REWARD_CATEGORY = "dealRewardCategory";
        String ENDECA_SORT_RELEVANCE = "relevance";

        String ENDECA_PRODUCT_NAME = "productName";
        String BULKYBOARD = "bulkyboardstorenum";

        String ENDECA_PRODUCT_CODE = "productCode";
        String ENDECA_DESCRIPTION = "description";
        String ENDECA_ONLINE_EXCLUSIVE = "onlineExclusive";
        String ENDECA_ESSENTIAL = "essential";
        String ENDECA_HOTPRODUCT = "hotProduct";
        String ENDECA_NEW_TO_ONLINE = "newToOnline";
        String ENDECA_TARGET_EXCLUSIVE = "targetExclusive";
        String ENDECA_CLEARANCE = "clearance";
        String ENDECA_STOCKLEVELSTATUS = "stockLevelStatus";
        String TOTAL_INTEREST_ON_PRD = "totalInterest";
        String ENDECA_ORIG_CATEGORY = "originalCategoryCode";


        String ENDECA_NUMBER_OF_REVIEWS = "numOfReviews";
        String ENDECA_REVIEW_AVG_RATING = "reviewAvgRating";
        String ENDECA_COLOUR_VARIANT_CODE = "colourVariantCode";
        String ENDECA_SIZE_VARIANT_CODE = "sizeVariantCode";
        String ENDECA_COLOUR = "colour";
        String ENDECA_SWATCH_COLOUR = "swatchColour";
        String ENDECA_IMG_GRID = "imgGrid";
        String ENDECA_IMG_LARGE = "imgLarge";
        String ENDECA_IMG_HERO = "imgHero";
        String ENDECA_IMG_LIST = "imgList";
        String ENDECA_IMG_SWATCH = "imgSwatch";
        String ENDECA_IMG_THUMB = "imgThumb";
        String ENDECA_SHOW_WHEN_OUT_OF_STOCK = "showwhenoutofstock";
        String ENDECA_INSTOCK_FLAG = "inStockFlag";
        String ENDECA_PROMOTIONAL_STATUS = "promotionalStatus";
        String ENDECA_MIN_PRICE = "min_price";
        String ENDECA_MAX_PRICE = "max_price";
        String ENDECA_MIN_WAS_PRICE = "min_wasPrice";
        String ENDECA_MAX_WAS_PRICE = "max_wasPrice";
        String ENDECA_PRICE = "price";
        String ENDECA_WASPRICE = "wasPrice";
        String ENDECA_SIZE = "size";
        String ENDECA_DISPLAY_SIZE = "displaySize";
        String ENDECA_GENDER = "gender";
        String ENDECA_SHOP_BY_PRICE = "shopByPrice";
        String ENDECA_DEAL_DESCRIPTION = "dealDescription";
        String ENDECA_DEAL_STARTTIME = "dealstarttime";
        String ENDECA_DEAL_ENDTIME = "dealendtime";
        String ENDECA_DEAL_FACET = "dealFacet";
        String ENDECA_DIMENSION_SEARCH_AUTO_SUGGEST_ITEM = "DimensionSearchAutoSuggestItem";
        String ENDECA_TYPE = "@type";
        String ENDECA_APPLICABLE_DELIVERY_PROMOTIONS = "applicableDeliveryPromos";
        String ENDECA_PRODUCT_DISPLAY_FLAG = "productDisplayFlag";
        String ENDECA_TOP_LEVEL_CATEGORY_NAME = "topLevelCategoryName";
        String ENDECA_IS_GIFT_CARD = "isGiftCard";
        String ENDECA_PROD_TAGS = "tag";
        String ENDECA_NEW_LOWER_PRICE_FLAG = "newLowerPriceFlag";
        String ENDECA_PREVIEW_FLAG = "previewFlag";
        String ENDECA_AVAIL_QTY = "availQty";
        String ENDECA_AVAIL_QTY_IN_STORE = "availQtyInStore";
        String ENDECA_ACTUAL_CONSOLIDATED_STORE_STOCK = "actualConsolidatedStock";
        String ENDECA_MAX_ACTUAL_CONSOLIDATED_STORE_STOCK = "max_actualConsolidatedStock";
        String ENDECA_ASSORTED = "assorted";
        String ENDECA_AVAILABLE_ONLINE = "availableOnline";
        String ENDECA_MAX_AVAIL_ONLINE_QTY = "max_availQtyOnline";
        String ENDECA_MAX_AVAIL_STORE_QTY = "max_availQtyInStore";
        String ENDECA_DISPLAYONLY_FLAG = "displayOnlyFlag";
        String ENDECA_COLOUR_VARIANT_COUNT = "colourVariantCount";
        String ENDECA_SIZE_VARIANT_COUNT = "sizeVariantCount";
        String ENDECA_MERCH_DEPARTMENT_CODE = "merchDepartmentCode";
        String ENDECA_SIZE_GROUP = "sizeGroup";
        String ENDECA_PRODUCT_DISPLAY_TYPE = "productDisplayType";
        String ENDECA_NORMAL_SALE_START_DATE_TIME = "normalSaleStartDateTime";

    }

    public interface EndecaErrorCodes {
        String ENDECA_ASSEMBLER_EXCEPTION = "assembler-exception";
        String ENDECA_ERROR_STATUS = "endeca-error";
    }

    public interface EndecaSortParams {
        String ENDECA_SORT_NAME_ASC = "||productName|0";
        String ENDECA_SORT_NAME_DESC = "||productName|1";
        String ENDECA_SORT_PRICE_ASC = "||price|0";
        String ENDECA_SORT_PRICE_DESC = "||price|1";
        String ENDECA_SORT_RATING = "||reviewAvgRating|1";
        String ENDECA_SORT_LATEST = "||onlineDate|1";
        String ENDECA_SORT_BRAND = "||brand|0";
        String ENDECA_SORT_SALES = "||noOfSales|1";
        String ENDECA_SORT_DEFAULT = "||inStockFlag|1";
    }

    public interface FieldList {
        String COMPONENT_FIELDLIST = "tgtstorefront.components.selection.fieldlist";
        String URGENCYTOPURCHASE_FIELDLIST = "tgtstorefront.urgenttopurchase.selection.fieldlist";
        String STORESTOCK_FIELDLIST = "tgtfacades.instorestock.selection.fieldlist";
        String SITEMPAP_FIELDLIST = "tgtfacades.sitemap.selection.fieldlist";

    }

    public interface EndecaParserNodes {
        String ENDECA_CONTENTS = "contents";
        String ENDECA_MAIN_CONTENT = "mainContent";
        String ENDECA_BREADCRUMBS = "Breadcrumbs";
        String ENDECA_CONTENT_SLOT_SECONDARY = "ContentSlotSecondary";
        String ENDECA_SECONDARY_CONTENT = "secondaryContent";
        String ENDECA_GUIDED_NAVIGATION = "GuidedNavigation";
        String ENDECA_CONTENT_SLOT_MAIN = "ContentSlotMain";
        String ENDECA_RESULTS_LIST = "ResultsList";
        String ENDECA_RESULTS_LIST_SEARCH = "ResultsListSearch";
        String ENDECA_RESULTS_LIST_CATEGORY = "ResultsListCategory";
        String ENDECA_RESULTS_LIST_BRAND = "ResultsListBrand";
        String ENDECA_RECOMMENDATIONS_BOUGHTBOUGHT = "RecommendationsBoughtBought";

        String ENDECA_REFINEMENTCRUMBS = "refinementCrumbs";
        String ENDECA_REFINEMENT_CAT_CODE = "code";
        String ENDECA_REFINEMENTS_CODE = "code";
        String ENDECA_REFINEMENTS = "refinements";
        String ENDECA_NAVIGATION = "navigation";
        String REFINEMENT_PROPERTY_NAME = "name";
        String ENDECA_REFINEMENT_SELECTED = "selected";
        String ENDECA_REFINEMENT_DIMID = "dimId";


        String ENDECA_TOTAL_NUM_RECS = "totalNumRecs";
        String ENDECA_RECS_PER_PAGE = "recsPerPage";
        String ENDECA_SORT_OPTIONS = "sortOptions";
        String ENDECA_PAGING_ACTION_TEMPLATE = "pagingActionTemplate";
        String ENDECA_RECORDS = "records";
        String ENDECA_PARENT_REC_KEY = "parent";
        String ENDECA_CHILD_REC_KEY = "child";

        String ENDECA_ERROR = "@error";

    }

    public interface EndecaParamMarshaller {
        String ENDECA_PRODUCT_CODES = "productCodes";
        String ENDECA_TOP_LEVEL_CATEGORY_CODE = "topLevelCatCode";
    }

    public interface RefinementMenuConfig {
        String SORT_DEFAULT = "default";
        String SORT_ALPHABETICAL = "static";
        String SORT_FREQUENCY = "dynRank";
    }

    public interface DimensionValue {
        String AVAILABLE_ONLINE_TRUE = "Available Online";
        String NEW_ARRIVALS = "New Arrivals";
    }

}

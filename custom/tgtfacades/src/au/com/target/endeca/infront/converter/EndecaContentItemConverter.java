/**
 * 
 */
package au.com.target.endeca.infront.converter;

import com.endeca.infront.assembler.ContentItem;


/**
 * Endeca content Item converts it and returns the object of type T
 * 
 * @author rsamuel3
 * 
 */
public interface EndecaContentItemConverter<T> {

    /**
     * Method to be implemented to convert the content item to the required type
     * 
     * @param item
     * @param targetObject
     * @return T
     */
    public T convertContentItem(ContentItem item, T targetObject);

}

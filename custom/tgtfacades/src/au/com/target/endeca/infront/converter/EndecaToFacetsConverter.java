/**
 * 
 */
package au.com.target.endeca.infront.converter;

import de.hybris.platform.commerceservices.search.facetdata.FacetData;

import java.util.List;

import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.tgtfacades.product.data.BrandData;


/**
 * @author rsamuel3
 * 
 */
public interface EndecaToFacetsConverter {


    /**
     * Converts the Endeca Dimension to list of facet data in the right order
     * 
     * @param endecaSearch
     * @param searchState
     * @param categoryCode
     * @param brand
     * @return List of faceted data
     */

    List<FacetData<EndecaSearchStateData>> convertToFacetData(final EndecaSearch endecaSearch,
            final EndecaSearchStateData searchState, final String categoryCode, BrandData brand);
}

/**
 * 
 */
package au.com.target.endeca.infront.converter;

import java.util.List;

import au.com.target.endeca.infront.data.EndecaOptions;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.tgtfacades.sort.TargetSortData;


/**
 * @author smudumba
 * 
 */
public interface EndecaToSortConverter {

    /**
     * Converts EndecaOptions to the TargetSortData DTO
     * 
     * @param endecaOptions
     * @param searchStateData
     * @return TargetSortData
     */
    public List<TargetSortData> covertToTargetSortData(List<EndecaOptions> endecaOptions,
            EndecaSearchStateData searchStateData);


}
/**
 * 
 */
package au.com.target.endeca.infront.converter.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.model.Ancestor;
import com.endeca.infront.cartridge.model.NavigationAction;
import com.endeca.infront.cartridge.model.RefinementBreadcrumb;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaAncestor;
import au.com.target.endeca.infront.data.EndecaBreadCrumb;
import au.com.target.endeca.infront.data.EndecaSearch;


/**
 * @author Paul
 * 
 */
public class EndecaContentItemBreadCrumbProcessor {

    public void process(final ContentItem contentItem, final EndecaSearch endecaSearch) {
        if (contentItem.containsKey(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENTCRUMBS)) {
            final List<ContentItem> contentItemList = (List<ContentItem>)contentItem
                    .get(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENTCRUMBS);
            if (CollectionUtils.isEmpty(contentItemList)) {
                return;
            }
            final List<EndecaBreadCrumb> endecaBreadCrumbList = new ArrayList<>();
            for (int i = 0; i < contentItemList.size(); i++) {
                final RefinementBreadcrumb refinementBreadCrumb = (RefinementBreadcrumb)contentItemList.get(i);
                if (refinementBreadCrumb == null || refinementBreadCrumb.isMultiSelect()) {
                    continue;
                }
                final EndecaBreadCrumb endecaBreadCrumb = new EndecaBreadCrumb();
                if (null != refinementBreadCrumb.getProperties()
                        && null != refinementBreadCrumb.getProperties().get(
                                EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_CAT_CODE)) {
                    endecaBreadCrumb.setCode(refinementBreadCrumb.getProperties().get(
                            EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_CAT_CODE));
                }
                endecaBreadCrumb.setDimensionType(refinementBreadCrumb.getDimensionName());
                endecaBreadCrumb.setMultiSelect(refinementBreadCrumb.isMultiSelect());
                endecaBreadCrumb.setCount(String.valueOf(refinementBreadCrumb.getCount()));
                final NavigationAction removeAction = refinementBreadCrumb.getRemoveAction();
                if (null != removeAction) {
                    endecaBreadCrumb.setRemoveNavUrl(removeAction.getNavigationState());
                }
                endecaBreadCrumb.setElement(refinementBreadCrumb.getLabel());
                if (CollectionUtils.isEmpty(refinementBreadCrumb.getAncestors())) {
                    endecaBreadCrumbList.add(endecaBreadCrumb);
                    continue;
                }
                final List<EndecaAncestor> endecaAncestorList = new ArrayList<>();
                for (final Ancestor ancestor : refinementBreadCrumb.getAncestors()) {
                    final EndecaAncestor endecaAncestor = new EndecaAncestor();
                    if (null == ancestor) {
                        continue;
                    }
                    if (null != ancestor.getProperties() && null != ancestor.getProperties().get(
                            EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_CAT_CODE)) {
                        endecaAncestor.setCode(ancestor.getProperties().get(
                                EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_CAT_CODE));
                    }
                    endecaAncestor.setElement(ancestor.getLabel());
                    endecaAncestor.setNavigationState(ancestor.getNavigationState());
                    endecaAncestorList.add(endecaAncestor);
                }
                endecaBreadCrumb.setAncestor(endecaAncestorList);
                endecaBreadCrumbList.add(endecaBreadCrumb);

            }
            endecaSearch.setBreadCrumb(endecaBreadCrumbList);
        }
    }
}

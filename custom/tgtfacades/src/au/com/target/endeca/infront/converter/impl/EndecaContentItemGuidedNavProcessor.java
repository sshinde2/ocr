/**
 * 
 */
package au.com.target.endeca.infront.converter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;

import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.model.Refinement;

import au.com.target.endeca.infront.cartridge.TargetRefinementMenu;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaDimension;
import au.com.target.endeca.infront.data.EndecaSearch;


/**
 * @author Paul
 * 
 */
public class EndecaContentItemGuidedNavProcessor {

    public void process(final ContentItem contentItem, final EndecaSearch endecaSearch) {

        if (contentItem.containsKey(EndecaConstants.EndecaParserNodes.ENDECA_NAVIGATION)) {
            final List<TargetRefinementMenu> contentItemList = (List<TargetRefinementMenu>)contentItem
                    .get(EndecaConstants.EndecaParserNodes.ENDECA_NAVIGATION);
            if (CollectionUtils.isEmpty(contentItemList)) {
                return;
            }
            endecaSearch.setDimension(processGuidedNavigation(contentItemList));
        }

    }

    /**
     * build the guided navigation using the contentItemList
     * 
     * @param refinements
     * @return list of EndecaDimensions
     */
    private List<EndecaDimension> processGuidedNavigation(final List<TargetRefinementMenu> refinements) {
        final List<EndecaDimension> guidedNavigation = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(refinements)) {
            for (final TargetRefinementMenu refinementMenu : refinements) {
                final EndecaDimension dimension = populateEndecaDimension(refinementMenu);
                guidedNavigation.add(dimension);
            }
        }
        return guidedNavigation;
    }

    /**
     * @param refinementMenu
     * @return Endeca dimension
     */
    protected EndecaDimension populateEndecaDimension(final TargetRefinementMenu refinementMenu) {
        final EndecaDimension dimension = new EndecaDimension();

        String name = StringUtils.EMPTY;
        if (refinementMenu.containsKey(EndecaConstants.EndecaParserNodes.REFINEMENT_PROPERTY_NAME)) {
            name = (String)refinementMenu.get(EndecaConstants.EndecaParserNodes.REFINEMENT_PROPERTY_NAME);
        }
        dimension.setName(name);
        dimension.setMultiSelect(refinementMenu.isMultiSelect());
        dimension.setDimensionName(refinementMenu.getDimensionName());
        if (refinementMenu.containsKey(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENTS)) {
            final List<Refinement> refinements = (List<Refinement>)refinementMenu
                    .get(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENTS);
            final List<EndecaDimension> dimensions = new ArrayList<>();
            if (CollectionUtils.isNotEmpty(refinements)) {
                for (final Refinement menu : refinements) {
                    final EndecaDimension menuDimension = new EndecaDimension();
                    menuDimension.setNavigationState(menu.getNavigationState());
                    menuDimension.setCount(menu.getCount().toString());
                    menuDimension.setName(menu.getLabel());
                    menuDimension.setMultiSelect(menu.isMultiSelect());
                    final Map<String, String> properties = menu.getProperties();
                    if (MapUtils.isNotEmpty(properties)) {
                        final String code = properties.get(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENTS_CODE);
                        if (StringUtils.isNotBlank(code)) {
                            menuDimension.setCode(code);
                        }
                        else {
                            menuDimension.setCode(properties.get(
                                    EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_DIMID));
                        }
                        if (properties.containsKey(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_SELECTED)) {
                            menuDimension.setSelected(true);
                            dimension.setSelected(true);
                        }
                    }
                    dimensions.add(menuDimension);
                }
            }
            dimension.setRefinement(dimensions);
            dimension.setSeoFollowEnabled(
                    refinementMenu.isSeoFollowEnabled());
            dimension.setActualFollowup(refinementMenu.isActualFollowup());
        }
        return dimension;
    }

}



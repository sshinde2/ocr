/**
 * 
 */
package au.com.target.endeca.infront.converter.impl;

import java.util.LinkedList;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.endeca.infront.data.EndecaSearch;

import com.endeca.infront.assembler.ContentItem;


/**
 * @author ragarwa3
 *
 */
public class EndecaContentItemRecommendationsProcessor {

    /**
     * Process the content item for recommended products data.
     *
     * @param contentItem
     *            the content item
     * @param endecaSearch
     *            the endeca search
     */
    public void process(final ContentItem contentItem, final EndecaSearch endecaSearch) {

        final LinkedList recommendations = new LinkedList<>();
        final LinkedList<ContentItem> existingRecommendations = endecaSearch.getRecommendations();

        if (CollectionUtils.isNotEmpty(existingRecommendations)) {
            for (final ContentItem contItem : existingRecommendations) {
                recommendations.add(contItem);
            }
        }

        recommendations.add(contentItem);
        endecaSearch.setRecommendations(recommendations);
    }
}

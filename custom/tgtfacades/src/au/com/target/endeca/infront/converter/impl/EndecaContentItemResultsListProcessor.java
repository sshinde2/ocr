/**
 * 
 */
package au.com.target.endeca.infront.converter.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.model.NavigationAction;
import com.endeca.infront.cartridge.model.Record;
import com.endeca.infront.cartridge.model.SortOptionLabel;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaOptions;
import au.com.target.endeca.infront.data.EndecaProduct;
import au.com.target.endeca.infront.data.EndecaRecords;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.tgtutility.util.TargetDateUtil;


/**
 * @author Paul
 * 
 */
public class EndecaContentItemResultsListProcessor {

    private static final Logger LOG = Logger.getLogger(EndecaContentItemResultsListProcessor.class);

    public void process(final ContentItem contentItem, final EndecaSearch endecaSearch) {

        final EndecaRecords endecaRecs = new EndecaRecords();
        String totalNoOfRecs = StringUtils.EMPTY;
        String totalRecsPerPage = StringUtils.EMPTY;

        if (null != contentItem.get(EndecaConstants.EndecaParserNodes.ENDECA_TOTAL_NUM_RECS)) {
            totalNoOfRecs = String.valueOf(contentItem
                    .get(EndecaConstants.EndecaParserNodes.ENDECA_TOTAL_NUM_RECS));
            endecaRecs.setTotalCount(totalNoOfRecs);
        }
        if (null != contentItem.get(EndecaConstants.EndecaParserNodes.ENDECA_RECS_PER_PAGE)) {
            totalRecsPerPage = String.valueOf(contentItem
                    .get(EndecaConstants.EndecaParserNodes.ENDECA_RECS_PER_PAGE));
            endecaRecs.setRecordsPerPage(totalRecsPerPage);
        }

        //logging the total records and total records per page
        LOG.info("Total number of records/products:" + totalNoOfRecs + " Total number of records/products per page:"
                + totalRecsPerPage);


        if (null != contentItem.get(EndecaConstants.EndecaParserNodes.ENDECA_SORT_OPTIONS)) {
            endecaRecs.setOption(processSortOptions((List<SortOptionLabel>)contentItem
                    .get(EndecaConstants.EndecaParserNodes.ENDECA_SORT_OPTIONS)));
        }
        if (null != contentItem.get(EndecaConstants.EndecaParserNodes.ENDECA_PAGING_ACTION_TEMPLATE)) {
            final NavigationAction navState = (NavigationAction)contentItem
                    .get(EndecaConstants.EndecaParserNodes.ENDECA_PAGING_ACTION_TEMPLATE);
            endecaRecs.setCurrentNavState(navState.getNavigationState());
        }
        final List<EndecaProduct> endecaProductList = new ArrayList<>();
        if (null != contentItem.get(EndecaConstants.EndecaParserNodes.ENDECA_RECORDS)) {
            final List<Record> recordList = (List<Record>)contentItem
                    .get(EndecaConstants.EndecaParserNodes.ENDECA_RECORDS);
            processRecords(recordList, endecaProductList, EndecaConstants.EndecaParserNodes.ENDECA_PARENT_REC_KEY);
            endecaRecs.setProduct(endecaProductList);
        }

        endecaSearch.setRecords(endecaRecs);

    }

    /**
     * Endeca Product List can be a list of child products or the parent products.
     * 
     * @param recordList
     * @param endecaProductList
     * @param type
     */
    private void processRecords(final List<Record> recordList, final List<EndecaProduct> endecaProductList,
            final String type) {

        for (final Record record : recordList) {
            //endecaProduct will be null for the parent product.
            if (StringUtils.equals(EndecaConstants.EndecaParserNodes.ENDECA_PARENT_REC_KEY, type)) {
                final EndecaProduct endecaParentProduct = new EndecaProduct();
                if (null != record.getAttributes()) {
                    populateProductLevelDetails(record.getAttributes(), endecaParentProduct);
                }
                if (null != record.getRecords()) {
                    final List<EndecaProduct> endecaChildProductList = new ArrayList<>();
                    processRecords(record.getRecords(), endecaChildProductList,
                            EndecaConstants.EndecaParserNodes.ENDECA_CHILD_REC_KEY);
                    endecaParentProduct.setProduct(endecaChildProductList);
                }
                endecaProductList.add(endecaParentProduct);
            }
            else { //endecaProduct exists - processing child records
                final EndecaProduct endecaChildProduct = new EndecaProduct();
                if (null != record.getAttributes()) {
                    populateChildProductDetails(record.getAttributes(), endecaChildProduct);
                }
                endecaProductList.add(endecaChildProduct);
            }
        }

    }




    private String getStringValueOfRecordAttribute(final Map attributes, final String key) {
        if (null != attributes.get(key)) {
            return String.valueOf(attributes.get(key));
        }
        return null;
    }

    private Date getDateValueOfRecordAttribute(final Map attributes, final String key) {
        final String value = getStringValueOfRecordAttribute(attributes, key);
        if (StringUtils.isNotEmpty(value)) {
            return TargetDateUtil.getStringAsDate(value, "yyyy-MM-dd HH:mm:ss.S");
        }
        return null;
    }

    private Boolean evaluateBooleanForRecordAttribute(final Map attributes, final String key) {
        if (StringUtils.equals(getStringValueOfRecordAttribute(attributes, key), "1")) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    private List<String> getListValueForRecordAttribute(final Map attributes, final String key) {
        final Object listVals = attributes.get(key);
        if (null != listVals && listVals instanceof List<?>) {
            return (List<String>)listVals;
        }
        return null;
    }

    private void populateProductLevelDetails(final Map attributes, final EndecaProduct endecaParentProduct) {
        final String code = getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE);
        endecaParentProduct.setCode(code);
        endecaParentProduct.setBaseProductCode(code);
        endecaParentProduct.setName(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_NAME));
        endecaParentProduct.setBrand(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND));
        endecaParentProduct.setDescription(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DESCRIPTION));
        endecaParentProduct.setOnlineExclusive(evaluateBooleanForRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_ONLINE_EXCLUSIVE));
        endecaParentProduct.setEssential(evaluateBooleanForRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_ESSENTIAL));
        endecaParentProduct.setClearance(evaluateBooleanForRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_CLEARANCE));
        endecaParentProduct.setHotProduct(evaluateBooleanForRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_HOTPRODUCT));
        endecaParentProduct.setNewToOnline(evaluateBooleanForRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_NEW_TO_ONLINE));
        endecaParentProduct.setTargetExclusive(evaluateBooleanForRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_TARGET_EXCLUSIVE));
        endecaParentProduct.setTopLevelCategoryName(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_TOP_LEVEL_CATEGORY_NAME));
        endecaParentProduct.setIsGiftCard(evaluateBooleanForRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_IS_GIFT_CARD));
        endecaParentProduct.setMaxPrice(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_PRICE));
        endecaParentProduct.setMinPrice(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_MIN_PRICE));
        endecaParentProduct.setMaxWasPrice(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_WAS_PRICE));
        endecaParentProduct.setMinWasPrice(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_MIN_WAS_PRICE));

        endecaParentProduct.setNumOfReviews(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_NUMBER_OF_REVIEWS));

        endecaParentProduct.setReviewAvgRating(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_REVIEW_AVG_RATING));

        endecaParentProduct.setDealDescription(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_DESCRIPTION));
        endecaParentProduct.setDealStartTime(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_STARTTIME));
        endecaParentProduct.setDealEndtime(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_ENDTIME));
        endecaParentProduct.setApplicableDeliveryPromotions(getListValueForRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_APPLICABLE_DELIVERY_PROMOTIONS));
        endecaParentProduct.setTag(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_PROD_TAGS));
        endecaParentProduct.setNewLowerPriceFlag(evaluateBooleanForRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_NEW_LOWER_PRICE_FLAG));
        endecaParentProduct.setPreviewFlag(evaluateBooleanForRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_PREVIEW_FLAG));

        endecaParentProduct.setMaxAvailOnlineQty(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_AVAIL_ONLINE_QTY));

        endecaParentProduct.setMaxAvailStoreQty(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_AVAIL_STORE_QTY));

    }



    private void populateChildProductDetails(final Map attributes, final EndecaProduct endecaChildProduct) {
        endecaChildProduct.setColourVariantCode(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE));
        endecaChildProduct.setColour(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR));
        endecaChildProduct.setShowwhenoutofstock(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_SHOW_WHEN_OUT_OF_STOCK));
        endecaChildProduct.setSwatchColour(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_SWATCH_COLOUR));

        endecaChildProduct.setInStockFlag(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_INSTOCK_FLAG));
        endecaChildProduct.setStockLevelStatus(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_STOCKLEVELSTATUS));

        endecaChildProduct.setGridImage(getListValueForRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_IMG_GRID));
        endecaChildProduct.setListImage(getListValueForRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_IMG_LIST));
        endecaChildProduct.setSwatchImage(getListValueForRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_IMG_SWATCH));
        endecaChildProduct.setThumbImage(getListValueForRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_IMG_THUMB));
        endecaChildProduct.setLargeImage(getListValueForRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_IMG_LARGE));
        endecaChildProduct.setHeroImage(getListValueForRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_IMG_HERO));
        endecaChildProduct.setAssorted(evaluateBooleanForRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_ASSORTED));
        endecaChildProduct.setDisplayOnlyFlag(evaluateBooleanForRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DISPLAYONLY_FLAG));

        endecaChildProduct.setProductDisplayType(getStringValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_DISPLAY_TYPE));
        endecaChildProduct.setNormalSaleStartDateTime(getDateValueOfRecordAttribute(attributes,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_NORMAL_SALE_START_DATE_TIME));
    }

    private List<EndecaOptions> processSortOptions(final List<SortOptionLabel> sortOptionLabels) {
        final List<EndecaOptions> endecaOptionsList = new ArrayList<>();
        for (final SortOptionLabel sortOptionLabel : sortOptionLabels) {
            final EndecaOptions endecaOptions = new EndecaOptions();
            endecaOptions.setName(sortOptionLabel.getLabel());
            endecaOptions.setNavigationState(sortOptionLabel.getNavigationState());
            endecaOptions.setSelected(sortOptionLabel.isSelected());
            endecaOptionsList.add(endecaOptions);
        }
        return endecaOptionsList;
    }




}

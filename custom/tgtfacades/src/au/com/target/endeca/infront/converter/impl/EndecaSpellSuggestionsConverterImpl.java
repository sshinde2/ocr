/**
 * 
 */
package au.com.target.endeca.infront.converter.impl;

import java.util.List;

import au.com.target.endeca.infront.converter.EndecaContentItemConverter;
import au.com.target.endeca.infront.data.EndecaSearch;

import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.assembler.event.request.RequestEvent;


/**
 * Converter to populate EndecaSearch object with spell suggestion data.
 * 
 * @author jjayawa1
 * 
 */
public class EndecaSpellSuggestionsConverterImpl implements EndecaContentItemConverter<EndecaSearch> {
    private static final String ENDECA_ASSEMBLER_REQUEST_INFO_FIELD = "endeca:assemblerRequestInformation";
    private static final String ENDECA_ASSEMBLER_DID_YOU_MEAN_FIELD = "endeca:didYouMeanTo";
    private static final String ENDECA_ASSEMBLER_AUTOCORRECT_FIELD = "endeca:autocorrectTo";

    /* (non-Javadoc)
     * @see au.com.target.endeca.infront.converter.EndecaContentItemConverter#convertContentItem(com.endeca.infront.assembler.ContentItem, java.lang.Object)
     */
    @Override
    public EndecaSearch convertContentItem(final ContentItem item, final EndecaSearch targetObject) {
        if (item.containsKey(ENDECA_ASSEMBLER_REQUEST_INFO_FIELD)) {
            final Object obj = item.get(ENDECA_ASSEMBLER_REQUEST_INFO_FIELD);
            if (obj instanceof RequestEvent) {
                final RequestEvent requestEvent = (RequestEvent)obj;
                if (requestEvent.containsKey(ENDECA_ASSEMBLER_DID_YOU_MEAN_FIELD)) {
                    targetObject.setSpellingSuggestions((List)requestEvent
                            .get(ENDECA_ASSEMBLER_DID_YOU_MEAN_FIELD));
                }

                if (requestEvent.containsKey(ENDECA_ASSEMBLER_AUTOCORRECT_FIELD)) {
                    targetObject.setAutoCorrectedTerm((String)requestEvent
                            .get(ENDECA_ASSEMBLER_AUTOCORRECT_FIELD));
                }
            }
        }
        return targetObject;
    }

}

/**
 * 
 */

package au.com.target.endeca.infront.converter.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;
import de.hybris.platform.commerceservices.url.UrlResolver;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.converter.EndecaToFacetsConverter;
import au.com.target.endeca.infront.data.EndecaDimension;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.data.TargetFacetData;
import au.com.target.endeca.infront.transform.TargetEndecaURLHelper;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.product.data.BrandData;


/**
 * @author rsamuel3
 * 
 */

public class EndecaToFacetsConverterImpl<STATE> implements EndecaToFacetsConverter, EndecaConstants {

    private UrlResolver<CategoryData> categoryDataUrlResolver;

    private UrlResolver<BrandData> brandDataUrlResolver;

    private TargetEndecaURLHelper targetEndecaURLHelper;

    private CommerceCategoryService commerceCategoryService;

    private UrlResolver<TargetDealCategoryModel> dealCategoryModelUrlResolver;

    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    private List<String> isolatedFacets;


    @Override
    public List<FacetData<EndecaSearchStateData>> convertToFacetData(final EndecaSearch endecaSearch,
            final EndecaSearchStateData searchState, final String categoryCode, final BrandData brand) {
        final List<FacetData<EndecaSearchStateData>> facetedData = new ArrayList<FacetData<EndecaSearchStateData>>();
        final List<EndecaDimension> dimensions = endecaSearch.getDimension();
        if (CollectionUtils.isNotEmpty(dimensions)) {
            for (final EndecaDimension dimension : dimensions) {
                processDealFacetData(dimension);
                final String name = dimension.getName();
                final String dimensionName = dimension.getDimensionName();
                final boolean shouldDisplayFacet = shouldDisplayFacets(brand, dimensionName);
                if (shouldDisplayFacet) {
                    final TargetFacetData<EndecaSearchStateData> facetData = createFacetData(dimensionName, name);
                    populateFacetData(searchState, categoryCode, dimension, dimensionName, facetData);
                    facetedData.add(facetData);
                }
            }
        }

        return facetedData;
    }

    /**
     * populates all the data required for the facet.
     * 
     * @param searchState
     * @param categoryCode
     * @param dimension
     * @param dimensionName
     * @param facetData
     */
    private void populateFacetData(final EndecaSearchStateData searchState, final String categoryCode,
            final EndecaDimension dimension, final String dimensionName,
            final TargetFacetData<EndecaSearchStateData> facetData) {
        facetData.setMultiSelect(dimension.isMultiSelect());
        facetData.setExpand(dimension.isSelected());
        facetData.setSeoFollowEnabled(dimension.isSeoFollowEnabled());

        if (isolatedFacets.contains(dimensionName)) {
            facetData.setIsolated(true);
        }

        if (EndecaConstants.EndecaRecordSpecificFields.ENDECA_DISPLAY_SIZE
                .equalsIgnoreCase(dimensionName)) {
            if (targetFeatureSwitchFacade.isSizeOrderingFacetsEnabled()) {
                final List<TargetFacetData<EndecaSearchStateData>> data = populateDisplaySizeObjects(
                        dimension.getRefinement(), dimensionName,
                        searchState, categoryCode);
                facetData.setFacets(data);
            }
        }
        else {
            final List<FacetValueData<EndecaSearchStateData>> facetValues = new ArrayList<FacetValueData<EndecaSearchStateData>>();
            facetValues.addAll(getFacetValues(
                    dimension.getRefinement(), dimensionName,
                    searchState,
                    categoryCode));
            facetData.setValues(facetValues);
        }
    }

    /**
     * @param refinementList
     * @param searchState
     * @param dimensionName
     * @param categoryCode
     * @return list
     */
    protected List<TargetFacetData<EndecaSearchStateData>> populateDisplaySizeObjects(
            final List<EndecaDimension> refinementList, final String dimensionName,
            final EndecaSearchStateData searchState, final String categoryCode) {
        Collections.sort(refinementList, new Comparator<EndecaDimension>() {

            @Override
            public int compare(final EndecaDimension dimension1, final EndecaDimension dimension2) {
                return dimension1.getName().compareTo(dimension2.getName());
            }
        });
        return populateFacets(refinementList, dimensionName, searchState, categoryCode);
    }


    /**
     * @param refinementList
     * @param dimensionName
     * @param searchState
     * @param categoryCode
     * @return list
     */
    protected List<TargetFacetData<EndecaSearchStateData>> populateFacets(
            final List<EndecaDimension> refinementList, final String dimensionName,
            final EndecaSearchStateData searchState, final String categoryCode) {
        final Map<String, TargetFacetData<EndecaSearchStateData>> facetedData = new LinkedHashMap<String, TargetFacetData<EndecaSearchStateData>>();
        for (final EndecaDimension refinement : refinementList) {
            final String name = refinement.getName();
            final String[] split = name.split("\\*\\*");
            final String groupName = split[1];
            TargetFacetData<EndecaSearchStateData> data = null;
            List<FacetValueData<EndecaSearchStateData>> facetValues = null;
            if (facetedData.containsKey(groupName)) {
                data = facetedData.get(groupName);
                facetValues = data.getValues();
            }
            else {
                data = new TargetFacetData<>();
                data.setName(groupName);
                data.setCode(groupName.replaceAll("\\s", "").toLowerCase());
                facetValues = new ArrayList<FacetValueData<EndecaSearchStateData>>();
            }
            final FacetValueData<EndecaSearchStateData> facetValueData = populateFacetValueData(dimensionName,
                    searchState, categoryCode, refinement, split[3]);
            if (facetValueData.isSelected()) {
                data.setExpand(true);
            }
            facetValues.add(facetValueData);
            data.setValues(facetValues);
            facetedData.put(groupName, data);
        }
        return new ArrayList<TargetFacetData<EndecaSearchStateData>>(facetedData.values());
    }

    /**
     * Process the dimension to decide whether to display the facet
     * 
     * @param dimension
     * 
     */
    protected void processDealFacetData(final EndecaDimension dimension) {
        if (null == dimension.getDimensionName() || (null != dimension.getDimensionName()
                && (!EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_FACET.equalsIgnoreCase(dimension
                        .getDimensionName())))) {
            return;
        }
        final List<EndecaDimension> refinementsToBeRemoved = new ArrayList<>();
        final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy hh:mma");
        final Date currentDate = new Date();
        String startDateStr = null;
        String[] tokens;
        String endDateStr = null;
        String refinementName = null;
        if (null != dimension.getRefinement()) {
            for (final EndecaDimension refinement : dimension.getRefinement()) {
                tokens = refinement.getName().split("@@");
                startDateStr = tokens[0];
                endDateStr = tokens[2];
                refinementName = tokens[1];
                if (StringUtils.isNotEmpty(startDateStr) && StringUtils.isNotEmpty(endDateStr)) {

                    try {
                        final Date endDate = dateFormat.parse(endDateStr);
                        final Date startDate = dateFormat.parse(startDateStr);
                        if ((null == endDate) || (null == startDate) || endDate.before(currentDate)
                                || startDate.after(currentDate) || (StringUtils.isEmpty(refinementName))) {
                            refinementsToBeRemoved.add(refinement);
                        }
                        else {
                            refinement.setName(refinementName);
                        }
                    }
                    catch (final ParseException pex) {
                        refinementsToBeRemoved.add(refinement);
                        continue;
                    }
                }
            }
            dimension.getRefinement().removeAll(refinementsToBeRemoved);
        }
    }

    /**
     * create a basic facetData
     * 
     * @param dimensionName
     * @param name
     * @return a newly created FacetData
     */
    protected TargetFacetData<EndecaSearchStateData> createFacetData(final String dimensionName, final String name) {
        final TargetFacetData<EndecaSearchStateData> facetData = new TargetFacetData<>();
        facetData.setCode(dimensionName);
        if (dimensionName.equals(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY)) {
            facetData.setCategory(true);
        }
        facetData.setName(name);
        return facetData;
    }

    /**
     * @param brand
     * @param name
     * @return true if the facets are to be displayed and false otherwise
     */
    private boolean shouldDisplayFacets(final BrandData brand, final String name) {
        boolean shouldDisplayFacet = true;
        if (brand != null && EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND.equals(name)) {
            shouldDisplayFacet = false;
        }
        return shouldDisplayFacet;
    }

    /**
     * setup the faceted values
     * 
     * @param refinement
     * @param dimensionType
     * @param searchState
     * @param categoryCode
     * @return List of facetedValueData for the dimension
     */
    private List<FacetValueData<EndecaSearchStateData>> getFacetValues(final List<EndecaDimension> refinement,
            final String dimensionType, final EndecaSearchStateData searchState, final String categoryCode) {
        final List<FacetValueData<EndecaSearchStateData>> facetedValues = new ArrayList<FacetValueData<EndecaSearchStateData>>();
        for (final EndecaDimension dimension : refinement) {
            final FacetValueData<EndecaSearchStateData> facetValueData = populateFacetValueData(dimensionType,
                    searchState, categoryCode, dimension, null);
            facetedValues.add(facetValueData);
        }
        return facetedValues;
    }

    /**
     * @param dimensionType
     * @param searchState
     * @param categoryCode
     * @param dimension
     * @return FacetValueData<EndecaSearchStateData>
     */
    protected FacetValueData<EndecaSearchStateData> populateFacetValueData(final String dimensionType,
            final EndecaSearchStateData searchState, final String categoryCode, final EndecaDimension dimension,
            final String dimensionName) {
        final FacetValueData<EndecaSearchStateData> facetValueData = new FacetValueData<>();
        final String code = dimension.getCode();
        facetValueData.setCode(code);
        if (StringUtils.isNotEmpty(dimensionName)) {
            facetValueData.setName(dimensionName);
        }
        else {
            facetValueData.setName(dimension.getName());
        }
        final String count = dimension.getCount();
        if (StringUtils.isNotBlank(count) && StringUtils.isNumeric(count)) {
            facetValueData.setCount(Long.parseLong(count));
        }
        final String navigationState = dimension.getNavigationState();
        buildUrl(facetValueData, searchState, categoryCode, navigationState, dimensionType, code);
        facetValueData.setSelected(dimension.isSelected());
        return facetValueData;
    }

    private void buildUrl(final FacetValueData<EndecaSearchStateData> facetValueData,
            final EndecaSearchStateData searchState, final String categoryCode,
            final String navigationState, final String dimensionType, final String dimensionCode) {

        String url = StringUtils.EMPTY;
        String wsUrl = StringUtils.EMPTY;
        if (searchState.getBrand() != null) {
            url += getCategoryOrBrandUrl(searchState.getBrand().getCode(), null);
        }
        else if (StringUtils.isNotEmpty(categoryCode)) {
            final CategoryModel categoryModel = commerceCategoryService.getCategoryForCode(categoryCode);
            if (categoryModel instanceof TargetDealCategoryModel) {
                url += dealCategoryModelUrlResolver.resolve((TargetDealCategoryModel)categoryModel);
            }
            else {
                String code = categoryCode;
                if (EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY.equals(dimensionType)
                        && StringUtils.isNotBlank(dimensionCode)) {
                    code = dimensionCode;
                }
                if (StringUtils.isNotEmpty(code)) {
                    url += getCategoryUrl(code);
                }
            }

        }

        //Validate the navigationState
        if (StringUtils.isNotEmpty(navigationState)) {
            String finalNavigationString = StringUtils.EMPTY;
            finalNavigationString = targetEndecaURLHelper.getValidEndecaToTargetNavigationState(navigationState);
            if (StringUtils.isNotEmpty(finalNavigationString)) {
                url += finalNavigationString;
                wsUrl += this.createWsUrl(dimensionType, dimensionCode, finalNavigationString, searchState);
            }
        }

        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setNavigationState(Collections.singletonList(url));
        endecaSearchStateData.setUrl(url);
        endecaSearchStateData.setWsUrl(wsUrl);
        facetValueData.setQuery(endecaSearchStateData);
    }

    private String createWsUrl(final String dimensionType, final String dimensionCode,
            final String finalNavigationString,
            final EndecaSearchStateData searchState) {
        String wsUrl = StringUtils.EMPTY;
        if (!(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY.equals(dimensionType) && StringUtils
                .isNotEmpty(dimensionCode))) {
            wsUrl += finalNavigationString;
            if (StringUtils.isNotEmpty(searchState.getCurrentQueryParam())) {
                wsUrl += searchState.getCurrentQueryParam();
            }
        }
        return wsUrl;
    }

    /**
     * @param code
     * @return String
     */
    private String getCategoryUrl(final String code) {
        final CategoryData categoryData = new CategoryData();
        categoryData.setCode(code);
        return categoryDataUrlResolver.resolve(categoryData);
    }

    protected String getCategoryOrBrandUrl(final String brandCode, final String categoryCode) {
        if (brandCode != null) {
            final BrandData brandData = new BrandData();
            brandData.setCode(brandCode);
            return brandDataUrlResolver.resolve(brandData);
        }
        else {
            return getCategoryUrl(categoryCode);
        }
    }



    /**
     * @param categoryDataUrlResolver
     *            the categoryDataUrlResolver to set
     */
    @Required
    public void setCategoryDataUrlResolver(final UrlResolver<CategoryData> categoryDataUrlResolver) {
        this.categoryDataUrlResolver = categoryDataUrlResolver;
    }


    /**
     * @param brandDataUrlResolver
     *            the brandDataUrlResolver to set
     */
    @Required
    public void setBrandDataUrlResolver(final UrlResolver<BrandData> brandDataUrlResolver) {
        this.brandDataUrlResolver = brandDataUrlResolver;
    }

    /**
     * @param commerceCategoryService
     *            the commerceCategoryService to set
     */
    @Required
    public void setCommerceCategoryService(final CommerceCategoryService commerceCategoryService) {
        this.commerceCategoryService = commerceCategoryService;
    }

    /**
     * @param dealCategoryModelUrlResolver
     *            the dealCategoryModelUrlResolver to set
     */
    @Required
    public void setDealCategoryModelUrlResolver(
            final UrlResolver<TargetDealCategoryModel> dealCategoryModelUrlResolver) {
        this.dealCategoryModelUrlResolver = dealCategoryModelUrlResolver;
    }

    /**
     * @param targetFeatureSwitchFacade
     *            the targetFeatureSwitchFacade to set
     */
    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }

    /**
     * @param targetEndecaURLHelper
     *            the targetEndecaURLHelper to set
     */
    @Required
    public void setTargetEndecaURLHelper(final TargetEndecaURLHelper targetEndecaURLHelper) {
        this.targetEndecaURLHelper = targetEndecaURLHelper;
    }


    /**
     * @param isolatedFacets
     *            the isolatedFacets to set
     */
    @Required
    public void setIsolatedFacets(final List<String> isolatedFacets) {
        this.isolatedFacets = isolatedFacets;
    }
}

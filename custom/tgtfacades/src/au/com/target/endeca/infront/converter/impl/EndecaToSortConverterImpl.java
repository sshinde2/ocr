/**
 * 
 */
package au.com.target.endeca.infront.converter.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.converter.EndecaToSortConverter;
import au.com.target.endeca.infront.data.EndecaOptions;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.transform.TargetEndecaURLHelper;
import au.com.target.tgtfacades.sort.TargetSortData;


/**
 * @author smudumba
 * 
 */
public class EndecaToSortConverterImpl implements EndecaToSortConverter {

    private TargetEndecaURLHelper targetEndecaURLHelper;


    /**
     * @param targetEndecaURLHelper
     *            the targetEndecaURLHelper to set
     */
    @Required
    public void setTargetEndecaURLHelper(final TargetEndecaURLHelper targetEndecaURLHelper) {
        this.targetEndecaURLHelper = targetEndecaURLHelper;
    }


    /**
     * Converts EndecaOptions to the TargetSortData DTO
     * 
     * @param endecaOptions
     * @return TargetSortData
     */
    @Override
    public List<TargetSortData> covertToTargetSortData(final List<EndecaOptions> endecaOptions,
            final EndecaSearchStateData searchStateData) {

        final List<TargetSortData> tsdList = new ArrayList<>();
        if (null != endecaOptions) {
            for (final EndecaOptions eo : endecaOptions) {
                final TargetSortData tsd = new TargetSortData();
                //Setup the endeca options
                tsd.setSelected(eo.isSelected());
                tsd.setCode(eo.getName());
                //Validate the the navigation state                
                String finalValidatedNavigationState = StringUtils.EMPTY;
                if (StringUtils.isNotEmpty(eo.getNavigationState())) {
                    finalValidatedNavigationState = targetEndecaURLHelper.getValidEndecaToTargetNavigationState(eo
                            .getNavigationState());
                    if (StringUtils.isNotEmpty(finalValidatedNavigationState)) {
                        tsd.setUrl(searchStateData.getRequestUrl() + finalValidatedNavigationState);
                        String wsUrl = finalValidatedNavigationState;
                        if (StringUtils.isNotEmpty(searchStateData.getCurrentQueryParam())) {
                            wsUrl += searchStateData.getCurrentQueryParam();
                        }
                        tsd.setWsUrl(wsUrl);
                        tsdList.add(tsd);
                    }
                }

            }
        }
        return tsdList;

    }


}

/**
 * 
 */
package au.com.target.endeca.infront.converter.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * Interface to modify default Populator interface.
 * 
 * @author jjayawa1
 * 
 */
public interface EndecaComponentPopulator<SOURCE, TARGET> extends Populator<SOURCE, TARGET> {

    /**
     * Customise populate method to populate data based on selected variant.
     * 
     * @param source
     * @param target
     * @param selectedVariant
     * @throws ConversionException
     */
    public void populate(final SOURCE source, final TARGET target, final String selectedVariant)
            throws ConversionException;
}

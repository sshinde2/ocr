/**
 * 
 */
package au.com.target.endeca.infront.converter.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.data.EndecaProduct;
import au.com.target.endeca.infront.util.TargetEndecaDealDescHelper;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.enums.TargetPromotionStatusEnum;
import au.com.target.tgtfacades.url.impl.TargetProductDataUrlResolver;



/**
 * @author pthoma20
 * 
 */
public class TargetProductListerBasicDataPopulator<SOURCE extends EndecaProduct, TARGET extends TargetProductListerData>

        implements Populator<EndecaProduct, TargetProductListerData> {

    protected static final Logger LOG = Logger.getLogger(TargetProductListerBasicDataPopulator.class);


    private TargetProductDataUrlResolver targetProductDataUrlResolver;

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final EndecaProduct source, final TargetProductListerData target) throws ConversionException {

        target.setName(source.getName());
        target.setCode(source.getCode());
        target.setBaseProductCode(source.getCode());
        target.setBrand(source.getBrand());
        //set the baseName and topLevelCategory for GA
        target.setBaseName(StringEscapeUtils.unescapeHtml(source.getName()));
        target.setTopLevelCategory(source.getTopLevelCategoryName());
        target.setDisplayOnly(BooleanUtils.toBoolean(source.getDisplayOnlyFlag()));
        if (StringUtils.isNotBlank(source.getName()) && StringUtils.isNotBlank(source.getCode())) {
            target.setUrl(targetProductDataUrlResolver.resolve(source.getName(), source.getCode()));
        }
        target.setDescription(source.getDescription());

        if (source.getIsGiftCard() != null) {
            target.setGiftCard(source.getIsGiftCard().booleanValue());
        }

        if (source.getOnlineExclusive() != null) {
            target.setOnlineExclusive(source.getOnlineExclusive().booleanValue());
        }

        if (StringUtils.isNotEmpty(source.getNumOfReviews())) {
            try {
                target.setNumberOfReviews(Integer.valueOf(source.getNumOfReviews()));
            }
            catch (final NumberFormatException ne) {
                LOG.error("Number of reviews is not a number");
            }
        }
        if (StringUtils.isNotEmpty(source.getReviewAvgRating())) {
            try {
                target.setAverageRating(Double.valueOf(source.getReviewAvgRating()));
            }
            catch (final NumberFormatException ne) {
                LOG.error("Number of reviews is not a number");
            }
        }

        target.setPromotionStatuses(getPromotionStatuses(source));

        target.setPreview(BooleanUtils.toBoolean(source.getPreviewFlag()));

        //populate dealDescription  dealEndDate/deal startdate
        if (StringUtils.isNotEmpty(source.getDealDescription())) {
            try {
                final String startDateStr = source.getDealStartTime();
                final String endDateStr = source.getDealEndtime();
                if (StringUtils.isNotEmpty(startDateStr) && StringUtils.isNotEmpty(endDateStr)) {
                    TargetEndecaDealDescHelper.processDealMessage(startDateStr, endDateStr,
                            source.getDealDescription(), target);
                }

            }
            catch (final ParseException e) {
                LOG.error("Deal  StartDate/EndDate is  not a valid date");
            }
        }

        if (StringUtils.isNotEmpty(source.getMaxAvailOnlineQty())) {
            target.setMaxAvailOnlineQty(Integer.valueOf(source.getMaxAvailOnlineQty()));
        }

        if (StringUtils.isNotEmpty(source.getMaxAvailStoreQty())) {
            target.setMaxAvailStoreQty(Integer.valueOf(source.getMaxAvailStoreQty()));
        }

        target.setNewArrived(getNewArrivalStatus(source));
    }

    private List<TargetPromotionStatusEnum> getPromotionStatuses(final EndecaProduct source) {

        final List<TargetPromotionStatusEnum> promotionStatuses = new ArrayList<>();
        if (BooleanUtils.toBoolean(source.getNewLowerPriceFlag())) {
            promotionStatuses.add(TargetPromotionStatusEnum.NEW_LOWER_PRICE);
        }
        if (BooleanUtils.toBoolean(source.getOnlineExclusive())) {
            promotionStatuses.add(TargetPromotionStatusEnum.ONLINE_EXCLUSIVE);
        }
        if (BooleanUtils.toBoolean(source.getTargetExclusive())) {
            promotionStatuses.add(TargetPromotionStatusEnum.TARGET_EXCLUSIVE);
        }
        if (BooleanUtils.toBoolean(source.getClearance())) {
            promotionStatuses.add(TargetPromotionStatusEnum.CLEARANCE);
        }
        if (BooleanUtils.toBoolean(source.getHotProduct())) {
            promotionStatuses.add(TargetPromotionStatusEnum.HOT_PRODUCT);
        }
        if (BooleanUtils.toBoolean(source.getEssential())) {
            promotionStatuses.add(TargetPromotionStatusEnum.ESSENTIALS);
        }
        return promotionStatuses;
    }

    /**
     * @param source
     * @return boolean
     */
    private boolean getNewArrivalStatus(final EndecaProduct source) {
        if (StringUtils.equalsIgnoreCase(source.getTag(), TgtFacadesConstants.NEW_ARRIVALS)
                || StringUtils.contains(source.getTag(), TgtFacadesConstants.NEW_ARRIVALS)) {
            return true;
        }
        return false;
    }

    /**
     * @param targetProductDataUrlResolver
     *            the targetProductDataUrlResolver to set
     */
    @Required
    public void setTargetProductDataUrlResolver(final TargetProductDataUrlResolver targetProductDataUrlResolver) {
        this.targetProductDataUrlResolver = targetProductDataUrlResolver;
    }

}

/**
 * 
 */
package au.com.target.endeca.infront.converter.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.ParseException;
import java.util.Iterator;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.util.TargetEndecaDealDescHelper;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.url.impl.TargetProductDataUrlResolver;

import com.endeca.navigation.AggrERec;
import com.endeca.navigation.AssocDimLocations;
import com.endeca.navigation.DimLocation;
import com.endeca.navigation.PropertyMap;



/**
 * Populate basic data.
 * 
 * @author jjayawa1
 * 
 */
public class TargetProductListerBasicDataPopulatorForAggrERec<SOURCE extends AggrERec, TARGET extends TargetProductListerData>

        implements Populator<AggrERec, TargetProductListerData> {


    protected static final Logger LOG = Logger.getLogger(TargetProductListerBasicDataPopulatorForAggrERec.class);

    private TargetProductDataUrlResolver targetProductDataUrlResolver;

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final AggrERec source, final TargetProductListerData target) throws ConversionException {
        final PropertyMap representativeRecordProperties = source.getRepresentative().getProperties();

        for (final Iterator<AssocDimLocations> it = source.getRepresentative().getDimValues().iterator(); it
                .hasNext();) {
            final AssocDimLocations dimLoc = it.next();
            if (dimLoc.getDimRoot().getDimensionName()
                    .equals(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_NAME)) {
                final String name = ((DimLocation)dimLoc.get(0)).getDimValue().getName();
                target.setName(name);
                target.setBaseName(StringEscapeUtils.unescapeHtml(name));
            }
            else if (dimLoc.getDimRoot().getDimensionName()
                    .equals(EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND)) {
                target.setBrand(((DimLocation)dimLoc.get(0)).getDimValue().getName());
            }
        }
        if (representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_TOP_LEVEL_CATEGORY_NAME)) {
            target.setTopLevelCategory((String)representativeRecordProperties
                    .get((EndecaConstants.EndecaRecordSpecificFields.ENDECA_TOP_LEVEL_CATEGORY_NAME)));
        }

        if (representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE)) {
            final String code = (String)representativeRecordProperties
                    .get((EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE));
            target.setCode(code);
            target.setBaseProductCode(code);
        }

        if (StringUtils.isNotBlank(target.getName())
                && StringUtils.isNotBlank(target.getCode())) {
            target.setUrl(targetProductDataUrlResolver.resolve(target.getName(), target.getCode()));
        }

        if (representativeRecordProperties.containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DESCRIPTION)) {
            target.setDescription((String)representativeRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DESCRIPTION));
        }

        if (representativeRecordProperties.containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_ORIG_CATEGORY)) {
            target.setOriginalCategoryCode((String)representativeRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_ORIG_CATEGORY));
        }


        if (representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.TOTAL_INTEREST_ON_PRD)) {
            target.setTotalInterest((String)representativeRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.TOTAL_INTEREST_ON_PRD));
        }

        if (representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_NUMBER_OF_REVIEWS)) {
            target.setNumberOfReviews(Integer.valueOf((String)representativeRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_NUMBER_OF_REVIEWS)));
        }

        if (representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_REVIEW_AVG_RATING)) {
            target.setAverageRating(Double.valueOf((String)representativeRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_REVIEW_AVG_RATING)));
        }

        if (representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_ONLINE_EXCLUSIVE)) {
            target.setOnlineExclusive(
                    Boolean.getBoolean((String)representativeRecordProperties
                            .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_ONLINE_EXCLUSIVE)));
        }

        if (representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_IS_GIFT_CARD)) {
            final String isGiftCard = (String)representativeRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_IS_GIFT_CARD);
            if (StringUtils.equals(EndecaConstants.ENDECA_BOOLEAN_VALUE, isGiftCard)) {
                target.setGiftCard(Boolean.TRUE.booleanValue());
            }
            else {
                target.setGiftCard(Boolean.FALSE.booleanValue());
            }
        }

        if (representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PREVIEW_FLAG)) {
            final String previewFlag = (String)representativeRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PREVIEW_FLAG);
            if (StringUtils.equals(EndecaConstants.ENDECA_BOOLEAN_VALUE, previewFlag)) {
                target.setPreview(Boolean.TRUE.booleanValue());
            }
            else {
                target.setPreview(Boolean.FALSE.booleanValue());
            }
        }

        if ((representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_DESCRIPTION))) {
            try {
                String endDateStr = StringUtils.EMPTY;
                String startDateStr = StringUtils.EMPTY;
                String dealDesc = StringUtils.EMPTY;
                if (null != representativeRecordProperties
                        .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_ENDTIME)) {
                    endDateStr = (String)representativeRecordProperties
                            .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_ENDTIME);
                }
                if (null != representativeRecordProperties
                        .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_STARTTIME)) {
                    startDateStr = (String)representativeRecordProperties
                            .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_STARTTIME);
                }
                if (null != representativeRecordProperties
                        .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_DESCRIPTION)) {
                    dealDesc = (String)representativeRecordProperties
                            .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_DESCRIPTION);
                }
                TargetEndecaDealDescHelper.processDealMessage(startDateStr, endDateStr, dealDesc, target);
            }
            catch (final ParseException e) {
                LOG.error("Deal  StartDate/EndDate is  not a valid date");
            }
        }
        populateStockDetails(source, target);
    }

    /**
     * Method to populate the stock details.
     * 
     * @param source
     * @param target
     */
    private void populateStockDetails(final AggrERec source, final TargetProductListerData target) {
        final PropertyMap aggregateRecordProperties = source.getProperties();
        if (aggregateRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_AVAIL_ONLINE_QTY)) {
            final String maxAvailOnlineQty = (String)aggregateRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_AVAIL_ONLINE_QTY);
            target.setMaxAvailOnlineQty(StringUtils.isNotBlank(maxAvailOnlineQty) ? Integer.valueOf(maxAvailOnlineQty)
                    : Integer.valueOf(0));

        }
        if (aggregateRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_ACTUAL_CONSOLIDATED_STORE_STOCK)) {
            final String maxActualConsolidatedStoreStock = (String)aggregateRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_ACTUAL_CONSOLIDATED_STORE_STOCK);
            target.setMaxActualConsolidatedStoreStock(StringUtils.isNotBlank(maxActualConsolidatedStoreStock) ? Integer
                    .valueOf(maxActualConsolidatedStoreStock)
                    : Integer.valueOf(0));

        }
        if (aggregateRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_AVAIL_STORE_QTY)) {
            final String maxAvailStoreQty = (String)aggregateRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_AVAIL_STORE_QTY);
            target.setMaxAvailStoreQty(
                    StringUtils.isNotBlank(maxAvailStoreQty) ? Integer.valueOf(maxAvailStoreQty) : Integer.valueOf(0));
        }
    }

    /**
     * @param targetProductDataUrlResolver
     *            the targetProductDataUrlResolver to set
     */
    @Required
    public void setTargetProductDataUrlResolver(final TargetProductDataUrlResolver targetProductDataUrlResolver) {
        this.targetProductDataUrlResolver = targetProductDataUrlResolver;
    }
}

/**
 * 
 */
package au.com.target.endeca.infront.converter.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaProduct;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtfacades.delivery.data.DeliveryPromotionalData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;



/**
 * @author Pradeep
 *
 */
public class TargetProductListerDataDeliveryPromoPopulator<SOURCE extends EndecaProduct, TARGET extends TargetProductListerData>
        implements Populator<EndecaProduct, TargetProductListerData> {

    private static final int SALE_APPLICATION_INDEX = 6;
    private static final int EXPECTED_SIZE = 7;
    private static final int STICKER_MESSAGE_INDEX = 5;
    private static final int RESTRICTION_PRIORITY_INDEX = 4;
    private static final int DELIVERY_MODE_INDEX = 1;
    private static final int DISPLAY_ORDER_INDEX = 0;
    private static final int END_DATE_INDEX = 3;
    private static final int START_DATE_INDEX = 2;
    private static final Logger LOG = Logger.getLogger(TargetProductListerDataDeliveryPromoPopulator.class);


    private TargetSalesApplicationService targetSalesApplicationService;



    /* (non-Javadoc)
     * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final EndecaProduct source, final TargetProductListerData target) throws ConversionException {

        if (source.getApplicableDeliveryPromotions() != null) {

            final List<DeliveryPromotionalData> deliveryPromotionDataList = new ArrayList<>();
            for (final String eachApplicableDeliveryPromotion : source.getApplicableDeliveryPromotions()) {
                /**
                 * DeliveryPromotion expected format from Endeca Example :3@@home-delivery@@May 26 2015 12:00AM@@May 29
                 * 2015 12:00AM@@1000_Free click and collect@@: Format- displayOrder@@delivery mode@@start
                 * date@@Enddate@@priority@@sticker message@@Sale
                 */
                final String[] rawDeliveryPromotionalData = eachApplicableDeliveryPromotion
                        .split(EndecaConstants.ENDECA_CONCATENATE_SYMBOL, -1);
                Date startDate = null;
                Date endDate = null;
                if (isValid(rawDeliveryPromotionalData)) {
                    try {
                        startDate = parseDate(rawDeliveryPromotionalData[START_DATE_INDEX]);
                        endDate = parseDate(rawDeliveryPromotionalData[END_DATE_INDEX]);
                        final Date currentDate = new Date();
                        //Lets not populate outdated or future date Delivery Promotion
                        if (!((ObjectUtils.compare(currentDate, startDate, false) == -1 || ObjectUtils
                                .compare(endDate, currentDate, true) == -1))) {

                            final DeliveryPromotionalData deliveryPromotionalData = new DeliveryPromotionalData();
                            deliveryPromotionalData
                                    .setDeliveryPromoDisplayOrder(Integer
                                            .parseInt(rawDeliveryPromotionalData[DISPLAY_ORDER_INDEX]));
                            deliveryPromotionalData.setDeliveryMode(rawDeliveryPromotionalData[DELIVERY_MODE_INDEX]);

                            deliveryPromotionalData.setRestrictionStartDate(startDate);
                            deliveryPromotionalData.setRestrictionEndDate(endDate);
                            deliveryPromotionalData.setrZDMVPriority(Integer
                                    .parseInt(rawDeliveryPromotionalData[RESTRICTION_PRIORITY_INDEX]));
                            deliveryPromotionalData
                                    .setStickerMessage(rawDeliveryPromotionalData[STICKER_MESSAGE_INDEX]);
                            deliveryPromotionDataList.add(deliveryPromotionalData);
                        }
                    }
                    catch (final ParseException exception) {
                        LOG.warn("Error while Parsing deliveryPromotion start/End date", exception);
                    }
                }

            }
            if (CollectionUtils.isNotEmpty(deliveryPromotionDataList))
            {
                sort(deliveryPromotionDataList);
                target.setProductPromotionalDeliverySticker(getApplicableDeliveryPromotionalMessage(deliveryPromotionDataList));
            }
        }

    }


    /**
     * @param sortedDeliveryPromotionDataList
     * @return eachDeliveryPromotionalData
     */
    private String getApplicableDeliveryPromotionalMessage(
            final List<DeliveryPromotionalData> sortedDeliveryPromotionDataList) {
        int displayOrder = 0;
        for (final DeliveryPromotionalData eachDeliveryPromotionalData : sortedDeliveryPromotionDataList) {
            //To fetch next display rank 
            if (displayOrder != eachDeliveryPromotionalData.getDeliveryPromoDisplayOrder()) {
                displayOrder = eachDeliveryPromotionalData.getDeliveryPromoDisplayOrder();
                if (StringUtils.isNotBlank((eachDeliveryPromotionalData.getStickerMessage()))) {
                    return eachDeliveryPromotionalData.getStickerMessage();
                }
            }
        }
        return null;
    }



    /**
     * @param rawDeliveryPromotionalData
     * @return basic Validation Details
     */
    private boolean isValid(final String[] rawDeliveryPromotionalData) {
        final String salesApplicationAllowed = rawDeliveryPromotionalData[6];
        return (rawDeliveryPromotionalData.length == EXPECTED_SIZE
        && (StringUtils.isBlank(rawDeliveryPromotionalData[SALE_APPLICATION_INDEX]) || StringUtils.equals(
                salesApplicationAllowed,
                ObjectUtils.toString(targetSalesApplicationService.getCurrentSalesApplication()))));
    }

    /**
     * @param promotionalDate
     * @return parsed Date
     * @throws ParseException
     */
    private Date parseDate(final String promotionalDate) throws ParseException {
        if (StringUtils.isNotBlank(promotionalDate)) {
            return getParsedDate(promotionalDate);
        }
        return null;
    }


    /**
     * @param deliveryPromotionDataList
     */
    private void sort(final List<DeliveryPromotionalData> deliveryPromotionDataList) {
        if (deliveryPromotionDataList.size() > 1)
        {
            Collections.sort(deliveryPromotionDataList, new Comparator<DeliveryPromotionalData>() {

                @Override
                public int compare(final DeliveryPromotionalData firstElement,
                        final DeliveryPromotionalData secondElement) {

                    final int firstPromoDisplayOrder = firstElement.getDeliveryPromoDisplayOrder();
                    final int secondPromoDisplayOrder = secondElement.getDeliveryPromoDisplayOrder();
                    if (firstPromoDisplayOrder == secondPromoDisplayOrder) {
                        return secondElement.getrZDMVPriority() - firstElement.getrZDMVPriority();
                    }
                    return firstPromoDisplayOrder - secondPromoDisplayOrder;

                }
            });
        }
    }


    /**
     * @param date
     * @return Date parsed from MSsql date format
     * @throws ParseException
     */
    private Date getParsedDate(final String date) throws ParseException {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(EndecaConstants.ENDECA_DATE_FORMAT);
        return dateFormat.parse(date);
    }


    /**
     * @param targetSalesApplicationService
     *            the targetSalesApplicationService to set
     */
    @Required
    public void setTargetSalesApplicationService(final TargetSalesApplicationService targetSalesApplicationService) {
        this.targetSalesApplicationService = targetSalesApplicationService;
    }




}

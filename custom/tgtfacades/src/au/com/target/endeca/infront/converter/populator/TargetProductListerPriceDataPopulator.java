/**
 * 
 */
package au.com.target.endeca.infront.converter.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.data.EndecaProduct;
import au.com.target.tgtcore.price.data.PriceRangeInformation;
import au.com.target.tgtfacades.converters.populator.TargetProductPricePopulator;
import au.com.target.tgtfacades.product.data.TargetProductListerData;


/**
 * @author pthoma20
 * 
 */
public class TargetProductListerPriceDataPopulator<SOURCE extends EndecaProduct, TARGET extends TargetProductListerData>
        implements Populator<EndecaProduct, TargetProductListerData> {

    private TargetProductPricePopulator targetProductPricePopulator;

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final EndecaProduct source, final TargetProductListerData target) throws ConversionException {
        Double minPrice = null;
        if (StringUtils.isNotEmpty(source.getMinPrice())) {
            minPrice = new Double(source.getMinPrice());
        }

        Double maxPrice = null;
        if (StringUtils.isNotEmpty(source.getMaxPrice())) {
            maxPrice = new Double(source.getMaxPrice());
        }

        Double minWasPrice = null;
        if (StringUtils.isNotEmpty(source.getMinWasPrice())) {
            minWasPrice = new Double(source.getMinWasPrice());
        }

        Double maxWasPrice = null;
        if (StringUtils.isNotEmpty(source.getMaxWasPrice())) {
            maxWasPrice = new Double(source.getMaxWasPrice());
        }

        final PriceRangeInformation priceRangeInformation = new PriceRangeInformation(minPrice,
                maxPrice, minWasPrice, maxWasPrice);
        targetProductPricePopulator.populatePricesFromRangeInformation(target, priceRangeInformation);

    }

    /**
     * @param targetProductPricePopulator
     *            the targetProductPricePopulator to set
     */
    @Required
    public void setTargetProductPricePopulator(final TargetProductPricePopulator targetProductPricePopulator) {
        this.targetProductPricePopulator = targetProductPricePopulator;
    }


}

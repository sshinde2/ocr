/**
 * 
 */
package au.com.target.endeca.infront.converter.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.tgtcore.price.data.PriceRangeInformation;
import au.com.target.tgtfacades.converters.populator.TargetProductPricePopulator;
import au.com.target.tgtfacades.product.data.TargetProductListerData;

import com.endeca.navigation.AggrERec;
import com.endeca.navigation.PropertyMap;


/**
 * Populate price data.
 * 
 * @author jjayawa1
 * 
 */
public class TargetProductListerPriceDataPopulatorForAggrERec<SOURCE extends AggrERec, TARGET extends TargetProductListerData>
        implements Populator<AggrERec, TargetProductListerData> {

    private TargetProductPricePopulator targetProductPricePopulator;

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final AggrERec source, final TargetProductListerData target) throws ConversionException {
        final PropertyMap aggregateRecordProperties = source.getProperties();

        Double minPrice = null;
        if (aggregateRecordProperties.containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MIN_PRICE)) {
            final String minPriceString = (String)aggregateRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MIN_PRICE);
            if (StringUtils.isNotEmpty(minPriceString)) {
                minPrice = new Double(minPriceString);
            }
        }

        Double maxPrice = null;
        if (aggregateRecordProperties.containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_PRICE)) {
            final String maxPriceString = (String)aggregateRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_PRICE);
            if (StringUtils.isNotEmpty(maxPriceString)) {
                maxPrice = new Double(maxPriceString);
            }
        }

        Double minWasPrice = null;
        if (aggregateRecordProperties.containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MIN_WAS_PRICE)) {
            final String minWasPriceString = (String)aggregateRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MIN_WAS_PRICE);
            if (StringUtils.isNotEmpty(minWasPriceString)) {
                minWasPrice = new Double(minWasPriceString);
            }
        }

        Double maxWasPrice = null;
        if (aggregateRecordProperties.containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_WAS_PRICE)) {
            final String maxWasPriceString = (String)aggregateRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_WAS_PRICE);
            if (StringUtils.isNotEmpty(maxWasPriceString)) {
                maxWasPrice = new Double(maxWasPriceString);
            }
        }

        final PriceRangeInformation priceRangeInformation = new PriceRangeInformation(minPrice,
                maxPrice, minWasPrice, maxWasPrice);
        targetProductPricePopulator.populatePricesFromRangeInformation(target, priceRangeInformation);

    }

    /**
     * @param targetProductPricePopulator
     *            the targetProductPricePopulator to set
     */
    @Required
    public void setTargetProductPricePopulator(final TargetProductPricePopulator targetProductPricePopulator) {
        this.targetProductPricePopulator = targetProductPricePopulator;
    }


}

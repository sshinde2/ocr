/**
 * 
 */
package au.com.target.endeca.infront.data;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;


/**
 * @author rsamuel3
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class EndecaBreadCrumb {
    @XmlElement
    private String element;

    @XmlElement
    private String dimensionType;

    @XmlElement
    private String count;

    @XmlElement
    private String code;

    @XmlElement
    private String removeNavUrl;

    @XmlElement
    private boolean multiSelect;

    @XmlElement
    @XmlElementWrapper(name = "ancestors")
    private List<EndecaAncestor> ancestor;

    /**
     * @return the element
     */
    public String getElement() {
        return element;
    }

    /**
     * @param element
     *            the element to set
     */
    public void setElement(final String element) {
        this.element = element;
    }

    /**
     * @return the ancestor
     */
    public List<EndecaAncestor> getAncestor() {
        return ancestor;
    }

    /**
     * @param ancestor
     *            the ancestor to set
     */
    public void setAncestor(final List<EndecaAncestor> ancestor) {
        this.ancestor = ancestor;
    }

    /**
     * @return the removeNavUrl
     */
    public String getRemoveNavUrl() {
        return removeNavUrl;
    }

    /**
     * @param removeNavUrl
     *            the removeNavUrl to set
     */
    public void setRemoveNavUrl(final String removeNavUrl) {
        this.removeNavUrl = removeNavUrl;
    }

    /**
     * @return the count
     */
    public String getCount() {
        return count;
    }

    /**
     * @param count
     *            the count to set
     */
    public void setCount(final String count) {
        this.count = count;
    }

    /**
     * @return the dimensionType
     */
    public String getDimensionType() {
        return dimensionType;
    }

    /**
     * @param dimensionType
     *            the dimensionType to set
     */
    public void setDimensionType(final String dimensionType) {
        this.dimensionType = dimensionType;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the multiSelect
     */
    public boolean isMultiSelect() {
        return multiSelect;
    }

    /**
     * @param multiSelect
     *            the multiSelect to set
     */
    public void setMultiSelect(final boolean multiSelect) {
        this.multiSelect = multiSelect;
    }
}

/**
 * 
 */
package au.com.target.endeca.infront.data;

import java.util.Map;


/**
 * @author smudumba
 * 
 */
public class EndecaDimensions {

    private Map<String, Map<String, String>> dimensions;

    /**
     * @return the dimensions
     */
    public Map<String, Map<String, String>> getDimensions() {

        return dimensions;
    }

    /**
     * @param dimensions
     *            the dimensions to set
     */
    public void setDimensions(final Map<String, Map<String, String>> dimensions) {
        this.dimensions = dimensions;
    }

}

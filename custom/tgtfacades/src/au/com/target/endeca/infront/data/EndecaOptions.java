/**
 * 
 */
package au.com.target.endeca.infront.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author rsamuel3
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class EndecaOptions {

    @XmlElement
    private String name;

    @XmlElement
    private boolean selected;

    @XmlElement
    private String navigationState;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * @param selected
     *            the selected to set
     */
    public void setSelected(final boolean selected) {
        this.selected = selected;
    }

    /**
     * @return the navigationState
     */
    public String getNavigationState() {
        return navigationState;
    }

    /**
     * @param navigationState
     *            the navigationState to set
     */
    public void setNavigationState(final String navigationState) {
        this.navigationState = navigationState;
    }

}

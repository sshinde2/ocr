/**
 * 
 */
package au.com.target.endeca.infront.data;

import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.endeca.infront.assembler.ContentItem;


/**
 * @author rsamuel3
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class EndecaSearch {
    @XmlElement
    @XmlElementWrapper(name = "breadCrumbs")
    private List<EndecaBreadCrumb> breadCrumb;

    @XmlElement
    @XmlElementWrapper(name = "dimensions")
    private List<EndecaDimension> dimension;

    @XmlElement
    private EndecaRecords records;

    private List<String> spellingSuggestions;

    private String autoCorrectedTerm;

    @XmlElement
    private String redirectLink;

    private LinkedList<ContentItem> recommendations;

    /**
     * @return the records
     */
    public EndecaRecords getRecords() {
        return records;
    }

    /**
     * @param records
     *            the records to set
     */
    public void setRecords(final EndecaRecords records) {
        this.records = records;
    }

    /**
     * @return the breadCrumb
     */
    public List<EndecaBreadCrumb> getBreadCrumb() {
        return breadCrumb;
    }

    /**
     * @param breadCrumb
     *            the breadCrumb to set
     */
    public void setBreadCrumb(final List<EndecaBreadCrumb> breadCrumb) {
        this.breadCrumb = breadCrumb;
    }

    /**
     * @return the dimension
     */
    public List<EndecaDimension> getDimension() {
        return dimension;
    }

    /**
     * @param dimension
     *            the dimension to set
     */
    public void setDimension(final List<EndecaDimension> dimension) {
        this.dimension = dimension;
    }

    /**
     * @return the spellingSuggestions
     */
    public List<String> getSpellingSuggestions() {
        return spellingSuggestions;
    }

    /**
     * @param spellingSuggestions
     *            the spellingSuggestions to set
     */
    public void setSpellingSuggestions(final List<String> spellingSuggestions) {
        this.spellingSuggestions = spellingSuggestions;
    }

    /**
     * @return the autoCorrectedTerm
     */
    public String getAutoCorrectedTerm() {
        return autoCorrectedTerm;
    }

    /**
     * @param autoCorrectedTerm
     *            the autoCorrectedTerm to set
     */
    public void setAutoCorrectedTerm(final String autoCorrectedTerm) {
        this.autoCorrectedTerm = autoCorrectedTerm;
    }

    /**
     * @return the redirectLink
     */
    public String getRedirectLink() {
        return redirectLink;
    }

    /**
     * @param redirectLink
     *            the redirectLink to set
     */
    public void setRedirectLink(final String redirectLink) {
        this.redirectLink = redirectLink;
    }

    /**
     * @return the recommendations
     */
    public LinkedList<ContentItem> getRecommendations() {
        return recommendations;
    }

    /**
     * @param recommendations
     *            the recommendations to set
     */
    public void setRecommendations(final LinkedList<ContentItem> recommendations) {
        this.recommendations = recommendations;
    }

}

/**
 * 
 */
package au.com.target.endeca.infront.data;

import java.util.List;
import java.util.Map;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.tgtfacades.category.data.TargetCategoryData;
import au.com.target.tgtfacades.product.data.BrandData;



/**
 * @author Paul
 * 
 */
public class EndecaSearchStateData {


    private List<String> navigationState;

    /**
     * Key is the sort field and Value is order.
     * 
     * If ascending put the value as 0 and if descending the value should be 1.
     **/
    private Map<String, Integer> sortStateMap;

    /**
     * If no Items Per Page is passed, then the default value configured in Assembler-context.xml for Results List will
     * be taken.
     */
    private int itemsPerPage;

    /**
     * By default the pageNumber is considered as 0. This will be only considered only if the items per page is passed.
     * So it is mandatory to pass the items per page in the case when it it needs to traverse to next page.
     */
    private int pageNumber;

    private List<String> searchTerm;

    /**
     * Match mode is for relevancy of the search which can be set as matchany, matchall, matchpartial
     */
    private String matchMode;

    private String searchField;

    private String url;

    private BrandData brand;

    private String viewAs;

    private List<String> categoryCodes;

    private List<String> productCodes;

    private String brandCode;

    private boolean skipRedirect;

    private String storeNumber;

    private TargetCategoryData categoryData;

    private String requestUrl;

    private String wsUrl;

    private String currentQueryParam;

    /**
     * Set the list of option for building the recordfilter
     * Ex:EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE
     */
    private List<String> recordFilterOptions;

    /**
     * Set to NP1 by default
     */
    private String npSearchState = EndecaConstants.ENDECA_NP_1;

    private String nuRollupField = EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE;

    /**
     * When set to true skip adding the sort field items to the Endeca Query
     */
    private boolean skipDefaultSort;

    /**
     * When set to true skip adding the record filter for instock
     */
    private boolean skipInStockFilter;

    /**
     * Field List Configuration to be read project.properties This is a mandatory field and needs to be passed in
     */
    private String fieldListConfig;

    private Map<String, String> recordFilters;

    private String sizeGroup;

    /**
     * When set to false will skip persisting the applied availableOnline facet
     */
    private boolean persistAvailableOnlineFacet = true;

    /**
     * @return the sizeGroup
     */
    public String getSizeGroup() {
        return sizeGroup;
    }

    /**
     * @param sizeGroup
     *            the sizeGroup to set
     */
    public void setSizeGroup(final String sizeGroup) {
        this.sizeGroup = sizeGroup;
    }

    /**
     * @return the navigationState
     */
    public List<String> getNavigationState() {
        return navigationState;
    }

    /**
     * @param navigationState
     *            the navigationState to set
     */
    public void setNavigationState(final List<String> navigationState) {
        this.navigationState = navigationState;
    }

    /**
     * @return the searchTerm
     */
    public List<String> getSearchTerm() {
        return searchTerm;
    }

    /**
     * @param searchTerm
     *            the searchTerm to set
     */
    public void setSearchTerm(final List<String> searchTerm) {
        this.searchTerm = searchTerm;
    }

    /**
     * @return the matchMode
     */
    public String getMatchMode() {
        return matchMode;
    }

    /**
     * @param matchMode
     *            the matchMode to set
     */
    public void setMatchMode(final String matchMode) {
        this.matchMode = matchMode;
    }

    /**
     * @return the sortStateMap
     */
    public Map<String, Integer> getSortStateMap() {
        return sortStateMap;
    }

    /**
     * @param sortStateMap
     *            the sortStateMap to set
     */
    public void setSortStateMap(final Map<String, Integer> sortStateMap) {
        this.sortStateMap = sortStateMap;
    }

    /**
     * @param searchField
     *            the searchField to set
     */
    public void setSearchField(final String searchField) {
        this.searchField = searchField;
    }

    /**
     * @return the searchField
     */
    public String getSearchField() {
        return searchField;
    }

    /**
     * @return the itemsPerPage
     */
    public int getItemsPerPage() {
        return itemsPerPage;
    }

    /**
     * @param itemsPerPage
     *            the itemsPerPage to set
     */
    public void setItemsPerPage(final int itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    /**
     * @return the pageNumber
     */
    public int getPageNumber() {
        return pageNumber;
    }

    /**
     * @param pageNumber
     *            the pageNumber to set
     */
    public void setPageNumber(final int pageNumber) {
        this.pageNumber = pageNumber;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     *            the url to set
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    /**
     * @return the brand
     */
    public BrandData getBrand() {
        return brand;
    }

    /**
     * @param brand
     *            the brand to set
     */
    public void setBrand(final BrandData brand) {
        this.brand = brand;
    }

    /**
     * @return the viewAs
     */
    public String getViewAs() {
        return viewAs;
    }

    /**
     * @param viewAs
     *            the viewAs to set
     */
    public void setViewAs(final String viewAs) {
        this.viewAs = viewAs;
    }

    /**
     * @return the categoryCodes
     */
    public List<String> getCategoryCodes() {
        return categoryCodes;
    }

    /**
     * @param categoryCodes
     *            the categoryCodes to set
     */
    public void setCategoryCodes(final List<String> categoryCodes) {
        this.categoryCodes = categoryCodes;
    }

    /**
     * @return the productCodes
     */
    public List<String> getProductCodes() {
        return productCodes;
    }

    /**
     * @param productCodes
     *            the productCodes to set
     */
    public void setProductCodes(final List<String> productCodes) {
        this.productCodes = productCodes;
    }

    /**
     * @return the skipDefaultSort
     */
    public boolean isSkipDefaultSort() {
        return skipDefaultSort;
    }

    /**
     * @param skipDefaultSort
     *            the skipDefaultSort to set
     */
    public void setSkipDefaultSort(final boolean skipDefaultSort) {
        this.skipDefaultSort = skipDefaultSort;
    }

    /**
     * @return the skipInStockFilter
     */
    public boolean isSkipInStockFilter() {
        return skipInStockFilter;
    }

    /**
     * @return the brandCode
     */
    public String getBrandCode() {
        return brandCode;
    }

    /**
     * @param brandCode
     *            the brandCode to set
     */
    public void setBrandCode(final String brandCode) {
        this.brandCode = brandCode;
    }

    /**
     * @param skipInStockFilter
     *            the skipInStockFilter to set
     */
    public void setSkipInStockFilter(final boolean skipInStockFilter) {
        this.skipInStockFilter = skipInStockFilter;
    }

    /**
     * @return the skipRedirect
     */
    public boolean isSkipRedirect() {
        return skipRedirect;
    }

    /**
     * @param skipRedirect
     *            the skipRedirect to set
     */
    public void setSkipRedirect(final boolean skipRedirect) {
        this.skipRedirect = skipRedirect;
    }

    /**
     * @return the npSearchState
     */
    public String getNpSearchState() {
        return npSearchState;
    }

    /**
     * @param npSearchState
     *            the npSearchState to set
     */
    public void setNpSearchState(final String npSearchState) {
        this.npSearchState = npSearchState;
    }

    /**
     * @return the fieldListConfig
     */
    public String getFieldListConfig() {
        return fieldListConfig;
    }

    /**
     * @param fieldListConfig
     *            the fieldListConfig to set
     */
    public void setFieldListConfig(final String fieldListConfig) {
        this.fieldListConfig = fieldListConfig;
    }

    /**
     * @return the recordFilterOptions
     */
    public List<String> getRecordFilterOptions() {
        return recordFilterOptions;
    }

    /**
     * @param recordFilterOptions
     *            the recordFilterOptions to set
     */
    public void setRecordFilterOptions(final List<String> recordFilterOptions) {
        this.recordFilterOptions = recordFilterOptions;
    }

    public String getNuRollupField() {
        return nuRollupField;
    }

    public void setNuRollupField(final String nuRollupField) {
        this.nuRollupField = nuRollupField;
    }

    /**
     * @return the storeNumber
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the categoryData
     */
    public TargetCategoryData getCategoryData() {
        return categoryData;
    }

    /**
     * @param categoryData
     *            the categoryData to set
     */
    public void setCategoryData(final TargetCategoryData categoryData) {
        this.categoryData = categoryData;
    }

    /**
     * @return the requestUrl
     */
    public String getRequestUrl() {
        return requestUrl;
    }

    /**
     * @param requestUrl
     *            the requestUrl to set
     */
    public void setRequestUrl(final String requestUrl) {
        this.requestUrl = requestUrl;
    }

    /**
     * @return the wsUrl
     */
    public String getWsUrl() {
        return wsUrl;
    }

    /**
     * @param wsUrl
     *            the wsUrl to set
     */
    public void setWsUrl(final String wsUrl) {
        this.wsUrl = wsUrl;
    }

    /**
     * @return the currentQueryParam
     */
    public String getCurrentQueryParam() {
        return currentQueryParam;
    }

    /**
     * @param currentQueryParam
     *            the currentQueryParam to set
     */
    public void setCurrentQueryParam(final String currentQueryParam) {
        this.currentQueryParam = currentQueryParam;
    }

    /**
     * @return the recordFilters
     */
    public Map<String, String> getRecordFilters() {
        return recordFilters;
    }

    /**
     * @param recordFilters
     *            the recordFilters to set
     */
    public void setRecordFilters(final Map<String, String> recordFilters) {
        this.recordFilters = recordFilters;
    }

    /**
     * @return the persistAvailableOnlineFacet
     */
    public boolean isPersistAvailableOnlineFacet() {
        return persistAvailableOnlineFacet;
    }

    /**
     * @param persistAvailableOnlineFacet
     *            the persistAvailableOnlineFacet to set
     */
    public void setPersistAvailableOnlineFacet(final boolean persistAvailableOnlineFacet) {
        this.persistAvailableOnlineFacet = persistAvailableOnlineFacet;
    }


}

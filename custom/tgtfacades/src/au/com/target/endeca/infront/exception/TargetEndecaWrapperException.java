/**
 * 
 */
package au.com.target.endeca.infront.exception;

/**
 * Class for Endeca integration exceptions
 * 
 * @author pthoma20 *
 */
public class TargetEndecaWrapperException extends Exception {

    public TargetEndecaWrapperException() {
        super();
    }

    /**
     * @param message
     * @param cause
     */
    public TargetEndecaWrapperException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     */
    public TargetEndecaWrapperException(final String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public TargetEndecaWrapperException(final Throwable cause) {
        super(cause);
    }

}

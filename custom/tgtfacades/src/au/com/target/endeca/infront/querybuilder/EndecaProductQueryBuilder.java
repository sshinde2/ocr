package au.com.target.endeca.infront.querybuilder;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.UrlENEQuery;
import com.endeca.navigation.UrlENEQueryParseException;

import au.com.target.endeca.infront.cache.EndecaDimensionCacheService;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * Builds the Endeca query and fetch the results
 */
public class EndecaProductQueryBuilder extends AbstractEndecaQueryBuilder {


    protected static final Logger LOG = Logger.getLogger(EndecaProductQueryBuilder.class);

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Resource(name = "endecaDimensionCacheService")
    private EndecaDimensionCacheService endecaDimensionCacheService;

    /**
     * Connect to Endeca and get results back.
     * 
     * @param searchStateData
     *            DTO to hold search parameters
     * @return ENEQueryResults
     */
    protected UrlENEQuery buildProductQuery(final EndecaSearchStateData searchStateData,
            final List<ProductModel> productModels,
            final int maxSize) {
        final EndecaQueryBuilder builder = new EndecaQueryBuilder();

        UrlENEQuery query = null;
        try {

            if (CollectionUtils.isNotEmpty(searchStateData.getCategoryCodes())
                    || StringUtils.isNotBlank(searchStateData.getBrandCode())
                    || CollectionUtils.isNotEmpty(searchStateData.getSearchTerm())) {
                query = new UrlENEQuery("", EndecaConstants.CODE_LITERAL);
                query.setNr(builder.buildProductRecordQuery(searchStateData));
                query.setNs(builder.buildSortQuery(searchStateData));
                //For multicategory search support setting the N value to 999
                query.setN(EndecaConstants.ENDECA_N_VALUE);
                //skip the max number of products for recentlyviewed component
                query.setNavERecsPerAggrERec(Integer.parseInt(searchStateData.getNpSearchState()));
                if (containsMultipleVariantsWithSameBaseProduct(productModels)) {
                    query.setNavERecsPerAggrERec(Integer.parseInt(EndecaConstants.ENDECA_NP_2));
                }
                query.setNavNumAggrERecs(maxSize
                        + (CollectionUtils.isNotEmpty(productModels) ? productModels.size() : 0));
                query.setNavMerchRuleFilter("endeca.internal.nonexistent");
                query.setSelection(getFieldList(searchStateData.getFieldListConfig()));
                query.setNu(searchStateData.getNuRollupField());

            }
        }
        catch (final UrlENEQueryParseException e) {
            LOG.error("Exception when parsing endeca query", e);
        }

        return query;
    }

    /**
     * 
     * @param searchStateData
     * @param productModels
     * @return queryResults
     */
    public ENEQueryResults getQueryResults(final EndecaSearchStateData searchStateData,
            final List<ProductModel> productModels,
            final int maxSize) {
        final UrlENEQuery query = buildProductQuery(searchStateData, productModels, maxSize);
        ENEQueryResults queryResults = null;
        try {
            queryResults = fetchEndecaQueryResults(query);
        }
        catch (final TargetEndecaException | ENEQueryException e) {
            LOG.error("Exception when querying endeca", e);

        }
        return queryResults;
    }


    /**
     * Check whether there are multiple variants of same base products.
     * 
     * @param productModels
     *            list of colour variants
     * @return boolean
     */
    private boolean containsMultipleVariantsWithSameBaseProduct(final List<ProductModel> productModels) {
        final List<String> baseProducts = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(productModels)) {
            for (final ProductModel productModel : productModels) {
                if (productModel instanceof TargetColourVariantProductModel) {
                    final TargetColourVariantProductModel colourVariantModel = (TargetColourVariantProductModel)productModel;
                    if (baseProducts.contains(colourVariantModel.getBaseProduct().getCode())) {
                        return true;
                    }
                    baseProducts.add(colourVariantModel.getBaseProduct().getCode());
                }
                else if (productModel instanceof TargetSizeVariantProductModel) {
                    final TargetSizeVariantProductModel sizeVariantModel = (TargetSizeVariantProductModel)productModel;
                    final TargetColourVariantProductModel colourVariantModel = (TargetColourVariantProductModel)sizeVariantModel
                            .getBaseProduct();
                    if (baseProducts.contains(colourVariantModel.getBaseProduct().getCode())) {
                        return true;
                    }
                    baseProducts.add(colourVariantModel.getBaseProduct().getCode());
                }
            }
        }
        return false;
    }

    /**
     * @param searchStateData
     * @param baseProduct
     * @return ENEQueryResults
     */
    public ENEQueryResults getYouMayAlsoLikeQueryResults(final EndecaSearchStateData searchStateData,
            final TargetProductModel baseProduct) {
        ENEQueryResults queryResults = null;
        try {
            final UrlENEQuery query = buildYouMayAlsoLikeQuery(searchStateData, baseProduct);
            queryResults = fetchEndecaQueryResults(query);
        }
        catch (final TargetEndecaException | ENEQueryException e) {
            LOG.error("getYouMayAlsoLikeQueryResults::Exception parsing endeca query:: ", e);

        }
        return queryResults;
    }

    /**
     * @param searchStateData
     * @param baseProduct
     * @return UrlENEQuery
     */
    protected UrlENEQuery buildYouMayAlsoLikeQuery(final EndecaSearchStateData searchStateData,
            final TargetProductModel baseProduct) {
        final EndecaQueryBuilder builder = new EndecaQueryBuilder();
        UrlENEQuery query = null;
        try {
            query = new UrlENEQuery("", EndecaConstants.CODE_LITERAL);

            query.setNs(builder.buildYouMayAlsoLikeSortQuery(searchStateData));
            query.setN(
                    endecaDimensionCacheService.getDimension(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY,
                            baseProduct.getOriginalCategory().getCode()));
            query.setNavERecsPerAggrERec(Integer.parseInt(EndecaConstants.ENDECA_NP_2));
            query.setNavNumAggrERecs(searchStateData.getItemsPerPage());
            query.setNavMerchRuleFilter("endeca.internal.nonexistent");
            query.setSelection(getFieldList(searchStateData.getFieldListConfig()));
            query.setNu(searchStateData.getNuRollupField());
            query.setNr(builder.buildRecordFilterInStockProduct());
        }
        catch (final TargetEndecaException | ENEQueryException e) {
            LOG.error("buildYouMayAlsoLikeQuery::Exception parsing endeca query:: ", e);
        }
        return query;
    }

}

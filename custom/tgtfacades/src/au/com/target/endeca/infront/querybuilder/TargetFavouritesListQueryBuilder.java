/**
 * 
 */
package au.com.target.endeca.infront.querybuilder;

import java.util.List;

import org.springframework.util.CollectionUtils;

import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.UrlENEQuery;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaException;


/**
 * @author mjanarth
 *
 *         This class is responsible for building the query needed for fetching products for Favourites.
 */
public class TargetFavouritesListQueryBuilder extends AbstractEndecaQueryBuilder {



    public ENEQueryResults getQueryResults(final EndecaSearchStateData endecaSearchStateData,
            final int maxNumberOfProducts) throws TargetEndecaException, ENEQueryException {
        final List<String> productCodes = endecaSearchStateData.getProductCodes();
        final UrlENEQuery query;
        if (CollectionUtils.isEmpty(endecaSearchStateData.getProductCodes())) {
            return null;
        }
        query = new UrlENEQuery("", EndecaConstants.CODE_LITERAL);
        query.setNr(buildRecordFilter(productCodes, endecaSearchStateData.getRecordFilterOptions()));
        query.setN(EndecaConstants.ENDECA_N_0);
        query.setNavNumAggrERecs(productCodes.size());
        query.setNavERecsPerAggrERec(Integer.parseInt(endecaSearchStateData.getNpSearchState()));
        query.setNavMerchRuleFilter("endeca.internal.nonexistent");
        query.setSelection(getFieldList(endecaSearchStateData.getFieldListConfig()));
        query.setNu(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE);
        query.setNavNumAggrERecs(maxNumberOfProducts);
        return fetchEndecaQueryResults(query);
    }

}

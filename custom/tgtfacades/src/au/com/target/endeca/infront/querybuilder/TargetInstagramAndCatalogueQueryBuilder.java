/**
 * 
 */
package au.com.target.endeca.infront.querybuilder;

import java.util.List;

import org.springframework.util.CollectionUtils;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;

import com.endeca.navigation.UrlENEQuery;
import com.endeca.navigation.UrlENEQueryParseException;


/**
 * @author pthoma20
 *
 *         This class is responsible for building the query needed for fetching products for Instagram.
 */
public class TargetInstagramAndCatalogueQueryBuilder extends AbstractEndecaQueryBuilder {


    public UrlENEQuery getQueryForFetchingAllProductsForInstagram(final EndecaSearchStateData endecaSearchStateData)
            throws UrlENEQueryParseException {
        final List<String> productCodes = endecaSearchStateData.getProductCodes();
        if (CollectionUtils.isEmpty(endecaSearchStateData.getProductCodes())) {
            return null;
        }
        final UrlENEQuery query = new UrlENEQuery("", EndecaConstants.CODE_LITERAL);
        query.setNr(buildRecordFilter(productCodes, endecaSearchStateData.getRecordFilterOptions()));
        query.setN(EndecaConstants.ENDECA_N_0);
        query.setNavNumAggrERecs(productCodes.size());
        query.setNavERecsPerAggrERec(Integer.parseInt(endecaSearchStateData.getNpSearchState()));
        query.setNavMerchRuleFilter("endeca.internal.nonexistent");
        query.setSelection(getFieldList(endecaSearchStateData.getFieldListConfig()));
        query.setNu(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE);
        return query;
    }




}

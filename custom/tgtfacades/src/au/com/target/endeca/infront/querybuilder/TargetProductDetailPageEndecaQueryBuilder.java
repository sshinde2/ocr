/**
 * 
 */
package au.com.target.endeca.infront.querybuilder;

import org.springframework.util.CollectionUtils;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaException;

import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.UrlENEQuery;


/**
 * @author mjanarth
 *
 */
public class TargetProductDetailPageEndecaQueryBuilder extends AbstractEndecaQueryBuilder {

    public ENEQueryResults getQueryResults(final EndecaSearchStateData endecaSearchStateData,
            final int maxNumberOfProducts) throws TargetEndecaException, ENEQueryException {

        if (CollectionUtils.isEmpty(endecaSearchStateData.getSearchTerm())) {
            return null;
        }
        final UrlENEQuery query;
        query = new UrlENEQuery("", EndecaConstants.CODE_LITERAL);
        query.setNr(buildRecordFilter(endecaSearchStateData.getSearchTerm(),
                endecaSearchStateData.getRecordFilterOptions()));
        query.setN(EndecaConstants.ENDECA_N_0);
        query.setNavERecsPerAggrERec(Integer.parseInt(endecaSearchStateData.getNpSearchState()));
        query.setNavMerchRuleFilter("endeca.internal.nonexistent");
        query.setSelection(getFieldList(endecaSearchStateData.getFieldListConfig()));
        query.setNu(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        query.setNavNumAggrERecs(maxNumberOfProducts);
        return fetchEndecaQueryResults(query);
    }
}

/**
 * 
 */
package au.com.target.endeca.infront.querybuilder;

import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.UrlENEQuery;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.exception.TargetEndecaException;


/**
 * @author pvarghe2
 *
 */
public class TargetSiteMapQueryBuilder extends AbstractEndecaQueryBuilder {

    /**
     * 
     */
    private static final String ENDECA_RECORD_FILTER_QUERY = "AND(merchDepartmentCode:%s,productDisplayFlag:1)";

    public ENEQueryResults getQueryResults(final int merchCode,
            final int pageNumber, final int productsPerPage) throws TargetEndecaException, ENEQueryException {
        final UrlENEQuery query = new UrlENEQuery("", EndecaConstants.CODE_LITERAL);
        query.setN(EndecaConstants.ENDECA_N_0);
        query.setNr(String.format(ENDECA_RECORD_FILTER_QUERY, Integer.valueOf(merchCode)));
        query.setNavNumAggrERecs(productsPerPage);
        query.setNavAggrERecsOffset(pageNumber * productsPerPage);
        query.setNu(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING);
        query.setSelection(getFieldList(EndecaConstants.FieldList.SITEMPAP_FIELDLIST));
        query.setNs(EndecaConstants.EndecaSortParams.ENDECA_SORT_DEFAULT);
        return fetchEndecaQueryResults(query);
    }

}

/**
 * 
 */
package au.com.target.endeca.infront.transform;

import java.util.List;

import au.com.target.endeca.infront.data.EndecaSearchStateData;




/**
 * @author smudumba
 * 
 */

public interface TargetEndecaURLHelper {

    /**
     * Method converts Target url to Endeca url by replacing target terms with endeca terms.
     * 
     * @param queryParams
     * @return String
     */
    public String getTargetToEndecaURL(String queryParams);

    /**
     * Method converts Endeca url to Target url by replacing endeca terms with target terms.
     * 
     * @param queryParams
     * @return String
     */
    public String getEndecaToTargetURL(String queryParams);

    /**
     * Builds EndecaSearchStateData bean from the params.
     * 
     * @param navigationState
     * @param searchTerm
     * @param sortTerm
     * @param noOfRecords
     * @param recordsOffset
     * @return EndecaSearchStateData
     */
    public EndecaSearchStateData buildEndecaSearchStateData(String navigationState, List<String> searchTerm,
            String sortTerm, int noOfRecords, int recordsOffset, String viewAs, String requestUrl, String categorycode,
            String brandCode);

    /**
     * Method to Validate the navigation state string
     * 
     * @param navigationState
     * @return String
     */
    public String getValidEndecaToTargetNavigationState(final String navigationState);


    /**
     * Method to remove Page template from url
     * 
     * @param navigationState
     * @return String
     */
    public String constructCurrentPageUrl(String requestUri, final String navigationState);

    /**
     * Get ws url query parameters.
     * 
     * @param searchState
     * @return query parameters
     */
    public String getWsUrlQueryParameters(final EndecaSearchStateData searchState);

    /**
     * construct Current WsPageUrl
     * 
     * @param searchStateData
     * @param navigationState
     * @return wsUrl
     */
    public String constructCurrentWsPageUrl(final EndecaSearchStateData searchStateData, final String navigationState);

}

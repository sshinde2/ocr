/**
 * 
 */
package au.com.target.endeca.infront.transform.impl;


import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.transform.TargetEndecaURLHelper;
import au.com.target.endeca.infront.util.TargetEndecaURLUtil;
import au.com.target.endeca.infront.util.UrlUtility;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.product.BrandService;
import au.com.target.tgtfacades.category.TargetCategoryFacade;
import au.com.target.tgtfacades.product.data.BrandData;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * @author smudumba Helper class used to transform Target URL to Endeca URL and viceversa Generate EndecaSearchStateData
 *         bean which is used by Endeca. *
 * 
 */
public class TargetEndecaURLHelperImpl implements TargetEndecaURLHelper, EndecaConstants {


    protected static final Logger LOG = Logger.getLogger(TargetEndecaURLHelperImpl.class);



    private TargetEndecaURLUtil targetEndecaURLUtil;

    private TargetCategoryFacade targetCategoryFacade;

    private BrandService brandService;

    private Converter<BrandModel, BrandData> targetBrandConverter;

    private static class KVP {
        private final String key;
        private final String value;

        /**
         * Store key value pairs
         */
        KVP(final String key, final String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * @param targetEndecaURLUtil
     */
    @Required
    public void setTargetEndecaURLUtil(final TargetEndecaURLUtil targetEndecaURLUtil) {
        this.targetEndecaURLUtil = targetEndecaURLUtil;
    }


    /**
     * @return the targetEndecaURLUtil
     */
    public TargetEndecaURLUtil getTargetEndecaURLUtil() {
        return targetEndecaURLUtil;
    }


    /**
     * Method converts Target url to Endeca url by replacing target terms with endeca terms.
     * 
     * @param queryParams
     * @return String
     */
    @Override
    public String getTargetToEndecaURL(final String queryParams) {
        List<KVP> queryVar = new ArrayList<>();
        if (StringUtils.isNotEmpty(queryParams)) {

            try {
                queryVar = parse(queryParams, TO_ENDECA_URL);
            }
            catch (final UnsupportedEncodingException e) {
                //this should never occur
                LOG.error(SplunkLogFormatter.formatMessage(
                        "Invalid Query String passed " + queryParams,
                        TgtutilityConstants.ErrorCode.ERR_TGTFACADE_URL_ERROR, ""), e);
            }
            catch (final IllegalArgumentException iae) {
                LOG.error(SplunkLogFormatter.formatMessage(
                        "Invalid Query String passed " + queryParams,
                        TgtutilityConstants.ErrorCode.ERR_TGTFACADE_URL_ERROR, ""), iae);
                return null;
            }
            catch (final Exception e) {
                LOG.error(SplunkLogFormatter.formatMessage(
                        "Invalid Query String passed " + queryParams,
                        TgtutilityConstants.ErrorCode.ERR_TGTFACADE_URL_ERROR, ""), e);
                return null;
            }
            return toQueryString(queryVar);
        }
        else {
            return null;
        }

    }

    /**
     * Method converts Endeca url to Target url by replacing endeca terms with target terms.
     * 
     * @param queryParams
     * @return String
     */
    @Override
    public String getEndecaToTargetURL(final String queryParams) {
        List<KVP> queryVar = new ArrayList<>();
        if (StringUtils.isNotEmpty(queryParams)) {

            try {
                queryVar = parse(queryParams, TO_TARGET_URL);
            }
            catch (final UnsupportedEncodingException e) {
                LOG.error(SplunkLogFormatter.formatMessage(
                        "Invalid Query String passed " + queryParams,
                        TgtutilityConstants.ErrorCode.ERR_TGTFACADE_URL_ERROR, ""), e);

            }
            catch (final IllegalArgumentException iae) {
                LOG.error(SplunkLogFormatter.formatMessage(
                        "Invalid Query String passed " + queryParams,
                        TgtutilityConstants.ErrorCode.ERR_TGTFACADE_URL_ERROR, ""), iae);
                return null;
            }
            catch (final Exception e) {
                LOG.error(SplunkLogFormatter.formatMessage(
                        "Invalid Query String passed " + queryParams,
                        TgtutilityConstants.ErrorCode.ERR_TGTFACADE_URL_ERROR, ""), e);
                return null;
            }

            return this.toQueryString(queryVar);

        }
        else {

            return null;
        }



    }

    /**
     * Method to add key and value to the map.
     * 
     * @param key
     * @param value
     */
    public void addParam(final String key, final String value, final List<KVP> queryVar) {
        if (key == null || value == null) {
            throw new NullPointerException("null parameter key or value");
        }
        queryVar.add(new KVP(key, value));
    }


    /**
     * Method which convert the url string from TGT to ENDECA and viceversa.
     * 
     * @param queryString
     * @param toFlag
     */
    private List<KVP> parse(final String queryString, final String toFlag) throws UnsupportedEncodingException {

        //initialze the query;
        final List<KVP> queryVar = new ArrayList<>();

        for (final String pair : queryString.split(QUERY_PARAM_SEPERATOR_AMP)) {
            String transformedValue = null;
            String key = null;
            final int eq = pair.indexOf("=");
            //fetch the key 
            if (eq > 0) {
                key = pair.substring(0, eq);
            }
            else {
                key = pair;
            }
            //process the key value
            if (TO_TARGET_URL.equals(toFlag)) {
                transformedValue = targetEndecaURLUtil.getTgtName(key);
            }
            else if (TO_ENDECA_URL.equals(toFlag)) {
                transformedValue = targetEndecaURLUtil.getEndecaName(key);

            }
            else {
                //No conversion done.
                transformedValue = key;
            }

            if (eq < 0) {

                addParam(URLDecoder.decode(transformedValue, CODE_LITERAL), "", queryVar);

            }
            else {
                //only add values needed
                final String value = pair.substring(eq + 1);
                //if key is NS Sort term
                if (transformedValue.equals(SORT_TERM_KEY)) {

                    final String sortTerm = this.getSortTerm(value);
                    // key=value
                    if (null != sortTerm) {
                        final String keyKVP = URLDecoder.decode(transformedValue, CODE_LITERAL);
                        final String valueKVP = URLDecoder.decode(sortTerm,
                                CODE_LITERAL);
                        queryVar.add(new KVP(keyKVP, valueKVP));
                    }
                }
                else {
                    // key=value
                    final String keyKVP = URLDecoder.decode(transformedValue, CODE_LITERAL);
                    final String valueKVP = URLDecoder.decode(value,
                            CODE_LITERAL);
                    queryVar.add(new KVP(keyKVP, valueKVP));
                }
            }
        }

        return queryVar;

    }

    /**
     * Method returns the name values pairs to a request query string
     * 
     * @return String
     */
    public String toQueryString(final List<KVP> queryString) {


        final StringBuilder sb = new StringBuilder();
        final StringBuilder orphanValues = new StringBuilder();

        for (final KVP kvp : queryString) {
            if (StringUtils.isEmpty(kvp.key)) {
                orphanValues.append(kvp.value);
                orphanValues.append(',');
                continue;
            }

            if (sb.length() > 0) {
                sb.append('&');
            }
            try {
                sb.append(URLEncoder.encode(kvp.key, CODE_LITERAL));
            }
            catch (final UnsupportedEncodingException e) {
                LOG.error(SplunkLogFormatter.formatMessage(
                        "Invalid Query String passed " + queryString,
                        TgtutilityConstants.ErrorCode.ERR_TGTFACADE_URL_ERROR, ""), e);
            }
            if (!kvp.value.equals("")) {
                sb.append('=');
                try {
                    sb.append(URLEncoder.encode(kvp.value, CODE_LITERAL));
                }
                catch (final UnsupportedEncodingException e) {
                    LOG.error(SplunkLogFormatter.formatMessage(
                            "Invalid Query String passed " + queryString,
                            TgtutilityConstants.ErrorCode.ERR_TGTFACADE_URL_ERROR, ""), e);
                }
            }
        }

        LOG.debug("Orphan Values  : " + orphanValues.toString());

        return sb.toString();

    }

    /**
     * Utility Method to find value of a key.
     * 
     * @param key
     * @return String
     */
    public String getParameter(final String key, final List<KVP> queryVar) {
        for (final KVP kvp : queryVar) {
            if (kvp.key.equals(key)) {
                return kvp.value;
            }
        }
        return null;
    }

    /**
     * Utility Method to get a list of parameter values.
     * 
     * @param key
     * @return List
     */
    public List<String> getParameterValues(final String key, final List<KVP> queryVar) {
        final List<String> list = new LinkedList<>();
        for (final KVP kvp : queryVar) {
            if (kvp.key.equals(key)) {
                list.add(kvp.value);
            }
        }
        return list;
    }


    /**
     * Method build EndecaSearchStateData object from parameters which will used by for endeca.
     * 
     * @param navigationState
     * @param searchTerms
     * @param sortTerm
     * @param noOfRecords
     * @param recordsOffset
     * @return EndecaSearchStateData
     */
    @Override
    public EndecaSearchStateData buildEndecaSearchStateData(final String navigationState,
            final List<String> searchTerms,
            final String sortTerm, final int noOfRecords,
            final int recordsOffset, final String viewAs, final String requestUrl, final String categorycode,
            final String brandCode)

    {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        searchStateData.setRequestUrl(requestUrl);
        if (StringUtils.isNotEmpty(categorycode)) {
            searchStateData.setCategoryData(targetCategoryFacade.getCategoryData(categorycode));
        }

        if (StringUtils.isNotEmpty(brandCode)) {
            final BrandModel brand = brandService.getBrandForFuzzyName(brandCode, false);
            searchStateData.setBrand(targetBrandConverter.convert(brand));
        }

        if (StringUtils.isNotEmpty(navigationState)) {
            final List<String> navStateList = new ArrayList<>();
            navStateList.add(navigationState);
            searchStateData.setNavigationState(navStateList);
        }

        //process search term
        if (CollectionUtils.isNotEmpty(searchTerms)) {
            final List<String> searchTermList = new ArrayList<>();
            for (final String searchTerm : searchTerms) {
                if (StringUtils.isNotEmpty(searchTerm)) {
                    //Remove characters not recognized by Endeca for search terms.
                    final String sanitizedString = searchTerm.replaceAll(INVALID_ENDECA_SEARCH_LITERALS, "");
                    if (StringUtils.isNotEmpty(sanitizedString)) {
                        final String encodedString = UrlUtility.getEncodedString(sanitizedString);
                        if (StringUtils.isNotEmpty(encodedString)) {
                            searchTermList.add(encodedString);
                        }
                    }

                }
            }
            searchStateData.setSearchTerm(searchTermList);
        }

        searchStateData.setItemsPerPage(noOfRecords);
        searchStateData.setPageNumber(recordsOffset);
        searchStateData.setViewAs(viewAs);

        //Process the sort term,  format is [sortoption]|[0/1] 
        if (StringUtils.isNotEmpty(sortTerm)) {
            final String[] sortValues = sortTerm.split("\\" + EndecaConstants.QUERY_PARAM_SINGLE_PIPE);
            // ensure we have values in the list and no Index out of bound exception can be raised
            if (null != sortValues && sortValues.length > 0) {
                int i = 0;
                while (i < sortValues.length) {
                    if (StringUtils.isNotBlank(sortValues[i])) {
                        final String key = sortValues[i];
                        int order = 0;
                        if (sortValues.length > 1) {
                            try {

                                order = Integer.parseInt(sortValues[++i]);

                            }
                            catch (final Exception e) {
                                order = 0;
                            }
                        }
                        //incase if the map is null
                        if (null == searchStateData.getSortStateMap()) {
                            searchStateData.setSortStateMap(new LinkedHashMap<String, Integer>());
                        }
                        searchStateData.getSortStateMap().put(key, new Integer(order));
                    }
                    i++;
                }
            }
        }
        return searchStateData;
    }

    /**
     * To filter the sort term and to concatenate the other sortterms
     * 
     * @param value
     * @return String
     */
    private String getSortTerm(final String value) {

        final String pattern = ENCODED_QUERY_PARAM_SINGLE_PIPE;
        //value is a name value pairs concatenated with %7C,  processing rule is,        
        final Pattern splitter = Pattern.compile(pattern);
        //Reading all the values into an array
        final String[] resultValues = splitter.split(value);
        final Map<String, String> resultMap = new LinkedHashMap<String, String>();
        String[] keyValueArray = new String[2];
        //Process it an create a map with key and values
        int j = 0;
        for (int i = 0; i < resultValues.length; i++) {

            final String resultValue = resultValues[i];
            if (j > 1) {
                if (null != keyValueArray[0]) {
                    resultMap.put(keyValueArray[0], keyValueArray[1]);
                }
                keyValueArray = new String[2];
                j = 0;
                if (StringUtils.isEmpty(resultValue)) {
                    continue;
                }
            }
            keyValueArray[j] = resultValues[i];
            j++;
        }
        //cater for the last value
        if (null != keyValueArray[0]) {
            resultMap.put(keyValueArray[0], keyValueArray[1]);
        }
        final StringBuilder sb = new StringBuilder();
        for (final Entry<String, String> entry : resultMap.entrySet()) {
            if (StringUtils.isNotBlank(sb.toString())) {
                sb.append(EndecaConstants.QUERY_PARAM_DOUBLE_PIPE);
            }
            if (null != entry.getKey()) {
                sb.append(entry.getKey())
                        .append(null != entry.getValue() ? EndecaConstants.QUERY_PARAM_SINGLE_PIPE : "").append
                        (null != entry.getValue() ? entry.getValue() : "");
            }
        }
        return (0 == sb.toString().length() ? null : sb.toString());

    }

    /**
     * Method to Validate the navigation state string
     * 
     * @param navigationState
     * @return String
     */
    @Override
    public String getValidEndecaToTargetNavigationState(final String navigationState) {
        String finalValidatedNavigationState = StringUtils.EMPTY;
        if (StringUtils.isNotEmpty(navigationState)) {
            //Remove the prefix of ? or &
            final String validatedNavigationState = getEndecaToTargetURL(navigationState.substring(1));
            if (StringUtils.isNotEmpty(validatedNavigationState)) {
                finalValidatedNavigationState = EndecaConstants.QUERY_PARAM_SEPERATOR_QNMRK
                        + validatedNavigationState;
            }
        }
        return finalValidatedNavigationState;
    }

    @Override
    public String constructCurrentPageUrl(final String requestUri, final String navigationState) {
        String navState = "";
        if (navigationState != null) {
            if (navigationState.contains(EndecaConstants.ENDECA_PAGE_DEFAULT_TEMPLATE)) {
                navState = navigationState.replaceAll(EndecaConstants.QUERY_PARAM_SEPERATOR_AMP
                        + EndecaConstants.ENDECA_PAGE_DEFAULT_TEMPLATE, "");
                navState = navState.replaceAll(EndecaConstants.ENDECA_PAGE_DEFAULT_TEMPLATE, "");
            }

            navState = getValidEndecaToTargetNavigationState(navState);
        }
        return requestUri + navState;
    }

    @Override
    public String constructCurrentWsPageUrl(final EndecaSearchStateData searchStateData, final String navigationState) {
        String wsUrl = StringUtils.EMPTY;
        String navState = StringUtils.EMPTY;
        if (navigationState != null) {
            if (navigationState.contains(EndecaConstants.ENDECA_PAGE_DEFAULT_TEMPLATE)) {
                navState = navigationState.replaceAll(EndecaConstants.QUERY_PARAM_SEPERATOR_AMP
                        + EndecaConstants.ENDECA_PAGE_DEFAULT_TEMPLATE, "");
                navState = navState.replaceAll(EndecaConstants.ENDECA_PAGE_DEFAULT_TEMPLATE, "");
            }
            navState = getValidEndecaToTargetNavigationState(navState);

        }
        if (StringUtils.isNotEmpty(navState)) {
            wsUrl += navState;
            if (StringUtils.isNotEmpty(searchStateData.getCurrentQueryParam())) {
                wsUrl += searchStateData.getCurrentQueryParam();
            }

        }
        else if (StringUtils.isNotEmpty(searchStateData.getCurrentQueryParam())) {
            wsUrl = wsUrl + EndecaConstants.QUERY_PARAM_SEPERATOR_QNMRK
                    + searchStateData.getCurrentQueryParam().substring(1);
        }
        return wsUrl;
    }

    @Override
    public String getWsUrlQueryParameters(final EndecaSearchStateData searchState) {
        final StringBuilder wsUrl = new StringBuilder();
        if (searchState != null && searchState.getBrand() != null
                && StringUtils.isNotEmpty(searchState.getBrand().getCode())) {
            wsUrl.append(EndecaConstants.QUERY_PARAM_SEPERATOR_AMP).append(EndecaConstants.WS_URL_BRAND)
                    .append(searchState.getBrand().getCode());
        }
        else if (searchState != null && searchState.getCategoryData() != null
                && StringUtils.isNotEmpty(searchState.getCategoryData().getCode())) {
            wsUrl.append(EndecaConstants.QUERY_PARAM_SEPERATOR_AMP).append(EndecaConstants.WS_URL_CATEGORY)
                    .append(searchState.getCategoryData().getCode());
        }

        return wsUrl.toString();
    }

    /**
     * @param targetCategoryFacade
     *            the targetCategoryFacade to set
     */
    @Required
    public void setTargetCategoryFacade(final TargetCategoryFacade targetCategoryFacade) {
        this.targetCategoryFacade = targetCategoryFacade;
    }


    /**
     * @param brandService
     *            the brandService to set
     */
    @Required
    public void setBrandService(final BrandService brandService) {
        this.brandService = brandService;
    }


    /**
     * @param targetBrandConverter
     *            the targetBrandConverter to set
     */
    @Required
    public void setTargetBrandConverter(final Converter<BrandModel, BrandData> targetBrandConverter) {
        this.targetBrandConverter = targetBrandConverter;
    }



}

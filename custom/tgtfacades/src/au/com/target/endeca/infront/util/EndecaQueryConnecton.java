/**
 * 
 */
package au.com.target.endeca.infront.util;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.exception.TargetEndecaException;

import com.endeca.navigation.HttpENEConnection;


/**
 * @author jjayawa1
 * 
 */
public class EndecaQueryConnecton {
    private String host;
    private String port;
    private HttpENEConnection connection;

    /**
     * Method to create HttpENEConnection
     * 
     * @return HttpENEConnection
     * @throws TargetEndecaException
     */
    public HttpENEConnection getConection() throws TargetEndecaException {


        if (null == connection) {
            if (StringUtils.isEmpty(host) || StringUtils.isEmpty(port)) {
                throw new TargetEndecaException("Connection Details Not Set");
            }
            connection = new HttpENEConnection(host, port);
        }

        return connection;
    }

    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * @param host
     *            the host to set
     */
    @Required
    public void setHost(final String host) {
        this.host = host;
    }

    /**
     * @return the port
     */

    public String getPort() {
        return port;
    }

    /**
     * @param port
     *            the port to set
     */
    @Required
    public void setPort(final String port) {
        this.port = port;

    }
}

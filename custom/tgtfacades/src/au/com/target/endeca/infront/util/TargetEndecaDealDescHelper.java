/**
 * 
 */
package au.com.target.endeca.infront.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtfacades.product.data.TargetProductListerData;


/**
 * @author mjanarth
 * 
 */
public final class TargetEndecaDealDescHelper {


    private TargetEndecaDealDescHelper() {
        //prevent construction
    }

    /**
     * This method sets the deal Message if the deal is active at the current date/time
     * 
     * @param startDateStr
     * @param endDateStr
     * @param dealDesc
     * @param productData
     * @throws ParseException
     */
    public static void processDealMessage(final String startDateStr, final String endDateStr,
            final String dealDesc, final TargetProductListerData productData) throws ParseException {

        final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy hh:mma");
        final Date currentDate = new Date();
        if (StringUtils.isNotEmpty(startDateStr) && StringUtils.isNotEmpty(endDateStr)) {
            final Date endDate = dateFormat.parse(endDateStr);
            final Date startDate = dateFormat.parse(startDateStr);
            if (endDate.before(currentDate) || startDate.after(currentDate)) {
                productData.setDealDescription(StringUtils.EMPTY);
            }
            else {
                if (StringUtils.isNotEmpty(dealDesc)) {
                    productData.setDealDescription(dealDesc);
                }
                else {
                    productData.setDealDescription(StringUtils.EMPTY);
                }
            }
        }
        else {
            productData.setDealDescription(StringUtils.EMPTY);
        }

    }

}

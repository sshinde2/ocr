/**
 * 
 */
package au.com.target.endeca.infront.util;

import java.util.Collections;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;


/**
 * @author smudumba Utility class converts Endeca Name to Target Name and vice versa.
 * 
 */
public class TargetEndecaURLUtil {


    private static final String NO_MATCH_FOUND = "";
    private final Map<String, String> tgtEndecaMap;
    private final BiMap<String, String> biTgtEndecaMap;


    /**
     * Constructor with initializes the map from a properties file.
     * 
     * @param tgtEndecaMap
     */
    @Autowired
    public TargetEndecaURLUtil(final Map<String, String> tgtEndecaMap) {
        super();
        this.tgtEndecaMap = tgtEndecaMap;
        biTgtEndecaMap = ImmutableBiMap.copyOf(Collections.unmodifiableMap(tgtEndecaMap));

    }

    /**
     * @return the biTgtEndecaMap
     */
    public BiMap<String, String> getBiTgtEndecaMap() {
        return biTgtEndecaMap;
    }

    /**
     * @return the tgtEndecaMap
     */
    public Map<String, String> getTgtEndecaMap() {
        return tgtEndecaMap;
    }


    /**
     * @param endecaName
     * @return String
     */
    public String getTgtName(final String endecaName) {

        return biTgtEndecaMap.containsValue(endecaName) ? biTgtEndecaMap.inverse()
                .get(endecaName) : NO_MATCH_FOUND;

    }

    /**
     * @param tgtName
     * @return String
     */
    public String getEndecaName(final String tgtName) {

        return biTgtEndecaMap.containsKey(tgtName) ? biTgtEndecaMap
                .get(tgtName) : NO_MATCH_FOUND;

    }


    /**
     * @param propertyValue
     *            )
     * @return String
     */
    public String getPropertValue(final String propertyValue) {

        return biTgtEndecaMap.containsKey(propertyValue) ? biTgtEndecaMap
                .get(propertyValue) : NO_MATCH_FOUND;

    }

}

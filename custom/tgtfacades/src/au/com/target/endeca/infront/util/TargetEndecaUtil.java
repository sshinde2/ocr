/**
 * 
 */
package au.com.target.endeca.infront.util;

import de.hybris.platform.commercefacades.product.data.PriceData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.endeca.infront.cartridge.model.Record;
import com.endeca.infront.cartridge.model.RecordAction;
import com.endeca.infront.navigation.model.CollectionFilter;
import com.endeca.infront.navigation.model.PropertyFilter;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.tgtfacades.url.impl.TargetProductRecordSearchUrlResover;
import au.com.target.tgtfacades.util.TargetPriceHelper;


/**
 * The Class TargetEndecaUtil, responsible for doing pre and post operation during queries Endeca.
 *
 * @author ragarwa3
 */
public class TargetEndecaUtil {

    private TargetProductRecordSearchUrlResover targetProductRecordSearchUrlResover;

    private TargetPriceHelper targetPriceHelper;

    /**
     * Creates the stratify for ordered recommendations. This method will add the colourVariantCodes as stratify so that
     * we'll receive from Endeca in order in which they are needed.
     *
     * @param colourVariantCodes
     *            the colour variant codes
     * @return the list
     */
    public List<CollectionFilter> createStratifyForOrderedRecommendations(final List<String> colourVariantCodes) {
        final List<CollectionFilter> collFilter = new ArrayList<>();

        for (final String code : colourVariantCodes) {
            final PropertyFilter propFilter = new PropertyFilter(
                    EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, code);
            collFilter.add(new CollectionFilter(propFilter));
        }
        return collFilter;
    }

    /**
     * Creates the record filter for colour variants. This will return like -
     * OR(colourVariantCode:abcd,colourVariantCode:pqrs)
     *
     * @param colourVariantCodes
     *            the colour variant codes
     * @return the string
     */
    public String createRecordFilterForColourVariants(final List<String> colourVariantCodes) {

        if (CollectionUtils.isEmpty(colourVariantCodes)) {
            return StringUtils.EMPTY;
        }

        final StringBuilder stringBuffer = new StringBuilder();
        stringBuffer.append(EndecaConstants.EndecaFilterQuery.ENDECA_OR_FILTER_KEY).append(
                EndecaConstants.EndecaFilterQuery.ENDECA_OR_FILTER_START);

        for (final String code : colourVariantCodes) {
            stringBuffer.append(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING)
                    .append(EndecaConstants.EndecaFilterQuery.ENDECA_FILTER_KEY_VALUE_SEPERATOR).append(code)
                    .append(EndecaConstants.EndecaFilterQuery.ENDECA_MULTIPLE_FILTER_VALUES_SEPERATOR);
        }

        stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        stringBuffer.append(
                EndecaConstants.EndecaFilterQuery.ENDECA_OR_FILTER_END);

        return stringBuffer.toString();
    }

    /**
     * Method to enrich data that came from Endeca before passing to hybris so that the data can be used efficiently.
     *
     * @param recordList
     *            the record list
     * @param unEscapeHtml
     *            flag to determine whether html needs to be unEscaped.
     * @return the list
     */
    public List<Record> enrichEndecaRecords(final List<Record> recordList, final boolean unEscapeHtml) {
        if (CollectionUtils.isNotEmpty(recordList)) {
            for (final Record record : recordList) {

                //enriching product url
                final String url = targetProductRecordSearchUrlResover.resolve(record);
                if (StringUtils.isNotEmpty(url)) {
                    RecordAction recordAction = record.getDetailsAction();
                    if (recordAction == null) {
                        recordAction = new RecordAction();
                        record.setDetailsAction(recordAction);
                    }
                    recordAction.setRecordState(url);
                }

                //enriching product prices
                final Map recordAttrs = record.getAttributes();

                String endecaPropertyKey = EndecaConstants.EndecaRecordSpecificFields.ENDECA_MIN_PRICE;
                String minPriceString = "0.0";

                if (recordAttrs.containsKey(endecaPropertyKey)) {
                    minPriceString = getPropertyValueAsString(recordAttrs, endecaPropertyKey);
                    createPriceDataAndPopulate(recordAttrs, endecaPropertyKey);
                }

                endecaPropertyKey = EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_PRICE;
                if (recordAttrs.containsKey(endecaPropertyKey)) {
                    final String maxPriceString = getPropertyValueAsString(recordAttrs, endecaPropertyKey);
                    if (StringUtils.isNotBlank(maxPriceString)
                            && Double.valueOf(maxPriceString).compareTo(Double.valueOf(minPriceString)) > 0) {
                        createPriceDataAndPopulate(recordAttrs, endecaPropertyKey);
                    }
                    else {
                        recordAttrs.remove(endecaPropertyKey);
                    }
                }

                //populate was price only if valid
                endecaPropertyKey = EndecaConstants.EndecaRecordSpecificFields.ENDECA_MIN_WAS_PRICE;
                if (recordAttrs.containsKey(endecaPropertyKey)) {
                    final String minWasPriceString = getPropertyValueAsString(recordAttrs, endecaPropertyKey);

                    // min_wasprice value must be greater than min_price value for valid was price
                    if (StringUtils.isNotBlank(minWasPriceString)
                            && Double.valueOf(minWasPriceString).compareTo(Double.valueOf(minPriceString)) > 0) {

                        createPriceDataAndPopulate(recordAttrs, endecaPropertyKey);

                        endecaPropertyKey = EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_WAS_PRICE;
                        if (recordAttrs.containsKey(endecaPropertyKey)) {
                            createPriceDataAndPopulate(recordAttrs, endecaPropertyKey);
                        }
                    }
                    else {
                        recordAttrs.remove(endecaPropertyKey);
                        recordAttrs.remove(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_WAS_PRICE);
                    }
                }

                //Unescape productName
                if (unEscapeHtml) {
                    endecaPropertyKey = EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_NAME;
                    if (recordAttrs.containsKey(endecaPropertyKey)) {
                        final String productName = getPropertyValueAsString(recordAttrs, endecaPropertyKey);
                        if (StringUtils.isNotEmpty(productName)) {
                            recordAttrs.put(endecaPropertyKey, StringEscapeUtils.unescapeHtml(productName));
                        }
                    }
                }
            }
        }
        return recordList;
    }

    private void createPriceDataAndPopulate(final Map recordAttrs, final String endecaPropertyKey) {
        final String endecaPropertyValue = getPropertyValueAsString(recordAttrs, endecaPropertyKey);
        if (StringUtils.isNotBlank(endecaPropertyValue)) {
            final PriceData priceData = targetPriceHelper
                    .createPriceData(Double.valueOf(endecaPropertyValue));
            recordAttrs.put(endecaPropertyKey, priceData);
        }
        else {
            recordAttrs.remove(endecaPropertyKey);
        }
    }

    private String getPropertyValueAsString(final Map recordAttrs, final String endecaPropertyKey) {
        return ObjectUtils.toString(recordAttrs.get(endecaPropertyKey));
    }

    /**
     * @param targetProductRecordSearchUrlResover
     *            the targetProductRecordSearchUrlResover to set
     */
    @Required
    public void setTargetProductRecordSearchUrlResover(
            final TargetProductRecordSearchUrlResover targetProductRecordSearchUrlResover) {
        this.targetProductRecordSearchUrlResover = targetProductRecordSearchUrlResover;
    }

    /**
     * @param targetPriceHelper
     *            the targetPriceHelper to set
     */
    @Required
    public void setTargetPriceHelper(final TargetPriceHelper targetPriceHelper) {
        this.targetPriceHelper = targetPriceHelper;
    }

}

/**
 * 
 */
package au.com.target.endeca.infront.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author rsamuel3
 * 
 */
public final class UrlUtility {

    private static final Logger LOG = Logger.getLogger(UrlUtility.class);

    private UrlUtility() {
        //not to be instantiated
    }


    /**
     * @param navigationState
     * @param searchQueryParam
     * @return String representing the query parameters
     */
    public static String buildUrlQueryString(final String navigationState, final String searchQueryParam) {
        try
        {
            if (StringUtils.isNotBlank(searchQueryParam))
            {
                return "?q=" + URLEncoder.encode(searchQueryParam, "UTF-8");
            }
            else if (StringUtils.isNotEmpty(navigationState)) {
                return navigationState;
            }
        }
        catch (final UnsupportedEncodingException e)
        {
            LOG.error("Error occured when encoding the URL", e);
        }
        return "";
    }

    /**
     * Method to encode a string
     * 
     * @param plainText
     * @return String
     */
    public static String getEncodedString(final String plainText) {

        String result = StringUtils.EMPTY;
        try
        {
            if (StringUtils.isNotEmpty(plainText)) {
                result = URLEncoder.encode(plainText, "UTF-8");
            }

        }

        catch (final UnsupportedEncodingException e)
        {
            LOG.error("Error occured when encoding the URL", e);
        }

        return result;

    }
}

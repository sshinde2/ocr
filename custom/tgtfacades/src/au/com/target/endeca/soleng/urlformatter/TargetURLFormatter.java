/**
 * 
 */
package au.com.target.endeca.soleng.urlformatter;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.endeca.soleng.urlformatter.NavStateUrlParam;
import com.endeca.soleng.urlformatter.UrlFormatException;
import com.endeca.soleng.urlformatter.UrlParam;
import com.endeca.soleng.urlformatter.UrlState;
import com.endeca.soleng.urlformatter.seo.SeoUrlFormatter;
import com.endeca.soleng.urlformatter.seo.SeoUrlParam;

import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * URL formatter extending SEOUrlFormatter to customize Endeca specific URL formatting to handle category and brand URL
 * mapping in target.
 * 
 * @author jjayawa1
 * 
 */
public class TargetURLFormatter extends SeoUrlFormatter {

    protected static final Logger LOG = Logger.getLogger(TargetURLFormatter.class);

    @Override
    public String formatUrl(final UrlState pUrlState)
            throws UrlFormatException {
        return formatUrl(pUrlState, false);
    }

    @Override
    public String formatCanonicalUrl(final UrlState pUrlState) throws UrlFormatException {
        return formatUrl(pUrlState, true);
    }

    /*
     * SeoUrlFormatter handles path parameters differently from what we expect in Target.
     * So this method was copied and changed from SeoUrlFormatter.
     */
    private String formatUrl(final UrlState pUrlState, final boolean forceCanonical) throws UrlFormatException {
        if (null != getNavStateCanonicalizer() && (useNavStateCanonicalizer() || forceCanonical)) {
            final UrlParam urlParam = pUrlState.getUrlParam(getNavStateParamKey());
            if (urlParam instanceof NavStateUrlParam) {
                getNavStateCanonicalizer().canonicalize((NavStateUrlParam)urlParam);
            }
        }

        final String pathParams = formatPathParams(pUrlState);
        final String queryString = formatQueryString(pUrlState);

        final StringBuilder buffer = new StringBuilder();

        if (pathParams.length() > 0) {
            buffer.append('?');
            buffer.append(pathParams);
        }

        if (queryString.length() > 0) {
            buffer.append('&');
            buffer.append(queryString);
        }

        return buffer.toString();
    }

    @Override
    protected String formatPathParams(final UrlState pUrlState)
            throws UrlFormatException {
        final StringBuilder buffer = new StringBuilder();

        final Collection parameters = pUrlState.getParameters();
        if (CollectionUtils.isEmpty(parameters)) {
            return buffer.toString();
        }
        for (final Object urlParamObject : parameters) {
            final UrlParam urlParam = (UrlParam)urlParamObject;
            if (urlParam instanceof SeoUrlParam) {
                final SeoUrlParam seoUrlParam = (SeoUrlParam)urlParam;
                if (seoUrlParam.getIsPathParam()) {
                    updateBuffer(buffer, urlParam);
                }
            }
        }
        return buffer.toString();
    }

    /**
     * @param buffer
     * @param urlParam
     * @throws UrlFormatException
     */
    private void updateBuffer(final StringBuilder buffer, final UrlParam urlParam) throws UrlFormatException {
        buffer.append(urlParam.getEncodedKey());
        final String value = urlParam.getEncodedValue();
        if (null != value) {
            buffer.append('=');
            buffer.append(value);
        }
    }

    @Override
    protected void parseQueryString(final UrlState pUrlState, final String pQueryString)
            throws UrlFormatException {

        try {
            super.parseQueryString(pUrlState, pQueryString);
            LOG.info("query String in endeca : " + pQueryString + " -----purlState---- : " + pUrlState);
        }
        catch (final UrlFormatException e) {
            LOG.error(SplunkLogFormatter.formatMessage(
                    "URL format is not as expected by endeca for decoding encoded navigation state",
                    TgtutilityConstants.ErrorCode.ERR_SEARCH, ""), e);
        }
    }

}

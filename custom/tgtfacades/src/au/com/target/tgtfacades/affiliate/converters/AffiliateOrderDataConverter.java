/**
 * 
 */
package au.com.target.tgtfacades.affiliate.converters;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.affiliate.data.AffiliateOrderData;
import au.com.target.tgtfacades.affiliate.data.AffiliateOrderEntryData;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.order.data.TargetOrderEntryData;


/**
 * @author sbryan6
 *
 */
public class AffiliateOrderDataConverter
        extends AbstractPopulatingConverter<AbstractOrderModel, TargetOrderData> {

    private static final Logger LOG = Logger.getLogger(AffiliateOrderDataConverter.class);

    private String delimiter;
    private String discountLabel;

    @Override
    public void populate(final AbstractOrderModel source, final TargetOrderData orderData) {

        final String orderCode = orderData.getCode();

        // rakuten wants delimited strings for each in the pixel tag
        final StringBuilder codes = new StringBuilder();
        final StringBuilder quantities = new StringBuilder();
        final StringBuilder prices = new StringBuilder();

        final Map<String, AffiliateOrderEntryData> affiliateOfferDetails = new HashMap<String, AffiliateOrderEntryData>();

        if (CollectionUtils.isNotEmpty(orderData.getEntries())) {

            for (final OrderEntryData orderEntryData : orderData.getEntries()) {


                if (orderEntryData.getProduct() == null || orderEntryData.getProduct().getCode() == null) {
                    logWarn("no product or code in order entry", orderCode, null);
                    continue;
                }

                final String productCode = orderEntryData.getProduct().getCode();

                if (orderEntryData.getQuantity() == null) {
                    logWarn("no quantity in order entry", orderCode, productCode);
                    continue;
                }
                if (orderEntryData.getTotalPrice() == null || orderEntryData.getTotalPrice().getValue() == null) {
                    logWarn("no total price in order entry", orderCode, productCode);
                    continue;
                }
                if (orderEntryData instanceof TargetOrderEntryData) {
                    populateAffiliateOfferDetails(affiliateOfferDetails, orderEntryData);
                }
                else {
                    appendString(codes, orderEntryData.getProduct().getCode());
                    appendLong(quantities, orderEntryData.getQuantity().longValue());
                    appendPrice(prices, orderEntryData.getTotalPrice().getValue());
                }

            }

            //aggregate the different affiliate categories
            if (MapUtils.isNotEmpty(affiliateOfferDetails)) {
                for (final String key : affiliateOfferDetails.keySet()) {
                    appendString(codes, key);
                    final AffiliateOrderEntryData affiliateData = affiliateOfferDetails.get(key);
                    appendLong(quantities, affiliateData.getQuantity());
                    appendPrice(prices, affiliateData.getPrice());
                }

            }

            //apply discounts
            if (codes.length() > 0) {

                // Discount entry
                final BigDecimal discount = getDiscountExcludingFlybuys(orderData);
                if (discount != null && discount.compareTo(BigDecimal.ZERO) > 0) {

                    appendString(codes, discountLabel);
                    appendLong(quantities, 0);
                    appendPrice(prices, discount.negate());
                }

                final AffiliateOrderData affiliateOrderData = new AffiliateOrderData(codes.toString(),
                        quantities.toString(),
                        prices.toString());
                orderData.setAffiliateOrderData(affiliateOrderData);

                doLogging(orderData, affiliateOrderData);
            }

        }

    }

    /**
     * @param affiliateOfferDetails
     * @param orderEntryData
     */
    private void populateAffiliateOfferDetails(final Map<String, AffiliateOrderEntryData> affiliateOfferDetails,
            final OrderEntryData orderEntryData) {
        final TargetOrderEntryData targetOrderEntryData = (TargetOrderEntryData)orderEntryData;
        final String affiliateCategory = targetOrderEntryData.getAffiliateOfferCategory();
        AffiliateOrderEntryData value = affiliateOfferDetails.get(affiliateCategory);
        if (null == value) {
            value = new AffiliateOrderEntryData();
        }
        value.addPriceAndQuantity(targetOrderEntryData.getTotalPrice().getValue(), targetOrderEntryData.getQuantity());
        affiliateOfferDetails.put(affiliateCategory, value);
    }

    protected BigDecimal getDiscountExcludingFlybuys(final TargetOrderData orderData) {

        BigDecimal discount = getOrderDiscounts(orderData);

        if (discount != null) {

            final BigDecimal flybuys = getFlybuysDiscount(orderData);
            if (flybuys != null) {
                discount = discount.subtract(flybuys);
            }
        }

        return discount;
    }

    protected BigDecimal getOrderDiscounts(final TargetOrderData orderData) {

        if (orderData.getOrderDiscounts() != null && orderData.getOrderDiscounts().getValue() != null) {
            return orderData.getOrderDiscounts().getValue();
        }
        return null;
    }

    protected BigDecimal getProductDiscounts(final TargetOrderData orderData) {

        if (orderData.getProductDiscounts() != null && orderData.getProductDiscounts().getValue() != null) {
            return orderData.getProductDiscounts().getValue();
        }
        return null;
    }

    protected BigDecimal getFlybuysDiscount(final TargetOrderData orderData) {

        if (orderData.getFlybuysDiscountData() != null && orderData.getFlybuysDiscountData().getValue() != null) {
            return orderData.getFlybuysDiscountData().getValue().getValue();
        }

        return null;
    }

    private void logWarn(final String message, final String orderCode, final String productCode) {

        final String logMessage = "AffiliateOrder orderCode=" + orderCode
                + ", productCode=" + productCode
                + ", message=" + message;
        LOG.warn(logMessage);
    }

    private void appendString(final StringBuilder in, final String toAppend) {

        addDelimiter(in);
        in.append(toAppend);
    }

    private void appendLong(final StringBuilder in, final long toAppend) {

        addDelimiter(in);
        in.append(toAppend);
    }

    private void appendPrice(final StringBuilder in, final BigDecimal price) {

        // Multiply by 100 to get cents
        appendLong(in, getCentsPrice(price));
    }

    private long getCentsPrice(final BigDecimal price) {

        return price.multiply(BigDecimal.valueOf(100)).longValue();
    }

    private void addDelimiter(final StringBuilder in) {

        if (in.length() > 0) {
            in.append(delimiter);
        }
    }

    private void doLogging(final TargetOrderData orderData, final AffiliateOrderData affiliateOrderData) {

        // Logging
        String logMessage = "codes=" + affiliateOrderData.getCodes()
                + ", quantities=" + affiliateOrderData.getQuantities()
                + ", prices=" + affiliateOrderData.getPrices();

        final BigDecimal orderDiscounts = getOrderDiscounts(orderData);
        if (orderDiscounts != null) {
            logMessage += ", orderDiscounts=" + orderDiscounts;
        }

        final BigDecimal productDiscounts = getProductDiscounts(orderData);
        if (productDiscounts != null) {
            logMessage += ", productDiscounts=" + productDiscounts;
        }

        final BigDecimal flybuysDiscounts = getFlybuysDiscount(orderData);
        if (flybuysDiscounts != null) {
            logMessage += ", flybuysDiscounts=" + flybuysDiscounts;
        }

        LOG.info("AffiliateOrder orderCode=" + orderData.getCode() + ", " + logMessage);
    }

    /**
     * @param delimiter
     *            the delimiter to set
     */
    @Required
    public void setDelimiter(final String delimiter) {
        this.delimiter = delimiter;
    }


    /**
     * @param discountLabel
     *            the discountLabel to set
     */
    @Required
    public void setDiscountLabel(final String discountLabel) {
        this.discountLabel = discountLabel;
    }


}

/**
 * 
 */
package au.com.target.tgtfacades.affiliate.data;



/**
 * DTO with order item details concatenated into strings.
 *
 */
public class AffiliateOrderData {

    private String codes;
    private String quantities;
    private String prices;

    public AffiliateOrderData(final String codes, final String quantities, final String prices) {

        this.codes = codes;
        this.quantities = quantities;
        this.prices = prices;
    }

    /**
     * @return the codes
     */
    public String getCodes() {
        return codes;
    }

    /**
     * @param codes
     *            the codes to set
     */
    public void setCodes(final String codes) {
        this.codes = codes;
    }

    /**
     * @return the quantities
     */
    public String getQuantities() {
        return quantities;
    }

    /**
     * @param quantities
     *            the quantities to set
     */
    public void setQuantities(final String quantities) {
        this.quantities = quantities;
    }

    /**
     * @return the prices
     */
    public String getPrices() {
        return prices;
    }

    /**
     * @param prices
     *            the prices to set
     */
    public void setPrices(final String prices) {
        this.prices = prices;
    }


}

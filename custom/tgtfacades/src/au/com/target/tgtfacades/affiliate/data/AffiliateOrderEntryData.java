/**
 * 
 */
package au.com.target.tgtfacades.affiliate.data;

import java.math.BigDecimal;


/**
 * DTO to temporarily hold the quantity and price when adding up the quantity and price for the affiliate categories
 * 
 * @author rsamuel3
 *
 */
public class AffiliateOrderEntryData {
    private BigDecimal price;

    private long quantity;

    public AffiliateOrderEntryData() {
        price = new BigDecimal(0);
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @param pricetoAdd
     *            the price to add
     */
    public void addPrice(final BigDecimal pricetoAdd) {
        this.price = this.price.add(pricetoAdd);
    }

    /**
     * @return the quantity
     */
    public long getQuantity() {
        return quantity;
    }

    /**
     * @param quantityToAdd
     *            the quantity to add
     */
    public void addQuantity(final long quantityToAdd) {
        this.quantity += quantityToAdd;
    }

    /**
     * add the price and quantity to the already existing price and quantity
     * 
     * @param priceToAdd
     * @param quantityToAdd
     */
    public void addPriceAndQuantity(final BigDecimal priceToAdd, final Long quantityToAdd) {
        if (null != priceToAdd) {
            addPrice(priceToAdd);
        }
        if (null != quantityToAdd) {
            addQuantity(quantityToAdd.longValue());
        }
    }
}

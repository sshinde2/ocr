/**
 *
 */
package au.com.target.tgtfacades.assistedcheckout.converters.populator;

import de.hybris.platform.commercefacades.user.converters.populator.PrincipalPopulator;
import de.hybris.platform.commercefacades.user.data.PrincipalData;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.StoreEmployeeModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfacades.assistedcheckout.data.StoreEmployeeData;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;


/**
 * @author rmcalave
 *
 */
public class StoreEmployeePopulator extends PrincipalPopulator {

    private Converter<TargetPointOfServiceModel, TargetPointOfServiceData> targetPointOfServiceConverter;

    /* (non-Javadoc)
     * @see de.hybris.platform.commercefacades.user.converters.PrincipalConverter#populate(de.hybris.platform.core.model.security.PrincipalModel, de.hybris.platform.commercefacades.user.data.PrincipalData)
     */
    @Override
    public void populate(final PrincipalModel source, final PrincipalData target) {
        if (!(source instanceof StoreEmployeeModel)) {
            return;
        }

        if (!(target instanceof StoreEmployeeData)) {
            return;
        }

        final StoreEmployeeModel storeEmployeeSource = (StoreEmployeeModel)source;
        final StoreEmployeeData storeEmployeeTarget = (StoreEmployeeData)target;

        final TargetPointOfServiceData store = targetPointOfServiceConverter
                .convert(storeEmployeeSource.getStore());

        storeEmployeeTarget.setStore(store);
    }

    /**
     * @param targetPointOfServiceConverter
     *            the targetPointOfServiceConverter to set
     */
    @Required
    public void setTargetPointOfServiceConverter(
            final Converter<TargetPointOfServiceModel, TargetPointOfServiceData> targetPointOfServiceConverter) {
        this.targetPointOfServiceConverter = targetPointOfServiceConverter;
    }
}

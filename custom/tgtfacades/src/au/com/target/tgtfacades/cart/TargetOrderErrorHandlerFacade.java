/**
 * Process cary with payment services
 */
package au.com.target.tgtfacades.cart;

import au.com.target.tgtfacades.order.enums.TargetPlaceOrderResultEnum;


/**
 * @author smudumba Create ticket with CS for a cart with a payment.
 * 
 */
public interface TargetOrderErrorHandlerFacade {

    /**
     * @param code
     *            code
     * 
     */
    void sendTicketToCS(String code);

    /**
     * @param placeOrderResult
     *            result
     * @param orderCode
     *            Code
     * 
     */
    void handleOrderCreationFailed(final TargetPlaceOrderResultEnum placeOrderResult, final String orderCode);

}

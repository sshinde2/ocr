/**
 * 
 */
package au.com.target.tgtfacades.cart.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;
import au.com.target.tgtfacades.cart.TargetOrderErrorHandlerFacade;
import au.com.target.tgtfacades.order.enums.TargetPlaceOrderResultEnum;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * Create ticket for CS Agents.
 * 
 */
public class TargetOrderErrorHandlerFacadeImpl implements TargetOrderErrorHandlerFacade {


    private static final Logger LOG = Logger.getLogger(TargetOrderErrorHandlerFacadeImpl.class);
    private static final String INVALID_TICKET_HEADLINE = "Duplicate Charge";
    private static final String INVALID_TICKET_SUBJECT = "Duplicate Charge for the following cart code: ";
    private static final String ERROR_MESSAGE = "Duplicate Charge. The payment was processed, but there was a problem "
            + "converting the cart to an order, for cart with code: ";
    private TargetTicketBusinessService targetTicketBusinessService;



    /* Create a ticket for CS
     */
    @Override
    public void sendTicketToCS(final String code) {

        sendTicketToCsAgent(code, INVALID_TICKET_HEADLINE, INVALID_TICKET_SUBJECT, ERROR_MESSAGE);

    }


    /**
     * Method to create ticket for CSAgetn
     * 
     * @param code
     *            cart code
     * @param headline
     *            Headline
     * @param subject
     *            Subject
     * @param errorMessage
     *            Error message
     */
    private void sendTicketToCsAgent(final String code, final String headline, final String subject,
            final String errorMessage) {

        targetTicketBusinessService.createCsAgentTicket(headline, subject + code, errorMessage + code, null);

        LOG.info("Successfully raised ticket for cs agent for cart code: " + code);

    }


    /*
     *  Method to handle order creation failed status
     */
    @Override
    public void handleOrderCreationFailed(final TargetPlaceOrderResultEnum placeOrderResult, final String orderCode) {

        // Splunk logging
        final String result = null != placeOrderResult ? placeOrderResult.toString() : "null";

        LOG.error(SplunkLogFormatter.formatOrderMessage(
                "Payment taken but Place Order failed with result: " + result,
                TgtutilityConstants.ErrorCode.ERR_PAYMENT, orderCode));

        //create ticket for CS
        sendTicketToCS(orderCode);
    }


    /**
     * @param targetTicketBusinessService
     *            the targetTicketBusinessService to set
     */
    @Required
    public void setTargetTicketBusinessService(final TargetTicketBusinessService targetTicketBusinessService) {
        this.targetTicketBusinessService = targetTicketBusinessService;
    }



}

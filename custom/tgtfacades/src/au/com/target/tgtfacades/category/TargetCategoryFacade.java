package au.com.target.tgtfacades.category;

import java.util.List;

import au.com.target.tgtfacades.category.data.TargetCategoryData;


/**
 * Facade layer service that primarily operates with categories.
 */
public interface TargetCategoryFacade {

    /**
     * Returns data for leaf categories, i.e. those without children.
     * 
     * @return the list of categories
     */
    List<TargetCategoryData> getLeafCategoryData();

    /**
     * Returns data for all categories in the system.
     * 
     * @return the list of categories
     */
    List<TargetCategoryData> getAllCategoryData();

    /**
     * Returns a list of all categories that contain products
     * 
     * @return the list of categories
     */
    List<TargetCategoryData> getCategoriesWithProducts();


    /**
     * Determines if the provided category is a root category (ie. has no supercategories).
     * 
     * @param categoryCode
     *            the category code
     * @return true if category is root category, false otherwise
     */
    boolean isRootCategory(final String categoryCode);

    /**
     * Determines if the provided category is a top level category (ie. has APO1 as super categories).
     *
     * @param categoryCode
     * @return true if category is root category, false otherwise
     */
    boolean isTopCategory(final String categoryCode);

    /**
     * Get TargetCategoryData by code
     * 
     * @param categoryCode
     * @return TargetCategoryData
     */
    TargetCategoryData getCategoryData(final String categoryCode);

}

package au.com.target.tgtfacades.category.data;

import de.hybris.platform.commercefacades.product.data.CategoryData;

import org.joda.time.DateTime;


/**
 * Target extension to default category DTO.
 *
 * @see CategoryData
 */
public class TargetCategoryData extends CategoryData {

    private DateTime lastModified;

    private boolean rewardDealCategory;

    private boolean qualifierDealCategory;

    /**
     * Returns category last modification timestamp.
     *
     * @return category last modification timestamp
     */
    public DateTime getLastModified() {
        return lastModified;
    }

    /**
     * Sets category last modification timestamp.
     *
     * @param lastModified
     *            the timestamp to set as category
     */
    public void setLastModified(final DateTime lastModified) {
        this.lastModified = lastModified;
    }

    /**
     * 
     * @return the rewardDealCategory
     */
    public boolean isRewardDealCategory() {
        return rewardDealCategory;
    }

    /**
     * @param rewardDealCategory
     *            the rewardDealCategory to set
     */
    public void setRewardDealCategory(final boolean rewardDealCategory) {
        this.rewardDealCategory = rewardDealCategory;
    }

    /**
     * @return the qualifierDealCategory
     */
    public boolean isQualifierDealCategory() {
        return qualifierDealCategory;
    }

    /**
     * @param qualifierDealCategory
     *            the qualifierDealCategory to set
     */
    public void setQualifierDealCategory(final boolean qualifierDealCategory) {
        this.qualifierDealCategory = qualifierDealCategory;
    }
}

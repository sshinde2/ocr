package au.com.target.tgtfacades.category.exceptions;

/**
 * Indicates that requested category does not appear to be a primary super category (as defined at the product level)
 */
public class NotAPrimarySuperCategoryException extends Exception {

    /**
     * Creates new not a supercategory exception.
     */
    public NotAPrimarySuperCategoryException() {
        // intentional constructor
    }

}

package au.com.target.tgtfacades.category.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.category.TargetCategoryService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.category.TargetCategoryFacade;
import au.com.target.tgtfacades.category.data.TargetCategoryData;


/**
 * Default implementation for {@link TargetCategoryFacade}.
 */
public class TargetCategoryFacadeImpl implements TargetCategoryFacade {

    private TargetCategoryService categoryService;
    private Converter<CategoryModel, CategoryData> categoryConverter;
    private CatalogVersionService catalogVersionService;

    @Override
    public List<TargetCategoryData> getLeafCategoryData() {
        return ImmutableList.copyOf(Collections2.transform(
                categoryService.getLeafCategoriesWithProducts(), new Function<CategoryModel, TargetCategoryData>() {
                    @Override
                    public TargetCategoryData apply(final CategoryModel input) {
                        return (TargetCategoryData)categoryConverter.convert(input);
                    }
                }));
    }

    @Override
    public List<TargetCategoryData> getAllCategoryData() {
        return ImmutableList.copyOf(Collections2.transform(
                categoryService.getAllCategories(), new Function<CategoryModel, TargetCategoryData>() {
                    @Override
                    public TargetCategoryData apply(final CategoryModel input) {
                        return (TargetCategoryData)categoryConverter.convert(input);
                    }
                }));
    }

    @Override
    public boolean isRootCategory(final String categoryCode) {
        final CatalogVersionModel onlineProductCatalog = catalogVersionService.getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS, TgtCoreConstants.Catalog.ONLINE_VERSION);

        final CategoryModel category = categoryService.getCategoryForCode(onlineProductCatalog, categoryCode);
        return categoryService.isRoot(category);
    }

    @Override
    public boolean isTopCategory(final String categoryCode) {
        final CatalogVersionModel onlineProductCatalog = catalogVersionService.getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS, TgtCoreConstants.Catalog.ONLINE_VERSION);
        final CategoryModel category = categoryService.getCategoryForCode(onlineProductCatalog, categoryCode);

        if (CollectionUtils.isNotEmpty(category.getSupercategories())) {
            for (final CategoryModel oneLevelUpCategoryData : category.getSupercategories()) {
                if (CollectionUtils.isEmpty(oneLevelUpCategoryData.getSupercategories())) {
                    return true;
                }
            }
        }
        return false;
    }


    @Override
    public TargetCategoryData getCategoryData(final String categoryCode) {
        final CategoryModel category = categoryService.getCategoryForCode(categoryCode);
        return (TargetCategoryData)categoryConverter.convert(category);
    }


    @Override
    public List<TargetCategoryData> getCategoriesWithProducts() {

        return ImmutableList.copyOf(Collections2.transform(
                categoryService.getAllCategoriesWithProducts(), new Function<CategoryModel, TargetCategoryData>() {
                    @Override
                    public TargetCategoryData apply(final CategoryModel input) {
                        return (TargetCategoryData)categoryConverter.convert(input);
                    }
                }));
    }

    /**
     * Sets the category service for this facade.
     * 
     * @param categoryService
     *            the category service to set
     */
    @Required
    public void setCategoryService(final TargetCategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /**
     * Sets the category converter for this facade.
     * 
     * @param categoryConverter
     *            the category converter to set
     */
    @Required
    public void setCategoryConverter(final Converter<CategoryModel, CategoryData> categoryConverter) {
        this.categoryConverter = categoryConverter;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.category.TargetCategoryFacade#getCategoryWithProducts()
     */


}

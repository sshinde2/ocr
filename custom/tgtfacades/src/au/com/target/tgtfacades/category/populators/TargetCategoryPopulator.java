package au.com.target.tgtfacades.category.populators;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.converters.Populator;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.deals.strategy.TargetDealCategoryStrategy;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import au.com.target.tgtfacades.category.data.TargetCategoryData;


/**
 * Target category populator that fills in {@link TargetCategoryData} fields.
 */
public class TargetCategoryPopulator implements Populator<CategoryModel, TargetCategoryData> {

    private TargetDealCategoryStrategy targetDealCategoryStrategy;

    @Override
    public void populate(final CategoryModel categoryModel, final TargetCategoryData targetCategoryData) {
        targetCategoryData.setLastModified(new DateTime(categoryModel.getModifiedtime()));
        if (categoryModel instanceof TargetDealCategoryModel) {
            final TargetDealCategoryModel dealCategoryModel = (TargetDealCategoryModel)categoryModel;
            if (targetDealCategoryStrategy.isQualifierDealCategory(dealCategoryModel)) {
                targetCategoryData.setQualifierDealCategory(true);
            }
            if (targetDealCategoryStrategy.isRewardDealCategory(dealCategoryModel)) {
                targetCategoryData.setRewardDealCategory(true);
            }
        }
    }

    /**
     * @param targetDealCategoryStrategy
     *            the targetDealCategoryStrategy to set
     */
    @Required
    public void setTargetDealCategoryStrategy(final TargetDealCategoryStrategy targetDealCategoryStrategy) {
        this.targetDealCategoryStrategy = targetDealCategoryStrategy;
    }

}

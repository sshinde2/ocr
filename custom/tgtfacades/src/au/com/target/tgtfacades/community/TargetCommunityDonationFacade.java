/**
 * 
 */
package au.com.target.tgtfacades.community;

import au.com.target.tgtcore.community.dto.TargetCommunityDonationRequestDto;


/**
 * @author knemalik
 * 
 */
public interface TargetCommunityDonationFacade {

    /**
     * Sends the donation form to service
     * 
     * @param targetCommunityDonationRequestDto
     *            the targetCommunityDonationRequestDto
     * 
     * @return true if the TargetCommunityDonationRequestDto is sent to web methods successfully, false otherwise
     */
    boolean sendDonationRequest(TargetCommunityDonationRequestDto targetCommunityDonationRequestDto);

}

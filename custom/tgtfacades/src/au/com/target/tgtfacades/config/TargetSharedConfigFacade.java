/**
 * 
 */
package au.com.target.tgtfacades.config;



/**
 * @author mjanarth
 *
 */
public interface TargetSharedConfigFacade {

    /**
     * Reads the config from hybris SharedConfig by code
     * 
     * @param configCode
     * @return String
     */
    String getConfigByCode(String configCode);

    /**
     * Reads the config from hybris SharedConfig by code and converts it to an int. Returns defaultValue if there is no
     * value for configCode.
     * 
     * @param configCode
     *            the config value to read
     * @param defaultValue
     *            the value to return if there is no value defined for configCode
     * @return the value at configCode, or defaultValue if there is no value defined for configCode
     */
    int getInt(final String configCode, final int defaultValue);
}

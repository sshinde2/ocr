/**
 * 
 */
package au.com.target.tgtfacades.converters.populator;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.price.TargetCommercePriceService;
import au.com.target.tgtcore.product.TargetSizeTypeService;
import au.com.target.tgtfacades.product.data.AbstractTargetProductData;
import au.com.target.tgtfacades.url.impl.TargetSizeTypeModelUrlResolver;


/**
 * @author rmcalave
 *
 */
public abstract class AbstractTargetProductPopulator<SOURCE extends ProductModel, TARGET extends AbstractTargetProductData>
        extends AbstractProductPopulator<ProductModel, AbstractTargetProductData> {

    private static final char PRODUCT_FEATURE_SEPARATOR = '|';
    private static final String PRODUCT_TYPE_BULKY2 = "bulky2";

    private TargetSizeTypeService sizeTypeService;

    private TargetSizeTypeModelUrlResolver sizeTypeModelUrlResolver;

    private TargetCommercePriceService targetCommercePriceService;

    /**
     * This method will be used to populate whether it is a gift card or not.
     * 
     * @param source
     * @param target
     */
    protected void populateIsGiftCard(final TargetProductModel source, final AbstractTargetProductData target) {
        if (null != source.getGiftCard()) {
            target.setGiftCard(true);
        }
    }

    /**
     * Populate size info
     * 
     * @param source
     *            The source model
     * @param target
     *            The target data
     */
    protected void populateSize(final ProductModel source, final AbstractTargetProductData target) {
        final AbstractTargetProductData targetData = target;
        ProductModel productModel = source;
        Collection<VariantProductModel> variants = source.getVariants();
        while (CollectionUtils.isNotEmpty(variants)) {
            productModel = variants.iterator().next();
            variants = productModel.getVariants();
        }
        if (productModel instanceof TargetSizeVariantProductModel) {
            setSize((TargetSizeVariantProductModel)productModel, targetData);
        }
    }

    /**
     * Get product features as a list. Splits featureList by PRODUCT_FEATURE_SEPARATOR.
     * 
     * @param featureList
     *            A delimited string of features
     * @return A list of product features
     * @see #PRODUCT_FEATURE_SEPARATOR
     */
    protected List<String> getProductFeatureList(final String featureList) {
        final String[] features = StringUtils.split(featureList, PRODUCT_FEATURE_SEPARATOR);

        if (ArrayUtils.isEmpty(features)) {
            return Collections.EMPTY_LIST;
        }

        final List<String> formattedFeatures = new ArrayList<String>(features.length);
        for (final String feature : features) {
            final String formattedFeature = StringUtils.trimToEmpty(feature);

            if (StringUtils.isNotBlank(formattedFeature)) {
                formattedFeatures.add(formattedFeature);
            }
        }

        return formattedFeatures;
    }

    /**
     * Populates product type information on target from source.
     * 
     * @param source
     *            The source product
     * @param target
     *            The target data
     */
    protected void populateProductTypeInformation(final TargetProductModel source,
            final AbstractTargetProductData target) {
        if (source.getProductType() != null) {
            final ProductTypeModel productType = source.getProductType();
            target.setBigAndBulky(productType.getBulky().booleanValue());

            // Bulky2 can't be sent to Target Country Stores
            target.setClickAndCollectExcludeTargetCountry(PRODUCT_TYPE_BULKY2.equalsIgnoreCase(productType.getCode()));

            target.setProductTypeCode(productType.getCode());
        }
    }

    /**
     * Populates the video information on target from source
     * 
     * @param source
     * @param target
     */
    protected void populateVideoInformation(final TargetProductModel source,
            final AbstractTargetProductData target) {
        if (CollectionUtils.isNotEmpty(source.getVideos())) {
            final MediaModel mediaModel = source.getVideos().iterator().next();
            target.setYouTubeLink(mediaModel.getURL());
        }
    }

    private void setSize(final TargetSizeVariantProductModel sizeVariantProductModel,
            final AbstractTargetProductData target) {
        SizeTypeModel sizeClassification = null;
        if (sizeVariantProductModel.getSizeClassification() != null) {
            sizeClassification = sizeVariantProductModel.getSizeClassification();
        }
        else {
            sizeClassification = sizeTypeService.getDefaultSizeType();
        }
        target.setSize(sizeVariantProductModel.getSize());
        if (sizeClassification != null) {
            target.setSizeType(sizeClassification.getCode());
            if (Boolean.TRUE.equals(sizeClassification.getIsSizeChartDisplay())) {
                target.setSizeChartURL(sizeTypeModelUrlResolver.resolve(sizeClassification));
            }
            target.setSizeChartDisplay(sizeClassification.getIsSizeChartDisplay().booleanValue());
        }
    }

    protected TargetProductModel getBaseTargetProduct(final AbstractTargetVariantProductModel targetVariantProduct) {
        final ProductModel product = targetVariantProduct.getBaseProduct();

        if (product instanceof TargetProductModel) {
            return (TargetProductModel)product;
        }
        else if (product instanceof AbstractTargetVariantProductModel) {
            return getBaseTargetProduct((AbstractTargetVariantProductModel)product);
        }
        return null;
    }

    /**
     * @param sizeTypeService
     *            the sizeTypeService to set
     */
    @Required
    public void setSizeTypeService(final TargetSizeTypeService sizeTypeService) {
        this.sizeTypeService = sizeTypeService;
    }

    /**
     * @return the sizeTypeService
     */
    protected TargetSizeTypeService getSizeTypeService() {
        return sizeTypeService;
    }

    /**
     * @param sizeTypeModelUrlResolver
     *            the sizeTypeModelUrlResolver to set
     */
    @Required
    public void setSizeTypeModelUrlResolver(final TargetSizeTypeModelUrlResolver sizeTypeModelUrlResolver) {
        this.sizeTypeModelUrlResolver = sizeTypeModelUrlResolver;
    }

    /**
     * @return the targetCommercePriceService
     */
    protected TargetCommercePriceService getTargetCommercePriceService() {
        return targetCommercePriceService;
    }

    /**
     * @param targetCommercePriceService
     *            the targetCommercePriceService to set
     */
    @Required
    public void setTargetCommercePriceService(final TargetCommercePriceService targetCommercePriceService) {
        this.targetCommercePriceService = targetCommercePriceService;
    }
}

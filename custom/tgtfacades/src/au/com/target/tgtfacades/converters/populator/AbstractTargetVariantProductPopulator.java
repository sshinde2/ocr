/**
 * 
 */
package au.com.target.tgtfacades.converters.populator;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtfacades.product.data.enums.TargetPromotionStatusEnum;


/**
 * @author asingh78
 * 
 */
public abstract class AbstractTargetVariantProductPopulator<SOURCE extends ProductModel, TARGET extends ProductData>
        extends
        AbstractProductPopulator<SOURCE, TARGET> {


    /**
     * 
     * @param sourceModel
     * @return promotionStatusEnum list
     */
    protected List<TargetPromotionStatusEnum> getPromotionStatuses(final TargetColourVariantProductModel sourceModel) {
        final List<TargetPromotionStatusEnum> promotionStatuses = new ArrayList<>();

        if (BooleanUtils.toBoolean(sourceModel.getNewLowerPriceFlag())) {
            promotionStatuses.add(TargetPromotionStatusEnum.NEW_LOWER_PRICE);
        }

        if (BooleanUtils.toBoolean(sourceModel.getOnlineExclusive())) {
            promotionStatuses.add(TargetPromotionStatusEnum.ONLINE_EXCLUSIVE);
        }

        if (BooleanUtils.toBoolean(sourceModel.getTargetExclusive())) {
            promotionStatuses.add(TargetPromotionStatusEnum.TARGET_EXCLUSIVE);
        }

        if (BooleanUtils.toBoolean(sourceModel.getClearance())) {
            promotionStatuses.add(TargetPromotionStatusEnum.CLEARANCE);
        }

        if (BooleanUtils.toBoolean(sourceModel.getHotProduct())) {
            promotionStatuses.add(TargetPromotionStatusEnum.HOT_PRODUCT);
        }

        if (BooleanUtils.toBoolean(sourceModel.getEssential())) {
            promotionStatuses.add(TargetPromotionStatusEnum.ESSENTIALS);
        }

        return promotionStatuses;
    }

}

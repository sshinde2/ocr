/**
 * 
 */
package au.com.target.tgtfacades.converters.populator;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Set;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.product.data.AbstractTargetProductData;


/**
 * @author rmcalave
 *
 */
public class TargetBulkyProductDetailPopulator<SOURCE extends ProductModel, TARGET extends AbstractTargetProductData>
        implements Populator<SOURCE, TARGET> {

    private TargetDeliveryModeHelper targetDeliveryModeHelper;
    private TargetCheckoutFacade targetCheckoutFacade;

    /* (non-Javadoc)
     * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final SOURCE source, final TARGET target)
            throws ConversionException {
        if (!target.isBigAndBulky()) {
            return;
        }

        final Set<DeliveryModeModel> deliveryModes =
                targetDeliveryModeHelper.getDeliveryModesForProduct(source);

        PriceData bulkyHomeDeliveryFee = null;
        PriceData bulkyCncFreeThreshold = null;
        for (final DeliveryModeModel deliveryModeModel : deliveryModes) {
            if (deliveryModeModel instanceof TargetZoneDeliveryModeModel) {
                final TargetZoneDeliveryModeModel targetDeliveryModeModel = (TargetZoneDeliveryModeModel)deliveryModeModel;
                if (BooleanUtils.isTrue(targetDeliveryModeModel.getIsDeliveryToStore())) {
                    if (bulkyCncFreeThreshold != null) {
                        // already have this one
                        continue;
                    }
                    bulkyCncFreeThreshold = targetCheckoutFacade
                            .getLastThreshold(targetDeliveryModeModel, source);
                }
                else if (BooleanUtils.isTrue(targetDeliveryModeModel.getDefault())) {
                    if (bulkyHomeDeliveryFee != null) {
                        // already have this one
                        continue;
                    }
                    bulkyHomeDeliveryFee = targetCheckoutFacade.getFirstPrice(targetDeliveryModeModel, source);
                }
            }
            if (bulkyHomeDeliveryFee != null && bulkyCncFreeThreshold != null) {
                break;
            }
        }

        if (bulkyHomeDeliveryFee != null) {
            target.setBulkyHomeDeliveryFee(bulkyHomeDeliveryFee);
        }

        if (bulkyCncFreeThreshold != null) {
            target.setBulkyCncFreeThreshold(bulkyCncFreeThreshold);
        }
    }

    /**
     * @param targetDeliveryModeHelper
     *            the targetDeliveryModeHelper to set
     */
    @Required
    public void setTargetDeliveryModeHelper(final TargetDeliveryModeHelper targetDeliveryModeHelper) {
        this.targetDeliveryModeHelper = targetDeliveryModeHelper;
    }

    /**
     * @param targetCheckoutFacade
     *            the targetCheckoutFacade to set
     */
    @Required
    public void setTargetCheckoutFacade(final TargetCheckoutFacade targetCheckoutFacade) {
        this.targetCheckoutFacade = targetCheckoutFacade;
    }
}

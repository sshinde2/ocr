/**
 * 
 */
package au.com.target.tgtfacades.converters.populator;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeValueModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtfacades.converters.util.TargetCategoryUtil;
import au.com.target.tgtfacades.product.data.AbstractTargetProductData;
import au.com.target.tgtfacades.util.TargetPriceHelper;
import au.com.target.tgtutility.util.TargetDateUtil;


/**
 * @author ajit
 *
 */
public class TargetDeliveryPromotionalMessagePopulator extends
        AbstractProductPopulator<ProductModel, AbstractTargetProductData> {

    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    private TargetDeliveryService targetDeliveryService;
    @Resource(name = "targetPriceHelper")
    private TargetPriceHelper targetPriceHelper;

    /**
     * (non-Javadoc)
     * 
     * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final ProductModel source, final AbstractTargetProductData targetData)
            throws ConversionException {

        final Map<Integer, RestrictableTargetZoneDeliveryModeValueModel> promotionDeliveryModeWithPriority = new TreeMap();
        RestrictableTargetZoneDeliveryModeValueModel restrictableTargetModel;

        final Collection<TargetZoneDeliveryModeModel> listTargetZoneDeliveryModeModel = targetDeliveryModeHelper
                .getAllCurrentActiveDeliveryModesForProduct(source);
        for (final TargetZoneDeliveryModeModel targetZoneDeliveryModeModel : listTargetZoneDeliveryModeModel) {
            restrictableTargetModel = getHighestPriorityRTZDMVM(targetDeliveryService
                    .getAllApplicableDeliveryValuesForModeAndProduct(targetZoneDeliveryModeModel, source));
            if (targetZoneDeliveryModeModel.getDeliveryPromotionRank() != null && restrictableTargetModel != null) {
                promotionDeliveryModeWithPriority.put(targetZoneDeliveryModeModel.getDeliveryPromotionRank(),
                        restrictableTargetModel);
            }
        }

        setPromotionalDeliveryMessage(targetData, promotionDeliveryModeWithPriority);
        setNewStickerTag(source, targetData);
    }

    /**
     * @param source
     * @param targetData
     */
    protected void setNewStickerTag(final ProductModel source, final AbstractTargetProductData targetData) {
        final TargetProductCategoryModel topLevelCategory = getTopLevelCategoryForProduct(source);
        final Date onlineDate = source.getOnlineDate() != null ? source.getOnlineDate() : source.getCreationtime();
        if (null != onlineDate && null != topLevelCategory) {
            if (null != topLevelCategory.getNewToOnlineConfigDays()) {
                final int diff = TargetDateUtil.getDifferenceBetweenDateInDay(new Date(), onlineDate);
                if (topLevelCategory.getNewToOnlineConfigDays().intValue() >= diff) {
                    targetData.setNewArrived(true);
                }
            }
            else {
                final List<CategoryModel> superCategoryModel = topLevelCategory.getSupercategories();
                for (final CategoryModel categoryModel : superCategoryModel) {
                    if (categoryModel instanceof TargetProductCategoryModel) {
                        final TargetProductCategoryModel targetProductModel = (TargetProductCategoryModel)categoryModel;
                        if (null != targetProductModel.getNewToOnlineConfigDays()
                                && TgtCoreConstants.Category.ALL_PRODUCTS
                                        .equalsIgnoreCase(targetProductModel.getCode())) {
                            final int diff = TargetDateUtil.getDifferenceBetweenDateInDay(new Date(), onlineDate);
                            if (targetProductModel.getNewToOnlineConfigDays().intValue() >= diff) {
                                targetData.setNewArrived(true);
                            }
                        }
                    }
                }
            }
        }
    }

    protected TargetProductCategoryModel getTopLevelCategoryForProduct(final ProductModel source) {
        return TargetCategoryUtil.getTopLevelCategory(source);
    }

    /**
     * @param targetData
     * @param promotionDeliveryModeWithPriority
     */
    private void setPromotionalDeliveryMessage(final AbstractTargetProductData targetData,
            final Map<Integer, RestrictableTargetZoneDeliveryModeValueModel> promotionDeliveryModeWithPriority) {

        String giftCardDeliveryFee = null;
        final StringBuilder stringBuilder = new StringBuilder(StringUtils.EMPTY);
        RestrictableTargetZoneDeliveryModeValueModel restrictableTargetModel;
        for (final Map.Entry<Integer, RestrictableTargetZoneDeliveryModeValueModel> entry : promotionDeliveryModeWithPriority
                .entrySet()) {
            restrictableTargetModel = entry.getValue();
            final String deliveryPromotionMessage = restrictableTargetModel.getDeliveryPromotionMessage();
            if (StringUtils.isNotBlank(restrictableTargetModel.getPromotionalStickerMessage())
                    && StringUtils.isEmpty(targetData.getProductPromotionalDeliverySticker())) {
                targetData.setProductPromotionalDeliverySticker(restrictableTargetModel.getPromotionalStickerMessage());
            }
            if (StringUtils.isNotBlank(stringBuilder.toString())
                    && StringUtils.isNotBlank(deliveryPromotionMessage)) {
                stringBuilder.append(" or ");
            }
            if (StringUtils.isNotBlank(deliveryPromotionMessage)) {
                stringBuilder.append(deliveryPromotionMessage);
            }
            if (TgtCoreConstants.PHYSICAL_GIFTCARD.equals(targetData.getProductTypeCode())
                    && null == giftCardDeliveryFee) {
                giftCardDeliveryFee = getFormattedGiftCardDeliveryFee(restrictableTargetModel.getValue());
                targetData.setGiftCardDeliveryFee(giftCardDeliveryFee);

            }
        }

        targetData.setProductPromotionalDeliveryText(stringBuilder.toString());
    }

    /**
     * @param giftCardFee
     * @return formatted giftCardDeliveryFee
     */
    private String getFormattedGiftCardDeliveryFee(final Double giftCardFee) {
        final PriceData giftCardPriceData = targetPriceHelper.createPriceData(giftCardFee);
        return giftCardPriceData.getFormattedValue();
    }

    private RestrictableTargetZoneDeliveryModeValueModel getHighestPriorityRTZDMVM(
            final List<AbstractTargetZoneDeliveryModeValueModel> zoneDeliveryModeValueModels) {
        RestrictableTargetZoneDeliveryModeValueModel restrictableTargetModel = null;
        for (final ZoneDeliveryModeValueModel zoneDeliveryModeValueModel : zoneDeliveryModeValueModels) {
            if (zoneDeliveryModeValueModel instanceof RestrictableTargetZoneDeliveryModeValueModel) {
                if (restrictableTargetModel == null) {
                    restrictableTargetModel = (RestrictableTargetZoneDeliveryModeValueModel)zoneDeliveryModeValueModel;
                }
                else if (((RestrictableTargetZoneDeliveryModeValueModel)zoneDeliveryModeValueModel).getPriority()
                        .intValue() > restrictableTargetModel
                                .getPriority().intValue()) {
                    restrictableTargetModel = (RestrictableTargetZoneDeliveryModeValueModel)zoneDeliveryModeValueModel;
                }
            }
        }
        return restrictableTargetModel;
    }

    /**
     * @param targetDeliveryModeHelper
     *            the targetDeliveryModeHelper to set
     */
    @Required
    public void setTargetDeliveryModeHelper(final TargetDeliveryModeHelper targetDeliveryModeHelper) {
        this.targetDeliveryModeHelper = targetDeliveryModeHelper;
    }

    /**
     * @param targetDeliveryService
     *            the targetDeliveryService to set
     */
    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        this.targetDeliveryService = targetDeliveryService;
    }

}

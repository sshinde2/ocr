package au.com.target.tgtfacades.converters.populator;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERec;
import com.endeca.navigation.PropertyMap;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.querybuilder.EndecaProductQueryBuilder;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.product.data.AbstractTargetProductData;


/**
 * @author htan3
 * 
 */
public class TargetProductConsolidatedStoreStockPopulator<S extends ProductModel, T extends AbstractTargetProductData>
        extends
        AbstractProductPopulator<S, T> {
    private static final int MAX_RESULT_CNT = 50;
    private EndecaProductQueryBuilder endecaProductQueryBuilder;
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;


    @Override
    public void populate(final ProductModel source, final AbstractTargetProductData target) throws ConversionException {
        final boolean isColourVariant = source instanceof TargetColourVariantProductModel;
        if (targetFeatureSwitchFacade.isConsolidatedStoreStockAvailable()) {
            final List<ERec> endecaProductList = searchEndeca(source);
            final String productCode = source.getCode();
            populateStockData(target, isColourVariant, endecaProductList, productCode);
        }
    }

    /**
     * populates the stock data into the target object
     * 
     * @param target
     * @param isColourVariant
     * @param endecaProductList
     * @param productCode
     */
    private void populateStockData(final AbstractTargetProductData target, final boolean isColourVariant,
            final List<ERec> endecaProductList, final String productCode) {
        for (final ERec eRec : endecaProductList) {
            final PropertyMap properties = eRec.getProperties();
            final String sizeVariantCode = (String)properties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE);
            if (isColourVariant || productCode.equals(sizeVariantCode)) {
                final boolean isInStock = populateStoreStockData(target, properties);
                if (isInStock) {
                    break;
                }
            }
        }
    }

    protected List<ERec> searchEndeca(final ProductModel source) {
        final EndecaSearchStateData searchStateData = populateEndecaSearchStateData(source);
        final ENEQueryResults eNEQueryResults = endecaProductQueryBuilder.getQueryResults(searchStateData,
                null, MAX_RESULT_CNT);
        if (eNEQueryResults == null || eNEQueryResults.getNavigation() == null) {
            return Collections.emptyList();
        }
        return eNEQueryResults.getNavigation().getERecs();
    }

    protected EndecaSearchStateData populateEndecaSearchStateData(final ProductModel productModel) {
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setSkipInStockFilter(false);
        if (targetFeatureSwitchFacade.isFindInStoreStockVisibilityEnabled()) {
            endecaSearchStateData.setSkipInStockFilter(true);
        }
        endecaSearchStateData.setSkipDefaultSort(true);
        final List<String> productCodes = new ArrayList<>();
        if (productModel instanceof TargetSizeVariantProductModel) {
            //use colourVariantCode to search Endeca if the productModel is a size variant
            productCodes.add(((TargetSizeVariantProductModel)productModel).getBaseProduct().getCode());
        }
        else {
            //for colour variant or base product use the product directly
            productCodes.add(productModel.getCode());
        }
        endecaSearchStateData.setMatchMode(EndecaConstants.MATCH_MODE_ANY);
        endecaSearchStateData.setNpSearchState(EndecaConstants.ENDECA_N_0);
        endecaSearchStateData.setSearchField(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING);
        endecaSearchStateData.setSearchTerm(productCodes);
        endecaSearchStateData.setNuRollupField(null);
        endecaSearchStateData.setFieldListConfig(EndecaConstants.FieldList.STORESTOCK_FIELDLIST);
        return endecaSearchStateData;
    }

    private boolean populateStoreStockData(final AbstractTargetProductData target,
            final PropertyMap endecaRecordProperties) {
        if (target.getConsolidatedStoreStock() == null) {
            final StockData stockData = new StockData();
            stockData.setStockLevel(Long.valueOf(0L));
            stockData.setStockLevelStatus(StockLevelStatus.OUTOFSTOCK);
            target.setConsolidatedStoreStock(stockData);
        }
        final StockData consolidatedStoreStock = target.getConsolidatedStoreStock();

        String availQtyInStore = (String)endecaRecordProperties
                .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY_IN_STORE);
        if (targetFeatureSwitchFacade.isFindInStoreStockVisibilityEnabled()) {
            availQtyInStore = (String)endecaRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_ACTUAL_CONSOLIDATED_STORE_STOCK);
        }
        if (StringUtils.isNotEmpty(availQtyInStore)) {
            final Long stockLevel = Long.valueOf(availQtyInStore);
            if (stockLevel.longValue() > 0L) {
                consolidatedStoreStock.setStockLevelStatus(StockLevelStatus.INSTOCK);
                consolidatedStoreStock.setStockLevel(stockLevel);
                return true;
            }
        }
        return false;
    }

    @Required
    public void setEndecaProductQueryBuilder(final EndecaProductQueryBuilder endecaProductQueryBuilder) {
        this.endecaProductQueryBuilder = endecaProductQueryBuilder;
    }

    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }

}

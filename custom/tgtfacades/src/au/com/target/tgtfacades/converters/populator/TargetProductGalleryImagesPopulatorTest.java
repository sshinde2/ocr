/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package au.com.target.tgtfacades.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static au.com.target.tgtfacades.constants.TgtFacadesConstants.IMAGE_WIDE_FORMAT;

import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.product.converters.TargetProductGalleryImagesPopulator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.ImageFormatMapping;
import de.hybris.platform.commercefacades.product.converters.populator.ProductGalleryImagesPopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.media.MediaContainerService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Test suite for {@link ProductGalleryImagesPopulator}
 */
@UnitTest
public class TargetProductGalleryImagesPopulatorTest
{
	private static final String IMAGE_FULL_FORMAT = "full";
	private static final String IMAGE_GRID_FORMAT = "grid";
	private static final String MEDIA_FORMAT_QUALIFIER_FULL = "1100x1100";
	private static final String MEDIA_FORMAT_QUALIFIER_WIDE = "2600x1100";

	@Mock
	private MediaService mediaService;
	@Mock
	private MediaContainerService mediaContainerService;
	@Mock
	private ImageFormatMapping imageFormatMapping;
	@Mock
	private Converter<MediaModel, ImageData> imageConverter;
	@Mock
	private ModelService modelService;

	private TargetProductGalleryImagesPopulator<ProductModel, ProductData> productGalleryImagesPopulator;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		List<String> imageFormats = new ArrayList<>();
		imageFormats.add(IMAGE_FULL_FORMAT);
		imageFormats.add(IMAGE_WIDE_FORMAT);
		productGalleryImagesPopulator = new TargetProductGalleryImagesPopulator<>();
		productGalleryImagesPopulator.setModelService(modelService);
		productGalleryImagesPopulator.setImageConverter(imageConverter);
		productGalleryImagesPopulator.setImageFormatMapping(imageFormatMapping);
		productGalleryImagesPopulator.setImageFormats(imageFormats);
		productGalleryImagesPopulator.setMediaContainerService(mediaContainerService);
		productGalleryImagesPopulator.setMediaService(mediaService);
	}


	@Test
	public void testPopulate()
	{
		final ProductModel source = mock(TargetProductModel.class);
		final MediaContainerModel mediaContainerModel = mock(MediaContainerModel.class);
		final MediaFormatModel fullFormat = mock(MediaFormatModel.class);
		final MediaFormatModel wideFormat = mock(MediaFormatModel.class);
		final MediaModel fullMedia = mock(MediaModel.class);
		final MediaModel wideMedia = mock(MediaModel.class);
		final ImageData fullImage =new ImageData(),wideImage =new ImageData(),existingImage =new ImageData();

		fullImage.setFormat(IMAGE_FULL_FORMAT);
		wideImage.setFormat(IMAGE_WIDE_FORMAT);
		existingImage.setFormat(IMAGE_GRID_FORMAT);

		given(mediaService.getFormat(MEDIA_FORMAT_QUALIFIER_FULL)).willReturn(fullFormat);
		given(mediaService.getFormat(MEDIA_FORMAT_QUALIFIER_WIDE)).willReturn(wideFormat);
		given(mediaContainerService.getMediaForFormat(mediaContainerModel, fullFormat)).willReturn(fullMedia);
		given(mediaContainerService.getMediaForFormat(mediaContainerModel, wideFormat)).willReturn(wideMedia);
		given(imageConverter.convert(fullMedia)).willReturn(fullImage);
		given(imageConverter.convert(wideMedia)).willReturn(wideImage);
		given(imageFormatMapping.getMediaFormatQualifierForImageFormat(IMAGE_FULL_FORMAT)).willReturn(MEDIA_FORMAT_QUALIFIER_FULL);
		given(imageFormatMapping.getMediaFormatQualifierForImageFormat(IMAGE_WIDE_FORMAT)).willReturn(MEDIA_FORMAT_QUALIFIER_WIDE);
		given(modelService.getAttributeValue(source, ProductModel.GALLERYIMAGES)).willReturn(
				Collections.singletonList(mediaContainerModel));

		final ProductData result = new ProductData();
		final List<ImageData> images = new ArrayList<>();
		images.add(existingImage);
		result.setImages(images);
		productGalleryImagesPopulator.populate(source, result);

		assertThat(2).isEqualTo(result.getImages().size());
		assertThat(1).isEqualTo(result.getWideImages().size());

		assertThat(result.getImages()).contains(fullImage);
		assertThat(result.getWideImages()).contains(wideImage);
	}




}

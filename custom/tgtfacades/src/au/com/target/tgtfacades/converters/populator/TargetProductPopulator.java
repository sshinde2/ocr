/**
 *
 */
package au.com.target.tgtfacades.converters.populator;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.url.impl.DefaultProductDataUrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.ColourModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.order.TargetPreOrderService;
import au.com.target.tgtfacades.converters.util.TargetConvertersUtil;
import au.com.target.tgtfacades.data.CanonicalData;
import au.com.target.tgtfacades.product.data.AbstractTargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtutility.util.TargetDateUtil;
import au.com.target.tgtutility.util.TargetProductUtils;


/**
 * @author rmcalave
 *
 */
public class TargetProductPopulator extends AbstractTargetProductPopulator<ProductModel, TargetProductData> {
    private static final String HERO_MEDIA = "product";
    private DefaultProductDataUrlResolver productDataUrlResolver;

    private TargetPreOrderService targetPreOrderService;

    @Override
    public void populate(final ProductModel source, final AbstractTargetProductData target) throws ConversionException {
        if (!(target instanceof TargetProductData)) {
            return;
        }

        final TargetProductData targetProductData = (TargetProductData)target;

        TargetProductModel sourceModel = null;
        AbstractTargetVariantProductModel targetVariantModel = null;

        if (source instanceof TargetProductModel) {
            sourceModel = (TargetProductModel)source;
        }
        else if (source instanceof AbstractTargetVariantProductModel) {
            targetVariantModel = (AbstractTargetVariantProductModel)source;
            sourceModel = getBaseTargetProduct(targetVariantModel);

            final TargetColourVariantProductModel targetColourVariantProductModel = getTargetColourVariantProductModel(
                    targetVariantModel);
            if (targetColourVariantProductModel != null) {
                targetProductData.setCanonical(createCanonicalData(targetColourVariantProductModel));
                setPreOrderDetails(targetColourVariantProductModel, targetProductData);
                final ColourModel swatch = targetColourVariantProductModel.getSwatch();
                if (swatch != null && BooleanUtils.isTrue(swatch.getDisplay())) {
                    targetProductData.setSwatch(targetColourVariantProductModel.getSwatchName());
                }

                targetProductData.setAssorted(targetColourVariantProductModel.isAssorted());
                targetProductData.setExcludeForAfterpay(targetColourVariantProductModel.isExcludeForAfterpay());
                targetProductData.setExcludeForZipPayment(targetColourVariantProductModel.isExcludeForZipPayment());
            }
            targetProductData.setName(targetVariantModel.getDisplayName()); // override with display name
        }

        if (sourceModel == null) {
            return;
        }

        populateTargetProductData(sourceModel, targetProductData);
        populateProductTypeInformation(sourceModel, targetProductData);
        populateSize(source, targetProductData);
        setImageUrl(targetProductData);
    }

    protected void setImageUrl(final TargetProductData targetProductData) {
        final Collection<ImageData> images = targetProductData.getImages();
        if (CollectionUtils.isNotEmpty(images)) {
            for (final ImageData imageData : images) {
                if (imageData.getFormat().equalsIgnoreCase(HERO_MEDIA)) {
                    targetProductData.setImageUrl(imageData.getUrl());
                    break;
                }
            }
        }
    }

    private void populateTargetProductData(final TargetProductModel source, final TargetProductData target) {
        target.setCareInstructions(getProductFeatureList(source.getCareInstructions()));
        target.setProductFeatures(getProductFeatureList(source.getProductFeatures()));
        target.setMaterials(getProductFeatureList(source.getMaterials()));
        target.setExtraInfo(source.getExtraInfo());

        final BrandModel brand = source.getBrand();
        if (brand != null) {
            target.setBrand(brand.getName());
        }

        populateIsGiftCard(source, target);

        final ProductTypeModel productType = source.getProductType();
        if (productType != null) {
            target.setProductType(productType.getCode());
        }
        final TargetProductCategoryModel category = source.getOriginalCategory();
        if (category != null) {
            target.setOriginalCategoryCode(category.getCode());
        }

        populateVideoInformation(source, target);
        target.setTopLevelCategory(getTopLevelCategory(source));
        target.setHasSizeChart(isChart(source));
        target.setLastModified(new DateTime(source.getModifiedtime()));
        target.setBaseProductCode(source.getCode());
        final CanonicalData canonicalData = createCanonicalData(source);
        if (canonicalData != null) {
            target.setBaseProductCanonical(canonicalData);
            target.setBaseName(StringEscapeUtils.unescapeHtml(canonicalData.getName()));
        }
        target.setShowWhenOutOfStock(BooleanUtils.isTrue(source.getShowWhenOutOfStock()));
        target.setBulkyBoardProduct(BooleanUtils.isTrue(source.getBulkyBoardProduct()));
        target.setPreview(BooleanUtils.isTrue(source.getPreview()));
    }

    /**
     * This method will include the Normal Sales Date and Product Display type for pre-order that is needed for frontend
     * order details summary in my account
     * 
     * @param targetColourVariantProductModel
     * @param targetProductData
     */
    private void setPreOrderDetails(final TargetColourVariantProductModel targetColourVariantProductModel,
            final TargetProductData targetProductData) {

        targetProductData.setNormalSaleStartDate(
                TargetDateUtil.getDateAsString(
                        TargetProductUtils.getNormalSaleStartDateTime(targetColourVariantProductModel), "d MMMM"));
        targetProductData.setProductDisplayType(
                targetPreOrderService.getProductDisplayType(targetColourVariantProductModel));
    }

    /**
     * Get the colour variants product
     *
     * @param targetVariantModel
     * @return TargetColourVariantProductModel
     */
    private TargetColourVariantProductModel getTargetColourVariantProductModel(
            final AbstractTargetVariantProductModel targetVariantModel) {
        if (targetVariantModel instanceof TargetColourVariantProductModel) {
            return (TargetColourVariantProductModel)targetVariantModel;
        }
        final ProductModel product = targetVariantModel.getBaseProduct();
        if (product != null && product instanceof AbstractTargetVariantProductModel) {
            return getTargetColourVariantProductModel((AbstractTargetVariantProductModel)product);
        }
        return null;
    }

    private String getTopLevelCategory(final TargetProductModel productModel) {
        final TargetProductModel baseProductModel = (TargetProductModel)TargetConvertersUtil
                .getBaseProduct(productModel);
        TargetProductCategoryModel category = baseProductModel.getPrimarySuperCategory();
        while (category != null) {
            final List<CategoryModel> categoryModelList = category.getSupercategories();
            if (CollectionUtils.isNotEmpty(categoryModelList)) {
                for (final CategoryModel model : categoryModelList) {
                    if ((model instanceof TargetProductCategoryModel)) {
                        if (CollectionUtils.isEmpty(model.getSupercategories())) {
                            return category.getName();
                        }
                        category = (TargetProductCategoryModel)model;
                    }
                }
            }
            else {
                return category.getName();
            }
        }
        return null;
    }

    /**
     * @param productModel
     */
    private CanonicalData createCanonicalData(final ProductModel productModel) {
        final CanonicalData canonicalData = new CanonicalData();
        canonicalData.setCode(productModel.getCode());
        canonicalData.setName(productModel.getName());
        final ProductData productData = new ProductData();
        productData.setCode(productModel.getCode());
        try {
            canonicalData.setUrl(productDataUrlResolver.resolve(productData));
        }
        catch (final UnknownIdentifierException ex) {
            canonicalData.setUrl(null);
        }

        return canonicalData;
    }

    /**
     *
     * @param productModel
     *            productModel
     * @return true if the product is size chart
     */
    private boolean isChart(final ProductModel productModel) {

        if (productModel instanceof TargetSizeVariantProductModel) {
            return getSizeChartDisplay(productModel);
        }
        else if (CollectionUtils.isNotEmpty(productModel.getVariants())) {
            final VariantProductModel variantProductModel = productModel.getVariants().iterator().next();
            for (final VariantProductModel variants : variantProductModel.getVariants()) {
                return getSizeChartDisplay(variants);
            }
        }
        return false;
    }

    /**
     *
     * @param productModel
     *            productModel
     * @return IsSizeChartDisplay from SizeType
     */
    private boolean getSizeChartDisplay(final ProductModel productModel) {
        if (productModel instanceof TargetSizeVariantProductModel) {
            final TargetSizeVariantProductModel sizeVariantProductModel = (TargetSizeVariantProductModel)productModel;
            if (null != sizeVariantProductModel.getSizeClassification()) {
                return BooleanUtils.isTrue(sizeVariantProductModel
                        .getSizeClassification().getIsSizeChartDisplay());
            }
            else {
                final SizeTypeModel dafaultTypeModel = getSizeTypeService().getDefaultSizeType();
                if (null != dafaultTypeModel) {
                    return BooleanUtils.isTrue(dafaultTypeModel.getIsSizeChartDisplay());
                }
            }
        }
        return false;
    }

    /**
     * @param productDataUrlResolver
     *            the productDataUrlResolver to set
     */
    @Required
    public void setProductDataUrlResolver(final DefaultProductDataUrlResolver productDataUrlResolver) {
        this.productDataUrlResolver = productDataUrlResolver;
    }

    /**
     * @param targetPreOrderService
     *            the targetPreOrderService to set
     */
    @Required
    public void setTargetPreOrderService(final TargetPreOrderService targetPreOrderService) {
        this.targetPreOrderService = targetPreOrderService;
    }


}
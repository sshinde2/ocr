/**
 *
 */
package au.com.target.tgtfacades.converters.populator;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.price.TargetCommercePriceService;
import au.com.target.tgtcore.price.data.PriceRangeInformation;
import au.com.target.tgtfacades.product.data.AbstractTargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.util.TargetPriceHelper;
import au.com.target.tgtfacades.util.TargetPriceInformation;


/**
 * @author Benoit Vanalderweireldt
 *
 */
public class TargetProductPricePopulator extends
        AbstractProductPopulator<ProductModel, AbstractTargetProductData> {

    private TargetPriceHelper targetPriceHelper;

    private TargetCommercePriceService targetCommercePriceService;

    @Override
    public void populate(final ProductModel productModel, final AbstractTargetProductData productData)
            throws ConversionException {

        populatePricesFromRangeInformation(productData, getTargetCommercePriceService().getPriceRangeInfoForProduct(
                productModel));
        populateAfterPayPrice(productData);
    }

    /**
     * Populate prices from range information.
     *
     * @param productData
     *            the product data
     * @param priceRange
     *            the price range
     */
    public void populatePricesFromRangeInformation(final AbstractTargetProductData productData,
            final PriceRangeInformation priceRange) {
        validateParameterNotNull(priceRange, "PriceRange model cannot be null");
        validateParameterNotNull(productData, "productData model cannot be null");

        final TargetPriceInformation targetPriceInformation = targetPriceHelper
                .populatePricesFromRangeInformation(priceRange);

        productData.setPriceRange(targetPriceInformation.getPriceRangeData());
        productData.setPrice(targetPriceInformation.getPriceData());
        if (targetPriceInformation.isMissingPrice()) {
            if (productData instanceof TargetProductData) {
                //If their is no price at all we desactivate all purchase option
                ((TargetProductData)productData).setAvailable(Collections.EMPTY_MAP);
                ((TargetProductData)productData).setInStock(false);
            }
            else if (productData instanceof TargetProductListerData) {
                ((TargetProductListerData)productData).setMissingPrice(true);
            }
        }
        productData.setShowWasPrice(targetPriceInformation.isShowWasPrice());
        productData.setWasPrice(targetPriceInformation.getWasPriceData());
        productData.setWasPriceRange(targetPriceInformation.getWasPriceRangeData());
    }

    /**
     * @param productData
     */
    protected void populateAfterPayPrice(final AbstractTargetProductData productData) {
        if (productData.getPrice() != null) {
            productData
                    .setInstallmentPrice(targetPriceHelper.calculateAfterpayInstallmentPrice(productData.getPrice()));
        }
    }

    /**
     * @return the targetCommercePriceService
     */
    protected TargetCommercePriceService getTargetCommercePriceService() {
        return targetCommercePriceService;
    }

    /**
     * @param targetCommercePriceService
     *            the targetCommercePriceService to set
     */
    @Required
    public void setTargetCommercePriceService(final TargetCommercePriceService targetCommercePriceService) {
        this.targetCommercePriceService = targetCommercePriceService;
    }

    /**
     * @param targetPriceHelper
     *            the targetPriceHelper to set
     */
    @Required
    public void setTargetPriceHelper(final TargetPriceHelper targetPriceHelper) {
        this.targetPriceHelper = targetPriceHelper;
    }

}

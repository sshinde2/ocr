package au.com.target.tgtfacades.converters.populator;

import de.hybris.platform.commercefacades.product.converters.populator.ProductReviewsPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantProductModel;


/**
 * 
 */
public class TargetProductReviewsPopulator extends ProductReviewsPopulator {

    @Override
    public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException {

        if (productModel == null || productData == null) {
            return;
        }

        if (productModel instanceof VariantProductModel) {
            final VariantProductModel variantProduct = (VariantProductModel)productModel;
            populate(variantProduct.getBaseProduct(), productData);
        }
        else {
            super.populate(productModel, productData);
            productData.setAverageRating(productModel.getAverageRating());
        }
    }

}

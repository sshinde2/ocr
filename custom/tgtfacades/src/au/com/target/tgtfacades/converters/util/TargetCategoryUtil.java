/**
 * 
 */
package au.com.target.tgtfacades.converters.util;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * @author ajit
 *
 */
public class TargetCategoryUtil {

    private TargetCategoryUtil() {
        // Private Constructor
    }

    public static TargetProductCategoryModel getTopLevelCategory(final ProductModel productModel) {
        final ProductModel baseProductModel = TargetConvertersUtil
                .getBaseProduct(productModel);
        if (baseProductModel instanceof TargetProductModel) {
            TargetProductCategoryModel category = ((TargetProductModel)baseProductModel).getPrimarySuperCategory();
            while (category != null) {
                final List<CategoryModel> categoryModelList = category.getSupercategories();
                if (CollectionUtils.isNotEmpty(categoryModelList)) {
                    for (final CategoryModel model : categoryModelList) {
                        if ((model instanceof TargetProductCategoryModel)) {
                            if (CollectionUtils.isEmpty(model.getSupercategories())) {
                                return category;
                            }
                            category = (TargetProductCategoryModel)model;
                        }
                    }
                }
                else {
                    return category;
                }
            }
        }
        return null;
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.converters.util;

import de.hybris.platform.core.model.product.ProductModel;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;


/**
 * @author rsamuel3
 *
 */
public class TargetConvertersUtil {

    private TargetConvertersUtil() {
        //not to be instantiated
    }

    /**
     * retrieves the base product for this product passed in
     * 
     * @param product
     * @return BaseProduct
     */
    public static ProductModel getBaseProduct(final ProductModel product) {
        ProductModel baseProduct = product;

        while (baseProduct instanceof AbstractTargetVariantProductModel) {
            baseProduct = ((AbstractTargetVariantProductModel)baseProduct).getBaseProduct();
        }

        return baseProduct;
    }
}

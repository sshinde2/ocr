/**
 * 
 */
package au.com.target.tgtfacades.customer;

import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionResponseDto;



/**
 * @author rmcalave
 *
 */
public interface TargetCustomerSubscriptionFacade {

    /**
     * Update the customer's details (or add if not already present) in the subscription database.
     * 
     * 1) first it will try to call the service to create the record directly via webmethod.
     * 
     * 2) if failed, it will create a business process to retry the creation.
     * 
     * @param customerSubscriptionDto
     *            customer information
     * @return true if success, false otherwise
     */
    TargetCustomerSubscriptionResponseDto performCustomerSubscription(
            TargetCustomerSubscriptionRequestDto customerSubscriptionDto);

    /**
     * Update the customer personal details by triggering the business process
     * 
     * @param customerSubscriptionDto
     * @return boolean
     */
    boolean processCustomerPersonalDetails(
            TargetCustomerSubscriptionRequestDto customerSubscriptionDto);

}
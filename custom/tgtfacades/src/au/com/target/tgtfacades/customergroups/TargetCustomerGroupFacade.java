/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package au.com.target.tgtfacades.customergroups;

import de.hybris.platform.commercefacades.customergroups.CustomerGroupFacade;
import de.hybris.platform.core.model.user.UserModel;


/**
 * Facade for management of customer groups
 * 
 */
public interface TargetCustomerGroupFacade extends CustomerGroupFacade {

    /**
     * This method will check whether the user is a member of a group directly or even by inheritance. In either case
     * will return positive.
     * 
     * @param uid
     * @return boolean
     */
    boolean isUserMemberOfTheGroup(String uid);

    /**
     * Add customer to the group
     * 
     * @param userGroup
     * @param user
     */
    void addUserToCustomerGroup(final String userGroup, final UserModel user);
}

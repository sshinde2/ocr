/*
 *  
 */
package au.com.target.tgtfacades.customergroups.impl;

import de.hybris.platform.commercefacades.customergroups.impl.DefaultCustomerGroupFacade;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;

import au.com.target.tgtfacades.customergroups.TargetCustomerGroupFacade;


/**
 * @author pthoma20
 */
public class DefaultTargetCustomerGroupFacade extends DefaultCustomerGroupFacade implements TargetCustomerGroupFacade {

    @Override
    public boolean isUserMemberOfTheGroup(final String customerGroupId) {
        final UserModel currentUser = getUserService().getCurrentUser();
        final Set<UserGroupModel> allUserGroups = getUserService().getAllUserGroupsForUser(currentUser);
        final UserGroupModel customerGroupToAssign = getCustomerGroupById(customerGroupId);
        Assert.assertNotNull(customerGroupToAssign);
        for (final UserGroupModel userGroup : allUserGroups) {
            if (getUserService().isMemberOfGroup(userGroup, customerGroupToAssign)
                    || customerGroupToAssign.equals(userGroup)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void addUserToCustomerGroup(final String customerGroupId, final UserModel user) {
        final Set<PrincipalGroupModel> groups = user.getGroups();
        final UserGroupModel userGroupModel = getCustomerGroupById(customerGroupId);
        final HashSet<PrincipalGroupModel> modifiedGroups = new HashSet<PrincipalGroupModel>(groups);
        modifiedGroups.add(userGroupModel);
        user.setGroups(modifiedGroups);
        getModelService().save(user);
        getModelService().refresh(user);
    }

}

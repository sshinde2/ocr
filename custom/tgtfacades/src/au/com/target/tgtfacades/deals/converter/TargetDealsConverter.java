/**
 * 
 */
package au.com.target.tgtfacades.deals.converter;


import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.promotions.model.AbstractDealModel;

import au.com.target.tgtfacades.deals.data.TargetDealData;


/**
 * @author pthoma20
 * 
 */
public class TargetDealsConverter extends
        AbstractPopulatingConverter<AbstractDealModel, TargetDealData> {

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.impl.AbstractPopulatingConverter#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final AbstractDealModel source, final TargetDealData target) {
        super.populate(source, target);
    }

}

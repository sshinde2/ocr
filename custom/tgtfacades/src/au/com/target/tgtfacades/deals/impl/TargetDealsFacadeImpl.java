/**
 * 
 */
package au.com.target.tgtfacades.deals.impl;

import de.hybris.platform.promotions.model.AbstractDealModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.deals.TargetDealsFacade;
import au.com.target.tgtfacades.deals.converter.TargetDealsConverter;
import au.com.target.tgtfacades.deals.data.TargetDealData;
import au.com.target.tgtfacades.deals.data.TargetDealsData;
import au.com.target.tgtfacades.offer.TargetMobileOfferHeadingFacade;
import au.com.target.tgtmarketing.deals.TargetMobileDealService;


/**
 * @author paul
 *
 */
public class TargetDealsFacadeImpl implements TargetDealsFacade {

    private TargetMobileDealService targetMobileDealService;


    private TargetMobileOfferHeadingFacade targetMobileOfferHeadingFacade;


    private TargetDealsConverter targetDealsConverter;


    protected List<TargetDealData> getAllMobileActiveDeals() {
        final List<TargetDealData> targetDealsDataList = new ArrayList<>();
        final List<AbstractDealModel> abstractDealModelList = targetMobileDealService.getAllActiveMobileDeals();

        if (CollectionUtils.isEmpty(abstractDealModelList)) {
            return targetDealsDataList;
        }
        for (final AbstractDealModel abstractDealModel : abstractDealModelList) {
            final TargetDealData targetDealsData = targetDealsConverter.convert(abstractDealModel);
            targetDealsDataList.add(targetDealsData);
        }
        return targetDealsDataList;
    }

    @Override
    public TargetDealsData getAllMobileActiveDealsWithOfferHeadings() {
        final TargetDealsData targetDealsData = new TargetDealsData();
        targetDealsData.setTargetDeals(getAllMobileActiveDeals());
        targetDealsData.setTargetMobileOfferHeadings(targetMobileOfferHeadingFacade.getTargetMobileOfferHeadings());
        return targetDealsData;
    }

    /**
     * @param targetMobileDealService
     *            the targetMobileDealService to set
     */
    @Required
    public void setTargetMobileDealService(final TargetMobileDealService targetMobileDealService) {
        this.targetMobileDealService = targetMobileDealService;
    }

    /**
     * @param targetDealsConverter
     *            the targetDealsConverter to set
     */
    @Required
    public void setTargetDealsConverter(final TargetDealsConverter targetDealsConverter) {
        this.targetDealsConverter = targetDealsConverter;
    }

    /**
     * @param targetMobileOfferHeadingFacade
     *            the targetMobileOfferHeadingFacade to set
     */
    @Required
    public void setTargetMobileOfferHeadingFacade(final TargetMobileOfferHeadingFacade targetMobileOfferHeadingFacade) {
        this.targetMobileOfferHeadingFacade = targetMobileOfferHeadingFacade;
    }
}

/**
 * 
 */
package au.com.target.tgtfacades.delivery.converters;

import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.c2l.RegionModel;

import au.com.target.tgtfacades.delivery.data.TargetRegionData;


/**
 * @author rmcalave
 * 
 */
public class TargetRegionConverter<SOURCE extends RegionModel, TARGET extends TargetRegionData> extends
        AbstractPopulatingConverter<SOURCE, TARGET> {

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.impl.AbstractPopulatingConverter#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final SOURCE source, final TARGET target) {
        super.populate(source, target);

        target.setName(source.getName());
        target.setAbbreviation(source.getAbbreviation());
    }
}

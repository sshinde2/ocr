/**
 * 
 */
package au.com.target.tgtfacades.delivery.data;

import java.util.Date;


/**
 * @author Pradeep
 * 
 *         Intermediate Data object used while populating Product ListingPage DeliveryPromotional sticker Message
 *
 */
public class DeliveryPromotionalData {

    private int deliveryPromoDisplayOrder;

    private int rZDMVPriority;

    private String deliveryMode;

    private Date restrictionStartDate;

    private Date restrictionEndDate;

    private String stickerMessage;

    /**
     * @return the deliveryPromoDisplayOrder
     */
    public int getDeliveryPromoDisplayOrder() {
        return deliveryPromoDisplayOrder;
    }

    /**
     * @param deliveryPromoDisplayOrder
     *            the deliveryPromoDisplayOrder to set
     */
    public void setDeliveryPromoDisplayOrder(final int deliveryPromoDisplayOrder) {
        this.deliveryPromoDisplayOrder = deliveryPromoDisplayOrder;
    }





    /**
     * @return the deliveryMode
     */
    public String getDeliveryMode() {
        return deliveryMode;
    }

    /**
     * @param deliveryMode
     *            the deliveryMode to set
     */
    public void setDeliveryMode(final String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    /**
     * @return the restrictionStartDate
     */
    public Date getRestrictionStartDate() {
        return restrictionStartDate;
    }

    /**
     * @param restrictionStartDate
     *            the restrictionStartDate to set
     */
    public void setRestrictionStartDate(final Date restrictionStartDate) {
        this.restrictionStartDate = restrictionStartDate;
    }

    /**
     * @return the restrictionEndDate
     */
    public Date getRestrictionEndDate() {
        return restrictionEndDate;
    }

    /**
     * @param restrictionEndDate
     *            the restrictionEndDate to set
     */
    public void setRestrictionEndDate(final Date restrictionEndDate) {
        this.restrictionEndDate = restrictionEndDate;
    }

    /**
     * @return the stickerMessage
     */
    public String getStickerMessage() {
        return stickerMessage;
    }

    /**
     * @param stickerMessage
     *            the stickerMessage to set
     */
    public void setStickerMessage(final String stickerMessage) {
        this.stickerMessage = stickerMessage;
    }

    /**
     * @return the rZDMVPriority
     */
    public int getrZDMVPriority() {
        return rZDMVPriority;
    }

    /**
     * @param rZDMVPriority
     *            the rZDMVPriority to set
     */
    public void setrZDMVPriority(final int rZDMVPriority) {
        this.rZDMVPriority = rZDMVPriority;
    }



}

/**
 * 
 */
package au.com.target.tgtfacades.delivery.data;

/**
 * @author pvarghe2
 *
 */
public class ShipsterData {

    private boolean available;
    private String reason;

    /**
     * @return the available
     */
    public boolean isAvailable() {
        return available;
    }

    /**
     * @param available
     *            the available to set
     */
    public void setAvailable(final boolean available) {
        this.available = available;
    }

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason
     *            the reason to set
     */
    public void setReason(final String reason) {
        this.reason = reason;
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.delivery.data;

import de.hybris.platform.commercefacades.product.data.PriceData;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class ZoneDeliveryModeValueData {
    private PriceData threshold;
    private PriceData value;
    private PriceData bulky;
    private PriceData extendedBulky;

    /**
     * @return the threshold
     */
    public PriceData getThreshold() {
        return threshold;
    }

    /**
     * @param threshold
     *            the threshold to set
     */
    public void setThreshold(final PriceData threshold) {
        this.threshold = threshold;
    }

    /**
     * @return the value
     */
    public PriceData getValue() {
        return value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(final PriceData value) {
        this.value = value;
    }

    /**
     * @return the bulky
     */
    public PriceData getBulky() {
        return bulky;
    }

    /**
     * @param bulky
     *            the bulky to set
     */
    public void setBulky(final PriceData bulky) {
        this.bulky = bulky;
    }

    /**
     * @return the extendedBulky
     */
    public PriceData getExtendedBulky() {
        return extendedBulky;
    }

    /**
     * @param extendedBulky
     *            the extendedBulky to set
     */
    public void setExtendedBulky(final PriceData extendedBulky) {
        this.extendedBulky = extendedBulky;
    }
}

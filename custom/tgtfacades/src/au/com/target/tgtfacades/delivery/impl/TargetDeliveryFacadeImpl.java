/**
 * 
 */
package au.com.target.tgtfacades.delivery.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtfacades.delivery.TargetDeliveryFacade;
import au.com.target.tgtfacades.delivery.data.TargetRegionData;


/**
 * @author rmcalave
 * 
 */
public class TargetDeliveryFacadeImpl implements TargetDeliveryFacade {

    private static final Logger LOG = Logger.getLogger(TargetDeliveryFacadeImpl.class);

    @Autowired
    private TargetDeliveryService deliveryService;
    @Autowired
    private CartService cartService;
    @Autowired
    private CommonI18NService commonI18NService;
    @Autowired
    private Converter<RegionModel, TargetRegionData> targetRegionConverter;

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.delivery.TargetDeliveryFacade#getRegionsForCountry(de.hybris.platform.commercefacades.user.data.CountryData)
     */
    @Override
    public List<TargetRegionData> getRegionsForCountry(final String countryIsoCode) {
        Validate.notNull(countryIsoCode, "countryIsoCode must not be null");

        final CountryModel countryModel = commonI18NService.getCountry(countryIsoCode);

        if (countryModel == null) {
            return null;
        }

        final List<RegionModel> regionModels = deliveryService.getRegionsForCountry(countryModel);

        if (CollectionUtils.isEmpty(regionModels)) {
            LOG.info("No regions found");
            return null;
        }

        final List<TargetRegionData> regions = new ArrayList<>();

        for (final RegionModel regionModel : regionModels) {
            regions.add(targetRegionConverter.convert(regionModel));
        }

        return regions;
    }


    @Override
    public boolean isDeliveryModePostCodeCombinationValid(final String deliveryMode, final String postCode) {
        return deliveryService.isDeliveryModePostCodeCombinationValid(
                deliveryService.getDeliveryModeForCode(deliveryMode), postCode, cartService.getSessionCart());
    }

}

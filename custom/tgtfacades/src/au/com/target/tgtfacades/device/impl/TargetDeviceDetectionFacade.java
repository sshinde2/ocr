package au.com.target.tgtfacades.device.impl;

import de.hybris.platform.acceleratorfacades.device.DeviceDetectionFacade;
import de.hybris.platform.acceleratorfacades.device.data.DeviceData;
import de.hybris.platform.acceleratorfacades.device.impl.DefaultDeviceDetectionFacade;
import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.commerceservices.enums.UiExperienceLevel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserConstants;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.constants.TgtFacadesConstants;



public class TargetDeviceDetectionFacade implements DeviceDetectionFacade {
    protected static final String DETECTED_DEVICE = "DeviceDetectionFacade-Detected-Device";
    private static final Logger LOG = Logger.getLogger(DefaultDeviceDetectionFacade.class.getName());
    private Converter<HttpServletRequest, DeviceData> requestDeviceDataConverter;
    private Converter<DeviceData, UiExperienceLevel> deviceDataUiExperienceLevelConverter;
    private SessionService sessionService;
    private UiExperienceService uiExperienceService;


    protected Converter<HttpServletRequest, DeviceData> getRequestDeviceDataConverter() {
        return requestDeviceDataConverter;
    }

    @Required
    public void setRequestDeviceDataConverter(
            final Converter<HttpServletRequest, DeviceData> requestDeviceDataConverter) {
        this.requestDeviceDataConverter = requestDeviceDataConverter;
    }

    protected Converter<DeviceData, UiExperienceLevel> getDeviceDataUiExperienceLevelConverter() {
        return deviceDataUiExperienceLevelConverter;
    }

    @Required
    public void setDeviceDataUiExperienceLevelConverter(
            final Converter<DeviceData, UiExperienceLevel> deviceDataUiExperienceLevelConverter) {
        this.deviceDataUiExperienceLevelConverter = deviceDataUiExperienceLevelConverter;
    }

    protected SessionService getSessionService() {
        return sessionService;
    }

    @Required
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

    protected UiExperienceService getUiExperienceService() {
        return uiExperienceService;
    }

    @Required
    public void setUiExperienceService(final UiExperienceService uiExperienceService) {
        this.uiExperienceService = uiExperienceService;
    }

    @Override
    public void initializeRequest(final HttpServletRequest request) {
        // Only initialise the detected device once per session
        if (getCurrentDetectedDevice() == null || BooleanUtils.toBoolean(request.getParameter("clear"))) {
            // Detect the device in the current request
            final DeviceData deviceData = getRequestDeviceDataConverter().convert(request);

            // Map the detected device to a UiExperienceLevel
            final UiExperienceLevel uiExperienceLevel = getDeviceDataUiExperienceLevelConverter().convert(deviceData);
            setCurrentDetectedDevice(deviceData);
            getUiExperienceService().setDetectedUiExperienceLevel(UiExperienceLevel.DESKTOP);
            getSessionService().setAttribute("device", uiExperienceLevel);
            detectIosApp(request);
            if (LOG.isDebugEnabled()) {
                final UserModel userModel = (UserModel)getSessionService().getAttribute(
                        UserConstants.USER_SESSION_ATTR_KEY);
                final String userUid = (userModel != null) ? userModel.getUid() : "<null>";

                LOG.debug("Detected device [" + deviceData.getId() + "] User Agent [" + deviceData.getUserAgent()
                        + "] Mobile ["
                        + deviceData.getMobileBrowser() + "] Session user [" + userUid + "]");
            }
        }
    }

    /**
     * Detect io app by user agent
     * 
     * @param request
     */
    protected void detectIosApp(final HttpServletRequest request) {
        final Boolean isIosApp = isIosApp(request);
        if (isIosApp.booleanValue()) {
            request.getSession().setAttribute(TgtFacadesConstants.IOS_APP_WV, Boolean.TRUE);
        }
    }

    /**
     * Returns true if it is an iOS app
     * 
     * @param request
     * @return true or false
     */
    private Boolean isIosApp(final HttpServletRequest request) {
        final String targetHeader = request.getHeader(TgtFacadesConstants.CustomHeaders.X_TARGET_HEADER);
        final String platformHeader = request.getHeader(TgtFacadesConstants.CustomHeaders.X_APP_PLATFORM_HEADER);
        if (LOG.isDebugEnabled()) {
            LOG.debug("X_TARGET_HEADER=" + targetHeader + " X_APP_PLATFORM_HEADER=" + platformHeader);
        }
        final Boolean isIosApp = Boolean
                .valueOf(TgtFacadesConstants.CustomHeaders.MOBILE_APP.equalsIgnoreCase(targetHeader)
                        && TgtFacadesConstants.CustomHeaders.IOS.equalsIgnoreCase(platformHeader));
        return isIosApp;
    }

    @Override
    public DeviceData getCurrentDetectedDevice() {
        return getSessionService().getAttribute(DETECTED_DEVICE);
    }

    protected void setCurrentDetectedDevice(final DeviceData deviceData) {
        getSessionService().setAttribute(DETECTED_DEVICE, deviceData);
    }

}

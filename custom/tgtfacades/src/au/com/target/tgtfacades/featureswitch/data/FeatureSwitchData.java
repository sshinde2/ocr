/**
 * 
 */
package au.com.target.tgtfacades.featureswitch.data;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * @author rmcalave
 *
 */
public class FeatureSwitchData {

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss dd-mm-yyyy");

    private String name;

    private boolean enabled;

    private String comment;

    private String attrName;

    private String formattedModifiedDate;

    /**
     * Default Constructor
     */
    public FeatureSwitchData() {
        //Default Constructor
    }

    /**
     * @param name
     * @param enabled
     */
    public FeatureSwitchData(final String name, final boolean enabled) {
        this.name = name;
        this.enabled = enabled;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled
     *            the enabled to set
     */
    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the attrName
     */
    public String getAttrName() {
        return attrName;
    }

    /**
     * @param attrName
     *            the attrName to set
     */
    public void setAttrName(final String attrName) {
        this.attrName = attrName;
    }


    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment
     *            the comment to set
     */
    public void setComment(final String comment) {
        this.comment = comment;
    }

    /**
     * @return the formattedModifiedDate
     */
    public String getFormattedModifiedDate() {
        return formattedModifiedDate;
    }

    /**
     * @param date
     *            the formattedModifiedDate to set
     */
    public void setFormattedModifiedDate(final Date date) {
        if (date != null) {
            this.formattedModifiedDate = dateFormat.format(date);
        }
    }




}

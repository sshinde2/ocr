/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */

package au.com.target.tgtfacades.flow.impl;

import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum;
import de.hybris.platform.core.Registry;
import de.hybris.platform.servicelayer.session.SessionService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.checkout.pci.CheckoutPciStrategy;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;


/**
 * Specialised version of the DefaultCheckoutFlowFacade that allows the checkout flow and pci strategy to be overridden
 * in the session. This is primarily used for demonstration purposes and you may not need to use this sub-class in your
 * environment.
 */
public class SessionOverrideCheckoutFlowFacade {
    public static final String SESSION_KEY_SUBSCRIPTION_PCI_OPTION = "SessionOverrideCheckoutFlow-SubscriptionPciOption";

    private static final Logger LOG = Logger.getLogger(SessionOverrideCheckoutFlowFacade.class);

    private CheckoutPciStrategy checkoutPciStrategy;

    private SessionService sessionService;

    protected SessionService getSessionService()
    {
        return sessionService;
    }

    @Required
    public void setSessionService(final SessionService sessionService)
    {
        this.sessionService = sessionService;
    }

    public CheckoutPciOptionEnum getSubscriptionPciOption()
    {
        final CheckoutPciOptionEnum sessionOverride = getSessionService().getAttribute(
                SESSION_KEY_SUBSCRIPTION_PCI_OPTION);
        if (sessionOverride != null)
        {
            LOG.info("Session Override SubscriptionPciOption [" + sessionOverride + "]");
            return sessionOverride;
        }
        return getCheckoutPciStrategy().getSubscriptionPciOption();
    }

    public static void resetSessionOverrides()
    {
        final SessionService sessionService = getStaticSessionService();
        sessionService.removeAttribute(SESSION_KEY_SUBSCRIPTION_PCI_OPTION);
        sessionService.removeAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE);
    }

    public static void setSessionOverrideSubscriptionPciOption(final CheckoutPciOptionEnum checkoutPciOption)
    {
        getStaticSessionService().setAttribute(SESSION_KEY_SUBSCRIPTION_PCI_OPTION, checkoutPciOption);
    }

    protected static SessionService getStaticSessionService()
    {
        return Registry.getApplicationContext().getBean("sessionService", SessionService.class);
    }

    protected CheckoutPciStrategy getCheckoutPciStrategy()
    {
        return this.checkoutPciStrategy;
    }

    @Required
    public void setCheckoutPciStrategy(final CheckoutPciStrategy strategy)
    {
        this.checkoutPciStrategy = strategy;
    }

}

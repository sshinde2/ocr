/**
 * 
 */
package au.com.target.tgtfacades.flybuys.data;

import de.hybris.platform.commercefacades.product.data.PriceData;


/**
 * @author rmcalave
 * 
 */
public class FlybuysRedeemTierData {
    private Integer points;
    private PriceData dollars;
    private String redeemCode;

    /**
     * @return the points
     */
    public Integer getPoints() {
        return points;
    }

    /**
     * @param points
     *            the points to set
     */
    public void setPoints(final Integer points) {
        this.points = points;
    }

    /**
     * @return the dollars
     */
    public PriceData getDollars() {
        return dollars;
    }

    /**
     * @param dollars
     *            the dollars to set
     */
    public void setDollars(final PriceData dollars) {
        this.dollars = dollars;
    }

    /**
     * @return the redeemCode
     */
    public String getRedeemCode() {
        return redeemCode;
    }

    /**
     * @param redeemCode
     *            the redeemCode to set
     */
    public void setRedeemCode(final String redeemCode) {
        this.redeemCode = redeemCode;
    }
}

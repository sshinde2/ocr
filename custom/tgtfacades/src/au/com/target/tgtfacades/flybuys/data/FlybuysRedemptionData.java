/**
 * 
 */
package au.com.target.tgtfacades.flybuys.data;

import java.util.List;


/**
 * @author rmcalave
 * 
 */
public class FlybuysRedemptionData {

    private Integer availablePoints;

    private List<FlybuysRedeemTierData> redeemTiers;

    /**
     * @return the availablePoints
     */
    public Integer getAvailablePoints() {
        return availablePoints;
    }

    /**
     * @param availablePoints
     *            the availablePoints to set
     */
    public void setAvailablePoints(final Integer availablePoints) {
        this.availablePoints = availablePoints;
    }

    /**
     * @return the redeemTiers
     */
    public List<FlybuysRedeemTierData> getRedeemTiers() {
        return redeemTiers;
    }

    /**
     * @param redeemTiers
     *            the redeemTiers to set
     */
    public void setRedeemTiers(final List<FlybuysRedeemTierData> redeemTiers) {
        this.redeemTiers = redeemTiers;
    }

}

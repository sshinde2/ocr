/**
 * 
 */
package au.com.target.tgtfacades.flybuys.data;

import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;



/**
 * Return success/error info about the flybuys authenticate request we made
 * 
 */
public class FlybuysRequestStatusData {

    private FlybuysResponseType response;
    private String errorMessage;

    /**
     * @return the response
     */
    public FlybuysResponseType getResponse() {
        return response;
    }

    /**
     * @param response
     *            the response to set
     */
    public void setResponse(final FlybuysResponseType response) {
        this.response = response;
    }

    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage
     *            the errorMessage to set
     */
    public void setErrorMessage(final String errorMessage) {
        this.errorMessage = errorMessage;
    }




}

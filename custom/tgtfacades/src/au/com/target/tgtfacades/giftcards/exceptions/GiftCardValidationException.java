/**
 * 
 */
package au.com.target.tgtfacades.giftcards.exceptions;

import au.com.target.tgtfacades.giftcards.validate.GiftCardValidationErrorType;


/**
 * GiftCard validation exception.
 * 
 * @author jjayawa1
 *
 */
public class GiftCardValidationException extends Exception {

    private final GiftCardValidationErrorType errorType;
    private final String permittedValue;

    public GiftCardValidationException(final String message, final GiftCardValidationErrorType errorType) {
        super(message);
        this.errorType = errorType;
        this.permittedValue = null;
    }

    public GiftCardValidationException(final String message, final GiftCardValidationErrorType errorType,
            final String permittedValue) {
        super(message);
        this.errorType = errorType;
        this.permittedValue = permittedValue;
    }

    public GiftCardValidationException(final String message, final Throwable t) {
        super(message, t);
        this.errorType = null;
        this.permittedValue = null;
    }

    /**
     * @return the permittedValue
     */
    public String getPermittedValue() {
        return permittedValue;
    }

    /**
     * @return the errorType
     */
    public GiftCardValidationErrorType getErrorType() {
        return errorType;
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.giftcards.validate;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.cart.TargetCartService;
import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.giftcards.GiftCardService;
import au.com.target.tgtcore.product.TargetProductService;


/**
 * Base gift card validator with wiring but no validation implementation.
 * 
 * @author jjayawa1
 *
 */
public abstract class BaseGiftCardValidator implements GiftCardValidator {

    private TargetCartService targetCartService;

    private GiftCardService giftCardService;

    private TargetProductService targetProductService;
    private TargetSharedConfigService targetSharedConfigService;

    /**
     * @return the targetSharedConfigService
     */
    protected TargetSharedConfigService getTargetSharedConfigService() {
        return targetSharedConfigService;
    }

    /**
     * @param targetSharedConfigService
     *            the targetSharedConfigService to set
     */
    @Required
    public void setTargetSharedConfigService(final TargetSharedConfigService targetSharedConfigService) {
        this.targetSharedConfigService = targetSharedConfigService;
    }

    /**
     * @return the targetCartService
     */
    public TargetCartService getTargetCartService() {
        return targetCartService;
    }

    /**
     * @param targetCartService
     *            the targetCartService to set
     */
    @Required
    public void setTargetCartService(final TargetCartService targetCartService) {
        this.targetCartService = targetCartService;
    }

    /**
     * @return the giftCardService
     */
    public GiftCardService getGiftCardService() {
        return giftCardService;
    }

    /**
     * @param giftCardService
     *            the giftCardService to set
     */
    @Required
    public void setGiftCardService(final GiftCardService giftCardService) {
        this.giftCardService = giftCardService;
    }

    /**
     * @return the targetProductService
     */
    public TargetProductService getTargetProductService() {
        return targetProductService;
    }

    /**
     * @param targetProductService
     *            the targetProductService to set
     */
    public void setTargetProductService(final TargetProductService targetProductService) {
        this.targetProductService = targetProductService;
    }

}

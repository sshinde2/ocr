/**
 * 
 */
package au.com.target.tgtfacades.giftcards.validate;

/**
 * Enum to hold the validation error type.
 * 
 * @author jjayawa1
 *
 */
public enum GiftCardValidationErrorType {
    VALUE,
    QUANTITY;

    public String getValue()
    {
        return this.toString().toLowerCase();
    }
}

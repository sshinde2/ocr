/**
 * 
 */
package au.com.target.tgtfacades.giftcards.validate;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import org.springframework.util.Assert;

import au.com.target.tgtcore.model.GiftCardModel;
import au.com.target.tgtcore.product.exception.ProductNotFoundException;
import au.com.target.tgtcore.util.ProductUtil;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.giftcards.exceptions.GiftCardValidationException;


/**
 * Validates that the maximum permitted quantity for a gift card has reached.
 * 
 * @author jjayawa1
 *
 */
public class MaxGiftCardQtyValidator extends BaseGiftCardValidator {

    /**
     * {@inheritDoc}
     * 
     */
    @Override
    public void validateGiftCards(final String productCode, final long newQty)
            throws GiftCardValidationException, ProductNotFoundException {
        final CartModel cartModel = getTargetCartService().getSessionCart();
        Assert.notNull(cartModel, "Cart cannot be null");

        final GiftCardModel giftCard = getGiftCardService().getGiftCardForProduct(productCode);
        if (giftCard != null) {
            if (giftCard.getMaxOrderQuantity() != null) {
                final int maxOrderQty = giftCard.getMaxOrderQuantity().intValue();
                if (getGiftCardQuantityInCart(giftCard.getBrandId(), cartModel) + newQty > maxOrderQty) {
                    throw new GiftCardValidationException("Not allowed to purchase the quantity",
                            GiftCardValidationErrorType.QUANTITY, String.valueOf(maxOrderQty));
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     * 
     */
    @Override
    public void validateGiftCardsUpdateQty(final String productCode, final long newQty)
            throws GiftCardValidationException {
        final CartModel cartModel = getTargetCartService().getSessionCart();
        Assert.notNull(cartModel, "Cart cannot be null");
        final ProductModel productModel = getTargetProductService().getProductForCode(productCode);
        if (ProductUtil.isProductTypePhysicalGiftcard(productModel)) {
            final int configuredQuantity = getTargetSharedConfigService()
                    .getInt(TgtFacadesConstants.GIFT_CARD_QUANTITY_INCART_LIMIT, 9);
            final int physicalGiftCardQtyInCart = getUpdatedPhysicalGiftCardQuantity(cartModel.getEntries(),
                    productCode, newQty);
            if (physicalGiftCardQtyInCart > configuredQuantity) {
                throw new GiftCardValidationException("Not allowed to purchase the quantity.",
                        GiftCardValidationErrorType.QUANTITY, String.valueOf(configuredQuantity));
            }
        }

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.giftcards.validate.GiftCardValidator#validatePhysicalGiftCards(de.hybris.platform.core.model.product.ProductModel, long)
     */
    @Override
    public void validatePhysicalGiftCards(final ProductModel productModel, final long newQty)
            throws GiftCardValidationException {

        final CartModel cartModel = getTargetCartService().getSessionCart();
        Assert.notNull(cartModel, "Cart cannot be null");

        final int physicalGiftCardQtyInCart = getPhysicalGiftCardQuantityInCart(cartModel.getEntries());
        final int configuredQuantity = getTargetSharedConfigService()
                .getInt(TgtFacadesConstants.GIFT_CARD_QUANTITY_INCART_LIMIT, 9);
        if (physicalGiftCardQtyInCart + newQty > configuredQuantity) {
            throw new GiftCardValidationException("Not allowed to purchase the quantity.",
                    GiftCardValidationErrorType.QUANTITY, String.valueOf(configuredQuantity));
        }
    }

    /**
     * @param cartEntries
     * @param productCode
     * @param newQty
     * @return updatedGiftCardQtyInCart
     */
    private int getUpdatedPhysicalGiftCardQuantity(final List<AbstractOrderEntryModel> cartEntries,
            final String productCode,
            final long newQty) {
        int updatedGiftCardQtyInCart = 0;
        for (final AbstractOrderEntryModel cartEntry : cartEntries) {
            if (ProductUtil.isProductTypePhysicalGiftcard(cartEntry.getProduct())) {
                updatedGiftCardQtyInCart += cartEntry.getProduct().getCode().equals(productCode) ? newQty
                        : cartEntry.getQuantity().intValue();
            }
        }
        return updatedGiftCardQtyInCart;
    }

    /**
     * @param cartEntries
     * @return giftCardQtyInCart
     */
    private int getPhysicalGiftCardQuantityInCart(final List<AbstractOrderEntryModel> cartEntries) {
        int giftCardQtyInCart = 0;
        for (final AbstractOrderEntryModel cartEntry : cartEntries) {
            if (ProductUtil.isProductTypePhysicalGiftcard(cartEntry.getProduct())) {
                giftCardQtyInCart += cartEntry.getQuantity().intValue();
            }
        }
        return giftCardQtyInCart;
    }

    private int getGiftCardQuantityInCart(final String brandId, final CartModel cart) throws ProductNotFoundException {
        int count = 0;
        final List<AbstractOrderEntryModel> cartEntries = cart.getEntries();
        for (final AbstractOrderEntryModel ce : cartEntries) {
            final GiftCardModel giftCard = getGiftCardService().getGiftCardForProduct(ce.getProduct().getCode());
            if (giftCard != null) {
                if (giftCard.getBrandId().equals(brandId)) {
                    if (ce.getQuantity() != null) {
                        count = count + ce.getQuantity().intValue();
                    }
                }
            }
        }
        return count;
    }

}

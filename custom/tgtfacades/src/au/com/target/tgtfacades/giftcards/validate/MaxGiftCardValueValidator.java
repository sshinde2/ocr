/**
 * 
 */
package au.com.target.tgtfacades.giftcards.validate;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;

import au.com.target.tgtcore.model.GiftCardModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.product.exception.ProductNotFoundException;
import au.com.target.tgtfacades.giftcards.exceptions.GiftCardValidationException;


/**
 * Validates that the maximum permitted order value for a gift card has reached.
 * 
 * @author jjayawa1
 *
 */
public class MaxGiftCardValueValidator extends BaseGiftCardValidator {

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateGiftCards(final String productCode, final long newQty)
            throws GiftCardValidationException, ProductNotFoundException {
        final CartModel cartModel = getTargetCartService().getSessionCart();
        Assert.assertNotNull("CartModel cannot be null!", cartModel);
        final GiftCardModel giftCard = getGiftCardService().getGiftCardForProduct(productCode);

        if (giftCard != null) {
            if (giftCard.getMaxOrderValue() != null) {
                final double maxOrderValue = giftCard.getMaxOrderValue().doubleValue();
                final Double denomination = getDenomination(productCode);
                if (null != denomination) {
                    if (getGiftCardValueInCart(giftCard.getBrandId(), cartModel)
                            + (denomination.doubleValue() * newQty) > maxOrderValue) {
                        throw new GiftCardValidationException("Gift Card beyond the total allowed value : ",
                                GiftCardValidationErrorType.VALUE, String.valueOf(maxOrderValue));
                    }
                }
            }
        }
    }

    protected double getGiftCardValueInCart(final String brandId, final CartModel cart)
            throws ProductNotFoundException {
        double value = 0d;
        final List<AbstractOrderEntryModel> cartEntries = cart.getEntries();
        if (CollectionUtils.isNotEmpty(cartEntries)) {
            for (final AbstractOrderEntryModel ce : cartEntries) {
                final GiftCardModel giftCard = getGiftCardService().getGiftCardForProduct(ce.getProduct().getCode());
                if (null != giftCard && null != giftCard.getBrandId() && giftCard.getBrandId().equals(brandId)) {
                    final Double denomination = getDenomination(ce.getProduct().getCode());
                    if (null != denomination) {
                        if (ce.getQuantity() != null) {
                            value += ce.getQuantity().intValue() * denomination.doubleValue();
                        }
                    }
                }
            }
        }
        return value;
    }

    protected Double getDenomination(final String productCode) {
        final ProductModel productModel = getTargetProductService().getProductForCode(productCode);
        if (null != productModel && productModel instanceof TargetSizeVariantProductModel) {
            final TargetSizeVariantProductModel sizeVariantProduct = (TargetSizeVariantProductModel)productModel;
            return sizeVariantProduct.getDenomination();
        }
        return null;
    }


    @Override
    public void validateGiftCardsUpdateQty(final String productCode, final long newQty)
            throws GiftCardValidationException {
        throw new UnsupportedOperationException("Physical giftcard value validator is not considered!");

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.giftcards.validate.GiftCardValidator#validatePhysicalGiftCards(de.hybris.platform.core.model.product.ProductModel, long)
     */
    @Override
    public void validatePhysicalGiftCards(final ProductModel productModel, final long qty)
            throws GiftCardValidationException {
        throw new UnsupportedOperationException("Physical giftcard value validator is not considered!");

    }

}

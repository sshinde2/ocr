/**
 * 
 */
package au.com.target.tgtfacades.google.location.finder.data;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * @author bbaral1
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Result {

    private String state;

    private String suburb;

    private String postalCode;

    private int hdShortLeadTime;

    private int edShortLeadTime;

    private int bulky1ShortLeadTime;

    private int bulky2ShortLeadTime;

    private int hdLongLeadTime;

    private int edLongLeadTime;

    private int bulky1LongLeadTime;

    private int bulky2LongLeadTime;

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @return the suburb
     */
    public String getSuburb() {
        return suburb;
    }

    /**
     * @return the postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * @return the hdShortLeadTime
     */
    public int getHdShortLeadTime() {
        return hdShortLeadTime;
    }

    /**
     * @return the edShortLeadTime
     */
    public int getEdShortLeadTime() {
        return edShortLeadTime;
    }

    /**
     * @return the bulky1ShortLeadTime
     */
    public int getBulky1ShortLeadTime() {
        return bulky1ShortLeadTime;
    }

    /**
     * @return the bulky2ShortLeadTime
     */
    public int getBulky2ShortLeadTime() {
        return bulky2ShortLeadTime;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(final String state) {
        this.state = state;
    }

    /**
     * @param suburb
     *            the suburb to set
     */
    public void setSuburb(final String suburb) {
        this.suburb = suburb;
    }

    /**
     * @param postalCode
     *            the postalCode to set
     */
    public void setPostalCode(final String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * @param hdShortLeadTime
     *            the hdShortLeadTime to set
     */
    public void setHdShortLeadTime(final int hdShortLeadTime) {
        this.hdShortLeadTime = hdShortLeadTime;
    }

    /**
     * @param edShortLeadTime
     *            the edShortLeadTime to set
     */
    public void setEdShortLeadTime(final int edShortLeadTime) {
        this.edShortLeadTime = edShortLeadTime;
    }

    /**
     * @param bulky1ShortLeadTime
     *            the bulky1ShortLeadTime to set
     */
    public void setBulky1ShortLeadTime(final int bulky1ShortLeadTime) {
        this.bulky1ShortLeadTime = bulky1ShortLeadTime;
    }

    /**
     * @param bulky2ShortLeadTime
     *            the bulky2ShortLeadTime to set
     */
    public void setBulky2ShortLeadTime(final int bulky2ShortLeadTime) {
        this.bulky2ShortLeadTime = bulky2ShortLeadTime;
    }

    /**
     * @return the hdLongLeadTime
     */
    public int getHdLongLeadTime() {
        return hdLongLeadTime;
    }

    /**
     * @param hdLongLeadTime
     *            the hdLongLeadTime to set
     */
    public void setHdLongLeadTime(final int hdLongLeadTime) {
        this.hdLongLeadTime = hdLongLeadTime;
    }

    /**
     * @return the edLongLeadTime
     */
    public int getEdLongLeadTime() {
        return edLongLeadTime;
    }

    /**
     * @param edLongLeadTime
     *            the edLongLeadTime to set
     */
    public void setEdLongLeadTime(final int edLongLeadTime) {
        this.edLongLeadTime = edLongLeadTime;
    }

    /**
     * @return the bulky1LongLeadTime
     */
    public int getBulky1LongLeadTime() {
        return bulky1LongLeadTime;
    }

    /**
     * @param bulky1LongLeadTime
     *            the bulky1LongLeadTime to set
     */
    public void setBulky1LongLeadTime(final int bulky1LongLeadTime) {
        this.bulky1LongLeadTime = bulky1LongLeadTime;
    }

    /**
     * @return the bulky2LongLeadTime
     */
    public int getBulky2LongLeadTime() {
        return bulky2LongLeadTime;
    }

    /**
     * @param bulky2LongLeadTime
     *            the bulky2LongLeadTime to set
     */
    public void setBulky2LongLeadTime(final int bulky2LongLeadTime) {
        this.bulky2LongLeadTime = bulky2LongLeadTime;
    }
}

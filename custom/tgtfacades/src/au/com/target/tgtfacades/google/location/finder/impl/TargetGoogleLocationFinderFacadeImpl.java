/**
 * 
 */
package au.com.target.tgtfacades.google.location.finder.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.google.location.finder.data.GeocodeAddressComponent;
import au.com.target.tgtcore.google.location.finder.data.GeocodeResponse;
import au.com.target.tgtcore.google.location.finder.data.GeocodeResultData;
import au.com.target.tgtcore.google.location.finder.service.TargetLocationFinderService;
import au.com.target.tgtfacades.config.TargetSharedConfigFacade;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.google.location.finder.TargetGoogleLocationFinderFacade;
import au.com.target.tgtfacades.google.location.finder.data.GoogleResponseData;
import au.com.target.tgtfacades.google.location.finder.data.Result;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.CurrentLocationResponse;
import au.com.target.tgtfacades.response.data.Error;
import au.com.target.tgtfacades.response.data.Response;


/**
 * @author bbaral1
 *
 */
public class TargetGoogleLocationFinderFacadeImpl implements TargetGoogleLocationFinderFacade {

    private static final Logger LOG = Logger.getLogger(TargetGoogleLocationFinderFacadeImpl.class);

    private TargetLocationFinderService targetLocationFinderService;

    private TargetSharedConfigFacade targetSharedConfigFacade;

    @Override
    public Response searchUserDeliveryLocation(final String location, final Double latitude,
            final Double longitude) {
        GoogleResponseData googleResponse = null;
        try {
            final GeocodeResponse geocodingResponse = targetLocationFinderService
                    .searchUserDeliveryLocationSuburb(location, latitude, longitude);
            if (isFailureResponse(geocodingResponse)) {
                return createErrorResponse(getResponseCode(geocodingResponse));
            }
            if (null != geocodingResponse.getResults()) {
                final List<GeocodeResultData> geocodingResultsList = geocodingResponse.getResults();
                //Google maps API always gives one result with this service call.
                final GeocodeResultData geocodingResultData = geocodingResultsList.get(0);
                final List<Result> resultList = new ArrayList<>();
                final String[] postcodeLocalities = geocodingResultData.getPostcodeLocalities();
                setGeolocationData(geocodingResultData, resultList, postcodeLocalities);
                googleResponse = populateResponseData(resultList);
                googleResponse.setSearchTerm(location);
            }
        }
        catch (final IOException ioException) {
            LOG.error("Exception occured processing geocoding request:", ioException);
            return createErrorResponse("ERROR_LOCATION_SERVICES");
        }
        final Response response = new Response(true);
        response.setData(googleResponse);
        return response;
    }

    @Override
    public String searchUserCurrentAddressUsingLatLng(final Double latitude, final Double longitude) {
        GeocodeResponse geocodingResponse = null;
        try {
            geocodingResponse = targetLocationFinderService.searchUserDeliveryLocationSuburb(null, latitude, longitude);
        }
        catch (final IOException ioException) {
            LOG.error("Exception occured while making a geoCoding request:", ioException);
        }
        final CurrentLocationResponse currentLocationResponse = new CurrentLocationResponse();
        if (null != geocodingResponse && null != geocodingResponse.getResults()) {
            //Google Maps will always set the first result-set as the current location. Hence first result-set is considered with this aspect.
            final GeocodeResultData geocodingResultData = geocodingResponse.getResults().get(0);
            for (final GeocodeAddressComponent geocoderAddressComponent : geocodingResultData.getAddressComponents()) {
                //POLITICAL and LOCALITY address components type always consider current location(suburb)
                if (Arrays.asList(TgtFacadesConstants.AddressComponentType.POLITICAL,
                        TgtFacadesConstants.AddressComponentType.LOCALITY)
                        .containsAll(geocoderAddressComponent.getTypes())) {
                    currentLocationResponse.setLocationName(geocoderAddressComponent.getLongName());
                }
                if (geocoderAddressComponent.getTypes()
                        .contains(TgtFacadesConstants.AddressComponentType.POSTAL_CODE)) {
                    currentLocationResponse.setPostCode(geocoderAddressComponent.getLongName());
                }
            }
        }
        return userCurrentLocation(currentLocationResponse);
    }

    /**
     * @param geocodingResultData
     * @param resultList
     * @param postcodeLocalities
     */
    private void setGeolocationData(final GeocodeResultData geocodingResultData, final List<Result> resultList,
            final String[] postcodeLocalities) {
        //geoCode always respond with postCode localities information with given postCode input                
        if (null != postcodeLocalities) {
            for (final String postCodeLocality : postcodeLocalities) {
                setDeliveryLocationData(geocodingResultData, resultList, postCodeLocality);
            }
        }
        //It is not always necessary google will respond with postCode localities information with location/post code.
        else {
            setDeliveryLocationData(geocodingResultData, resultList, null);
        }
    }

    /**
     * Response code received from Geocoding API request.
     * 
     * @param geocodingResponse
     * @return String
     */
    private String getResponseCode(final GeocodeResponse geocodingResponse) {
        return (null != geocodingResponse && null != geocodingResponse.getStatus())
                ? geocodingResponse.getStatus() : null;
    }

    /**
     * Setting required delivery location data set
     * 
     * @param geocodingResultData
     * @param resultList
     * @param postCodeLocality
     */
    private void setDeliveryLocationData(final GeocodeResultData geocodingResultData, final List<Result> resultList,
            final String postCodeLocality) {
        final Result result = new Result();
        setAddressComponentsAndShortLeadTime(geocodingResultData, postCodeLocality, result);
        resultList.add(result);
    }

    /**
     * Setting Address component and Short Lead time based on location/postcode/latlng search.
     * 
     * @param geocodingResultData
     * @param postcodeLocality
     * @param result
     */
    private void setAddressComponentsAndShortLeadTime(final GeocodeResultData geocodingResultData,
            final String postcodeLocality,
            final Result result) {
        for (final GeocodeAddressComponent geocoderAddressComponent : geocodingResultData.getAddressComponents()) {
            final List<String> types = geocoderAddressComponent.getTypes();
            if (null != types) {
                if (containsBothElementTypes(types,
                        TgtFacadesConstants.AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_1,
                        TgtFacadesConstants.AddressComponentType.POLITICAL)) {
                    result.setState(geocoderAddressComponent.getShortName().toLowerCase());
                }
                if (types.contains(TgtFacadesConstants.AddressComponentType.POSTAL_CODE)) {
                    result.setPostalCode(geocoderAddressComponent.getShortName());
                }
            }
            if (StringUtils.isNotEmpty(postcodeLocality)) {
                result.setSuburb(postcodeLocality);
            }
            //In case of city/suburb search, it always fetched political/locality information by city name itself.
            else if (containsBothElementTypes(types, TgtFacadesConstants.AddressComponentType.POLITICAL,
                    TgtFacadesConstants.AddressComponentType.LOCALITY)) {
                result.setSuburb(geocoderAddressComponent.getLongName());
            }
        }
        setShortLeadTime(result);
    }

    /**
     * Method will verify in case of no response from Google Maps API.
     *
     * @param geocodingResponse
     * @return Response
     */
    private boolean isFailureResponse(final GeocodeResponse geocodingResponse) {
        return geocodingResponse == null || CollectionUtils.isEmpty(geocodingResponse.getResults())
                || !geocodingResponse.getStatus().equals(TgtFacadesConstants.GeocoderStatus.OK);
    }

    /**
     * This method will throw error message when no response received from geocoding api call..
     * 
     * @param errCode
     * @return Response
     */
    private Response createErrorResponse(final String errCode) {
        final Response response = new Response(false);
        final Error error = new Error(errCode);
        error.setMessage(getErrorMessage(errCode));
        final BaseResponseData responseData = new BaseResponseData();
        responseData.setError(error);
        response.setData(responseData);
        return response;
    }

    /**
     * This methods are handling exceptional scenarios that occurred while making geoCoding request will
     * location/postCode input search for delivery location.
     * 
     * @param errorCode
     * @return String
     */
    private String getErrorMessage(final String errorCode) {
        String errorMessage;
        if (TgtFacadesConstants.GeocoderStatus.ZERO_RESULTS.equals(errorCode)) {
            errorMessage = TgtFacadesConstants.GeocodingExceptionMessage.ZERO_RESULTS;
        }
        else if (TgtFacadesConstants.GeocoderStatus.OVER_QUERY_LIMIT.equals(errorCode)) {
            errorMessage = TgtFacadesConstants.GeocodingExceptionMessage.OVER_QUERY_LIMIT;
        }
        else if (TgtFacadesConstants.GeocoderStatus.REQUEST_DENIED.equals(errorCode)) {
            errorMessage = TgtFacadesConstants.GeocodingExceptionMessage.REQUEST_DENIED;
        }
        else if (TgtFacadesConstants.GeocoderStatus.INVALID_REQUEST.equals(errorCode)) {
            errorMessage = TgtFacadesConstants.GeocodingExceptionMessage.INVALID_REQUEST;
        }
        else {
            errorMessage = TgtFacadesConstants.GeocodingExceptionMessage.ERROR_DELIVERY_LOCATION_MESSAGE;
        }
        return errorMessage;
    }


    /**
     * Method will check if both element types are present in the response
     * 
     * @param elementTypes
     * @param elementType1
     * @param elementType2
     * @return boolean
     */
    private boolean containsBothElementTypes(final List<String> elementTypes, final String elementType1,
            final String elementType2) {
        return null != elementTypes && elementTypes.contains(elementType1) && elementTypes.contains(elementType2);
    }

    /**
     * Method will set HD, ED, Bulky1 and Bulky2 Short Lead time based on the various states accross Australia
     * 
     * @param result
     */
    private void setShortLeadTime(final Result result) {
        final String state = result.getState();
        if (StringUtils.isNotEmpty(state)) {
            final int defaultShortLeadTime = getLeadTime(
                    TgtFacadesConstants.LeadTimes.LEADTIME_DEFAULT_CONFIG_CODE, null,
                    TgtFacadesConstants.LeadTimes.DEFAULT_SHORT_LEAD_TIME_VALUE);
            result.setHdShortLeadTime(
                    getLeadTime(TgtFacadesConstants.LeadTimes.LEADTIME_HD_SHORT, state,
                            defaultShortLeadTime));
            result.setEdShortLeadTime(
                    getLeadTime(TgtFacadesConstants.LeadTimes.LEADTIME_ED_SHORT, state,
                            defaultShortLeadTime));
            result.setBulky1ShortLeadTime(getLeadTime(
                    TgtFacadesConstants.LeadTimes.LEADTIME_BULKY1_SHORT, state, defaultShortLeadTime));
            result.setBulky2ShortLeadTime(getLeadTime(
                    TgtFacadesConstants.LeadTimes.LEADTIME_BULKY2_SHORT, state, defaultShortLeadTime));
            result.setHdLongLeadTime(
                    getLeadTime(TgtFacadesConstants.LeadTimes.LEADTIME_HD_LONG, state,
                            TgtFacadesConstants.LeadTimes.DEFAULT_LONG_LEAD_TIME_VALUE));
            result.setEdLongLeadTime(
                    getLeadTime(TgtFacadesConstants.LeadTimes.LEADTIME_ED_LONG, state,
                            TgtFacadesConstants.LeadTimes.DEFAULT_LONG_LEAD_TIME_VALUE));
            result.setBulky1LongLeadTime(getLeadTime(
                    TgtFacadesConstants.LeadTimes.LEADTIME_BULKY1_LONG, state,
                    TgtFacadesConstants.LeadTimes.DEFAULT_LONG_LEAD_TIME_VALUE));
            result.setBulky2LongLeadTime(getLeadTime(
                    TgtFacadesConstants.LeadTimes.LEADTIME_BULKY2_LONG, state,
                    TgtFacadesConstants.LeadTimes.DEFAULT_LONG_LEAD_TIME_VALUE));
        }
    }

    /**
     * Method to retrieve short lead time.
     * 
     * @param configCode
     * @param state
     * @param defaultValue
     * @return int
     */
    private int getLeadTime(final String configCode, final String state, final int defaultValue) {
        return targetSharedConfigFacade.getInt(StringUtils.isNotEmpty(state) ? configCode.concat(state) : configCode,
                defaultValue);
    }


    /**
     * This method will populate user delivery location information.
     * 
     * @param resultList
     */
    private GoogleResponseData populateResponseData(final List<Result> resultList) {
        final GoogleResponseData googleResponse = new GoogleResponseData();
        googleResponse.setResults(resultList);
        return googleResponse;
    }

    /**
     * Method will return current location for the user. Which is consisting of location name and post code.
     * 
     * @param currentLocationResponse
     * @return String
     */
    private String userCurrentLocation(final CurrentLocationResponse currentLocationResponse) {
        final String locationName = StringUtils.isEmpty(currentLocationResponse.getLocationName()) ? StringUtils.EMPTY
                : currentLocationResponse.getLocationName();
        final String postCode = StringUtils.isEmpty(currentLocationResponse.getPostCode()) ? StringUtils.EMPTY
                : currentLocationResponse.getPostCode();

        if (StringUtils.isEmpty(locationName) && StringUtils.isEmpty(postCode)) {
            return StringUtils.EMPTY;
        }

        final StringBuilder locationDetails = new StringBuilder();
        return locationDetails.append(locationName).append(" ").append(postCode).toString().trim();
    }

    /**
     * @param targetLocationFinderService
     *            the targetLocationFinderService to set
     */
    @Required
    public void setTargetLocationFinderService(final TargetLocationFinderService targetLocationFinderService) {
        this.targetLocationFinderService = targetLocationFinderService;
    }

    /**
     * @param targetSharedConfigFacade
     *            the targetSharedConfigFacade to set
     */
    @Required
    public void setTargetSharedConfigFacade(final TargetSharedConfigFacade targetSharedConfigFacade) {
        this.targetSharedConfigFacade = targetSharedConfigFacade;
    }

}

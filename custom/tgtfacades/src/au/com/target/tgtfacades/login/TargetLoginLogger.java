/**
 * 
 */
package au.com.target.tgtfacades.login;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import au.com.target.tgtfacades.response.data.Response;


/**
 * This class is the spc login logging aspect which is currently configured for CheckoutLoginServiceController. It can
 * be updated and configured for other targets if required.
 * 
 * @author htan3
 *
 */
@Component
public class TargetLoginLogger {
    public static final String LOG_FORMAT_BEGIN = "Receive API Request: method=%s, Parameters:[%s]";
    public static final String LOG_FORMAT_FINISH = "Finish API Request: method=%s, Result:[%s]";
    public static final String LOG_FORMAT_FINISH_ERROR = "Finish API Request with Error: method=%s, Result:[%s]";
    public static final String LOG_FORMAT_FINISH_EX = "Finish API Request with Exception: method=%s, Exception Thrown:[%s]";
    private static final Logger LOG = Logger.getLogger("Login SPC");

    /**
     * to log method name and parameters before method execution
     * 
     * @param joinPoint
     */
    public void beforeAction(final JoinPoint joinPoint) {
        LOG.info(String.format(LOG_FORMAT_BEGIN, joinPoint.getSignature().getName(),
                getArgs(joinPoint)));
    }


    /**
     * to log method name and return value after method execution
     * 
     * @param joinPoint
     * @param returnValue
     */
    public void afterReturnAction(final JoinPoint joinPoint, final Object returnValue) {

        if (responseSucessful(returnValue)) {
            LOG.info(String.format(LOG_FORMAT_FINISH, joinPoint.getSignature().getName(),
                    getResult(returnValue)));
        }
        else {
            LOG.error(String.format(LOG_FORMAT_FINISH_ERROR, joinPoint.getSignature().getName(),
                    getResult(returnValue)));
        }
    }

    private boolean responseSucessful(final Object returnValue) {
        if (returnValue instanceof Response && !((Response)returnValue).isSuccess()) {
            return false;
        }
        return true;
    }

    /**
     * to log exception if thrown from method execution
     * 
     * @param joinPoint
     */
    public void afterThrow(final JoinPoint joinPoint, final Exception exception) {
        LOG.error(String.format(LOG_FORMAT_FINISH_EX, joinPoint.getSignature().getName(),
                exception.getClass().getSimpleName() + "|" + exception.getMessage()), exception);
    }

    /**
     * return method arguments
     * 
     * @param joinPoint
     * @return joint string with all argument values
     */
    private String getArgs(final JoinPoint joinPoint) {
        final StringBuilder sb = new StringBuilder();
        final Object[] args = joinPoint.getArgs();

        if (null != args) {
            for (final Object arg : args) {
                if (!(arg instanceof Model || arg instanceof HttpServletRequest)) {
                    sb.append(arg);
                    sb.append("|");
                }
            }
        }
        return sb.toString();
    }

    /**
     * return string value of the return object. please implement toString of the return class properly.
     * 
     * @param returnValue
     * @return string
     */
    private String getResult(final Object returnValue) {
        final StringBuilder sb = new StringBuilder();
        if (returnValue != null) {
            sb.append(returnValue.toString());
            sb.append(" ");
            if (returnValue instanceof Response) {
                final Response response = (Response)returnValue;
                if (response.isSuccess() && response.getData() != null) {
                    sb.append(response.getData().toString());
                }
            }
        }
        return sb.toString();
    }
}

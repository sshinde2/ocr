/**
 * 
 */
package au.com.target.tgtfacades.login.impl;

import de.hybris.platform.servicelayer.user.UserService;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.login.TargetAuthenticationService;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.login.TargetAuthenticationFacade;


/**
 * @author rmcalave
 * 
 */
public class TargetAuthenticationFacadeImpl implements TargetAuthenticationFacade {

    private UserService userService;

    private TargetCustomerFacade targetCustomerFacade;

    private TargetAuthenticationService targetAuthenticationService;

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.login.impl.TargetAuthenticationFacade#isAccountLocked(java.lang.String)
     */
    @Override
    public boolean isAccountLocked(final String username) {
        final TargetCustomerModel customer = getTargetCustomerFacade().getCustomerForUsername(username);

        return isAccountLocked(customer);
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtfacades.login.TargetAuthenticationFacade#isAccountLocked(au.com.target.tgtcore.model.TargetCustomerModel)
     */
    @Override
    public boolean isAccountLocked(final TargetCustomerModel customer) {
        if (customer == null) {
            return false;
        }

        return getTargetAuthenticationService().checkLoginAttempts(customer);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.login.impl.TargetAuthenticationFacade#incrementFailedLoginAttemptCounter(java.lang.String)
     */
    @Override
    public void incrementFailedLoginAttemptCounter(final String username) {
        final TargetCustomerModel customer = getTargetCustomerFacade().getCustomerForUsername(username);

        if (customer == null) {
            return;
        }

        getTargetAuthenticationService().checkAndIncrementLoginAttempts(customer);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.login.impl.TargetAuthenticationFacade#resetFailedLoginAttemptCounter(java.lang.String)
     */
    @Override
    public void resetFailedLoginAttemptCounter(final String username) {
        final TargetCustomerModel customer = getTargetCustomerFacade().getCustomerForUsername(username);

        if (customer == null) {
            return;
        }

        getTargetAuthenticationService().resetLoginAttempts(customer);
    }

    /**
     * @return the userService
     */
    protected UserService getUserService() {
        return userService;
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @return the targetAuthenticationService
     */
    protected TargetAuthenticationService getTargetAuthenticationService() {
        return targetAuthenticationService;
    }

    /**
     * @param targetAuthenticationService
     *            the targetAuthenticationService to set
     */
    @Required
    public void setTargetAuthenticationService(final TargetAuthenticationService targetAuthenticationService) {
        this.targetAuthenticationService = targetAuthenticationService;
    }

    /**
     * @return the targetCustomerFacade
     */
    protected TargetCustomerFacade getTargetCustomerFacade() {
        return targetCustomerFacade;
    }

    /**
     * @param targetCustomerFacade
     *            the targetCustomerFacade to set
     */
    @Required
    public void setTargetCustomerFacade(final TargetCustomerFacade targetCustomerFacade) {
        this.targetCustomerFacade = targetCustomerFacade;
    }
}

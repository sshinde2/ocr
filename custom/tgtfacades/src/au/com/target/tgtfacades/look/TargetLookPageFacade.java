/**
 * 
 */
package au.com.target.tgtfacades.look;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtwebcore.model.cms2.pages.TargetProductGroupPageModel;


/**
 * @author ayushman
 *
 */
public interface TargetLookPageFacade {

    /**
     * Get Look Details for the look page
     * 
     * @param targetProductGroupPage
     * @return LookDetailsData
     */
    LookDetailsData getLookDetails(TargetProductGroupPageModel targetProductGroupPage);

    /**
     * Get all active looks for all colourvariants of the given base product
     * 
     * @param productModel
     * @param limit
     * @return list of TargetProductGroupPageModel
     */
    public List<LookDetailsData> getActiveLooksForProduct(final ProductModel productModel, int limit);

}
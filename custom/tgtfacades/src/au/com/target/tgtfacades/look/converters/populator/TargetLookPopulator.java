/**
 * 
 */
package au.com.target.tgtfacades.look.converters.populator;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtfacades.util.TargetPriceDataHelper;
import au.com.target.tgtwebcore.model.cms2.pages.TargetProductGroupPageModel;


/**
 * @author ayushman
 *
 */
public class TargetLookPopulator implements Populator<TargetProductGroupPageModel, LookDetailsData> {

    private TargetPriceDataHelper targetPriceDataHelper;

    /* (non-Javadoc)
     * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
     */

    @Override
    public void populate(final TargetProductGroupPageModel source, final LookDetailsData target)
            throws ConversionException {

        target.setVisible(true);
        target.setLabel(source.getLabel());
        target.setName(source.getName());

        if (null != source.getGroupImage()) {
            target.setGroupImgUrl(source.getGroupImage().getURL());
        }

        if (null != source.getGroupThumb()) {
            target.setGroupThumbUrl(source.getGroupThumb().getURL());
        }

        if (null != source.getFromPrice()) {
            final PriceData price = targetPriceDataHelper.populatePriceData(source.getFromPrice(),
                    PriceDataType.FROM);
            target.setPriceData(price);
        }
    }

    @Required
    public void setTargetPriceDataHelper(final TargetPriceDataHelper targetPriceDataHelper) {
        this.targetPriceDataHelper = targetPriceDataHelper;
    }
}

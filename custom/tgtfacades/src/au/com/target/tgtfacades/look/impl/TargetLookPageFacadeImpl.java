/**
 * 
 */
package au.com.target.tgtfacades.look.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.look.TargetLookPageFacade;
import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.shopthelook.LookService;
import au.com.target.tgtwebcore.looks.service.TargetProductGroupService;
import au.com.target.tgtwebcore.model.cms2.pages.TargetProductGroupPageModel;


/**
 * @author ayushman
 *
 */
public class TargetLookPageFacadeImpl implements TargetLookPageFacade {

    private Converter<TargetProductGroupPageModel, LookDetailsData> lookConverter;

    private TargetProductGroupService targetProductGroupService;

    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    private LookService lookService;

    private Converter<TargetLookModel, LookDetailsData> lookDataConverter;

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.look.TargetLookPageFacade#getLookDetails()
     */
    @Override
    public LookDetailsData getLookDetails(final TargetProductGroupPageModel page) {
        final LookDetailsData lookDetailsData = lookConverter.convert(page);
        return lookDetailsData;
    }

    @Override
    public List<LookDetailsData> getActiveLooksForProduct(final ProductModel productModel, final int limit) {
        Assert.notNull(productModel, "Product model can not be null");
        List<LookDetailsData> lookDetails = new ArrayList<>();

        if (targetFeatureSwitchFacade
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.STEP_SHOP_THE_LOOK)) {
            final List<TargetLookModel> looks = lookService.getVisibleLooksForProduct(productModel, limit);
            if (CollectionUtils.isNotEmpty(looks)) {
                for (final TargetLookModel look : looks) {
                    lookDetails.add(lookDataConverter.convert(look));
                }
            }
        }
        else {
            final List<TargetProductGroupPageModel> productGroupPages = targetProductGroupService
                    .getAllActiveLooksForProduct(productModel);
            if (CollectionUtils.isNotEmpty(productGroupPages)) {
                for (final TargetProductGroupPageModel groupPage : productGroupPages) {
                    final LookDetailsData lookData = lookConverter.convert(groupPage);
                    lookDetails.add(lookData);
                }
            }
            if (lookDetails.size() > limit) {
                lookDetails = lookDetails.subList(0, limit);
            }
        }

        return lookDetails;
    }

    /**
     * @param lookConverter
     *            the lookConverter to set
     */
    @Required
    public void setLookConverter(final Converter<TargetProductGroupPageModel, LookDetailsData> lookConverter) {
        this.lookConverter = lookConverter;
    }

    /**
     * @param targetProductGroupService
     *            the targetProductGroupService to set
     */
    @Required
    public void setTargetProductGroupService(final TargetProductGroupService targetProductGroupService) {
        this.targetProductGroupService = targetProductGroupService;
    }

    /**
     * @param targetFeatureSwitchFacade
     *            the targetFeatureSwitchFacade to set
     */
    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }

    /**
     * @param lookService
     *            the lookService to set
     */
    @Required
    public void setLookService(final LookService lookService) {
        this.lookService = lookService;
    }

    /**
     * @param lookDataConverter
     *            the lookDataConverter to set
     */
    @Required
    public void setLookDataConverter(final Converter<TargetLookModel, LookDetailsData> lookDataConverter) {
        this.lookDataConverter = lookDataConverter;
    }
}

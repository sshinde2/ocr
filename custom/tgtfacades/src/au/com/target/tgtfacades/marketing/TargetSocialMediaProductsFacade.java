/**
 * 
 */
package au.com.target.tgtfacades.marketing;

import au.com.target.tgtfacades.marketing.data.SocialMediaProductsListData;


/**
 * @author rmcalave
 *
 */
public interface TargetSocialMediaProductsFacade {

    /**
     * Get a list of <code>SocialMediaProductsData</code>, in the form of <code>SocialMediaProductsListData</code>,
     * ordered by descending creation date, limited by <code>count</code>.
     * 
     * @param count
     *            The maximum number of records to retrieve
     * @return A SocialMediaProductsListData
     */
    SocialMediaProductsListData getSocialMediaProductsByCreationTimeDescending(int count);

}
/**
 * 
 */
package au.com.target.tgtfacades.marketing.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtfacades.marketing.data.SocialMediaProductsData;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetLookProductModel;


/**
 * @author mgazal
 *
 */
public class TargetSocialMediaProductsDataPopulator
        implements Populator<TargetLookModel, SocialMediaProductsData> {

    /* (non-Javadoc)
     * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final TargetLookModel source, final SocialMediaProductsData target)
            throws ConversionException {
        target.setUrl(source.getInstagramUrl());

        if (CollectionUtils.isNotEmpty(source.getProducts())) {
            final List<String> productList = new ArrayList<>(source.getProducts().size());
            for (final TargetLookProductModel product : source.getProducts()) {
                productList.add(product.getProductCode());
            }
            target.setProductCodes(productList);
        }
    }
}

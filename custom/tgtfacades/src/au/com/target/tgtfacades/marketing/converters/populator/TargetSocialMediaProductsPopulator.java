/**
 * 
 */
package au.com.target.tgtfacades.marketing.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import au.com.target.tgtfacades.marketing.data.SocialMediaProductsData;
import au.com.target.tgtmarketing.model.SocialMediaProductsModel;


/**
 * @author rmcalave
 *
 */
public class TargetSocialMediaProductsPopulator
        implements Populator<SocialMediaProductsModel, SocialMediaProductsData> {

    /* (non-Javadoc)
     * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final SocialMediaProductsModel source, final SocialMediaProductsData target)
            throws ConversionException {
        target.setUrl(source.getUrl());

        final List<String> productList = Lists
                .newArrayList(Splitter.on(',').trimResults().split(source.getProductList()));
        target.setProductCodes(productList);
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.navigation;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.enums.LinkTargets;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author rmcalave
 * 
 */
public abstract class AbstractMenuBuilder {

    private Converter<CategoryModel, CategoryData> categoryConverter;

    private UrlResolver<CategoryData> categoryDataUrlResolver;

    protected NavigationMenuItem createNavigationMenuItem(final CMSNavigationEntryModel entry,
            final ContentPageModel currentPage,
            final Set<NavigationMenuItem> childMenuItems,
            final String name) {
        final ItemModel entryItem = entry.getItem();
        if (entryItem instanceof ContentPageModel) {
            return createNavigationMenuItemForContentPage((ContentPageModel)entryItem, currentPage, childMenuItems,
                    name);
        }
        else if (entryItem instanceof CMSLinkComponentModel) {
            return createNavigationMenuItemForLink((CMSLinkComponentModel)entryItem, childMenuItems, name);
        }
        else if (entryItem instanceof CategoryModel) {
            return createNavigationMenuItemForCategory((CategoryModel)entryItem, childMenuItems, name);
        }
        else {
            return null;
        }
    }

    protected NavigationMenuItem createNavigationMenuItemForContentPage(final ContentPageModel contentPage,
            final ContentPageModel currentPage,
            final Set<NavigationMenuItem> childMenuItems, final String name) {
        String menuItemName = name;

        if (StringUtils.isBlank(menuItemName)) {
            menuItemName = contentPage.getTitle();
        }

        if (StringUtils.isBlank(menuItemName)) {
            menuItemName = contentPage.getName();
        }

        final String link = contentPage.getLabel();

        if (currentPage == null) {
            return new NavigationMenuItem(menuItemName, link, childMenuItems, false, false, false);
        }
        else {
            return new NavigationMenuItem(menuItemName, link, childMenuItems, contentPage.equals(currentPage), false,
                    false);
        }
    }

    protected NavigationMenuItem createNavigationMenuItemForLink(final CMSLinkComponentModel cmsLink,
            final Set<NavigationMenuItem> childMenuItems, final String name) {
        if (Boolean.FALSE.equals(cmsLink.getVisible())) {
            return null;
        }

        String menuItemName = name;
        if (StringUtils.isBlank(menuItemName)) {
            menuItemName = cmsLink.getLinkName();
        }

        if (StringUtils.isBlank(menuItemName)) {
            menuItemName = cmsLink.getName();
        }

        final String link = cmsLink.getUrl();
        final boolean isNewWindow = LinkTargets.NEWWINDOW.equals(cmsLink.getTarget());
        final boolean isExternal = cmsLink.isExternal();

        return new NavigationMenuItem(menuItemName, link, childMenuItems, false, isNewWindow, isExternal);
    }

    protected NavigationMenuItem createNavigationMenuItemForCategory(final CategoryModel category,
            final Set<NavigationMenuItem> childMenuItems, final String name) {
        final CategoryData categoryData = categoryConverter.convert(category);

        String menuItemName = name;
        if (StringUtils.isBlank(name)) {
            menuItemName = categoryData.getName();
        }

        final String link = categoryDataUrlResolver.resolve(categoryData);

        return new NavigationMenuItem(menuItemName, link, childMenuItems, false, false, false);
    }

    protected boolean isValidEntryTypeInNavigationNode(final CMSNavigationNodeModel navigationNode) {
        final List<CMSNavigationEntryModel> entries = navigationNode.getEntries();

        if (CollectionUtils.isEmpty(entries)) {
            return false;
        }

        for (final CMSNavigationEntryModel entry : entries) {
            final ItemModel entryItem = entry.getItem();

            if (entryItem instanceof ContentPageModel ||
                    entryItem instanceof CMSLinkComponentModel ||
                    entryItem instanceof CategoryModel) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param categoryConverter
     *            the categoryConverter to set
     */
    @Required
    public void setCategoryConverter(final Converter<CategoryModel, CategoryData> categoryConverter) {
        this.categoryConverter = categoryConverter;
    }

    /**
     * @param categoryDataUrlResolver
     *            the categoryDataUrlResolver to set
     */
    @Required
    public void setCategoryDataUrlResolver(final UrlResolver<CategoryData> categoryDataUrlResolver) {
        this.categoryDataUrlResolver = categoryDataUrlResolver;
    }

}

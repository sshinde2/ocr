/**
 * 
 */
package au.com.target.tgtfacades.offer.converter;


import de.hybris.platform.converters.impl.AbstractPopulatingConverter;

import au.com.target.tgtfacades.offers.data.TargetMobileOfferHeadingData;
import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;


/**
 * @author pthoma20
 * 
 */
public class TargetMobileOfferHeadingConverter extends
        AbstractPopulatingConverter<TargetMobileOfferHeadingModel, TargetMobileOfferHeadingData> {

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.impl.AbstractPopulatingConverter#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final TargetMobileOfferHeadingModel source, final TargetMobileOfferHeadingData target) {
        super.populate(source, target);
    }

}

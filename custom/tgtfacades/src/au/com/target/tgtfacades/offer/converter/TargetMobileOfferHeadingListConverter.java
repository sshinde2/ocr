/**
 * 
 */
package au.com.target.tgtfacades.offer.converter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.offers.data.TargetMobileOfferHeadingData;
import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;


/**
 * @author pthoma20
 *
 */
public class TargetMobileOfferHeadingListConverter {


    private TargetMobileOfferHeadingConverter targetMobileOfferHeadingConverter;

    /**
     * For populating the Target Mobile Offer Heading information associated to a Deal or Voucher.
     * 
     * @param targetMobileOfferHeadingList
     * @return tgtMoboffrHeadingDataList
     */
    public List<TargetMobileOfferHeadingData> convert(
            final List<TargetMobileOfferHeadingModel> targetMobileOfferHeadingList) {
        if (CollectionUtils.isEmpty(targetMobileOfferHeadingList)) {
            return null;
        }
        final List<TargetMobileOfferHeadingData> tgtMoboffrHeadingDataList = new ArrayList<>();
        for (final TargetMobileOfferHeadingModel tgtMobOffrHdngModel : targetMobileOfferHeadingList) {
            final TargetMobileOfferHeadingData tgtMobOfferHeadingsData = targetMobileOfferHeadingConverter
                    .convert(tgtMobOffrHdngModel);
            tgtMoboffrHeadingDataList.add(tgtMobOfferHeadingsData);
        }
        return tgtMoboffrHeadingDataList;
    }

    /**
     * @param targetMobileOfferHeadingConverter
     *            the targetMobileOfferHeadingConverter to set
     */
    @Required
    public void setTargetMobileOfferHeadingConverter(
            final TargetMobileOfferHeadingConverter targetMobileOfferHeadingConverter) {
        this.targetMobileOfferHeadingConverter = targetMobileOfferHeadingConverter;
    }

}

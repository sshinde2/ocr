/**
 * 
 */
package au.com.target.tgtfacades.order;

import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;
import au.com.target.tgtcore.product.exception.ProductNotFoundException;
import au.com.target.tgtfacades.giftcards.exceptions.GiftCardValidationException;
import au.com.target.tgtfacades.order.data.DeliveryModeData;
import au.com.target.tgtfacades.order.data.TargetCartData;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public interface TargetCartFacade extends CartFacade {

    /**
     * 
     * @return true if all the items in the cart have delivery mode digital
     */
    public boolean isCartWithDigitalOnlyProducts();

    /**
     * 
     * @return all user carts
     */
    public Map<String, TargetCartData> getCarts();

    /**
     * @param productModel
     * @param qty
     * @return Modification in the cart
     * @throws CommerceCartModificationException
     */
    CartModificationData addToCart(final ProductModel productModel, final long qty)
            throws CommerceCartModificationException;

    /**
     * Add product to cart.
     * 
     * @param productCode
     * @param qty
     * @return Modification in the cart
     * @throws CommerceCartModificationException
     */
    CartModificationData addToCart(final String productCode, final long qty, final GiftRecipientDTO recipient)
            throws CommerceCartModificationException;

    /**
     * 
     * @param productModel
     * @param newQty
     * @return Modification in the cart
     * @throws CommerceCartModificationException
     */
    CartModificationData updateCartEntry(final ProductModel productModel, final long newQty)
            throws CommerceCartModificationException;


    /**
     * remove the recipient entry for the product from the cart
     * 
     * @param productCode
     * @param recipientEntryToRemove
     * @param id
     *            PK
     * @return Modification in the cart
     * @throws CommerceCartModificationException
     */
    CartModificationData removeGiftCardRecipientEntry(final String productCode, final int recipientEntryToRemove,
            String id)
                    throws CommerceCartModificationException;

    /**
     * 
     * @param productCode
     * @param newQty
     * @return Modification in the cart
     * @throws CommerceCartModificationException
     */
    CartModificationData updateCartEntry(final String productCode, final long newQty)
            throws CommerceCartModificationException;

    Map<String, DeliveryModeData> getDeliveryModeOptionsInformation();

    DeliveryModeData getDeliveryModeOptionInformation(String deliveryModeCode);

    /**
     * Check any cart have any Item
     * 
     * @return true if any cart have Item
     */
    boolean hasItemsInAnyCart();

    /**
     * Restore a previously saved cart for the current session user
     * 
     * @return CartRestorationData
     * @throws CommerceCartRestorationException
     */
    boolean restoreSavedCart() throws CommerceCartRestorationException;


    /**
     * to fetch guid for the current active cart.
     */
    String getGuidForCurrentSessionCart();

    /**
     * @param guid
     * @throws CommerceCartRestorationException
     */
    void restoreCart(final String guid) throws CommerceCartRestorationException;

    /**
     * Gets the postal code from cart or session.
     * 
     * @param cartData
     *
     * @return the postal code from cart or session
     */
    String getPostalCodeFromCartOrSession(CartData cartData);

    /**
     * @param postcode
     * @return catchment area
     */
    String getCatchmentAreaByPostcode(String postcode);

    /**
     * Validate gift cards
     * 
     * @param productCode
     * @param newQty
     * @throws GiftCardValidationException
     */
    void validateGiftCards(final String productCode, final long newQty)
            throws GiftCardValidationException, ProductNotFoundException;

    /**
     * Return the list of colorVariantCodes of cart products
     * 
     * @return productCodes
     */
    List<String> getCartsColorVariantProductCodes();

    /**
     * update the recipient entry
     * 
     * @param productCode
     * @param recipient
     * @param giftcardRecipientEntry
     * @param id
     *            PK
     * @return Modification in the cart
     * @throws CommerceCartModificationException
     */
    CartModificationData updateGiftCardDetails(final String productCode, final GiftRecipientDTO recipient,
            final int giftcardRecipientEntry, final String id)
                    throws CommerceCartModificationException;

    /**
     * validate and update quantity of product
     * 
     * @param productCode
     * @param qty
     * @throws GiftCardValidationException
     */
    void validateGiftCardsUpdateQty(String productCode, long qty)
            throws GiftCardValidationException;

    /**
     * Validate quantity of a product
     * 
     * @param productCode
     * @param qty
     * @throws GiftCardValidationException
     */
    public void validatePhysicalGiftCards(final String productCode, long qty)
            throws GiftCardValidationException;

    /**
     * @param productCode
     * @return true if product is a gift card and false otherwise
     */
    public boolean isProductAGiftCard(final String productCode);

    /**
     * 
     * @param productCode
     * @return true - if product is digital gift card
     */
    public boolean isProductADigitalGiftCard(final String productCode);


    /**
     * 
     * @param productCode
     * @return true if the product has any variants
     */
    public boolean hasVariantsForProduct(final String productCode);

    /**
     * 
     * @param productCode
     * @return true if the product has any size variants
     */
    public boolean hasSizeVariantsForProduct(final String productCode);

    /**
     * 
     * @param productCode
     * @return true if the product has any size variants
     */
    public boolean isProductAPhysicalGiftcard(final String productCode);
}

/**
 * 
 */
package au.com.target.tgtfacades.order;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.response.data.Response;


/**
 * This class is the checkout logging aspect which is currently configured for CheckoutServiceController. It can be
 * updated and configured for other targets if required.
 * 
 * @author htan3
 *
 */
@Component
public class TargetCheckoutLogger {
    public static final String LOG_FORMAT_BEGIN = "Receive API Request:method=%s, %s, Parameters:[%s]";
    public static final String LOG_FORMAT_FINISH = "Finish API Request:method=%s, %s, Result:[%s]";
    public static final String LOG_FORMAT_FINISH_ERROR = "Finish API Request with Error:method=%s, %s, Result:[%s]";
    public static final String LOG_FORMAT_FINISH_EX = "Finish API Request with Exception:method=%s, %s, Exception Thrown:[%s]";
    private static final Logger LOG = Logger.getLogger("Checkout SPC");
    private static final ThreadLocal<String> CARTINFO = new ThreadLocal<>();

    @Autowired
    private TargetCheckoutFacade targetCheckoutFacade;

    /**
     * to log method name and parameters before method execution
     * 
     * @param joinPoint
     */
    public void beforeAction(final JoinPoint joinPoint) {
        LOG.info(String.format(LOG_FORMAT_BEGIN, joinPoint.getSignature().getName(), getCartInfo(true),
                getArgs(joinPoint)));
    }


    /**
     * to log method name and return value after method execution
     * 
     * @param joinPoint
     * @param returnValue
     */
    public void afterReturnAction(final JoinPoint joinPoint, final Object returnValue) {

        if (responseSucessful(returnValue)) {
            LOG.info(String.format(LOG_FORMAT_FINISH, joinPoint.getSignature().getName(), getCartInfo(),
                    getResult(returnValue)));
        }
        else {
            LOG.error(String.format(LOG_FORMAT_FINISH_ERROR, joinPoint.getSignature().getName(), getCartInfo(),
                    getResult(returnValue)));
        }
        CARTINFO.remove();
    }

    private boolean responseSucessful(final Object returnValue) {
        if (returnValue instanceof Response && !((Response)returnValue).isSuccess()) {
            return false;
        }
        return true;
    }

    /**
     * to log exception if thrown from method execution
     * 
     * @param joinPoint
     */
    public void afterThrow(final JoinPoint joinPoint, final Exception exception) {
        LOG.error(String.format(LOG_FORMAT_FINISH_EX, joinPoint.getSignature().getName(), getCartInfo(),
                exception.getClass().getSimpleName() + "|" + exception.getMessage()), exception);
        CARTINFO.remove();
    }

    /**
     * Return cart info attached to current thread, if no result then try to fetch it from TargetCheckoutFacade.
     * 
     * @return String
     */
    public String getCartInfo() {
        return getCartInfo(false);
    }

    /**
     * Return cart info attached to current thread.
     * 
     * @return String
     */
    public String getCurrentCartInfo() {
        return CARTINFO.get();
    }

    /**
     * return method arguments
     * 
     * @param joinPoint
     * @return joint string with all argument values
     */
    private String getArgs(final JoinPoint joinPoint) {
        final StringBuilder sb = new StringBuilder();
        final Object[] args = joinPoint.getArgs();

        if (null != args) {
            for (final Object arg : args) {
                if (!(arg instanceof Model || arg instanceof HttpServletRequest)) {
                    sb.append(arg);
                    sb.append("|");
                }
            }
        }
        return sb.toString();
    }

    /**
     * return string value of the return object. please implement toString of the return class properly.
     * 
     * @param returnValue
     * @return string
     */
    private String getResult(final Object returnValue) {
        if (returnValue != null) {
            return returnValue.toString();
        }
        return null;
    }

    /**
     * get session cart code and user email for current cart.
     * 
     * @param refresh
     * @return string value of cart code | user uid
     */
    private String getCartInfo(final boolean refresh) {
        if (refresh || CARTINFO.get() == null) {
            CARTINFO.remove();
            final CartModel cartModel = targetCheckoutFacade.getCart();
            if (cartModel != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("cartCode=");
                sb.append(cartModel.getCode());
                final UserModel user = cartModel.getUser();
                if (user != null && user instanceof TargetCustomerModel) {
                    final TargetCustomerModel customerModel = (TargetCustomerModel)user;
                    sb.append(", customerID=");
                    sb.append(customerModel.getCustomerID());
                }
                CARTINFO.set(sb.toString());
            }
        }
        return CARTINFO.get();
    }
}

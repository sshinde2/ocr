/**
 * 
 */
package au.com.target.tgtfacades.order;

/**
 * @author rmcalave
 * 
 */
public interface TargetDiscountFacade {
    /**
     * Validate the provided team member discount card number.
     * 
     * @param cardNumber
     *            The card number to validate
     * @return true if the number is valid, false otherwise
     */
    boolean validateTeamMemberDiscountCard(String cardNumber);
}

/**
 * 
 */
package au.com.target.tgtfacades.order;

import de.hybris.platform.commercefacades.order.OrderFacade;

import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.order.exception.TargetUpdateCardException;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;



/**
 * @author Benoit Vanalderweireldt
 * 
 */
public interface TargetOrderFacade extends OrderFacade {

    @Override
    public TargetOrderData getOrderDetailsForCode(String code);

    /**
     * Fetch order details for code under current session user.
     * 
     * @param code
     * @return TargetOrderData
     */
    TargetOrderData getOrderDetailsForCodeUnderCurrentCustomer(String code);

    /**
     * Fetch last transaction status for order code under current session
     * 
     * @param code
     * @return true if transaction status is pending else false
     */
    boolean getOrderLastTransactionDetailsForCodeUnderCurrentCustomer(String code);

    /**
     * Link an order to the given customer
     * 
     * @param orderId
     */
    void linkOrderToCustomer(String orderId);

    /**
     * Create a update card iFrame Url
     * 
     * @param orderId
     * @param cancelUrl
     * @param returnUrl
     * @return iFrameUrl
     * @throws TargetUpdateCardException
     */
    String createIpgUrl(String orderId, String cancelUrl, String returnUrl) throws TargetUpdateCardException;

    /**
     * update credit card details for the order.
     * 
     * @param orderId
     * @param ipgToken
     *            - token from IPG
     * @return {@link TargetQueryTransactionDetailsResult}
     */
    boolean isUpdateCreditCardDetailsSuccessful(String orderId, String ipgToken);
}

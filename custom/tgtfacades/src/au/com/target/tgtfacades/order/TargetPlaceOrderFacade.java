/**
 * 
 */
package au.com.target.tgtfacades.order;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;

import java.util.List;

import au.com.target.tgtfacades.order.data.TargetPlaceOrderRequest;
import au.com.target.tgtfacades.order.data.TargetPlaceOrderResult;
import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;



/**
 * @author rmcalave
 * 
 */
public interface TargetPlaceOrderFacade {

    /**
     * Place the order for given request. the intension of this new interface is to cover as much business logic as
     * possible in one single facade call instead of controller making multiple facade calls depends on logic. it is
     * currently used for Single Page Checkout only and currently only for happy path. once complete, should retire the
     * original placeOrder()
     * 
     * @return The {@link TargetPlaceOrderResult} containing the result status and, if applicable, the order data
     */
    TargetPlaceOrderResult placeOrder(TargetPlaceOrderRequest request);

    /**
     * Place the order.
     * 
     * @return The {@link TargetPlaceOrderResult} containing the result status and, if applicable, the order data
     */
    TargetPlaceOrderResult placeOrder();

    /**
     * Allocate stock and capture funds. If capture fails, reallocate the stock
     * 
     * @return The {@link TargetPlaceOrderResult} containing the result status. null if it succeeds
     */
    TargetPlaceOrderResult prepareForPlaceOrder();

    /**
     * Creates the order and performs post order creation processes
     * 
     * @return The {@link TargetPlaceOrderResult} containing the result status
     */
    TargetPlaceOrderResult finishPlaceOrder();

    /**
     * @param ipAddress
     * @param browserCookies
     */
    void setClientDetailsForFraudDetection(String ipAddress, String browserCookies);

    /**
     * Set the transaction ID from PayPal Here to the payment transaction entry on the order. Sets the transaction ID
     * into both the code and requestId fields on {@link de.hybris.platform.payment.model.PaymentTransactionEntryModel}.
     * 
     * 
     * @param transactionId
     *            the transactionId
     */
    void addPayPalTransactionIdToPaymentTransactionEntry(final String transactionId);

    /**
     * Get gift card payment information
     * 
     * @param cartModel
     *            - cart
     * @return - list of gift cards associated with payment
     */
    List<TargetCardResult> getGiftCardPayments(CartModel cartModel);

    /**
     * Reverse gift card payments
     * 
     * @return - has gift card reversal successful
     */
    boolean reverseGiftCardPayment();

    /**
     * Get query transaction details
     * 
     * @return query transaction details
     */
    TargetQueryTransactionDetailsResult getQueryTransactionDetailResults();

    /**
     * @param ipgToken
     * @param ipgSessionId
     */
    boolean reverseGiftCardPayment(String ipgToken, String ipgSessionId);

    /**
     * Get the amount paid so far for the given cart
     * 
     * @param cartModel
     * @return amount paid
     */
    Double getAmountPaidSoFar(CartModel cartModel);

    /**
     * Get query transaction details
     * 
     * @return query transaction details
     */
    TargetQueryTransactionDetailsResult getQueryTransactionDetailResults(AbstractOrderModel orderModel);
}

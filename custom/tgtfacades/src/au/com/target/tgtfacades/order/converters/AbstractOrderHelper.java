/**
 * 
 */
package au.com.target.tgtfacades.order.converters;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.PromotionResultData;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.promotions.jalo.ProductPromotion;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Currency;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.delivery.dto.DeliveryCostDto;
import au.com.target.tgtcore.delivery.dto.DeliveryCostEnumType;
import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtcore.util.ProductUtil;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.delivery.data.ShipsterData;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.order.data.LayByDueDateData;
import au.com.target.tgtfacades.order.data.LayByPaymentData;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtfacades.util.PriceFormatUtils;
import au.com.target.tgtfacades.util.TargetPriceHelper;
import au.com.target.tgtlayby.data.PaymentDueData;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;


/**
 * @author Benoit VanalderWeireldt
 * 
 */
public class AbstractOrderHelper {

    public static final FastDateFormat DUE_DATE_FORMATTER = FastDateFormat.getInstance("dd/MM/yyyy");
    public static final DecimalFormat PERCENTAGE_FORMATTER = new DecimalFormat("#.##'%'");
    public static final NumberFormat ABSOLUTE_AMOUNT_FORMATTER = NumberFormat
            .getCurrencyInstance(new Locale("en", "AU"));
    private static final Logger LOG = Logger.getLogger(AbstractOrderHelper.class);

    static {
        PERCENTAGE_FORMATTER.setDecimalSeparatorAlwaysShown(false);
        ABSOLUTE_AMOUNT_FORMATTER.setCurrency(Currency.getInstance("AUD"));
    }

    private PriceDataFactory priceDataFactory;

    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    private TargetDeliveryService targetDeliveryService;

    private Converter<TargetZoneDeliveryModeModel, TargetZoneDeliveryModeData> targetZoneDeliveryModeConverter;

    private SessionService sessionService;

    private ConfigurationService configurationService;

    private TargetSalesApplicationService targetSalesApplicationService;

    @Resource(name = "targetPriceHelper")
    private TargetPriceHelper targetPriceHelper;

    /**
     * 
     * @param abstractOrderModel
     * @return true if the order is layby
     */
    public boolean isLayBy(final AbstractOrderModel abstractOrderModel) {
        final PurchaseOptionConfigModel purchOptConf = abstractOrderModel.getPurchaseOptionConfig();
        return (purchOptConf != null && BooleanUtils.isTrue(purchOptConf.getAllowPaymentDues()));
    }

    /**
     * 
     * @param paymentDueDatas
     * @return a list of layby due dates
     */
    public List<LayByDueDateData> createAmountDueDates(final Set<PaymentDueData> paymentDueDatas) {

        if (CollectionUtils.isEmpty(paymentDueDatas)) {
            return null;
        }
        final List<PaymentDueData> paymentDueDatasList = new ArrayList<>();
        paymentDueDatasList.addAll(paymentDueDatas);

        Collections.sort(paymentDueDatasList, new Comparator<PaymentDueData>() {
            @Override
            public int compare(final PaymentDueData arg0, final PaymentDueData arg1) {
                return Double.valueOf(arg0.getPercentage()).compareTo(Double.valueOf(arg1.getPercentage()));
            }
        });

        final List<LayByDueDateData> dueDates = new ArrayList<>();

        for (final PaymentDueData laybyPaymentDuesData : paymentDueDatasList) {
            if (laybyPaymentDuesData.getDueDate() != null) {
                final String formattedDueDate = DUE_DATE_FORMATTER.format(laybyPaymentDuesData.getDueDate());
                final String formattedAmount = createFormattedAmount(laybyPaymentDuesData);

                if (StringUtils.isNotBlank(formattedAmount) && StringUtils.isNotBlank(formattedDueDate)) {
                    final LayByDueDateData dueDateData = new LayByDueDateData();

                    dueDateData.setFormattedAmount(formattedAmount);
                    dueDateData.setFormattedDueDate(formattedDueDate);

                    dueDates.add(dueDateData);
                }
            }
        }
        return dueDates;
    }

    /**
     * 
     * @param order
     * 
     * @return a true if the order contains a digital product
     */

    public boolean hasDigitalProducts(final AbstractOrderModel order) {
        return targetDeliveryModeHelper.hasDigitalProducts(order);
    }

    /**
     * 
     * @param laybyPaymentDuesData
     * @return formatted String
     */

    public String createFormattedAmount(final PaymentDueData laybyPaymentDuesData) {
        if (laybyPaymentDuesData == null) {
            return null;
        }

        String formattedAmount = null;
        if (Double.valueOf(laybyPaymentDuesData.getPercentage()).compareTo(Double.valueOf(0)) != 0) {
            formattedAmount = PERCENTAGE_FORMATTER.format(laybyPaymentDuesData.getPercentage());
        }
        else {
            formattedAmount = ABSOLUTE_AMOUNT_FORMATTER.format(laybyPaymentDuesData.getAmount());
        }

        return formattedAmount;
    }

    /**
     * 
     * @param source
     * @param val
     * @return PriceData
     */
    public PriceData createPrice(final AbstractOrderModel source, final double val) {
        return createPrice(source, BigDecimal.valueOf(val));
    }

    /**
     * @param source
     * @param amount
     * @return formatted price data
     */
    public PriceData createPrice(final AbstractOrderModel source, final BigDecimal amount) {
        if (source == null) {
            throw new IllegalArgumentException("source order must not be null");
        }

        final CurrencyModel currency = source.getCurrency();
        if (currency == null) {
            throw new IllegalArgumentException("source order currency must not be null");
        }

        return priceDataFactory.create(PriceDataType.BUY, amount, currency.getIsocode());
    }


    /**
     * 
     * @param cart
     * @param paymentDueData
     * @return LayByPaymentData
     */
    public LayByPaymentData createMinimumPaymentAmountData(final AbstractOrderModel cart,
            final PaymentDueData paymentDueData) {
        final double dueAmount = paymentDueData.getAmount();

        final LayByPaymentData minimumPaymentAmount = new LayByPaymentData();

        minimumPaymentAmount.setActualAmount(createPrice(cart, dueAmount));

        if (Double.valueOf(paymentDueData.getPercentage()).compareTo(Double.valueOf(0)) != 0) {
            minimumPaymentAmount.setPercentage(PERCENTAGE_FORMATTER.format(paymentDueData.getPercentage()));
        }

        return minimumPaymentAmount;
    }

    /**
     * 
     * @param cart
     * @return LayByPaymentData
     */
    public LayByPaymentData createMaximumPaymentAmountData(final AbstractOrderModel cart) {
        final Double maxAmountPercentage = cart.getPurchaseOptionConfig().getMaxPercentage();

        final LayByPaymentData maximumPaymentAmount = new LayByPaymentData();

        final BigDecimal totalPrice = BigDecimal.valueOf(cart.getTotalPrice().doubleValue());
        final BigDecimal maxAmountPercentageDecimal = BigDecimal.valueOf(maxAmountPercentage.doubleValue()).divide(
                BigDecimal.valueOf(100.0d));
        final BigDecimal maxPayTodayAmount = totalPrice.multiply(maxAmountPercentageDecimal);

        maximumPaymentAmount.setActualAmount(createPrice(cart, maxPayTodayAmount));
        maximumPaymentAmount.setPercentage(PERCENTAGE_FORMATTER.format(maxAmountPercentage));

        return maximumPaymentAmount;
    }

    /**
     * Adds the target product promotions.
     * 
     * @param promotionOrderResults
     *            the all results
     * @param modelService
     *            the model service
     * @param promotionResultConverter
     *            the promotion result converter
     * @param prototype
     *            the prototype
     */
    public static void addTargetProductPromotions(
            final PromotionOrderResults promotionOrderResults,
            final ModelService modelService,
            final Converter<PromotionResultModel, PromotionResultData> promotionResultConverter,
            final AbstractOrderData prototype) {

        if (null != promotionOrderResults) {
            final List<PromotionResultModel> appliedPromotions = new ArrayList<>();
            for (final PromotionResult promo : promotionOrderResults.getAllResults()) {
                if (promo.isApplied() && promo.getPromotion() instanceof ProductPromotion) {
                    appliedPromotions.add((PromotionResultModel)modelService.get(promo));
                }
            }
            prototype.setAppliedProductPromotions(Converters.convertAll(appliedPromotions, promotionResultConverter));
        }
    }

    /**
     * Adds the target order promotions.
     * 
     * @param promotionOrderResults
     *            the promotion order results
     * @param modelService
     *            the model service
     * @param promotionResultConverter
     *            the promotion result converter
     * @param prototype
     *            the prototype
     */
    public static void addTargetOrderPromotions(
            final PromotionOrderResults promotionOrderResults,
            final ModelService modelService,
            final Converter<PromotionResultModel, PromotionResultData> promotionResultConverter,
            final AbstractOrderData prototype) {

        if (null != promotionOrderResults) {
            final List<PromotionResultModel> appliedPromotions = new ArrayList<>();
            for (final PromotionResult promo : promotionOrderResults.getAllResults()) {
                if (promo.isApplied() && !(promo.getPromotion() instanceof ProductPromotion)) {
                    appliedPromotions.add((PromotionResultModel)modelService.get(promo));
                }
            }
            prototype.setAppliedOrderPromotions(Converters.convertAll(appliedPromotions, promotionResultConverter));
        }
    }

    /**
     * Get set of physical product types for given cart
     * 
     * @param cartModel
     * @return set of physical product types for given cart
     */
    public Set<String> getProductTypeSet(final AbstractOrderModel cartModel) {
        Assert.notNull(cartModel, "Cart Model should not be null");
        final List<AbstractOrderEntryModel> orderEntries = cartModel.getEntries();
        final Set<String> productTypeModelSet = new HashSet<>();
        if (orderEntries != null) {
            for (final AbstractOrderEntryModel entry : orderEntries) {
                final ProductModel product = entry.getProduct();
                final AbstractTargetVariantProductModel targetProduct = (AbstractTargetVariantProductModel)product;
                if (targetDeliveryModeHelper.doesProductHavePhysicalDeliveryMode(targetProduct)) {
                    final ProductTypeModel productTypeModel = targetProduct.getProductType();
                    productTypeModelSet.add(productTypeModel.getCode());
                }
            }
        }
        return productTypeModelSet;
    }

    /**
     * @param cartProductTypeSet
     * @param posData
     */
    public void updateStoreWithPickupAvailability(final Set<String> cartProductTypeSet,
            final PointOfServiceData posData) {
        final TargetPointOfServiceData targetPosData = (TargetPointOfServiceData)posData;
        if (Boolean.FALSE.equals(targetPosData.getClosed()) && Boolean.TRUE.equals(targetPosData.getAcceptCNC())) {
            targetPosData.setAvailableForPickup(Boolean.valueOf(CollectionUtils.isSubCollection(cartProductTypeSet,
                    targetPosData.getAcceptedProductTypes())));
        }
        else {
            targetPosData.setAvailableForPickup(Boolean.FALSE);
        }
    }

    /**
     * @param source
     * @return List<TargetZoneDeliveryModeData>
     */
    public List<TargetZoneDeliveryModeData> getPhysicalDeliveryModes(final CartModel source) {
        final List<TargetZoneDeliveryModeData> targetZoneDeliveryModeDataList = new ArrayList<>();
        final Collection<TargetZoneDeliveryModeModel> targetZoneDeliveryModeModelList = targetDeliveryService
                .getAllCurrentlyActiveDeliveryModes(targetSalesApplicationService.getCurrentSalesApplication());
        boolean hasAtleastOnePhysicalValidDM = false;

        final ProductModel giftCardProduct = getPhysicalGiftCardProductInCart(source.getEntries());

        for (final TargetZoneDeliveryModeModel targetZoneDeliveryModeModel : targetZoneDeliveryModeModelList) {
            //getting the converted deliveryModeData
            final TargetZoneDeliveryModeData targetZoneDeliveryModeData = targetZoneDeliveryModeConverter
                    .convert(targetZoneDeliveryModeModel);

            // getting the list of mode value available for the cart
            boolean hasAtleastOneDMValue = false;
            List<AbstractTargetZoneDeliveryModeValueModel> valueModelList = null;
            List<AbstractTargetZoneDeliveryModeValueModel> potentialValueModeList = null;
            List<AbstractTargetZoneDeliveryModeValueModel> physicalGiftCardValueModeList = null;
            try {
                if (null != giftCardProduct) {
                    physicalGiftCardValueModeList = getDeliveryModeValuesForPhysicalGiftCard(
                            targetZoneDeliveryModeModel,
                            giftCardProduct);
                }
                valueModelList = targetDeliveryService
                        .getAllApplicableDeliveryValuesForModeAndOrder(targetZoneDeliveryModeModel, source);
                hasAtleastOneDMValue = CollectionUtils.isNotEmpty(valueModelList);
                potentialValueModeList = targetDeliveryService
                        .getAllApplicableDeliveryValuesForModeAndOrderWithoutOrderThreshold(
                                targetZoneDeliveryModeModel,
                                source);
                targetZoneDeliveryModeData.setAvailable(false);
            }
            catch (final TargetNoPostCodeException e) {
                hasAtleastOneDMValue = true;
                valueModelList = null;
                potentialValueModeList = null;
            }
            DeliveryCostDto costDto = null;
            if (CollectionUtils.isNotEmpty(valueModelList)) {
                costDto = targetDeliveryService.getDeliveryCostForDeliveryType(valueModelList.get(0),
                        source);
            }

            hasAtleastOnePhysicalValidDM = setDeliveryCost(source,
                    hasAtleastOnePhysicalValidDM,
                    targetZoneDeliveryModeModel,
                    targetZoneDeliveryModeData, hasAtleastOneDMValue, valueModelList, costDto,
                    ObjectUtils.toString(sessionService.getAttribute(TgtCoreConstants.SESSION_POSTCODE)));

            setDeliveryModeDescriptionMessages(targetZoneDeliveryModeData, hasAtleastOneDMValue, valueModelList,
                    physicalGiftCardValueModeList, source);
            setShipsterData(costDto, targetZoneDeliveryModeData);
            if (!targetDeliveryService.containsBulkyProducts(source.getEntries())
                    && CollectionUtils.isNotEmpty(potentialValueModeList)
                    && (CollectionUtils.isNotEmpty(valueModelList))
                    && !BooleanUtils.isTrue(source.getContainsPreOrderItems())) {
                final AbstractTargetZoneDeliveryModeValueModel targetZoneDeliveryModeValueModel = potentialValueModeList
                        .get(0);

                if (!targetZoneDeliveryModeValueModel.equals(valueModelList.get(0))) {
                    final String potentialIncentiveMessage = getPotentialIncentiveMessage(
                            targetZoneDeliveryModeValueModel,
                            source);
                    targetZoneDeliveryModeData.setPotentialIncentiveMessage(potentialIncentiveMessage);
                }
                else {
                    final String incentiveMetMessage = getIncentiveMetMessage(targetZoneDeliveryModeValueModel,
                            source);
                    targetZoneDeliveryModeData.setIncentiveMetMessage(incentiveMetMessage);
                }
            }

            if (BooleanUtils.isFalse(source.getContainsPreOrderItems())
                    || targetZoneDeliveryModeModel.isAvailableForPreOrder()) {
                targetZoneDeliveryModeDataList.add(targetZoneDeliveryModeData);
            }
        }

        if (hasAtleastOnePhysicalValidDM) {
            return targetZoneDeliveryModeDataList;
        }
        return null;
    }


    /**
     * @param targetZoneDeliveryModeModel
     * @param giftCardProduct
     * @return AbstractTargetZoneDeliveryModeValueModel list
     */
    private List<AbstractTargetZoneDeliveryModeValueModel> getDeliveryModeValuesForPhysicalGiftCard(
            final TargetZoneDeliveryModeModel targetZoneDeliveryModeModel,
            final ProductModel giftCardProduct) {
        final List<AbstractTargetZoneDeliveryModeValueModel> physicalGiftCardValueModeList = targetDeliveryService
                .getAllApplicableDeliveryValuesForModeAndProduct(targetZoneDeliveryModeModel, giftCardProduct);
        return physicalGiftCardValueModeList;
    }

    /**
     * @param cartEntries
     * @return giftCardProductModel
     */
    private ProductModel getPhysicalGiftCardProductInCart(final List<AbstractOrderEntryModel> cartEntries) {

        for (final AbstractOrderEntryModel entryModel : cartEntries) {
            if (ProductUtil.isProductTypePhysicalGiftcard(entryModel.getProduct())) {
                return entryModel.getProduct();
            }
        }
        return null;
    }

    /**
     * This method will set the Long message either Normal Products or Pre-order products
     * 
     * @param cartModel
     * @param deliveryValueModel
     * @return String
     */
    private String getLongDescriptionMessage(final CartModel cartModel,
            final AbstractTargetZoneDeliveryModeValueModel deliveryValueModel) {

        if (BooleanUtils.isTrue(cartModel.getContainsPreOrderItems())) {
            return deliveryValueModel.getPreOrderLongMessage() != null ? deliveryValueModel.getPreOrderLongMessage()
                    : deliveryValueModel.getMessage();
        }

        return deliveryValueModel.getMessage();
    }

    /**
     * @param source
     * @param hasAtleastOneValidDM
     * @param targetZoneDeliveryModeModel
     * @param targetZoneDeliveryModeData
     * @param hasAtleastOneDMValue
     * @param valueModelList
     * @return boolean
     */
    public boolean setDeliveryCost(final CartModel source, boolean hasAtleastOneValidDM,
            final TargetZoneDeliveryModeModel targetZoneDeliveryModeModel,
            final TargetZoneDeliveryModeData targetZoneDeliveryModeData, final boolean hasAtleastOneDMValue,
            final List<AbstractTargetZoneDeliveryModeValueModel> valueModelList, final DeliveryCostDto costDto,
            final String postCode) {
        if (targetDeliveryModeHelper
                .isDeliveryModeApplicableForOrder(source, targetZoneDeliveryModeModel)
                && hasAtleastOneDMValue) {
            if (!targetZoneDeliveryModeModel.getIsShortDeliveryTime().booleanValue()
                    || StringUtils.isEmpty(postCode) ||
                    targetDeliveryService.isDeliveryModePostCodeCombinationValid(targetZoneDeliveryModeModel,
                            postCode, source)) {
                targetZoneDeliveryModeData.setAvailable(true);
                hasAtleastOneValidDM = true;
                if (CollectionUtils.isNotEmpty(valueModelList) && costDto != null) {
                    final double fee = costDto.getDeliveryCost();
                    targetZoneDeliveryModeData.setDeliveryCost(priceDataFactory.create(
                            PriceDataType.BUY, BigDecimal.valueOf(fee), valueModelList.get(0).getCurrency()));
                }
            }
            else {
                targetZoneDeliveryModeData.setPostCodeNotSupported(true);
            }
        }
        return hasAtleastOneValidDM;
    }


    /**
     * 
     * @param order
     * 
     * @return a true if the order contains a digital product
     */

    public boolean hasPhysicalGiftCards(final AbstractOrderModel order) {
        if (null == order) {
            return false;
        }
        final List<AbstractOrderEntryModel> orderEntries = order.getEntries();
        if (CollectionUtils.isNotEmpty(orderEntries)) {
            for (final AbstractOrderEntryModel entryModel : orderEntries) {
                final ProductModel product = entryModel.getProduct();
                if (ProductUtil.isProductTypePhysicalGiftcard(product)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Set Shipster details
     * 
     * @param costDto
     * @param targetZoneDeliveryModeData
     */
    private void setShipsterData(final DeliveryCostDto costDto,
            final TargetZoneDeliveryModeData targetZoneDeliveryModeData) {
        final ShipsterData shipsterData = new ShipsterData();
        if (costDto != null) {
            if (costDto.getType() == DeliveryCostEnumType.SHIPSTERFREEDELIVERY) {
                shipsterData.setAvailable(true);
            }
            shipsterData
                    .setReason(
                            costDto.getShipsterStatusCode() == null ? "" : costDto.getShipsterStatusCode().toString());
        }
        targetZoneDeliveryModeData.setShipsterData(shipsterData);
    }



    /**
     * @param targetZoneDeliveryModeData
     * @param hasAtleastOneDMValue
     * @param valueModelList
     * @param physicalGiftCardValueModeList
     */
    private void setDeliveryModeDescriptionMessages(final TargetZoneDeliveryModeData targetZoneDeliveryModeData,
            final boolean hasAtleastOneDMValue, final List<AbstractTargetZoneDeliveryModeValueModel> valueModelList,
            final List<AbstractTargetZoneDeliveryModeValueModel> physicalGiftCardValueModeList,
            final CartModel cartModel) {

        if (hasAtleastOneDMValue) {
            if (CollectionUtils.isNotEmpty(valueModelList)) {

                final AbstractTargetZoneDeliveryModeValueModel deliveryValueModel = valueModelList.get(0);
                targetZoneDeliveryModeData.setLongDescription(getLongDescriptionMessage(cartModel, deliveryValueModel));
                targetZoneDeliveryModeData.setShortDescription(deliveryValueModel
                        .getShortMessage());
                targetZoneDeliveryModeData.setDisclaimer(deliveryValueModel.getDisclaimer());
            }
            else if (CollectionUtils.isNotEmpty(physicalGiftCardValueModeList)
                    && CollectionUtils.isEmpty(valueModelList)) {
                final AbstractTargetZoneDeliveryModeValueModel giftCardDeliveryValueModel = physicalGiftCardValueModeList
                        .get(0);
                final PriceData priceData = targetPriceHelper
                        .createPriceData(giftCardDeliveryValueModel.getValue());
                final String longMessage = MessageFormat.format(configurationService.getConfiguration().getString(
                        TgtFacadesConstants.NO_DEL_MODE_VALUE_LONG_MSG_WITH_GIFTCARD),
                        priceData.getFormattedValue());
                targetZoneDeliveryModeData
                        .setLongDescription(longMessage);
            }
            else {
                targetZoneDeliveryModeData
                        .setLongDescription(configurationService.getConfiguration().getString(
                                TgtFacadesConstants.NO_DEL_MODE_VALUE_LONG_MSG));
            }

        }
    }

    /**
     * @param targetZoneDeliveryModeValueModel
     * @param cart
     * @return potentialIncentiveMessage
     */
    protected String getPotentialIncentiveMessage(
            final AbstractTargetZoneDeliveryModeValueModel targetZoneDeliveryModeValueModel, final CartModel cart) {
        final String potentialIncentiveMessageFromModel = targetZoneDeliveryModeValueModel
                .getPotentialIncentiveMessage();
        if (StringUtils.isEmpty(potentialIncentiveMessageFromModel)) {
            return null;
        }

        final BigDecimal determinantThreshold = BigDecimal.valueOf(targetZoneDeliveryModeValueModel
                .getMinimum().doubleValue());
        final BigDecimal cartSubtotal = BigDecimal.valueOf(cart.getSubtotal().doubleValue());
        final BigDecimal amountToQualify = determinantThreshold.subtract(cartSubtotal);

        final BigDecimal deliveryFee = BigDecimal.valueOf(targetZoneDeliveryModeValueModel.getValue().doubleValue());

        final PriceData amountToQualifyPriceData = priceDataFactory.create(PriceDataType.BUY, amountToQualify,
                cart.getCurrency().getIsocode());
        final PriceData deliveryFeePriceData = priceDataFactory.create(PriceDataType.BUY, deliveryFee,
                cart.getCurrency().getIsocode());

        try {
            return MessageFormat.format(potentialIncentiveMessageFromModel,
                    PriceFormatUtils.formatPrice(amountToQualifyPriceData.getFormattedValue()),
                    PriceFormatUtils.formatPrice(deliveryFeePriceData.getFormattedValue()));
        }
        catch (final IllegalArgumentException ex) {
            LOG.error("Invalid potential incentive message for AbstractTargetZoneDeliveryModeValueModel with PK "
                    + targetZoneDeliveryModeValueModel.getPk().toString());
            return "";
        }
    }

    protected String getIncentiveMetMessage(
            final AbstractTargetZoneDeliveryModeValueModel targetZoneDeliveryModeValueModel, final CartModel cart) {
        final String incentiveMetMessageFromModel = targetZoneDeliveryModeValueModel
                .getIncentiveMetMessage();
        if (StringUtils.isEmpty(incentiveMetMessageFromModel)) {
            return null;
        }

        final BigDecimal deliveryFee = BigDecimal.valueOf(targetZoneDeliveryModeValueModel.getValue().doubleValue());
        final PriceData deliveryFeePriceData = priceDataFactory.create(PriceDataType.BUY, deliveryFee,
                cart.getCurrency().getIsocode());

        try {
            return MessageFormat.format(incentiveMetMessageFromModel,
                    PriceFormatUtils.formatPrice(deliveryFeePriceData.getFormattedValue()));
        }
        catch (final IllegalArgumentException ex) {
            LOG.error("Invalid incentive met message for AbstractTargetZoneDeliveryModeValueModel with PK "
                    + targetZoneDeliveryModeValueModel.getPk().toString());
            return "";
        }
    }

    /**
     * @param priceDataFactory
     *            the priceDataFactory to set
     */
    @Required
    public void setPriceDataFactory(final PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }

    @Required
    public void setTargetDeliveryModeHelper(final TargetDeliveryModeHelper targetDeliveryModeHelper) {
        this.targetDeliveryModeHelper = targetDeliveryModeHelper;
    }

    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        this.targetDeliveryService = targetDeliveryService;
    }

    /**
     * @param targetZoneDeliveryModeConverter
     *            the targetZoneDeliveryModeConverter to set
     */
    @Required
    public void setTargetZoneDeliveryModeConverter(
            final Converter<TargetZoneDeliveryModeModel, TargetZoneDeliveryModeData> targetZoneDeliveryModeConverter) {
        this.targetZoneDeliveryModeConverter = targetZoneDeliveryModeConverter;
    }

    /**
     * @param sessionService
     *            the sessionService to set
     */
    @Required
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

    /**
     * @param configurationService
     *            the configurationService to set
     */
    @Required
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    /**
     * @param targetSalesApplicationService
     *            the targetSalesApplicationService to set
     */
    @Required
    public void setTargetSalesApplicationService(final TargetSalesApplicationService targetSalesApplicationService) {
        this.targetSalesApplicationService = targetSalesApplicationService;
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.order.converters;


import de.hybris.platform.converters.impl.AbstractPopulatingConverter;

import au.com.target.tgtcore.model.FlybuysRedeemConfigModel;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedeemConfigData;


/**
 * @author vivek
 * 
 */
public class FlybuysRedeemConfigConverter extends
        AbstractPopulatingConverter<FlybuysRedeemConfigModel, FlybuysRedeemConfigData> {

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.impl.AbstractPopulatingConverter#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final FlybuysRedeemConfigModel source, final FlybuysRedeemConfigData target) {
        target.setMinRedeemable(source.getMinRedeemable());
        target.setMaxRedeemable(source.getMaxRedeemable());
        target.setMinCartValue(source.getMinCartValue());
        target.setMinPointsBalance(source.getMinPointsBalance());
        target.setActive(source.getActive());
        super.populate(source, target);
    }

}

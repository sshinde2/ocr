/**
 * 
 */
package au.com.target.tgtfacades.order.converters;

import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.returns.model.OrderEntryReturnRecordEntryModel;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import au.com.target.tgtfacades.order.data.OrderModificationData;


/**
 *
 */
public class OrderEntryReturnRecordEntryConverter extends OrderEntryModificationRecordEntryConverter {

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.converters.OrderEntryModificationRecordEntryConverter#populate(de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel, au.com.target.tgtfacades.order.data.OrderModificationData)
     */
    @Override
    public void populate(final OrderEntryModificationRecordEntryModel source, final OrderModificationData target) {
        super.populate(source, target);
        if ((source instanceof OrderEntryReturnRecordEntryModel) && isNeedToCalculatePriceIncludingDiscounts(source)
                && isTargetNecessaryFieldNotNull(target)) {
            final String currencyIsoCode = source.getOrderEntry().getOrder().getCurrency().getIsocode();

            final BigDecimal basePriceWithDiscounts = new BigDecimal(getPriceWithDiscounts(source.getOrderEntry(),
                    Long.valueOf(1L),
                    target.getBasePrice().getValue())
                    .doubleValue());

            final BigDecimal totalPriceWithDiscounts = new BigDecimal(getPriceWithDiscounts(source.getOrderEntry(),
                    target.getQuantity(), target.getTotalPrice().getValue()).doubleValue());

            target.setBasePrice(getPriceDataFactory()
                    .create(PriceDataType.BUY, basePriceWithDiscounts, currencyIsoCode));
            target.setTotalPrice(getPriceDataFactory().create(PriceDataType.BUY, totalPriceWithDiscounts,
                    currencyIsoCode));
        }
    }

    protected boolean isTargetNecessaryFieldNotNull(final OrderModificationData target) {
        return target.getQuantity() != null && target.getBasePrice() != null && target.getTotalPrice() != null;
    }

    protected boolean isNeedToCalculatePriceIncludingDiscounts(final OrderEntryModificationRecordEntryModel source) {
        return !(source.getOrderEntry().getDiscountValues().isEmpty());
    }

    protected Double getPriceWithDiscounts(final OrderEntryModel orderEntry, final Long quantity,
            final BigDecimal priceWithoutDiscounts) {
        final List<DiscountValue> discounts = orderEntry.getDiscountValues();
        final AbstractOrderModel order = orderEntry.getOrder();
        final CurrencyModel curr = order.getCurrency();
        final int digits = curr.getDigits().intValue();
        final List appliedDiscounts = DiscountValue.apply(quantity.doubleValue(), priceWithoutDiscounts.doubleValue(),
                digits,
                discounts, curr.getIsocode());
        double priceWithDiscounts = priceWithoutDiscounts.doubleValue();
        for (final Iterator it = appliedDiscounts.iterator(); it.hasNext();) {
            priceWithDiscounts -= ((DiscountValue)it.next()).getAppliedValue();
        }
        return Double.valueOf(priceWithDiscounts);
    }

}

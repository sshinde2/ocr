/**
 * 
 */
package au.com.target.tgtfacades.order.converters;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.voucher.VoucherService;

import java.math.BigDecimal;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtfacades.order.data.FlybuysDiscountData;
import au.com.target.tgtfacades.order.data.TargetOrderDataInterface;



/**
 * Flybuys discount converter.
 * 
 * @author knemalik
 */
public class TargetFlybuysDiscountConverter {

    private VoucherService voucherService;
    private PriceDataFactory priceDataFactory;

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.converters.TargetFlybuysDiscountConverter#convertFlybuysDiscount
     * (au.com.target.tgtfacades.order.converters.TargetFlybuysDiscountConverter)
     */
    public void convertFlybuysDiscount(final AbstractOrderModel orderModel, final AbstractOrderData orderData) {
        if (orderData instanceof TargetOrderDataInterface) {
            final Collection<DiscountModel> discounts = voucherService.getAppliedVouchers(orderModel);
            if (CollectionUtils.isNotEmpty(discounts)) {

                for (final DiscountModel discount : discounts) {
                    if (discount instanceof FlybuysDiscountModel) {

                        final FlybuysDiscountModel voucher = (FlybuysDiscountModel)discount;
                        final FlybuysDiscountData flybuysDiscountData = new FlybuysDiscountData();

                        flybuysDiscountData.setFlybuysCardNumber(voucher.getFlybuysCardNumber());
                        flybuysDiscountData.setPointsConsumed(orderModel.getFlybuysPointsConsumed());

                        flybuysDiscountData.setValue(priceDataFactory.create(PriceDataType.BUY,
                                BigDecimal.valueOf(voucher.getValue().doubleValue()),
                                voucher.getCurrency().getIsocode()));

                        final TargetOrderDataInterface targetOrderData = (TargetOrderDataInterface)orderData;
                        targetOrderData.setFlybuysDiscountData(flybuysDiscountData);
                        break;
                    }
                }
            }
        }
    }

    /**
     * @param voucherService
     *            the voucherService to set
     */
    @Required
    public void setVoucherService(final VoucherService voucherService) {
        this.voucherService = voucherService;
    }

    /**
     * @param priceDataFactory
     *            the priceDataFactory to set
     */
    @Required
    public void setPriceDataFactory(final PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }
}

/**
 * 
 */
package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.platform.commercefacades.order.converters.populator.CartModificationPopulator;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;


/**
 * @author Vivek
 *
 */
public class TargetCartModificationPopulator extends CartModificationPopulator {
    @Override
    public void populate(final CommerceCartModification source, final CartModificationData target) {

        if (null != source.getEntry()) {
            target.setEntry(getOrderEntryConverter().convert(source.getEntry()));

            if (null != source.getEntry().getOrder()) {
                target.setCartCode(source.getEntry().getOrder().getCode());
            }
        }

        target.setStatusCode(source.getStatusCode());
        target.setQuantity(source.getQuantity());
        target.setQuantityAdded(source.getQuantityAdded());
        target.setDeliveryModeChanged(source.getDeliveryModeChanged());
    }
}

/**
 * 
 */
package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.platform.commercefacades.order.converters.populator.ConsignmentEntryPopulator;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtfacades.order.data.TargetConsignmentEntryData;


/**
 * @author cbi
 *
 */
public class TargetConsignmentEntryPopulator extends ConsignmentEntryPopulator {

    private PriceDataFactory priceDataFactory;

    @Override
    public void populate(final ConsignmentEntryModel source, final ConsignmentEntryData target) {
        super.populate(source, target);
        Assert.notNull(source.getConsignment(), "Consignment cannot be null.");
        Assert.notNull(source.getConsignment().getOrder(), "Order cannot be null.");
        if (target instanceof TargetConsignmentEntryData) {
            final TargetConsignmentEntryData targetConsignmentEntryData = (TargetConsignmentEntryData)target;
            final OrderEntryData orderEntryData = targetConsignmentEntryData.getOrderEntry();
            if (null != orderEntryData) {
                //Set total shipped price
                final PriceData basePrice = orderEntryData.getBasePrice();
                final CurrencyModel currencyModel = source.getConsignment().getOrder().getCurrency();
                if (null != basePrice) {
                    targetConsignmentEntryData.setTotalShippedPrice(createTotalPriceByQuantityAndBasePrice(basePrice,
                            source.getShippedQuantity(), currencyModel));
                    targetConsignmentEntryData.setTotalPrice(createTotalPriceByQuantityAndBasePrice(basePrice,
                            source.getQuantity(), currencyModel));
                }
            }
        }
    }

    private PriceData createTotalPriceByQuantityAndBasePrice(final PriceData basePrice, final Long quantity,
            final CurrencyModel currencyModel) {
        if (basePrice == null) {
            return null;
        }
        final BigDecimal basePriceValue = basePrice.getValue();
        final BigDecimal decimalQuantity = BigDecimal.valueOf(quantity == null ? 0 : quantity.longValue());
        final BigDecimal totalPrice = basePriceValue.multiply(decimalQuantity);
        return priceDataFactory.create(PriceDataType.BUY, totalPrice, currencyModel);
    }

    /**
     * @param priceDataFactory
     *            the priceDataFactory to set
     */
    @Required
    public void setPriceDataFactory(final PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }

}
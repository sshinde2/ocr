/**
 * 
 */
package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.order.converters.populator.OrderEntryPopulator;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.GiftRecipientModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.converters.util.TargetConvertersUtil;
import au.com.target.tgtfacades.giftcards.converters.GiftRecipientConverter;
import au.com.target.tgtfacades.order.data.GiftRecipientData;
import au.com.target.tgtfacades.order.data.TargetOrderEntryData;
import au.com.target.tgtmarketing.model.AffiliateOfferCategoryModel;


/**
 * @author asingh78
 * 
 */
public class TargetOrderEntryPopulator extends OrderEntryPopulator {

    private GiftRecipientConverter giftRecipientConverter;

    @Override
    public void populate(final AbstractOrderEntryModel source, final OrderEntryData target) {
        super.populate(source, target);
        populateGiftRecipientDetails(source, target);
        populateDepartment(source.getProduct(), target);
    }


    protected void populateGiftRecipientDetails(final AbstractOrderEntryModel source, final OrderEntryData target) {
        final List<GiftRecipientModel> recipients = source.getGiftRecipients();
        if (CollectionUtils.isNotEmpty(recipients) && target instanceof TargetOrderEntryData) {
            final TargetOrderEntryData orderEntryData = (TargetOrderEntryData)target;
            orderEntryData.setRecipients(getRecipientList(recipients));
        }
    }

    protected List<GiftRecipientData> getRecipientList(final List<GiftRecipientModel> recipients) {
        final List<GiftRecipientData> recipientDataList = new ArrayList<>();
        for (final GiftRecipientModel model : recipients) {
            recipientDataList.add(giftRecipientConverter.convert(model));
        }
        return recipientDataList;
    }

    @Override
    protected void addTotals(final AbstractOrderEntryModel orderEntry, final OrderEntryData entry) {
        entry.setBasePrice(createPrice(orderEntry, orderEntry.getBasePrice()));
        double totalPrice = 0.0;
        if (null != orderEntry.getTotalPrice()) {
            totalPrice = orderEntry.getTotalPrice().doubleValue();
        }
        BigDecimal price = BigDecimal.valueOf(totalPrice);
        BigDecimal totalItemPrice = price;
        price = price.setScale(2, BigDecimal.ROUND_HALF_UP);
        entry.setTotalPrice(createPrice(orderEntry, Double.valueOf(price.doubleValue())));

        final double discount = getDiscount(orderEntry);
        if (entry instanceof TargetOrderEntryData) {
            if (discount > 0) {
                ((TargetOrderEntryData)entry).setDiscountPrice(createPrice(orderEntry, Double.valueOf(discount)));
                totalItemPrice = totalItemPrice.add(BigDecimal.valueOf(discount));
            }

            // set total item price (before discount)
            totalItemPrice = totalItemPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
            ((TargetOrderEntryData)entry).setTotalItemPrice(createPrice(orderEntry,
                    Double.valueOf(totalItemPrice.doubleValue())));
        }
    }

    /**
     * @param product
     */
    protected void populateDepartment(final ProductModel product, final OrderEntryData target) {
        final TargetMerchDepartmentModel merchDept = getMerchDepartment(product);
        if (null != merchDept && target instanceof TargetOrderEntryData) {
            final TargetOrderEntryData targetOrderEntry = (TargetOrderEntryData)target;
            targetOrderEntry.setDepartmentName(merchDept.getName());
            targetOrderEntry.setDepartmentCode(merchDept.getCode());
            targetOrderEntry.setAffiliateOfferCategory(getAffiliateOfferCategory(merchDept));
        }

    }

    /**
     * @param merchDept
     * @return String that represents the affiliateOfferCategory
     */
    protected String getAffiliateOfferCategory(final TargetMerchDepartmentModel merchDept) {
        final Collection<CategoryModel> categoryModels = merchDept.getSupercategories();
        if (CollectionUtils.isNotEmpty(categoryModels)) {
            for (final CategoryModel category : categoryModels) {
                if (category instanceof AffiliateOfferCategoryModel) {
                    return category.getCode();
                }
            }
        }
        return null;
    }

    /**
     * @param product
     * @return TargetmerchDepartmentModel
     */
    protected TargetMerchDepartmentModel getMerchDepartment(final ProductModel product) {
        final ProductModel baseProduct = TargetConvertersUtil.getBaseProduct(product);
        if (baseProduct != null && baseProduct instanceof TargetProductModel) {
            return ((TargetProductModel)baseProduct).getMerchDepartment();
        }
        return null;
    }

    private double getDiscount(final AbstractOrderEntryModel orderEntryModel) {
        double result = 0D;
        for (final DiscountValue dv : orderEntryModel.getDiscountValues()) {
            result = result + dv.getAppliedValue();
        }
        return result;
    }

    /**
     * @param giftRecipientConverter
     *            the giftRecipientConverter to set
     */
    @Required
    public void setGiftRecipientConverter(
            final GiftRecipientConverter giftRecipientConverter) {
        this.giftRecipientConverter = giftRecipientConverter;
    }

}

/**
 *
 */
package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Required;


/**
 * Target Simple Order Entry Converter for the purpose of populating email context
 *
 */
public class TargetSimpleOrderEntryPopulator extends TargetOrderEntryPopulator {

    private Converter<ProductModel, ProductData> productUrlConverter;

    @Override
    protected void addProduct(final AbstractOrderEntryModel orderEntry,
            final OrderEntryData entry) {
        entry.setProduct(getProductUrlConverter().convert(orderEntry.getProduct()));
    }

    /**
     * @return the productUrlConverter
     */
    protected Converter<ProductModel, ProductData> getProductUrlConverter() {
        return productUrlConverter;
    }

    /**
     * @param productUrlConverter
     *            the productUrlConverter to set
     */
    @Required
    public void setProductUrlConverter(final Converter<ProductModel, ProductData> productUrlConverter) {
        this.productUrlConverter = productUrlConverter;
    }

}

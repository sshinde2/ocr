/**
 *
 */
package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import org.apache.commons.lang.BooleanUtils;

import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;


/**
 * @author dcwillia
 *
 */
public class TargetZoneDeliveryModePopulator implements
        Populator<TargetZoneDeliveryModeModel, TargetZoneDeliveryModeData> {

    @Override
    public void populate(final TargetZoneDeliveryModeModel source, final TargetZoneDeliveryModeData target) {

        target.setDeliveryToStore(BooleanUtils.isTrue(source.getIsDeliveryToStore()));
        target.setDigitalDelivery(BooleanUtils.isTrue(source.getIsDigital()));
        target.setDisplayOrder(source.getDisplayOrder() != null ? source.getDisplayOrder().intValue() : 0);
    }
}

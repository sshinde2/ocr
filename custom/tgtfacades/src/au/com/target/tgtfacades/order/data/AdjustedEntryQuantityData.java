/**
 * 
 */
package au.com.target.tgtfacades.order.data;

import au.com.target.tgtfacades.product.data.TargetProductData;


/**
 * @author gbaker2
 * 
 */
public class AdjustedEntryQuantityData {

    private int quantity;
    private TargetProductData product;


    /**
     * 
     */
    public AdjustedEntryQuantityData() {
        // Nothing todo
    }

    public AdjustedEntryQuantityData(final int quantity, final TargetProductData product) {
        super();
        this.quantity = quantity;
        this.product = product;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(final int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the product
     */
    public TargetProductData getProduct() {
        return product;
    }

    /**
     * @param product
     *            the product to set
     */
    public void setProduct(final TargetProductData product) {
        this.product = product;
    }
}
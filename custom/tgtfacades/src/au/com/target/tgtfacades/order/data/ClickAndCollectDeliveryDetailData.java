/**
 * 
 */
package au.com.target.tgtfacades.order.data;

import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;


/**
 * @author rmcalave
 * 
 */
public class ClickAndCollectDeliveryDetailData {
    private Integer storeNumber;

    private String title;

    private String firstName;

    private String lastName;

    private String telephoneNumber;

    private DeliveryModeModel deliveryModeModel;

    /**
     * @return the storeNumber
     */
    public Integer getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final Integer storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the telephoneNumber
     */
    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    /**
     * @param telephoneNumber
     *            the telephoneNumber to set
     */
    public void setTelephoneNumber(final String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    /**
     * @return the deliveryModeModel
     */
    public DeliveryModeModel getDeliveryModeModel() {
        return deliveryModeModel;
    }

    /**
     * @param deliveryModeModel
     *            the deliveryModeModel to set
     */
    public void setDeliveryModeModel(final DeliveryModeModel deliveryModeModel) {
        this.deliveryModeModel = deliveryModeModel;
    }
}

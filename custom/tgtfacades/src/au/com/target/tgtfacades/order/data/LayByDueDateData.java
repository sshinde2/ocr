/**
 * 
 */
package au.com.target.tgtfacades.order.data;

/**
 * @author rmcalave
 * 
 */
public class LayByDueDateData {
    private String formattedAmount;

    private String formattedDueDate;

    /**
     * @return the formattedAmount
     */
    public String getFormattedAmount() {
        return formattedAmount;
    }

    /**
     * @param formattedAmount
     *            the formattedAmount to set
     */
    public void setFormattedAmount(final String formattedAmount) {
        this.formattedAmount = formattedAmount;
    }

    /**
     * @return the formattedDueDate
     */
    public String getFormattedDueDate() {
        return formattedDueDate;
    }

    /**
     * @param formattedDueDate
     *            the formattedDueDate to set
     */
    public void setFormattedDueDate(final String formattedDueDate) {
        this.formattedDueDate = formattedDueDate;
    }
}

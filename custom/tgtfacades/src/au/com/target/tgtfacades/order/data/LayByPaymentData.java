/**
 * 
 */
package au.com.target.tgtfacades.order.data;

import de.hybris.platform.commercefacades.product.data.PriceData;


/**
 * @author rmcalave
 * 
 */
public class LayByPaymentData {
    private String percentage;

    private PriceData actualAmount;

    /**
     * @return the percentage
     */
    public String getPercentage() {
        return percentage;
    }

    /**
     * @param percentage
     *            the percentage to set
     */
    public void setPercentage(final String percentage) {
        this.percentage = percentage;
    }

    /**
     * @return the actualAmount
     */
    public PriceData getActualAmount() {
        return actualAmount;
    }

    /**
     * @param actualAmount
     *            the actualAmount to set
     */
    public void setActualAmount(final PriceData actualAmount) {
        this.actualAmount = actualAmount;
    }
}

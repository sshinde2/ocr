/**
 * 
 */
package au.com.target.tgtfacades.order.data;

import java.util.Date;

import au.com.target.tgtfacades.order.enums.OrderModificationStatusEnum;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class OrderModificationData extends TargetOrderEntryData {

    private String code;
    private Date date;

    private OrderModificationStatusEnum status;

    private String replacementOrderCode;

    private String reason;



     /**
      * @return the code
     */
    public String getCode() { return code; }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) { this.code = code; }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date
     *            the date to set
     */
    public void setDate(final Date date) {
        this.date = date;
    }

    /**
     * @return the status
     */
    public OrderModificationStatusEnum getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(final OrderModificationStatusEnum status) {
        this.status = status;
    }

    /**
     * @return the replacementOrderCode
     */
    public String getReplacementOrderCode() {
        return replacementOrderCode;
    }

    /**
     * @param replacementOrderCode
     *            the replacementOrderCode to set
     */
    public void setReplacementOrderCode(final String replacementOrderCode) {
        this.replacementOrderCode = replacementOrderCode;
    }

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason
     *            the reason to set
     */
    public void setReason(final String reason) {
        this.reason = reason;
    }
}

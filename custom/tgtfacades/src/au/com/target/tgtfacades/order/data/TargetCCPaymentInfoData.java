/**
 * 
 */
package au.com.target.tgtfacades.order.data;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.product.data.PriceData;

import au.com.target.tgtfacades.user.data.AfterpayPaymentInfoData;
import au.com.target.tgtfacades.user.data.IpgPaymentInfoData;
import au.com.target.tgtfacades.user.data.PayPalHereInfoData;
import au.com.target.tgtfacades.user.data.PaypalInfoData;
import au.com.target.tgtfacades.user.data.PinPadPaymentInfoData;
import au.com.target.tgtfacades.user.data.ZipPaymentInfoData;


/**
 * @author asingh78
 * 
 */
public class TargetCCPaymentInfoData extends CCPaymentInfoData {

    private boolean paymentSucceeded;

    private boolean paypalPaymentInfo;
    private boolean pinPadPaymentInfo;
    private boolean payPalHerePaymentInfo;
    private boolean ipgPaymentInfo;
    private boolean afterpayPaymentInfo;
    private boolean zipPaymentInfo;
    private PaypalInfoData paypalInfoData;
    private PinPadPaymentInfoData pinPadPaymentInfoData;
    private PayPalHereInfoData payPalHereInfoData;
    private IpgPaymentInfoData ipgPaymentInfoData;
    private AfterpayPaymentInfoData afterpayPaymentInfoData;
    private ZipPaymentInfoData zipPaymentInfoData;
    private PriceData amount;
    private String receiptNumber;

    /**
     * @return the paypalInfoData
     */
    public PaypalInfoData getPaypalInfoData() {
        return paypalInfoData;
    }

    /**
     * @param paypalInfoData
     *            the paypalInfoData to set
     */
    public void setPaypalInfoData(final PaypalInfoData paypalInfoData) {
        this.paypalInfoData = paypalInfoData;
    }

    /**
     * @return the paymentSucceeded
     */
    public boolean isPaymentSucceeded() {
        return paymentSucceeded;
    }

    /**
     * @param paymentSucceeded
     *            the paymentSucceeded to set
     */
    public void setPaymentSucceeded(final boolean paymentSucceeded) {
        this.paymentSucceeded = paymentSucceeded;
    }

    /**
     * @return the paypalPaymentInfo
     */
    public boolean isPaypalPaymentInfo() {
        return paypalPaymentInfo;
    }

    /**
     * @param paypalPaymentInfo
     *            the paypalPaymentInfo to set
     */
    public void setPaypalPaymentInfo(final boolean paypalPaymentInfo) {
        this.paypalPaymentInfo = paypalPaymentInfo;
    }

    /**
     * @return the pinPadPaymentInfoData
     */
    public PinPadPaymentInfoData getPinPadPaymentInfoData() {
        return pinPadPaymentInfoData;
    }

    /**
     * @param pinPadPaymentInfoData
     *            the pinPadPaymentInfoData to set
     */
    public void setPinPadPaymentInfoData(final PinPadPaymentInfoData pinPadPaymentInfoData) {
        this.pinPadPaymentInfoData = pinPadPaymentInfoData;
    }

    /**
     * @return the pinPadPaymentInfo
     */
    public boolean isPinPadPaymentInfo() {
        return pinPadPaymentInfo;
    }

    /**
     * @param pinPadPaymentInfo
     *            the pinPadPaymentInfo to set
     */
    public void setPinPadPaymentInfo(final boolean pinPadPaymentInfo) {
        this.pinPadPaymentInfo = pinPadPaymentInfo;
    }

    /**
     * @return the payPalHerePaymentInfo
     */
    public boolean isPayPalHerePaymentInfo() {
        return payPalHerePaymentInfo;
    }

    /**
     * @param payPalHerePaymentInfo
     *            the payPalHerePaymentInfo to set
     */
    public void setPayPalHerePaymentInfo(final boolean payPalHerePaymentInfo) {
        this.payPalHerePaymentInfo = payPalHerePaymentInfo;
    }



    /**
     * @return the ipgPaymentInfo
     */
    public boolean isIpgPaymentInfo() {
        return ipgPaymentInfo;
    }

    /**
     * @param ipgPaymentInfo
     *            the ipgPaymentInfo to set
     */
    public void setIpgPaymentInfo(final boolean ipgPaymentInfo) {
        this.ipgPaymentInfo = ipgPaymentInfo;
    }

    /**
     * @return the afterpayPaymentInfo
     */
    public boolean isAfterpayPaymentInfo() {
        return afterpayPaymentInfo;
    }

    /**
     * @param afterpayPaymentInfo
     *            the afterpayPaymentInfo to set
     */
    public void setAfterpayPaymentInfo(final boolean afterpayPaymentInfo) {
        this.afterpayPaymentInfo = afterpayPaymentInfo;
    }

    /**
     * @return the zipPaymentInfo
    */
    public boolean isZipPaymentInfo() { return zipPaymentInfo; }

    /**
     * @param zipPaymentInfo
     *            the zipPaymentInfo to set
     */
    public void setZipPaymentInfo(final boolean zipPaymentInfo) {
        this.zipPaymentInfo = zipPaymentInfo;
    }

    /**
     * @return the ipgPaymentInfoData
     */
    public IpgPaymentInfoData getIpgPaymentInfoData() {
        return ipgPaymentInfoData;
    }

    /**
     * @param ipgPaymentInfoData
     *            the ipgPaymentInfoData to set
     */
    public void setIpgPaymentInfoData(final IpgPaymentInfoData ipgPaymentInfoData) {
        this.ipgPaymentInfoData = ipgPaymentInfoData;
    }

    /**
     * @return the payPalHereInfoData
     */
    public PayPalHereInfoData getPayPalHereInfoData() {
        return payPalHereInfoData;
    }

    /**
     * @param payPalHereInfoData
     *            the payPalHereInfoData to set
     */
    public void setPayPalHereInfoData(final PayPalHereInfoData payPalHereInfoData) {
        this.payPalHereInfoData = payPalHereInfoData;
    }

    /**
     * @return the afterpayPaymentInfoData
     */
    public AfterpayPaymentInfoData getAfterpayPaymentInfoData() {
        return afterpayPaymentInfoData;
    }

    /**
     * @param afterpayPaymentInfoData
     *            the afterpayPaymentInfoData to set
     */
    public void setAfterpayPaymentInfoData(final AfterpayPaymentInfoData afterpayPaymentInfoData) {
        this.afterpayPaymentInfoData = afterpayPaymentInfoData;
    }

    /**
     * @return the zipPaymentInfoData
     */
    public ZipPaymentInfoData getZipPaymentInfoData() { return zipPaymentInfoData; }

    /**
     * @param zipPaymentInfoData
     *            the zipPaymentInfoData to set
    */
    public void setZipPaymentInfoData(final ZipPaymentInfoData zipPaymentInfoData) {
        this.zipPaymentInfoData = zipPaymentInfoData;
    }

    /**
     * @return the amount
     */
    public PriceData getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final PriceData amount) {
        this.amount = amount;
    }

    /**
     * @return the receiptNumber
     */
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * @param receiptNumber
     *            the receiptNumber to set
     */
    public void setReceiptNumber(final String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }
}
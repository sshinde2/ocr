/**
 * 
 */
package au.com.target.tgtfacades.order.data;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.AddressData;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;


/**
 * @author rmcalave
 * 
 */
public class TargetCartData extends CartData implements TargetOrderDataInterface {
    private Integer cncStoreNumber;

    private String tmdNumber;

    private boolean isLayBy;

    private List<LayByDueDateData> dueDates;

    private String fullPaymentDueDate;

    private PriceData layByFee;

    private PriceData initialLayByPaymentAmount;

    private String purchaseOptionCode;

    private boolean allowBillingSameAsDelivery;

    private boolean allowGuestCheckout;

    private boolean allowSaveAddress;

    private boolean allowSavePayment;

    private PriceData layByMin;

    private LayByPaymentData minimumPaymentAmount;

    private LayByPaymentData maximumPaymentAmount;

    private String flybuysNumber;

    private String maskedFlybuysNumber;

    private boolean insufficientAmount;

    private boolean dataStale = false;

    private List<TargetZoneDeliveryModeData> deliveryModes;

    private List<TargetZoneDeliveryModeData> digitalDeliveryModes;

    /**
     * this boolean will be true only if at least one of the delivery modes is available
     */
    private boolean hasValidDeliveryModes;

    private FlybuysDiscountData flybuysDiscountData;

    private List<CCPaymentInfoData> paymentsInfo;

    private AddressData billingAddress;

    private PriceData preOrderDepositAmount;

    private PriceData preOrderOutstandingAmount;

    /**
     * @return the cncStoreNumber
     */
    @Override
    public Integer getCncStoreNumber() {
        return cncStoreNumber;
    }

    /**
     * @param cncStoreNumber
     *            the cncStoreNumber to set
     */
    public void setCncStoreNumber(final Integer cncStoreNumber) {
        this.cncStoreNumber = cncStoreNumber;
    }

    /**
     * @return the purchaseOptionCode
     */
    @Override
    public String getPurchaseOptionCode() {
        return purchaseOptionCode;
    }

    /**
     * @param purchaseOptionCode
     *            the purchaseOptionCode to set
     */
    public void setPurchaseOptionCode(final String purchaseOptionCode) {
        this.purchaseOptionCode = purchaseOptionCode;
    }

    /**
     * @return the insufficientAmount
     */
    public boolean isInsufficientAmount() {
        return insufficientAmount;
    }

    /**
     * @param insufficientAmount
     *            the insufficientAmount to set
     */
    public void setInsufficientAmount(final boolean insufficientAmount) {
        this.insufficientAmount = insufficientAmount;
    }

    /**
     * @return the isLayBy
     */
    @Override
    public boolean isLayBy() {
        return isLayBy;
    }

    /**
     * @param isLayBy
     *            the isLayBy to set
     */
    public void setIsLayBy(final boolean isLayBy) {
        this.isLayBy = isLayBy;
    }


    /**
     * @return the allowBillingSameAsDelivery
     */
    public boolean isAllowBillingSameAsDelivery() {
        return allowBillingSameAsDelivery;
    }

    /**
     * @param allowBillingSameAsDelivery
     *            the allowBillingSameAsDelivery to set
     */
    public void setAllowBillingSameAsDelivery(final boolean allowBillingSameAsDelivery) {
        this.allowBillingSameAsDelivery = allowBillingSameAsDelivery;
    }



    /**
     * @return the allowGuestCheckout
     */
    public boolean isAllowGuestCheckout() {
        return allowGuestCheckout;
    }

    /**
     * @param allowGuestCheckout
     *            the allowGuestCheckout to set
     */
    public void setAllowGuestCheckout(final boolean allowGuestCheckout) {
        this.allowGuestCheckout = allowGuestCheckout;
    }

    /**
     * @return the allowSaveAddress
     */
    public boolean isAllowSaveAddress() {
        return allowSaveAddress;
    }

    /**
     * @param allowSaveAddress
     *            the allowSaveAddress to set
     */
    public void setAllowSaveAddress(final boolean allowSaveAddress) {
        this.allowSaveAddress = allowSaveAddress;
    }

    /**
     * @return the allowSavePayment
     */
    public boolean isAllowSavePayment() {
        return allowSavePayment;
    }

    /**
     * @param allowSavePayment
     *            the allowSavePayment to set
     */
    public void setAllowSavePayment(final boolean allowSavePayment) {
        this.allowSavePayment = allowSavePayment;
    }

    /**
     * @return the layByFee
     */
    @Override
    public PriceData getLayByFee() {
        return layByFee;
    }

    /**
     * @param layByFee
     *            the layByFee to set
     */
    public void setLayByFee(final PriceData layByFee) {
        this.layByFee = layByFee;
    }

    /**
     * @return the layByMin
     */
    public PriceData getLayByMin() {
        return layByMin;
    }

    /**
     * @param layByMin
     *            the layByMin to set
     */
    public void setLayByMin(final PriceData layByMin) {
        this.layByMin = layByMin;
    }


    /**
     * @return the initialLayByPaymentAmount
     */
    @Override
    public PriceData getInitialLayByPaymentAmount() {
        return initialLayByPaymentAmount;
    }

    /**
     * @param initialLayByPaymentAmount
     *            the initialLayByPaymentAmount to set
     */
    public void setInitialLayByPaymentAmount(final PriceData initialLayByPaymentAmount) {
        this.initialLayByPaymentAmount = initialLayByPaymentAmount;
    }

    /**
     * @return the minimumPaymentAmount
     */
    public LayByPaymentData getMinimumPaymentAmount() {
        return minimumPaymentAmount;
    }

    /**
     * @param minimumPaymentAmount
     *            the minimumPaymentAmount to set
     */
    public void setMinimumPaymentAmount(final LayByPaymentData minimumPaymentAmount) {
        this.minimumPaymentAmount = minimumPaymentAmount;
    }

    /**
     * @return the maximumPaymentAmount
     */
    public LayByPaymentData getMaximumPaymentAmount() {
        return maximumPaymentAmount;
    }

    /**
     * @param maximumPaymentAmount
     *            the maximumPaymentAmount to set
     */
    public void setMaximumPaymentAmount(final LayByPaymentData maximumPaymentAmount) {
        this.maximumPaymentAmount = maximumPaymentAmount;
    }

    /**
     * @return the dueDates
     */
    @Override
    public List<LayByDueDateData> getDueDates() {
        return dueDates;
    }

    /**
     * @param dueDates
     *            the dueDates to set
     */
    public void setDueDates(final List<LayByDueDateData> dueDates) {
        this.dueDates = dueDates;
    }

    /**
     * @return the fullPaymentDueDate
     */
    @Override
    public String getFullPaymentDueDate() {
        return fullPaymentDueDate;
    }

    /**
     * @param fullPaymentDueDate
     *            the fullPaymentDueDate to set
     */
    public void setFullPaymentDueDate(final String fullPaymentDueDate) {
        this.fullPaymentDueDate = fullPaymentDueDate;
    }

    /**
     * @return the tmdNumber
     */
    @Override
    public String getTmdNumber() {
        return tmdNumber;
    }

    /**
     * @param tmdNumber
     *            the tmdNumber to set
     */
    public void setTmdNumber(final String tmdNumber) {
        this.tmdNumber = tmdNumber;
    }

    /**
     * @return the flybuysNumber
     */
    public String getFlybuysNumber() {
        return flybuysNumber;
    }

    /**
     * @param flybuysNumber
     *            the flybuysNumber to set
     */
    public void setFlybuysNumber(final String flybuysNumber) {
        this.flybuysNumber = flybuysNumber;
    }

    /**
     * @return the maskedFlybuysNumber
     */
    public String getMaskedFlybuysNumber() {
        return maskedFlybuysNumber;
    }

    /**
     * @param maskedFlybuysNumber
     *            the maskedFlybuysNumber to set
     */
    public void setMaskedFlybuysNumber(final String maskedFlybuysNumber) {
        this.maskedFlybuysNumber = maskedFlybuysNumber;
    }

    /**
     * @return true if entries are present false otherwise
     */
    public boolean hasEntries() {
        return CollectionUtils.isNotEmpty(getEntries());
    }

    /**
     * @return the dataStale
     */
    public boolean isDataStale() {
        return dataStale;
    }

    /**
     * @param dataStale
     *            the dataStale to set
     */
    public void setDataStale(final boolean dataStale) {
        this.dataStale = dataStale;
    }

    /**
     * @return the deliveryModes
     */
    public List<TargetZoneDeliveryModeData> getDeliveryModes() {
        return deliveryModes;
    }

    /**
     * @param deliveryModes
     *            the deliveryModes to set
     */
    public void setDeliveryModes(final List<TargetZoneDeliveryModeData> deliveryModes) {
        this.deliveryModes = deliveryModes;
    }

    /**
     * @return the hasValidDeliveryModes
     */
    public boolean gethasValidDeliveryModes() {
        return hasValidDeliveryModes;
    }

    /**
     * @param hasValidDeliveryModes
     *            the hasValidDeliveryModes to set
     */
    public void setHasValidDeliveryModes(final boolean hasValidDeliveryModes) {
        this.hasValidDeliveryModes = hasValidDeliveryModes;
    }


    @Override
    public FlybuysDiscountData getFlybuysDiscountData() {

        return flybuysDiscountData;
    }


    @Override
    public void setFlybuysDiscountData(final FlybuysDiscountData flybuysDiscountData) {
        this.flybuysDiscountData = flybuysDiscountData;
    }

    /**
     * @return the digitalDeliveryModes
     */
    public List<TargetZoneDeliveryModeData> getDigitalDeliveryModes() {
        return digitalDeliveryModes;
    }

    /**
     * @param digitalDeliveryModes
     *            the digitalDeliveryModes to set
     */
    public void setDigitalDeliveryModes(final List<TargetZoneDeliveryModeData> digitalDeliveryModes) {
        this.digitalDeliveryModes = digitalDeliveryModes;
    }

    /**
     * @return true if the cart has any digital product
     */
    @Override
    public boolean getHasDigitalProducts() {
        return CollectionUtils.isNotEmpty(getDigitalDeliveryModes());
    }

    /**
     * paymentsInfo represents a list of payment types (credit card/gift card/PayPal etc.) participated in payment.
     * There are no more than one payment type in the cart for TNS payment. The JSP tag which is shared across
     * displaying payment information for cart and order expects a list of payment types details. So, wrap the payment
     * information in an appropriate list.
     * 
     * @return the paymentsInfo
     */
    public List<CCPaymentInfoData> getPaymentsInfo() {
        if (CollectionUtils.isEmpty(paymentsInfo)) {
            addPaymentInfo(getPaymentInfo());
        }
        return paymentsInfo;
    }

    /**
     * Ass a payment info to list of payments
     * 
     * @param paymentInfo
     *            the payment info to be added
     */
    public void addPaymentInfo(final CCPaymentInfoData paymentInfo) {
        if (paymentsInfo == null) {
            paymentsInfo = new ArrayList<>();
        }
        if (paymentInfo != null) {
            paymentsInfo.add(paymentInfo);
        }
    }

    /**
     * @return the billingAddress
     */
    public AddressData getBillingAddress() {
        return billingAddress;
    }

    /**
     * @param billingAddress
     *            the billingAddress to set
     */
    public void setBillingAddress(final AddressData billingAddress) {
        this.billingAddress = billingAddress;
    }

    /**
     * @return the preOrderDepositAmount
     */
    public PriceData getPreOrderDepositAmount() {
        return preOrderDepositAmount;
    }

    /**
     * @param preOrderDepositAmount
     *            the preOrderDepositAmount to set
     */
    public void setPreOrderDepositAmount(final PriceData preOrderDepositAmount) {
        this.preOrderDepositAmount = preOrderDepositAmount;
    }

    /**
     * @return the preOrderOutstandingAmount
     */
    public PriceData getPreOrderOutstandingAmount() {
        return preOrderOutstandingAmount;
    }

    /**
     * @param preOrderOutstandingAmount
     *            the preOrderOutstandingAmount to set
     */
    public void setPreOrderOutstandingAmount(final PriceData preOrderOutstandingAmount) {
        this.preOrderOutstandingAmount = preOrderOutstandingAmount;
    }

}
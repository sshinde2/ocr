/**
 * 
 */
package au.com.target.tgtfacades.order.data;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;

import java.util.List;


/**
 * Extend the default OrderEntryData to add a discount field
 * 
 */
public class TargetOrderEntryData extends OrderEntryData {

    private PriceData discountPrice;

    private String departmentName;

    private String departmentCode;

    private String affiliateOfferCategory;

    private List<GiftRecipientData> recipients;

    private PriceData totalItemPrice;

    private boolean dealsApplied;

    public PriceData getDiscountPrice()
    {
        return discountPrice;
    }

    public void setDiscountPrice(final PriceData discountPrice)
    {
        this.discountPrice = discountPrice;
    }

    /**
     * @return the departmentName
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * @param departmentName
     *            the departmentName to set
     */
    public void setDepartmentName(final String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * @return the affiliateOfferCategory
     */
    public String getAffiliateOfferCategory() {
        return affiliateOfferCategory;
    }

    /**
     * @param affiliateOfferCategory
     *            the affiliateOfferCategory to set
     */
    public void setAffiliateOfferCategory(final String affiliateOfferCategory) {
        this.affiliateOfferCategory = affiliateOfferCategory;
    }

    /**
     * @return the recipients
     */
    public List<GiftRecipientData> getRecipients() {
        return recipients;
    }

    /**
     * @param recipients
     *            the recipients to set
     */
    public void setRecipients(final List<GiftRecipientData> recipients) {
        this.recipients = recipients;
    }

    /**
     * @return the totalItemPrice
     */
    public PriceData getTotalItemPrice() {
        return totalItemPrice;
    }

    /**
     * @param totalItemPrice
     *            the totalItemPrice to set
     */
    public void setTotalItemPrice(final PriceData totalItemPrice) {
        this.totalItemPrice = totalItemPrice;
    }

    /**
     * @return the dealsApplied
     */
    public boolean isDealsApplied() {
        return dealsApplied;
    }

    /**
     * @param dealsApplied
     *            the dealsApplied to set
     */
    public void setDealsApplied(final boolean dealsApplied) {
        this.dealsApplied = dealsApplied;
    }

    /**
     * @return the departmentCode
     */
    public String getDepartmentCode() {
        return departmentCode;
    }

    /**
     * @param departmentCode
     *            the departmentCode to set
     */
    public void setDepartmentCode(final String departmentCode) {
        this.departmentCode = departmentCode;
    }
}
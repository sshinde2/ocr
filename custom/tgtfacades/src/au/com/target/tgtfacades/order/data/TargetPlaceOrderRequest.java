/**
 * 
 */
package au.com.target.tgtfacades.order.data;

import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderPaymentInfoData;


public class TargetPlaceOrderRequest {

    private TargetPlaceOrderPaymentInfoData paymentInfo;

    private String ipAddress;

    private String cookieString;


    /**
     * @return the paymentInfo
     */
    public TargetPlaceOrderPaymentInfoData getPaymentInfo() {
        return paymentInfo;
    }

    /**
     * @param paymentInfo
     *            the paymentInfo to set
     */
    public void setPaymentInfo(final TargetPlaceOrderPaymentInfoData paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    /**
     * @return the ipAddress
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * @param ipAddress
     *            the ipAddress to set
     */
    public void setIpAddress(final String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * @return the cookieString
     */
    public String getCookieString() {
        return cookieString;
    }

    /**
     * @param cookieString
     *            the cookieString to set
     */
    public void setCookieString(final String cookieString) {
        this.cookieString = cookieString;
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.order.data;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import org.apache.commons.lang.Validate;

import au.com.target.tgtfacades.order.enums.TargetPlaceOrderResultEnum;


/**
 * @author rmcalave
 * 
 */
public class TargetPlaceOrderResult {
    private TargetPlaceOrderResultEnum placeOrderResult;

    private final AbstractOrderModel abstractOrderModel;

    private AdjustedCartEntriesData adjustedCartEntriesData;

    private String cartCode;

    public TargetPlaceOrderResult(final TargetPlaceOrderResultEnum placeOrderResult,
            final AbstractOrderModel abstractOrderModel) {
        Validate.notNull(placeOrderResult);

        this.placeOrderResult = placeOrderResult;
        this.abstractOrderModel = abstractOrderModel;
    }

    /**
     * @return the placeOrderResult
     */
    public TargetPlaceOrderResultEnum getPlaceOrderResult() {
        return placeOrderResult;
    }

    /**
     * @return the abstractOrderModel
     */
    public AbstractOrderModel getAbstractOrderModel() {
        return abstractOrderModel;
    }

    /**
     * @return the adjustedCartEntriesData
     */
    public AdjustedCartEntriesData getAdjustedCartEntriesData() {
        return adjustedCartEntriesData;
    }

    /**
     * @param adjustedCartEntriesData
     *            the adjustedCartEntriesData to set
     */
    public void setAdjustedCartEntriesData(final AdjustedCartEntriesData adjustedCartEntriesData) {
        this.adjustedCartEntriesData = adjustedCartEntriesData;
    }

    /**
     * @return the cartCode
     */
    public String getCartCode() {
        return cartCode;
    }

    /**
     * @param cartCode
     *            the cartCode to set
     */
    public void setCartCode(final String cartCode) {
        this.cartCode = cartCode;
    }

    public void setPlaceOrderResult(final TargetPlaceOrderResultEnum placeOrderResult) {
        this.placeOrderResult = placeOrderResult;
    }
}

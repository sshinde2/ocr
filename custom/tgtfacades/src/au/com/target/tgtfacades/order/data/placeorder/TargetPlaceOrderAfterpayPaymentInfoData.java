/**
 * 
 */
package au.com.target.tgtfacades.order.data.placeorder;

/**
 * @author rmcalave
 *
 */
public class TargetPlaceOrderAfterpayPaymentInfoData implements TargetPlaceOrderPaymentInfoData {

    private String afterpayOrderToken;

    /**
     * @return the afterpayOrderToken
     */
    public String getAfterpayOrderToken() {
        return afterpayOrderToken;
    }

    /**
     * @param afterpayOrderToken
     *            the afterpayOrderToken to set
     */
    public void setAfterpayOrderToken(final String afterpayOrderToken) {
        this.afterpayOrderToken = afterpayOrderToken;
    }

}

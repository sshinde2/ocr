/**
 * 
 */
package au.com.target.tgtfacades.order.data.placeorder;

/**
 * @author rmcalave
 *
 */
public class TargetPlaceOrderPayPalPaymentInfoData implements TargetPlaceOrderPaymentInfoData {

    private String paypalSessionToken;

    private String paypalPayerId;

    /**
     * @return the paypalSessionToken
     */
    public String getPaypalSessionToken() {
        return paypalSessionToken;
    }

    /**
     * @param paypalSessionToken
     *            the paypalSessionToken to set
     */
    public void setPaypalSessionToken(final String paypalSessionToken) {
        this.paypalSessionToken = paypalSessionToken;
    }

    /**
     * @return the paypalPayerId
     */
    public String getPaypalPayerId() {
        return paypalPayerId;
    }

    /**
     * @param paypalPayerId
     *            the paypalPayerId to set
     */
    public void setPaypalPayerId(final String paypalPayerId) {
        this.paypalPayerId = paypalPayerId;
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.order.enums;

import de.hybris.platform.ordercancel.model.OrderCancelRecordModel;
import de.hybris.platform.returns.model.OrderReturnRecordModel;
import de.hybris.platform.returns.model.ReturnEntryModel;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public enum OrderModificationStatusEnum {
    Cancelled,
    Exchanged,
    Refunded;

    public static OrderModificationStatusEnum getStatusType(final Object object) {
        if (object instanceof OrderReturnRecordModel) {
            return Refunded;
        }
        if (object instanceof OrderCancelRecordModel) {
            return Cancelled;
        }
        if (object instanceof ReturnEntryModel) {
            return Exchanged;
        }
        throw new UnsupportedOperationException("No Order modification for status : " + object.getClass());
    }

}

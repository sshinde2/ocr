/**
 * 
 */
package au.com.target.tgtfacades.order.enums;

/**
 * @author jhermoso
 *
 */
public enum TargetUpdateCardErrorType {

    INVALID_ORDER, INVALID_ORDER_STATE;

}

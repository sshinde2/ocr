/**
 * 
 */
package au.com.target.tgtfacades.order.exception;

import au.com.target.tgtfacades.order.enums.TargetUpdateCardErrorType;


/**
 * @author jhermoso
 *
 */
public class TargetUpdateCardException extends Exception {

    private final TargetUpdateCardErrorType errorType;

    public TargetUpdateCardException(final String message, final TargetUpdateCardErrorType errorType) {
        super(message);
        this.errorType = errorType;
    }

    /**
     * @return the errorType
     */
    public TargetUpdateCardErrorType getErrorType() {
        return errorType;
    }

}

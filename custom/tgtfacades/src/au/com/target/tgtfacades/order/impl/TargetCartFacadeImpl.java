/**
 * 
 */
package au.com.target.tgtfacades.order.impl;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartRestoration;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.util.PriceValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.cart.exception.ProductNotFoundCommerceCartModificationException;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.giftcards.GiftCardService;
import au.com.target.tgtcore.giftcards.GiftRecipientService;
import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.PostCodeGroupModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.postcode.service.TargetPostCodeService;
import au.com.target.tgtcore.product.exception.ProductNotFoundException;
import au.com.target.tgtcore.util.ProductUtil;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade;
import au.com.target.tgtfacades.giftcards.exceptions.GiftCardValidationException;
import au.com.target.tgtfacades.giftcards.validate.BaseGiftCardValidator;
import au.com.target.tgtfacades.giftcards.validate.GiftCardValidator;
import au.com.target.tgtfacades.order.TargetCartFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.data.DeliveryModeData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtlayby.cart.data.DeliveryModeInformation;
import au.com.target.tgtpayment.util.PaymentTransactionHelper;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class TargetCartFacadeImpl extends DefaultCartFacade implements TargetCartFacade {

    protected static final Logger LOG = Logger.getLogger(TargetCartFacadeImpl.class);

    private static final String SLASH = "/";

    private static final String UNALLOCATED = "unallocated";

    private TargetCommerceCartService targetCommerceCartService;

    private TargetLaybyCartService targetLaybyCartService;

    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;

    private TargetDeliveryService targetDeliveryService;

    private FlybuysDiscountFacade flybuysDiscountFacade;

    private SessionService sessionService;

    private ModelService modelService;

    private TargetOrderService targetOrderService;

    private TargetUserFacade targetUserFacade;

    private TargetPostCodeService targetPostCodeService;

    private TargetCheckoutFacade targetCheckoutFacade;

    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    private GiftRecipientService giftRecipientService;

    private GiftCardService giftCardService;

    private List<BaseGiftCardValidator> giftCardValidators;

    private ConfigurationService configurationService;

    private GiftCardValidator maxGiftCardQtyValidator;


    /* 
     * Override to handle the case of products going missing
     */
    @Override
    public CartData getSessionCart() {
        CartData cartData;
        if (hasSessionCart()) {
            final CartModel cart = targetLaybyCartService.getSessionCart();
            modelService.refresh(cart);
            try {
                cartData = getCartConverter().convert(cart);
            }
            catch (final UnknownIdentifierException e) {
                // Cart has unknown products in it, eg one might have become unapproved
                // Refresh cart then try again
                targetCommerceCartService.removeMissingProducts(cart);
                cartData = getCartConverter().convert(cart);
            }
        }
        else {
            cartData = createEmptyCart();
        }

        return cartData;
    }

    @Override
    public Map<String, TargetCartData> getCarts() {
        final CartModel cartModel = targetLaybyCartService.getSessionCart();
        final Map<String, TargetCartData> cartsData = new HashMap<String, TargetCartData>();
        cartsData.put(cartModel.getPurchaseOption().getCode(), (TargetCartData)getCartConverter().convert(cartModel));

        return cartsData;
    }

    @Override
    public boolean isCartWithDigitalOnlyProducts() {
        final CartModel currentCart = targetLaybyCartService.getSessionCart();
        Assert.notNull(currentCart, "Cart cannot be null");
        final List<AbstractOrderEntryModel> orderEntries = currentCart.getEntries();
        Assert.notNull(orderEntries, "Order Entries cannot be null");

        for (final AbstractOrderEntryModel entry : orderEntries) {
            final TargetZoneDeliveryModeModel deliveryMode = targetDeliveryModeHelper.getDigitalDeliveryMode(entry
                    .getProduct());
            if (null == deliveryMode) {
                return false;
            }
        }
        return true;
    }

    @Override
    public CartModificationData addToCart(final ProductModel productModel, final long qty)
            throws CommerceCartModificationException {
        final CartModel cartModel = targetLaybyCartService.getSessionCart();

        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cartModel, productModel,
                qty, productModel.getUnit());

        return getCartModificationConverter().convert(cartModification);
    }

    @Override
    public CartModificationData addToCart(final String productCode, final long qty)
            throws CommerceCartModificationException {
        final GiftRecipientDTO giftRecipient = null;
        return addToCart(productCode, qty, giftRecipient);
    }

    @Override
    public CartModificationData addToCart(final String productCode, final long qty, final GiftRecipientDTO recipient)
            throws CommerceCartModificationException {
        final ProductModel productModel;

        try {
            productModel = getProductService().getProductForCode(productCode);
        }
        catch (final UnknownIdentifierException | AmbiguousIdentifierException ex) {
            throw new ProductNotFoundCommerceCartModificationException(
                    ex.getMessage(), ex);
        }
        final boolean isProductADigitalGiftCard = giftCardService.isProductADigitalGiftCard(productModel);
        if (isProductADigitalGiftCard && recipient == null) {
            throw new CommerceCartModificationException("Error GiftCard Details missing for GiftCard Product");
        }
        else if (!isProductADigitalGiftCard && recipient != null) {
            throw new CommerceCartModificationException("Error GiftCard Details present for a Non-GiftCard product");
        }

        final CartModel cartModel = targetLaybyCartService.getSessionCart();

        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cartModel, productModel,
                qty, productModel.getUnit());

        if (recipient != null && cartModification.getEntry() != null) {
            giftRecipientService.addGiftRecipientToOrderEntry(recipient, cartModification.getEntry());
        }

        flybuysDiscountFacade.reassessFlybuysDiscountOnCheckoutCart();

        return getCartModificationConverter().convert(cartModification);
    }

    @Override
    public void validateGiftCards(final String productCode, final long newQty)
            throws GiftCardValidationException, ProductNotFoundException {
        if (CollectionUtils.isNotEmpty(giftCardValidators)) {
            for (final BaseGiftCardValidator validator : giftCardValidators) {
                validator.validateGiftCards(productCode, newQty);
            }
        }
    }

    @Override
    public CartModificationData updateCartEntry(final ProductModel productModel, final long newQty)
            throws CommerceCartModificationException {
        CommerceCartModification commerceCartModification = null;
        try {
            commerceCartModification = targetCommerceCartService.updateEntry(
                    targetLaybyCartService.getSessionCart(), productModel,
                    newQty);
        }
        catch (final CommerceCartModificationException e) {
            LOG.error(e.getMessage(), e);
            throw new CommerceCartModificationException("Cannot update cart entry", e);
        }
        return getCartModificationConverter().convert(commerceCartModification);
    }

    @Override
    public CartModificationData updateCartEntry(final String productCode, final long newQty)
            throws CommerceCartModificationException {
        final ProductModel productModel = getProductService().getProductForCode(productCode);
        return updateCartEntry(productModel, newQty);
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCartFacade#removeGiftCardRecipientEntry(java.lang.String, int)
     */
    @Override
    public CartModificationData removeGiftCardRecipientEntry(final String productCode,
            final int recipientEntryToRemove, final String id)
                    throws CommerceCartModificationException {
        final ProductModel productModel = getProductService().getProductForCode(productCode);
        CommerceCartModification commerceCartModification = null;
        try {
            commerceCartModification = targetCommerceCartService.removeGiftCardRecipient(
                    targetLaybyCartService.getSessionCart(), productModel,
                    recipientEntryToRemove, id);
        }
        catch (final CommerceCartModificationException e) {
            LOG.error(e.getMessage(), e);
            throw new CommerceCartModificationException("Cannot remove recipient for the giftcard entry for product="
                    + productCode, e);
        }
        return getCartModificationConverter().convert(commerceCartModification);
    }

    @Override
    public Map<String, DeliveryModeData> getDeliveryModeOptionsInformation() {
        final CartModel checkoutCart = getTheSessionCart();

        if (checkoutCart == null) {
            return null;
        }

        final List<DeliveryModeInformation> deliveryModeInformationList = targetCommerceCartService
                .getDeliveryModeOptionsInformation(checkoutCart);

        final Map<String, DeliveryModeData> deliveryModeDataMap = new HashMap<String, DeliveryModeData>();

        for (final DeliveryModeInformation deliveryModeInformation : deliveryModeInformationList) {
            final DeliveryModeData deliveryModeData = createDeliveryModeData(deliveryModeInformation);

            if (deliveryModeData != null) {
                deliveryModeDataMap.put(deliveryModeData.getDeliveryModeCode(), deliveryModeData);
            }
        }

        return deliveryModeDataMap;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCartFacade#getDeliveryModeOptionInformation(java.lang.String)
     */
    @Override
    public DeliveryModeData getDeliveryModeOptionInformation(final String deliveryModeCode) {
        Validate.notNull(deliveryModeCode, "deliveryModeCode must not be null");

        final CartModel checkoutCart = getTheSessionCart();

        if (checkoutCart == null) {
            return null;
        }

        final DeliveryModeModel deliveryMode = targetDeliveryService.getDeliveryModeForCode(deliveryModeCode);

        if (deliveryMode == null) {
            LOG.info("No delivery mode found for code " + deliveryModeCode);
            return null;
        }

        final DeliveryModeInformation deliveryModeInformation = targetCommerceCartService
                .getDeliveryModeOptionInformation(checkoutCart, deliveryMode);

        if (deliveryModeInformation == null) {
            LOG.info("No delivery mode information found for delivery mode " + deliveryMode.getCode());
            return null;
        }

        return createDeliveryModeData(deliveryModeInformation);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CartModificationData updateGiftCardDetails(final String productCode, final GiftRecipientDTO recipient,
            final int giftcardRecipientEntry, final String id)
                    throws CommerceCartModificationException {
        Assert.notNull(recipient, "Recipient cannot be null");
        final ProductModel product = getProductService().getProductForCode(productCode);
        final CommerceCartModification modification = targetCommerceCartService.updateGiftcardRecipientDetails(
                targetLaybyCartService.getSessionCart(), product, giftcardRecipientEntry, id, recipient);
        return getCartModificationConverter().convert(modification);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasItemsInAnyCart() {
        final CartModel cart = getCartService().getSessionCart();
        return CollectionUtils.isNotEmpty(cart.getEntries());
    }

    private DeliveryModeData createDeliveryModeData(final DeliveryModeInformation deliveryModeInformation) {
        if (!(deliveryModeInformation.getDeliveryMode() instanceof TargetZoneDeliveryModeModel)) {
            return null;
        }

        final TargetZoneDeliveryModeModel targetDeliveryModeModel = (TargetZoneDeliveryModeModel)deliveryModeInformation
                .getDeliveryMode();
        final AbstractTargetZoneDeliveryModeValueModel targetDeliveryModeValueModel = (AbstractTargetZoneDeliveryModeValueModel)deliveryModeInformation
                .getDeliveryModeValue();

        final DeliveryModeData deliveryModeData = new DeliveryModeData();
        deliveryModeData.setDeliveryModeCode(targetDeliveryModeModel.getCode());
        deliveryModeData.setDeliverToStores(targetDeliveryModeModel.getIsDeliveryToStore().booleanValue());
        if (targetDeliveryModeValueModel != null) {
            deliveryModeData.setMessage(targetDeliveryModeValueModel.getMessage());
            deliveryModeData.setDisclaimer(targetDeliveryModeValueModel.getDisclaimer());
        }
        else {
            deliveryModeData
                    .setMessage(configurationService.getConfiguration().getString(
                            TgtFacadesConstants.NO_DEL_MODE_VALUE_LONG_MSG));
        }
        final PriceValue deliveryFee = deliveryModeInformation.getDeliveryFee();
        if (deliveryFee != null) {
            final PriceData deliveryFeeData = getPriceDataFactory().create(PriceDataType.BUY,
                    BigDecimal.valueOf(deliveryFee.getValue()), deliveryFee.getCurrencyIso());

            deliveryModeData.setDeliveryFee(deliveryFeeData);
        }
        else {
            deliveryModeData.setDeliveryFee(null);
        }

        return deliveryModeData;
    }

    protected CartModel getTheSessionCart() {
        final CartModel cartModel = getCartService().getSessionCart();

        if (cartModel == null) {
            LOG.info("No cart found in session");
        }

        return cartModel;
    }

    @Override
    public boolean restoreSavedCart() throws CommerceCartRestorationException {
        final CustomerModel currentUser = (CustomerModel)getUserService().getCurrentUser();
        final CartModel savedCart = targetLaybyCartService.getMostRecentCartForCustomer(currentUser);
        if (null != savedCart) {
            return restoreCart(savedCart);
        }
        return false;
    }

    /**
     * @param savedCart
     * @throws CommerceCartRestorationException
     */
    private boolean restoreCart(final CartModel savedCart) throws CommerceCartRestorationException {
        final List<PaymentTransactionModel> ptmList = savedCart.getPaymentTransactions();
        if (!PaymentTransactionHelper.hasCaptureTransactionInAcceptedOrReview(ptmList)) {
            savedCart.setPurchaseOptionConfig(targetPurchaseOptionHelper.getCurrentPurchaseOptionConfigModel());
            final CommerceCartParameter cartParameter = new CommerceCartParameter();
            cartParameter.setCart(savedCart);
            return isCartEntryModified(targetCommerceCartService.restoreCart(cartParameter));
        }
        return false;
    }

    @Override
    public void restoreCart(final String guid) throws CommerceCartRestorationException {
        final CartModel cartForGuidAndSite = getCommerceCartService().getCartForGuidAndSite(guid,
                getBaseSiteService().getCurrentBaseSite());
        if (cartForGuidAndSite != null) {
            restoreCart(cartForGuidAndSite);
        }

    }

    /**
     * This method is used when restoring the cart, to identify the the change of order entry quantity.
     *
     * @param restoreCart
     */
    private boolean isCartEntryModified(final CommerceCartRestoration restoreCart) {
        for (final CommerceCartModification modification : restoreCart.getModifications()) {
            if (modification.getQuantityAdded() != 0) {
                return true;
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCartFacade#getGuidForCurrentSessionCart()
     */
    @Override
    public String getGuidForCurrentSessionCart() {
        if (hasSessionCart()) {
            final CartModel currentCart = targetLaybyCartService.getSessionCart();
            if (StringUtils.isNotEmpty(currentCart.getGuid())) {
                return targetLaybyCartService.getSessionCart().getGuid();
            }
            else {
                LOG.warn("There is no Guid for the cart Number::" + currentCart.getCode());
            }
        }
        return null;
    }

    @Override
    public String getPostalCodeFromCartOrSession(final CartData cartData) {
        final AddressData addressData = cartData.getDeliveryAddress();
        String postCode = ObjectUtils.toString(getSessionService().getAttribute(TgtCoreConstants.SESSION_POSTCODE));
        if (null != addressData) {
            postCode = ObjectUtils.toString(addressData.getPostalCode());
            getSessionService().setAttribute(TgtCoreConstants.SESSION_POSTCODE, postCode);
            return postCode;
        }
        else if (cartData.getDeliveryMode() == null && StringUtils.isBlank(postCode)) {
            postCode = getPostCodeFormPreferredDeliveryMode();
            getSessionService().setAttribute(TgtCoreConstants.SESSION_POSTCODE, postCode);
            final CartModel cartModel = getTheSessionCart();
            final DeliveryModeModel preferredDeliveryMode = preferredDeliveryMode(cartModel);
            if (null != preferredDeliveryMode
                    && getTargetDeliveryModeHelper().isDeliveryModeApplicableForOrder(cartModel,
                            preferredDeliveryMode)) {
                getTargetCheckoutFacade().updateDeliveryMode(preferredDeliveryMode.getCode());
            }
            return postCode;
        }
        return postCode;
    }

    protected String getPostCodeFormPreferredDeliveryMode() {

        String preferredPostCode = StringUtils.EMPTY;
        final CartModel cartModel = getTheSessionCart();
        if (!targetUserFacade.isAnonymousUser() && cartModel.getUser() instanceof TargetCustomerModel) {
            final TargetCustomerModel customer = (TargetCustomerModel)cartModel.getUser();
            final OrderModel previousOrder = targetOrderService.findLatestOrderForUser(customer);
            if (null != previousOrder && null != previousOrder.getDeliveryAddress()) {
                preferredPostCode = previousOrder.getDeliveryAddress().getPostalcode();
            }
        }
        return preferredPostCode;
    }

    protected DeliveryModeModel preferredDeliveryMode(final CartModel cartModel) {

        if (!targetUserFacade.isAnonymousUser() && cartModel.getUser() instanceof TargetCustomerModel) {
            final TargetCustomerModel customer = (TargetCustomerModel)cartModel.getUser();
            final OrderModel previousOrder = targetOrderService.findLatestOrderForUser(customer);
            if (null != previousOrder && null != previousOrder.getDeliveryAddress()) {
                return previousOrder.getDeliveryMode();
            }
        }
        return null;
    }

    @Override
    public String getCatchmentAreaByPostcode(final String postcode) {
        if (StringUtils.isEmpty(postcode)) {
            return StringUtils.EMPTY;
        }
        final Collection<PostCodeGroupModel> postcodeGroups = targetPostCodeService
                .getPostCodeGroupByPostcode(postcode);
        if (CollectionUtils.isEmpty(postcodeGroups)) {
            return UNALLOCATED;
        }
        final Iterator<PostCodeGroupModel> it = postcodeGroups.iterator();
        final StringBuilder stringBuilder = new StringBuilder(it.next().getCatchmentArea());
        while (it.hasNext()) {
            stringBuilder.append(SLASH + it.next().getCatchmentArea());
        }
        return stringBuilder.toString();
    }

    /**
     * {@inheritDoc} parameter cartService must be an instance of TargetLaybyCartService
     */
    @Override
    @Required
    public void setCartService(final CartService cartService) {
        Assert.isInstanceOf(TargetLaybyCartService.class, cartService);
        targetLaybyCartService = (TargetLaybyCartService)cartService;
        super.setCartService(cartService);
    }

    /**
     * {@inheritDoc} parameter cartService must be an instance of TargetCommerceCartService
     */
    @Override
    @Required
    public void setCommerceCartService(final CommerceCartService commerceCartService) {
        Assert.isInstanceOf(TargetCommerceCartService.class, commerceCartService);
        targetCommerceCartService = (TargetCommerceCartService)commerceCartService;
        super.setCommerceCartService(commerceCartService);
    }

    @Override
    public void validateGiftCardsUpdateQty(final String productCode, final long newQty)
            throws GiftCardValidationException {
        maxGiftCardQtyValidator.validateGiftCardsUpdateQty(productCode, newQty);
    }

    @Override
    public void validatePhysicalGiftCards(final String productCode, final long qty)
            throws GiftCardValidationException {
        final ProductModel productModel = getProductService().getProductForCode(productCode);
        maxGiftCardQtyValidator.validatePhysicalGiftCards(productModel, qty);
    }

    @Override
    public boolean isProductAGiftCard(final String productCode) {
        final ProductModel product = getProductService().getProductForCode(productCode);
        return giftCardService.isProductAGiftCard(product);
    }


    @Override
    public boolean isProductADigitalGiftCard(final String productCode) {
        final ProductModel product = getProductService().getProductForCode(productCode);
        return giftCardService.isProductADigitalGiftCard(product);
    }

    @Override
    public boolean hasVariantsForProduct(final String productCode) {
        final ProductModel productModel = getProductService().getProductForCode(productCode);
        return productModel.getVariantType() != null
                && CollectionUtils.isNotEmpty(productModel.getVariants());
    }

    @Override
    public boolean hasSizeVariantsForProduct(final String productCode) {
        final ProductModel productModel = getProductService().getProductForCode(productCode);
        return TargetSizeVariantProductModel._TYPECODE.equals(productModel.getVariantType().getCode())
                || productModel.getVariants().size() == 1;
    }

    @Override
    public boolean isProductAPhysicalGiftcard(final String productCode) {
        final ProductModel productModel = getProductService().getProductForCode(productCode);
        return ProductUtil.isProductTypePhysicalGiftcard(productModel);
    }

    protected SessionService getSessionService() {
        return sessionService;
    }

    /**
     * @param targetPurchaseOptionHelper
     *            the targetPurchaseOptionHelper to set
     */
    @Required
    public void setTargetPurchaseOptionHelper(final TargetPurchaseOptionHelper targetPurchaseOptionHelper) {
        this.targetPurchaseOptionHelper = targetPurchaseOptionHelper;
    }

    /**
     * @param targetDeliveryService
     *            the targetDeliveryService to set
     */
    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        this.targetDeliveryService = targetDeliveryService;
    }

    /**
     * @param flybuysDiscountFacade
     *            the flybuysDiscountFacade to set
     */
    @Required
    public void setFlybuysDiscountFacade(final FlybuysDiscountFacade flybuysDiscountFacade) {
        this.flybuysDiscountFacade = flybuysDiscountFacade;
    }

    /**
     * @param sessionService
     *            the sessionService to set
     */
    @Required
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

    /**
     * 
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @return the modelService
     */
    public ModelService getModelService() {
        return modelService;
    }

    /**
     * @param targetOrderService
     *            the targetOrderService to set
     */
    @Required
    public void setTargetOrderService(final TargetOrderService targetOrderService) {
        this.targetOrderService = targetOrderService;
    }

    /**
     * @param targetUserFacade
     *            the targetUserFacade to set
     */
    @Required
    public void setTargetUserFacade(final TargetUserFacade targetUserFacade) {
        this.targetUserFacade = targetUserFacade;
    }


    @Required
    public void setTargetPostCodeService(final TargetPostCodeService targetPostCodeService) {
        this.targetPostCodeService = targetPostCodeService;
    }

    /**
     * @param targetCheckoutFacade
     *            the targetCheckoutFacade to set
     */
    @Required
    public void setTargetCheckoutFacade(final TargetCheckoutFacade targetCheckoutFacade) {
        this.targetCheckoutFacade = targetCheckoutFacade;
    }

    /**
     * @param configurationService
     *            the configurationService to set
     */
    @Required
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    /**
     * @return the targetCheckoutFacade
     */
    public TargetCheckoutFacade getTargetCheckoutFacade() {
        return targetCheckoutFacade;
    }


    /**
     * @return the targetDeliveryModeHelper
     */
    public TargetDeliveryModeHelper getTargetDeliveryModeHelper() {
        return targetDeliveryModeHelper;
    }

    /**
     * @param targetDeliveryModeHelper
     *            the targetDeliveryModeHelper to set
     */
    @Required
    public void setTargetDeliveryModeHelper(final TargetDeliveryModeHelper targetDeliveryModeHelper) {
        this.targetDeliveryModeHelper = targetDeliveryModeHelper;
    }

    /**
     * @param giftRecipientService
     *            the giftRecipientService to set
     */
    @Required
    public void setGiftRecipientService(final GiftRecipientService giftRecipientService) {
        this.giftRecipientService = giftRecipientService;
    }

    /**
     * @param giftCardValidators
     *            the giftCardValidators to set
     */
    @Required
    public void setGiftCardValidators(final List<BaseGiftCardValidator> giftCardValidators) {
        this.giftCardValidators = giftCardValidators;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCartFacade#getCartsColorVariantProductCodes()
     */
    @Override
    public List<String> getCartsColorVariantProductCodes() {
        List<String> cvProductCodes = null;
        final CartModel cart = getTheSessionCart();
        if (CollectionUtils.isNotEmpty(cart.getEntries())) {
            cvProductCodes = new ArrayList<>();
            for (final AbstractOrderEntryModel entry : cart.getEntries()) {
                final AbstractTargetVariantProductModel colorVariant = getColorVariant(entry.getProduct());
                if (null != colorVariant) {
                    cvProductCodes.add(colorVariant.getCode());
                }
            }
        }
        return cvProductCodes;
    }

    /**
     * @param product
     * @return TargetColourVariantProductModel
     */
    protected TargetColourVariantProductModel getColorVariant(final ProductModel product) {
        if (product instanceof TargetColourVariantProductModel) {
            return (TargetColourVariantProductModel)product;
        }
        else if (product instanceof AbstractTargetVariantProductModel) {
            return getColorVariant(((AbstractTargetVariantProductModel)product).getBaseProduct());
        }
        return null;
    }

    /**
     * @param giftCardService
     *            the giftCardService to set
     */
    @Required
    public void setGiftCardService(final GiftCardService giftCardService) {
        this.giftCardService = giftCardService;
    }

    /**
     * @param maxGiftCardQtyValidator
     *            the maxGiftCardQtyValidator to set
     */
    @Required
    public void setMaxGiftCardQtyValidator(final GiftCardValidator maxGiftCardQtyValidator) {
        this.maxGiftCardQtyValidator = maxGiftCardQtyValidator;
    }


}

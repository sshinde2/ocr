/**
 *
 */
package au.com.target.tgtfacades.order.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.impl.DefaultCheckoutFacade;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.payment.dto.BillingInfo;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.voucher.VoucherModelService;
import de.hybris.platform.voucher.jalo.util.VoucherValue;
import de.hybris.platform.voucher.model.VoucherModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.customer.TargetCustomerAccountService;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.giftcards.GiftCardService;
import au.com.target.tgtcore.jalo.ProductType;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.model.StoreEmployeeModel;
import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtcore.order.TargetCommerceCartModificationStatus;
import au.com.target.tgtcore.order.TargetCommerceCheckoutService;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.postcode.service.TargetPostCodeService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.util.ProductUtil;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.converters.AbstractOrderHelper;
import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;
import au.com.target.tgtfacades.order.data.AdjustedEntryQuantityData;
import au.com.target.tgtfacades.order.data.ClickAndCollectDeliveryDetailData;
import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtfacades.user.converters.populator.TargetAddressReversePopulator;
import au.com.target.tgtfacades.user.data.IpgPaymentInfoData;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtfluent.data.StockDatum;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.service.FluentStockLookupService;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.order.TargetLaybyCommerceCheckoutService;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.dto.HostedSessionTokenRequest;
import au.com.target.tgtpayment.dto.TargetCardInfo;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;
import au.com.target.tgtpayment.model.ZippayPaymentInfoModel;
import au.com.target.tgtpayment.service.TargetPaymentService;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;




/**
 * @author rmcalave
 * 
 */
public class TargetCheckoutFacadeImpl extends DefaultCheckoutFacade implements TargetCheckoutFacade {

    private static final Logger LOG = Logger.getLogger(TargetCheckoutFacadeImpl.class);

    private static final String THREATMATRIX_SESSIONID_PREFIX = "target";

    private static final String IPG_BASE_URL = "tgtpaymentprovider.ipg.ipgSessionUrl";

    private static final String CLICK_AND_COLLECT = "click-and-collect";

    private static final String HOME_DELIVERY = "home-delivery";

    private static final String EXPRESS_DELIVERY = "express-delivery";

    @Autowired
    private TargetCommerceCheckoutService targetCommerceCheckoutService;

    @Autowired
    private TargetLaybyCommerceCheckoutService targetLaybyCommerceCheckoutService;

    @Autowired
    private CartService cartService;

    @Autowired
    private Converter<CartModel, CartData> cartConverter;

    @Autowired
    private Converter<CartModel, CartData> miniCartConverter;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;

    @Autowired
    private TargetDeliveryService targetDeliveryService;

    @Autowired
    private PriceDataFactory priceDataFactory;

    @Autowired
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Autowired
    private CommerceCartService commerceCartService;

    @Autowired
    private TargetPaymentService targetPaymentService;

    @Autowired
    private PaymentModeService paymentModeService;

    @Autowired
    private TargetCustomerAccountService targetCustomerAccountService;

    @Autowired
    private AbstractOrderHelper abstractOrderHelper;

    @Autowired
    private TargetUserFacade targetUserFacade;

    @Autowired
    private TargetOrderService targetOrderService;

    @Autowired
    private TargetVoucherService targetVoucherService;

    @Autowired
    private VoucherModelService voucherModelService;

    @Autowired
    private TargetAddressReversePopulator targetAddressReversePopulator;

    @Autowired
    private Converter<PinPadPaymentInfoModel, CCPaymentInfoData> targetPinPadPaymentInfoConverter;

    @Autowired
    private Converter<IpgPaymentInfoModel, CCPaymentInfoData> targetIpgPaymentInfoConverter;

    @Autowired
    private ModelService modelService;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private CalculationService calculationService;

    @Autowired
    private GiftCardService giftCardService;

    @Autowired
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Autowired
    private TargetPostCodeService postcodeService;

    @Autowired
    private TargetDeliveryService deliveryService;

    @Autowired
    private ProductFacade productFacade;

    @Autowired
    private FluentStockLookupService fluentStockLookupService;

    @Autowired
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Autowired
    private TargetStockService targetStockService;

    @Autowired
    private TargetWarehouseService targetWarehouseService;


    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#setDeliveryType(de.hybris.platform.commercefacades.order.data.CartData, au.com.target.tgtcore.enums.DeliveryTypeEnum)
     */
    @Override
    public boolean setDeliveryMode(final String deliveryModeCode) {
        final CartModel cartModel = getCart();
        if (cartModel != null) {
            final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
            checkoutParameter.setCart(cartModel);
            if (StringUtils.isNotBlank(deliveryModeCode)) {
                final DeliveryModeModel deliveryMode = targetDeliveryService.getDeliveryModeForCode(deliveryModeCode);
                checkoutParameter.setDeliveryMode(deliveryMode);
            }
            return targetCommerceCheckoutService.setDeliveryMode(checkoutParameter);
        }

        return false;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#setClickAndCollectDeliveryDetails(au.com.target.tgtfacades.order.data.ClickAndCollectDeliveryDetailData)
     */
    @Override
    public boolean setClickAndCollectDeliveryDetails(final ClickAndCollectDeliveryDetailData cncData) {
        final CartModel cartModel = getCart();

        if (cartModel != null) {
            return targetCommerceCheckoutService.fillCart4ClickAndCollect(cartModel, cncData.getStoreNumber()
                    .intValue(), cncData.getTitle(), cncData.getFirstName(), cncData.getLastName(),
                    cncData.getTelephoneNumber(), cncData.getDeliveryModeModel());
        }

        return false;
    }

    @Override
    public TargetPointOfServiceData setClickAndCollectStore(final int storeNumber) {
        final CartModel cartModel = getCart();
        final TargetPointOfServiceData posData = targetStoreLocatorFacade.getPointOfService(Integer
                .valueOf(storeNumber));
        if (cartModel != null && posData != null) {
            final Set<String> cartProductTypeSet = abstractOrderHelper.getProductTypeSet(cartModel);
            abstractOrderHelper.updateStoreWithPickupAvailability(cartProductTypeSet, posData);
            if (Boolean.TRUE.equals(posData.getAvailableForPickup())) {
                cartModel.setCncStoreNumber(Integer.valueOf(storeNumber));
                modelService.save(cartModel);
            }
        }
        return posData;
    }

    @Override
    public boolean isSupportedDeliveryMode(final String deliveryModeCode) {
        boolean isSupportedDeliveryMode = false;
        final CartModel cartModel = getCart();
        if (cartModel != null) {
            final DeliveryModeModel deliveryMode = targetDeliveryService.getDeliveryModeForCode(deliveryModeCode);
            if (deliveryMode != null) {
                isSupportedDeliveryMode = targetDeliveryModeHelper
                        .isDeliveryModeApplicableForOrder(cartModel, deliveryMode);
            }
        }
        return isSupportedDeliveryMode;
    }

    @Override
    protected TargetAddressData getDeliveryAddress() {
        final CartModel cart = getCart();
        if (cart != null) {
            final AddressModel deliveryAddress = cart.getDeliveryAddress();
            if (deliveryAddress != null) {
                final DeliveryModeModel deliveryMode = cart.getDeliveryMode();
                if (null != deliveryMode && targetDeliveryModeHelper.isDeliveryModeStoreDelivery(deliveryMode)) {
                    return (TargetAddressData)getAddressConverter().convert(deliveryAddress);
                }

                // Ensure that the delivery address is in the set of supported addresses
                final AddressModel supportedAddress = getDeliveryAddressModelForCode(
                        deliveryAddress.getPk().toString());
                if (supportedAddress != null) {
                    return (TargetAddressData)getAddressConverter().convert(supportedAddress);
                }
            }
        }

        return null;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#hasPurchaseOptionCheckoutCart(java.lang.String)
     */
    @Override
    public boolean hasCheckoutCart() {
        final PurchaseOptionModel activePurchaseOption = getActivePurchaseOption();

        if (activePurchaseOption == null) {
            return false;
        }

        final boolean hasCheckoutCart = targetLaybyCommerceCheckoutService.hasCheckoutCart(activePurchaseOption);

        return hasCheckoutCart;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#getPurchaseOptionCheckoutCart(java.lang.String)
     */
    @Override
    public CartData getCheckoutCart() {
        final CartModel cart = getSessionCart();
        return getCheckoutCart(cart);
    }

    @Override
    public CartData getCheckoutCart(final CartModel cart) {
        final CartData cartData;

        if (cart != null) {
            cartData = cartConverter.convert(cart);
            cartData.setDeliveryAddress(getDeliveryAddress());
            cartData.setDeliveryMode(getDeliveryMode());
            cartData.setPaymentInfo(getPaymentDetails());
        }
        else {
            cartData = createEmptyCart();
        }
        return cartData;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#getPurchaseOptionCart(java.lang.String)
     */
    @Override
    public CartModel getCart() {
        final CartModel cartModel = cartService.getSessionCart();

        return cartModel;
    }

    private CartModel getSessionCart() {
        if (getCartFacade().hasSessionCart()) {
            return cartService.getSessionCart();
        }
        return null;
    }

    /**
     * 
     * @return CartData
     */
    protected CartData createEmptyCart() {
        return miniCartConverter.convert(null);
    }

    @Override
    public boolean setTeamMemberDiscountCardNumber(final String cardNumber) {
        final CartModel cart = getCart();

        if (cart == null) {
            return false;
        }

        return targetCommerceCheckoutService.setTMDCardNumber(cart, cardNumber);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#getTNSSessionToken()
     */
    @Override
    public TargetCreateSubscriptionResult getTNSSessionToken() {
        final PaymentModeModel paymentMode = paymentModeService.getPaymentModeForCode(TgtFacadesConstants.CREDIT_CARD);
        final CartModel cart = getCart();

        if (cart == null) {
            return null;
        }
        final HostedSessionTokenRequest hostedSessionTokenRequest = getHostedSessionTokenRequest(cart,
                paymentMode, null, null, PaymentCaptureType.PLACEORDER,
                null);
        return getTargetPaymentService().getHostedSessionToken(hostedSessionTokenRequest);
    }

    @Override
    public TargetCreateSubscriptionResult getPayPalSessionToken(final String returnUrl, final String cancelUrl) {
        return getSessionToken(returnUrl, cancelUrl, TgtFacadesConstants.PAYPAL);
    }

    @Override
    public TargetCreateSubscriptionResult getAfterpaySessionToken(final String returnUrl, final String cancelUrl) {
        return getSessionToken(returnUrl, cancelUrl, TgtFacadesConstants.AFTERPAY);
    }

    @Override
    public TargetCreateSubscriptionResult getZippaySessionToken(final String redirectUrl) {
        return getSessionToken(redirectUrl, redirectUrl, TgtFacadesConstants.ZIPPAY);
    }

    /**
     * @param returnUrl
     * @param cancelUrl
     * @param paymentModeCode
     * @return the session token
     */
    private TargetCreateSubscriptionResult getSessionToken(final String returnUrl, final String cancelUrl,
            final String paymentModeCode) {
        Validate.isTrue(StringUtils.isNotBlank(returnUrl), "returnUrl must not be blank");
        Validate.isTrue(StringUtils.isNotBlank(cancelUrl), "cancelUrl must not be blank");

        final PaymentModeModel paymentMode = paymentModeService.getPaymentModeForCode(paymentModeCode);
        final CartModel cart = getCart();

        if (cart == null) {
            return null;
        }

        final HostedSessionTokenRequest hostedSessionTokenRequest = getHostedSessionTokenRequest(cart, paymentMode,
                cancelUrl, returnUrl, PaymentCaptureType.PLACEORDER,
                null);
        return getTargetPaymentService()
                .getHostedSessionToken(hostedSessionTokenRequest);
    }

    private HostedSessionTokenRequest getHostedSessionTokenRequest(final CartModel cart,
            final PaymentModeModel paymentMode,
            final String cancelUrl, final String returnUrl, final PaymentCaptureType pct,
            final String sessionId) {
        final CustomerModel customerModel = (CustomerModel)cart.getUser();
        final List<CreditCardPaymentInfoModel> savedCreditCards = targetCustomerAccountService
                .getCreditCardPaymentInfos(customerModel, true);
        final HostedSessionTokenRequest hostedSessionTokenRequest = targetPaymentService
                .createHostedSessionTokenRequest(cart, paymentMode, cancelUrl,
                        returnUrl, pct,
                        sessionId, savedCreditCards);
        return hostedSessionTokenRequest;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#getTNSPostActionUrl()
     */
    @Override
    public CreditCardType getCreditCardType(final String creditCardType) {
        if (creditCardType.equalsIgnoreCase("amex")) {
            return CreditCardType.AMEX;
        }
        else if (creditCardType.equalsIgnoreCase("mastercard")) {
            return CreditCardType.MASTER;
        }
        else if (creditCardType.equalsIgnoreCase("diners_club")) {
            return CreditCardType.DINERS;
        }
        else if (creditCardType.equalsIgnoreCase("Visa")) {
            return CreditCardType.VISA;
        }
        return null;

    }

    @Override
    public CCPaymentInfoData createPaymentSubscription(final CCPaymentInfoData paymentInfoData) {
        validateParameterNotNullStandardMessage("paymentInfoData", paymentInfoData);
        final TargetAddressData billingAddressData = (TargetAddressData)paymentInfoData.getBillingAddress();
        validateParameterNotNullStandardMessage("billingAddress", billingAddressData);

        final CartModel cartModel = getCart();
        if (cartModel != null) {
            final UserModel currentUser = cartModel.getUser();
            final TargetCardInfo cardInfo = new TargetCardInfo();
            cardInfo.setPaymentSessionId(paymentInfoData.getId());
            cardInfo.setCardHolderFullName(paymentInfoData.getAccountHolderName());
            cardInfo.setCardNumber(paymentInfoData.getCardNumber());
            final CreditCardType cardType = (CreditCardType)getEnumerationService().getEnumerationValue(
                    CreditCardType.class.getSimpleName(), paymentInfoData.getCardType());
            cardInfo.setCardType(cardType);
            cardInfo.setExpirationMonth(Integer.valueOf(paymentInfoData.getExpiryMonth()));
            cardInfo.setExpirationYear(Integer.valueOf(paymentInfoData.getExpiryYear()));
            cardInfo.setIssueNumber(paymentInfoData.getIssueNumber());

            final BillingInfo billingInfo = new BillingInfo();
            billingInfo.setCity(billingAddressData.getTown());
            if (billingAddressData.getCountry() != null) {
                billingInfo.setCountry(billingAddressData.getCountry().getIsocode());
            }
            billingInfo.setFirstName(billingAddressData.getFirstName());
            billingInfo.setLastName(billingAddressData.getLastName());
            billingInfo.setPhoneNumber(billingAddressData.getPhone());
            billingInfo.setPostalCode(billingAddressData.getPostalCode());
            billingInfo.setStreet1(billingAddressData.getLine1());
            billingInfo.setStreet2(billingAddressData.getLine2());
            billingInfo.setState(billingAddressData.getState());

            cardInfo.setBillingInfo(billingInfo);
            final PaymentModeModel paymentMode = paymentModeService
                    .getPaymentModeForCode(TgtFacadesConstants.CREDIT_CARD);
            final CreditCardPaymentInfoModel ccPaymentInfoModel = targetCustomerAccountService
                    .createCreditCardPaymentInfo(
                            (CustomerModel)currentUser, paymentMode, cardInfo, billingAddressData.getTitleCode(),
                            paymentInfoData.isSaved());
            if (ccPaymentInfoModel != null) {
                return getCreditCardPaymentInfoConverter().convert(ccPaymentInfoModel);
            }

        }

        return null;
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#createPayPalPaymentInfo(java.lang.String, java.lang.String)
     */
    @Override
    public void createPayPalPaymentInfo(final String token, final String payerId) {
        validateParameterNotNullStandardMessage("token", token);
        validateParameterNotNullStandardMessage("payerId", payerId);

        final CartModel cartModel = getCart();
        if (cartModel == null) {
            return;
        }

        final UserModel currentUser = cartModel.getUser();
        final PaypalPaymentInfoModel payPalPaymentInfo = getCustomerAccountService().createPaypalPaymentInfo(
                (CustomerModel)currentUser, payerId, token);

        final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
        checkoutParameter.setCart(cartModel);
        checkoutParameter.setPaymentInfo(payPalPaymentInfo);
        getCommerceCheckoutService().setPaymentInfo(checkoutParameter);
    }

    @Override
    public void createAfterpayPaymentInfo(final String afterpayToken) {
        validateParameterNotNullStandardMessage("token", afterpayToken);

        final CartModel cartModel = getCart();
        if (cartModel == null) {
            return;
        }

        final UserModel currentUser = cartModel.getUser();
        final AfterpayPaymentInfoModel afterpayPaymentInfo = getCustomerAccountService().createAfterpayPaymentInfo(
                (CustomerModel)currentUser, afterpayToken);

        final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
        checkoutParameter.setCart(cartModel);
        checkoutParameter.setPaymentInfo(afterpayPaymentInfo);
        getCommerceCheckoutService().setPaymentInfo(checkoutParameter);
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#createPayPalPaymentInfo(java.lang.String, java.lang.String)
     */
    @Override
    public void createPinPadPaymentInfo(final TargetAddressData targetBillingAddressData) {
        final CartModel cartModel = getCart();
        if (cartModel == null) {
            return;
        }

        final UserModel currentUser = cartModel.getUser();
        final TargetAddressModel targetBillingAddressModel = getModelService().create(TargetAddressModel.class);

        targetAddressReversePopulator.populate(targetBillingAddressData, targetBillingAddressModel);

        final PinPadPaymentInfoModel pinPadPaymentInfoModel = getCustomerAccountService().createPinPadPaymentInfo(
                (CustomerModel)currentUser, targetBillingAddressModel);

        final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
        checkoutParameter.setCart(cartModel);
        checkoutParameter.setPaymentInfo(pinPadPaymentInfoModel);
        getCommerceCheckoutService().setPaymentInfo(checkoutParameter);
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#createPayPalPaymentInfo(java.lang.String, java.lang.String)
     */
    @Override
    public void createIpgPaymentInfo(final TargetAddressData targetBillingAddressData,
            final IpgPaymentTemplateType ipgPaymentTemplateType) {
        final CartModel cartModel = getCart();
        if (cartModel == null) {
            return;
        }

        final UserModel currentUser = cartModel.getUser();
        TargetAddressModel targetBillingAddressModel = null;
        if (targetBillingAddressData != null) {
            targetBillingAddressModel = getModelService().create(TargetAddressModel.class);
            targetAddressReversePopulator.populate(targetBillingAddressData, targetBillingAddressModel);
        }

        final IpgPaymentInfoModel ipgPaymentInfoModel = getCustomerAccountService().createIpgPaymentInfo(
                (CustomerModel)currentUser, targetBillingAddressModel, ipgPaymentTemplateType);
        final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
        checkoutParameter.setCart(cartModel);
        checkoutParameter.setPaymentInfo(ipgPaymentInfoModel);
        getCommerceCheckoutService().setPaymentInfo(checkoutParameter);
    }

    @Override
    public boolean updateIpgPaymentInfoWithToken(final String token) {
        final CartModel cartModel = getCart();
        boolean updated = false;
        if (cartModel != null) {
            synchronized (cartModel.getCode().intern()) {
                final PaymentInfoModel paymentInfo = cartModel.getPaymentInfo();
                modelService.refresh(paymentInfo);
                if (paymentInfo instanceof IpgPaymentInfoModel) {
                    updated = true;
                    final IpgPaymentInfoModel ipgPaymentInfo = (IpgPaymentInfoModel)paymentInfo;
                    if (!token.equals(ipgPaymentInfo.getToken())) {
                        ipgPaymentInfo.setToken(token);
                        ipgPaymentInfo.setSessionId(cartModel.getUniqueKeyForPayment());
                        modelService.save(ipgPaymentInfo);
                    }
                }
            }
        }
        return updated;
    }

    @Override
    public String getIpgSessionTokenFromCart(final CartData cartData) {
        String sessionToken = null;
        if (cartData != null) {
            final CCPaymentInfoData paymentInfo = cartData.getPaymentInfo();

            if (paymentInfo != null && paymentInfo instanceof TargetCCPaymentInfoData) {
                final IpgPaymentInfoData ipgPaymentInfoData = ((TargetCCPaymentInfoData)paymentInfo)
                        .getIpgPaymentInfoData();

                if (ipgPaymentInfoData != null) {
                    sessionToken = ipgPaymentInfoData.getToken();
                }
            }
        }
        return sessionToken;
    }

    @Override
    public PurchaseOptionModel getActivePurchaseOption() {
        final String purchaseOptionCode = sessionService.getAttribute(TgtFacadesConstants.ACTIVE_PURCHASE_OPTION);
        if (null == purchaseOptionCode) {
            return null;
        }

        final PurchaseOptionModel purchaseOptionModel = targetPurchaseOptionHelper
                .getPurchaseOptionModel(purchaseOptionCode);
        return purchaseOptionModel;
    }

    @Override
    public PurchaseOptionConfigModel getActivePurchaseOptionConfig() {
        final PurchaseOptionModel purchaseOptionModel = getActivePurchaseOption();
        if (null == purchaseOptionModel) {
            return null;
        }

        final CartModel cart = super.getCart();
        if (null == cart) {
            return null;
        }

        return cart.getPurchaseOptionConfig();
    }

    @Override
    public boolean setAmountCurrentPayment(final String payTodayAmount) {
        final CartModel cartModel = getCart();
        if (cartModel == null) {
            return false;
        }

        try {

            if (null != payTodayAmount) {
                return targetLaybyCommerceCheckoutService
                        .setAmountCurrentPayment(cartModel, Double.valueOf(payTodayAmount));
            }

        }
        catch (final NumberFormatException ex) {
            // Shouldn't happen, should be captured by the validator
            return false;
        }
        return targetLaybyCommerceCheckoutService
                .setAmountCurrentPayment(cartModel, null);
    }

    @Override
    protected void afterPlaceOrder(final CartModel cartModel, final OrderModel orderModel) {
        targetLaybyCommerceCheckoutService.afterPlaceOrder(cartModel, orderModel);
    }

    /**
     * @return the commerceCartService
     */
    protected CommerceCartService getCommerceCartService() {
        return commerceCartService;
    }



    /**
     * validate cart delivery mode
     */
    @Override
    public void validateDeliveryMode() {
        final CartModel cartModel = getCart();

        if (cartModel == null) {
            return;
        }
        final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
        checkoutParameter.setCart(cartModel);
        targetLaybyCommerceCheckoutService.validateDeliveryMode(checkoutParameter);
    }

    @Override
    public boolean setPaymentDetails(final String paymentInfoId) {
        validateParameterNotNullStandardMessage("paymentInfoId", paymentInfoId);
        final CartModel cartModel = getCart();
        if (null != cartModel) {
            if (StringUtils.isNotBlank(paymentInfoId)) {
                final CreditCardPaymentInfoModel ccPaymentInfoModel = getCustomerAccountService()
                        .getCreditCardPaymentInfoForCode(
                                (CustomerModel)cartModel.getUser(), paymentInfoId);

                if (null != ccPaymentInfoModel) {
                    final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
                    checkoutParameter.setCart(cartModel);
                    checkoutParameter.setPaymentInfo(ccPaymentInfoModel);
                    return getCommerceCheckoutService().setPaymentInfo(checkoutParameter);
                }
            }
        }
        return false;
    }

    @Override
    public boolean setFlybuysNumber(final String flybuysNumber) {
        final CartModel cartModel = getCart();
        if (cartModel == null) {
            return false;
        }

        if (flybuysNumber == null || !flybuysNumber.equals(cartModel.getFlyBuysCode())) {
            sessionService.removeAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE);
            return getCommerceCheckoutService().setFlybuysNumber(cartModel, flybuysNumber);
        }
        return true;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#isFlybuysDiscountAlreadyApplied(java.lang.String)
     */
    @Override
    public boolean isFlybuysDiscountAlreadyApplied(final String excludeFlybuysNumber) {
        final CartModel cartModel = getCart();
        if (cartModel == null) {
            return false;
        }
        if (null == targetVoucherService.getFlybuysDiscountForOrder(cartModel)) {
            return false;
        }
        if (StringUtils.isEmpty(cartModel.getFlyBuysCode())
                || StringUtils.equals(excludeFlybuysNumber, cartModel.getFlyBuysCode())) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#applyUsersFlybuysNumberToCart()
     */
    @Override
    public void applyUsersFlybuysNumberToCart() {
        final CartModel cartModel = getCart();
        if (cartModel != null && cartModel.getFlyBuysCode() == null) {
            final TargetCustomerModel user = (TargetCustomerModel)cartModel.getUser();
            if (user != null) {
                final String userFlybuysCode = user.getFlyBuysCode();
                if (userFlybuysCode != null) {
                    setFlybuysNumber(userFlybuysCode);
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.commercefacades.order.impl.DefaultCheckoutFacade#getSupportedDeliveryAddresses(boolean)
     */
    @Override
    public List<AddressData> getSupportedDeliveryAddresses(final boolean visibleAddressesOnly) {
        final List<AddressData> supportedDeliveryAddresses = super.getSupportedDeliveryAddresses(visibleAddressesOnly);

        if (CollectionUtils.isEmpty(supportedDeliveryAddresses)) {
            return supportedDeliveryAddresses;
        }

        final AddressData defaultAddress = getTargetUserFacade().getDefaultAddress();

        final List<AddressData> sortedDeliveryAddresses = new ArrayList<>();
        for (final AddressData deliveryAddress : supportedDeliveryAddresses) {
            if (defaultAddress != null && deliveryAddress.getId().equals(defaultAddress.getId())) {
                deliveryAddress.setDefaultAddress(true);
                sortedDeliveryAddresses.add(0, deliveryAddress);
            }
            else {
                sortedDeliveryAddresses.add(deliveryAddress);
            }
        }

        return sortedDeliveryAddresses;
    }

    /**
     * fetch all visible saved addresses with availability for given delivery mode. If no given delivery mode, will
     * check for all applicable home delivery mode
     */
    @Override
    public List<AddressData> getSupportedDeliveryAddresses(final String deliveryModeCode,
            final boolean visibleAddressesOnly) {
        final List<AddressData> supportedDeliveryAddresses = getSupportedDeliveryAddresses(visibleAddressesOnly);
        if (CollectionUtils.isEmpty(supportedDeliveryAddresses)) {
            return supportedDeliveryAddresses;
        }
        if (deliveryModeCode == null) {
            final List<TargetZoneDeliveryModeData> applicableDeliveryModes = getDeliveryModes();
            if (CollectionUtils.isNotEmpty(applicableDeliveryModes)) {
                for (final TargetZoneDeliveryModeData deliveryModeData : applicableDeliveryModes) {
                    if (!deliveryModeData.isDeliveryToStore() && !deliveryModeData.isDigitalDelivery()) {
                        checkAddressStatusForDeliveryMode(deliveryModeData.getCode(), supportedDeliveryAddresses);
                    }
                }
            }
        }
        else {
            checkAddressStatusForDeliveryMode(deliveryModeCode, supportedDeliveryAddresses);
        }
        return supportedDeliveryAddresses;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#setPaymentMode(java.lang.String)
     */
    @Override
    public boolean setPaymentMode(final String paymentModeCode) {
        final PaymentModeModel paymentMode = paymentModeService
                .getPaymentModeForCode(paymentModeCode);
        return targetCommerceCheckoutService.setPaymentMode(getCart(), paymentMode);
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#removePaymentInfo()
     */
    @Override
    public void removePaymentInfo() {
        targetCommerceCheckoutService.removePaymentInfo(getCart());
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.commercefacades.order.impl.DefaultCheckoutFacade#getCustomerAccountService()
     */
    @Override
    protected TargetCustomerAccountService getCustomerAccountService() {
        return targetCustomerAccountService;
    }

    /*
     * (non-Javadoc)
     * @see de.hybris.platform.commercefacades.order.impl.DefaultCheckoutFacade#getCommerceCheckoutService()
     */
    @Override
    protected TargetCommerceCheckoutService getCommerceCheckoutService() {
        return targetCommerceCheckoutService;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#getOrderUserUid(java.lang.String)
     */
    @Override
    public String getOrderUserUid(final String orderCode) {
        final OrderModel order = targetOrderService.findOrderModelForOrderId(orderCode);
        if (null != order) {
            return order.getUser().getUid();
        }
        return null;
    }



    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#redeemVoucherForCart(java.lang.String)
     */
    @Override
    public boolean applyVoucher(final String voucher) {
        boolean validVoucher = false;
        if (voucher != null) {
            try {
                final CartModel cartModel = getCart();
                final VoucherModel voucherModel = targetVoucherService.getVoucher(voucher);
                if (voucherModel != null && voucherModelService.isApplicable(voucherModel, cartModel)) {
                    validVoucher = targetVoucherService.redeemVoucher(voucher, cartModel);
                    final CommerceCartParameter cartParameter = new CommerceCartParameter();
                    cartModel.setCalculated(Boolean.FALSE);
                    cartParameter.setCart(cartModel);
                    // Recalculate with promotions
                    commerceCartService.recalculateCart(cartParameter);
                    getModelService().refresh(cartModel);
                }
            }
            catch (final JaloPriceFactoryException e) {
                LOG.error("Failed to redeem voucher on cartModel", e);
            }
            catch (final CalculationException e) {
                LOG.error("Failed to recalculate cartModel including voucher", e);
            }
        }
        return validVoucher;
    }



    /*
     * Returns the first voucher on the cart if it is not a FlybuysDiscountModel.
     */
    @Override
    public String getFirstAppliedVoucher() {
        final CartModel cartModel = getCart();
        final Collection<DiscountModel> voucherModels = targetVoucherService.getAppliedVouchers(cartModel);
        if (CollectionUtils.isNotEmpty(voucherModels)) {
            final DiscountModel discountModel = voucherModels.iterator().next();
            if (!(discountModel instanceof FlybuysDiscountModel)) {
                final Collection<String> vouchers = targetVoucherService.getAppliedVoucherCodes(cartModel);
                if (CollectionUtils.isNotEmpty(vouchers)) {
                    return vouchers.iterator().next();
                }
            }
        }
        return null;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#removeVoucher()
     */
    @Override
    public void removeVoucher(final String voucher) {
        if (voucher != null) {
            final CartModel cartModel = getCart();
            try {
                targetVoucherService.releaseVoucher(voucher, cartModel);
                final CommerceCartParameter cartParameter = new CommerceCartParameter();
                cartModel.setCalculated(Boolean.FALSE);
                cartParameter.setCart(cartModel);
                // Recalculate with promotions
                commerceCartService.recalculateCart(cartParameter);
                getModelService().refresh(cartModel);
            }
            catch (final JaloPriceFactoryException e) {
                LOG.error("Failed to release voucher from cartModel", e);
            }
            catch (final CalculationException e) {
                LOG.error("Failed to recalculate cartModel removing voucher", e);
            }
        }
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#getAppliedVoucherPrice(java.lang.String)
     */
    @Override
    public PriceData getAppliedVoucherPrice(final String voucher) {
        final VoucherModel voucherModel = targetVoucherService.getVoucher(voucher);
        if (voucherModel != null) {
            final CartModel cart = getCart();
            final VoucherValue voucherValue = voucherModelService.getAppliedValue(voucherModel, cart);
            if (voucherValue != null) {
                return getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(voucherValue.getValue()),
                        cart.getCurrency().getIsocode());
            }
        }
        return null;
    }

    @Override
    public PriceData getFirstPrice(final TargetZoneDeliveryModeModel targetDeliveryModeModel,
            final ProductModel productModel) {

        final List<AbstractTargetZoneDeliveryModeValueModel> valueModelList = targetDeliveryService
                .getAllApplicableDeliveryValuesForModeAndProduct(targetDeliveryModeModel,
                        productModel);

        if (CollectionUtils.isEmpty(valueModelList)) {
            return null;
        }
        else {
            final AbstractTargetZoneDeliveryModeValueModel valueModel = valueModelList.get(0);
            final String currencyIso = valueModel.getCurrency().getIsocode();

            if (valueModel instanceof TargetZoneDeliveryModeValueModel) {
                return priceDataFactory.create(PriceDataType.BUY,
                        BigDecimal.valueOf(((TargetZoneDeliveryModeValueModel)valueModel).getBulky().doubleValue()),
                        currencyIso);
            }
            else {
                return priceDataFactory.create(PriceDataType.BUY,
                        BigDecimal.valueOf(valueModel.getValue().doubleValue()), currencyIso);
            }
        }
    }

    @Override
    public PriceData getLastThreshold(final TargetZoneDeliveryModeModel targetDeliveryModeModel,
            final ProductModel productModel) {
        final List<AbstractTargetZoneDeliveryModeValueModel> valueModelList = targetDeliveryService
                .getAllApplicableDeliveryValuesForModeAndProduct(targetDeliveryModeModel,
                        productModel);
        if (CollectionUtils.isEmpty(valueModelList)) {
            return null;
        }
        else {
            // note the valueModelList has values in reverse order so the last is the first or the one having value as 0
            AbstractTargetZoneDeliveryModeValueModel valueModel = null;
            for (final AbstractTargetZoneDeliveryModeValueModel modeValueModel : valueModelList) {
                if (modeValueModel.getValue().equals(Double.valueOf(0))) {
                    valueModel = modeValueModel;
                    break;
                }
            }
            if (null != valueModel) {
                final String currencyIso = valueModel.getCurrency().getIsocode();
                return priceDataFactory.create(PriceDataType.BUY,
                        BigDecimal.valueOf(valueModel.getMinimum().doubleValue()), currencyIso);
            }
        }
        return null;
    }

    @Override
    public boolean hasDeliveryOptionConflicts(final CartModel cartModel,
            final DeliveryModeModel deliveryModeModel) {
        Assert.notNull(cartModel, "Cart can't be null!");
        Assert.notNull(deliveryModeModel, "Delivery mode can't be null!");
        final Collection<AbstractOrderEntryModel> entries = cartModel.getEntries();
        Assert.notEmpty(entries, "Entries Can't be Empty!");

        for (final AbstractOrderEntryModel entry : entries) {
            ProductModel product = entry.getProduct();
            while (product instanceof AbstractTargetVariantProductModel) {
                product = ((AbstractTargetVariantProductModel)product).getBaseProduct();
            }

            if (product != null
                    && !product.getDeliveryModes().contains(deliveryModeModel)
                    && !targetDeliveryModeHelper.hasOnlyDigitalDeliveryMode(entry.getProduct())) {
                return true;
            }
        }

        return false;
    }

    /**
     * check delivery availability for given delivery mode code and list of address.
     * 
     * @param deliveryModeCode
     * @param supportedDeliveryAddresses
     */
    private void checkAddressStatusForDeliveryMode(final String deliveryModeCode,
            final List<AddressData> supportedDeliveryAddresses) {
        if (StringUtils.isNotEmpty(deliveryModeCode)) {
            final DeliveryModeModel deliveryMode = getDeliveryService().getDeliveryModeForCode(deliveryModeCode);
            if (deliveryMode != null) {
                for (final AddressData address : supportedDeliveryAddresses) {
                    checkAddressStatusForDeliveryMode(deliveryMode, address);
                }
            }
        }
    }

    /**
     * check delivery availability for given delivery mode and address
     * 
     * @param deliveryMode
     * @param addressData
     */
    private void checkAddressStatusForDeliveryMode(final DeliveryModeModel deliveryMode,
            final AddressData addressData) {
        Assert.notNull(deliveryMode, "delivery mode cannot be null to check address valibility");
        Assert.notNull(addressData, "address cannot be null to check address valibility");
        if (addressData instanceof TargetAddressData) {
            final TargetAddressData targetAddressData = (TargetAddressData)addressData;
            try {
                final boolean isValidForDeliveryMode = targetDeliveryService
                        .isDeliveryModePostCodeCombinationValid(
                                deliveryMode, targetAddressData.getPostalCode(), getCart());
                targetAddressData.setValidForDeliveryMode(Boolean.valueOf(isValidForDeliveryMode));
            }
            catch (final TargetNoPostCodeException e) {
                targetAddressData.setValidForDeliveryMode(Boolean.TRUE);
                LOG.debug(e);
            }
            if (targetAddressData.getValidForDeliveryMode().booleanValue()) {
                if (targetAddressData.getValidDeliveryModeIds() == null) {
                    targetAddressData.setValidDeliveryModeIds(new ArrayList<String>());
                }
                targetAddressData.getValidDeliveryModeIds().add(deliveryMode.getCode());
            }
        }
    }

    @Override
    public boolean setKioskStoreNumber(final String kioskStoreNumber) {
        final CartModel cartModel = getCart();
        if (cartModel == null) {
            return false;
        }

        return getCommerceCheckoutService().setKioskStoreNumber(cartModel, kioskStoreNumber);
    }

    @Override
    public boolean setStoreAssisted(final String storeEmployeeUid) {
        final CartModel cartModel = getCart();
        if (cartModel == null) {
            return false;
        }

        final StoreEmployeeModel storeEmployeeModel = getUserService().getUserForUID(storeEmployeeUid,
                StoreEmployeeModel.class);

        return getCommerceCheckoutService().setStoreAssisted(cartModel, storeEmployeeModel);
    }

    /**
     * @return the targetPaymentService
     */
    protected TargetPaymentService getTargetPaymentService() {
        return targetPaymentService;
    }

    /**
     * @return the abstractOrderHelper
     */
    protected AbstractOrderHelper getAbstractOrderHelper() {
        return abstractOrderHelper;
    }

    /**
     * @return the targetUserFacade
     */
    protected TargetUserFacade getTargetUserFacade() {
        return targetUserFacade;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.commercefacades.order.impl.DefaultCheckoutFacade#getPaymentDetails()
     */
    @Override
    protected CCPaymentInfoData getPaymentDetails() {
        final CartModel cart = getCart();
        CCPaymentInfoData paymentInfoData = null;
        if (cart != null) {
            final PaymentInfoModel paymentInfo = cart.getPaymentInfo();
            if (paymentInfo instanceof CreditCardPaymentInfoModel) {
                paymentInfoData = getCreditCardPaymentInfoConverter().convert((CreditCardPaymentInfoModel)paymentInfo);
            }
            else if (paymentInfo instanceof PinPadPaymentInfoModel) {
                paymentInfoData = getTargetPinPadPaymentInfoConverter().convert((PinPadPaymentInfoModel)paymentInfo);
            }
            else if (paymentInfo instanceof IpgPaymentInfoModel) {
                paymentInfoData = getTargetIpgPaymentInfoConverter().convert((IpgPaymentInfoModel)paymentInfo);
            }
        }
        return paymentInfoData;
    }

    /**
     * @return targetIpgPaymentInfoConverter
     */
    protected Converter<IpgPaymentInfoModel, CCPaymentInfoData> getTargetIpgPaymentInfoConverter() {
        return targetIpgPaymentInfoConverter;
    }

    /**
     * @return the targetPinPadPaymentInfoConverter
     */
    protected Converter<PinPadPaymentInfoModel, CCPaymentInfoData> getTargetPinPadPaymentInfoConverter() {
        return targetPinPadPaymentInfoConverter;
    }


    @Override
    public String getThreatMatrixSessionID() {
        final CartModel cartModel = getCart();
        if (cartModel != null) {
            if (StringUtils.isEmpty(cartModel.getThreatMatrixSessionID())) {
                setThreatMatrixSessionID(cartModel);
            }
            return cartModel.getThreatMatrixSessionID();
        }
        return null;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#setThreatMatrixSessionID(de.hybris.platform.core.model.order.CartModel)
     */
    @Override
    public void setThreatMatrixSessionID(final CartModel cartModel) {
        if (cartModel != null) {
            cartModel.setThreatMatrixSessionID(generateThreatMatrixSessionID(cartModel.getCode()));
            modelService.save(cartModel);
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#recalculateCart()
     */
    @Override
    public void recalculateCart() {
        try {
            final CommerceCartParameter cartParameter = new CommerceCartParameter();
            cartParameter.setCart(getCart());
            commerceCartService.recalculateCart(cartParameter);
            getModelService().refresh(getCart());
        }
        catch (final CalculationException e) {
            LOG.error("Failed to recalculate cartModel", e);
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#removeSelectedCncStore()
     */
    @Override
    public void removeSelectedCncStore() {
        final CartModel cartModel = getCart();
        if (cartModel != null && cartModel.getCncStoreNumber() != null) {
            cartModel.setDeliveryAddress(null);
            cartModel.setCncStoreNumber(null);
            getModelService().save(cartModel);
            getModelService().refresh(cartModel);
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#updateDeliveryMode()
     */
    @Override
    public void updateDeliveryMode(final String selectedDeliveryMode) {
        if (StringUtils.isNotBlank(selectedDeliveryMode)) {
            setDeliveryMode(selectedDeliveryMode);
            final DeliveryModeModel deliveryModeModel = targetDeliveryModeHelper
                    .getDeliveryModeModel(selectedDeliveryMode);
            if (deliveryModeModel != null) {
                setDeliveryAddress(null);
            }
        }
    }

    /**
     * removes the address from cart and sets null
     */
    protected boolean removeDeliveryAddressFromCart() {
        final CartModel cartModel = getCart();
        if (cartModel != null) {
            cartModel.setDeliveryAddress(null);
            getModelService().save(cartModel);
            getModelService().refresh(cartModel);
            return true;
        }
        return false;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#removeDeliveryInfo()
     */
    @Override
    public void removeDeliveryInfo() {
        final CartModel cartModel = getCart();
        if (cartModel != null) {
            sessionService.removeAttribute(TgtCoreConstants.SESSION_POSTCODE);
            cartModel.setDeliveryMode(null);
            cartModel.setDeliveryAddress(null);
            cartModel.setCncStoreNumber(null);
            cartModel.setZoneDeliveryModeValue(null);
            getModelService().save(cartModel);
            getModelService().refresh(cartModel);
            try {
                calculationService.calculateTotals(cartModel, true);
            }
            catch (final CalculationException calculationException) {
                throw new IllegalStateException("Cart model " + cartModel.getCode()
                        + " totals were not calculated due to: " + calculationException.getMessage());
            }
        }
    }


    @Override
    public TargetCreateSubscriptionResult getIpgSessionToken(final String returnUrl) {
        final PaymentModeModel paymentMode = paymentModeService.getPaymentModeForCode(TgtFacadesConstants.IPG);
        final CartModel cart = getCart();
        if (cart == null) {
            return null;
        }

        String uniqueKey = cart.getUniqueKeyForPayment();
        if (StringUtils.isEmpty(uniqueKey)) {
            uniqueKey = targetOrderService.createUniqueKeyForPaymentInitiation(cart);
        }

        final HostedSessionTokenRequest hostedSessionTokenRequest = getHostedSessionTokenRequest(cart,
                paymentMode, returnUrl, returnUrl, PaymentCaptureType.PLACEORDER,
                uniqueKey);
        return getTargetPaymentService().getHostedSessionToken(hostedSessionTokenRequest);
    }

    @Override
    public boolean isDeliveryFeeVariesAfterAddressChange(final BigDecimal deliveryFee) {
        final CartModel cart = getCart();
        if (null != cart.getDeliveryCost()
                && deliveryFee.compareTo(BigDecimal.valueOf(cart.getDeliveryCost().doubleValue())) != 0) {
            return true;
        }
        return false;
    }

    @Override
    public TargetZoneDeliveryModeData getDeliveryModeWithRangeInfoForCart(
            final TargetZoneDeliveryModeData deliveryData) {
        Assert.notNull(deliveryData, "Delivery mode data cannot be null");
        final CartModel cart = getCart();
        if (null != cart) {
            if (StringUtils.isEmpty(postcodeService.getPostalCodeFromCartOrSession(cart))
                    && null == deliveryData.getDeliveryCost()) {
                if (StringUtils.isNotEmpty(deliveryData.getCode())) {
                    final ZoneDeliveryModeModel zoneDeliveryMode = (ZoneDeliveryModeModel)deliveryService
                            .getDeliveryModeForCode(deliveryData.getCode());
                    final List<AbstractTargetZoneDeliveryModeValueModel> deliveryValueList = deliveryService
                            .getAllApplicableDeliveryValuesForModeAndOrder(zoneDeliveryMode, cart, true);
                    if (CollectionUtils.isNotEmpty(deliveryValueList)) {
                        final AbstractTargetZoneDeliveryModeValueModel minimumFee = deliveryValueList.get(0);
                        if (minimumFee.getLowestPossibleValue() != null) {
                            deliveryData.setFeeRange(Boolean.TRUE);
                            final double fee = minimumFee.getLowestPossibleValue().doubleValue();
                            deliveryData.setDeliveryCost(priceDataFactory.create(
                                    PriceDataType.BUY, BigDecimal.valueOf(fee), minimumFee.getCurrency()));
                        }
                    }
                }
            }
            else {
                deliveryData.setFeeRange(Boolean.FALSE);
            }
        }
        return deliveryData;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#getIpgBaseUrl()
     */
    @Override
    public String getIpgBaseUrl() {
        return configurationService.getConfiguration().getString(IPG_BASE_URL);
    }

    @Override
    public String createIpgUrl(final String sessionId, final String token) {
        final StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(getIpgBaseUrl());
        urlBuilder.append("?sessionId=");
        urlBuilder.append(sessionId);
        urlBuilder.append("&SST=");
        urlBuilder.append(token);
        return urlBuilder.toString();
    }

    @Override
    public boolean setDeliveryAddress(final AddressData addressData) {
        if (null != addressData) {
            return super.setDeliveryAddress(addressData);
        }
        return removeDeliveryAddressFromCart();
    }

    @Override
    public boolean isGiftCardInCart() {
        return giftCardService.doesCartHaveAGiftCard(getSessionCart());
    }

    @Override
    public boolean doesCartHaveGiftCardOnly() {
        return giftCardService.doesCartHaveGiftCardsOnly(getCart());
    }

    @Override
    public boolean doesCartHavePhysicalGiftCardOnly() {
        return giftCardService.doesCartHavePhysicalGiftCardOnly(getCart());
    }

    @Override
    public boolean doesCartHavePhysicalAndDigitalGiftCardsOnly() {
        return giftCardService.doesCartHavePhysicalAndDigitalGiftCardsOnly(getCart());
    }

    @Override
    public boolean isGiftCardPaymentAllowedForCart() {
        return !isGiftCardInCart();
    }

    @Override
    public boolean isPaymentModeGiftCard() {
        final CartModel cart = getCart();
        final PaymentModeModel paymentMode = cart.getPaymentMode();
        if (paymentMode != null && paymentMode.getCode().equalsIgnoreCase(TgtFacadesConstants.GIFT_CARD)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isPaymentModePaypal() {
        return isRequestedPaymentMode(TgtFacadesConstants.PAYPAL);
    }

    @Override
    public boolean isPaymentModeAfterpay() {
        return isRequestedPaymentMode(TgtFacadesConstants.AFTERPAY);
    }

    private boolean isRequestedPaymentMode(final String paymentModeCode) {
        final CartModel cart = getCart();
        if (null != cart) {
            final PaymentModeModel paymentMode = cart.getPaymentMode();
            return paymentMode != null && paymentMode.getCode().equalsIgnoreCase(paymentModeCode);
        }
        return false;
    }

    @Override
    public List<TargetZoneDeliveryModeData> getDeliveryModes() {
        final List<TargetZoneDeliveryModeData> deliveryModes = abstractOrderHelper.getPhysicalDeliveryModes(getCart());
        if (targetFeatureSwitchFacade.isFluentEnabled() && CollectionUtils.isNotEmpty(deliveryModes)) {
            overlayFluentStockDataOnDeliveryModes(deliveryModes);
        }
        if (targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)) {
            checkFastlineStockOnExpressDeliveryModes(deliveryModes);
        }
        return deliveryModes;
    }

    @Override
    public AdjustedCartEntriesData adjustCart() {
        final CartModel cartModel = getCart();
        if (cartModel != null) {
            final PurchaseOptionModel purchaseOption = cartModel.getPurchaseOption();
            final AdjustedCartEntriesData adjustedCartEntriesData = new AdjustedCartEntriesData(
                    purchaseOption.getCode());
            try {
                final List<CommerceCartModification> cartModifications = targetLaybyCommerceCheckoutService
                        .performSOHOnCart(cartModel);
                if (CollectionUtils.isNotEmpty(cartModifications)) {
                    for (final CommerceCartModification entry : cartModifications) {
                        if (!TargetCommerceCartModificationStatus.NO_MODIFICATION_OCCURRED.equals(entry
                                .getStatusCode())) {
                            final ProductModel productModel = entry.getProduct();
                            final TargetProductData productData = (TargetProductData)productFacade
                                    .getProductForOptions(
                                            productModel,
                                            Arrays.asList(ProductOption.BASIC));

                            switch ((int)entry.getQuantity()) {
                                case 0:
                                    adjustedCartEntriesData.getOutOfStockItems().add(
                                            new AdjustedEntryQuantityData((int)entry.getQuantity(), productData));
                                    break;
                                default:
                                    adjustedCartEntriesData.getAdjustedItems().add(
                                            new AdjustedEntryQuantityData((int)entry.getQuantity(), productData));

                                    break;
                            }
                        }
                    }
                }
            }
            catch (final CommerceCartModificationException e) {
                LOG.error("Unexpected error while performing SOH checks during checkout: ", e);
            }
            return adjustedCartEntriesData;
        }
        return null;
    }

    /**
     * Validates the completeness of delivery mode and delivery address details of the cart.
     * 
     * @return true if delivery data is valid
     */
    @Override
    public boolean hasIncompleteDeliveryDetails() {
        final CartModel cartModel = getCart();
        boolean hasIncompleteDeliveryDetails = false;
        if (cartModel == null || cartModel.getDeliveryMode() == null) {
            hasIncompleteDeliveryDetails = true;
        }
        else {
            final DeliveryModeModel deliveryMode = cartModel.getDeliveryMode();
            if (deliveryMode instanceof TargetZoneDeliveryModeModel) {
                final TargetZoneDeliveryModeModel targetDeliveryMode = (TargetZoneDeliveryModeModel)deliveryMode;
                if (hasDeliveryOptionConflicts(cartModel, deliveryMode)) {
                    hasIncompleteDeliveryDetails = true;
                }
                else if (BooleanUtils.isFalse(targetDeliveryMode.getIsDigital())) {
                    if (cartModel.getDeliveryAddress() == null) {
                        hasIncompleteDeliveryDetails = true;
                    }
                    if (BooleanUtils.isTrue(targetDeliveryMode.getIsDeliveryToStore())) {
                        if (cartModel.getCncStoreNumber() == null) {
                            hasIncompleteDeliveryDetails = true;
                        }
                    }
                    else if (cartModel.getCncStoreNumber() != null) {
                        hasIncompleteDeliveryDetails = true;
                    }
                }
            }
        }
        return hasIncompleteDeliveryDetails;
    }

    @Override
    public boolean hasIncompleteBillingAddress() {
        final CartModel cartModel = getCart();
        boolean hasIncompleteDeliveryDetails = false;
        if (cartModel.getPaymentAddress() == null) {
            final DeliveryModeModel deliveryMode = cartModel.getDeliveryMode();
            if (deliveryMode instanceof TargetZoneDeliveryModeModel) {
                final TargetZoneDeliveryModeModel targetDeliveryMode = (TargetZoneDeliveryModeModel)deliveryMode;
                if (BooleanUtils.isTrue(targetDeliveryMode.getIsDigital())
                        || BooleanUtils.isTrue(targetDeliveryMode.getIsDeliveryToStore())) {
                    hasIncompleteDeliveryDetails = true;
                }
            }
        }
        return hasIncompleteDeliveryDetails;
    }

    @Override
    public void createBillingAddress(final TargetAddressData addressData) {
        final TargetAddressModel addressModel = getModelService().create(TargetAddressModel.class);
        getAddressReversePopulator().populate(addressData, addressModel);
        final CartModel cartModel = getCart();

        addressModel.setOwner(cartModel);
        modelService.save(addressModel);

        cartModel.setPaymentAddress(addressModel);
        modelService.save(cartModel);

        addressData.setId(addressModel.getPk().toString());
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#isGuestCheckoutAllowed()
     */
    @Override
    public boolean isGuestCheckoutAllowed() {
        return targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.CHECKOUT_ALLOW_GUEST);
    }

    @Override
    public void overlayFluentStockDataOnDeliveryModes(final List<TargetZoneDeliveryModeData> deliveryModes) {
        final Map<String, Integer> skuQtyMap = new HashMap<>();
        for (final AbstractOrderEntryModel orderEntry : getCart().getEntries()) {
            skuQtyMap.put(orderEntry.getProduct().getCode(), Integer.valueOf(orderEntry.getQuantity().intValue()));
        }

        try {
            final List<StockDatum> stockData = fluentStockLookupService.lookupStock(skuQtyMap.keySet(),
                    Arrays.asList(TgtCoreConstants.DELIVERY_TYPE.CC, TgtCoreConstants.DELIVERY_TYPE.HD,
                            TgtCoreConstants.DELIVERY_TYPE.ED, TgtCoreConstants.DELIVERY_TYPE.PO),
                    null);
            final Map<String, TargetZoneDeliveryModeData> deliveryModeMap = new HashMap<>();
            for (final TargetZoneDeliveryModeData deliveryModeData : deliveryModes) {
                deliveryModeMap.put(deliveryModeData.getCode(), deliveryModeData);
            }

            for (final StockDatum stockDatum : stockData) {
                checkFluentStock(skuQtyMap.get(stockDatum.getVariantCode()),
                        stockDatum.getAtsCcQty() == null ? Integer.valueOf(0) : stockDatum.getAtsCcQty(),
                        CLICK_AND_COLLECT, deliveryModeMap);
                checkFluentStock(skuQtyMap.get(stockDatum.getVariantCode()),
                        stockDatum.getAtsHdQty() == null ? Integer.valueOf(0) : stockDatum.getAtsHdQty(), HOME_DELIVERY,
                        deliveryModeMap);
                final Integer atsQty = atsQtyForExpressDelivery(stockDatum.getAtsPoQty(), stockDatum.getAtsEdQty());
                checkFluentStock(skuQtyMap.get(stockDatum.getVariantCode()), atsQty,
                        EXPRESS_DELIVERY,
                        deliveryModeMap);
            }
        }
        catch (final FluentClientException e) {
            throw new FluentOrderException(e.getLocalizedMessage(), e);
        }
    }


    @Override
    public void checkFastlineStockOnExpressDeliveryModes(final List<TargetZoneDeliveryModeData> deliveryModes) {
        if (CollectionUtils.isNotEmpty(deliveryModes)) {
            for (final TargetZoneDeliveryModeData deliveryModeData : deliveryModes) {
                checkFastlineStockOnExpressDeliveryMode(deliveryModeData);
            }
        }
    }

    /**
     * This method will validate atsPoQts and atsEdQty values for express-delivery mode. For instance if the fluent
     * feature switch is ON and a pre-order item is present in the basket, when customer landed in SPC page then this
     * method will verify if atsPoQty is present or not; if present then it will return the value accordingly or it will
     * return atsEdQty.
     * 
     * @param atsPoQty
     * @param atsEdQty
     * @return Integer
     */
    private Integer atsQtyForExpressDelivery(final Integer atsPoQty, final Integer atsEdQty) {
        if (atsPoQty != null && atsPoQty.intValue() > 0) {
            return atsPoQty;
        }
        if (atsEdQty == null) {
            return Integer.valueOf(0);
        }
        return atsEdQty;
    }


    private void checkFastlineStockOnExpressDeliveryMode(final TargetZoneDeliveryModeData deliveryModeData) {
        if (deliveryModeData != null && EXPRESS_DELIVERY.equals(deliveryModeData.getCode())) {
            final Map<ProductModel, Integer> skuQtyMap = new HashMap<>();
            final Map<String, Integer> skuCodeQtyMap = new HashMap<>();
            for (final AbstractOrderEntryModel orderEntry : getCart().getEntries()) {
                if (!ProductUtil.isProductTypeDigital(orderEntry.getProduct())) {
                    skuQtyMap.put(orderEntry.getProduct(), Integer.valueOf(orderEntry.getQuantity().intValue()));
                    skuCodeQtyMap.put(orderEntry.getProduct().getCode(),
                            Integer.valueOf(orderEntry.getQuantity().intValue()));
                }

            }
            final List<StockDatum> stockDatumList = getFastlineStockDatum(skuQtyMap.keySet());
            for (final StockDatum stockDatum : stockDatumList) {
                if (stockDatum.getAtsEdQty().compareTo(skuCodeQtyMap.get(stockDatum.getVariantCode())) < 0) {
                    deliveryModeData.setAvailable(false);
                    break;
                }
            }
        }
    }

    private List<StockDatum> getFastlineStockDatum(final Collection<ProductModel> skuRefs) {
        final List<StockDatum> stocks = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(skuRefs)) {
            final WarehouseModel warehouse = targetWarehouseService.getDefaultOnlineWarehouse();
            for (final ProductModel sku : skuRefs) {
                final int amount = targetStockService.getStockLevelAmount(sku, warehouse);
                final StockDatum stock = new StockDatum();
                stock.setAtsEdQty(Integer.valueOf(amount));
                stock.setVariantCode(sku.getCode());
                stocks.add(stock);
            }
        }
        return stocks;
    }

    /**
     * @param requestedQuantity
     * @param ats
     * @param deliveryMode
     * @param deliveryModeMap
     */
    private void checkFluentStock(final Integer requestedQuantity, final Integer ats, final String deliveryMode,
            final Map<String, TargetZoneDeliveryModeData> deliveryModeMap) {
        final TargetZoneDeliveryModeData deliveryModeData = deliveryModeMap.get(deliveryMode);
        if (ats.compareTo(requestedQuantity) < 0 && deliveryModeData != null) {
            deliveryModeData.setAvailable(false);
        }
    }

    private String generateThreatMatrixSessionID(final String cartId) {
        if (StringUtils.isEmpty(cartId)) {
            return null;
        }
        else {
            return DigestUtils.md5Hex(THREATMATRIX_SESSIONID_PREFIX + cartId);
        }
    }

    @Override
    public boolean removeFlybuysNumber() {
        final CartModel cartModel = getCart();
        if (cartModel == null) {
            return false;
        }
        sessionService.removeAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE);
        return getCommerceCheckoutService().removeFlybuysNumber(cartModel);

    }

    @Override
    public boolean setBillingAddress(final String addressId) {
        final CartModel cartModel = getCart();
        if (cartModel != null && cartModel.getUser() != null) {
            final AddressModel addressModel = getCustomerAccountService().getAddressForCode(
                    (CustomerModel)cartModel.getUser(),
                    addressId);
            if (addressModel != null) {
                cartModel.setPaymentAddress(addressModel);
                getModelService().save(cartModel);
                return true;
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#isCartAllowedGuestCheckout()
     */
    @Override
    public boolean isCartHavingPreOrderItem() {
        final CartModel sessionCart = getSessionCart();
        if (sessionCart != null) {
            return BooleanUtils.toBoolean(sessionCart.getContainsPreOrderItems());
        }
        return false;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetCheckoutFacade#isGiftCardMixedBasket()
     */
    @Override
    public boolean isGiftCardMixedBasket() {
        final CartModel cartModel = getCart();
        Assert.notNull(cartModel, "Cart can't be null!");
        final List<AbstractOrderEntryModel> orderEntries = cartModel.getEntries();
        if (CollectionUtils.isNotEmpty(orderEntries) && orderEntries.size() > 1) {
            boolean containsNonGiftCard = false;
            boolean containsGiftCard = false;
            for (final AbstractOrderEntryModel orderEntry : orderEntries) {
                final String productType = ProductUtil.getProductTypeCode(orderEntry.getProduct()).toLowerCase();
                switch (productType) {
                    case ProductType.DIGITAL:
                    case TgtFacadesConstants.GIFT_CARD:
                        containsGiftCard = true;
                        break;
                    default:
                        containsNonGiftCard = true;
                        break;
                }
            }
            //If following condition met that means the cart is a mixed basket.
            if (containsGiftCard && containsNonGiftCard) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean paymentModeEquals(final String paymentMode) {
        return isRequestedPaymentMode(paymentMode);
    }

    @Override
    public void createZipPaymentInfo(final String checkoutId) {
        validateParameterNotNullStandardMessage("checkoutId", checkoutId);

        final CartModel cartModel = getCart();
        if (cartModel == null) {
            return;
        }
        final UserModel currentUser = cartModel.getUser();
        final ZippayPaymentInfoModel paymentInfo = getCustomerAccountService().createZipPaymentInfo(
                (CustomerModel)currentUser, checkoutId);

        final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
        checkoutParameter.setCart(cartModel);
        checkoutParameter.setPaymentInfo(paymentInfo);
        getCommerceCheckoutService().setPaymentInfo(checkoutParameter);
    }
}
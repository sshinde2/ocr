/**
 * 
 */
package au.com.target.tgtfacades.order.impl;

import de.hybris.platform.commercefacades.converter.ConfigurablePopulator;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.DeliveryModeService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtauspost.deliveryclub.service.ShipsterClientService;
import au.com.target.tgtauspost.exception.AuspostShipsterClientException;
import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;
import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.delivery.TargetDeliveryFacade;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade;
import au.com.target.tgtfacades.flybuys.data.FlybuysRequestStatusData;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.TargetCheckoutResponseFacade;
import au.com.target.tgtfacades.order.TargetOrderFacade;
import au.com.target.tgtfacades.order.TargetPlaceOrderFacade;
import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;
import au.com.target.tgtfacades.order.data.ClickAndCollectDeliveryDetailData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.payment.AfterpayConfigFacade;
import au.com.target.tgtfacades.payment.data.AfterpayConfigData;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.BillingCountryResponseData;
import au.com.target.tgtfacades.response.data.CartDetailOption;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;
import au.com.target.tgtfacades.response.data.CheckoutOptionsResponseData;
import au.com.target.tgtfacades.response.data.CreateAddressResponseData;
import au.com.target.tgtfacades.response.data.Error;
import au.com.target.tgtfacades.response.data.OrderDetailGaData;
import au.com.target.tgtfacades.response.data.OrderDetailResponseData;
import au.com.target.tgtfacades.response.data.OrderDetailsData;
import au.com.target.tgtfacades.response.data.PaymentMethodData;
import au.com.target.tgtfacades.response.data.PaymentResponseData;
import au.com.target.tgtfacades.response.data.RegisterUserResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.response.data.ShipsterEmailVerificationResponseData;
import au.com.target.tgtfacades.response.data.StoreSearchResultResponseData;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtfacades.user.data.TargetRegisterData;
import au.com.target.tgtfacades.voucher.TargetVoucherFacade;
import au.com.target.tgtfacades.voucher.data.TargetFlybuysLoginData;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.constants.TgtpaymentConstants;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.service.TargetPaymentService;
import au.com.target.tgtverifyaddr.TargetAddressVerificationService;
import au.com.target.tgtverifyaddr.data.CountryEnum;
import au.com.target.tgtverifyaddr.exception.NoMatchFoundException;
import au.com.target.tgtverifyaddr.exception.RequestTimeOutException;
import au.com.target.tgtverifyaddr.exception.ServiceNotAvailableException;
import au.com.target.tgtverifyaddr.exception.TooManyMatchesFoundException;


/**
 * Facade for single page checkout.
 * 
 * @author htan3
 *
 */
@Transactional(timeout = 300)
public class TargetCheckoutResponseFacadeImpl implements TargetCheckoutResponseFacade {

    private static final Logger LOG = Logger.getLogger(TargetCheckoutResponseFacadeImpl.class);

    private static final String CLICK_AND_COLLECT = "click-and-collect";

    private static final String IPG_PAYMENT_NOT_AVAILABLE = "Credit and gift card payments currently unavailable. We recommend PayPal.";

    private static final String FLYBUYS_REMOVE_DISCOUNT_CODE = "remove";

    private static final String PAYMENT_OPTION_NOT_AVAILABLE = "Items in your basket prevent this payment option.";

    private TargetCheckoutFacade targetCheckoutFacade;

    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    private DeliveryModeService deliveryModeService;

    private TargetAddressVerificationService targetAddressVerificationService;

    private ConfigurablePopulator<TargetCartData, CartDetailResponseData, CartDetailOption> cartDetailPopulator;

    private TargetVoucherFacade targetVoucherFacade;

    private TargetUserFacade targetUserFacade;

    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    private TargetPlaceOrderFacade targetPlaceOrderFacade;

    private Converter<AddressModel, AddressData> addressConverter;

    private TargetDeliveryFacade targetDeliveryFacade;

    private FlybuysDiscountFacade flybuysDiscountFacade;

    private TargetOrderFacade targetOrderFacade;

    private Populator<TargetOrderData, OrderDetailGaData> orderDetailGaPopulator;

    private TargetCustomerFacade targetCustomerFacade;

    private TargetCommerceCartService targetCommerceCartService;

    private AfterpayConfigFacade afterpayConfigFacade;

    private ShipsterClientService shipsterClientService;

    private CartService cartService;

    private ModelService modelService;

    private CalculationService calculationService;

    private TargetPaymentService targetPaymentService;

    @Override
    public Response getApplicableDeliveryModes() {
        final Response response;
        try {
            List<TargetZoneDeliveryModeData> deliveryModeList = targetCheckoutFacade.getDeliveryModes();
            if (CollectionUtils.isNotEmpty(deliveryModeList)) {
                for (final TargetZoneDeliveryModeData deliveryModeData : deliveryModeList) {
                    targetCheckoutFacade.getDeliveryModeWithRangeInfoForCart(deliveryModeData);
                }
            }
            if (deliveryModeList == null) {
                deliveryModeList = Collections.<TargetZoneDeliveryModeData> emptyList();
            }
            final CheckoutOptionsResponseData data = new CheckoutOptionsResponseData();
            data.setDeliveryModes(deliveryModeList);
            response = new Response(true);
            response.setData(data);
            return response;
        }
        catch (final FluentOrderException e) {
            return createErrorResponse("ERR_DELIVERY_MODES_FLUENT_EXCEPTION", "Error communicating with fluent");
        }
    }

    @Override
    public Response setDeliveryMode(final String deliveryMode) {
        if (StringUtils.isNotEmpty(deliveryMode)) {
            if (targetCheckoutFacade.isSupportedDeliveryMode(deliveryMode)) {
                targetCheckoutFacade.setDeliveryMode(deliveryMode);
                removeDeliveryAddressIfNotValidForDeliveryMode();
                return createResponseWithCartDetail(CartDetailOption.DELIVERY_MODE);
            }
        }
        return createErrorResponse("ERR_DELIVERY_MODE_UNAVAILABLE", "The delivery mode isn't available for the cart");
    }

    private void removeDeliveryAddressIfNotValidForDeliveryMode() {
        final CartModel cartModel = targetCheckoutFacade.getCart();
        if (cartModel != null) {
            final AddressModel addressModel = cartModel.getDeliveryAddress();
            //if there is no address to remove return
            if (addressModel == null) {
                return;
            }
            final DeliveryModeModel deliveryModeModel = cartModel.getDeliveryMode();
            if (null == deliveryModeModel || !(targetDeliveryFacade.isDeliveryModePostCodeCombinationValid(
                    deliveryModeModel.getCode(), addressModel.getPostalcode()))) {
                targetCheckoutFacade.removeDeliveryAddress();
            }
        }
    }

    @Override
    public Response searchCncStores(final String locationText, final PageableData pageableData) {
        final StoreFinderSearchPageData<PointOfServiceData> result = targetStoreLocatorFacade
                .searchStoresWithCncAvailability(
                        locationText, targetCheckoutFacade.getCart(), pageableData);
        final StoreSearchResultResponseData responseData = new StoreSearchResultResponseData();
        if (result != null) {
            responseData.setStores(result.getResults());
        }
        final Response response = new Response(true);
        response.setData(responseData);
        return response;
    }

    @Override
    public Response setCncStore(final Integer storeNumber) {
        Assert.notNull(storeNumber, "Store Number cannot be empty");
        final TargetPointOfServiceData posData = targetCheckoutFacade.setClickAndCollectStore(storeNumber.intValue());
        if (posData != null && Boolean.TRUE.equals(posData.getAvailableForPickup())) {
            final Response response = new Response(true);
            final StoreSearchResultResponseData responseData = new StoreSearchResultResponseData();
            responseData.setStores(ImmutableList.of((PointOfServiceData)posData));
            response.setData(responseData);
            return response;
        }
        return createErrorResponse("ERR_STORE_UNAVAILABLE", "The store isn't available for the cart");
    }

    @Override
    public Response getCartDetail() {
        return createResponseWithCartDetail(CartDetailOption.ALL);
    }

    @Override
    public Response getCartSummary() {
        return createResponseWithCartDetail(CartDetailOption.CART_SUMMARY);
    }

    @Override
    public Response getDeliveryAddresses() {
        final Response response = new Response(true);
        final CheckoutOptionsResponseData data = new CheckoutOptionsResponseData();
        final List<AddressData> deliveryAddresses = targetCheckoutFacade
                .getSupportedDeliveryAddresses(null, true);
        data.setDeliveryAddresses(deliveryAddresses);
        response.setData(data);
        return response;
    }

    @Override
    public Response setCncPickupDetails(final ClickAndCollectDeliveryDetailData cncDeliveryDetails) {
        Assert.notNull(cncDeliveryDetails, "ClickAndCollectDeliveryDetailData cannot be empty");
        final DeliveryModeModel deliveryModel = deliveryModeService.getDeliveryModeForCode(CLICK_AND_COLLECT);
        if (null != deliveryModel) {
            cncDeliveryDetails.setDeliveryModeModel(deliveryModel);
            if (targetCheckoutFacade.setClickAndCollectDeliveryDetails(cncDeliveryDetails)) {
                final TargetCartData cartData = (TargetCartData)targetCheckoutFacade.getCheckoutCart();
                if (cartData != null && cartData.getDeliveryAddress() != null) {
                    return createResponseWithCartDetail(CartDetailOption.CNC);
                }
            }
        }
        LOG.error("TargetCheckoutResponseFacadeImpl : Exception with store number ");
        return createErrorResponse("ERR_STORE_UNAVAILABLE", "The store isn't available for the cart");
    }

    @Override
    public Response applyTmd(final String tmdNumber) {
        final boolean success = targetCheckoutFacade.setTeamMemberDiscountCardNumber(tmdNumber);
        if (success) {
            flybuysDiscountFacade.reassessFlybuysDiscountOnCheckoutCart();
            return createResponseWithCartDetail(CartDetailOption.TMD);
        }
        return createErrorResponse("ERR_TMD_INVALID",
                "We don't recognise that team member card number. Please check and re-submit.");
    }

    @Override
    public Response selectDeliveryAddress(final String addressId) {
        if (StringUtils.isNotBlank(addressId)) {
            final AddressData selectedAddressData = targetCheckoutFacade.getDeliveryAddressForCode(addressId);
            final Double deliveryCostBeforeAddressChange = targetCheckoutFacade.getCart().getDeliveryCost();
            final DeliveryModeModel deliveryMode = targetCheckoutFacade.getCart().getDeliveryMode();
            if (null == selectedAddressData) {
                return this.createErrorResponse("ERR_VALIDATION", "Invalid post data");
            }
            //verify whether the  delivery mode is applicable for the address selected
            if (null == deliveryMode
                    || !(targetDeliveryFacade.isDeliveryModePostCodeCombinationValid(deliveryMode.getCode(),
                            selectedAddressData.getPostalCode()))) {
                return this.createErrorResponse("ERR_ADDRESS_UNAVAILABLE",
                        "Provided address is not valid for the selected delivery mode");
            }
            if (targetCheckoutFacade.setDeliveryAddress(selectedAddressData)) {
                final Response response = createResponseWithCartDetail(CartDetailOption.DELIVERY_ADDRESS);
                final CartDetailResponseData responseData = (CartDetailResponseData)response.getData();
                responseData.setDeliveryFeeChanged(Boolean.valueOf(targetCheckoutFacade
                        .isDeliveryFeeVariesAfterAddressChange(BigDecimal
                                .valueOf(deliveryCostBeforeAddressChange.doubleValue()))));
                return response;
            }
        }
        return this.createErrorResponse("ERR_VALIDATION", "Invalid post data");
    }

    @Override
    public Response selectBillingAddress(final String addressId) {
        if (StringUtils.isNotBlank(addressId)) {
            if (targetCheckoutFacade.setBillingAddress(addressId)) {
                return createResponseWithCartDetail(CartDetailOption.BILLING_ADDRESS);
            }
        }
        return this.createErrorResponse("ERR_VALIDATION", "Invalid post data");
    }

    @Override
    public Response createAddress(final TargetAddressData newAddressData) {
        Assert.notNull(newAddressData, "address cannot be null when create address");
        newAddressData.setVisibleInAddressBook(true);
        targetUserFacade.addAddress(newAddressData);
        if (newAddressData.getId() != null) {
            final Response response = new Response(true);
            final CreateAddressResponseData responseData = new CreateAddressResponseData();
            responseData.setCreatedAddress(targetCheckoutFacade
                    .getDeliveryAddressForCode(newAddressData.getId()));
            response.setData(responseData);
            return response;
        }
        return this.createErrorResponse("ERR_VALIDATION", "Invalid post data");
    }

    private Response createResponseWithCartDetail(final CartDetailOption option) {
        Response response = new Response(true);
        final TargetCartData cartData = (TargetCartData)targetCheckoutFacade.getCheckoutCart();
        if (cartData != null) {
            if (CollectionUtils.isNotEmpty(cartData.getEntries())) {
                final CartDetailResponseData cartDetailResponseData = new CartDetailResponseData();
                cartDetailPopulator.populate(cartData, cartDetailResponseData, ImmutableList.of(option));
                response.setData(cartDetailResponseData);
            }
            else {
                response = createErrorResponse("ERR_CART_NO_ENTRIES", "There are no products in the cart");
            }
        }

        return response;
    }

    @Override
    public Response applyVoucher(final String voucherCode) {

        if (targetCheckoutFacade.isCartHavingPreOrderItem()) {
            return createErrorResponse("ERR_VOUCHER_NOT_AVAILABLE_FOR_PREORDER",
                    "Promo Code not available for pre-orders.");
        }

        if (!targetVoucherFacade.doesExist(voucherCode) || targetVoucherFacade.isExpired(voucherCode)) {
            return createErrorResponse("ERR_VOUCHER_DOES_NOT_EXIST", "Promo Code unknown or expired");
        }

        final String appliedVoucher = targetCheckoutFacade.getFirstAppliedVoucher();

        if (StringUtils.isEmpty(appliedVoucher)) {
            if (targetCheckoutFacade.applyVoucher(voucherCode)) {
                return createResponseWithCartDetail(CartDetailOption.VOUCHER);
            }
        }
        else {
            final CartModel checkoutCart = targetCheckoutFacade.getCart();
            LOG.warn("WARNING : Trying to apply a voucher when there is already a voucher with code " + appliedVoucher
                    + " in cart " + (checkoutCart != null ? checkoutCart.getCode() : ""));
        }

        return createErrorResponse("ERR_VOUCHER_CONDITIONS_NOT_MET", "Promo Code conditions not met");
    }

    @Override
    public Response removeVoucher() {
        final String appliedVoucher = targetCheckoutFacade.getFirstAppliedVoucher();
        if (StringUtils.isNotEmpty(appliedVoucher)) {
            targetCheckoutFacade.removeVoucher(appliedVoucher);
            return createResponseWithCartDetail(CartDetailOption.VOUCHER);
        }

        return createErrorResponse("ERR_VOUCHER_REMOVE",
                "There was an error while removing the voucher.");
    }

    @Override
    public Response applyFlybuys(final String flybuysNumber) {
        if (!targetCheckoutFacade.isFlybuysDiscountAlreadyApplied(flybuysNumber)) {
            if (StringUtils.isNotBlank(flybuysNumber)
                    && targetCheckoutFacade.setFlybuysNumber(flybuysNumber)) {
                return createResponseWithCartDetail(CartDetailOption.FLYBUYS);
            }
        }

        return createErrorResponse("ERR_FLYBUYS_INVALID",
                "We don't recognize that flybuys number. Please check and re-submit.");
    }

    @Override
    public Response getApplicablePaymentMethods() {
        final List<PaymentMethodData> enabledPayments = new ArrayList<>();
        final Response response = new Response(true);
        final PaymentMethodData ipg = new PaymentMethodData(TgtFacadesConstants.IPG, false);
        final PaymentMethodData giftCard = new PaymentMethodData(TgtFacadesConstants.GIFT_CARD, false);
        final PaymentMethodData paypal = new PaymentMethodData(TgtFacadesConstants.PAYPAL, false);
        final PaymentMethodData afterpay = new PaymentMethodData(TgtFacadesConstants.AFTERPAY, false);
        final boolean isCartHavingPreOrderItem = targetCheckoutFacade.isCartHavingPreOrderItem();

        giftCard.setReason(PAYMENT_OPTION_NOT_AVAILABLE);

        if (targetFeatureSwitchFacade.isIpgEnabled()) {
            if (targetFeatureSwitchFacade.isGiftCardPaymentEnabled()) {
                if (!targetCheckoutFacade.isGiftCardInCart() && !isCartHavingPreOrderItem) {
                    giftCard.setAvailable(true);
                    giftCard.setReason(null);
                }
                enabledPayments.add(giftCard);
            }
            ipg.setAvailable(true);
            enabledPayments.add(ipg);
        }

        paypal.setReason(PAYMENT_OPTION_NOT_AVAILABLE);
        if (targetFeatureSwitchFacade.isPaypalEnabled()) {
            if (!isCartHavingPreOrderItem) {
                paypal.setAvailable(true);
                paypal.setReason(null);
                if (!targetFeatureSwitchFacade.isIpgEnabled()) {
                    paypal.setReason(IPG_PAYMENT_NOT_AVAILABLE);
                }
            }
            enabledPayments.add(paypal);
        }

        if (targetFeatureSwitchFacade.isZipEnabled()) {
            populateZipPaymentOption(enabledPayments);
        }

        if (targetFeatureSwitchFacade.isAfterpayEnabled()) {
            performAfterpayChecks(enabledPayments, afterpay);
        }

        final CheckoutOptionsResponseData responseData = new CheckoutOptionsResponseData();
        responseData.setPaymentMethods(enabledPayments);
        response.setData(responseData);
        return response;
    }

    /**
     * @param enabledPayments
     * @param afterpay
     */
    private void performAfterpayChecks(final List<PaymentMethodData> enabledPayments,
            final PaymentMethodData afterpay) {
        final CartModel cartModel = targetCheckoutFacade.getCart();
        if (targetCommerceCartService.isExcludedForAfterpay(cartModel)) {
            // cart contains product(s) excluded for afterpay
            afterpay.setReason(PAYMENT_OPTION_NOT_AVAILABLE);
        }
        else {
            try {
                checkAfterpayThreshold(cartModel, afterpay);
            }
            catch (final IllegalArgumentException e) {
                // when AfterpayConfig doesn't exist
                LOG.error(e);
                afterpay.setAvailable(false);
            }
        }
        enabledPayments.add(afterpay);
    }

    /**
     * @param enabledPayments
     */
    private void populateZipPaymentOption(final List<PaymentMethodData> enabledPayments) {
        final PaymentMethodData zip = new PaymentMethodData(TgtFacadesConstants.ZIP, false);
        final CartModel cartModel = targetCheckoutFacade.getCart();
        if (targetCommerceCartService.isExcludedForZip(cartModel)) {
            // cart contains product(s) excluded for zip
            zip.setReason(PAYMENT_OPTION_NOT_AVAILABLE);
        }
        else {
            if (targetPaymentService.ping(TgtpaymentConstants.ZIPPAY_PAYMENT_TYPE)) {
                zip.setAvailable(true);
            }
            else {
                zip.setRetryRequired(true);
            }
        }
        enabledPayments.add(zip);
    }

    /**
     * Check whether the cart total is within afterpay threshold
     * 
     * @param cartModel
     * @param afterpay
     */
    private void checkAfterpayThreshold(final CartModel cartModel, final PaymentMethodData afterpay) {
        final BigDecimal cartTotal = cartModel.getTotalPrice() != null ? BigDecimal.valueOf(cartModel.getTotalPrice()
                .doubleValue()) : BigDecimal.ZERO;
        final boolean cartWithinAfterpayThresholds = targetCommerceCartService
                .isCartWithinAfterpayThresholds(cartTotal);
        // cart products are included for afterpay, so perform further checks
        if (cartWithinAfterpayThresholds) {
            // cart within threshold so check for afterpay availability
            if (afterpayConfigFacade.ping()) {
                // ping success
                afterpay.setAvailable(true);
            }
            else {
                // ping failed
                afterpay.setRetryRequired(true);
            }
        }
        else {
            // cart products are outside threshold, so afterpay not available
            final AfterpayConfigData afterpayConfigData = afterpayConfigFacade.getAfterpayConfig();
            final String afterpayCartThresholdError = "Available on orders "
                    + StringUtils.substringBefore(afterpayConfigData.getMinimumAmount().getFormattedValue(), ".")
                    + " to "
                    + StringUtils.substringBefore(afterpayConfigData.getMaximumAmount().getFormattedValue(), ".");
            afterpay.setReason(afterpayCartThresholdError);
        }
    }


    @Override
    public Response searchAddress(final String keyword, final int maxSize) {
        Response response;
        try {
            response = new Response(true);
            final CheckoutOptionsResponseData responseData = new CheckoutOptionsResponseData();
            List<au.com.target.tgtverifyaddr.data.AddressData> addressSuggestions = targetAddressVerificationService
                    .searchAddress(keyword,
                            CountryEnum.AUSTRALIA);
            if (addressSuggestions.size() > maxSize) {
                addressSuggestions = addressSuggestions.subList(0, maxSize);
            }
            responseData.setAddressSuggestions(addressSuggestions);
            response.setData(responseData);
        }
        catch (final ServiceNotAvailableException | RequestTimeOutException e) {
            response = createErrorResponse("ERR_QAS_UNAVAILABLE", e.getMessage());
        }
        catch (final NoMatchFoundException e) {
            response = createErrorResponse("ERR_NO_MATCH", e.getMessage());
        }
        catch (final TooManyMatchesFoundException e) {
            response = createErrorResponse("ERR_TOOMANY_MATCHES", e.getMessage());
        }
        return response;
    }


    @Override
    public Response setIpgPaymentMode(final String paymentMode, final String returnUrl) {
        Response response = null;
        IpgPaymentTemplateType ipgPaymentTemplateType = null;
        if (TgtFacadesConstants.IPG.equals(paymentMode)) {
            ipgPaymentTemplateType = IpgPaymentTemplateType.CREDITCARDSINGLE;
        }
        else if (TgtFacadesConstants.GIFT_CARD.equals(paymentMode)) {
            ipgPaymentTemplateType = IpgPaymentTemplateType.GIFTCARDMULTIPLE;
        }
        else {
            response = createErrorResponse("ERR_PAYMENTMODE_NONEXIST", "invalid payment mode");
        }
        if (ipgPaymentTemplateType != null) {
            final CartModel cartModel = targetCheckoutFacade.getCart();
            if (cartModel != null) {
                targetCheckoutFacade.setPaymentMode(paymentMode);

                TargetAddressData billingAddress = null;
                if (cartModel.getPaymentAddress() != null) {
                    billingAddress = (TargetAddressData)addressConverter.convert(cartModel.getPaymentAddress());
                }
                targetCheckoutFacade.createIpgPaymentInfo(billingAddress, ipgPaymentTemplateType);
                final TargetCreateSubscriptionResult targetCreateSubscriptionResult = targetCheckoutFacade
                        .getIpgSessionToken(returnUrl);
                final String token = targetCreateSubscriptionResult != null
                        ? targetCreateSubscriptionResult.getRequestToken() : StringUtils.EMPTY;
                targetCheckoutFacade.updateIpgPaymentInfoWithToken(token);
                response = new Response(true);
                final PaymentResponseData setPaymentResponseData = new PaymentResponseData();
                setPaymentResponseData.setPaymentMethod(paymentMode);
                final String sessionId = cartModel.getUniqueKeyForPayment();
                setPaymentResponseData.setIframeUrl(targetCheckoutFacade.createIpgUrl(sessionId,
                        token));
                setPaymentResponseData.setIpgToken(token);
                setPaymentResponseData.setSessionId(sessionId);
                response.setData(setPaymentResponseData);
            }
            else {
                response = createErrorResponse("ERR_CART_INVALID", "cart data is not complete to process to payment");
            }
        }
        return response;
    }

    @Override
    public void removeExistingPayment() {
        targetPlaceOrderFacade.reverseGiftCardPayment();
        targetCheckoutFacade.removePaymentInfo();
    }

    @Override
    public Response setPaypalPaymentMode(final String payPalReturnUrl, final String payPalCancelUrl) {
        final Response response = new Response(true);
        final PaymentResponseData paymentResponseData = new PaymentResponseData();
        TargetCreateSubscriptionResult targetCreateSubscriptionResult;
        try {
            targetCreateSubscriptionResult = targetCheckoutFacade.getPayPalSessionToken(payPalReturnUrl,
                    payPalCancelUrl);
            if (null == targetCreateSubscriptionResult
                    || StringUtils.isEmpty(targetCreateSubscriptionResult.getRequestToken())) {
                return createErrorResponse("ERR_CART_INVALID", "Cart data is not complete to process to payment");
            }
            paymentResponseData.setPaypalSessionToken(targetCreateSubscriptionResult.getRequestToken());
        }
        catch (final AdapterException adapterException) {
            LOG.error("PayPal is currently unavailable " + new Date());
            return createErrorResponse("ERR_PAYPAL_UNAVAILABLE", "PayPal is currently unavailable");
        }
        targetCheckoutFacade.setPaymentMode(TgtFacadesConstants.PAYPAL);
        response.setData(paymentResponseData);
        return response;
    }

    @Override
    public Response setAfterpayPaymentMode(final String afterpayReturnUrl, final String afterpayCancelUrl) {
        final Response response = new Response(true);
        final PaymentResponseData paymentResponseData = new PaymentResponseData();
        TargetCreateSubscriptionResult targetCreateSubscriptionResult;
        try {
            targetCreateSubscriptionResult = targetCheckoutFacade.getAfterpaySessionToken(afterpayReturnUrl,
                    afterpayCancelUrl);
            if (null == targetCreateSubscriptionResult
                    || StringUtils.isEmpty(targetCreateSubscriptionResult.getRequestToken())) {
                return createErrorResponse("ERR_CART_INVALID", "Cart data is not complete to process to payment");
            }
            paymentResponseData.setAfterpaySessionToken(targetCreateSubscriptionResult.getRequestToken());
        }
        catch (final AdapterException adapterException) {
            LOG.error("Afterpay is currently unavailable " + new Date());
            return createErrorResponse("ERR_AFTERPAY_UNAVAILABLE", "Afterpay is currently unavailable");
        }
        targetCheckoutFacade.setPaymentMode(TgtFacadesConstants.AFTERPAY);
        response.setData(paymentResponseData);
        return response;
    }

    @Override
    public Response setZippayPaymentMode(final String zippayRedirectUrl) {
        final Response response = new Response(true);
        final PaymentResponseData paymentResponseData = new PaymentResponseData();
        TargetCreateSubscriptionResult targetCreateSubscriptionResult;
        try {
            targetCreateSubscriptionResult = targetCheckoutFacade.getZippaySessionToken(zippayRedirectUrl);
            if (null == targetCreateSubscriptionResult
                    || StringUtils.isEmpty(targetCreateSubscriptionResult.getRequestToken())
                    || StringUtils.isEmpty(targetCreateSubscriptionResult.getZipPaymentRedirectUrl())) {
                return createErrorResponse("ERR_CART_INVALID", "Cart data is not complete to process to payment");
            }
            paymentResponseData.setZipPaymentSessionToken(targetCreateSubscriptionResult.getRequestToken());
            paymentResponseData.setZipPaymentRedirectUrl(targetCreateSubscriptionResult.getZipPaymentRedirectUrl());
        }
        catch (final AdapterException adapterException) {
            LOG.log(Level.ERROR, TgtFacadesConstants.ZIPPAY_LOG_FORMAT + " Zippay is currently unavailable");
            return createErrorResponse("ERR_ZIPPAY_UNAVAILABLE", "Zippay is currently unavailable");
        }
        targetCheckoutFacade.setPaymentMode(TgtFacadesConstants.ZIPPAY);
        response.setData(paymentResponseData);
        return response;
    }

    @Override
    public Response getBillingCountries() {
        final Response response = new Response(true);
        final BillingCountryResponseData billingCountryResponseData = new BillingCountryResponseData();
        final List<CountryData> countries = targetCheckoutFacade.getBillingCountries();
        billingCountryResponseData.setCountries(countries);
        response.setData(billingCountryResponseData);
        return response;
    }

    @Override
    public Response createBillingAddress(final TargetAddressData newAddressData) {
        Assert.notNull(newAddressData, "address cannot be null when create billing address");
        targetCheckoutFacade.createBillingAddress(newAddressData);
        if (newAddressData.getId() != null) {
            final Response response = new Response(true);
            final CartDetailResponseData responseData = new CartDetailResponseData();
            responseData.setBillingAddress(newAddressData);
            response.setData(responseData);
            return response;
        }
        return this.createErrorResponse("ERR_VALIDATION", "Invalid post data");
    }


    @Override
    public Response removeFlybuys() {
        if (targetCheckoutFacade.removeFlybuysNumber()) {
            return createResponseWithCartDetail(CartDetailOption.FLYBUYS);
        }
        return createErrorResponse("ERR_CART_INVALID", "Invalid cart data");

    }

    @Override
    public Response getIpgPaymentStatus() {
        final Response response = new Response(true);
        final PaymentResponseData responseData = new PaymentResponseData();
        responseData.setHasPartialPayment(Boolean.FALSE);
        final CartModel cart = targetCheckoutFacade.getCart();
        if (cart != null) {
            final List<TargetCardResult> giftcardPayments = targetPlaceOrderFacade.getGiftCardPayments(cart);
            if (giftcardPayments != null) {
                for (final TargetCardResult cartResult : giftcardPayments) {
                    if (cartResult.getTargetCardPaymentResult() != null
                            && cartResult.getTargetCardPaymentResult().isPaymentSuccessful()) {
                        responseData.setHasPartialPayment(Boolean.TRUE);
                        break;
                    }
                }
            }
        }
        response.setData(responseData);
        return response;
    }

    @Override
    public Response loginFlybuys(final TargetFlybuysLoginData flybuysLoginData) {
        final FlybuysRequestStatusData statusData = flybuysDiscountFacade.getFlybuysRedemptionData(
                flybuysLoginData.getCardNumber(), flybuysLoginData.getDateOfBirth(), flybuysLoginData.getPostCode());
        final FlybuysResponseType flybuysRequestStatus = statusData.getResponse();
        if (FlybuysResponseType.SUCCESS != flybuysRequestStatus) {
            return createErrorResponse(
                    "ERR_FLYBUYS_" + StringUtils.upperCase(flybuysRequestStatus.getMessageKey()), null);
        }
        return createResponseWithCartDetail(CartDetailOption.FLYBUYS);
    }

    @Override
    public Response redeemFlybuys(final String redeemCode) {

        if (targetCheckoutFacade.isCartHavingPreOrderItem()) {
            return createErrorResponse("ERR_FLYBUYS_REDEMPTION_NOT_AVAILABLE_FOR_PREORDER",
                    "Flybuys redemption not available for pre-orders.");
        }

        final boolean success;
        if (FLYBUYS_REMOVE_DISCOUNT_CODE.equals(redeemCode)) {
            success = flybuysDiscountFacade.removeFlybuysDiscountFromCheckoutCart();
        }
        else {
            success = flybuysDiscountFacade.applyFlybuysDiscountToCheckoutCart(redeemCode);
        }
        if (success) {
            return createResponseWithCartDetail(CartDetailOption.FLYBUYS);
        }
        return createErrorResponse(
                "ERR_FLYBUYS_REDEMPTION", "There was an error saving your details. Please try again.");
    }

    @Override
    public Response showFlybuysRedemptionOption() {
        return createResponseWithCartDetail(CartDetailOption.FLYBUYS);
    }

    @Override
    public Response getOrderDetail(final String orderCode, final AdjustedCartEntriesData sohUpdates) {
        final Response response = new Response(true);
        final TargetOrderData orderData = targetOrderFacade.getOrderDetailsForCode(orderCode);
        final OrderDetailGaData orderDetailGaData = new OrderDetailGaData();
        orderDetailGaPopulator.populate(orderData, orderDetailGaData);
        final OrderDetailResponseData data = new OrderDetailResponseData();
        populateOrderDetails(data, orderData);
        data.setOrderDetailGaData(orderDetailGaData);
        data.setSohUpdates(sohUpdates);
        response.setData(data);
        return response;
    }

    private void populateOrderDetails(final OrderDetailResponseData data, final TargetOrderData orderData) {
        final OrderDetailsData orderDetails = new OrderDetailsData();
        if (orderData.getStatus() != null) {
            orderDetails.setStatus(orderData.getStatus().getCode());
        }
        orderDetails.setEmail(orderData.getEmail());
        orderDetails.setTotalPrice(orderData.getTotalPrice());
        orderDetails.setDisplayMutipleConsignment(orderData.isMultipleConsignments());
        data.setOrderDetails(orderDetails);
    }

    @Override
    public Response registerCustomer(final String orderId, final String password) {
        final TargetOrderData orderData = targetOrderFacade.getOrderDetailsForCode(orderId);
        if (targetCustomerFacade.isPasswordBlacklisted(password)) {
            return createErrorResponse(TgtFacadesConstants.WebServiceError.ERR_LOGIN_REGISTER_WEAK_PASSWORD,
                    "Supplied password is too weak");
        }

        try {
            final TargetRegisterData registerData = new TargetRegisterData();
            registerData.setLogin(orderData.getEmail());
            registerData.setFirstName(orderData.getDeliveryAddress().getFirstName());
            registerData.setLastName(orderData.getDeliveryAddress().getLastName());
            if (null != orderData.getDeliveryAddress().getTitle()) {
                registerData.setTitleCode(orderData.getDeliveryAddress().getTitleCode());
            }
            registerData.setPassword(password);
            registerData.setMobileNumber(orderData.getDeliveryAddress().getPhone());
            targetCustomerFacade.register(registerData);
            final Response response = new Response(true);
            final RegisterUserResponseData data = new RegisterUserResponseData();
            data.setEmail(registerData.getLogin());
            response.setData(data);
            return response;
        }
        catch (final DuplicateUidException ex) {
            return createErrorResponse(TgtFacadesConstants.WebServiceError.ERR_LOGIN_REGISTER_USER_EXISTS,
                    "A user with this ID already exists.");
        }
    }

    @Override
    public Response checkoutProblem() {
        final CartModel cart = targetCheckoutFacade.getCart();

        final boolean paidValueMatchesCartTotal = cart == null
                || cart.getTotalPrice().compareTo(Double.valueOf(0d)) == 0 || paidValueMatchesCartTotal(cart);

        targetCustomerFacade.invalidateUserSession();

        if (paidValueMatchesCartTotal) {
            return createSuccessResponse("THANKYOU_CHECK_PAYMENT_RECEIVED",
                    "Payment was received but further checks required");
        }
        else {
            return createSuccessResponse("THANKYOU_CHECK_PAYMENT_ERROR",
                    "There was an error in payment and order has not been placed");
        }

    }

    /**
     * Checks whether paid value matches cart total.
     * 
     * @param cart
     * @return flag
     */
    private boolean paidValueMatchesCartTotal(final CartModel cart) {
        final Double amountPaidSoFar = getAmountPaidSoFar(cart);
        return amountPaidSoFar != null && amountPaidSoFar.doubleValue() > 0
                && cart.getTotalPrice().compareTo(amountPaidSoFar) == 0;
    }

    /**
     * Gets the amount paid so far for the cart.
     * 
     * @param cart
     * @return Double
     */
    private Double getAmountPaidSoFar(final CartModel cart) {
        Double paidAmount = Double.valueOf(0);
        if (cart != null) {
            paidAmount = targetPlaceOrderFacade.getAmountPaidSoFar(cart);
        }
        return paidAmount;
    }

    /**
     * Helper method to create an Error DTO.
     * 
     * @param errorCode
     * @param message
     * @return Error
     */
    private Response createErrorResponse(final String errorCode, final String message) {
        final Response response = new Response(false);
        final BaseResponseData baseResponseData = new BaseResponseData();
        final Error error = new Error(errorCode);
        error.setMessage(message);
        baseResponseData.setError(error);
        response.setData(baseResponseData);
        return response;
    }

    /**
     * Helper method to create an Error DTO.
     * 
     * @param errorCode
     * @param message
     * @return Error
     */
    private Response createSuccessResponse(final String errorCode, final String message) {
        final Response response = new Response(true);
        final BaseResponseData baseResponseData = new BaseResponseData();
        final Error error = new Error(errorCode);
        error.setMessage(message);
        baseResponseData.setError(error);
        response.setData(baseResponseData);
        return response;
    }

    /**
     * @param targetCheckoutFacade
     *            the targetCheckoutFacade to set
     */
    @Required
    public void setTargetCheckoutFacade(final TargetCheckoutFacade targetCheckoutFacade) {
        this.targetCheckoutFacade = targetCheckoutFacade;
    }

    /**
     * @param targetStoreLocatorFacade
     *            the targetStoreLocatorFacade to set
     */
    @Required
    public void setTargetStoreLocatorFacade(final TargetStoreLocatorFacade targetStoreLocatorFacade) {
        this.targetStoreLocatorFacade = targetStoreLocatorFacade;
    }

    /**
     * @param deliveryModeService
     *            the deliveryModeService to set
     */
    @Required
    public void setDeliveryModeService(final DeliveryModeService deliveryModeService) {
        this.deliveryModeService = deliveryModeService;
    }

    @Required
    public void setCartDetailPopulator(
            final ConfigurablePopulator<TargetCartData, CartDetailResponseData, CartDetailOption> cartDetailPopulator) {
        this.cartDetailPopulator = cartDetailPopulator;
    }

    /**
     * @param targetVoucherFacade
     *            the targetVoucherFacade to set
     */
    @Required
    public void setTargetVoucherFacade(final TargetVoucherFacade targetVoucherFacade) {
        this.targetVoucherFacade = targetVoucherFacade;
    }

    /**
     * @param targetUserFacade
     *            the targetUserFacade to set
     */
    @Required
    public void setTargetUserFacade(final TargetUserFacade targetUserFacade) {
        this.targetUserFacade = targetUserFacade;
    }

    /**
     * @param targetFeatureSwitchFacade
     *            the targetFeatureSwitchFacade to set
     */
    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }

    @Required
    public void setTargetAddressVerificationService(
            final TargetAddressVerificationService targetAddressVerificationService) {
        this.targetAddressVerificationService = targetAddressVerificationService;
    }

    @Required
    public void setTargetPlaceOrderFacade(final TargetPlaceOrderFacade targetPlaceOrderFacade) {
        this.targetPlaceOrderFacade = targetPlaceOrderFacade;
    }

    /**
     * @param addressConverter
     *            the addressConverter to set
     */
    @Required
    public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter) {
        this.addressConverter = addressConverter;
    }

    /**
     * @param targetDeliveryFacade
     *            the targetDeliveryFacade to set
     */
    @Required
    public void setTargetDeliveryFacade(final TargetDeliveryFacade targetDeliveryFacade) {
        this.targetDeliveryFacade = targetDeliveryFacade;
    }

    /**
     * @param flybuysDiscountFacade
     *            the flybuysDiscountFacade to set
     */
    @Required
    public void setFlybuysDiscountFacade(final FlybuysDiscountFacade flybuysDiscountFacade) {
        this.flybuysDiscountFacade = flybuysDiscountFacade;
    }

    /**
     * @param targetOrderFacade
     *            the targetOrderFacade to set
     */
    @Required
    public void setTargetOrderFacade(final TargetOrderFacade targetOrderFacade) {
        this.targetOrderFacade = targetOrderFacade;
    }

    /**
     * @param orderDetailGaPopulator
     *            the orderDetailGaPopulator to set
     */
    @Required
    public void setOrderDetailGaPopulator(final Populator<TargetOrderData, OrderDetailGaData> orderDetailGaPopulator) {
        this.orderDetailGaPopulator = orderDetailGaPopulator;
    }

    /**
     * @param targetCustomerFacade
     *            the targetCustomerFacade to set
     */
    @Required
    public void setTargetCustomerFacade(final TargetCustomerFacade targetCustomerFacade) {
        this.targetCustomerFacade = targetCustomerFacade;
    }

    /**
     * @param targetCommerceCartService
     *            the targetCommerceCartService to set
     */
    @Required
    public void setTargetCommerceCartService(final TargetCommerceCartService targetCommerceCartService) {
        this.targetCommerceCartService = targetCommerceCartService;
    }

    /**
     * @param afterpayConfigFacade
     *            the afterpayConfigFacade to set
     */
    @Required
    public void setAfterpayConfigFacade(final AfterpayConfigFacade afterpayConfigFacade) {
        this.afterpayConfigFacade = afterpayConfigFacade;
    }

    @Override
    public Response verifyShipsterStatus() {
        try {
            final CartModel sessionCart = cartService.getSessionCart();
            sessionCart.setAusPostClubMember(Boolean.FALSE);
            sessionCart.setAusPostDeliveryClubFreeDelivery(Boolean.FALSE);
            final String cartUserEmail = targetCustomerFacade.getCartUserEmail();
            final boolean isEmailVerified = shipsterClientService.verifyEmail(cartUserEmail);
            sessionCart.setAusPostClubMember(BooleanUtils.toBooleanObject(isEmailVerified));
            recalculateCartWithDeliveryMode(sessionCart);
            modelService.save(sessionCart);
            modelService.refresh(sessionCart);

            final ShipsterEmailVerificationResponseData verifyEmailResponse = new ShipsterEmailVerificationResponseData();
            verifyEmailResponse.setDeliveryClubMember(isEmailVerified);
            final Response response = new Response(true);
            response.setData(verifyEmailResponse);
            return response;
        }
        catch (final AuspostShipsterClientException e) {

            return createErrorResponse("UNABLE_TO_VERIFY", "Unable to verify email for Shipster");
        }
    }

    private void recalculateCartWithDeliveryMode(final CartModel cartModel) {
        if (cartModel.getDeliveryMode() != null) {
            try {
                calculationService.calculateTotals(cartModel, true);
            }
            catch (final CalculationException e) {
                LOG.error("Failed to calculate cart [" + cartModel + "]", e);
            }
        }
    }

    /**
     * @param cartService
     *            the cartService to set
     */
    @Required
    public void setCartService(final CartService cartService) {
        this.cartService = cartService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param shipsterClientService
     *            the shipsterClientService to set
     */
    @Required
    public void setShipsterClientService(final ShipsterClientService shipsterClientService) {
        this.shipsterClientService = shipsterClientService;
    }

    /**
     * @param calculationService
     *            the calculationService to set
     */
    @Required
    public void setCalculationService(final CalculationService calculationService) {
        this.calculationService = calculationService;
    }

    /**
     * @param targetPaymentService
     *            the targetPaymentService to set
     */
    @Required
    public void setTargetPaymentService(final TargetPaymentService targetPaymentService) {
        this.targetPaymentService = targetPaymentService;
    }

}
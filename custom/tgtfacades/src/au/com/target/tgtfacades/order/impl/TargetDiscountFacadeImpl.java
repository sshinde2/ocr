/**
 * 
 */
package au.com.target.tgtfacades.order.impl;

import org.springframework.beans.factory.annotation.Autowired;

import au.com.target.tgtcore.order.TargetDiscountService;
import au.com.target.tgtfacades.order.TargetDiscountFacade;


/**
 * @author rmcalave
 * 
 */
public class TargetDiscountFacadeImpl implements TargetDiscountFacade {

    @Autowired
    private TargetDiscountService targetDiscountService;

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.order.TargetDiscountFacade#validateTeamMemberDiscountCard(java.lang.String)
     */
    @Override
    public boolean validateTeamMemberDiscountCard(final String cardNumber) {
        return getTargetDiscountService().isValidTMDCard(cardNumber);
    }

    /**
     * @return the targetDiscountService
     */
    protected TargetDiscountService getTargetDiscountService() {
        return targetDiscountService;
    }
}

/**
 * 
 */
package au.com.target.tgtfacades.order.impl;

import de.hybris.platform.commercefacades.order.impl.DefaultOrderFacade;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;

import java.text.MessageFormat;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.helper.ClickAndCollectOrderDetailsHelper;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.order.TargetOrderFacade;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.order.enums.TargetUpdateCardErrorType;
import au.com.target.tgtfacades.order.exception.TargetUpdateCardException;
import au.com.target.tgtpayment.commands.request.TargetQueryTransactionDetailsRequest;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.dto.HostedSessionTokenRequest;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.service.TargetPaymentService;
import au.com.target.tgtpayment.strategy.PaymentMethodStrategy;
import au.com.target.tgtpayment.util.PaymentTransactionHelper;



/**
 * @author Benoit VanalderWeireldt
 * 
 */
public class TargetOrderFacadeImpl extends DefaultOrderFacade implements TargetOrderFacade {

    private static final Logger LOG = Logger.getLogger(TargetOrderFacadeImpl.class);

    private ClickAndCollectOrderDetailsHelper clickAndCollectOrderDetailsHelper;

    private TargetOrderService targetOrderService;

    @Autowired
    private TargetPaymentService targetPaymentService;

    @Autowired
    private PaymentModeService paymentModeService;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private PaymentMethodStrategy paymentMethodStrategy;

    @Autowired
    private ModelService modelService;

    @Override
    public TargetOrderData getOrderDetailsForCode(final String code) {

        final OrderModel orderModel = getTargetOrderService().findOrderModelForOrderId(code);

        if (orderModel == null) {
            throw new UnknownIdentifierException("Order with code " + code
                    + " not found for current user in current BaseStore");
        }
        return (TargetOrderData)getOrderConverter().convert(orderModel);
    }

    @Override
    public TargetOrderData getOrderDetailsForCodeUnderCurrentCustomer(final String code) {
        TargetOrderData targetOrderData = null;
        try {
            targetOrderData = (TargetOrderData)super.getOrderDetailsForCode(code);
            setCncCustomOrderInformation(code, targetOrderData);
        }
        catch (final ModelNotFoundException ex) {
            throw new UnknownIdentifierException("Order with code " + code
                    + " not found for current user in current BaseStore", ex);
        }
        return targetOrderData;
    }

    @Override
    public boolean getOrderLastTransactionDetailsForCodeUnderCurrentCustomer(final String code) {

        final OrderModel orderModel = getTargetOrderService().findOrderModelForOrderId(code);

        return PaymentTransactionHelper.isLastTransactionPending(orderModel);
    }

    @Override
    public void linkOrderToCustomer(final String orderId) {
        final OrderModel orderModel = getTargetOrderService().findOrderModelForOrderId(orderId);
        final UserModel currentUser = getUserService().getCurrentUser();
        if (orderModel.getUser() != null) {
            final String guestOrderEmailId = StringUtils.substringAfter(orderModel.getUser().getUid(), "|");
            if (StringUtils.equalsIgnoreCase(guestOrderEmailId, currentUser.getUid())) {
                getTargetOrderService().updateOwner(orderModel, currentUser);
            }
        }
    }

    @Override
    public String createIpgUrl(final String orderId, final String cancelUrl, final String returnUrl)
            throws TargetUpdateCardException {
        final OrderModel orderModel = getOrderModelForCode(orderId);
        if (orderModel != null && OrderStatus.PARKED.equals(orderModel.getStatus())) {
            final String uniqueKey = orderModel.getUniqueKeyForPayment();
            final HostedSessionTokenRequest hostedSessionTokenRequest = targetPaymentService
                    .createUpdateCardHostedSessionTokenRequest(orderModel,
                            paymentModeService.getPaymentModeForCode(TgtFacadesConstants.IPG), cancelUrl, returnUrl,
                            PaymentCaptureType.PLACEORDER,
                            uniqueKey);
            final TargetCreateSubscriptionResult targetCreateSubscriptionResult = targetPaymentService
                    .getHostedSessionToken(hostedSessionTokenRequest);
            final String token = targetCreateSubscriptionResult != null
                    ? targetCreateSubscriptionResult.getRequestToken() : StringUtils.EMPTY;
            return createIpgIFrameUrl(uniqueKey, token);
        }
        else {
            throw new TargetUpdateCardException(MessageFormat.format(
                    "Updating credit card details is not applicable for this order orderNumber={0}", orderId),
                    TargetUpdateCardErrorType.INVALID_ORDER_STATE);
        }
    }

    private OrderModel getOrderModelForCode(final String orderId) throws TargetUpdateCardException {
        try {
            final BaseStoreModel baseStoreModel = getBaseStoreService().getCurrentBaseStore();
            final OrderModel orderModel = getCustomerAccountService()
                    .getOrderForCode((TargetCustomerModel)getUserService().getCurrentUser(), orderId, baseStoreModel);
            return orderModel;
        }
        catch (final ModelNotFoundException | AmbiguousIdentifierException ex) {
            throw new TargetUpdateCardException(MessageFormat.format(
                    "Attempted to load an order that does not exist or is not visible, with orderNumber={0}", orderId),
                    TargetUpdateCardErrorType.INVALID_ORDER);
        }
    }

    private String createIpgIFrameUrl(final String sessionId, final String token) {
        final StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(configurationService.getConfiguration().getString(TgtFacadesConstants.IPG_BASE_URL));
        urlBuilder.append("?sessionId=");
        urlBuilder.append(sessionId);
        urlBuilder.append("&SST=");
        urlBuilder.append(token);
        return urlBuilder.toString();
    }

    @Override
    public boolean isUpdateCreditCardDetailsSuccessful(final String orderId, final String ipgToken) {

        OrderModel orderModel = null;
        try {
            orderModel = getOrderModelForCode(orderId);
        }
        catch (final TargetUpdateCardException e) {
            LOG.info(MessageFormat.format(
                    "ORDER-UPDATECREDITCARD-FAILED : Attempted to update CC of non-existing or not visible order, orderNumber={0} ",
                    orderId));
            return false;
        }

        if (orderModel.getStatus() != OrderStatus.PARKED) {
            LOG.info(MessageFormat.format(
                    "ORDER-UPDATECREDITCARD-FAILED : Attempted to update CC for non PARKED state order, orderNumber={0} ",
                    orderModel.getCode()));
            // only allowed if order status is PARKED
            return false;
        }
        final TargetQueryTransactionDetailsRequest targetQueryTransactionDetailsRequest = new TargetQueryTransactionDetailsRequest();
        targetQueryTransactionDetailsRequest.setSessionId(orderModel.getUniqueKeyForPayment());
        targetQueryTransactionDetailsRequest.setSessionToken(ipgToken);
        final TargetQueryTransactionDetailsResult queryTransactionDetailsResult = paymentMethodStrategy
                .getPaymentMethod(orderModel.getPaymentInfo())
                .queryTransactionDetails(targetQueryTransactionDetailsRequest);

        if (null == queryTransactionDetailsResult || queryTransactionDetailsResult.isCancel()) {
            LOG.info(MessageFormat.format(
                    "ORDER-UPDATECREDITCARD-FAILED : Update credit card failed, orderNumber={0}",
                    orderModel.getCode()));
            return false;
        }
        else {
            // before updating the new card, check if any paymentTransaction model exists for state DEFERRED, then remove that.
            final PaymentTransactionEntryModel existingDeferredPaymentModel = PaymentTransactionHelper
                    .findTransactionEntryWithState(orderModel.getPaymentTransactions(),
                            PaymentTransactionType.DEFERRED);
            if (existingDeferredPaymentModel != null) {
                modelService.remove(existingDeferredPaymentModel.getPaymentTransaction());
            }

            targetPaymentService.createTransactionWithQueryResult(orderModel, queryTransactionDetailsResult,
                    PaymentTransactionType.DEFERRED);
            LOG.info(MessageFormat.format(
                    "ORDER-UPDATECREDITCARD-SUCCESS : Update credit card success, orderNumber={0}",
                    orderModel.getCode()));
            return true;
        }
    }

    /**
     * Method to populate Custom order information for the Cnc order.
     *
     * @param code
     * @param targetOrderData
     */
    private void setCncCustomOrderInformation(final String code, final TargetOrderData targetOrderData) {
        if (null == targetOrderData) {
            return;
        }
        final OrderModel orderModel = getTargetOrderService().findOrderModelForOrderId(code);
        if (null != orderModel && clickAndCollectOrderDetailsHelper.isCncDeliveryMode(orderModel)) {
            targetOrderData.setDeliveryModeStoreDelivery(true);
            targetOrderData
                    .setCncCustomOrderStatus(clickAndCollectOrderDetailsHelper.getCustomOrderStatus(orderModel));
            targetOrderData.setCncOrderWithConsignmentPickedUpDate(
                    clickAndCollectOrderDetailsHelper.isOrderCompletedWithConsignmentPickedUpDate(orderModel));
            targetOrderData
                    .setCncOrderReadyForPickup(clickAndCollectOrderDetailsHelper.isOrderReadyForPickup(orderModel));
            targetOrderData.setCncOrderRefunded(clickAndCollectOrderDetailsHelper.isOrderRefunded(orderModel));
            targetOrderData
                    .setCncOrderPartialRefund(clickAndCollectOrderDetailsHelper.isCncOrderPartialRefund(orderModel));
        }

    }

    /**
     * @return the targetOrderService
     */
    protected TargetOrderService getTargetOrderService() {
        return targetOrderService;
    }

    /**
     * @param targetOrderService
     *            the targetOrderService to set
     */
    @Required
    public void setTargetOrderService(final TargetOrderService targetOrderService) {
        this.targetOrderService = targetOrderService;
    }

    /**
     * @return the targetPaymentService
     */
    public TargetPaymentService getTargetPaymentService() {
        return targetPaymentService;
    }

    /**
     * @param targetPaymentService
     *            the targetPaymentService to set
     */
    public void setTargetPaymentService(final TargetPaymentService targetPaymentService) {
        this.targetPaymentService = targetPaymentService;
    }

    /**
     * @return the paymentModeService
     */
    public PaymentModeService getPaymentModeService() {
        return paymentModeService;
    }

    /**
     * @param paymentModeService
     *            the paymentModeService to set
     */
    public void setPaymentModeService(final PaymentModeService paymentModeService) {
        this.paymentModeService = paymentModeService;
    }

    /**
     * @return the configurationService
     */
    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    /**
     * @param configurationService
     *            the configurationService to set
     */
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    /**
     * @return the clickAndCollectOrderDetailsHelper
     */
    public ClickAndCollectOrderDetailsHelper getClickAndCollectOrderDetailsHelper() {
        return clickAndCollectOrderDetailsHelper;
    }

    /**
     * @param clickAndCollectOrderDetailsHelper
     *            the clickAndCollectOrderDetailsHelper to set
     */
    @Required
    public void setClickAndCollectOrderDetailsHelper(
            final ClickAndCollectOrderDetailsHelper clickAndCollectOrderDetailsHelper) {
        this.clickAndCollectOrderDetailsHelper = clickAndCollectOrderDetailsHelper;
    }

}

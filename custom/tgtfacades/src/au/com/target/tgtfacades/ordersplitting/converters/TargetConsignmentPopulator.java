/**
 * 
 */
package au.com.target.tgtfacades.ordersplitting.converters;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commercefacades.order.converters.populator.ConsignmentPopulator;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;


/**
 * @author rmcalave
 * 
 */
public class TargetConsignmentPopulator extends ConsignmentPopulator {

    private static final Logger LOG = Logger.getLogger(TargetConsignmentPopulator.class);

    private EnumerationService enumerationService;

    private TargetStoreConsignmentService targetStoreConsignmentService;

    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Override
    public void populate(final ConsignmentModel source, final ConsignmentData target) {
        if (CollectionUtils.isEmpty(source.getConsignmentEntries())) {
            return;
        }

        final List<ConsignmentEntryData> entries = new ArrayList<>();
        for (final ConsignmentEntryModel consignmentEntryModel : source.getConsignmentEntries()) {
            entries.add(getConsignmentEntryConverter().convert(consignmentEntryModel));
        }
        target.setEntries(entries);

        final ConsignmentStatus consignmentStatus = source.getStatus();
        if (consignmentStatus != null) {
            target.setStatus(consignmentStatus);
            target.setStatusDisplay(enumerationService.getEnumerationName(consignmentStatus));
        }

        target.setCode(source.getCode());

        final boolean isDigitalDelivery = targetDeliveryModeHelper.isDeliveryModeDigital(source.getDeliveryMode());
        target.setIsDigitalDelivery(isDigitalDelivery);

        final String trackingId = source.getTrackingID();
        if (trackingId != null) {
            if (!(source instanceof TargetConsignmentModel)) {
                LOG.warn("Found non target consignment model: " + source.getCode());
            }
            else if (!OrderStatus.CANCELLED.equals(source.getOrder().getStatus())
                    && !ConsignmentStatus.CANCELLED.equals(consignmentStatus)) {
                final TargetCarrierModel carrier = ((TargetConsignmentModel)source).getTargetCarrier();
                if (carrier != null) {
                    target.setTrackingID(getTrackingId(source, trackingId, carrier));
                }
            }
        }
        else if (!isDigitalDelivery && ConsignmentStatus.SHIPPED.equals(consignmentStatus)
                && !targetStoreConsignmentService.isConsignmentAssignedToAnyStore(source)) {
            // Long term lay-bys, we expect a tracking ID in order to say it is truly shipped. Exclude Digital Delivery
            target.setStatus(null);
            target.setStatusDisplay(null);
        }

        if (ConsignmentStatus.SHIPPED.equals(source.getStatus())
                || ConsignmentStatus.READY_FOR_PICKUP.equals(source.getStatus())) {
            target.setStatusDate(source.getShippingDate());
        }

        if (source instanceof TargetConsignmentModel) {
            final TargetConsignmentModel targetConsignmentModel = (TargetConsignmentModel)source;
            target.setPickedUpDate(targetConsignmentModel.getPickedUpDate());
            target.setReadyForPickUpDate(targetConsignmentModel.getReadyForPickUpDate());
        }
    }

    /**
     * Setting tracking id for the order placed. Tracking is not required for the order with click and collect delivery
     * mode.
     *
     * @param source
     * @param trackingId
     * @param carrier
     * @return String
     */
    private String getTrackingId(final ConsignmentModel source, final String trackingId,
            final TargetCarrierModel carrier) {
        return targetDeliveryModeHelper.isDeliveryModeStoreDelivery(source.getDeliveryMode()) ? StringUtils.EMPTY
                : carrier.getTrackingUrl().concat(trackingId);
    }

    /**
     * @param enumerationService
     *            the enumerationService to set
     */
    @Required
    public void setEnumerationService(final EnumerationService enumerationService) {
        this.enumerationService = enumerationService;
    }

    /**
     * @param targetStoreConsignmentService
     *            the targetStoreConsignmentService to set
     */
    @Required
    public void setTargetStoreConsignmentService(final TargetStoreConsignmentService targetStoreConsignmentService) {
        this.targetStoreConsignmentService = targetStoreConsignmentService;
    }

    /**
     * @param targetDeliveryModeHelper
     *            the targetDeliveryModeHelper to set
     */
    @Required
    public void setTargetDeliveryModeHelper(final TargetDeliveryModeHelper targetDeliveryModeHelper) {
        this.targetDeliveryModeHelper = targetDeliveryModeHelper;
    }
}

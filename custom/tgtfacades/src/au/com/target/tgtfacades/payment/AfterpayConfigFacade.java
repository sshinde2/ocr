/**
 * 
 */
package au.com.target.tgtfacades.payment;

import au.com.target.tgtfacades.payment.data.AfterpayConfigData;


/**
 * @author mgazal
 *
 */
public interface AfterpayConfigFacade {

    AfterpayConfigData getAfterpayConfig();

    /**
     * check afterpay availability.
     * 
     * @return - boolean
     */
    boolean ping();
}

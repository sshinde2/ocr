/**
 * 
 */
package au.com.target.tgtfacades.payment.data;

import de.hybris.platform.commercefacades.product.data.PriceData;


/**
 * @author mgazal
 *
 */
public class AfterpayConfigData {

    private String paymentType;

    private String description;

    private PriceData minimumAmount;

    private PriceData maximumAmount;

    /**
     * @return the paymentType
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * @param paymentType
     *            the paymentType to set
     */
    public void setPaymentType(final String paymentType) {
        this.paymentType = paymentType;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return the minimumAmount
     */
    public PriceData getMinimumAmount() {
        return minimumAmount;
    }

    /**
     * @param minimumAmount
     *            the minimumAmount to set
     */
    public void setMinimumAmount(final PriceData minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    /**
     * @return the maximumAmount
     */
    public PriceData getMaximumAmount() {
        return maximumAmount;
    }

    /**
     * @param maximumAmount
     *            the maximumAmount to set
     */
    public void setMaximumAmount(final PriceData maximumAmount) {
        this.maximumAmount = maximumAmount;
    }
}

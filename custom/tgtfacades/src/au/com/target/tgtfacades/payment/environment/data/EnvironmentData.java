/**
 * 
 */
package au.com.target.tgtfacades.payment.environment.data;

import java.math.BigDecimal;

import au.com.target.tgtfacades.payment.data.AfterpayConfigData;


/**
 * @author bbaral1
 *
 */
public class EnvironmentData {

    private AfterpayConfigData afterpayConfigData;

    private boolean isKioskMode;

    private String bazaarVoiceApiUri;

    private String fqdn;

    private BigDecimal preOrderDeposit;

    private String bbStoreName;

    private Integer bbStoreNumber;

    private String addThisClientId;

    private String addThisContainer;

    private String gMapsApiKey;

    /**
     * @return the gMapsApiKey
     */
    public String getgMapsApiKey() {
        return gMapsApiKey;
    }

    /**
     * @param gMapsApiKey
     *            the gMapsApiKey to set
     */
    public void setgMapsApiKey(final String gMapsApiKey) {
        this.gMapsApiKey = gMapsApiKey;
    }

    /**
     * @return the afterpayConfigData
     */
    public AfterpayConfigData getAfterpayConfigData() {
        return afterpayConfigData;
    }

    /**
     * @param afterpayConfigData
     *            the afterpayConfigData to set
     */
    public void setAfterpayConfigData(final AfterpayConfigData afterpayConfigData) {
        this.afterpayConfigData = afterpayConfigData;
    }

    /**
     * @return isKioskMode
     */
    public boolean getIsKioskMode() {
        return isKioskMode;
    }

    /**
     * 
     * @param isKioskMode
     *            the isKioskMode to set
     */
    public void setIsKioskMode(final boolean isKioskMode) {
        this.isKioskMode = isKioskMode;
    }

    /**
     * @return the bazaarVoiceApiUri
     */
    public String getBazaarVoiceApiUri() {
        return bazaarVoiceApiUri;
    }

    /**
     * @param bazaarVoiceApiUri
     *            the bazaarVoiceApiUri to set
     */
    public void setBazaarVoiceApiUri(final String bazaarVoiceApiUri) {
        this.bazaarVoiceApiUri = bazaarVoiceApiUri;
    }

    /**
     * @return the fqdn
     */
    public String getFqdn() {
        return fqdn;
    }

    /**
     * @param fqdn
     *            the fqdn to set
     */
    public void setFqdn(final String fqdn) {
        this.fqdn = fqdn;
    }

    /**
     * @return the preOrderDeposit
     */
    public BigDecimal getPreOrderDeposit() {
        return preOrderDeposit;
    }

    /**
     * @param preOrderDeposit
     *            the preOrderDeposit to set
     */
    public void setPreOrderDeposit(final BigDecimal preOrderDeposit) {
        this.preOrderDeposit = preOrderDeposit;
    }

    /**
     * @return the bbStoreName
     */
    public String getBbStoreName() {
        return bbStoreName;
    }

    /**
     * @param bbStoreName
     *            the bbStoreName to set
     */
    public void setBbStoreName(final String bbStoreName) {
        this.bbStoreName = bbStoreName;
    }

    /**
     * @return the bbStoreNumber
     */
    public Integer getBbStoreNumber() {
        return bbStoreNumber;
    }

    /**
     * @param integer
     *            the bbStoreNumber to set
     */
    public void setBbStoreNumber(final Integer integer) {
        this.bbStoreNumber = integer;
    }

    /**
     * @return the addThisClientId
     */
    public String getAddThisClientId() {
        return addThisClientId;
    }

    /**
     * @return the addThisContainer
     */
    public String getAddThisContainer() {
        return addThisContainer;
    }

    /**
     * @param addThisClientId
     *            the addThisClientId to set
     */
    public void setAddThisClientId(final String addThisClientId) {
        this.addThisClientId = addThisClientId;
    }

    /**
     * @param addThisContainer
     *            the addThisContainer to set
     */
    public void setAddThisContainer(final String addThisContainer) {
        this.addThisContainer = addThisContainer;
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.payment.impl;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.payment.AfterpayConfigFacade;
import au.com.target.tgtfacades.payment.data.AfterpayConfigData;
import au.com.target.tgtpayment.model.AfterpayConfigModel;
import au.com.target.tgtpayment.service.AfterpayConfigService;


/**
 * @author mgazal
 *
 */
public class AfterpayConfigFacadeImpl implements AfterpayConfigFacade {

    private AfterpayConfigService afterpayConfigService;

    private Converter<AfterpayConfigModel, AfterpayConfigData> afterpayConfigConverter;

    @Override
    public AfterpayConfigData getAfterpayConfig() {
        final AfterpayConfigModel afterpayConfigModel = afterpayConfigService.getAfterpayConfig();
        if (afterpayConfigModel != null) {
            return afterpayConfigConverter.convert(afterpayConfigModel);
        }
        return null;
    }

    @Override
    public boolean ping() {
        return afterpayConfigService.ping();
    }

    /**
     * @param afterpayConfigService
     *            the afterpayConfigService to set
     */
    @Required
    public void setAfterpayConfigService(final AfterpayConfigService afterpayConfigService) {
        this.afterpayConfigService = afterpayConfigService;
    }

    /**
     * @param afterpayConfigConverter
     *            the afterpayConfigConverter to set
     */
    @Required
    public void setAfterpayConfigConverter(
            final Converter<AfterpayConfigModel, AfterpayConfigData> afterpayConfigConverter) {
        this.afterpayConfigConverter = afterpayConfigConverter;
    }
}

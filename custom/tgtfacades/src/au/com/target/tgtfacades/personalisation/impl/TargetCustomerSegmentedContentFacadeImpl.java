/**
 * 
 */
package au.com.target.tgtfacades.personalisation.impl;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.contents.components.SimpleCMSComponentModel;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.usergroups.model.UserSegmentModel;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.CollectionUtils;

import au.com.target.tgtfacades.order.impl.TargetCheckoutFacadeImpl;
import au.com.target.tgtfacades.personalisation.TargetCustomerSegmentedContentFacade;
import au.com.target.tgtmarketing.segments.UserSegmentRestrictionForFragmentPageEvaluator;
import au.com.target.tgtwebcore.model.cms2.TargetPersonaliseContainerComponentModel;
import au.com.target.tgtwebcore.model.cms2.pages.TargetFragmentPageModel;


/**
 * @author pthoma20
 *
 */
public class TargetCustomerSegmentedContentFacadeImpl implements TargetCustomerSegmentedContentFacade {

    private static final Logger LOG = Logger.getLogger(TargetCheckoutFacadeImpl.class);

    private static final String RETRIEVE_CONTENT_DYNAMICALLY_ERROR = "SEG-DYNAMIC-CONTENT-ERROR :";

    private CMSComponentService cmsComponentService;

    private UserSegmentRestrictionForFragmentPageEvaluator userSegmentRestrictionForFragmentEvaluator;

    private UserService userService;


    @Override
    public TargetFragmentPageModel getSegmentedContentFromPersonalisedComponent(final String componentId,
            final String userSegmentId) {

        final TargetPersonaliseContainerComponentModel targetPersonaliseContainerComponent;
        try {
            final SimpleCMSComponentModel cmsComponent = cmsComponentService.getSimpleCMSComponent(componentId);

            if (!(cmsComponent instanceof TargetPersonaliseContainerComponentModel)) {
                LOG.error(RETRIEVE_CONTENT_DYNAMICALLY_ERROR + "Cannot find dynamic component with componentId="
                        + componentId);
                return null;
            }
            targetPersonaliseContainerComponent = (TargetPersonaliseContainerComponentModel)cmsComponent;
        }
        catch (final CMSItemNotFoundException e) {
            LOG.error(RETRIEVE_CONTENT_DYNAMICALLY_ERROR + "Cannot find dynamic component with componentId="
                    + componentId, e);
            return null;
        }

        final UserGroupModel group = userService.getUserGroupForUID(userSegmentId);
        if (group instanceof UserSegmentModel) {
            return findPersonalisationComponentsAssociatedPageForSegment((UserSegmentModel)group,
                    targetPersonaliseContainerComponent
                            .getPages());
        }
        else {
            LOG.error(RETRIEVE_CONTENT_DYNAMICALLY_ERROR + "Cannot find user segment for userSegmentId="
                    + userSegmentId);

        }
        return null;
    }

    private TargetFragmentPageModel findPersonalisationComponentsAssociatedPageForSegment(
            final UserSegmentModel segment,
            final Collection<TargetFragmentPageModel> targetFragmentPageModels) {

        if (CollectionUtils.isEmpty(targetFragmentPageModels)) {
            return null;
        }
        for (final TargetFragmentPageModel targetFragmentPageModel : targetFragmentPageModels) {
            if (userSegmentRestrictionForFragmentEvaluator.evaluate(targetFragmentPageModel, segment)) {
                return targetFragmentPageModel;
            }
        }
        return null;
    }


    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @param cmsComponentService
     *            the cmsComponentService to set
     */
    @Required
    public void setCmsComponentService(final CMSComponentService cmsComponentService) {
        this.cmsComponentService = cmsComponentService;
    }

    /**
     * @param userSegmentRestrictionForFragmentEvaluator
     *            the userSegmentRestrictionForFragmentEvaluator to set
     */
    @Required
    public void setUserSegmentRestrictionForFragmentEvaluator(
            final UserSegmentRestrictionForFragmentPageEvaluator userSegmentRestrictionForFragmentEvaluator) {
        this.userSegmentRestrictionForFragmentEvaluator = userSegmentRestrictionForFragmentEvaluator;
    }

}

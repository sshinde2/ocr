/**
 * 
 */
package au.com.target.tgtfacades.prepopulatecart;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import au.com.target.tgtcore.model.TargetCustomerModel;


/**
 * @author pratik
 *
 */
public interface TargetPrepopulateCheckoutCartFacade {

    /**
     * Method to check if the card is pre-populated for checkout
     * 
     * @return PrePopulatedUserInfo
     */
    boolean isCartPrePopulatedForCheckout();

    /**
     * Method to check if user has a preferred delivery mode if not for example for existing users, take the last order
     * and set that delivery mode as preferred.
     * 
     * @param customer
     * @param previousOrder
     * @return {@link TargetZoneDeliveryModeModel}
     */
    TargetZoneDeliveryModeModel getPreferredDeliveryMode(TargetCustomerModel customer, OrderModel previousOrder);

}

/**
 * 
 */
package au.com.target.tgtfacades.prepopulatecart.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.cart.TargetCartService;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.prepopulatecart.TargetPrepopulateCheckoutCartFacade;
import au.com.target.tgtfacades.user.TargetUserFacade;


/**
 * @author pratik
 * 
 */
public class TargetPrepopulateCheckoutCartFacadeImpl implements TargetPrepopulateCheckoutCartFacade {

    private TargetSalesApplicationService targetSalesApplicationService;

    private TargetUserFacade targetUserFacade;

    private TargetOrderService targetOrderService;

    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    private TargetCheckoutFacade targetCheckoutFacade;

    private TargetCartService targetCartService;

    private TargetDeliveryService targetDeliveryService;

    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Override
    public boolean isCartPrePopulatedForCheckout() {
        if (!targetFeatureSwitchFacade.shouldPrepopulateCheckoutFields()) {
            return false;
        }
        final CartModel cartModel = targetCheckoutFacade.getCart();
        boolean isPopulated = false;
        if (cartModel.getDeliveryAddress() == null && cartModel.getPaymentMode() == null) {
            if (!isUsingKiosk() && !targetUserFacade.isAnonymousUser()) {
                if (cartModel.getUser() instanceof TargetCustomerModel) {
                    final TargetCustomerModel customer = (TargetCustomerModel)cartModel.getUser();
                    final OrderModel previousOrder = targetOrderService.findLatestOrderForUser(customer);
                    final TargetZoneDeliveryModeModel preferredDeliveryMode = getPreferredDeliveryMode(customer,
                            previousOrder);
                    if (isPreferredDeliveryModeEligible(cartModel, preferredDeliveryMode)
                            && targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                                    preferredDeliveryMode)) {
                        boolean restrictionDeliveryValueAvailable = false;
                        try {
                            restrictionDeliveryValueAvailable = CollectionUtils.isNotEmpty(targetDeliveryService
                                    .getAllApplicableDeliveryValuesForModeAndOrder(preferredDeliveryMode,
                                            cartModel));
                        }
                        catch (final TargetNoPostCodeException exception) {
                            restrictionDeliveryValueAvailable = true;
                        }
                        if (restrictionDeliveryValueAvailable) {
                            targetCartService.prePopulateCart(cartModel, preferredDeliveryMode, customer,
                                    null, previousOrder);
                            isPopulated = true;
                            targetCheckoutFacade.setThreatMatrixSessionID(cartModel);
                            targetCheckoutFacade.recalculateCart();
                        }
                    }
                }
            }
        }
        return isPopulated;
    }

    /**
     * Method to evaluate if preferred delivery mode can be used for expedited checkout.
     * 
     * @param cartModel
     * @param preferredDeliveryMode
     * @return boolean
     */
    private boolean isPreferredDeliveryModeEligible(final CartModel cartModel,
            final TargetZoneDeliveryModeModel preferredDeliveryMode) {
        if (preferredDeliveryMode != null
                && canSelectedDeliveryModeBeExpedited(cartModel, preferredDeliveryMode)
                && BooleanUtils.isTrue(preferredDeliveryMode.getIsEligibleForSinglePageCheckout())
                && BooleanUtils.isTrue(preferredDeliveryMode.getActive())) {
            return true;
        }
        return false;
    }

    /**
     * Method to check if delivery mode is allowed for expedited checkout, if cart doesnt have delivery mode set then it
     * can be expedited or if it is set it should match preferred delivery mode.
     * 
     * @param cartModel
     * @param preferredDeliveryMode
     * @return boolean
     */
    private boolean canSelectedDeliveryModeBeExpedited(final CartModel cartModel,
            final TargetZoneDeliveryModeModel preferredDeliveryMode) {
        if (cartModel.getDeliveryMode() == null
                || StringUtils.equalsIgnoreCase(cartModel.getDeliveryMode().getCode(),
                        preferredDeliveryMode.getCode())) {
            return true;
        }
        return false;
    }

    @Override
    public TargetZoneDeliveryModeModel getPreferredDeliveryMode(final TargetCustomerModel customer,
            final OrderModel previousOrder) {
        if (customer.getPreferredDeliveryMode() != null) {
            return customer.getPreferredDeliveryMode();
        }
        else if (previousOrder != null) {
            if (previousOrder.getDeliveryMode() instanceof TargetZoneDeliveryModeModel) {
                return (TargetZoneDeliveryModeModel)previousOrder.getDeliveryMode();
            }
        }
        return null;
    }


    /**
     * Method to check if user is accessing from Kiosk sales application
     * 
     * @return boolean
     */
    private boolean isUsingKiosk() {
        return SalesApplication.KIOSK.toString().equalsIgnoreCase(
                targetSalesApplicationService.getCurrentSalesApplication().getCode());
    }

    /**
     * @param targetSalesApplicationService
     *            the targetSalesApplicationService to set
     */
    @Required
    public void setTargetSalesApplicationService(final TargetSalesApplicationService targetSalesApplicationService) {
        this.targetSalesApplicationService = targetSalesApplicationService;
    }

    /**
     * @param targetUserFacade
     *            the targetUserFacade to set
     */
    @Required
    public void setTargetUserFacade(final TargetUserFacade targetUserFacade) {
        this.targetUserFacade = targetUserFacade;
    }

    /**
     * @param targetOrderService
     *            the targetOrderService to set
     */
    @Required
    public void setTargetOrderService(final TargetOrderService targetOrderService) {
        this.targetOrderService = targetOrderService;
    }


    /**
     * @param targetCheckoutFacade
     *            the targetCheckoutFacade to set
     */
    @Required
    public void setTargetCheckoutFacade(final TargetCheckoutFacade targetCheckoutFacade) {
        this.targetCheckoutFacade = targetCheckoutFacade;
    }

    /**
     * @param targetDeliveryModeHelper
     *            the targetDeliveryHelper to set
     */
    @Required
    public void setTargetDeliveryModeHelper(final TargetDeliveryModeHelper targetDeliveryModeHelper) {
        this.targetDeliveryModeHelper = targetDeliveryModeHelper;
    }

    /**
     * @param targetCartService
     *            the targetCartService to set
     */
    @Required
    public void setTargetCartService(final TargetCartService targetCartService) {
        this.targetCartService = targetCartService;
    }

    /**
     * @param targetDeliveryService
     *            the targetDeliveryService to set
     */
    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        this.targetDeliveryService = targetDeliveryService;
    }

    /**
     * @param targetFeatureSwitchFacade
     *            the targetFeatureSwitchFacade to set
     */
    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }

}

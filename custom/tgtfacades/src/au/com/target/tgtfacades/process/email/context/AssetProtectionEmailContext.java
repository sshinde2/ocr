/**
 * 
 */
package au.com.target.tgtfacades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import org.springframework.beans.factory.annotation.Required;


/**
 * Extend OrderNotificationEmailContext to override the TO address with Asset Protection details
 * 
 */
public class AssetProtectionEmailContext extends OrderNotificationEmailContext {

    private String assetProtectionDisplayName;
    private String assetProtectionEmail;


    @Override
    public void init(final BusinessProcessModel businessProcessModel, final EmailPageModel emailPageModel)
    {
        super.init(businessProcessModel, emailPageModel);

        // Instead of the customer details, use the configured name and email for Asset Protection team
        put(TITLE, "");
        put(DISPLAY_NAME, assetProtectionDisplayName);
        put(EMAIL, assetProtectionEmail);
    }


    /**
     * @param assetProtectionDisplayName
     *            the assetProtectionDisplayName to set
     */
    @Required
    public void setAssetProtectionDisplayName(final String assetProtectionDisplayName) {
        this.assetProtectionDisplayName = assetProtectionDisplayName;
    }


    /**
     * @param assetProtectionEmail
     *            the assetProtectionEmail to set
     */
    @Required
    public void setAssetProtectionEmail(final String assetProtectionEmail) {
        this.assetProtectionEmail = assetProtectionEmail;
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.process.email.context;


import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.velocity.tools.generic.DateTool;
import org.springframework.util.Assert;

import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.process.email.data.ClickAndCollectEmailData;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtfacades.user.data.TargetAddressData;


/**
 * 
 * Email context for cnc notice emails, with tax invoice.
 * 
 * @author bhuang3
 * 
 */
public class CncNotificationEmailContext extends TaxInvoiceOrderEmailContext {

    private ClickAndCollectEmailData cncNotificationData;
    private List<ConsignmentData> sentDigitalConsignments;
    private List<ConsignmentData> shippedConsignments;
    private DateTool dateTool;

    @Override
    public void init(final BusinessProcessModel businessProcessModel, final EmailPageModel emailPageModel) {
        super.init(businessProcessModel, emailPageModel);
        dateTool = new DateTool();
        if (businessProcessModel instanceof OrderProcessModel) {
            cncNotificationData = (ClickAndCollectEmailData)getOrderProcessParameterHelper().getCncNotificationData(
                    (OrderProcessModel)businessProcessModel);
            Assert.notNull(cncNotificationData, "targetPointOfServiceData cannot be null");

            final Integer storeNumber = getOrder().getCncStoreNumber();
            Assert.notNull(cncNotificationData.getStoreNumber(), "store number cannot be null");

            final int cncStoreNumber = Integer.parseInt(cncNotificationData.getStoreNumber());
            //replace the selectedStore in the super class with the new data if storeNumbers are different. Also replace the addressData in orderData in super class
            if (storeNumber != null && cncStoreNumber != storeNumber.intValue()) {
                final TargetPointOfServiceData selectedStore = getTargetStoreLocatorFacade().getPointOfService(
                        Integer.valueOf(cncStoreNumber));
                Assert.notNull(selectedStore, "targetPointOfServiceData cannot be null");
                setSelectedStore(selectedStore);
                final TargetAddressData originalAddress = (TargetAddressData)getOrder().getDeliveryAddress();
                final TargetAddressData newAddress = selectedStore.getTargetAddressData();
                replaceTheDeliveryAddress(originalAddress, newAddress);
            }
            updateShippedConsignmentList(getOrder());
            consolidateConsignmentShippedItems(shippedConsignments);
        }
    }

    private void updateShippedConsignmentList(final TargetOrderData orderData) {

        if (orderData != null && CollectionUtils.isNotEmpty(orderData.getConsignment())) {

            final List<ConsignmentData> consignments = orderData.getConsignment();

            for (final ConsignmentData orderConsignment : consignments) {
                if (orderConsignment != null) {
                    addToShippedOrSentConsignmentList(orderConsignment);
                }
            }
        }
    }

    private void consolidateConsignmentShippedItems(final List<ConsignmentData> shippedConsignments) {

        final Map<String, ConsignmentEntryData> customOrderEntries = new HashMap<>();

        for (final ConsignmentData shippedConsignment : shippedConsignments) {

            final List<ConsignmentEntryData> entries = shippedConsignment.getEntries();

            for (final ConsignmentEntryData entryData : entries) {

                final OrderEntryData orderEntryData = entryData.getOrderEntry();
                final String productCode = orderEntryData.getProduct().getCode();

                if (customOrderEntries.containsKey(productCode)) {

                    final ConsignmentEntryData existingEntry = customOrderEntries.get(productCode);
                    existingEntry.setShippedQuantity(Long.valueOf(
                            existingEntry.getShippedQuantity().longValue()
                                    + entryData.getShippedQuantity().longValue()));
                    continue;
                }
                customOrderEntries.put(productCode, entryData);
            }
        }

        if (!customOrderEntries.isEmpty()) {
            final List<ConsignmentEntryData> consignmentEntryList = new ArrayList<>(customOrderEntries.values());
            final List<ConsignmentData> consignmentDataList = new ArrayList<>();
            final ConsignmentData consignmentData = new ConsignmentData();

            consignmentData.setEntries(consignmentEntryList);
            consignmentDataList.add(consignmentData);
            shippedConsignments.clear();
            shippedConsignments.addAll(consignmentDataList);
        }
    }

    private void addToShippedOrSentConsignmentList(final ConsignmentData consignmentData) {
        if (consignmentData.isIsDigitalDelivery()) {
            if (sentDigitalConsignments == null) {
                sentDigitalConsignments = new ArrayList<>();
            }
            sentDigitalConsignments.add(consignmentData);
        }
        else if (ConsignmentStatus.SHIPPED.equals(consignmentData.getStatus())) {
            if (shippedConsignments == null) {
                shippedConsignments = new ArrayList<>();
            }

            shippedConsignments.add(consignmentData);
        }
    }

    /**
     * replace the delivery address data in the orderData with new value for email template.
     * 
     * @param originalAddress
     * @param newAddress
     */
    private void replaceTheDeliveryAddress(final TargetAddressData originalAddress,
            final TargetAddressData newAddress) {
        if (originalAddress != null && newAddress != null) {
            originalAddress.setLine1(newAddress.getLine1());
            originalAddress.setLine2(newAddress.getLine2());
            originalAddress.setTown(newAddress.getTown());
            originalAddress.setState(newAddress.getState());
            originalAddress.setPostalCode(newAddress.getPostalCode());
        }

    }

    /**
     * @return the dateTool
     */
    public DateTool getDateTool() {
        return dateTool;
    }

    /**
     * @return the cncNotificationData
     */
    public ClickAndCollectEmailData getCncNotificationData() {
        return cncNotificationData;
    }

    /**
     * @param cncNotificationData
     *            the cncNotificationData to set
     */
    public void setCncNotificationData(final ClickAndCollectEmailData cncNotificationData) {
        this.cncNotificationData = cncNotificationData;
    }


    /**
     * @return the shippedConsignments
     */
    public List<ConsignmentData> getShippedConsignments() {
        return shippedConsignments;
    }

    /**
     * @return the sentDigitalConsignments
     */
    public List<ConsignmentData> getSentDigitalConsignments() {
        return sentDigitalConsignments;
    }

}

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtfacades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontProcessModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.email.context.AbstractTargetEmailContext;
import au.com.target.tgtcore.model.TargetCustomerModel;


/**
 * Velocity context for a customer email.
 */
public class CustomerEmailContext extends AbstractTargetEmailContext
{
    private Converter<UserModel, CustomerData> customerConverter;
    private CustomerData customerData;

    @Override
    public void init(final BusinessProcessModel businessProcessModel, final EmailPageModel emailPageModel)
    {
        super.init(businessProcessModel, emailPageModel);
        final CustomerModel customer = getCustomer(businessProcessModel);
        if (customer != null)
        {
            customerData = getCustomerConverter().convert(customer);

            if (TargetCustomerModel.FIRSTNAME.equals(getCustomer().getFirstName()))
            {
                put(DISPLAY_NAME, StringUtils.EMPTY);
            }
            else
            {
                put(DISPLAY_NAME, getCustomer().getFirstName());
            }
        }
    }

    @Override
    protected BaseSiteModel getSite(final BusinessProcessModel businessProcessModel)
    {
        if (businessProcessModel instanceof StoreFrontProcessModel)
        {
            return ((StoreFrontProcessModel)businessProcessModel).getSite();
        }

        return null;
    }

    @Override
    protected CustomerModel getCustomer(final BusinessProcessModel businessProcessModel)
    {
        if (businessProcessModel instanceof StoreFrontCustomerProcessModel)
        {
            return ((StoreFrontCustomerProcessModel)businessProcessModel).getCustomer();
        }

        return null;
    }

    protected Converter<UserModel, CustomerData> getCustomerConverter()
    {
        return customerConverter;
    }

    @Required
    public void setCustomerConverter(final Converter<UserModel, CustomerData> customerConverter)
    {
        this.customerConverter = customerConverter;
    }

    public CustomerData getCustomer()
    {
        return customerData;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#getEmailLanguage(de.hybris.platform.processengine.model.BusinessProcessModel)
     */
    @Override
    protected LanguageModel getEmailLanguage(final BusinessProcessModel businessProcessModel) {
        // YTODO Auto-generated method stub
        return null;
    }
}

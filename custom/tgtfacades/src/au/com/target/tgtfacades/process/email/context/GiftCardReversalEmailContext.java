/**
 * 
 */
package au.com.target.tgtfacades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontProcessModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtpayment.dto.GiftCardReversalData;


/**
 * @author htan3
 *
 */
public class GiftCardReversalEmailContext extends CustomerEmailContext {

    private GiftCardReversalData reversalData;

    @Override
    public void init(final BusinessProcessModel businessProcessModel, final EmailPageModel emailPageModel)
    {
        super.init(businessProcessModel, emailPageModel);
        final GiftCardReversalData giftCardReversalData = (GiftCardReversalData)getOrderProcessParameterHelper()
                .getGiftCardReversalData(businessProcessModel);
        if (giftCardReversalData != null) {
            setReversalData(giftCardReversalData);
            this.put(EMAIL, reversalData.getCustomerEmail());
        }
    }

    /**
     * @return the orderProcessParameterHelper
     */
    public OrderProcessParameterHelper getOrderProcessParameterHelper() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getOrderProcessParameterHelper().");
    }

    /**
     * @return the reversalData
     */
    public GiftCardReversalData getReversalData() {
        return reversalData;
    }

    /**
     * @param reversalData
     *            the reversalData to set
     */
    public void setReversalData(final GiftCardReversalData reversalData) {

        this.reversalData = reversalData;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#getSite(de.hybris.platform.processengine.model.BusinessProcessModel)
     */
    @Override
    protected BaseSiteModel getSite(final BusinessProcessModel businessProcessModel) {
        if (businessProcessModel instanceof StoreFrontProcessModel) {
            return ((StoreFrontProcessModel)businessProcessModel).getSite();
        }
        return null;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#getCustomer(de.hybris.platform.processengine.model.BusinessProcessModel)
     */
    @Override
    protected CustomerModel getCustomer(final BusinessProcessModel businessProcessModel) {
        // YTODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#getEmailLanguage(de.hybris.platform.processengine.model.BusinessProcessModel)
     */
    @Override
    protected LanguageModel getEmailLanguage(final BusinessProcessModel businessProcessModel) {
        // YTODO Auto-generated method stub
        return null;
    }
}

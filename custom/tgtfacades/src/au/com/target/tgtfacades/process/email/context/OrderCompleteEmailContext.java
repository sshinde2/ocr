package au.com.target.tgtfacades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.velocity.tools.generic.DateTool;

import au.com.target.tgtfacades.order.data.TargetOrderData;

public class OrderCompleteEmailContext extends TaxInvoiceOrderEmailContext {

	private List<ConsignmentData> sentDigitalConsignments;
	private List<ConsignmentData> shippedConsignments;
	private DateTool dateTool;


	@Override
	public void init(final BusinessProcessModel businessProcessModel, final EmailPageModel emailPageModel) {

		super.init(businessProcessModel, emailPageModel);
		dateTool = new DateTool();
		if (businessProcessModel instanceof OrderProcessModel) {
			updateShippedConsignmentList(getOrder());
		}

	}

	private void updateShippedConsignmentList(final TargetOrderData orderData) {
		if (orderData != null && CollectionUtils.isNotEmpty(orderData.getConsignment())) {
			final List<ConsignmentData> consignments = orderData.getConsignment();

			for (final ConsignmentData orderConsignment : consignments) {
				if (orderConsignment != null) {
					addToShippedOrSentConsignmentList(orderConsignment);
				}
			}
		}
	}

	private void addToShippedOrSentConsignmentList(final ConsignmentData consignmentData) {
		if (consignmentData.isIsDigitalDelivery()) {
			if (sentDigitalConsignments == null) {
				sentDigitalConsignments = new ArrayList<>();
			}
			sentDigitalConsignments.add(consignmentData);
		} else if (ConsignmentStatus.SHIPPED.equals(consignmentData.getStatus())) {
			if (shippedConsignments == null) {
				shippedConsignments = new ArrayList<>();
			}
			shippedConsignments.add(consignmentData);
		}
	}

	/**
	 * @return the dateTool
	 */
	public DateTool getDateTool() {
		return dateTool;
	}


	/**
	 * @return the shippedConsignments
	 */
	public List<ConsignmentData> getShippedConsignments() {
		return shippedConsignments;
	}

	/**
	 * @return the sentDigitalConsignments
	 */
	public List<ConsignmentData> getSentDigitalConsignments() {
		return sentDigitalConsignments;
	}


}


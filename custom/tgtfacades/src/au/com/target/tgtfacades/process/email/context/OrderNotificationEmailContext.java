/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtfacades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.text.DecimalFormat;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


/**
 * Velocity context for a order notification email.
 */
public class OrderNotificationEmailContext extends CustomerEmailContext {
    private static final Logger LOG = Logger.getLogger(OrderNotificationEmailContext.class);
    private static final String DATE_DISPLAY_PATTERN = "dd/MM/yyyy H:mm";
    private static final DateTimeFormatter DATE_DISPLAY_FORMATTER = DateTimeFormat.forPattern(DATE_DISPLAY_PATTERN);
    private static final DecimalFormat COMMA_FORMAT = new DecimalFormat("#,###");
    private TargetDeliveryModeHelper targetDeliveryModeHelper;
    private TargetStoreLocatorFacade targetStoreLocatorFacade;
    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;
    private Converter<OrderModel, TargetOrderData> orderConverter;
    private TargetOrderData orderData;
    private TargetPointOfServiceData selectedStore;
    private OrderProcessParameterHelper orderProcessParameterHelper;
    private Boolean isShortDeliveryTime;
    private int quantitiesInOrder;

    @Override
    public void init(final BusinessProcessModel businessProcessModel, final EmailPageModel emailPageModel) {
        super.init(businessProcessModel, emailPageModel);
        if (businessProcessModel instanceof OrderProcessModel) {
            final OrderModel orderModel = ((OrderProcessModel)businessProcessModel).getOrder();
            orderData = getOrderConverter().convert(orderModel);

            if (targetDeliveryModeHelper.isDeliveryModeStoreDelivery(orderModel.getDeliveryMode())) {
                final Integer storeNumber = orderData.getCncStoreNumber();

                if (storeNumber != null) {
                    selectedStore = getTargetStoreLocatorFacade().getPointOfService(storeNumber);
                }
            }
            if (orderModel.getDeliveryMode() instanceof TargetZoneDeliveryModeModel) {
                final Boolean shortDelivery = ((TargetZoneDeliveryModeModel)orderModel.getDeliveryMode())
                        .getIsShortDeliveryTime();
                isShortDeliveryTime = (shortDelivery != null ? shortDelivery : Boolean.FALSE);
            }
        }
    }

    @Override
    protected BaseSiteModel getSite(final BusinessProcessModel businessProcessModel) {
        if (businessProcessModel instanceof OrderProcessModel) {
            return ((OrderProcessModel)businessProcessModel).getOrder().getSite();
        }

        return null;
    }

    @Override
    protected CustomerModel getCustomer(final BusinessProcessModel businessProcessModel) {
        if (businessProcessModel instanceof OrderProcessModel) {
            return (CustomerModel)((OrderProcessModel)businessProcessModel).getOrder().getUser();
        }

        return null;
    }

    public TargetOrderData getOrder() {
        return orderData;
    }

    /**
     * @return the selectedStore
     */
    public TargetPointOfServiceData getSelectedStore() {
        return selectedStore;
    }



    /**
     * @param selectedStore
     *            the selectedStore to set
     */
    public void setSelectedStore(final TargetPointOfServiceData selectedStore) {
        this.selectedStore = selectedStore;
    }

    public String getPurchaseOptionName() {
        final PurchaseOptionModel purchaseOption = targetPurchaseOptionHelper.getPurchaseOptionModel(getOrder()
                .getPurchaseOptionCode());

        return purchaseOption.getName();
    }

    public String getOrderDateDisplay() {
        final DateTime orderCreatedDate = new DateTime(getOrder().getCreated());
        return DATE_DISPLAY_FORMATTER.print(orderCreatedDate) + " (GMT+10)";
    }

    /**
     * @return the targetDeliveryModeHelper
     */
    protected TargetDeliveryModeHelper getTargetDeliveryModeHelper() {
        return targetDeliveryModeHelper;
    }

    /**
     * @param targetDeliveryModeHelper
     *            the targetDeliveryModeHelper to set
     */
    @Required
    public void setTargetDeliveryModeHelper(final TargetDeliveryModeHelper targetDeliveryModeHelper) {
        this.targetDeliveryModeHelper = targetDeliveryModeHelper;
    }

    /**
     * @return the targetStoreLocatorFacade
     */
    protected TargetStoreLocatorFacade getTargetStoreLocatorFacade() {
        return targetStoreLocatorFacade;
    }

    /**
     * @param targetStoreLocatorFacade
     *            the targetStoreLocatorFacade to set
     */
    @Required
    public void setTargetStoreLocatorFacade(final TargetStoreLocatorFacade targetStoreLocatorFacade) {
        this.targetStoreLocatorFacade = targetStoreLocatorFacade;
    }

    /**
     * @return the targetPurchaseOptionHelper
     */
    protected TargetPurchaseOptionHelper getTargetPurchaseOptionHelper() {
        return targetPurchaseOptionHelper;
    }

    /**
     * @param targetPurchaseOptionHelper
     *            the targetPurchaseOptionHelper to set
     */
    @Required
    public void setTargetPurchaseOptionHelper(final TargetPurchaseOptionHelper targetPurchaseOptionHelper) {
        this.targetPurchaseOptionHelper = targetPurchaseOptionHelper;
    }



    protected Converter<OrderModel, TargetOrderData> getOrderConverter() {
        return orderConverter;
    }

    @Required
    public void setOrderConverter(final Converter<OrderModel, TargetOrderData> orderConverter) {
        this.orderConverter = orderConverter;
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }

    /**
     * @return the orderProcessParameterHelper
     */
    public OrderProcessParameterHelper getOrderProcessParameterHelper() {
        return orderProcessParameterHelper;
    }

    public String getFormatedFlybuysPoints() {
        String result = "0";
        if (orderData.getFlybuysDiscountData() != null) {
            final Integer consumedPoint = orderData.getFlybuysDiscountData().getPointsConsumed();
            if (consumedPoint != null) {
                try {
                    result = COMMA_FORMAT.format(consumedPoint);
                }
                catch (final IllegalArgumentException e) {
                    LOG.error("Failed to format the flybuys consumed point", e);
                    return consumedPoint.toString();
                }
            }
        }
        return result;
    }

    /**
     * @return the isShortDelivery
     */
    public Boolean getIsShortDeliveryTime() {
        return isShortDeliveryTime;
    }

    /**
     * @return the quantitiesInOrder
     */
    public int getQuantitiesInOrder() {
        quantitiesInOrder = 0;
        if (CollectionUtils.isNotEmpty(orderData.getEntries())) {
            for (final OrderEntryData entryData : orderData.getEntries()) {
                quantitiesInOrder += entryData.getQuantity().intValue();
            }
        }

        return quantitiesInOrder;
    }


}

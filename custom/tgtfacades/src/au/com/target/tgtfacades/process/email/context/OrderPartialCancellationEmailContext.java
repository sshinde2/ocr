/**
 *
 */
package au.com.target.tgtfacades.process.email.context;

import static java.util.Collections.sort;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.order.data.OrderModificationData;
import au.com.target.tgtfacades.order.data.TargetOrderData;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

/**
 * Context for order cancellation emails
 *
 */
public class OrderPartialCancellationEmailContext extends OrderShippingNoticeEmailContext {

    private static final Logger LOG = Logger.getLogger(OrderPartialCancellationEmailContext.class);
    private OrderModificationDataHelper orderModificationDataHelper;
    private List<OrderModificationData> currentlyCancelledItems;
    private PriceData refundAmount;
    private PriceData deliveryRefundAmount;

    @Override
    public void init(final BusinessProcessModel businessProcessModel, final EmailPageModel emailPageModel) {
        super.init(businessProcessModel, emailPageModel);

        final List<OrderModificationData> cancelledProducts = getOrder().getCancelledProducts();
        if (CollectionUtils.isNotEmpty(cancelledProducts)) {
            if(CollectionUtils.size(cancelledProducts) > 1) {
                sort(getOrder().getCancelledProducts(), new Comparator<OrderModificationData>() {
                    @Override
                    public int compare(final OrderModificationData c1, final OrderModificationData c2) {
                        if (c1.getDate() == null || c2.getDate() == null) {
                            return 0;
                        } else {
                            return c2.getDate().compareTo(c1.getDate());
                        }
                    }
                });
            }
            currentlyCancelledItems = new ArrayList<>();
            currentlyCancelledItems.add(cancelledProducts.get(0));
            getOrder().getCancelledProducts().remove(0);
        }

        populateCurrentlyCancelledItemDetails(businessProcessModel);

    }



    /**
     * Update the shipped consignments only for non-cnc store orders
     * @param  orderData
     */
    @Override
    protected void updateShippedConsignmentList(final TargetOrderData orderData,
            final ConsignmentData currentConsignment) {
        super.updateShippedConsignmentList(getOrder(), null);
        if (getOrder().getCncStoreNumber() != null && CollectionUtils.isNotEmpty(getShippedConsignments())) {
            getShippedConsignments().clear();
        }
    }



    private void populateCurrentlyCancelledItemDetails(final BusinessProcessModel businessProcessModel) {

        final OrderModificationRecordEntryModel orderCancelRecordEntry = getOrderProcessParameterHelper()
                .getOrderCancelRequest((OrderProcessModel) businessProcessModel);

        if (orderCancelRecordEntry != null) {
            final CurrencyModel currency = orderModificationDataHelper.getCurrency(orderCancelRecordEntry);

            if (currency == null) {
                LOG.warn("currency is null for process : " + businessProcessModel.getCode());
            }
            refundAmount = orderModificationDataHelper.readRefundAmount((OrderProcessModel) businessProcessModel,
                    currency);
            deliveryRefundAmount = orderModificationDataHelper.readDeliveryRefundAmount(orderCancelRecordEntry,
                    currency);
        }

    }


    /**
     * @param currentlyCancelledItems
     *            the currentlyCancelledItems to set
     */

    public void setCurrentlyCancelledItems(final List<OrderModificationData> currentlyCancelledItems) {
        this.currentlyCancelledItems = currentlyCancelledItems;
    }
    /**
     * @return the currentlyCancelledItems
     */
    public List<OrderModificationData> getCurrentlyCancelledItems() {
        return currentlyCancelledItems;
    }

    /**
     * @return the refundAmount
     */
    public PriceData getRefundAmount() {
        return refundAmount;
    }

    /**
     * @return the deliveryRefundAmount
     */
    public PriceData getDeliveryRefundAmount() {
        return deliveryRefundAmount;
    }

    /**
     * @param orderModificationDataHelper
     *            the orderModificationDataHelper to set
     */
    @Required
    public void setOrderModificationDataHelper(final OrderModificationDataHelper orderModificationDataHelper) {
        this.orderModificationDataHelper = orderModificationDataHelper;
    }

}
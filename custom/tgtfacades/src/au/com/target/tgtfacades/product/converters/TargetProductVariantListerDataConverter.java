/**
 * 
 */
package au.com.target.tgtfacades.product.converters;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.fest.assertions.Assertions;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;


/**
 * Converter for TargetProductVariantListerDataConverter
 * 
 */
public class TargetProductVariantListerDataConverter
        extends AbstractPopulatingConverter<VariantProductModel, TargetVariantProductListerData> {

    private Map<String, List<Populator<VariantProductModel, TargetVariantProductListerData>>> variantSpecificPopulator;

    private List<Populator<VariantProductModel, TargetVariantProductListerData>> genericVariantListerPopulators;

    @Override
    public void populate(final VariantProductModel source, final TargetVariantProductListerData target) {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        final List<TargetVariantProductListerData> variantsList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(source.getVariants())) {
            for (final VariantProductModel variant : source.getVariants()) {
                final TargetVariantProductListerData targetVariantProductListerData = new TargetVariantProductListerData();
                populate(variant, targetVariantProductListerData);
                variantsList.add(targetVariantProductListerData);
            }
        }
        //setting the child variants which got populated above.
        target.setVariants(variantsList);
        addPopulators(source.getItemtype());
        super.populate(source, target);
    }

    private void addPopulators(final String itemType) {
        Assertions.assertThat(itemType).isNotNull();
        final List<Populator<VariantProductModel, TargetVariantProductListerData>> populators = new ArrayList<Populator<VariantProductModel, TargetVariantProductListerData>>();
        populators.addAll(genericVariantListerPopulators);
        populators.addAll(variantSpecificPopulator.get(itemType));
        setPopulators(populators);
    }

    /**
     * @param variantSpecificPopulator
     *            the variantSpecificPopulator to set
     */
    @Required
    public void setVariantSpecificPopulator(
            final Map<String, List<Populator<VariantProductModel, TargetVariantProductListerData>>> variantSpecificPopulator) {
        this.variantSpecificPopulator = variantSpecificPopulator;
    }

    /**
     * @param genericVariantListerPopulators
     *            the genericVariantListerPopulators to set
     */
    @Required
    public void setGenericVariantListerPopulators(
            final List<Populator<VariantProductModel, TargetVariantProductListerData>> genericVariantListerPopulators) {
        this.genericVariantListerPopulators = genericVariantListerPopulators;
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.product.converters.populator;


import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtfacades.product.data.BrandData;


/**
 * @author ragarwa3
 *
 */
public class TargetBrandPopulator implements Populator<BrandModel, BrandData> {

    @Override
    public void populate(final BrandModel source, final BrandData target) throws ConversionException {

        Assert.notNull(source, "Brand can't be null");

        target.setCode(source.getCode());
        target.setName(source.getName());
    }

}

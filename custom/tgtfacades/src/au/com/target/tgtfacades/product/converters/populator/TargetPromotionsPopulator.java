/**
 *
 */
package au.com.target.tgtfacades.product.converters.populator;

import de.hybris.platform.commercefacades.product.converters.populator.PromotionsPopulator;
import de.hybris.platform.promotions.jalo.AbstractPromotion;
import de.hybris.platform.promotions.jalo.ProductPromotion;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.jalo.TargetDealWithRewardResult;
import de.hybris.platform.promotions.result.PromotionOrderResults;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * @author mjanarth
 *
 */
public class TargetPromotionsPopulator extends PromotionsPopulator {


    @Override
    protected List<String> getFiredPromotionsMessages(final PromotionOrderResults promoOrderResults,
            final AbstractPromotion promotion)
    {
        final List<String> descriptions = new LinkedList<>();

        final List<PromotionResult> list = new ArrayList<>();
        for (final PromotionResult result : promoOrderResults.getFiredProductPromotions()) {

            if (!(result instanceof TargetDealWithRewardResult)) {
                list.add(result);
            }
            else {

                final TargetDealWithRewardResult rewardResult = (TargetDealWithRewardResult)result;
                if (!rewardResult.getCouldhaveMoreRewards()) {
                    list.add(result);
                }
            }
        }
        if (promotion instanceof ProductPromotion)
        {
            addDescriptions(descriptions, filter(list, promotion));
        }
        else
        {
            addDescriptions(descriptions, filter(promoOrderResults.getFiredOrderPromotions(), promotion));
        }

        return descriptions;
    }

    @Override
    protected List<String> getCouldFirePromotionsMessages(final PromotionOrderResults promoOrderResults,
            final AbstractPromotion promotion) {

        final List<String> descriptions = new LinkedList<>();

        final List<PromotionResult> list = new ArrayList<>();
        for (final PromotionResult result : promoOrderResults.getAllProductPromotions()) {

            if (result.getCouldFire()) {

                list.add(result);
            }
            else if (result instanceof TargetDealWithRewardResult) {

                final TargetDealWithRewardResult rewardResult = (TargetDealWithRewardResult)result;
                if (rewardResult.getCouldhaveMoreRewards()) {
                    list.add(result);
                }
            }
        }
        if (promotion instanceof ProductPromotion) {
            addDescriptions(descriptions, filter(list, promotion));
        }
        else {
            addDescriptions(descriptions, filter(promoOrderResults.getPotentialOrderPromotions(), promotion));

        }
        return descriptions;
    }




}

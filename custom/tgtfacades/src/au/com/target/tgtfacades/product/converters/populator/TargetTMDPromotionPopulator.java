/**
 * 
 */
package au.com.target.tgtfacades.product.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import au.com.target.tgtcore.model.TMDiscountProductPromotionModel;
import au.com.target.tgtcore.model.TMDiscountPromotionModel;
import au.com.target.tgtfacades.product.data.TargetPromotionData;


/**
 * @author rmcalave
 * 
 */
public class TargetTMDPromotionPopulator<SOURCE extends ProductPromotionModel, TARGET extends TargetPromotionData>
        implements Populator<SOURCE, TARGET> {

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final SOURCE source, final TARGET target) throws ConversionException {
        target.setTmdPromotion(source instanceof TMDiscountPromotionModel
                || source instanceof TMDiscountProductPromotionModel);
    }

}

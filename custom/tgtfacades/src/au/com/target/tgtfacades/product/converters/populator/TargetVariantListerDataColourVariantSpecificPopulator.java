/**
 *
 */
package au.com.target.tgtfacades.product.converters.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.ColourModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.order.TargetPreOrderService;
import au.com.target.tgtcore.product.data.ProductDisplayType;
import au.com.target.tgtfacades.converters.populator.AbstractTargetVariantProductPopulator;
import au.com.target.tgtfacades.converters.populator.TargetProductDeliveryZonePopulator;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.product.data.enums.TargetPromotionStatusEnum;


/**
 * The Class TargetVariantListerDataColourVariantSpecificPopulator.
 *
 * @author pthoma20
 */
public class TargetVariantListerDataColourVariantSpecificPopulator<SOURCE extends ProductModel, TARGET extends ProductData>
        extends AbstractTargetVariantProductPopulator<SOURCE, TARGET> {


    private TargetPreOrderService targetPreOrderService;
    private TargetProductDeliveryZonePopulator targetProductDeliveryZonePopulator;




    /* (non-Javadoc)
     * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Obsject)
     */
    @Override
    public void populate(final SOURCE source, final TARGET target)
            throws ConversionException {
        if (!(target instanceof TargetVariantProductListerData)
                || !(source instanceof TargetColourVariantProductModel)) {
            return;
        }
        final TargetVariantProductListerData targetData = (TargetVariantProductListerData)target;
        final TargetColourVariantProductModel sourceModel = (TargetColourVariantProductModel)source;
        targetData.setAssorted(Boolean.valueOf(sourceModel.isAssorted()));
        targetData.setColourName(sourceModel.getColourName());
        targetData.setExcludeForAfterpay(sourceModel.isExcludeForAfterpay());
        targetData.setExcludeForZipPayment(sourceModel.isExcludeForZipPayment());

        final List<String> promotionClassesList = new ArrayList<>();
        for (final TargetPromotionStatusEnum targetPromotionStatusEnum : getPromotionStatuses(sourceModel)) {
            promotionClassesList.add(targetPromotionStatusEnum.getPromotionClass());
        }
        //Populates the product display type
        targetData.setNormalSaleStartDate(sourceModel.getNormalSaleStartDateTime());
        final ProductDisplayType productDisplayType = targetPreOrderService.getProductDisplayType(sourceModel);
        targetData.setProductDisplayType(productDisplayType);
        if (ProductDisplayType.PREORDER_AVAILABLE.equals(productDisplayType)) {
            targetProductDeliveryZonePopulator.populate(sourceModel, targetData);
        }
        targetData.setPromotionStatusClass(promotionClassesList);
        final ColourModel colourModel = sourceModel.getSwatch();
        if (colourModel != null && BooleanUtils.isTrue(colourModel.getDisplay())) {
            targetData.setSwatchColour(sourceModel.getSwatchName());
        }
    }

    /**
     * @param targetPreOrderService
     *            the targetPreOrderService to set
     */
    @Required
    public void setTargetPreOrderService(final TargetPreOrderService targetPreOrderService) {
        this.targetPreOrderService = targetPreOrderService;
    }


    /**
     * @param targetProductDeliveryZonePopulator
     *            the targetProductDeliveryZonePopulator to set
     */
    public void setTargetProductDeliveryZonePopulator(
            final TargetProductDeliveryZonePopulator targetProductDeliveryZonePopulator) {
        this.targetProductDeliveryZonePopulator = targetProductDeliveryZonePopulator;
    }


}
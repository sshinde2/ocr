/**
 *
 */
package au.com.target.tgtfacades.product.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;


/**
 * The Class TargetVariantListerDataSizeVariantSpecificPopulator.
 *
 * @author pthoma20
 */
public class TargetVariantListerDataSizeVariantSpecificPopulator implements
        Populator<TargetSizeVariantProductModel, TargetVariantProductListerData> {


    /* (non-Javadoc)
     * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final TargetSizeVariantProductModel source, final TargetVariantProductListerData target)
            throws ConversionException {
        target.setSize(source.getSize());
        target.setSizePosition(
                source.getProductSize() != null ? source.getProductSize().getPosition() : Integer.valueOf(0));
        target.setSizeType(source.getSizeType());
    }
}
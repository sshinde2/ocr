/**
 *
 */
package au.com.target.tgtfacades.product.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.price.TargetCommercePriceService;
import au.com.target.tgtcore.price.data.PriceRangeInformation;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.util.PriceFormatUtils;
import au.com.target.tgtfacades.util.TargetPriceHelper;
import au.com.target.tgtfacades.util.TargetPriceInformation;


/**
 * The Class TargetVariantProductListerBasicDataPopulator.
 *
 * @author pthoma20
 */
public class TargetVariantProductListerPriceDataPopulator implements
        Populator<AbstractTargetVariantProductModel, TargetVariantProductListerData> {

    private TargetPriceHelper targetPriceHelper;

    private TargetCommercePriceService targetCommercePriceService;

    /* (non-Javadoc)
     * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final AbstractTargetVariantProductModel source, final TargetVariantProductListerData target)
            throws ConversionException {


        final PriceRangeInformation priceRangeInformation = targetCommercePriceService.getPriceRangeInfoForProduct(
                source);

        final TargetPriceInformation targetPriceInformation = targetPriceHelper
                .populatePricesFromRangeInformation(priceRangeInformation);

        if (null != targetPriceInformation.getPriceRangeData()) {
            target.setDisplayPrice(targetPriceHelper.createFormattedPriceRange(
                    targetPriceInformation.getPriceRangeData()));
            target.setPriceRange(targetPriceInformation.getPriceRangeData());
        }
        else if (null != targetPriceInformation.getPriceData()) {
            target.setDisplayPrice(PriceFormatUtils.formatPrice(targetPriceInformation.getPriceData()
                    .getFormattedValue()));
            target.setInstallmentPrice(
                    targetPriceHelper.calculateAfterpayInstallmentPrice(targetPriceInformation.getPriceData()));
            target.setPrice(targetPriceInformation.getPriceData());
        }
        if (null != targetPriceInformation.getWasPriceRangeData()) {
            target.setDisplayWasPrice(targetPriceHelper.createFormattedPriceRange(
                    targetPriceInformation.getWasPriceRangeData()));
        }
        else if (null != targetPriceInformation.getWasPriceData()) {
            target.setDisplayWasPrice(PriceFormatUtils.formatPrice(targetPriceInformation.getWasPriceData()
                    .getFormattedValue()));
        }
    }

    /**
     * @param targetPriceHelper
     *            the targetPriceHelper to set
     */
    @Required
    public void setTargetPriceHelper(final TargetPriceHelper targetPriceHelper) {
        this.targetPriceHelper = targetPriceHelper;
    }

    /**
     * @param targetCommercePriceService
     *            the targetCommercePriceService to set
     */
    @Required
    public void setTargetCommercePriceService(final TargetCommercePriceService targetCommercePriceService) {
        this.targetCommercePriceService = targetCommercePriceService;
    }
}
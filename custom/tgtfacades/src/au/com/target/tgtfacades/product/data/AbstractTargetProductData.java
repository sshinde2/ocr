/**
 * 
 */
package au.com.target.tgtfacades.product.data;

import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.StockData;

import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;

import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.kiosk.product.data.BulkyBoardProductData;
import au.com.target.tgtfacades.product.data.enums.TargetPromotionStatusEnum;


/**
 * @author pthoma20
 * 
 */
public abstract class AbstractTargetProductData extends ProductData {

    private List<TargetZoneDeliveryModeData> deliveryModes;

    private String brand;

    // Promotional Status covers of Essentials, Online Exclusive, Hot Product, Clearance,
    // As confirmed by business this is always equal for a style group.
    private List<TargetPromotionStatusEnum> promotionStatuses;

    //Promotional status class populates essentials, only-at, hot-product, clearance,
    //online-only, new-lower-price.
    private List<String> promotionStatusClass;

    private boolean onlineExclusive;

    //Will be set using the derived values of max price and min price.
    private PriceRangeData priceRange;

    // Will be set using the derived values of max price and min price.
    private PriceRangeData wasPriceRange;

    private PriceData wasPrice;

    private boolean giftCard;

    private boolean showWasPrice;

    private DateTime lastModified;

    private Map<String, Boolean> available;

    private boolean inStock;

    // This is calculated based on whether show when out of stock is set for a child. 
    // Even if one child has a show when out of stock then it will be set as true.
    private boolean showWhenOutOfStock;

    // Bulkyboard data is populated given a store
    private BulkyBoardProductData bulkyBoard;

    // Indicates whether the product could be a bulkyboard product (at any store)
    private boolean bulkyBoardProduct;

    private String dealDescription;

    private List<String> productFeatures;
    private List<String> careInstructions;
    private List<String> materials;
    private String extraInfo;

    private String youTubeLink;
    private String sizeChartURL;

    private boolean allowDeliverToStore;

    private boolean productDeliveryToStoreOnly;

    private String sizeType;
    private String size;
    private boolean sizeChartDisplay;

    private boolean clickAndCollectExcludeTargetCountry;
    private boolean bigAndBulky;

    private PriceData bulkyHomeDeliveryFee;
    private PriceData bulkyCncFreeThreshold;

    private String topLevelCategory;
    private String baseName;

    private String baseProductCode;

    private boolean newArrived;

    private String itemType;

    private String productPromotionalDeliverySticker;
    private String productPromotionalDeliveryText;

    private String productTypeCode;

    private boolean preview;

    private String apn;

    private StockData consolidatedStoreStock;

    private boolean displayOnly;

    private boolean excludeForAfterpay;

    private boolean excludeForZipPayment;

    private PriceData installmentPrice;

    private String originalCategoryCode;

    private String originalCategoryName;

    private String giftCardDeliveryFee;

    private String merchDepartmentCode;

    private String merchDepartmentName;

    /**
     * @return the giftCardDeliveryFee
     */
    public String getGiftCardDeliveryFee() {
        return giftCardDeliveryFee;
    }

    /**
     * @param giftCardDeliveryFee
     *            the giftCardDeliveryFee to set
     */
    public void setGiftCardDeliveryFee(final String giftCardDeliveryFee) {
        this.giftCardDeliveryFee = giftCardDeliveryFee;
    }




    /**
     * @return the consolidatedStoreStock
     */
    public StockData getConsolidatedStoreStock() {
        return consolidatedStoreStock;
    }

    /**
     * @param consolidatedStoreStock
     *            the consolidatedStoreStock to set
     */
    public void setConsolidatedStoreStock(final StockData consolidatedStoreStock) {
        this.consolidatedStoreStock = consolidatedStoreStock;
    }

    /**
     * @return the deliveryModes
     */
    public List<TargetZoneDeliveryModeData> getDeliveryModes() {
        return deliveryModes;
    }

    /**
     * @param deliveryModes
     *            the deliveryModes to set
     */
    public void setDeliveryModes(final List<TargetZoneDeliveryModeData> deliveryModes) {
        this.deliveryModes = deliveryModes;
    }


    /**
     * @return the promotionStatuses
     */

    public List<TargetPromotionStatusEnum> getPromotionStatuses() {
        return promotionStatuses;
    }

    /**
     * @param promotionStatuses
     *            the promotionStatuses to set
     */
    public void setPromotionStatuses(final List<TargetPromotionStatusEnum> promotionStatuses) {
        this.promotionStatuses = promotionStatuses;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand
     *            the brand to set
     */
    public void setBrand(final String brand) {
        this.brand = brand;
    }


    /**
     * @return the priceRange
     */
    @Override
    public PriceRangeData getPriceRange() {
        return priceRange;
    }

    /**
     * @param priceRange
     *            the priceRange to set
     */
    @Override
    public void setPriceRange(final PriceRangeData priceRange) {
        this.priceRange = priceRange;
    }

    /**
     * @return the wasPriceRange
     */
    public PriceRangeData getWasPriceRange() {
        return wasPriceRange;
    }

    /**
     * @param wasPriceRange
     *            the wasPriceRange to set
     */
    public void setWasPriceRange(final PriceRangeData wasPriceRange) {
        this.wasPriceRange = wasPriceRange;
    }

    /**
     * @return the showWasPrice
     */
    public boolean isShowWasPrice() {
        return showWasPrice;
    }

    /**
     * @param showWasPrice
     *            the showWasPrice to set
     */
    public void setShowWasPrice(final boolean showWasPrice) {
        this.showWasPrice = showWasPrice;
    }

    /**
     * @return the wasPrice
     */
    public PriceData getWasPrice() {
        return wasPrice;
    }

    /**
     * @param wasPrice
     *            the wasPrice to set
     */
    public void setWasPrice(final PriceData wasPrice) {
        this.wasPrice = wasPrice;
    }


    /**
     * @return the giftCard
     */
    public boolean isGiftCard() {
        return giftCard;
    }

    /**
     * @param giftCard
     *            the giftCard to set
     */
    public void setGiftCard(final boolean giftCard) {
        this.giftCard = giftCard;
    }

    /**
     * Returns product last modification time.
     * 
     * @return product the last modification time
     */
    public DateTime getLastModified() {
        return lastModified;
    }

    /**
     * Sets product last modification time
     * 
     * @param lastModified
     *            the product last modification to set
     */
    public void setLastModified(final DateTime lastModified) {
        this.lastModified = lastModified;
    }

    /**
     * @return the showWhenOutOfStock
     */
    public boolean isShowWhenOutOfStock() {
        return showWhenOutOfStock;
    }

    /**
     * @param showWhenOutOfStock
     *            the showWhenOutOfStock to set
     */
    public void setShowWhenOutOfStock(final boolean showWhenOutOfStock) {
        this.showWhenOutOfStock = showWhenOutOfStock;
    }

    /**
     * @return the onlineExclusive
     */
    public boolean isOnlineExclusive() {
        return onlineExclusive;
    }

    /**
     * @param onlineExclusive
     *            the onlineExclusive to set
     */
    public void setOnlineExclusive(final boolean onlineExclusive) {
        this.onlineExclusive = onlineExclusive;
    }

    /**
     * @return the bulkyBoard
     */
    public BulkyBoardProductData getBulkyBoard() {
        return bulkyBoard;
    }

    /**
     * @param bulkyBoard
     *            the bulkyBoard to set
     */
    public void setBulkyBoard(final BulkyBoardProductData bulkyBoard) {
        this.bulkyBoard = bulkyBoard;
    }

    /**
     * @return whether this is a bulky board product at all
     */
    public boolean isBulkyBoardProduct() {
        return bulkyBoardProduct;
    }

    /**
     * @param bulkyBoardProduct
     */
    public void setBulkyBoardProduct(final boolean bulkyBoardProduct) {
        this.bulkyBoardProduct = bulkyBoardProduct;
    }

    /**
     * @return the dealDescription
     */
    public String getDealDescription() {
        return dealDescription;
    }

    /**
     * @param dealDescription
     *            the dealDescription to set
     */
    public void setDealDescription(final String dealDescription) {
        this.dealDescription = dealDescription;
    }

    /**
     * @return the productFeatures
     */
    public List<String> getProductFeatures() {
        return productFeatures;
    }

    /**
     * @param productFeatures
     *            the productFeatures to set
     */
    public void setProductFeatures(final List<String> productFeatures) {
        this.productFeatures = productFeatures;
    }

    /**
     * @return the careInstructions
     */
    public List<String> getCareInstructions() {
        return careInstructions;
    }

    /**
     * @param careInstructions
     *            the careInstructions to set
     */
    public void setCareInstructions(final List<String> careInstructions) {
        this.careInstructions = careInstructions;
    }

    /**
     * @return the materials
     */
    public List<String> getMaterials() {
        return materials;
    }

    /**
     * @param materials
     *            the materials to set
     */
    public void setMaterials(final List<String> materials) {
        this.materials = materials;
    }

    /**
     * @return the extraInfo
     */
    public String getExtraInfo() {
        return extraInfo;
    }

    /**
     * @param extraInfo
     *            the extraInfo to set
     */
    public void setExtraInfo(final String extraInfo) {
        this.extraInfo = extraInfo;
    }

    /**
     * @return the youTubeLink
     */
    public String getYouTubeLink() {
        return youTubeLink;
    }

    /**
     * @param youTubeLink
     *            the youTubeLink to set
     */
    public void setYouTubeLink(final String youTubeLink) {
        this.youTubeLink = youTubeLink;
    }

    /**
     * @return the sizeChartURL
     */
    public String getSizeChartURL() {
        return sizeChartURL;
    }

    /**
     * @param sizeChartURL
     *            the sizeChartURL to set
     */
    public void setSizeChartURL(final String sizeChartURL) {
        this.sizeChartURL = sizeChartURL;
    }

    /**
     * @return the allowDeliverToStore
     */
    public boolean isAllowDeliverToStore() {
        return allowDeliverToStore;
    }

    /**
     * @param allowDeliverToStore
     *            the allowDeliverToStore to set
     */
    public void setAllowDeliverToStore(final boolean allowDeliverToStore) {
        this.allowDeliverToStore = allowDeliverToStore;
    }

    /**
     * @return the productDeliveryToStoreOnly
     */
    public boolean isProductDeliveryToStoreOnly() {
        return productDeliveryToStoreOnly;
    }

    /**
     * @param productDeliveryToStoreOnly
     *            the productDeliveryToStoreOnly to set
     */
    public void setProductDeliveryToStoreOnly(final boolean productDeliveryToStoreOnly) {
        this.productDeliveryToStoreOnly = productDeliveryToStoreOnly;
    }

    /**
     * @return the sizeType
     */
    public String getSizeType() {
        return sizeType;
    }

    /**
     * @param sizeType
     *            the sizeType to set
     */
    public void setSizeType(final String sizeType) {
        this.sizeType = sizeType;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size
     *            the size to set
     */
    public void setSize(final String size) {
        this.size = size;
    }

    /**
     * @return the sizeChartDisplay
     */
    public boolean isSizeChartDisplay() {
        return sizeChartDisplay;
    }

    /**
     * @param sizeChartDisplay
     *            the sizeChartDisplay to set
     */
    public void setSizeChartDisplay(final boolean sizeChartDisplay) {
        this.sizeChartDisplay = sizeChartDisplay;
    }

    /**
     * @return the available
     */
    public Map<String, Boolean> getAvailable() {
        return available;
    }

    /**
     * @param available
     *            the available to set
     */
    public void setAvailable(final Map<String, Boolean> available) {
        this.available = available;
    }

    /**
     * @return the clickAndCollectExcludeTargetCountry
     */
    public boolean isClickAndCollectExcludeTargetCountry() {
        return clickAndCollectExcludeTargetCountry;
    }

    /**
     * @param clickAndCollectExcludeTargetCountry
     *            the clickAndCollectExcludeTargetCountry to set
     */
    public void setClickAndCollectExcludeTargetCountry(final boolean clickAndCollectExcludeTargetCountry) {
        this.clickAndCollectExcludeTargetCountry = clickAndCollectExcludeTargetCountry;
    }

    /**
     * @return the bigAndBulky
     */
    public boolean isBigAndBulky() {
        return bigAndBulky;
    }

    /**
     * @param bigAndBulky
     *            the bigAndBulky to set
     */
    public void setBigAndBulky(final boolean bigAndBulky) {
        this.bigAndBulky = bigAndBulky;
    }

    /**
     * @return the bulkyHomeDeliveryFee
     */
    public PriceData getBulkyHomeDeliveryFee() {
        return bulkyHomeDeliveryFee;
    }

    /**
     * @param bulkyHomeDeliveryFee
     *            the bulkyHomeDeliveryFee to set
     */
    public void setBulkyHomeDeliveryFee(final PriceData bulkyHomeDeliveryFee) {
        this.bulkyHomeDeliveryFee = bulkyHomeDeliveryFee;
    }

    /**
     * @return the bulkyCncFreeThreshold
     */
    public PriceData getBulkyCncFreeThreshold() {
        return bulkyCncFreeThreshold;
    }

    /**
     * @param bulkyCncFreeThreshold
     *            the bulkyCncFreeThreshold to set
     */
    public void setBulkyCncFreeThreshold(final PriceData bulkyCncFreeThreshold) {
        this.bulkyCncFreeThreshold = bulkyCncFreeThreshold;
    }

    /**
     * @return the topLevelCategory
     */
    public String getTopLevelCategory() {
        return topLevelCategory;
    }

    /**
     * @param topLevelCategory
     *            the topLevelCategory to set
     */
    public void setTopLevelCategory(final String topLevelCategory) {
        this.topLevelCategory = topLevelCategory;
    }

    /**
     * @return the baseName
     */
    public String getBaseName() {
        return baseName;
    }

    /**
     * @param baseName
     *            the baseName to set
     */
    public void setBaseName(final String baseName) {
        this.baseName = baseName;
    }

    /**
     * @return the baseProductCode
     */
    public String getBaseProductCode() {
        return baseProductCode;
    }

    /**
     * @param baseProductCode
     *            the baseProductCode to set
     */
    public void setBaseProductCode(final String baseProductCode) {
        this.baseProductCode = baseProductCode;
    }

    /**
     * @return the newArrived
     */
    public boolean isNewArrived() {
        return newArrived;
    }

    /**
     * @param newArrived
     *            the newArrived to set
     */
    public void setNewArrived(final boolean newArrived) {
        this.newArrived = newArrived;
    }

    /**
     * @return the itemType
     */
    public String getItemType() {
        return itemType;
    }

    /**
     * @param itemType
     *            the itemType to set
     */
    public void setItemType(final String itemType) {
        this.itemType = itemType;
    }

    /**
     * @return the productPromotionalDeliverySticker
     */
    public String getProductPromotionalDeliverySticker() {
        return productPromotionalDeliverySticker;
    }

    /**
     * @param productPromotionalDeliverySticker
     *            the productPromotionalDeliverySticker to set
     */
    public void setProductPromotionalDeliverySticker(final String productPromotionalDeliverySticker) {
        this.productPromotionalDeliverySticker = productPromotionalDeliverySticker;
    }

    /**
     * @return the productPromotionalDeliveryText
     */
    public String getProductPromotionalDeliveryText() {
        return productPromotionalDeliveryText;
    }

    /**
     * @param productPromotionalDeliveryText
     *            the productPromotionalDeliveryText to set
     */
    public void setProductPromotionalDeliveryText(final String productPromotionalDeliveryText) {
        this.productPromotionalDeliveryText = productPromotionalDeliveryText;
    }

    /**
     * @return the productTypeCode
     */
    public String getProductTypeCode() {
        return productTypeCode;
    }

    /**
     * @param productTypeCode
     *            the productTypeCode to set
     */
    public void setProductTypeCode(final String productTypeCode) {
        this.productTypeCode = productTypeCode;
    }

    /**
     * @return the preview
     */
    public boolean isPreview() {
        return preview;
    }

    /**
     * @param preview
     *            the preview to set
     */
    public void setPreview(final boolean preview) {
        this.preview = preview;
    }

    /**
     * @return the apn
     */
    public String getApn() {
        return apn;
    }

    /**
     * @param apn
     *            the apn to set
     */
    public void setApn(final String apn) {
        this.apn = apn;
    }

    /**
     * @return the displayOnly
     */
    public boolean isDisplayOnly() {
        return displayOnly;
    }

    /**
     * @param displayOnly
     *            the displayOnly to set
     */
    public void setDisplayOnly(final boolean displayOnly) {
        this.displayOnly = displayOnly;
    }

    /**
     * @return the inStock
     */
    public boolean isInStock() {
        return inStock;
    }

    /**
     * @param inStock
     *            the inStock to set
     */
    public void setInStock(final boolean inStock) {
        this.inStock = inStock;
    }

    /**
     * @return the excludeForAfterpay
     */
    public boolean isExcludeForAfterpay() {
        return excludeForAfterpay;
    }

    /**
     * @param excludeForAfterpay
     *            the excludeForAfterpay to set
     */
    public void setExcludeForAfterpay(final boolean excludeForAfterpay) {
        this.excludeForAfterpay = excludeForAfterpay;
    }

    /**
     * @return the excludeForZipPayment
     */
    public boolean isExcludeForZipPayment() { return excludeForZipPayment; }

    /**
     * @param excludeForZipPayment
     *            the excludeForZipPayment to set
     */
    public void setExcludeForZipPayment(final boolean excludeForZipPayment) {
        this.excludeForZipPayment = excludeForZipPayment;
    }

    /**
     * @return the installmentPrice
     */
    public PriceData getInstallmentPrice() {
        return installmentPrice;
    }

    /**
     * @param installmentPrice
     *            the installmentPrice to set
     */
    public void setInstallmentPrice(final PriceData installmentPrice) {
        this.installmentPrice = installmentPrice;
    }

    /**
     * @return the originalCategoryCode
     */
    public String getOriginalCategoryCode() {
        return originalCategoryCode;
    }

    /**
     * @param originalCategoryCode
     *            the originalCategoryCode to set
     */
    public void setOriginalCategoryCode(final String originalCategoryCode) {
        this.originalCategoryCode = originalCategoryCode;
    }

    /**
     * @return the promotionStatusClass
     */
    public List<String> getPromotionStatusClass() {
        return promotionStatusClass;
    }

    /**
     * @param promotionStatusClass
     *            the promotionStatusClass to set
     */
    public void setPromotionStatusClass(final List<String> promotionStatusClass) {
        this.promotionStatusClass = promotionStatusClass;
    }

    /**
     * @return the originalCategoryName
     */
    public String getOriginalCategoryName() {
        return originalCategoryName;
    }

    /**
     * @param originalCategoryName
     *            the originalCategoryName to set
     */
    public void setOriginalCategoryName(final String originalCategoryName) {
        this.originalCategoryName = originalCategoryName;
    }

    /**
     * @return the merchDepartmentCode
     */
    public String getMerchDepartmentCode() {
        return merchDepartmentCode;
    }

    /**
     * @param merchDepartmentCode
     *            the merchDepartmentCode to set
     */
    public void setMerchDepartmentCode(final String merchDepartmentCode) {
        this.merchDepartmentCode = merchDepartmentCode;
    }

    /**
     * @return the merchDepartmentName
     */
    public String getMerchDepartmentName() {
        return merchDepartmentName;
    }

    /**
     * @param merchDepartmentName
     *            the merchDepartmentName to set
     */
    public void setMerchDepartmentName(final String merchDepartmentName) {
        this.merchDepartmentName = merchDepartmentName;
    }

}

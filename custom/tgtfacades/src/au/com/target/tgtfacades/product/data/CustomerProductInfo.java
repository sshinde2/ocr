/**
 * 
 */
package au.com.target.tgtfacades.product.data;

import org.codehaus.jackson.annotate.JsonProperty;


/**
 * @author mjanarth
 *
 */

public class CustomerProductInfo {

    @JsonProperty("pc")
    private String selectedVariantCode;
    @JsonProperty("bc")
    private String baseProductCode;
    @JsonProperty("t")
    private String timeStamp;


    /**
     * @return the baseProductCode
     */
    public String getBaseProductCode() {
        return baseProductCode;
    }

    /**
     * @param baseProductCode
     *            the baseProductCode to set
     */
    public void setBaseProductCode(final String baseProductCode) {
        this.baseProductCode = baseProductCode;
    }

    /**
     * @return the timeStamp
     */
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     * @param timeStamp
     *            the timeStamp to set
     */
    public void setTimeStamp(final String timeStamp) {
        this.timeStamp = timeStamp;
    }

    /**
     * @return the selectedVariantCode
     */
    public String getSelectedVariantCode() {
        return selectedVariantCode;
    }

    /**
     * @param selectedVariantCode
     *            the selectedVariantCode to set
     */
    public void setSelectedVariantCode(final String selectedVariantCode) {
        this.selectedVariantCode = selectedVariantCode;
    }


}

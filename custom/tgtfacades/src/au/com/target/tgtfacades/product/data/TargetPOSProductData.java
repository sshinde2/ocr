/**
 * 
 */
package au.com.target.tgtfacades.product.data;

import de.hybris.platform.commercefacades.product.data.PriceData;


/**
 * Represent pos product data in facade layer
 * 
 */
public class TargetPOSProductData {

    private String itemCode;
    private String description;

    private PriceData price;
    private PriceData wasPrice;

    private String failureReason;

    public enum ErrorType {
        NONE,
        POS, // Error originated in POS, eg item not found 
        SYSTEM_ERROR, // Error originating in service, when looking up store or state 
        INVALID_DATA // Error validating parameters or returned prices
    }

    private ErrorType errorType;


    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(final String itemCode) {
        this.itemCode = itemCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public PriceData getPrice() {
        return price;
    }

    public void setPrice(final PriceData price) {
        this.price = price;
    }

    public PriceData getWasPrice() {
        return wasPrice;
    }

    public void setWasPrice(final PriceData wasPrice) {
        this.wasPrice = wasPrice;
    }

    public String getFailureReason() {
        return failureReason;
    }

    public void setFailureReason(final String failureReason) {
        this.failureReason = failureReason;
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    public void setErrorType(final ErrorType errorType) {
        this.errorType = errorType;
    }

}

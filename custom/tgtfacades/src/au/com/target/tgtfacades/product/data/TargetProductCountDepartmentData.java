package au.com.target.tgtfacades.product.data;



/**
 * @author bhuang3 get the page count for department from total product in department and the product count in every
 *         page
 */
public class TargetProductCountDepartmentData {

    private String targetMerchDepartmentCode;

    private int pageCount;



    /**
     * @return the targetMerchDepartmentCode
     */
    public String getTargetMerchDepartmentCode() {
        return targetMerchDepartmentCode;
    }

    /**
     * @param targetMerchDepartmentCode
     *            the targetMerchDepartmentCode to set
     */
    public void setTargetMerchDepartmentCode(final String targetMerchDepartmentCode) {
        this.targetMerchDepartmentCode = targetMerchDepartmentCode;
    }

    /**
     * @return the pageCount
     */
    public int getPageCount() {
        return pageCount;
    }

    /**
     * @param pageCount
     *            the pageCount to set
     */
    public void setPageCount(final int pageCount) {
        this.pageCount = pageCount;
    }


}

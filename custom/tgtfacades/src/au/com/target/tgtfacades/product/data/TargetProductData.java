/**
 * 
 */
package au.com.target.tgtfacades.product.data;

import java.util.Date;
import java.util.List;

import au.com.target.tgtcore.product.data.ProductDisplayType;
import au.com.target.tgtfacades.data.CanonicalData;
import au.com.target.tgtfacades.product.data.enums.TargetManchesterSizeEnum;


/**
 * @author rmcalave
 * 
 */
public class TargetProductData extends AbstractTargetProductData {
    private String productType;

    private String sellableVariantDisplayCode;
    private String colourName;
    private String swatch;
    private TargetManchesterSizeEnum manchesterSize;
    private boolean colourVariant;
    private boolean sizeVariant;

    private CanonicalData canonical;
    private CanonicalData baseProductCanonical;

    private boolean hasSizeChart;

    private String colourVariantCode;

    private Date newLowerPriceStartDate;

    private String imageUrl;

    private int totalInterest;

    private int maxThresholdInterest;

    private boolean showStoreStockForProduct;

    private boolean assorted;

    private List<TargetProductEndLinkData> productEndLinks;

    private String normalSaleStartDate;
    private ProductDisplayType productDisplayType;

    /**
     * @return the productEndLinks
     */
    public List<TargetProductEndLinkData> getProductEndLinks() {
        return productEndLinks;
    }

    /**
     * @param productEndLinks
     *            the productEndLinks to set
     */
    public void setProductEndLinks(final List<TargetProductEndLinkData> productEndLinks) {
        this.productEndLinks = productEndLinks;
    }

    /**
     * @return the productType
     */
    public String getProductType() {
        return productType;
    }

    /**
     * @param productType
     *            the productType to set
     */
    public void setProductType(final String productType) {
        this.productType = productType;
    }

    /**
     * @return the colourName
     */
    public String getColourName() {
        return colourName;
    }

    /**
     * @return the sellableVariantDisplayCode
     */
    public String getSellableVariantDisplayCode() {
        return sellableVariantDisplayCode;
    }

    /**
     * @param sellableVariantDisplayCode
     *            the sellableVariantDisplayCode to set
     */
    public void setSellableVariantDisplayCode(final String sellableVariantDisplayCode) {
        this.sellableVariantDisplayCode = sellableVariantDisplayCode;
    }

    /**
     * @param colourName
     *            the colourName to set
     */
    public void setColourName(final String colourName) {
        this.colourName = colourName;
    }

    /**
     * @return the swatch
     */
    public String getSwatch() {
        return swatch;
    }

    /**
     * @param swatch
     *            the swatch to set
     */
    public void setSwatch(final String swatch) {
        this.swatch = swatch;
    }

    /**
     * @return the manchesterSize
     */
    public TargetManchesterSizeEnum getManchesterSize() {
        return manchesterSize;
    }

    /**
     * @param manchesterSize
     *            the manchesterSize to set
     */
    public void setManchesterSize(final TargetManchesterSizeEnum manchesterSize) {
        this.manchesterSize = manchesterSize;
    }

    /**
     * @return the colourVariant
     */
    public boolean isColourVariant() {
        return colourVariant;
    }

    /**
     * @param colourVariant
     *            the colourVariant to set
     */
    public void setColourVariant(final boolean colourVariant) {
        this.colourVariant = colourVariant;
    }

    /**
     * @return the sizeVariant
     */
    public boolean isSizeVariant() {
        return sizeVariant;
    }

    /**
     * @param sizeVariant
     *            the sizeVariant to set
     */
    public void setSizeVariant(final boolean sizeVariant) {
        this.sizeVariant = sizeVariant;
    }

    /**
     * 
     * @return the canonical
     */
    public CanonicalData getCanonical() {
        return canonical;
    }

    /**
     * @param canonical
     *            the canonical to set
     */
    public void setCanonical(final CanonicalData canonical) {
        this.canonical = canonical;
    }

    /**
     * @return the baseProductCanonical
     */
    public CanonicalData getBaseProductCanonical() {
        return baseProductCanonical;
    }

    /**
     * @param baseProductCanonical
     *            the baseProductCanonical to set
     */
    public void setBaseProductCanonical(final CanonicalData baseProductCanonical) {
        this.baseProductCanonical = baseProductCanonical;
    }

    /**
     * @return the hasSizeChart
     */
    public boolean isHasSizeChart() {
        return hasSizeChart;
    }

    /**
     * @param hasSizeChart
     *            the hasSizeChart to set
     */
    public void setHasSizeChart(final boolean hasSizeChart) {
        this.hasSizeChart = hasSizeChart;
    }

    /**
     * @return the colourVariantCode
     */
    public String getColourVariantCode() {
        return colourVariantCode;
    }

    /**
     * @param colourVariantCode
     *            the colourVariantCode to set
     */
    public void setColourVariantCode(final String colourVariantCode) {
        this.colourVariantCode = colourVariantCode;
    }

    /**
     * @return the newLowerPriceStartDate
     */
    public Date getNewLowerPriceStartDate() {
        return newLowerPriceStartDate;
    }

    /**
     * @param newLowerPriceStartDate
     *            the newLowerPriceStartDate to set
     */
    public void setNewLowerPriceStartDate(final Date newLowerPriceStartDate) {
        this.newLowerPriceStartDate = newLowerPriceStartDate;
    }

    /**
     * @return the imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * @param imageUrl
     *            the imageUrl to set
     */
    public void setImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * @return the totalInterest
     */
    public int getTotalInterest() {
        return totalInterest;
    }

    /**
     * @param totalInterest
     *            the totalInterest to set
     */
    public void setTotalInterest(final int totalInterest) {
        this.totalInterest = totalInterest;
    }

    /**
     * @return the maxThresholdInterest
     */
    public int getMaxThresholdInterest() {
        return maxThresholdInterest;
    }

    /**
     * @param maxThresholdInterest
     *            the maxThresholdInterest to set
     */
    public void setMaxThresholdInterest(final int maxThresholdInterest) {
        this.maxThresholdInterest = maxThresholdInterest;
    }

    /**
     * @return the showStoreStockForProduct
     */
    public boolean isShowStoreStockForProduct() {
        return showStoreStockForProduct;
    }

    /**
     * @param showStoreStockForProduct
     *            the showStoreStockForProduct to set
     */
    public void setShowStoreStockForProduct(final boolean showStoreStockForProduct) {
        this.showStoreStockForProduct = showStoreStockForProduct;
    }

    /**
     * @return the assorted value
     */
    public boolean isAssorted() {
        return assorted;
    }

    /**
     * @param assorted
     *            the assorted to set
     */
    public void setAssorted(final boolean assorted) {
        this.assorted = assorted;
    }

    /**
     * @return the normalSaleStartDate
     */
    public String getNormalSaleStartDate() {
        return normalSaleStartDate;
    }

    /**
     * @param normalSaleStartDate
     *            the normalSaleStartDate to set
     */
    public void setNormalSaleStartDate(final String normalSaleStartDate) {
        this.normalSaleStartDate = normalSaleStartDate;
    }

    /**
     * @return the productDisplayType
     */
    public ProductDisplayType getProductDisplayType() {
        return productDisplayType;
    }

    /**
     * @param productDisplayType
     *            the productDisplayType to set
     */
    public void setProductDisplayType(final ProductDisplayType productDisplayType) {
        this.productDisplayType = productDisplayType;
    }


}
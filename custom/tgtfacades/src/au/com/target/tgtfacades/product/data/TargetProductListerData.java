/**
 * 
 */
package au.com.target.tgtfacades.product.data;

import java.util.List;



/**
 * @author pthoma20
 * 
 *         To Display the product Level Details on the Listing Page.
 */
public class TargetProductListerData extends AbstractTargetProductData {

    // Will be set as true even if one of the product is instock.
    private boolean missingPrice;

    private List<TargetVariantProductListerData> targetVariantProductListerData;

    private String dealDescription;

    private String productPromotionalDeliverySticker;

    private String totalInterest;

    private TargetSelectedVariantData selectedVariantData;

    private Integer maxAvailOnlineQty;

    private Integer maxAvailStoreQty;

    /**
     * max of actualConsolidatedStock (without buffer applied)
     */
    private Integer maxActualConsolidatedStoreStock;

    private int colourVariantCount;

    /**
     * If there are more than variants available for the base product
     */
    private boolean otherVariantsAvailable;

    private int maxThresholdInterest;

    /**
     * @return the targetVariantProductListerData
     */
    public List<TargetVariantProductListerData> getTargetVariantProductListerData() {
        return targetVariantProductListerData;
    }

    /**
     * @param targetVariantProductListerData
     *            the targetVariantProductListerData to set
     */
    public void setTargetVariantProductListerData(
            final List<TargetVariantProductListerData> targetVariantProductListerData) {
        this.targetVariantProductListerData = targetVariantProductListerData;
    }

    /**
     * @return the missingPrice
     */
    public boolean isMissingPrice() {
        return missingPrice;
    }

    /**
     * @param missingPrice
     *            the missingPrice to set
     */
    public void setMissingPrice(final boolean missingPrice) {
        this.missingPrice = missingPrice;
    }

    /**
     * @return the dealDescription
     */
    @Override
    public String getDealDescription() {
        return dealDescription;
    }

    /**
     * @param dealDescription
     *            the dealDescription to set
     */
    @Override
    public void setDealDescription(final String dealDescription) {
        this.dealDescription = dealDescription;
    }

    /**
     * @return the productPromotionalDeliverySticker
     */
    @Override
    public String getProductPromotionalDeliverySticker() {
        return productPromotionalDeliverySticker;
    }

    /**
     * @param productPromotionalDeliverySticker
     *            the productPromotionalDeliverySticker to set
     */
    @Override
    public void setProductPromotionalDeliverySticker(final String productPromotionalDeliverySticker) {
        this.productPromotionalDeliverySticker = productPromotionalDeliverySticker;
    }

    /**
     * @return the totalInterest
     */
    public String getTotalInterest() {
        return totalInterest;
    }

    /**
     * @param totalInterest
     *            the totalInterest to set
     */
    public void setTotalInterest(final String totalInterest) {
        this.totalInterest = totalInterest;
    }

    /**
     * @return the selectedVariantData
     */
    public TargetSelectedVariantData getSelectedVariantData() {
        return selectedVariantData;
    }

    /**
     * @param selectedVariantData
     *            the selectedVariantData to set
     */
    public void setSelectedVariantData(final TargetSelectedVariantData selectedVariantData) {
        this.selectedVariantData = selectedVariantData;
    }

    /**
     * @return the otherVariantsAvailable
     */
    public boolean isOtherVariantsAvailable() {
        return otherVariantsAvailable;
    }

    /**
     * @param otherVariantsAvailable
     *            the otherVariantsAvailable to set
     */
    public void setOtherVariantsAvailable(final boolean otherVariantsAvailable) {
        this.otherVariantsAvailable = otherVariantsAvailable;
    }

    /**
     * @return the maxAvailOnlineQty
     */
    public Integer getMaxAvailOnlineQty() {
        return maxAvailOnlineQty;
    }

    /**
     * @param maxAvailOnlineQty
     *            the maxAvailOnlineQty to set
     */
    public void setMaxAvailOnlineQty(final Integer maxAvailOnlineQty) {
        this.maxAvailOnlineQty = maxAvailOnlineQty;
    }

    /**
     * @return the maxAvailStoreQty
     */
    public Integer getMaxAvailStoreQty() {
        return maxAvailStoreQty;
    }

    /**
     * @param maxAvailStoreQty
     *            the maxAvailStoreQty to set
     */
    public void setMaxAvailStoreQty(final Integer maxAvailStoreQty) {
        this.maxAvailStoreQty = maxAvailStoreQty;
    }

    /**
     * @return the colourVariantCount
     */
    public int getColourVariantCount() {
        return colourVariantCount;
    }

    /**
     * @param colourVariantCount
     *            the colourVariantCount to set
     */
    public void setColourVariantCount(final int colourVariantCount) {
        this.colourVariantCount = colourVariantCount;
    }

    /**
     * @return the maxActualConsolidatedStoreStock
     */
    public Integer getMaxActualConsolidatedStoreStock() {
        return maxActualConsolidatedStoreStock;
    }

    /**
     * @param maxActualConsolidatedStoreStock
     *            the maxActualConsolidatedStoreStock to set
     */
    public void setMaxActualConsolidatedStoreStock(final Integer maxActualConsolidatedStoreStock) {
        this.maxActualConsolidatedStoreStock = maxActualConsolidatedStoreStock;
    }

    /**
     * @return the maxThresholdInterest
     */
    public int getMaxThresholdInterest() {
        return maxThresholdInterest;
    }

    /**
     * @param maxThresholdInterest
     *            the maxThresholdInterest to set
     */
    public void setMaxThresholdInterest(final int maxThresholdInterest) {
        this.maxThresholdInterest = maxThresholdInterest;
    }

}

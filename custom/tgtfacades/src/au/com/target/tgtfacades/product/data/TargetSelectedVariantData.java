/**
 * 
 */
package au.com.target.tgtfacades.product.data;

import de.hybris.platform.commercefacades.product.data.PriceData;


/**
 * @author mjanarth
 *
 */
public class TargetSelectedVariantData {

    private String productCode;

    private int availableQty;

    private String url;

    private String size;

    private String color;

    private PriceData price;

    private PriceData wasPrice;

    /**
     * Whether the selected variant is sellable variant
     */
    private boolean sellableVariant;

    private boolean assorted;

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     *            the url to set
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size
     *            the size to set
     */
    public void setSize(final String size) {
        this.size = size;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color
     *            the color to set
     */
    public void setColor(final String color) {
        this.color = color;
    }

    /**
     * @return the sellableVariant
     */
    public boolean isSellableVariant() {
        return sellableVariant;
    }

    /**
     * @param sellableVariant
     *            the sellableVariant to set
     */
    public void setSellableVariant(final boolean sellableVariant) {
        this.sellableVariant = sellableVariant;
    }

    /**
     * @return the availableQty
     */
    public int getAvailableQty() {
        return availableQty;
    }

    /**
     * @param availableQty
     *            the availableQty to set
     */
    public void setAvailableQty(final int availableQty) {
        this.availableQty = availableQty;
    }

    /**
     * @return the price
     */
    public PriceData getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(final PriceData price) {
        this.price = price;
    }

    /**
     * @return the wasPrice
     */
    public PriceData getWasPrice() {
        return wasPrice;
    }

    /**
     * @param wasPrice
     *            the wasPrice to set
     */
    public void setWasPrice(final PriceData wasPrice) {
        this.wasPrice = wasPrice;
    }

    /**
     * @return the assorted
     */
    public boolean isAssorted() {
        return assorted;
    }

    /**
     * @param assorted
     *            the assorted to set
     */
    public void setAssorted(final boolean assorted) {
        this.assorted = assorted;
    }
}

/**
 * 
 */
package au.com.target.tgtfacades.product.data;

import java.util.Date;
import java.util.List;

import au.com.target.tgtcore.product.data.ProductDisplayType;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;



/**
 * @author pthoma20
 * 
 *         Target Product specific attributes at sellable level
 * 
 */
public class TargetVariantProductListerData extends AbstractTargetProductData {

    private String colourVariantCode;

    private String colourName;

    private String swatchColour;

    private List<String> swatchImageUrls;

    private List<String> gridImageUrls;

    private List<String> heroImageUrls;

    private List<String> largeImageUrls;

    private List<String> listImageUrls;

    private List<String> thumbImageUrl;

    private String displayPrice;

    private String displayWasPrice;

    private List<TargetVariantProductListerData> variants;

    private Boolean assorted;

    private int sizeVariantCount;

    private ProductDisplayType productDisplayType;

    private Date normalSaleStartDate;

    private List<TargetZoneDeliveryModeData> deliveryModes;

    private Integer sizePosition;


    /**
     * @return the colourName
     */
    public String getColourName() {
        return colourName;
    }


    /**
     * @param colourName
     *            the colourName to set
     */
    public void setColourName(final String colourName) {
        this.colourName = colourName;
    }


    /**
     * @return the swatchColour
     */
    public String getSwatchColour() {
        return swatchColour;
    }


    /**
     * @param swatchColour
     *            the swatchColour to set
     */
    public void setSwatchColour(final String swatchColour) {
        this.swatchColour = swatchColour;
    }


    /**
     * @return the swatchImageUrls
     */
    public List<String> getSwatchImageUrls() {
        return swatchImageUrls;
    }


    /**
     * @param swatchImageUrls
     *            the swatchImageUrls to set
     */
    public void setSwatchImageUrls(final List<String> swatchImageUrls) {
        this.swatchImageUrls = swatchImageUrls;
    }


    /**
     * @return the gridImageUrls
     */
    public List<String> getGridImageUrls() {
        return gridImageUrls;
    }


    /**
     * @param gridImageUrls
     *            the gridImageUrls to set
     */
    public void setGridImageUrls(final List<String> gridImageUrls) {
        this.gridImageUrls = gridImageUrls;
    }


    /**
     * @return the heroImageUrls
     */
    public List<String> getHeroImageUrls() {
        return heroImageUrls;
    }


    /**
     * @param heroImageUrls
     *            the heroImageUrls to set
     */
    public void setHeroImageUrls(final List<String> heroImageUrls) {
        this.heroImageUrls = heroImageUrls;
    }


    /**
     * @return the listImageUrls
     */
    public List<String> getListImageUrls() {
        return listImageUrls;
    }


    /**
     * @param listImageUrls
     *            the listImageUrls to set
     */
    public void setListImageUrls(final List<String> listImageUrls) {
        this.listImageUrls = listImageUrls;
    }


    /**
     * @return the thumbImageUrl
     */
    public List<String> getThumbImageUrl() {
        return thumbImageUrl;
    }


    /**
     * @param thumbImageUrl
     *            the thumbImageUrl to set
     */
    public void setThumbImageUrl(final List<String> thumbImageUrl) {
        this.thumbImageUrl = thumbImageUrl;
    }

    /**
     * @return the colourVariantCode
     */
    public String getColourVariantCode() {
        return colourVariantCode;
    }


    /**
     * @param colourVariantCode
     *            the colourVariantCode to set
     */
    public void setColourVariantCode(final String colourVariantCode) {
        this.colourVariantCode = colourVariantCode;
    }


    /**
     * @return the largeImageUrls
     */
    public List<String> getLargeImageUrls() {
        return largeImageUrls;
    }


    /**
     * @param largeImageUrls
     *            the largeImageUrls to set
     */
    public void setLargeImageUrls(final List<String> largeImageUrls) {
        this.largeImageUrls = largeImageUrls;
    }


    /**
     * @return the displayPrice
     */
    public String getDisplayPrice() {
        return displayPrice;
    }


    /**
     * @param displayPrice
     *            the displayPrice to set
     */
    public void setDisplayPrice(final String displayPrice) {
        this.displayPrice = displayPrice;
    }


    /**
     * @return the displayWasPrice
     */
    public String getDisplayWasPrice() {
        return displayWasPrice;
    }


    /**
     * @param displayWasPrice
     *            the displayWasPrice to set
     */
    public void setDisplayWasPrice(final String displayWasPrice) {
        this.displayWasPrice = displayWasPrice;
    }


    /**
     * @return the variants
     */
    public List<TargetVariantProductListerData> getVariants() {
        return variants;
    }


    /**
     * @param variants
     *            the variants to set
     */
    public void setVariants(final List<TargetVariantProductListerData> variants) {
        this.variants = variants;
    }

    /**
     * @return the assorted
     */
    public Boolean getAssorted() {
        return assorted;
    }

    /**
     * @param assorted
     *            the assorted to set
     */

    public void setAssorted(final Boolean assorted) {
        this.assorted = assorted;
    }


    /**
     * @return the sizeVariantCount
     */
    public int getSizeVariantCount() {
        return sizeVariantCount;
    }


    /**
     * @param sizeVariantCount
     *            the sizeVariantCount to set
     */
    public void setSizeVariantCount(final int sizeVariantCount) {
        this.sizeVariantCount = sizeVariantCount;
    }


    /**
     * @return the productDisplayType
     */
    public ProductDisplayType getProductDisplayType() {
        return productDisplayType;
    }


    /**
     * @param productDisplayType
     *            the productDisplayType to set
     */
    public void setProductDisplayType(final ProductDisplayType productDisplayType) {
        this.productDisplayType = productDisplayType;
    }


    /**
     * @return the normalSaleStartDate
     */
    public Date getNormalSaleStartDate() {
        return normalSaleStartDate;
    }

    /**
     * @return the deliveryModes
     */
    @Override
    public List<TargetZoneDeliveryModeData> getDeliveryModes() {
        return deliveryModes;
    }


    /**
     * @param normalSaleStartDate
     *            the normalSaleStartDate to set
     */
    public void setNormalSaleStartDate(final Date normalSaleStartDate) {
        this.normalSaleStartDate = normalSaleStartDate;
    }

    @Override
    public void setDeliveryModes(final List<TargetZoneDeliveryModeData> deliveryModes) {
        this.deliveryModes = deliveryModes;
    }


    /**
     * @return the sizePosition
     */
    public Integer getSizePosition() {
        return sizePosition;
    }


    /**
     * @param sizePosition
     *            the sizePosition to set
     */
    public void setSizePosition(final Integer sizePosition) {
        this.sizePosition = sizePosition;
    }

}
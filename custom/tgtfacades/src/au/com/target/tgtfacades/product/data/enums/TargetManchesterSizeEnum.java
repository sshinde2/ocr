/**
 * 
 */
package au.com.target.tgtfacades.product.data.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;


/**
 * @author rmcalave
 * 
 */
public enum TargetManchesterSizeEnum {
    SINGLE_BED("SB", "Single Bed"),
    KING_SINGLE_BED("KSB", "King Single Bed"),
    DOUBLE_BED("DB", "Double Bed"),
    QUEEN_BED("QB", "Queen Bed"),
    KING_BED("KB", "King Bed");

    private static final Map<String, TargetManchesterSizeEnum> MANCHESTER_SIZE_VALUE_LOOKUP_MAP = new HashMap<String, TargetManchesterSizeEnum>();

    private String code;
    private String value;

    static {
        for (final TargetManchesterSizeEnum targetPricePrefix : EnumSet.allOf(TargetManchesterSizeEnum.class)) {
            MANCHESTER_SIZE_VALUE_LOOKUP_MAP.put(targetPricePrefix.getValue(), targetPricePrefix);
        }
    }

    private TargetManchesterSizeEnum(final String code, final String value) {
        this.code = code;
        this.value = value;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    public static TargetManchesterSizeEnum getByValue(final String value) {
        return MANCHESTER_SIZE_VALUE_LOOKUP_MAP.get(value);
    }
}

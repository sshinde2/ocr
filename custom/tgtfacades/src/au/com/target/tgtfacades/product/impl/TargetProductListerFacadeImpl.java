/**
 * 
 */
package au.com.target.tgtfacades.product.impl;

import de.hybris.platform.commercefacades.converter.ConfigurablePopulator;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtfacades.product.TargetProductListerFacade;
import au.com.target.tgtfacades.product.data.TargetProductListerData;


/**
 * @author rmcalave
 *
 */
public class TargetProductListerFacadeImpl implements TargetProductListerFacade {
    private TargetProductService targetProductService;


    private ConfigurablePopulator<TargetProductModel, TargetProductListerData, ProductOption> productListerConverter;


    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.product.impl.TargetProductListerFacade#getBaseProductDataByCode(java.lang.String)
     */
    @Override
    public TargetProductListerData getBaseProductDataByCode(final String code, final List<ProductOption> options) {

        ProductModel product = null;
        final TargetProductListerData targetProductListerData = new TargetProductListerData();


        try {
            product = targetProductService.getProductForCode(code);
        }
        catch (final UnknownIdentifierException | AmbiguousIdentifierException ex) {
            product = null;
        }

        if (product == null) {
            return null;
        }

        final ProductModel baseProduct = getBaseProduct(product);

        productListerConverter
                .populate((TargetProductModel)baseProduct, targetProductListerData, options);

        return targetProductListerData;
    }

    private ProductModel getBaseProduct(final ProductModel product) {
        if (product instanceof AbstractTargetVariantProductModel) {
            final ProductModel baseProduct = ((AbstractTargetVariantProductModel)product).getBaseProduct();
            if (baseProduct != null) {
                return getBaseProduct(baseProduct);
            }
        }

        return product;
    }



    /**
     * @param targetProductService
     *            the targetProductService to set
     */
    @Required
    public void setTargetProductService(final TargetProductService targetProductService) {
        this.targetProductService = targetProductService;
    }

    /**
     * @param productListerConverter
     *            the productListerConverter to set
     */
    @Required
    public void setProductListerConverter(
            final ConfigurablePopulator<TargetProductModel, TargetProductListerData, ProductOption> productListerConverter) {
        this.productListerConverter = productListerConverter;
    }




}

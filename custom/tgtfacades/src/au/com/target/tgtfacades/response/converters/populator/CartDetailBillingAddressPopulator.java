/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;


/**
 * Populator to populate billing address for CartDetailResponseData from TargetCartData.
 * 
 */
public class CartDetailBillingAddressPopulator implements Populator<TargetCartData, CartDetailResponseData> {

    @Override
    public void populate(final TargetCartData source, final CartDetailResponseData target) throws ConversionException {
        target.setBillingAddress(source.getBillingAddress());
    }
}

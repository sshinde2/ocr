/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.StringUtils;

import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;
import au.com.target.tgtfacades.user.data.TargetAddressData;


/**
 * Populator to populate delivery address for CartDetailResponseData from TargetCartData.
 * 
 * @author htan3
 *
 */
public class CartDetailDeliveryAddressPopulator implements Populator<TargetCartData, CartDetailResponseData> {

    @Override
    public void populate(final TargetCartData source, final CartDetailResponseData target) throws ConversionException {
        if (source.getDeliveryAddress() instanceof TargetAddressData && StringUtils.isEmpty(source.getCncStoreNumber())) {
            target.setDeliveryAddress((TargetAddressData)source.getDeliveryAddress());
        }
    }

}

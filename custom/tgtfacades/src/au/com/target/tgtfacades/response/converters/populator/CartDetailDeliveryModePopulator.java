/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;

import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;


/**
 * @author htan3
 *
 */
public class CartDetailDeliveryModePopulator implements Populator<TargetCartData, CartDetailResponseData> {


    @Override
    public void populate(final TargetCartData source, final CartDetailResponseData target) throws ConversionException {
        target.setDeliveryMode(getDeliveryModeInfo(source));
    }

    protected TargetZoneDeliveryModeData getDeliveryModeInfo(final TargetCartData cart) {
        TargetZoneDeliveryModeData selectedDeliveryMode = null;
        final List<TargetZoneDeliveryModeData> deliveryModes = cart.getDeliveryModes();
        if (cart.getDeliveryMode() != null && deliveryModes != null) {
            for (final TargetZoneDeliveryModeData targetZoneDeliveryModeData : deliveryModes) {
                if (targetZoneDeliveryModeData.getCode().equalsIgnoreCase(cart.getDeliveryMode().getCode())) {
                    selectedDeliveryMode = targetZoneDeliveryModeData;
                    selectedDeliveryMode.setAvailable(true);
                    break;
                }
            }
        }
        return selectedDeliveryMode;
    }
}

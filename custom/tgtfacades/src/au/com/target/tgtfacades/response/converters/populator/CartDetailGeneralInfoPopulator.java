/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;


/**
 * This class is to populate general information of current cart
 * 
 * @author htan3
 *
 */
public class CartDetailGeneralInfoPopulator implements Populator<TargetCartData, CartDetailResponseData> {

    private TargetCheckoutFacade targetCheckoutFacade;

    /* (non-Javadoc)
     * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final TargetCartData source, final CartDetailResponseData target) throws ConversionException {
        target.setId(source.getCode());
        target.setTmid(targetCheckoutFacade.getThreatMatrixSessionID());
        target.setContainsDigitalEntriesOnly(Boolean.valueOf(targetCheckoutFacade.doesCartHaveGiftCardOnly()));
        target.setExcludedForAfterpay(source.isExcludeForAfterpay());
    }

    /**
     * @param targetCheckoutFacade
     *            the targetCheckoutFacade to set
     */
    @Required
    public void setTargetCheckoutFacade(final TargetCheckoutFacade targetCheckoutFacade) {
        this.targetCheckoutFacade = targetCheckoutFacade;
    }

}

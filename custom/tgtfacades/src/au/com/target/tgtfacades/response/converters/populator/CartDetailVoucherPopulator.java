/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;
import au.com.target.tgtfacades.voucher.data.VoucherData;

import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;



/**
 * Populator to populate cart details with voucher data.
 * 
 * @author jjayawa1
 *
 */
public class CartDetailVoucherPopulator implements Populator<TargetCartData, CartDetailResponseData> {

    private TargetCheckoutFacade targetCheckoutFacade;

    /* (non-Javadoc)
     * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final TargetCartData source, final CartDetailResponseData target) throws ConversionException {
        final String appliedVoucher = targetCheckoutFacade.getFirstAppliedVoucher();
        if (StringUtils.isNotEmpty(appliedVoucher)) {
            final VoucherData voucher = new VoucherData();
            voucher.setCode(appliedVoucher);
            voucher.setDiscount(source.getTotalDiscounts());
            target.setVoucher(voucher);
        }
    }

    /**
     * @param targetCheckoutFacade
     *            the targetCheckoutFacade to set
     */
    @Required
    public void setTargetCheckoutFacade(final TargetCheckoutFacade targetCheckoutFacade) {
        this.targetCheckoutFacade = targetCheckoutFacade;
    }

}

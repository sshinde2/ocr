/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang.StringUtils;

import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.tgtfacades.response.data.CommonResponseData;
import au.com.target.tgtfacades.response.data.ProductSearchResponseData;
import au.com.target.tgtfacades.search.data.TargetProductCategorySearchPageData;


/**
 * @author bhuang3
 *
 */
public class ProductSearchCommonDataPopulator
        implements
        Populator<TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData>, ProductSearchResponseData> {

    private static final String REPLACE_PAGE_SIZE = "%7BrecordsPerPage%7D";

    @Override
    public void populate(
            final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> source,
            final ProductSearchResponseData target) throws ConversionException {
        final CommonResponseData commonResponseData = new CommonResponseData();
        final PaginationData paginationData = source.getPagination();
        if (paginationData != null) {
            commonResponseData.setTotalNumberOfResults(paginationData.getTotalNumberOfResults());
            commonResponseData.setPageSize(paginationData.getPageSize());
            commonResponseData.setTotalPages(paginationData.getNumberOfPages());
            commonResponseData.setCurrentPage(paginationData.getCurrentPage());
        }
        if (source.getCurrentQuery() != null) {
            final String pageSize = String.valueOf(commonResponseData.getPageSize());
            final EndecaSearchStateData searchStateData = (EndecaSearchStateData)(source.getCurrentQuery());
            commonResponseData.setCurrentPageUrl(replaceRecordPerPage(searchStateData.getUrl(), pageSize));
            commonResponseData.setCurrentPageWsUrl(replaceRecordPerPage(searchStateData.getWsUrl(), pageSize));
        }
        commonResponseData.setRedirectUrl(source.getKeywordRedirectUrl());
        commonResponseData.setClearAllUrl(source.getClearAllUrl());
        target.setCommonData(commonResponseData);
    }

    private String replaceRecordPerPage(final String url, final String replacement) {
        String result = url;
        if (StringUtils.isNotEmpty(url)) {
            result = StringUtils.replace(url, REPLACE_PAGE_SIZE, replacement);
        }
        return result;
    }

}

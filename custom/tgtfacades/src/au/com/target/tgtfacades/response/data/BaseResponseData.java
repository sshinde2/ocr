/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * This is the base response data object which should be extended for other response data
 * 
 * @author rsamuel3
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class BaseResponseData {
    private Error error;

    /**
     * @return the error
     */
    public Error getError() {
        return error;
    }

    /**
     * @param error
     *            the error to set
     */
    public void setError(final Error error) {
        this.error = error;
    }
}

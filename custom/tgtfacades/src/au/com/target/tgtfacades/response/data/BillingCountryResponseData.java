package au.com.target.tgtfacades.response.data;

import de.hybris.platform.commercefacades.user.data.CountryData;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class BillingCountryResponseData extends BaseResponseData {

    private List<CountryData> countries;

    /**
     * @return the countries
     */
    public List<CountryData> getCountries() {
        return countries;
    }

    /**
     * @param countries
     *            the countries to set
     */
    public void setCountries(final List<CountryData> countries) {
        this.countries = countries;
    }
}

/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * @author bhuang3
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class CommonResponseData {

    private long totalNumberOfResults;

    private int pageSize;

    private int totalPages;

    private int currentPage;

    private String redirectUrl;

    private String clearAllUrl;

    private String clearAllQueryUrl;

    private String currentPageUrl;

    private String currentPageWsUrl;



    /**
     * @return the currentPageUrl
     */
    public String getCurrentPageUrl() {
        return currentPageUrl;
    }

    /**
     * @param currentPageUrl
     *            the currentPageUrl to set
     */
    public void setCurrentPageUrl(final String currentPageUrl) {
        this.currentPageUrl = currentPageUrl;
    }

    /**
     * @return the currentPageWsUrl
     */
    public String getCurrentPageWsUrl() {
        return currentPageWsUrl;
    }

    /**
     * @param currentPageWsUrl
     *            the currentPageWsUrl to set
     */
    public void setCurrentPageWsUrl(final String currentPageWsUrl) {
        this.currentPageWsUrl = currentPageWsUrl;
    }


    /**
     * @return the totalResults
     */
    public long getTotalNumberOfResults() {
        return totalNumberOfResults;
    }

    /**
     * @param totalNumberOfResults
     *            the totalResults to set
     */
    public void setTotalNumberOfResults(final long totalNumberOfResults) {
        this.totalNumberOfResults = totalNumberOfResults;
    }

    /**
     * @return the pageSize
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize
     *            the pageSize to set
     */
    public void setPageSize(final int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * @return the totalPages
     */
    public int getTotalPages() {
        return totalPages;
    }

    /**
     * @param totalPages
     *            the totalPages to set
     */
    public void setTotalPages(final int totalPages) {
        this.totalPages = totalPages;
    }

    /**
     * @return the currentPage
     */
    public int getCurrentPage() {
        return currentPage;
    }

    /**
     * @param currentPage
     *            the currentPage to set
     */
    public void setCurrentPage(final int currentPage) {
        this.currentPage = currentPage;
    }

    /**
     * @return the redirectUrl
     */
    public String getRedirectUrl() {
        return redirectUrl;
    }

    /**
     * @param redirectUrl
     *            the redirectUrl to set
     */
    public void setRedirectUrl(final String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    /**
     * @return the clearAllUrl
     */
    public String getClearAllUrl() {
        return clearAllUrl;
    }

    /**
     * @param clearAllUrl
     *            the clearAllUrl to set
     */
    public void setClearAllUrl(final String clearAllUrl) {
        this.clearAllUrl = clearAllUrl;
    }

    /**
     * @return the clearAllQueryUrl
     */
    public String getClearAllQueryUrl() {
        return clearAllQueryUrl;
    }

    /**
     * @param clearAllQueryUrl
     *            the clearAllQueryUrl to set
     */
    public void setClearAllQueryUrl(final String clearAllQueryUrl) {
        this.clearAllQueryUrl = clearAllQueryUrl;
    }

}

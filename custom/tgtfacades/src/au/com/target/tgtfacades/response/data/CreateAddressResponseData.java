package au.com.target.tgtfacades.response.data;

import de.hybris.platform.commercefacades.user.data.AddressData;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * This class is currently used by /ws-api/v1/{baseSiteId}/checkout/delivery/addresses/create for single page checkout
 * 
 * @author htan3
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class CreateAddressResponseData extends BaseResponseData {
    private AddressData createdAddress;

    public AddressData getCreatedAddress() {
        return createdAddress;
    }

    public void setCreatedAddress(final AddressData createdAddress) {
        this.createdAddress = createdAddress;
    }

}

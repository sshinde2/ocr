/**
 * 
 */
package au.com.target.tgtfacades.response.data;


/**
 * @author bbaral1
 *
 */
public class CurrentLocationResponse {

    private String locationName;

    private String postCode;

    public void setLocationName(final String locationName) {
        this.locationName = locationName;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setPostCode(final String postCode) {
        this.postCode = postCode;
    }

    public String getPostCode() {
        return postCode;
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.response.data;

/**
 * @author rmcalave
 *
 */
public class CustomerRegisteredResponseData extends BaseResponseData {
    private boolean registered;
    private boolean accountLocked;

    /**
     * @return the registered
     */
    public boolean isRegistered() {
        return registered;
    }

    /**
     * @param registered
     *            the registered to set
     */
    public void setRegistered(final boolean registered) {
        this.registered = registered;
    }

    /**
     * @return the accountLocked
     */
    public boolean isAccountLocked() {
        return accountLocked;
    }

    /**
     * @param accountLocked
     *            the accountLocked to set
     */
    public void setAccountLocked(final boolean accountLocked) {
        this.accountLocked = accountLocked;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(" Registered:");
        sb.append(registered);
        sb.append(" accountLocked:");
        sb.append(accountLocked);
        return sb.toString();
    }

}

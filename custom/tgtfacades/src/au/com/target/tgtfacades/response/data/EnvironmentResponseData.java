/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import au.com.target.tgtfacades.order.data.EnvironmentData;


/**
 * Response object to wrap environment data.
 * 
 * @author jjayawa1
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class EnvironmentResponseData extends BaseResponseData {
    private EnvironmentData env;

    /**
     * @return the env
     */
    public EnvironmentData getEnv() {
        return env;
    }

    /**
     * @param env
     *            the env to set
     */
    public void setEnv(final EnvironmentData env) {
        this.env = env;
    }

}

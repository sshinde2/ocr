/**
 * 
 */
package au.com.target.tgtfacades.response.data;

/**
 * @author bhuang3
 *
 */
public class FacetOptionResponseData {

    private String id;

    private String name;

    private boolean selected;

    private String url;

    private String queryUrl;

    private long count;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * @param selected
     *            the selected to set
     */
    public void setSelected(final boolean selected) {
        this.selected = selected;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     *            the url to set
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    /**
     * @return the queryUrl
     */
    public String getQueryUrl() {
        return queryUrl;
    }

    /**
     * @param queryUrl
     *            the queryUrl to set
     */
    public void setQueryUrl(final String queryUrl) {
        this.queryUrl = queryUrl;
    }

    /**
     * @return the count
     */
    public long getCount() {
        return count;
    }

    /**
     * @param count
     *            the count to set
     */
    public void setCount(final long count) {
        this.count = count;
    }


}

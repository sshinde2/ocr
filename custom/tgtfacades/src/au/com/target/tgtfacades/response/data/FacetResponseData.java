/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import java.util.List;


/**
 * @author bhuang3
 *
 */
public class FacetResponseData {

    private String id;

    private String name;

    private boolean multiSelect;

    private List<FacetOptionResponseData> facetOptionList;

    private List<FacetResponseData> facetList;

    private boolean expand;

    private boolean isolated;

    private boolean seoFollowEnabled;

    /**
     * @return the seoFollowEnabled
     */
    public boolean isSeoFollowEnabled() {
        return seoFollowEnabled;
    }

    /**
     * @param seoFollowEnabled
     *            the seoFollowEnabled to set
     */
    public void setSeoFollowEnabled(final boolean seoFollowEnabled) {
        this.seoFollowEnabled = seoFollowEnabled;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the multiSelect
     */
    public boolean isMultiSelect() {
        return multiSelect;
    }

    /**
     * @param multiSelect
     *            the multiSelect to set
     */
    public void setMultiSelect(final boolean multiSelect) {
        this.multiSelect = multiSelect;
    }

    /**
     * @return the facetOptionList
     */
    public List<FacetOptionResponseData> getFacetOptionList() {
        return facetOptionList;
    }

    /**
     * @param facetOptionList
     *            the facetOptionList to set
     */
    public void setFacetOptionList(final List<FacetOptionResponseData> facetOptionList) {
        this.facetOptionList = facetOptionList;
    }

    /**
     * @return the facetList
     */
    public List<FacetResponseData> getFacetList() {
        return facetList;
    }

    /**
     * @param facetList
     *            the facetList to set
     */
    public void setFacetList(final List<FacetResponseData> facetList) {
        this.facetList = facetList;
    }

    /**
     * @return the expand
     */
    public boolean isExpand() {
        return expand;
    }

    /**
     * @param expand
     *            the expand to set
     */
    public void setExpand(final boolean expand) {
        this.expand = expand;
    }

    /**
     * @return the isolated
     */
    public boolean isIsolated() {
        return isolated;
    }

    /**
     * @param isolated
     *            the isolated to set
     */
    public void setIsolated(final boolean isolated) {
        this.isolated = isolated;
    }

}

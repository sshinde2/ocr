/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * @author bhuang3
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class FlybuysResponseData {

    private String code;

    private boolean canRedeemPoints;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the canRedeemPoints
     */
    public boolean isCanRedeemPoints() {
        return canRedeemPoints;
    }

    /**
     * @param canRedeemPoints
     *            the canRedeemPoints to set
     */
    public void setCanRedeemPoints(final boolean canRedeemPoints) {
        this.canRedeemPoints = canRedeemPoints;
    }



}

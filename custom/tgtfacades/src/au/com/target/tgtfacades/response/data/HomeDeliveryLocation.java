/**
 * 
 */
package au.com.target.tgtfacades.response.data;

/**
 * @author bbaral1
 *
 */
public class HomeDeliveryLocation extends BaseResponseData {

    private String suburb;
    private String postalCode;
    private int hdShortLeadTime;
    private int edShortLeadTime;
    private int bulky1ShortLeadTime;
    private int bulky2ShortLeadTime;

    /**
     * @return the suburb
     */
    public String getSuburb() {
        return suburb;
    }

    /**
     * @return the postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * @param suburb
     *            the suburb to set
     */
    public void setSuburb(final String suburb) {
        this.suburb = suburb;
    }

    /**
     * @param postalCode
     *            the postalCode to set
     */
    public void setPostalCode(final String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * @return the hdShortLeadTime
     */
    public int getHdShortLeadTime() {
        return hdShortLeadTime;
    }

    /**
     * @param hdShortLeadTime
     *            the hdShortLeadTime to set
     */
    public void setHdShortLeadTime(final int hdShortLeadTime) {
        this.hdShortLeadTime = hdShortLeadTime;
    }

    /**
     * @return edShortLeadTime
     */
    public int getEdShortLeadTime() {
        return edShortLeadTime;
    }

    /**
     * @param edShortLeadTime
     *            the edShortLeadTime to set
     */
    public void setEdShortLeadTime(final int edShortLeadTime) {
        this.edShortLeadTime = edShortLeadTime;
    }

    /**
     * @return bulky1ShortLeadTime
     */
    public int getBulky1ShortLeadTime() {
        return bulky1ShortLeadTime;
    }

    /**
     * @param bulky1ShortLeadTime
     *            the bulky1ShortLeadTime to set
     */
    public void setBulky1ShortLeadTime(final int bulky1ShortLeadTime) {
        this.bulky1ShortLeadTime = bulky1ShortLeadTime;
    }

    /**
     * @return bulky2ShortLeadTime
     */
    public int getBulky2ShortLeadTime() {
        return bulky2ShortLeadTime;
    }

    /**
     * @param bulky2ShortLeadTime
     *            the bulky2ShortLeadTime to set
     */
    public void setBulky2ShortLeadTime(final int bulky2ShortLeadTime) {
        this.bulky2ShortLeadTime = bulky2ShortLeadTime;
    }
}

/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceStockData;


/**
 * @author bbaral1
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class LocationResponseData extends BaseResponseData {

    private TargetPointOfServiceStockData clickAndCollectPos;
    private HomeDeliveryLocation homeDeliveryPos;

    /**
     * @return the clickAndCollectPos
     */
    public TargetPointOfServiceStockData getClickAndCollectPos() {
        return clickAndCollectPos;
    }

    /**
     * @param clickAndCollectPos
     *            the clickAndCollectPos to set
     */
    public void setClickAndCollectPos(final TargetPointOfServiceStockData clickAndCollectPos) {
        this.clickAndCollectPos = clickAndCollectPos;
    }

    /**
     * @return the homeDeliveryPos
     */
    public HomeDeliveryLocation getHomeDeliveryPos() {
        return homeDeliveryPos;
    }

    /**
     * @param homeDeliveryPos
     *            the homeDeliveryPos to set
     */
    public void setHomeDeliveryPos(final HomeDeliveryLocation homeDeliveryPos) {
        this.homeDeliveryPos = homeDeliveryPos;
    }
}

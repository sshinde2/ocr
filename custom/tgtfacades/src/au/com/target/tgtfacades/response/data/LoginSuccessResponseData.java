/**
 * 
 */
package au.com.target.tgtfacades.response.data;

/**
 * @author rmcalave
 *
 */
public class LoginSuccessResponseData extends BaseResponseData {
    private String redirectUrl;

    /**
     * @return the redirectUrl
     */
    public String getRedirectUrl() {
        return redirectUrl;
    }

    /**
     * @param redirectUrl
     *            the redirectUrl to set
     */
    public void setRedirectUrl(final String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    @Override
    public String toString() {
        return "redirectUrl" + ":" + redirectUrl;
    }

}

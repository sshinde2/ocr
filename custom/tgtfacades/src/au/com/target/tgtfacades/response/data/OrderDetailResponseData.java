/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;


/**
 * @author mgazal
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class OrderDetailResponseData extends BaseResponseData {

    private OrderDetailsData orderDetails;

    private OrderDetailGaData orderDetailGaData;

    private AdjustedCartEntriesData sohUpdates;

    /**
     * @return the orderDetails
     */
    public OrderDetailsData getOrderDetails() {
        return orderDetails;
    }

    /**
     * @param orderDetails
     *            the orderDetails to set
     */
    public void setOrderDetails(final OrderDetailsData orderDetails) {
        this.orderDetails = orderDetails;
    }

    /**
     * @return the orderDetailGaData
     */
    public OrderDetailGaData getOrderDetailGaData() {
        return orderDetailGaData;
    }

    /**
     * @param orderDetailGaData
     *            the orderDetailGaData to set
     */
    public void setOrderDetailGaData(final OrderDetailGaData orderDetailGaData) {
        this.orderDetailGaData = orderDetailGaData;
    }

    /**
     * @return the sohUpdates
     */
    public AdjustedCartEntriesData getSohUpdates() {
        return sohUpdates;
    }

    /**
     * @param sohUpdates
     *            the sohUpdates to set
     */
    public void setSohUpdates(final AdjustedCartEntriesData sohUpdates) {
        this.sohUpdates = sohUpdates;
    }

}

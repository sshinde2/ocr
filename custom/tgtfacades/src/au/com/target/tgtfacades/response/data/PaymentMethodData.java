/**
 * 
 */
package au.com.target.tgtfacades.response.data;

/**
 * Payment method data place holder.
 * 
 * @author jjayawa1
 *
 */
public class PaymentMethodData {
    private String name;
    private boolean available;
    private String reason;
    private boolean retryRequired;

    /**
     * @param name
     * @param available
     */
    public PaymentMethodData(final String name, final boolean available) {
        super();
        this.name = name;
        this.available = available;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the available
     */
    public boolean isAvailable() {
        return available;
    }

    /**
     * @param available
     *            the available to set
     */
    public void setAvailable(final boolean available) {
        this.available = available;
    }

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason
     *            the reason to set
     */
    public void setReason(final String reason) {
        this.reason = reason;
    }

    /**
     * @return the retryRequired
     */
    public boolean isRetryRequired() {
        return retryRequired;
    }

    /**
     * @param retryRequired
     *            the retryRequired to set
     */
    public void setRetryRequired(final boolean retryRequired) {
        this.retryRequired = retryRequired;
    }

}

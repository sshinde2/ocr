/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * @author bhuang3
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ProductSearchResponseData extends BaseResponseData {

    private CommonResponseData commonData;

    private List<SortResponseData> sortDataList;

    private List<FacetResponseData> facetList;

    private List<ProductResponseData> productDataList;

    /**
     * @return the commonData
     */
    public CommonResponseData getCommonData() {
        return commonData;
    }

    /**
     * @param commonData
     *            the commonData to set
     */
    public void setCommonData(final CommonResponseData commonData) {
        this.commonData = commonData;
    }

    /**
     * @return the sortDataList
     */
    public List<SortResponseData> getSortDataList() {
        return sortDataList;
    }

    /**
     * @param sortDataList
     *            the sortDataList to set
     */
    public void setSortDataList(final List<SortResponseData> sortDataList) {
        this.sortDataList = sortDataList;
    }

    /**
     * @return the facetList
     */
    public List<FacetResponseData> getFacetList() {
        return facetList;
    }

    /**
     * @param facetList
     *            the facetList to set
     */
    public void setFacetList(final List<FacetResponseData> facetList) {
        this.facetList = facetList;
    }

    /**
     * @return the productDataList
     */
    public List<ProductResponseData> getProductDataList() {
        return productDataList;
    }

    /**
     * @param productDataList
     *            the productDataList to set
     */
    public void setProductDataList(final List<ProductResponseData> productDataList) {
        this.productDataList = productDataList;
    }

}

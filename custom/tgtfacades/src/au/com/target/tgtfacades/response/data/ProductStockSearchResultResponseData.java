/**
 * 
 */
package au.com.target.tgtfacades.response.data;


import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceStockData;


/**
 * @author htan3
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ProductStockSearchResultResponseData extends BaseResponseData {

    private String selectedLocation;

    private String availabilityTime;

    private boolean stockDataIncluded;

    private boolean stockAvailable;

    private List<TargetPointOfServiceStockData> stores;

    /**
     * @return the stores
     */
    public List<TargetPointOfServiceStockData> getStores() {
        return stores;
    }

    /**
     * @param stores
     *            the stores to set
     */
    public void setStores(final List<TargetPointOfServiceStockData> stores) {
        this.stores = stores;
    }

    /**
     * @return the selectedLocation
     */
    public String getSelectedLocation() {
        return selectedLocation;
    }

    /**
     * @param selectedLocation
     *            the selectedLocation to set
     */
    public void setSelectedLocation(final String selectedLocation) {
        this.selectedLocation = selectedLocation;
    }

    /**
     * @return the availabilityTime
     */
    public String getAvailabilityTime() {
        return availabilityTime;
    }

    /**
     * @param availabilityTime
     *            the availabilityTime to set
     */
    public void setAvailabilityTime(final String availabilityTime) {
        this.availabilityTime = availabilityTime;
    }

    /**
     * @return the stockDataIncluded
     */
    public boolean isStockDataIncluded() {
        return stockDataIncluded;
    }

    /**
     * @param stockDataIncluded
     *            the stockDataIncluded to set
     */
    public void setStockDataIncluded(final boolean stockDataIncluded) {
        this.stockDataIncluded = stockDataIncluded;
    }

    /**
     * @return the stockAvailable
     */
    public boolean isStockAvailable() {
        return stockAvailable;
    }

    /**
     * @param stockAvailable
     *            the stockAvailable to set
     */
    public void setStockAvailable(final boolean stockAvailable) {
        this.stockAvailable = stockAvailable;
    }
}

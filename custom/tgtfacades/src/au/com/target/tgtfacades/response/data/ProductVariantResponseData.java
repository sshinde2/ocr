/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import au.com.target.tgtcore.product.data.ProductDisplayType;



/**
 * @author bhuang3
 *
 */
public class ProductVariantResponseData {

    private String thumbImageUrl;

    private String gridImageUrl;

    private String colourVariantCode;

    private String url;

    private boolean inStock;

    private String colourName;

    private String swatchColour;

    private String displayPrice;

    private String displayWasPrice;

    private Boolean assorted;

    private Boolean displayOnly;

    private ProductDisplayType productDisplayType;

    private String normalSaleStartDate;

    /**
     * @return the thumbImageUrl
     */
    public String getThumbImageUrl() {
        return thumbImageUrl;
    }

    /**
     * @param thumbImageUrl
     *            the thumbImageUrl to set
     */
    public void setThumbImageUrl(final String thumbImageUrl) {
        this.thumbImageUrl = thumbImageUrl;
    }

    /**
     * @return the gridImageUrl
     */
    public String getGridImageUrl() {
        return gridImageUrl;
    }

    /**
     * @param gridImageUrl
     *            the gridImageUrl to set
     */
    public void setGridImageUrl(final String gridImageUrl) {
        this.gridImageUrl = gridImageUrl;
    }

    /**
     * @return the assorted
     */
    public Boolean getAssorted() {
        return assorted;
    }

    /**
     * @return the colourVariantCode
     */
    public String getColourVariantCode() {
        return colourVariantCode;
    }

    /**
     * @param colourVariantCode
     *            the colourVariantCode to set
     */
    public void setColourVariantCode(final String colourVariantCode) {
        this.colourVariantCode = colourVariantCode;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     *            the url to set
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    /**
     * @return the inStock
     */
    public boolean isInStock() {
        return inStock;
    }

    /**
     * @param inStock
     *            the inStock to set
     */
    public void setInStock(final boolean inStock) {
        this.inStock = inStock;
    }

    /**
     * @return the colourName
     */
    public String getColourName() {
        return colourName;
    }

    /**
     * @param colourName
     *            the colourName to set
     */
    public void setColourName(final String colourName) {
        this.colourName = colourName;
    }

    /**
     * @return the swatchColour
     */
    public String getSwatchColour() {
        return swatchColour;
    }

    /**
     * @param swatchColour
     *            the swatchColour to set
     */
    public void setSwatchColour(final String swatchColour) {
        this.swatchColour = swatchColour;
    }

    /**
     * @return the displayPrice
     */
    public String getDisplayPrice() {
        return displayPrice;
    }

    /**
     * @param displayPrice
     *            the displayPrice to set
     */
    public void setDisplayPrice(final String displayPrice) {
        this.displayPrice = displayPrice;
    }

    /**
     * @return the displayWasPrice
     */
    public String getDisplayWasPrice() {
        return displayWasPrice;
    }

    /**
     * @param displayWasPrice
     *            the displayWasPrice to set
     */
    public void setDisplayWasPrice(final String displayWasPrice) {
        this.displayWasPrice = displayWasPrice;
    }

    /**
     * @param assorted
     *            the assorted to set
     */
    public void setAssorted(final Boolean assorted) {
        this.assorted = assorted;
    }

    /**
     * @return the displayOnly
     */
    public Boolean getDisplayOnly() {
        return displayOnly;
    }

    /**
     * @param displayOnly
     *            the displayOnly to set
     */
    public void setDisplayOnly(final Boolean displayOnly) {
        this.displayOnly = displayOnly;
    }

    /**
     * @return the productDisplayType
     */
    public ProductDisplayType getProductDisplayType() {
        return productDisplayType;
    }

    /**
     * @param productDisplayType
     *            the productDisplayType to set
     */
    public void setProductDisplayType(final ProductDisplayType productDisplayType) {
        this.productDisplayType = productDisplayType;
    }

    /**
     * @return the normalSaleStartDate
     */
    public String getNormalSaleStartDate() {
        return normalSaleStartDate;
    }

    /**
     * @param normalSaleStartDate
     *            the normalSaleStartDate to set
     */
    public void setNormalSaleStartDate(final String normalSaleStartDate) {
        this.normalSaleStartDate = normalSaleStartDate;
    }

}

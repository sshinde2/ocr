/**
 * 
 */
package au.com.target.tgtfacades.response.data;

/**
 * @author mgazal
 *
 */
public class RegisterUserResponseData extends BaseResponseData {

    private String email;

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }
}

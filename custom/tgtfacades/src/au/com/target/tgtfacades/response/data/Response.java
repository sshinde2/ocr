/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * @author rsamuel3
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Response {
    private final boolean success;
    private BaseResponseData data;


    public Response(final boolean success) {
        this.success = success;
    }

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @return the data
     */
    public BaseResponseData getData() {
        return data;
    }

    /**
     * @param data
     *            the data to set
     */
    public void setData(final BaseResponseData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SUCCESS:");
        sb.append(success);
        if (!success && data != null && data.getError() != null) {
            sb.append(" ");
            sb.append(data.getError().toString());
        }
        return sb.toString();
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import au.com.target.tgtfacades.order.data.SessionData;


/**
 * Response object to wrap global session data.
 * 
 * @author jjayawa1
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class SessionResponseData extends BaseResponseData {
    private SessionData session;

    /**
     * @return the session
     */
    public SessionData getSession() {
        return session;
    }

    /**
     * @param session
     *            the session to set
     */
    public void setSession(final SessionData session) {
        this.session = session;
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.response.data;

/**
 * @author gsing236
 *
 */
public class ShipsterEmailVerificationResponseData extends BaseResponseData {

    private boolean deliveryClubMember;

    /**
     * @return the deliveryClubMember
     */
    public boolean isDeliveryClubMember() {
        return deliveryClubMember;
    }

    /**
     * @param deliveryClubMember
     *            the deliveryClubMember to set
     */
    public void setDeliveryClubMember(final boolean deliveryClubMember) {
        this.deliveryClubMember = deliveryClubMember;
    }
}

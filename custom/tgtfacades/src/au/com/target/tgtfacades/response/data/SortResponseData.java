/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * @author bhuang3
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class SortResponseData {

    private String queryUrl;

    private String url;

    private boolean selected;

    private String name;

    /**
     * @return the queryUrl
     */
    public String getQueryUrl() {
        return queryUrl;
    }

    /**
     * @param queryUrl
     *            the queryUrl to set
     */
    public void setQueryUrl(final String queryUrl) {
        this.queryUrl = queryUrl;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     *            the url to set
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    /**
     * @return the selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * @param selected
     *            the selected to set
     */
    public void setSelected(final boolean selected) {
        this.selected = selected;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

}

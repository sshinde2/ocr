/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import java.util.Date;
import java.util.List;

import au.com.target.tgtfluent.data.StockDatum;


/**
 * @author mgazal
 *
 */
public class StockResponseData extends BaseResponseData {

    private List<StockDatum> stockData;

    private Date availabilityTime;

    /**
     * @param stockData
     */
    public StockResponseData(final List<StockDatum> stockData) {
        super();
        this.stockData = stockData;
        availabilityTime = new Date();
    }

    /**
     * @return the stockData
     */
    public List<StockDatum> getStockData() {
        return stockData;
    }

    /**
     * @param stockData
     *            the stockData to set
     */
    public void setStockData(final List<StockDatum> stockData) {
        this.stockData = stockData;
    }

    /**
     * @return the availabilityTime
     */
    public Date getAvailabilityTime() {
        return availabilityTime;
    }

    /**
     * @param availabilityTime
     *            the availabilityTime to set
     */
    public void setAvailabilityTime(final Date availabilityTime) {
        this.availabilityTime = availabilityTime;
    }
}

package au.com.target.tgtfacades.response.data;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class StoreResponseData extends BaseResponseData {

    private Integer storeNumber;

    private TargetPointOfServiceData storeDetails;

    public Integer getStoreNumber() {
        return storeNumber;
    }

    public void setStoreNumber(final Integer storeNumber) {
        this.storeNumber = storeNumber;
    }

    public TargetPointOfServiceData getStoreDetails() {
        return storeDetails;
    }

    public void setStoreDetails(final TargetPointOfServiceData storeDetails) {
        this.storeDetails = storeDetails;
    }

}

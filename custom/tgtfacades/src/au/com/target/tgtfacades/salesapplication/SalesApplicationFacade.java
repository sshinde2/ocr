/**
 * 
 */
package au.com.target.tgtfacades.salesapplication;

import de.hybris.platform.commerceservices.enums.SalesApplication;

import javax.servlet.http.HttpServletRequest;


/**
 * Facade to detect sales application
 * 
 * @author jjayawa1
 * 
 */
public interface SalesApplicationFacade {

    /**
     * Initialize sales application detection for the specified request.
     * 
     * @param request
     *            the request
     */
    void initializeRequest(HttpServletRequest request);

    /**
     * Get the currently active sales application.
     * 
     * @return String
     */
    SalesApplication getCurrentSalesApplication();

    /**
     * Check whether the current application is Kiosk
     * 
     * @return boolean
     */
    boolean isKioskApplication();

    /**
     * Check whether the current application is Mobile-App
     * 
     * @return boolean
     */
    boolean isSalesChannelMobileApp();
}

/**
 * 
 */
package au.com.target.tgtfacades.search;

import au.com.target.tgtfacades.product.data.AbstractTargetProductData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;


/**
 * @author dcwillia
 * 
 */
public interface TargetProductHideStrategy {
    /**
     * Is there a reason this product should not be visible on the product listing/carousels
     * 
     * @param productData
     * @return true if we need to hide this product
     */
    boolean hideThisProduct(final AbstractTargetProductData productData);

    /**
     * This determines whether the product is out of stock and/or whether the show when out of stock flag is set
     * 
     * @param variantData
     * @return true if this needs to be hidden and false otherwise
     */
    boolean hideThisProduct(final TargetVariantProductListerData variantData);
}

/**
 * 
 */
package au.com.target.tgtfacades.search.data;

import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;

import java.util.List;

import au.com.target.tgtfacades.sort.TargetSortData;


/**
 * @author schen13
 * 
 */
public class TargetProductCategorySearchPageData<STATE, RESULT, CATEGORY> extends
        ProductCategorySearchPageData {
    private boolean isExternalServerDown;

    private List<String> spellingSuggestions;

    private String autoCorrectedTerm;

    private List<TargetSortData> targetSortData;

    private String errorStatus;

    private String clearAllUrl;

    private String clearAllQueryUrl;

    private boolean showClearAllUrl;

    private Integer bulkyBoardNumber;

    /**
     * @param isExternalServerDown
     *            to set isExternalServerDown
     */
    public void setIsExternalServerDown(final boolean isExternalServerDown) {
        this.isExternalServerDown = isExternalServerDown;
    }

    /**
     * @return the isExternalServerDown
     */
    public boolean isExternalServerDown() {
        return this.isExternalServerDown;
    }

    /**
     * @return the targetSortData
     */
    public List<TargetSortData> getTargetSortData() {
        return targetSortData;
    }

    /**
     * @param targetSortData
     *            the targetSortData to set
     */
    public void setTargetSortData(final List<TargetSortData> targetSortData) {
        this.targetSortData = targetSortData;
    }

    /**
     * Get did you mean values
     * 
     * @return the spellingSuggestions
     */
    public List<String> getSpellingSuggestions() {
        return spellingSuggestions;
    }

    /**
     * Set did you mean values.
     * 
     * @param spellingSuggestions
     *            the spellingSuggestions to set
     */
    public void setSpellingSuggestions(final List<String> spellingSuggestions) {
        this.spellingSuggestions = spellingSuggestions;
    }

    /**
     * Get term with correct spelling.
     * 
     * @return the autoCorrectedTerm
     */
    public String getAutoCorrectedTerm() {
        return autoCorrectedTerm;
    }

    /**
     * . Set term with correct spelling.
     * 
     * @param autoCorrectedTerm
     *            the autoCorrectedTerm to set
     */
    public void setAutoCorrectedTerm(final String autoCorrectedTerm) {
        this.autoCorrectedTerm = autoCorrectedTerm;
    }

    /**
     * @return the errorStatus
     */
    public String getErrorStatus() {
        return errorStatus;
    }

    /**
     * @param errorStatus
     *            the errorStatus to set
     */
    public void setErrorStatus(final String errorStatus) {
        this.errorStatus = errorStatus;
    }

    /**
     * @return the clearAllUrl
     */
    public String getClearAllUrl() {
        return clearAllUrl;
    }

    /**
     * @param clearAllUrl
     *            the clearAllUrl to set
     */
    public void setClearAllUrl(final String clearAllUrl) {
        this.clearAllUrl = clearAllUrl;
    }

    /**
     * @return the clearAllQueryUrl
     */
    public String getClearAllQueryUrl() {
        return clearAllQueryUrl;
    }

    /**
     * @param clearAllQueryUrl
     *            the clearAllQueryUrl to set
     */
    public void setClearAllQueryUrl(final String clearAllQueryUrl) {
        this.clearAllQueryUrl = clearAllQueryUrl;
    }

    /**
     * @return the showClearAllUrl
     */
    public boolean isShowClearAllUrl() {
        return showClearAllUrl;
    }

    /**
     * @param showClearAllUrl
     *            the showClearAllUrl to set
     */
    public void setShowClearAllUrl(final boolean showClearAllUrl) {
        this.showClearAllUrl = showClearAllUrl;
    }

    /**
     * @return the bulkyBoardNumber
     */
    public Integer getBulkyBoardNumber() {
        return bulkyBoardNumber;
    }

    /**
     * @param bulkyBoardNumber
     *            the bulkyBoardNumber to set
     */
    public void setBulkyBoardNumber(final Integer bulkyBoardNumber) {
        this.bulkyBoardNumber = bulkyBoardNumber;
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.search.data;

import de.hybris.platform.commercefacades.search.data.SearchStateData;

import au.com.target.tgtcore.model.BrandModel;


/**
 * Extend SearchStateData so it knows about brand searching.
 * 
 */
public class TargetSearchStateData extends SearchStateData {
    private BrandModel brand;

    /**
     * @return the brand
     */
    public BrandModel getBrand() {
        return brand;
    }

    /**
     * @param brand
     *            the brand to set
     */
    public void setBrand(final BrandModel brand) {
        this.brand = brand;
    }
}

/**
 * 
 */
package au.com.target.tgtfacades.search.impl;

import au.com.target.tgtfacades.product.data.AbstractTargetProductData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;


/**
 * Hide the out of stock product only if the showWhenSoldOut flag is not set
 */
public class ConditionalOutOfStockHideStrategy extends OutOfStockHideStrategy {
    @Override
    public boolean hideThisProduct(final AbstractTargetProductData productData) {
        if (productData.isShowWhenOutOfStock()) {
            return false;
        }
        return super.hideThisProduct(productData);
    }

    @Override
    public boolean hideThisProduct(final TargetVariantProductListerData variantData) {
        if (variantData.isShowWhenOutOfStock()) {
            return false;
        }
        return super.hideThisProduct(variantData);
    }
}

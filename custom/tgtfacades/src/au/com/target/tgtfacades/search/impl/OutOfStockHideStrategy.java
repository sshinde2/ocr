/**
 * 
 */
package au.com.target.tgtfacades.search.impl;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;

import au.com.target.tgtfacades.product.data.AbstractTargetProductData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.search.TargetProductHideStrategy;


/**
 * Hide this product if it is out of stock
 */
public class OutOfStockHideStrategy implements TargetProductHideStrategy {

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.search.TargetProductHideStrategy#hideThisProduct(au.com.target.tgtfacades.product.data.TargetProductData)
     */
    @Override
    public boolean hideThisProduct(final AbstractTargetProductData productData) {
        return StockLevelStatus.OUTOFSTOCK.equals(productData.getStock().getStockLevelStatus());
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.search.TargetProductHideStrategy#hideThisProduct(au.com.target.tgtfacades.product.data.TargetVariantProductListerData)
     */
    @Override
    public boolean hideThisProduct(final TargetVariantProductListerData variantData) {
        return !variantData.isInStock();
    }
}

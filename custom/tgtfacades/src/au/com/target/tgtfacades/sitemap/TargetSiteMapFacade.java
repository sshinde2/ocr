/**
 * 
 */
package au.com.target.tgtfacades.sitemap;

import java.util.List;

import com.endeca.navigation.ENEQueryException;

import au.com.target.endeca.infront.exception.TargetEndecaException;


/**
 * @author pvarghe2
 *
 */
public interface TargetSiteMapFacade {
    /**
     * Method to get the url of all the inStock colour variants in a particular merchandize department.
     * 
     * @param merchCode
     * @param pageNumber
     * @param productsPerPage
     * @return List
     * @throws TargetEndecaException
     * @throws ENEQueryException
     */
    List<String> getSiteMapColourVariantUrl(int merchCode, int pageNumber, int productsPerPage)
            throws TargetEndecaException, ENEQueryException;
}

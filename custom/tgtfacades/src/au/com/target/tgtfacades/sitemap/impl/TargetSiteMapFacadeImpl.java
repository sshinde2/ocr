/**
 * 
 */
package au.com.target.tgtfacades.sitemap.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.endeca.navigation.AggrERec;
import com.endeca.navigation.AggrERecList;
import com.endeca.navigation.AssocDimLocations;
import com.endeca.navigation.DimLocation;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.Navigation;
import com.endeca.navigation.PropertyMap;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.querybuilder.TargetSiteMapQueryBuilder;
import au.com.target.tgtfacades.sitemap.TargetSiteMapFacade;
import au.com.target.tgtfacades.url.impl.TargetProductDataUrlResolver;


/**
 * @author pvarghe2
 *
 */
public class TargetSiteMapFacadeImpl implements TargetSiteMapFacade {

    private TargetProductDataUrlResolver targetProductDataUrlResolver;

    @Resource(name = "targetSiteMapQueryBuilder")
    private TargetSiteMapQueryBuilder targetSiteMapQueryBuilder;

    @Override
    public List<String> getSiteMapColourVariantUrl(final int merchCode, final int pageNumber,
            final int productsPerPage) throws TargetEndecaException, ENEQueryException {
        final ENEQueryResults queryResults = targetSiteMapQueryBuilder.getQueryResults(merchCode,
                pageNumber,
                productsPerPage);
        if (null != queryResults && null != queryResults.getNavigation()) {
            final Navigation navigation = queryResults.getNavigation();
            final AggrERecList aggregateERecs = navigation.getAggrERecs();
            if (CollectionUtils.isNotEmpty(aggregateERecs)) {
                return getAllInstockProductUrl(aggregateERecs);
            }
        }
        return Collections.emptyList();
    }

    /**
     * @param aggregateERecs
     * @return List<String>
     */
    private List<String> getAllInstockProductUrl(final AggrERecList aggregateERecs) {
        final List<String> productsUrl = new ArrayList<>();
        for (final Object eRecObject : aggregateERecs) {
            final AggrERec aggrERec = (AggrERec)eRecObject;
            if (isInStock(aggrERec)) {
                productsUrl.add(createProductUrl(aggrERec));
            }
            else {
                break;
            }
        }
        return productsUrl;
    }

    /**
     * 
     * @param aggrERec
     * @return String
     */
    private boolean isInStock(final AggrERec aggrERec) {
        final PropertyMap representativeRecordProperties = aggrERec.getRepresentative().getProperties();
        if (representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_INSTOCK_FLAG)) {
            return ((String)representativeRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_INSTOCK_FLAG))
                            .equals(EndecaConstants.ENDECA_BOOLEAN_VALUE);
        }
        return false;
    }

    /**
     * @param aggrERec
     * @return String
     */
    private String createProductUrl(final AggrERec aggrERec) {
        String productName = null;
        String productCode = null;
        for (final Iterator<AssocDimLocations> it = aggrERec.getRepresentative().getDimValues().iterator(); it
                .hasNext();) {
            final AssocDimLocations dimLoc = it.next();
            if (dimLoc.getDimRoot().getDimensionName()
                    .equals(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_NAME)) {
                productName = ((DimLocation)dimLoc.get(0)).getDimValue().getName();
                break;
            }
        }
        final PropertyMap representativeRecordProperties = aggrERec.getRepresentative().getProperties();
        if (representativeRecordProperties
                .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE)) {
            productCode = (String)representativeRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        }
        return targetProductDataUrlResolver.resolve(productName, productCode);
    }

    /**
     * @param targetProductDataUrlResolver
     */
    @Required
    public void setTargetProductDataUrlResolver(final TargetProductDataUrlResolver targetProductDataUrlResolver) {
        this.targetProductDataUrlResolver = targetProductDataUrlResolver;
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.size.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtfacades.size.SizeChartFacade;
import au.com.target.tgtwebcore.model.SizeChartModel;
import au.com.target.tgtwebcore.size.chart.SizeChartService;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class SizeChartFacadeImpl implements SizeChartFacade {

    protected static final Logger LOG = Logger.getLogger(SizeChartFacadeImpl.class);

    private SizeChartService sizeChartService;

    @Override
    public boolean isSizeChart(final String sizeType) {
        SizeChartModel sizeChartModel = null;
        try {
            sizeChartModel = sizeChartService.getSizeChartBySizeName(sizeType);
        }
        catch (final TargetUnknownIdentifierException e) {
            //We don't need to do anything it means the given size must not show any size chart
            return false;
        }
        catch (final TargetAmbiguousIdentifierException e) {
            LOG.error("Ambigious size name : " + sizeType);
            return false;
        }
        return sizeChartModel != null;
    }

    /**
     * @param sizeChartService
     *            the sizeChartService to set
     */
    @Required
    public void setSizeChartService(final SizeChartService sizeChartService) {
        this.sizeChartService = sizeChartService;
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.stl;

import java.util.List;

import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtfacades.looks.data.ShopTheLookPageData;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;


/**
 * @author bhuang3
 *
 */
public interface TargetShopTheLookFacade {

    /**
     * Get the list of visible looks given a collectionCode.
     * 
     * @param collectionCode
     * @return list of {@link TargetLookModel}
     */
    List<TargetLookModel> getVisibleLooksForCollection(String collectionCode);

    /**
     * Get shop the look model by code
     * 
     * @param code
     * @return TargetShopTheLookModel
     */
    TargetShopTheLookModel getShopTheLookByCode(String code);

    /**
     * populate shopTheLookPageData by shopTheLook code
     * 
     * @param code
     * @return ShopTheLookPageData
     */
    ShopTheLookPageData populateShopTheLookPageData(String code);

    /**
     * Get look by code
     * 
     * @param code
     * @return TargetLookModel
     */
    TargetLookModel getLookByCode(String code);

    /**
     * Populate look details data
     * 
     * @param code
     * @return LookDetailsData
     */
    LookDetailsData populateLookDetailsData(String code);

    /**
     * Get a list of the product code by look id
     * 
     * @param code
     * @return List<String>
     */
    List<String> getProductCodeListByLook(String code);

    /**
     * Populate a list of look details data
     * 
     * @param targetLookModels
     * @return List<LookDetailsData>
     */
    List<LookDetailsData> populateLookDetailsDataList(List<TargetLookModel> targetLookModels);

    /**
     * @param code
     * @return TargetShopTheLookModel
     */
    TargetShopTheLookModel getShopTheLookByCategoryCode(String code);
}

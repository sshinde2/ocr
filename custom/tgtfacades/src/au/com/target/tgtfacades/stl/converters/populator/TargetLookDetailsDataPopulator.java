/**
 * 
 */
package au.com.target.tgtfacades.stl.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.looks.data.ImageContainer;
import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtfacades.stl.TargetShopTheLookFacade;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;
import au.com.target.tgtmarketing.model.TargetLookModel;


/**
 * @author bhuang3
 *
 */
public class TargetLookDetailsDataPopulator implements Populator<TargetLookModel, LookDetailsData> {

    private static final String FULL_IMAGE = "full";

    private static final String GRID_IMAGE = "grid";

    private static final String HERO_IMAGE = "hero";

    private static final String SWATCH_IMAGE = "swatch";

    private static final String LIST_IMAGE = "list";

    private static final String LARGE_IMAGE = "large";

    private static final String THUMB_IMAGE = "thumb";

    private TargetShopTheLookFacade targetShopTheLookFacade;

    @Override
    public void populate(final TargetLookModel source, final LookDetailsData target) throws ConversionException {
        if (source != null) {
            target.setVisible(true);
            target.setName(source.getName());
            target.setId(source.getId());
            target.setDescription(source.getDescription());
            target.setUrl(source.getUrl());
            final TargetLookCollectionModel collectionModel = source.getCollection();
            if (collectionModel != null) {
                target.setCollectionId(collectionModel.getId());
                target.setCollectionName(collectionModel.getName());
                target.setCollectionUrl(collectionModel.getUrl());
            }
            if (null != source.getImages()) {
                target.setImages(populateImages(source));
            }
            final List<String> productCodeList = targetShopTheLookFacade.getProductCodeListByLook(source.getId());
            if (productCodeList != null) {
                target.setProductCodeList(productCodeList);
            }
        }
    }


    private ImageContainer populateImages(final TargetLookModel source) {
        final ImageContainer images = new ImageContainer();
        for (final MediaModel media : source.getImages().getMedias()) {
            if (media.getMediaFormat() != null) {
                if (GRID_IMAGE.equalsIgnoreCase(media.getMediaFormat().getQualifier())) {
                    images.setGridImageUrl(media.getURL());
                }
                else if (HERO_IMAGE.equalsIgnoreCase(media.getMediaFormat().getQualifier())) {
                    images.setHeroImageUrl(media.getURL());
                }
                else if (SWATCH_IMAGE.equalsIgnoreCase(media.getMediaFormat().getQualifier())) {
                    images.setSwatchImageUrl(media.getURL());
                }
                else if (LIST_IMAGE.equalsIgnoreCase(media.getMediaFormat().getQualifier())) {
                    images.setListImageUrl(media.getURL());
                }
                else if (LARGE_IMAGE.equalsIgnoreCase(media.getMediaFormat().getQualifier())) {
                    images.setLargeImageUrl(media.getURL());
                }
                else if (THUMB_IMAGE.equalsIgnoreCase(media.getMediaFormat().getQualifier())) {
                    images.setThumbImageUrl(media.getURL());
                }
                else if (FULL_IMAGE.equalsIgnoreCase(media.getMediaFormat().getQualifier())) {
                    images.setFullImageUrl(media.getURL());
                }
            }
        }
        return images;
    }

    /**
     * @param targetShopTheLookFacade
     *            the targetShopTheLookFacade to set
     */
    @Required
    public void setTargetShopTheLookFacade(final TargetShopTheLookFacade targetShopTheLookFacade) {
        this.targetShopTheLookFacade = targetShopTheLookFacade;
    }

}

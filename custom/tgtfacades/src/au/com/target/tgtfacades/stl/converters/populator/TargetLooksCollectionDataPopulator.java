/**
 * 
 */
package au.com.target.tgtfacades.stl.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import au.com.target.tgtfacades.looks.data.LooksCollectionData;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;


/**
 * @author bhuang3
 *
 */
public class TargetLooksCollectionDataPopulator implements Populator<TargetLookCollectionModel, LooksCollectionData> {

    @Override
    public void populate(final TargetLookCollectionModel source, final LooksCollectionData target)
            throws ConversionException {
        if (source != null) {
            target.setId(source.getId());
            target.setName(source.getName());
            target.setDescription(source.getDescription());
        }
    }

}

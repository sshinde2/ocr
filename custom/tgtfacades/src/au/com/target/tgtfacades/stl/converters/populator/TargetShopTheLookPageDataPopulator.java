/**
 * 
 */
package au.com.target.tgtfacades.stl.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtfacades.looks.data.LooksCollectionData;
import au.com.target.tgtfacades.looks.data.ShopTheLookPageData;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;
import au.com.target.tgtmarketing.shopthelook.ShopTheLookService;


/**
 * @author bhuang3
 *
 */
public class TargetShopTheLookPageDataPopulator implements
        Populator<TargetShopTheLookModel, ShopTheLookPageData> {

    private static final Logger LOG = Logger.getLogger(TargetShopTheLookPageDataPopulator.class);

    private ShopTheLookService shopTheLookService;

    private Converter<TargetLookModel, LookDetailsData> targetLookDetailsDataConverter;

    private Converter<TargetLookCollectionModel, LooksCollectionData> targetLooksCollectionDataConverter;

    @Override
    public void populate(final TargetShopTheLookModel source, final ShopTheLookPageData target)
            throws ConversionException {
        if (source != null) {
            target.setTitle(source.getName());
            target.setDescription(source.getDescription());

            try {
                final List<TargetLookModel> lookModels = shopTheLookService
                        .getListOfLooksForShopTheLookUsingCode(source
                                .getId());
                if (lookModels != null) {
                    target.setCountOfLooks(lookModels.size());
                    final List<LookDetailsData> lookDetalsDataList = new ArrayList<>();
                    final Set<TargetLookCollectionModel> collectionModels = new LinkedHashSet<>();
                    final Map<TargetLookCollectionModel, Integer> collectionModelCountMap = new HashMap<TargetLookCollectionModel, Integer>();
                    for (final TargetLookModel lookModel : lookModels) {
                        final LookDetailsData lookDetailsData = targetLookDetailsDataConverter.convert(lookModel);
                        if (lookDetailsData != null) {
                            lookDetalsDataList.add(lookDetailsData);
                        }
                        final TargetLookCollectionModel collection = lookModel.getCollection();
                        if (collection != null) {
                            final Integer count = collectionModelCountMap.get(collection);
                            if (count != null) {
                                collectionModelCountMap.put(collection, Integer.valueOf(count.intValue() + 1));
                            }
                            else {
                                collectionModelCountMap.put(collection, Integer.valueOf(1));
                            }
                            collectionModels.add(collection);
                        }
                    }
                    final List<LooksCollectionData> collectionDataList = populateLooksCollectionDataList(
                            collectionModels, collectionModelCountMap);
                    target.setLooks(lookDetalsDataList);
                    target.setCollections(collectionDataList);
                }
            }
            catch (final IllegalArgumentException | TargetAmbiguousIdentifierException e) {
                LOG.error("Exception when get the shopTheLookModel", e);
            }
            catch (final TargetUnknownIdentifierException e) {
                LOG.warn("UnknownIdentifierException when get the shopTheLookModel", e);
            }
        }
    }

    private List<LooksCollectionData> populateLooksCollectionDataList(
            final Set<TargetLookCollectionModel> collectionModels,
            final Map<TargetLookCollectionModel, Integer> collectionModelCountMap) {
        final List<LooksCollectionData> collectionDataList = new ArrayList<>();
        for (final TargetLookCollectionModel collectionModel : collectionModels) {
            LooksCollectionData looksCollectionData = new LooksCollectionData();
            looksCollectionData.setCountOfLooks(collectionModelCountMap.get(collectionModel).intValue());
            looksCollectionData = targetLooksCollectionDataConverter.convert(collectionModel, looksCollectionData);
            if (looksCollectionData != null) {
                collectionDataList.add(looksCollectionData);
            }
        }
        return collectionDataList;
    }

    /**
     * @return the shopTheLookService
     */
    protected ShopTheLookService getShopTheLookService() {
        return shopTheLookService;
    }

    /**
     * @param shopTheLookService
     *            the shopTheLookService to set
     */
    @Required
    public void setShopTheLookService(final ShopTheLookService shopTheLookService) {
        this.shopTheLookService = shopTheLookService;
    }

    /**
     * @return the targetLookDetailsDataConverter
     */
    protected Converter<TargetLookModel, LookDetailsData> getTargetLookDetailsDataConverter() {
        return targetLookDetailsDataConverter;
    }

    /**
     * @param targetLookDetailsDataConverter
     *            the targetLookDetailsDataConverter to set
     */
    @Required
    public void setTargetLookDetailsDataConverter(
            final Converter<TargetLookModel, LookDetailsData> targetLookDetailsDataConverter) {
        this.targetLookDetailsDataConverter = targetLookDetailsDataConverter;
    }

    /**
     * @return the targetLooksCollectionDataConverter
     */
    protected Converter<TargetLookCollectionModel, LooksCollectionData> getTargetLooksCollectionDataConverter() {
        return targetLooksCollectionDataConverter;
    }

    /**
     * @param targetLooksCollectionDataConverter
     *            the targetLooksCollectionDataConverter to set
     */
    @Required
    public void setTargetLooksCollectionDataConverter(
            final Converter<TargetLookCollectionModel, LooksCollectionData> targetLooksCollectionDataConverter) {
        this.targetLooksCollectionDataConverter = targetLooksCollectionDataConverter;
    }



}

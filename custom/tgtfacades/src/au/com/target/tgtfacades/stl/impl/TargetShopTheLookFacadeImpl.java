/**
 * 
 */
package au.com.target.tgtfacades.stl.impl;


import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtfacades.looks.data.ShopTheLookPageData;
import au.com.target.tgtfacades.stl.TargetShopTheLookFacade;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetLookProductModel;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;
import au.com.target.tgtmarketing.shopthelook.LookService;
import au.com.target.tgtmarketing.shopthelook.ShopTheLookService;


/**
 * @author bhuang3
 *
 */
public class TargetShopTheLookFacadeImpl implements TargetShopTheLookFacade {

    private static final Logger LOG = Logger.getLogger(TargetShopTheLookFacadeImpl.class);

    private ShopTheLookService shopTheLookService;

    private LookService lookService;

    private Converter<TargetShopTheLookModel, ShopTheLookPageData> targetShopTheLookPageDataConverter;

    private Converter<TargetLookModel, LookDetailsData> targetLookDetailsDataConverter;



    @Override
    public List<TargetLookModel> getVisibleLooksForCollection(final String collectionCode) {
        return lookService.getVisibleLooksForCollection(collectionCode);
    }

    @Override
    public TargetShopTheLookModel getShopTheLookByCode(final String code) {
        try {
            return shopTheLookService.getShopTheLookForCode(code);
        }
        catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
            LOG.warn(e.getMessage());
        }
        return null;
    }

    @Override
    public ShopTheLookPageData populateShopTheLookPageData(final String code) {
        ShopTheLookPageData shopTheLookPageData = null;
        final TargetShopTheLookModel shopTheLook = this.getShopTheLookByCode(code);
        if (shopTheLook != null) {
            shopTheLookPageData = targetShopTheLookPageDataConverter.convert(shopTheLook);
        }

        return shopTheLookPageData;
    }

    @Override
    public TargetLookModel getLookByCode(final String code) {

        try {
            return lookService.getLookForCode(code);
        }
        catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
            LOG.warn(e.getMessage());
        }
        return null;
    }

    @Override
    public LookDetailsData populateLookDetailsData(final String code) {
        final TargetLookModel targetLookModel = this.getLookByCode(code);
        final LookDetailsData lookDetailsData = populateLookDetailsData(targetLookModel);
        return lookDetailsData;
    }

    @Override
    public List<String> getProductCodeListByLook(final String code) {
        List<String> productCodeList = null;
        final TargetLookModel targetLookModel = this.getLookByCode(code);
        if (targetLookModel != null) {
            final Set<TargetLookProductModel> orginLookProductModelList = targetLookModel.getProducts();
            if (orginLookProductModelList != null) {
                final List<TargetLookProductModel> lookProductModelList = new ArrayList<>(
                        orginLookProductModelList);
                sortLookProductModelListByPosition(lookProductModelList);
                productCodeList = getProductCodeList(lookProductModelList);
            }
        }
        return productCodeList;
    }

    @Override
    public List<LookDetailsData> populateLookDetailsDataList(final List<TargetLookModel> targetLookModels) {
        List<LookDetailsData> lookDetailsDatas = null;
        if (CollectionUtils.isNotEmpty(targetLookModels)) {
            lookDetailsDatas = new ArrayList<>();
            for (final TargetLookModel lookModel : targetLookModels) {
                lookDetailsDatas.add(populateLookDetailsData(lookModel));
            }
        }
        return lookDetailsDatas;
    }

    private LookDetailsData populateLookDetailsData(final TargetLookModel targetLookModel) {
        LookDetailsData lookDetailsData = null;
        if (targetLookModel != null) {
            lookDetailsData = targetLookDetailsDataConverter.convert(targetLookModel);
        }
        return lookDetailsData;
    }

    private List<String> getProductCodeList(final List<TargetLookProductModel> lookProductModelList) {
        final List<String> productCodeList = new ArrayList<>(lookProductModelList.size());
        for (final TargetLookProductModel lookProduct : lookProductModelList) {
            productCodeList.add(lookProduct.getProductCode());
        }
        return productCodeList;
    }

    private void sortLookProductModelListByPosition(final List<TargetLookProductModel> lookProductModelList) {
        if (CollectionUtils.isNotEmpty(lookProductModelList)) {
            Collections.sort(lookProductModelList, new Comparator<TargetLookProductModel>() {

                @Override
                public int compare(final TargetLookProductModel o1, final TargetLookProductModel o2) {
                    return o1.getPosition() - o2.getPosition();
                }

            });
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.stl.TargetShopTheLookFacade#getShopTheLookByCagegoryCode(java.lang.String)
     */
    @Override
    public TargetShopTheLookModel getShopTheLookByCategoryCode(final String code) {
        return shopTheLookService.getShopTheLookByCategoryCode(code);
    }

    /**
     * @param shopTheLookService
     *            the shopTheLookService to set
     */
    @Required
    public void setShopTheLookService(final ShopTheLookService shopTheLookService) {
        this.shopTheLookService = shopTheLookService;
    }

    /**
     * @param targetShopTheLookPageDataConverter
     *            the targetShopTheLookPageDataConverter to set
     */
    @Required
    public void setTargetShopTheLookPageDataConverter(
            final Converter<TargetShopTheLookModel, ShopTheLookPageData> targetShopTheLookPageDataConverter) {
        this.targetShopTheLookPageDataConverter = targetShopTheLookPageDataConverter;
    }

    /**
     * @param lookService
     *            the lookService to set
     */
    @Required
    public void setLookService(final LookService lookService) {
        this.lookService = lookService;
    }

    /**
     * @param targetLookDetailsDataConverter
     *            the targetLookDetailsDataConverter to set
     */
    @Required
    public void setTargetLookDetailsDataConverter(
            final Converter<TargetLookModel, LookDetailsData> targetLookDetailsDataConverter) {
        this.targetLookDetailsDataConverter = targetLookDetailsDataConverter;
    }


}

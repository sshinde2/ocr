/**
 * 
 */
package au.com.target.tgtfacades.stock;

import java.util.Collection;
import java.util.Map;

import au.com.target.tgtfacades.product.stock.StockAvailabilitiesData;
import au.com.target.tgtfacades.response.data.Response;


/**
 * @author pthoma20
 * 
 */
public interface TargetStockLookUpFacade {

    /**
     * Get Stock for products from Warehouse for the warehouseCode and creates the response. If warehouseCode is null -
     * will get Stock for the default warehouse.
     * 
     * @param stockAvailabilitiesData
     * @param source
     * @return stockAvailabilitiesData
     */
    StockAvailabilitiesData getStockFromWarehouse(StockAvailabilitiesData stockAvailabilitiesData, String source,
            String warehouseCode);

    /**
     * Looks up product availability for given variants and deliveryTypes and additionally any store SOH for given
     * locations for variants.
     * 
     * @param variants
     * @param deliveryTypes
     * @param locations
     * @return {@link Response}
     */
    Response lookupStock(Collection<String> variants, Collection<String> deliveryTypes, Collection<String> locations);

    /**
     * Looks up product availability for given base product and deliveryTypes and additionally any store SOH for given
     * locations for variants.
     * 
     * @param baseProductCode
     * @param deliveryTypes
     * @param locations
     * @return {@link Response}
     */
    Response lookupStock(String baseProductCode, Collection<String> deliveryTypes, Collection<String> locations);

    /**
     * @param variantCode
     * @return {@link Map}
     */
    Map<String, Boolean> lookupStockOnline(String variantCode);
}

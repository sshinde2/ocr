/**
 * 
 */
package au.com.target.tgtfacades.stock.impl;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.querybuilder.EndecaProductQueryBuilder;
import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.util.ProductUtil;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.product.stock.StockAvailabilitiesData;
import au.com.target.tgtfacades.product.stock.StockAvailabilityData;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.Error;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.response.data.StockResponseData;
import au.com.target.tgtfacades.stock.TargetStockLookUpFacade;
import au.com.target.tgtfluent.data.StockDatum;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.service.FluentStockLookupService;
import au.com.target.tgtsale.stock.service.TargetStoreStockService;
import au.com.target.tgtutility.util.TargetProductUtils;

import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERec;
import com.endeca.navigation.PropertyMap;


/**
 * @author pthoma20
 * 
 */
public class DefaultTargetStockLookUpFacade implements TargetStockLookUpFacade {

    protected static final Logger LOG = Logger.getLogger(DefaultTargetStockLookUpFacade.class);

    private static final String STOCK_LOOKUP_LOG = "STOCK-LOOKUP: source={0} trackingUIID={1} message={2}";

    private TargetStockService targetStockService;

    private TargetStoreStockService targetStoreStockService;

    private TargetProductFacade targetProductFacade;

    private FluentStockLookupService fluentStockLookupService;

    private EndecaProductQueryBuilder endecaProductQueryBuilder;

    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    private TargetSharedConfigService targetSharedConfigService;

    private TargetWarehouseService targetWarehouseService;

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.stock.TargetStockLookUpFacade#getStockFromWarehouse(au.com.target.tgtfacades.product.stock.StockAvailabilitiesData)
     */
    @Override
    public StockAvailabilitiesData getStockFromWarehouse(final StockAvailabilitiesData stockAvailabilitiesData,
            final String source, final String warehouseCode) {

        if (stockAvailabilitiesData == null || CollectionUtils.isEmpty(stockAvailabilitiesData.getAvailabilities())) {
            LOG.error(MessageFormat.format(STOCK_LOOKUP_LOG,
                    source, null, "Availabilities in the request is null or empty"));
            return stockAvailabilitiesData;
        }
        final List<String> itemCodesForWhichStockRequired = new ArrayList<>();
        String trackingUIID = null;
        for (final StockAvailabilityData stockAvailabilityData : stockAvailabilitiesData.getAvailabilities()) {
            if (stockAvailabilityData == null) {
                LOG.error(MessageFormat.format(STOCK_LOOKUP_LOG,
                        source, null, "Stock Availability Data in request is null"));
                continue;
            }
            trackingUIID = stockAvailabilityData.getTrackingUUID();
            final String itemCode = stockAvailabilityData.getItemCode();
            if (StringUtils.isEmpty(itemCode)) {
                LOG.error(MessageFormat.format(STOCK_LOOKUP_LOG,
                        source, stockAvailabilityData.getTrackingUUID(), "Has an empty itemcode in the request"));
                continue;
            }
            itemCodesForWhichStockRequired.add(itemCode);
        }
        if (CollectionUtils.isEmpty(itemCodesForWhichStockRequired)) {
            LOG.error(MessageFormat.format(STOCK_LOOKUP_LOG,
                    source, trackingUIID, "No valid itemcodes found in request"));
            return stockAvailabilitiesData;
        }

        try {
            return populateStockAvailabilitiesResponseData(stockAvailabilitiesData,
                    getStockMap(itemCodesForWhichStockRequired, warehouseCode), source, trackingUIID);
        }
        catch (final FluentClientException e) {
            LOG.error(MessageFormat.format(STOCK_LOOKUP_LOG, source, trackingUIID, e));
            return stockAvailabilitiesData;
        }
    }

    @Override
    public Response lookupStock(final String baseProductCode, final Collection<String> deliveryTypes,
            final Collection<String> locations) {
        if (StringUtils.isBlank(baseProductCode)) {
            return createErrorResponse(TgtFacadesConstants.WebServiceError.ERR_EMPTY_BASE_PRODUCT,
                    TgtFacadesConstants.WebServiceMessage.ERR_EMPTY_BASE_PRODUCT);
        }
        if (CollectionUtils.isEmpty(deliveryTypes) && CollectionUtils.isEmpty(locations)) {
            return createErrorResponse(TgtFacadesConstants.WebServiceError.ERR_EMPTY_OPTIONS,
                    TgtFacadesConstants.WebServiceMessage.ERR_EMPTY_OPTIONS);
        }
        try {
            final ProductModel product = targetProductFacade.getProductForCode(baseProductCode);
            final Collection<String> variants = TargetProductUtils
                    .getAllSellableVariantCodes(product);
            return lookupStock(variants, deliveryTypes, locations);
        }
        catch (final UnknownIdentifierException | AmbiguousIdentifierException e) {
            LOG.error("STOCK_LOOKUP_ERROR: base product not found", e);
            return createErrorResponse(TgtFacadesConstants.WebServiceError.ERR_BASE_PRODUCT_UNKNOWN,
                    TgtFacadesConstants.WebServiceMessage.ERR_BASE_PRODUCT_UNKNOWN);
        }
    }

    @Override
    public Response lookupStock(final Collection<String> variants, final Collection<String> deliveryTypes,
            final Collection<String> locations) {
        List<StockDatum> stockData;
        if (CollectionUtils.isEmpty(variants)) {
            return createErrorResponse(TgtFacadesConstants.WebServiceError.ERR_EMPTY_VARIANTS,
                    TgtFacadesConstants.WebServiceMessage.ERR_EMPTY_VARIANTS);
        }
        if (CollectionUtils.isEmpty(deliveryTypes) && CollectionUtils.isEmpty(locations)) {
            return createErrorResponse(TgtFacadesConstants.WebServiceError.ERR_EMPTY_OPTIONS,
                    TgtFacadesConstants.WebServiceMessage.ERR_EMPTY_OPTIONS);
        }
        if (targetFeatureSwitchFacade.isFluentEnabled()) {
            stockData = fluentLookupStock(variants, deliveryTypes, locations);
        }
        else {
            stockData = legacyLookupStock(variants, deliveryTypes, locations);
        }
        if (CollectionUtils.isEmpty(stockData)) {
            return createErrorResponse(TgtFacadesConstants.WebServiceError.ERR_STOCK_LOOKUP,
                    TgtFacadesConstants.WebServiceMessage.ERR_STOCK_LOOKUP);
        }
        final Response response = new Response(true);
        response.setData(new StockResponseData(stockData));
        return response;
    }

    @Override
    public Map<String, Boolean> lookupStockOnline(final String variantCode) {
        TargetProductModel baseProduct;
        try {
            final ProductModel product = targetProductFacade.getProductForCode(variantCode);
            if (product instanceof TargetProductModel) {
                baseProduct = (TargetProductModel)product;
            }
            else {
                baseProduct = targetProductFacade
                        .getBaseTargetProduct((AbstractTargetVariantProductModel)product);
            }

            final Collection<String> variants = TargetProductUtils
                    .getAllSellableVariantCodes(baseProduct);

            if (targetFeatureSwitchFacade.isFluentEnabled()) {
                return convertStockLevelToBool(fluentStockLookupService.lookupAts(variants.toArray(new String[] {})));
            }
            else {
                return getLegacyStockMap(variants);
            }
        }
        catch (final Exception e) {
            LOG.error("STOCK LOOKUP: Failed to retrieve product from variant code", e);
            return Collections.emptyMap();
        }
    }

    /**
     * Convert map of variants stockLevel to inStock bool
     * 
     * @param stock
     * @return map of inStock by variant
     */
    protected Map<String, Boolean> convertStockLevelToBool(final Map<String, Integer> stock) {
        final Map<String, Boolean> stockMap = new LinkedHashMap();
        for (final Entry<String, Integer> stockItem : stock.entrySet()) {
            int stockLevel = 0;
            if (null != stockItem.getValue()) {
                stockLevel = stockItem.getValue().intValue();
            }
            stockMap.put(stockItem.getKey(), Boolean.valueOf(stockLevel > 0));
        }
        return stockMap;
    }

    /**
     * Sets up a stock map for legacy warehouse
     * 
     * @param variants
     * @return map of storeSoh by variant
     */
    protected Map<String, Boolean> getLegacyStockMap(final Collection<String> variants) {
        final Map<String, Boolean> stockMap = new LinkedHashMap();
        for (final String variant : variants) {
            final ProductModel product = targetProductFacade.getProductForCode(variant);
            final StockData stockData = targetStockService
                    .getStockLevelAndStatusAmount(product);
            if (null != stockData) {
                final boolean outOfStock = StockLevelStatus.OUTOFSTOCK.equals(stockData.getStockLevelStatus());
                stockMap.put(variant, Boolean.valueOf(!outOfStock));

            }
        }
        return stockMap;
    }

    /**
     * Looks up stock using fluent API
     * 
     * @param variants
     * @param deliveryTypes
     * @param locations
     * @return list of {@link StockDatum}
     */
    protected List<StockDatum> fluentLookupStock(final Collection<String> variants,
            final Collection<String> deliveryTypes, final Collection<String> locations) {
        try {
            return fluentStockLookupService.lookupStock(variants, deliveryTypes, locations);
        }
        catch (final FluentClientException e) {
            LOG.error("FLUENT: Failed looking up stock", e);
            return Collections.emptyList();
        }
    }

    /**
     * Uses the legacy mechanism to lookup online, consolidated and store stock
     * 
     * @param variants
     * @param deliveryTypes
     * @param locations
     * @return list of {@link StockDatum}
     */
    protected List<StockDatum> legacyLookupStock(final Collection<String> variants,
            final Collection<String> deliveryTypes, final Collection<String> locations) {
        try {
            final Map<String, Map<String, StockLevelStatus>> storeSohByVariantMap = getStoreSohByVariantMap(variants,
                    locations);
            final Map<String, StockLevelStatus> consolidatedSohByVariantMap = getConsolidatedSohByVariantMap(variants,
                    deliveryTypes);

            final List<StockDatum> stockData = new ArrayList<>(variants.size());
            StockDatum stockDatum;
            for (final String variantCode : variants) {
                stockDatum = new StockDatum();
                stockDatum.setVariantCode(variantCode);
                if (CollectionUtils.isNotEmpty(deliveryTypes)) {
                    setAtsValues(deliveryTypes, stockDatum, variantCode);
                    stockDatum.setConsolidatedStoreStock(consolidatedSohByVariantMap.get(variantCode));
                }
                stockDatum.setStoreSoh(storeSohByVariantMap.get(variantCode));
                stockData.add(stockDatum);
            }
            return stockData;
        }
        catch (final Exception e) {
            LOG.error("LEGACY_STOCK_LOOKUP_ERROR", e);
            return Collections.emptyList();
        }
    }

    /**
     * @param variants
     * @param locations
     * @return map of storeSoh by variant
     */
    private Map<String, Map<String, StockLevelStatus>> getStoreSohByVariantMap(final Collection<String> variants,
            final Collection<String> locations) {
        if (CollectionUtils.isEmpty(locations)) {
            return Collections.emptyMap();
        }

        final Map<String, Map<String, StockLevelStatus>> storeSohByVariantMap = new HashMap<>(variants.size());
        Map<String, StockLevelStatus> stockLevelForProducts;
        for (final String location : locations) {
            try {
                stockLevelForProducts = targetStoreStockService
                        .getStoreStockLevelStatusMapForProducts(Integer.valueOf(location),
                                Arrays.asList(variants.toArray(new String[] {})));
                for (final Entry<String, StockLevelStatus> stockLevelForProduct : stockLevelForProducts
                        .entrySet()) {
                    getStoreSohByVariant(storeSohByVariantMap, stockLevelForProduct.getKey()).put(location,
                            stockLevelForProduct.getValue());
                }
            }
            catch (final NumberFormatException e) {
                LOG.error("invalid location=" + location);
            }
        }
        return storeSohByVariantMap;
    }

    /**
     * @param itemCodesForWhichStockRequired
     * @param warehouseCode
     * @return map of variantCodes to stock quantity
     * @throws FluentClientException
     */
    private Map<String, Integer> getStockMap(final List<String> itemCodesForWhichStockRequired,
            final String warehouseCode) throws FluentClientException {
        Map<String, Integer> stockMap;
        if (targetFeatureSwitchFacade.isFluentEnabled()) {
            final List<StockDatum> stockData = fluentStockLookupService.lookupStock(itemCodesForWhichStockRequired,
                    Arrays.asList(TgtCoreConstants.DELIVERY_TYPE.CC), null);
            stockMap = new LinkedHashMap<>();
            for (final StockDatum stockDatum : stockData) {
                stockMap.put(StringUtils.lowerCase(stockDatum.getVariantCode()), stockDatum.getAtsCcQty());
            }
        }
        else {
            stockMap = targetStockService.getStockForProductsInWarehouse(itemCodesForWhichStockRequired, warehouseCode);
        }
        return stockMap;
    }

    /**
     * @param stockAvailabilitiesData
     * @param stockDataMap
     * @param source
     * @param trackingUIID
     * @return {@link StockAvailabilitiesData}
     */
    private StockAvailabilitiesData populateStockAvailabilitiesResponseData(
            final StockAvailabilitiesData stockAvailabilitiesData, final Map<String, Integer> stockDataMap,
            final String source, final String trackingUIID) {
        if (MapUtils.isEmpty(stockDataMap)) {
            LOG.error(MessageFormat.format(STOCK_LOOKUP_LOG,
                    source, trackingUIID,
                    "Stock lookup returned no records for products"));
            return stockAvailabilitiesData;
        }

        for (final StockAvailabilityData stockAvailabilityData : stockAvailabilitiesData.getAvailabilities()) {

            Integer availableQty = stockDataMap.get(StringUtils.lowerCase(stockAvailabilityData.getItemCode()));
            if (availableQty == null) {
                LOG.error(MessageFormat.format(STOCK_LOOKUP_LOG,
                        source, trackingUIID,
                        "Stock lookup returned availableQty as null for itemCode="
                                + stockAvailabilityData.getItemCode()));
                continue;
            }
            if (targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)
                    && !ProductUtil.isProductTypeDigital(
                            targetProductFacade.getProductForCode(stockAvailabilityData.getItemCode()))
                    && !ProductUtil.isProductTypePhysicalGiftcard(
                            targetProductFacade.getProductForCode(stockAvailabilityData.getItemCode()))
                    && !TargetProductUtils.isProductOpenForPreOrder(
                            targetProductFacade.getProductForCode(stockAvailabilityData.getItemCode()))) {
                final ProductModel productModel = targetProductFacade
                        .getProductForCode(stockAvailabilityData.getItemCode());
                availableQty = availableStockFulfillByFastline(targetStockService.getAllStockLevels(productModel));
            }

            if (stockAvailabilityData.getRequestedQuantity() <= availableQty.intValue()) {
                stockAvailabilityData.setIsAvailable(true);
            }
            if (availableQty.intValue() > 0) {
                stockAvailabilityData.setTotalAvailableQuantity(availableQty.intValue());
            }
            stockAvailabilityData.setLastUpdated(new Date());
        }

        return stockAvailabilitiesData;
    }

    /**
     * @param deliveryTypes
     * @param stockDatum
     * @param variantCode
     */
    private void setAtsValues(final Collection<String> deliveryTypes, final StockDatum stockDatum,
            final String variantCode) {
        if (!CollectionUtils.containsAny(deliveryTypes, Arrays
                .asList(TgtCoreConstants.DELIVERY_TYPE.CC,
                        TgtCoreConstants.DELIVERY_TYPE.HD, TgtCoreConstants.DELIVERY_TYPE.ED,
                        TgtCoreConstants.DELIVERY_TYPE.PO))) {
            return;
        }
        final ProductModel product = targetProductFacade.getProductForCode(variantCode);
        final StockData stockData = targetStockService.getStockLevelAndStatusAmount(product);
        // all ATS values = online/fastline stock
        if (stockData != null) {
            if (deliveryTypes.contains(TgtCoreConstants.DELIVERY_TYPE.CC)) {
                stockDatum.setAtsCc(stockData.getStockLevelStatus());
            }
            if (deliveryTypes.contains(TgtCoreConstants.DELIVERY_TYPE.HD)) {
                stockDatum.setAtsHd(stockData.getStockLevelStatus());
            }
            if (deliveryTypes.contains(TgtCoreConstants.DELIVERY_TYPE.ED)) {
                stockDatum.setAtsEd(stockData.getStockLevelStatus());
            }
            if (deliveryTypes.contains(TgtCoreConstants.DELIVERY_TYPE.PO)) {
                stockDatum.setAtsPo(stockData.getStockLevelStatus());
            }
        }
    }

    /**
     * @param storeSohByVariantMap
     * @param variantCode
     * @return map of location to {@link StockLevelStatus} for given variant
     */
    private Map<String, StockLevelStatus> getStoreSohByVariant(
            final Map<String, Map<String, StockLevelStatus>> storeSohByVariantMap, final String variantCode) {
        Map<String, StockLevelStatus> storeSohByVariant = storeSohByVariantMap.get(variantCode);
        if (storeSohByVariant == null) {
            storeSohByVariant = new HashMap<>();
            storeSohByVariantMap.put(variantCode, storeSohByVariant);
        }
        return storeSohByVariant;
    }

    /**
     * @param variants
     * @param deliveryTypes
     * @return map of variant to {@link StockLevelStatus}
     */
    protected Map<String, StockLevelStatus> getConsolidatedSohByVariantMap(final Collection<String> variants,
            final Collection<String> deliveryTypes) {
        if (CollectionUtils.isEmpty(deliveryTypes)
                || !deliveryTypes.contains(TgtCoreConstants.DELIVERY_TYPE.CONSOLIDATED_STORES_SOH)
                || !targetFeatureSwitchFacade.isConsolidatedStoreStockAvailable()) {
            return Collections.emptyMap();
        }
        final Map<String, StockLevelStatus> consolidatedSohByVariantMap = new HashMap<>();
        PropertyMap properties;
        String sellableVariantCode;
        for (final String searchField : Arrays.asList(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE)) {
            for (final ERec eRec : searchEndeca(variants, searchField)) {
                properties = eRec.getProperties();
                sellableVariantCode = (String)properties.get(searchField);
                if (variants.contains(sellableVariantCode)) {
                    consolidatedSohByVariantMap.put(sellableVariantCode, getConsolidatedStoreStockLevel(properties));
                }
            }
        }
        return consolidatedSohByVariantMap;
    }

    /**
     * @param variants
     * @return list of {@link ERec}
     */
    protected List<ERec> searchEndeca(final Collection<String> variants, final String searchField) {
        final EndecaSearchStateData searchStateData = populateEndecaSearchStateData(variants, searchField);
        final ENEQueryResults eNEQueryResults = endecaProductQueryBuilder.getQueryResults(searchStateData,
                null, variants.size());
        if (eNEQueryResults == null || eNEQueryResults.getNavigation() == null) {
            return Collections.emptyList();
        }
        return eNEQueryResults.getNavigation().getERecs();
    }


    private Integer availableStockFulfillByFastline(final Collection<StockLevelModel> stockLevels) {
        final int maxQtyFastlineFulfillPerDay = targetSharedConfigService
                .getInt(TgtCoreConstants.Config.MAX_QTY_FASTLINE_FULFILLED_PER_DAY, 100);
        final int totalQtyFulfilledAtFastlineToday = targetWarehouseService.getDefaultOnlineWarehouse()
                .getTotalQtyFulfilledAtFastlineToday();
        int availableQtyFulfilledByFastline = maxQtyFastlineFulfillPerDay - totalQtyFulfilledAtFastlineToday;
        availableQtyFulfilledByFastline = availableQtyFulfilledByFastline > 0 ? availableQtyFulfilledByFastline : 0;
        int totalActualAmount = 0;
        for (final StockLevelModel stockLevel : stockLevels) {
            int actualAmount = 0;
            if (TgtCoreConstants.FASTLINE_WAREHOUSE.equals(stockLevel.getWarehouse().getCode())) {
                actualAmount = stockLevel.getAvailable() - stockLevel.getReserved();
                actualAmount = availableQtyFulfilledByFastline > actualAmount ? actualAmount
                        : availableQtyFulfilledByFastline;
            }
            else {
                actualAmount = stockLevel.getAvailable() - stockLevel.getReserved();
            }
            totalActualAmount += actualAmount;
        }
        return Integer.valueOf(totalActualAmount);
    }

    /**
     * @param variants
     * @return {@link EndecaSearchStateData}
     */
    protected EndecaSearchStateData populateEndecaSearchStateData(final Collection<String> variants,
            final String searchField) {

        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setSkipInStockFilter(false);
        if (targetFeatureSwitchFacade.isFindInStoreStockVisibilityEnabled()) {
            endecaSearchStateData.setSkipInStockFilter(true);
        }
        endecaSearchStateData.setSkipDefaultSort(true);
        endecaSearchStateData.setMatchMode(EndecaConstants.MATCH_MODE_ANY);
        endecaSearchStateData.setNpSearchState(EndecaConstants.ENDECA_N_0);
        endecaSearchStateData.setSearchField(searchField);

        endecaSearchStateData.setSearchTerm(new ArrayList<>(variants));
        endecaSearchStateData.setNuRollupField(null);
        endecaSearchStateData.setFieldListConfig(EndecaConstants.FieldList.STORESTOCK_FIELDLIST);
        return endecaSearchStateData;
    }

    /**
     * @param endecaRecordProperties
     */
    protected StockLevelStatus getConsolidatedStoreStockLevel(final PropertyMap endecaRecordProperties) {
        String availQtyInStore = (String)endecaRecordProperties
                .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY_IN_STORE);
        if (targetFeatureSwitchFacade.isFindInStoreStockVisibilityEnabled()) {
            availQtyInStore = (String)endecaRecordProperties
                    .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_ACTUAL_CONSOLIDATED_STORE_STOCK);
        }
        if (StringUtils.isNotEmpty(availQtyInStore)) {
            final long stockLevel = Long.parseLong(availQtyInStore);
            if (stockLevel > 0L) {
                return StockLevelStatus.INSTOCK;
            }
        }
        return StockLevelStatus.OUTOFSTOCK;
    }

    /**
     * @param errorCode
     * @param errorMessage
     * @return {@link Response}
     */
    private Response createErrorResponse(final String errorCode, final String errorMessage) {
        final Response response = new Response(false);

        if (errorCode != null) {
            final Error error = new Error(errorCode);
            error.setMessage(errorMessage);

            final BaseResponseData data = new BaseResponseData();
            data.setError(error);

            response.setData(data);
        }
        return response;
    }

    /**
     * @param targetStockService
     *            the targetStockService to set
     */
    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }

    /**
     * @param targetStoreStockService
     *            the targetStoreStockService to set
     */
    @Required
    public void setTargetStoreStockService(final TargetStoreStockService targetStoreStockService) {
        this.targetStoreStockService = targetStoreStockService;
    }

    /**
     * @param targetProductFacade
     *            the targetProductFacade to set
     */
    @Required
    public void setTargetProductFacade(final TargetProductFacade targetProductFacade) {
        this.targetProductFacade = targetProductFacade;
    }

    /**
     * @param fluentStockLookupService
     *            the fluentStockLookupService to set
     */
    @Required
    public void setFluentStockLookupService(final FluentStockLookupService fluentStockLookupService) {
        this.fluentStockLookupService = fluentStockLookupService;
    }


    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */
    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }

    /**
     * @param targetSharedConfigService
     *            the targetSharedConfigService to set
     */
    @Required
    public void setTargetSharedConfigService(final TargetSharedConfigService targetSharedConfigService) {
        this.targetSharedConfigService = targetSharedConfigService;
    }

    /**
     * @param endecaProductQueryBuilder
     *            the endecaProductQueryBuilder to set
     */
    @Required
    public void setEndecaProductQueryBuilder(final EndecaProductQueryBuilder endecaProductQueryBuilder) {
        this.endecaProductQueryBuilder = endecaProductQueryBuilder;
    }

    /**
     * @param targetFeatureSwitchFacade
     *            the targetFeatureSwitchFacade to set
     */
    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }

}

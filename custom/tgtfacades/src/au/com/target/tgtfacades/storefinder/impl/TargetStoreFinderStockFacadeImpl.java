/**
 * 
 */
package au.com.target.tgtfacades.storefinder.impl;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtcore.storefinder.TargetStoreFinderService;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.google.location.finder.TargetGoogleLocationFinderFacade;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.LocationResponseData;
import au.com.target.tgtfacades.response.data.ProductStockSearchResultResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.storefinder.TargetStoreFinderStockFacade;
import au.com.target.tgtfacades.storefinder.data.StoreStockVisiblityHolder;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceStockData;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.service.FluentStockLookupService;
import au.com.target.tgtsale.stock.service.TargetStoreStockService;
import au.com.target.tgtutility.util.TargetDateUtil;


/**
 * @author htan3
 *
 */
public class TargetStoreFinderStockFacadeImpl implements TargetStoreFinderStockFacade {

    private static final Logger LOG = Logger.getLogger(TargetStoreFinderStockFacadeImpl.class);

    private static final String STOCK_ERROR_TRYAGAIN = "Sorry, there was a problem locating stores. Please try again.";

    private static final String STOCK_ERROR = "Sorry, there was a problem locating stores.";

    private static final String STOCK_SERVICE_DISABLED = "Sorry, the store stock service is disabled.";

    private static final String CLICK_AND_COLLECT = "cnc";

    private static final String HOME_DELIVERY = "hd";

    private BaseStoreService baseStoreService;

    private TargetStoreFinderService<PointOfServiceDistanceData, StoreFinderSearchPageData<PointOfServiceDistanceData>> storeFinderService;

    private ProductService productService;

    private Converter<StoreStockVisiblityHolder, TargetPointOfServiceStockData> storeStockConverter;

    private TargetStoreStockService targetStoreStockService;

    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    private TargetPointOfServiceService targetPointOfServiceService;

    private FluentStockLookupService fluentStockLookupService;

    private String[] stockStatusLevel;

    private TargetGoogleLocationFinderFacade targetGoogleLocationFinderFacade;

    @Override
    public Response doSearchProductStock(final String productCode, final String location, final Double latitude,
            final Double longitude, final Integer storeNumber, final PageableData pageableData,
            final boolean moveNearestInStockStoreToTop) {
        if (!targetFeatureSwitchFacade.isInStoreStockVisibilityEnabled()) {
            return createErrorResponse("ERR_CHECK_STOCK_SERVICE_ERROR", STOCK_SERVICE_DISABLED);
        }

        final StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageData = searchStoreByLocationOrPosition(
                location, latitude, longitude, storeNumber, pageableData);

        if (storeFinderSearchPageData == null
                || CollectionUtils.isEmpty(storeFinderSearchPageData.getResults())) {
            return createErrorResponse("ERR_CHECK_STOCK_LOCATION_NOTFOUND", STOCK_ERROR_TRYAGAIN);
        }

        final StoreStockVisiblityHolder storeStockHolder = new StoreStockVisiblityHolder();

        if (StringUtils.isNotBlank(productCode)) {
            addStockForProductInStores(productCode, storeFinderSearchPageData, storeStockHolder);

            if (storeStockHolder.getStockLookupResult() == null) {
                return createErrorResponse("ERR_CHECK_STOCK_SERVICE_ERROR", STOCK_ERROR_TRYAGAIN);
            }
            else if (CollectionUtils.isEmpty(storeStockHolder.getStockLookupResult().getItems())) {
                return createErrorResponse("ERR_CHECK_STOCK_SERVICE_FAILURE", STOCK_ERROR);
            }
        }

        return checkStock(storeFinderSearchPageData, storeStockHolder, moveNearestInStockStoreToTop);
    }

    @Override
    public Response nearestStoreLocation(final String deliveryType, final String location,
            final Integer storeNumber,
            final PageableData pageableData) {
        if (CLICK_AND_COLLECT.equalsIgnoreCase(deliveryType)) {
            final StoreFinderSearchPageData<PointOfServiceDistanceData> storeLocationData = searchCncStoreByLocationOrStoreNumber(
                    location, storeNumber, pageableData);
            if (isPOSDataAvailable(storeLocationData)) {
                return cncResponseData(storeLocationData);
            }
        }
        else if (HOME_DELIVERY.equalsIgnoreCase(deliveryType)) {
            return hdResponseData(location, storeNumber);
        }
        return createErrorResponse("STORE_LOCATION_NOTFOUND_OR_FOUND_MORE_THAN_ONE_LOCATION",
                STOCK_ERROR_TRYAGAIN);
    }

    private boolean isPOSDataAvailable(
            final StoreFinderSearchPageData<PointOfServiceDistanceData> storeLocation) {
        if (storeLocation == null || CollectionUtils.isEmpty(storeLocation.getResults())
                || storeLocation.getResults().size() > 1) {
            return false;
        }
        return true;
    }

    /**
     * @param storeLocationData
     * @return Response
     */
    private Response cncResponseData(final StoreFinderSearchPageData<PointOfServiceDistanceData> storeLocationData) {
        final LocationResponseData locationResponseData = new LocationResponseData();
        final StoreStockVisiblityHolder storeVisibilityHolder = new StoreStockVisiblityHolder();
        if (storeLocationData != null && storeLocationData.getResults() != null) {
            storeVisibilityHolder.setPointOfServiceDistanceData(storeLocationData.getResults().get(0));
        }
        locationResponseData.setClickAndCollectPos(storeStockConverter.convert(storeVisibilityHolder));
        final Response response = new Response(true);
        response.setData(locationResponseData);
        return response;
    }

    private Response hdResponseData(final String location, final Integer storeNumber) {
        Double latitude = null;
        Double longitude = null;
        GeoPoint geoPoint = null;
        if (null != storeNumber) {
            geoPoint = getGeoPoint(storeNumber);
        }
        if (null != geoPoint) {
            latitude = Double.valueOf(geoPoint.getLatitude());
            longitude = Double.valueOf(geoPoint.getLongitude());
        }
        if (null != location || null != geoPoint) {
            return targetGoogleLocationFinderFacade.searchUserDeliveryLocation(location, latitude, longitude);
        }
        return createErrorResponse("STORE_LOCATION_NOTFOUND_OR_FOUND_MORE_THAN_ONE_LOCATION",
                STOCK_ERROR_TRYAGAIN);
    }



    private GeoPoint getGeoPoint(final Integer storeNumber) {
        try {
            final TargetPointOfServiceModel pointOfService = targetPointOfServiceService
                    .getPOSByStoreNumber(storeNumber);
            return createGeoPoint(pointOfService.getLatitude(), pointOfService.getLongitude());
        }
        catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException exception) {
            LOG.error("Exception occurred getting latitude and longitude for the store:" + storeNumber + ",",
                    exception);
        }
        return null;
    }

    private void addStockForProductInStores(final String productCode,
            final StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageData,
            final StoreStockVisiblityHolder storeStockHolder) {
        ProductModel productModel = null;
        try {
            productModel = productService.getProductForCode(productCode);
        }
        catch (final UnknownIdentifierException | AmbiguousIdentifierException ex) {
            return;
        }

        final List<PointOfServiceDistanceData> stores = storeFinderSearchPageData.getResults();
        StockVisibilityItemLookupResponseDto stockResult;
        if (targetFeatureSwitchFacade.isFluentEnabled()) {
            try {
                stockResult = fluentStockLookupService.lookupStock(ImmutableList.of(productCode),
                        getStoreNumbers(stores));
            }
            catch (final FluentClientException e) {
                stockResult = null;
            }
        }
        else {
            stockResult = targetStoreStockService.lookupStockForItemsInStores(
                    getStoreNumbers(stores),
                    ImmutableList.of(productCode));
        }

        storeStockHolder.setStockLookupResult(stockResult);
        storeStockHolder.setProduct(productModel);
    }

    /**
     * Check stock for given productCode and list of stores
     * 
     * @param storeFinderSearchPageData
     * @return response with store and stock info
     */
    protected Response checkStock(final StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageData,
            final StoreStockVisiblityHolder storeStockHolder, final boolean moveNearestInStockStoreToTop) {
        final String locationText = storeFinderSearchPageData.getLocationText();
        final List<PointOfServiceDistanceData> stores = storeFinderSearchPageData.getResults();

        final ProductStockSearchResultResponseData responseData = new ProductStockSearchResultResponseData();
        responseData.setSelectedLocation(storeFinderSearchPageData.getLocationText());
        responseData.setAvailabilityTime(TargetDateUtil.getCurrentFormattedDate());

        boolean hasStockInStore = false;

        storeStockHolder.setLocationText(StringUtils.isNotEmpty(locationText) ? locationText : "Current+Location");

        final List<TargetPointOfServiceStockData> storeStockList = new ArrayList<>();
        TargetPointOfServiceStockData topStore = null;
        for (final PointOfServiceDistanceData store : stores) {
            storeStockHolder.setPointOfServiceDistanceData(store);
            final TargetPointOfServiceStockData storeStockData = storeStockConverter.convert(storeStockHolder);
            storeStockList.add(storeStockData);
            if (storeStockData.getStockLevel() == null || storeStockData.getStockLevel().equals(stockStatusLevel[0])
                    || topStore != null) {
                continue;
            }
            if (moveNearestInStockStoreToTop) {
                topStore = storeStockData;
            }
            hasStockInStore = true;
        }
        if (topStore != null) {
            storeStockList.remove(topStore);
            storeStockList.add(0, topStore);
        }

        responseData.setStockAvailable(hasStockInStore);
        responseData.setStockDataIncluded(storeStockHolder.getProduct() != null);

        final Response response = new Response(true);
        responseData.setStores(storeStockList);
        response.setData(responseData);
        return response;
    }

    private StoreFinderSearchPageData<PointOfServiceDistanceData> searchStoreByLocationOrPosition(
            final String location, final Double latitude, final Double longitude, final Integer storeNumber,
            final PageableData pageableData) {
        StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageData = null;
        if (storeNumber != null) {
            try {
                final TargetPointOfServiceModel pos = targetPointOfServiceService.getPOSByStoreNumber(storeNumber);

                final GeoPoint point = createGeoPoint(pos.getLatitude(), pos.getLongitude());
                storeFinderSearchPageData = storeFinderService
                        .positionSearch(baseStoreService.getCurrentBaseStore(), point, pageableData);
            }
            catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException ex) {
                return null;
            }
        }
        else if (latitude != null && longitude != null) {
            final GeoPoint point = createGeoPoint(latitude, longitude);
            storeFinderSearchPageData = storeFinderService
                    .positionSearch(baseStoreService.getCurrentBaseStore(), point, pageableData);

        }
        else if (location != null) {
            storeFinderSearchPageData = storeFinderService
                    .doSearchByLocation(baseStoreService.getCurrentBaseStore(), location, pageableData);
        }
        return storeFinderSearchPageData;
    }

    private StoreFinderSearchPageData<PointOfServiceDistanceData> searchCncStoreByLocationOrStoreNumber(
            final String location, final Integer storeNumber,
            final PageableData pageableData) {
        StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageData = null;
        if (storeNumber != null) {
            try {
                final TargetPointOfServiceModel pointOfService = targetPointOfServiceService
                        .getPOSByStoreNumber(storeNumber);
                final GeoPoint point = createGeoPoint(pointOfService.getLatitude(), pointOfService.getLongitude());
                storeFinderSearchPageData = storeFinderService
                        .doSearchNearestClickAndCollectLocation(location, point,
                                pageableData);
            }
            catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException ex) {
                return null;
            }
        }
        else if (StringUtils.isNotEmpty(location)) {
            storeFinderSearchPageData = storeFinderService
                    .doSearchClickAndCollectStoresByLocation(baseStoreService.getCurrentBaseStore(), location,
                            pageableData);
        }
        return storeFinderSearchPageData;
    }

    protected GeoPoint createGeoPoint(final Double latitude, final Double longitude) {
        final GeoPoint point = new GeoPoint();
        point.setLatitude(latitude.doubleValue());
        point.setLongitude(longitude.doubleValue());
        return point;
    }

    private List<String> getStoreNumbers(final List<PointOfServiceDistanceData> stores) {
        final List<String> storeNumbers = new ArrayList<>();
        for (final PointOfServiceDistanceData store : stores) {
            storeNumbers.add(((TargetPointOfServiceModel)store.getPointOfService()).getStoreNumber().toString());
        }
        return storeNumbers;
    }

    private Response createErrorResponse(final String errCode, final String errMsg) {
        final Response response = new Response(false);
        final au.com.target.tgtfacades.response.data.Error error = new au.com.target.tgtfacades.response.data.Error(
                errCode);
        error.setMessage(errMsg);
        final BaseResponseData responseData = new BaseResponseData();
        responseData.setError(error);
        response.setData(responseData);
        return response;
    }

    @Required
    public void setBaseStoreService(final BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    @Required
    public void setStoreFinderService(
            final TargetStoreFinderService<PointOfServiceDistanceData, StoreFinderSearchPageData<PointOfServiceDistanceData>> storeFinderService) {
        this.storeFinderService = storeFinderService;
    }

    @Required
    public void setProductService(final ProductService productService) {
        this.productService = productService;
    }

    @Required
    public void setStoreStockConverter(
            final Converter<StoreStockVisiblityHolder, TargetPointOfServiceStockData> storeStockConverter) {
        this.storeStockConverter = storeStockConverter;
    }

    @Required
    public void setTargetStoreStockService(final TargetStoreStockService targetStoreStockService) {
        this.targetStoreStockService = targetStoreStockService;
    }

    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }

    @Required
    public void setTargetPointOfServiceService(final TargetPointOfServiceService targetPointOfServiceService) {
        this.targetPointOfServiceService = targetPointOfServiceService;
    }

    /**
     * @param fluentStockLookupService
     *            the fluentStockLookupService to set
     */
    @Required
    public void setFluentStockLookupService(final FluentStockLookupService fluentStockLookupService) {
        this.fluentStockLookupService = fluentStockLookupService;
    }

    /**
     * @param stockStatusLevel
     *            the stockStatusLevel to set
     */
    @Required
    public void setStockStatusLevel(final String[] stockStatusLevel) {
        this.stockStatusLevel = stockStatusLevel;
    }

    /**
     * 
     * @param targetGoogleLocationFinderFacade
     *            the targetGoogleLocationFinderFacade to set
     */
    @Required
    public void setTargetGoogleLocationFinderFacade(
            final TargetGoogleLocationFinderFacade targetGoogleLocationFinderFacade) {
        this.targetGoogleLocationFinderFacade = targetGoogleLocationFinderFacade;
    }

}

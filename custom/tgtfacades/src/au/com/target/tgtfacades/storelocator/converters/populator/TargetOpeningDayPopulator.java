/**
 *
 */
package au.com.target.tgtfacades.storelocator.converters.populator;


import de.hybris.platform.converters.Populator;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Minutes;
import org.joda.time.ReadableInstant;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import au.com.target.tgtcore.model.TargetOpeningDayModel;
import au.com.target.tgtfacades.storelocator.data.TargetOpeningDayData;


/**
 * @author rmcalave
 *
 */
public class TargetOpeningDayPopulator implements Populator<TargetOpeningDayModel, TargetOpeningDayData> {
    private static final DateTimeFormatter HOUR_FORMATTER = DateTimeFormat.forPattern("h:mm a");

    private static final String MIDDAY = "Midday";
    private static final String MIDNIGHT = "Midnight";
    private static final String TODAY = "Today";
    private static final String TOMORROW = "Tomorrow";


    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.impl.AbstractPopulatingConverter#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final TargetOpeningDayModel source, final TargetOpeningDayData target) {
        final DateTime openingDateTime = new DateTime(source.getOpeningTime());
        target.setFormattedDay(openingDateTime.dayOfWeek().getAsText());
        target.setFormattedRelativeDay(getFormattedRelativeDay(openingDateTime));
        target.setOpeningDateTime(openingDateTime);

        final DateTime closingDateTime = new DateTime(source.getClosingTime());
        target.setClosingDateTime(closingDateTime);

        target.setClosed(source.isClosed());
        target.setMessage(source.getMessage());

        target.setFormattedOpeningTime(formatStartOfDayTradingHour(openingDateTime));
        target.setFormattedClosingTime(formatEndOfDayTradingHour(openingDateTime, closingDateTime));

        target.setAllDayTrade(MIDNIGHT.equals(target.getFormattedOpeningTime())
                && MIDNIGHT.equals(target.getFormattedClosingTime()));
    }

    private String formatStartOfDayTradingHour(final DateTime tradingDateTime) {
        final DateTime startOfOpeningDay = tradingDateTime.withTimeAtStartOfDay();
        return formatTradingHour(tradingDateTime, startOfOpeningDay);
    }

    protected String formatEndOfDayTradingHour(final DateTime openingDateTime, final DateTime closingDateTime) {
        final DateTime startOfNextDay = openingDateTime.plusDays(1).withTimeAtStartOfDay();
        return formatTradingHour(closingDateTime, startOfNextDay);
    }

    private String formatTradingHour(final DateTime tradingHour, final ReadableInstant startOfDay) {
        final int minutesBetweenTradingHourAndMidnight = Math.abs(Minutes.minutesBetween(startOfDay,
                tradingHour)
                .getMinutes());
        if (minutesBetweenTradingHourAndMidnight <= 1) {
            return MIDNIGHT;
        }
        else {
            final DateTime middayOfTradingDay = new DateTime(tradingHour).withTime(12, 0, 0, 0);
            final int minutesBetweenTradingHourAndMidday = Math.abs(Minutes.minutesBetween(middayOfTradingDay,
                    tradingHour)
                    .getMinutes());

            if (minutesBetweenTradingHourAndMidday <= 1) {
                return MIDDAY;
            }
        }

        return HOUR_FORMATTER.print(tradingHour);
    }

    private String getFormattedRelativeDay(final DateTime openingDateTime) {
        final LocalDate startOfToday = DateTime.now().toLocalDate();
        final LocalDate openingDate = openingDateTime.toLocalDate();

        final Days daysBetween = Days.daysBetween(startOfToday, openingDate);
        final int days = daysBetween.getDays();

        if (days == 0) {
            return TODAY;
        }
        else if (days == 1) {
            return TOMORROW;
        }

        return null;
    }
}

/**
 *
 */
package au.com.target.tgtfacades.storelocator.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.storelocator.model.OpeningDayModel;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import au.com.target.tgtcore.model.TargetOpeningDayModel;
import au.com.target.tgtfacades.storelocator.data.TargetOpeningDayData;
import au.com.target.tgtfacades.storelocator.data.TargetOpeningScheduleData;
import au.com.target.tgtfacades.storelocator.data.TargetOpeningWeekData;


/**
 * @author rmcalave
 *
 */
public class TargetOpeningSchedulePopulator implements Populator<OpeningScheduleModel, TargetOpeningScheduleData> {
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("E, d MMM");

    private Converter<TargetOpeningDayModel, TargetOpeningDayData> targetOpeningDayConverter;


    /* (non-Javadoc)
     * @see de.hybris.platform.commercefacades.storelocator.converters.OpeningScheduleConverter#populate(de.hybris.platform.storelocator.model.OpeningScheduleModel, de.hybris.platform.commercefacades.storelocator.data.OpeningScheduleData)
     */
    @Override
    public void populate(final OpeningScheduleModel source, final TargetOpeningScheduleData target) {

        final TargetOpeningScheduleData targetOpeningScheduleData = target;

        final List<TargetOpeningWeekData> targetOpeningWeeks = new ArrayList<>();

        final List<TargetOpeningDayData> openingDays = new ArrayList<>();

        for (final OpeningDayModel openingDayModel : source.getOpeningDays()) {
            if (!(openingDayModel instanceof TargetOpeningDayModel)) {
                continue;
            }

            openingDays.add(targetOpeningDayConverter.convert((TargetOpeningDayModel)openingDayModel));
        }

        if (openingDays.isEmpty()) {
            targetOpeningScheduleData.setTargetOpeningWeeks(targetOpeningWeeks);
            return;
        }

        Collections.sort(openingDays, new Comparator<TargetOpeningDayData>() {
            /* (non-Javadoc)
             * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
             */
            @Override
            public int compare(final TargetOpeningDayData o1, final TargetOpeningDayData o2) {
                return o1.getOpeningDateTime().compareTo(o2.getOpeningDateTime());
            }
        });

        final LocalDate startOfThisWeek = LocalDate.now().withDayOfWeek(DateTimeConstants.MONDAY);

        final Map<LocalDate, List<TargetOpeningDayData>> openingDaysMap = new TreeMap<LocalDate, List<TargetOpeningDayData>>();
        for (final TargetOpeningDayData openingDayData : openingDays) {
            final LocalDate openingDate = openingDayData.getOpeningDateTime().toLocalDate();

            if (openingDate.isBefore(startOfThisWeek)) {
                continue;
            }

            final LocalDate startOfWeek = new LocalDate(openingDate).withDayOfWeek(DateTimeConstants.MONDAY);

            List<TargetOpeningDayData> openingDaysForWeek = openingDaysMap.get(startOfWeek);
            if (openingDaysForWeek == null) {
                openingDaysForWeek = new ArrayList<>();
                openingDaysMap.put(startOfWeek, openingDaysForWeek);
            }

            openingDaysForWeek.add(openingDayData);
        }


        for (final LocalDate startOfWeek : openingDaysMap.keySet()) {
            final LocalDate endOfWeek = startOfWeek.withDayOfWeek(DateTimeConstants.SUNDAY);

            final TargetOpeningWeekData thisWeek = new TargetOpeningWeekData();
            thisWeek.setStartDate(startOfWeek);
            thisWeek.setEndDate(endOfWeek);
            thisWeek.setFormattedStartDate(DATE_FORMATTER.print(startOfWeek));
            thisWeek.setFormattedEndDate(DATE_FORMATTER.print(endOfWeek));
            thisWeek.setTargetOpeningDayList(openingDaysMap.get(startOfWeek));
            targetOpeningWeeks.add(thisWeek);
        }

        targetOpeningScheduleData.setTargetOpeningWeeks(targetOpeningWeeks);
    }

    /**
     * @param targetOpeningDayConverter
     *            the targetOpeningDayConverter to set
     */
    public void setTargetOpeningDayConverter(
            final Converter<TargetOpeningDayModel, TargetOpeningDayData> targetOpeningDayConverter) {
        this.targetOpeningDayConverter = targetOpeningDayConverter;
    }
}

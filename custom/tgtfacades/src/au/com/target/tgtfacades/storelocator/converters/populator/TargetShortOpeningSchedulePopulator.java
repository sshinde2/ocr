/**
 * 
 */
package au.com.target.tgtfacades.storelocator.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.storelocator.model.OpeningDayModel;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetOpeningDayModel;
import au.com.target.tgtfacades.storelocator.data.TargetOpeningDayData;
import au.com.target.tgtfacades.storelocator.data.TargetShortOpeningScheduleData;


/**
 * @author rmcalave
 *
 */
public class TargetShortOpeningSchedulePopulator
        implements Populator<OpeningScheduleModel, TargetShortOpeningScheduleData> {

    private Converter<TargetOpeningDayModel, TargetOpeningDayData> targetOpeningDayConverter;

    private int numberOfDays;

    /* (non-Javadoc)
     * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final OpeningScheduleModel source, final TargetShortOpeningScheduleData target)
            throws ConversionException {
        final List<OpeningDayModel> openingDays = new ArrayList<OpeningDayModel>(source.getOpeningDays());

        CollectionUtils.filter(openingDays, new Predicate() {

            @Override
            public boolean evaluate(final Object openingDay) {
                final LocalDate openingDate = LocalDate.fromDateFields(((OpeningDayModel)openingDay).getOpeningTime());
                final LocalDate now = LocalDate.now();

                final Days daysBetween = Days.daysBetween(now, openingDate);

                return daysBetween.getDays() >= 0 && daysBetween.getDays() < numberOfDays;
            }

        });

        if (openingDays.isEmpty()) {
            return;
        }

        final List<TargetOpeningDayData> openingDayDatas = new ArrayList<>();

        for (final OpeningDayModel openingDay : openingDays) {
            openingDayDatas.add(targetOpeningDayConverter.convert((TargetOpeningDayModel)openingDay));
        }

        target.setOpeningDays(openingDayDatas);
    }

    /**
     * @param targetOpeningDayConverter
     *            the targetOpeningDayConverter to set
     */
    @Required
    public void setTargetOpeningDayConverter(
            final Converter<TargetOpeningDayModel, TargetOpeningDayData> targetOpeningDayConverter) {
        this.targetOpeningDayConverter = targetOpeningDayConverter;
    }

    /**
     * @param numberOfDays
     *            the numberOfDays to set
     */
    @Required
    public void setNumberOfDays(final int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

}

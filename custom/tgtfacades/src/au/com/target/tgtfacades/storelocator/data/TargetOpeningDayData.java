/**
 * 
 */
package au.com.target.tgtfacades.storelocator.data;

import org.joda.time.DateTime;


/**
 * @author rmcalave
 * 
 */
public class TargetOpeningDayData {
    private DateTime openingDateTime;
    private DateTime closingDateTime;

    private String formattedDay;
    private String formattedRelativeDay;
    private String formattedOpeningTime;
    private String formattedClosingTime;

    private boolean closed;
    private boolean allDayTrade;

    private String message;

    /**
     * @return the openingDateTime
     */
    public DateTime getOpeningDateTime() {
        return openingDateTime;
    }

    /**
     * @param openingDateTime
     *            the openingDateTime to set
     */
    public void setOpeningDateTime(final DateTime openingDateTime) {
        this.openingDateTime = openingDateTime;
    }

    /**
     * @return the closingDateTime
     */
    public DateTime getClosingDateTime() {
        return closingDateTime;
    }

    /**
     * @param closingDateTime
     *            the closingDateTime to set
     */
    public void setClosingDateTime(final DateTime closingDateTime) {
        this.closingDateTime = closingDateTime;
    }

    /**
     * @return the formattedDay
     */
    public String getFormattedDay() {
        return formattedDay;
    }

    /**
     * @param formattedDay
     *            the formattedDay to set
     */
    public void setFormattedDay(final String formattedDay) {
        this.formattedDay = formattedDay;
    }

    /**
     * @return the formattedRelativeDay
     */
    public String getFormattedRelativeDay() {
        return formattedRelativeDay;
    }

    /**
     * @param formattedRelativeDay
     *            the formattedRelativeDay to set
     */
    public void setFormattedRelativeDay(final String formattedRelativeDay) {
        this.formattedRelativeDay = formattedRelativeDay;
    }

    /**
     * @return the formattedOpeningTime
     */
    public String getFormattedOpeningTime() {
        return formattedOpeningTime;
    }

    /**
     * @param formattedOpeningTime
     *            the formattedOpeningTime to set
     */
    public void setFormattedOpeningTime(final String formattedOpeningTime) {
        this.formattedOpeningTime = formattedOpeningTime;
    }

    /**
     * @return the formattedClosingTime
     */
    public String getFormattedClosingTime() {
        return formattedClosingTime;
    }

    /**
     * @param formattedClosingTime
     *            the formattedClosingTime to set
     */
    public void setFormattedClosingTime(final String formattedClosingTime) {
        this.formattedClosingTime = formattedClosingTime;
    }

    /**
     * @return the closed
     */
    public boolean isClosed() {
        return closed;
    }

    /**
     * @param closed
     *            the closed to set
     */
    public void setClosed(final boolean closed) {
        this.closed = closed;
    }

    /**
     * @return the allDayTrade
     */
    public boolean isAllDayTrade() {
        return allDayTrade;
    }

    /**
     * @param allDayTrade
     *            the allDayTrade to set
     */
    public void setAllDayTrade(final boolean allDayTrade) {
        this.allDayTrade = allDayTrade;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(final String message) {
        this.message = message;
    }
}

/**
 * 
 */
package au.com.target.tgtfacades.storelocator.data;

import de.hybris.platform.commercefacades.storelocator.data.OpeningScheduleData;

import java.util.List;


/**
 * @author rmcalave
 * 
 */
public class TargetOpeningScheduleData extends OpeningScheduleData {
    private List<TargetOpeningWeekData> targetOpeningWeeks;

    /**
     * @return the targetOpeningWeeks
     */
    public List<TargetOpeningWeekData> getTargetOpeningWeeks() {
        return targetOpeningWeeks;
    }

    /**
     * @param targetOpeningWeeks
     *            the targetOpeningWeeks to set
     */
    public void setTargetOpeningWeeks(final List<TargetOpeningWeekData> targetOpeningWeeks) {
        this.targetOpeningWeeks = targetOpeningWeeks;
    }
}

/**
 * 
 */
package au.com.target.tgtfacades.storelocator.data;

import java.util.List;

import org.joda.time.LocalDate;


/**
 * @author rmcalave
 * 
 */
public class TargetOpeningWeekData {
    private LocalDate startDate;
    private LocalDate endDate;

    private String formattedStartDate;
    private String formattedEndDate;

    private List<TargetOpeningDayData> targetOpeningDayList;

    /**
     * @return the startDate
     */
    public LocalDate getStartDate() {
        return startDate;
    }

    /**
     * @param startDate
     *            the startDate to set
     */
    public void setStartDate(final LocalDate startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public LocalDate getEndDate() {
        return endDate;
    }

    /**
     * @param endDate
     *            the endDate to set
     */
    public void setEndDate(final LocalDate endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the formattedStartDate
     */
    public String getFormattedStartDate() {
        return formattedStartDate;
    }

    /**
     * @param formattedStartDate
     *            the formattedStartDate to set
     */
    public void setFormattedStartDate(final String formattedStartDate) {
        this.formattedStartDate = formattedStartDate;
    }

    /**
     * @return the formattedEndDate
     */
    public String getFormattedEndDate() {
        return formattedEndDate;
    }

    /**
     * @param formattedEndDate
     *            the formattedEndDate to set
     */
    public void setFormattedEndDate(final String formattedEndDate) {
        this.formattedEndDate = formattedEndDate;
    }

    /**
     * @return the targetOpeningDayList
     */
    public List<TargetOpeningDayData> getTargetOpeningDayList() {
        return targetOpeningDayList;
    }

    /**
     * @param targetOpeningDayList
     *            the targetOpeningDayList to set
     */
    public void setTargetOpeningDayList(final List<TargetOpeningDayData> targetOpeningDayList) {
        this.targetOpeningDayList = targetOpeningDayList;
    }
}

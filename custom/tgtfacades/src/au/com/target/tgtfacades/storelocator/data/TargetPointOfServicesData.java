/*
 *
 */
package au.com.target.tgtfacades.storelocator.data;

import java.io.Serializable;
import java.util.List;


public class TargetPointOfServicesData implements Serializable {

    private List<TargetPointOfServiceData> targetPointOfServices;


    public void setTargetPointOfServices(final List<TargetPointOfServiceData> targetPointOfServices) {
        this.targetPointOfServices = targetPointOfServices;
    }


    public List<TargetPointOfServiceData> getTargetPointOfServices() {
        return targetPointOfServices;
    }

}
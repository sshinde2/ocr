/**
 * 
 */
package au.com.target.tgtfacades.storelocator.data;

import java.util.List;


/**
 * @author rmcalave
 *
 */
public class TargetShortOpeningScheduleData extends TargetOpeningScheduleData {

    private List<TargetOpeningDayData> openingDays;

    /**
     * @return the openingDays
     */
    public List<TargetOpeningDayData> getOpeningDays() {
        return openingDays;
    }

    /**
     * @param openingDays
     *            the openingDays to set
     */
    public void setOpeningDays(final List<TargetOpeningDayData> openingDays) {
        this.openingDays = openingDays;
    }

}

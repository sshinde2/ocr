/**
 * 
 */
package au.com.target.tgtfacades.storelocator.utils;

import de.hybris.platform.commercefacades.storelocator.data.OpeningScheduleData;
import de.hybris.platform.commercefacades.storelocator.data.WeekdayOpeningDayData;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;

import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;


public final class StoreLocatorUtil {

    private static final String CLOSED = "Closed";

    private StoreLocatorUtil() {
        //prevent construction
    }

    /**
     * Formatted Opening Schedule for TargetPointOfServiceData Data Object
     * 
     * @param pointOfService
     * @return Map<String, String>
     */
    public static Map<String, String> getFormattedStoreOpeningSchedule(
            final TargetPointOfServiceData pointOfService) {
        Assert.notNull(pointOfService);

        final OpeningScheduleData scheduleData = pointOfService.getOpeningHours();
        Assert.notNull(scheduleData);

        final List<WeekdayOpeningDayData> openingDayData = scheduleData.getWeekDayOpeningList();

        if (CollectionUtils.isEmpty(openingDayData)) {
            return Collections.EMPTY_MAP;
        }

        final Map<String, String> openingDays = new LinkedHashMap<String, String>();

        for (final WeekdayOpeningDayData openDayData : openingDayData) {
            final String dayOfWeek = openDayData.getWeekDay();
            final String openTime = openDayData.getOpeningTime() == null ? "" : openDayData.getOpeningTime()
                    .getFormattedHour();
            final String closeTime = openDayData.getClosingTime() == null ? "" : openDayData.getClosingTime()
                    .getFormattedHour();
            final boolean isClosed = openDayData.isClosed();

            openingDays.put(dayOfWeek, buildStoreHours(openTime, closeTime, isClosed));
        }

        return openingDays;

    }

    private static String buildStoreHours(final String openTime, final String closeTime,
            final boolean isClosed) {
        if (isClosed) {
            return CLOSED;
        }

        return openTime + " - " + closeTime;
    }
}

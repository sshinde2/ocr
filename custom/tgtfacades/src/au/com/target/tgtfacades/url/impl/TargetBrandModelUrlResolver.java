package au.com.target.tgtfacades.url.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;
import au.com.target.tgtfacades.url.transform.UrlTransformer;


/**
 * Resolver that will work with the Target brand types.
 */
public class TargetBrandModelUrlResolver extends AbstractUrlResolver<BrandModel> {

    private static final String CACHE_KEY = TargetBrandModelUrlResolver.class.getName();

    private String pattern;
    private List<UrlTransformer> urlTransformers = new ArrayList<>();
    private List<UrlTokenTransformer> urlTokenTransformers = new ArrayList<>();

    @Override
    protected String resolveInternal(final BrandModel source)
    {
        // Work out values
        final String brandName = urlSafe(source.getName());
        final String brandCode = source.getCode();

        // Replace pattern values
        String url = getPattern();
        url = url.replace("{brand-name}", brandName);
        url = url.replace("{brand-code}", brandCode);

        for (final UrlTransformer urlTransformer : getUrlTransformers()) {
            url = urlTransformer.transform(url);
        }
        return url;

    }

    @Override
    protected String urlSafe(final String text) {
        String transformedText = text;
        for (final UrlTokenTransformer urlTokenTransformer : getUrlTokenTransformers()) {
            transformedText = urlTokenTransformer.transform(transformedText);
        }
        return super.urlSafe(transformedText);
    }

    protected String getPattern()
    {
        return pattern;
    }

    @Required
    public void setPattern(final String pattern)
    {
        this.pattern = pattern;
    }

    protected String getKey(final CategoryModel source)
    {
        return CACHE_KEY + "." + source.getPk().toString();
    }

    /**
     * Returns the list of url level transformers.
     * 
     * @return the list of transformers
     */
    public List<UrlTransformer> getUrlTransformers() {
        return urlTransformers;
    }

    /**
     * Sets the list of url level transformers
     * 
     * @param urlTransformers
     *            the list of transformers to set
     */
    public void setUrlTransformers(final List<UrlTransformer> urlTransformers) {
        this.urlTransformers = urlTransformers;
    }

    /**
     * Returns the list of url token level transformers.
     * 
     * @return the list of transformers
     */
    public List<UrlTokenTransformer> getUrlTokenTransformers() {
        return urlTokenTransformers;
    }

    /**
     * Sets the list of url token level transformers.
     * 
     * @param urlTokenTransformers
     *            the list of transformers to set
     */
    public void setUrlTokenTransformers(final List<UrlTokenTransformer> urlTokenTransformers) {
        this.urlTokenTransformers = urlTokenTransformers;
    }
}

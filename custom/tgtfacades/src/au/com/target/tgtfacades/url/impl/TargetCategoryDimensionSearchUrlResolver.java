/**
 * 
 */
package au.com.target.tgtfacades.url.impl;

import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;

import com.endeca.infront.cartridge.model.Ancestor;
import com.endeca.infront.cartridge.model.DimensionSearchValue;


/**
 * @author bhuang3
 *
 */
public class TargetCategoryDimensionSearchUrlResolver extends AbstractUrlResolver<DimensionSearchValue> {

    private static final String BASE_CATEGORY_URL = "/c";
    private static final String SLASH = "/";
    private static final String CODE = "code";

    private List<UrlTokenTransformer> urlTokenTransformers = new ArrayList<>();

    @Override
    protected String resolveInternal(final DimensionSearchValue dimensionSearchValue) {
        final StringBuilder url = new StringBuilder();
        if (dimensionSearchValue != null) {
            final String navigationState = dimensionSearchValue.getNavigationState();
            final StringBuilder prependUrl = new StringBuilder(BASE_CATEGORY_URL);
            final List<Ancestor> ancestorList = dimensionSearchValue.getAncestors();
            if (CollectionUtils.isNotEmpty(ancestorList)) {
                for (final Ancestor ancestor : ancestorList) {
                    if (StringUtils.isNotEmpty(ancestor.getLabel())) {
                        prependUrl.append(SLASH).append(urlSafe(ancestor.getLabel()));
                    }
                }
            }
            final String categorylabel = dimensionSearchValue.getLabel();
            if (StringUtils.isNotEmpty(categorylabel)) {
                prependUrl.append(SLASH).append(urlSafe(categorylabel));
            }
            final Map<String, String> propertiesMap = dimensionSearchValue.getProperties();
            if (MapUtils.isNotEmpty(propertiesMap)) {
                final String categoryCode = propertiesMap.get(CODE);
                if (StringUtils.isNotEmpty(categoryCode)) {
                    url.append(prependUrl).append(SLASH).append(categoryCode);
                    if (StringUtils.isNotEmpty(navigationState)) {
                        url.append(navigationState);
                    }
                }
            }
        }
        return url.toString();
    }


    @Override
    protected String urlSafe(final String text) {
        String transformedText = text;
        for (final UrlTokenTransformer urlTokenTransformer : getUrlTokenTransformers()) {
            transformedText = urlTokenTransformer.transform(transformedText);
        }
        return super.urlSafe(transformedText);
    }


    @Required
    public void setUrlTokenTransformers(final List<UrlTokenTransformer> urlTokenTransformers) {
        this.urlTokenTransformers = urlTokenTransformers;
    }

    /**
     * @return the urlTokenTransformers
     */
    protected List<UrlTokenTransformer> getUrlTokenTransformers() {
        return urlTokenTransformers;
    }

}

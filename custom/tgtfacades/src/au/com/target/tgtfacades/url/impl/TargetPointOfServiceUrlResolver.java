/**
 * 
 */
package au.com.target.tgtfacades.url.impl;

import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;


/**
 * @author mjanarth
 * 
 */
public class TargetPointOfServiceUrlResolver extends AbstractUrlResolver<TargetPointOfServiceModel> {


    private List<UrlTokenTransformer> urlTokenTransformers = new ArrayList<>();

    @Override
    protected String resolveInternal(final TargetPointOfServiceModel source) {

        final String encodedStoreName = urlSafe(source.getName());
        final String state = source.getAddress() == null ? "" : source.getAddress().getDistrict();
        final String resolvedUrl = String.format("/store/%s/%s/%d",
                state.toLowerCase(), encodedStoreName,
                source.getStoreNumber());
        return resolvedUrl;
    }

    @Override
    protected String urlSafe(final String text) {
        String transformedText = text;
        for (final UrlTokenTransformer urlTokenTransformer : getUrlTokenTransformers()) {
            transformedText = urlTokenTransformer.transform(transformedText);
        }
        return super.urlSafe(transformedText);
    }

    /**
     * @return the urlTokenTransformers
     */
    public List<UrlTokenTransformer> getUrlTokenTransformers() {
        return urlTokenTransformers;
    }

    @Required
    public void setUrlTokenTransformers(final List<UrlTokenTransformer> urlTokenTransformers) {
        this.urlTokenTransformers = urlTokenTransformers;
    }


}

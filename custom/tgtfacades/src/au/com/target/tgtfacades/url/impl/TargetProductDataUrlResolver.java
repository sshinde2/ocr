/**
 * 
 */
package au.com.target.tgtfacades.url.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.url.impl.DefaultProductModelUrlResolver;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;


/**
 * @author pthoma20
 * 
 * 
 *         This is a basic class which will give the url for a product with {product-name}/{product-code} based on the
 *         product name and code which is being passed to it.
 */
public class TargetProductDataUrlResolver extends DefaultProductModelUrlResolver {

    private List<UrlTokenTransformer> urlTokenTransformers = new ArrayList<>();

    public String resolve(final String productName, final String productCode) {
        final String url = resolveInternal(productName, productCode);
        return url;
    }

    protected String resolveInternal(final String productName, final String productCode) {
        final BaseSiteModel currentBaseSite = getBaseSiteService().getCurrentBaseSite();
        String url = getPattern();
        if (currentBaseSite != null) {
            url = url.replace("{baseSite-uid}", currentBaseSite.getUid());
        }
        url = url.replace("{product-name}", urlSafe(productName));
        url = url.toLowerCase();
        if (StringUtils.isNotBlank(productCode)) {
            url = url.replace("{product-code}", productCode);
        }
        return url;
    }

    @Override
    protected String urlSafe(final String text) {
        if (StringUtils.isEmpty(text)) {
            return StringUtils.EMPTY;
        }
        String transformedText = text;
        for (final UrlTokenTransformer urlTokenTransformer : getUrlTokenTransformers()) {
            transformedText = urlTokenTransformer.transform(transformedText);
        }
        return super.urlSafe(transformedText);
    }


    /**
     * @return the urlTokenTransformers
     */
    public List<UrlTokenTransformer> getUrlTokenTransformers() {
        return urlTokenTransformers;
    }


    /**
     * @param urlTokenTransformers
     *            the urlTokenTransformers to set
     */
    @Required
    public void setUrlTokenTransformers(final List<UrlTokenTransformer> urlTokenTransformers) {
        this.urlTokenTransformers = urlTokenTransformers;
    }


}

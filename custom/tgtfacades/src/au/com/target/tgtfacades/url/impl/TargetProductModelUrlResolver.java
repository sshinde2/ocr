package au.com.target.tgtfacades.url.impl;

import de.hybris.platform.commerceservices.url.impl.DefaultProductModelUrlResolver;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.List;

import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;
import au.com.target.tgtfacades.url.transform.UrlTransformer;

/**
 * Extension to {@link DefaultProductModelUrlResolver} which applies url and token -level
 * product URL transformations, imposed by Target standards.
 */
public class TargetProductModelUrlResolver extends DefaultProductModelUrlResolver {

    private List<UrlTransformer> urlTransformers = new ArrayList<>();
    private List<UrlTokenTransformer> urlTokenTransformers = new ArrayList<>();

    @Override
    protected String resolveInternal(final ProductModel source) {
        String url = super.resolveInternal(source);
        for (UrlTransformer urlTransformer : getUrlTransformers()) {
            url = urlTransformer.transform(url);
        }
        return url;
    }

    @Override
    protected String urlSafe(final String text) {
        String transformedText = text;
        for (UrlTokenTransformer urlTokenTransformer : getUrlTokenTransformers()) {
            transformedText = urlTokenTransformer.transform(transformedText);
        }
        return super.urlSafe(transformedText);
    }

    /**
     * Returns the list of url level transformers.
     *
     * @return the list of transformers
     */
    public List<UrlTransformer> getUrlTransformers() {
        return urlTransformers;
    }

    /**
     * Sets the list of url level transformers
     *
     * @param urlTransformers the list of transformers to set
     */
    public void setUrlTransformers(final List<UrlTransformer> urlTransformers) {
        this.urlTransformers = urlTransformers;
    }

    /**
     * Returns the list of url token level transformers.
     *
     * @return the list of transformers
     */
    public List<UrlTokenTransformer> getUrlTokenTransformers() {
        return urlTokenTransformers;
    }

    /**
     * Sets the list of url token level transformers.
     *
     * @param urlTokenTransformers the list of transformers to set
     */
    public void setUrlTokenTransformers(final List<UrlTokenTransformer> urlTokenTransformers) {
        this.urlTokenTransformers = urlTokenTransformers;
    }
}

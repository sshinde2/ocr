/**
 * 
 */
package au.com.target.tgtfacades.url.impl;

import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;


/**
 * @author bhuang3
 * 
 */
public class TargetShopTheLookUrlResolver extends AbstractUrlResolver<TargetShopTheLookModel> {


    private List<UrlTokenTransformer> urlTokenTransformers;

    @Override
    protected String resolveInternal(final TargetShopTheLookModel source) {

        final String encodedName = urlSafe(source.getName());
        final String resolvedUrl = String.format("/inspiration/%s/%s", encodedName, source.getId());
        return resolvedUrl;
    }

    @Override
    protected String urlSafe(final String text) {
        String transformedText = text;
        for (final UrlTokenTransformer urlTokenTransformer : getUrlTokenTransformers()) {
            transformedText = urlTokenTransformer.transform(transformedText);
        }
        return super.urlSafe(transformedText);
    }

    /**
     * @return the urlTokenTransformers
     */
    public List<UrlTokenTransformer> getUrlTokenTransformers() {
        return urlTokenTransformers;
    }

    @Required
    public void setUrlTokenTransformers(final List<UrlTokenTransformer> urlTokenTransformers) {
        this.urlTokenTransformers = urlTokenTransformers;
    }


}

package au.com.target.tgtfacades.url.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;
import au.com.target.tgtfacades.url.transform.UrlTransformer;


/**
 * Resolver that will work with the Target brand types.
 */
public class TargetSizeTypeModelUrlResolver extends AbstractUrlResolver<SizeTypeModel> {

    private static final String CACHE_KEY = TargetSizeTypeModelUrlResolver.class.getName();

    private String pattern;
    private List<UrlTransformer> urlTransformers = new ArrayList<>();
    private List<UrlTokenTransformer> urlTokenTransformers = new ArrayList<>();

    @Override
    protected String resolveInternal(final SizeTypeModel source)
    {
        if (null != source && null != source.getCode()) {
            // Replace pattern values
            String url = getPattern();
            url = url.replace("{sizetype.code}", source.getCode());

            for (final UrlTransformer urlTransformer : getUrlTransformers()) {
                url = urlTransformer.transform(url);
            }
            if (StringUtils.isNotBlank(url))
            {
                return url;
            }
        }
        return null;

    }

    @Override
    protected String urlSafe(final String text) {
        String transformedText = text;
        for (final UrlTokenTransformer urlTokenTransformer : getUrlTokenTransformers()) {
            transformedText = urlTokenTransformer.transform(transformedText);
        }
        return super.urlSafe(transformedText);
    }

    /**
     * This is pattern of ULR
     * 
     * @return pattern
     */
    protected String getPattern()
    {
        return pattern;
    }

    /**
     * Sets the pattern of URL
     * 
     * @param pattern
     *            URL Pattern
     */
    @Required
    public void setPattern(final String pattern)
    {
        this.pattern = pattern;
    }

    protected String getKey(final CategoryModel source)
    {
        return CACHE_KEY + "." + source.getPk().toString();
    }

    /**
     * Returns the list of url level transformers.
     * 
     * @return the list of transformers
     */
    public List<UrlTransformer> getUrlTransformers() {
        return urlTransformers;
    }

    /**
     * Sets the list of url level transformers
     * 
     * @param urlTransformers
     *            the list of transformers to set
     */
    public void setUrlTransformers(final List<UrlTransformer> urlTransformers) {
        this.urlTransformers = urlTransformers;
    }

    /**
     * @return the urlTokenTransformers
     */
    public List<UrlTokenTransformer> getUrlTokenTransformers() {
        return urlTokenTransformers;
    }

    /**
     * @param urlTokenTransformers
     *            the urlTokenTransformers to set
     */
    public void setUrlTokenTransformers(final List<UrlTokenTransformer> urlTokenTransformers) {
        this.urlTokenTransformers = urlTokenTransformers;
    }

}

package au.com.target.tgtfacades.url.transform;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;


/**
 * Transforms URL tokens according to the Target standards. So far, they are:
 * 
 * <ul>
 * <li>category or product or store name should only include alphanumerics and dashes to replace spaces</li>
 * <li>no double dashes</li>
 * <li>url should be lower case</li>
 * </ul>
 */
public class TargetUrlTokenTransformer implements UrlTokenTransformer {

    private static final Map<Pattern, String> REGEXP_MAP = new LinkedHashMap<Pattern, String>();

    /**
     * Initializes this transformer with the list of patterns used for data substitution.
     */
    @PostConstruct
    public void initialize() {
        REGEXP_MAP.put(Pattern.compile("[^A-Za-z1-9\\-]"), "-"); // replace all non-alphanumeric characters with hyphens
        REGEXP_MAP.put(Pattern.compile("-+"), "-"); // collapse consequent hyphens
        REGEXP_MAP.put(Pattern.compile("^\\-|\\-$"), ""); // remove leading and trailing hyphens
    }

    @Override
    public String transform(final String token) {
        String result = token;
        for (final Map.Entry<Pattern, String> entry : REGEXP_MAP.entrySet()) {
            result = entry.getKey().matcher(result).replaceAll(entry.getValue());
        }
        return result.toLowerCase();
    }
}

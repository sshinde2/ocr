/**
 *
 */
package au.com.target.tgtfacades.user.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;

import org.apache.commons.lang.BooleanUtils;

import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtutility.format.PhoneFormat;


/**
 * @author rmcalave
 *
 */
public class TargetAddressPopulator implements Populator<AddressModel, TargetAddressData> {

    @Override
    public void populate(final AddressModel source, final TargetAddressData target) {
        target.setState(source.getDistrict());
        target.setPhone(PhoneFormat.formatNumber(source.getPhone1()));

        if (!(source instanceof TargetAddressModel)) {
            return;
        }

        final TargetAddressModel tgtSource = (TargetAddressModel)source;
        target.setAddressValidated(BooleanUtils.toBoolean(tgtSource.getAddressValidated()));
    }

}

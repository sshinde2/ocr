/**
 * 
 */
package au.com.target.tgtfacades.user.converters.populator;

import de.hybris.platform.commercefacades.user.converters.populator.AddressReversePopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtfacades.user.data.TargetAddressData;


/**
 * @author rmcalave
 * 
 */
public class TargetAddressReversePopulator extends AddressReversePopulator {

    /* (non-Javadoc)
     * @see de.hybris.platform.commercefacades.user.converters.populator.AddressReversePopulator#populate(de.hybris.platform.commercefacades.user.data.AddressData, de.hybris.platform.core.model.user.AddressModel)
     */
    @Override
    public void populate(final AddressData addressData, final AddressModel addressModel) throws ConversionException {
        super.populate(addressData, addressModel);

        if (!(addressData instanceof TargetAddressData && addressModel instanceof TargetAddressModel)) {
            return;
        }

        final TargetAddressData tgtAddressData = (TargetAddressData)addressData;
        final TargetAddressModel tgtAddressModel = (TargetAddressModel)addressModel;

        tgtAddressModel.setDistrict(tgtAddressData.getState());
        tgtAddressModel.setAddressValidated(Boolean.valueOf(tgtAddressData.isAddressValidated()));
    }

}

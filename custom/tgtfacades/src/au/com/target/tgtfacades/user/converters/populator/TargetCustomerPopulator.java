/**
 * 
 */
package au.com.target.tgtfacades.user.converters.populator;

import de.hybris.platform.commercefacades.storesession.data.CurrencyData;
import de.hybris.platform.commercefacades.storesession.data.LanguageData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.user.data.TargetCustomerData;


/**
 * @author ayushman
 *
 */
public class TargetCustomerPopulator implements Populator<UserModel, TargetCustomerData> {

    private Converter<CurrencyModel, CurrencyData> currencyConverter;
    private Converter<LanguageModel, LanguageData> languageConverter;
    private Converter<AddressModel, AddressData> addressConverter;

    @Override
    public void populate(final UserModel source, final TargetCustomerData target) throws ConversionException {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        if (source.getSessionCurrency() != null) {
            target.setCurrency(currencyConverter.convert(source.getSessionCurrency()));
        }
        if (source.getDefaultPaymentAddress() != null) {
            target.setDefaultBillingAddress(addressConverter.convert(source.getDefaultPaymentAddress()));
        }
        if (source.getDefaultShipmentAddress() != null) {
            target.setDefaultShippingAddress(addressConverter.convert(source.getDefaultShipmentAddress()));
        }
        if (source.getSessionLanguage() != null) {
            target.setLanguage(languageConverter.convert(source.getSessionLanguage()));
        }
        if (source instanceof CustomerModel) {
            final TitleModel title = ((CustomerModel)source).getTitle();
            if (title != null) {
                target.setTitleCode(title.getCode());
                target.setTitle(title.getName());
            }

            if (source instanceof TargetCustomerModel) {
                final TargetCustomerModel targetCustomerModel = (TargetCustomerModel)source;
                target.setFirstName(targetCustomerModel.getFirstname());
                target.setLastName(targetCustomerModel.getLastname());
                target.setAllowTracking(targetCustomerModel.getAllowTracking() == null ? false
                        : targetCustomerModel.getAllowTracking().booleanValue());
                target.setTrackingGuid(targetCustomerModel.getTrackingGuid());
            }
        }

        target.setUid(source.getUid());
        target.setName(source.getName());
    }

    @Required
    public void setCurrencyConverter(final Converter<CurrencyModel, CurrencyData> currencyConverter) {
        this.currencyConverter = currencyConverter;
    }

    @Required
    public void setLanguageConverter(final Converter<LanguageModel, LanguageData> languageConverter) {
        this.languageConverter = languageConverter;
    }

    @Required
    public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter) {
        this.addressConverter = addressConverter;
    }
}

/**
 * 
 */
package au.com.target.tgtfacades.user.data;

/**
 * Data representation of AfterpayPaymentInfoModel for front end.
 * 
 * @author mgazal
 * 
 */
public class AfterpayPaymentInfoData {

    public static final String AFTERPAY = "Afterpay";

    public String getPaymentProvider() {
        return AFTERPAY;
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.user.data;

import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;


/**
 * Data representation of IpgPaymentInfoModel for front end.
 * 
 * @author jjayawa1
 * 
 */
public class IpgPaymentInfoData {

    private String token;

    private IpgPaymentTemplateType ipgPaymentTemplateType;

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token
     *            the token to set
     */
    public void setToken(final String token) {
        this.token = token;
    }

    /**
     * @return the ipgPaymentTemplateType
     */
    public IpgPaymentTemplateType getIpgPaymentTemplateType() {
        return ipgPaymentTemplateType;
    }

    /**
     * @param ipgPaymentTemplateType
     *            the ipgPaymentTemplateType to set
     */
    public void setIpgPaymentTemplateType(final IpgPaymentTemplateType ipgPaymentTemplateType) {
        this.ipgPaymentTemplateType = ipgPaymentTemplateType;
    }


}

/**
 * 
 */
package au.com.target.tgtfacades.user.data;

/**
 * @author rmcalave
 * 
 */
public class PayPalHereInfoData {
    public static final String PAYPAL_HERE = "PayPal Here";

    public String getPaymentProvider() {
        return PAYPAL_HERE;
    }
}

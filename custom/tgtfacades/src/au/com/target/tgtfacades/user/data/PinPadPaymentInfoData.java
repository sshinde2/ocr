/**
 * 
 */
package au.com.target.tgtfacades.user.data;

/**
 * Data representation of PinPadPaymentInfoModel for front end.
 * 
 * @author jjayawa1
 * 
 */
public class PinPadPaymentInfoData {

    private String journRoll;
    private String respAscii;
    private String accountType;
    private String maskedCardNumber;
    private String rrn;
    private String cardType;
    private String authCode;

    /**
     * @return the journRoll
     */
    public String getJournRoll() {
        return journRoll;
    }

    /**
     * @return the respAscii
     */
    public String getRespAscii() {
        return respAscii;
    }

    /**
     * @return the accountType
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * @return the maskedCardNumber
     */
    public String getMaskedCardNumber() {
        return maskedCardNumber;
    }

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @return the cardType
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * @return the authCode
     */
    public String getAuthCode() {
        return authCode;
    }

    /**
     * @param journRoll
     *            the journRoll to set
     */
    public void setJournRoll(final String journRoll) {
        this.journRoll = journRoll;
    }

    /**
     * @param respAscii
     *            the respAscii to set
     */
    public void setRespAscii(final String respAscii) {
        this.respAscii = respAscii;
    }

    /**
     * @param accountType
     *            the accountType to set
     */
    public void setAccountType(final String accountType) {
        this.accountType = accountType;
    }

    /**
     * @param maskedCardNumber
     *            the maskedCardNumber to set
     */
    public void setMaskedCardNumber(final String maskedCardNumber) {
        this.maskedCardNumber = maskedCardNumber;
    }

    /**
     * @param rrn
     *            the rrn to set
     */
    public void setRrn(final String rrn) {
        this.rrn = rrn;
    }

    /**
     * @param cardType
     *            the cardType to set
     */
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }

    /**
     * @param authCode
     *            the authCode to set
     */
    public void setAuthCode(final String authCode) {
        this.authCode = authCode;
    }

}

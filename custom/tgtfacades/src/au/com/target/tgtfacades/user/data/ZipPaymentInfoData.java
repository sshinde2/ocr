/**
 * 
 */
package au.com.target.tgtfacades.user.data;

/**
 * Data representation of ZippayPaymentInfoModel for front end.
 * 
 * @author ramsatish
 * 
 */
public class ZipPaymentInfoData {

    public static final String ZIP = "Zip";

    public String getPaymentProvider() {
        return ZIP;
    }

}

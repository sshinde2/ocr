/**
 * 
 */
package au.com.target.tgtfacades.util;

import de.hybris.platform.commercefacades.user.data.AddressData;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.storelocator.TargetGoogleMapTools;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;




/**
 * @author mjanarth
 *
 */
public class TargetPointOfServiceHelper {

    private TargetGoogleMapTools targetGoogleMapTools;
    private SalesApplicationFacade salesApplicationFacade;
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    /**
     * @param locationText
     * @param pointOfServiceData
     */
    public void populateDirectionUrl(final String locationText,
            final TargetPointOfServiceData pointOfServiceData) {

        final String strLocationText = StringUtils.isNotEmpty(locationText) ? locationText : "Current+Location";
        final AddressData addressData = pointOfServiceData.getAddress();
        if (!salesApplicationFacade.isKioskApplication()
                && targetFeatureSwitchFacade
                        .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SHOW_GOOGLE_DIRECTION_URL)) {
            pointOfServiceData
                    .setDirectionUrl(targetGoogleMapTools.getDirectionUrl(
                            strLocationText,
                            StringUtils.join(new String[] { "Target",
                                    pointOfServiceData.getName(),
                                    addressData.getLine1(), addressData.getTown(),
                                    addressData.getPostalCode(), addressData.getCountry().getName() }, '+')));
        }
    }

    @Required
    public void setTargetGoogleMapTools(final TargetGoogleMapTools targetGoogleMapTools) {
        this.targetGoogleMapTools = targetGoogleMapTools;
    }

    @Required
    public void setSalesApplicationFacade(final SalesApplicationFacade salesApplicationFacade) {
        this.salesApplicationFacade = salesApplicationFacade;
    }

    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.util;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtcore.jalo.TargetColourVariantProduct;
import au.com.target.tgtcore.jalo.TargetSizeVariantProduct;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;


/**
 * @author Nandini
 *
 */
public class TargetProductDataHelper {

    private static final Logger LOG = Logger.getLogger(TargetProductDataHelper.class);

    private TargetProductDataHelper() {
        // prevent construction
    }

    /**
     * Determine if a product model is a base product
     * 
     * @param productModel
     * @return true if the product is a base product
     */
    public static boolean isBaseProduct(final ProductModel productModel) {
        //If we have variance it means there is a product variance below this product
        if (productModel.getVariantType() != null
                && productModel.getVariantType().getCode() != null) {
            //Every products are Color variance, so if the variant type is color variant it means we are on a base product
            if (productModel.getVariantType().getCode()
                    .equalsIgnoreCase(TargetColourVariantProduct.class.getSimpleName())) {
                return true;
            }
            //If the variant type is size it means we are already on a color variance
            else if (productModel.getVariantType().getCode()
                    .equalsIgnoreCase(TargetSizeVariantProduct.class.getSimpleName())) {
                return false;
            }
            else {
                LOG.warn("Cannot determine if the product is a base product, unknown variant type.");
            }
        }
        return false;
    }

    /**
     * This method will populate all the sellable variant codes from the lister data
     * 
     * @param productListerData
     * @return List
     */
    public static List<String> populateSellableVariantsFromListerData(final TargetProductListerData productListerData) {
        final List variants = new ArrayList<String>();
        if (CollectionUtils.isEmpty(productListerData.getTargetVariantProductListerData())) {
            return variants;
        }
        for (final TargetVariantProductListerData colourVariant : productListerData
                .getTargetVariantProductListerData()) {
            if (CollectionUtils.isEmpty(colourVariant.getVariants())) {
                variants.add(colourVariant.getCode());
                continue;
            }
            for (final TargetVariantProductListerData sizeVariant : colourVariant.getVariants()) {
                variants.add(sizeVariant.getCode());
            }

        }
        return variants;

    }

    /**
     * retrieves the base product for this product passed in
     * 
     * @param product
     * @return BaseProduct
     */
    public static ProductModel getBaseProduct(final ProductModel product)
    {
        if (product instanceof VariantProductModel)
        {
            return getBaseProduct(((VariantProductModel)product).getBaseProduct());
        }
        return product;
    }
}

/**
 * 
 */
package au.com.target.tgtfacades.voucher;

import au.com.target.tgtfacades.deals.data.TargetVouchersData;


/**
 * @author paul
 *
 */
public interface TargetVoucherFacade {

    /**
     * This method fetches all the vouchers which are applicable for mobile along with the offer headings
     * 
     * @return targetVouchersData
     */
    public TargetVouchersData getAllMobileActivePromotionVouchersWithOfferHeadings();

    /**
     * Checks whether a voucher is available.
     * 
     * @param voucherCode
     * @return true if voucher exist otherwise false
     */
    public boolean doesExist(final String voucherCode);

    /**
     * Checks whether a voucher is expired
     * 
     * @param voucherCode
     * @return true if voucher has expired otherwise false
     */
    public boolean isExpired(final String voucherCode);
}

/**
 *
 */
package au.com.target.tgtfacades.voucher.converter.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.voucher.model.PromotionVoucherModel;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.util.RestrictionStartDateEndDateResult;
import au.com.target.tgtcore.util.RestrictionUtils;
import au.com.target.tgtfacades.offer.converter.TargetMobileOfferHeadingListConverter;
import au.com.target.tgtfacades.voucher.data.TargetPromotionVoucherData;


/**
 * The Class TargetPromotionVoucherPopulator.
 *
 * @author pthoma20
 */
public class TargetPromotionVoucherPopulator implements
        Populator<PromotionVoucherModel, TargetPromotionVoucherData> {

    private TargetMobileOfferHeadingListConverter targetMobileOfferHeadingListConverter;

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.converter.impl.AbstractPopulatingConverter#populate(java.lang.Object, java.lang.Object)
     */
    @Override
    public void populate(final PromotionVoucherModel source, final TargetPromotionVoucherData target) {
        target.setBarcode(source.getBarcode());
        target.setAvailableForMobile(source.getAvailableForMobile());
        target.setMobileLoyaltyVoucher(source.getMobileLoyaltyVoucher());
        target.setEnabledInStore(source.getEnabledInstore());
        target.setEnabledOnline(source.getEnabledOnline());
        target.setFeatured(source.getFeatured());
        target.setCreatedTimeFormatted(new DateTime(source.getCreationtime()));
        target.setModifiedTimeFormatted(new DateTime(source.getModifiedtime()));
        target.setTargetMobileOfferHeadings(targetMobileOfferHeadingListConverter
                .convert(source.getTargetMobileOfferHeadings()));
        final RestrictionStartDateEndDateResult restrictionStartDateEndDateResult = RestrictionUtils
                .findStartAndEndDateBasedOnRestriction(source.getRestrictions());
        if (null != restrictionStartDateEndDateResult) {
            target.setStartTimeFormatted(restrictionStartDateEndDateResult.getStartDate());
            target.setEndTimeFormatted(restrictionStartDateEndDateResult.getEndDate());
        }
    }

    /**
     * @param targetMobileOfferHeadingListConverter
     *            the targetMobileOfferHeadingListConverter to set
     */
    @Required
    public void setTargetMobileOfferHeadingListConverter(
            final TargetMobileOfferHeadingListConverter targetMobileOfferHeadingListConverter) {
        this.targetMobileOfferHeadingListConverter = targetMobileOfferHeadingListConverter;
    }

}
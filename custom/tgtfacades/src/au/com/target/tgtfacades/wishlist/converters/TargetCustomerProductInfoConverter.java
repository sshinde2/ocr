/**
 * 
 */
package au.com.target.tgtfacades.wishlist.converters;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.util.Assert;

import au.com.target.tgtfacades.product.data.CustomerProductInfo;
import au.com.target.tgtwishlist.data.TargetWishListEntryData;


/**
 * @author rsamuel3
 *
 */
public class TargetCustomerProductInfoConverter implements Converter<CustomerProductInfo, TargetWishListEntryData> {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.dto.converter.Converter#convert(java.lang.Object)
     */
    @Override
    public TargetWishListEntryData convert(final CustomerProductInfo customerProductInfo) throws ConversionException {
        return convert(customerProductInfo, new TargetWishListEntryData());
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.dto.converter.Converter#convert(java.lang.Object, java.lang.Object)
     */
    @Override
    public TargetWishListEntryData convert(final CustomerProductInfo customerProductInfo,
            final TargetWishListEntryData wishListEntryData)
            throws ConversionException {
        Assert.notNull(customerProductInfo, "CustomerProductInfo object cannot be null");
        Assert.notNull(wishListEntryData, "WishListEntryData cannot be null");
        wishListEntryData.setBaseProductCode(customerProductInfo.getBaseProductCode());
        wishListEntryData.setSelectedVariantCode(customerProductInfo.getSelectedVariantCode());
        wishListEntryData.setTimeStamp(customerProductInfo.getTimeStamp());
        return wishListEntryData;
    }



}

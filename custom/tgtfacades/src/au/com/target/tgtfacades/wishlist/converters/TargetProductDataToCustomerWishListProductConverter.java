/**
 * 
 */
package au.com.target.tgtfacades.wishlist.converters;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetSelectedVariantData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.util.PriceFormatUtils;
import au.com.target.tgtwishlist.data.TargetCustomerWishListProductsDto;


/**
 * @author rsamuel3
 *
 */
public class TargetProductDataToCustomerWishListProductConverter implements
        Converter<TargetProductListerData, TargetCustomerWishListProductsDto> {

    /**
     * 
     */
    private static final String SLASH = "/";

    private String baseUrl;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.dto.converter.Converter#convert(java.lang.Object)
     */
    @Override
    public TargetCustomerWishListProductsDto convert(final TargetProductListerData productListerData)
            throws ConversionException {
        final TargetCustomerWishListProductsDto requestDto = new TargetCustomerWishListProductsDto();
        return convert(productListerData, requestDto);
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.dto.converter.Converter#convert(java.lang.Object, java.lang.Object)
     */
    @Override
    public TargetCustomerWishListProductsDto convert(final TargetProductListerData productListerData,
            final TargetCustomerWishListProductsDto wishListRequest) throws ConversionException {

        final String baseProductCode = productListerData.getBaseProductCode();
        Assert.notNull(baseProductCode, "Base product code cannot be null");
        wishListRequest.setBaseCode(baseProductCode);
        wishListRequest.setName(StringEscapeUtils.escapeHtml(productListerData.getBaseName()));
        final TargetSelectedVariantData selectedVariantData = productListerData.getSelectedVariantData();
        if (selectedVariantData == null) {
            wishListRequest.setProductURL(baseUrl + productListerData.getUrl());
        }
        else {
            wishListRequest.setProductURL(baseUrl + selectedVariantData.getUrl());
            wishListRequest.setVariantCode(selectedVariantData.getProductCode());
            wishListRequest.setColour(selectedVariantData.getColor());
            wishListRequest.setSize(selectedVariantData.getSize());
        }
        setPrice(productListerData, wishListRequest);
        setImageUrl(productListerData, wishListRequest);

        return wishListRequest;
    }

    /**
     * @param productListerData
     * @param wishListRequest
     */
    protected void setImageUrl(final TargetProductListerData productListerData,
            final TargetCustomerWishListProductsDto wishListRequest) {
        final List<TargetVariantProductListerData> variants = productListerData.getTargetVariantProductListerData();
        if (CollectionUtils.isEmpty(variants)) {
            throw new IllegalArgumentException("Product need to have variants");
        }
        final TargetVariantProductListerData variant = variants.get(0);
        final List<String> imageUrls = variant.getGridImageUrls();
        if (CollectionUtils.isEmpty(imageUrls)) {
            throw new IllegalArgumentException("No grid images available for product");
        }

        wishListRequest.setImageURL(formatImageUrl(imageUrls.get(0)));
    }

    /**
     * @param productListerData
     * @param wishListRequest
     */
    protected void setPrice(final TargetProductListerData productListerData,
            final TargetCustomerWishListProductsDto wishListRequest) {
        String price = null;
        final TargetSelectedVariantData selectedVariantData = productListerData.getSelectedVariantData();
        if (selectedVariantData != null && selectedVariantData.getPrice() != null) {
            price = PriceFormatUtils.formatPrice(selectedVariantData.getPrice().getFormattedValue());
        }
        else if (productListerData.getPriceRange() != null) {
            price = PriceFormatUtils.formatPrice(productListerData.getPriceRange());
        }
        else if (productListerData.getPrice() != null) {
            price = PriceFormatUtils.formatPrice(productListerData.getPrice().getFormattedValue());
        }
        Assert.notNull(price, "Price cannot be null");
        wishListRequest.setPrice(price);
    }

    /**
     * @param imageUrl
     * @return formatted url
     */
    private String formatImageUrl(final String imageUrl) {
        return baseUrl + SLASH + StringUtils.substringAfter(imageUrl, SLASH);
    }

    /**
     * @param baseUrl
     *            the baseUrl to set
     */
    @Required
    public void setBaseUrl(final String baseUrl) {
        this.baseUrl = baseUrl;
    }


}

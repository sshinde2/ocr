/**
 * 
 */
package au.com.target.config;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.HostConfigService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserConstants;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.cart.data.TargetCheckoutCartData;
import au.com.target.tgtfacades.config.CheckoutConfigurationFacadeImpl;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.constants.ThirdPartyConstants;
import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.featureswitch.data.FeatureSwitchData;
import au.com.target.tgtfacades.login.TargetAuthenticationFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.TargetOrderFacade;
import au.com.target.tgtfacades.order.data.EnvironmentData;
import au.com.target.tgtfacades.order.data.SessionData;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.response.data.EnvironmentResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.user.data.TargetCustomerInfoData;
import au.com.target.tgtfacades.util.SalesApplicationPropertyResolver;


/**
 * Unit test for CheckoutConfigurationFacade
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CheckoutConfigurationFacadeImplTest {

    @InjectMocks
    private final CheckoutConfigurationFacadeImpl facade = new CheckoutConfigurationFacadeImpl();

    @Mock
    private HostConfigService hostConfigService;

    @Mock
    private SalesApplicationPropertyResolver salesApplicationPropertyResolver;

    @Mock
    private TargetCustomerFacade targetCustomerFacade;

    @Mock
    private TargetCheckoutFacade mockTargetCheckoutFacade;

    @Mock
    private TargetFeatureSwitchFacade mockTargetFeatureSwitchFacade;

    @Mock
    private SalesApplicationFacade mockSalesApplicationFacade;

    @Mock
    private TargetAuthenticationFacade targetAuthenticationFacade;

    @Mock
    private SessionService sessionService;

    @Mock
    private TargetOrderFacade targetOrderFacade;

    @Mock
    private TargetSharedConfigService targetSharedConfigService;

    @Before
    public void setup() {
        facade.setHostSuffix("dev");
        given(salesApplicationPropertyResolver.getGAValueForSalesApplication()).willReturn(
                "UA-XXXXX-10");
        given(hostConfigService.getProperty("tgtstorefront.host.fe.fqdn", "dev")).willReturn("fqdn");
        given(hostConfigService.getProperty(ThirdPartyConstants.TagManager.CONTAINER, "dev"))
                .willReturn("gtmId");
        given(hostConfigService.getProperty(ThirdPartyConstants.GOptimize.CONTAINER, "dev"))
                .willReturn("goId");

        willReturn(Boolean.TRUE).given(mockTargetCheckoutFacade).isGuestCheckoutAllowed();
        willReturn(Boolean.FALSE).given(mockTargetCheckoutFacade).isGiftCardInCart();

        willReturn(Boolean.FALSE).given(mockSalesApplicationFacade).isSalesChannelMobileApp();
    }

    @Test
    public void getCheckoutEnvConfigsResponseDTONotNull() {
        final Response response = facade.getCheckoutEnvConfigs();
        given(targetSharedConfigService.getConfigByCode(TgtFacadesConstants.ZIP_PAYMENT_THRESHOLD_AMOUNT))
                .willReturn("1000");
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isNotNull();
    }

    @Test
    public void getCheckoutEnvConfigsResponseDTOHasData() {
        final List<String> featureSwitches = new ArrayList<>();
        featureSwitches.add("cool.feature.switch");
        facade.setIncludedFeatureSwitches(featureSwitches);

        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchFacade).isFeatureEnabled("cool.feature.switch");
        given(targetSharedConfigService.getConfigByCode(TgtFacadesConstants.ZIP_PAYMENT_THRESHOLD_AMOUNT))
                .willReturn("1000");

        final Response response = facade.getCheckoutEnvConfigs();

        final EnvironmentResponseData responseData = (EnvironmentResponseData)response.getData();
        final EnvironmentData data = responseData.getEnv();

        assertThat(data).isNotNull();
        assertThat(data.getFqdn()).isEqualTo("fqdn");
        assertThat(data.getGaId()).isEqualTo("UA-XXXXX-10");
        assertThat(data.getGtmId()).isEqualTo("gtmId");
        assertThat(data.getGoId()).isEqualTo("goId");

        assertThat(data.getSwitches()).hasSize(1);
        final FeatureSwitchData featureSwitch = data.getSwitches().get(0);

        assertThat(featureSwitch.getName()).isEqualTo("cool.feature.switch");
        assertThat(featureSwitch.isEnabled()).isTrue();
    }

    @Test
    public void getCheckoutEnvConfigsResponseDTOHasDataAndGANull() {
        given(salesApplicationPropertyResolver.getGAValueForSalesApplication()).willReturn(
                null);
        given(targetSharedConfigService.getConfigByCode(TgtFacadesConstants.ZIP_PAYMENT_THRESHOLD_AMOUNT))
                .willReturn("1000");
        final Response response = facade.getCheckoutEnvConfigs();
        final EnvironmentResponseData responseData = (EnvironmentResponseData)response.getData();
        final EnvironmentData data = responseData.getEnv();
        assertThat(data).isNotNull();
        assertThat(data.getFqdn()).isEqualTo("fqdn");
        assertThat(data.getGaId()).isNull();
        assertThat(data.getGtmId()).isEqualTo("gtmId");
        assertThat(data.getGoId()).isEqualTo("goId");
    }

    @Test
    public void getSessionInformationForGuestUser() {
        final TargetCustomerInfoData customer = new TargetCustomerInfoData();
        customer.setUid(UserConstants.ANONYMOUS_CUSTOMER_UID);
        given(targetCustomerFacade.getCustomerInfo()).willReturn(customer);
        final SessionData sessionData = facade.getSessionInformation(null);
        assertThat(sessionData).isNotNull();
        assertThat(sessionData.isGuest()).isTrue();
        assertThat(sessionData.getCustomer()).isNotNull();
        assertThat(sessionData.getCustomer().getFirstName()).isNull();
        assertThat(sessionData.getCustomer().getLastName()).isNull();
        assertThat(sessionData.getCustomer().getMobileNumber()).isNull();
        assertThat(sessionData.getCustomer().getTitle()).isNull();
        assertThat(sessionData.getCustomer().getFormattedTitle()).isNull();
        assertThat(sessionData.isCheckoutRegisteredAsGuest()).isFalse();

        assertThat(sessionData.isMobileApp()).isFalse();

        final TargetCheckoutCartData cart = sessionData.getCart();
        assertThat(cart).isNotNull();
        assertThat(cart.isIsGuestCheckoutEnabled()).isTrue();
        assertThat(cart.isIsGiftCardInCart()).isFalse();
    }

    @Test
    public void getSessionInformationForRegisteredUserGuestCheckout() {
        final TargetCustomerInfoData customer = new TargetCustomerInfoData();
        customer.setUid("test@test.com.au");
        given(targetCustomerFacade.getCustomerInfo()).willReturn(customer);
        given(sessionService.getAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST)).willReturn(Boolean.TRUE);
        final SessionData sessionData = facade.getSessionInformation(null);
        assertThat(sessionData).isNotNull();
        assertThat(sessionData.isGuest()).isFalse();
        assertThat(sessionData.getCustomer()).isNotNull();
        assertThat(sessionData.getCustomer().getFirstName()).isNull();
        assertThat(sessionData.getCustomer().getLastName()).isNull();
        assertThat(sessionData.getCustomer().getMobileNumber()).isNull();
        assertThat(sessionData.getCustomer().getTitle()).isNull();
        assertThat(sessionData.getCustomer().getFormattedTitle()).isNull();
        assertThat(sessionData.isCheckoutRegisteredAsGuest()).isTrue();

        assertThat(sessionData.isMobileApp()).isFalse();

        final TargetCheckoutCartData cart = sessionData.getCart();
        assertThat(cart).isNotNull();
        assertThat(cart.isIsGuestCheckoutEnabled()).isTrue();
        assertThat(cart.isIsGiftCardInCart()).isFalse();
    }

    @Test
    public void getSessionInformationForGuestUserUsingMobileApp() {
        willReturn(Boolean.TRUE).given(mockSalesApplicationFacade).isSalesChannelMobileApp();

        final TargetCustomerInfoData customer = new TargetCustomerInfoData();
        customer.setUid(UserConstants.ANONYMOUS_CUSTOMER_UID);

        given(targetCustomerFacade.getCustomerInfo()).willReturn(customer);

        final SessionData sessionData = facade.getSessionInformation(null);
        assertThat(sessionData).isNotNull();
        assertThat(sessionData.isGuest()).isTrue();
        assertThat(sessionData.getCustomer()).isNotNull();
        assertThat(sessionData.getCustomer().getFirstName()).isNull();
        assertThat(sessionData.getCustomer().getLastName()).isNull();
        assertThat(sessionData.getCustomer().getMobileNumber()).isNull();
        assertThat(sessionData.getCustomer().getTitle()).isNull();
        assertThat(sessionData.getCustomer().getFormattedTitle()).isNull();
        assertThat(sessionData.isCheckoutRegisteredAsGuest()).isFalse();

        assertThat(sessionData.isMobileApp()).isTrue();

        final TargetCheckoutCartData cart = sessionData.getCart();
        assertThat(cart).isNotNull();
        assertThat(cart.isIsGuestCheckoutEnabled()).isTrue();
        assertThat(cart.isIsGiftCardInCart()).isFalse();
    }

    @Test
    public void getSessionInformationForGuestUserGuestCheckoutNotAllowed() {
        final TargetCustomerInfoData customer = new TargetCustomerInfoData();
        customer.setUid(UserConstants.ANONYMOUS_CUSTOMER_UID);
        given(targetCustomerFacade.getCustomerInfo()).willReturn(customer);

        willReturn(Boolean.FALSE).given(mockTargetCheckoutFacade).isGuestCheckoutAllowed();

        final SessionData sessionData = facade.getSessionInformation(null);

        assertThat(sessionData).isNotNull();
        assertThat(sessionData.isGuest()).isTrue();
        assertThat(sessionData.getCustomer()).isNotNull();
        assertThat(sessionData.getCustomer().getFirstName()).isNull();
        assertThat(sessionData.getCustomer().getLastName()).isNull();
        assertThat(sessionData.getCustomer().getMobileNumber()).isNull();
        assertThat(sessionData.getCustomer().getTitle()).isNull();
        assertThat(sessionData.getCustomer().getFormattedTitle()).isNull();
        assertThat(sessionData.isCheckoutRegisteredAsGuest()).isFalse();

        final TargetCheckoutCartData cart = sessionData.getCart();
        assertThat(cart).isNotNull();
        assertThat(cart.isIsGuestCheckoutEnabled()).isFalse();
        assertThat(cart.isIsGiftCardInCart()).isFalse();
    }

    @Test
    public void getSessionInformationWithCartUserEmail() {
        final TargetCustomerInfoData customer = new TargetCustomerInfoData();
        customer.setUid(UserConstants.ANONYMOUS_CUSTOMER_UID);
        given(targetCustomerFacade.getCustomerInfo()).willReturn(customer);
        given(targetCustomerFacade.getCartUserEmail()).willReturn("test@email.com");
        willReturn(Boolean.FALSE).given(mockTargetCheckoutFacade).isGuestCheckoutAllowed();

        final SessionData sessionData = facade.getSessionInformation(null);

        assertThat(sessionData).isNotNull();
        assertThat(sessionData.isGuest()).isTrue();
        assertThat(sessionData.getCustomer()).isNotNull();
        assertThat(sessionData.getCustomer().getFirstName()).isNull();
        assertThat(sessionData.getCustomer().getLastName()).isNull();
        assertThat(sessionData.getCustomer().getMobileNumber()).isNull();
        assertThat(sessionData.getCustomer().getTitle()).isNull();
        assertThat(sessionData.getCustomer().getFormattedTitle()).isNull();
        assertThat(sessionData.isCheckoutRegisteredAsGuest()).isFalse();

        final TargetCheckoutCartData cart = sessionData.getCart();
        assertThat(cart).isNotNull();
        assertThat(cart.isIsGuestCheckoutEnabled()).isFalse();
        assertThat(cart.isIsGiftCardInCart()).isFalse();
        assertThat(cart.getCheckoutEmail()).isEqualTo("test@email.com");
    }

    @Test
    public void getSessionInformationWithNotRegisteredCartUserEmail() {
        final TargetCustomerInfoData customer = new TargetCustomerInfoData();
        customer.setUid(UserConstants.ANONYMOUS_CUSTOMER_UID);
        given(targetCustomerFacade.getCustomerInfo()).willReturn(customer);
        given(targetCustomerFacade.getCartUserEmail()).willReturn("test@email.com");
        given(targetCustomerFacade.getCustomerForUsername("test@email.com")).willReturn(null);
        willReturn(Boolean.FALSE).given(mockTargetCheckoutFacade).isGuestCheckoutAllowed();

        final SessionData sessionData = facade.getSessionInformation(null);

        assertThat(sessionData).isNotNull();
        assertThat(sessionData.isGuest()).isTrue();
        assertThat(sessionData.getCustomer()).isNotNull();
        assertThat(sessionData.getCustomer().getFirstName()).isNull();
        assertThat(sessionData.getCustomer().getLastName()).isNull();
        assertThat(sessionData.getCustomer().getMobileNumber()).isNull();
        assertThat(sessionData.getCustomer().getTitle()).isNull();
        assertThat(sessionData.getCustomer().getFormattedTitle()).isNull();
        assertThat(sessionData.isCheckoutRegisteredAsGuest()).isFalse();

        final TargetCheckoutCartData cart = sessionData.getCart();
        assertThat(cart).isNotNull();
        assertThat(cart.isIsGuestCheckoutEnabled()).isFalse();
        assertThat(cart.isIsGiftCardInCart()).isFalse();
        assertThat(cart.getCheckoutEmail()).isEqualTo("test@email.com");
        assertThat(cart.isRegistered()).isFalse();
    }

    @Test
    public void getSessionInformationWithRegisteredCartUserEmail() {
        final TargetCustomerInfoData customer = new TargetCustomerInfoData();
        customer.setUid(UserConstants.ANONYMOUS_CUSTOMER_UID);
        given(targetCustomerFacade.getCustomerInfo()).willReturn(customer);
        given(targetCustomerFacade.getCartUserEmail()).willReturn("test@email.com");
        final TargetCustomerModel mockCustomer = Mockito.mock(TargetCustomerModel.class);
        given(targetCustomerFacade.getCustomerForUsername("test@email.com")).willReturn(mockCustomer);
        given(Boolean.valueOf(targetAuthenticationFacade.isAccountLocked(mockCustomer))).willReturn(
                Boolean.valueOf(false));
        willReturn(Boolean.FALSE).given(mockTargetCheckoutFacade).isGuestCheckoutAllowed();

        final SessionData sessionData = facade.getSessionInformation(null);

        assertThat(sessionData).isNotNull();
        assertThat(sessionData.isGuest()).isTrue();
        assertThat(sessionData.getCustomer()).isNotNull();
        assertThat(sessionData.getCustomer().getFirstName()).isNull();
        assertThat(sessionData.getCustomer().getLastName()).isNull();
        assertThat(sessionData.getCustomer().getMobileNumber()).isNull();
        assertThat(sessionData.getCustomer().getTitle()).isNull();
        assertThat(sessionData.getCustomer().getFormattedTitle()).isNull();
        assertThat(sessionData.isCheckoutRegisteredAsGuest()).isFalse();

        final TargetCheckoutCartData cart = sessionData.getCart();
        assertThat(cart).isNotNull();
        assertThat(cart.isIsGuestCheckoutEnabled()).isFalse();
        assertThat(cart.isIsGiftCardInCart()).isFalse();
        assertThat(cart.getCheckoutEmail()).isEqualTo("test@email.com");
        assertThat(cart.isRegistered()).isTrue();
        assertThat(cart.isAccountLocked()).isFalse();
    }

    @Test
    public void getSessionInformationWithRegisteredCartUserEmailWithAccountLocked() {
        final TargetCustomerInfoData customer = new TargetCustomerInfoData();
        customer.setUid(UserConstants.ANONYMOUS_CUSTOMER_UID);
        given(targetCustomerFacade.getCustomerInfo()).willReturn(customer);
        given(targetCustomerFacade.getCartUserEmail()).willReturn("test@email.com");
        final TargetCustomerModel mockCustomer = Mockito.mock(TargetCustomerModel.class);
        given(targetCustomerFacade.getCustomerForUsername("test@email.com")).willReturn(mockCustomer);
        given(Boolean.valueOf(targetAuthenticationFacade.isAccountLocked(mockCustomer))).willReturn(
                Boolean.valueOf(true));
        willReturn(Boolean.FALSE).given(mockTargetCheckoutFacade).isGuestCheckoutAllowed();

        final SessionData sessionData = facade.getSessionInformation(null);

        assertThat(sessionData).isNotNull();
        assertThat(sessionData.isGuest()).isTrue();
        assertThat(sessionData.getCustomer()).isNotNull();
        assertThat(sessionData.getCustomer().getFirstName()).isNull();
        assertThat(sessionData.getCustomer().getLastName()).isNull();
        assertThat(sessionData.getCustomer().getMobileNumber()).isNull();
        assertThat(sessionData.getCustomer().getTitle()).isNull();
        assertThat(sessionData.getCustomer().getFormattedTitle()).isNull();
        assertThat(sessionData.isCheckoutRegisteredAsGuest()).isFalse();

        final TargetCheckoutCartData cart = sessionData.getCart();
        assertThat(cart).isNotNull();
        assertThat(cart.isIsGuestCheckoutEnabled()).isFalse();
        assertThat(cart.isIsGiftCardInCart()).isFalse();
        assertThat(cart.getCheckoutEmail()).isEqualTo("test@email.com");
        assertThat(cart.isRegistered()).isTrue();
        assertThat(cart.isAccountLocked()).isTrue();
    }

    @Test
    public void getSessionInformationForGuestUserGiftCardInCart() {
        final TargetCustomerInfoData customer = new TargetCustomerInfoData();
        customer.setUid(UserConstants.ANONYMOUS_CUSTOMER_UID);
        given(targetCustomerFacade.getCustomerInfo()).willReturn(customer);

        willReturn(Boolean.TRUE).given(mockTargetCheckoutFacade).isGiftCardInCart();

        final SessionData sessionData = facade.getSessionInformation(null);

        assertThat(sessionData).isNotNull();
        assertThat(sessionData.isGuest()).isTrue();
        assertThat(sessionData.getCustomer()).isNotNull();
        assertThat(sessionData.getCustomer().getFirstName()).isNull();
        assertThat(sessionData.getCustomer().getLastName()).isNull();
        assertThat(sessionData.getCustomer().getMobileNumber()).isNull();
        assertThat(sessionData.getCustomer().getTitle()).isNull();
        assertThat(sessionData.getCustomer().getFormattedTitle()).isNull();
        assertThat(sessionData.isCheckoutRegisteredAsGuest()).isFalse();

        assertThat(sessionData.isMobileApp()).isFalse();

        final TargetCheckoutCartData cart = sessionData.getCart();
        assertThat(cart).isNotNull();
        assertThat(cart.isIsGuestCheckoutEnabled()).isTrue();
        assertThat(cart.isIsGiftCardInCart()).isTrue();
    }

    @Test
    public void getSessionInformationForRegisteredUser() {
        final TargetCustomerInfoData customer = new TargetCustomerInfoData();
        customer.setFirstName("firstname");
        customer.setLastName("lastname");
        customer.setTitle("mr");
        customer.setFormattedTitle("Mr");
        customer.setUid("firstname.lastname");
        customer.setMobileNumber("0400 000 000");
        given(targetCustomerFacade.getCustomerInfo()).willReturn(customer);
        final SessionData sessionData = facade.getSessionInformation(null);
        assertThat(sessionData).isNotNull();
        assertThat(sessionData.isGuest()).isFalse();
        assertThat(sessionData.getCustomer()).isNotNull();
        assertThat(sessionData.getCustomer().getFirstName()).isEqualTo("firstname");
        assertThat(sessionData.getCustomer().getLastName()).isEqualTo("lastname");
        assertThat(sessionData.getCustomer().getMobileNumber()).isEqualTo("0400 000 000");
        assertThat(sessionData.getCustomer().getTitle()).isEqualTo("mr");
        assertThat(sessionData.getCustomer().getFormattedTitle()).isEqualTo("Mr");
        assertThat(sessionData.isCheckoutRegisteredAsGuest()).isFalse();

        assertThat(sessionData.isMobileApp()).isFalse();

        final TargetCheckoutCartData cart = sessionData.getCart();
        assertThat(cart).isNotNull();
        assertThat(cart.isIsGuestCheckoutEnabled()).isTrue();
        assertThat(cart.isIsGiftCardInCart()).isFalse();
    }

    @Test
    public void getSessionInformationForRegisteredUserGuestCheckoutNotAllowed() {
        final TargetCustomerInfoData customer = new TargetCustomerInfoData();
        customer.setFirstName("firstname");
        customer.setLastName("lastname");
        customer.setTitle("mr");
        customer.setFormattedTitle("Mr");
        customer.setUid("firstname.lastname");
        customer.setMobileNumber("0400 000 000");
        given(targetCustomerFacade.getCustomerInfo()).willReturn(customer);

        willReturn(Boolean.FALSE).given(mockTargetCheckoutFacade).isGuestCheckoutAllowed();

        final SessionData sessionData = facade.getSessionInformation(null);

        assertThat(sessionData).isNotNull();
        assertThat(sessionData.isGuest()).isFalse();
        assertThat(sessionData.getCustomer()).isNotNull();
        assertThat(sessionData.getCustomer().getFirstName()).isEqualTo("firstname");
        assertThat(sessionData.getCustomer().getLastName()).isEqualTo("lastname");
        assertThat(sessionData.getCustomer().getMobileNumber()).isEqualTo("0400 000 000");
        assertThat(sessionData.getCustomer().getTitle()).isEqualTo("mr");
        assertThat(sessionData.getCustomer().getFormattedTitle()).isEqualTo("Mr");
        assertThat(sessionData.isCheckoutRegisteredAsGuest()).isFalse();

        assertThat(sessionData.isMobileApp()).isFalse();

        final TargetCheckoutCartData cart = sessionData.getCart();
        assertThat(cart).isNotNull();
        assertThat(cart.isIsGuestCheckoutEnabled()).isFalse();
        assertThat(cart.isIsGiftCardInCart()).isFalse();
    }

    @Test
    public void getSessionInformationForRegisteredUserGiftCardInCart() {
        final TargetCustomerInfoData customer = new TargetCustomerInfoData();
        customer.setFirstName("firstname");
        customer.setLastName("lastname");
        customer.setTitle("mr");
        customer.setFormattedTitle("Mr");
        customer.setUid("firstname.lastname");
        customer.setMobileNumber("0400 000 000");
        given(targetCustomerFacade.getCustomerInfo()).willReturn(customer);

        willReturn(Boolean.TRUE).given(mockTargetCheckoutFacade).isGiftCardInCart();

        final SessionData sessionData = facade.getSessionInformation(null);

        assertThat(sessionData).isNotNull();
        assertThat(sessionData.isGuest()).isFalse();
        assertThat(sessionData.getCustomer()).isNotNull();
        assertThat(sessionData.getCustomer().getFirstName()).isEqualTo("firstname");
        assertThat(sessionData.getCustomer().getLastName()).isEqualTo("lastname");
        assertThat(sessionData.getCustomer().getMobileNumber()).isEqualTo("0400 000 000");
        assertThat(sessionData.getCustomer().getTitle()).isEqualTo("mr");
        assertThat(sessionData.getCustomer().getFormattedTitle()).isEqualTo("Mr");
        assertThat(sessionData.isCheckoutRegisteredAsGuest()).isFalse();

        assertThat(sessionData.isMobileApp()).isFalse();

        final TargetCheckoutCartData cart = sessionData.getCart();
        assertThat(cart).isNotNull();
        assertThat(cart.isIsGuestCheckoutEnabled()).isTrue();
        assertThat(cart.isIsGiftCardInCart()).isTrue();
    }

    @Test
    public void getSessionInformationAfterPlaceOrder() {
        final TargetCustomerInfoData customerInfo = mock(TargetCustomerInfoData.class);
        given(customerInfo.getUid()).willReturn(UserConstants.ANONYMOUS_CUSTOMER_UID);
        given(targetCustomerFacade.getCustomerInfo()).willReturn(customerInfo);
        final TargetOrderData orderData = mock(TargetOrderData.class);
        given(targetOrderFacade.getOrderDetailsForCode("1234")).willReturn(orderData);
        given(orderData.getEmail()).willReturn("customer@company.com");
        final TargetCustomerModel customer = mock(TargetCustomerModel.class);
        given(targetCustomerFacade.getCustomerForUsername("customer@company.com")).willReturn(customer);
        given(sessionService.getAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST)).willReturn(Boolean.TRUE);
        final SessionData sessionData = facade.getSessionInformation("1234");

        assertThat(sessionData).isNotNull();
        assertThat(sessionData.isGuest()).isTrue();
        assertThat(sessionData.getCustomer()).isNotNull();
        assertThat(sessionData.isCheckoutRegisteredAsGuest()).isTrue();

        assertThat(sessionData.isMobileApp()).isFalse();

        final TargetCheckoutCartData cart = sessionData.getCart();
        assertThat(cart).isNotNull();
        assertThat(cart.isIsGuestCheckoutEnabled()).isTrue();
        assertThat(cart.isIsGiftCardInCart()).isFalse();
        assertThat(cart.getCheckoutEmail()).isNull();
        assertThat(cart.isRegistered()).isTrue();
    }

    /**
     * This method is to verify if the cart does not contain combination of gift cards and any other items(except
     * pre-order).
     */
    @Test
    public void testGetSessionInformationIfCartDoesNotContainGiftCardMixedItems() {
        final TargetCustomerInfoData targetCustomerInfoData = mock(TargetCustomerInfoData.class);
        given(targetCustomerInfoData.getUid()).willReturn(UserConstants.ANONYMOUS_CUSTOMER_UID);
        given(targetCustomerFacade.getCustomerInfo()).willReturn(targetCustomerInfoData);
        final TargetCustomerModel targetCustomerModel = mock(TargetCustomerModel.class);
        given(targetCustomerFacade.getCustomerForUsername("anonymous@examplemaildomain.com.au"))
                .willReturn(targetCustomerModel);
        willReturn(Boolean.FALSE).given(mockTargetCheckoutFacade).isGiftCardMixedBasket();
        final SessionData sessionData = facade.getSessionInformation(null);
        assertThat(sessionData.getCart().isGiftCardMixedBasket()).isFalse();
    }

    /**
     * This method is to verify if the cart contain combination of gift cards and any other items(except pre-order).
     */
    @Test
    public void testGetSessionInformationIfCartContainGiftCardMixedItems() {
        final TargetCustomerInfoData targetCustomerInfoData = mock(TargetCustomerInfoData.class);
        given(targetCustomerInfoData.getUid()).willReturn(UserConstants.ANONYMOUS_CUSTOMER_UID);
        given(targetCustomerFacade.getCustomerInfo()).willReturn(targetCustomerInfoData);
        final TargetCustomerModel targetCustomerModel = mock(TargetCustomerModel.class);
        given(targetCustomerFacade.getCustomerForUsername("anonymous@examplemaildomain.com.au"))
                .willReturn(targetCustomerModel);
        willReturn(Boolean.TRUE).given(mockTargetCheckoutFacade).isGiftCardMixedBasket();
        final SessionData sessionData = facade.getSessionInformation(null);
        assertThat(sessionData.getCart().isGiftCardMixedBasket()).isTrue();
    }

}

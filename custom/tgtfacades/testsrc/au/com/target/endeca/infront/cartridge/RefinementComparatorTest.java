/**
 * 
 */
package au.com.target.endeca.infront.cartridge;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.endeca.infront.cartridge.RefinementMenuConfig;
import com.endeca.infront.cartridge.model.Refinement;

import au.com.target.endeca.infront.constants.EndecaConstants;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class RefinementComparatorTest {

    @Mock
    private RefinementMenuConfig cartridgeConfig;

    @Test
    public void testDefaultSort() {
        final List<Refinement> refinements = new ArrayList<>();
        refinements.add(createRefinement("b", null, null));
        refinements.add(createRefinement("c", null, null));
        refinements.add(createRefinement("a", null, null));
        given(cartridgeConfig.getSort()).willReturn(EndecaConstants.RefinementMenuConfig.SORT_DEFAULT);
        Collections.sort(refinements, new RefinementComparator(cartridgeConfig));
        assertThat(refinements).onProperty("label").containsExactly("a", "b", "c");
    }

    @Test
    public void testDefaultSortWithBoostedAndBuriedRefinements() {
        final List<Refinement> refinements = new ArrayList<>();
        refinements.add(createRefinement("a", null, "-1"));
        refinements.add(createRefinement("b", null, null));
        refinements.add(createRefinement("c", null, null));
        refinements.add(createRefinement("d", null, "1"));
        given(cartridgeConfig.getSort()).willReturn(EndecaConstants.RefinementMenuConfig.SORT_DEFAULT);
        Collections.sort(refinements, new RefinementComparator(cartridgeConfig));
        assertThat(refinements).onProperty("label").containsExactly("d", "b", "c", "a");
    }

    @Test
    public void testStaticSort() {
        final List<Refinement> refinements = new ArrayList<>();
        refinements.add(createRefinement("b", null, null));
        refinements.add(createRefinement("c", null, null));
        refinements.add(createRefinement("a", null, null));
        given(cartridgeConfig.getSort()).willReturn(EndecaConstants.RefinementMenuConfig.SORT_ALPHABETICAL);
        Collections.sort(refinements, new RefinementComparator(cartridgeConfig));
        assertThat(refinements).onProperty("label").containsExactly("a", "b", "c");
    }

    @Test
    public void testStaticSortWithBoostedAndBuriedRefinements() {
        final List<Refinement> refinements = new ArrayList<>();
        refinements.add(createRefinement("a", null, "-1"));
        refinements.add(createRefinement("b", null, null));
        refinements.add(createRefinement("c", null, null));
        refinements.add(createRefinement("d", null, "1"));
        given(cartridgeConfig.getSort()).willReturn(EndecaConstants.RefinementMenuConfig.SORT_ALPHABETICAL);
        Collections.sort(refinements, new RefinementComparator(cartridgeConfig));
        assertThat(refinements).onProperty("label").containsExactly("d", "b", "c", "a");
    }

    @Test
    public void testDynRankSort() {
        final List<Refinement> refinements = new ArrayList<>();
        refinements.add(createRefinement("a", Integer.valueOf(1), null));
        refinements.add(createRefinement("b", Integer.valueOf(2), null));
        refinements.add(createRefinement("c", Integer.valueOf(3), null));
        given(cartridgeConfig.getSort()).willReturn(EndecaConstants.RefinementMenuConfig.SORT_FREQUENCY);
        Collections.sort(refinements, new RefinementComparator(cartridgeConfig));
        assertThat(refinements).onProperty("label").containsExactly("c", "b", "a");
    }

    @Test
    public void testDynRankSortWithBoostedAndBuriedRefinements() {
        final List<Refinement> refinements = new ArrayList<>();
        refinements.add(createRefinement("a", Integer.valueOf(1), "1"));
        refinements.add(createRefinement("b", Integer.valueOf(2), null));
        refinements.add(createRefinement("c", Integer.valueOf(3), null));
        refinements.add(createRefinement("d", Integer.valueOf(4), "-1"));
        given(cartridgeConfig.getSort()).willReturn(EndecaConstants.RefinementMenuConfig.SORT_FREQUENCY);
        Collections.sort(refinements, new RefinementComparator(cartridgeConfig));
        assertThat(refinements).onProperty("label").containsExactly("a", "c", "b", "d");
    }

    private Refinement createRefinement(final String label, final Integer count, final String buryOrBoostIndex) {
        final Refinement refinement = Mockito.mock(Refinement.class);
        given(refinement.getLabel()).willReturn(label);
        given(refinement.getCount()).willReturn(count);
        final Map<String, String> properties = new HashMap<>();
        if (StringUtils.isNotBlank(buryOrBoostIndex)) {
            properties.put("DGraph.Strata", buryOrBoostIndex);
        }
        given(refinement.getProperties()).willReturn(properties);
        return refinement;
    }

}

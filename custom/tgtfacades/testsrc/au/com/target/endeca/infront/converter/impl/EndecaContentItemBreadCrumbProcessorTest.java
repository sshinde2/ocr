/**
 * 
 */
package au.com.target.endeca.infront.converter.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.endeca.infront.assembler.BasicContentItem;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.model.RefinementBreadcrumb;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearch;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EndecaContentItemBreadCrumbProcessorTest {

    private final EndecaContentItemBreadCrumbProcessor contentItemBreadCrumbProcessor = new EndecaContentItemBreadCrumbProcessor();

    @Test
    public void testSkipMultiSelectBreadCrumbs() {
        final ContentItem contentItem = new BasicContentItem();
        final List<RefinementBreadcrumb> breadcrumbs = new ArrayList<>();
        final RefinementBreadcrumb breadcrumb1 = mock(RefinementBreadcrumb.class);
        given(Boolean.valueOf(breadcrumb1.isMultiSelect())).willReturn(Boolean.TRUE);
        given(breadcrumb1.getLabel()).willReturn("breadcrumb1");
        final RefinementBreadcrumb breadcrumb2 = mock(RefinementBreadcrumb.class);
        given(Boolean.valueOf(breadcrumb2.isMultiSelect())).willReturn(Boolean.FALSE);
        given(breadcrumb2.getLabel()).willReturn("breadcrumb2");
        breadcrumbs.add(breadcrumb1);
        breadcrumbs.add(breadcrumb2);
        contentItem.put(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENTCRUMBS, breadcrumbs);
        final EndecaSearch endecaSearch = new EndecaSearch();

        contentItemBreadCrumbProcessor.process(contentItem, endecaSearch);
        assertThat(endecaSearch.getBreadCrumb()).isNotEmpty();
        assertThat(endecaSearch.getBreadCrumb().size()).isEqualTo(1);
        assertThat(endecaSearch.getBreadCrumb()).onProperty("element").containsExactly("breadcrumb2");
    }
}

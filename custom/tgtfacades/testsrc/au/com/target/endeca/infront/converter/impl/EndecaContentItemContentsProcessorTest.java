/**
 * 
 */
package au.com.target.endeca.infront.converter.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearch;

import com.endeca.infront.assembler.BasicContentItem;
import com.endeca.infront.assembler.ContentItem;


/**
 * Test cases for testing EndecaContentItemContentsProcessor
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EndecaContentItemContentsProcessorTest {

    public static final String CONTENT_TYPE = "@type";

    @Mock
    private EndecaSearch endecaSearch;

    @InjectMocks
    private final EndecaContentItemContentsProcessor endecaContentItemContentsProcessor = new EndecaContentItemContentsProcessor();


    @Test
    public void testConvertContentItemWithANullList() {
        final ContentItem contentItem = new BasicContentItem();
        final List<ContentItem> secondaryItemContentsList = new ArrayList<>();
        final ContentItem secondaryContentItem = new BasicContentItem();
        secondaryContentItem.put(CONTENT_TYPE, EndecaConstants.EndecaParserNodes.ENDECA_CONTENT_SLOT_SECONDARY);
        secondaryContentItem.put(EndecaConstants.EndecaParserNodes.ENDECA_CONTENT_SLOT_SECONDARY, null);

        secondaryItemContentsList.add(secondaryContentItem);

        final List<ContentItem> contentItemContentsList = new ArrayList<>();
        final ContentItem contentItemContent = new BasicContentItem();
        contentItemContent.put(EndecaConstants.EndecaParserNodes.ENDECA_SECONDARY_CONTENT, secondaryItemContentsList);
        contentItemContentsList.add(contentItemContent);
        contentItem.put(EndecaConstants.EndecaParserNodes.ENDECA_CONTENTS, contentItemContentsList);

        Assert.assertNotNull(endecaContentItemContentsProcessor.convertContentItem(contentItem, endecaSearch));

    }
}

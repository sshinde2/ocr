/**
 * 
 */
package au.com.target.endeca.infront.converter.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.endeca.infront.cartridge.model.Refinement;

import au.com.target.endeca.infront.cartridge.TargetRefinementMenu;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaDimension;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EndecaContentItemGuidedNavProcessorTest {

    private final EndecaContentItemGuidedNavProcessor contentItemGuidedNavProcessor = new EndecaContentItemGuidedNavProcessor();

    @Test
    public void testPopulateSelectedInEndecaDimension() {
        final TargetRefinementMenu refinementMenu = mock(TargetRefinementMenu.class);
        given(Boolean.valueOf(refinementMenu.containsKey(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENTS)))
                .willReturn(Boolean.TRUE);
        final List<Refinement> refinements = new ArrayList<>();
        refinements.add(createRefinement(false, "1"));
        refinements.add(createRefinement(true, "2"));
        given(refinementMenu.get(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENTS)).willReturn(refinements);
        willReturn(Boolean.TRUE).given(refinementMenu).isSeoFollowEnabled();
        final EndecaDimension endecaDimension = contentItemGuidedNavProcessor.populateEndecaDimension(refinementMenu);
        assertThat(endecaDimension).isNotNull();
        assertThat(endecaDimension.getRefinement()).isNotEmpty();
        assertThat(endecaDimension.getRefinement()).onProperty("selected").containsExactly(Boolean.FALSE, Boolean.TRUE);
        assertThat(endecaDimension.getRefinement()).onProperty("code").containsExactly("1", "2");
        assertThat(endecaDimension.isSelected()).isTrue();
        assertThat(endecaDimension.isSeoFollowEnabled()).isTrue();
    }

    private Refinement createRefinement(final boolean selected, final String code) {
        final Refinement refinement = mock(Refinement.class);
        final Map<String, String> properties = new HashMap<>();
        if (selected) {
            properties.put(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_SELECTED, StringUtils.EMPTY);
        }
        properties.put(EndecaConstants.EndecaParserNodes.ENDECA_REFINEMENT_DIMID, code);
        given(refinement.getProperties()).willReturn(properties);
        return refinement;
    }
}

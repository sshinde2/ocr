/**
 * 
 */
package au.com.target.endeca.infront.converter.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;
import de.hybris.platform.commerceservices.url.UrlResolver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaDimension;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.data.TargetFacetData;
import au.com.target.endeca.infront.transform.TargetEndecaURLHelper;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.product.data.BrandData;



/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EndecaToFacetsConverterImplTest {

    @Mock
    private UrlResolver<CategoryData> categoryDataUrlResolver;
    @Mock
    private UrlResolver<BrandData> brandDataUrlResolver;
    @Mock
    private TargetEndecaURLHelper targetEndecaURLHelper;
    @Mock
    private CommerceCategoryService commerceCategoryService;
    @Mock
    private UrlResolver<TargetDealCategoryModel> dealCategoryModelUrlResolver;
    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;
    @InjectMocks
    private final EndecaToFacetsConverterImpl<?> endecaToFacetsConverter = new EndecaToFacetsConverterImpl<>();

    private final String brandUrl = "testBrandUrl";

    private final String categoryUrl = "testCategoryUrl";

    @Before
    public void setup() {
        given(brandDataUrlResolver.resolve(any(BrandData.class))).willReturn(brandUrl);
        given(categoryDataUrlResolver.resolve(any(CategoryData.class))).willReturn(categoryUrl);
        endecaToFacetsConverter.setIsolatedFacets(Arrays.asList("availableOnline", "preOrder"));
    }

    @Test
    public void testSelectedInPopulateFacetValueData() {
        final EndecaDimension endecaDimension = mock(EndecaDimension.class);
        final EndecaSearchStateData searchStateData = mock(EndecaSearchStateData.class);
        given(Boolean.valueOf(endecaDimension.isSelected())).willReturn(Boolean.TRUE);
        final FacetValueData<EndecaSearchStateData> facetValueData = endecaToFacetsConverter
                .populateFacetValueData(null, searchStateData, null, endecaDimension, null);
        assertThat(facetValueData).isNotNull();
        assertThat(facetValueData.isSelected()).isTrue();
    }

    @Test
    public void testNotSelectedInPopulateFacetValueData() {
        final EndecaDimension endecaDimension = mock(EndecaDimension.class);
        final EndecaSearchStateData searchStateData = mock(EndecaSearchStateData.class);
        given(Boolean.valueOf(endecaDimension.isSelected())).willReturn(Boolean.FALSE);
        final FacetValueData<EndecaSearchStateData> facetValueData = endecaToFacetsConverter
                .populateFacetValueData(null, searchStateData, null, endecaDimension, null);
        assertThat(facetValueData).isNotNull();
        assertThat(facetValueData.isSelected()).isFalse();
    }

    @Test
    public void testIsolatedInPopulateFacetValueData() {
        final EndecaDimension endecaDimension = mock(EndecaDimension.class);
        final EndecaSearchStateData searchStateData = mock(EndecaSearchStateData.class);
        final EndecaSearch endecaSearch = new EndecaSearch();
        final String categoryFacet = "category";
        final String availableOnlineFacet = "availableOnline";
        final String preOrderFacet = "preOrder";
        endecaSearch.setDimension(buildEndecaDimensions(categoryFacet, availableOnlineFacet, preOrderFacet));
        given(Boolean.valueOf(endecaDimension.isSelected())).willReturn(Boolean.FALSE);
        final List<FacetData<EndecaSearchStateData>> facetData = endecaToFacetsConverter
                .convertToFacetData(endecaSearch, searchStateData, "W12345", null);
        assertThat(facetData).isNotNull().isNotEmpty().hasSize(3);
        for (final FacetData<EndecaSearchStateData> facet : facetData) {
            final TargetFacetData<EndecaSearchStateData> tgtfacet = (TargetFacetData)facet;
            if (tgtfacet.getName().equals(categoryFacet)) {
                assertThat(tgtfacet.isIsolated()).isFalse();
            }
            else if (tgtfacet.getName().equals(availableOnlineFacet)) {
                assertThat(tgtfacet.isIsolated()).isTrue();
            }
            else if (tgtfacet.getName().equals(preOrderFacet)) {
                assertThat(tgtfacet.isIsolated()).isTrue();
            }
            assertThat(tgtfacet.isSeoFollowEnabled()).isTrue();
        }
    }

    /**
     * @param dimensionNames
     * @return List<EndecaDimension>
     */
    private List<EndecaDimension> buildEndecaDimensions(final String... dimensionNames) {
        final List<EndecaDimension> dimensions = new ArrayList<EndecaDimension>();
        for (final String dimensionName : dimensionNames) {
            final EndecaDimension dimension = new EndecaDimension();
            dimension.setCode(dimensionName);
            dimension.setDimensionName(dimensionName);
            dimension.setName(dimensionName);
            dimension.setSeoFollowEnabled(true);
            dimension.setRefinement(new ArrayList<EndecaDimension>());
            dimensions.add(dimension);
        }
        return dimensions;
    }

    @Test
    public void testCreateFacetData() {
        final TargetFacetData<EndecaSearchStateData> facetData = endecaToFacetsConverter
                .createFacetData("dimensionName", "name");
        assertThat(facetData).isNotNull();
        assertThat(facetData.getCode()).isEqualTo("dimensionName");
        assertThat(facetData.getName()).isEqualTo("name");
        assertThat(facetData.isCategory()).isFalse();
    }

    @Test
    public void testCreateFacetDataForCategory() {
        final TargetFacetData<EndecaSearchStateData> facetData = endecaToFacetsConverter
                .createFacetData("category", "name");
        assertThat(facetData).isNotNull();
        assertThat(facetData.getCode()).isEqualTo("category");
        assertThat(facetData.getName()).isEqualTo("name");
        assertThat(facetData.isCategory()).isTrue();
    }

    public void testUrlInPopulateFacetValueDataWithBrand() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final BrandData brand = new BrandData();
        brand.setCode("testBrandCode");
        searchStateData.setBrand(brand);
        final String currentQueryParam = "testCurrentQueryParam";
        searchStateData.setCurrentQueryParam(currentQueryParam);
        final String dimensionType = EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND;
        final EndecaDimension dimension = new EndecaDimension();
        dimension.setSelected(false);
        final String navigationState = "testNavigationState";
        dimension.setNavigationState(navigationState);
        final String categoryCode = "testCategoryCode";
        final String dimensionName = "testDimensionName";
        given(targetEndecaURLHelper.getValidEndecaToTargetNavigationState(navigationState)).willReturn(navigationState);
        final FacetValueData<EndecaSearchStateData> facetValueData = endecaToFacetsConverter
                .populateFacetValueData(dimensionType, searchStateData, categoryCode, dimension, dimensionName);
        assertThat(facetValueData).isNotNull();
        assertThat(facetValueData.getName()).isEqualTo(dimensionName);
        assertThat(facetValueData.isSelected()).isFalse();
        assertThat(facetValueData.getQuery()).isNotNull();
        assertThat(facetValueData.getQuery().getNavigationState().get(0)).isEqualTo(brandUrl + navigationState);
        assertThat(facetValueData.getQuery().getUrl()).isEqualTo(brandUrl + navigationState);
        assertThat(facetValueData.getQuery().getWsUrl()).isEqualTo(navigationState + currentQueryParam);
    }

    @Test
    public void testUrlInPopulateFacetValueDataWithCategoryWithDimensioncode() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final String currentQueryParam = "testCurrentQueryParam";
        searchStateData.setCurrentQueryParam(currentQueryParam);
        final String dimensionType = EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY;
        final EndecaDimension dimension = new EndecaDimension();
        dimension.setSelected(false);
        final String dimensionCode = "testCode";
        dimension.setCode(dimensionCode);
        final String navigationState = "testNavigationState";
        dimension.setNavigationState(navigationState);
        final String categoryCode = "testCategoryCode";
        final String dimensionName = "testDimensionName";
        given(targetEndecaURLHelper.getValidEndecaToTargetNavigationState(navigationState)).willReturn(navigationState);
        final FacetValueData<EndecaSearchStateData> facetValueData = endecaToFacetsConverter
                .populateFacetValueData(dimensionType, searchStateData, categoryCode, dimension, dimensionName);
        assertThat(facetValueData).isNotNull();
        assertThat(facetValueData.getName()).isEqualTo(dimensionName);
        assertThat(facetValueData.isSelected()).isFalse();
        assertThat(facetValueData.getQuery()).isNotNull();
        assertThat(facetValueData.getQuery().getNavigationState().get(0)).isEqualTo(categoryUrl + navigationState);
        assertThat(facetValueData.getQuery().getUrl()).isEqualTo(categoryUrl + navigationState);
        assertThat(facetValueData.getQuery().getWsUrl()).isEqualTo(StringUtils.EMPTY);
    }

    @Test
    public void testUrlInPopulateFacetValueDataWithCategoryWithoutDimensionCode() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final String currentQueryParam = "testCurrentQueryParam";
        searchStateData.setCurrentQueryParam(currentQueryParam);
        final String dimensionType = EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY;
        final EndecaDimension dimension = new EndecaDimension();
        dimension.setSelected(false);
        final String navigationState = "testNavigationState";
        dimension.setNavigationState(navigationState);
        final String categoryCode = "testCategoryCode";
        final String dimensionName = "testDimensionName";
        given(targetEndecaURLHelper.getValidEndecaToTargetNavigationState(navigationState)).willReturn(navigationState);
        final FacetValueData<EndecaSearchStateData> facetValueData = endecaToFacetsConverter
                .populateFacetValueData(dimensionType, searchStateData, categoryCode, dimension, dimensionName);
        assertThat(facetValueData).isNotNull();
        assertThat(facetValueData.getName()).isEqualTo(dimensionName);
        assertThat(facetValueData.isSelected()).isFalse();
        assertThat(facetValueData.getQuery()).isNotNull();
        assertThat(facetValueData.getQuery().getNavigationState().get(0)).isEqualTo(categoryUrl + navigationState);
        assertThat(facetValueData.getQuery().getUrl()).isEqualTo(categoryUrl + navigationState);
        assertThat(facetValueData.getQuery().getWsUrl()).isEqualTo(navigationState + currentQueryParam);
    }

    @Test
    public void testUrlInPopulateFacetValueDataWithSearch() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final String currentQueryParam = "testCurrentQueryParam";
        searchStateData.setCurrentQueryParam(currentQueryParam);
        final String dimensionType = EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR;
        final EndecaDimension dimension = new EndecaDimension();
        dimension.setSelected(false);
        final String navigationState = "testNavigationState";
        dimension.setNavigationState(navigationState);
        final String dimensionName = "testDimensionName";
        given(targetEndecaURLHelper.getValidEndecaToTargetNavigationState(navigationState)).willReturn(navigationState);
        final FacetValueData<EndecaSearchStateData> facetValueData = endecaToFacetsConverter
                .populateFacetValueData(dimensionType, searchStateData, null, dimension, dimensionName);
        assertThat(facetValueData).isNotNull();
        assertThat(facetValueData.getName()).isEqualTo(dimensionName);
        assertThat(facetValueData.isSelected()).isFalse();
        assertThat(facetValueData.getQuery()).isNotNull();
        assertThat(facetValueData.getQuery().getNavigationState().get(0)).isEqualTo(navigationState);
        assertThat(facetValueData.getQuery().getUrl()).isEqualTo(navigationState);
        assertThat(facetValueData.getQuery().getWsUrl()).isEqualTo(navigationState + currentQueryParam);

    }
}

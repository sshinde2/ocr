/**
 * 
 */
package au.com.target.endeca.infront.converter.impl;


import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;
import de.hybris.platform.commerceservices.url.UrlResolver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.converter.EndecaContentItemConverter;
import au.com.target.endeca.infront.data.EndecaBreadCrumb;
import au.com.target.endeca.infront.data.EndecaDimension;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.data.TargetFacetData;
import au.com.target.endeca.infront.parser.EndecaSearchRecordsBuilderTestHelper;
import au.com.target.endeca.infront.parser.impl.EndecaDataParserImpl;
import au.com.target.endeca.infront.transform.TargetEndecaURLHelper;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.product.data.BrandData;


/**
 * @author rsamuel3
 * 
 */
@UnitTest
public class EndecaToFacetsConverterTest {

    private EndecaDataParserImpl endecaDataParser;

    @InjectMocks
    private final EndecaToFacetsConverterImpl<EndecaSearchStateData> endecaToFacetsConverter = new EndecaToFacetsConverterImpl();

    @Mock
    private UrlResolver<CategoryData> categoryDataUrlResolver;

    @Mock
    private UrlResolver<BrandData> brandDataUrlResolver;

    @Mock
    private TargetEndecaURLHelper targetEndecaUrlHelper;

    @Mock
    private CommerceCategoryService commerceCategoryService;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Mock
    private BrandData brand;

    private EndecaDimension dealDimension;

    @Mock
    private UrlResolver<TargetDealCategoryModel> dealCategoryModelUrlResolver;

    private final CategoryModel categoryModel = new CategoryModel();


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        final EndecaContentItemBreadCrumbProcessor endecaContentItemBreadCrumbProcessor = new EndecaContentItemBreadCrumbProcessor();
        final EndecaContentItemGuidedNavProcessor endecaContentItemGuidedNavProcessor = new EndecaContentItemGuidedNavProcessor();
        final EndecaContentItemResultsListProcessor endecaContentItemResultsListProcessor = new EndecaContentItemResultsListProcessor();
        final EndecaContentItemContentsProcessor endecaContentItemContentsProcessor = new EndecaContentItemContentsProcessor();
        final EndecaSpellSuggestionsConverterImpl endecaSpellSuggestionsConverter = new EndecaSpellSuggestionsConverterImpl();
        final Map<String, EndecaContentItemConverter> endecaContentItemConverters = new HashMap<String, EndecaContentItemConverter>();
        final EndecaRedirectUrlConverterImpl endecaRedirectUrlConverter = new EndecaRedirectUrlConverterImpl();
        endecaDataParser = new EndecaDataParserImpl();
        endecaContentItemContentsProcessor
                .setEndecaContentItemBreadCrumbProcessor(endecaContentItemBreadCrumbProcessor);
        endecaContentItemContentsProcessor.setEndecaContentItemGuidedNavProcessor(endecaContentItemGuidedNavProcessor);
        endecaContentItemContentsProcessor
                .setEndecaContentItemResultsListProcessor(endecaContentItemResultsListProcessor);
        endecaContentItemConverters.put("contents", endecaContentItemContentsProcessor);
        endecaContentItemConverters.put("endeca:redirect", endecaRedirectUrlConverter);
        endecaContentItemConverters.put("endeca:assemblerRequestInformation", endecaSpellSuggestionsConverter);
        endecaDataParser.setEndecaContentItemConverters(endecaContentItemConverters);

        categoryDataUrlResolver = mock(UrlResolver.class);
        brandDataUrlResolver = mock(UrlResolver.class);
        targetEndecaUrlHelper = mock(TargetEndecaURLHelper.class);
        brand = mock(BrandData.class);
        endecaToFacetsConverter.setBrandDataUrlResolver(brandDataUrlResolver);
        endecaToFacetsConverter.setCategoryDataUrlResolver(categoryDataUrlResolver);
        endecaToFacetsConverter.setTargetEndecaURLHelper(targetEndecaUrlHelper);
        endecaToFacetsConverter.setIsolatedFacets(Arrays.asList("availableOnline"));
        dealDimension = new EndecaDimension();
        dealDimension.setDimensionName("dealFacet");

        given(commerceCategoryService.getCategoryForCode(Mockito.anyString())).willReturn(categoryModel);
        when(Boolean.valueOf(targetFeatureSwitchFacade.isSizeOrderingFacetsEnabled()))
                .thenReturn(Boolean.FALSE);
    }

    @Test
    public void testDummy() {
        return;
    }

    @Test
    public void testFacetsConverterWithDimensionsEmpty() {
        final EndecaSearch search = new EndecaSearch();
        final List<FacetData<EndecaSearchStateData>> facets = endecaToFacetsConverter.convertToFacetData(search,
                new EndecaSearchStateData(), "Shoes", null);
        assertThat(facets).isNotNull().isEmpty();
    }

    @Test
    public void testFacetsConverterWithBrandNotNullWithPName() throws TransformerConfigurationException, IOException,
            TransformerFactoryConfigurationError, JAXBException, TransformerException {
        given(brandDataUrlResolver.resolve(Mockito.any(BrandData.class)))
                .willReturn("/b/target");
        given(brand.getCode()).willReturn("123456");
        final EndecaSearch search = EndecaSearchRecordsBuilderTestHelper.buildEndecaSearchRecords(
                endecaDataParser, "/endeca/test/samplebrandcontentitem.xml");
        assertThat(search.getDimension()).isNotNull().isNotEmpty().hasSize(7);
        final EndecaDimension dimension = search.getDimension().get(0);
        dimension.setName(EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND);
        dimension.setDimensionName(EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND);

        final EndecaSearchStateData searchState = new EndecaSearchStateData();
        searchState.setBrand(brand);
        final List<FacetData<EndecaSearchStateData>> facets = endecaToFacetsConverter.convertToFacetData(search,
                searchState, "Shoes", brand);
        assertThat(facets).isNotNull().isNotEmpty().hasSize(6);
    }

    @Test
    public void testFacetsConverter() throws TransformerConfigurationException, IOException,
            TransformerFactoryConfigurationError, JAXBException, TransformerException {
        given(categoryDataUrlResolver.resolve(Mockito.any(CategoryData.class)))
                .willReturn("/w124445/Womens");
        final EndecaSearch search = EndecaSearchRecordsBuilderTestHelper.buildEndecaSearchRecords(
                endecaDataParser, "/endeca/test/samplecategorycontentitem.xml");
        final EndecaSearchStateData searchState = new EndecaSearchStateData();
        final List<FacetData<EndecaSearchStateData>> facets = endecaToFacetsConverter.convertToFacetData(search,
                searchState, "Shoes", null);
        assertThat(facets).isNotNull().isNotEmpty().hasSize(4);

        final FacetData<EndecaSearchStateData> facet1 = facets.get(0);
        final List<String> facetValueNames = verifyFacet(facet1, "Category", 10);
        Assertions
                .assertThat(facetValueNames)
                .isNotNull()
                .isNotEmpty()
                .hasSize(10)
                .contains(
                        "AFL", "Boys", "Christening Wear", "Girls", "Newborn", "Shoes", "Sleepwear",
                        "Socks &amp; Tights", "Swimwear", "Underwear");

        final TargetFacetData<EndecaSearchStateData> facet2 = (TargetFacetData<EndecaSearchStateData>)facets.get(1);
        final List<String> facetValueNames2 = verifyFacet(facet2, "Colour", 6);
        Assertions
                .assertThat(facetValueNames2)
                .isNotNull()
                .isNotEmpty()
                .hasSize(6)
                .contains("Black", "Blue", "Red");
        assertThat(facet2.isExpand()).isTrue();
        assertThat(facet2.getValues()).onProperty("selected").containsExactly(Boolean.TRUE, Boolean.FALSE,
                Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE);
    }


    @Test
    public void testFacetsConverterWithRefinementsInFacetsExceptCategoryWithPName()
            throws TransformerConfigurationException,
            IOException,
            TransformerFactoryConfigurationError, JAXBException, TransformerException {
        given(categoryDataUrlResolver.resolve(Mockito.any(CategoryData.class)))
                .willReturn("/w124445/Womens");
        final EndecaSearch search = EndecaSearchRecordsBuilderTestHelper.buildEndecaSearchRecords(
                endecaDataParser, "/endeca/test/samplecategorycontentitem.xml");
        final EndecaSearchStateData searchState = new EndecaSearchStateData();
        final List<FacetData<EndecaSearchStateData>> facets = endecaToFacetsConverter.convertToFacetData(search,
                searchState, "Shoes", null);
        assertThat(facets).isNotNull().isNotEmpty().hasSize(4);

        verifyMultiselectValuesForFacets(facets, search.getBreadCrumb());

    }

    @Test
    public void testFacetsConverterWithRefinementsNotPartOfFacets()
            throws TransformerConfigurationException,
            IOException,
            TransformerFactoryConfigurationError, JAXBException, TransformerException {
        given(categoryDataUrlResolver.resolve(Mockito.any(CategoryData.class)))
                .willReturn("/w124445/Womens");
        final EndecaSearch search = EndecaSearchRecordsBuilderTestHelper.buildEndecaSearchRecords(
                endecaDataParser, "/endeca/test/samplebrandcontentitem.xml");
        final EndecaSearchStateData searchState = new EndecaSearchStateData();
        final List<FacetData<EndecaSearchStateData>> facets = endecaToFacetsConverter.convertToFacetData(search,
                searchState, "Shoes", null);
        assertThat(facets).isNotNull().isNotEmpty().hasSize(7);

        verifyMultiselectValuesForFacets(facets, search.getBreadCrumb());

    }

    @Test
    public void testFacetsConverterUrlWithDealCategory() throws TransformerConfigurationException, IOException,
            TransformerFactoryConfigurationError, JAXBException, TransformerException {
        given(commerceCategoryService.getCategoryForCode(Mockito.anyString())).willReturn(
                new TargetDealCategoryModel());
        given(dealCategoryModelUrlResolver.resolve(Mockito.any(TargetDealCategoryModel.class))).willReturn(
                "test/testDealCategoryId");
        final EndecaSearch search = EndecaSearchRecordsBuilderTestHelper.buildEndecaSearchRecords(
                endecaDataParser, "/endeca/test/samplecategorycontentitem.xml");
        final EndecaSearchStateData searchState = new EndecaSearchStateData();
        final List<FacetData<EndecaSearchStateData>> facets = endecaToFacetsConverter.convertToFacetData(search,
                searchState, "Shoes", null);
        assertThat(facets).isNotNull().isNotEmpty().hasSize(4);

        final FacetData<EndecaSearchStateData> facet1 = facets.get(0);
        final List<FacetValueData<EndecaSearchStateData>> facetValueDataList = facet1.getValues();
        for (final FacetValueData facetValueData : facetValueDataList) {
            final String result = ((EndecaSearchStateData)facetValueData.getQuery()).getUrl();
            assertThat(result).isEqualTo("test/testDealCategoryId");
        }

    }

    /**
     * @param facets
     * @param breadCrumbs
     */
    private void verifyMultiselectValuesForFacets(final List<FacetData<EndecaSearchStateData>> facets,
            final List<EndecaBreadCrumb> breadCrumbs) {
        for (final EndecaBreadCrumb breadCrumb : breadCrumbs) {
            final FacetData<EndecaSearchStateData> filteredFacet = findFacet(facets, breadCrumb.getDimensionType());
            if (filteredFacet != null && filteredFacet.isMultiSelect()) {
                final List<FacetValueData<EndecaSearchStateData>> facetValues = filteredFacet.getValues();
                assertThat(facetValues).isNotNull().isNotEmpty();

                assertThat(getFacetValueNamesAndVerifyValues(facetValues)).isNotNull().isNotEmpty();
            }
        }

    }

    @Test
    public void testFacetsConverterWithNoBrandORCategory() throws TransformerConfigurationException, IOException,
            TransformerFactoryConfigurationError, JAXBException, TransformerException {
        final EndecaSearch search = EndecaSearchRecordsBuilderTestHelper.buildEndecaSearchRecords(
                endecaDataParser, "/endeca/test/samplecategorycontentitem.xml");
        assertThat(search.getDimension()).isNotNull().isNotEmpty().hasSize(4);

        final EndecaSearchStateData searchState = new EndecaSearchStateData();
        searchState.setNavigationState(Collections.singletonList("123345345345"));
        searchState.setSearchTerm(Collections.singletonList("baby"));
        final List<FacetData<EndecaSearchStateData>> facets = endecaToFacetsConverter.convertToFacetData(search,
                searchState, "Shoes", brand);
        assertThat(facets).isNotNull().isNotEmpty().hasSize(4);
        final FacetData<EndecaSearchStateData> facet = facets.get(0);
        final List<FacetValueData<EndecaSearchStateData>> values = facet.getValues();
        assertThat(values).isNotNull().isNotEmpty();
        final FacetValueData<EndecaSearchStateData> valueData = values.get(0);
        final EndecaSearchStateData searchStateData = valueData.getQuery();
        assertThat(searchStateData).isNotNull();
        assertThat(searchStateData.getUrl()).isNotNull();
    }


    @Test
    public void testProcessDealFacetDataWithInvalidDates() {
        final EndecaDimension refinement = new EndecaDimension();
        final List<EndecaDimension> refinements = new ArrayList<>();
        refinement.setName("1456787@@Target Offer@@456gt");
        refinements.add(refinement);
        dealDimension.setRefinement(refinements);
        endecaToFacetsConverter.processDealFacetData(dealDimension);
        assertThat(dealDimension.getRefinement().size()).isEqualTo(0);
    }



    @Test
    public void testProcessDealFacetDataWithValidDates() {
        final EndecaDimension refinement = new EndecaDimension();
        final List<EndecaDimension> refinements = new ArrayList<>();
        refinement.setName("Jan  1 2014 12:00AM@@Target Offer@@Jan  1 2099 12:00AM");
        refinements.add(refinement);
        dealDimension.setRefinement(refinements);
        endecaToFacetsConverter.processDealFacetData(dealDimension);
        assertThat(dealDimension.getRefinement().size()).isEqualTo(1);
        assertThat(dealDimension.getRefinement().get(0).getName()).isEqualTo("Target Offer");
    }


    @Test
    public void testProcessDealFacetDataWithEmptyRefinementName() {
        final EndecaDimension refinement = new EndecaDimension();
        final List<EndecaDimension> refinements = new ArrayList<>();
        refinement.setName("Jan  1 2014 12:00AM@@@@Jan  1 2099 12:00AM");
        refinements.add(refinement);
        dealDimension.setRefinement(refinements);
        endecaToFacetsConverter.processDealFacetData(dealDimension);
        assertThat(dealDimension.getRefinement().size()).isEqualTo(0);

    }

    @Test
    public void testProcessDealFacetDataWithExpiredDeal() {
        final EndecaDimension refinement = new EndecaDimension();
        final List<EndecaDimension> refinements = new ArrayList<>();
        refinement.setName("Mar  1 2014 12:00AM@@Target Offer@@Apr  1 2014 12:00AM");
        refinements.add(refinement);
        dealDimension.setRefinement(refinements);
        endecaToFacetsConverter.processDealFacetData(dealDimension);
        assertThat(dealDimension.getRefinement().size()).isEqualTo(0);

    }

    @Test
    public void testProcessDealFacetDataWithNotStartedDeal() {
        final EndecaDimension refinement = new EndecaDimension();
        final List<EndecaDimension> refinements = new ArrayList<>();
        refinement.setName("Mar  1 2098 12:00AM@@Target Offer@@Apr  1 2099 12:00AM");
        refinements.add(refinement);
        dealDimension.setRefinement(refinements);
        endecaToFacetsConverter.processDealFacetData(dealDimension);
        assertThat(dealDimension.getRefinement().size()).isEqualTo(0);

    }

    @Test
    public void testProcessDealFacetDataWithExpiredandValidDeal() {
        final EndecaDimension refinement = new EndecaDimension();
        final EndecaDimension refinement1 = new EndecaDimension();
        final List<EndecaDimension> refinements = new ArrayList<>();
        refinement.setName("Mar  1 2098 12:00AM@@Target Offer@@Apr  1 2099 12:00AM");
        refinement1.setName("Jan  1 2014 12:00AM@@Target Offer@@Jan  1 2099 12:00AM");
        refinements.add(refinement1);
        refinements.add(refinement);
        dealDimension.setRefinement(refinements);
        endecaToFacetsConverter.processDealFacetData(dealDimension);
        assertThat(dealDimension.getRefinement().size()).isEqualTo(1);
        assertThat(dealDimension.getRefinement().get(0).getName()).isEqualTo("Target Offer");

    }

    @Test
    public void testPopulateDisplaySizeObjects() {
        final EndecaDimension refinement1 = new EndecaDimension();
        final EndecaDimension refinement2 = new EndecaDimension();
        final EndecaDimension refinement3 = new EndecaDimension();
        final EndecaDimension refinement4 = new EndecaDimension();
        final EndecaDimension refinement5 = new EndecaDimension();
        final EndecaDimension refinement6 = new EndecaDimension();
        final EndecaDimension refinement7 = new EndecaDimension();
        final EndecaDimension refinement8 = new EndecaDimension();

        refinement1.setName("0001**menshoes**00002**9");
        refinement2.setName("0003**kids**00003**7-9 months");
        refinement3.setName("0002**women**00003**large");
        refinement4.setName("0002**women**00001**small");
        refinement4.setSelected(true);
        refinement5.setName("0003**kids**00002**4-6 months");
        refinement6.setName("0002**women**00002**medium");
        refinement7.setName("0003**kids**00001**0-3 months");
        refinement8.setName("0001**menshoes**00001**7");

        final List<EndecaDimension> refinementList = new ArrayList<>();
        refinementList.add(refinement1);
        refinementList.add(refinement2);
        refinementList.add(refinement3);
        refinementList.add(refinement4);
        refinementList.add(refinement5);
        refinementList.add(refinement6);
        refinementList.add(refinement7);
        refinementList.add(refinement8);

        final List<EndecaDimension> refinementListExpected = new ArrayList<>();
        refinementListExpected.add(refinement8);
        refinementListExpected.add(refinement1);
        refinementListExpected.add(refinement4);
        refinementListExpected.add(refinement6);
        refinementListExpected.add(refinement3);
        refinementListExpected.add(refinement7);
        refinementListExpected.add(refinement5);
        refinementListExpected.add(refinement2);

        final EndecaSearchStateData searchState = new EndecaSearchStateData();
        final List<TargetFacetData<EndecaSearchStateData>> facetObjects = endecaToFacetsConverter
                .populateDisplaySizeObjects(refinementList, "displaySize", searchState, "kids");
        assertThat(facetObjects.size()).isNotNull().isEqualTo(3);
        assertThat(facetObjects.get(0).getName()).isEqualTo("menshoes");
        assertThat(facetObjects.get(1).getName()).isEqualTo("women");
        assertThat(facetObjects.get(2).getName()).isEqualTo("kids");
        for (int i = 0; i < refinementList.size(); i++) {
            assertThat(refinementList.get(i).getName()).isEqualTo(refinementListExpected.get(i).getName());
        }
        assertThat(facetObjects).onProperty("expand").containsExactly(Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
        assertThat(facetObjects).onProperty("code").containsExactly("menshoes", "women", "kids");
    }


    /**
     * @param facets
     * @param dimensionType
     * @return filteredFacet
     */
    private FacetData<EndecaSearchStateData> findFacet(final List<FacetData<EndecaSearchStateData>> facets,
            final String dimensionType) {
        if (dimensionType.equalsIgnoreCase(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY)) {
            return null;
        }
        final FacetData<EndecaSearchStateData> filteredFacet = (FacetData<EndecaSearchStateData>)CollectionUtils.find(
                facets, new Predicate() {

                    @Override
                    public boolean evaluate(final Object arg0) {
                        if (arg0 instanceof FacetData) {
                            final FacetData facet = (FacetData)arg0;
                            return dimensionType.equalsIgnoreCase(facet.getCode());
                        }
                        return false;
                    }
                });
        return filteredFacet;
    }

    /**
     * @param facet1
     * @return list of string which has the names of all the facets
     */
    public List<String> verifyFacet(final FacetData<EndecaSearchStateData> facet1, final String facetName,
            final int noOfFacets) {
        assertThat(facet1.getName()).isNotNull().isNotEmpty().isEqualTo(facetName);
        assertThat(facet1.getValues()).isNotNull().isNotEmpty().hasSize(noOfFacets);
        final List<String> facetValueNames = getFacetValueNamesAndVerifyValues(facet1.getValues());
        return facetValueNames;
    }

    /**
     * @param values
     * @return list of string which has the names of all the facets
     */
    private List<String> getFacetValueNamesAndVerifyValues(final List<FacetValueData<EndecaSearchStateData>> values) {
        final List<String> facetValueNames = new ArrayList<>();
        for (final FacetValueData<EndecaSearchStateData> facetValue : values) {
            facetValueNames.add(facetValue.getName());
            assertThat(facetValue.getName()).isNotNull().isNotEmpty();
            assertThat(facetValue.getCount()).isNotNull().isPositive();
        }
        return facetValueNames;
    }
}

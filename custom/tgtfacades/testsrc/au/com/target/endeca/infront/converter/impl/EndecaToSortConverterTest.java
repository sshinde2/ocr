/**
 * 
 */
package au.com.target.endeca.infront.converter.impl;


import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaOptions;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.transform.TargetEndecaURLHelper;
import au.com.target.tgtfacades.sort.TargetSortData;


/**
 * @author rsamuel3
 * 
 */
@UnitTest
public class EndecaToSortConverterTest {

    private EndecaToSortConverterImpl endecaToSortConverter;

    @Mock
    private TargetEndecaURLHelper targetEndecaUrlHelper;

    @Before
    public void setUp() {

        targetEndecaUrlHelper = Mockito.mock(TargetEndecaURLHelper.class);
        endecaToSortConverter = new EndecaToSortConverterImpl();
        endecaToSortConverter.setTargetEndecaURLHelper(targetEndecaUrlHelper);
    }

    @Test
    public void testEndecaToSortConverter() {
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        final String url = "http://localhost:9001/c/women/W93743";
        endecaSearchStateData.setRequestUrl(url);
        final List<EndecaOptions> eoList = new ArrayList<>();
        final EndecaOptions eo = new EndecaOptions();
        eo.setName("Latest");
        eo.setNavigationState("?Ns=12234&ntt=OnlineDate");
        eo.setSelected(true);
        eoList.add(eo);
        final List<TargetSortData> sortData = endecaToSortConverter.covertToTargetSortData(eoList,
                endecaSearchStateData);
        assertThat(sortData).isNotNull();
    }


    @Test
    public void testEndecaOptionsToSortConversionWithRelevance() {
        final String url = "www.blah.com";
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        final String currenetQueryParam = "testQuery";
        endecaSearchStateData.setCurrentQueryParam(currenetQueryParam);
        endecaSearchStateData.setRequestUrl(url);
        final List<EndecaOptions> endecaOptions = buildEndecaOptions(true, "relevance", "price", "latest", "rating");
        BDDMockito.when(targetEndecaUrlHelper.getValidEndecaToTargetNavigationState(Mockito.anyString())).thenReturn(
                EndecaConstants.QUERY_PARAM_SEPERATOR_QNMRK);
        final List<TargetSortData> sortData = endecaToSortConverter.covertToTargetSortData(endecaOptions,
                endecaSearchStateData);
        Assertions.assertThat(sortData).isNotNull().isNotEmpty().hasSize(4);
        for (final TargetSortData sort : sortData) {
            assertThat(sort.getCode()).isNotNull().isNotEmpty();
            assertThat(sort.getName()).isNull();
            assertThat(sort.getUrl()).isNotNull().isNotEmpty()
                    .contains("www.blah.com" + EndecaConstants.QUERY_PARAM_SEPERATOR_QNMRK);
            assertThat(sort.getWsUrl()).isEqualTo(EndecaConstants.QUERY_PARAM_SEPERATOR_QNMRK
                    + currenetQueryParam);
        }
    }


    @Test
    public void testEndecaOptionsToSortConversionWithNullOptions() {
        final String url = "www.blah.com";
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setRequestUrl(url);
        final List<EndecaOptions> endecaOptions = null;
        final List<TargetSortData> sortData = endecaToSortConverter
                .covertToTargetSortData(endecaOptions, endecaSearchStateData);
        assertThat(sortData).isNotNull().isEmpty();
    }

    @Test
    public void testEndecaOptionsToSortConversionWithEmptyOptions() {
        final String url = "www.blah.com";
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setRequestUrl(url);
        final List<EndecaOptions> endecaOptions = new ArrayList<>();
        final List<TargetSortData> sortData = endecaToSortConverter
                .covertToTargetSortData(endecaOptions, endecaSearchStateData);
        assertThat(sortData).isNotNull().isEmpty();
    }

    @Test
    public void testEndecaOptionsToSortConversionWithRelevanceNavStateNull() {
        final String url = "www.blah.com";
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setRequestUrl(url);
        final List<EndecaOptions> endecaOptions = buildEndecaOptions(false, "relevance", "price", "latest", "rating");
        final List<TargetSortData> sortData = endecaToSortConverter.covertToTargetSortData(endecaOptions,
                endecaSearchStateData);
        assertThat(sortData).isEmpty();
    }

    /**
     * @param options
     * @return List of EndecaOptions
     */
    private List<EndecaOptions> buildEndecaOptions(final boolean hasNavState, final String... options) {
        final List<EndecaOptions> endecaOptions = new ArrayList<>();
        for (final String option : options) {
            final EndecaOptions endecaOption = new EndecaOptions();
            endecaOption.setName(option);
            endecaOption.setSelected(false);
            if (hasNavState) {
                endecaOption.setNavigationState("?N=option");
            }
            endecaOptions.add(endecaOption);
        }
        return endecaOptions;
    }

}

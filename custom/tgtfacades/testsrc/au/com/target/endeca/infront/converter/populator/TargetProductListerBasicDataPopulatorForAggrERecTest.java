/**
 * 
 */
package au.com.target.endeca.infront.converter.populator;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.url.impl.TargetProductDataUrlResolver;

import com.endeca.navigation.AggrERec;
import com.endeca.navigation.AssocDimLocations;
import com.endeca.navigation.AssocDimLocationsList;
import com.endeca.navigation.DimLocation;
import com.endeca.navigation.DimVal;
import com.endeca.navigation.ERec;
import com.endeca.navigation.MAssocDimLocationsList;
import com.endeca.navigation.MPropertyMap;
import com.endeca.navigation.PropertyMap;


/**
 * @author pvarghe2
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductListerBasicDataPopulatorForAggrERecTest {

    @Mock
    private TargetProductDataUrlResolver targetProductDataUrlResolver;

    @InjectMocks
    private final TargetProductListerBasicDataPopulatorForAggrERec<AggrERec, TargetProductListerData> targetProductListerBasicDataPopulatorForAggrERec = new TargetProductListerBasicDataPopulatorForAggrERec<>();

    @Mock
    private AggrERec source;

    @Mock
    private TargetProductListerData target;

    @Mock
    private ERec eRec;

    @Before
    public void setUp() {
        final AssocDimLocationsList assocDimLocationsList = new MAssocDimLocationsList();
        final AssocDimLocations assocDimLocationProductName = mock(AssocDimLocations.class);
        final DimVal dimVal = mock(DimVal.class);
        final DimLocation dimLocation1 = mock(DimLocation.class);
        final DimLocation dimLocation2 = mock(DimLocation.class);
        final DimVal dimVal1 = mock(DimVal.class);
        final DimVal dimVal2 = mock(DimVal.class);
        final DimVal dimVal3 = mock(DimVal.class);
        when(assocDimLocationProductName.getDimRoot()).thenReturn(dimVal);
        when(assocDimLocationProductName.getDimRoot().getDimensionName())
                .thenReturn(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_NAME);
        when(assocDimLocationProductName.get(0)).thenReturn(dimLocation1);
        when(dimLocation1.getDimValue()).thenReturn(dimVal1);
        when(dimVal1.getName()).thenReturn("name");
        final AssocDimLocations assocDimLocationBrandName = mock(AssocDimLocations.class);
        when(assocDimLocationBrandName.getDimRoot()).thenReturn(dimVal3);
        when(dimVal3.getDimensionName())
                .thenReturn(EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND);
        when(assocDimLocationBrandName.get(0)).thenReturn(dimLocation2);
        when(dimLocation2.getDimValue()).thenReturn(dimVal2);
        when(dimVal2.getName()).thenReturn("band");
        assocDimLocationsList.add(assocDimLocationProductName);
        assocDimLocationsList.add(assocDimLocationBrandName);
        when(eRec.getDimValues()).thenReturn(assocDimLocationsList);
        when(source.getRepresentative()).thenReturn(eRec);
    }

    @Test
    public void testPopulateWithFullValidData() {
        final PropertyMap eRecMap = populateERecFullData();

        final PropertyMap sourceMap = new MPropertyMap();
        sourceMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_AVAIL_ONLINE_QTY, "10");
        sourceMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_AVAIL_STORE_QTY, "100");
        sourceMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_ACTUAL_CONSOLIDATED_STORE_STOCK, "100");

        when(eRec.getProperties()).thenReturn(eRecMap);
        when(source.getProperties()).thenReturn(sourceMap);
        targetProductListerBasicDataPopulatorForAggrERec.populate(source, target);
        verify(target).setName("name");
        verify(target).setBaseName("name");
        verify(target).setBrand("band");
        verify(target).setTopLevelCategory(
                eRecMap.get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_TOP_LEVEL_CATEGORY_NAME).toString());
        verify(target).setBaseProductCode(
                eRecMap.get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE).toString());
        verify(target).setCode(eRecMap.get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE).toString());
        verify(target)
                .setDescription(eRecMap.get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DESCRIPTION).toString());
        verify(target).setTotalInterest(
                eRecMap.get(EndecaConstants.EndecaRecordSpecificFields.TOTAL_INTEREST_ON_PRD).toString());
        verify(target).setOriginalCategoryCode(
                eRecMap.get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_ORIG_CATEGORY).toString());
        verify(target)
                .setNumberOfReviews(
                        Integer.valueOf(eRecMap
                                .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_NUMBER_OF_REVIEWS)
                                .toString()));
        verify(target)
                .setAverageRating(Double.valueOf(
                        eRecMap.get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_REVIEW_AVG_RATING).toString()));
        verify(target).setOnlineExclusive(Boolean.getBoolean(
                eRecMap.get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_ONLINE_EXCLUSIVE).toString()));
        verify(target).setGiftCard(Boolean.TRUE.booleanValue());
        verify(target).setPreview(Boolean.TRUE.booleanValue());
        verify(target).setDealDescription(StringUtils.EMPTY);
        verify(target).setMaxAvailOnlineQty(
                Integer.valueOf(sourceMap.get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_AVAIL_ONLINE_QTY)
                        .toString()));
        verify(target).setMaxActualConsolidatedStoreStock(
                Integer.valueOf(sourceMap.get(
                        EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_ACTUAL_CONSOLIDATED_STORE_STOCK)
                        .toString()));
        verify(target).setMaxAvailStoreQty(
                Integer.valueOf(sourceMap.get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_AVAIL_STORE_QTY)
                        .toString()));
    }

    @Test
    public void testPopulateWithValidUrl() {
        final PropertyMap eRecMap = new MPropertyMap();
        final PropertyMap sourceMap = new MPropertyMap();
        sourceMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_AVAIL_ONLINE_QTY, null);
        sourceMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_AVAIL_STORE_QTY, null);
        when(eRec.getProperties()).thenReturn(eRecMap);
        when(source.getProperties()).thenReturn(sourceMap);
        when(targetProductDataUrlResolver.resolve(anyString(), anyString())).thenReturn("someUrl");
        when(target.getName()).thenReturn("name");
        when(target.getCode()).thenReturn("111123");
        targetProductListerBasicDataPopulatorForAggrERec.populate(source, target);
        verify(target).setUrl("someUrl");
    }

    @Test
    public void testPopulateStockDetailsWithNullData() {
        final PropertyMap eRecMap = new MPropertyMap();
        final PropertyMap sourceMap = new MPropertyMap();
        sourceMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_AVAIL_ONLINE_QTY, null);
        sourceMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_AVAIL_STORE_QTY, null);
        when(eRec.getProperties()).thenReturn(eRecMap);
        when(source.getProperties()).thenReturn(sourceMap);
        targetProductListerBasicDataPopulatorForAggrERec.populate(source, target);
        verify(target).setMaxAvailOnlineQty(Integer.valueOf(0));
        verify(target).setMaxAvailStoreQty(Integer.valueOf(0));
    }

    @Test
    public void testPopulateStockDetailsWithEmptyData() {
        final PropertyMap eRecMap = new MPropertyMap();
        final PropertyMap sourceMap = new MPropertyMap();
        sourceMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_AVAIL_ONLINE_QTY, "");
        sourceMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_MAX_AVAIL_STORE_QTY, "");
        when(eRec.getProperties()).thenReturn(eRecMap);
        when(source.getProperties()).thenReturn(sourceMap);
        targetProductListerBasicDataPopulatorForAggrERec.populate(source, target);
        verify(target).setMaxAvailOnlineQty(Integer.valueOf(0));
        verify(target).setMaxAvailStoreQty(Integer.valueOf(0));
    }

    /**
     * @return propertyMap
     */
    private PropertyMap populateERecFullData() {
        final PropertyMap eRecMap = new MPropertyMap();
        eRecMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_TOP_LEVEL_CATEGORY_NAME,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_TOP_LEVEL_CATEGORY_NAME);
        eRecMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE);
        eRecMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DESCRIPTION,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DESCRIPTION);
        eRecMap.put(EndecaConstants.EndecaRecordSpecificFields.TOTAL_INTEREST_ON_PRD,
                EndecaConstants.EndecaRecordSpecificFields.TOTAL_INTEREST_ON_PRD);
        eRecMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_NUMBER_OF_REVIEWS,
                "1");
        eRecMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_REVIEW_AVG_RATING,
                "2.67");
        eRecMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_ONLINE_EXCLUSIVE,
                "true");
        eRecMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_IS_GIFT_CARD,
                "1");
        eRecMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_ORIG_CATEGORY,
                "W345");
        eRecMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PREVIEW_FLAG, "1");
        eRecMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_DESCRIPTION,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_DESCRIPTION);
        eRecMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_ENDTIME,
                null);
        eRecMap.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_STARTTIME,
                null);
        return eRecMap;
    }

}

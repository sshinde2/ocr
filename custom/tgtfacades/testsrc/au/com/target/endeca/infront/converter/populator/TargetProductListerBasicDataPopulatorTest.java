/**
 * 
 */
package au.com.target.endeca.infront.converter.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.data.EndecaProduct;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.enums.TargetPromotionStatusEnum;
import au.com.target.tgtfacades.url.impl.TargetProductDataUrlResolver;


/**
 * @author pthoma20
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductListerBasicDataPopulatorTest {

    @Mock
    private TargetProductDataUrlResolver targetProductDataUrlResolver;

    @InjectMocks
    private final TargetProductListerBasicDataPopulator targetProductListerBasicDataPopulator = new TargetProductListerBasicDataPopulator();

    @Test
    public void test() {
        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        source.setBrand("Target");
        source.setName("Target Shirt");
        source.setCode("P1000");
        source.setTopLevelCategoryName("Baby");
        source.setIsGiftCard(Boolean.TRUE);
        source.setPreviewFlag(Boolean.TRUE);
        source.setName("BabyShoes");
        source.setDescription("This is an awesome T-shirt");
        source.setOnlineExclusive(Boolean.TRUE);
        source.setNumOfReviews("10");
        source.setReviewAvgRating("4.6");
        given(
                targetProductDataUrlResolver.resolve(source.getName(), source.getCode()))
                        .willReturn(
                                source.getName() + "/" + source.getCode());
        source.setOnlineExclusive(Boolean.TRUE);
        source.setHotProduct(Boolean.TRUE);
        source.setTargetExclusive(Boolean.TRUE);
        source.setDisplayOnlyFlag(Boolean.TRUE);
        targetProductListerBasicDataPopulator.populate(source, target);
        assertThat(target.getName()).isEqualTo(source.getName());
        assertThat(target.getBaseProductCode()).isEqualTo("P1000");
        assertThat(target.getDescription()).isEqualTo(source.getDescription());
        assertThat(target.getBrand()).isEqualTo(source.getBrand());
        assertThat(target.getTopLevelCategory()).isEqualTo(source.getTopLevelCategoryName());
        assertThat(target.isGiftCard()).isEqualTo(source.getIsGiftCard().booleanValue());
        assertThat(target.getBaseName()).isEqualTo(source.getName());
        assertThat(target.isOnlineExclusive()).isEqualTo(source.getOnlineExclusive().booleanValue());
        assertThat(target.getNumberOfReviews()).isEqualTo(Integer.valueOf(10));
        assertThat(target.getAverageRating()).isEqualTo(Double.valueOf(4.6));
        assertThat(target.getUrl()).isEqualTo(source.getName() + "/" + source.getCode());
        assertThat(target.getPromotionStatuses().size()).isEqualTo(3);
        assertThat(target.isPreview()).isTrue();
        assertThat(target.isDisplayOnly()).isTrue();
    }

    @Test
    public void testWhenNewArrivals() {
        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        source.setTag(TgtFacadesConstants.NEW_ARRIVALS);
        targetProductListerBasicDataPopulator.populate(source, target);
        assertThat(target.isNewArrived()).isTrue();
    }

    @Test
    public void testPreviewFlag() {
        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        source.setPreviewFlag(Boolean.TRUE);
        targetProductListerBasicDataPopulator.populate(source, target);
        assertThat(target.isPreview()).isTrue();
    }

    @Test
    public void testWhenNull() {
        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        source.setTag(null);
        targetProductListerBasicDataPopulator.populate(source, target);
        assertThat(target.isNewArrived());
    }

    @Test
    public void testWhenEmptyString() {
        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        source.setTag(StringUtils.EMPTY);
        targetProductListerBasicDataPopulator.populate(source, target);
        assertThat(target.isNewArrived());
    }

    @Test
    public void testWhenContainsMultipleValues() {
        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        source.setTag("Essentials," + TgtFacadesConstants.NEW_ARRIVALS + ",Exclusive");
        targetProductListerBasicDataPopulator.populate(source, target);
        assertThat(target.isNewArrived()).isTrue();
    }

    @Test
    public void testWhenContainsMaxStoreQty() {
        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        source.setMaxAvailStoreQty("50");
        targetProductListerBasicDataPopulator.populate(source, target);
        assertThat(target.getMaxAvailStoreQty()).isEqualTo(50);
    }

    @Test
    public void testWhenContainsMaxStoreQtyEmpty() {
        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        source.setMaxAvailStoreQty("");
        targetProductListerBasicDataPopulator.populate(source, target);
        assertThat(target.getMaxAvailStoreQty()).isNull();
    }

    @Test
    public void testWhenContainsMaxStoreQtyNull() {
        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        source.setMaxAvailStoreQty(null);
        targetProductListerBasicDataPopulator.populate(source, target);
        assertThat(target.getMaxAvailStoreQty()).isNull();
    }

    @Test
    public void testWhenContainsMaxOnlineQtyEmpty() {
        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        source.setMaxAvailOnlineQty("");
        targetProductListerBasicDataPopulator.populate(source, target);
        assertThat(target.getMaxAvailOnlineQty()).isNull();
    }

    @Test
    public void testWhenContainsMaxOnlineQtyNull() {
        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        source.setMaxAvailOnlineQty(null);
        targetProductListerBasicDataPopulator.populate(source, target);
        assertThat(target.getMaxAvailOnlineQty()).isNull();
    }

    @Test
    public void testWhenContainsMaxOnlineQty() {
        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        source.setMaxAvailOnlineQty("100");
        targetProductListerBasicDataPopulator.populate(source, target);
        assertThat(target.getMaxAvailOnlineQty()).isEqualTo(100);
    }

    @Test
    public void testWhenProductHasJustNewLowerPrice() {
        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        source.setNewLowerPriceFlag(Boolean.TRUE);
        targetProductListerBasicDataPopulator.populate(source, target);
        assertThat(target.getPromotionStatuses());
        assertThat(target.getPromotionStatuses().size());
        assertThat(target.getPromotionStatuses().get(0)).isEqualTo(TargetPromotionStatusEnum.NEW_LOWER_PRICE);
    }

    @Test
    public void testWhenProductHasNewLowerPriceAndEssentialPromotion() {
        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        source.setNewLowerPriceFlag(Boolean.TRUE);
        source.setEssential(Boolean.TRUE);
        targetProductListerBasicDataPopulator.populate(source, target);
        assertThat(target.getPromotionStatuses());
        assertThat(target.getPromotionStatuses().size()).isEqualTo(2);
        assertThat(target.getPromotionStatuses().get(0)).isEqualTo(TargetPromotionStatusEnum.NEW_LOWER_PRICE);
    }

    @Test
    public void testWhenProductDoesntHaveNewLowerPrice() {
        final TargetProductListerData target = new TargetProductListerData();
        final EndecaProduct source = new EndecaProduct();
        source.setNewLowerPriceFlag(Boolean.FALSE);
        targetProductListerBasicDataPopulator.populate(source, target);
        assertThat(target.getPromotionStatuses()).isEmpty();
    }
}

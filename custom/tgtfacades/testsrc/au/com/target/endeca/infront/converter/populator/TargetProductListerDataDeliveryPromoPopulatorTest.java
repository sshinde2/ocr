/**
 * 
 */
package au.com.target.endeca.infront.converter.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaProduct;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtfacades.product.data.TargetProductListerData;


/**
 * @author Pradeep
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductListerDataDeliveryPromoPopulatorTest {

    private static final String HOME_DELIVERY = "2@@home-delivery@@";
    private static final String EXPRESS_DELIVERY = "1@@Express-delivery@@";
    private static final String FREE_EXPRESS_DELIVERY_HURRY_UP = "Free Express Delivery Hurry Up";
    private static final String FREE_HOME_DELIVERY_HURRY_UP = "Free Home Delivery Hurry Up";
    private static final String FREE_HOME_DELIVERY_EMPTY_DATE = "Free Express Delivery Empty Date";

    @Mock
    private TargetSalesApplicationService targetSalesApplicationService;

    @InjectMocks
    private final TargetProductListerDataDeliveryPromoPopulator deliveryPromoPopulator = new TargetProductListerDataDeliveryPromoPopulator();

    /**
     * Test method for Null Promo Delivery Data.
     */
    @Test
    public void testPopulateWithNullData() {
        final EndecaProduct edecaProduct = new EndecaProduct();
        final TargetProductListerData productData = new TargetProductListerData();
        deliveryPromoPopulator.populate(edecaProduct, productData);
        Assert.assertTrue(StringUtils.isEmpty(productData.getProductPromotionalDeliverySticker()));
    }

    @Test
    public void testPopulateWithDifferentSale() {
        final EndecaProduct edecaProduct = new EndecaProduct();
        final TargetProductListerData productData = new TargetProductListerData();
        final List promoDataList = new ArrayList<>();
        BDDMockito.given(
                targetSalesApplicationService.getCurrentSalesApplication())
                .willReturn(SalesApplication.WEB);
        promoDataList.add(getHomeDeliveryPromoDataWithDifferentSale());
        edecaProduct.setApplicableDeliveryPromotions(promoDataList);
        deliveryPromoPopulator.populate(edecaProduct, productData);
        Assert.assertTrue(StringUtils.isEmpty(productData.getProductPromotionalDeliverySticker()));
    }

    @Test
    public void testPopulateWithNullSale() {
        final EndecaProduct edecaProduct = new EndecaProduct();
        final TargetProductListerData productData = new TargetProductListerData();
        final List promoDataList = new ArrayList<>();
        BDDMockito.given(
                targetSalesApplicationService.getCurrentSalesApplication())
                .willReturn(null);
        promoDataList.add(getHomeDeliveryPromoDataWithDifferentSale());
        edecaProduct.setApplicableDeliveryPromotions(promoDataList);
        deliveryPromoPopulator.populate(edecaProduct, productData);
        Assert.assertTrue(StringUtils.isEmpty(productData.getProductPromotionalDeliverySticker()));
    }


    /**
     * Test method for Single Promo DeliveryData.
     */
    @Test
    public void testPopulate() {
        final EndecaProduct edecaProduct = new EndecaProduct();
        final List promoDataList = new ArrayList<>();
        promoDataList.add(getHomeDeliveryPromoData());
        edecaProduct.setApplicableDeliveryPromotions(promoDataList);
        final TargetProductListerData productData = new TargetProductListerData();
        deliveryPromoPopulator.populate(edecaProduct, productData);
        Assert.assertTrue(StringUtils.equals(productData.getProductPromotionalDeliverySticker(),
                "Free Home Delivery"));
    }


    /**
     * Test method for Multiple Data of different priority.
     */
    @Test
    public void testPopulateWithDifferentPriority() {
        final EndecaProduct edecaProduct = new EndecaProduct();
        final List promoDataList = new ArrayList<>();
        promoDataList.add(getHomeDeliveryPromoData());
        promoDataList.add(getHomeDeliveryPromoDataWithHigherPriority());
        edecaProduct.setApplicableDeliveryPromotions(promoDataList);
        final TargetProductListerData productData = new TargetProductListerData();
        deliveryPromoPopulator.populate(edecaProduct, productData);
        Assert.assertTrue(StringUtils.equals(productData.getProductPromotionalDeliverySticker(),
                FREE_HOME_DELIVERY_HURRY_UP));
    }



    /**
     * Test method for Multiple Data of Different Promo Display Order.
     */
    @Test
    public void testPopulateWithDifferentPromoDisplayOrder() {
        final EndecaProduct edecaProduct = new EndecaProduct();
        final List promoDataList = new ArrayList<>();
        promoDataList.add(getHomeDeliveryPromoData());
        promoDataList.add(getHomeDeliveryPromoDataWithHigherPriority());
        promoDataList.add(getExpressDeliveryPromoData());
        edecaProduct.setApplicableDeliveryPromotions(promoDataList);
        final TargetProductListerData productData = new TargetProductListerData();
        deliveryPromoPopulator.populate(edecaProduct, productData);
        Assert.assertTrue(StringUtils.equals(productData.getProductPromotionalDeliverySticker(),
                FREE_EXPRESS_DELIVERY_HURRY_UP));
    }

    /**
     * Test method for Multiple Data of Different Promo Display Order and empty sticker.
     */
    @Test
    public void testPopulateWithDifferentPromoDisplayOrderAndEmptySticker() {
        final EndecaProduct edecaProduct = new EndecaProduct();
        final List promoDataList = new ArrayList<>();
        promoDataList.add(getHomeDeliveryPromoData());
        promoDataList.add(getHomeDeliveryPromoDataWithHigherPriority());
        promoDataList.add(getExpressDeliveryPromoData());
        promoDataList.add(getExpressDeliveryPromoDataWithEmptySticker());
        edecaProduct.setApplicableDeliveryPromotions(promoDataList);
        final TargetProductListerData productData = new TargetProductListerData();
        deliveryPromoPopulator.populate(edecaProduct, productData);
        Assert.assertTrue(StringUtils.equals(productData.getProductPromotionalDeliverySticker(),
                FREE_HOME_DELIVERY_HURRY_UP));
    }

    /**
     * Test method for Multiple Data and Higher priority Data with Past Date .
     */
    @Test
    public void testPopulateWithPastDate() {
        final EndecaProduct edecaProduct = new EndecaProduct();
        final List promoDataList = new ArrayList<>();
        promoDataList.add(getHomeDeliveryPromoData());
        promoDataList.add(getHomeDeliveryPromoDataWithHigherPriority());
        promoDataList.add(getExpressDeliveryPromoDataWithPastDate());
        promoDataList.add(getExpressDeliveryPromoData());
        edecaProduct.setApplicableDeliveryPromotions(promoDataList);
        final TargetProductListerData productData = new TargetProductListerData();
        deliveryPromoPopulator.populate(edecaProduct, productData);
        Assert.assertTrue(StringUtils.equals(productData.getProductPromotionalDeliverySticker(),
                FREE_EXPRESS_DELIVERY_HURRY_UP));
    }

    /**
     * Test method for Multiple Data and Higher priority Data with Past Date .
     */
    @Test
    public void testPopulateWithEmptyDate() {
        final EndecaProduct edecaProduct = new EndecaProduct();
        final List promoDataList = new ArrayList<>();
        promoDataList.add(getHomeDeliveryPromoData());
        promoDataList.add(getHomeDeliveryPromoDataWithHigherPriority());
        promoDataList.add(getExpressDeliveryPromoDataWithEmptyDate());
        promoDataList.add(getExpressDeliveryPromoData());
        edecaProduct.setApplicableDeliveryPromotions(promoDataList);
        final TargetProductListerData productData = new TargetProductListerData();
        deliveryPromoPopulator.populate(edecaProduct, productData);
        Assert.assertTrue(StringUtils.equals(productData.getProductPromotionalDeliverySticker(),
                FREE_HOME_DELIVERY_EMPTY_DATE));
    }

    /**
     * Test method for Multiple Data and Higher priority Data with Past Date .
     */
    @Test
    public void testPopulateWithEmptyStartDate() {
        final EndecaProduct edecaProduct = new EndecaProduct();
        final List promoDataList = new ArrayList<>();
        promoDataList.add(getHomeDeliveryPromoData());
        promoDataList.add(getHomeDeliveryPromoDataWithHigherPriority());
        promoDataList.add(getExpressDeliveryPromoDataWithEmptyStartDate());
        promoDataList.add(getExpressDeliveryPromoData());
        edecaProduct.setApplicableDeliveryPromotions(promoDataList);
        final TargetProductListerData productData = new TargetProductListerData();
        deliveryPromoPopulator.populate(edecaProduct, productData);
        Assert.assertTrue(StringUtils.equals(productData.getProductPromotionalDeliverySticker(),
                FREE_HOME_DELIVERY_EMPTY_DATE));
    }

    private String getExpressDeliveryPromoData() {
        final Calendar calendar = Calendar.getInstance();
        final String startDate = getFormatedDate(calendar.getTime());
        calendar.add(Calendar.DAY_OF_YEAR, 5);
        final String endDate = getFormatedDate(calendar.getTime());
        return EXPRESS_DELIVERY + startDate + EndecaConstants.ENDECA_CONCATENATE_SYMBOL + endDate
                + "@@100@@Free Express Delivery Hurry Up@@ ";
    }

    private String getExpressDeliveryPromoDataWithPastDate() {
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -30);
        final String startDate = getFormatedDate(calendar.getTime());
        calendar.add(Calendar.MINUTE, -5);
        final String endDate = getFormatedDate(calendar.getTime());
        return EXPRESS_DELIVERY + startDate + EndecaConstants.ENDECA_CONCATENATE_SYMBOL + endDate
                + "@@1000@@Free Express Delivery Time Out@@ ";
    }

    private String getExpressDeliveryPromoDataWithEmptyDate() {
        return EXPRESS_DELIVERY + " " + EndecaConstants.ENDECA_CONCATENATE_SYMBOL + " "
                + "@@1000@@Free Express Delivery Empty Date@@ ";
    }

    private String getExpressDeliveryPromoDataWithEmptyStartDate() {
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, 5);
        final String endDate = getFormatedDate(calendar.getTime());
        return EXPRESS_DELIVERY + " " + EndecaConstants.ENDECA_CONCATENATE_SYMBOL + endDate
                + "@@1000@@Free Express Delivery Empty Date@@ ";
    }

    private String getExpressDeliveryPromoDataWithEmptySticker() {
        final Calendar calendar = Calendar.getInstance();
        final String startDate = getFormatedDate(calendar.getTime());
        calendar.add(Calendar.DAY_OF_YEAR, 5);
        final String endDate = getFormatedDate(calendar.getTime());
        return EXPRESS_DELIVERY + startDate + EndecaConstants.ENDECA_CONCATENATE_SYMBOL + endDate
                + "@@5000@@ @@ ";
    }



    private String getHomeDeliveryPromoData() {
        final Calendar calendar = Calendar.getInstance();
        final String startDate = getFormatedDate(calendar.getTime());
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        final String endDate = getFormatedDate(calendar.getTime());
        return HOME_DELIVERY + startDate + EndecaConstants.ENDECA_CONCATENATE_SYMBOL + endDate
                + "@@1000@@Free Home Delivery@@ ";

    }

    private String getHomeDeliveryPromoDataWithDifferentSale() {
        final Calendar calendar = Calendar.getInstance();
        final String startDate = getFormatedDate(calendar.getTime());
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        final String endDate = getFormatedDate(calendar.getTime());
        return HOME_DELIVERY + startDate + EndecaConstants.ENDECA_CONCATENATE_SYMBOL + endDate
                + "@@1000@@Free Home Delivery@@KIOSK ";

    }

    private String getHomeDeliveryPromoDataWithHigherPriority() {
        final Calendar calendar = Calendar.getInstance();
        final String startDate = getFormatedDate(calendar.getTime());
        calendar.add(Calendar.MINUTE, 10);
        final String endDate = getFormatedDate(calendar.getTime());
        return HOME_DELIVERY + startDate + EndecaConstants.ENDECA_CONCATENATE_SYMBOL + endDate
                + "@@1001@@Free Home Delivery Hurry Up@@ ";
    }

    private String getFormatedDate(final Date date) {
        return new SimpleDateFormat(EndecaConstants.ENDECA_DATE_FORMAT).format(date);
    }

}

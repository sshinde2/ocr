/**
 * 
 */
package au.com.target.endeca.infront.converter.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.product.data.ProductDisplayType;
import au.com.target.tgtcore.stock.impl.TargetStockLevelStatusStrategyImpl;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetSelectedVariantData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.url.impl.TargetProductDataUrlResolver;
import au.com.target.tgtfacades.util.TargetPriceHelper;
import au.com.target.tgtutility.util.TargetDateUtil;

import com.endeca.navigation.AggrERec;
import com.endeca.navigation.MAggrERec;
import com.endeca.navigation.MAssocDimLocations;
import com.endeca.navigation.MAssocDimLocationsList;
import com.endeca.navigation.MDimLocation;
import com.endeca.navigation.MDimVal;
import com.endeca.navigation.MERec;
import com.endeca.navigation.MERecList;
import com.endeca.navigation.MPropertyMap;
import com.endeca.navigation.PropertyMap;


/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductListerDataVariantPopulatorForAggrERecTest {
    private Converter<StockLevelStatus, StockData> stockLevelStatusConverter;
    private final TargetStockLevelStatusStrategyImpl onlineStockLevelStatusStrategy = new TargetStockLevelStatusStrategyImpl();
    @Mock
    private TargetProductDataUrlResolver targetProductDataUrlResolver;
    @Mock
    private TargetPriceHelper targetPriceHelper;
    @Mock
    private PriceData priceData;
    @Mock
    private TargetSharedConfigService targetSharedConfigService;
    @InjectMocks
    private final TargetProductListerDataVariantPopulatorForAggrERec<AggrERec, TargetProductListerData> populator = new TargetProductListerDataVariantPopulatorForAggrERec<>();

    @Before
    public void setUp() {
        when(targetPriceHelper.createPriceData(any(Double.class))).thenReturn(priceData);
        onlineStockLevelStatusStrategy.setLowStockThreshold(8);
        stockLevelStatusConverter = new Converter<StockLevelStatus, StockData>() {
            @Override
            public StockData convert(final StockLevelStatus stockLevelStatus) throws ConversionException {
                final StockData stockData = new StockData();
                stockData.setStockLevelStatus(stockLevelStatus);
                return stockData;
            }

            @Override
            public StockData convert(final StockLevelStatus stockLevelStatus, final StockData stockData)
                    throws ConversionException {
                stockData.setStockLevelStatus(stockLevelStatus);
                return stockData;
            }
        };
        populator.setStockLevelStatusConverter(stockLevelStatusConverter);
        populator.setOnlineStocklevelStatusStrategy(onlineStockLevelStatusStrategy);
        onlineStockLevelStatusStrategy.setTargetSharedConfigService(targetSharedConfigService);
        doReturn(Integer.valueOf(8)).when(targetSharedConfigService).getInt(null, 8);
    }

    @Test
    public void testPopulateSelectedVariantDataWithSelectedVariantColourAndSizeSellable() {
        final PropertyMap map = new MPropertyMap();
        when(targetProductDataUrlResolver.resolve("peppa pig",
                "P1000_Black")).thenReturn("/p/P1000_Black");
        final Map<String, String> dimKeyValues = new HashMap<String, String>();
        dimKeyValues.put("swatchColour", "no colour");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, "P1000_Black");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE, "P1000_Black_S");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRICE, "10.3500");
        final TargetSelectedVariantData data = populator.populateSelectedVariantData(map, "P1000_Black", "peppa pig",
                dimKeyValues);
        assertThat(data).isNotNull();
        assertThat(data.getProductCode()).isEqualTo("P1000_Black");
        assertThat(Boolean.valueOf(data.isSellableVariant())).isFalse();
        assertThat(data.getUrl()).isEqualTo("/p/P1000_Black");
        assertThat(data.getColor()).isEqualTo(StringUtils.EMPTY);
        assertThat(data.getAvailableQty()).isEqualTo(0);
        assertThat(data.getPrice()).isNull();

    }

    @Test
    public void testPopulateSelectedVariantDataWithSelectedVariantSizeSellable() {
        final PropertyMap map = new MPropertyMap();
        when(targetProductDataUrlResolver.resolve("peppa pig",
                "P1000_Black_S")).thenReturn("/p/P1000_Black_S");
        final Map<String, String> dimKeyValues = new HashMap<String, String>();
        dimKeyValues.put("swatchColour", "Blue");
        dimKeyValues.put("size", "S");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, "P1000_Black");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE, "P1000_Black_S");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRICE, "10.3500");
        final TargetSelectedVariantData data = populator.populateSelectedVariantData(map, "P1000_Black_S", "peppa pig",
                dimKeyValues);
        assertThat(data).isNotNull();
        assertThat(data.getProductCode()).isEqualTo("P1000_Black_S");
        assertThat(data.isSellableVariant()).isTrue();
        assertThat(data.getUrl()).isEqualTo("/p/P1000_Black_S");
        assertThat(data.getColor()).isEqualTo("Blue");
        assertThat(data.getAvailableQty()).isEqualTo(0);
        assertThat(data.getSize()).isEqualTo("S");
        assertThat(data.getPrice()).isEqualTo(priceData);
    }

    @Test
    public void testPopulateSelectedVariantDataWithNoMatchingRecord() {
        final PropertyMap map = new MPropertyMap();
        final Map<String, String> dimKeyValues = new HashMap<String, String>();
        dimKeyValues.put("colour", "Blue");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, "P1000_Black");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE, "P1000_Black_S");
        final TargetSelectedVariantData data = populator.populateSelectedVariantData(map, "CP2000", "peppa pig",
                dimKeyValues);
        assertThat(data).isNull();
    }


    @Test
    public void testPopulateSelectedVariantDataWithSelectedVariantColourWithColourSellable() {
        final PropertyMap map = new MPropertyMap();
        when(targetProductDataUrlResolver.resolve("peppa pig",
                "CP2012")).thenReturn("/p/CP2012");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, "CP2012");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY, "79");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRICE, null);
        final Map<String, String> dimKeyValues = new HashMap<String, String>();
        dimKeyValues.put("swatchColour", "Blue");
        final TargetSelectedVariantData data = populator.populateSelectedVariantData(map, "CP2012", "peppa pig",
                dimKeyValues);
        assertThat(data).isNotNull();
        assertThat(data.getProductCode()).isEqualTo("CP2012");
        assertThat(data.isSellableVariant()).isTrue();
        assertThat(data.getUrl()).isEqualTo("/p/CP2012");
        assertThat(data.getColor()).isEqualTo("Blue");
        assertThat(data.getAvailableQty()).isEqualTo(79);
        assertThat(data.getPrice()).isNull();
    }

    @Test
    public void testPopulateSelectedVariantDataWithSelectedVariantSize() {
        final PropertyMap map = new MPropertyMap();
        when(targetProductDataUrlResolver.resolve("peppa pig",
                "P1000_Black_S")).thenReturn("/p/P1000_Black_S");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, "P1000_Black");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE, "P1000_Black_S");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY, "79");
        final Map<String, String> dimKeyValues = new HashMap<String, String>();
        dimKeyValues.put("swatchColour", "Black");
        final TargetSelectedVariantData data = populator.populateSelectedVariantData(map, "P1000_Black_S", "peppa pig",
                dimKeyValues);
        assertThat(data).isNotNull();
        assertThat(data.getProductCode()).isEqualTo("P1000_Black_S");
        assertThat(data.isSellableVariant()).isTrue();
        assertThat(data.getUrl()).isEqualTo("/p/P1000_Black_S");
        assertThat(data.getColor()).isEqualTo("Black");
        assertThat(data.getAvailableQty()).isEqualTo(79);
        assertThat(data.isSellableVariant()).isTrue();
    }

    @Test
    public void testPopulateWithSelectedVariantNotEmpty() {
        final TargetProductListerData listerData = new TargetProductListerData();
        when(targetProductDataUrlResolver.resolve("peppa pig",
                "P1000_Black")).thenReturn("/p/P1000_Black");
        listerData.setName("peppa pig");
        final PropertyMap map = new MPropertyMap();
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, "P1000_Black");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE, "P1000_Black_S");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY, "79");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_COUNT, "79");
        final PropertyMap map1 = new MPropertyMap();
        map1.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, "P1000_Red");
        map1.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE, "P1000_Red_S");
        map1.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY, "100");
        final MAggrERec aggrecErec = new MAggrERec();
        final MERecList list = new MERecList();
        final MERec rec = new MERec();
        final MERec rec1 = new MERec();
        rec.setProperties(map);
        rec1.setProperties(map1);
        list.add(rec);
        list.add(rec1);
        aggrecErec.setERecs(list);
        populator.populate(aggrecErec, listerData, "P1000_Black");
        assertThat(listerData.getCode()).isEqualTo("P1000_Black");
        assertThat(listerData.getColourVariantCount()).isEqualTo(79);
        assertThat(listerData.getUrl()).isEqualTo("/p/P1000_Black");
        assertThat(listerData.getSelectedVariantData()).isNotNull();
        assertThat(listerData.getSelectedVariantData().getProductCode()).isEqualTo("P1000_Black");
        assertThat(listerData.isOtherVariantsAvailable()).isTrue();


    }



    @Test
    public void testPopulateWithSelectedVariantNotEmptyOnlySellableVariantForBaseProduct() {
        final TargetProductListerData listerData = new TargetProductListerData();
        when(targetProductDataUrlResolver.resolve("peppa pig",
                "P1000_purple_s")).thenReturn("/p/P1000_purple_s");
        listerData.setName("peppa pig");
        final PropertyMap map = new MPropertyMap();
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE, "P1000");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, "P1000_purple");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE, "P1000_purple_s");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY, "79");
        final MAggrERec aggrecErec = new MAggrERec();
        final MERecList list = new MERecList();
        final MERec rec = new MERec();
        populateDimensionInErec(rec, "size", "small");
        rec.setProperties(map);
        list.add(rec);
        aggrecErec.setERecs(list);
        populator.populate(aggrecErec, listerData, null);
        assertThat(listerData.getSelectedVariantData()).isNotNull();
        assertThat(listerData.getSelectedVariantData().getProductCode()).isEqualTo("P1000_purple_s");
        assertThat(listerData.getSelectedVariantData().getUrl()).isEqualTo("/p/P1000_purple_s");
        assertThat(listerData.getSelectedVariantData().isSellableVariant()).isTrue();
        assertThat(listerData.getSelectedVariantData().getSize()).isEqualTo("small");
        assertThat(listerData.isOtherVariantsAvailable()).isFalse();
    }

    @Test
    public void testPopulateWithSelectedVariantNotEmptyOnlySellableVariantColorForBaseProduct() {
        final TargetProductListerData listerData = new TargetProductListerData();
        when(targetProductDataUrlResolver.resolve("peppa pig",
                "P1000_purple")).thenReturn("/p/P1000_purple");
        listerData.setName("peppa pig");
        final PropertyMap map = new MPropertyMap();
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE, "P1000");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, "P1000_purple");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY, "79");
        final MAggrERec aggrecErec = new MAggrERec();
        final MERecList list = new MERecList();
        final MERec rec = new MERec();
        populateDimensionInErec(rec, "swatchColour", "Coral");
        rec.setProperties(map);
        list.add(rec);
        aggrecErec.setERecs(list);
        populator.populate(aggrecErec, listerData, null);
        assertThat(listerData.getSelectedVariantData()).isNotNull();
        assertThat(listerData.getSelectedVariantData().getProductCode()).isEqualTo("P1000_purple");
        assertThat(listerData.getSelectedVariantData().getUrl()).isEqualTo("/p/P1000_purple");
        assertThat(listerData.getSelectedVariantData().getColor()).isEqualTo("Coral");
        assertThat(listerData.getSelectedVariantData().isSellableVariant()).isTrue();
        assertThat(listerData.isOtherVariantsAvailable()).isFalse();

    }

    @Test
    public void testPopulateWithSelectedVariantSizeNotEmpty() {
        final TargetProductListerData listerData = new TargetProductListerData();
        when(targetProductDataUrlResolver.resolve("peppa pig",
                "P1000_Black_S")).thenReturn("/p/P1000_Black_S");
        listerData.setName("peppa pig");
        final PropertyMap map = new MPropertyMap();
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, "P1000_Black");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE, "P1000_Black_S");
        map.put("colour", "Black");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY, "79");
        final MAggrERec aggrecErec = new MAggrERec();
        final MERecList list = new MERecList();
        final MERec rec = new MERec();
        rec.setProperties(map);
        list.add(rec);
        aggrecErec.setERecs(list);
        populator.populate(aggrecErec, listerData, "P1000_Black_S");
        assertThat(listerData.getCode()).isEqualTo("P1000_Black_S");
        assertThat(listerData.getUrl()).isEqualTo("/p/P1000_Black_S");
        assertThat(listerData.getSelectedVariantData()).isNotNull();
        assertThat(listerData.getSelectedVariantData().getAvailableQty()).isEqualTo(79);
        assertThat(listerData.isOtherVariantsAvailable()).isFalse();


    }

    @Test
    public void testPopulateWithSelectedVariantEmpty() {
        final TargetProductListerData listerData = new TargetProductListerData();
        when(targetProductDataUrlResolver.resolve("peppa pig",
                "P1000_Black")).thenReturn("/p/P1000_Black");
        when(targetProductDataUrlResolver.resolve("peppa pig",
                "P1000_Black_S")).thenReturn("/p/P1000_Black_S");
        listerData.setName("peppa pig");
        final PropertyMap map = new MPropertyMap();
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, "P1000_Black");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE, "P1000_Black_S");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_COUNT, "55");

        map.put("colour", "Black");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY, "79");
        final MAggrERec aggrecErec = new MAggrERec();
        final MERecList list = new MERecList();
        final MERec rec = new MERec();
        rec.setProperties(map);
        list.add(rec);
        aggrecErec.setERecs(list);
        populator.populate(aggrecErec, listerData, null);
        assertThat(listerData.getTargetVariantProductListerData()).hasSize(1);
        assertThat(listerData.getTargetVariantProductListerData().get(0).getColourVariantCode())
                .isEqualTo("P1000_Black");
        assertThat(listerData.getTargetVariantProductListerData().get(0).getSizeVariantCount())
                .isEqualTo(55);
        assertThat(listerData.getSelectedVariantData()).isNotNull();
        assertThat(listerData.getSelectedVariantData().getProductCode()).isEqualTo("P1000_Black_S");
        assertThat(listerData.getSelectedVariantData().getUrl()).isEqualTo("/p/P1000_Black_S");
    }

    @Test
    public void testGetDimensionValuesForDimensionsWithNull() {
        final MERec rec = new MERec();
        final Map<String, String> dimKeyValues = populator.getDimensionValuesForDimensions(rec, null);
        assertThat(dimKeyValues).isNull();
    }

    @Test
    public void testGetDimensionValuesForDimensionsWithEmptyKeys() {
        final MERec rec = new MERec();
        final ArrayList<String> keys = new ArrayList<>();
        final Map<String, String> dimKeyValues = populator.getDimensionValuesForDimensions(rec, keys);
        assertThat(dimKeyValues).isNull();
    }

    @Test
    public void testGetDimensionValuesForDimensionsWithEmptyDim() {
        final MERec rec = new MERec();
        final MAssocDimLocationsList dimList = new MAssocDimLocationsList();
        final ArrayList<String> keys = new ArrayList<>();
        keys.add("colour");
        rec.setDimValues(dimList);
        final Map<String, String> dimKeyValues = populator.getDimensionValuesForDimensions(rec, keys);
        assertThat(dimKeyValues).isNull();
    }

    @Test
    public void testGetDimensionValuesForDimensionsWithValid() {
        final MERec rec = new MERec();
        final ArrayList<String> keys = new ArrayList<>();
        keys.add("colour");
        populateDimensionInErec(rec, "colour", "Blue");
        final Map<String, String> dimKeyValues = populator.getDimensionValuesForDimensions(rec, keys);
        assertThat(dimKeyValues).isNotNull();
        assertThat(dimKeyValues.get("colour")).isEqualTo("Blue");
    }

    @Test
    public void testGetDimensionValuesForDimensionsWithMoreValid() {
        final MERec rec = new MERec();
        final MAssocDimLocationsList dimList = new MAssocDimLocationsList();
        final ArrayList<String> keys = new ArrayList<>();
        keys.add("colour");
        keys.add("size");
        final MAssocDimLocations dimLocs = new MAssocDimLocations();
        final MDimLocation mDimLoc = new MDimLocation();
        final MDimVal val = new MDimVal();
        val.setDimensionName("colour");
        val.setName("Blue");
        final PropertyMap map = new MPropertyMap();
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR, "Black");
        val.setProperties(map);
        val.setId(1234);
        mDimLoc.setDimValue(val);
        dimLocs.setDimGroup(val);
        dimLocs.add(mDimLoc);
        dimList.add(0, dimLocs);
        rec.setDimValues(dimList);
        final Map<String, String> dimKeyValues = populator.getDimensionValuesForDimensions(rec, keys);
        assertThat(dimKeyValues).isNotNull();
        assertThat(dimKeyValues.get("colour")).isEqualTo("Blue");
        assertThat(dimKeyValues.get("size")).isNull();

    }

    @Test
    public void testGetAssortedProduct() {
        final TargetProductListerData listerData = preparePeppaPigListerData();
        PropertyMap map = new MPropertyMap();
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE, "P1000");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, "P1000_purple");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_ASSORTED, "1");
        final MAggrERec aggrecErec = new MAggrERec();
        final MERecList list = new MERecList();
        MERec rec = new MERec();
        rec.setProperties(map);
        list.add(rec);
        map = new MPropertyMap();
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE, "P1000");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, "P1000_pink");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_ASSORTED, "0");
        rec = new MERec();
        rec.setProperties(map);
        list.add(rec);
        aggrecErec.setERecs(list);
        populator.populate(aggrecErec, listerData, null);
        final TargetVariantProductListerData firstVariant = listerData.getTargetVariantProductListerData().get(0);
        assertThat(firstVariant).isNotNull();
        assertThat(firstVariant.getAssorted()).isTrue();
        final TargetVariantProductListerData secondVariant = listerData.getTargetVariantProductListerData().get(1);
        assertThat(secondVariant).isNotNull();
        assertThat(secondVariant.getAssorted()).isFalse();
    }

    @Test
    public void testGetAssortedProductWithSelectedVariant() {
        final TargetProductListerData listerData = preparePeppaPigListerData();
        final PropertyMap map = new MPropertyMap();
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE, "P1000");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, "P1000_purple");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_ASSORTED, "1");
        final MAggrERec aggrecErec = new MAggrERec();
        final MERecList list = new MERecList();
        final MERec rec = new MERec();
        rec.setProperties(map);
        list.add(rec);
        aggrecErec.setERecs(list);
        populator.populate(aggrecErec, listerData, "P1000_purple");
        assertThat(listerData.getSelectedVariantData()).isNotNull();
        assertThat(listerData.getSelectedVariantData().getProductCode()).isEqualTo("P1000_purple");
        assertThat(listerData.getSelectedVariantData().getUrl()).isEqualTo("/p/P1000_purple");
        assertThat(listerData.getSelectedVariantData().isAssorted()).isTrue();
    }

    @Test
    public void testGetDisplayOnlyProduct() {
        final TargetProductListerData listerData = preparePeppaPigListerData();
        PropertyMap map = new MPropertyMap();
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE, "P1000");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, "P1000_purple");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DISPLAYONLY_FLAG, "1");
        final MAggrERec aggrecErec = new MAggrERec();
        final MERecList list = new MERecList();
        MERec rec = new MERec();
        rec.setProperties(map);
        list.add(rec);
        map = new MPropertyMap();
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE, "P1000");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, "P1000_pink");
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DISPLAYONLY_FLAG, "0");
        rec = new MERec();
        rec.setProperties(map);
        list.add(rec);
        aggrecErec.setERecs(list);
        populator.populate(aggrecErec, listerData, null);
        assertThat(listerData.getTargetVariantProductListerData()).hasSize(2).onProperty("displayOnly")
                .containsExactly(Boolean.valueOf(true), Boolean.valueOf(false));
    }

    @Test
    public void testPopulateDisplayFlagAsTrueWithDisplayOnlyVariants() {
        final TargetProductListerData listerData = preparePeppaPigListerData();
        final MAggrERec aggrecErec = new MAggrERec();
        final MERecList list = new MERecList();
        list.add(prepareRecordWithDisplayOnlyFlag("P1000", "P1000_purple",
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DISPLAYONLY_FLAG, "1"));
        list.add(prepareRecordWithDisplayOnlyFlag("P1000", "P1000_pink",
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DISPLAYONLY_FLAG, "1"));
        aggrecErec.setERecs(list);
        populator.populate(aggrecErec, listerData, null);
        assertThat(listerData.isDisplayOnly()).isTrue();
    }

    @Test
    public void testPopulateDisplayFlagAsFalseWithMixedVariants() {
        final TargetProductListerData listerData = preparePeppaPigListerData();
        final MAggrERec aggrecErec = new MAggrERec();
        final MERecList list = new MERecList();
        list.add(prepareRecordWithDisplayOnlyFlag("P1000", "P1000_purple",
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DISPLAYONLY_FLAG, "0"));
        list.add(prepareRecordWithDisplayOnlyFlag("P1000", "P1000_pink",
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DISPLAYONLY_FLAG, "1"));
        aggrecErec.setERecs(list);
        populator.populate(aggrecErec, listerData, "P1000_pink");
        assertThat(listerData.isDisplayOnly()).isFalse();
    }

    @Test
    public void testPopulateDisplayFlagAsFalseWithNonDisplayOnlyVariants() {
        final TargetProductListerData listerData = preparePeppaPigListerData();
        final MAggrERec aggrecErec = new MAggrERec();
        final MERecList list = new MERecList();
        list.add(prepareRecordWithDisplayOnlyFlag("P1000", "P1000_purple",
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DISPLAYONLY_FLAG, "0"));
        list.add(prepareRecordWithDisplayOnlyFlag("P1000", "P1000_pink",
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DISPLAYONLY_FLAG, "0"));
        aggrecErec.setERecs(list);
        populator.populate(aggrecErec, listerData, null);
        assertThat(listerData.isDisplayOnly()).isFalse();
    }

    @Test
    public void testPopulateOnlineStockAllVariantsOutOfStock() {
        final TargetProductListerData listerData = preparePeppaPigListerData();
        final MAggrERec aggrecErec = new MAggrERec();
        final MERecList list = new MERecList();
        list.add(prepareRecordWithDisplayOnlyFlag("P1000", "P1000_purple",
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY, "0"));
        list.add(prepareRecordWithDisplayOnlyFlag("P1000", "P1000_pink",
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY, "0"));
        aggrecErec.setERecs(list);
        populator.populate(aggrecErec, listerData, null);
        assertThat(listerData.getStock().getStockLevelStatus()).isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    @Test
    public void testPopulateOnlineStockSelectedVariantInStock() {
        final TargetProductListerData listerData = preparePeppaPigListerData();
        final MAggrERec aggrecErec = new MAggrERec();
        final MERecList list = new MERecList();
        list.add(prepareRecordWithDisplayOnlyFlag("P1000", "P1000_purple",
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY, "0"));
        list.add(prepareRecordWithDisplayOnlyFlag("P1000", "P1000_pink",
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY, "10"));
        aggrecErec.setERecs(list);
        populator.populate(aggrecErec, listerData, "P1000_pink");
        assertThat(listerData.getStock().getStockLevelStatus()).isEqualTo(StockLevelStatus.INSTOCK);
    }

    @Test
    public void testPopulateOnlineStockOtherVariantInStock() {
        final TargetProductListerData listerData = preparePeppaPigListerData();
        final MAggrERec aggrecErec = new MAggrERec();
        final MERecList list = new MERecList();
        list.add(prepareRecordWithDisplayOnlyFlag("P1000", "P1000_purple",
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY, "10"));
        list.add(prepareRecordWithDisplayOnlyFlag("P1000", "P1000_pink",
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY, "0"));
        aggrecErec.setERecs(list);
        populator.populate(aggrecErec, listerData, "P1000_pink");
        assertThat(listerData.getStock().getStockLevelStatus()).isEqualTo(StockLevelStatus.INSTOCK);
    }


    @Test
    public void testPopulateProductDisplayType() {
        final TargetProductListerData listerData = preparePeppaPigListerData();
        final MAggrERec aggrecErec = new MAggrERec();
        final MERecList list = new MERecList();
        list.add(prepareRecordWithDisplayOnlyFlag("P1000", "P1000_purple",
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_DISPLAY_TYPE, "AVAILABLE_FOR_SALE"));
        aggrecErec.setERecs(list);
        populator.populate(aggrecErec, listerData, null);
        assertThat(listerData.getTargetVariantProductListerData().size()).isEqualTo(1);
        assertThat(listerData.getTargetVariantProductListerData().get(0).getProductDisplayType()).isNotNull();
        assertThat(listerData.getTargetVariantProductListerData().get(0).getProductDisplayType()).isEqualTo(
                ProductDisplayType.AVAILABLE_FOR_SALE);
    }


    @Test
    public void testPopulateProductDisplayTypeComingSoon() {
        final TargetProductListerData listerData = preparePeppaPigListerData();
        final MAggrERec aggrecErec = new MAggrERec();
        final MERecList list = new MERecList();
        list.add(prepareRecordWithDisplayOnlyFlag("P1000", "P1000_purple",
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_DISPLAY_TYPE, "COMING_SOON"));
        aggrecErec.setERecs(list);
        populator.populate(aggrecErec, listerData, null);
        assertThat(listerData.getTargetVariantProductListerData().size()).isEqualTo(1);
        assertThat(listerData.getTargetVariantProductListerData().get(0).getProductDisplayType()).isNotNull();
        assertThat(listerData.getTargetVariantProductListerData().get(0).getProductDisplayType()).isEqualTo(
                ProductDisplayType.COMING_SOON);
    }


    @Test
    public void testPopulateNormalSalesDate() {
        final TargetProductListerData listerData = preparePeppaPigListerData();
        final MAggrERec aggrecErec = new MAggrERec();
        final MERecList list = new MERecList();
        list.add(prepareRecordWithDisplayOnlyFlag("P1000", "P1000_purple",
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_NORMAL_SALE_START_DATE_TIME, "2018-06-13 00:00:00.0"));
        aggrecErec.setERecs(list);
        populator.populate(aggrecErec, listerData, null);
        assertThat(listerData.getTargetVariantProductListerData().size()).isEqualTo(1);
        assertThat(listerData.getTargetVariantProductListerData().get(0).getNormalSaleStartDate()).isNotNull();
        assertThat(listerData.getTargetVariantProductListerData().get(0).getNormalSaleStartDate()).isEqualTo(
                TargetDateUtil.getStringAsDate("2018-06-13 00:00:00.0", "yyyy-MM-dd HH:mm:ss.S"));
    }

    private TargetProductListerData preparePeppaPigListerData() {
        final TargetProductListerData listerData = new TargetProductListerData();
        given(targetProductDataUrlResolver.resolve("peppa pig", "P1000_purple")).willReturn("/p/P1000_purple");
        given(targetProductDataUrlResolver.resolve("peppa pig", "P1000_pink")).willReturn("/p/P1000_pink");
        listerData.setName("peppa pig");
        return listerData;
    }

    private MERec prepareRecordWithDisplayOnlyFlag(final String baseProductCode, final String colourVariantCode,
            final String key, final String value) {
        final PropertyMap map;
        final MERec rec;
        map = new MPropertyMap();
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE, baseProductCode);
        map.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE, colourVariantCode);
        map.put(key, value);
        rec = new MERec();
        rec.setProperties(map);
        return rec;
    }

    private void populateDimensionInErec(final MERec rec, final String key, final String value) {
        final MAssocDimLocationsList dimList = new MAssocDimLocationsList();
        final MAssocDimLocations dimLocs = new MAssocDimLocations();
        final MDimLocation mDimLoc = new MDimLocation();
        final MDimVal val = new MDimVal();
        val.setDimensionName(key);
        val.setName(value);
        final PropertyMap map = new MPropertyMap();
        map.put(key, value);
        val.setProperties(map);
        val.setId(1234);
        mDimLoc.setDimValue(val);
        dimLocs.setDimGroup(val);
        dimLocs.add(mDimLoc);
        dimList.add(0, dimLocs);
        rec.setDimValues(dimList);
    }
}

/**
 * 
 */
package au.com.target.endeca.infront.converter.populator;

import de.hybris.bootstrap.annotations.UnitTest;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.data.EndecaProduct;
import au.com.target.tgtcore.price.data.PriceRangeInformation;
import au.com.target.tgtfacades.converters.populator.TargetProductPricePopulator;
import au.com.target.tgtfacades.product.data.TargetProductListerData;


/**
 * @author pthoma20
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductListerPriceDataPopulatorTest {

    @Mock
    private TargetProductPricePopulator targetProductPricePopulator;

    @InjectMocks
    private final TargetProductListerPriceDataPopulator targetProductListerPriceDataPopulator = new TargetProductListerPriceDataPopulator();

    @Test
    public void test() {

        final EndecaProduct source = new EndecaProduct();
        source.setMinPrice("8.3");
        source.setMaxPrice("12");
        source.setMinWasPrice("15.5");
        source.setMaxWasPrice("25");

        final TargetProductListerData targetProductListerData = new TargetProductListerData();
        final ArgumentCaptor<TargetProductListerData> targetProductListerDataCaptor = ArgumentCaptor
                .forClass(TargetProductListerData.class);
        final ArgumentCaptor<PriceRangeInformation> priceRangeInfoCaptor = ArgumentCaptor
                .forClass(PriceRangeInformation.class);
        targetProductListerPriceDataPopulator.populate(source, targetProductListerData);


        Mockito.verify(targetProductPricePopulator).populatePricesFromRangeInformation(
                targetProductListerDataCaptor.capture(),
                priceRangeInfoCaptor.capture());

        final PriceRangeInformation priceRangeInformationVerify = priceRangeInfoCaptor.getValue();
        Assert.assertEquals(priceRangeInformationVerify.getFromPrice(), Double.valueOf(8.3));
        Assert.assertEquals(priceRangeInformationVerify.getToPrice(), Double.valueOf(12));
        Assert.assertEquals(priceRangeInformationVerify.getFromWasPrice(), Double.valueOf(15.5));
        Assert.assertEquals(priceRangeInformationVerify.getToWasPrice(), Double.valueOf(25));


    }
}

/**
 * 
 */
package au.com.target.endeca.infront.parser;

import static org.fest.assertions.Assertions.assertThat;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.content.support.XmlContentItem;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

import au.com.target.endeca.infront.data.EndecaBreadCrumb;
import au.com.target.endeca.infront.data.EndecaDimension;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.endeca.infront.parser.impl.EndecaDataParserImpl;


/**
 * @author rsamuel3
 * 
 */
public final class EndecaSearchRecordsBuilderTestHelper {

    private EndecaSearchRecordsBuilderTestHelper() {
    }

    /**
     * @param endecaDataParser
     * @throws IOException
     * @throws TransformerConfigurationException
     * @throws TransformerFactoryConfigurationError
     * @throws JAXBException
     * @throws TransformerException
     */
    public static EndecaSearch buildEndecaSearchRecords(final EndecaDataParserImpl endecaDataParser,
            final String file) throws IOException,
            TransformerConfigurationException, TransformerFactoryConfigurationError, JAXBException,
            TransformerException {

        final InputStream inputStream = EndecaSearchRecordsBuilderTestHelper.class.getResourceAsStream(file);

        assertThat(inputStream).isNotNull();

        final XStream xstream = new XStream(new StaxDriver());
        final XmlContentItem contentItem = (XmlContentItem)xstream.fromXML(inputStream);

        final List<ContentItem> contentItems = new ArrayList<>();
        contentItems.add(contentItem);

        final EndecaSearch records = endecaDataParser.convertSourceToSearchObjects(contentItems);

        assertThat(records).isNotNull();

        return records;
    }

    public static List<EndecaBreadCrumb> buildEndecaBreadCrumbRecords(final EndecaDataParserImpl endecaDataParser,
            final String file) throws TransformerConfigurationException, IOException,
            TransformerFactoryConfigurationError, JAXBException, TransformerException {
        final EndecaSearch records = buildEndecaSearchRecords(endecaDataParser, file);
        final List<EndecaBreadCrumb> breadCrumbs = records.getBreadCrumb();
        assertThat(breadCrumbs).isNotNull().isNotEmpty();
        return breadCrumbs;

    }

    public static List<EndecaDimension> buildEndecaDimensionRecords(final EndecaDataParserImpl endecaDataParser,
            final String file) throws TransformerConfigurationException, IOException,
            TransformerFactoryConfigurationError, JAXBException, TransformerException {
        final EndecaSearch records = buildEndecaSearchRecords(endecaDataParser, file);
        final List<EndecaDimension> dimensions = records.getDimension();
        assertThat(dimensions).isNotNull().isNotEmpty();
        return dimensions;
    }

}

/**
 * 
 */
package au.com.target.endeca.infront.parser.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.assembler.event.request.RequestEvent;

import au.com.target.endeca.infront.converter.EndecaContentItemConverter;
import au.com.target.endeca.infront.converter.impl.EndecaContentItemBreadCrumbProcessor;
import au.com.target.endeca.infront.converter.impl.EndecaContentItemContentsProcessor;
import au.com.target.endeca.infront.converter.impl.EndecaContentItemGuidedNavProcessor;
import au.com.target.endeca.infront.converter.impl.EndecaContentItemResultsListProcessor;
import au.com.target.endeca.infront.converter.impl.EndecaRedirectUrlConverterImpl;
import au.com.target.endeca.infront.converter.impl.EndecaSpellSuggestionsConverterImpl;
import au.com.target.endeca.infront.data.EndecaAncestor;
import au.com.target.endeca.infront.data.EndecaBreadCrumb;
import au.com.target.endeca.infront.data.EndecaDimension;
import au.com.target.endeca.infront.data.EndecaRecords;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.endeca.infront.parser.EndecaSearchRecordsBuilderTestHelper;


/**
 * @author rsamuel3
 * 
 */
@UnitTest
public class EndecaDataParserImplTest {

    private static final Logger LOG = Logger.getLogger(EndecaDataParserImplTest.class);

    private EndecaDataParserImpl endecaDataParser;

    @Before
    public void setUp() {
        endecaDataParser = new EndecaDataParserImpl();

        final EndecaContentItemBreadCrumbProcessor endecaContentItemBreadCrumbProcessor = new EndecaContentItemBreadCrumbProcessor();
        final EndecaContentItemGuidedNavProcessor endecaContentItemGuidedNavProcessor = new EndecaContentItemGuidedNavProcessor();
        final EndecaContentItemResultsListProcessor endecaContentItemResultsListProcessor = new EndecaContentItemResultsListProcessor();
        final EndecaContentItemContentsProcessor endecaContentItemContentsProcessor = new EndecaContentItemContentsProcessor();
        final EndecaSpellSuggestionsConverterImpl endecaSpellSuggestionsConverter = new EndecaSpellSuggestionsConverterImpl();
        final Map<String, EndecaContentItemConverter> endecaContentItemConverters = new HashMap<String, EndecaContentItemConverter>();
        final EndecaRedirectUrlConverterImpl endecaRedirectUrlConverter = new EndecaRedirectUrlConverterImpl();
        endecaContentItemContentsProcessor
                .setEndecaContentItemBreadCrumbProcessor(endecaContentItemBreadCrumbProcessor);
        endecaContentItemContentsProcessor.setEndecaContentItemGuidedNavProcessor(endecaContentItemGuidedNavProcessor);
        endecaContentItemContentsProcessor
                .setEndecaContentItemResultsListProcessor(endecaContentItemResultsListProcessor);
        endecaContentItemConverters.put("contents", endecaContentItemContentsProcessor);
        endecaContentItemConverters.put("endeca:redirect", endecaRedirectUrlConverter);
        endecaContentItemConverters.put("endeca:assemblerRequestInformation", endecaSpellSuggestionsConverter);
        endecaDataParser.setEndecaContentItemConverters(endecaContentItemConverters);
    }

    /**
     * @throws TransformerException
     * @throws JAXBException
     * @throws IOException
     */
    @Test
    public void testConversion() throws TransformerException,
            JAXBException, IOException {
        final EndecaSearch records = EndecaSearchRecordsBuilderTestHelper.buildEndecaSearchRecords(endecaDataParser,
                "/endeca/test/samplecategorycontentitem.xml");
        LOG.info("number of records :" + records.getRecords().getTotalCount());
        assertThat(records).isNotNull();


        final List<EndecaBreadCrumb> crumbs = records.getBreadCrumb();
        assertThat(crumbs).isNotNull().isNotEmpty().hasSize(1);
        final EndecaBreadCrumb crumb = crumbs.get(0);
        assertThat(crumb.getDimensionType()).isEqualTo("category");
        assertThat(crumb.getElement()).isEqualTo("Babywear");
        assertThat(crumb.getRemoveNavUrl()).isEqualTo(
                "&Nr=productDisplayFlag%3A1&Nrpp=30&Ns=inStockFlag%7C1%7C%7CproductName&viewAs=grid");
        final List<EndecaAncestor> crumbAncestors = crumb.getAncestor();
        assertThat(crumbAncestors).isNotNull().isNotEmpty().hasSize(1);
        final EndecaAncestor crumbAncestor = crumbAncestors.get(0);
        assertThat(crumbAncestor.getCode()).isEqualTo("W93741");
        assertThat(crumbAncestor.getElement()).isEqualTo("Baby");
        assertThat(crumbAncestor.getNavigationState()).isEqualTo(
                "?N=26ui&Nr=productDisplayFlag%3A1&Nrpp=30&Ns=inStockFlag%7C1%7C%7CproductName&viewAs=grid");


        final List<EndecaDimension> dimensions = records.getDimension();
        assertThat(dimensions).isNotNull().isNotEmpty().hasSize(4);
        final EndecaDimension catDimension = dimensions.get(0);
        assertThat(catDimension.getName()).isEqualTo("Category");
        final List<EndecaDimension> refinements = catDimension.getRefinement();
        assertThat(refinements).isNotNull().isNotEmpty().hasSize(10);
        final EndecaDimension refinement = refinements.get(0);
        assertThat(refinement.getCount()).isNotNull().isNotEmpty().isEqualTo("1");
        assertThat(refinement.getName()).isNotNull().isNotEmpty().isEqualTo("AFL");
        assertThat(refinement.getNavigationState()).isNotNull().isNotEmpty()
                .isEqualTo("?N=26uz&Nr=productDisplayFlag%3A1&Nrpp=30&Ns=inStockFlag%7C1%7C%7CproductName&viewAs=grid");

        final EndecaDimension colourDimension = dimensions.get(1);
        assertThat(colourDimension.getName()).isEqualTo("Colour");

        final EndecaRecords resultsList = records.getRecords();
        assertThat(resultsList).isNotNull();
        assertThat(resultsList.getTotalCount()).isNotNull().isNotEmpty().isEqualTo("13");
        assertThat(resultsList.getRecordsPerPage()).isNotNull().isNotEmpty().isEqualTo("30");
        assertThat(resultsList.getOption()).isNotNull().isNotEmpty().hasSize(8);
        assertThat(resultsList.getProduct()).isNotNull().isNotEmpty().hasSize(13);

        assertThat(resultsList.getProduct().get(0).getCode()).isNotNull().isNotEmpty().isEqualTo("P1008");
        assertThat(resultsList.getProduct().get(0).getBrand()).isNotNull().isNotEmpty().isEqualTo("Target");
        assertThat(resultsList.getProduct().get(0).getColour()).isNullOrEmpty();
        assertThat(resultsList.getProduct().get(0).getName()).isNotNull().isEqualTo("Baby AFL product");
        assertThat(resultsList.getProduct().get(0).getProduct()).isNotNull().hasSize(9);
        assertThat(resultsList.getProduct().get(0).getProduct().get(0).getColour()).isNotNull()
                .isEqualTo("Blue");
        assertThat(resultsList.getProduct().get(0).getProduct().get(0).getSwatchColour()).isNotNull()
                .isEqualTo("Blue");
    }

    /**
     * Test whether spell suggestions and auto correct can be made available in EndecaSearch record.
     * 
     * @throws JAXBException
     * @throws IOException
     * @throws TransformerException
     */
    @Test
    public void testSpellSuggetionsAndAutoCorrect() throws TransformerException, IOException, JAXBException {
        final ContentItem contentItem = Mockito.mock(ContentItem.class);
        final RequestEvent event = Mockito.mock(RequestEvent.class);
        final EndecaContentItemConverter spellingSuggestionConverter = new EndecaSpellSuggestionsConverterImpl();

        final Map<String, EndecaContentItemConverter> endecaContentItemConverters = new HashMap<String, EndecaContentItemConverter>();
        endecaContentItemConverters.put("endeca:assemblerRequestInformation", spellingSuggestionConverter);
        endecaDataParser.setEndecaContentItemConverters(endecaContentItemConverters);

        final List<ContentItem> contentItems = new ArrayList<>();
        contentItems.add(contentItem);
        final String autoCorrectedTerm = "buy";
        final List<String> suggestions = new ArrayList<>();
        suggestions.add("boot");
        suggestions.add("body");
        suggestions.add("boys");
        given(Boolean.valueOf(contentItem.containsKey("endeca:assemblerRequestInformation"))).willReturn(
                Boolean.TRUE);
        given(contentItem.get("endeca:assemblerRequestInformation")).willReturn(event);
        given(Boolean.valueOf(event.containsKey("endeca:didYouMeanTo"))).willReturn(
                Boolean.TRUE);
        given(event.get("endeca:didYouMeanTo")).willReturn(suggestions);
        given(Boolean.valueOf(event.containsKey("endeca:autocorrectTo"))).willReturn(
                Boolean.TRUE);
        given(event.get("endeca:autocorrectTo")).willReturn(autoCorrectedTerm);
        final EndecaSearch record = endecaDataParser.convertSourceToSearchObjects(contentItems);
        assertThat(record).isNotNull();
        assertThat(record.getAutoCorrectedTerm()).isEqualTo(autoCorrectedTerm);
        assertThat(record.getSpellingSuggestions().size()).isEqualTo(suggestions.size());
        assertThat(record.getSpellingSuggestions()).isEqualTo(suggestions);
    }

}

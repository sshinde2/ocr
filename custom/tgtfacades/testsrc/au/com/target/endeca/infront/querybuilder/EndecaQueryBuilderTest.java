/**
 * 
 */
package au.com.target.endeca.infront.querybuilder;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;

import au.com.target.endeca.infront.data.EndecaSearchStateData;


/**
 * Test class for EndecaQueryBuilder.
 * 
 * @author jjayawa1
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EndecaQueryBuilderTest {

    @Mock
    private EndecaSearchStateData endecaSearchStateData;

    private final EndecaQueryBuilder queryBuilder = new EndecaQueryBuilder();

    @Test
    public void buildRecordQueryWithMultipleProductsAndNoCategory() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P1000");
        productCodes.add("P1001");
        productCodes.add("P1002");
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSearchField("productCode");
        final String q = queryBuilder.buildProductRecordQuery(searchStateData);
        Assert.assertNotNull(q);
        Assert.assertEquals("AND(productDisplayFlag:1,OR(productCode:P1000,productCode:P1001,productCode:P1002))", q);
    }

    @Test
    public void buildRecordQueryWithMultipleProductsAndCategory() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> productCodes = new ArrayList<>();
        final List<String> categoryCodes = new ArrayList<>();
        productCodes.add("P1000");
        productCodes.add("P1001");
        productCodes.add("P1002");
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSearchField("productCode");
        categoryCodes.add("cat123");
        searchStateData.setCategoryCodes(categoryCodes);
        final String q = queryBuilder.buildProductRecordQuery(searchStateData);
        Assert.assertNotNull(q);
        Assert.assertEquals(
                "AND(productDisplayFlag:1,OR(productCode:P1000,productCode:P1001,productCode:P1002,cat123))", q);
    }

    @Test
    public void buildRecordQueryWithMultipleProductsAndCategoryAndBrand() {

        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> productCodes = new ArrayList<>();
        final List<String> categoryCodes = new ArrayList<>();
        productCodes.add("P1000");
        productCodes.add("P1001");
        categoryCodes.add("cat123");
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSearchField("productCode");
        searchStateData.setBrandCode("D100");
        searchStateData.setCategoryCodes(categoryCodes);
        final String q = queryBuilder.buildProductRecordQuery(searchStateData);
        Assert.assertNotNull(q);
        Assert.assertEquals(
                "AND(productDisplayFlag:1,OR(productCode:P1000,productCode:P1001,AND(D100,cat123)))",
                q);

    }

    @Test
    public void buildRecordQueryWithMultipleProductsAndMultipleCategoryAndBrand() {

        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> productCodes = new ArrayList<>();
        final List<String> categoryCodes = new ArrayList<>();
        productCodes.add("P1000");
        productCodes.add("P1001");
        categoryCodes.add("cat123");
        categoryCodes.add("cat456");
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSearchField("productCode");
        searchStateData.setBrandCode("D100");
        searchStateData.setCategoryCodes(categoryCodes);
        final String q = queryBuilder.buildProductRecordQuery(searchStateData);
        Assert.assertNotNull(q);
        Assert.assertEquals(
                "AND(productDisplayFlag:1,OR(productCode:P1000,productCode:P1001,AND(D100,OR(cat123,cat456))))",
                q);

    }


    @Test
    public void buildRecordQueryWithMultipleProductsAndEmptyCategoryAndBrand() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> productCodes = new ArrayList<>();
        final List<String> categoryCodes = new ArrayList<>();
        productCodes.add("P1000");
        productCodes.add("P1001");
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSearchField("productCode");
        searchStateData.setBrandCode("D100");
        searchStateData.setCategoryCodes(categoryCodes);
        final String q = queryBuilder.buildProductRecordQuery(searchStateData);
        Assert.assertNotNull(q);

        Assert.assertEquals(
                "AND(productDisplayFlag:1,OR(productCode:P1000,productCode:P1001,D100))",
                q);
    }


    @Test
    public void buildRecordQueryWithMultipleProductsAndNullCategoryAndBrand() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P1000");
        productCodes.add("P1001");
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSearchField("productCode");
        searchStateData.setBrandCode("D100");
        searchStateData.setCategoryCodes(null);
        final String q = queryBuilder.buildProductRecordQuery(searchStateData);
        Assert.assertNotNull(q);

        Assert.assertEquals(
                "AND(productDisplayFlag:1,OR(productCode:P1000,productCode:P1001,D100))",
                q);
    }

    @Test
    public void buildRecordQueryWithMultipleProductsAndCategoriesAndBrand() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> productCodes = new ArrayList<>();
        final List<String> categoryCodes = new ArrayList<>();
        productCodes.add("P1000");
        productCodes.add("P1001");
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSearchField("productCode");
        categoryCodes.add("D200");
        categoryCodes.add("D300");
        searchStateData.setBrandCode("D100");
        searchStateData.setCategoryCodes(categoryCodes);
        final String q = queryBuilder.buildProductRecordQuery(searchStateData);
        Assert.assertNotNull(q);
        Assert.assertEquals(
                "AND(productDisplayFlag:1,OR(productCode:P1000,productCode:P1001,AND(D100,OR(D200,D300))))",
                q);
    }

    @Test
    public void buildRecordQueryWithNoProductsAndOneCategory() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> categoryCodes = new ArrayList<>();
        categoryCodes.add("cat123");
        searchStateData.setCategoryCodes(categoryCodes);
        final String q = queryBuilder.buildProductRecordQuery(searchStateData);
        Assert.assertNotNull(q);
        Assert.assertEquals("AND(productDisplayFlag:1,cat123)", q);
    }

    @Test
    public void buildRecordQueryWithNoProductsAndMultipleCategory() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> categoryCodes = new ArrayList<>();
        categoryCodes.add("cat123");
        categoryCodes.add("cat456");
        searchStateData.setCategoryCodes(categoryCodes);
        final String q = queryBuilder.buildProductRecordQuery(searchStateData);
        Assert.assertNotNull(q);
        Assert.assertEquals("AND(productDisplayFlag:1,OR(cat123,cat456))", q);
    }

    @Test
    public void buildRecordQueryWithNoProductsAndMultipleCategoryBrand() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> categoryCodes = new ArrayList<>();
        categoryCodes.add("cat123");
        categoryCodes.add("cat456");
        searchStateData.setBrandCode("D100");
        searchStateData.setCategoryCodes(categoryCodes);
        final String q = queryBuilder.buildProductRecordQuery(searchStateData);
        Assert.assertNotNull(q);
        Assert.assertEquals("AND(productDisplayFlag:1,AND(D100,OR(cat123,cat456)))", q);
    }

    @Test
    public void buildRecordQueryWithNoProductsNoCategoryBrand() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        searchStateData.setBrandCode("D100");
        final String q = queryBuilder.buildProductRecordQuery(searchStateData);
        Assert.assertNotNull(q);
        Assert.assertEquals("AND(productDisplayFlag:1,D100)", q);
    }


    @Test
    public void buildRecordQueryWithProductsAndMultipleCategory() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> productCodes = new ArrayList<>();
        final List<String> categoryCodes = new ArrayList<>();
        productCodes.add("P1000");
        productCodes.add("P1001");
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSearchField("productCode");
        categoryCodes.add("cat123");
        categoryCodes.add("cat456");
        searchStateData.setCategoryCodes(categoryCodes);
        final String q = queryBuilder.buildProductRecordQuery(searchStateData);
        Assert.assertNotNull(q);
        Assert.assertEquals(
                "AND(productDisplayFlag:1,OR(productCode:P1000,productCode:P1001,OR(cat123,cat456)))",
                q);
    }

    @Test
    public void buildRecordQueryWithSingleProductAndNoCategory() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P1000");
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSearchField("productCode");
        final String q = queryBuilder.buildProductRecordQuery(searchStateData);
        Assert.assertNotNull(q);
        Assert.assertEquals("AND(productDisplayFlag:1,OR(productCode:P1000))", q);
    }

    @Test
    public void buildRecordQueryWithSingleProductAndCategory() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> productCodes = new ArrayList<>();
        final List<String> categoryCodes = new ArrayList<>();
        productCodes.add("P1000");
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSearchField("productCode");
        categoryCodes.add("cat123");
        searchStateData.setCategoryCodes(categoryCodes);
        final String q = queryBuilder.buildProductRecordQuery(searchStateData);
        Assert.assertNotNull(q);
        Assert.assertEquals("AND(productDisplayFlag:1,OR(productCode:P1000,cat123))", q);
    }

    @Ignore
    @Test
    public void buildSortQueryWithMultipleProductsAndDefaultSort() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P1000");
        productCodes.add("P1001");
        productCodes.add("P1002");
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSearchField("productCode");
        final String q = queryBuilder.buildSortQuery(searchStateData);
        Assert.assertNotNull(q);
        Assert.assertEquals(
                "Endeca.stratify(collection()/record[productCode=\"P1000\"],collection()/record[productCode=\"P1001\"],collection()/record[productCode=\"P1002\"])inStockFlag|1",
                q);
    }

    @Ignore
    @Test
    public void buildSortQueryWithMultipleProductsAndNonDefaultSort() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P1000");
        productCodes.add("P1001");
        productCodes.add("P1002");
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSearchField("productCode");
        final Map<String, Integer> sortMap = new HashMap<String, Integer>();
        sortMap.put("name-asc", Integer.valueOf(1));
        searchStateData.setSortStateMap(sortMap);
        final String q = queryBuilder.buildSortQuery(searchStateData);
        Assert.assertNotNull(q);
        Assert.assertEquals(
                "Endeca.stratify(collection()/record[productCode=\"P1000\"],collection()/record[productCode=\"P1001\"],collection()/record[productCode=\"P1002\"])inStockFlag|1",
                q);
    }

    @Ignore
    @Test
    public void buildSortQueryWithSingleProductAndNonDefaultSort() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P1000");
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSearchField("productCode");
        final Map<String, Integer> sortMap = new HashMap<String, Integer>();
        sortMap.put("name-asc", Integer.valueOf(1));
        searchStateData.setSortStateMap(sortMap);

        final String q = queryBuilder.buildSortQuery(searchStateData);
        Assert.assertNotNull(q);
        Assert.assertEquals(
                "Endeca.stratify(collection()/record[productCode=\"P1000\"])inStockFlag|1", q);
    }

    @Test
    public void buildQueryWithNullSearchState() {
        final String q = queryBuilder.buildProductRecordQuery(null);
        Assert.assertNotNull(q);
    }

    @Test
    public void buildQueryWithEmptySearchState() {
        final String q = queryBuilder.buildProductRecordQuery(new EndecaSearchStateData());
        Assert.assertNotNull(q);
    }


    @Test
    public void buildRecordQueryWithMultipleProductsAndSkipStockFilter() {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P1000");
        productCodes.add("P1001");
        productCodes.add("P1002");
        searchStateData.setSkipInStockFilter(true);
        searchStateData.setSearchTerm(productCodes);
        searchStateData.setSearchField("productCode");
        final String q = queryBuilder.buildProductRecordQuery(searchStateData);
        Assert.assertNotNull(q);
        Assert.assertFalse(q.contains("productDisplayFlag"));
    }

    @Test
    public void buildYouMayAlsoLikeSortQueryWithSizeGroup() {
        populateEndecaSearchData(true);
        final String sortQuery = queryBuilder.buildYouMayAlsoLikeSortQuery(endecaSearchStateData);
        assertThat(sortQuery).isNotNull();
        assertThat(sortQuery).isEqualTo(
                "Endeca.stratify(collection()/record[sizeGroup=\"babywear\"])||onlineDate|1");

    }

    @Test
    public void buildYouMayAlsoLikeSortQueryWithoutSizeGroup() {
        populateEndecaSearchData(false);
        final String sortQuery = queryBuilder.buildYouMayAlsoLikeSortQuery(endecaSearchStateData);
        assertThat(sortQuery).isNotNull();
        assertThat(sortQuery).isEqualTo("||onlineDate|1");
    }

    @Test
    public void buildYouMayAlsoLikeSortInStockFilterQuery() {
        populateEndecaSearchData(false);
        final String sortQuery = queryBuilder.buildRecordFilterInStockProduct();
        assertThat(sortQuery).isNotNull();
        assertThat(sortQuery).isEqualTo("AND(inStockFlag:1)");
    }

    /**
     * 
     */
    private void populateEndecaSearchData(final boolean isSizeAvailable) {

        given(endecaSearchStateData.getCategoryCodes()).willReturn(ImmutableList.of("W1234"));
        if (isSizeAvailable) {
            given(endecaSearchStateData.getSizeGroup()).willReturn("babywear");
        }
    }
}

package au.com.target.endeca.infront.querybuilder;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.endeca.navigation.UrlENEQuery;


@RunWith(MockitoJUnitRunner.class)
public class TargetDealProductsQueryBuilderTest {
    private static final String FIELD_LIST = "productCode|productName|colourVariantCode|sizeVariantCode|max_wasPrice|min_wasPrice|max_price|min_price|reviewAvgRating|imgLarge|imgThumb";

    @Mock
    private Configuration mockConfiguration;

    @Mock
    private ConfigurationService mockConfigurationService;

    @InjectMocks
    private final TargetDealProductsQueryBuilder targetDealProductsQueryBuilder = new TargetDealProductsQueryBuilder();

    @Before
    public void setUp() {
        given(mockConfigurationService.getConfiguration()).willReturn(mockConfiguration);
        given(mockConfigurationService.getConfiguration().getString("tgtpublicws.dealProducts.selection.fieldlist"))
                .willReturn(FIELD_LIST);
    }

    @Test
    public void testGetQueryForFetchingAllProductsForDealCategoryWithNullDealCategoryDimId() throws Exception {
        final UrlENEQuery result = targetDealProductsQueryBuilder.getQueryForFetchingAllProductsForDealCategory(null);

        assertThat(result).isNull();
    }

    @Test
    public void testGetQueryForFetchingAllProductsForDealCategoryWithBlankDealCategoryDimId() throws Exception {
        final UrlENEQuery result = targetDealProductsQueryBuilder.getQueryForFetchingAllProductsForDealCategory("");

        assertThat(result).isNull();
    }

    @Test
    public void testGetQueryForFetchingAllProductsForDealCategory() throws Exception {
        final String dealCategoryDimensionId = "1928307845";

        final UrlENEQuery result = targetDealProductsQueryBuilder
                .getQueryForFetchingAllProductsForDealCategory(dealCategoryDimensionId);

        assertThat(result.getNavRecordFilter()).isEqualTo("productDisplayFlag:1");
        assertThat(result.getNavDescriptors()).containsExactly(Long.valueOf(dealCategoryDimensionId));
        assertThat(result.getNavNumAggrERecs()).isEqualTo(1000);
        assertThat(result.getNavERecsPerAggrERec()).isEqualTo(1);
        assertThat(result.getNavMerchRuleFilter()).isEqualTo("endeca.internal.nonexistent");
        assertThat(result.getSelection()).containsExactly((Object[])StringUtils.split(FIELD_LIST, '|'));
        assertThat(result.getNavRollupKey()).isEqualTo("productCode");
    }
}

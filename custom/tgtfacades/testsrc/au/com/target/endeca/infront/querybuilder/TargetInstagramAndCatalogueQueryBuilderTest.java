/**
 * 
 */
package au.com.target.endeca.infront.querybuilder;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.configuration.BaseConfiguration;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;

import com.endeca.navigation.UrlENEQuery;
import com.endeca.navigation.UrlENEQueryParseException;


/**
 * @author pthoma20
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TargetInstagramAndCatalogueQueryBuilderTest {

    @Mock
    private ConfigurationService configurationService;

    private final String fields = "productCode|productName|colourVariantCode|sizeVariantCode|max_wasPrice|min_wasPrice|max_price|min_price|reviewAvgRating|imgLarge|imgThumb";

    @InjectMocks
    private final TargetInstagramAndCatalogueQueryBuilder targetInstagramAndCatalogueQueryBuilder = new TargetInstagramAndCatalogueQueryBuilder();

    @Test
    public void testCreateQueryWhenProductCodeContainsBaseProduct() throws UrlENEQueryParseException {
        final List<String> recFilterOptions = new ArrayList<>();
        recFilterOptions.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE);
        recFilterOptions.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setRecordFilterOptions(recFilterOptions);
        endecaSearchStateData.setNpSearchState(EndecaConstants.ENDECA_NP_1);
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P1000");
        productCodes.add("12334");
        endecaSearchStateData.setProductCodes(productCodes);
        final BaseConfiguration configuration = Mockito.mock(BaseConfiguration.class);
        BDDMockito.given(configurationService.getConfiguration()).willReturn(configuration);
        BDDMockito.given(configuration.getString("tgtpublicws.instagram.catalogues.selection.fieldlist"))
                .willReturn(fields);
        final UrlENEQuery urlEneQUery = targetInstagramAndCatalogueQueryBuilder
                .getQueryForFetchingAllProductsForInstagram(endecaSearchStateData);
        Assertions.assertThat(urlEneQUery.getNavRecordFilter())
                .isEqualTo("OR(productCode:P1000,colourVariantCode:P1000,productCode:12334,colourVariantCode:12334)");
        Assertions.assertThat(urlEneQUery.getNavRollupKey())
                .isEqualTo("productCode");
        Assertions.assertThat(urlEneQUery.getNavNumERecs())
                .isEqualTo(2);
    }

    @Test
    public void testCreateQueryWhenNoProductsPresent() throws UrlENEQueryParseException {

        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setNpSearchState(EndecaConstants.ENDECA_NP_1);
        final List<String> productCodes = new ArrayList<>();
        endecaSearchStateData.setProductCodes(productCodes);
        final BaseConfiguration configuration = Mockito.mock(BaseConfiguration.class);
        BDDMockito.given(configurationService.getConfiguration()).willReturn(configuration);
        BDDMockito.given(configuration.getString("tgtpublicws.instagram.catalogues.selection.fieldlist"))
                .willReturn(fields);
        final UrlENEQuery urlEneQUery = targetInstagramAndCatalogueQueryBuilder
                .getQueryForFetchingAllProductsForInstagram(endecaSearchStateData);
        Assertions.assertThat(urlEneQUery)
                .isNull();
    }

    @Test
    public void testBuildRecordFilterForProductsWithMultiplePrds() {
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P1000");
        productCodes.add("P2000");
        final List<String> recFilterOptions = new ArrayList<>();
        recFilterOptions.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE);
        recFilterOptions.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        final String recFilter = targetInstagramAndCatalogueQueryBuilder.buildRecordFilter(productCodes,
                recFilterOptions);
        Assert.assertEquals("OR(productCode:P1000,colourVariantCode:P1000,productCode:P2000,colourVariantCode:P2000)",
                recFilter);

    }

    @Test
    public void testBuildRecordFilterForProductsWithSinglePrdAndMultipleOption() {
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P2000");
        final List<String> recFilterOptions = new ArrayList<>();
        recFilterOptions.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE);
        recFilterOptions.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        final String recFilter = targetInstagramAndCatalogueQueryBuilder.buildRecordFilter(productCodes,
                recFilterOptions);
        Assert.assertEquals("OR(productCode:P2000,colourVariantCode:P2000)",
                recFilter);

    }

    @Test
    public void testBuildRecordFilterForProductsWithSinglePrdAndSingleOption() {
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P2000");
        final List<String> recFilterOptions = new ArrayList<>();
        recFilterOptions.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        final String recFilter = targetInstagramAndCatalogueQueryBuilder.buildRecordFilter(productCodes,
                recFilterOptions);
        Assert.assertEquals("OR(colourVariantCode:P2000)",
                recFilter);

    }

    @Test
    public void testBuildRecordFilterForProductsWithMultiplePrdAndSingleOption() {
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P2000");
        productCodes.add("P1000");
        final List<String> recFilterOptions = new ArrayList<>();
        recFilterOptions.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        final String recFilter = targetInstagramAndCatalogueQueryBuilder.buildRecordFilter(productCodes,
                recFilterOptions);
        Assert.assertEquals("OR(colourVariantCode:P2000,colourVariantCode:P1000)",
                recFilter);

    }


}

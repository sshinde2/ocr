/**
 * 
 */
package au.com.target.endeca.infront.querybuilder;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.util.EndecaQueryConnecton;

import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.HttpENEConnection;
import com.endeca.navigation.UrlENEQuery;


/**
 * @author mjanarth
 *
 */
public class TargetProductDetailPageEndecaQueryBuilderTest {


    @Mock
    private EndecaQueryConnecton endecaQueryConnection;

    @Mock
    private HttpENEConnection httpENEConnection;

    @Mock
    private ConfigurationService configurationService;


    @Mock
    private Configuration config;

    @Mock
    private ENEQueryResults queryResults;


    @InjectMocks
    private final TargetProductDetailPageEndecaQueryBuilder queryBuilder = new TargetProductDetailPageEndecaQueryBuilder();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        given(configurationService.getConfiguration()).willReturn(config);
    }


    @Test
    public void testGetQueryResultsEmptyProduct() throws Exception {
        final ArgumentCaptor<UrlENEQuery> query = ArgumentCaptor
                .forClass(UrlENEQuery.class);
        final List<String> recFilterOptions = new ArrayList<>();
        recFilterOptions.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE);
        recFilterOptions.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setRecordFilterOptions(recFilterOptions);
        endecaSearchStateData.setNpSearchState(EndecaConstants.ENDECA_NP_1);
        final List<String> productCodes = new ArrayList<>();
        endecaSearchStateData.setSearchTerm(productCodes);
        given(endecaQueryConnection.getConection()).willReturn(httpENEConnection);
        given(httpENEConnection.query(query.capture())).willReturn(queryResults);
        final ENEQueryResults results = queryBuilder.getQueryResults(endecaSearchStateData, productCodes.size());
        assertThat(results).isNull();

    }

    @Test
    public void testGetQueryResults() throws Exception {
        final ArgumentCaptor<UrlENEQuery> query = ArgumentCaptor
                .forClass(UrlENEQuery.class);
        final List<String> recFilterOptions = new ArrayList<>();
        recFilterOptions.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE);
        recFilterOptions.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setRecordFilterOptions(recFilterOptions);
        endecaSearchStateData.setNpSearchState(EndecaConstants.ENDECA_NP_1);
        endecaSearchStateData.setNuRollupField(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P1000");
        productCodes.add("12334");
        endecaSearchStateData.setSearchTerm(productCodes);
        given(endecaQueryConnection.getConection()).willReturn(httpENEConnection);
        given(httpENEConnection.query(query.capture())).willReturn(queryResults);
        final ENEQueryResults results = queryBuilder.getQueryResults(endecaSearchStateData, productCodes.size());
        final UrlENEQuery expectedQuery = query.getValue();
        assertThat(results).isEqualTo(queryResults);
        verify(httpENEConnection).query(query.capture());
        assertThat(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE).isEqualTo(
                expectedQuery.getNavRollupKey());
        assertThat("OR(sizeVariantCode:P1000,colourVariantCode:P1000,sizeVariantCode:12334,colourVariantCode:12334)")
                .isEqualTo(expectedQuery.getNavRecordFilter());
        assertThat(expectedQuery.getNavNumERecs())
                .isEqualTo(2);
    }

    @Test(expected = TargetEndecaException.class)
    public void testGetQueryResultsException() throws Exception {
        final EndecaSearchStateData endecaSearchStateData = mock(EndecaSearchStateData.class);
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P1000");
        productCodes.add("12334");
        given(endecaSearchStateData.getSearchTerm()).willReturn(productCodes);
        given(endecaSearchStateData.getRecordFilterOptions()).willReturn(productCodes);
        given(endecaSearchStateData.getNpSearchState()).willReturn("1");
        given(endecaSearchStateData.getFieldListConfig()).willReturn("1");
        given(endecaQueryConnection.getConection()).willThrow(new TargetEndecaException());
        queryBuilder.getQueryResults(endecaSearchStateData, 1);
    }
}

/**
 * 
 */
package au.com.target.endeca.infront.transform;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.transform.impl.TargetEndecaURLHelperImpl;
import au.com.target.endeca.infront.util.TargetEndecaURLUtil;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.product.BrandService;
import au.com.target.tgtfacades.category.TargetCategoryFacade;
import au.com.target.tgtfacades.category.data.TargetCategoryData;
import au.com.target.tgtfacades.product.data.BrandData;



/**
 * @author smudumba
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetEndecaURLHelperImplTest {

    private static final String INVALID_URL = "N=10%&C|&Ns=test";
    private static final String VALID_TARGET_URL = "N=638749722&page=10&Nrpp=10&text=10";
    private static final String EXPECTED_ENDECA_RESULT = "N=638749722&No=10&Nrpp=10&Ntt=10";

    private static final String ENDECA_URL = "N=638749722&No=10&Nrpp=10&Ntt=10&Ns=productName";
    private static final String EXPECTED_TARGET_RESULT = "N=638749722&page=10&Nrpp=10&text=10&Ns=productName";
    private static final int ITEMS_PER_PAGE = 30;
    private static final int PAGE = 10;
    private static final String NAV_STATE_STRING = "638749722";

    private static final String SORT_TERM = "test";
    private static final String VIEW_AS = "viewtest";
    private static final String REQUEST_URL = "urlTest";
    @InjectMocks
    @Spy
    private final TargetEndecaURLHelperImpl targetEndecaURLHelper = new TargetEndecaURLHelperImpl();
    private TargetEndecaURLUtil targetEndecaURLUtil;
    private List<String> navStateList;

    @Mock
    private TargetCategoryFacade targetCategoryFacade;
    @Mock
    private BrandService brandService;
    @Mock
    private Converter<BrandModel, BrandData> targetBrandConverter;



    @Before
    public void setUp() {
        //simiulate name value pairs from for tgt and Endeca
        final Map<String, String> tgtendMap = new HashMap<String, String>();
        tgtendMap.put("N", "N");
        tgtendMap.put("viewAs", "viewAs");
        tgtendMap.put("page", "No");
        tgtendMap.put("Nrpp", "Nrpp");
        tgtendMap.put("Ns", "Ns");
        tgtendMap.put("text", "Ntt");

        targetEndecaURLUtil = new TargetEndecaURLUtil(tgtendMap);
        //set the utility to the helper
        targetEndecaURLHelper.setTargetEndecaURLUtil(targetEndecaURLUtil);
        navStateList = new ArrayList<>();
        navStateList.add(NAV_STATE_STRING);

    }


    /**
     * Test method for
     * {@link au.com.target.endeca.infront.transform.impl.TargetEndecaURLHelperImpl#getTargetToEndecaURL(java.lang.String)}
     */
    @Test
    public void testGetTagetToEndecaURL() {
        final String result = targetEndecaURLHelper.getTargetToEndecaURL(VALID_TARGET_URL);
        //Valid URL
        Assert.assertEquals(EXPECTED_ENDECA_RESULT, result);
    }

    /**
     * Test method for
     * {@link au.com.target.endeca.infront.transform.impl.TargetEndecaURLHelperImpl#getEndecaToTargetURL(java.lang.String)}
     * .
     */
    @Test
    public void testGetEndecaToTargetURL() {
        final String result = targetEndecaURLHelper.getEndecaToTargetURL(ENDECA_URL);
        //Valid URL
        Assert.assertEquals(EXPECTED_TARGET_RESULT, result);
    }

    /**
     * Test method for
     */
    @Test
    public void testBuildEndecaSearchStateDataPageSizeListOfStringStringInt() {
        final String categoryCode = "testCategoryCode";
        final String brandCode = "testBrandCode";
        final BrandModel brand = mock(BrandModel.class);
        given(brandService.getBrandForFuzzyName(brandCode, false)).willReturn(brand);
        final List<String> searchTerm = new ArrayList<>();
        searchTerm.add("serachTerm");
        final EndecaSearchStateData result = targetEndecaURLHelper.buildEndecaSearchStateData(NAV_STATE_STRING,
                searchTerm,
                SORT_TERM, ITEMS_PER_PAGE, PAGE, VIEW_AS, REQUEST_URL, categoryCode, brandCode);
        Assert.assertEquals(ITEMS_PER_PAGE, result.getItemsPerPage());
        Assert.assertEquals(PAGE, result.getPageNumber());
        Assert.assertEquals(VIEW_AS, result.getViewAs());
        verify(targetCategoryFacade).getCategoryData(categoryCode);
        verify(brandService).getBrandForFuzzyName(brandCode, false);
        verify(targetBrandConverter).convert(brand);
    }

    @Test
    public void testBuildEndecaSearchStateDataMultipleSortTerms() {
        final List<String> searchTerm = new ArrayList<>();
        searchTerm.add("serachTerm");
        final EndecaSearchStateData result = targetEndecaURLHelper.buildEndecaSearchStateData(NAV_STATE_STRING,
                searchTerm,
                "reviewAvgRating|0||inStockFlag|1", ITEMS_PER_PAGE, PAGE, "grid", REQUEST_URL, null, null);
        Assert.assertEquals(ITEMS_PER_PAGE, result.getItemsPerPage());
        Assert.assertEquals(PAGE, result.getPageNumber());
        Assert.assertEquals(2, result.getSortStateMap().size());
        Assert.assertTrue(result.getSortStateMap().keySet().contains("reviewAvgRating"));
        Assert.assertTrue(result.getSortStateMap().keySet().contains("inStockFlag"));
        Assert.assertEquals(Integer.valueOf(0), result.getSortStateMap().get("reviewAvgRating"));
        Assert.assertEquals(Integer.valueOf(1), result.getSortStateMap().get("inStockFlag"));
    }


    @Test
    public void testGetTagetToEndecaURLwithInvalidNavigationState() {
        final String result = targetEndecaURLHelper.getTargetToEndecaURL(INVALID_URL);
        Assert.assertNull(result);
    }

    @Test
    public void testGetTagetToEndecaURLwithNull() throws Exception {
        final String result = targetEndecaURLHelper.getTargetToEndecaURL(null);
        //Valid URL
        Assert.assertEquals(null, result);
    }

    @Test
    public void testGetEndecaToTargetURLwithInvalidNavigationState() {
        final String result = targetEndecaURLHelper.getTargetToEndecaURL(INVALID_URL);
        Assert.assertNull(result);
    }

    @Test
    public void testgetValidEndecaToTargetNavigationStatewithMultipleSorts() {
        final String navigationState = "?Ns=inStockFlag%7C0%7C%7ConlineDate%7C1";
        final String result = targetEndecaURLHelper.getValidEndecaToTargetNavigationState(navigationState);
        Assert.assertNotNull(result);
        Assert.assertEquals("?Ns=inStockFlag%7C0%7C%7ConlineDate%7C1", result);
    }

    @Test
    public void testGetGetEndecaToTargetURLwithNull() throws Exception {
        final String result = targetEndecaURLHelper.getTargetToEndecaURL(null);
        //Valid URL
        Assert.assertEquals(null, result);
    }

    @Test
    public void testConstructCurrentPageUrl() throws Exception {
        final String requestUri = "/c/baby/W1234";
        String navigationState = "?No=%7Boffset%7D&Nrpp=%7BrecordsPerPage%7D";
        String result = targetEndecaURLHelper.constructCurrentPageUrl(requestUri, navigationState);
        Assert.assertEquals("/c/baby/W1234?Nrpp=%7BrecordsPerPage%7D", result);

        navigationState = "?Nrpp=%7BrecordsPerPage%7D&Ntt=jacket&No=%7Boffset%7D&Ns=inStockFlag%7C1";
        result = targetEndecaURLHelper.constructCurrentPageUrl(requestUri, navigationState);
        Assert.assertEquals("/c/baby/W1234?Nrpp=%7BrecordsPerPage%7D&text=jacket&Ns=inStockFlag%7C1", result);

    }

    @Test
    public void testGetWsUrlQueryParametersWithEndecaSearchStateDataIsNull() throws Exception {
        final String result = targetEndecaURLHelper.getWsUrlQueryParameters(null);
        assertThat(result).isEqualTo(StringUtils.EMPTY);
    }

    @Test
    public void testGetWsUrlQueryParametersWithBrand() throws Exception {
        final EndecaSearchStateData searchState = new EndecaSearchStateData();
        final BrandData brand = new BrandData();
        final String brandCode = "testBrandCode";
        brand.setCode(brandCode);
        searchState.setBrand(brand);
        final String result = targetEndecaURLHelper.getWsUrlQueryParameters(searchState);
        assertThat(result).isEqualTo(
                EndecaConstants.QUERY_PARAM_SEPERATOR_AMP + EndecaConstants.WS_URL_BRAND + brandCode);
    }

    @Test
    public void testGetWsUrlQueryParametersWithCategory() throws Exception {
        final EndecaSearchStateData searchState = new EndecaSearchStateData();
        final TargetCategoryData category = new TargetCategoryData();
        final String categoryCode = "testCode";
        category.setCode(categoryCode);
        searchState.setCategoryData(category);
        final String result = targetEndecaURLHelper.getWsUrlQueryParameters(searchState);
        assertThat(result).isEqualTo(
                EndecaConstants.QUERY_PARAM_SEPERATOR_AMP + EndecaConstants.WS_URL_CATEGORY + categoryCode);
    }

    @Test
    public void testConstructCurrentWsPageUrlWithEmptyNavigationState() throws Exception {
        final EndecaSearchStateData searchState = new EndecaSearchStateData();
        final String currentQueryParam = "&category=w123456";
        searchState.setCurrentQueryParam(currentQueryParam);
        final String result = targetEndecaURLHelper.constructCurrentWsPageUrl(searchState, null);
        assertThat(result).isEqualTo(EndecaConstants.QUERY_PARAM_SEPERATOR_QNMRK + currentQueryParam.substring(1));
    }

    @Test
    public void testConstructCurrentWsPageUrl() throws Exception {
        final EndecaSearchStateData searchState = new EndecaSearchStateData();
        final String currentQueryParam = "&category=w123456";
        searchState.setCurrentQueryParam(currentQueryParam);
        final String navigationState = "?N=1z117jcZ1z141gw&Nrpp=30&text=shirt&viewAs=grid";
        doReturn(navigationState).when(targetEndecaURLHelper).getValidEndecaToTargetNavigationState(anyString());
        final String result = targetEndecaURLHelper.constructCurrentWsPageUrl(searchState, navigationState);
        assertThat(result).isEqualTo(navigationState + currentQueryParam);
    }
}

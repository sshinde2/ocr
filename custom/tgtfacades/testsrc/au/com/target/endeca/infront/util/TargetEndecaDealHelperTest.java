/**
 * 
 */
package au.com.target.endeca.infront.util;

import de.hybris.bootstrap.annotations.UnitTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Assert;
import org.junit.Test;

import au.com.target.tgtfacades.product.data.TargetProductListerData;


/**
 * @author mjanarth
 * 
 */
@UnitTest
public class TargetEndecaDealHelperTest {


    @Test
    public void processDealMessageWithNullDates() throws ParseException {
        final String startDate = null;
        final String endDate = null;
        final String dealDescription = "Test Deal Msg";
        final TargetProductListerData productData = new TargetProductListerData();
        TargetEndecaDealDescHelper.processDealMessage(startDate, endDate, dealDescription, productData);
        Assert.assertEquals(productData.getDealDescription(), "");

    }

    @Test
    public void processDealMessageWithNullDealMsg() throws ParseException {
        final String startDate = null;
        final String endDate = "12/14/98";
        final String dealDescription = null;
        final TargetProductListerData productData = new TargetProductListerData();
        TargetEndecaDealDescHelper.processDealMessage(startDate, endDate, dealDescription, productData);
        Assert.assertEquals(productData.getDealDescription(), "");

    }

    @Test
    public void processDealMessageWithAllNull() throws ParseException {
        final String startDate = null;
        final String endDate = null;
        final String dealDescription = null;
        final TargetProductListerData productData = new TargetProductListerData();
        TargetEndecaDealDescHelper.processDealMessage(startDate, endDate, dealDescription, productData);
        Assert.assertEquals(productData.getDealDescription(), "");

    }

    @Test(expected = ParseException.class)
    public void processDealMessageWithAllInvalidDate() throws ParseException {
        final String startDate = "bbbb";
        final String endDate = "aaaa";
        final String dealDescription = null;
        final TargetProductListerData productData = new TargetProductListerData();
        TargetEndecaDealDescHelper.processDealMessage(startDate, endDate, dealDescription, productData);
        Assert.assertEquals(productData.getDealDescription(), "");

    }

    @Test
    public void processDealMessageWithValidDealMsgtest() throws ParseException {

        final Date endate = DateUtils.addMonths(new Date(), 1);

        final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy hh:mma");
        final String startDate = "May 18 2009 12:00AM ";
        final String dealDescription = "Test Deal Msg";
        final TargetProductListerData productData = new TargetProductListerData();
        TargetEndecaDealDescHelper.processDealMessage(startDate, dateFormat.format(endate), dealDescription,
                productData);
        Assert.assertEquals(productData.getDealDescription(), dealDescription);

    }

    @Test
    public void processDealMessageWithNoStartDate() throws ParseException {
        final String startDate = null;
        final String endDate = "May 18 2099 12:00AM ";
        final String dealDescription = "Test Deal Msg";
        final TargetProductListerData productData = new TargetProductListerData();
        TargetEndecaDealDescHelper.processDealMessage(startDate, endDate, dealDescription, productData);
        Assert.assertEquals(productData.getDealDescription(), StringUtils.EMPTY);

    }

    @Test
    public void processDealMessageWithNoEndDate() throws ParseException {
        final String startDate = "May 18 2099 12:00AM ";
        final String endDate = null;
        final String dealDescription = "Test Deal Msg";
        final TargetProductListerData productData = new TargetProductListerData();
        productData.setAverageRating(Double.valueOf(10.00));
        productData.setBrand("Peppa Pig");
        TargetEndecaDealDescHelper.processDealMessage(startDate, endDate, dealDescription, productData);
        Assert.assertEquals(productData.getDealDescription(), StringUtils.EMPTY);
        Assert.assertEquals(productData.getAverageRating(), Double.valueOf(10.00));
        Assert.assertEquals(productData.getBrand(), "Peppa Pig");

    }

    @Test
    public void processDealMessageForExpiredDeal() throws ParseException {
        final String startDate = "May 18 2009 12:00AM ";
        final String endDate = "May 18 2010 12:00AM ";
        final String dealDescription = "Test Deal Msg";
        final TargetProductListerData productData = new TargetProductListerData();
        TargetEndecaDealDescHelper.processDealMessage(startDate, endDate, dealDescription,
                productData);
        Assert.assertEquals(productData.getDealDescription(), "");

    }

    @Test
    public void processDealMessageForFutureDeal() throws ParseException {
        final Date startDate = DateUtils.addMonths(new Date(), 1);

        final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy hh:mma");
        final Date endDate = DateUtils.addMonths(new Date(), 4);
        final String dealDescription = "Test Deal Msg";
        final TargetProductListerData productData = new TargetProductListerData();
        TargetEndecaDealDescHelper.processDealMessage(dateFormat.format(startDate), dateFormat.format(endDate),
                dealDescription,
                productData);
        Assert.assertEquals(productData.getDealDescription(), "");

    }

}

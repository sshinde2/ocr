/**
 * 
 */
package au.com.target.endeca.infront.util;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author smudumba
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetEndecaURLUtilTest {
    @Test
    public void testgetTgtName() {
        final String endcaname = "end1";
        final String tgtname = "tgt1";

        final TargetEndecaURLUtil mockGetTgtName = Mockito.mock(TargetEndecaURLUtil.class);
        BDDMockito.given(mockGetTgtName.getTgtName(endcaname)).willReturn(tgtname);
        final String result = mockGetTgtName.getTgtName(endcaname);
        Assert.assertEquals(result, tgtname);
    }

    @Test
    public void testgetEndecaName() {
        final String endcaname = "end1";
        final String tgtname = "tgt1";

        final TargetEndecaURLUtil mockGetTgtName = Mockito.mock(TargetEndecaURLUtil.class);
        BDDMockito.given(mockGetTgtName.getEndecaName(tgtname)).willReturn(endcaname);
        final String result = mockGetTgtName.getEndecaName(tgtname);
        Assert.assertEquals(result, endcaname);
    }

}

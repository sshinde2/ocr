package au.com.target.tgtfacades.assistedcheckout.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.PrincipalData;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.StoreEmployeeModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfacades.assistedcheckout.data.StoreEmployeeData;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StoreEmployeePopulatorTest {
    @Mock
    private Converter<TargetPointOfServiceModel, TargetPointOfServiceData> mockTargetPointOfServiceConverter;

    @InjectMocks
    private final StoreEmployeePopulator storeEmployeePopulator = new StoreEmployeePopulator();

    @Test
    public void testPopulateWithNullSourceAndTarget() throws Exception {
        storeEmployeePopulator.populate(null, null);
    }

    @Test
    public void testPopulateWithNullSource() throws Exception {
        final StoreEmployeeData mockStoreEmployeeData = mock(StoreEmployeeData.class);

        storeEmployeePopulator.populate(null, mockStoreEmployeeData);

        verifyZeroInteractions(mockStoreEmployeeData);
    }

    @Test
    public void testPopulateWithNullTarget() throws Exception {
        final StoreEmployeeModel mockStoreEmployeeModel = mock(StoreEmployeeModel.class);

        storeEmployeePopulator.populate(mockStoreEmployeeModel, null);

        verifyZeroInteractions(mockStoreEmployeeModel);
    }

    @Test
    public void testPopulateWithIncorrectSourceType() throws Exception {
        final PrincipalModel mockPrincipalModel = mock(PrincipalModel.class);
        final StoreEmployeeData mockStoreEmployeeData = mock(StoreEmployeeData.class);

        storeEmployeePopulator.populate(mockPrincipalModel, mockStoreEmployeeData);

        verifyZeroInteractions(mockPrincipalModel, mockStoreEmployeeData);
    }

    @Test
    public void testPopulateWithIncorrectTargetType() throws Exception {
        final StoreEmployeeModel mockStoreEmployeeModel = mock(StoreEmployeeModel.class);
        final PrincipalData mockPrincipalData = mock(PrincipalData.class);

        storeEmployeePopulator.populate(mockStoreEmployeeModel, mockPrincipalData);

        verifyZeroInteractions(mockStoreEmployeeModel, mockPrincipalData);
    }

    public void testPopulate() {
        final String uid = "uid";
        final String name = "name";

        final StoreEmployeeModel mockStoreEmployeeModel = mock(StoreEmployeeModel.class);
        final StoreEmployeeData storeEmployeeData = new StoreEmployeeData();

        given(mockStoreEmployeeModel.getUid()).willReturn(uid);
        given(mockStoreEmployeeModel.getName()).willReturn(name);

        final TargetPointOfServiceModel mockTargetPointOfServiceModel = mock(TargetPointOfServiceModel.class);
        given(mockStoreEmployeeModel.getStore()).willReturn(mockTargetPointOfServiceModel);

        final TargetPointOfServiceData mockTargetPointOfServiceData = mock(TargetPointOfServiceData.class);
        given(mockTargetPointOfServiceConverter.convert(mockTargetPointOfServiceModel)).willReturn(
                mockTargetPointOfServiceData);

        storeEmployeePopulator.populate(mockStoreEmployeeModel, storeEmployeeData);

        assertThat(storeEmployeeData.getUid()).isEqualTo(uid);
        assertThat(storeEmployeeData.getName()).isEqualTo(name);
        assertThat(storeEmployeeData.getStore()).isEqualTo(mockTargetPointOfServiceData);
    }
}

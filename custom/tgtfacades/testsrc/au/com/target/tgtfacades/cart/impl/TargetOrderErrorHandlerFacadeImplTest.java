/**
 * 
 */
package au.com.target.tgtfacades.cart.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;
import au.com.target.tgtfacades.order.enums.TargetPlaceOrderResultEnum;


/**
 * @author smudumba
 *
 */
/**
 * Unit test for {@link TargetOrderErrorHandlerFacadeImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetOrderErrorHandlerFacadeImplTest {



    @Mock
    private ModelService modelService;


    @Mock
    private TargetTicketBusinessService targetTicketBusinessService;


    @Mock
    private OrderModel orderModel;




    /**
     * Initial Setup
     * 
     */
    @Before
    public void setUp() {
        BDDMockito.given(orderModel.getCode()).willReturn(null);
        BDDMockito.given(
                targetTicketBusinessService.createCsAgentTicket(Mockito.anyString(), Mockito.anyString(),
                        Mockito.anyString(), Mockito.any(OrderModel.class))).willReturn("1000");

    }

    /**
     * Initial Setup
     */
    @Test
    public void testSendTicketToCS() {

        final TargetOrderErrorHandlerFacadeImpl targetOrderErrorHandlerFacade = new TargetOrderErrorHandlerFacadeImpl();
        targetOrderErrorHandlerFacade.setTargetTicketBusinessService(targetTicketBusinessService);
        targetOrderErrorHandlerFacade.sendTicketToCS("1000");
        Mockito.verify(targetTicketBusinessService).createCsAgentTicket(Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(), Mockito.any(OrderModel.class));
        Mockito.verifyNoMoreInteractions(targetTicketBusinessService);


    }

    /**
     * SendTicketToCSWithNull
     */
    @Test
    public void testSendTicketToCSWithNull() {

        final TargetOrderErrorHandlerFacadeImpl targetOrderErrorHandlerFacade = new TargetOrderErrorHandlerFacadeImpl();
        targetOrderErrorHandlerFacade.setTargetTicketBusinessService(targetTicketBusinessService);
        targetOrderErrorHandlerFacade.sendTicketToCS(null);
        Mockito.verify(targetTicketBusinessService).createCsAgentTicket(Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(),
                Mockito.any(OrderModel.class));
        Mockito.verifyNoMoreInteractions(targetTicketBusinessService);


    }

    /**
     * HandleOrderCreationFailed
     */
    @Test
    public void testHandleOrderCreationFailed() {

        final TargetOrderErrorHandlerFacadeImpl targetOrderErrorHandlerFacade = new TargetOrderErrorHandlerFacadeImpl();
        targetOrderErrorHandlerFacade.setTargetTicketBusinessService(targetTicketBusinessService);
        targetOrderErrorHandlerFacade.handleOrderCreationFailed(TargetPlaceOrderResultEnum.UNKNOWN, "1000");
        Mockito.verify(targetTicketBusinessService).createCsAgentTicket(Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(), Mockito.any(OrderModel.class));
        Mockito.verifyNoMoreInteractions(targetTicketBusinessService);



    }

    /**
     * HandleOrderCreationFailedWithNull
     */
    @Test
    public void testHandleOrderCreationFailedWithNull() {

        final TargetOrderErrorHandlerFacadeImpl targetOrderErrorHandlerFacade = new TargetOrderErrorHandlerFacadeImpl();
        targetOrderErrorHandlerFacade.setTargetTicketBusinessService(targetTicketBusinessService);
        targetOrderErrorHandlerFacade.handleOrderCreationFailed(null, null);
        Mockito.verify(targetTicketBusinessService).createCsAgentTicket(Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(), Mockito.any(OrderModel.class));
        Mockito.verifyNoMoreInteractions(targetTicketBusinessService);
    }




}

package au.com.target.tgtfacades.category.impl;

import static junit.framework.Assert.assertEquals;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.category.TargetCategoryService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.category.TargetCategoryFacade;
import au.com.target.tgtfacades.category.data.TargetCategoryData;


/**
 * Test suite for {@link TargetCategoryFacadeImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCategoryFacadeImplTest {

    @Mock
    private TargetCategoryService categoryService;
    @Mock
    private Converter<CategoryModel, CategoryData> categoryConverter;

    @Mock
    private CategoryModel category;
    @Mock
    private TargetCategoryData categoryData;

    @Mock
    private CatalogVersionService mockCatalogVersionService;

    @Mock
    private CatalogVersionModel mockCatalogVersion;

    @InjectMocks
    private final TargetCategoryFacade facade = new TargetCategoryFacadeImpl();

    /**
     * Initializes test cases before run.
     */
    @Before
    public void setUp() {
        given(
                mockCatalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                        TgtCoreConstants.Catalog.ONLINE_VERSION)).willReturn(mockCatalogVersion);
    }

    /**
     * Verifies that leaf categories list is properly retrieved and converted.
     */
    @Test
    public void testLeafCategoriesRetrieval() {
        when(categoryService.getLeafCategoriesWithProducts()).thenReturn(ImmutableList.of(category));
        given(categoryConverter.convert(category)).willReturn(categoryData);
        final List<TargetCategoryData> result = facade.getLeafCategoryData();
        assertEquals(categoryData, result.get(0));
    }

    /**
     * Verifies that all categories for current session (within current catalog version) are properly retrieved and
     * converted.
     */
    @Test
    public void testAllCategoriesRetrieval() {
        when(categoryService.getAllCategories()).thenReturn(ImmutableList.of(category));
        given(categoryConverter.convert(category)).willReturn(categoryData);
        final List<TargetCategoryData> result = facade.getAllCategoryData();
        assertEquals(categoryData, result.get(0));
    }

    /**
     * Verifies that categories with products is properly retrieved and converted.
     */
    @Test
    public void testLeafCategoriesWithProductsRetrieval() {
        when(categoryService.getAllCategoriesWithProducts()).thenReturn(ImmutableList.of(category));
        given(categoryConverter.convert(category)).willReturn(categoryData);
        final List<TargetCategoryData> result = facade.getCategoriesWithProducts();
        assertEquals(categoryData, result.get(0));
    }


    @Test
    @SuppressWarnings("boxing")
    public void testIsRootCategoryWithRootCategory() {
        final String categoryCode = "C123";

        final CategoryModel mockCategory = mock(CategoryModel.class);

        given(categoryService.getCategoryForCode(mockCatalogVersion, categoryCode)).willReturn(mockCategory);
        given(categoryService.isRoot(mockCategory)).willReturn(Boolean.TRUE);

        assertThat(facade.isRootCategory(categoryCode)).isTrue();
    }

    @Test
    @SuppressWarnings("boxing")
    public void testIsRootCategoryWithNotRootCategory() {
        final String categoryCode = "C123";

        final CategoryModel mockCategory = mock(CategoryModel.class);

        given(categoryService.getCategoryForCode(mockCatalogVersion, categoryCode)).willReturn(mockCategory);
        given(categoryService.isRoot(mockCategory)).willReturn(Boolean.FALSE);

        assertThat(facade.isRootCategory(categoryCode)).isFalse();
    }

    @Test
    public void testIsTopCategoryWithTopCategory() {
        final String categoryCode = "TEST123";
        final CategoryModel topCategory = new CategoryModel();
        topCategory.setSupercategories(Arrays.asList(category));

        given(categoryService.getCategoryForCode(mockCatalogVersion, categoryCode)).willReturn(topCategory);

        assertThat(facade.isTopCategory(categoryCode)).isTrue();
    }

    @Test
    public void testIsTopCategoryWithNotTopCategory() {
        final String categoryCode = "TEST123";
        final CategoryModel nonTopCategory = new CategoryModel();

        given(categoryService.getCategoryForCode(mockCatalogVersion, categoryCode)).willReturn(nonTopCategory);

        assertThat(facade.isTopCategory(categoryCode)).isFalse();
    }
}

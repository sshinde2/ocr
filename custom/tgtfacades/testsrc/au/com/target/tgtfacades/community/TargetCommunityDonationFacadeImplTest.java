/**
 * 
 */
package au.com.target.tgtfacades.community;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.community.dto.TargetCommunityDonationRequestDto;
import au.com.target.tgtcore.community.service.TargetCommunityDonationService;
import au.com.target.tgtfacades.community.impl.TargetCommunityDonationFacadeImpl;
import au.com.target.tgtfacades.community.utils.TargetCommunityDonationHelper;


/**
 * @author knemalik
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCommunityDonationFacadeImplTest {

    @Mock
    private TargetCommunityDonationService targetCommunityDonationService;

    @Mock
    private TargetCommunityDonationHelper targetCommunityDonationHelper;

    private final TargetCommunityDonationFacadeImpl targetCommunityDonationFacadeImpl = new TargetCommunityDonationFacadeImpl();

    @SuppressWarnings("boxing")
    @Before
    public void setup() {
        given(targetCommunityDonationHelper.getStoreState(Mockito.anyString())).willReturn("123");
        given(targetCommunityDonationService.sendDonationRequest(Mockito.any(TargetCommunityDonationRequestDto.class)))
                .willReturn(true);

    }

    @Test
    public void testSendDonationRequestSuccess() {

        final String mockStoreNumber = "123";
        final String mockStoreState = "VIC";

        final TargetCommunityDonationRequestDto targetCommunityDonationRequestDto = new TargetCommunityDonationRequestDto();

        targetCommunityDonationRequestDto.setStoreNumber(mockStoreNumber);
        targetCommunityDonationRequestDto.setStoreState(mockStoreState);
        targetCommunityDonationFacadeImpl.setTargetCommunityDonationService(targetCommunityDonationService);
        targetCommunityDonationFacadeImpl.setTargetCommunityDonationHelper(targetCommunityDonationHelper);


        final boolean result = targetCommunityDonationFacadeImpl.sendDonationRequest(targetCommunityDonationRequestDto);

        assertTrue(result);

    }

    @Test
    public void testSendDonationRequestWithNullCommunityDonationRequestDto() {

        final TargetCommunityDonationRequestDto targetCommunityDonationRequestDto = null;

        targetCommunityDonationFacadeImpl.setTargetCommunityDonationService(targetCommunityDonationService);
        targetCommunityDonationFacadeImpl.setTargetCommunityDonationHelper(targetCommunityDonationHelper);

        final boolean result = targetCommunityDonationFacadeImpl.sendDonationRequest(targetCommunityDonationRequestDto);

        assertFalse(result);

    }

    @Test
    public void testSendDonationRequestWithNullStoreState() {

        final TargetCommunityDonationRequestDto targetCommunityDonationRequestDto = null;

        given(targetCommunityDonationHelper.getStoreState(Mockito.anyString())).willReturn(null);

        targetCommunityDonationFacadeImpl.setTargetCommunityDonationService(targetCommunityDonationService);
        targetCommunityDonationFacadeImpl.setTargetCommunityDonationHelper(targetCommunityDonationHelper);

        final boolean result = targetCommunityDonationFacadeImpl.sendDonationRequest(targetCommunityDonationRequestDto);

        assertFalse(result);

    }
}

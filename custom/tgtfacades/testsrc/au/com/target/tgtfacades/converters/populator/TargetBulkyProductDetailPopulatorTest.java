package au.com.target.tgtfacades.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.product.data.AbstractTargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetBulkyProductDetailPopulatorTest {
    @Mock
    private TargetCheckoutFacade mockTargetCheckoutFacade;

    @Mock
    private TargetDeliveryModeHelper mockTargetDeliveryModeHelper;

    @InjectMocks
    private final TargetBulkyProductDetailPopulator targetBulkyProductDetailPopulator = new TargetBulkyProductDetailPopulator<ProductModel, AbstractTargetProductData>();

    @Test
    public void testPopulateWithNotBulkyProduct() throws Exception {
        final ProductModel mockProduct = mock(ProductModel.class);
        final TargetProductData productData = new TargetProductData();

        targetBulkyProductDetailPopulator.populate(mockProduct, productData);

        verifyZeroInteractions(mockProduct);
    }

    @Test
    public void testPopulate() {
        final ProductModel mockProduct = mock(ProductModel.class);

        final TargetProductData productData = new TargetProductData();
        productData.setBigAndBulky(true);

        final TargetZoneDeliveryModeModel mockHDDeliveryMode = mock(TargetZoneDeliveryModeModel.class);
        given(mockHDDeliveryMode.getDefault()).willReturn(Boolean.TRUE);

        final TargetZoneDeliveryModeModel mockCNCDeliveryMode = mock(TargetZoneDeliveryModeModel.class);
        given(mockCNCDeliveryMode.getIsDeliveryToStore()).willReturn(Boolean.TRUE);

        final Set<DeliveryModeModel> deliveryModes = new HashSet<>();
        Collections.addAll(deliveryModes, mockHDDeliveryMode, mockCNCDeliveryMode);

        given(mockTargetDeliveryModeHelper.getDeliveryModesForProduct(mockProduct)).willReturn(deliveryModes);

        final PriceData bulkyHomeDeliveryFee = new PriceData();
        final PriceData bulkyCncFreeThreshold = new PriceData();

        given(mockTargetCheckoutFacade.getFirstPrice(mockHDDeliveryMode, mockProduct)).willReturn(bulkyHomeDeliveryFee);
        given(mockTargetCheckoutFacade.getLastThreshold(mockCNCDeliveryMode, mockProduct)).willReturn(
                bulkyCncFreeThreshold);

        targetBulkyProductDetailPopulator.populate(mockProduct, productData);

        assertThat(productData.getBulkyHomeDeliveryFee()).isEqualTo(bulkyHomeDeliveryFee);
        assertThat(productData.getBulkyCncFreeThreshold()).isEqualTo(bulkyCncFreeThreshold);
    }

    @Test
    public void testPopulateWithHDOnly() {
        final ProductModel mockProduct = mock(ProductModel.class);

        final TargetProductData productData = new TargetProductData();
        productData.setBigAndBulky(true);

        final TargetZoneDeliveryModeModel mockHDDeliveryMode = mock(TargetZoneDeliveryModeModel.class);
        given(mockHDDeliveryMode.getDefault()).willReturn(Boolean.TRUE);

        final Set<DeliveryModeModel> deliveryModes = new HashSet<>();
        Collections.addAll(deliveryModes, mockHDDeliveryMode);

        given(mockTargetDeliveryModeHelper.getDeliveryModesForProduct(mockProduct)).willReturn(deliveryModes);

        final PriceData bulkyHomeDeliveryFee = new PriceData();

        given(mockTargetCheckoutFacade.getFirstPrice(mockHDDeliveryMode, mockProduct)).willReturn(bulkyHomeDeliveryFee);

        targetBulkyProductDetailPopulator.populate(mockProduct, productData);

        assertThat(productData.getBulkyHomeDeliveryFee()).isEqualTo(bulkyHomeDeliveryFee);
        assertThat(productData.getBulkyCncFreeThreshold()).isNull();

        verify(mockTargetCheckoutFacade, never()).getLastThreshold(any(TargetZoneDeliveryModeModel.class),
                eq(mockProduct));
    }

    @Test
    public void testPopulateWithCNCOnly() {
        final ProductModel mockProduct = mock(ProductModel.class);

        final TargetProductData productData = new TargetProductData();
        productData.setBigAndBulky(true);

        final TargetZoneDeliveryModeModel mockCNCDeliveryMode = mock(TargetZoneDeliveryModeModel.class);
        given(mockCNCDeliveryMode.getIsDeliveryToStore()).willReturn(Boolean.TRUE);

        final Set<DeliveryModeModel> deliveryModes = new HashSet<>();
        Collections.addAll(deliveryModes, mockCNCDeliveryMode);

        given(mockTargetDeliveryModeHelper.getDeliveryModesForProduct(mockProduct)).willReturn(deliveryModes);

        final PriceData bulkyCncFreeThreshold = new PriceData();

        given(mockTargetCheckoutFacade.getLastThreshold(mockCNCDeliveryMode, mockProduct)).willReturn(
                bulkyCncFreeThreshold);

        targetBulkyProductDetailPopulator.populate(mockProduct, productData);

        assertThat(productData.getBulkyHomeDeliveryFee()).isNull();
        assertThat(productData.getBulkyCncFreeThreshold()).isEqualTo(bulkyCncFreeThreshold);

        verify(mockTargetCheckoutFacade, never())
                .getFirstPrice(any(TargetZoneDeliveryModeModel.class), eq(mockProduct));
    }

    @Test
    public void testPopulateWithExpressOnly() {
        final ProductModel mockProduct = mock(ProductModel.class);

        final TargetProductData productData = new TargetProductData();
        productData.setBigAndBulky(true);

        final TargetZoneDeliveryModeModel mockExpressDeliveryMode = mock(TargetZoneDeliveryModeModel.class);

        final Set<DeliveryModeModel> deliveryModes = new HashSet<>();
        Collections.addAll(deliveryModes, mockExpressDeliveryMode);

        given(mockTargetDeliveryModeHelper.getDeliveryModesForProduct(mockProduct)).willReturn(deliveryModes);

        targetBulkyProductDetailPopulator.populate(mockProduct, productData);

        assertThat(productData.getBulkyHomeDeliveryFee()).isNull();
        assertThat(productData.getBulkyCncFreeThreshold()).isNull();

        verify(mockTargetCheckoutFacade, never()).getLastThreshold(any(TargetZoneDeliveryModeModel.class),
                eq(mockProduct));
        verify(mockTargetCheckoutFacade, never())
                .getFirstPrice(any(TargetZoneDeliveryModeModel.class), eq(mockProduct));
    }
}

package au.com.target.tgtfacades.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doReturn;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.strategy.ShowStoreStockStrategy;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetColourVariantPopulatorTest {

    @Spy
    private final TargetColourVariantPopulator targetColourVariantPopulator = new TargetColourVariantPopulator();
    private TargetProductModel baseProduct;
    private TargetColourVariantProductModel mockSource;
    private TargetProductFacade productFacade;

    @Mock
    private TargetProductModel targetBaseProductModel;

    @Mock
    private ShowStoreStockStrategy mockShowStoreStockStrategy;
    private List<ShowStoreStockStrategy> showStoreStockStrategies;

    @Before
    public void setUp() {
        baseProduct = mock(TargetProductModel.class);
        mockSource = mock(TargetColourVariantProductModel.class);
        given(mockSource.getBaseProduct()).willReturn(baseProduct);
        productFacade = mock(TargetProductFacade.class);
        BDDMockito.doReturn(productFacade).when(targetColourVariantPopulator).getProductFacade();

        doReturn(Boolean.TRUE).when(mockShowStoreStockStrategy).showStoreStockForProduct(targetBaseProductModel);

        showStoreStockStrategies = new ArrayList<>();
        showStoreStockStrategies.add(mockShowStoreStockStrategy);

        given(productFacade.getBaseTargetProduct(mockSource)).willReturn(targetBaseProductModel);
    }

    /**
     * perform test
     */
    @Test
    public void testPopulate() {
        final String code = "01928374656";
        final String swatchName = "black";
        final String colourName = "red";
        final String displayName = "Product1";
        final String apn = "1234567981234";
        final boolean assorted = false;

        given(mockSource.getSwatchName()).willReturn(swatchName);
        given(mockSource.getColourName()).willReturn(colourName);
        given(mockSource.getDisplayName()).willReturn(displayName);
        given(mockSource.getOnlineExclusive()).willReturn(Boolean.TRUE);
        given(mockSource.getEan()).willReturn(apn);
        given(mockSource.getCode()).willReturn(code);
        willReturn(Boolean.valueOf(assorted)).given(mockSource).isAssorted();
        willReturn(Boolean.TRUE).given(mockSource).isExcludeForAfterpay();
        willReturn(Boolean.TRUE).given(mockSource).isExcludeForZipPayment();

        given(targetBaseProductModel.getCode()).willReturn("testBaseProductCode");
        given(targetBaseProductModel.getShowStoreStock()).willReturn(Boolean.TRUE);


        final TargetProductData target = new TargetProductData();

        targetColourVariantPopulator.populate(mockSource, target);

        assertThat(target.getSwatch()).isEqualTo(swatchName);
        assertThat(target.getColourName()).isEqualTo(colourName);
        assertThat(target.getName()).isEqualTo(displayName);
        assertThat(target.getSellableVariantDisplayCode()).isEqualTo(code);
        assertThat(target.isColourVariant()).isTrue();
        assertThat(target.isSizeVariant()).isFalse();
        assertThat(target.isOnlineExclusive()).isTrue();
        assertThat(target.getApn()).isEqualTo(apn);
        assertThat(target.getBaseProductCode()).isEqualTo("testBaseProductCode");
        assertThat(target.getColourVariantCode()).isEqualTo(code);
        assertThat(target.isAssorted()).isEqualTo(assorted);
        assertThat(target.isShowStoreStockForProduct()).isTrue();
        assertThat(target.isExcludeForAfterpay()).isTrue();
        assertThat(target.isExcludeForZipPayment()).isTrue();
    }

    @Test
    public void testPopulateWithAlternateValuesAndVariants() {
        final String code = "01928374656";
        final String swatchName = "orange";
        final String colourName = "yellow";
        final String apn = "9876543219876";
        final boolean assorted = true;

        final List<VariantProductModel> variants = new ArrayList<>();
        variants.add(mock(VariantProductModel.class));
        given(mockSource.getSwatchName()).willReturn(swatchName);
        given(mockSource.getColourName()).willReturn(colourName);
        given(mockSource.getEan()).willReturn(apn);
        given(mockSource.getCode()).willReturn(code);
        given(mockSource.getVariants()).willReturn(variants);
        given(Boolean.valueOf(mockSource.isAssorted())).willReturn(Boolean.valueOf(assorted));
        final TargetProductData target = new TargetProductData();

        doReturn(Boolean.FALSE).when(mockShowStoreStockStrategy).showStoreStockForProduct(targetBaseProductModel);

        targetColourVariantPopulator.populate(mockSource, target);

        //assumption is display only flag is only set on the sellable variant level.
        verify(mockSource, times(0)).getDisplayOnly();

        assertThat(target.getSwatch()).isEqualTo(swatchName);
        assertThat(target.getSellableVariantDisplayCode()).isNull();
        assertThat(target.isColourVariant()).isTrue();
        assertThat(target.isSizeVariant()).isFalse();
        assertThat(target.isOnlineExclusive()).isFalse();
        assertThat(target.getApn()).isEqualTo(apn);
        assertThat(target.getColourVariantCode()).isEqualTo(code);
        assertThat(target.isAssorted()).isEqualTo(assorted);
        assertThat(target.isShowStoreStockForProduct()).isFalse();
        assertThat(target.isDisplayOnly()).isFalse();
    }

    @Test
    public void testPopulateWithEmptyVariants() {
        final String code = "01928374656";
        final String swatchName = "orange";
        final String colourName = "yellow";
        final String apn = "9876543219876";

        final List<VariantProductModel> variants = new ArrayList<>();
        given(mockSource.getSwatchName()).willReturn(swatchName);
        given(mockSource.getColourName()).willReturn(colourName);
        given(mockSource.getEan()).willReturn(apn);
        given(mockSource.getCode()).willReturn(code);
        given(baseProduct.getShowStoreStock()).willReturn(Boolean.FALSE);
        given(mockSource.getVariants()).willReturn(variants);
        given(mockSource.getDisplayOnly()).willReturn(Boolean.TRUE);
        final TargetProductData target = new TargetProductData();

        targetColourVariantPopulator.populate(mockSource, target);

        assertThat(target.getSwatch()).isEqualTo(swatchName);
        assertThat(target.getSellableVariantDisplayCode()).isEqualTo(code);
        assertThat(target.isColourVariant()).isTrue();
        assertThat(target.isSizeVariant()).isFalse();
        assertThat(target.isOnlineExclusive()).isFalse();
        assertThat(target.getApn()).isEqualTo(apn);
        assertThat(target.getColourVariantCode()).isEqualTo(code);
        assertThat(target.isAssorted()).isFalse();
        assertThat(target.isShowStoreStockForProduct()).isFalse();
        assertThat(target.isDisplayOnly()).isTrue();
    }

    /**
     * perform test with null Colour
     */
    @Test
    public void testPopulateWithNullValues() {
        final TargetProductData target = new TargetProductData();
        targetColourVariantPopulator.populate(mockSource, target);

        assertThat(target.getSellableVariantDisplayCode()).isNull();
        assertThat(target.getSwatch()).isNull();
        assertThat(target.getColourName()).isNull();
        assertThat(target.getPromotionStatuses()).isNotNull();
        assertThat(target.getPromotionStatuses()).isEmpty();
        assertThat(target.isOnlineExclusive()).isFalse();
        assertThat(target.isColourVariant()).isTrue();
        assertThat(target.isSizeVariant()).isFalse();
        assertThat(target.getApn()).isNull();
    }

    @Test
    public void testPopulateWithNullColourName() {
        final String swatchName = "black";

        given(mockSource.getSwatchName()).willReturn(swatchName);

        final TargetProductData target = new TargetProductData();
        targetColourVariantPopulator.populate(mockSource, target);

        assertThat(target.getSwatch()).isEqualTo(swatchName);
        assertThat(target.getColourName()).isNull();
        assertThat(target.isColourVariant()).isTrue();
        assertThat(target.isSizeVariant()).isFalse();
    }

    @Test
    public void testPopulateWithNullSwatchName() {
        final String colourName = "tangarine";

        given(mockSource.getColourName()).willReturn(colourName);

        final TargetProductData target = new TargetProductData();
        targetColourVariantPopulator.populate(mockSource, target);

        assertThat(target.getSwatch()).isNull();
        assertThat(target.getColourName()).isEqualTo(colourName);
        assertThat(target.isColourVariant()).isTrue();
        assertThat(target.isSizeVariant()).isFalse();
    }

    @Test
    public void testPopulateWithBlankValues() {
        final String swatchName = "";
        final String colourName = "";
        final String apn = "";

        given(mockSource.getSwatchName()).willReturn(swatchName);
        given(mockSource.getColourName()).willReturn(colourName);
        given(mockSource.getEan()).willReturn(apn);
        final TargetProductData target = new TargetProductData();

        targetColourVariantPopulator.populate(mockSource, target);

        assertThat(target.getSwatch()).isEqualTo(swatchName);
        assertThat(target.getColourName()).isEqualTo(colourName);
        assertThat(target.getPromotionStatuses()).isNotNull();
        assertThat(target.getPromotionStatuses().isEmpty()).isTrue();
        assertThat(target.isOnlineExclusive()).isFalse();
        assertThat(target.isColourVariant()).isTrue();
        assertThat(target.isSizeVariant()).isFalse();
        assertThat(target.getApn()).isEqualTo(apn);
    }


    /**
     * perform test with null source
     */
    @Test
    public void testPopulateWithNullSource() {
        final TargetProductData mockTarget = mock(TargetProductData.class);

        targetColourVariantPopulator.populate(null, mockTarget);

        Mockito.verifyZeroInteractions(mockTarget);
    }

    @Test
    public void testPopulateWithNullTarget() {

        targetColourVariantPopulator.populate(mockSource, null);

        Mockito.verifyZeroInteractions(mockSource);
    }

    @Test
    public void testPopulateWithIncorrectSourceType() {
        final AbstractTargetVariantProductModel mockSourceAbstract = mock(AbstractTargetVariantProductModel.class);
        final TargetProductData mockTarget = mock(TargetProductData.class);

        targetColourVariantPopulator.populate(mockSourceAbstract, mockTarget);

        Mockito.verifyZeroInteractions(mockSourceAbstract);
        Mockito.verifyZeroInteractions(mockTarget);
    }

    @Test
    public void testPopulateWithIncorrectTargetType() {
        final TargetColourVariantProductModel mockSourceAbstract = mock(TargetColourVariantProductModel.class);
        final ProductData mockTarget = mock(ProductData.class);

        targetColourVariantPopulator.populate(mockSourceAbstract, mockTarget);

        Mockito.verifyZeroInteractions(mockSourceAbstract);
        Mockito.verifyZeroInteractions(mockTarget);
    }

    @Test
    public void testPopulateWithNullBaseProduct() {
        final String code = "01928374656";
        final String swatchName = "black";
        final String colourName = "red";
        final String displayName = "Product1";
        final String apn = "1234567981234";

        given(mockSource.getSwatchName()).willReturn(swatchName);
        given(mockSource.getColourName()).willReturn(colourName);
        given(mockSource.getDisplayName()).willReturn(displayName);
        given(mockSource.getOnlineExclusive()).willReturn(Boolean.TRUE);
        given(mockSource.getEan()).willReturn(apn);
        given(mockSource.getCode()).willReturn(code);

        given(productFacade.getBaseTargetProduct(mockSource)).willReturn(null);

        final TargetProductData target = new TargetProductData();

        targetColourVariantPopulator.populate(mockSource, target);

        assertThat(target.getSwatch()).isEqualTo(swatchName);
        assertThat(target.getColourName()).isEqualTo(colourName);
        assertThat(target.getName()).isEqualTo(displayName);
        assertThat(target.getSellableVariantDisplayCode()).isEqualTo(code);
        assertThat(target.isColourVariant()).isTrue();
        assertThat(target.isSizeVariant()).isFalse();
        assertThat(target.isOnlineExclusive()).isTrue();
        assertThat(target.getApn()).isEqualTo(apn);
        assertThat(target.getBaseProductCode()).isNull();
        assertThat(target.getColourVariantCode()).isEqualTo(code);
        assertThat(target.isShowStoreStockForProduct()).isFalse();
    }

    @Test
    public void testPopulateNewLowerPriceStartDateWhenFlagIsTrue() {
        final Date newLowerPriceStartDate = new Date();
        given(mockSource.getNewLowerPriceStartDate()).willReturn(newLowerPriceStartDate);
        given(mockSource.getNewLowerPriceFlag()).willReturn(Boolean.TRUE);
        final TargetProductData target = new TargetProductData();
        targetColourVariantPopulator.populate(mockSource, target);

        assertThat(target.getNewLowerPriceStartDate()).isEqualTo(newLowerPriceStartDate);
    }

    @Test
    public void testPopulateNewLowerPriceStartDateWhenFlagIsFalse() {
        final Date newLowerPriceStartDate = new Date();
        given(mockSource.getNewLowerPriceStartDate()).willReturn(newLowerPriceStartDate);
        given(mockSource.getNewLowerPriceFlag()).willReturn(Boolean.FALSE);
        final TargetProductData target = new TargetProductData();
        targetColourVariantPopulator.populate(mockSource, target);

        assertThat(target.getNewLowerPriceStartDate()).isNull();
    }

    @Test
    public void testPopulateWithNullShowStoreStock() {
        given(mockSource.getBaseProduct()).willReturn(baseProduct);
        given(targetBaseProductModel.getShowStoreStock()).willReturn(null);
        final TargetProductData target = new TargetProductData();
        targetColourVariantPopulator.populate(mockSource, target);
        assertThat(target.isShowStoreStockForProduct()).isFalse();

    }

    @Test
    public void testPopulateWithShowStoreStock() {
        given(mockSource.getBaseProduct()).willReturn(baseProduct);
        given(targetBaseProductModel.getShowStoreStock()).willReturn(Boolean.TRUE);
        final TargetProductData target = new TargetProductData();
        targetColourVariantPopulator.populate(mockSource, target);
        assertThat(target.isShowStoreStockForProduct()).isTrue();

    }
}

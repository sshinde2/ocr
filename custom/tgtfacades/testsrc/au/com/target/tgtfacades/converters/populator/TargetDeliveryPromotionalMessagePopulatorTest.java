/**
 * 
 */
package au.com.target.tgtfacades.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeValueModel;
import de.hybris.platform.servicelayer.StubLocaleProvider;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.internal.model.impl.LocaleProvider;
import de.hybris.platform.servicelayer.model.ItemModelContextImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.TargetDiscountCategoryModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.product.data.AbstractTargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.util.TargetPriceHelper;


/**
 * @author ajit
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetDeliveryPromotionalMessagePopulatorTest {

    private static final String EXPRESS_DELIVERY = "express-delivery";
    private static final String CNC_DELIVERY = "click-and-delivery";
    private static final String HOME_DELIVERY = "home-delivery";

    @InjectMocks
    private final TargetDeliveryPromotionalMessagePopulator promotionalPopulator = new TargetDeliveryPromotionalMessagePopulator();

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Mock
    private TargetDeliveryService targetDeliveryService;

    private List<TargetZoneDeliveryModeModel> targetZoneDeliveryModes;

    private TargetZoneDeliveryModeModel homeDelivery;
    private TargetZoneDeliveryModeModel cncDelivery;
    private TargetZoneDeliveryModeModel expressDelivery;
    private final TargetProductModel productModel = Mockito.mock(TargetProductModel.class);

    // TargetDeliveryPromotionalMessagePopulator populates AbstractTargetProductData, so create
    // one to make the test more accurate.
    private final AbstractTargetProductData productData = new AbstractTargetProductData() {
        // No overrides required
    };

    private List<AbstractTargetZoneDeliveryModeValueModel> listofZDMVMforHD, listofZDMVMforCNC, listofZDMVMforEXD;
    private RestrictableTargetZoneDeliveryModeValueModel restrictableTZDMVM1, restrictableTZDMVM2, restrictableTZDMVM3,
            restrictableTZDMVM4, restrictableTZDMVM5, restrictableTZDMVM6,
            restrictableTZDMVM7, restrictableTZDMVM8, restrictableTZDMVM9;

    private final String stickerMegHD1 = "HD Sticker for priority : 100";
    private final String promotionalMessageHD1 = "HD promotional message for priority : 100";
    private final String stickerMegHD2 = "HD Sticker for priority : 200";
    private final String promotionalMessageHD2 = "HD promotional message for priority : 200";
    private final String stickerMegHD3 = "HD Sticker for priority : 300";
    private final String promotionalMessageHD3 = "HD promotional message for priority : 300";

    private final String stickerMegCNC1 = "CNC Sticker for priority : 100";
    private final String promotionalMessageCNC1 = "CNC promotional message for priority : 100";
    private final String stickerMegCNC2 = "CNC Sticker for priority : 200";
    private final String promotionalMessageCNC2 = "CNC promotional message for priority : 200";
    private final String stickerMegCNC3 = "CNC Sticker for priority : 300";
    private final String promotionalMessageCNC3 = "CNC promotional message for priority : 300";

    private final String stickerMegEX1 = "EXD Sticker for priority : 100";
    private final String promotionalMessageEX1 = "EXD promotional message for priority : 100";
    private final String stickerMegEX2 = "EXD Sticker for priority : 200";
    private final String promotionalMessageEX2 = "EXD promotional message for priority : 200";
    private final String stickerMegEX3 = "EXD Sticker for priority : 300";
    private final String promotionalMessageEX3 = "EXD promotional message for priority : 300";

    private ItemModelContextImpl itemModelContext1, itemModelContext2, itemModelContext3;

    private final LocaleProvider locale = new StubLocaleProvider(Locale.ENGLISH);

    @Mock
    private TargetPriceHelper targetPriceHelper;
    @Mock
    private PriceDataFactory priceDataFactory;
    @Mock
    private CommonI18NService commonI18NService;

    @Before
    public void setUp() {
        targetZoneDeliveryModes = new ArrayList<>();
        setHomeDeliveryData();
        setCNCDeliveryData();
        setExpressDeliveryData();

        given(targetDeliveryService
                .getAllApplicableDeliveryValuesForModeAndProduct(homeDelivery, productModel)).willReturn(
                        listofZDMVMforHD);
        given(targetDeliveryService
                .getAllApplicableDeliveryValuesForModeAndProduct(cncDelivery, productModel)).willReturn(
                        listofZDMVMforCNC);
        given(targetDeliveryService
                .getAllApplicableDeliveryValuesForModeAndProduct(expressDelivery, productModel)).willReturn(
                        listofZDMVMforEXD);

        given(targetDeliveryModeHelper.getAllCurrentActiveDeliveryModesForProduct(productModel)).willReturn(
                targetZoneDeliveryModes);
    }

    /**
     * Test delivery promotional message. when all the delivery options are available with different priority then the
     * highest priority delivery promotions will be displayed
     */
    @Test
    public void testGetDeliveryPromotionalMessage() {

        promotionalPopulator.populate(productModel, productData);
        assertThat(productData.getProductPromotionalDeliverySticker()).isEqualTo(stickerMegHD3);
        assertThat(productData.getProductPromotionalDeliveryText())
                .isEqualTo(promotionalMessageHD3 + " or " + promotionalMessageCNC3 + " or " + promotionalMessageEX3);


    }

    /**
     * Test get delivery promotional message when delivery rank null.
     */
    @Test
    public void testGetDeliveryPromotionalMessageWhenDeliveryRankNull() {

        for (final TargetZoneDeliveryModeModel deliveryMode : targetZoneDeliveryModes) {
            if (deliveryMode.getCode().equals(HOME_DELIVERY)) {
                deliveryMode.setDeliveryPromotionRank(null);
            }
        }

        promotionalPopulator.populate(productModel, productData);
        assertThat(productData.getProductPromotionalDeliverySticker()).isEqualTo(stickerMegCNC3);
        assertThat(productData.getProductPromotionalDeliveryText())
                .isEqualTo(promotionalMessageCNC3 + " or " + promotionalMessageEX3);
    }

    /**
     * Test get delivery promotional message when all delivery rank are not defined.
     */
    @Test
    public void testGetDeliveryPromotionalMessageWhenAllDeliveryRankNull() {

        for (final TargetZoneDeliveryModeModel deliveryMode : targetZoneDeliveryModes) {
            deliveryMode.setDeliveryPromotionRank(null);
        }

        promotionalPopulator.populate(productModel, productData);
        assertThat(productData.getProductPromotionalDeliverySticker()).isNull();
        assertThat(productData.getProductPromotionalDeliveryText()).isEqualTo(StringUtils.EMPTY);

    }

    /**
     * Test get delivery promotional message when sticker & promotional message is null of the highest priority
     * restrictTZDMVM. the second highest promotional message will be displayed
     */
    @Test
    public void testGetDeliveryPromotionalMessageWhenStickerAndMessageNull() {

        for (final TargetZoneDeliveryModeModel deliveryMode : targetZoneDeliveryModes) {
            if (deliveryMode.getCode().equals(HOME_DELIVERY)) {
                for (final ZoneDeliveryModeValueModel deliveryModeValueModel : deliveryMode.getValues()) {
                    if (deliveryModeValueModel instanceof RestrictableTargetZoneDeliveryModeValueModel
                            && ((RestrictableTargetZoneDeliveryModeValueModel)deliveryModeValueModel).getPriority()
                                    .intValue() == 300) {
                        ((RestrictableTargetZoneDeliveryModeValueModel)deliveryModeValueModel)
                                .setPromotionalStickerMessage("");
                        ((RestrictableTargetZoneDeliveryModeValueModel)deliveryModeValueModel)
                                .setDeliveryPromotionMessage("");
                    }
                }

            }
        }
        promotionalPopulator.populate(productModel, productData);
        assertThat(productData.getProductPromotionalDeliverySticker()).isEqualTo(stickerMegCNC3);
        assertThat(productData.getProductPromotionalDeliveryText())
                .isEqualTo(promotionalMessageCNC3 + " or " + promotionalMessageEX3);

    }

    /**
     * Test get delivery promotional message when only sticker message is null of the highest priority restrictTZDMVM.
     * the second highest sticker message will be displayed and the promotional message will remain unchanged
     */
    @Test
    public void testGetDeliveryPromotionalMessageWhenOnlyStickerNull() {

        for (final TargetZoneDeliveryModeModel deliveryMode : targetZoneDeliveryModes) {
            if (deliveryMode.getCode().equals(HOME_DELIVERY)) {
                for (final ZoneDeliveryModeValueModel deliveryModeValueModel : deliveryMode.getValues()) {
                    if (deliveryModeValueModel instanceof RestrictableTargetZoneDeliveryModeValueModel
                            && ((RestrictableTargetZoneDeliveryModeValueModel)deliveryModeValueModel).getPriority()
                                    .intValue() == 300) {
                        ((RestrictableTargetZoneDeliveryModeValueModel)deliveryModeValueModel)
                                .setPromotionalStickerMessage("");
                    }
                }

            }
        }
        promotionalPopulator.populate(productModel, productData);
        assertThat(productData.getProductPromotionalDeliverySticker()).isEqualTo(stickerMegCNC3);
        assertThat(productData.getProductPromotionalDeliveryText())
                .isEqualTo(promotionalMessageHD3 + " or " + promotionalMessageCNC3 + " or " + promotionalMessageEX3);

    }

    /**
     * Test get delivery promotional message when only sticker message is null of the highest priority restrictTZDMVM.
     * the second highest sticker message will be displayed and the promotional message will remain unchanged
     */
    @Test
    public void testGetDeliveryPromotionalMessageWhenOnlyPromotionalTextNull() {

        for (final TargetZoneDeliveryModeModel deliveryMode : targetZoneDeliveryModes) {
            if (deliveryMode.getCode().equals(HOME_DELIVERY)) {
                for (final ZoneDeliveryModeValueModel deliveryModeValueModel : deliveryMode.getValues()) {
                    if (deliveryModeValueModel instanceof RestrictableTargetZoneDeliveryModeValueModel
                            && ((RestrictableTargetZoneDeliveryModeValueModel)deliveryModeValueModel).getPriority()
                                    .intValue() == 300) {
                        ((RestrictableTargetZoneDeliveryModeValueModel)deliveryModeValueModel)
                                .setDeliveryPromotionMessage("");
                    }
                }

            }
        }
        promotionalPopulator.populate(productModel, productData);
        assertThat(productData.getProductPromotionalDeliverySticker()).isEqualTo(stickerMegHD3);
        assertThat(productData.getProductPromotionalDeliveryText())
                .isEqualTo(promotionalMessageCNC3 + " or " + promotionalMessageEX3);

    }

    /**
     * Test delivery mode not found for the product. Both the promotional fields will be null
     */
    @Test
    public void testDeliveryModeNotFoundForTheProduct() {
        given(targetDeliveryModeHelper.getAllCurrentActiveDeliveryModesForProduct(productModel)).willReturn(
                Collections.EMPTY_SET);
        promotionalPopulator.populate(productModel, productData);
        assertThat(productData.getProductPromotionalDeliverySticker()).isNull();
        assertThat(productData.getProductPromotionalDeliveryText()).isEqualTo(StringUtils.EMPTY);
    }

    /**
     * Test some delivery mode not found for the product. Lets consider HD is not available. then the second highest
     * priority promotional details will be displayed
     */
    @Test
    public void testSomeDeliveryModeNotFoundForTheProduct() {

        targetZoneDeliveryModes.remove(homeDelivery);
        promotionalPopulator.populate(productModel, productData);
        assertThat(productData.getProductPromotionalDeliverySticker()).isEqualTo(stickerMegCNC3);
        assertThat(productData.getProductPromotionalDeliveryText())
                .isEqualTo(promotionalMessageCNC3 + " or " + promotionalMessageEX3);
    }

    /**
     * Test when no restrictable TZDMVM present for all delivery mode.
     */
    @Test
    public void testWhenNoRestrictableTZDMVMPresent() {

        given(targetDeliveryService
                .getAllApplicableDeliveryValuesForModeAndProduct(homeDelivery, productModel)).willReturn(
                        Collections.EMPTY_LIST);
        given(targetDeliveryService
                .getAllApplicableDeliveryValuesForModeAndProduct(cncDelivery, productModel)).willReturn(
                        Collections.EMPTY_LIST);
        given(targetDeliveryService
                .getAllApplicableDeliveryValuesForModeAndProduct(expressDelivery, productModel)).willReturn(
                        Collections.EMPTY_LIST);
        promotionalPopulator.populate(productModel, productData);
        assertThat(productData.getProductPromotionalDeliverySticker()).isNull();
        assertThat(productData.getProductPromotionalDeliveryText()).isEmpty();
    }

    /**
     * Test when The highest restrictable TZDMVM is not present then the second highest sticker, 2nd and 3rd promotional
     * message will be set.
     */
    @Test
    public void testWhenHighestRestrictableTZDMVMNotPresent() {

        given(targetDeliveryService
                .getAllApplicableDeliveryValuesForModeAndProduct(homeDelivery, productModel)).willReturn(
                        Collections.EMPTY_LIST);
        promotionalPopulator.populate(productModel, productData);
        assertThat(productData.getProductPromotionalDeliverySticker()).isEqualTo(stickerMegCNC3);
        assertThat(productData.getProductPromotionalDeliveryText())
                .isEqualTo(promotionalMessageCNC3 + " or " + promotionalMessageEX3);
    }

    private void setHomeDeliveryData() {
        homeDelivery = new TargetZoneDeliveryModeModel();
        homeDelivery.setCode(HOME_DELIVERY);
        homeDelivery.setDeliveryPromotionRank(Integer.valueOf(1));
        listofZDMVMforHD = new ArrayList<>();
        restrictableTZDMVM1 = new RestrictableTargetZoneDeliveryModeValueModel();
        itemModelContext1 = (ItemModelContextImpl)restrictableTZDMVM1.getItemModelContext();
        itemModelContext1.setLocaleProvider(locale);
        restrictableTZDMVM1.setPriority(Integer.valueOf(100));
        restrictableTZDMVM1.setPromotionalStickerMessage(stickerMegHD1);
        restrictableTZDMVM1.setDeliveryPromotionMessage(promotionalMessageHD1);
        listofZDMVMforHD.add(restrictableTZDMVM1);
        restrictableTZDMVM2 = new RestrictableTargetZoneDeliveryModeValueModel();
        itemModelContext2 = (ItemModelContextImpl)restrictableTZDMVM2.getItemModelContext();
        itemModelContext2.setLocaleProvider(locale);
        restrictableTZDMVM2.setPriority(Integer.valueOf(200));
        restrictableTZDMVM2.setPromotionalStickerMessage(stickerMegHD2);
        restrictableTZDMVM2.setDeliveryPromotionMessage(promotionalMessageHD2);
        listofZDMVMforHD.add(restrictableTZDMVM2);
        restrictableTZDMVM3 = new RestrictableTargetZoneDeliveryModeValueModel();
        itemModelContext3 = (ItemModelContextImpl)restrictableTZDMVM3.getItemModelContext();
        itemModelContext3.setLocaleProvider(locale);
        restrictableTZDMVM3.setPriority(Integer.valueOf(300));
        restrictableTZDMVM3.setPromotionalStickerMessage(stickerMegHD3);
        restrictableTZDMVM3.setDeliveryPromotionMessage(promotionalMessageHD3);
        restrictableTZDMVM3.setValue(Double.valueOf(2.00));
        listofZDMVMforHD.add(restrictableTZDMVM3);
        homeDelivery.setValues(new HashSet<ZoneDeliveryModeValueModel>(listofZDMVMforHD));

        targetZoneDeliveryModes.add(homeDelivery);
    }

    private void setCNCDeliveryData() {
        cncDelivery = new TargetZoneDeliveryModeModel();
        cncDelivery.setCode(CNC_DELIVERY);
        cncDelivery.setDeliveryPromotionRank(Integer.valueOf(2));
        listofZDMVMforCNC = new ArrayList<>();
        restrictableTZDMVM4 = new RestrictableTargetZoneDeliveryModeValueModel();
        itemModelContext1 = (ItemModelContextImpl)restrictableTZDMVM4.getItemModelContext();
        itemModelContext1.setLocaleProvider(locale);
        restrictableTZDMVM4.setPriority(Integer.valueOf(100));
        restrictableTZDMVM4.setPromotionalStickerMessage(stickerMegCNC1);
        restrictableTZDMVM4.setDeliveryPromotionMessage(promotionalMessageCNC1);
        listofZDMVMforCNC.add(restrictableTZDMVM4);

        restrictableTZDMVM5 = new RestrictableTargetZoneDeliveryModeValueModel();
        itemModelContext2 = (ItemModelContextImpl)restrictableTZDMVM5.getItemModelContext();
        itemModelContext2.setLocaleProvider(locale);
        restrictableTZDMVM5.setPriority(Integer.valueOf(200));
        restrictableTZDMVM5.setPromotionalStickerMessage(stickerMegCNC2);
        restrictableTZDMVM5.setDeliveryPromotionMessage(promotionalMessageCNC2);
        listofZDMVMforCNC.add(restrictableTZDMVM5);

        restrictableTZDMVM6 = new RestrictableTargetZoneDeliveryModeValueModel();
        itemModelContext3 = (ItemModelContextImpl)restrictableTZDMVM6.getItemModelContext();
        itemModelContext3.setLocaleProvider(locale);
        restrictableTZDMVM6.setPriority(Integer.valueOf(300));
        restrictableTZDMVM6.setPromotionalStickerMessage(stickerMegCNC3);
        restrictableTZDMVM6.setDeliveryPromotionMessage(promotionalMessageCNC3);
        listofZDMVMforCNC.add(restrictableTZDMVM6);
        cncDelivery.setValues(new HashSet<ZoneDeliveryModeValueModel>(listofZDMVMforCNC));

        targetZoneDeliveryModes.add(cncDelivery);
    }

    private void setExpressDeliveryData() {
        expressDelivery = new TargetZoneDeliveryModeModel();
        expressDelivery.setCode(EXPRESS_DELIVERY);
        expressDelivery.setDeliveryPromotionRank(Integer.valueOf(3));
        listofZDMVMforEXD = new ArrayList<>();
        restrictableTZDMVM7 = new RestrictableTargetZoneDeliveryModeValueModel();
        itemModelContext1 = (ItemModelContextImpl)restrictableTZDMVM7.getItemModelContext();
        itemModelContext1.setLocaleProvider(locale);
        restrictableTZDMVM7.setPriority(Integer.valueOf(100));
        restrictableTZDMVM7.setPromotionalStickerMessage(stickerMegEX1);
        restrictableTZDMVM7.setDeliveryPromotionMessage(promotionalMessageEX1);
        listofZDMVMforEXD.add(restrictableTZDMVM7);

        restrictableTZDMVM8 = new RestrictableTargetZoneDeliveryModeValueModel();
        itemModelContext2 = (ItemModelContextImpl)restrictableTZDMVM8.getItemModelContext();
        itemModelContext2.setLocaleProvider(locale);
        restrictableTZDMVM8.setPriority(Integer.valueOf(200));
        restrictableTZDMVM8.setPromotionalStickerMessage(stickerMegEX2);
        restrictableTZDMVM8.setDeliveryPromotionMessage(promotionalMessageEX2);
        listofZDMVMforEXD.add(restrictableTZDMVM8);

        restrictableTZDMVM9 = new RestrictableTargetZoneDeliveryModeValueModel();
        itemModelContext3 = (ItemModelContextImpl)restrictableTZDMVM9.getItemModelContext();
        itemModelContext3.setLocaleProvider(locale);
        restrictableTZDMVM9.setPriority(Integer.valueOf(300));
        restrictableTZDMVM9.setPromotionalStickerMessage(stickerMegEX3);
        restrictableTZDMVM9.setDeliveryPromotionMessage(promotionalMessageEX3);
        listofZDMVMforEXD.add(restrictableTZDMVM9);
        expressDelivery.setValues(new HashSet<ZoneDeliveryModeValueModel>(listofZDMVMforEXD));

        targetZoneDeliveryModes.add(expressDelivery);
    }

    @Test
    public void testNewStickerTagNoNewTagWhenOnlineDateNull() {
        final TargetDeliveryPromotionalMessagePopulator populator = Mockito.spy(promotionalPopulator);
        final TargetProductModel sourceProduct = new TargetProductModel();
        final TargetProductData targetData = new TargetProductData();
        final TargetProductCategoryModel productCategory = new TargetProductCategoryModel();
        populator.setNewStickerTag(sourceProduct, targetData);
        given(populator.getTopLevelCategoryForProduct(sourceProduct)).willReturn(productCategory);
        assertThat(targetData.isNewArrived()).isFalse();
    }

    @Test
    public void testNewStickerTagNoNewTagFromTopLevelCategory() {
        final TargetDeliveryPromotionalMessagePopulator populator = mock(
                TargetDeliveryPromotionalMessagePopulator.class);
        final TargetProductModel sourceProduct = new TargetProductModel();
        Mockito.doCallRealMethod().when(populator).setNewStickerTag(sourceProduct, productData);
        final Calendar calender = Calendar.getInstance();
        calender.add(Calendar.DATE, -3);
        sourceProduct.setOnlineDate(calender.getTime());
        final TargetProductCategoryModel productTopCategory = new TargetProductCategoryModel();
        productTopCategory.setNewToOnlineConfigDays(Integer.valueOf(5));
        given(populator.getTopLevelCategoryForProduct(sourceProduct)).willReturn(productTopCategory);
        populator.setNewStickerTag(sourceProduct, productData);
        assertThat(productData.isNewArrived()).isTrue();
    }

    @Test
    public void testNewStickerTagNoNewTagFromTopLevelCategoryNewConfigIsNull() {
        final TargetDeliveryPromotionalMessagePopulator populator = Mockito
                .mock(TargetDeliveryPromotionalMessagePopulator.class);
        final TargetProductModel sourceProduct = new TargetProductModel();
        Mockito.doCallRealMethod().when(populator).setNewStickerTag(sourceProduct, productData);
        final Calendar calender = Calendar.getInstance();
        calender.add(Calendar.DATE, -6);
        sourceProduct.setOnlineDate(calender.getTime());
        final TargetProductCategoryModel productTopCategory = new TargetProductCategoryModel();
        productTopCategory.setNewToOnlineConfigDays(Integer.valueOf(5));
        given(populator.getTopLevelCategoryForProduct(sourceProduct)).willReturn(productTopCategory);
        populator.setNewStickerTag(sourceProduct, productData);
        assertThat(productData.isNewArrived()).isFalse();
    }

    @Test
    public void testNewStickerTagNoNewTagFromTopLevelCategoryIsNullAndAllProductHasValue() {
        final TargetDeliveryPromotionalMessagePopulator populator = Mockito
                .mock(TargetDeliveryPromotionalMessagePopulator.class);
        final TargetProductModel sourceProduct = new TargetProductModel();
        Mockito.doCallRealMethod().when(populator).setNewStickerTag(sourceProduct, productData);
        final Calendar calender = Calendar.getInstance();
        calender.add(Calendar.DATE, -6);
        sourceProduct.setOnlineDate(calender.getTime());
        final TargetProductCategoryModel productTopCategory = new TargetProductCategoryModel();
        productTopCategory.setNewToOnlineConfigDays(null);
        given(populator.getTopLevelCategoryForProduct(sourceProduct)).willReturn(productTopCategory);
        final List<CategoryModel> listOfCategory = new ArrayList<>();
        final TargetProductCategoryModel model1 = new TargetProductCategoryModel();
        model1.setCode(TgtCoreConstants.Category.ALL_PRODUCTS);
        model1.setNewToOnlineConfigDays(Integer.valueOf(3));
        final CategoryModel model2 = new TargetDiscountCategoryModel();
        listOfCategory.add(model1);
        listOfCategory.add(model2);
        productTopCategory.setSupercategories(listOfCategory);
        populator.setNewStickerTag(sourceProduct, productData);
        assertThat(productData.isNewArrived()).isFalse();
    }

    @Test
    public void testNewStickerTagWhenOnlineDateInNull() {
        final TargetDeliveryPromotionalMessagePopulator populator = Mockito
                .mock(TargetDeliveryPromotionalMessagePopulator.class);
        final TargetProductModel sourceProduct = new TargetProductModel();
        Mockito.doCallRealMethod().when(populator).setNewStickerTag(sourceProduct, productData);
        final Calendar calender = Calendar.getInstance();
        calender.add(Calendar.DATE, -20);
        sourceProduct.setOnlineDate(null);
        sourceProduct.setCreationtime(calender.getTime());
        final TargetProductCategoryModel productTopCategory = new TargetProductCategoryModel();
        productTopCategory.setNewToOnlineConfigDays(null);
        given(populator.getTopLevelCategoryForProduct(sourceProduct)).willReturn(productTopCategory);
        final List<CategoryModel> listOfCategory = new ArrayList<>();
        final TargetProductCategoryModel model1 = new TargetProductCategoryModel();
        model1.setCode(TgtCoreConstants.Category.ALL_PRODUCTS);
        model1.setNewToOnlineConfigDays(Integer.valueOf(20));
        final CategoryModel model2 = new TargetDiscountCategoryModel();
        listOfCategory.add(model1);
        listOfCategory.add(model2);
        productTopCategory.setSupercategories(listOfCategory);
        populator.setNewStickerTag(sourceProduct, productData);
        assertThat(productData.isNewArrived()).isTrue();
    }

    @Test
    public void testGiftCardDeliveryFeeForGiftCardProduct() {
        final ProductTypeModel productTypeModel = mock(ProductTypeModel.class);
        final PriceData giftCardPriceData = mock(PriceData.class);
        given(giftCardPriceData.getFormattedValue()).willReturn("$2.00");
        given(targetPriceHelper.createPriceData(Double.valueOf(2.00))).willReturn(giftCardPriceData);
        given(productTypeModel.getCode()).willReturn("giftCard");
        given(productModel.getProductType()).willReturn(productTypeModel);
        productData.setProductTypeCode("giftCard");
        promotionalPopulator.populate(productModel, productData);
        assertThat(productData.getGiftCardDeliveryFee()).isEqualTo("$2.00");

    }
}

/**
 * 
 */
package au.com.target.tgtfacades.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERec;
import com.endeca.navigation.ERecList;
import com.endeca.navigation.Navigation;
import com.endeca.navigation.PropertyMap;
import com.google.common.collect.ImmutableList;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.querybuilder.EndecaProductQueryBuilder;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.product.data.AbstractTargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductConsolidatedStoreStockPopulatorTest {

    @InjectMocks
    @Spy
    private final TargetProductConsolidatedStoreStockPopulator populator = new TargetProductConsolidatedStoreStockPopulator();

    @Mock
    private EndecaProductQueryBuilder endecaProductQueryBuilder;

    @Mock
    private TargetColourVariantProductModel colourVariantProductModel;

    @Mock
    private TargetSizeVariantProductModel sizeVariantProductModel;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    private AbstractTargetProductData productData;

    @Before
    public void setUp() {
        given(sizeVariantProductModel.getBaseProduct()).willReturn(colourVariantProductModel);
        given(sizeVariantProductModel.getCode()).willReturn("P1000_Red_M");
        given(colourVariantProductModel.getCode()).willReturn("P1000_Red");
        doReturn(Boolean.TRUE).when(targetFeatureSwitchFacade).isConsolidatedStoreStockAvailable();
        productData = new TargetProductData();
    }

    @Test
    public void testPopulateWhenFeatureIsDisabled() {
        doReturn(Boolean.FALSE).when(targetFeatureSwitchFacade).isConsolidatedStoreStockAvailable();
        final List<ERec> endecaResult = ImmutableList.of(
                createStoreStock("0", "P1000_Red_S"),
                createStoreStock("1", "P1000_Red_M"),
                createStoreStock("2", "P1000_Red_L"));
        doReturn(endecaResult).when(populator).searchEndeca(sizeVariantProductModel);

        populator.populate(sizeVariantProductModel, productData);

        assertThat(productData.getConsolidatedStoreStock()).isNull();
    }

    @Test
    public void testPopulateStoreStockWhenNoRecordFound() {
        doReturn(Collections.emptyList()).when(populator).searchEndeca(sizeVariantProductModel);

        populator.populate(sizeVariantProductModel, productData);

        assertThat(productData.getConsolidatedStoreStock()).isNull();
    }

    @Test
    public void testPopulateStoreStockWhenZeroStoreStockForSizeVariant() {
        final List<ERec> endecaResult = ImmutableList.of(
                createStoreStock("1", "P1000_Red_S"),
                createStoreStock("0", "P1000_Red_M"),
                createStoreStock("2", "P1000_Red_L"));
        doReturn(endecaResult).when(populator).searchEndeca(sizeVariantProductModel);

        populator.populate(sizeVariantProductModel, productData);

        assertThat(productData.getConsolidatedStoreStock().getStockLevel()).isEqualTo(0);
        assertThat(productData.getConsolidatedStoreStock().getStockLevelStatus())
                .isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    @Test
    public void testPopulateStoreStockWhenStoreStockForSizeVariant() {
        final List<ERec> endecaResult = ImmutableList.of(
                createStoreStock("2", "P1000_Red_S"),
                createStoreStock("1", "P1000_Red_M"),
                createStoreStock("2", "P1000_Red_L"));
        doReturn(endecaResult).when(populator).searchEndeca(sizeVariantProductModel);

        populator.populate(sizeVariantProductModel, productData);

        assertThat(productData.getConsolidatedStoreStock().getStockLevel()).isEqualTo(1);
        assertThat(productData.getConsolidatedStoreStock().getStockLevelStatus())
                .isEqualTo(StockLevelStatus.INSTOCK);
    }

    @Test
    public void testPopulateStoreStockWhenNoStoreStockForColourVariant() {
        final List<ERec> endecaResult = ImmutableList.of(
                createStoreStock("0", "P1000_Red_S"),
                createStoreStock("0", "P1000_Red_M"),
                createStoreStock("", "P1000_Red_L"));
        doReturn(endecaResult).when(populator).searchEndeca(colourVariantProductModel);

        populator.populate(colourVariantProductModel, productData);

        assertThat(productData.getConsolidatedStoreStock().getStockLevel()).isEqualTo(0);
        assertThat(productData.getConsolidatedStoreStock().getStockLevelStatus())
                .isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    @Test
    public void testPopulateStoreStockWhenStoreStockForColourVariant() {
        final List<ERec> endecaResult = ImmutableList.of(
                createStoreStock("0", "P1000_Red_S"),
                createStoreStock("1", "P1000_Red_M"),
                createStoreStock("2", "P1000_Red_L"));
        doReturn(endecaResult).when(populator).searchEndeca(colourVariantProductModel);

        populator.populate(colourVariantProductModel, productData);

        assertThat(productData.getConsolidatedStoreStock().getStockLevelStatus())
                .isEqualTo(StockLevelStatus.INSTOCK);
        assertThat(productData.getConsolidatedStoreStock().getStockLevel()).isEqualTo(1);//will be the stock of the first found
    }

    @Test
    public void testPopulateEndecaSearchStateDataForSizeVariant() {
        final EndecaSearchStateData searchStateData = populator
                .populateEndecaSearchStateData(sizeVariantProductModel);
        verifyEndecaSearchData(searchStateData);
    }

    @Test
    public void testPopulateEndecaSearchStateDataForColourVariant() {
        final EndecaSearchStateData searchStateData = populator
                .populateEndecaSearchStateData(colourVariantProductModel);
        verifyEndecaSearchData(searchStateData);
    }

    @Test
    public void testSearchEndeca() {
        final EndecaSearchStateData searchStateData = mock(EndecaSearchStateData.class);
        final ENEQueryResults eNEQueryResults = mock(ENEQueryResults.class);
        final Navigation navigation = mock(Navigation.class);
        final ERecList eRecList = mock(ERecList.class);
        given(navigation.getERecs()).willReturn(eRecList);
        given(eNEQueryResults.getNavigation()).willReturn(navigation);
        doReturn(searchStateData).when(populator).populateEndecaSearchStateData(sizeVariantProductModel);
        doReturn(eNEQueryResults).when(endecaProductQueryBuilder).getQueryResults(searchStateData, null, 50);
        populator.searchEndeca(sizeVariantProductModel);
        verify(endecaProductQueryBuilder).getQueryResults(searchStateData, null, 50);
    }

    @Test
    public void testSearchEndecaWithNullResult() {
        final EndecaSearchStateData searchStateData = mock(EndecaSearchStateData.class);
        final ENEQueryResults eNEQueryResults = mock(ENEQueryResults.class);
        given(eNEQueryResults.getNavigation()).willReturn(null);
        doReturn(searchStateData).when(populator).populateEndecaSearchStateData(sizeVariantProductModel);
        doReturn(eNEQueryResults).when(endecaProductQueryBuilder).getQueryResults(searchStateData, null, 50);
        final List results = populator.searchEndeca(sizeVariantProductModel);
        assertThat(results).isEmpty();
    }

    @Test
    public void testSearchEndecaWithNullQueryResult() {
        final EndecaSearchStateData searchStateData = mock(EndecaSearchStateData.class);
        doReturn(searchStateData).when(populator).populateEndecaSearchStateData(sizeVariantProductModel);
        doReturn(null).when(endecaProductQueryBuilder).getQueryResults(searchStateData, null, 50);
        final List results = populator.searchEndeca(sizeVariantProductModel);
        assertThat(results).isEmpty();
    }

    private ERec createStoreStock(final String stockLevel, final String sizeVariantCode) {
        final ERec eRec = mock(ERec.class);
        final PropertyMap propertyMap = mock(PropertyMap.class);
        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isFindInStoreStockVisibilityEnabled();
        given(propertyMap.get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY_IN_STORE))
                .willReturn(stockLevel);
        given(propertyMap.get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE))
                .willReturn(sizeVariantCode);
        given(eRec.getProperties()).willReturn(propertyMap);
        return eRec;
    }

    private void verifyEndecaSearchData(final EndecaSearchStateData searchStateData) {
        assertThat(searchStateData.getFieldListConfig()).isEqualTo(EndecaConstants.FieldList.STORESTOCK_FIELDLIST);
        assertThat(searchStateData.getNuRollupField()).isNull();
        assertThat(searchStateData.getMatchMode()).isEqualTo(EndecaConstants.MATCH_MODE_ANY);
        assertThat(searchStateData.getSearchTerm()).containsExactly("P1000_Red");
        assertThat(searchStateData.getSearchField()).isEqualTo(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING);
        assertThat(searchStateData.getNpSearchState()).isEqualTo(EndecaConstants.ENDECA_N_0);
    }
}

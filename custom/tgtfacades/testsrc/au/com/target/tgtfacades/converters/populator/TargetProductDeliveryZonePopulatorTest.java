/**
 *
 */
package au.com.target.tgtfacades.converters.populator;


import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.ZoneDeliveryModeData;
import de.hybris.platform.commercefacades.url.impl.DefaultProductDataUrlResolver;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeValueModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.order.TargetPreOrderService;
import au.com.target.tgtcore.price.TargetCommercePriceService;
import au.com.target.tgtcore.price.data.PriceRangeInformation;
import au.com.target.tgtcore.product.TargetSizeTypeService;
import au.com.target.tgtcore.product.data.ProductDisplayType;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.order.converters.populator.TargetZoneDeliveryModePopulator;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.impl.TargetProductFacadeImpl;
import au.com.target.tgtfacades.url.impl.TargetSizeTypeModelUrlResolver;
import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;
import au.com.target.tgtfacades.url.transform.UrlTransformer;
import au.com.target.tgtlayby.purchase.PurchaseOptionService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductDeliveryZonePopulatorTest {

    private static final String DISCLAIMER = "disclaimer";
    private static final String EXPRESS_DELIVERY = "express-delivery";
    private static final String DIGITAL_DELIVERY = "digital-delivery";
    private static final String CNC_DELIVERY = "cnc-delivery";
    private static final String HOME_DELIVERY = "home-delivery";

    private static final String FREE_CNC_DELIVERY = "FREE cnc-delivery";
    private static final String FREE_HOME_DELIVERY = "FREE home-delivery";

    private final String TEST_MESSAGE = "test-message";
    private final String TEST_SHORT_MESSAGE = "test-short-message";
    private final String TEST_PRE_ORDER_MESSAGE = "test-preorder-message";

    @InjectMocks
    private final TargetProductDeliveryZonePopulator targetProductPopulator = new TargetProductDeliveryZonePopulator();

    @Mock
    private ModelService mockModelService;

    @Mock
    private TargetDeliveryModeHelper deliveryModeHelper;

    @Mock
    private PurchaseOptionService purchaseOptionService;

    @Mock
    private TargetDeliveryService targetDeliveryService;

    @Mock
    private DefaultProductDataUrlResolver mockProductDataUrlResolver;

    private List<TargetZoneDeliveryModeModel> targetZoneDeliveryModes;

    private TargetZoneDeliveryModeModel homeDelivery;
    private TargetZoneDeliveryModeModel cncDelivery;
    private TargetZoneDeliveryModeModel expressDelivery;
    private TargetZoneDeliveryModeModel digitalDelivery;

    @Mock
    private TargetSizeTypeService sizeTypeService;

    @Mock
    private AbstractTargetZoneDeliveryModeValueModel modeValueModel;

    @Mock
    private TargetSalesApplicationService targetSalesApplicationService;

    @Mock
    private Converter<ZoneDeliveryModeModel, ZoneDeliveryModeData> targetZoneDeliveryModeConverter;

    @Mock
    private TargetCommercePriceService targetCommercePriceService;

    @Mock
    private PriceRangeInformation priceRangeInformation;

    @Mock
    private TargetProductFacadeImpl targetProductFacade;

    @Mock
    private TargetPreOrderService targetPreOrderService;

    @Mock
    private Converter<ZoneDeliveryModeModel, ZoneDeliveryModeData> targetZoneDeliveryModeTestConverter;


    @Before
    public void setUp() {
        targetZoneDeliveryModes = new ArrayList<>();
        homeDelivery = new TargetZoneDeliveryModeModel();
        homeDelivery.setCode(HOME_DELIVERY);
        homeDelivery.setDisplayOrder(Integer.valueOf(1));
        final TargetZoneDeliveryModeValueModel targetZoneDeliveryModeValue1 = new TargetZoneDeliveryModeValueModel();
        homeDelivery.setValues(Collections.singleton(((ZoneDeliveryModeValueModel)targetZoneDeliveryModeValue1)));
        targetZoneDeliveryModes.add(homeDelivery);
        cncDelivery = new TargetZoneDeliveryModeModel();
        cncDelivery.setCode(CNC_DELIVERY);
        cncDelivery.setDisplayOrder(Integer.valueOf(2));
        cncDelivery.setIsDeliveryToStore(Boolean.TRUE);
        final TargetZoneDeliveryModeValueModel targetZoneDeliveryModeValue2 = new TargetZoneDeliveryModeValueModel();
        cncDelivery.setValues(Collections.singleton(((ZoneDeliveryModeValueModel)targetZoneDeliveryModeValue2)));
        targetZoneDeliveryModes.add(cncDelivery);

        expressDelivery = new TargetZoneDeliveryModeModel();
        expressDelivery.setCode(EXPRESS_DELIVERY);
        expressDelivery.setDisplayOrder(Integer.valueOf(3));
        final TargetZoneDeliveryModeValueModel targetZoneDeliveryModeValue3 = new TargetZoneDeliveryModeValueModel();
        expressDelivery.setValues(Collections.singleton(((ZoneDeliveryModeValueModel)targetZoneDeliveryModeValue3)));
        targetZoneDeliveryModes.add(expressDelivery);

        digitalDelivery = new TargetZoneDeliveryModeModel();
        digitalDelivery.setCode(DIGITAL_DELIVERY);
        digitalDelivery.setDisplayOrder(Integer.valueOf(3));
        digitalDelivery.setIsDigital(Boolean.TRUE);
        final TargetZoneDeliveryModeValueModel targetZoneDeliveryModeValue4 = new TargetZoneDeliveryModeValueModel();
        digitalDelivery.setValues(Collections.singleton(((ZoneDeliveryModeValueModel)targetZoneDeliveryModeValue4)));

        final TargetSizeTypeModelUrlResolver resolver = new TargetSizeTypeModelUrlResolver();

        final TargetZoneDeliveryModeData deliveryModeData4 = new TargetZoneDeliveryModeData();
        deliveryModeData4.setCode(DIGITAL_DELIVERY);
        deliveryModeData4.setName(DIGITAL_DELIVERY);

        given(targetZoneDeliveryModeConverter.convert(digitalDelivery)).willReturn(deliveryModeData4);

        final List<TargetZoneDeliveryModeData> deliveryModeDatas = new ArrayList<>();
        final TargetZoneDeliveryModeData deliveryModeData1 = new TargetZoneDeliveryModeData();
        final TargetZoneDeliveryModeData deliveryModeData2 = new TargetZoneDeliveryModeData();
        final TargetZoneDeliveryModeData deliveryModeData3 = new TargetZoneDeliveryModeData();
        deliveryModeData1.setCode(HOME_DELIVERY);
        deliveryModeData1.setName(HOME_DELIVERY);
        deliveryModeData2.setCode(CNC_DELIVERY);
        deliveryModeData2.setName(CNC_DELIVERY);
        deliveryModeData3.setCode(EXPRESS_DELIVERY);
        deliveryModeData3.setName(EXPRESS_DELIVERY);
        deliveryModeDatas.add(deliveryModeData1);
        deliveryModeDatas.add(deliveryModeData2);
        deliveryModeDatas.add(deliveryModeData3);
        for (int i = 0; i < targetZoneDeliveryModes.size(); i++) {
            given(targetZoneDeliveryModeConverter.convert(targetZoneDeliveryModes.get(i)))
                    .willReturn(deliveryModeDatas.get(i));
        }

        resolver.setPattern("/size-chart/{sizetype.code}");
        resolver.setUrlTokenTransformers(new ArrayList<UrlTokenTransformer>());
        resolver.setUrlTransformers(new ArrayList<UrlTransformer>());

        targetProductPopulator.setSizeTypeModelUrlResolver(resolver);
        given(targetProductFacade.getBaseTargetProduct(Mockito.any(AbstractTargetVariantProductModel.class)))
                .willCallRealMethod();
    }

    @Test
    public void testPopulateDeliveryData() {
        final String testMsg = "test-message";
        final String testShortMsg = "test-short-message";

        final TargetProductModel productModel = mock(TargetProductModel.class);
        given(targetCommercePriceService.getPriceRangeInfoForProduct(productModel)).willReturn(
                priceRangeInformation);
        final TargetProductData productData = new TargetProductData();
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        given(targetDeliveryService.getAllCurrentlyActiveDeliveryModes(SalesApplication.WEB)).willReturn(
                targetZoneDeliveryModes);

        given(targetDeliveryService.getAllApplicableDeliveryModesForProduct(productModel)).willReturn(
                targetZoneDeliveryModes);
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(homeDelivery, productModel))
                .willReturn(Collections.singletonList(modeValueModel));
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(cncDelivery, productModel))
                .willReturn(Collections.singletonList(modeValueModel));
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(expressDelivery, productModel))
                .willReturn(Collections.singletonList(modeValueModel));
        willReturn(Boolean.TRUE).given(deliveryModeHelper).isProductAvailableForDeliveryMode(productModel,
                homeDelivery);
        willReturn(Boolean.TRUE).given(deliveryModeHelper).isProductAvailableForDeliveryMode(productModel, cncDelivery);
        willReturn(Boolean.TRUE).given(deliveryModeHelper).isProductAvailableForDeliveryMode(productModel,
                expressDelivery);

        given(modeValueModel.getMessage()).willReturn(testMsg);
        given(modeValueModel.getShortMessage()).willReturn(testShortMsg);
        given(modeValueModel.getDisclaimer()).willReturn(DISCLAIMER);

        targetProductPopulator.populate(productModel, productData);

        assertThat(productData.isProductDeliveryToStoreOnly()).isFalse();
        assertThat(productData.isAllowDeliverToStore()).isTrue();

        assertThat(CollectionUtils.isNotEmpty(productData.getDeliveryModes())).isTrue();

        for (final TargetZoneDeliveryModeData modeData : productData.getDeliveryModes()) {
            assertThat(modeData.getLongDescription()).isEqualTo(testMsg);
            assertThat(modeData.getShortDescription()).isEqualTo(testShortMsg);
            assertThat(modeData.getDisclaimer()).isEqualTo(DISCLAIMER);
        }
    }

    @Test
    public void testPopulateDeliveryDataNoCNC() {
        final String testMsg = "test-message";
        final String testShortMsg = "test-short-message";

        final TargetProductModel productModel = mock(TargetProductModel.class);
        given(targetCommercePriceService.getPriceRangeInfoForProduct(productModel)).willReturn(
                priceRangeInformation);
        final TargetProductData productData = new TargetProductData();
        targetZoneDeliveryModes = new ArrayList<>();
        targetZoneDeliveryModes.add(homeDelivery);
        targetZoneDeliveryModes.add(expressDelivery);

        final List<TargetZoneDeliveryModeModel> activetargetZoneDeliveryModes = new ArrayList<>();
        activetargetZoneDeliveryModes.add(homeDelivery);
        activetargetZoneDeliveryModes.add(expressDelivery);
        activetargetZoneDeliveryModes.add(cncDelivery);

        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        given(targetDeliveryService.getAllCurrentlyActiveDeliveryModes(SalesApplication.WEB)).willReturn(
                activetargetZoneDeliveryModes);

        given(targetDeliveryService.getAllApplicableDeliveryModesForProduct(productModel)).willReturn(
                targetZoneDeliveryModes);
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(homeDelivery, productModel))
                .willReturn(Collections.singletonList(modeValueModel));
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(expressDelivery, productModel))
                .willReturn(Collections.singletonList(modeValueModel));
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(cncDelivery, productModel))
                .willReturn(Collections.singletonList(modeValueModel));
        willReturn(Boolean.TRUE).given(deliveryModeHelper).isProductAvailableForDeliveryMode(productModel,
                homeDelivery);
        willReturn(Boolean.TRUE).given(deliveryModeHelper).isProductAvailableForDeliveryMode(productModel,
                expressDelivery);

        given(modeValueModel.getMessage()).willReturn(testMsg);
        given(modeValueModel.getShortMessage()).willReturn(testShortMsg);
        given(modeValueModel.getDisclaimer()).willReturn(DISCLAIMER);

        targetProductPopulator.populate(productModel, productData);

        assertThat(productData.isProductDeliveryToStoreOnly()).isFalse();
        assertThat(productData.isAllowDeliverToStore()).isFalse();
        assertThat(CollectionUtils.isNotEmpty(productData.getDeliveryModes())).isTrue();

        for (final TargetZoneDeliveryModeData modeData : productData.getDeliveryModes()) {
            assertThat(modeData.getLongDescription()).isEqualTo(testMsg);
            assertThat(modeData.getShortDescription()).isEqualTo(testShortMsg);
            assertThat(modeData.getDisclaimer()).isEqualTo(DISCLAIMER);
        }
    }

    @Test
    public void testPopulateDeliveryDataOnlyCNC() {
        final String testMsg = "test-message";
        final String testShortMsg = "test-short-message";

        final TargetProductModel productModel = mock(TargetProductModel.class);
        given(targetCommercePriceService.getPriceRangeInfoForProduct(productModel)).willReturn(
                priceRangeInformation);
        final TargetProductData productData = new TargetProductData();
        targetZoneDeliveryModes = new ArrayList<>();
        targetZoneDeliveryModes.add(cncDelivery);
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        given(targetDeliveryService.getAllCurrentlyActiveDeliveryModes(SalesApplication.WEB)).willReturn(
                targetZoneDeliveryModes);

        given(targetDeliveryService.getAllApplicableDeliveryModesForProduct(productModel)).willReturn(
                targetZoneDeliveryModes);
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(cncDelivery, productModel))
                .willReturn(Collections.singletonList(modeValueModel));
        willReturn(Boolean.TRUE).given(deliveryModeHelper).isProductAvailableForDeliveryMode(productModel, cncDelivery);

        given(modeValueModel.getMessage()).willReturn(testMsg);
        given(modeValueModel.getShortMessage()).willReturn(testShortMsg);
        given(modeValueModel.getDisclaimer()).willReturn(DISCLAIMER);

        targetProductPopulator.populate(productModel, productData);

        assertThat(productData.isProductDeliveryToStoreOnly()).isTrue();
        assertThat(productData.isAllowDeliverToStore()).isTrue();
        assertThat(CollectionUtils.isNotEmpty(productData.getDeliveryModes())).isTrue();

        for (final TargetZoneDeliveryModeData modeData : productData.getDeliveryModes()) {
            assertThat(modeData.getLongDescription()).isEqualTo(testMsg);
            assertThat(modeData.getShortDescription()).isEqualTo(testShortMsg);
            assertThat(modeData.getDisclaimer()).isEqualTo(DISCLAIMER);
        }
    }


    @Test
    public void testPopulateDigitalDeliveryData() {
        final String testMsg = "test-message";
        final String testShortMsg = "test-short-message";

        final TargetProductModel productModel = mock(TargetProductModel.class);
        given(targetCommercePriceService.getPriceRangeInfoForProduct(productModel)).willReturn(
                priceRangeInformation);
        final TargetProductData productData = new TargetProductData();
        given(deliveryModeHelper.getDigitalDeliveryMode(productModel)).willReturn(digitalDelivery);

        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(digitalDelivery, productModel))
                .willReturn(Collections.singletonList(modeValueModel));

        given(modeValueModel.getMessage()).willReturn(testMsg);
        given(modeValueModel.getShortMessage()).willReturn(testShortMsg);
        given(modeValueModel.getDisclaimer()).willReturn(DISCLAIMER);

        targetProductPopulator.populate(productModel, productData);

        assertThat(CollectionUtils.isNotEmpty(productData.getDeliveryModes())).isTrue();
        assertThat(productData.isProductDeliveryToStoreOnly()).isFalse();
        assertThat(productData.isAllowDeliverToStore()).isFalse();

        for (final TargetZoneDeliveryModeData modeData : productData.getDeliveryModes()) {
            assertThat(modeData.getLongDescription()).isEqualTo(testMsg);
            assertThat(modeData.getShortDescription()).isEqualTo(testShortMsg);
            assertThat(modeData.getDisclaimer()).isEqualTo(DISCLAIMER);
            assertThat(modeData.isAvailable()).isTrue();
            assertThat(modeData.isDeliveryToStore()).isFalse();
            assertThat(modeData.getName()).isEqualTo(DIGITAL_DELIVERY);
        }
    }

    @Test
    public void testPopulateDeliveryDataWhenNoMDVAvailable() {
        final TargetProductModel productModel = mock(TargetProductModel.class);
        given(targetCommercePriceService.getPriceRangeInfoForProduct(productModel)).willReturn(
                priceRangeInformation);
        final TargetProductData productData = new TargetProductData();
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        given(targetDeliveryService.getAllCurrentlyActiveDeliveryModes(SalesApplication.WEB)).willReturn(
                targetZoneDeliveryModes);

        given(targetDeliveryService.getAllApplicableDeliveryModesForProduct(productModel)).willReturn(
                targetZoneDeliveryModes);
        given(
                targetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(homeDelivery, productModel))
                        .willReturn(null);

        targetProductPopulator.populate(productModel, productData);

        assertThat(CollectionUtils.isNotEmpty(productData.getDeliveryModes())).isTrue();

        for (final TargetZoneDeliveryModeData modeData : productData.getDeliveryModes()) {
            assertThat(modeData.isAvailable()).isEqualTo(false);
        }
    }

    @Test
    public void testPopulateDeliveryModeDataWhenFreeDeliveryIsApplicable() {
        final TargetProductModel productModel = mock(TargetProductModel.class);
        given(targetCommercePriceService.getPriceRangeInfoForProduct(productModel)).willReturn(
                priceRangeInformation);
        given(priceRangeInformation.getFromPrice()).willReturn(new Double(10.0));
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        given(targetDeliveryService.getAllCurrentlyActiveDeliveryModes(SalesApplication.WEB)).willReturn(
                targetZoneDeliveryModes);
        mockDeliveryModeInfo(productModel);
        final TargetProductData productData = new TargetProductData();
        targetProductPopulator.populate(productModel, productData);
        assertDeliveryModeNames(productData.getDeliveryModes());
    }

    @Test
    public void testPopulateDeliveryModeDataWhenFreeDeliveryIsNotApplicableForAPriceRange() {
        final TargetColourVariantProductModel productModel = mock(TargetColourVariantProductModel.class);
        final TargetProductModel baseProductModel = mock(TargetProductModel.class);
        given(productModel.getBaseProduct()).willReturn(baseProductModel);
        given(targetCommercePriceService.getPriceRangeInfoForProduct(baseProductModel)).willReturn(
                priceRangeInformation);
        given(priceRangeInformation.getFromPrice()).willReturn(new Double(20.0));
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        given(targetDeliveryService.getAllCurrentlyActiveDeliveryModes(SalesApplication.WEB)).willReturn(
                targetZoneDeliveryModes);
        mockDeliveryModeInfo(productModel);
        final TargetProductData productData = new TargetProductData();
        targetProductPopulator.populate(productModel, productData);
        assertDeliveryModeNamesWhenPriceInfoMissing(productData.getDeliveryModes());
    }

    @Test
    public void testPopulateDeliveryModeDataWhenProductPriceInfoIsMissing() {
        final TargetProductModel productModel = mock(TargetProductModel.class);
        given(targetCommercePriceService.getPriceRangeInfoForProduct(productModel)).willReturn(
                null);
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        given(targetDeliveryService.getAllCurrentlyActiveDeliveryModes(SalesApplication.WEB)).willReturn(
                targetZoneDeliveryModes);
        mockDeliveryModeInfo(productModel);
        final TargetProductData productData = new TargetProductData();
        targetProductPopulator.populate(productModel, productData);
        assertDeliveryModeNamesWhenPriceInfoMissing(productData.getDeliveryModes());
    }

    @Test
    public void testPopulateDeliveryModeDataWhenProductsFromPriceIsMissing() {
        final TargetProductModel productModel = mock(TargetProductModel.class);
        given(targetCommercePriceService.getPriceRangeInfoForProduct(productModel)).willReturn(
                priceRangeInformation);
        given(priceRangeInformation.getFromPrice()).willReturn(null);
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        given(targetDeliveryService.getAllCurrentlyActiveDeliveryModes(SalesApplication.WEB)).willReturn(
                targetZoneDeliveryModes);
        mockDeliveryModeInfo(productModel);
        final TargetProductData productData = new TargetProductData();
        targetProductPopulator.populate(productModel, productData);
        assertDeliveryModeNamesWhenPriceInfoMissing(productData.getDeliveryModes());
    }

    /**
     * Method will verify pre-order tool-tip delivery message for pre-order product.
     */
    @Test
    public void testPopulateVerifyPreOrderMessage() {

        final TargetColourVariantProductModel productModel = mock(TargetColourVariantProductModel.class);
        given(targetCommercePriceService.getPriceRangeInfoForProduct(productModel)).willReturn(
                priceRangeInformation);
        final TargetProductListerData productData = new TargetProductListerData();
        targetZoneDeliveryModes = new ArrayList<>();
        targetZoneDeliveryModes.add(homeDelivery);
        targetZoneDeliveryModes.add(expressDelivery);

        final List<TargetZoneDeliveryModeModel> activetargetZoneDeliveryModes = new ArrayList<>();
        activetargetZoneDeliveryModes.add(homeDelivery);
        activetargetZoneDeliveryModes.add(expressDelivery);

        preOrderMessageData(productModel, activetargetZoneDeliveryModes);

        given(targetPreOrderService.getProductDisplayType(productModel))
                .willReturn(ProductDisplayType.PREORDER_AVAILABLE);

        given(modeValueModel.getMessage()).willReturn(TEST_MESSAGE);
        given(modeValueModel.getPreOrderMessage()).willReturn(TEST_PRE_ORDER_MESSAGE);
        given(modeValueModel.getDisclaimer()).willReturn(DISCLAIMER);

        final TargetProductModel targetProductModel = mock(TargetProductModel.class);
        given(productModel.getBaseProduct()).willReturn(targetProductModel);

        targetProductPopulator.populate(productModel, productData);

        for (final TargetZoneDeliveryModeData modeData : productData.getDeliveryModes()) {
            assertThat(modeData.getLongDescription()).isEqualTo(TEST_MESSAGE);
            assertThat(modeData.getShortDescription()).isEqualTo(TEST_PRE_ORDER_MESSAGE);
            assertThat(modeData.getDisclaimer()).isEqualTo(DISCLAIMER);
        }
    }

    /**
     * Method will verify pre-order tool-tip delivery short message for pre-order product. This test case will check if
     * pre-order message is set or not. If not present then it will set short message.
     */
    @Test
    public void testPopulateVerifyNoPreOrderMessage() {

        final TargetColourVariantProductModel productModel = mock(TargetColourVariantProductModel.class);
        given(targetCommercePriceService.getPriceRangeInfoForProduct(productModel)).willReturn(
                priceRangeInformation);
        final TargetProductListerData productData = new TargetProductListerData();
        targetZoneDeliveryModes = new ArrayList<>();
        targetZoneDeliveryModes.add(homeDelivery);
        targetZoneDeliveryModes.add(expressDelivery);

        final List<TargetZoneDeliveryModeModel> activetargetZoneDeliveryModes = new ArrayList<>();
        activetargetZoneDeliveryModes.add(homeDelivery);
        activetargetZoneDeliveryModes.add(expressDelivery);

        preOrderMessageData(productModel, activetargetZoneDeliveryModes);

        given(modeValueModel.getMessage()).willReturn(TEST_MESSAGE);
        given(modeValueModel.getShortMessage()).willReturn(TEST_SHORT_MESSAGE);
        given(modeValueModel.getDisclaimer()).willReturn(DISCLAIMER);

        final TargetProductModel targetProductModel = mock(TargetProductModel.class);
        given(productModel.getBaseProduct()).willReturn(targetProductModel);

        given(targetPreOrderService.getProductDisplayType(productModel))
                .willReturn(ProductDisplayType.PREORDER_AVAILABLE);

        targetProductPopulator.populate(productModel, productData);

        for (final TargetZoneDeliveryModeData modeData : productData.getDeliveryModes()) {
            assertThat(modeData.getLongDescription()).isEqualTo(TEST_MESSAGE);
            assertThat(modeData.getShortDescription()).isEqualTo(TEST_SHORT_MESSAGE);
            assertThat(modeData.getDisclaimer()).isEqualTo(DISCLAIMER);
        }
    }

    /**
     * This method will verify if the product is not pre-order product.
     */
    @Test
    public void testPopulateVerifyPreOrderMessageForNonPreOrderProduct() {

        final TargetColourVariantProductModel productModel = mock(TargetColourVariantProductModel.class);
        given(targetCommercePriceService.getPriceRangeInfoForProduct(productModel)).willReturn(
                priceRangeInformation);
        final TargetProductListerData productData = new TargetProductListerData();
        targetZoneDeliveryModes = new ArrayList<>();
        targetZoneDeliveryModes.add(homeDelivery);
        targetZoneDeliveryModes.add(expressDelivery);

        final List<TargetZoneDeliveryModeModel> activetargetZoneDeliveryModes = new ArrayList<>();
        activetargetZoneDeliveryModes.add(homeDelivery);
        activetargetZoneDeliveryModes.add(expressDelivery);

        preOrderMessageData(productModel, activetargetZoneDeliveryModes);

        given(targetPreOrderService.getProductDisplayType(productModel))
                .willReturn(ProductDisplayType.COMING_SOON);

        given(modeValueModel.getMessage()).willReturn(TEST_MESSAGE);
        given(modeValueModel.getShortMessage()).willReturn(TEST_SHORT_MESSAGE);
        given(modeValueModel.getDisclaimer()).willReturn(DISCLAIMER);

        final TargetProductModel targetProductModel = mock(TargetProductModel.class);
        given(productModel.getBaseProduct()).willReturn(targetProductModel);

        targetProductPopulator.populate(productModel, productData);

        for (final TargetZoneDeliveryModeData modeData : productData.getDeliveryModes()) {
            assertThat(modeData.getLongDescription()).isEqualTo(TEST_MESSAGE);
            assertThat(modeData.getShortDescription()).isEqualTo(TEST_SHORT_MESSAGE);
            assertThat(modeData.getDisclaimer()).isEqualTo(DISCLAIMER);
        }
    }

    /**
     * @param productModel
     * @param activetargetZoneDeliveryModes
     */
    private void preOrderMessageData(final TargetColourVariantProductModel productModel,
            final List<TargetZoneDeliveryModeModel> activetargetZoneDeliveryModes) {
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        given(targetDeliveryService.getAllCurrentlyActiveDeliveryModes(SalesApplication.WEB)).willReturn(
                activetargetZoneDeliveryModes);

        given(targetDeliveryService.getAllApplicableDeliveryModesForProduct(productModel)).willReturn(
                targetZoneDeliveryModes);
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(homeDelivery, productModel))
                .willReturn(Collections.singletonList(modeValueModel));
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(expressDelivery, productModel))
                .willReturn(Collections.singletonList(modeValueModel));

        willReturn(Boolean.TRUE).given(deliveryModeHelper).isProductAvailableForDeliveryMode(productModel,
                homeDelivery);
        willReturn(Boolean.TRUE).given(deliveryModeHelper).isProductAvailableForDeliveryMode(productModel,
                expressDelivery);
    }

    /**
     * Method to verify delivery mode names
     * 
     * @param deliveryModes
     */
    private void assertDeliveryModeNames(final List<TargetZoneDeliveryModeData> deliveryModes) {
        for (final TargetZoneDeliveryModeData deliveryModeData : deliveryModes) {
            if (deliveryModeData.getCode().equalsIgnoreCase(CNC_DELIVERY)) {
                assertThat(deliveryModeData.getName()).isEqualTo(FREE_CNC_DELIVERY);
            }
            if (deliveryModeData.getCode().equalsIgnoreCase(HOME_DELIVERY)) {
                assertThat(deliveryModeData.getName()).isEqualTo(FREE_HOME_DELIVERY);
            }
            if (deliveryModeData.getCode().equalsIgnoreCase(EXPRESS_DELIVERY)) {
                assertThat(deliveryModeData.getName()).isEqualTo(EXPRESS_DELIVERY);
            }
            if (deliveryModeData.getCode().equalsIgnoreCase(DIGITAL_DELIVERY)) {
                assertThat(deliveryModeData.getName()).isEqualTo(DIGITAL_DELIVERY);
            }
        }
    }

    /**
     * Method to verify delivery mode names when price information is missing for the product
     * 
     * @param deliveryModes
     */
    private void assertDeliveryModeNamesWhenPriceInfoMissing(final List<TargetZoneDeliveryModeData> deliveryModes) {
        for (final TargetZoneDeliveryModeData deliveryModeData : deliveryModes) {
            if (deliveryModeData.getCode().equalsIgnoreCase(CNC_DELIVERY)) {
                assertThat(deliveryModeData.getName()).isEqualTo(CNC_DELIVERY);
            }
            if (deliveryModeData.getCode().equalsIgnoreCase(HOME_DELIVERY)) {
                assertThat(deliveryModeData.getName()).isEqualTo(HOME_DELIVERY);
            }
            if (deliveryModeData.getCode().equalsIgnoreCase(EXPRESS_DELIVERY)) {
                assertThat(deliveryModeData.getName()).isEqualTo(EXPRESS_DELIVERY);
            }
            if (deliveryModeData.getCode().equalsIgnoreCase(DIGITAL_DELIVERY)) {
                assertThat(deliveryModeData.getName()).isEqualTo(DIGITAL_DELIVERY);
            }
        }
    }

    /**
     * Method to return delivery mode model for code
     * 
     * @param deliveryModelCode
     */
    private TargetZoneDeliveryModeModel getDeliveryModeForCode(final String deliveryModelCode) {
        for (final TargetZoneDeliveryModeModel deliveryMode : targetZoneDeliveryModes) {
            if (deliveryMode.getCode().equalsIgnoreCase(deliveryModelCode)) {
                return deliveryMode;
            }
        }
        return null;
    }

    class TestDeliveryModeConverter
            extends TargetZoneDeliveryModePopulator {

        public TargetZoneDeliveryModeData convert(final TargetZoneDeliveryModeModel zoneDeliveryModel)
                throws ConversionException {
            final TargetZoneDeliveryModeData zoneDeliverydata = new TargetZoneDeliveryModeData();
            zoneDeliverydata.setCode(zoneDeliveryModel.getCode());
            return zoneDeliverydata;
        }

        @Override
        public void populate(final TargetZoneDeliveryModeModel source, final TargetZoneDeliveryModeData target) {
            target.setCode(source.getCode());
        }
    }

    /**
     * Method to mock delivery mode information
     * 
     * @param productModel
     */
    private void mockDeliveryModeInfo(final ProductModel productModel) {
        willReturn(Boolean.TRUE).given(targetDeliveryService).isFreeDeliveryApplicableOnProductForDeliveryMode(
                productModel, 10, getDeliveryModeForCode(CNC_DELIVERY));
        willReturn(Boolean.TRUE).given(targetDeliveryService).isFreeDeliveryApplicableOnProductForDeliveryMode(
                productModel, 10, getDeliveryModeForCode(HOME_DELIVERY));
        willReturn(Boolean.FALSE).given(targetDeliveryService).isFreeDeliveryApplicableOnProductForDeliveryMode(
                productModel, 10, getDeliveryModeForCode(EXPRESS_DELIVERY));
        willReturn(Boolean.TRUE).given(targetDeliveryService).isFreeDeliveryApplicableOnProductForDeliveryMode(
                productModel, 10, getDeliveryModeForCode(DIGITAL_DELIVERY));
        final List<AbstractTargetZoneDeliveryModeValueModel> cncDeliveryValues = new ArrayList<>();
        cncDeliveryValues.add(mock(RestrictableTargetZoneDeliveryModeValueModel.class));
        final List<AbstractTargetZoneDeliveryModeValueModel> homeDeliveryValues = new ArrayList<>();
        homeDeliveryValues.add(mock(RestrictableTargetZoneDeliveryModeValueModel.class));
        final List<AbstractTargetZoneDeliveryModeValueModel> expressDeliveryValues = new ArrayList<>();
        expressDeliveryValues.add(mock(RestrictableTargetZoneDeliveryModeValueModel.class));
        final List<AbstractTargetZoneDeliveryModeValueModel> digitalDeliveryValues = new ArrayList<>();
        digitalDeliveryValues.add(mock(RestrictableTargetZoneDeliveryModeValueModel.class));
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(
                getDeliveryModeForCode(CNC_DELIVERY), productModel)).willReturn(cncDeliveryValues);
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(
                getDeliveryModeForCode(HOME_DELIVERY), productModel)).willReturn(homeDeliveryValues);
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(
                getDeliveryModeForCode(EXPRESS_DELIVERY), productModel)).willReturn(expressDeliveryValues);
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(
                getDeliveryModeForCode(DIGITAL_DELIVERY), productModel)).willReturn(digitalDeliveryValues);

        willReturn(Boolean.TRUE).given(deliveryModeHelper).isProductAvailableForDeliveryMode(productModel,
                getDeliveryModeForCode(CNC_DELIVERY));

        willReturn(Boolean.TRUE).given(deliveryModeHelper).isProductAvailableForDeliveryMode(productModel,
                getDeliveryModeForCode(HOME_DELIVERY));

        willReturn(Boolean.FALSE).given(deliveryModeHelper).isProductAvailableForDeliveryMode(productModel,
                getDeliveryModeForCode(EXPRESS_DELIVERY));

        willReturn(Boolean.FALSE).given(deliveryModeHelper).isProductAvailableForDeliveryMode(productModel,
                getDeliveryModeForCode(DIGITAL_DELIVERY));
    }
}

/**
 *
 */
package au.com.target.tgtfacades.converters.populator;


import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.url.impl.DefaultProductDataUrlResolver;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.ColourModel;
import au.com.target.tgtcore.model.GiftCardModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.order.TargetPreOrderService;
import au.com.target.tgtcore.price.TargetCommercePriceService;
import au.com.target.tgtcore.price.data.PriceRangeInformation;
import au.com.target.tgtcore.product.TargetSizeTypeService;
import au.com.target.tgtcore.product.data.ProductDisplayType;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.order.converters.populator.TargetZoneDeliveryModePopulator;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.impl.TargetProductFacadeImpl;
import au.com.target.tgtfacades.url.impl.TargetSizeTypeModelUrlResolver;
import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;
import au.com.target.tgtfacades.url.transform.UrlTransformer;
import au.com.target.tgtlayby.purchase.PurchaseOptionService;
import au.com.target.tgtutility.util.TargetDateUtil;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductPopulatorTest {

    @InjectMocks
    private final TargetProductPopulator targetProductPopulator = new TargetProductPopulator();

    @Mock
    private ModelService mockModelService;

    @Mock
    private PurchaseOptionService purchaseOptionService;

    @Mock
    private DefaultProductDataUrlResolver mockProductDataUrlResolver;

    @Mock
    private TargetSizeTypeService sizeTypeService;

    @Mock
    private TargetCommercePriceService targetCommercePriceService;

    @Mock
    private PriceRangeInformation priceRangeInformation;

    @Mock
    private TargetProductFacadeImpl targetProductFacade;

    @Mock
    private TargetPreOrderService targetPreOrderService;

    @Before
    public void setUp() {
        final TargetSizeTypeModelUrlResolver resolver = new TargetSizeTypeModelUrlResolver();

        resolver.setPattern("/size-chart/{sizetype.code}");
        resolver.setUrlTokenTransformers(new ArrayList<UrlTokenTransformer>());
        resolver.setUrlTransformers(new ArrayList<UrlTransformer>());

        targetProductPopulator.setSizeTypeModelUrlResolver(resolver);
        given(targetProductFacade.getBaseTargetProduct(Mockito.any(AbstractTargetVariantProductModel.class)))
                .willCallRealMethod();
    }

    @Test
    public void testPopulateWithAllFields() {
        final String careInstructions = "Keep dry | Do not feed after midnight";
        final String productFeatures = "Lightweight | Invisible to radar";
        final String materials = "Cotton | Polyester";
        final String extraInfo = "Build a house before night time";
        final String baseProductUrl = "http://www.target.com.au/p/product";
        final Boolean showWhenOutOfStock = Boolean.FALSE;
        final Boolean bulkyBoardProduct = Boolean.FALSE;
        final Boolean preview = Boolean.FALSE;

        final GiftCardModel giftCard = new GiftCardModel();
        final Boolean isGiftCard = Boolean.TRUE;//assigning it to variable to make it more readable

        final TargetProductCategoryModel originalCategory = new TargetProductCategoryModel();
        originalCategory.setCode("W3456");

        final String brandName = "Brand name";
        final BrandModel brand = new BrandModel();
        brand.setName(brandName);

        final String productTypeCode = "Product Type Code";
        final ProductTypeModel productType = new ProductTypeModel();
        productType.setCode(productTypeCode);
        productType.setBulky(Boolean.FALSE);

        final String videoUrl = "http://www.youtube.com/embed/{0}?rel=0";
        final MediaModel media = Mockito.mock(MediaModel.class);
        given(media.getURL()).willReturn(videoUrl);

        given(mockProductDataUrlResolver.resolve(Mockito.any(ProductData.class))).willReturn(baseProductUrl);

        final TargetProductModel productModel = createTargetProductModel(careInstructions, productFeatures, materials,
                extraInfo, giftCard, brand, productType, media, showWhenOutOfStock, bulkyBoardProduct, preview);
        given(productModel.getOriginalCategory()).willReturn(originalCategory);
        given(targetCommercePriceService.getPriceRangeInfoForProduct(productModel)).willReturn(
                priceRangeInformation);
        given(priceRangeInformation.getFromPrice()).willReturn(new Double(10.0));
        final TargetProductData productData = new TargetProductData();
        targetProductPopulator.populate(productModel, productData);

        final List<String> careInstructionList = new ArrayList<>();
        careInstructionList.add("Keep dry");
        careInstructionList.add("Do not feed after midnight");

        final List<String> productFeatureList = new ArrayList<>();
        productFeatureList.add("Lightweight");
        productFeatureList.add("Invisible to radar");

        final List<String> materialList = new ArrayList<>();
        materialList.add("Cotton");
        materialList.add("Polyester");

        validateProductDataEquals(productData, careInstructionList, productFeatureList, materialList, extraInfo,
                isGiftCard, brandName, productTypeCode, videoUrl, baseProductUrl, showWhenOutOfStock,
                bulkyBoardProduct,
                preview, originalCategory);
    }

    @Test
    public void testPopulateWithAlternateValuesForAllFields() {
        final String careInstructions = "";
        final String productFeatures = "";
        final String materials = "";
        final String extraInfo = "";
        final String baseProductUrl = "http://www.target.com.au/p/product";
        final Boolean showWhenOutOfStock = Boolean.TRUE;
        final Boolean bulkyBoardProduct = Boolean.TRUE;
        final Boolean preview = Boolean.TRUE;

        //Test for item that is not a giftCard
        final GiftCardModel giftCard = null;
        final Boolean isGiftCard = Boolean.FALSE;

        final String brandName = "";
        final BrandModel brand = new BrandModel();
        brand.setName(brandName);

        final String productTypeCode = "";
        final ProductTypeModel productType = new ProductTypeModel();
        productType.setCode(productTypeCode);
        productType.setBulky(Boolean.FALSE);

        final String videoUrl = null;
        final MediaModel media = new MediaModel();
        media.setURL(videoUrl);

        given(mockProductDataUrlResolver.resolve(Mockito.any(ProductData.class))).willReturn(baseProductUrl);

        final TargetProductModel productModel = createTargetProductModel(careInstructions, productFeatures, materials,
                extraInfo, giftCard, brand, productType, media, showWhenOutOfStock, bulkyBoardProduct, preview);
        final TargetProductData productData = new TargetProductData();
        given(targetCommercePriceService.getPriceRangeInfoForProduct(productModel)).willReturn(
                priceRangeInformation);
        targetProductPopulator.populate(productModel, productData);

        validateProductDataEquals(productData, Collections.EMPTY_LIST, Collections.EMPTY_LIST, Collections.EMPTY_LIST,
                extraInfo, isGiftCard, brandName,
                productTypeCode, videoUrl, baseProductUrl, showWhenOutOfStock, bulkyBoardProduct, preview, null);
    }

    @Test
    public void testPopulateWithNullValuesForAllFields() {
        final TargetProductModel productModel = Mockito.mock(TargetProductModel.class);
        final TargetProductData productData = new TargetProductData();
        given(targetCommercePriceService.getPriceRangeInfoForProduct(productModel)).willReturn(
                priceRangeInformation);
        targetProductPopulator.populate(productModel, productData);

        assertThat(productData.getCareInstructions()).isEqualTo(Collections.EMPTY_LIST);
        assertThat(productData.getProductFeatures()).isEqualTo(Collections.EMPTY_LIST);
        assertThat(productData.getMaterials()).isEqualTo(Collections.EMPTY_LIST);
        assertThat(productData.getExtraInfo()).isNull();
        assertThat(productData.getBrand()).isNull();
        assertThat(productData.getProductType()).isNull();
        assertThat(productData.getYouTubeLink()).isNull();
    }

    @Test
    public void testPopulateWithAllFieldsFromVariant() {
        final String careInstructions = "Keep dry | Do not feed after midnight";
        final String productFeatures = "Lightweight | Invisible to radar";
        final String materials = "Cotton | Polyester";
        final String extraInfo = "Build a house before night time";
        final String baseProductUrl = "http://www.target.com.au/p/product";
        final String displayName = "Pretty Name";
        final Boolean showWhenOutOfStock = Boolean.FALSE;
        final Boolean bulkyBoardProduct = Boolean.FALSE;
        final Boolean preview = Boolean.FALSE;

        final GiftCardModel giftCard = new GiftCardModel();
        final Boolean isGiftCard = Boolean.TRUE;

        final String brandName = "Brand name";
        final BrandModel brand = new BrandModel();
        brand.setName(brandName);

        final String productTypeCode = "Product Type Code";
        final ProductTypeModel productType = new ProductTypeModel();
        productType.setCode(productTypeCode);
        productType.setBulky(Boolean.FALSE);

        final String videoUrl = "http://www.youtube.com/embed/{0}?rel=0";
        final MediaModel media = Mockito.mock(MediaModel.class);
        given(media.getURL()).willReturn(videoUrl);

        given(mockProductDataUrlResolver.resolve(Mockito.any(ProductData.class))).willReturn(baseProductUrl);

        final TargetProductModel productModel = createTargetProductModel(careInstructions, productFeatures, materials,
                extraInfo, giftCard, brand, productType, media, showWhenOutOfStock, bulkyBoardProduct, preview);
        final AbstractTargetVariantProductModel mockProductModel = Mockito
                .mock(AbstractTargetVariantProductModel.class);
        given(mockProductModel.getBaseProduct()).willReturn(productModel);
        given(mockProductModel.getDisplayName()).willReturn(displayName);

        final TargetProductData productData = new TargetProductData();
        given(targetCommercePriceService.getPriceRangeInfoForProduct(mockProductModel)).willReturn(
                priceRangeInformation);
        targetProductPopulator.populate(mockProductModel, productData);

        final List<String> careInstructionList = new ArrayList<>();
        careInstructionList.add("Keep dry");
        careInstructionList.add("Do not feed after midnight");

        final List<String> productFeatureList = new ArrayList<>();
        productFeatureList.add("Lightweight");
        productFeatureList.add("Invisible to radar");

        final List<String> materialList = new ArrayList<>();
        materialList.add("Cotton");
        materialList.add("Polyester");

        validateProductDataEquals(productData, careInstructionList, productFeatureList, materialList, extraInfo,
                isGiftCard, brandName, productTypeCode, videoUrl, baseProductUrl, showWhenOutOfStock,
                bulkyBoardProduct,
                preview, null);

        assertThat(productData.getName()).isEqualTo(displayName);
    }

    @Test
    public void testPopulateWithNullProductModel() {
        final TargetProductData productData = Mockito.mock(TargetProductData.class);

        targetProductPopulator.populate(null, productData);

        Mockito.verifyZeroInteractions(productData);
    }

    @Test
    public void testPopulateWithWrongProductModel() {
        final ProductModel productModel = Mockito.mock(ProductModel.class);
        final TargetProductData productData = Mockito.mock(TargetProductData.class);

        targetProductPopulator.populate(productModel, productData);

        Mockito.verifyZeroInteractions(productModel);
        Mockito.verifyZeroInteractions(productData);
    }

    private SizeTypeModel getSizeType() {
        final SizeTypeModel sizeType = new SizeTypeModel();
        sizeType.setCode("testSize");
        sizeType.setName("TestSize");
        sizeType.setIsSizeChartDisplay(Boolean.TRUE);
        sizeType.setIsDefault(Boolean.FALSE);
        return sizeType;
    }

    private SizeTypeModel getDefaultSizeType() {
        final SizeTypeModel sizeType = new SizeTypeModel();
        sizeType.setCode("defaultTestSize");
        sizeType.setName("DefaultTestSize");
        sizeType.setIsSizeChartDisplay(Boolean.TRUE);
        sizeType.setIsDefault(Boolean.TRUE);
        return sizeType;
    }

    @Test
    public void testPopulateSizeTypeUrlforBaseProductWithNullOrigCategory() {
        final TargetProductModel productModel = Mockito.mock(TargetProductModel.class);
        final TargetProductData productData = new TargetProductData();
        final TargetColourVariantProductModel colourVariantProductModel = new TargetColourVariantProductModel();
        final TargetSizeVariantProductModel sizeVariantProductModel = Mockito.mock(TargetSizeVariantProductModel.class);

        final Collection<VariantProductModel> colourVariants = new ArrayList<>();
        colourVariants.add(colourVariantProductModel);
        productModel.setVariants(colourVariants);

        final Collection<VariantProductModel> sizeVariants = new ArrayList<>();
        sizeVariants.add(sizeVariantProductModel);
        colourVariantProductModel.setVariants(sizeVariants);

        given(sizeTypeService.getDefaultSizeType()).willReturn(getDefaultSizeType());
        given(productModel.getOriginalCategory()).willReturn(null);
        given(productModel.getVariants()).willReturn(colourVariants);
        given(sizeVariantProductModel.getSizeClassification()).willReturn(null);

        targetProductPopulator.populate(productModel, productData);

        assertThat(productData.getSizeChartURL()).isEqualTo("/size-chart/defaultTestSize");
        assertThat(productData.getOriginalCategoryCode()).isNull();
    }

    @Test
    public void testPopulateSizeTypeReturnNullIfThereIsNoVarientProductFound() {
        final TargetProductModel productModel = Mockito.mock(TargetProductModel.class);
        final TargetProductData productData = new TargetProductData();
        targetProductPopulator.populate(productModel, productData);
        assertThat(productData.getSizeChartURL()).isNull();
    }

    @Test
    public void testPopulateSizeTypeReturnNullIfInvalidProductType() {
        final TargetProductModel productModel = Mockito.mock(TargetProductModel.class);
        final TargetProductData productData = new TargetProductData();
        final TargetSizeVariantProductModel sizeVariantProductModel = Mockito.mock(TargetSizeVariantProductModel.class);
        targetProductPopulator.populate(productModel, productData);
        Mockito.verifyZeroInteractions(sizeVariantProductModel);
    }

    @Test
    public void testPopulateSizeTypeUrl() {
        final TargetProductModel productModel = Mockito.mock(TargetProductModel.class);
        final TargetProductData productData = new TargetProductData();
        final TargetColourVariantProductModel colourVariantProductModel = new TargetColourVariantProductModel();
        final TargetSizeVariantProductModel sizeVariantProductModel = Mockito.mock(TargetSizeVariantProductModel.class);

        final Collection<VariantProductModel> colourVariants = new ArrayList<>();
        colourVariants.add(colourVariantProductModel);
        productModel.setVariants(colourVariants);

        final Collection<VariantProductModel> sizeVariants = new ArrayList<>();
        sizeVariants.add(sizeVariantProductModel);
        colourVariantProductModel.setVariants(sizeVariants);

        given(productModel.getVariants()).willReturn(colourVariants);
        given(sizeVariantProductModel.getSizeClassification()).willReturn(getSizeType());

        targetProductPopulator.populate(productModel, productData);

        assertThat(productData.getSizeChartURL()).isEqualTo("/size-chart/testSize");
    }

    @Test
    public void testSetProductHeroImageWhenOnlyHeroImageIsAvailable() {
        final String heroFormat = "product";
        final String heroUrl = "hero_image_url";
        final ImageData imageHero = new ImageData();
        imageHero.setFormat(heroFormat);
        imageHero.setUrl(heroUrl);

        final Collection<ImageData> images = new ArrayList<>(Arrays.asList(imageHero));
        final TargetProductData targetProductData = new TargetProductData();
        targetProductData.setImages(images);

        assertThat(targetProductData.getImageUrl()).isNull();
        targetProductPopulator.setImageUrl(targetProductData);
        assertThat(targetProductData.getImageUrl()).isEqualTo(heroUrl);
    }

    @Test
    public void testSetProductHeroImageWhenHeroImageIsNotAvailable() {
        final String listFormat = "list";
        final String listUrl = "list_image_url";
        final ImageData imageList = new ImageData();
        imageList.setFormat(listFormat);
        imageList.setUrl(listUrl);

        final Collection<ImageData> images = new ArrayList<>(Arrays.asList(imageList));
        final TargetProductData targetProductData = new TargetProductData();
        targetProductData.setImages(images);

        targetProductPopulator.setImageUrl(targetProductData);
        assertThat(targetProductData.getImageUrl()).isNull();
    }

    @Test
    public void testSetProductHeroImageWhenMultipleImagesAreAvailable() {
        final String heroFormat = "product";
        final String heroUrl = "hero_image_url";
        final ImageData imageHero = new ImageData();
        imageHero.setFormat(heroFormat);
        imageHero.setUrl(heroUrl);

        final String listFormat = "list";
        final String listUrl = "list_image_url";
        final ImageData imageList = new ImageData();
        imageList.setFormat(listFormat);
        imageList.setUrl(listUrl);

        final String zoomFormat = "zoom";
        final String zoomUrl = "zoom_image_url";
        final ImageData imageZoom = new ImageData();
        imageZoom.setFormat(zoomFormat);
        imageZoom.setUrl(zoomUrl);

        final Collection<ImageData> images = new ArrayList<>(Arrays.asList(imageZoom, imageHero, imageList));
        final TargetProductData targetProductData = new TargetProductData();
        targetProductData.setImages(images);

        assertThat(targetProductData.getImageUrl()).isNull();
        targetProductPopulator.setImageUrl(targetProductData);
        assertThat(targetProductData.getImageUrl()).isEqualTo(heroUrl);
    }

    @Test
    public void testPopulateSwatchforSizeVariantProduct() {
        final TargetProductData productData = new TargetProductData();
        final TargetColourVariantProductModel colourVariantProductModel = mock(TargetColourVariantProductModel.class);
        final TargetSizeVariantProductModel sizeVariantProductModel = mock(TargetSizeVariantProductModel.class);

        given(sizeTypeService.getDefaultSizeType()).willReturn(getDefaultSizeType());
        given(sizeVariantProductModel.getBaseProduct()).willReturn(colourVariantProductModel);
        given(sizeVariantProductModel.getSizeClassification()).willReturn(null);
        final ColourModel swatch = mock(ColourModel.class);
        given(swatch.getDisplay()).willReturn(Boolean.TRUE);
        given(colourVariantProductModel.getSwatch()).willReturn(swatch);
        given(colourVariantProductModel.getSwatchName()).willReturn("red");

        targetProductPopulator.populate(sizeVariantProductModel, productData);

        assertThat(productData.getSwatch()).isEqualTo("red");
    }

    @Test
    public void testPopulateAssortedforSizeVariantProduct() {
        final TargetProductData productData = new TargetProductData();
        final TargetColourVariantProductModel colourVariantProductModel = mock(TargetColourVariantProductModel.class);
        final TargetSizeVariantProductModel sizeVariantProductModel = mock(TargetSizeVariantProductModel.class);

        given(sizeTypeService.getDefaultSizeType()).willReturn(getDefaultSizeType());
        given(sizeVariantProductModel.getBaseProduct()).willReturn(colourVariantProductModel);
        given(sizeVariantProductModel.getSizeClassification()).willReturn(null);
        doReturn(Boolean.TRUE).when(colourVariantProductModel).isAssorted();

        targetProductPopulator.populate(sizeVariantProductModel, productData);

        assertThat(productData.isAssorted()).isTrue();
    }

    @Test
    public void testPopulateExcludeForAfterpayForSizeVariantProduct() {
        final TargetProductData productData = new TargetProductData();
        final TargetColourVariantProductModel colourVariantProductModel = mock(TargetColourVariantProductModel.class);
        final TargetSizeVariantProductModel sizeVariantProductModel = mock(TargetSizeVariantProductModel.class);

        given(sizeTypeService.getDefaultSizeType()).willReturn(getDefaultSizeType());
        given(sizeVariantProductModel.getBaseProduct()).willReturn(colourVariantProductModel);
        given(sizeVariantProductModel.getSizeClassification()).willReturn(null);
        doReturn(Boolean.TRUE).when(colourVariantProductModel).isExcludeForAfterpay();

        targetProductPopulator.populate(sizeVariantProductModel, productData);

        assertThat(productData.isExcludeForAfterpay()).isTrue();
    }


    @Test
    public void testPopulateExcludeForZipPaymentForSizeVariantProduct() {
        final TargetProductData productData = new TargetProductData();
        final TargetColourVariantProductModel colourVariantProductModel = mock(TargetColourVariantProductModel.class);
        final TargetSizeVariantProductModel sizeVariantProductModel = mock(TargetSizeVariantProductModel.class);

        given(sizeTypeService.getDefaultSizeType()).willReturn(getDefaultSizeType());
        given(sizeVariantProductModel.getBaseProduct()).willReturn(colourVariantProductModel);
        given(sizeVariantProductModel.getSizeClassification()).willReturn(null);

        doReturn(Boolean.TRUE).when(colourVariantProductModel).isExcludeForZipPayment();
        targetProductPopulator.populate(sizeVariantProductModel, productData);
        assertThat(productData.isExcludeForZipPayment()).isTrue();

        doReturn(Boolean.FALSE).when(colourVariantProductModel).isExcludeForZipPayment();
        targetProductPopulator.populate(sizeVariantProductModel, productData);
        assertThat(productData.isExcludeForZipPayment()).isFalse();
    }

    @Test
    public void testPopulateSwatchforSizeVariantProductWithNoColour() {
        final TargetProductData productData = new TargetProductData();
        final TargetColourVariantProductModel colourVariantProductModel = mock(TargetColourVariantProductModel.class);
        final TargetSizeVariantProductModel sizeVariantProductModel = Mockito.mock(TargetSizeVariantProductModel.class);

        given(sizeTypeService.getDefaultSizeType()).willReturn(getDefaultSizeType());
        given(sizeVariantProductModel.getBaseProduct()).willReturn(colourVariantProductModel);
        given(sizeVariantProductModel.getSizeClassification()).willReturn(null);
        given(colourVariantProductModel.getSwatchName()).willReturn("nocolour");
        final ColourModel swatch = mock(ColourModel.class);
        given(swatch.getDisplay()).willReturn(Boolean.FALSE);
        given(colourVariantProductModel.getSwatch()).willReturn(swatch);
        targetProductPopulator.populate(sizeVariantProductModel, productData);
        assertThat(productData.getSwatch()).isNull();
    }

    private TargetProductModel createTargetProductModel(final String careInstructions,
            final String productFeatures, final String materials, final String extraInfo,
            final GiftCardModel giftCard, final BrandModel brand, final ProductTypeModel productType,
            final MediaModel video, final Boolean showWhenOutOfStock, final Boolean bulkyBoardProduct,
            final Boolean preview) {
        final TargetProductModel productModel = Mockito.mock(TargetProductModel.class);

        given(productModel.getCareInstructions()).willReturn(careInstructions);
        given(productModel.getProductFeatures()).willReturn(productFeatures);
        given(productModel.getMaterials()).willReturn(materials);
        given(productModel.getExtraInfo()).willReturn(extraInfo);
        given(productModel.getGiftCard()).willReturn(giftCard);
        given(productModel.getBrand()).willReturn(brand);
        given(productModel.getProductType()).willReturn(productType);
        given(productModel.getBulkyBoardProduct()).willReturn(bulkyBoardProduct);
        given(productModel.getPreview()).willReturn(preview);

        if (video != null) {
            given(productModel.getVideos()).willReturn(Collections.singletonList(video));
        }
        given(productModel.getShowWhenOutOfStock()).willReturn(showWhenOutOfStock);

        return productModel;
    }

    private void validateProductDataEquals(final TargetProductData productData, final List<String> careInstructions,
            final List<String> productFeatures, final List<String> materials, final String extraInfo,
            final Boolean isGiftCard, final String brandName,
            final String productTypeCode, final String videoUrl, final String baseProductUrl,
            final Boolean showWhenOutOfStock, final Boolean bulkyBoardProduct, final Boolean preview,
            final TargetProductCategoryModel originalCategory) {
        assertThat(productData.getCareInstructions()).isEqualTo(careInstructions);
        assertThat(productData.getProductFeatures()).isEqualTo(productFeatures);
        assertThat(productData.getMaterials()).isEqualTo(materials);
        assertThat(productData.getExtraInfo()).isEqualTo(extraInfo);
        assertThat(productData.isGiftCard()).isEqualTo(isGiftCard);
        assertThat(productData.getBrand()).isEqualTo(brandName);
        assertThat(productData.getProductType()).isEqualTo(productTypeCode);
        assertThat(productData.getYouTubeLink()).isEqualTo(videoUrl);

        assertThat(productData.getBaseProductCanonical().getUrl()).isEqualTo(baseProductUrl);
        assertThat(productData.isShowWhenOutOfStock()).isEqualTo(showWhenOutOfStock.booleanValue());
        assertThat(productData.isBulkyBoardProduct()).isEqualTo(bulkyBoardProduct.booleanValue());
        assertThat(productData.isPreview()).isEqualTo(preview);
        if (null != originalCategory) {
            assertThat(productData.getOriginalCategoryCode()).isEqualTo(originalCategory.getCode());
        }
    }

    @Test
    public void testPreOrderDetails() {
        final TargetProductData productData = new TargetProductData();
        final TargetColourVariantProductModel colourVariantProductModel = mock(TargetColourVariantProductModel.class);
        final TargetProductModel baseProduct = mock(TargetProductModel.class);

        final Calendar end = Calendar.getInstance();
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 2, 22, 0, 0, 0);

        final Calendar start = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), start.get(Calendar.MONTH) - 2, 22, 0, 0, 0);

        final Calendar embargo = Calendar.getInstance();
        embargo.set(embargo.get(Calendar.YEAR), embargo.get(Calendar.MONTH) + 2, 25, 0, 0, 0);

        final Date normalSalesDate = embargo.getTime();

        given(colourVariantProductModel.getPreOrderStartDateTime()).willReturn(start.getTime());
        given(colourVariantProductModel.getPreOrderEndDateTime()).willReturn(end.getTime());
        given(colourVariantProductModel.getNormalSaleStartDateTime()).willReturn(normalSalesDate);
        given(colourVariantProductModel.getBaseProduct()).willReturn(baseProduct);
        given(targetPreOrderService.getProductDisplayType(colourVariantProductModel))
                .willReturn(ProductDisplayType.PREORDER_AVAILABLE);

        targetProductPopulator.populate(colourVariantProductModel, productData);

        assertThat(productData.getNormalSaleStartDate())
                .isEqualTo(TargetDateUtil.getDateAsString(normalSalesDate, "d MMMM"));
        assertThat(productData.getProductDisplayType()).isEqualTo(ProductDisplayType.PREORDER_AVAILABLE);
    }

    @Test
    public void testPreOrderDetailsComingSoon() {
        final TargetProductData productData = new TargetProductData();
        final TargetColourVariantProductModel colourVariantProductModel = mock(TargetColourVariantProductModel.class);
        final TargetProductModel baseProduct = mock(TargetProductModel.class);

        final Calendar end = Calendar.getInstance();
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 2, 22, 0, 0, 0);

        final Calendar start = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), start.get(Calendar.MONTH) - 2, 22, 0, 0, 0);

        final Calendar embargo = Calendar.getInstance();
        embargo.set(embargo.get(Calendar.YEAR), embargo.get(Calendar.MONTH) + 2, 25, 0, 0, 0);

        final Date normalSalesDate = embargo.getTime();

        given(colourVariantProductModel.getPreOrderStartDateTime()).willReturn(start.getTime());
        given(colourVariantProductModel.getPreOrderEndDateTime()).willReturn(end.getTime());
        given(colourVariantProductModel.getNormalSaleStartDateTime()).willReturn(normalSalesDate);
        given(colourVariantProductModel.getBaseProduct()).willReturn(baseProduct);
        given(targetPreOrderService.getProductDisplayType(colourVariantProductModel))
                .willReturn(ProductDisplayType.COMING_SOON);

        targetProductPopulator.populate(colourVariantProductModel, productData);

        assertThat(productData.getNormalSaleStartDate())
                .isEqualTo(TargetDateUtil.getDateAsString(normalSalesDate, "d MMMM"));
        assertThat(productData.getProductDisplayType()).isEqualTo(ProductDisplayType.COMING_SOON);
    }

    @Test
    public void testNullPreOrderDetails() {
        final TargetProductData productData = new TargetProductData();
        final TargetColourVariantProductModel colourVariantProductModel = mock(TargetColourVariantProductModel.class);
        final TargetProductModel baseProduct = mock(TargetProductModel.class);

        given(colourVariantProductModel.getBaseProduct()).willReturn(baseProduct);
        targetProductPopulator.populate(colourVariantProductModel, productData);

        assertThat(productData.getNormalSaleStartDate()).isNull();
        assertThat(productData.getProductDisplayType()).isNull();
    }

    class TestDeliveryModeConverter
            extends TargetZoneDeliveryModePopulator {

        public TargetZoneDeliveryModeData convert(final TargetZoneDeliveryModeModel zoneDeliveryModel)
                throws ConversionException {
            final TargetZoneDeliveryModeData zoneDeliverydata = new TargetZoneDeliveryModeData();
            zoneDeliverydata.setCode(zoneDeliveryModel.getCode());
            return zoneDeliverydata;
        }

        @Override
        public void populate(final TargetZoneDeliveryModeModel source, final TargetZoneDeliveryModeData target) {
            target.setCode(source.getCode());
        }
    }
}

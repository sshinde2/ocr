/**
 * 
 */
package au.com.target.tgtfacades.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.price.TargetCommercePriceService;
import au.com.target.tgtcore.price.data.PriceRangeInformation;
import au.com.target.tgtfacades.product.data.AbstractTargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.util.TargetPriceHelper;
import au.com.target.tgtfacades.util.TargetPriceInformation;


/**
 * @author pthoma20
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductPricePopulatorTest {

    @InjectMocks
    private final TargetProductPricePopulator targetProductPricePopulator = new TargetProductPricePopulator();

    @Mock
    private TargetCommercePriceService targetCommercePriceService;

    @Mock
    private TargetPriceHelper targetPriceHelper;

    public PriceData createPriceData(final PriceDataType type, final BigDecimal value, final String iso) {
        final PriceData priceData = new PriceData();
        priceData.setCurrencyIso(iso);
        priceData.setPriceType(type);
        priceData.setValue(value);
        return priceData;
    }

    /**
     * Test method for
     * {@link au.com.target.tgtfacades.converters.populator.TargetProductPricePopulator#populatePricesFromRangeInformation(au.com.target.tgtfacades.product.data.AbstractTargetProductData, au.com.target.tgtcore.price.data.PriceRangeInformation)}
     * .
     */
    @Test
    public void testPopulatePricesFromRangeInformation() {

        final AbstractTargetProductData productData = new TargetProductData();

        final PriceRangeInformation priceRangeInfo = new PriceRangeInformation(Double.valueOf(10), Double.valueOf(20),
                Double.valueOf(15), Double.valueOf(25));

        final PriceData fromPrice = createPriceData(PriceDataType.BUY, BigDecimal.valueOf(10), "$");
        final PriceData toPrice = createPriceData(PriceDataType.BUY, BigDecimal.valueOf(20), "$");
        final PriceData fromWasPrice = createPriceData(PriceDataType.BUY, BigDecimal.valueOf(15), "$");
        final PriceData toWasPrice = createPriceData(PriceDataType.BUY, BigDecimal.valueOf(25), "$");

        final PriceRangeData priceRangeData = new PriceRangeData();
        priceRangeData.setMaxPrice(toPrice);
        priceRangeData.setMinPrice(fromPrice);

        final PriceRangeData wasPriceRangeData = new PriceRangeData();
        wasPriceRangeData.setMaxPrice(toWasPrice);
        wasPriceRangeData.setMinPrice(fromWasPrice);

        final TargetPriceInformation targetPriceInformation = Mockito.mock(TargetPriceInformation.class);
        given(targetPriceInformation.getPriceRangeData()).willReturn(priceRangeData);
        given(targetPriceInformation.getWasPriceRangeData()).willReturn(wasPriceRangeData);

        given(targetPriceHelper.populatePricesFromRangeInformation(priceRangeInfo))
                .willReturn(targetPriceInformation);

        targetProductPricePopulator.populatePricesFromRangeInformation(productData, priceRangeInfo);

        assertThat(productData.getPriceRange().getMinPrice()
                .getValue().doubleValue()).isEqualTo(priceRangeInfo.getFromPrice().doubleValue());
        assertThat(productData.getPriceRange().getMaxPrice()
                .getValue().doubleValue()).isEqualTo(priceRangeInfo.getToPrice().doubleValue());
        assertThat(productData.getWasPriceRange()
                .getMinPrice().getValue().doubleValue()).isEqualTo(priceRangeInfo.getFromWasPrice().doubleValue());
        assertThat(productData.getWasPriceRange().getMaxPrice()
                .getValue().doubleValue()).isEqualTo(priceRangeInfo.getToWasPrice().doubleValue());

    }

    @Test
    public void testPopulatePricesFromRangeInformationMissingPrice() {

        final TargetProductListerData productData = new TargetProductListerData();
        final PriceRangeInformation priceRangeInfo = new PriceRangeInformation(null, null, null, null);
        final TargetPriceInformation targetPriceInformation = new TargetPriceInformation();
        targetPriceInformation.setMissingPrice(true);
        given(targetPriceHelper.populatePricesFromRangeInformation(priceRangeInfo))
                .willReturn(targetPriceInformation);
        targetProductPricePopulator.populatePricesFromRangeInformation(productData, priceRangeInfo);
        assertThat(productData.isMissingPrice()).isTrue();
    }

    @Test
    public void testPopulateAfterpayPrice() {
        final PriceData price = createPriceData(PriceDataType.BUY, BigDecimal.valueOf(10), "$");
        final PriceRangeInformation priceRange = mock(PriceRangeInformation.class);
        final TargetPriceInformation priceInfo = mock(TargetPriceInformation.class);
        given(priceInfo.getPriceData()).willReturn(price);
        given(targetPriceHelper.populatePricesFromRangeInformation(priceRange)).willReturn(priceInfo);
        final AbstractTargetProductData productData = Mockito.spy(new TargetProductListerData());
        final AbstractTargetVariantProductModel productModel = mock(AbstractTargetVariantProductModel.class);
        given(targetCommercePriceService.getPriceRangeInfoForProduct(productModel)).willReturn(priceRange);

        targetProductPricePopulator.populate(productModel, productData);
        final ArgumentCaptor<PriceData> installmentPriceValue = ArgumentCaptor.forClass(PriceData.class);
        verify(targetPriceHelper).calculateAfterpayInstallmentPrice(installmentPriceValue.capture());
        assertThat(installmentPriceValue.getValue()).isEqualTo(price);
        verify(productData).setInstallmentPrice(Mockito.any(PriceData.class));
    }

    @Test
    public void testPopulateAfterpayPriceForProductWithoutPrice() {
        final PriceRangeInformation priceRange = mock(PriceRangeInformation.class);
        final TargetPriceInformation priceInfo = mock(TargetPriceInformation.class);
        given(priceInfo.getPriceData()).willReturn(null);
        given(targetPriceHelper.populatePricesFromRangeInformation(priceRange)).willReturn(priceInfo);
        final AbstractTargetProductData productData = Mockito.spy(new TargetProductListerData());
        final AbstractTargetVariantProductModel productModel = mock(AbstractTargetVariantProductModel.class);
        given(targetCommercePriceService.getPriceRangeInfoForProduct(productModel)).willReturn(priceRange);

        targetProductPricePopulator.populate(productModel, productData);
        verify(productData, never()).setInstallmentPrice(Mockito.any(PriceData.class));
    }
}

package au.com.target.tgtfacades.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ReviewData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.customerreview.CustomerReviewService;
import de.hybris.platform.customerreview.model.CustomerReviewModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * Unit test for {@link TargetProductReviewsPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductReviewsPopulatorTest {

    @InjectMocks
    private final TargetProductReviewsPopulator targetProductReviewsPopulator = new TargetProductReviewsPopulator();

    @Mock
    private CustomerReviewService customerReviewService;

    @Mock
    private CommonI18NService commonI18NService;

    @Mock
    private Converter<CustomerReviewModel, ReviewData> customerReviewConverter;

    @Test
    public void testPopulateWithNullProductModel() {

        final ProductData productData = Mockito.mock(ProductData.class);
        targetProductReviewsPopulator.populate(null, productData);

        Mockito.verifyZeroInteractions(customerReviewService);
        Mockito.verifyZeroInteractions(commonI18NService);
        Mockito.verifyZeroInteractions(customerReviewConverter);
    }

    @Test
    public void testPopulateWithNullProductData() {

        final TargetProductModel targetProduct = Mockito.mock(TargetProductModel.class);
        targetProductReviewsPopulator.populate(targetProduct, null);

        Mockito.verifyZeroInteractions(customerReviewService);
        Mockito.verifyZeroInteractions(commonI18NService);
        Mockito.verifyZeroInteractions(customerReviewConverter);
    }

    @Test
    public void testPopulateWithBaseProduct() {

        final TargetProductModel targetProduct = Mockito.mock(TargetProductModel.class);
        final ProductData productData = Mockito.mock(ProductData.class);
        targetProductReviewsPopulator.populate(targetProduct, productData);

        Mockito.verify(customerReviewService).getReviewsForProductAndLanguage(Mockito.eq(targetProduct),
                Mockito.any(LanguageModel.class));

        Mockito.verify(targetProduct).getAverageRating();
    }

    @Test
    public void testPopulateWithColourVariantProduct() {

        final TargetProductModel targetProduct = Mockito.mock(TargetProductModel.class);
        final TargetColourVariantProductModel colourVariantProduct = Mockito
                .mock(TargetColourVariantProductModel.class);
        Mockito.when(colourVariantProduct.getBaseProduct()).thenReturn(targetProduct);

        final ProductData productData = Mockito.mock(ProductData.class);
        targetProductReviewsPopulator.populate(colourVariantProduct, productData);

        Mockito.verify(customerReviewService).getReviewsForProductAndLanguage(Mockito.eq(targetProduct),
                Mockito.any(LanguageModel.class));

        Mockito.verify(targetProduct).getAverageRating();
        Mockito.verify(colourVariantProduct, Mockito.never()).getAverageRating();
    }

    @Test
    public void testPopulateWithSizeVariantProduct() {

        final TargetProductModel targetProduct = Mockito.mock(TargetProductModel.class);
        final TargetColourVariantProductModel colourVariantProduct = Mockito
                .mock(TargetColourVariantProductModel.class);
        final TargetSizeVariantProductModel sizeVariantProduct = Mockito
                .mock(TargetSizeVariantProductModel.class);
        Mockito.when(colourVariantProduct.getBaseProduct()).thenReturn(targetProduct);
        Mockito.when(sizeVariantProduct.getBaseProduct()).thenReturn(colourVariantProduct);

        final ProductData productData = Mockito.mock(ProductData.class);
        targetProductReviewsPopulator.populate(sizeVariantProduct, productData);

        Mockito.verify(customerReviewService).getReviewsForProductAndLanguage(Mockito.eq(targetProduct),
                Mockito.any(LanguageModel.class));

        Mockito.verify(targetProduct).getAverageRating();
        Mockito.verify(colourVariantProduct, Mockito.never()).getAverageRating();
        Mockito.verify(sizeVariantProduct, Mockito.never()).getAverageRating();
    }
}

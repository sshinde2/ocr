/**
 * 
 */
package au.com.target.tgtfacades.customer.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.commercefacades.storesession.data.CurrencyData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ClassMismatchException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserConstants;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Arrays;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.customer.TargetCustomerAccountService;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.customergroups.impl.DefaultTargetCustomerGroupFacade;
import au.com.target.tgtfacades.prepopulatecart.TargetPrepopulateCheckoutCartFacade;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.user.data.TargetCustomerInfoData;
import au.com.target.tgtfacades.user.data.TargetRegisterData;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtlayby.order.TargetLaybyCommerceCheckoutService;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;


/**
 * @author asingh78
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultTargetCustomerFacadeTest {

    private static final String TEST_DUMMY = "dummy";

    @InjectMocks
    private final DefaultTargetCustomerFacade defaultTargetCustomerFacade = new DefaultTargetCustomerFacade();

    @Mock
    private CartService cartService;

    @Mock
    private ModelService modelService;

    @Mock
    private AbstractPopulatingConverter<UserModel, CustomerData> customerConverter;

    @Mock
    private CommonI18NService commonI18NService;

    @Mock
    private TargetCustomerAccountService customerAccountService;

    @Mock
    private TargetLaybyCommerceCheckoutService targetLaybyCommerceCheckoutService;

    @Mock
    private CustomerNameStrategy customerNameStrategy;

    @Mock
    private SalesApplicationFacade salesApplicationFacade;

    @Mock
    private UserService userService;

    @Mock
    private Map<String, String> userGroupForSalesApplication;

    @Mock
    private DefaultTargetCustomerGroupFacade targetCustomerGroupFacade;

    @Mock
    private ConfigurationService mockConfigurationService;

    @Mock
    private Configuration mockConfiguration;

    @Mock
    private StoreSessionFacade storeSessionFacade;

    @Mock
    private UserFacade userFacade;

    @Mock
    private TargetCommerceCartService commerceCartService;

    @Mock
    private SessionService sessionService;

    @Mock
    private Converter<AddressModel, AddressData> addressConverter;

    @Mock
    private TargetOrderService targetOrderService;

    @Mock
    private TargetPrepopulateCheckoutCartFacade targetPrepopulateCheckoutCartFacade;

    @Before
    public void setUp() {
        given(mockConfigurationService.getConfiguration()).willReturn(mockConfiguration);
        given(mockConfiguration.getString(DefaultTargetCustomerFacade.BLACKLISTED_PASSWORD_KEY))
                .willReturn(
                        "password|123456|abc123|qwerty|monkey|letmein|dragon|111111|baseball|iloveyou|trustno1|sunshine|master|123123|rockyou|princess");
    }

    @Test
    public void testRegisterGuestWithNoSessionCart() throws DuplicateUidException {
        final String email = "test@test.com";
        final String userName = "Guest";

        final TargetCustomerModel mockCustomer = mock(TargetCustomerModel.class);
        given(customerAccountService.registerGuestForAnonymousCheckout(email, userName)).willReturn(mockCustomer);

        final CustomerData customerData = new CustomerData();
        customerData.setUid(email);
        given(customerConverter.convert(mockCustomer)).willReturn(customerData);

        defaultTargetCustomerFacade.createGuestUserForAnonymousCheckout(email, userName);
        verify(customerAccountService).registerGuestForAnonymousCheckout(email, userName);
    }

    @Test
    public void testRegisterGuest() throws DuplicateUidException {
        final String email = "test@test.com";
        final String userName = "Guest";

        final TargetCustomerModel mockCustomer = mock(TargetCustomerModel.class);
        given(customerAccountService.registerGuestForAnonymousCheckout(email, userName)).willReturn(mockCustomer);

        final CustomerData customerData = new CustomerData();
        customerData.setUid(email);
        given(customerConverter.convert(mockCustomer)).willReturn(customerData);

        final CartModel mockCart = mock(CartModel.class);
        doReturn(Boolean.TRUE).when(cartService).hasSessionCart();
        given(cartService.getSessionCart()).willReturn(mockCart);

        given(userService.getUserForUID(email)).willReturn(mockCustomer);

        defaultTargetCustomerFacade.createGuestUserForAnonymousCheckout(email, userName);

        verify(customerAccountService).registerGuestForAnonymousCheckout(email, userName);
        verify(targetLaybyCommerceCheckoutService).changeCurrentCustomerOnAllCheckoutCarts(mockCart, mockCustomer);
    }

    @Test
    public void testRegister() throws DuplicateUidException {

        final TargetRegisterData data = new TargetRegisterData();
        data.setFirstName(TEST_DUMMY);
        data.setLastName(TEST_DUMMY);
        data.setLogin(TEST_DUMMY);
        data.setPassword(TEST_DUMMY);
        data.setTitleCode(TEST_DUMMY);
        final TargetCustomerModel model = new TargetCustomerModel();
        given(modelService.create(TargetCustomerModel.class)).willReturn(model);
        defaultTargetCustomerFacade.register(data);
        verify(customerAccountService).register(model, TEST_DUMMY);
    }

    @SuppressFBWarnings(value = "NP_NONNULL_PARAM_VIOLATION", justification = "Testing that null is handled correctly")
    @Test(expected = IllegalArgumentException.class)
    public void testValidateDataBeforeUpdateWithNullCustomerData() {
        defaultTargetCustomerFacade.validateDataBeforeUpdate(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateDataBeforeUpdateWithBlankFirstName() {
        final CustomerData customerData = new CustomerData();
        defaultTargetCustomerFacade.validateDataBeforeUpdate(customerData);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateDataBeforeUpdateWithBlankLastName() {
        final CustomerData customerData = new CustomerData();
        customerData.setFirstName("First name");
        defaultTargetCustomerFacade.validateDataBeforeUpdate(customerData);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateDataBeforeUpdateWithBlankUid() {
        final CustomerData customerData = new CustomerData();
        customerData.setFirstName("First name");
        customerData.setLastName("Last name");
        defaultTargetCustomerFacade.validateDataBeforeUpdate(customerData);
    }

    @Test
    public void testValidateDataBeforeUpdate() {
        final CustomerData customerData = new CustomerData();
        customerData.setFirstName("First name");
        customerData.setLastName("Last name");
        customerData.setUid("Uid");
        defaultTargetCustomerFacade.validateDataBeforeUpdate(customerData);
    }

    @SuppressFBWarnings(value = "NP_NONNULL_PARAM_VIOLATION", justification = "Testing that null is handled correctly")
    @Test(expected = IllegalArgumentException.class)
    public void testUpdateProfileWithNullCustomerData() throws DuplicateUidException {
        defaultTargetCustomerFacade.updateProfile(null);
    }

    @Test
    public void testUpdateProfile() throws DuplicateUidException {
        final String uid = "uid";
        final String titleCode = "mr";
        final String firstName = "John";
        final String lastName = "Smith";

        final CustomerData customerData = new CustomerData();
        customerData.setUid(uid);
        customerData.setTitleCode(titleCode);
        customerData.setFirstName(firstName);
        customerData.setLastName(lastName);

        final TargetCustomerModel mockTargetCustomerModel = mock(TargetCustomerModel.class);
        given(userService.getCurrentUser()).willReturn(mockTargetCustomerModel);

        final TitleModel mockTitle = mock(TitleModel.class);
        given(userService.getTitleForCode(titleCode)).willReturn(mockTitle);

        defaultTargetCustomerFacade.updateProfile(customerData);

        verify(mockTargetCustomerModel).setTitle(mockTitle);
        verify(mockTargetCustomerModel).setFirstname(firstName);
        verify(mockTargetCustomerModel).setLastname(lastName);
        verify(modelService).save(mockTargetCustomerModel);
    }

    @Test
    public void testUpdateProfileWithNoTitle() throws DuplicateUidException {
        final String uid = "uid";
        final String firstName = "John";
        final String lastName = "Smith";

        final CustomerData customerData = new CustomerData();
        customerData.setUid(uid);
        customerData.setFirstName(firstName);
        customerData.setLastName(lastName);

        final TargetCustomerModel mockTargetCustomerModel = mock(TargetCustomerModel.class);
        given(userService.getCurrentUser()).willReturn(mockTargetCustomerModel);

        defaultTargetCustomerFacade.updateProfile(customerData);

        verify(mockTargetCustomerModel).setFirstname(firstName);
        verify(mockTargetCustomerModel).setLastname(lastName);
        verify(modelService).save(mockTargetCustomerModel);
    }

    @Test
    public void testGetCustomerInfoSuccessful() {

        final TargetCustomerModel mockTargetCustomerModel = mock(TargetCustomerModel.class);
        given(userService.getCurrentUser()).willReturn(mockTargetCustomerModel);
        given(mockTargetCustomerModel.getFirstname()).willReturn("hello");
        given(mockTargetCustomerModel.getLastname()).willReturn("world");
        given(mockTargetCustomerModel.getContactEmail()).willReturn("hello@world.com");
        given(mockTargetCustomerModel.getUid()).willReturn("hello@world.com");
        final CartModel mockCartModel = mock(CartModel.class);
        given(cartService.getSessionCart()).willReturn(mockCartModel);
        given(mockCartModel.getGuid()).willReturn("222-333-444-555");
        final UserModel cartUser = mock(UserModel.class);
        given(mockCartModel.getUser()).willReturn(cartUser);
        given(cartUser.getUid()).willReturn("hello@world.com");
        final TargetCustomerInfoData customerData = defaultTargetCustomerFacade.getCustomerInfo();
        assertThat(customerData.getFirstName()).isEqualTo("hello");
        assertThat(customerData.getLastName()).isEqualTo("world");
        assertThat(customerData.getEmail()).isEqualTo("hello@world.com");
        assertThat(customerData.getCartGuid()).isEqualTo("222-333-444-555");
    }

    @Test
    public void testGetCustomerInfoWhenCartUserIsNotCurrentUser() {

        final TargetCustomerModel mockTargetCustomerModel = mock(TargetCustomerModel.class);
        given(userService.getCurrentUser()).willReturn(mockTargetCustomerModel);
        given(mockTargetCustomerModel.getFirstname()).willReturn("hello");
        given(mockTargetCustomerModel.getLastname()).willReturn("world");
        given(mockTargetCustomerModel.getContactEmail()).willReturn("hello@world.com");
        given(mockTargetCustomerModel.getUid()).willReturn("hello@world.com");
        final CartModel mockCartModel = mock(CartModel.class);
        given(cartService.getSessionCart()).willReturn(mockCartModel);
        given(mockCartModel.getGuid()).willReturn("222-333-444-555");
        final UserModel cartUser = mock(UserModel.class);
        given(mockCartModel.getUser()).willReturn(cartUser);
        given(cartUser.getUid()).willReturn("hello2@world.com");
        final TargetCustomerInfoData customerData = defaultTargetCustomerFacade.getCustomerInfo();
        assertThat(customerData.getFirstName()).isNull();
        assertThat(customerData.getLastName()).isNull();
        assertThat(customerData.getEmail()).isNull();
        assertThat(customerData.getCartGuid()).isNull();
    }

    @SuppressWarnings("boxing")
    @Test
    public void associateUserGroupForUserBasedOnSalesApplicationSucessful() {
        final UserModel currentUser = mock(UserModel.class);
        given(userService.getCurrentUser()).willReturn(currentUser);
        given(userService.isAnonymousUser(currentUser)).willReturn(Boolean.FALSE);
        given(currentUser.getUid()).willReturn("uid");
        final SalesApplication salesApplication = mock(SalesApplication.class);
        given(salesApplication.getCode()).willReturn("MobileApp");
        given(salesApplicationFacade.getCurrentSalesApplication()).willReturn(salesApplication);
        given(userGroupForSalesApplication.get("MobileApp")).willReturn("mobileappusergroup");
        given(userGroupForSalesApplication.containsKey("MobileApp")).willReturn(true);
        given(targetCustomerGroupFacade.isUserMemberOfTheGroup("mobileappusergroup")).willReturn(false);
        final ArgumentCaptor<String> argumentCaptor1 = ArgumentCaptor.forClass(String.class);
        final ArgumentCaptor<UserModel> argumentCaptor2 = ArgumentCaptor.forClass(UserModel.class);
        defaultTargetCustomerFacade.associateUserGroupForUserBasedOnSalesApplication();
        verify(targetCustomerGroupFacade).addUserToCustomerGroup(argumentCaptor1.capture(), argumentCaptor2.capture());
        assertThat(argumentCaptor1.getValue()).isEqualTo("mobileappusergroup");
        assertThat(argumentCaptor2.getValue()).isEqualTo(currentUser);
    }

    @SuppressWarnings("boxing")
    @Test
    public void associateUserGroupForUserBasedOnSalesApplicationWhenUserIsAlreadyAMember() {
        final UserModel currentUser = mock(UserModel.class);
        given(userService.getCurrentUser()).willReturn(currentUser);
        given(userService.isAnonymousUser(currentUser)).willReturn(Boolean.FALSE);
        given(currentUser.getUid()).willReturn("uid");
        final SalesApplication salesApplication = mock(SalesApplication.class);
        given(salesApplication.getCode()).willReturn("MobileApp");
        given(salesApplicationFacade.getCurrentSalesApplication()).willReturn(salesApplication);
        given(userGroupForSalesApplication.get("MobileApp")).willReturn("mobileappusergroup");
        given(userGroupForSalesApplication.containsKey("MobileApp")).willReturn(true);
        given(targetCustomerGroupFacade.isUserMemberOfTheGroup("mobileappusergroup")).willReturn(true);
        defaultTargetCustomerFacade.associateUserGroupForUserBasedOnSalesApplication();
        verify(targetCustomerGroupFacade, never()).addUserToCustomerGroup("mobileappusergroup", "uid");
    }

    @SuppressWarnings("boxing")
    @Test
    public void testAssociateUserGroupForUserBasedOnSalesApplicationWhenUserIsAnonymous() {
        final UserModel currentUser = mock(UserModel.class);
        given(userService.getCurrentUser()).willReturn(currentUser);
        given(userService.isAnonymousUser(currentUser)).willReturn(Boolean.TRUE);
        given(currentUser.getUid()).willReturn("uid");
        final SalesApplication salesApplication = mock(SalesApplication.class);
        given(salesApplication.getCode()).willReturn("MobileApp");
        given(salesApplicationFacade.getCurrentSalesApplication()).willReturn(salesApplication);
        given(userGroupForSalesApplication.get("MobileApp")).willReturn("mobileappusergroup");
        given(userGroupForSalesApplication.containsKey("MobileApp")).willReturn(true);
        given(targetCustomerGroupFacade.isUserMemberOfTheGroup("mobileappusergroup")).willReturn(false);
        defaultTargetCustomerFacade.associateUserGroupForUserBasedOnSalesApplication();
        verify(targetCustomerGroupFacade, never()).addUserToCustomerGroup("mobileappusergroup", "uid");
    }

    @SuppressWarnings("boxing")
    @Test
    public void testAssociateUserGroupForUserBasedOnSalesApplicationForANonConfiguredSalesApplication() {
        final UserModel currentUser = mock(UserModel.class);
        given(userService.getCurrentUser()).willReturn(currentUser);
        given(userService.isAnonymousUser(currentUser)).willReturn(Boolean.TRUE);
        given(currentUser.getUid()).willReturn("uid");
        final SalesApplication salesApplication = mock(SalesApplication.class);
        given(salesApplication.getCode()).willReturn("WEB");
        given(salesApplicationFacade.getCurrentSalesApplication()).willReturn(salesApplication);
        given(userGroupForSalesApplication.get("MobileApp")).willReturn("mobileappusergroup");
        given(userGroupForSalesApplication.containsKey("MobileApp")).willReturn(true);
        given(targetCustomerGroupFacade.isUserMemberOfTheGroup("mobileappusergroup")).willReturn(true);
        defaultTargetCustomerFacade.associateUserGroupForUserBasedOnSalesApplication();
        verify(targetCustomerGroupFacade, never()).addUserToCustomerGroup("mobileappusergroup", "uid");
    }

    @Test
    public void testGetUidForPasswordResetToken() {
        final String token = "token";
        final TargetCustomerModel targetCustomerModelMock = mock(TargetCustomerModel.class);
        given(targetCustomerModelMock.getUid()).willReturn("uid");
        given(customerAccountService.getUserForResetPasswordToken(token)).willReturn(targetCustomerModelMock);
        final String uid = defaultTargetCustomerFacade.getUidForPasswordResetToken(token);
        assertThat(uid).isEqualTo("uid");
    }

    @Test
    public void testGetRedirectPageForPasswordResetToken() {
        final String token = "token";
        final TargetCustomerModel targetCustomerModelMock = mock(TargetCustomerModel.class);
        given(targetCustomerModelMock.getUid()).willReturn("uid");
        given(customerAccountService.getUserForResetPasswordToken(token)).willReturn(targetCustomerModelMock);
        given(targetCustomerModelMock.getRedirectPage()).willReturn("basket");
        assertThat(defaultTargetCustomerFacade.getRedirectPageForPasswordResetToken(token)).isEqualTo(
                "basket");
    }

    @Test
    public void testGetRedirectPageForPasswordResetTokenNoCustomer() {
        final String token = "token";
        final TargetCustomerModel targetCustomerModelMock = mock(TargetCustomerModel.class);
        given(targetCustomerModelMock.getUid()).willReturn("uid");
        given(customerAccountService.getUserForResetPasswordToken(token)).willReturn(null);
        assertThat(defaultTargetCustomerFacade.getRedirectPageForPasswordResetToken(token)).isNull();
    }

    @Test
    public void verifyForgottenPassword() {
        final TargetCustomerModel targetCustomerModelMock = mock(TargetCustomerModel.class);
        given(userService.getUserForUID("uid", TargetCustomerModel.class)).willReturn(targetCustomerModelMock);
        defaultTargetCustomerFacade.forgottenPassword("uid", "basket");
        verify(targetCustomerModelMock, times(1)).setRedirectPage("basket");
        verify(customerAccountService, times(1)).forgottenPassword(targetCustomerModelMock);
    }

    @Test
    public void verifyForgottenPasswordEmptyRedirect() {
        final TargetCustomerModel targetCustomerModelMock = mock(TargetCustomerModel.class);
        given(userService.getUserForUID("uid", TargetCustomerModel.class)).willReturn(targetCustomerModelMock);
        defaultTargetCustomerFacade.forgottenPassword("uid", "");
        verify(targetCustomerModelMock, never()).setRedirectPage("");
        verify(customerAccountService, times(1)).forgottenPassword(targetCustomerModelMock);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testForgottenPasswordForNullUid() {
        defaultTargetCustomerFacade.forgottenPassword(null, "basket");
    }

    @Test
    public void verifyResetRedirectPageForPasswordResetToken() {
        final TargetCustomerModel targetCustomerModelMock = mock(TargetCustomerModel.class);
        given(targetCustomerModelMock.getUid()).willReturn("uid");
        given(userService.getUserForUID("uid", TargetCustomerModel.class)).willReturn(targetCustomerModelMock);
        defaultTargetCustomerFacade.resetRedirectPageForPasswordResetToken("uid");
        verify(targetCustomerModelMock, times(1)).setRedirectPage(StringUtils.EMPTY);
        verify(modelService, times(1)).save(targetCustomerModelMock);
    }

    @Test(expected = IllegalArgumentException.class)
    public void verifyResetRedirectPageForPasswordForNullUserId() {
        defaultTargetCustomerFacade.resetRedirectPageForPasswordResetToken(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetCustomerForUsernameWithBlankUsername() {
        defaultTargetCustomerFacade.getCustomerForUsername("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetCustomerForUsernameWithNullUsername() {
        defaultTargetCustomerFacade.getCustomerForUsername(null);
    }

    @Test
    public void testGetCustomerForUsernameWithUnknownUser() {
        given(userService.getUserForUID("idontexist@target.com.au", TargetCustomerModel.class))
                .willThrow(new UnknownIdentifierException("Unknown identifier!"));

        final TargetCustomerModel result = defaultTargetCustomerFacade
                .getCustomerForUsername("idontexist@target.com.au");

        assertThat(result).isNull();
    }

    @Test
    public void testGetCustomerForUsernameWithWrongUserType() {
        given(userService.getUserForUID("iamadmin@target.com.au", TargetCustomerModel.class))
                .willThrow(new ClassMismatchException(TargetCustomerModel.class, EmployeeModel.class));

        final TargetCustomerModel result = defaultTargetCustomerFacade.getCustomerForUsername("iamadmin@target.com.au");

        assertThat(result).isNull();
    }

    @Test
    public void testGetCustomerForUsername() {
        final TargetCustomerModel mockTargetCustomer = mock(TargetCustomerModel.class);
        given(userService.getUserForUID("customer@target.com.au", TargetCustomerModel.class))
                .willReturn(mockTargetCustomer);

        final TargetCustomerModel result = defaultTargetCustomerFacade.getCustomerForUsername("customer@target.com.au");

        assertThat(result).isEqualTo(mockTargetCustomer);

    }

    @Test
    public void testIsPasswordBlacklistedWithBlacklistedPasswords() {
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("123456")).isTrue();
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("1234567")).isTrue();
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("12345678")).isTrue();
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("123456111")).isTrue();
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("monkey")).isTrue();
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("MONKEY")).isTrue();
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("MoNkEy")).isTrue();
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("MoNkEY")).isTrue();
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("letmein")).isTrue();
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("123letmein")).isTrue();
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("letmein123")).isTrue();
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("111111")).isTrue();
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("111111111")).isTrue();
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("dddMoNkEyddd")).isTrue();
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("123456123123")).isTrue();
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("fffMoNkEy123")).isTrue();
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("rrrMoNkEyaaa")).isTrue();
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("pUrPlEmOnKeYdIsHwAsHeR")).isTrue();
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("dddMoNkEydddzzz")).isTrue();
    }

    @Test
    public void testIsPasswordBlacklistedWithNotBlacklistedPassword() {
        assertThat(defaultTargetCustomerFacade.isPasswordBlacklisted("!12;345F6")).isFalse();
    }

    @Test
    public void testGetCartUserEmailWithGuestEmail() {
        final CartModel mockCart = mock(CartModel.class);
        final TargetCustomerModel cartUser = mock(TargetCustomerModel.class);
        given(mockCart.getUser()).willReturn(cartUser);
        given(cartUser.getUid()).willReturn("123456|hello@world.com");
        given(cartUser.getType()).willReturn(CustomerType.GUEST);
        given(cartService.getSessionCart()).willReturn(mockCart);
        given(Boolean.valueOf(cartService.hasSessionCart())).willReturn(Boolean.TRUE);
        final String result = defaultTargetCustomerFacade.getCartUserEmail();
        assertThat(result).isEqualTo("hello@world.com");
    }

    @Test
    public void testGetCartUserEmailWithRegisteredEmail() {
        final CartModel mockCart = mock(CartModel.class);
        final TargetCustomerModel cartUser = mock(TargetCustomerModel.class);
        given(mockCart.getUser()).willReturn(cartUser);
        given(cartUser.getUid()).willReturn("hello@world.com");
        given(cartUser.getType()).willReturn(null);
        given(cartService.getSessionCart()).willReturn(mockCart);
        given(Boolean.valueOf(cartService.hasSessionCart())).willReturn(Boolean.TRUE);
        final String result = defaultTargetCustomerFacade.getCartUserEmail();
        assertThat(result).isEqualTo("hello@world.com");
    }

    @Test
    public void testGetCartUserEmailWithAnonymous() {
        final CartModel mockCart = mock(CartModel.class);
        final UserModel cartUser = mock(UserModel.class);
        given(mockCart.getUser()).willReturn(cartUser);
        given(cartUser.getUid()).willReturn(UserConstants.ANONYMOUS_CUSTOMER_UID);
        given(cartService.getSessionCart()).willReturn(mockCart);
        final String result = defaultTargetCustomerFacade.getCartUserEmail();
        assertThat(result).isEqualTo(null);
    }

    @Test
    public void testLoginSuccessHasSessionCart() {
        final CartModel mockCart = mock(CartModel.class);
        final UserModel guestUser = mock(TargetCustomerModel.class);
        final UserModel customer = mock(TargetCustomerModel.class);
        given(mockCart.getUser()).willReturn(guestUser);
        given(mockCart.getEntries()).willReturn(Arrays.asList(mock(AbstractOrderEntryModel.class)));
        given(guestUser.getUid()).willReturn(UserConstants.ANONYMOUS_CUSTOMER_UID);
        given(Boolean.valueOf(cartService.hasSessionCart())).willReturn(Boolean.TRUE);
        given(cartService.getSessionCart()).willReturn(mockCart);
        given(userService.getCurrentUser()).willReturn(customer);
        given(defaultTargetCustomerFacade.getCurrentCustomer()).willReturn(mock(CustomerData.class));
        given(storeSessionFacade.getDefaultCurrency()).willReturn(mock(CurrencyData.class));

        defaultTargetCustomerFacade.loginSuccess();
        verify(mockCart).setUser(customer);
        verify(modelService, never()).clone(any());
        verify(sessionService).removeAttribute(TgtFacadesConstants.ANONYMOUS_CHECKOUT);
        verify(sessionService).removeAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST);
    }

    @Test
    public void testLoginSuccessHasSessionCartChangeOwnerOfDeliveryAddress() {
        final CartModel mockCart = mock(CartModel.class);
        final TargetCustomerModel guestUser = mock(TargetCustomerModel.class);
        given(guestUser.getItemtype()).willReturn(TargetCustomerModel._TYPECODE);
        final TargetCustomerModel customer = mock(TargetCustomerModel.class);

        given(mockCart.getUser()).willReturn(guestUser);
        given(mockCart.getEntries()).willReturn(Arrays.asList(mock(AbstractOrderEntryModel.class)));
        final AddressModel oldAddressModel = mock(AddressModel.class);
        final AddressModel newAddressModel = mock(AddressModel.class);
        given(oldAddressModel.getOwner()).willReturn(guestUser);
        given(mockCart.getDeliveryAddress()).willReturn(oldAddressModel);
        given(modelService.clone(oldAddressModel)).willReturn(newAddressModel);
        given(Boolean.valueOf(cartService.hasSessionCart())).willReturn(Boolean.TRUE);
        given(cartService.getSessionCart()).willReturn(mockCart);
        given(userService.getCurrentUser()).willReturn(customer);
        given(defaultTargetCustomerFacade.getCurrentCustomer()).willReturn(mock(CustomerData.class));
        given(storeSessionFacade.getDefaultCurrency()).willReturn(mock(CurrencyData.class));

        defaultTargetCustomerFacade.loginSuccess();
        verify(mockCart).setUser(customer);
        verify(modelService).clone(oldAddressModel);
        verify(newAddressModel).setOwner(customer);
        verify(sessionService).removeAttribute(TgtFacadesConstants.ANONYMOUS_CHECKOUT);
        verify(sessionService).removeAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST);
    }

    @Test
    public void testLoginSuccessHasSessionCartRemoveGuestUser() {
        final CartModel mockCart = mock(CartModel.class);
        final TargetCustomerModel guestUser = mock(TargetCustomerModel.class);
        given(guestUser.getItemtype()).willReturn(TargetCustomerModel._TYPECODE);
        given(guestUser.getType()).willReturn(CustomerType.GUEST);
        final TargetCustomerModel customer = mock(TargetCustomerModel.class);
        given(customer.getAddresses()).willReturn(Arrays.asList(mock(AddressModel.class)));
        final TargetZoneDeliveryModeModel preferredDeliveryMode = mock(TargetZoneDeliveryModeModel.class);
        given(preferredDeliveryMode.getCode()).willReturn("CnC");
        final TargetZoneDeliveryModeModel cartDeliveryMode = mock(TargetZoneDeliveryModeModel.class);
        given(cartDeliveryMode.getCode()).willReturn("HD");

        given(mockCart.getDeliveryMode()).willReturn(cartDeliveryMode);
        given(mockCart.getUser()).willReturn(guestUser);
        given(mockCart.getEntries()).willReturn(Arrays.asList(mock(AbstractOrderEntryModel.class)));
        final AddressModel oldAddressModel = mock(AddressModel.class);
        given(oldAddressModel.getOwner()).willReturn(guestUser);
        given(mockCart.getDeliveryAddress()).willReturn(oldAddressModel);
        given(guestUser.getUid()).willReturn(UserConstants.ANONYMOUS_CUSTOMER_UID);
        given(Boolean.valueOf(cartService.hasSessionCart())).willReturn(Boolean.TRUE);
        given(cartService.getSessionCart()).willReturn(mockCart);
        given(userService.getCurrentUser()).willReturn(customer);
        given(defaultTargetCustomerFacade.getCurrentCustomer()).willReturn(mock(CustomerData.class));
        given(storeSessionFacade.getDefaultCurrency()).willReturn(mock(CurrencyData.class));
        final OrderModel previousOrder = mock(OrderModel.class);
        given(targetOrderService.findLatestOrderForUser(customer)).willReturn(previousOrder);
        given(targetPrepopulateCheckoutCartFacade.getPreferredDeliveryMode(customer, previousOrder))
                .willReturn(preferredDeliveryMode);

        defaultTargetCustomerFacade.loginSuccess();
        verify(mockCart).setUser(customer);
        verify(modelService).remove(guestUser);
        verify(mockCart).setDeliveryMode(null);
        verify(sessionService).removeAttribute(TgtFacadesConstants.ANONYMOUS_CHECKOUT);
        verify(sessionService).removeAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST);
        verify(mockCart).setDeliveryAddress(null);
    }

    @Test
    public void testLoginSuccessHasSessionCartDoesNotClearDeliveryModeIfSameAsPreferred() {
        final CartModel mockCart = mock(CartModel.class);
        final TargetCustomerModel guestUser = mock(TargetCustomerModel.class);
        given(guestUser.getItemtype()).willReturn(TargetCustomerModel._TYPECODE);
        given(guestUser.getType()).willReturn(CustomerType.GUEST);
        final TargetCustomerModel customer = mock(TargetCustomerModel.class);
        given(customer.getAddresses()).willReturn(Arrays.asList(mock(AddressModel.class)));
        final TargetZoneDeliveryModeModel preferredDeliveryMode = mock(TargetZoneDeliveryModeModel.class);
        given(preferredDeliveryMode.getCode()).willReturn("CnC");
        final TargetZoneDeliveryModeModel cartDeliveryMode = mock(TargetZoneDeliveryModeModel.class);
        given(cartDeliveryMode.getCode()).willReturn("CnC");

        given(mockCart.getDeliveryMode()).willReturn(cartDeliveryMode);
        given(mockCart.getUser()).willReturn(guestUser);
        given(mockCart.getEntries()).willReturn(Arrays.asList(mock(AbstractOrderEntryModel.class)));
        final AddressModel oldAddressModel = mock(AddressModel.class);
        given(oldAddressModel.getOwner()).willReturn(guestUser);
        given(mockCart.getDeliveryAddress()).willReturn(oldAddressModel);
        given(guestUser.getUid()).willReturn(UserConstants.ANONYMOUS_CUSTOMER_UID);
        given(Boolean.valueOf(cartService.hasSessionCart())).willReturn(Boolean.TRUE);
        given(cartService.getSessionCart()).willReturn(mockCart);
        given(userService.getCurrentUser()).willReturn(customer);
        given(defaultTargetCustomerFacade.getCurrentCustomer()).willReturn(mock(CustomerData.class));
        given(storeSessionFacade.getDefaultCurrency()).willReturn(mock(CurrencyData.class));
        final OrderModel previousOrder = mock(OrderModel.class);
        given(targetOrderService.findLatestOrderForUser(customer)).willReturn(previousOrder);
        given(targetPrepopulateCheckoutCartFacade.getPreferredDeliveryMode(customer, previousOrder))
                .willReturn(preferredDeliveryMode);

        defaultTargetCustomerFacade.loginSuccess();
        verify(mockCart).setUser(customer);
        verify(modelService).remove(guestUser);
        verify(sessionService).removeAttribute(TgtFacadesConstants.ANONYMOUS_CHECKOUT);
        verify(sessionService).removeAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST);
    }

    @Test
    public void testLoginSuccessHasSessionCartDoesntChangeDeliveryAddressForCnc() {
        final CartModel mockCart = mock(CartModel.class);
        final UserModel guestUser = mock(TargetCustomerModel.class);
        given(mockCart.getItemtype()).willReturn(CartModel._TYPECODE);
        final UserModel customer = mock(TargetCustomerModel.class);
        given(mockCart.getUser()).willReturn(guestUser);
        given(mockCart.getEntries()).willReturn(Arrays.asList(mock(AbstractOrderEntryModel.class)));
        final AddressModel oldAddressModel = mock(AddressModel.class);
        given(oldAddressModel.getOwner()).willReturn(mockCart);
        given(mockCart.getDeliveryAddress()).willReturn(oldAddressModel);
        final AddressData addressData = mock(AddressData.class);
        given(addressConverter.convert(oldAddressModel)).willReturn(addressData);
        given(guestUser.getUid()).willReturn(UserConstants.ANONYMOUS_CUSTOMER_UID);
        given(Boolean.valueOf(cartService.hasSessionCart())).willReturn(Boolean.TRUE);
        given(cartService.getSessionCart()).willReturn(mockCart);
        given(userService.getCurrentUser()).willReturn(customer);
        given(defaultTargetCustomerFacade.getCurrentCustomer()).willReturn(mock(CustomerData.class));
        given(storeSessionFacade.getDefaultCurrency()).willReturn(mock(CurrencyData.class));

        defaultTargetCustomerFacade.loginSuccess();
        verify(mockCart).setUser(customer);
        verify(modelService, never()).clone(any());
        verify(mockCart, never()).setDeliveryMode(null);
        verify(sessionService).removeAttribute(TgtFacadesConstants.ANONYMOUS_CHECKOUT);
        verify(sessionService).removeAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST);
    }

}

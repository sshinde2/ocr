package au.com.target.tgtfacades.customergroups.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultTargetCustomerGroupFacadeTest {

    private static final String GROUP_TO_CHECK = "group";

    @InjectMocks
    private final DefaultTargetCustomerGroupFacade defaultTargetCustomerGroupFacade = new DefaultTargetCustomerGroupFacade();

    @Mock
    private UserService userService;

    @Mock
    private ModelService modelService;


    private UserGroupModel userGroupToCheckModel;



    @SuppressWarnings("boxing")
    @Before
    public void setup() {
        final UserGroupModel baseCustomerGroupModel;
        final String baseCustomerGroupId = "baseCustomerGroupId";
        defaultTargetCustomerGroupFacade.setBaseCustomerGroupId(baseCustomerGroupId);
        baseCustomerGroupModel = Mockito.mock(UserGroupModel.class);
        BDDMockito.given(userService.getUserGroupForUID(baseCustomerGroupId)).willReturn(baseCustomerGroupModel);

        userGroupToCheckModel = Mockito.mock(UserGroupModel.class);
        BDDMockito.given(userService.isMemberOfGroup(userGroupToCheckModel, baseCustomerGroupModel))
                .willReturn(true);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testIsUserMemberOfTheGroupWhenTheUserIsPartOfAParentGroupOfGrpBeingChecked() throws Exception {

        final UserModel currentUser = Mockito.mock(UserModel.class);
        BDDMockito.given(userService.getCurrentUser()).willReturn(currentUser);
        final UserGroupModel currentUserGroup = Mockito.mock(UserGroupModel.class);
        final Set<UserGroupModel> userGroupList = new HashSet<>(Arrays.asList(currentUserGroup));
        BDDMockito.given(userService.getAllUserGroupsForUser(currentUser)).willReturn(userGroupList);
        BDDMockito.given(userService.getUserGroupForUID(GROUP_TO_CHECK)).willReturn(userGroupToCheckModel);
        BDDMockito.given(userService.isMemberOfGroup(currentUserGroup, userGroupToCheckModel))
                .willReturn(true);
        Assertions.assertThat(defaultTargetCustomerGroupFacade.isUserMemberOfTheGroup(GROUP_TO_CHECK)).isTrue();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testIsUserMemberOfTheGroupWhenTheUserIsNotPartOfAParentGrpOrIsNotSame() throws Exception {
        final UserModel currentUser = Mockito.mock(UserModel.class);
        BDDMockito.given(userService.getCurrentUser()).willReturn(currentUser);
        final UserGroupModel currentUserGroup = Mockito.mock(UserGroupModel.class);
        final Set<UserGroupModel> userGroupList = new HashSet<>(Arrays.asList(currentUserGroup));
        BDDMockito.given(userService.getAllUserGroupsForUser(currentUser)).willReturn(userGroupList);
        BDDMockito.given(userService.getUserGroupForUID(GROUP_TO_CHECK)).willReturn(userGroupToCheckModel);
        BDDMockito.given(userService.isMemberOfGroup(currentUserGroup, userGroupToCheckModel))
                .willReturn(false);
        Assertions.assertThat(defaultTargetCustomerGroupFacade.isUserMemberOfTheGroup(GROUP_TO_CHECK)).isFalse();
    }


    @Test
    public void testAddCustomerToGroup() {

        final UserModel currentUser = new UserModel();
        final String groupToAssociate = "newGroup";
        BDDMockito.given(userService.getUserGroupForUID(groupToAssociate)).willReturn(userGroupToCheckModel);
        final UserGroupModel existingCustomerGroup1 = Mockito.mock(UserGroupModel.class);
        final UserGroupModel existingCustomerGroup2 = Mockito.mock(UserGroupModel.class);

        final Set<PrincipalGroupModel> existingUserGroupList = new HashSet<PrincipalGroupModel>(
                Arrays.asList(existingCustomerGroup1, existingCustomerGroup2));
        currentUser.setGroups(existingUserGroupList);
        final ArgumentCaptor<UserModel> userModelToSave = ArgumentCaptor
                .forClass(UserModel.class);

        defaultTargetCustomerGroupFacade.addUserToCustomerGroup(groupToAssociate, currentUser);

        Mockito.verify(modelService).save(userModelToSave.capture());

        final UserModel userModelSaved = userModelToSave.getValue();
        Assertions.assertThat(userModelSaved.getGroups()).hasSize(3);
    }


}

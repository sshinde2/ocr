/**
 * 
 */
package au.com.target.tgtfacades.deals.converter.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.AbstractDealModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.deals.data.TargetDealData;
import au.com.target.tgtfacades.offer.converter.TargetMobileOfferHeadingListConverter;
import au.com.target.tgtfacades.offers.data.TargetMobileOfferHeadingData;
import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetDealsPopulatorTest {


    @InjectMocks
    private final TargetDealsPopulator targetDealsPopulator = new TargetDealsPopulator();

    @Mock
    private TargetMobileOfferHeadingListConverter targetMobileOfferHeadingListConverter;


    @Test
    public void testTargetDealsPopulate() {

        final DateTime currentDate = new DateTime();
        final DateTime createdDate = currentDate.minusDays(10);
        final DateTime modifiedDate = currentDate.minusDays(6);
        final DateTime startDate = currentDate.minusDays(2);
        final DateTime endDate = currentDate.plusDays(5);

        final AbstractDealModel source = new AbstractDealModel();
        source.setLongTitle("Long Title");
        source.setCode("101");
        source.setStartDate(startDate.toDate());
        source.setEndDate(endDate.toDate());
        source.setModifiedtime(modifiedDate.toDate());
        source.setCreationtime(createdDate.toDate());
        source.setCode("101");
        source.setEnabled(Boolean.TRUE);
        source.setFeatured(Boolean.TRUE);
        source.setAvailableForMobile(Boolean.TRUE);
        source.setPriority(Integer.valueOf(2));
        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel = Mockito
                .mock(TargetMobileOfferHeadingModel.class);

        final List<TargetMobileOfferHeadingModel> targetMobileOfferHeadingList = new ArrayList<TargetMobileOfferHeadingModel>(
                Arrays.asList(targetMobileOfferHeadingModel));
        source.setTargetMobileOfferHeadings(targetMobileOfferHeadingList);

        final TargetMobileOfferHeadingData targetMobileOfferHeadingData = Mockito
                .mock(TargetMobileOfferHeadingData.class);

        final List<TargetMobileOfferHeadingData> targetMobileOfferHeadingDataList = new ArrayList<TargetMobileOfferHeadingData>(
                Arrays.asList(targetMobileOfferHeadingData));

        BDDMockito.given(targetMobileOfferHeadingListConverter.convert(targetMobileOfferHeadingList)).willReturn(
                targetMobileOfferHeadingDataList);

        final TargetDealData target = new TargetDealData();

        targetDealsPopulator.populate(source, target);
        Assert.assertNotNull(target);
        Assert.assertEquals(source.getCode(), target.getCode());
        Assert.assertEquals(source.getStartDate(), target.getStartDate());
        Assert.assertEquals(source.getEndDate(), target.getEndDate());
        Assert.assertEquals(source.getDescription(), target.getDescription());
        Assert.assertEquals(source.getLongTitle(), target.getLongTitle());
        Assert.assertEquals(source.getTitle(), target.getTitle());
        Assert.assertEquals(source.getEnabled(), target.getEnabled());
        Assert.assertEquals(source.getFeatured(), target.getFeatured());
        Assert.assertEquals(source.getAvailableForMobile(), target.getAvailableForMobile());
        Assert.assertEquals(createdDate, target.getCreatedTimeFormatted());
        Assert.assertEquals(modifiedDate, target.getModifiedTimeFormatted());
        Assert.assertEquals(startDate, target.getStartTimeFormatted());
        Assert.assertEquals(endDate, target.getEndTimeFormatted());
        Assert.assertNotNull(target.getTargetMobileOfferHeadings());
        Assert.assertSame(targetMobileOfferHeadingData, target.getTargetMobileOfferHeadings().get(0));

    }
}

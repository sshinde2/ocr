/**
 * 
 */
package au.com.target.tgtfacades.deals.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.AbstractDealModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.deals.converter.TargetDealsConverter;
import au.com.target.tgtfacades.deals.data.TargetDealData;
import au.com.target.tgtfacades.deals.data.TargetDealsData;
import au.com.target.tgtfacades.offer.TargetMobileOfferHeadingFacade;
import au.com.target.tgtfacades.offers.data.TargetMobileOfferHeadingData;
import au.com.target.tgtmarketing.deals.TargetMobileDealService;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetDealsFacadeImplTest {

    @InjectMocks
    private final TargetDealsFacadeImpl targetDealsFacade = new TargetDealsFacadeImpl();

    @Mock
    private TargetMobileDealService targetMobileDealService;

    @Mock
    private TargetMobileOfferHeadingFacade targetMobileOfferHeadingFacade;

    @Mock
    private TargetDealsConverter targetDealsConverter;

    @Before
    public void setup() {
        targetDealsFacade.setTargetDealsConverter(targetDealsConverter);
        targetDealsFacade.setTargetMobileDealService(targetMobileDealService);
        targetDealsFacade.setTargetMobileOfferHeadingFacade(targetMobileOfferHeadingFacade);
    }

    @Test
    public void testAllMobileActiveDealsWhenNoDealsAvailable() {

        BDDMockito.given(targetMobileDealService.getAllActiveMobileDeals()).willReturn(null);

        final List<TargetDealData> targetDealsDataResult = targetDealsFacade.getAllMobileActiveDeals();
        Assert.assertNotNull(targetDealsDataResult);
        Assert.assertTrue(CollectionUtils.isEmpty(targetDealsDataResult));
    }

    @Test
    public void testAllMobileActiveDealsWhenValidDealsAvailable() {

        final List<AbstractDealModel> abstractDealExpected = setupAbstractDeals();
        final List<TargetDealData> targetDealExpected = setupTargetDeals(abstractDealExpected);
        final List<TargetDealData> targetDealsDataResult = targetDealsFacade.getAllMobileActiveDeals();
        Assert.assertNotNull(targetDealsDataResult);
        Assert.assertTrue(CollectionUtils.isNotEmpty(targetDealsDataResult));
        Assert.assertSame(targetDealExpected.get(0), targetDealsDataResult.get(0));
        Assert.assertSame(targetDealExpected.get(1), targetDealsDataResult.get(1));
    }

    @Test
    public void testAllMobileActiveDealsWithOfferHeadingsWhenValidDealsAvailable() {

        final List<AbstractDealModel> abstractDealExpected = setupAbstractDeals();
        final List<TargetDealData> targetDealExpected = setupTargetDeals(abstractDealExpected);

        final TargetMobileOfferHeadingData targetMobileOfferHeadingData1 = Mockito
                .mock(TargetMobileOfferHeadingData.class);
        final TargetMobileOfferHeadingData targetMobileOfferHeadingData2 = Mockito
                .mock(TargetMobileOfferHeadingData.class);
        BDDMockito.given(targetMobileOfferHeadingFacade.getTargetMobileOfferHeadings()).willReturn(
                new ArrayList<TargetMobileOfferHeadingData>(Arrays.asList(targetMobileOfferHeadingData1,
                        targetMobileOfferHeadingData2)));

        final TargetDealsData targetDealsDataResult = targetDealsFacade.getAllMobileActiveDealsWithOfferHeadings();

        Assert.assertNotNull(targetDealsDataResult);
        final List<TargetDealData> targetDealListResult = targetDealsDataResult.getTargetDeals();
        Assert.assertTrue(CollectionUtils.isNotEmpty(targetDealListResult));
        Assert.assertSame(targetDealExpected.get(0), targetDealListResult.get(0));
        Assert.assertSame(targetDealExpected.get(1), targetDealListResult.get(1));
        final List<TargetMobileOfferHeadingData> targetMobileOfferHeadingDataList = targetDealsDataResult
                .getTargetMobileOfferHeadings();
        Assert.assertTrue(CollectionUtils.isNotEmpty(targetMobileOfferHeadingDataList));
        Assert.assertSame(targetMobileOfferHeadingData1, targetMobileOfferHeadingDataList.get(0));
        Assert.assertSame(targetMobileOfferHeadingData2, targetMobileOfferHeadingDataList.get(1));


    }

    private List<AbstractDealModel> setupAbstractDeals() {

        final AbstractDealModel abstractDealModel1 = Mockito.mock(AbstractDealModel.class);
        final AbstractDealModel abstractDealModel2 = Mockito.mock(AbstractDealModel.class);

        final List<AbstractDealModel> abstractDealExpected = new ArrayList<AbstractDealModel>(Arrays.asList(
                abstractDealModel1, abstractDealModel2));

        BDDMockito.given(targetMobileDealService.getAllActiveMobileDeals()).willReturn(abstractDealExpected);

        return abstractDealExpected;
    }

    private List<TargetDealData> setupTargetDeals(final List<AbstractDealModel> abstractDealExpected) {

        final TargetDealData targetDealData1 = Mockito.mock(TargetDealData.class);
        final TargetDealData targetDealData2 = Mockito.mock(TargetDealData.class);

        BDDMockito.given(targetDealsConverter.convert(abstractDealExpected.get(0))).willReturn(targetDealData1);
        BDDMockito.given(targetDealsConverter.convert(abstractDealExpected.get(1))).willReturn(targetDealData2);

        final List<TargetDealData> targetDealExpected = new ArrayList<TargetDealData>(Arrays.asList(
                targetDealData1, targetDealData2));

        return targetDealExpected;
    }



}

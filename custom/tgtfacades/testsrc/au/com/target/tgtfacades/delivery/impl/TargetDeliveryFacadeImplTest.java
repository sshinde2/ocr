/**
 * 
 */
package au.com.target.tgtfacades.delivery.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtfacades.delivery.data.TargetRegionData;


/**
 * @author rmcalave
 * 
 */
@UnitTest
public class TargetDeliveryFacadeImplTest {
    @Mock
    private TargetDeliveryService mockDeliveryService;

    @Mock
    private CommonI18NService mockCommonI18NService;

    @Mock
    private Converter<RegionModel, TargetRegionData> mockTargetRegionConverter;

    @InjectMocks
    private final TargetDeliveryFacadeImpl targetDeliveryFacadeImpl = new TargetDeliveryFacadeImpl();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetRegionsForCountryWithNoCountry() {
        final String countryIsoCode = "AU";

        final List<TargetRegionData> targetRegions = targetDeliveryFacadeImpl.getRegionsForCountry(countryIsoCode);

        Assert.assertNull(targetRegions);
    }

    @Test
    public void testGetRegionsForCountryWithNoRegions() {
        final String countryIsoCode = "AU";

        final CountryModel mockCountry = Mockito.mock(CountryModel.class);
        BDDMockito.given(mockCommonI18NService.getCountry(countryIsoCode)).willReturn(mockCountry);

        final List<TargetRegionData> targetRegions = targetDeliveryFacadeImpl.getRegionsForCountry(countryIsoCode);

        Assert.assertNull(targetRegions);
    }

    @Test
    public void testGetRegionsForCountryWithOneRegion() {
        final String countryIsoCode = "AU";

        final CountryModel mockCountry = Mockito.mock(CountryModel.class);
        BDDMockito.given(mockCommonI18NService.getCountry(countryIsoCode)).willReturn(mockCountry);

        final RegionModel mockRegion = Mockito.mock(RegionModel.class);
        final List<RegionModel> regionList = Collections.singletonList(mockRegion);

        BDDMockito.given(mockDeliveryService.getRegionsForCountry(mockCountry)).willReturn(regionList);

        final TargetRegionData targetRegion = new TargetRegionData();
        BDDMockito.given(mockTargetRegionConverter.convert(mockRegion)).willReturn(targetRegion);

        final List<TargetRegionData> targetRegions = targetDeliveryFacadeImpl.getRegionsForCountry(countryIsoCode);

        Assert.assertNotNull(targetRegions);
        Assert.assertEquals(1, targetRegions.size());
        Assert.assertEquals(targetRegion, targetRegions.get(0));
    }

    @Test
    public void testGetRegionsForCountryWithTwoRegions() {
        final String countryIsoCode = "AU";

        final CountryModel mockCountry = Mockito.mock(CountryModel.class);
        BDDMockito.given(mockCommonI18NService.getCountry(countryIsoCode)).willReturn(mockCountry);

        final RegionModel mockRegionOne = Mockito.mock(RegionModel.class);
        final RegionModel mockRegionTwo = Mockito.mock(RegionModel.class);

        final List<RegionModel> regionList = new ArrayList<>();
        regionList.add(mockRegionOne);
        regionList.add(mockRegionTwo);

        BDDMockito.given(mockDeliveryService.getRegionsForCountry(mockCountry)).willReturn(regionList);

        final TargetRegionData targetRegionOne = new TargetRegionData();
        final TargetRegionData targetRegionTwo = new TargetRegionData();
        BDDMockito.given(mockTargetRegionConverter.convert(mockRegionOne)).willReturn(targetRegionOne);
        BDDMockito.given(mockTargetRegionConverter.convert(mockRegionTwo)).willReturn(targetRegionTwo);

        final List<TargetRegionData> targetRegions = targetDeliveryFacadeImpl.getRegionsForCountry(countryIsoCode);

        Assert.assertNotNull(targetRegions);
        Assert.assertEquals(2, targetRegions.size());
        Assert.assertEquals(targetRegionOne, targetRegions.get(0));
        Assert.assertEquals(targetRegionTwo, targetRegions.get(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetRegionsForCountryWithNullCountryCode() {
        targetDeliveryFacadeImpl.getRegionsForCountry(null);
    }
}

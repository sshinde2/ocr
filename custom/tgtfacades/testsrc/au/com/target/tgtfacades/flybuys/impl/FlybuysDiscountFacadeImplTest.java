/**
 * 
 */
package au.com.target.tgtfacades.flybuys.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.voucher.model.PromotionVoucherModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetRuntimeException;
import au.com.target.tgtcore.flybuys.dto.FlybuysDiscountDenialDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysAuthenticateResponseDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysRedeemTierDto;
import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;
import au.com.target.tgtcore.flybuys.service.FlybuysDiscountService;
import au.com.target.tgtcore.flybuys.service.FlybuysRedeemConfigService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.model.FlybuysRedeemConfigModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedeemConfigData;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedeemTierData;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedemptionData;
import au.com.target.tgtfacades.flybuys.data.FlybuysRequestStatusData;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.converters.FlybuysRedeemConfigConverter;


/**
 * @author knemalik
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FlybuysDiscountFacadeImplTest {

    private static final String AUD_CURRENCY_ISOCODE = "AUD";

    @Mock
    private TargetVoucherService mockTargetVoucherService;

    @Mock
    private FlybuysDiscountService mockFlybuysDiscountService;

    @Mock
    private FlybuysRedeemConfigService mockFlybuysRedeemConfigService;

    @Mock
    private FlybuysAuthenticateResponseDto mockFlybuysAuthenticateResponseDto;

    @Mock
    private SessionService mockSessionService;

    @Mock
    private PriceDataFactory mockPriceDataFactory;

    @Mock
    private TargetCheckoutFacade mockTargetCheckoutFacade;

    @InjectMocks
    private final FlybuysDiscountFacadeImpl flybuysDiscountFacadeImpl = new FlybuysDiscountFacadeImpl();

    @Mock
    private CurrencyModel mockCurrency;

    @Mock
    private CartModel mockCart;

    @Mock
    private ModelService modelService;

    @Mock
    private CommerceCartService commerceCartService;

    @Mock
    private AbstractOrderEntryModel abstractOrderEntry;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private Configuration configuration;

    @Mock
    private ProductTypeModel mockProductType1;

    @Mock
    private ProductTypeModel mockProductType2;

    @Mock
    private AbstractOrderEntryModel mockOrderEntry1;

    @Mock
    private AbstractOrderEntryModel mockOrderEntry2;

    @Mock
    private TargetProductModel mockProduct1;

    @Mock
    private TargetProductModel mockProduct2;

    @Mock
    private AbstractTargetVariantProductModel mockVariantProduct1;

    @Mock
    private AbstractTargetVariantProductModel mockVariantProduct2;

    @Mock
    private FlybuysRedeemTierDto mock10DollarFlybuysRedeemTierDto;

    @Mock
    private FlybuysRedeemTierDto mock20DollarFlybuysRedeemTierDto;

    @Mock
    private FlybuysRedeemTierDto mock30DollarFlybuysRedeemTierDto;

    @Mock
    private PriceData mockPriceData1;

    @Mock
    private PriceData mockPriceData2;

    @Mock
    private PriceData mockPriceData3;


    @Before
    public void setup() {
        given(mockCurrency.getIsocode()).willReturn(AUD_CURRENCY_ISOCODE);
        given(mockCart.getCurrency()).willReturn(mockCurrency);
        given(mockCart.getDeliveryCost()).willReturn(Double.valueOf(0.0d));
        given(mockTargetCheckoutFacade.getCart()).willReturn(mockCart);
        given(configurationService.getConfiguration()).willReturn(configuration);
        given(configuration.getBigDecimal(TgtFacadesConstants.FLYBUYS_CART_THRESHOLD_LIMIT, BigDecimal.ZERO))
                .willReturn(BigDecimal.ZERO);
    }

    @Test
    public void testGetFlybuysRedemptionDataSuccess() {

        final String flybuysCardNumber = "82972778169675";
        final Date dateOfBirth = new Date();
        final String postCode = "3220";

        final FlybuysResponseType dummySuccessResponse = FlybuysResponseType.SUCCESS;

        when(mockFlybuysDiscountService.authenticateFlybuys(flybuysCardNumber, dateOfBirth, postCode)).thenReturn(
                mockFlybuysAuthenticateResponseDto);
        when(mockFlybuysAuthenticateResponseDto.getResponse()).thenReturn(FlybuysResponseType.SUCCESS);

        final FlybuysRequestStatusData flybuysRequestStatusData = flybuysDiscountFacadeImpl.getFlybuysRedemptionData(
                flybuysCardNumber, dateOfBirth, postCode);

        assertThat(flybuysRequestStatusData.getResponse()).isEqualTo(dummySuccessResponse);
        assertThat(flybuysRequestStatusData.getErrorMessage()).isNull();
        verify(mockSessionService).setAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE,
                mockFlybuysAuthenticateResponseDto);
    }

    @Test
    public void testGetFlybuysRedemptionDataError() {

        final String flybuysCardNumber = "23472778169675";
        final Date dateOfBirth = new Date();
        final String postCode = "3009";
        final String dummyErrorMessage = "Error response code is 1";

        final FlybuysResponseType dummyIsError = FlybuysResponseType.FLYBUYS_OTHER_ERROR;

        when(mockFlybuysDiscountService.authenticateFlybuys(flybuysCardNumber, dateOfBirth, postCode)).thenReturn(
                mockFlybuysAuthenticateResponseDto);
        when(mockFlybuysAuthenticateResponseDto.getResponse()).thenReturn(FlybuysResponseType.FLYBUYS_OTHER_ERROR);
        when(mockFlybuysAuthenticateResponseDto.getErrorMessage()).thenReturn(dummyErrorMessage);

        final FlybuysRequestStatusData flybuysRequestStatusData = flybuysDiscountFacadeImpl.getFlybuysRedemptionData(
                flybuysCardNumber, dateOfBirth, postCode);

        assertThat(flybuysRequestStatusData.getResponse()).isEqualTo(dummyIsError);

        assertThat(flybuysRequestStatusData.getErrorMessage()).isNotNull();
    }

    @Test
    public void testGetFlybuysRedemptionDataNull() {

        final String flybuysCardNumber = "23472778169675";
        final Date dateOfBirth = new Date();
        final String postCode = "3009";

        when(mockFlybuysDiscountService.authenticateFlybuys(flybuysCardNumber, dateOfBirth, postCode)).thenReturn(null);

        final FlybuysRequestStatusData flybuysRequestStatusData = flybuysDiscountFacadeImpl.getFlybuysRedemptionData(
                flybuysCardNumber, dateOfBirth, postCode);

        assertThat(flybuysRequestStatusData.getResponse()).isEqualTo(FlybuysResponseType.UNAVAILABLE);
        assertThat(flybuysRequestStatusData.getErrorMessage()).isNotEmpty();
    }

    @Test
    public void testGetFlybuysRedemptionDataForCheckoutCartWithNoDataInSession() throws Exception {
        final FlybuysRedemptionData flybuysRedemptionData = flybuysDiscountFacadeImpl
                .getFlybuysRedemptionDataForCheckoutCart();

        assertThat(flybuysRedemptionData).isNull();
    }

    @Test
    public void testGetFlybuysRedemptionDataForCheckoutCartWithPointsNoTiers() throws Exception {
        given(mockFlybuysAuthenticateResponseDto.getAvailPoints()).willReturn(Integer.valueOf(1500));

        given(mockSessionService.getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE)).willReturn(
                mockFlybuysAuthenticateResponseDto);

        final FlybuysRedemptionData flybuysRedemptionData = flybuysDiscountFacadeImpl
                .getFlybuysRedemptionDataForCheckoutCart();

        assertThat(flybuysRedemptionData).isNotNull();
        assertThat(flybuysRedemptionData.getAvailablePoints()).isEqualTo(1500);
        assertThat(flybuysRedemptionData.getRedeemTiers()).isNotNull().isEmpty();
    }

    @Test
    public void testGetFlybuysRedemptionDataForCheckoutCartWithPointsOneTierValidForCart() throws Exception {
        final FlybuysRedeemTierDto mockFlybuysRedeemTierDto = mock(FlybuysRedeemTierDto.class);
        given(mockFlybuysRedeemTierDto.getDollarAmt()).willReturn(BigDecimal.valueOf(10.00));
        given(mockFlybuysRedeemTierDto.getPoints()).willReturn(Integer.valueOf(2000));
        given(mockFlybuysRedeemTierDto.getRedeemCode()).willReturn("tendollarydoos");

        given(mockFlybuysAuthenticateResponseDto.getAvailPoints()).willReturn(Integer.valueOf(2500));
        given(mockFlybuysAuthenticateResponseDto.getRedeemTiers()).willReturn(
                Collections.singletonList(mockFlybuysRedeemTierDto));

        given(mockSessionService.getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE)).willReturn(
                mockFlybuysAuthenticateResponseDto);

        final PriceData mockPriceData = mock(PriceData.class);
        given(mockPriceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(10.00), AUD_CURRENCY_ISOCODE))
                .willReturn(
                        mockPriceData);

        given(mockCart.getSubtotal()).willReturn(Double.valueOf(150.00));

        final FlybuysRedemptionData flybuysRedemptionData = flybuysDiscountFacadeImpl
                .getFlybuysRedemptionDataForCheckoutCart();

        assertThat(flybuysRedemptionData).isNotNull();
        assertThat(flybuysRedemptionData.getAvailablePoints()).isEqualTo(2500);

        assertThat(flybuysRedemptionData.getRedeemTiers()).hasSize(1);
        final FlybuysRedeemTierData flybuysRedeemTier = flybuysRedemptionData.getRedeemTiers().iterator().next();

        assertThat(flybuysRedeemTier.getDollars()).isEqualTo(mockPriceData);
        assertThat(flybuysRedeemTier.getPoints()).isEqualTo(Integer.valueOf(2000));
        assertThat(flybuysRedeemTier.getRedeemCode()).isEqualTo("tendollarydoos");
    }

    @Test
    public void testGetFlybuysRedemptionDataForCheckoutCartWithPointsThreeTiersTwoValidForCart() throws Exception {
        given(mock10DollarFlybuysRedeemTierDto.getDollarAmt()).willReturn(BigDecimal.valueOf(10.00));
        given(mock10DollarFlybuysRedeemTierDto.getPoints()).willReturn(Integer.valueOf(2000));
        given(mock10DollarFlybuysRedeemTierDto.getRedeemCode()).willReturn("tendollarydoos");

        given(mock20DollarFlybuysRedeemTierDto.getDollarAmt()).willReturn(BigDecimal.valueOf(20.00));
        given(mock20DollarFlybuysRedeemTierDto.getPoints()).willReturn(Integer.valueOf(4000));
        given(mock20DollarFlybuysRedeemTierDto.getRedeemCode()).willReturn("twentydollarydoos");

        given(mock30DollarFlybuysRedeemTierDto.getDollarAmt()).willReturn(BigDecimal.valueOf(30.00));
        given(mock30DollarFlybuysRedeemTierDto.getPoints()).willReturn(Integer.valueOf(6000));
        given(mock30DollarFlybuysRedeemTierDto.getRedeemCode()).willReturn("thirtydollarydoos");

        given(mockFlybuysAuthenticateResponseDto.getAvailPoints()).willReturn(Integer.valueOf(6500));

        final List<FlybuysRedeemTierDto> redeemTiers = new ArrayList<>();
        redeemTiers.add(mock10DollarFlybuysRedeemTierDto);
        redeemTiers.add(mock20DollarFlybuysRedeemTierDto);
        redeemTiers.add(mock30DollarFlybuysRedeemTierDto);
        given(mockFlybuysAuthenticateResponseDto.getRedeemTiers()).willReturn(redeemTiers);

        given(mockSessionService.getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE)).willReturn(
                mockFlybuysAuthenticateResponseDto);

        final PriceData mock10DollarPriceData = mock(PriceData.class);
        given(mockPriceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(10.00), AUD_CURRENCY_ISOCODE))
                .willReturn(
                        mock10DollarPriceData);
        final PriceData mock20DollarPriceData = mock(PriceData.class);
        given(mockPriceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(20.00), AUD_CURRENCY_ISOCODE))
                .willReturn(
                        mock20DollarPriceData);

        given(mockCart.getSubtotal()).willReturn(Double.valueOf(25.00));

        final FlybuysRedemptionData flybuysRedemptionData = flybuysDiscountFacadeImpl
                .getFlybuysRedemptionDataForCheckoutCart();

        assertThat(flybuysRedemptionData).isNotNull();
        assertThat(flybuysRedemptionData.getAvailablePoints()).isEqualTo(6500);

        assertThat(flybuysRedemptionData.getRedeemTiers()).hasSize(2);

        final FlybuysRedeemTierData flybuys20DollarRedeemTier = flybuysRedemptionData.getRedeemTiers().get(0);
        assertThat(flybuys20DollarRedeemTier.getDollars()).isEqualTo(mock20DollarPriceData);
        assertThat(flybuys20DollarRedeemTier.getPoints()).isEqualTo(Integer.valueOf(4000));
        assertThat(flybuys20DollarRedeemTier.getRedeemCode()).isEqualTo("twentydollarydoos");

        final FlybuysRedeemTierData flybuys10DollarRedeemTier = flybuysRedemptionData.getRedeemTiers().get(1);
        assertThat(flybuys10DollarRedeemTier.getDollars()).isEqualTo(mock10DollarPriceData);
        assertThat(flybuys10DollarRedeemTier.getPoints()).isEqualTo(Integer.valueOf(2000));
        assertThat(flybuys10DollarRedeemTier.getRedeemCode()).isEqualTo("tendollarydoos");
    }

    @Test
    public void testGetFlybuysRedemptionDataForCheckoutCartWithPointsThreeTiersForCartValue()
            throws Exception {
        given(mock10DollarFlybuysRedeemTierDto.getDollarAmt()).willReturn(BigDecimal.valueOf(10.00));
        given(mock10DollarFlybuysRedeemTierDto.getPoints()).willReturn(Integer.valueOf(2000));
        given(mock10DollarFlybuysRedeemTierDto.getRedeemCode()).willReturn("tendollarydoos");

        given(mock20DollarFlybuysRedeemTierDto.getDollarAmt()).willReturn(BigDecimal.valueOf(20.00));
        given(mock20DollarFlybuysRedeemTierDto.getPoints()).willReturn(Integer.valueOf(4000));
        given(mock20DollarFlybuysRedeemTierDto.getRedeemCode()).willReturn("twentydollarydoos");

        given(mock30DollarFlybuysRedeemTierDto.getDollarAmt()).willReturn(BigDecimal.valueOf(30.00));
        given(mock30DollarFlybuysRedeemTierDto.getPoints()).willReturn(Integer.valueOf(6000));
        given(mock30DollarFlybuysRedeemTierDto.getRedeemCode()).willReturn("thirtydollarydoos");

        given(mockFlybuysAuthenticateResponseDto.getAvailPoints()).willReturn(Integer.valueOf(6500));

        final List<FlybuysRedeemTierDto> redeemTiers = new ArrayList<>();
        redeemTiers.add(mock10DollarFlybuysRedeemTierDto);
        redeemTiers.add(mock20DollarFlybuysRedeemTierDto);
        redeemTiers.add(mock30DollarFlybuysRedeemTierDto);
        given(mockFlybuysAuthenticateResponseDto.getRedeemTiers()).willReturn(redeemTiers);

        given(mockSessionService.getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE)).willReturn(
                mockFlybuysAuthenticateResponseDto);

        final PriceData mock10DollarPriceData = mock(PriceData.class);
        given(mockPriceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(10.00), AUD_CURRENCY_ISOCODE))
                .willReturn(
                        mock10DollarPriceData);
        final PriceData mock20DollarPriceData = mock(PriceData.class);
        given(mockPriceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(20.00), AUD_CURRENCY_ISOCODE))
                .willReturn(
                        mock20DollarPriceData);

        given(mockCart.getSubtotal()).willReturn(Double.valueOf(21.00));

        final FlybuysRedemptionData flybuysRedemptionData = flybuysDiscountFacadeImpl
                .getFlybuysRedemptionDataForCheckoutCart();

        assertThat(flybuysRedemptionData).isNotNull();
        assertThat(flybuysRedemptionData.getAvailablePoints()).isEqualTo(6500);

        assertThat(flybuysRedemptionData.getRedeemTiers()).hasSize(2);

        final FlybuysRedeemTierData flybuys20DollarRedeemTier = flybuysRedemptionData.getRedeemTiers().get(0);
        assertThat(flybuys20DollarRedeemTier.getDollars()).isEqualTo(mock20DollarPriceData);
        assertThat(flybuys20DollarRedeemTier.getPoints()).isEqualTo(Integer.valueOf(4000));
        assertThat(flybuys20DollarRedeemTier.getRedeemCode()).isEqualTo("twentydollarydoos");

        final FlybuysRedeemTierData flybuys10DollarRedeemTier = flybuysRedemptionData.getRedeemTiers().get(1);
        assertThat(flybuys10DollarRedeemTier.getDollars()).isEqualTo(mock10DollarPriceData);
        assertThat(flybuys10DollarRedeemTier.getPoints()).isEqualTo(Integer.valueOf(2000));
        assertThat(flybuys10DollarRedeemTier.getRedeemCode()).isEqualTo("tendollarydoos");
    }

    @Test
    public void testGetFlybuysRedemptionDataForCheckoutCartWithPointsThreeTiersOneExactlyForCartValue()
            throws Exception {
        given(mock10DollarFlybuysRedeemTierDto.getDollarAmt()).willReturn(BigDecimal.valueOf(10.00));
        given(mock10DollarFlybuysRedeemTierDto.getPoints()).willReturn(Integer.valueOf(2000));
        given(mock10DollarFlybuysRedeemTierDto.getRedeemCode()).willReturn("tendollarydoos");

        given(mock20DollarFlybuysRedeemTierDto.getDollarAmt()).willReturn(BigDecimal.valueOf(20.00));
        given(mock20DollarFlybuysRedeemTierDto.getPoints()).willReturn(Integer.valueOf(4000));
        given(mock20DollarFlybuysRedeemTierDto.getRedeemCode()).willReturn("twentydollarydoos");

        given(mock30DollarFlybuysRedeemTierDto.getDollarAmt()).willReturn(BigDecimal.valueOf(30.00));
        given(mock30DollarFlybuysRedeemTierDto.getPoints()).willReturn(Integer.valueOf(6000));
        given(mock30DollarFlybuysRedeemTierDto.getRedeemCode()).willReturn("thirtydollarydoos");

        given(mockFlybuysAuthenticateResponseDto.getAvailPoints()).willReturn(Integer.valueOf(6500));

        final List<FlybuysRedeemTierDto> redeemTiers = new ArrayList<>();
        redeemTiers.add(mock10DollarFlybuysRedeemTierDto);
        redeemTiers.add(mock20DollarFlybuysRedeemTierDto);
        redeemTiers.add(mock30DollarFlybuysRedeemTierDto);
        given(mockFlybuysAuthenticateResponseDto.getRedeemTiers()).willReturn(redeemTiers);

        given(mockSessionService.getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE)).willReturn(
                mockFlybuysAuthenticateResponseDto);

        final PriceData mock10DollarPriceData = mock(PriceData.class);
        given(mockPriceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(10.00), AUD_CURRENCY_ISOCODE))
                .willReturn(
                        mock10DollarPriceData);
        final PriceData mock20DollarPriceData = mock(PriceData.class);
        given(mockPriceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(20.00), AUD_CURRENCY_ISOCODE))
                .willReturn(
                        mock20DollarPriceData);

        given(mockCart.getSubtotal()).willReturn(Double.valueOf(20.00));

        final FlybuysRedemptionData flybuysRedemptionData = flybuysDiscountFacadeImpl
                .getFlybuysRedemptionDataForCheckoutCart();

        assertThat(flybuysRedemptionData).isNotNull();
        assertThat(flybuysRedemptionData.getAvailablePoints()).isEqualTo(6500);

        assertThat(flybuysRedemptionData.getRedeemTiers()).hasSize(1);

        final FlybuysRedeemTierData flybuys10DollarRedeemTier = flybuysRedemptionData.getRedeemTiers().get(0);
        assertThat(flybuys10DollarRedeemTier.getDollars()).isEqualTo(mock10DollarPriceData);
        assertThat(flybuys10DollarRedeemTier.getPoints()).isEqualTo(Integer.valueOf(2000));
        assertThat(flybuys10DollarRedeemTier.getRedeemCode()).isEqualTo("tendollarydoos");
    }

    @Test
    public void testGetFlybuysRedeemConfigDataNull() {

        when(mockFlybuysRedeemConfigService.getFlybuysRedeemConfig()).thenReturn(null);

        assertThat(flybuysDiscountFacadeImpl.getFlybuysRedeemConfigData()).isNull();
    }

    @Test
    public void testGetFlybuysRedeemConfigDataException() {

        when(mockFlybuysRedeemConfigService.getFlybuysRedeemConfig()).thenThrow(new TargetRuntimeException(null, null));

        assertThat(flybuysDiscountFacadeImpl.getFlybuysRedeemConfigData()).isNull();
    }

    @Test
    public void testGetFlybuysRedeemConfigData() {

        final FlybuysRedeemConfigModel flybuysRedeemConfigModel = mock(FlybuysRedeemConfigModel.class);
        final FlybuysRedeemConfigData flybuysRedeemConfigData = mock(FlybuysRedeemConfigData.class);
        final FlybuysRedeemConfigConverter flybuysRedeemConfigConverter = mock(FlybuysRedeemConfigConverter.class);
        flybuysDiscountFacadeImpl.setFlybuysRedeemConfigConverter(flybuysRedeemConfigConverter);

        when(mockFlybuysRedeemConfigService.getFlybuysRedeemConfig()).thenReturn(flybuysRedeemConfigModel);
        when(flybuysRedeemConfigConverter.convert(flybuysRedeemConfigModel)).thenReturn(
                flybuysRedeemConfigData);

        assertThat(flybuysDiscountFacadeImpl.getFlybuysRedeemConfigData()).isEqualTo(flybuysRedeemConfigData);
    }

    @Test
    public void testGetFlybuysDiscountAllowedNull() {
        given(mockTargetVoucherService.getFlybuysDiscountAllowed(mockCart)).willReturn(null);
        assertThat(flybuysDiscountFacadeImpl.getFlybuysDiscountAllowed()).isNull();
    }

    @Test
    public void testGetFlybuysDiscountAllowed() {
        final FlybuysDiscountDenialDto flybuysDiscountDenialDto = mock(FlybuysDiscountDenialDto.class);
        given(mockTargetVoucherService.getFlybuysDiscountAllowed(mockCart)).willReturn(flybuysDiscountDenialDto);
        assertThat(flybuysDiscountFacadeImpl.getFlybuysDiscountAllowed()).isEqualTo(flybuysDiscountDenialDto);
    }

    @Test
    @SuppressWarnings("boxing")
    public void testRemoveFlybuysDiscountFromCheckoutCartUnsuccessfully() throws CalculationException {
        given(mockTargetVoucherService.removeFlybuysDiscount(mockCart)).willReturn(Boolean.FALSE);

        final boolean result = flybuysDiscountFacadeImpl.removeFlybuysDiscountFromCheckoutCart();

        assertThat(result).isFalse();

        verify(commerceCartService, Mockito.times(0)).recalculateCart(Mockito.any(CommerceCartParameter.class));
        verify(modelService, Mockito.times(0)).refresh(mockCart);
        verify(mockTargetVoucherService).removeFlybuysDiscount(mockCart);
    }

    @Test
    @SuppressWarnings("boxing")
    public void testRemoveFlybuysDiscountFromCheckoutCartSuccessfully() throws CalculationException {
        given(mockTargetVoucherService.removeFlybuysDiscount(mockCart)).willReturn(Boolean.TRUE);

        final boolean result = flybuysDiscountFacadeImpl.removeFlybuysDiscountFromCheckoutCart();

        assertThat(result).isTrue();

        verify(commerceCartService).recalculateCart(Mockito.any(CommerceCartParameter.class));
        verify(modelService).refresh(mockCart);
        verify(mockTargetVoucherService).removeFlybuysDiscount(mockCart);
    }

    @Test
    public void testApplyFlybuysDiscountToCheckoutCartWithBlankRedeemCode() {
        final boolean result = flybuysDiscountFacadeImpl.applyFlybuysDiscountToCheckoutCart("");

        assertThat(result).isFalse();

        verifyZeroInteractions(mockTargetCheckoutFacade, mockSessionService, mockTargetVoucherService);
    }

    @Test
    @SuppressWarnings("boxing")
    public void testApplyFlybuysDiscountToCheckoutCartWhereDiscountNotRemoved() {
        final String redeemCode = "redeemCode";

        given(mockTargetVoucherService.removeFlybuysDiscount(mockCart)).willReturn(Boolean.FALSE);

        final boolean result = flybuysDiscountFacadeImpl.applyFlybuysDiscountToCheckoutCart(redeemCode);

        assertThat(result).isFalse();

        verifyZeroInteractions(mockSessionService);
    }

    @Test
    @SuppressWarnings("boxing")
    public void testApplyFlybuysDiscountToCheckoutCartWithFlybuysDiscountsNotAllowed() {
        final String redeemCode = "redeemCode";

        given(mockTargetVoucherService.removeFlybuysDiscount(mockCart)).willReturn(Boolean.TRUE);

        final FlybuysDiscountDenialDto flybuysDiscountDenialDto = new FlybuysDiscountDenialDto();
        flybuysDiscountDenialDto.setDenied(true);
        given(mockTargetVoucherService.getFlybuysDiscountAllowed(mockCart)).willReturn(flybuysDiscountDenialDto);

        final boolean result = flybuysDiscountFacadeImpl.applyFlybuysDiscountToCheckoutCart(redeemCode);

        assertThat(result).isFalse();
    }

    @Test
    @SuppressWarnings("boxing")
    public void testApplyFlybuysDiscountToCheckoutCartWithNothingInSession() {
        final String redeemCode = "redeemCode";

        given(mockTargetVoucherService.removeFlybuysDiscount(mockCart)).willReturn(Boolean.TRUE);
        given(mockSessionService.getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE)).willReturn(null);

        final boolean result = flybuysDiscountFacadeImpl.applyFlybuysDiscountToCheckoutCart(redeemCode);

        assertThat(result).isFalse();
    }

    @Test
    @SuppressWarnings("boxing")
    public void testApplyFlybuysDiscountToCheckoutCartWithNoRedeemTiersInSessionData() {
        final String redeemCode = "redeemCode";

        given(mockTargetVoucherService.removeFlybuysDiscount(mockCart)).willReturn(Boolean.TRUE);

        final FlybuysAuthenticateResponseDto flybuysAuthenticateResponseDto = new FlybuysAuthenticateResponseDto();

        given(mockSessionService.getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE)).willReturn(
                flybuysAuthenticateResponseDto);

        final boolean result = flybuysDiscountFacadeImpl.applyFlybuysDiscountToCheckoutCart(redeemCode);

        assertThat(result).isFalse();
    }

    @Test
    @SuppressWarnings("boxing")
    public void testApplyFlybuysDiscountToCheckoutCartWithNoMatchingRedeemTier() {
        final String redeemCode = "redeemCode";

        given(mockTargetVoucherService.removeFlybuysDiscount(mockCart)).willReturn(Boolean.TRUE);

        final FlybuysRedeemTierDto redeemTier1 = new FlybuysRedeemTierDto();
        redeemTier1.setDollarAmt(new BigDecimal(10.00));
        redeemTier1.setPoints(2000);
        redeemTier1.setRedeemCode("redeemCode1");

        final FlybuysRedeemTierDto redeemTier2 = new FlybuysRedeemTierDto();
        redeemTier2.setDollarAmt(new BigDecimal(20.00));
        redeemTier2.setPoints(4000);
        redeemTier2.setRedeemCode("redeemCode2");

        final FlybuysRedeemTierDto redeemTier3 = new FlybuysRedeemTierDto();
        redeemTier3.setDollarAmt(new BigDecimal(30.00));
        redeemTier3.setPoints(6000);
        redeemTier3.setRedeemCode("redeemCode3");

        final FlybuysRedeemTierDto redeemTier4 = new FlybuysRedeemTierDto();
        redeemTier3.setDollarAmt(new BigDecimal(40.00));
        redeemTier3.setPoints(8000);
        redeemTier3.setRedeemCode("redeemCode4");

        final List<FlybuysRedeemTierDto> redeemTiers = new ArrayList<>();
        redeemTiers.add(redeemTier1);
        redeemTiers.add(redeemTier2);
        redeemTiers.add(redeemTier3);
        redeemTiers.add(redeemTier4);

        final FlybuysAuthenticateResponseDto flybuysAuthenticateResponseDto = new FlybuysAuthenticateResponseDto();
        flybuysAuthenticateResponseDto.setRedeemTiers(redeemTiers);

        given(mockSessionService.getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE)).willReturn(
                flybuysAuthenticateResponseDto);

        final boolean result = flybuysDiscountFacadeImpl.applyFlybuysDiscountToCheckoutCart(redeemCode);

        assertThat(result).isFalse();
    }

    @Test
    @SuppressWarnings("boxing")
    public void testApplyFlybuysDiscountToCheckoutCartWithMatchingRedeemTierFailedToApply() {
        final String redeemCode = "redeemCode3";
        final String flybuysCardNumber = "6008943218616910";
        final String securityToken = "thisisasecuritycode";

        given(mockTargetVoucherService.removeFlybuysDiscount(mockCart)).willReturn(Boolean.TRUE);

        final FlybuysRedeemTierDto redeemTier1 = new FlybuysRedeemTierDto();
        redeemTier1.setDollarAmt(new BigDecimal(10.00));
        redeemTier1.setPoints(2000);
        redeemTier1.setRedeemCode("redeemCode1");

        final FlybuysRedeemTierDto redeemTier2 = new FlybuysRedeemTierDto();
        redeemTier2.setDollarAmt(new BigDecimal(20.00));
        redeemTier2.setPoints(4000);
        redeemTier2.setRedeemCode("redeemCode2");

        final FlybuysRedeemTierDto redeemTier3 = new FlybuysRedeemTierDto();
        redeemTier3.setDollarAmt(new BigDecimal(30.00));
        redeemTier3.setPoints(6000);
        redeemTier3.setRedeemCode("redeemCode3");

        final List<FlybuysRedeemTierDto> redeemTiers = new ArrayList<>();
        redeemTiers.add(redeemTier1);
        redeemTiers.add(redeemTier2);
        redeemTiers.add(redeemTier3);

        final FlybuysAuthenticateResponseDto flybuysAuthenticateResponseDto = new FlybuysAuthenticateResponseDto();
        flybuysAuthenticateResponseDto.setSecurityToken(securityToken);
        flybuysAuthenticateResponseDto.setRedeemTiers(redeemTiers);

        given(mockSessionService.getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE)).willReturn(
                flybuysAuthenticateResponseDto);

        given(mockCart.getFlyBuysCode()).willReturn(flybuysCardNumber);

        given(
                mockTargetVoucherService.applyFlybuysDiscount(eq(mockCart), eq(redeemCode), any(BigDecimal.class),
                        any(Integer.class), eq(securityToken))).willReturn(Boolean.FALSE);

        final boolean result = flybuysDiscountFacadeImpl.applyFlybuysDiscountToCheckoutCart(redeemCode);

        assertThat(result).isFalse();

        final ArgumentCaptor<BigDecimal> valueCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        final ArgumentCaptor<Integer> pointsConsumedCaptor = ArgumentCaptor.forClass(Integer.class);
        verify(mockTargetVoucherService).applyFlybuysDiscount(eq(mockCart), eq(redeemCode), valueCaptor.capture(),
                pointsConsumedCaptor.capture(), eq(securityToken));

        final BigDecimal value = valueCaptor.getValue();
        assertThat(value).isEqualByComparingTo(BigDecimal.valueOf(30.00));

        final Integer pointsConsumed = pointsConsumedCaptor.getValue();
        assertThat(pointsConsumed).isEqualTo(6000);
    }

    @Test
    @SuppressWarnings("boxing")
    public void testApplyFlybuysDiscountToCheckoutCartWithMatchingRedeemTierAppliedSuccessfully()
            throws CalculationException {
        final String redeemCode = "redeemCode3";
        final String flybuysCardNumber = "6008943218616910";
        final String securityToken = "thisisasecuritycode";

        given(mockTargetVoucherService.removeFlybuysDiscount(mockCart)).willReturn(Boolean.TRUE);

        final FlybuysRedeemTierDto redeemTier1 = new FlybuysRedeemTierDto();
        redeemTier1.setDollarAmt(new BigDecimal(10.00));
        redeemTier1.setPoints(2000);
        redeemTier1.setRedeemCode("redeemCode1");

        final FlybuysRedeemTierDto redeemTier2 = new FlybuysRedeemTierDto();
        redeemTier2.setDollarAmt(new BigDecimal(20.00));
        redeemTier2.setPoints(4000);
        redeemTier2.setRedeemCode("redeemCode2");

        final FlybuysRedeemTierDto redeemTier3 = new FlybuysRedeemTierDto();
        redeemTier3.setDollarAmt(new BigDecimal(30.00));
        redeemTier3.setPoints(6000);
        redeemTier3.setRedeemCode("redeemCode3");

        final List<FlybuysRedeemTierDto> redeemTiers = new ArrayList<>();
        redeemTiers.add(redeemTier1);
        redeemTiers.add(redeemTier2);
        redeemTiers.add(redeemTier3);

        final FlybuysAuthenticateResponseDto flybuysAuthenticateResponseDto = new FlybuysAuthenticateResponseDto();
        flybuysAuthenticateResponseDto.setSecurityToken(securityToken);
        flybuysAuthenticateResponseDto.setRedeemTiers(redeemTiers);

        given(mockSessionService.getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE)).willReturn(
                flybuysAuthenticateResponseDto);

        given(mockCart.getFlyBuysCode()).willReturn(flybuysCardNumber);

        given(
                mockTargetVoucherService.applyFlybuysDiscount(eq(mockCart), eq(redeemCode), any(BigDecimal.class),
                        any(Integer.class), eq(securityToken))).willReturn(Boolean.TRUE);


        final boolean result = flybuysDiscountFacadeImpl.applyFlybuysDiscountToCheckoutCart(redeemCode);

        assertThat(result).isTrue();

        final ArgumentCaptor<BigDecimal> valueCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        final ArgumentCaptor<Integer> pointsConsumedCaptor = ArgumentCaptor.forClass(Integer.class);
        verify(mockTargetVoucherService).applyFlybuysDiscount(eq(mockCart), eq(redeemCode), valueCaptor.capture(),
                pointsConsumedCaptor.capture(), eq(securityToken));

        verify(modelService).refresh(mockCart);
        verify(commerceCartService).recalculateCart(Mockito.any(CommerceCartParameter.class));

        final InOrder inOrder = inOrder(modelService, commerceCartService);
        inOrder.verify(commerceCartService).recalculateCart(Mockito.any(CommerceCartParameter.class));
        inOrder.verify(modelService).refresh(mockCart);

        final BigDecimal value = valueCaptor.getValue();
        assertThat(value).isEqualByComparingTo(BigDecimal.valueOf(30.00));

        final Integer pointsConsumed = pointsConsumedCaptor.getValue();
        assertThat(pointsConsumed).isEqualTo(6000);
    }

    @Test
    public void testGetAppliedFlybuysRedeemTierForCheckoutCartWithNothingInSession() {
        final FlybuysRedeemTierData result = flybuysDiscountFacadeImpl.getAppliedFlybuysRedeemTierForCheckoutCart();

        assertThat(result).isNull();

        verify(mockSessionService).getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE);
        verifyNoMoreInteractions(mockSessionService);

        verifyZeroInteractions(mockTargetCheckoutFacade);
    }

    @Test
    public void testGetAppliedFlybuysRedeemTierForCheckoutCartWithNoRedeemTiersInSession() {
        final FlybuysAuthenticateResponseDto flybuysAuthenticationResponseDto = new FlybuysAuthenticateResponseDto();
        flybuysAuthenticationResponseDto.setRedeemTiers(new ArrayList<FlybuysRedeemTierDto>());

        given(mockSessionService.getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE)).willReturn(
                flybuysAuthenticationResponseDto);

        final FlybuysRedeemTierData result = flybuysDiscountFacadeImpl.getAppliedFlybuysRedeemTierForCheckoutCart();

        assertThat(result).isNull();

        verify(mockSessionService).getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE);
        verifyNoMoreInteractions(mockSessionService);

        verifyZeroInteractions(mockTargetCheckoutFacade);
    }

    @Test
    public void testGetAppliedFlybuysRedeemTierForCheckoutCartWithNoDiscountOnCart() {
        final FlybuysRedeemTierDto redeemTier = new FlybuysRedeemTierDto();

        final List<FlybuysRedeemTierDto> redeemTiers = new ArrayList<>();
        redeemTiers.add(redeemTier);

        final FlybuysAuthenticateResponseDto flybuysAuthenticationResponseDto = new FlybuysAuthenticateResponseDto();
        flybuysAuthenticationResponseDto.setRedeemTiers(redeemTiers);

        given(mockSessionService.getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE)).willReturn(
                flybuysAuthenticationResponseDto);

        final FlybuysRedeemTierData result = flybuysDiscountFacadeImpl.getAppliedFlybuysRedeemTierForCheckoutCart();

        assertThat(result).isNull();

        verify(mockSessionService).getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE);
        verify(mockTargetCheckoutFacade).getCart();
    }

    @Test
    public void testGetAppliedFlybuysRedeemTierForCheckoutCartWhereCartDiscountNotMatchSessionTiers() {
        final FlybuysRedeemTierDto redeemTier = new FlybuysRedeemTierDto();
        redeemTier.setRedeemCode("redeemcode");

        final List<FlybuysRedeemTierDto> redeemTiers = new ArrayList<>();
        redeemTiers.add(redeemTier);

        final FlybuysAuthenticateResponseDto flybuysAuthenticationResponseDto = new FlybuysAuthenticateResponseDto();
        flybuysAuthenticationResponseDto.setRedeemTiers(redeemTiers);

        given(mockSessionService.getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE)).willReturn(
                flybuysAuthenticationResponseDto);

        final FlybuysDiscountModel mockFlybuysDiscount = mock(FlybuysDiscountModel.class);
        given(mockFlybuysDiscount.getRedeemCode()).willReturn("adifferentredeemcode");
        given(mockTargetVoucherService.getFlybuysDiscountForOrder(mockCart)).willReturn(mockFlybuysDiscount);

        final FlybuysRedeemTierData result = flybuysDiscountFacadeImpl.getAppliedFlybuysRedeemTierForCheckoutCart();

        assertThat(result).isNull();

        verify(mockSessionService).getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE);
        verify(mockTargetCheckoutFacade).getCart();
    }

    @Test
    public void testGetAppliedFlybuysRedeemTierForCheckoutCartWithDiscountOnCart() {
        final FlybuysRedeemTierDto redeemTier = new FlybuysRedeemTierDto();
        redeemTier.setRedeemCode("redeemcode");
        redeemTier.setDollarAmt(BigDecimal.valueOf(10.00));
        redeemTier.setPoints(2000);

        final List<FlybuysRedeemTierDto> redeemTiers = new ArrayList<>();
        redeemTiers.add(redeemTier);

        final FlybuysAuthenticateResponseDto flybuysAuthenticationResponseDto = new FlybuysAuthenticateResponseDto();
        flybuysAuthenticationResponseDto.setRedeemTiers(redeemTiers);

        given(mockSessionService.getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE)).willReturn(
                flybuysAuthenticationResponseDto);

        final FlybuysDiscountModel mockFlybuysDiscount = mock(FlybuysDiscountModel.class);
        given(mockFlybuysDiscount.getRedeemCode()).willReturn("redeemcode");
        given(mockFlybuysDiscount.getValue()).willReturn(Double.valueOf(10.00));
        given(mockTargetVoucherService.getFlybuysDiscountForOrder(mockCart)).willReturn(mockFlybuysDiscount);

        final PriceData priceData = new PriceData();
        given(mockPriceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(10.00), "AUD")).willReturn(priceData);

        final FlybuysRedeemTierData result = flybuysDiscountFacadeImpl.getAppliedFlybuysRedeemTierForCheckoutCart();

        assertThat(result).isNotNull();

        assertThat(result.getRedeemCode()).isEqualTo(redeemTier.getRedeemCode());
        assertThat(result.getDollars()).isEqualTo(priceData);
        assertThat(result.getPoints()).isEqualTo(redeemTier.getPoints());

        verify(mockSessionService).getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE);
        verify(mockTargetCheckoutFacade).getCart();
    }

    @Test
    public void testReassessFlybuysDiscountOnCheckoutCartWithNoCart() {
        given(mockTargetCheckoutFacade.getCart()).willReturn(null);

        final boolean result = flybuysDiscountFacadeImpl.reassessFlybuysDiscountOnCheckoutCart();

        assertThat(result).isFalse();
    }

    @Test
    public void testReassessFlybuysDiscountOnCheckoutCartWithEmptyCart() {
        given(mockTargetCheckoutFacade.getCart()).willReturn(mockCart);
        given(mockCart.getEntries()).willReturn(null);

        final FlybuysDiscountModel mockFlybuysDiscount = mock(FlybuysDiscountModel.class);
        final Collection<DiscountModel> appliedVouchers = new ArrayList<>();
        appliedVouchers.add(mockFlybuysDiscount);
        given(mockTargetVoucherService.getAppliedVouchers(mockCart)).willReturn(appliedVouchers);

        final FlybuysDiscountFacadeImpl flybuysDiscountFacadeImplSpy = spy(flybuysDiscountFacadeImpl);
        doReturn(null).when(flybuysDiscountFacadeImplSpy).getFlybuysRedemptionDataForCart(mockCart);

        doReturn(Boolean.TRUE).when(flybuysDiscountFacadeImplSpy).removeFlybuysDiscountFromCheckoutCart();

        final boolean result = flybuysDiscountFacadeImplSpy.reassessFlybuysDiscountOnCheckoutCart();

        assertThat(result).isTrue();

        verify(mockTargetCheckoutFacade).getCart();
        verify(mockTargetVoucherService).getAppliedVouchers(mockCart);
        verify(flybuysDiscountFacadeImplSpy).getFlybuysRedemptionDataForCart(mockCart);
    }

    @Test
    public void testReassessFlybuysDiscountOnCheckoutCartWithNoFlybuysDiscounts() {
        given(mockTargetCheckoutFacade.getCart()).willReturn(mockCart);
        given(mockCart.getEntries()).willReturn(getEntries());

        final Collection<DiscountModel> appliedVouchers = new ArrayList<>();
        given(mockTargetVoucherService.getAppliedVouchers(mockCart)).willReturn(appliedVouchers);

        final boolean result = flybuysDiscountFacadeImpl.reassessFlybuysDiscountOnCheckoutCart();

        assertThat(result).isFalse();

        verify(mockTargetCheckoutFacade).getCart();
        verify(mockTargetVoucherService).getAppliedVouchers(mockCart);
    }

    @Test
    public void testReassessFlybuysDiscountOnCheckoutCartWithNonFlybuysDiscount() {
        given(mockTargetCheckoutFacade.getCart()).willReturn(mockCart);
        given(mockCart.getEntries()).willReturn(getEntries());

        final PromotionVoucherModel mockPromotionVoucher = mock(PromotionVoucherModel.class);
        final Collection<DiscountModel> appliedVouchers = new ArrayList<>();
        appliedVouchers.add(mockPromotionVoucher);
        given(mockTargetVoucherService.getAppliedVouchers(mockCart)).willReturn(appliedVouchers);

        final boolean result = flybuysDiscountFacadeImpl.reassessFlybuysDiscountOnCheckoutCart();

        assertThat(result).isFalse();

        verify(mockTargetCheckoutFacade).getCart();
        verify(mockTargetVoucherService).getAppliedVouchers(mockCart);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testReassessFlybuysDiscountOnCheckoutCartWithNoRedemptionDataRemoveSuccess() {
        given(mockTargetCheckoutFacade.getCart()).willReturn(mockCart);
        given(mockCart.getEntries()).willReturn(getEntries());

        final FlybuysDiscountModel mockFlybuysDiscount = mock(FlybuysDiscountModel.class);
        final Collection<DiscountModel> appliedVouchers = new ArrayList<>();
        appliedVouchers.add(mockFlybuysDiscount);
        given(mockTargetVoucherService.getAppliedVouchers(mockCart)).willReturn(appliedVouchers);

        final FlybuysDiscountFacadeImpl flybuysDiscountFacadeImplSpy = spy(flybuysDiscountFacadeImpl);
        doReturn(null).when(flybuysDiscountFacadeImplSpy).getFlybuysRedemptionDataForCart(mockCart);

        doReturn(true).when(flybuysDiscountFacadeImplSpy).removeFlybuysDiscountFromCheckoutCart();

        final boolean result = flybuysDiscountFacadeImplSpy.reassessFlybuysDiscountOnCheckoutCart();

        assertThat(result).isTrue();

        verify(mockTargetCheckoutFacade).getCart();
        verify(mockTargetVoucherService).getAppliedVouchers(mockCart);
        verify(flybuysDiscountFacadeImplSpy).getFlybuysRedemptionDataForCart(mockCart);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testReassessFlybuysDiscountOnCheckoutCartFBNotAllowedWithRedemptionData() {
        given(mockTargetCheckoutFacade.getCart()).willReturn(mockCart);
        given(mockCart.getEntries()).willReturn(getEntries());

        final FlybuysDiscountModel mockFlybuysDiscount = mock(FlybuysDiscountModel.class);
        final Collection<DiscountModel> appliedVouchers = new ArrayList<>();
        appliedVouchers.add(mockFlybuysDiscount);
        given(mockTargetVoucherService.getAppliedVouchers(mockCart)).willReturn(appliedVouchers);

        final FlybuysDiscountDenialDto discountDenialDto = mock(FlybuysDiscountDenialDto.class);
        given(mockTargetVoucherService.getFlybuysDiscountAllowed(mockCart)).willReturn(discountDenialDto);

        final FlybuysDiscountFacadeImpl flybuysDiscountFacadeImplSpy = spy(flybuysDiscountFacadeImpl);
        doReturn(null).when(flybuysDiscountFacadeImplSpy).getFlybuysRedemptionDataForCart(mockCart);

        doReturn(true).when(flybuysDiscountFacadeImplSpy).removeFlybuysDiscountFromCheckoutCart();

        final boolean result = flybuysDiscountFacadeImplSpy.reassessFlybuysDiscountOnCheckoutCart();

        assertThat(result).isTrue();

        verify(mockTargetCheckoutFacade).getCart();
        verify(mockTargetVoucherService).getAppliedVouchers(mockCart);
        verify(flybuysDiscountFacadeImplSpy).getFlybuysRedemptionDataForCart(mockCart);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testReassessFlybuysDiscountOnCheckoutCartWithNoRedeemTiers() {
        given(mockTargetCheckoutFacade.getCart()).willReturn(mockCart);
        given(mockCart.getEntries()).willReturn(getEntries());

        final FlybuysDiscountModel mockFlybuysDiscount = mock(FlybuysDiscountModel.class);
        final Collection<DiscountModel> appliedVouchers = new ArrayList<>();
        appliedVouchers.add(mockFlybuysDiscount);
        given(mockTargetVoucherService.getAppliedVouchers(mockCart)).willReturn(appliedVouchers);

        final FlybuysDiscountFacadeImpl flybuysDiscountFacadeImplSpy = spy(flybuysDiscountFacadeImpl);
        final FlybuysRedemptionData flybuysRedemptionData = new FlybuysRedemptionData();
        doReturn(flybuysRedemptionData).when(flybuysDiscountFacadeImplSpy).getFlybuysRedemptionDataForCart(mockCart);

        doReturn(true).when(flybuysDiscountFacadeImplSpy).removeFlybuysDiscountFromCheckoutCart();

        final boolean result = flybuysDiscountFacadeImplSpy.reassessFlybuysDiscountOnCheckoutCart();

        assertThat(result).isTrue();

        verify(mockTargetCheckoutFacade).getCart();
        verify(mockTargetVoucherService).getAppliedVouchers(mockCart);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testReassessFlybuysDiscountOnCheckoutCartWithAppliedDiscountStillValid() {
        final String redeemCode = "redeemCode";

        given(mockTargetCheckoutFacade.getCart()).willReturn(mockCart);
        given(mockCart.getEntries()).willReturn(getEntries());

        final FlybuysDiscountModel mockFlybuysDiscount = mock(FlybuysDiscountModel.class);
        given(mockFlybuysDiscount.getRedeemCode()).willReturn(redeemCode);

        final Collection<DiscountModel> appliedVouchers = new ArrayList<>();
        appliedVouchers.add(mockFlybuysDiscount);
        given(mockTargetVoucherService.getAppliedVouchers(mockCart)).willReturn(appliedVouchers);

        final FlybuysDiscountFacadeImpl flybuysDiscountFacadeImplSpy = spy(flybuysDiscountFacadeImpl);

        final FlybuysRedeemTierData redeemTier = new FlybuysRedeemTierData();
        redeemTier.setRedeemCode(redeemCode);

        final List<FlybuysRedeemTierData> redeemTiers = new ArrayList<>();
        redeemTiers.add(redeemTier);

        final FlybuysRedemptionData flybuysRedemptionData = new FlybuysRedemptionData();
        flybuysRedemptionData.setRedeemTiers(redeemTiers);

        doReturn(flybuysRedemptionData).when(flybuysDiscountFacadeImplSpy).getFlybuysRedemptionDataForCart(mockCart);
        doReturn(false).when(flybuysDiscountFacadeImplSpy).removeFlybuysDiscountFromCheckoutCart();

        final boolean result = flybuysDiscountFacadeImplSpy.reassessFlybuysDiscountOnCheckoutCart();

        assertThat(result).isFalse();

        verify(mockTargetCheckoutFacade, times(2)).getCart();
        verify(mockTargetVoucherService).getAppliedVouchers(mockCart);
        verify(flybuysDiscountFacadeImplSpy, never()).removeFlybuysDiscountFromCheckoutCart();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testReassessFlybuysDiscountOnCheckoutCartWithRemoveSuccess() {
        given(mockTargetCheckoutFacade.getCart()).willReturn(mockCart);
        given(mockCart.getEntries()).willReturn(getEntries());

        final FlybuysDiscountModel mockFlybuysDiscount = mock(FlybuysDiscountModel.class);
        given(mockFlybuysDiscount.getRedeemCode()).willReturn("appliedredeemcode");

        final Collection<DiscountModel> appliedVouchers = new ArrayList<>();
        appliedVouchers.add(mockFlybuysDiscount);
        given(mockTargetVoucherService.getAppliedVouchers(mockCart)).willReturn(appliedVouchers);

        final FlybuysDiscountFacadeImpl flybuysDiscountFacadeImplSpy = spy(flybuysDiscountFacadeImpl);

        final FlybuysRedeemTierData redeemTier = new FlybuysRedeemTierData();
        redeemTier.setRedeemCode("validredeemcode");

        final List<FlybuysRedeemTierData> redeemTiers = new ArrayList<>();
        redeemTiers.add(redeemTier);

        final FlybuysRedemptionData flybuysRedemptionData = new FlybuysRedemptionData();
        flybuysRedemptionData.setRedeemTiers(redeemTiers);

        doReturn(flybuysRedemptionData).when(flybuysDiscountFacadeImplSpy).getFlybuysRedemptionDataForCart(mockCart);

        doReturn(true).when(flybuysDiscountFacadeImplSpy).removeFlybuysDiscountFromCheckoutCart();

        final boolean result = flybuysDiscountFacadeImplSpy.reassessFlybuysDiscountOnCheckoutCart();

        assertThat(result).isTrue();

        verify(mockTargetCheckoutFacade, times(2)).getCart();
        verify(mockTargetVoucherService).getAppliedVouchers(mockCart);
        verify(flybuysDiscountFacadeImplSpy).removeFlybuysDiscountFromCheckoutCart();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testReassessFlybuysDiscountOnCheckoutCartWithRemoveFailure() {
        given(mockTargetCheckoutFacade.getCart()).willReturn(mockCart);
        given(mockCart.getEntries()).willReturn(getEntries());

        final FlybuysDiscountModel mockFlybuysDiscount = mock(FlybuysDiscountModel.class);
        given(mockFlybuysDiscount.getRedeemCode()).willReturn("appliedredeemcode");

        final Collection<DiscountModel> appliedVouchers = new ArrayList<>();
        appliedVouchers.add(mockFlybuysDiscount);
        given(mockTargetVoucherService.getAppliedVouchers(mockCart)).willReturn(appliedVouchers);

        final FlybuysDiscountFacadeImpl flybuysDiscountFacadeImplSpy = spy(flybuysDiscountFacadeImpl);

        final FlybuysRedeemTierData redeemTier = new FlybuysRedeemTierData();
        redeemTier.setRedeemCode("validredeemcode");

        final List<FlybuysRedeemTierData> redeemTiers = new ArrayList<>();
        redeemTiers.add(redeemTier);

        final FlybuysRedemptionData flybuysRedemptionData = new FlybuysRedemptionData();
        flybuysRedemptionData.setRedeemTiers(redeemTiers);

        doReturn(flybuysRedemptionData).when(flybuysDiscountFacadeImplSpy).getFlybuysRedemptionDataForCart(mockCart);

        doReturn(false).when(flybuysDiscountFacadeImplSpy).removeFlybuysDiscountFromCheckoutCart();

        final boolean result = flybuysDiscountFacadeImplSpy.reassessFlybuysDiscountOnCheckoutCart();

        assertThat(result).isFalse();

        verify(mockTargetCheckoutFacade, times(2)).getCart();
        verify(mockTargetVoucherService).getAppliedVouchers(mockCart);
        verify(flybuysDiscountFacadeImplSpy).removeFlybuysDiscountFromCheckoutCart();
    }

    private List<AbstractOrderEntryModel> getEntries() {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        entries.add(abstractOrderEntry);
        return entries;
    }

    @Test
    public void testCheckFlybuysDiscountUsedWithNoFlybuysDiscount() {
        given(mockTargetCheckoutFacade.getCart()).willReturn(mockCart);
        given(mockTargetVoucherService.getFlybuysDiscountForOrder(mockCart)).willReturn(null);
        final boolean result = flybuysDiscountFacadeImpl.checkFlybuysDiscountUsed();
        assertThat(result).isFalse();
    }

    @Test
    public void testCheckFlybuysDiscountUsedWithFlybuysDiscount() {
        given(mockTargetCheckoutFacade.getCart()).willReturn(mockCart);
        final FlybuysDiscountModel flybuysDiscountModel = new FlybuysDiscountModel();
        given(mockTargetVoucherService.getFlybuysDiscountForOrder(mockCart)).willReturn(flybuysDiscountModel);
        final boolean result = flybuysDiscountFacadeImpl.checkFlybuysDiscountUsed();
        assertThat(result).isTrue();
    }

    /**
     * Cart contains only gift card items. In this scenario, user can not redeem points for gift card items. No redeem
     * tiers will get populated in this case.
     */
    @Test
    public void testGetFlybuysRedemptionDataForCheckoutCartWithDigitalAndPhysicalGiftCard() {
        setUpGiftCardMixedBasket();
        willReturn("digital").given(mockProductType1).getCode();
        willReturn("PGC1003_skydive").given(mockVariantProduct1).getCode();
        willReturn("giftCard").given(mockProductType2).getCode();
        willReturn("PGC1004_ribbon").given(mockVariantProduct2).getCode();
        given(mockOrderEntry1.getTotalPrice()).willReturn(Double.valueOf(30.0));
        given(mockOrderEntry2.getTotalPrice()).willReturn(Double.valueOf(70.0));
        willReturn(Arrays.asList(mockOrderEntry1, mockOrderEntry2))
                .given(mockCart).getEntries();
        given(mockFlybuysAuthenticateResponseDto.getRedeemTiers()).willReturn(
                Collections.singletonList(mock10DollarFlybuysRedeemTierDto));
        given(mockCart.getSubtotal()).willReturn(Double.valueOf(100.00));
        final FlybuysRedemptionData flybuysRedemptionData = flybuysDiscountFacadeImpl
                .getFlybuysRedemptionDataForCheckoutCart();
        assertThat(flybuysRedemptionData).isNotNull();
        assertThat(flybuysRedemptionData.getAvailablePoints()).isEqualTo(2500);
        assertThat(flybuysRedemptionData.getRedeemTiers()).hasSize(0);
    }

    /**
     * Cart contains only one gift card. In this scenario, user can not redeem points for gift card items. No redeem
     * tiers will get populated in this case.
     */
    @Test
    public void testGetFlybuysRedemptionDataForCheckoutCartWithOnlyGiftCard() {
        setUpGiftCardMixedBasket();
        willReturn("giftCard").given(mockProductType1).getCode();
        willReturn("PGC1004_ribbon").given(mockVariantProduct1).getCode();
        given(mockOrderEntry1.getTotalPrice()).willReturn(Double.valueOf(70.0));
        willReturn(Arrays.asList(mockOrderEntry1))
                .given(mockCart).getEntries();
        given(mockFlybuysAuthenticateResponseDto.getRedeemTiers()).willReturn(
                Collections.singletonList(mock10DollarFlybuysRedeemTierDto));
        given(mockCart.getSubtotal()).willReturn(Double.valueOf(70.00));
        final FlybuysRedemptionData flybuysRedemptionData = flybuysDiscountFacadeImpl
                .getFlybuysRedemptionDataForCheckoutCart();
        assertThat(flybuysRedemptionData).isNotNull();
        assertThat(flybuysRedemptionData.getAvailablePoints()).isEqualTo(2500);
        assertThat(flybuysRedemptionData.getRedeemTiers()).hasSize(0);
    }

    /**
     * Cart contains only one non gift card. In this scenario, user can redeem points. Redeem tiers information will get
     * populated in this case.
     */
    @Test
    public void testGetFlybuysRedemptionDataForCartWithOnlyNonGiftCardItem() {
        setUpGiftCardMixedBasket();
        willReturn("normal").given(mockProductType1).getCode();
        willReturn("P1001").given(mockVariantProduct1).getCode();
        given(mockOrderEntry1.getTotalPrice()).willReturn(Double.valueOf(70.0));
        willReturn(Arrays.asList(mockOrderEntry1))
                .given(mockCart).getEntries();
        given(mockFlybuysAuthenticateResponseDto.getRedeemTiers()).willReturn(
                Collections.singletonList(mock10DollarFlybuysRedeemTierDto));
        given(mockCart.getSubtotal()).willReturn(Double.valueOf(70.00));
        given(mockPriceData1.getValue()).willReturn(BigDecimal.valueOf(10.00));
        final ArgumentCaptor<BigDecimal> priceDataValueCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        given(mockPriceDataFactory.create(eq(PriceDataType.BUY), priceDataValueCaptor.capture(),
                eq(AUD_CURRENCY_ISOCODE))).willReturn(mockPriceData1);
        final FlybuysRedemptionData flybuysRedemptionData = flybuysDiscountFacadeImpl
                .getFlybuysRedemptionDataForCheckoutCart();
        assertThat(flybuysRedemptionData).isNotNull();
        assertThat(flybuysRedemptionData.getAvailablePoints()).isEqualTo(2500);
        assertThat(flybuysRedemptionData.getRedeemTiers()).hasSize(1);
        final FlybuysRedeemTierData flybuys10DollarRedeemTier = flybuysRedemptionData.getRedeemTiers().get(0);
        assertThat(flybuys10DollarRedeemTier.getDollars()).isEqualTo(mockPriceData1);
        assertThat(flybuys10DollarRedeemTier.getPoints()).isEqualTo(Integer.valueOf(2000));
        assertThat(flybuys10DollarRedeemTier.getRedeemCode()).isEqualTo("tendollarydoos");
    }

    /**
     * Cart contains normal and gift card. In this scenario fluybuys respond with 2 redeem tiers information. But based
     * on remaining total amount after by deducting the gift card amount from the cart total, customer can able to see
     * few redeem tier(s) information(here only 1).
     *
     */
    @Test
    public void testGetFlybuysRedemptionDataForCartWithNormalAndDigitalItems() {
        setUpGiftCardMixedBasket();
        willReturn("normal").given(mockProductType1).getCode();
        willReturn("P1001").given(mockVariantProduct1).getCode();
        willReturn("giftCard").given(mockProductType2).getCode();
        willReturn("PGC1004_ribbon").given(mockVariantProduct2).getCode();
        given(mockOrderEntry1.getTotalPrice()).willReturn(Double.valueOf(15.0));
        given(mockOrderEntry2.getTotalPrice()).willReturn(Double.valueOf(80.0));
        willReturn(Arrays.asList(mockOrderEntry1, mockOrderEntry2))
                .given(mockCart).getEntries();
        given(mockCart.getSubtotal()).willReturn(Double.valueOf(95.00));
        given(mock20DollarFlybuysRedeemTierDto.getDollarAmt()).willReturn(BigDecimal.valueOf(20.00));
        given(mock20DollarFlybuysRedeemTierDto.getPoints()).willReturn(Integer.valueOf(4000));
        given(mock20DollarFlybuysRedeemTierDto.getRedeemCode()).willReturn("twentydollarydoos");
        given(mockFlybuysAuthenticateResponseDto.getRedeemTiers()).willReturn(
                Arrays.asList(mock10DollarFlybuysRedeemTierDto, mock20DollarFlybuysRedeemTierDto));
        given(mockPriceData1.getValue()).willReturn(BigDecimal.valueOf(10.00));
        final ArgumentCaptor<BigDecimal> priceDataValueCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        given(mockPriceDataFactory.create(eq(PriceDataType.BUY), priceDataValueCaptor.capture(),
                eq(AUD_CURRENCY_ISOCODE))).willReturn(mockPriceData1);
        final FlybuysRedemptionData flybuysRedemptionData = flybuysDiscountFacadeImpl
                .getFlybuysRedemptionDataForCheckoutCart();
        assertThat(flybuysRedemptionData).isNotNull();
        assertThat(flybuysRedemptionData.getAvailablePoints()).isEqualTo(2500);
        assertThat(flybuysRedemptionData.getRedeemTiers()).hasSize(1);
        final FlybuysRedeemTierData flybuys10DollarRedeemTier = flybuysRedemptionData.getRedeemTiers().get(0);
        assertThat(flybuys10DollarRedeemTier.getDollars()).isEqualTo(mockPriceData1);
        assertThat(flybuys10DollarRedeemTier.getPoints()).isEqualTo(Integer.valueOf(2000));
        assertThat(flybuys10DollarRedeemTier.getRedeemCode()).isEqualTo("tendollarydoos");
    }

    private void setUpGiftCardMixedBasket() {
        willReturn(mockProductType1).given(mockProduct1).getProductType();
        willReturn(mockProduct1).given(mockVariantProduct1).getBaseProduct();
        willReturn(mockVariantProduct1).given(mockOrderEntry1).getProduct();
        willReturn(Long.valueOf(2L)).given(mockOrderEntry1).getQuantity();
        willReturn(mockProduct2).given(mockVariantProduct2)
                .getBaseProduct();
        willReturn(mockProductType2).given(mockProduct2).getProductType();
        willReturn(mockVariantProduct2).given(mockOrderEntry2).getProduct();
        willReturn(Long.valueOf(10L)).given(mockOrderEntry2).getQuantity();
        given(mock10DollarFlybuysRedeemTierDto.getDollarAmt()).willReturn(BigDecimal.valueOf(10.00));
        given(mock10DollarFlybuysRedeemTierDto.getPoints()).willReturn(Integer.valueOf(2000));
        given(mock10DollarFlybuysRedeemTierDto.getRedeemCode()).willReturn("tendollarydoos");
        given(mockFlybuysAuthenticateResponseDto.getAvailPoints()).willReturn(Integer.valueOf(2500));
        given(mockSessionService.getAttribute(TgtFacadesConstants.FLYBUYS_AUTHENTICATION_RESPONSE)).willReturn(
                mockFlybuysAuthenticateResponseDto);
    }
}

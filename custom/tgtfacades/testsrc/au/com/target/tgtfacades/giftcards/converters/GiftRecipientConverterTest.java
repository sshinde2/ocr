package au.com.target.tgtfacades.giftcards.converters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.GiftRecipientModel;
import au.com.target.tgtfacades.order.data.GiftRecipientData;


/**
 * Tests for GiftRecipientModelToRecipientDataConverter
 * 
 * @author smishra1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GiftRecipientConverterTest {

    /**
     * 
     */
    @Mock
    private GiftRecipientModel giftRecipientModel;

    @InjectMocks
    private final GiftRecipientConverter converter = new GiftRecipientConverter();

    @Before
    public void setup() {
        BDDMockito.given(giftRecipientModel.getFirstName()).willReturn("Shreeram");
        BDDMockito.given(giftRecipientModel.getLastName()).willReturn("Mishra");
        BDDMockito.given(giftRecipientModel.getMessageText()).willReturn(
                "TestGiftCard - Message @@ Target $$ From OBLIX");
        BDDMockito.given(giftRecipientModel.getEmail()).willReturn("shreeram.mishra@target.com.au");
        BDDMockito.given(giftRecipientModel.getPk()).willReturn(PK.parse("12345678"));
    }

    @Test
    public void testDTOWithValues() {
        final GiftRecipientData recipientData = converter.convert(giftRecipientModel);
        Assertions.assertThat(recipientData).isNotNull();
        Assertions.assertThat(giftRecipientModel.getFirstName()).isNotNull().isNotEmpty()
                .isEqualTo(recipientData.getFirstName());
        Assertions.assertThat(giftRecipientModel.getLastName()).isNotNull().isNotEmpty()
                .isEqualTo(recipientData.getLastName());
        Assertions.assertThat(giftRecipientModel.getMessageText()).isNotNull().isNotEmpty()
                .isEqualTo(recipientData.getMessageText());
        Assertions.assertThat(giftRecipientModel.getEmail()).isNotNull().isNotEmpty()
                .isEqualTo(recipientData.getRecipientEmailAddress());
        Assertions.assertThat(recipientData.getId()).isNotNull().isNotEmpty();
    }
}

/**
 * 
 */
package au.com.target.tgtfacades.giftcards.validate;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.giftcards.GiftCardService;
import au.com.target.tgtcore.model.GiftCardModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtcore.product.exception.ProductNotFoundException;
import au.com.target.tgtfacades.giftcards.exceptions.GiftCardValidationException;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;


/**
 * Unit tests for MaxGiftCardQtyValidator.
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MaxGiftCardQtyValidatorTest {

    @Mock
    private TargetLaybyCartService targetLaybyCartService;

    @Mock
    private GiftCardService giftCardService;

    @Mock
    private MessageSource messageSource;

    @Mock
    private I18NService i18nService;

    @Mock
    private TargetProductService targetProductService;

    @Mock
    private TargetSharedConfigService targetSharedConfigService;

    @Mock
    private TargetProductModel productModel;

    @InjectMocks
    private final MaxGiftCardQtyValidator validator = new MaxGiftCardQtyValidator();

    @Test(expected = GiftCardValidationException.class)
    public void testValidateDigitalGiftCardQuantityAllowed()
            throws GiftCardValidationException, ProductNotFoundException {
        final String productCode = "PGC1000_iTunes_100";
        final int quantity = 10;
        final CartEntryModel nonGiftCardCartEntry = getNonGiftCardEntry(quantity);
        final ProductTypeModel productType = getProductType("digital");
        final TargetProductModel giftCardProduct = getGiftCardProduct(productType);
        final CartModel cart = getCart(nonGiftCardCartEntry, getGiftCardCartEntry(giftCardProduct));

        given(targetProductService.getProductForCode("PGC1000_iTunes_100")).willReturn(giftCardProduct);
        given(messageSource.getMessage(Mockito.anyString(), Mockito.any(Object[].class),
                Mockito.any(Locale.class)))
                        .willReturn(
                                "ERROR");
        given(giftCardService.getGiftCardForProduct(productCode)).willReturn(getGiftCard());
        given(targetLaybyCartService.getSessionCart()).willReturn(cart);
        validator.validateGiftCards(productCode, 2);
    }

    @Test(expected = GiftCardValidationException.class)
    public void testValidateDigitalGiftCardQuantityNotAllowed()
            throws GiftCardValidationException, ProductNotFoundException {
        final String productCode = "PGC1000_iTunes_100";
        final int quantity = 15;
        final CartEntryModel nonGiftCardCartEntry = getNonGiftCardEntry(quantity);
        final ProductTypeModel productType = getProductType("digital");
        final TargetProductModel giftCardProduct = getGiftCardProduct(productType);
        final CartModel cart = getCart(nonGiftCardCartEntry, getGiftCardCartEntry(giftCardProduct));
        given(targetProductService.getProductForCode("PGC1000_iTunes_100")).willReturn(giftCardProduct);
        given(messageSource.getMessage(Mockito.anyString(), Mockito.any(Object[].class),
                Mockito.any(Locale.class)))
                        .willReturn(
                                "ERROR");
        given(giftCardService.getGiftCardForProduct(productCode)).willReturn(getGiftCard());
        given(targetLaybyCartService.getSessionCart()).willReturn(cart);
        validator.validateGiftCards(productCode, 2);
    }

    @Test(expected = GiftCardValidationException.class)
    public void testValidatePhysicalGiftCardQuantityAllowed()
            throws GiftCardValidationException, ProductNotFoundException {
        final String productCode = "PGC1000_iTunes_100";
        final int quantity = 10;
        final CartEntryModel nonGiftCardCartEntry = getNonGiftCardEntry(quantity);
        final ProductTypeModel productType = getProductType("giftCard");
        final TargetProductModel giftCardProduct = getGiftCardProduct(productType);
        final CartModel cart = getCart(nonGiftCardCartEntry, getGiftCardCartEntry(giftCardProduct));

        given(targetProductService.getProductForCode("PGC1000_iTunes_100")).willReturn(giftCardProduct);
        given(messageSource.getMessage(Mockito.anyString(), Mockito.any(Object[].class),
                Mockito.any(Locale.class)))
                        .willReturn(
                                "ERROR");
        given(giftCardService.getGiftCardForProduct(productCode)).willReturn(getGiftCard());
        given(targetLaybyCartService.getSessionCart()).willReturn(cart);
        validator.validatePhysicalGiftCards(giftCardProduct, 5);
    }

    @Test(expected = GiftCardValidationException.class)
    public void testValidateUpdatePhysicalGiftCardQuantityAllowed()
            throws GiftCardValidationException, ProductNotFoundException {
        final String productCode = "PGC1000_iTunes_100";
        final int quantity = 5;
        final CartEntryModel nonGiftCardCartEntry = getNonGiftCardEntry(quantity);
        final ProductTypeModel productType = getProductType("giftCard");
        final TargetProductModel giftCardProduct = getGiftCardProduct(productType);
        final CartModel cart = getCart(nonGiftCardCartEntry, getGiftCardCartEntry(giftCardProduct));

        given(targetProductService.getProductForCode("PGC1000_iTunes_100")).willReturn(giftCardProduct);
        given(messageSource.getMessage(Mockito.anyString(), Mockito.any(Object[].class),
                Mockito.any(Locale.class)))
                        .willReturn(
                                "ERROR");
        given(giftCardService.getGiftCardForProduct(productCode)).willReturn(getGiftCard());
        given(targetLaybyCartService.getSessionCart()).willReturn(cart);
        validator.validateGiftCardsUpdateQty(productCode, 5);
    }

    /**
     * @param quantity
     * @return nonGiftCardCartEntry
     */
    private CartEntryModel getNonGiftCardEntry(final int quantity) {
        final CartEntryModel nonGiftCardCartEntry = new CartEntryModel();
        final ProductModel nonGiftCardProduct = new ProductModel();
        nonGiftCardProduct.setCode("P1000_black_L");
        nonGiftCardCartEntry.setQuantity(new Long(quantity));
        nonGiftCardCartEntry.setProduct(nonGiftCardProduct);
        return nonGiftCardCartEntry;
    }

    /**
     * @param productType
     * @return giftCardProduct
     */
    private TargetProductModel getGiftCardProduct(final ProductTypeModel productType) {

        final TargetProductModel giftCardProduct = new TargetProductModel();
        giftCardProduct.setCode("PGC1000_iTunes_100");
        giftCardProduct.setGiftCard(getGiftCard());
        giftCardProduct.setProductType(productType);
        return giftCardProduct;
    }

    /**
     * @param cardType
     * @return productType
     */
    private ProductTypeModel getProductType(final String cardType) {
        final ProductTypeModel productType = new ProductTypeModel();
        productType.setCode(cardType);
        return productType;
    }

    /**
     * @param giftCardProduct
     * @return giftCardCartEntry
     */
    private CartEntryModel getGiftCardCartEntry(final TargetProductModel giftCardProduct) {

        final CartEntryModel giftCardCartEntry = new CartEntryModel();
        giftCardCartEntry.setQuantity(new Long(9));
        giftCardCartEntry.setProduct(giftCardProduct);
        return giftCardCartEntry;
    }

    /**
     * @return giftCard
     */
    private GiftCardModel getGiftCard() {
        final GiftCardModel giftCard = new GiftCardModel();
        giftCard.setBrandId("apple1234");
        giftCard.setMaxOrderQuantity(Integer.valueOf(10));
        return giftCard;
    }

    /**
     * @param giftCardCartEntry
     * @param nonGiftCardCartEntry
     * @return cart
     */
    private CartModel getCart(final CartEntryModel nonGiftCardCartEntry, final CartEntryModel giftCardCartEntry) {
        final CartModel cart = new CartModel();
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries.add(nonGiftCardCartEntry);
        orderEntries.add(giftCardCartEntry);
        cart.setEntries(orderEntries);

        return cart;
    }
}

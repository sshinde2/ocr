/**
 * 
 */
package au.com.target.tgtfacades.giftcards.validate;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Collections;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.cart.TargetCartService;
import au.com.target.tgtcore.giftcards.GiftCardService;
import au.com.target.tgtcore.model.GiftCardModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtcore.product.exception.ProductNotFoundException;
import au.com.target.tgtfacades.giftcards.exceptions.GiftCardValidationException;


/**
 * Unit tests for MaxGiftCardValueValidator.
 * 
 * @author smishra1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MaxGiftCardValueValidatorTest {

    private final String productCode = "TestGiftCard_010";

    @InjectMocks
    private final MaxGiftCardValueValidator validator = new MaxGiftCardValueValidator();

    @Mock
    private GiftCardModel giftCardModel;

    @Mock
    private ProductModel productModel;

    @Mock
    private CartModel cartModel;

    @Mock
    private TargetSizeVariantProductModel sizeVariantProduct;

    @Mock
    private OrderEntryModel orderEntry;

    @Mock
    private TargetProductService targetProductService;

    @Mock
    private TargetCartService targetCartService;

    @Mock
    private GiftCardService giftCardService;

    @Mock
    private AbstractOrderEntryModel abstractOrderEntryModel;

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    //CHECKSTYLE:ON

    /**
     * 
     * 
     * Cart has 4 qty
     * 
     * @throws ProductNotFoundException
     * 
     */
    @Before
    public void setUpGiftCardData() throws ProductNotFoundException {
        Mockito.when(targetCartService.getSessionCart()).thenReturn(cartModel);
        Mockito.when(giftCardService.getGiftCardForProduct(productCode)).thenReturn(giftCardModel);
        Mockito.when(giftCardModel.getMaxOrderValue()).thenReturn(Integer.valueOf(50));
        Mockito.when(targetProductService.getProductForCode(productCode)).thenReturn(sizeVariantProduct);
        Mockito.when(sizeVariantProduct.getDenomination()).thenReturn(Double.valueOf(10));
        Mockito.when(giftCardService.getGiftCardForProduct(productCode)).thenReturn(giftCardModel);
        Mockito.when(giftCardModel.getBrandId()).thenReturn("TestGiftCard_010");
        Mockito.when(cartModel.getEntries()).thenReturn(Collections.singletonList(abstractOrderEntryModel));
        Mockito.when(abstractOrderEntryModel.getProduct()).thenReturn(productModel);
        Mockito.when(abstractOrderEntryModel.getQuantity()).thenReturn(Long.valueOf(4));
        Mockito.when(productModel.getCode()).thenReturn(productCode);
    }

    @Test
    public void testWhenTheCardAddedDoesNotCrossTheMaxValue()
            throws GiftCardValidationException, ProductNotFoundException {
        validator.validateGiftCards(productCode, 1);
    }

    @Test
    public void testWhenTheTotalValueExceedsTheLimit() throws GiftCardValidationException, ProductNotFoundException {
        exception.expect(GiftCardValidationException.class);
        exception.expectMessage("Gift Card beyond the total allowed value :");
        validator.validateGiftCards(productCode, 2);
    }

    @Test
    public void testWhenCartModelIsNull() throws GiftCardValidationException, ProductNotFoundException {
        Mockito.when(targetCartService.getSessionCart()).thenReturn(null);
        exception.expect(AssertionError.class);
        exception.expectMessage("CartModel cannot be null");
        validator.validateGiftCards(productCode, 1);
    }

    @Test
    public void testWhenTheProductAddedIsNotAGiftCard() throws GiftCardValidationException, ProductNotFoundException {
        Mockito.when(giftCardService.getGiftCardForProduct(productCode)).thenReturn(null);
        validator.validateGiftCards(productCode, 5);
    }

    @Test
    public void testWhenThereIsNoMaxValueSet() throws GiftCardValidationException, ProductNotFoundException {
        Mockito.when(giftCardModel.getMaxOrderValue()).thenReturn(null);
        validator.validateGiftCards(productCode, 500);
    }
}

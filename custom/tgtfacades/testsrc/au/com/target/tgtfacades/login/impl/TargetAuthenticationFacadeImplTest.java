package au.com.target.tgtfacades.login.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.user.UserService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.login.TargetAuthenticationService;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.customer.TargetCustomerFacade;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetAuthenticationFacadeImplTest {
    @Mock
    private TargetAuthenticationService mockTargetAuthenticationService;

    @Mock
    private UserService mockUserService;

    @Mock
    private TargetCustomerFacade mockTargetCustomerFacade;

    @InjectMocks
    private final TargetAuthenticationFacadeImpl targetAuthenticationFacadeImpl = new TargetAuthenticationFacadeImpl();

    @Test
    public void testIsAccountLockedAccountIsLocked() throws Exception {
        final TargetCustomerModel mockCustomer = mock(TargetCustomerModel.class);

        doReturn(Boolean.TRUE).when(mockTargetAuthenticationService).checkLoginAttempts(mockCustomer);

        assertThat(targetAuthenticationFacadeImpl.isAccountLocked(mockCustomer)).isTrue();
    }

    @Test
    public void testIsAccountLockedAccountIsNotLocked() throws Exception {
        final TargetCustomerModel mockCustomer = mock(TargetCustomerModel.class);

        doReturn(Boolean.FALSE).when(mockTargetAuthenticationService).checkLoginAttempts(mockCustomer);

        assertThat(targetAuthenticationFacadeImpl.isAccountLocked(mockCustomer)).isFalse();
    }

    @Test
    public void testIsAccountLockedWithNullUser() throws Exception {
        final TargetCustomerModel customer = null;

        assertThat(targetAuthenticationFacadeImpl.isAccountLocked(customer)).isFalse();
        verifyZeroInteractions(mockTargetAuthenticationService);
    }

    @Test
    public void testIsAccountLockedStringAccountIsLocked() throws Exception {
        final String username = "user@domain.com";

        final TargetCustomerModel mockCustomer = mock(TargetCustomerModel.class);
        given(mockTargetCustomerFacade.getCustomerForUsername(username)).willReturn(mockCustomer);

        doReturn(Boolean.TRUE).when(mockTargetAuthenticationService).checkLoginAttempts(mockCustomer);

        assertThat(targetAuthenticationFacadeImpl.isAccountLocked(username)).isTrue();
    }

    @Test
    public void testIsAccountLockedStringAccountIsNotLocked() throws Exception {
        final String username = "user@domain.com";

        final TargetCustomerModel mockCustomer = mock(TargetCustomerModel.class);
        given(mockTargetCustomerFacade.getCustomerForUsername(username)).willReturn(mockCustomer);

        doReturn(Boolean.FALSE).when(mockTargetAuthenticationService).checkLoginAttempts(mockCustomer);

        assertThat(targetAuthenticationFacadeImpl.isAccountLocked(username)).isFalse();
    }

    @Test
    public void testIsAccountLockedStringWithNotExistingUser() throws Exception {
        final String username = "user@domain.com";

        given(mockTargetCustomerFacade.getCustomerForUsername(username)).willReturn(null);

        assertThat(targetAuthenticationFacadeImpl.isAccountLocked(username)).isFalse();
        verifyZeroInteractions(mockTargetAuthenticationService);
    }

    @Test
    public void testIncrementFailedLoginAttemptCounter() throws Exception {
        final String username = "user@domain.com";

        final TargetCustomerModel mockCustomer = Mockito.mock(TargetCustomerModel.class);
        BDDMockito.given(mockTargetCustomerFacade.getCustomerForUsername(username)).willReturn(mockCustomer);

        targetAuthenticationFacadeImpl.incrementFailedLoginAttemptCounter(username);
        Mockito.verify(mockTargetAuthenticationService).checkAndIncrementLoginAttempts(mockCustomer);
    }

    @Test
    public void testIncrementFailedLoginAttemptCounterWithNotExistingUser() throws Exception {
        final String username = "user@domain.com";

        BDDMockito.given(mockTargetCustomerFacade.getCustomerForUsername(username)).willReturn(null);

        targetAuthenticationFacadeImpl.incrementFailedLoginAttemptCounter(username);
        Mockito.verifyZeroInteractions(mockTargetAuthenticationService);
    }

    @Test
    public void testIncrementFailedLoginAttemptCounterWithWrongUserType() throws Exception {
        final String username = "user@domain.com";

        BDDMockito.given(mockTargetCustomerFacade.getCustomerForUsername(username)).willReturn(null);

        targetAuthenticationFacadeImpl.incrementFailedLoginAttemptCounter(username);
        Mockito.verifyZeroInteractions(mockTargetAuthenticationService);
    }

    @Test
    public void testResetFailedLoginAttemptCounter() throws Exception {
        final String username = "user@domain.com";

        final TargetCustomerModel mockCustomer = Mockito.mock(TargetCustomerModel.class);
        BDDMockito.given(mockTargetCustomerFacade.getCustomerForUsername(username)).willReturn(mockCustomer);

        targetAuthenticationFacadeImpl.resetFailedLoginAttemptCounter(username);
        Mockito.verify(mockTargetAuthenticationService).resetLoginAttempts(mockCustomer);
    }

    @Test
    public void testResetFailedLoginAttemptCounterWithNotExistingUser() throws Exception {
        final String username = "user@domain.com";

        BDDMockito.given(mockTargetCustomerFacade.getCustomerForUsername(username)).willReturn(null);

        targetAuthenticationFacadeImpl.resetFailedLoginAttemptCounter(username);
        Mockito.verifyZeroInteractions(mockTargetAuthenticationService);
    }

    @Test
    public void testResetFailedLoginAttemptCounterWithWrongUserType() throws Exception {
        final String username = "user@domain.com";

        BDDMockito.given(mockTargetCustomerFacade.getCustomerForUsername(username)).willReturn(null);

        targetAuthenticationFacadeImpl.resetFailedLoginAttemptCounter(username);
        Mockito.verifyZeroInteractions(mockTargetAuthenticationService);
    }

}

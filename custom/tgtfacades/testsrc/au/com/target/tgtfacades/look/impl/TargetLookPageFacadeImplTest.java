/**
 * 
 */
package au.com.target.tgtfacades.look.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.shopthelook.LookService;
import au.com.target.tgtwebcore.looks.service.TargetProductGroupService;
import au.com.target.tgtwebcore.model.cms2.pages.TargetProductGroupPageModel;
import junit.framework.Assert;


/**
 * @author Nandini
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetLookPageFacadeImplTest {

    private static final int PRODUCT_LOOKS_MAX = 4;

    @InjectMocks
    private final TargetLookPageFacadeImpl targetLookPageFacadeImpl = new TargetLookPageFacadeImpl();

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Mock
    private TargetProductGroupService targetProductGroupService;

    @Mock
    private LookService lookService;

    @Mock
    private Converter<TargetProductGroupPageModel, LookDetailsData> lookConverter;

    @Mock
    private Converter<TargetLookModel, LookDetailsData> lookDataConverter;

    @Mock
    private TargetProductGroupPageModel grpPageModel1, grpPageModel2;

    @Mock
    private TargetLookModel lookModel1, lookModel2;

    @Mock
    private ProductModel productModel;

    @Mock
    private MediaModel mockMedia;

    @Mock
    private LookDetailsData lookDetailsData1, lookDetailsData2;

    @Before
    public void setUp() {
        BDDMockito.given(Boolean.valueOf(
                targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.STEP_SHOP_THE_LOOK)))
                .willReturn(Boolean.FALSE);

        targetLookPageFacadeImpl.setTargetProductGroupService(targetProductGroupService);

        BDDMockito.given(targetProductGroupService.getAllActiveLooksForProduct(productModel)).willReturn(
                new ArrayList<TargetProductGroupPageModel>(Arrays.asList(
                        grpPageModel1, grpPageModel2)));

        BDDMockito.given(lookService.getVisibleLooksForProduct(Mockito.any(ProductModel.class), Mockito.anyInt()))
                .willReturn(
                        new ArrayList<TargetLookModel>(Arrays.asList(
                                lookModel1, lookModel2)));

        BDDMockito.given(lookConverter.convert(grpPageModel1)).willReturn(
                lookDetailsData1);
        BDDMockito.given(lookConverter.convert(grpPageModel2)).willReturn(
                lookDetailsData2);

        BDDMockito.given(lookDataConverter.convert(lookModel1)).willReturn(
                lookDetailsData1);
        BDDMockito.given(lookDataConverter.convert(lookModel2)).willReturn(
                lookDetailsData2);
    }

    @Test
    public void testGetLookDetails() {
        BDDMockito.given(grpPageModel1.getGroupImage()).willReturn(mockMedia);
        BDDMockito.given(grpPageModel1.getName()).willReturn("Test name");
        BDDMockito.given(grpPageModel1.getLabel()).willReturn("Test label");
        BDDMockito.given(grpPageModel1.getFromPrice()).willReturn(Double.valueOf(100.0));
        BDDMockito.given(grpPageModel1.getGroupThumb()).willReturn(mockMedia);

        final LookDetailsData result = targetLookPageFacadeImpl.getLookDetails(grpPageModel1);

        BDDMockito.given(lookConverter.convert(grpPageModel1)).willReturn(
                lookDetailsData1);

        Assert.assertNotNull(result);
        Assert.assertSame(lookDetailsData1, result);

    }

    @Test
    public void testgetAllActiveLooksForProductNoLookPages() {
        BDDMockito.given(targetProductGroupService.getAllActiveLooksForProduct(productModel)).willReturn(null);
        final List<LookDetailsData> lookDetails = targetLookPageFacadeImpl.getActiveLooksForProduct(productModel,
                Mockito.anyInt());
        Assert.assertNotNull(lookDetails);
        Assert.assertEquals(0, lookDetails.size());
    }

    @Test
    public void testgetAllActiveLooksForProduct() {
        final List<LookDetailsData> lookDetails = targetLookPageFacadeImpl.getActiveLooksForProduct(productModel,
                PRODUCT_LOOKS_MAX);

        Assert.assertNotNull(lookDetails);
        Assert.assertTrue(CollectionUtils.isNotEmpty(lookDetails));
        Assert.assertSame(lookDetailsData1, lookDetails.get(0));
        Assert.assertSame(lookDetailsData2, lookDetails.get(1));

        Mockito.verify(targetProductGroupService, Mockito.times(1))
                .getAllActiveLooksForProduct(productModel);
        Mockito.verify(lookService, Mockito.never()).getVisibleLooksForProduct(Mockito.any(ProductModel.class),
                Mockito.anyInt());
    }

    @Test
    public void testGetStepLooksForProduct() {
        BDDMockito.given(Boolean.valueOf(
                targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.STEP_SHOP_THE_LOOK)))
                .willReturn(Boolean.TRUE);

        final List<LookDetailsData> lookDetails = targetLookPageFacadeImpl.getActiveLooksForProduct(productModel,
                PRODUCT_LOOKS_MAX);

        Assert.assertNotNull(lookDetails);
        Assert.assertEquals(2, lookDetails.size());
        Assert.assertSame(lookDetailsData1, lookDetails.get(0));
        Assert.assertSame(lookDetailsData2, lookDetails.get(1));

        Mockito.verify(lookService, Mockito.times(1)).getVisibleLooksForProduct(productModel, PRODUCT_LOOKS_MAX);
        Mockito.verify(targetProductGroupService, Mockito.never())
                .getAllActiveLooksForProduct(Mockito.any(ProductModel.class));
    }

    @Test
    public void testGetStepLooksForProductEmpty() {
        BDDMockito.given(Boolean
                .valueOf(targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.STEP_SHOP_THE_LOOK)))
                .willReturn(Boolean.TRUE);

        BDDMockito.given(lookService.getVisibleLooksForProduct(productModel, PRODUCT_LOOKS_MAX))
                .willReturn(null);
        final List<LookDetailsData> lookDetails = targetLookPageFacadeImpl.getActiveLooksForProduct(productModel,
                PRODUCT_LOOKS_MAX);
        Assert.assertNotNull(lookDetails);
        Assert.assertEquals(0, lookDetails.size());
    }

}

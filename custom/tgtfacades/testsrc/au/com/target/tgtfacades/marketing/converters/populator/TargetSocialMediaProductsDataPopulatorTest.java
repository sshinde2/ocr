package au.com.target.tgtfacades.marketing.converters.populator;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.HashSet;
import java.util.Set;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.marketing.data.SocialMediaProductsData;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetLookProductModel;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetSocialMediaProductsDataPopulatorTest {

    private final TargetSocialMediaProductsDataPopulator populator = new TargetSocialMediaProductsDataPopulator();

    private final String url = "https://instagram.com/p/9Dm36yGK1O/";

    @Mock
    private TargetLookModel look;

    @Mock
    private TargetLookProductModel product1, product2;

    @Before
    public void setup() {
        given(look.getInstagramUrl()).willReturn(url);
        given(product1.getProductCode()).willReturn("LP01");
        given(product2.getProductCode()).willReturn("LP02");

        final Set<TargetLookProductModel> products = new HashSet<>();
        products.add(product1);
        products.add(product2);
        given(look.getProducts()).willReturn(products);
    }

    @Test
    public void testPopulate() throws Exception {
        final SocialMediaProductsData result = new SocialMediaProductsData();
        populator.populate(look, result);

        Assert.assertEquals(url, result.getUrl());
        Assert.assertThat(result.getProductCodes(), Matchers.hasItems("LP01", "LP02"));
    }
}

package au.com.target.tgtfacades.marketing.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.marketing.data.SocialMediaProductsData;
import au.com.target.tgtfacades.marketing.data.SocialMediaProductsListData;
import au.com.target.tgtmarketing.model.SocialMediaProductsModel;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.shopthelook.LookService;
import au.com.target.tgtmarketing.social.TargetSocialMediaProductsService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetSocialMediaProductsFacadeImplTest {

    @Mock
    private Converter<SocialMediaProductsModel, SocialMediaProductsData> targetSocialMediaProductsConverter;

    @Mock
    private Converter<TargetLookModel, SocialMediaProductsData> targetSocialMediaProductsDataConverter;

    @Mock
    private TargetSocialMediaProductsService mockTargetSocialMediaProductsService;

    @Mock
    private LookService lookService;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @InjectMocks
    private final TargetSocialMediaProductsFacadeImpl targetSocialMediaProductsFacadeImpl = new TargetSocialMediaProductsFacadeImpl();

    @Test
    public void testGetSocialMediaProductsByCreationTimeDescending() {
        final SocialMediaProductsModel mockSocialMediaProductsModel = mock(SocialMediaProductsModel.class);
        final SocialMediaProductsModel mockSocialMediaProductsModel2 = mock(SocialMediaProductsModel.class);
        final SocialMediaProductsModel mockSocialMediaProductsModel3 = mock(SocialMediaProductsModel.class);

        final List<SocialMediaProductsModel> serviceResults = new ArrayList<>();
        Collections.addAll(serviceResults, mockSocialMediaProductsModel, mockSocialMediaProductsModel2,
                mockSocialMediaProductsModel3);

        final SocialMediaProductsData socialMediaProductsData = new SocialMediaProductsData();
        given(targetSocialMediaProductsConverter.convert(mockSocialMediaProductsModel))
                .willReturn(socialMediaProductsData);
        final SocialMediaProductsData socialMediaProductsData2 = new SocialMediaProductsData();
        given(targetSocialMediaProductsConverter.convert(mockSocialMediaProductsModel2))
                .willReturn(socialMediaProductsData2);
        final SocialMediaProductsData socialMediaProductsData3 = new SocialMediaProductsData();
        given(targetSocialMediaProductsConverter.convert(mockSocialMediaProductsModel3))
                .willReturn(socialMediaProductsData3);

        given(mockTargetSocialMediaProductsService.getSocialMediaProductsByCreationTimeDescending(14))
                .willReturn(serviceResults);

        final SocialMediaProductsListData result = targetSocialMediaProductsFacadeImpl
                .getSocialMediaProductsByCreationTimeDescending(14);

        assertThat(result.getTargetSocialMediaProductsList()).containsExactly(socialMediaProductsData,
                socialMediaProductsData2, socialMediaProductsData3);
    }

    @Test
    public void testGetSocialMediaProductsByCreationTimeDescendingWithNoResults() {
        final List<SocialMediaProductsModel> serviceResults = new ArrayList<>();

        given(mockTargetSocialMediaProductsService.getSocialMediaProductsByCreationTimeDescending(14))
                .willReturn(serviceResults);

        final SocialMediaProductsListData result = targetSocialMediaProductsFacadeImpl
                .getSocialMediaProductsByCreationTimeDescending(14);

        assertThat(result.getTargetSocialMediaProductsList()).isEmpty();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetSocialMediaProductsByCreationTimeDescendingWithNoResultsCountZero() {
        targetSocialMediaProductsFacadeImpl.getSocialMediaProductsByCreationTimeDescending(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetSocialMediaProductsByCreationTimeDescendingWithNoResultsCountNeative() {
        targetSocialMediaProductsFacadeImpl.getSocialMediaProductsByCreationTimeDescending(-1);
    }

    @Test
    public void testGetSocialMediaProductsFromStepLooks() {
        Mockito.when(Boolean.valueOf(
                targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.STEP_SHOP_THE_LOOK_APP)))
                .thenReturn(Boolean.TRUE);

        final TargetLookModel look1 = mock(TargetLookModel.class);
        final TargetLookModel look2 = mock(TargetLookModel.class);
        final TargetLookModel look3 = mock(TargetLookModel.class);

        final List<TargetLookModel> serviceResults = new ArrayList<>();
        Collections.addAll(serviceResults, look1, look2,
                look3);

        final SocialMediaProductsData socialMediaProductsData1 = new SocialMediaProductsData();
        given(targetSocialMediaProductsDataConverter.convert(look1))
                .willReturn(socialMediaProductsData1);
        final SocialMediaProductsData socialMediaProductsData2 = new SocialMediaProductsData();
        given(targetSocialMediaProductsDataConverter.convert(look2))
                .willReturn(socialMediaProductsData2);
        final SocialMediaProductsData socialMediaProductsData3 = new SocialMediaProductsData();
        given(targetSocialMediaProductsDataConverter.convert(look3))
                .willReturn(socialMediaProductsData3);

        final ArgumentCaptor<Integer> limit = ArgumentCaptor.forClass(Integer.class);
        given(lookService.getAllVisibleLooksWithInstagramUrl(limit.capture().intValue())).willReturn(serviceResults);

        final SocialMediaProductsListData result = targetSocialMediaProductsFacadeImpl
                .getSocialMediaProductsByCreationTimeDescending(14);

        assertThat(result.getTargetSocialMediaProductsList()).containsExactly(socialMediaProductsData1,
                socialMediaProductsData2, socialMediaProductsData3);
        assertThat(limit.getValue()).isEqualTo(-1);
    }

    @Test
    public void testGetSocialMediaProductsFromStepLooksWithLimit() {
        Mockito.when(Boolean.valueOf(
                targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.STEP_SHOP_THE_LOOK_APP)))
                .thenReturn(Boolean.TRUE);
        Mockito.when(Boolean.valueOf(
                targetFeatureSwitchFacade
                        .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.STEP_SHOP_THE_LOOK_APP_LIMIT)))
                .thenReturn(Boolean.TRUE);

        final TargetLookModel look1 = mock(TargetLookModel.class);
        final TargetLookModel look2 = mock(TargetLookModel.class);
        final TargetLookModel look3 = mock(TargetLookModel.class);

        final List<TargetLookModel> serviceResults = new ArrayList<>();
        Collections.addAll(serviceResults, look1, look2,
                look3);

        final SocialMediaProductsData socialMediaProductsData1 = new SocialMediaProductsData();
        given(targetSocialMediaProductsDataConverter.convert(look1))
                .willReturn(socialMediaProductsData1);
        final SocialMediaProductsData socialMediaProductsData2 = new SocialMediaProductsData();
        given(targetSocialMediaProductsDataConverter.convert(look2))
                .willReturn(socialMediaProductsData2);
        final SocialMediaProductsData socialMediaProductsData3 = new SocialMediaProductsData();
        given(targetSocialMediaProductsDataConverter.convert(look3))
                .willReturn(socialMediaProductsData3);

        final ArgumentCaptor<Integer> limit = ArgumentCaptor.forClass(Integer.class);
        given(lookService.getAllVisibleLooksWithInstagramUrl(limit.capture().intValue())).willReturn(serviceResults);

        final SocialMediaProductsListData result = targetSocialMediaProductsFacadeImpl
                .getSocialMediaProductsByCreationTimeDescending(14);

        assertThat(result.getTargetSocialMediaProductsList()).containsExactly(socialMediaProductsData1,
                socialMediaProductsData2, socialMediaProductsData3);
        assertThat(limit.getValue()).isEqualTo(14);
    }
}

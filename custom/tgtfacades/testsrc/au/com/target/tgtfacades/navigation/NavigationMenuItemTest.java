/**
 * 
 */
package au.com.target.tgtfacades.navigation;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Test;


/**
 * @author rmcalave
 * 
 */
@UnitTest
public class NavigationMenuItemTest {
    @Test
    public void testNotEquals() {
        final NavigationMenuItem lhs = new NavigationMenuItem("Item1", "/help", null, true, false, false);
        final NavigationMenuItem rhs = new NavigationMenuItem("Item2", "/company", null, true, false, false);

        Assert.assertFalse(lhs.equals(rhs));
    }

    @Test
    public void testEquals() {
        final NavigationMenuItem lhs = new NavigationMenuItem("Item1", "/help", null, true, false, false);
        final NavigationMenuItem rhs = new NavigationMenuItem("Item1", "/help", null, true, false, false);

        Assert.assertTrue(lhs.equals(rhs));
    }
}

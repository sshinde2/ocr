/**
 * 
 */
package au.com.target.tgtfacades.offer.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.offer.converter.TargetMobileOfferHeadingConverter;
import au.com.target.tgtfacades.offers.data.TargetMobileOfferHeadingData;
import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;
import au.com.target.tgtmarketing.offers.TargetMobileOfferHeadingService;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetMobileOfferHeadingFacadeImplTest {

    @InjectMocks
    private final TargetMobileOfferHeadingFacadeImpl targetMobileOfferHeadingFacade = new TargetMobileOfferHeadingFacadeImpl();

    @Mock
    private TargetMobileOfferHeadingService targetMobileOfferHeadingsService;

    @Mock
    private TargetMobileOfferHeadingConverter targetMobileOfferHeadingConverter;

    @Before
    public void setup() {
        targetMobileOfferHeadingFacade.setTargetMobileOfferHeadingConverter(targetMobileOfferHeadingConverter);
        targetMobileOfferHeadingFacade.setTargetMobileOfferHeadingsService(targetMobileOfferHeadingsService);
    }

    @Test
    public void testGetTargetMobileOfferHeadingWithNoOfferHeadings() {
        BDDMockito.given(targetMobileOfferHeadingsService.getAllTargetMobileOfferHeadings()).willReturn(null);

        final List<TargetMobileOfferHeadingData> targetMobileOfferHeadingDataList = targetMobileOfferHeadingFacade
                .getTargetMobileOfferHeadings();
        Assert.assertNotNull(targetMobileOfferHeadingDataList);
        Assert.assertTrue(CollectionUtils.isEmpty(targetMobileOfferHeadingDataList));
    }

    @Test
    public void testGetTargetMobileOfferHeadingWithOfferHeadings() {
        final TargetMobileOfferHeadingData targetMobileOfferHeadingData1 = Mockito
                .mock(TargetMobileOfferHeadingData.class);

        final TargetMobileOfferHeadingData targetMobileOfferHeadingData2 = Mockito
                .mock(TargetMobileOfferHeadingData.class);

        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel1 = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel2 = Mockito
                .mock(TargetMobileOfferHeadingModel.class);

        BDDMockito.given(targetMobileOfferHeadingsService.getAllTargetMobileOfferHeadings()).willReturn(
                new ArrayList<TargetMobileOfferHeadingModel>(Arrays.asList(
                        targetMobileOfferHeadingModel1, targetMobileOfferHeadingModel2)));

        BDDMockito.given(targetMobileOfferHeadingConverter.convert(targetMobileOfferHeadingModel1)).willReturn(
                targetMobileOfferHeadingData1);
        BDDMockito.given(targetMobileOfferHeadingConverter.convert(targetMobileOfferHeadingModel2)).willReturn(
                targetMobileOfferHeadingData2);

        final List<TargetMobileOfferHeadingData> targetMobileOfferHeadingDataList = targetMobileOfferHeadingFacade
                .getTargetMobileOfferHeadings();
        Assert.assertNotNull(targetMobileOfferHeadingDataList);
        Assert.assertTrue(CollectionUtils.isNotEmpty(targetMobileOfferHeadingDataList));
        Assert.assertSame(targetMobileOfferHeadingData1, targetMobileOfferHeadingDataList.get(0));
        Assert.assertSame(targetMobileOfferHeadingData2, targetMobileOfferHeadingDataList.get(1));
    }
}

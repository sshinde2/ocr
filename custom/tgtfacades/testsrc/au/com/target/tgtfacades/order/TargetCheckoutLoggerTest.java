/**
 * 
 */
package au.com.target.tgtfacades.order;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.validation.support.BindingAwareModelMap;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.Error;
import au.com.target.tgtfacades.response.data.Response;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest({ TargetCheckoutLogger.class, Logger.class })
public class TargetCheckoutLoggerTest {

    @InjectMocks
    private final TargetCheckoutLogger targetCheckoutLogger = new TargetCheckoutLogger();

    @Mock
    private JoinPoint joinPoint;

    @Mock
    private TargetCheckoutFacade targetCheckoutFacade;

    @Mock
    private CartModel cartModel;

    @Mock
    private TargetCustomerModel userModel;

    @Mock
    private Logger logger;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockStatic(Logger.class);
        given(Logger.getLogger("Checkout SPC")).willReturn(logger);
        given(targetCheckoutFacade.getCart()).willReturn(cartModel);
        given(cartModel.getUser()).willReturn(userModel);
        given(cartModel.getCode()).willReturn("00100000");
        given(userModel.getCustomerID()).willReturn("54121242");
    }

    @Test
    public void testLogBeforeWithNullArgs() {
        final Signature signature = mock(Signature.class);
        given(signature.getName()).willReturn("setDeliveryMode");
        given(joinPoint.getSignature()).willReturn(signature);
        given(joinPoint.getArgs()).willReturn(null);
        targetCheckoutLogger.beforeAction(joinPoint);
        verify(logger)
                .info(
                        "Receive API Request:method=setDeliveryMode, cartCode=00100000, customerID=54121242, Parameters:[]");
        Assertions.assertThat(targetCheckoutLogger.getCurrentCartInfo()).isNotNull();
    }

    @Test
    public void testLogBeforeWithEmptyArgs() {
        final Signature signature = mock(Signature.class);
        given(signature.getName()).willReturn("setDeliveryMode");
        given(joinPoint.getSignature()).willReturn(signature);
        given(joinPoint.getArgs()).willReturn(new Object[1]);
        targetCheckoutLogger.beforeAction(joinPoint);
        verify(logger)
                .info(
                        "Receive API Request:method=setDeliveryMode, cartCode=00100000, customerID=54121242, Parameters:[null|]");
        Assertions.assertThat(targetCheckoutLogger.getCurrentCartInfo()).isNotNull();
    }

    @Test
    public void testLogBeforeWithSkippedParamsInArgs() {
        final Signature signature = mock(Signature.class);
        given(signature.getName()).willReturn("setDeliveryMode");
        given(joinPoint.getSignature()).willReturn(signature);
        final Object[] args = new Object[2];
        args[0] = new BindingAwareModelMap();
        args[1] = new MockHttpServletRequest();
        given(joinPoint.getArgs()).willReturn(args);
        targetCheckoutLogger.beforeAction(joinPoint);
        verify(logger)
                .info(
                        "Receive API Request:method=setDeliveryMode, cartCode=00100000, customerID=54121242, Parameters:[]");
        Assertions.assertThat(targetCheckoutLogger.getCurrentCartInfo()).isNotNull();
    }

    @Test
    public void testLogSuccessfulReturn() {
        final Signature signature = mock(Signature.class);
        given(signature.getName()).willReturn("setDeliveryMode");
        given(joinPoint.getSignature()).willReturn(signature);
        final Response response = new Response(true);
        targetCheckoutLogger.afterReturnAction(joinPoint, response);
        verify(logger)
                .info(
                        "Finish API Request:method=setDeliveryMode, cartCode=00100000, customerID=54121242, Result:[SUCCESS:true]");
        Assertions.assertThat(targetCheckoutLogger.getCurrentCartInfo()).isNull();
    }

    @Test
    public void testLogFailedReturn() {
        final Signature signature = mock(Signature.class);
        given(signature.getName()).willReturn("setDeliveryMode");
        given(joinPoint.getSignature()).willReturn(signature);
        final Response response = new Response(false);
        final BaseResponseData data = new BaseResponseData();
        final Error error = new Error("code");
        error.setMessage("errorDetail");
        data.setError(error);
        response.setData(data);
        targetCheckoutLogger.afterReturnAction(joinPoint, response);
        verify(logger)
                .error(
                        "Finish API Request with Error:method=setDeliveryMode, cartCode=00100000, customerID=54121242"
                                + ", Result:[SUCCESS:false ERROR=code(errorDetail)  , requestUri=null ,\n StackTrace:null]");
        Assertions.assertThat(targetCheckoutLogger.getCurrentCartInfo()).isNull();
    }

    @Test
    public void testLogException() {
        final Signature signature = mock(Signature.class);
        given(signature.getName()).willReturn("setDeliveryMode");
        given(joinPoint.getSignature()).willReturn(signature);
        final Exception exception = new RuntimeException("error!");
        targetCheckoutLogger.afterThrow(joinPoint, exception);
        verify(logger)
                .error("Finish API Request with Exception:method=setDeliveryMode, cartCode=00100000, customerID=54121242, Exception Thrown:[RuntimeException|error!]",
                        exception);
        Assertions.assertThat(targetCheckoutLogger.getCurrentCartInfo()).isNull();
    }
}
/**
 * 
 */
package au.com.target.tgtfacades.order.converters;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.FlybuysRedeemConfigModel;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedeemConfigData;
import junit.framework.Assert;


/**
 * @author vivek
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FlybuysRedeemConfigConverterTest {
    @InjectMocks
    private final FlybuysRedeemConfigConverter flybuysRedeemConfigConverter = new FlybuysRedeemConfigConverter();


    @Test
    public void testPopulate() {
        final Double minRedeemable = Double.valueOf(10);
        final Double maxRedeemable = Double.valueOf(1000);
        final Double minCartValue = Double.valueOf(100);
        final Integer minPointsBalance = Integer.valueOf(10);
        final Boolean active = Boolean.FALSE;

        final FlybuysRedeemConfigModel mockFlybuysRedeemConfigModel = mock(FlybuysRedeemConfigModel.class);

        given(mockFlybuysRedeemConfigModel.getMinRedeemable()).willReturn(minRedeemable);
        given(mockFlybuysRedeemConfigModel.getMaxRedeemable()).willReturn(maxRedeemable);
        given(mockFlybuysRedeemConfigModel.getMinCartValue()).willReturn(minCartValue);
        given(mockFlybuysRedeemConfigModel.getMinPointsBalance()).willReturn(minPointsBalance);
        given(mockFlybuysRedeemConfigModel.getActive()).willReturn(active);
        flybuysRedeemConfigConverter.setTargetClass(FlybuysRedeemConfigData.class);

        final FlybuysRedeemConfigData flybuysRedeemConfigData = flybuysRedeemConfigConverter
                .convert(mockFlybuysRedeemConfigModel);

        Assert.assertEquals(minRedeemable, flybuysRedeemConfigData.getMinRedeemable());
        Assert.assertEquals(maxRedeemable, flybuysRedeemConfigData.getMaxRedeemable());
        Assert.assertEquals(minCartValue, flybuysRedeemConfigData.getMinCartValue());
        Assert.assertEquals(minPointsBalance, flybuysRedeemConfigData.getMinPointsBalance());
        Assert.assertEquals(active, flybuysRedeemConfigData.getActive());

    }
}

/**
 * 
 */
package au.com.target.tgtfacades.order.converters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.model.OrderEntryReturnRecordEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.order.data.OrderModificationData;


/**
 * 
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderEntryReturnRecordEntryConverterTest {

    private static final String ORDER_CODE = "111111111111";
    private static final Integer DIGITS = Integer.valueOf(2);
    private static final String CURRENCY_ISO = "AU";


    @InjectMocks
    private final OrderEntryReturnRecordEntryConverter orderEntryReturnRecordEntryConverter = new OrderEntryReturnRecordEntryConverter();

    @Mock(name = "orderEntryConverter")
    private Converter<AbstractOrderEntryModel, OrderEntryData> mockOrderEntryConverter;

    @Mock
    private PriceDataFactory mockPriceDataFactory;

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateWithReturnRecordAndDiscounts() {
        final Date creationTime = new Date();
        final Long returnedQuantity = Long.valueOf(2L);

        final Double appliedDiscountForOne = Double.valueOf(0.5d);
        final Double appliedDiscountForAll = Double.valueOf(1d);

        final BigDecimal basePrice = new BigDecimal(10d);
        final PriceData basePriceData = new PriceData();
        basePriceData.setValue(basePrice);

        final BigDecimal totalPrice = new BigDecimal(20d);
        final PriceData totalPriceData = new PriceData();
        totalPriceData.setValue(totalPrice);

        final BigDecimal basePriceWithDiscount = new BigDecimal(9.5d);
        final PriceData basePriceDataWithDiscount = new PriceData();
        basePriceDataWithDiscount.setValue(basePriceWithDiscount);

        final BigDecimal totalPriceWithDiscount = new BigDecimal(19d);
        final PriceData totalPriceDataWithDiscount = new PriceData();
        totalPriceDataWithDiscount.setValue(totalPriceWithDiscount);

        given(mockPriceDataFactory.create(PriceDataType.BUY, totalPrice, CURRENCY_ISO)).willReturn(totalPriceData);
        given(mockPriceDataFactory.create(PriceDataType.BUY, basePriceWithDiscount, CURRENCY_ISO)).willReturn(
                basePriceDataWithDiscount);
        given(mockPriceDataFactory.create(PriceDataType.BUY, totalPriceWithDiscount, CURRENCY_ISO)).willReturn(
                totalPriceDataWithDiscount);

        final CurrencyModel mockCurrency = mock(CurrencyModel.class);
        given(mockCurrency.getIsocode()).willReturn(CURRENCY_ISO);
        given(mockCurrency.getDigits()).willReturn(DIGITS);

        final OrderModel mockOrderModel = mock(OrderModel.class);
        given(mockOrderModel.getCode()).willReturn(ORDER_CODE);
        given(mockOrderModel.getCurrency()).willReturn(mockCurrency);

        final DiscountValue discountValueForOne = mock(DiscountValue.class);
        given(discountValueForOne.getAppliedValue()).willReturn(appliedDiscountForOne);

        final DiscountValue discountValueForAll = mock(DiscountValue.class);
        given(discountValueForAll.getAppliedValue()).willReturn(appliedDiscountForAll);

        final DiscountValue discountValueMock = mock(DiscountValue.class);
        given(discountValueMock.apply(1d, basePrice.doubleValue(), DIGITS.intValue(), CURRENCY_ISO))
                .willReturn(discountValueForOne);
        given(
                discountValueMock.apply(returnedQuantity.doubleValue(), totalPrice.doubleValue(), DIGITS.intValue(),
                        CURRENCY_ISO))
                                .willReturn(discountValueForAll);

        final OrderEntryModel mockOrderEntryModel = mock(OrderEntryModel.class);
        given(mockOrderEntryModel.getOrder()).willReturn(mockOrderModel);
        given(mockOrderEntryModel.getDiscountValues()).willReturn(Arrays.asList(discountValueMock));

        final OrderEntryReturnRecordEntryModel mockOrderEntryReturnRecordEntryModel = mock(
                OrderEntryReturnRecordEntryModel.class);
        given(mockOrderEntryReturnRecordEntryModel.getOrderEntry()).willReturn(mockOrderEntryModel);
        given(mockOrderEntryReturnRecordEntryModel.getCreationtime()).willReturn(creationTime);
        given(mockOrderEntryReturnRecordEntryModel.getReturnedQuantity()).willReturn(returnedQuantity);

        final OrderModificationData orderModificationData = new OrderModificationData();
        orderModificationData.setBasePrice(basePriceData);

        orderEntryReturnRecordEntryConverter.populate(mockOrderEntryReturnRecordEntryModel, orderModificationData);

        assertEquals(orderModificationData.getBasePrice(), basePriceDataWithDiscount);
        assertEquals(orderModificationData.getTotalPrice(), totalPriceDataWithDiscount);

    }

    @Test
    public void testPopuplateWithNullNecessaryField() {
        final Date creationTime = new Date();
        final OrderModel mockOrderModel = mock(OrderModel.class);
        given(mockOrderModel.getCode()).willReturn(ORDER_CODE);

        final DiscountValue discountValueMock = mock(DiscountValue.class);

        final OrderEntryModel mockOrderEntryModel = mock(OrderEntryModel.class);
        given(mockOrderEntryModel.getOrder()).willReturn(mockOrderModel);
        given(mockOrderEntryModel.getDiscountValues()).willReturn(Arrays.asList(discountValueMock));

        final OrderEntryReturnRecordEntryModel mockOrderEntryReturnRecordEntryModel = mock(
                OrderEntryReturnRecordEntryModel.class);
        given(mockOrderEntryReturnRecordEntryModel.getOrderEntry()).willReturn(mockOrderEntryModel);
        given(mockOrderEntryReturnRecordEntryModel.getCreationtime()).willReturn(creationTime);
        given(mockOrderEntryReturnRecordEntryModel.getReturnedQuantity()).willReturn(null);

        final OrderModificationData orderModificationData = new OrderModificationData();

        orderEntryReturnRecordEntryConverter.populate(mockOrderEntryReturnRecordEntryModel, orderModificationData);

        assertTrue(orderModificationData.getBasePrice() == null);
        assertTrue(orderModificationData.getTotalPrice() == null);
        assertTrue(orderModificationData.getQuantity() == null);
    }

    @Test
    public void testPopulateWithEmptyDiscounts() {
        final Date creationTime = new Date();
        final Long returnedQuantity = Long.valueOf(2L);

        final BigDecimal basePrice = new BigDecimal(10d);
        final PriceData basePriceData = new PriceData();
        basePriceData.setValue(basePrice);

        final BigDecimal totalPrice = new BigDecimal(20d);
        final PriceData totalPriceData = new PriceData();
        totalPriceData.setValue(totalPrice);

        given(mockPriceDataFactory.create(PriceDataType.BUY, totalPrice, CURRENCY_ISO)).willReturn(totalPriceData);

        final CurrencyModel mockCurrency = mock(CurrencyModel.class);
        given(mockCurrency.getIsocode()).willReturn(CURRENCY_ISO);
        given(mockCurrency.getDigits()).willReturn(DIGITS);

        final OrderModel mockOrderModel = mock(OrderModel.class);
        given(mockOrderModel.getCode()).willReturn(ORDER_CODE);
        given(mockOrderModel.getCurrency()).willReturn(mockCurrency);

        final OrderEntryModel mockOrderEntryModel = mock(OrderEntryModel.class);
        given(mockOrderEntryModel.getOrder()).willReturn(mockOrderModel);
        given(mockOrderEntryModel.getDiscountValues()).willReturn(Collections.EMPTY_LIST);

        final OrderEntryReturnRecordEntryModel mockOrderEntryReturnRecordEntryModel = mock(
                OrderEntryReturnRecordEntryModel.class);
        given(mockOrderEntryReturnRecordEntryModel.getOrderEntry()).willReturn(mockOrderEntryModel);
        given(mockOrderEntryReturnRecordEntryModel.getCreationtime()).willReturn(creationTime);
        given(mockOrderEntryReturnRecordEntryModel.getReturnedQuantity()).willReturn(returnedQuantity);

        final OrderModificationData orderModificationData = new OrderModificationData();
        orderModificationData.setBasePrice(basePriceData);

        orderEntryReturnRecordEntryConverter.populate(mockOrderEntryReturnRecordEntryModel, orderModificationData);

        assertEquals(orderModificationData.getBasePrice(), basePriceData);
        assertEquals(orderModificationData.getTotalPrice(), totalPriceData);
    }

}

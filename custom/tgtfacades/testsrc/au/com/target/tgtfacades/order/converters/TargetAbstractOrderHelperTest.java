/**
 * 
 */
package au.com.target.tgtfacades.order.converters;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeValueModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionService;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.configuration.Configuration;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.delivery.dto.DeliveryCostDto;
import au.com.target.tgtcore.delivery.dto.DeliveryCostEnumType;
import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.model.TargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.order.data.LayByDueDateData;
import au.com.target.tgtfacades.order.data.LayByPaymentData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtlayby.data.PaymentDueData;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.order.TargetLaybyPaymentDueService;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


/**
 * @author Benoit VanalderWeireldt
 * 
 */
@UnitTest
public class TargetAbstractOrderHelperTest {

    private static final String HOME_DELIVERY = null;

    private static final String CNC_DELIVERY = null;

    private static final String EXPRESS_DELIVERY = null;

    private static final String DISCLAIMER = null;

    @Mock
    private AbstractOrderModel abstractOrderModel;

    @Mock
    private AbstractOrderEntryModel abstractOrderEntryModel;

    @Mock
    private AbstractOrderEntryModel abstractEntryModelSize;

    @Mock
    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;

    @Mock
    private PurchaseOptionModel purchaseOptionModelLayby;

    @Mock
    private PurchaseOptionConfigModel purchaseOptionConfigModelLayby;

    @Mock
    private PurchaseOptionModel purchaseOptionModelLongTermLayby;

    @Mock
    private PurchaseOptionConfigModel purchaseOptionConfigModelLongTermLayby;

    @Mock
    private PurchaseOptionModel purchaseOptionModelBuyNow;

    @Mock
    private PriceDataFactory mockPriceDataFactory;

    @Mock
    private PaymentDueData paymentDueData;

    @Mock
    private PaymentDueData initialPayment;

    @Mock
    private PaymentDueData finalPayment;

    @Mock
    private PaymentDueData intermediatePayment;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Mock
    private TargetLaybyPaymentDueService targetLaybyPaymentDueService;

    @Mock
    private AbstractTargetZoneDeliveryModeValueModel targetZoneDeliveryModeValueModel;

    @Mock
    private TargetDeliveryService targetDeliveryService;
    @Mock
    private Converter<TargetZoneDeliveryModeModel, TargetZoneDeliveryModeData> targetZoneDeliveryModeConverter;
    @Mock
    private SessionService sessionService;
    @Mock
    private ConfigurationService configurationService;
    @Mock
    private TargetSalesApplicationService targetSalesApplicationService;
    @Mock
    private CartModel cartModel;
    @Mock
    private AbstractTargetZoneDeliveryModeValueModel modeValueModel;
    @Mock
    private PriceData priceData;
    @Mock
    private CurrencyModel currencyModel;
    @Mock
    private Configuration config;
    @Mock
    private ProductModel productModel;
    @Mock
    private TargetProductModel targetProductModel;
    @Mock
    private ProductTypeModel productType;

    private TargetZoneDeliveryModeModel homeDelivery;
    private TargetZoneDeliveryModeModel cncDelivery;
    private TargetZoneDeliveryModeModel expressDelivery;

    private ArrayList<TargetZoneDeliveryModeModel> targetZoneDeliveryModes;

    @InjectMocks
    private final AbstractOrderHelper targetAbstractOrderHelper = new AbstractOrderHelper();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        given(targetLaybyPaymentDueService.getInitialPaymentDue(abstractOrderModel)).willReturn(initialPayment);
        given(targetLaybyPaymentDueService.getFinalPaymentDue(abstractOrderModel)).willReturn(finalPayment);
        given(purchaseOptionConfigModelLayby.getAllowPaymentDues()).willReturn(Boolean.TRUE);
        given(purchaseOptionConfigModelLongTermLayby.getAllowPaymentDues()).willReturn(Boolean.TRUE);
        given(configurationService.getConfiguration()).willReturn(config);
        given(
                targetDeliveryService.getDeliveryCostForDeliveryType(
                        any(AbstractTargetZoneDeliveryModeValueModel.class), any(AbstractOrderModel.class)))
                                .willReturn(new DeliveryCostDto());
    }

    @Test
    public void testIsLayBy() {
        given(abstractOrderModel.getPurchaseOptionConfig()).willReturn(purchaseOptionConfigModelLayby);
        assertThat(targetAbstractOrderHelper.isLayBy(abstractOrderModel)).isTrue();
    }

    @Test
    public void testIsLayByTriangulate() {
        given(abstractOrderModel.getPurchaseOptionConfig()).willReturn(purchaseOptionConfigModelLongTermLayby);
        assertThat(targetAbstractOrderHelper.isLayBy(abstractOrderModel)).isTrue();
    }

    @Test
    public void testIsNotLayBy() {
        given(abstractOrderModel.getPurchaseOption()).willReturn(purchaseOptionModelBuyNow);
        assertThat(targetAbstractOrderHelper.isLayBy(abstractOrderModel)).isFalse();
    }

    @Test
    public void testIsNotLayByTriangulate() {
        given(abstractOrderModel.getPurchaseOption()).willReturn(null);
        assertThat(targetAbstractOrderHelper.isLayBy(abstractOrderModel)).isFalse();
    }

    @Test
    public void testCreateFormattedAmountNull() {
        assertThat(targetAbstractOrderHelper.createFormattedAmount(null)).isNull();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCreateFormattedAmountPercentage() {
        given(paymentDueData.getPercentage()).willReturn(20.0);
        assertThat(targetAbstractOrderHelper.createFormattedAmount(paymentDueData)).isEqualTo("20%");
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCreateFormattedAmountPercentageTriangulate() {
        given(paymentDueData.getPercentage()).willReturn(1.5);
        assertThat(targetAbstractOrderHelper.createFormattedAmount(paymentDueData)).isEqualTo("1.5%");
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCreateFormattedAmountAbsolute() {
        given(paymentDueData.getAmount()).willReturn(15D);
        assertThat(targetAbstractOrderHelper.createFormattedAmount(paymentDueData)).isEqualTo("$15.00");
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCreateFormattedAmountAbsoluteTriangulate() {
        given(paymentDueData.getAmount()).willReturn(1.5D);
        assertThat(targetAbstractOrderHelper.createFormattedAmount(paymentDueData)).isEqualTo("$1.50");
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCreateAmountDueDates() throws ParseException {

        final Set<PaymentDueData> paymentDues = new HashSet<>();
        paymentDues.add(initialPayment);
        paymentDues.add(intermediatePayment);
        paymentDues.add(finalPayment);
        willReturn(paymentDues).given(targetLaybyPaymentDueService)
                .getAllPaymentDuesForAbstractOrder(abstractOrderModel);
        final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        final Date finalDate = sdf.parse("20/07/2013 00:00:00");

        willReturn(finalDate).given(finalPayment).getDueDate();
        willReturn(20.0).given(initialPayment).getAmount();
        final List<LayByDueDateData> dueData = targetAbstractOrderHelper.createAmountDueDates(paymentDues);
        assertThat(dueData).isNotEmpty();
        assertThat(dueData.iterator().next().getFormattedDueDate()).isNotNull();
        assertThat(dueData.iterator().next().getFormattedAmount()).isNotNull();
    }

    @Test
    public void testCreateMaximumPaymentAmountData() {
        final String currencyIsoCode = "AU";

        final CurrencyModel mockCurrencyModel = Mockito.mock(CurrencyModel.class);
        given(mockCurrencyModel.getIsocode()).willReturn(currencyIsoCode);

        final PurchaseOptionConfigModel mockPurchaseOptionConfig = Mockito.mock(PurchaseOptionConfigModel.class);
        given(mockPurchaseOptionConfig.getMaxPercentage()).willReturn(Double.valueOf(95.0d));

        final AbstractOrderModel mockAbstractOrderModel = Mockito.mock(AbstractOrderModel.class);
        given(mockAbstractOrderModel.getPurchaseOptionConfig()).willReturn(mockPurchaseOptionConfig);
        given(mockAbstractOrderModel.getTotalPrice()).willReturn(Double.valueOf(100.0d));
        given(mockAbstractOrderModel.getCurrency()).willReturn(mockCurrencyModel);

        final PriceData mockPriceData = Mockito.mock(PriceData.class);
        given(mockPriceDataFactory.create(Mockito.eq(PriceDataType.BUY),
                Mockito.any(BigDecimal.class), Mockito.eq(currencyIsoCode))).willReturn(mockPriceData);

        final LayByPaymentData result = targetAbstractOrderHelper
                .createMaximumPaymentAmountData(mockAbstractOrderModel);

        assertThat(result).isNotNull();
        assertThat(mockPriceData).isEqualTo(result.getActualAmount());
        assertThat(result.getPercentage()).isEqualTo("95%");
    }

    @Test
    public void testGetProductTypesForEmptyCart() {
        given(abstractOrderModel.getEntries()).willReturn(null);
        final Set<String> productTypes = targetAbstractOrderHelper.getProductTypeSet(abstractOrderModel);
        Assertions.assertThat(productTypes).isEmpty();
    }


    @Test
    public void testGetProductTypesForCart() {
        final AbstractOrderEntryModel cartEntry1 = mock(AbstractOrderEntryModel.class);
        final AbstractTargetVariantProductModel product1 = mock(AbstractTargetVariantProductModel.class);
        final ProductTypeModel normalProductModel = mock(ProductTypeModel.class);
        given(normalProductModel.getCode()).willReturn("normal");
        given(product1.getProductType()).willReturn(normalProductModel);
        final AbstractOrderEntryModel cartEntry2 = mock(AbstractOrderEntryModel.class);
        final AbstractTargetVariantProductModel product2 = mock(AbstractTargetVariantProductModel.class);
        final ProductTypeModel bulkyProductModel = mock(ProductTypeModel.class);
        given(bulkyProductModel.getCode()).willReturn("bulky");
        given(product2.getProductType()).willReturn(bulkyProductModel);

        final AbstractOrderEntryModel cartEntry3 = mock(AbstractOrderEntryModel.class);
        final AbstractTargetVariantProductModel product3 = mock(AbstractTargetVariantProductModel.class);
        final ProductTypeModel egiftcardModel = mock(ProductTypeModel.class);
        given(egiftcardModel.getCode()).willReturn("egiftcardModel");
        given(product3.getProductType()).willReturn(egiftcardModel);

        final AbstractOrderEntryModel cartEntry4 = mock(AbstractOrderEntryModel.class);
        final AbstractTargetVariantProductModel product4 = mock(AbstractTargetVariantProductModel.class);
        final ProductTypeModel physicalGiftcardModel = mock(ProductTypeModel.class);
        given(physicalGiftcardModel.getCode()).willReturn("giftCard");
        given(product4.getProductType()).willReturn(physicalGiftcardModel);

        final List<AbstractOrderEntryModel> entries = ImmutableList.of(cartEntry1, cartEntry2, cartEntry3, cartEntry4);
        given(abstractOrderModel.getEntries()).willReturn(entries);
        given(cartEntry1.getProduct()).willReturn(product1);
        given(cartEntry2.getProduct()).willReturn(product2);
        given(cartEntry3.getProduct()).willReturn(product3);
        given(cartEntry4.getProduct()).willReturn(product4);
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper).doesProductHavePhysicalDeliveryMode(product1);
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper).doesProductHavePhysicalDeliveryMode(product2);
        willReturn(Boolean.FALSE).given(targetDeliveryModeHelper).doesProductHavePhysicalDeliveryMode(product3);
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper).doesProductHavePhysicalDeliveryMode(product4);
        final Set<String> productTypes = targetAbstractOrderHelper.getProductTypeSet(abstractOrderModel);
        assertThat(productTypes.size()).isEqualTo(3);
        assertThat(productTypes.contains("normal")).isTrue();
        assertThat(productTypes.contains("bulky")).isTrue();
        assertThat(productTypes.contains("giftCard")).isTrue();
    }

    @Test
    public void testUpdatePickupAvailabilityForClosedStore() {
        final Set<String> cartProductTypeSet = mock(Set.class);
        final TargetPointOfServiceData posData = mock(TargetPointOfServiceData.class);
        given(posData.getClosed()).willReturn(Boolean.TRUE);
        targetAbstractOrderHelper.updateStoreWithPickupAvailability(cartProductTypeSet, posData);
        verify(posData).setAvailableForPickup(Boolean.FALSE);
    }

    @Test
    public void testUpdatePickupAvailabilityForStoreNotAccepteCNC() {
        final Set<String> cartProductTypeSet = mock(Set.class);
        final TargetPointOfServiceData posData = mock(TargetPointOfServiceData.class);
        given(posData.getClosed()).willReturn(Boolean.FALSE);
        given(posData.getAcceptCNC()).willReturn(Boolean.FALSE);
        targetAbstractOrderHelper.updateStoreWithPickupAvailability(cartProductTypeSet, posData);
        verify(posData).setAvailableForPickup(Boolean.FALSE);
    }

    @Test
    public void testUpdatePickupAvailabilityForNormalProduct() {
        final Set<String> cartProductTypeSet = ImmutableSet.of("normal");
        final TargetPointOfServiceData posData = mock(TargetPointOfServiceData.class);
        given(posData.getClosed()).willReturn(Boolean.FALSE);
        given(posData.getAcceptCNC()).willReturn(Boolean.TRUE);
        given(posData.getAcceptedProductTypes()).willReturn(ImmutableSet.of("normal"));
        targetAbstractOrderHelper.updateStoreWithPickupAvailability(cartProductTypeSet, posData);
        verify(posData).setAvailableForPickup(Boolean.TRUE);
    }

    @Test
    public void testUpdatePickupAvailabilityForBulkyProduct() {
        final Set<String> cartProductTypeSet = ImmutableSet.of("bulky");
        final TargetPointOfServiceData posData = mock(TargetPointOfServiceData.class);
        given(posData.getClosed()).willReturn(Boolean.FALSE);
        given(posData.getAcceptCNC()).willReturn(Boolean.TRUE);
        given(posData.getAcceptedProductTypes()).willReturn(ImmutableSet.of("normal"));
        targetAbstractOrderHelper.updateStoreWithPickupAvailability(cartProductTypeSet, posData);
        verify(posData).setAvailableForPickup(Boolean.FALSE);
    }

    @Test
    public void testGetPotentialIncentiveMessage() {
        final String currencyIsocode = "AUD";

        final Double determinantThreshold = Double.valueOf(75.0);
        final Double deliveryFee = Double.valueOf(9.0);
        final Double cartSubtotal = Double.valueOf(25.0);

        given(targetZoneDeliveryModeValueModel.getPotentialIncentiveMessage()).willReturn(
                "Spend {0} more to get {1} delivery!");
        given(targetZoneDeliveryModeValueModel.getMinimum()).willReturn(determinantThreshold);
        given(targetZoneDeliveryModeValueModel.getValue()).willReturn(deliveryFee);
        given(cartModel.getSubtotal()).willReturn(cartSubtotal);

        final CurrencyModel mockCurrency = mock(CurrencyModel.class);
        given(mockCurrency.getIsocode()).willReturn(currencyIsocode);
        given(cartModel.getCurrency()).willReturn(mockCurrency);

        final PriceData mockAmountToQualifyPriceData = mock(PriceData.class);
        given(mockAmountToQualifyPriceData.getFormattedValue()).willReturn("$50.00");

        final PriceData mockDeliveryFeePriceData = mock(PriceData.class);
        given(mockDeliveryFeePriceData.getFormattedValue()).willReturn("$9.00");

        final ArgumentCaptor<BigDecimal> priceDataValueCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        given(mockPriceDataFactory.create(eq(PriceDataType.BUY), priceDataValueCaptor.capture(), eq(currencyIsocode)))
                .willReturn(
                        mockAmountToQualifyPriceData, mockDeliveryFeePriceData);

        final String potentialIncentiveMessage = targetAbstractOrderHelper.getPotentialIncentiveMessage(
                targetZoneDeliveryModeValueModel, cartModel);
        assertThat(potentialIncentiveMessage).isEqualTo("Spend $50 more to get $9 delivery!");

        final List<BigDecimal> capturedPriceDataValues = priceDataValueCaptor.getAllValues();
        assertThat(capturedPriceDataValues).containsExactly(BigDecimal.valueOf(50.0),
                BigDecimal.valueOf(deliveryFee.doubleValue()));
    }

    @Test
    public void testGetIncentiveMetMessage() {
        final String currencyIsocode = "AUD";
        final Double deliveryFee = Double.valueOf(9.0);

        given(targetZoneDeliveryModeValueModel.getIncentiveMetMessage()).willReturn(
                "Your order qualifies for {0} delivery!");
        given(targetZoneDeliveryModeValueModel.getValue()).willReturn(deliveryFee);

        final CurrencyModel mockCurrency = mock(CurrencyModel.class);
        given(mockCurrency.getIsocode()).willReturn(currencyIsocode);
        given(cartModel.getCurrency()).willReturn(mockCurrency);

        final PriceData mockDeliveryFeePriceData = mock(PriceData.class);
        given(mockDeliveryFeePriceData.getFormattedValue()).willReturn("$9.00");

        final ArgumentCaptor<BigDecimal> priceDataValueCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        given(mockPriceDataFactory.create(eq(PriceDataType.BUY), priceDataValueCaptor.capture(), eq(currencyIsocode)))
                .willReturn(mockDeliveryFeePriceData);

        final String potentialIncentiveMessage = targetAbstractOrderHelper.getIncentiveMetMessage(
                targetZoneDeliveryModeValueModel, cartModel);
        assertThat(potentialIncentiveMessage).isEqualTo("Your order qualifies for $9 delivery!");

        final List<BigDecimal> capturedPriceDataValues = priceDataValueCaptor.getAllValues();
        assertThat(capturedPriceDataValues).containsExactly(BigDecimal.valueOf(deliveryFee.doubleValue()));
    }

    @Test
    public void testGetIncentiveMetMessageWithoutPlaceHolders() {
        final String currencyIsocode = "AUD";
        final Double deliveryFee = Double.valueOf(9.0);

        given(targetZoneDeliveryModeValueModel.getIncentiveMetMessage()).willReturn(
                "Your order qualifies for FREE click and collect delivery");
        given(targetZoneDeliveryModeValueModel.getValue()).willReturn(deliveryFee);

        final CurrencyModel mockCurrency = mock(CurrencyModel.class);
        given(mockCurrency.getIsocode()).willReturn(currencyIsocode);
        given(cartModel.getCurrency()).willReturn(mockCurrency);

        final PriceData mockDeliveryFeePriceData = mock(PriceData.class);
        given(mockDeliveryFeePriceData.getFormattedValue()).willReturn("$9.00");

        final ArgumentCaptor<BigDecimal> priceDataValueCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        given(mockPriceDataFactory.create(eq(PriceDataType.BUY), priceDataValueCaptor.capture(), eq(currencyIsocode)))
                .willReturn(mockDeliveryFeePriceData);

        final String potentialIncentiveMessage = targetAbstractOrderHelper.getIncentiveMetMessage(
                targetZoneDeliveryModeValueModel, cartModel);
        assertThat(potentialIncentiveMessage).isEqualTo("Your order qualifies for FREE click and collect delivery");

        final List<BigDecimal> capturedPriceDataValues = priceDataValueCaptor.getAllValues();
        assertThat(capturedPriceDataValues).containsExactly(BigDecimal.valueOf(deliveryFee.doubleValue()));
    }

    @Test
    public void testGetPhisicalDeliveryModes() {
        final TargetZoneDeliveryModeModel targetZoneDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        final TargetZoneDeliveryModeData targetZoneDeliveryModeData = mock(TargetZoneDeliveryModeData.class);
        final AbstractTargetZoneDeliveryModeValueModel deliveryModelValue = mock(
                AbstractTargetZoneDeliveryModeValueModel.class);
        given(targetZoneDeliveryModeConverter.convert(targetZoneDeliveryModeModel)).willReturn(
                targetZoneDeliveryModeData);
        given(targetDeliveryService.getAllCurrentlyActiveDeliveryModes(SalesApplication.WEB)).willReturn(
                ImmutableList.of(targetZoneDeliveryModeModel));
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(targetZoneDeliveryModeModel,
                cartModel)).willReturn(ImmutableList.of(deliveryModelValue));
        given(this.targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper).isDeliveryModeApplicableForOrder(cartModel,
                targetZoneDeliveryModeModel);
        final List<TargetZoneDeliveryModeData> physicalDeliveryModes = targetAbstractOrderHelper
                .getPhysicalDeliveryModes(cartModel);
        assertThat(physicalDeliveryModes.size()).isEqualTo(1);
    }

    @Test
    public void testGetPhisicalDeliveryModesWithNoPostcodeException() {
        final TargetZoneDeliveryModeModel targetZoneDeliveryModeModel = mock(TargetZoneDeliveryModeModel.class);
        final TargetZoneDeliveryModeData targetZoneDeliveryModeData = mock(TargetZoneDeliveryModeData.class);
        given(targetZoneDeliveryModeConverter.convert(targetZoneDeliveryModeModel)).willReturn(
                targetZoneDeliveryModeData);
        final TargetNoPostCodeException noPostcodeException = mock(TargetNoPostCodeException.class);
        given(targetDeliveryService.getAllCurrentlyActiveDeliveryModes(SalesApplication.WEB)).willReturn(
                ImmutableList.of(targetZoneDeliveryModeModel));
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(targetZoneDeliveryModeModel,
                cartModel)).willThrow(noPostcodeException);
        given(this.targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper).isDeliveryModeApplicableForOrder(cartModel,
                targetZoneDeliveryModeModel);
        final List<TargetZoneDeliveryModeData> physicalDeliveryModes = targetAbstractOrderHelper
                .getPhysicalDeliveryModes(cartModel);
        assertThat(physicalDeliveryModes.size()).isEqualTo(1);
    }



    @Test
    public void testPopulateDeliveryValidPostCode() {
        setupDeliveryData();

        final String testMsg = "test-message";
        final String testShortMsg = "test-short-message";

        final double price = 10.0;
        final DeliveryCostDto cost = new DeliveryCostDto();
        cost.setDeliveryCost(price);
        mockExternalDeliveryServiceCalls();
        given(targetZoneDeliveryModeConverter.convert(any(TargetZoneDeliveryModeModel.class))).willReturn(
                new TargetZoneDeliveryModeData(), new TargetZoneDeliveryModeData(), new TargetZoneDeliveryModeData());

        given(
                Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                        homeDelivery)))
                                .willReturn(Boolean.TRUE);
        given(
                Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                        cncDelivery)))
                                .willReturn(Boolean.TRUE);
        given(
                Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                        expressDelivery)))
                                .willReturn(Boolean.TRUE);
        given(targetDeliveryService.getDeliveryCostForDeliveryType(modeValueModel, cartModel))
                .willReturn(cost);
        given(modeValueModel.getCurrency()).willReturn(currencyModel);
        given(mockPriceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(price), currencyModel)).willReturn(
                priceData);

        given(modeValueModel.getShortMessage()).willReturn(testShortMsg);
        given(modeValueModel.getMessage()).willReturn(testMsg);
        given(sessionService.getAttribute(TgtCoreConstants.SESSION_POSTCODE)).willReturn("3000");



        final List<TargetZoneDeliveryModeData> deliveryModes = targetAbstractOrderHelper
                .getPhysicalDeliveryModes(cartModel);
        assertThat(CollectionUtils.isNotEmpty(deliveryModes)).isTrue();

        assertThat(deliveryModes.get(0).isPostCodeNotSupported()).isFalse();
        assertThat(deliveryModes.get(1).isPostCodeNotSupported()).isFalse();
        assertThat(deliveryModes.get(2).isPostCodeNotSupported()).isTrue();
    }

    @Test
    public void testPopulateDeliveryDataEmptyMessageList() {
        setupDeliveryData();

        final String testMsg = "test-message";
        final String testShortMsg = "test-short-message";

        mockExternalDeliveryServiceCalls();

        given(modeValueModel.getMessage()).willReturn(testMsg);
        given(modeValueModel.getShortMessage()).willReturn(testShortMsg);

        final List<AbstractTargetZoneDeliveryModeValueModel> deliveryModeValueModelList = new ArrayList<>();
        given(
                targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrderWithoutOrderThreshold(
                        any(TargetZoneDeliveryModeModel.class), (any(CartModel.class)))).willReturn(
                                deliveryModeValueModelList);

        given(
                Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(
                        Mockito.any(CartModel.class),
                        Mockito.any(TargetZoneDeliveryModeModel.class)))).willReturn(Boolean.TRUE);

        final List<TargetZoneDeliveryModeData> deliveryModes = targetAbstractOrderHelper
                .getPhysicalDeliveryModes(cartModel);
        assertThat(deliveryModes.get(0).getIncentiveMetMessage()).isNull();
        assertThat(deliveryModes.get(0).getPotentialIncentiveMessage()).isNull();
        assertThat(CollectionUtils.isNotEmpty(deliveryModes)).isTrue();

        for (final TargetZoneDeliveryModeData modeData : deliveryModes) {
            assertThat(modeData.getLongDescription()).isEqualTo(testMsg);
            assertThat(modeData.getShortDescription()).isEqualTo(testShortMsg);
        }
    }

    @Test
    public void testPopulateDeliveryDataMessageList() {
        setupDeliveryData();

        final String testMsg = "test-message";
        final String testShortMsg = "test-short-message";
        mockExternalDeliveryServiceCalls();

        given(modeValueModel.getMessage()).willReturn(testMsg);
        given(modeValueModel.getShortMessage()).willReturn(testShortMsg);

        final AbstractTargetZoneDeliveryModeValueModel deliveryModeValue = mock(
                AbstractTargetZoneDeliveryModeValueModel.class);
        given(deliveryModeValue.getIncentiveMetMessage()).willReturn("test incentive");
        given(deliveryModeValue.getPotentialIncentiveMessage()).willReturn("test potential");
        final List<AbstractTargetZoneDeliveryModeValueModel> deliveryModeValueModelList = new ArrayList<>();
        deliveryModeValueModelList.add(deliveryModeValue);
        given(
                targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrderWithoutOrderThreshold(
                        any(TargetZoneDeliveryModeModel.class), eq(cartModel))).willReturn(
                                deliveryModeValueModelList);

        final String currencyIsocode = "AUD";

        final CurrencyModel mockCurrency = mock(CurrencyModel.class);
        given(mockCurrency.getIsocode()).willReturn(currencyIsocode);
        given(cartModel.getCurrency()).willReturn(mockCurrency);

        final Double cartSubtotal = Double.valueOf(25.0);
        given(cartModel.getSubtotal()).willReturn(cartSubtotal);

        final Double determinantThreshold = Double.valueOf(75.0);
        given(deliveryModeValue.getMinimum()).willReturn(determinantThreshold);

        final Double deliveryFee = Double.valueOf(9.0);
        given(deliveryModeValue.getValue()).willReturn(deliveryFee);

        final PriceData mockAmountToQualifyPriceData = mock(PriceData.class);
        given(mockAmountToQualifyPriceData.getFormattedValue()).willReturn("$75.00");

        final PriceData mockDeliveryFeePriceData = mock(PriceData.class);
        given(mockDeliveryFeePriceData.getFormattedValue()).willReturn("$9.00");

        given(mockPriceDataFactory.create(eq(PriceDataType.BUY), any(BigDecimal.class), eq(currencyIsocode)))
                .willReturn(
                        mockAmountToQualifyPriceData, mockDeliveryFeePriceData);

        given(
                Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(
                        Mockito.any(CartModel.class),
                        Mockito.any(TargetZoneDeliveryModeModel.class)))).willReturn(Boolean.TRUE);

        final List<TargetZoneDeliveryModeData> deliveryModes = targetAbstractOrderHelper
                .getPhysicalDeliveryModes(cartModel);
        assertThat(deliveryModes.get(0).getPotentialIncentiveMessage()).isEqualTo("test potential");
        assertThat(deliveryModes.get(0).getIncentiveMetMessage()).isNull();
        assertThat(CollectionUtils.isNotEmpty(deliveryModes)).isTrue();

        for (final TargetZoneDeliveryModeData modeData : deliveryModes) {
            assertThat(modeData.getLongDescription()).isEqualTo(testMsg);
            assertThat(modeData.getShortDescription()).isEqualTo(testShortMsg);
        }
    }

    @Test
    public void testPopulateDeliveryDataMessageListForPreOrder() {
        setupDeliveryData();

        final String testPreOrderLongMsg = "test-preorder-long-message";
        final String testShortMsg = "test-short-message";

        mockExternalDeliveryServiceCalls();

        given(modeValueModel.getPreOrderLongMessage()).willReturn(testPreOrderLongMsg);
        given(modeValueModel.getShortMessage()).willReturn(testShortMsg);

        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper).isDeliveryModeApplicableForOrder(
                cartModel, expressDelivery);

        given(cartModel.getContainsPreOrderItems()).willReturn(Boolean.TRUE);

        final List<TargetZoneDeliveryModeData> deliveryModes = targetAbstractOrderHelper
                .getPhysicalDeliveryModes(cartModel);

        for (final TargetZoneDeliveryModeData modeData : deliveryModes) {
            assertThat(modeData.getLongDescription()).isEqualTo(testPreOrderLongMsg);
            assertThat(modeData.getShortDescription()).isEqualTo(testShortMsg);
        }
    }

    @Test
    public void testPopulateDeliveryIncentiveMetDataMessageList() {
        setupDeliveryData();

        final String testMsg = "test-message";
        final String testShortMsg = "test-short-message";
        mockExternalDeliveryServiceCalls();

        given(modeValueModel.getMessage()).willReturn(testMsg);
        given(modeValueModel.getShortMessage()).willReturn(testShortMsg);

        final AbstractTargetZoneDeliveryModeValueModel deliveryModeValue = Mockito
                .mock(AbstractTargetZoneDeliveryModeValueModel.class);
        given(deliveryModeValue.getIncentiveMetMessage()).willReturn("test incentive");
        final List<AbstractTargetZoneDeliveryModeValueModel> deliveryModeValueModelList = new ArrayList<>();
        final List<AbstractTargetZoneDeliveryModeValueModel> valueModelList = new ArrayList<>();
        deliveryModeValueModelList.add(deliveryModeValue);
        valueModelList.add(deliveryModeValue);
        given(
                targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(
                        any(TargetZoneDeliveryModeModel.class), (any(CartModel.class)))).willReturn(
                                valueModelList);
        given(
                targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrderWithoutOrderThreshold(
                        any(TargetZoneDeliveryModeModel.class), (any(CartModel.class)))).willReturn(
                                deliveryModeValueModelList);

        final String currencyIsocode = "AUD";

        final CurrencyModel mockCurrency = mock(CurrencyModel.class);
        given(mockCurrency.getIsocode()).willReturn(currencyIsocode);
        given(cartModel.getCurrency()).willReturn(mockCurrency);

        final Double cartSubtotal = Double.valueOf(25.0);
        given(cartModel.getSubtotal()).willReturn(cartSubtotal);

        final Double determinantThreshold = Double.valueOf(75.0);
        given(deliveryModeValue.getMinimum()).willReturn(determinantThreshold);

        final Double deliveryFee = Double.valueOf(9.0);
        given(deliveryModeValue.getValue()).willReturn(deliveryFee);

        final PriceData mockAmountToQualifyPriceData = mock(PriceData.class);
        given(mockAmountToQualifyPriceData.getFormattedValue()).willReturn("$75.00");

        final PriceData mockDeliveryFeePriceData = mock(PriceData.class);
        given(mockDeliveryFeePriceData.getFormattedValue()).willReturn("$9.00");

        given(mockPriceDataFactory.create(eq(PriceDataType.BUY), any(BigDecimal.class), eq(currencyIsocode)))
                .willReturn(
                        mockAmountToQualifyPriceData, mockDeliveryFeePriceData);

        given(
                Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(
                        Mockito.any(CartModel.class),
                        Mockito.any(TargetZoneDeliveryModeModel.class)))).willReturn(Boolean.TRUE);

        final List<TargetZoneDeliveryModeData> deliveryModes = targetAbstractOrderHelper
                .getPhysicalDeliveryModes(cartModel);
        assertThat(deliveryModes.get(0).getIncentiveMetMessage()).isEqualTo("test incentive");
        assertThat(deliveryModes.get(0).getPotentialIncentiveMessage()).isNull();
        assertThat(CollectionUtils.isNotEmpty(deliveryModes)).isTrue();
    }

    @Test
    public void testPopulateDeliveryDataWhenNoPostCodeFound() {
        setupDeliveryData();
        mockExternalDeliveryServiceCalls();

        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrderWithoutOrderThreshold(
                any(TargetZoneDeliveryModeModel.class), (any(CartModel.class)))).willThrow(
                        new TargetNoPostCodeException("No post code found"));

        final List<TargetZoneDeliveryModeData> deliveryModes = targetAbstractOrderHelper
                .getPhysicalDeliveryModes(cartModel);
        assertThat(deliveryModes).isNull();
    }

    @Test
    public void testPopulateDeliveryData() {
        setupDeliveryData();

        final String testMsg = "test-message";
        final String testShortMsg = "test-short-message";

        final double price = 10.0;
        final DeliveryCostDto cost = new DeliveryCostDto();
        cost.setDeliveryCost(price);
        mockExternalDeliveryServiceCalls();
        given(targetZoneDeliveryModeConverter.convert(any(TargetZoneDeliveryModeModel.class))).willReturn(
                new TargetZoneDeliveryModeData(), new TargetZoneDeliveryModeData(), new TargetZoneDeliveryModeData());


        given(Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                homeDelivery)))
                        .willReturn(Boolean.TRUE);
        given(Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                cncDelivery)))
                        .willReturn(Boolean.TRUE);
        given(Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                expressDelivery)))
                        .willReturn(Boolean.FALSE);
        given(targetDeliveryService.getDeliveryCostForDeliveryType(modeValueModel, cartModel))
                .willReturn(cost);
        given(modeValueModel.getCurrency()).willReturn(currencyModel);
        given(mockPriceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(price), currencyModel)).willReturn(
                priceData);

        given(modeValueModel.getShortMessage()).willReturn(testShortMsg);
        given(modeValueModel.getMessage()).willReturn(testMsg);
        given(modeValueModel.getDisclaimer()).willReturn(DISCLAIMER);



        final List<TargetZoneDeliveryModeData> deliveryModes = targetAbstractOrderHelper
                .getPhysicalDeliveryModes(cartModel);
        assertThat(deliveryModes).isNotEmpty();

        for (final TargetZoneDeliveryModeData modeData : deliveryModes) {
            assertThat(modeData.getLongDescription()).isEqualTo(testMsg);
            assertThat(modeData.getShortDescription()).isEqualTo(testShortMsg);
            assertThat(modeData.getDisclaimer()).isEqualTo(DISCLAIMER);
        }
        assertThat(deliveryModes.get(0).getDeliveryCost()).isEqualTo(priceData);
        assertThat(deliveryModes.get(1).getDeliveryCost()).isEqualTo(priceData);
        assertThat(deliveryModes.get(2).getDeliveryCost()).isNull();
        assertThat(deliveryModes.get(0).isPostCodeNotSupported()).isFalse();
        assertThat(deliveryModes.get(1).isPostCodeNotSupported()).isFalse();
        assertThat(deliveryModes.get(2).isPostCodeNotSupported()).isFalse();
    }

    @Test
    public void testPopulateDeliveryDataForPreOrder() {
        setupDeliveryData();

        final String testPreOrderLongMsg = "test-preorder-long-message";
        final String testShortMsg = "test-short-message";

        mockExternalDeliveryServiceCalls();

        willReturn(Boolean.TRUE)
                .given(targetDeliveryModeHelper).isDeliveryModeApplicableForOrder(cartModel,
                        expressDelivery);

        given(modeValueModel.getShortMessage()).willReturn(testShortMsg);
        given(modeValueModel.getPreOrderLongMessage()).willReturn(testPreOrderLongMsg);
        given(cartModel.getContainsPreOrderItems()).willReturn(Boolean.TRUE);

        final List<TargetZoneDeliveryModeData> deliveryModes = targetAbstractOrderHelper
                .getPhysicalDeliveryModes(cartModel);
        assertThat(deliveryModes).isNotEmpty();

        for (final TargetZoneDeliveryModeData modeData : deliveryModes) {
            assertThat(modeData.getLongDescription()).isEqualTo(testPreOrderLongMsg);
            assertThat(modeData.getShortDescription()).isEqualTo(testShortMsg);
        }
    }


    @Test
    public void testPopulateDeliveryDataWhenModeIsNotApplicable() {
        setupDeliveryData();

        final TargetCartData cartData = new TargetCartData();

        mockExternalDeliveryServiceCalls();
        given(targetZoneDeliveryModeConverter.convert(any(TargetZoneDeliveryModeModel.class))).willReturn(
                new TargetZoneDeliveryModeData(), new TargetZoneDeliveryModeData(), new TargetZoneDeliveryModeData());


        given(Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                homeDelivery)))
                        .willReturn(Boolean.FALSE);
        given(Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                cncDelivery)))
                        .willReturn(Boolean.FALSE);
        given(Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                expressDelivery)))
                        .willReturn(Boolean.FALSE);
        assertThat(cartData.gethasValidDeliveryModes()).isFalse();
    }


    @Test
    public void testgetPhysicalDeliveryModesWithShipsterData() {
        setupDeliveryData();
        mockExternalDeliveryServiceCalls();
        final DeliveryCostDto mockCostDto = mock(DeliveryCostDto.class);
        given(mockCostDto.getType()).willReturn(DeliveryCostEnumType.SHIPSTERFREEDELIVERY);
        given(targetDeliveryService.getDeliveryCostForDeliveryType(
                any(AbstractTargetZoneDeliveryModeValueModel.class), any(AbstractOrderModel.class)))
                        .willReturn(mockCostDto);
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper).isDeliveryModeApplicableForOrder(cartModel,
                homeDelivery);
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper).isDeliveryModeApplicableForOrder(cartModel,
                cncDelivery);
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper).isDeliveryModeApplicableForOrder(cartModel,
                expressDelivery);
        final List<TargetZoneDeliveryModeData> physicalDeliveryModes = targetAbstractOrderHelper
                .getPhysicalDeliveryModes(cartModel);
        assertThat(physicalDeliveryModes).isNotEmpty();
        assertThat(physicalDeliveryModes.get(0).getShipsterData()).isNotNull();
        assertThat(physicalDeliveryModes.get(0).getShipsterData().isAvailable()).isTrue();
    }

    @Test
    public void testgetPhysicalDeliveryModesWithNormalDeliveryMode() {
        setupDeliveryData();
        mockExternalDeliveryServiceCalls();
        final DeliveryCostDto mockCostDto = mock(DeliveryCostDto.class);
        given(mockCostDto.getType()).willReturn(DeliveryCostEnumType.NORMAL);
        given(targetDeliveryService.getDeliveryCostForDeliveryType(
                any(AbstractTargetZoneDeliveryModeValueModel.class), any(AbstractOrderModel.class)))
                        .willReturn(mockCostDto);
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper).isDeliveryModeApplicableForOrder(cartModel,
                homeDelivery);
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper).isDeliveryModeApplicableForOrder(cartModel,
                cncDelivery);
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper).isDeliveryModeApplicableForOrder(cartModel,
                expressDelivery);
        final List<TargetZoneDeliveryModeData> physicalDeliveryModes = targetAbstractOrderHelper
                .getPhysicalDeliveryModes(cartModel);
        assertThat(physicalDeliveryModes).isNotEmpty();
        assertThat(physicalDeliveryModes.get(0).getShipsterData()).isNotNull();
        assertThat(physicalDeliveryModes.get(0).getShipsterData().isAvailable()).isFalse();
    }

    @Test
    public void testGetPhisicalDeliveryModesWithGiftCardProductInBasket() {
        setupDeliveryData();
        final String testMsg = "giftCard-message";
        mockExternalDeliveryServiceCalls();
        given(targetZoneDeliveryModeConverter.convert(any(TargetZoneDeliveryModeModel.class))).willReturn(
                new TargetZoneDeliveryModeData(), new TargetZoneDeliveryModeData(), new TargetZoneDeliveryModeData());

        given(Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                homeDelivery)))
                        .willReturn(Boolean.TRUE);
        given(modeValueModel.getMessage()).willReturn(testMsg);

        final AbstractOrderEntryModel cartEntry = mock(AbstractOrderEntryModel.class);
        final AbstractTargetVariantProductModel giftCardproduct = mock(AbstractTargetVariantProductModel.class);
        final ProductTypeModel physicalGiftcardModel = mock(ProductTypeModel.class);
        given(physicalGiftcardModel.getCode()).willReturn("giftCard");
        given(giftCardproduct.getProductType()).willReturn(physicalGiftcardModel);
        final List<AbstractOrderEntryModel> entries = ImmutableList.of(cartEntry);
        given(cartEntry.getProduct()).willReturn(giftCardproduct);
        given(cartModel.getEntries()).willReturn(entries);

        final List<TargetZoneDeliveryModeData> deliveryModes = targetAbstractOrderHelper
                .getPhysicalDeliveryModes(cartModel);
        assertThat(deliveryModes).isNotEmpty();
        assertThat(deliveryModes.get(0).getLongDescription()).isEqualTo(testMsg);

    }

    @Test
    public void testHasPhysicalGiftCardsWithOrderContainingPhysicalCards() {
        given(abstractOrderModel.getEntries()).willReturn(Collections.singletonList(abstractOrderEntryModel));
        final AbstractTargetVariantProductModel abstractTargetVariantProductModel = createTargetColourVariantProductModel(
                "P1000_red", "giftcard");
        given(abstractOrderEntryModel.getProduct()).willReturn(abstractTargetVariantProductModel);
        assertThat(targetAbstractOrderHelper.hasPhysicalGiftCards(abstractOrderModel)).isTrue();

    }


    @Test
    public void testHasPhysicalGiftCardsWithOrderContainingPhysicalCardsSizeVariants() {
        given(abstractOrderModel.getEntries()).willReturn(Collections.singletonList(abstractOrderEntryModel));
        final AbstractTargetVariantProductModel abstractTargetVariantProductModel = createTargetColourVariantProductModel(
                "P1000_red", "giftcard");
        given(abstractOrderEntryModel.getProduct()).willReturn(abstractTargetVariantProductModel);
        assertThat(targetAbstractOrderHelper.hasPhysicalGiftCards(abstractOrderModel)).isTrue();

    }


    @Test
    public void testHasPhysicalGiftCardsWithOrderContainingNoPhysicalCards() {
        given(abstractOrderModel.getEntries()).willReturn(Collections.singletonList(abstractOrderEntryModel));
        final AbstractTargetVariantProductModel abstractTargetVariantProductModel = createTargetColourVariantProductModel(
                "P1000_red", "digital");
        given(abstractOrderEntryModel.getProduct()).willReturn(abstractTargetVariantProductModel);
        assertThat(targetAbstractOrderHelper.hasPhysicalGiftCards(abstractOrderModel)).isFalse();
    }

    @Test
    public void testHasPhysicalGiftCardsContainingPhysicalCardsColourAndSize() {
        final List orderEntries = new ArrayList<>();
        final AbstractTargetVariantProductModel colourVariantModel = createTargetColourVariantProductModel(
                "P1000_red", "digital");
        final AbstractTargetVariantProductModel sizeVariantModel = createTargetSizeVariantProductModel(
                "P1000_red_S", "giftcard");
        orderEntries.add(abstractOrderEntryModel);
        orderEntries.add(abstractEntryModelSize);
        given(abstractOrderModel.getEntries()).willReturn(orderEntries);
        given(abstractOrderEntryModel.getProduct()).willReturn(colourVariantModel);
        given(abstractEntryModelSize.getProduct()).willReturn(sizeVariantModel);
        assertThat(targetAbstractOrderHelper.hasPhysicalGiftCards(abstractOrderModel)).isTrue();
    }


    @Test
    public void testHasPhysicalGiftCardsContainingNoPhysicalCardsColourAndSize() {
        final List orderEntries = new ArrayList<>();
        final AbstractTargetVariantProductModel colourVariantModel = createTargetColourVariantProductModel(
                "P1000_red", "digital");
        final AbstractTargetVariantProductModel sizeVariantModel = createTargetSizeVariantProductModel(
                "P1000_red_S", "bulky");
        orderEntries.add(abstractOrderEntryModel);
        orderEntries.add(abstractEntryModelSize);
        given(abstractOrderModel.getEntries()).willReturn(orderEntries);
        given(abstractOrderEntryModel.getProduct()).willReturn(colourVariantModel);
        given(abstractEntryModelSize.getProduct()).willReturn(sizeVariantModel);
        assertThat(targetAbstractOrderHelper.hasPhysicalGiftCards(abstractOrderModel)).isFalse();

    }

    /**
     * Method to test incentive met message and potential incentive message for pre-order items in the basket.
     */
    @Test
    public void testGetPhysicalDeliveryModesWithIncentiveMessageForPreOrder() {
        setupDeliveryData();
        mockExternalDeliveryServiceCalls();
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper).isDeliveryModeApplicableForOrder(
                cartModel, expressDelivery);
        given(cartModel.getContainsPreOrderItems()).willReturn(Boolean.TRUE);
        final List<TargetZoneDeliveryModeData> deliveryModes = targetAbstractOrderHelper
                .getPhysicalDeliveryModes(cartModel);
        for (final TargetZoneDeliveryModeData targetZoneDeliveryModeData : deliveryModes) {
            assertThat(targetZoneDeliveryModeData.getPotentialIncentiveMessage())
                    .isNull();
            assertThat(targetZoneDeliveryModeData.getIncentiveMetMessage()).isNull();
        }
    }

    private AbstractTargetVariantProductModel createTargetColourVariantProductModel(final String code,
            final String prdTypeCode) {

        final AbstractTargetVariantProductModel colourVariantModel = Mockito
                .mock(TargetColourVariantProductModel.class);
        given(colourVariantModel.getCode()).willReturn(code);
        given(colourVariantModel.getBaseProduct()).willReturn(targetProductModel);
        given(productType.getCode()).willReturn(prdTypeCode);
        given(targetProductModel.getProductType()).willReturn(productType);
        return colourVariantModel;
    }

    private AbstractTargetVariantProductModel createTargetSizeVariantProductModel(final String code,
            final String prdCodeType) {
        final AbstractTargetVariantProductModel sizeVariantModel = Mockito
                .mock(TargetSizeVariantProductModel.class);
        sizeVariantModel.setCode(code);
        final AbstractTargetVariantProductModel colourVariant = createTargetColourVariantProductModel("colour",
                prdCodeType);
        sizeVariantModel.setBaseProduct(colourVariant);
        return sizeVariantModel;
    }

    private void setupDeliveryData() {
        targetZoneDeliveryModes = new ArrayList<>();
        homeDelivery = new TargetZoneDeliveryModeModel();
        homeDelivery.setCode(HOME_DELIVERY);
        homeDelivery.setDisplayOrder(Integer.valueOf(1));
        homeDelivery.setIsShortDeliveryTime(Boolean.FALSE);
        final TargetZoneDeliveryModeValueModel targetZoneDeliveryModeValue1 = new TargetZoneDeliveryModeValueModel();
        homeDelivery.setValues(Collections.singleton(((ZoneDeliveryModeValueModel)targetZoneDeliveryModeValue1)));
        targetZoneDeliveryModes.add(homeDelivery);
        cncDelivery = new TargetZoneDeliveryModeModel();
        cncDelivery.setCode(CNC_DELIVERY);
        cncDelivery.setDisplayOrder(Integer.valueOf(2));
        cncDelivery.setIsShortDeliveryTime(Boolean.FALSE);
        final TargetZoneDeliveryModeValueModel targetZoneDeliveryModeValue2 = new TargetZoneDeliveryModeValueModel();
        cncDelivery.setValues(Collections.singleton(((ZoneDeliveryModeValueModel)targetZoneDeliveryModeValue2)));
        targetZoneDeliveryModes.add(cncDelivery);
        expressDelivery = new TargetZoneDeliveryModeModel();
        expressDelivery.setCode(EXPRESS_DELIVERY);
        expressDelivery.setDisplayOrder(Integer.valueOf(3));
        expressDelivery.setIsShortDeliveryTime(Boolean.TRUE);
        expressDelivery.setAvailableForPreOrder(true);
        final TargetZoneDeliveryModeValueModel targetZoneDeliveryModeValue3 = new TargetZoneDeliveryModeValueModel();
        expressDelivery.setValues(Collections.singleton(((ZoneDeliveryModeValueModel)targetZoneDeliveryModeValue3)));
        targetZoneDeliveryModes.add(expressDelivery);

        given(targetZoneDeliveryModeConverter.convert(any(TargetZoneDeliveryModeModel.class))).willReturn(
                new TargetZoneDeliveryModeData());
    }

    private void mockExternalDeliveryServiceCalls() {
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        given(targetDeliveryService.getAllCurrentlyActiveDeliveryModes(SalesApplication.WEB)).willReturn(
                targetZoneDeliveryModes);
        given(
                targetDeliveryService.getAllApplicableDeliveryModesForOrder(cartModel, SalesApplication.CALLCENTER))
                        .willReturn(
                                targetZoneDeliveryModes);
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(homeDelivery, cartModel))
                .willReturn(
                        Collections.singletonList(modeValueModel));
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(cncDelivery, cartModel))
                .willReturn(
                        Collections.singletonList(modeValueModel));
        given(
                targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(expressDelivery, cartModel))
                        .willReturn(
                                Collections.singletonList(modeValueModel));
        given(
                targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrderWithoutOrderThreshold(homeDelivery,
                        cartModel))
                                .willReturn(
                                        Collections.singletonList(modeValueModel));
        given(
                targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrderWithoutOrderThreshold(cncDelivery,
                        cartModel))
                                .willReturn(
                                        Collections.singletonList(modeValueModel));
        given(
                targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrderWithoutOrderThreshold(
                        expressDelivery, cartModel))
                                .willReturn(
                                        Collections.singletonList(modeValueModel));
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndProduct(homeDelivery, productModel))
                .willReturn(
                        Collections.singletonList(modeValueModel));

    }
}

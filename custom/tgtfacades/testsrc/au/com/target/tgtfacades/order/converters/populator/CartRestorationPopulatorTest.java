package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.converters.populator.CartRestorationPopulator;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartRestoration;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;



/**
 * Unit test for {@link CartRestorationPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CartRestorationPopulatorTest {

    @InjectMocks
    private final CartRestorationPopulator cartRestorationPopulator = new CartRestorationPopulator();

    @Mock
    private Converter<CommerceCartModification, CartModificationData> cartModificationConverter;

    @Test
    public void testPopulateWithNullModel() {
        final CartRestorationData cartRestorationData = Mockito.mock(CartRestorationData.class);
        cartRestorationPopulator.populate(null, cartRestorationData);

        Mockito.verifyZeroInteractions(cartRestorationData);
        Mockito.verifyZeroInteractions(cartModificationConverter);
    }

    @Test(expected = NullPointerException.class)
    public void testPopulateWithNullModifications() {

        final CommerceCartRestoration cartRestoration = Mockito.mock(CommerceCartRestoration.class);
        BDDMockito.given(cartRestoration.getModifications()).willReturn(null);

        final CartRestorationData cartRestorationData = Mockito.mock(CartRestorationData.class);
        cartRestorationPopulator.populate(cartRestoration, cartRestorationData);

        Mockito.verifyZeroInteractions(cartRestorationData);
        Mockito.verifyZeroInteractions(cartModificationConverter);
    }

    @Test
    public void testPopulateWithEmptyModifications() {

        final CommerceCartRestoration cartRestoration = Mockito.mock(CommerceCartRestoration.class);
        BDDMockito.given(cartRestoration.getModifications()).willReturn(Collections.EMPTY_LIST);

        final CartRestorationData cartRestorationData = Mockito.mock(CartRestorationData.class);
        cartRestorationPopulator.populate(cartRestoration, cartRestorationData);

        Mockito.verifyZeroInteractions(cartModificationConverter);
    }

    @Test
    public void testPopulate() {

        final CommerceCartRestoration cartRestoration = Mockito.mock(CommerceCartRestoration.class);

        final CommerceCartModification cartModification1 = Mockito.mock(CommerceCartModification.class);
        final CommerceCartModification cartModification2 = Mockito.mock(CommerceCartModification.class);
        final CommerceCartModification cartModification3 = Mockito.mock(CommerceCartModification.class);

        BDDMockito.given(cartRestoration.getModifications()).willReturn(
                Arrays.asList(cartModification1, cartModification2, cartModification3));

        final CartRestorationData cartRestorationData = new CartRestorationData();
        cartRestorationPopulator.populate(cartRestoration, cartRestorationData);

        Mockito.verify(cartModificationConverter, Mockito.times(3))
                .convert(Mockito.any(CommerceCartModification.class));

        Assert.assertNotNull(cartRestorationData.getModifications());
        Assert.assertEquals(3, cartRestorationData.getModifications().size());
    }
}

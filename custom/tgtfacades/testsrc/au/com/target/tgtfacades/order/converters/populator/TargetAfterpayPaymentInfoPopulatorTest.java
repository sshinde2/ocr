/**
 * 
 */
package au.com.target.tgtfacades.order.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtpayment.model.AfterpayPaymentInfoModel;


/**
 * Unit tests for TargetAfterpayPaymentInfoPopulator
 * 
 * @author mgazal
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetAfterpayPaymentInfoPopulatorTest {

    @InjectMocks
    private final TargetAfterpayPaymentInfoPopulator populator = new TargetAfterpayPaymentInfoPopulator();

    @Test
    public void testPopulate() {
        final AfterpayPaymentInfoModel afterpayPaymentInfoModel = mock(AfterpayPaymentInfoModel.class);
        final PK pk = PK.parse("123");
        given(afterpayPaymentInfoModel.getPk()).willReturn(pk);

        final TargetCCPaymentInfoData paymentInfoData = new TargetCCPaymentInfoData();
        populator.populate(afterpayPaymentInfoModel, paymentInfoData);

        assertThat(paymentInfoData.isAfterpayPaymentInfo()).isTrue();
        assertThat(paymentInfoData.getId()).isEqualTo("123");
        assertThat(paymentInfoData.getAfterpayPaymentInfoData()).isNotNull();

    }
}
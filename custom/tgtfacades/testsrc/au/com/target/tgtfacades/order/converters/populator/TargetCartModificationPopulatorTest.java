/**
 * 
 */
package au.com.target.tgtfacades.order.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Vivek
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCartModificationPopulatorTest {

    @Mock
    private CommerceCartModification source;

    @Mock
    private AbstractOrderEntryModel entryModel;

    @Mock
    private AbstractOrderModel orderModel;

    @Mock
    private Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter;

    @InjectMocks
    private final TargetCartModificationPopulator populator = new TargetCartModificationPopulator();

    @Before
    public void setUp() {
        Mockito.when(source.getStatusCode()).thenReturn("mockStatusCode");
        Mockito.when(Long.valueOf(source.getQuantity())).thenReturn(Long.valueOf(10L));
        Mockito.when(Long.valueOf(source.getQuantityAdded())).thenReturn(Long.valueOf(2L));
        Mockito.when(source.getDeliveryModeChanged()).thenReturn(Boolean.FALSE);
    }

    @Test
    public void testPopulateWithEntryAndOrder() {
        final CartModificationData target = new CartModificationData();

        Mockito.when(source.getEntry()).thenReturn(entryModel);
        Mockito.when(entryModel.getOrder()).thenReturn(orderModel);
        Mockito.when(orderModel.getCode()).thenReturn("mockCartCode");

        populator.populate(source, target);

        assertCommonData(target);
        Assert.assertEquals(target.getCartCode(), "mockCartCode");
        Mockito.verify(orderEntryConverter).convert(entryModel);
    }

    @Test
    public void testPopulateWithEntryButNoOrder() {
        final CartModificationData target = new CartModificationData();

        Mockito.when(source.getEntry()).thenReturn(entryModel);
        Mockito.when(entryModel.getOrder()).thenReturn(null);

        populator.populate(source, target);

        assertCommonData(target);
        Assert.assertNull(target.getCartCode());
        Mockito.verify(orderEntryConverter).convert(entryModel);
    }

    @Test
    public void testPopulateWithNoEntry() {
        final CartModificationData target = new CartModificationData();

        Mockito.when(source.getEntry()).thenReturn(null);

        populator.populate(source, target);

        assertCommonData(target);
        Assert.assertNull(target.getCartCode());
        Mockito.verify(orderEntryConverter, Mockito.never()).convert(entryModel);
    }

    /**
     * @param target
     */
    private void assertCommonData(final CartModificationData target) {
        Assert.assertEquals(target.getStatusCode(), "mockStatusCode");
        Assert.assertEquals(Long.valueOf(target.getQuantity()), Long.valueOf(10L));
        Assert.assertEquals(Long.valueOf(target.getQuantityAdded()), Long.valueOf(2L));
        Assert.assertFalse(target.getDeliveryModeChanged().booleanValue());
    }
}

/**
 *
 */
package au.com.target.tgtfacades.order.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyCollection;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.order.data.PromotionOrderEntryConsumedData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PromotionResultData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.jalo.ProductPromotion;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.formatter.InvoiceFormatterHelper;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtfacades.checkout.flow.TargetCheckoutCustomerStrategy;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.order.converters.AbstractOrderHelper;
import au.com.target.tgtfacades.order.converters.TargetFlybuysDiscountConverter;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.order.data.TargetOrderEntryData;
import au.com.target.tgtfacades.util.TargetPriceHelper;
import junit.framework.Assert;


/**
 * @author rahul
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCartPopulatorTest {

    private static final String DISCLAIMER = "disclaimer";
    private static final String DIGITAL_DELIVERY = "digital-delivery";
    private static final String DIGITAL_DELIVERY2 = "digital-delivery2";

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @InjectMocks
    private final TargetCartPopulator targetCartPopulator = new TargetCartPopulator();

    @Mock
    private PriceDataFactory priceDataFactory;

    @Mock
    private PromotionsService promotionsService;

    @Mock
    private CartModel cartModel;

    @Mock
    private CurrencyModel currencyModel;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetFlybuysDiscountConverter targetFlybuysDiscountConverter;

    @Mock
    private TargetDeliveryService targetDeliveryService;

    @Mock
    private TargetCheckoutCustomerStrategy targetCheckoutCustomerStrategy;

    @Mock
    private AbstractOrderHelper abstractOrderHelper;

    @Mock
    private TargetSalesApplicationService targetSalesApplicationService;

    @Mock
    private Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter;

    @Mock
    private Converter<PromotionResultModel, PromotionResultData> promotionResultConverter;

    @Mock
    private Converter<TargetZoneDeliveryModeModel, TargetZoneDeliveryModeData> targetZoneDeliveryModeConverter;

    @Mock
    private TargetZoneDeliveryModeData targetZoneDeliveryModeData;

    @Mock
    private AbstractTargetZoneDeliveryModeValueModel targetZoneDeliveryModeValueModel;

    @Mock
    private AbstractTargetZoneDeliveryModeValueModel modeValueModel;

    @Mock
    private SessionService sessionService;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private Configuration configuration;

    @Mock
    private Converter<AddressModel, AddressData> addressConverter;

    @Mock
    private InvoiceFormatterHelper invoiceFormatterHelper;

    private TargetZoneDeliveryModeModel digitalDelivery;
    private TargetZoneDeliveryModeModel digitalDelivery2;

    @Mock
    private PriceData priceData;

    @Mock
    private TargetPriceHelper targetPriceHelper;

    @Before
    public void initData() {
        given(configurationService.getConfiguration()).willReturn(configuration);
        given(configuration.getString(Mockito.anyString())).willReturn("This is message");
        given(cartModel.getCurrency()).willReturn(currencyModel);
        digitalDelivery = new TargetZoneDeliveryModeModel();
        digitalDelivery.setCode(DIGITAL_DELIVERY);
        digitalDelivery.setIsDigital(Boolean.TRUE);

        digitalDelivery2 = new TargetZoneDeliveryModeModel();
        digitalDelivery2.setCode(DIGITAL_DELIVERY2);
        digitalDelivery2.setIsDigital(Boolean.TRUE);

    }

    @Test
    public void testAddPromotionsTMD() {
        final SessionContext sessioncontext = mock(SessionContext.class);
        final AbstractOrder abstractOrder = mock(AbstractOrder.class);
        final PromotionResult promotionResult = mock(PromotionResult.class);
        final PromotionOrderResults promotionOrderResults = new PromotionOrderResults(sessioncontext, abstractOrder,
                Collections.singletonList(promotionResult), 2.1);
        final DiscountValue discountValue = mock(DiscountValue.class);
        final AbstractOrderEntryModel abstractOrderEntryModel = mock(AbstractOrderEntryModel.class);
        final PromotionResultModel promotionResultModel = mock(PromotionResultModel.class);
        final List<PromotionResultModel> promotionResultModelList = new ArrayList<>();
        promotionResultModelList.add(promotionResultModel);
        final AbstractPromotionModel abstractPromotionModel = mock(AbstractPromotionModel.class);
        final PromotionOrderEntryConsumedModel promotionOrderEntryConsumedModel = mock(
                PromotionOrderEntryConsumedModel.class);
        final PromotionResultData promotionResultData = mock(PromotionResultData.class);
        final ProductPromotion promotion = mock(ProductPromotion.class);
        final JaloSession jaloSession = mock(JaloSession.class);

        targetCartPopulator.setModelService(modelService);

        given(abstractOrderEntryModel.getDiscountValues()).willReturn(Collections.singletonList(discountValue));
        given(cartModel.getEntries()).willReturn(Collections.singletonList(abstractOrderEntryModel));
        given(promotionsService.getPromotionResults(cartModel)).willReturn(promotionOrderResults);
        given(cartModel.getGlobalDiscountValues()).willReturn(Collections.singletonList(discountValue));
        given(Double.valueOf(discountValue.getAppliedValue())).willReturn(Double.valueOf(2.3));
        given(modelService.getAll(anyCollection(), anyCollection())).willReturn(
                promotionResultModelList);
        given(promotionResultModel.getPromotion()).willReturn(abstractPromotionModel);
        given(promotionResultModel.getConsumedEntries()).willReturn(
                Collections.singletonList(promotionOrderEntryConsumedModel));
        given(promotionResult.getSession()).willReturn(jaloSession);
        given(Boolean.valueOf(promotionResult.getFired())).willReturn(Boolean.TRUE);
        given(Boolean.valueOf(promotionResult.isApplied())).willReturn(Boolean.TRUE);
        given(promotionResult.getPromotion()).willReturn(promotion);
        given(modelService.get(promotionResult)).willReturn(promotionResultModel);
        given(promotionResultConverter.convert(promotionResultModel)).willReturn(promotionResultData);

        final TargetCartData orderData = new TargetCartData();
        targetCartPopulator.addPromotions(cartModel, orderData);
        Assert.assertEquals(promotionResultData, orderData.getAppliedProductPromotions().iterator().next());
    }

    @Test
    public void testPopulateDeliveryDataWithMultipleDigitalDeliveryMode() {

        List<DeliveryModeModel> lstdigitalDelivery = new ArrayList<>();
        lstdigitalDelivery.add(digitalDelivery);
        final AbstractOrderEntryModel entry1 = populateDummyCartEntry("ABC", lstdigitalDelivery);

        lstdigitalDelivery = new ArrayList<>();
        lstdigitalDelivery.add(digitalDelivery2);
        final AbstractOrderEntryModel entry2 = populateDummyCartEntry("ABC2", lstdigitalDelivery);

        final String testMsg = "test-message";
        final String testShortMsg = "test-short-message";

        given(modeValueModel.getShortMessage()).willReturn(testShortMsg);
        given(modeValueModel.getMessage()).willReturn(testMsg);
        given(modeValueModel.getDisclaimer()).willReturn(DISCLAIMER);

        BDDMockito.when(cartModel.getEntries()).thenReturn(Arrays.asList(entry1, entry2));

        final TargetCartData cartData = new TargetCartData();
        targetCartPopulator.populateAllDeliveryModes(cartModel, cartData);
        Assert.assertTrue(CollectionUtils.isEmpty(cartData.getDeliveryModes()));
        Assert.assertTrue(CollectionUtils.isNotEmpty(cartData.getDigitalDeliveryModes()));

        Assert.assertEquals(2, cartData.getDigitalDeliveryModes().size());

        for (final TargetZoneDeliveryModeData modeData : cartData.getDigitalDeliveryModes()) {
            Assert.assertEquals(testMsg, modeData.getLongDescription());
            Assert.assertEquals(testShortMsg, modeData.getShortDescription());
            Assert.assertEquals(testShortMsg, modeData.getShortDescription());
            Assert.assertEquals(DISCLAIMER, modeData.getDisclaimer());
        }
    }

    @Test
    public void testPopulateDeliveryDataWithDuplicateDigitalDeliveryMode() {

        List<DeliveryModeModel> lstdigitalDelivery = new ArrayList<>();
        lstdigitalDelivery.add(digitalDelivery);
        final AbstractOrderEntryModel entry1 = populateDummyCartEntry("ABC", lstdigitalDelivery);

        lstdigitalDelivery = new ArrayList<>();
        lstdigitalDelivery.add(digitalDelivery);
        final AbstractOrderEntryModel entry2 = populateDummyCartEntry("ABC2", lstdigitalDelivery);

        final String testMsg = "test-message";
        final String testShortMsg = "test-short-message";

        given(modeValueModel.getShortMessage()).willReturn(testShortMsg);
        given(modeValueModel.getMessage()).willReturn(testMsg);

        BDDMockito.when(cartModel.getEntries()).thenReturn(Arrays.asList(entry1, entry2));

        final TargetCartData cartData = new TargetCartData();
        targetCartPopulator.populateAllDeliveryModes(cartModel, cartData);
        Assert.assertTrue(CollectionUtils.isEmpty(cartData.getDeliveryModes()));
        Assert.assertTrue(CollectionUtils.isNotEmpty(cartData.getDigitalDeliveryModes()));

        Assert.assertEquals(1, cartData.getDigitalDeliveryModes().size());

        for (final TargetZoneDeliveryModeData modeData : cartData.getDigitalDeliveryModes()) {
            Assert.assertEquals(testMsg, modeData.getLongDescription());
            Assert.assertEquals(testShortMsg, modeData.getShortDescription());
        }
    }

    private AbstractOrderEntryModel populateDummyCartEntry(final String productCode,
            final List<DeliveryModeModel> deliveryModeModelListToSetup) {

        final AbstractOrderEntryModel abstractOrderEntryModel = new AbstractOrderEntryModel();
        final AbstractTargetVariantProductModel abstractTargetVariantProductModel = new AbstractTargetVariantProductModel();
        abstractTargetVariantProductModel.setCode(productCode);

        final Set<DeliveryModeModel> targetZoneDeliveryModeModelList = new HashSet<>();
        if (CollectionUtils.isNotEmpty(deliveryModeModelListToSetup)) {
            for (final DeliveryModeModel deliveryMode : deliveryModeModelListToSetup) {
                targetZoneDeliveryModeModelList.add(deliveryMode);
                BDDMockito.doReturn(Boolean.TRUE).when(
                        targetDeliveryModeHelper)
                        .isProductAvailableForDeliveryMode(abstractTargetVariantProductModel, deliveryMode);
                final TargetZoneDeliveryModeModel targetZonedeliveryMode = (TargetZoneDeliveryModeModel)deliveryMode;
                BDDMockito.given(targetDeliveryModeHelper.getDigitalDeliveryMode(abstractTargetVariantProductModel))
                        .willReturn(targetZonedeliveryMode);
                BDDMockito.when(targetZoneDeliveryModeConverter.convert(targetZonedeliveryMode)).thenReturn(
                        new TargetZoneDeliveryModeData());
                final ArrayList<AbstractTargetZoneDeliveryModeValueModel> entryModeValue = new ArrayList<>();
                entryModeValue.add(modeValueModel);
                BDDMockito.when(
                        targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrderWithoutOrderThreshold(
                                targetZonedeliveryMode, cartModel))
                        .thenReturn(
                                entryModeValue);
            }
        }


        abstractOrderEntryModel.setProduct(abstractTargetVariantProductModel);

        return abstractOrderEntryModel;
    }

    @Test
    public void testUpdateCartEntriesWithPromotionInformationWhenNoPromotions() {
        final CartData cartData = mock(CartData.class);
        given(cartData.getAppliedProductPromotions()).willReturn(null);

        final OrderEntryData orderEntry1 = new TargetOrderEntryData();
        orderEntry1.setEntryNumber(Integer.valueOf(1));
        final OrderEntryData orderEntry2 = new TargetOrderEntryData();
        orderEntry2.setEntryNumber(Integer.valueOf(2));
        final OrderEntryData orderEntry3 = new TargetOrderEntryData();
        orderEntry3.setEntryNumber(Integer.valueOf(3));

        final List<OrderEntryData> entries = new ArrayList<>(Arrays.asList(orderEntry1, orderEntry2, orderEntry3));
        given(cartData.getEntries()).willReturn(entries);

        targetCartPopulator.updateCartEntriesWithPromotionInformation(cartData);

        for (final OrderEntryData entry : cartData.getEntries()) {
            assertFalse(((TargetOrderEntryData)entry).isDealsApplied());
        }
    }

    @Test
    public void testUpdateCartEntriesWithPromotionInformationWhenNotTargetOrderEntryData() {
        final CartData cartData = mock(CartData.class);
        final PromotionResultData promotion = mock(PromotionResultData.class);
        final List<PromotionResultData> promotions = new ArrayList<>(Arrays.asList(promotion));
        given(cartData.getAppliedProductPromotions()).willReturn(promotions);

        final OrderEntryData orderEntry1 = mock(OrderEntryData.class);
        final OrderEntryData orderEntry2 = mock(OrderEntryData.class);
        final OrderEntryData orderEntry3 = mock(OrderEntryData.class);

        final List<OrderEntryData> entries = new ArrayList<>(Arrays.asList(orderEntry1, orderEntry2, orderEntry3));
        given(cartData.getEntries()).willReturn(entries);

        targetCartPopulator.updateCartEntriesWithPromotionInformation(cartData);

        for (final OrderEntryData entry : cartData.getEntries()) {
            verifyZeroInteractions(entry);
        }
    }

    @Test
    public void testUpdateCartEntriesWithPromotionInformationWithPromotionsHavingBlankDescription() {
        final CartData cartData = mock(CartData.class);
        final PromotionResultData promotion = mock(PromotionResultData.class);
        given(promotion.getOrderEntryNumber()).willReturn(Integer.valueOf(2));
        given(promotion.getDescription()).willReturn(null);
        final List<PromotionResultData> promotions = new ArrayList<>(Arrays.asList(promotion));
        given(cartData.getAppliedProductPromotions()).willReturn(promotions);

        final OrderEntryData orderEntry1 = new TargetOrderEntryData();
        orderEntry1.setEntryNumber(Integer.valueOf(1));
        final OrderEntryData orderEntry2 = new TargetOrderEntryData();
        orderEntry2.setEntryNumber(Integer.valueOf(2));
        final OrderEntryData orderEntry3 = new TargetOrderEntryData();
        orderEntry3.setEntryNumber(Integer.valueOf(3));

        final List<OrderEntryData> entries = new ArrayList<>(Arrays.asList(orderEntry1, orderEntry2, orderEntry3));
        given(cartData.getEntries()).willReturn(entries);

        targetCartPopulator.updateCartEntriesWithPromotionInformation(cartData);

        for (final OrderEntryData entry : cartData.getEntries()) {
            assertFalse(((TargetOrderEntryData)entry).isDealsApplied());
        }
    }

    @Test
    public void testUpdateCartEntriesWithPromotionInformationWithPromotionsHavingNoConsumedEntries() {
        final CartData cartData = mock(CartData.class);
        final PromotionResultData promotion = mock(PromotionResultData.class);
        given(promotion.getOrderEntryNumber()).willReturn(Integer.valueOf(2));
        given(promotion.getDescription()).willReturn("promotion description");
        given(promotion.getConsumedEntries()).willReturn(null);
        final List<PromotionResultData> promotions = new ArrayList<>(Arrays.asList(promotion));
        given(cartData.getAppliedProductPromotions()).willReturn(promotions);

        final OrderEntryData orderEntry1 = new TargetOrderEntryData();
        orderEntry1.setEntryNumber(Integer.valueOf(1));
        final OrderEntryData orderEntry2 = new TargetOrderEntryData();
        orderEntry2.setEntryNumber(Integer.valueOf(2));
        final OrderEntryData orderEntry3 = new TargetOrderEntryData();
        orderEntry3.setEntryNumber(Integer.valueOf(3));

        final List<OrderEntryData> entries = new ArrayList<>(Arrays.asList(orderEntry1, orderEntry2, orderEntry3));
        given(cartData.getEntries()).willReturn(entries);

        targetCartPopulator.updateCartEntriesWithPromotionInformation(cartData);

        for (final OrderEntryData entry : cartData.getEntries()) {
            assertFalse(((TargetOrderEntryData)entry).isDealsApplied());
        }
    }

    @Test
    public void testUpdateCartEntriesWithPromotionInformationWithPromotionsHavingUnmatchingConsumedEntries() {
        final CartData cartData = mock(CartData.class);
        final PromotionResultData promotion = mock(PromotionResultData.class);
        given(promotion.getOrderEntryNumber()).willReturn(Integer.valueOf(2));
        given(promotion.getDescription()).willReturn("promotion description");
        final PromotionOrderEntryConsumedData promotionOrderEntryConsumedData = mock(
                PromotionOrderEntryConsumedData.class);
        given(promotionOrderEntryConsumedData.getOrderEntryNumber()).willReturn(Integer.valueOf(5));
        final List<PromotionOrderEntryConsumedData> consumedEntries = new ArrayList<>(
                Arrays.asList(promotionOrderEntryConsumedData));
        given(promotion.getConsumedEntries()).willReturn(consumedEntries);
        final List<PromotionResultData> promotions = new ArrayList<>(Arrays.asList(promotion));
        given(cartData.getAppliedProductPromotions()).willReturn(promotions);

        final OrderEntryData orderEntry1 = new TargetOrderEntryData();
        orderEntry1.setEntryNumber(Integer.valueOf(1));
        final OrderEntryData orderEntry2 = new TargetOrderEntryData();
        orderEntry2.setEntryNumber(Integer.valueOf(2));
        final OrderEntryData orderEntry3 = new TargetOrderEntryData();
        orderEntry3.setEntryNumber(Integer.valueOf(3));

        final List<OrderEntryData> entries = new ArrayList<>(Arrays.asList(orderEntry1, orderEntry2, orderEntry3));
        given(cartData.getEntries()).willReturn(entries);

        targetCartPopulator.updateCartEntriesWithPromotionInformation(cartData);

        for (final OrderEntryData entry : cartData.getEntries()) {
            assertFalse(((TargetOrderEntryData)entry).isDealsApplied());
        }
    }

    @Test
    public void testUpdateCartEntriesWithPromotionInformationWithPromotionsHavingMatchingConsumedEntries() {
        final CartData cartData = mock(CartData.class);
        final PromotionResultData promotion = mock(PromotionResultData.class);
        given(promotion.getOrderEntryNumber()).willReturn(Integer.valueOf(2));
        given(promotion.getDescription()).willReturn("promotion description");
        final PromotionOrderEntryConsumedData promotionOrderEntryConsumedData = mock(
                PromotionOrderEntryConsumedData.class);
        given(promotionOrderEntryConsumedData.getOrderEntryNumber()).willReturn(Integer.valueOf(2));
        final List<PromotionOrderEntryConsumedData> consumedEntries = new ArrayList<>(
                Arrays.asList(promotionOrderEntryConsumedData));
        given(promotion.getConsumedEntries()).willReturn(consumedEntries);
        final List<PromotionResultData> promotions = new ArrayList<>(Arrays.asList(promotion));
        given(cartData.getAppliedProductPromotions()).willReturn(promotions);

        final OrderEntryData orderEntry1 = new TargetOrderEntryData();
        orderEntry1.setEntryNumber(Integer.valueOf(1));
        final OrderEntryData orderEntry2 = new TargetOrderEntryData();
        orderEntry2.setEntryNumber(Integer.valueOf(2));
        final OrderEntryData orderEntry3 = new TargetOrderEntryData();
        orderEntry3.setEntryNumber(Integer.valueOf(3));

        final List<OrderEntryData> entries = new ArrayList<>(Arrays.asList(orderEntry1, orderEntry2, orderEntry3));
        given(cartData.getEntries()).willReturn(entries);

        targetCartPopulator.updateCartEntriesWithPromotionInformation(cartData);

        assertFalse(((TargetOrderEntryData)entries.get(0)).isDealsApplied());
        assertTrue(((TargetOrderEntryData)entries.get(1)).isDealsApplied());
        assertFalse(((TargetOrderEntryData)entries.get(2)).isDealsApplied());
    }

    @Test
    public void testPopulateBillingAddress() {
        final AddressModel addressModel = mock(AddressModel.class);
        given(cartModel.getPaymentAddress()).willReturn(addressModel);

        final AddressData addressData = mock(AddressData.class);
        given(addressConverter.convert(addressModel)).willReturn(addressData);

        final AddressData addressDataReturned = targetCartPopulator.getBillingAddress(cartModel);

        assertThat(addressDataReturned).isNotNull();
        assertThat(addressDataReturned).isEqualTo(addressData);
    }

    @Test
    public void testPopulateMaskedFlybuysNumber() {
        final TargetCartData target = mock(TargetCartData.class);
        given(cartModel.getFlyBuysCode()).willReturn("flybuysCode");
        given(invoiceFormatterHelper.getMaskedFlybuysNumber("flybuysCode")).willReturn("maskedFlybuysNumber");
        targetCartPopulator.populate(cartModel, target);
        verify(target).setMaskedFlybuysNumber("maskedFlybuysNumber");
    }

    @Test
    public void testPopulateDeliveryDataWithDigitalDeliveryMode() {
        final List<DeliveryModeModel> lstdigitalDelivery = new ArrayList<>();
        lstdigitalDelivery.add(digitalDelivery);
        final AbstractOrderEntryModel entry = populateDummyCartEntry("digital-product", lstdigitalDelivery);

        final String testMsg = "test-message";
        final String testShortMsg = "test-short-message";
        given(modeValueModel.getShortMessage()).willReturn(testShortMsg);
        given(modeValueModel.getMessage()).willReturn(testMsg);

        when(cartModel.getEntries()).thenReturn(Arrays.asList(entry));

        given(cartModel.getDeliveryMode()).willReturn(digitalDelivery);
        final TargetCartData cartData = new TargetCartData();
        targetCartPopulator.populate(cartModel, cartData);
        assertThat(cartData.getDeliveryModes()).isEmpty();
        assertThat(cartData.getDigitalDeliveryModes()).isNotEmpty();
        assertThat(cartData.getDigitalDeliveryModes()).hasSize(1);
        assertThat(cartData.getDeliveryMode()).isNotNull();
        assertThat(cartData.getDigitalDeliveryModes().get(0).getLongDescription()).isEqualTo(testMsg);
        assertThat(cartData.getDigitalDeliveryModes().get(0).getShortDescription()).isEqualTo(testShortMsg);

    }

    /**
     * Method to verify pre-order deposit amount for the cart summary of pre-order product in the cart. Here cart
     * contians pre-order item and initial deposit amount is configured for the product.
     */
    @Test
    public void testPopulatePreOrderInfo() {
        final TargetCartData target = new TargetCartData();
        given(cartModel.getContainsPreOrderItems()).willReturn(Boolean.TRUE);
        given(cartModel.getPreOrderDepositAmount()).willReturn(Double.valueOf(10.0));
        given(cartModel.getTotalPrice()).willReturn(Double.valueOf(100.0));

        final PriceData depositPriceData = new PriceData();
        depositPriceData.setValue(BigDecimal.valueOf(10.0));

        final PriceData outstandingPriceData = new PriceData();
        outstandingPriceData.setValue(BigDecimal.valueOf(90.0));

        given(targetPriceHelper.createPriceData(cartModel.getPreOrderDepositAmount())).willReturn(depositPriceData);
        given(targetPriceHelper.createPriceData(Double.valueOf(90.0))).willReturn(outstandingPriceData);

        targetCartPopulator.populate(cartModel, target);
        assertThat(target.getPreOrderDepositAmount().getValue()).isNotNull().isEqualTo((BigDecimal.valueOf(10.0)));
        assertThat(target.getPreOrderOutstandingAmount().getValue()).isNotNull().isEqualTo((BigDecimal.valueOf(90.0)));
    }

    /**
     * Method to verify pre-order deposit amount for the cart summary of pre-order product in the cart. Here cart
     * contians pre-order item and initial deposit amount is not configured for the product(default to 0.0). In this
     * case no pre-order deposit value will be set.
     */
    @Test
    public void testPopulateIntialDepositAmountForPreOrderIfAmountNotConfigured() {
        final TargetCartData target = new TargetCartData();
        given(cartModel.getContainsPreOrderItems()).willReturn(Boolean.TRUE);
        final PriceData depositPriceData = new PriceData();
        depositPriceData.setValue(BigDecimal.valueOf(0d));
        given(targetPriceHelper.createPriceData(cartModel.getPreOrderDepositAmount())).willReturn(depositPriceData);
        targetCartPopulator.populate(cartModel, target);
        assertThat(target.getPreOrderDepositAmount().getValue()).isNotNull().isEqualTo((BigDecimal.valueOf(0.0)));
    }

    /**
     * Method to verify pre-order deposit amount for the cart summary of non pre-order(normal) product in the cart. Here
     * cart contains pre-order item. In this case initial deposit amount never set.
     */
    @Test
    public void testPopulateIntialDepositAmountForNonPreOrder() {
        final TargetCartData target = new TargetCartData();
        given(cartModel.getContainsPreOrderItems()).willReturn(Boolean.FALSE);
        targetCartPopulator.populate(cartModel, target);
        assertThat(target.getPreOrderDepositAmount()).isNull();
    }

    /**
     * Method to verify exclude for afterpay and zippay value of pre-order product in the cart. Here bakset contain pre-order item.
     */
    @Test
    public void testPopulateExcludeForAfterpayValueForPreOrder() {
        final TargetCartData target = new TargetCartData();
        given(cartModel.getContainsPreOrderItems()).willReturn(Boolean.TRUE);
        targetCartPopulator.populate(cartModel, target);
        assertThat(target.isExcludeForAfterpay()).isTrue();
        assertThat(target.isExcludeForZipPayment()).isTrue();
    }

    /**
     * Method to verify exclude for afterpay and zip value of pre-order product in the cart. Here bakset does not contain
     * pre-order item.
     */
    @Test
    public void testPopulateExcludeForAfterpayValueForNonPreOrder() {
        final TargetCartData target = new TargetCartData();
        given(cartModel.getContainsPreOrderItems()).willReturn(Boolean.FALSE);
        targetCartPopulator.populate(cartModel, target);
        assertThat(target.isExcludeForAfterpay()).isFalse();
        assertThat(target.isExcludeForZipPayment()).isFalse();
    }
}
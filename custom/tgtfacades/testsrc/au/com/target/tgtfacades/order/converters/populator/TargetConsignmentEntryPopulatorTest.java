/**
 * 
 */
package au.com.target.tgtfacades.order.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.order.data.TargetConsignmentEntryData;


/**
 * Unit test class for TargetConsignmentEntryPopulator
 * 
 * @author cbi
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetConsignmentEntryPopulatorTest {

    @InjectMocks
    private final TargetConsignmentEntryPopulator targetConsignmentEntryPopulator = new TargetConsignmentEntryPopulator();

    @Mock
    private ConsignmentEntryModel consignmentEntryModel;

    @Mock
    private PriceDataFactory priceDataFactory;

    @Mock
    private Converter<AbstractOrderEntryModel, OrderEntryData> consignmentConverter;

    @Test
    public void testSetConsignmentShippedPrice() {
        final TargetConsignmentEntryData targetConsignmentEntryData = mock(TargetConsignmentEntryData.class);
        final ConsignmentModel consignmentModel = mock(ConsignmentModel.class);
        final AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
        final CurrencyModel currencyModel = mock(CurrencyModel.class);
        final OrderEntryData orderEntryData = mock(OrderEntryData.class);
        final PriceData basePrice = mock(PriceData.class);
        final BigDecimal basePriceValue = mock(BigDecimal.class);
        given(consignmentEntryModel.getConsignment()).willReturn(consignmentModel);
        given(consignmentModel.getOrder()).willReturn(abstractOrderModel);
        given(abstractOrderModel.getCurrency()).willReturn(currencyModel);
        given(targetConsignmentEntryData.getOrderEntry()).willReturn(orderEntryData);
        given(orderEntryData.getBasePrice()).willReturn(basePrice);
        given(basePrice.getValue()).willReturn(basePriceValue);
        given(consignmentEntryModel.getShippedQuantity()).willReturn(Long.valueOf(2));
        given(consignmentEntryModel.getQuantity()).willReturn(Long.valueOf(4));
        targetConsignmentEntryPopulator.populate(consignmentEntryModel, targetConsignmentEntryData);
        verify(priceDataFactory, times(2)).create(PriceDataType.BUY, null, currencyModel);
    }

    @Test
    public void testOrderEntryIsNull() {
        final TargetConsignmentEntryData targetConsignmentEntryData = new TargetConsignmentEntryData();
        final ConsignmentModel consignmentModel = mock(ConsignmentModel.class);
        final AbstractOrderModel abstractOrderModel = mock(AbstractOrderModel.class);
        given(consignmentEntryModel.getConsignment()).willReturn(consignmentModel);
        given(consignmentModel.getOrder()).willReturn(abstractOrderModel);
        targetConsignmentEntryPopulator.populate(consignmentEntryModel, targetConsignmentEntryData);
        assertThat(targetConsignmentEntryData.getTotalShippedPrice()).isNull();
    }



}
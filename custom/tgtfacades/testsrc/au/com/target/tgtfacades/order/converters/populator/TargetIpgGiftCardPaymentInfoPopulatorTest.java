/**
 * 
 */
package au.com.target.tgtfacades.order.converters.populator;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CardTypeData;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.payment.IpgGiftCardPaymentInfoModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetIpgGiftCardPaymentInfoPopulatorTest {

    @Mock
    private Converter<CreditCardType, CardTypeData> cardTypeConverter;

    @InjectMocks
    private final TargetIpgGiftCardPaymentInfoPopulator populator = new TargetIpgGiftCardPaymentInfoPopulator();

    @Test
    public void testPopulate() {
        final IpgGiftCardPaymentInfoModel ipgGiftCardPaymentInforModel = mock(IpgGiftCardPaymentInfoModel.class);
        final TargetCCPaymentInfoData paymentInfoData = new TargetCCPaymentInfoData();

        final PK pk = PK.parse("123");
        final String cardNumber = "424242******4242";
        final CreditCardType cardType = CreditCardType.GIFTCARD;
        final CardTypeData cardTypeData = new CardTypeData();
        cardTypeData.setCode(cardType.getCode());

        given(ipgGiftCardPaymentInforModel.getPk()).willReturn(pk);
        given(ipgGiftCardPaymentInforModel.getMaskedNumber()).willReturn(cardNumber);
        given(ipgGiftCardPaymentInforModel.getCardType()).willReturn(cardType);
        given(cardTypeConverter.convert(cardType)).willReturn(cardTypeData);

        populator.populate(ipgGiftCardPaymentInforModel, paymentInfoData);

        assertEquals(pk.toString(), paymentInfoData.getId());
        assertEquals(cardNumber, paymentInfoData.getCardNumber());
        assertEquals(cardType.getCode(), paymentInfoData.getCardType());
        assertEquals(cardTypeData, paymentInfoData.getCardTypeData());
    }
}

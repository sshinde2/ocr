/**
 * 
 */
package au.com.target.tgtfacades.order.converters.populator;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.order.data.TargetOrderData;


/**
 * Unit test class for TargetOrderConsignmentPopulator
 * 
 * @author Pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetOrderConsignmentPopulatorTest {

    @InjectMocks
    private final TargetOrderConsignmentPopulator targetOrderConsignmentPopulator = new TargetOrderConsignmentPopulator();

    @Mock
    private OrderModel orderModel;

    @Mock
    private Converter<ConsignmentModel, ConsignmentData> consignmentConverter;


    private final TargetOrderData targetOrderData = new TargetOrderData();

    @Before
    public void initData() {
        targetOrderConsignmentPopulator.setConsignmentConverter(consignmentConverter);
    }

    /**
     * Method to test populator when status of consignment model is 'PICKED'
     */
    @Test
    public void testConverterConsignment() {
        final ConsignmentEntryModel mockConsignmentEntryModel = Mockito.mock(ConsignmentEntryModel.class);

        final ConsignmentModel consignmentModel = BDDMockito.mock(ConsignmentModel.class);
        BDDMockito.given(consignmentModel.getConsignmentEntries()).willReturn(
                Collections.singleton(mockConsignmentEntryModel));
        BDDMockito.given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);

        final Set<ConsignmentModel> consignments = new HashSet<>();
        consignments.add(consignmentModel);

        Mockito.when(orderModel.getConsignments()).thenReturn(consignments);

        final ConsignmentData consignmentData = new ConsignmentData();
        given(consignmentConverter.convert(consignmentModel)).willReturn(consignmentData);

        targetOrderConsignmentPopulator.populate(orderModel, targetOrderData);

        Assert.assertEquals(1, targetOrderData.getConsignment().size());
        Assert.assertEquals(consignmentData, targetOrderData.getConsignment().iterator().next());
    }

    /**
     * Method to test populator when status of consignment model is 'CANCELLED'
     */
    @Test
    public void testConverterConsignmentCancelled() {
        final ConsignmentEntryModel mockConsignmentEntryModel = Mockito.mock(ConsignmentEntryModel.class);

        final ConsignmentModel consignmentModel = BDDMockito.mock(ConsignmentModel.class);
        BDDMockito.given(consignmentModel.getConsignmentEntries()).willReturn(
                Collections.singleton(mockConsignmentEntryModel));
        BDDMockito.given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.CANCELLED);

        final Set<ConsignmentModel> consignments = new HashSet<>();
        consignments.add(consignmentModel);

        Mockito.when(orderModel.getConsignments()).thenReturn(consignments);

        final ConsignmentData consignmentData = new ConsignmentData();
        given(consignmentConverter.convert(consignmentModel)).willReturn(consignmentData);

        targetOrderConsignmentPopulator.populate(orderModel, targetOrderData);

        Assert.assertNull(targetOrderData.getConsignment());
    }

}

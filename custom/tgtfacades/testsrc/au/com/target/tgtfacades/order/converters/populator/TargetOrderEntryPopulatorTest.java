/**
 *
 */
package au.com.target.tgtfacades.order.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.strategies.ModifiableChecker;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import au.com.target.tgtcore.model.GiftRecipientModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfacades.giftcards.converters.GiftRecipientConverter;
import au.com.target.tgtfacades.order.data.GiftRecipientData;
import au.com.target.tgtfacades.order.data.TargetOrderEntryData;
import au.com.target.tgtmarketing.model.AffiliateOfferCategoryModel;


/**
 * @author asingh78
 *
 */
@UnitTest
public class TargetOrderEntryPopulatorTest {

    @InjectMocks
    @Spy
    private final TargetOrderEntryPopulator targetOrderEntryPopulator = new TargetOrderEntryPopulator();


    @Mock
    private PriceDataFactory priceDataFactory;

    @Mock
    private OrderEntryModel orderEntryModel;

    @Mock
    private OrderModel orderModel;

    @Mock
    private CurrencyModel currencyModel;

    @Mock
    private ModifiableChecker modifiableChecker;

    @Mock
    private GiftRecipientModel giftRecipientModel;

    @Mock
    private GiftRecipientData giftRecipientData;

    @Mock
    private Converter<ProductModel, ProductData> productConverter;

    @Mock
    private TargetMerchDepartmentModel merchDept;

    @Mock
    private GiftRecipientConverter giftRecipientModelToRecipientDataConverter;

    @Before
    public void initData() {
        MockitoAnnotations.initMocks(this);
        given(orderEntryModel.getOrder()).willReturn(orderModel);
        given(orderModel.getCurrency()).willReturn(currencyModel);
        given(currencyModel.getIsocode()).willReturn("AUD");
        given(orderEntryModel.getBasePrice()).willReturn(Double.valueOf(20.0));
        given(Boolean.valueOf(modifiableChecker.canModify(orderEntryModel))).willReturn(Boolean.TRUE);
        given(productConverter.convert(Mockito.any(ProductModel.class))).willReturn(new ProductData());
    }

    @Test
    public void addTotalsWithOrderEntryData() {

        // Given a total and base price on the orderEntryModel
        given(orderEntryModel.getTotalPrice()).willReturn(Double.valueOf(20.0));
        given(orderEntryModel.getBasePrice()).willReturn(Double.valueOf(10.0));
        final PriceData basePriceData = new PriceData();
        final PriceData totalPriceData = new PriceData();
        given(
                priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(10.0),
                        orderEntryModel.getOrder().getCurrency())).willReturn(basePriceData);
        given(
                priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(20.0),
                        orderEntryModel.getOrder().getCurrency())).willReturn(totalPriceData);
        // given call addTotals
        final OrderEntryData orderEntryData = new OrderEntryData();
        targetOrderEntryPopulator.addTotals(orderEntryModel, orderEntryData);

        // Expect the orderEntryData to be populated
        assertThat(orderEntryData.getBasePrice()).isEqualTo(basePriceData);
        assertThat(orderEntryData.getTotalPrice()).isEqualTo(totalPriceData);
    }


    @SuppressWarnings("boxing")
    @Test
    public void addTotalsWithTargetOrderEntryData() {

        // Given a total and base price on the orderEntryModel
        given(orderEntryModel.getTotalPrice()).willReturn(Double.valueOf(30.0));
        given(orderEntryModel.getBasePrice()).willReturn(Double.valueOf(20.0));
        // And a discount value
        final DiscountValue discountValue = Mockito.mock(DiscountValue.class);
        given(orderEntryModel.getDiscountValues()).willReturn(Collections.singletonList(discountValue));
        given(discountValue.getAppliedValue()).willReturn(Double.valueOf(10.0));

        // And prices returned by the priceDataFactory
        final PriceData basePriceData = new PriceData();
        final PriceData totalPriceData = new PriceData();
        final PriceData discountPriceData = new PriceData();
        final PriceData totalItemPriceData = new PriceData();

        given(
                priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(20.0),
                        orderEntryModel.getOrder().getCurrency())).willReturn(basePriceData);
        given(
                priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(30.0),
                        orderEntryModel.getOrder().getCurrency())).willReturn(totalPriceData);
        given(
                priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(0.0),
                        orderEntryModel.getOrder().getCurrency())).willReturn(discountPriceData);
        given(
                priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(40.0),
                        orderEntryModel.getOrder().getCurrency())).willReturn(totalItemPriceData);

        // given call addTotals with instance of TargetOrderEntryData
        final TargetOrderEntryData orderEntryData = new TargetOrderEntryData();
        targetOrderEntryPopulator.addTotals(orderEntryModel, orderEntryData);

        // Expect the orderEntryData to be populated with the same prices
        assertThat(orderEntryData.getBasePrice().getValue()).isEqualTo(basePriceData.getValue());
        assertThat(orderEntryData.getTotalPrice().getValue()).isEqualTo(totalPriceData.getValue());
        assertThat(orderEntryData.getTotalItemPrice().getValue()).isEqualTo(totalItemPriceData.getValue());

    }

    @SuppressWarnings("boxing")
    @Test
    public void addTotalsWithTargetOrderEntryDataZeroDiscount() {

        // Given a total and base price on the orderEntryModel
        given(orderEntryModel.getTotalPrice()).willReturn(Double.valueOf(30.0));
        given(orderEntryModel.getBasePrice()).willReturn(Double.valueOf(20.0));
        // And a discount value
        final DiscountValue discountValue = Mockito.mock(DiscountValue.class);
        given(orderEntryModel.getDiscountValues()).willReturn(Collections.singletonList(discountValue));
        given(discountValue.getAppliedValue()).willReturn(Double.valueOf(0.0));

        // And prices returned by the priceDataFactory
        final PriceData basePriceData = new PriceData();
        final PriceData totalPriceData = new PriceData();
        final PriceData discountPriceData = new PriceData();
        final PriceData totalItemPriceData = new PriceData();

        given(
                priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(20.0),
                        orderEntryModel.getOrder().getCurrency())).willReturn(basePriceData);
        given(
                priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(30.0),
                        orderEntryModel.getOrder().getCurrency())).willReturn(totalPriceData);
        given(
                priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(0.0),
                        orderEntryModel.getOrder().getCurrency())).willReturn(discountPriceData);
        given(
                priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(30.0),
                        orderEntryModel.getOrder().getCurrency())).willReturn(totalItemPriceData);

        // given call addTotals with instance of TargetOrderEntryData
        final TargetOrderEntryData orderEntryData = new TargetOrderEntryData();
        targetOrderEntryPopulator.addTotals(orderEntryModel, orderEntryData);

        // Expect the orderEntryData to be populated with the same prices
        assertThat(orderEntryData.getBasePrice().getValue()).isEqualTo(basePriceData.getValue());
        assertThat(orderEntryData.getTotalPrice().getValue()).isEqualTo(totalPriceData.getValue());
        assertThat(orderEntryData.getDiscountPrice()).isNull();
        assertThat(orderEntryData.getTotalItemPrice().getValue()).isEqualTo(totalItemPriceData.getValue());
    }

    @Test
    public void addTotalsIfTotalPriceIsNull() {

        // Given null orderEntryModel.getTotalPrice
        given(orderEntryModel.getTotalPrice()).willReturn(null);
        final PriceData priceData = new PriceData();
        given(
                priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(0.0),
                        orderEntryModel.getOrder().getCurrency())).willReturn(priceData);

        // given call addTotals
        final OrderEntryData orderEntryData = new OrderEntryData();
        targetOrderEntryPopulator.addTotals(orderEntryModel, orderEntryData);

        // Expect zero total price
        assertThat(orderEntryData.getTotalPrice()).isEqualTo(priceData);
    }

    @Test
    public void testpopulateDepartmentgivenProductInOrderEntryIsNull() {
        given(orderEntryModel.getProduct()).willReturn(null);
        final TargetOrderEntryData orderEntryData = new TargetOrderEntryData();
        targetOrderEntryPopulator.populate(orderEntryModel, orderEntryData);
        verify(targetOrderEntryPopulator).addTotals(orderEntryModel, orderEntryData);
        assertThat(orderEntryData).isNotNull();
        assertThat(orderEntryData.getDepartmentName()).isNull();
        assertThat(orderEntryData.getAffiliateOfferCategory()).isNull();
    }

    @Test
    public void testpopulateDepartmentgivenProductInOrderEntryIsProductModel() {
        given(orderEntryModel.getProduct()).willReturn(new ProductModel());
        final TargetOrderEntryData orderEntryData = new TargetOrderEntryData();
        targetOrderEntryPopulator.populate(orderEntryModel, orderEntryData);
        verify(targetOrderEntryPopulator).addTotals(orderEntryModel, orderEntryData);
        assertThat(orderEntryData).isNotNull();
        assertThat(orderEntryData.getDepartmentName()).isNull();
        assertThat(orderEntryData.getAffiliateOfferCategory()).isNull();
    }

    @Test
    public void testpopulateDepartmentgivenProductInOrderEntryIsTargetProductModelNullDept() {
        given(orderEntryModel.getProduct()).willReturn(new TargetProductModel());
        final TargetOrderEntryData orderEntryData = new TargetOrderEntryData();
        targetOrderEntryPopulator.populate(orderEntryModel, orderEntryData);
        verify(targetOrderEntryPopulator).addTotals(orderEntryModel, orderEntryData);
        assertThat(orderEntryData).isNotNull();
        assertThat(orderEntryData.getDepartmentName()).isNull();
        assertThat(orderEntryData.getAffiliateOfferCategory()).isNull();
    }

    @Test
    public void testpopulateDepartmentgivenProductInOrderEntryIsTargetSizeVariantModel() {
        final TargetSizeVariantProductModel sizeVariant = new TargetSizeVariantProductModel();
        sizeVariant.setBaseProduct(buildVariantWithDeptAndAffiliateCategory());
        given(orderEntryModel.getProduct()).willReturn(sizeVariant);
        final TargetOrderEntryData orderEntryData = new TargetOrderEntryData();
        targetOrderEntryPopulator.populate(orderEntryModel, orderEntryData);
        verify(targetOrderEntryPopulator).addTotals(orderEntryModel, orderEntryData);
        assertThat(orderEntryData).isNotNull();
        assertThat(orderEntryData.getDepartmentName()).isNotNull();
        assertThat(orderEntryData.getDepartmentName()).isEqualTo("Women");
        assertThat(orderEntryData.getAffiliateOfferCategory()).isNotNull();
        assertThat(orderEntryData.getAffiliateOfferCategory()).isEqualTo("Apparel");
    }

    @Test
    public void testpopulateDepartmentgivenProductInOrderEntryHasMerchDeptWithNoAffiliateCategory() {
        final TargetSizeVariantProductModel sizeVariant = new TargetSizeVariantProductModel();
        final TargetProductModel baseProduct = new TargetProductModel();
        given(merchDept.getName()).willReturn("Women");
        given(merchDept.getCode()).willReturn("355");
        given(merchDept.getSupercategories()).willReturn(new ArrayList());
        baseProduct.setMerchDepartment(merchDept);
        sizeVariant.setBaseProduct(baseProduct);
        given(orderEntryModel.getProduct()).willReturn(sizeVariant);
        final TargetOrderEntryData orderEntryData = new TargetOrderEntryData();
        targetOrderEntryPopulator.populate(orderEntryModel, orderEntryData);
        verify(targetOrderEntryPopulator).addTotals(orderEntryModel, orderEntryData);
        assertThat(orderEntryData).isNotNull();
        assertThat(orderEntryData.getDepartmentName()).isNotNull();
        assertThat(orderEntryData.getDepartmentName()).isEqualTo("Women");
        assertThat(orderEntryData.getAffiliateOfferCategory()).isNull();
        assertThat(orderEntryData.getDepartmentCode()).isNotNull();
        assertThat(orderEntryData.getDepartmentCode()).isEqualTo("355");
    }

    @Test
    public void testpopulateDepartmentgivenProductInOrderEntryHasMerchDeptWithOtherCategories() {
        final TargetSizeVariantProductModel sizeVariant = new TargetSizeVariantProductModel();
        final TargetProductModel baseProduct = new TargetProductModel();
        given(merchDept.getName()).willReturn("Women");
        given(merchDept.getCode()).willReturn("355");
        final CategoryModel categoryModel = new TargetProductCategoryModel();
        given(merchDept.getSupercategories()).willReturn(Collections.singletonList(categoryModel));
        baseProduct.setMerchDepartment(merchDept);
        sizeVariant.setBaseProduct(baseProduct);
        given(orderEntryModel.getProduct()).willReturn(sizeVariant);
        final TargetOrderEntryData orderEntryData = new TargetOrderEntryData();
        targetOrderEntryPopulator.populate(orderEntryModel, orderEntryData);
        verify(targetOrderEntryPopulator).addTotals(orderEntryModel, orderEntryData);
        assertThat(orderEntryData).isNotNull();
        assertThat(orderEntryData.getDepartmentName()).isNotNull();
        assertThat(orderEntryData.getDepartmentName()).isEqualTo("Women");
        assertThat(orderEntryData.getDepartmentCode()).isNotNull();
        assertThat(orderEntryData.getDepartmentCode()).isEqualTo("355");
        assertThat(orderEntryData.getAffiliateOfferCategory()).isNull();
    }

    @Test
    public void testPopulateGiftRecipientDetails() {
        final List<GiftRecipientModel> recipientList = new ArrayList<>();
        final TargetOrderEntryData oed = new TargetOrderEntryData();
        recipientList.add(giftRecipientModel);
        given(orderEntryModel.getGiftRecipients()).willReturn(recipientList);
        given(giftRecipientModelToRecipientDataConverter.convert(giftRecipientModel)).willReturn(
                getRecipientDataWithValues());
        targetOrderEntryPopulator.populateGiftRecipientDetails(orderEntryModel, oed);
        validateRecipientDetails(oed);
    }

    private GiftRecipientData getRecipientDataWithValues() {
        final GiftRecipientData model = new GiftRecipientData();
        model.setFirstName("GiftCardFirstName");
        model.setLastName("GiftCardLastName");
        model.setMessageText("GiftCardMessage");
        model.setRecipientEmailAddress("test@email.com");
        return model;
    }

    private void validateRecipientDetails(final TargetOrderEntryData oed) {
        assertThat(oed).isNotNull();
        assertThat(oed.getRecipients().size()).isEqualTo(1);
        assertThat(oed.getRecipients().get(0).getFirstName()).isEqualTo("GiftCardFirstName");
        assertThat(oed.getRecipients().get(0).getLastName()).isEqualTo("GiftCardLastName");
        assertThat(oed.getRecipients().get(0).getMessageText()).isEqualTo("GiftCardMessage");
        assertThat(oed.getRecipients().get(0).getRecipientEmailAddress()).isEqualTo("test@email.com");
    }

    /**
     * 
     */
    protected TargetProductModel buildVariantWithDeptAndAffiliateCategory() {
        final TargetProductModel baseProduct = new TargetProductModel();
        given(merchDept.getName()).willReturn("Women");
        final CategoryModel affiliateModel = new AffiliateOfferCategoryModel();
        affiliateModel.setCode("Apparel");
        given(merchDept.getSupercategories()).willReturn(Collections.singletonList(affiliateModel));
        baseProduct.setMerchDepartment(merchDept);
        return baseProduct;
    }
}

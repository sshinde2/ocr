/**
 *
 */
package au.com.target.tgtfacades.order.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartRestoration;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantTypeModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.cart.exception.ProductNotFoundCommerceCartModificationException;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.giftcards.GiftCardService;
import au.com.target.tgtcore.giftcards.GiftRecipientService;
import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;
import au.com.target.tgtcore.model.PostCodeGroupModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.model.TargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.postcode.service.TargetPostCodeService;
import au.com.target.tgtcore.product.exception.ProductNotFoundException;
import au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade;
import au.com.target.tgtfacades.giftcards.exceptions.GiftCardValidationException;
import au.com.target.tgtfacades.giftcards.validate.BaseGiftCardValidator;
import au.com.target.tgtfacades.giftcards.validate.GiftCardValidationErrorType;
import au.com.target.tgtfacades.giftcards.validate.MaxGiftCardQtyValidator;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.data.DeliveryModeData;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtlayby.cart.data.DeliveryModeInformation;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


/**
 * @author rmcalave
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCartFacadeImplTest {

    private static final String PK_STR = "12345678";

    private static final String DUMMY_GUID = "dummyGuid";

    @Mock
    private CartModel cartModel;

    @Mock
    private UnitModel unitModelMock;

    @Mock
    private BaseSiteModel baseSiteModel;

    @Mock
    private ProductModel product;

    @Mock
    private TargetZoneDeliveryModeModel mockZoneDeliveryMode;

    @Mock
    private DeliveryModeModel mockDeliveryMode;

    @Mock
    private PurchaseOptionModel mockPurchaseOption;

    @Mock
    private TargetZoneDeliveryModeValueModel mockZoneDeliveryModeValue;

    @Mock
    private TargetCustomerModel customerModel;

    @Mock
    private AddressModel addressModel;

    @Mock
    private OrderModel orderModel;

    @Mock
    private CustomerModel customer;

    @Mock
    private VariantTypeModel variantType;

    @Mock
    private VariantProductModel variantProductModel;

    @Mock
    private PriceDataFactory mockPriceDataFactory;

    @Mock
    private Configuration configuration;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Mock
    private DeliveryModeInformation mockDeliveryModeInformation;

    @Mock
    private PriceValue mockPriceValue;

    @Mock
    private PriceData priceData;

    @Mock
    private AddressData addressData;

    @Mock
    private CartData mockCartData;

    @Mock
    private CartModificationData cartModificationDataMock;

    @Mock
    private CommerceCartModification cartModificationMock;

    @Mock
    private Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter;

    @Mock
    private Converter<CommerceCartModification, CartModificationData> cartModificationConverter;

    @Mock
    private TargetPurchaseOptionHelper mockTargetPurchaseOptionHelper;

    @Mock
    private TargetCommerceCartService mockTargetCommerceCartService;

    @Mock
    private TargetDeliveryService mockTargetDeliveryService;

    @Mock
    private CartService cartService;

    @Mock
    private UserService mockUserService;

    @Mock
    private TargetLaybyCartService targetLaybyCartService;

    @Mock
    private BaseSiteService mockBaseSiteService;

    @Mock
    private ProductService productService;

    @Mock
    private FlybuysDiscountFacade flybuysDiscountFacade;

    @Mock
    private TargetOrderService targetOrderService;

    @Mock
    private TargetPostCodeService targetPostCodeService;

    @Mock
    private ModelService modelService;

    @Mock
    private GiftRecipientService giftRecipientService;

    @Mock
    private GiftCardService giftCardService;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private SessionService sessionService;
    
    @Mock
    private TargetUserFacade targetUserFacade;

    @Mock
    private TargetCheckoutFacade checkoutFacade;

    @InjectMocks
    @Spy
    private final TargetCartFacadeImpl targetCartFacade = new TargetCartFacadeImpl();

    @Test
    public void testGetDeliveryModeOptionInformation() {
        final String purchaseOptionCode = "purchaseOptionCode";
        final String deliveryModeCode = "deliveryModeCode";
        final double deliveryPrice = 9.0d;
        final String currencyIso = "AU";

        given(mockTargetPurchaseOptionHelper.getPurchaseOptionModel(purchaseOptionCode)).willReturn(
                mockPurchaseOption);
        given(cartService.getSessionCart()).willReturn(cartModel);
        given(mockTargetDeliveryService.getDeliveryModeForCode(deliveryModeCode)).willReturn(
                mockDeliveryMode);
        given(mockZoneDeliveryModeValue.getMessage()).willReturn("message");
        given(mockZoneDeliveryModeValue.getDisclaimer()).willReturn("disclaimer");
        given(mockDeliveryModeInformation.getDeliveryMode()).willReturn(mockZoneDeliveryMode);
        given(mockDeliveryModeInformation.getDeliveryModeValue()).willReturn(mockZoneDeliveryModeValue);
        given(Double.valueOf(mockPriceValue.getValue())).willReturn(Double.valueOf(deliveryPrice));
        given(mockPriceValue.getCurrencyIso()).willReturn(currencyIso);
        given(mockDeliveryModeInformation.getDeliveryFee()).willReturn(mockPriceValue);
        given(mockTargetCommerceCartService.getDeliveryModeOptionInformation(cartModel, mockDeliveryMode))
                .willReturn(mockDeliveryModeInformation);
        given(mockPriceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(deliveryPrice),
                currencyIso)).willReturn(priceData);

        final CartEntryModel mockOrderEntry = mock(CartEntryModel.class);
        final OrderEntryData mockOrderEntryData = mock(OrderEntryData.class);

        given(orderEntryConverter.convert(mockOrderEntry)).willReturn(mockOrderEntryData);
        given(mockPriceDataFactory.create(PriceDataType.BUY,
                BigDecimal.valueOf(mockPriceValue.getValue()), mockPriceValue.getCurrencyIso().toString()))
                        .willReturn(priceData);

        final DeliveryModeData result = targetCartFacade.getDeliveryModeOptionInformation(deliveryModeCode);

        assertThat(result).isNotNull();
        assertThat(priceData).isEqualTo(result.getDeliveryFee());
    }

    @Test
    public void testGetDeliveryModeOptionInformationWithNoFeeAndNoExcludedEntries() {
        final String purchaseOptionCode = "purchaseOptionCode";
        final String deliveryModeCode = "deliveryModeCode";

        given(mockTargetPurchaseOptionHelper.getPurchaseOptionModel(purchaseOptionCode)).willReturn(
                mockPurchaseOption);
        given(cartService.getSessionCart()).willReturn(cartModel);
        given(mockTargetDeliveryService.getDeliveryModeForCode(deliveryModeCode)).willReturn(
                mockDeliveryMode);
        given(mockZoneDeliveryModeValue.getMessage()).willReturn("message");
        given(mockZoneDeliveryModeValue.getDisclaimer()).willReturn("disclaimer");
        given(mockDeliveryModeInformation.getDeliveryMode()).willReturn(mockZoneDeliveryMode);
        given(mockDeliveryModeInformation.getDeliveryModeValue()).willReturn(mockZoneDeliveryModeValue);
        given(mockTargetCommerceCartService.getDeliveryModeOptionInformation(cartModel, mockDeliveryMode))
                        .willReturn(mockDeliveryModeInformation);

        final DeliveryModeData result = targetCartFacade.getDeliveryModeOptionInformation(deliveryModeCode);

        assertThat(result).isNotNull();
        assertThat(result.getDeliveryFee()).isNull();
    }

    @Test
    public void testGetDeliveryModeOptionInformationWithPostCode() {
        final String purchaseOptionCode = "purchaseOptionCode";
        final String deliveryModeCode = "deliveryModeCode";

        given(configurationService.getConfiguration()).willReturn(configuration);
        given(configuration.getString(Mockito.anyString())).willReturn("This is message");
        given(mockTargetPurchaseOptionHelper.getPurchaseOptionModel(purchaseOptionCode)).willReturn(
                mockPurchaseOption);
        given(cartService.getSessionCart()).willReturn(cartModel);
        given(mockTargetDeliveryService.getDeliveryModeForCode(deliveryModeCode)).willReturn(
                mockDeliveryMode);
        given(mockZoneDeliveryModeValue.getMessage()).willReturn("message");
        given(mockZoneDeliveryModeValue.getDisclaimer()).willReturn("disclaimer");
        given(mockDeliveryModeInformation.getDeliveryMode()).willReturn(mockZoneDeliveryMode);
        given(mockDeliveryModeInformation.getDeliveryModeValue()).willReturn(mockZoneDeliveryModeValue);
        given(mockDeliveryModeInformation.getDeliveryFee()).willReturn(mockPriceValue);
        given(mockPriceDataFactory.create(Mockito.any(PriceDataType.class), Mockito.any(BigDecimal.class),
                Mockito.anyString())).willReturn(priceData);
        given(mockTargetCommerceCartService.getDeliveryModeOptionInformation(cartModel, mockDeliveryMode))
                        .willReturn(mockDeliveryModeInformation);

        final DeliveryModeData result = targetCartFacade.getDeliveryModeOptionInformation(deliveryModeCode);

        assertThat(result).isNotNull();
        assertThat(result.getDeliveryFee()).isNotNull();
    }

    @Test
    public void testGetDeliveryModeOptionInformationWithNoPostCode() {
        final String purchaseOptionCode = "purchaseOptionCode";
        final String deliveryModeCode = "deliveryModeCode";

        given(configurationService.getConfiguration()).willReturn(configuration);
        given(configuration.getString(Mockito.anyString())).willReturn("Set postcode to find if delivery mode valid");
        given(mockTargetPurchaseOptionHelper.getPurchaseOptionModel(purchaseOptionCode)).willReturn(
                mockPurchaseOption);
        given(cartService.getSessionCart()).willReturn(cartModel);
        given(mockTargetDeliveryService.getDeliveryModeForCode(deliveryModeCode)).willReturn(
                mockDeliveryMode);
        given(mockZoneDeliveryModeValue.getMessage()).willReturn("message");
        given(mockZoneDeliveryModeValue.getDisclaimer()).willReturn("disclaimer");
        given(mockDeliveryModeInformation.getDeliveryMode()).willReturn(mockZoneDeliveryMode);
        given(mockDeliveryModeInformation.getDeliveryModeValue()).willReturn(null);
        given(mockDeliveryModeInformation.getDeliveryFee()).willReturn(null);
        given(mockTargetCommerceCartService.getDeliveryModeOptionInformation(cartModel, mockDeliveryMode))
                        .willReturn(mockDeliveryModeInformation);

        final DeliveryModeData result = targetCartFacade.getDeliveryModeOptionInformation(deliveryModeCode);

        assertThat(result).isNotNull();
        assertThat("Set postcode to find if delivery mode valid").isEqualTo(result.getMessage());
        assertThat(result.getDeliveryFee()).isNull();
    }



    @Test
    public void testGetDeliveryModeOptionInformationWithNullPurchaseOptionModel() {
        final String deliveryModeCode = "deliveryModeCode";
        final DeliveryModeData result = targetCartFacade.getDeliveryModeOptionInformation(deliveryModeCode);

        assertThat(result).isNull();
    }

    @Test
    public void testGetDeliveryModeOptionInformationWithNullCheckoutCart() {
        final String purchaseOptionCode = "purchaseOptionCode";
        final String deliveryModeCode = "deliveryModeCode";

        given(mockTargetPurchaseOptionHelper.getPurchaseOptionModel(purchaseOptionCode)).willReturn(
                mockPurchaseOption);

        final DeliveryModeData result = targetCartFacade.getDeliveryModeOptionInformation(deliveryModeCode);
        assertThat(result).isNull();
    }

    @Test
    public void testGetDeliveryModeOptionInformationWithNullDeliveryMode() {
        final String purchaseOptionCode = "purchaseOptionCode";
        final String deliveryModeCode = "deliveryModeCode";

        given(mockTargetPurchaseOptionHelper.getPurchaseOptionModel(purchaseOptionCode)).willReturn(
                mockPurchaseOption);
        given(cartService.getSessionCart()).willReturn(cartModel);

        final DeliveryModeData result = targetCartFacade.getDeliveryModeOptionInformation(deliveryModeCode);

        assertThat(result).isNull();
    }

    @Test
    public void testIsCartDeliveryModeDigitalForNonDigitalOnlyProducts() {

        final AbstractOrderEntryModel abstractOrderModel = mock(AbstractOrderEntryModel.class);

        given(targetLaybyCartService.getSessionCart()).willReturn(cartModel);
        given(cartModel.getEntries()).willReturn(
                Collections.singletonList(abstractOrderModel));
        given(abstractOrderModel.getProduct()).willReturn(product);
        given(targetDeliveryModeHelper.getDigitalDeliveryMode(product)).willReturn(
                null);

        assertThat(targetCartFacade.isCartWithDigitalOnlyProducts()).isFalse();
    }

    @Test
    public void testIsCartDeliveryModeDigitalForDigitalOnlyProducts() {

        final AbstractOrderEntryModel abstractOrderModel = mock(AbstractOrderEntryModel.class);

        given(targetLaybyCartService.getSessionCart()).willReturn(cartModel);
        given(cartModel.getEntries()).willReturn(
                Collections.singletonList(abstractOrderModel));
        given(abstractOrderModel.getProduct()).willReturn(product);
        given(targetDeliveryModeHelper.getDigitalDeliveryMode(product)).willReturn(
                mockZoneDeliveryMode);

        assertThat(targetCartFacade.isCartWithDigitalOnlyProducts()).isTrue();
    }

    @Test
    public void testGetDeliveryModeOptionInformationWithNullDeliveryModeInformation() {
        final String purchaseOptionCode = "purchaseOptionCode";
        final String deliveryModeCode = "deliveryModeCode";

        given(mockTargetPurchaseOptionHelper.getPurchaseOptionModel(purchaseOptionCode)).willReturn(
                mockPurchaseOption);
        given(cartService.getSessionCart()).willReturn(cartModel);

        given(mockTargetDeliveryService.getDeliveryModeForCode(deliveryModeCode)).willReturn(
                mockDeliveryMode);

        final DeliveryModeData result = targetCartFacade.getDeliveryModeOptionInformation(deliveryModeCode);

        assertThat(result).isNull();
    }

    @Test
    public void testGetDeliveryModeOptionInformationWithIncorrectDeliveryModeInformationType() {
        final String purchaseOptionCode = "purchaseOptionCode";
        final String deliveryModeCode = "deliveryModeCode";

        given(mockTargetPurchaseOptionHelper.getPurchaseOptionModel(purchaseOptionCode)).willReturn(
                mockPurchaseOption);
        given(cartService.getSessionCart()).willReturn(cartModel);
        final CartModel mockCheckoutCart = mock(CartModel.class);
        given(mockTargetDeliveryService.getDeliveryModeForCode(deliveryModeCode)).willReturn(mockDeliveryMode);
        given(mockDeliveryModeInformation.getDeliveryMode()).willReturn(mockZoneDeliveryMode);
        given(mockTargetCommerceCartService.getDeliveryModeOptionInformation(mockCheckoutCart, mockDeliveryMode))
                        .willReturn(mockDeliveryModeInformation);

        final DeliveryModeData result = targetCartFacade.getDeliveryModeOptionInformation(deliveryModeCode);

        assertThat(result).isNull();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetDeliveryModeOptionInformationWithNullDeliveryModeCode() {
        targetCartFacade.getDeliveryModeOptionInformation(null);
    }

    @Test
    public void testRestoreSavedCartForCustomerWithNoSavedCart() throws CommerceCartRestorationException {
        given(mockUserService.getCurrentUser()).willReturn(customer);
        given(targetLaybyCartService.getMostRecentCartForCustomer(customer)).willReturn(null);

        final boolean isCartEntryModified = targetCartFacade.restoreSavedCart();
        assertThat(isCartEntryModified).isFalse();
        verifyZeroInteractions(mockTargetCommerceCartService);
    }

    @Test
    public void testRestoreSavedCartWithAcceptedPaymentTransaction() throws CommerceCartRestorationException {
        final List<PaymentTransactionModel> ptmList = setupPaymentForStatus(TransactionStatus.ACCEPTED);

        given(mockUserService.getCurrentUser()).willReturn(customer);
        given(targetLaybyCartService.getMostRecentCartForCustomer(customer)).willReturn(cartModel);
        given(cartModel.getPaymentTransactions()).willReturn(ptmList);

        final boolean isCartEntryModified = targetCartFacade.restoreSavedCart();
        assertThat(isCartEntryModified).isFalse();
        verify(mockTargetCommerceCartService, Mockito.never()).restoreCart(
                Mockito.any(CommerceCartParameter.class));
    }

    @Test
    public void testRestoreSavedCartWithReviewPaymentTransaction() throws CommerceCartRestorationException {
        final List<PaymentTransactionModel> ptmList = setupPaymentForStatus(TransactionStatus.REVIEW);

        given(mockUserService.getCurrentUser()).willReturn(customer);
        given(targetLaybyCartService.getMostRecentCartForCustomer(customer)).willReturn(cartModel);
        given(cartModel.getPaymentTransactions()).willReturn(ptmList);

        final boolean isCartEntryModified = targetCartFacade.restoreSavedCart();
        assertThat(isCartEntryModified).isFalse();
        verify(mockTargetCommerceCartService, Mockito.never()).restoreCart(
                Mockito.any(CommerceCartParameter.class));
    }

    @Test
    public void testRestoreSavedCartWithRejectedPaymentTransaction() throws CommerceCartRestorationException {
        final List<PaymentTransactionModel> ptmList = setupPaymentForStatus(TransactionStatus.REJECTED);
        final CommerceCartRestoration restoreCart = setupCartModifications(0);

        given(mockUserService.getCurrentUser()).willReturn(customer);
        given(targetLaybyCartService.getMostRecentCartForCustomer(customer)).willReturn(cartModel);
        given(mockTargetCommerceCartService.restoreCart(Mockito.any(CommerceCartParameter.class)))
                .willReturn(restoreCart);
        given(cartModel.getPaymentTransactions()).willReturn(ptmList);

        final boolean isCartEntryModified = targetCartFacade.restoreSavedCart();
        assertThat(isCartEntryModified).isFalse();
        verify(mockTargetCommerceCartService).restoreCart(Mockito.any(CommerceCartParameter.class));
    }

    @Test
    public void testRestoreSavedCart() throws CommerceCartRestorationException {
        final CommerceCartRestoration restoreCart = setupCartModifications(0);

        given(mockUserService.getCurrentUser()).willReturn(customer);
        given(targetLaybyCartService.getMostRecentCartForCustomer(customer)).willReturn(cartModel);
        given(mockTargetCommerceCartService.restoreCart(Mockito.any(CommerceCartParameter.class)))
                .willReturn(restoreCart);

        final boolean isCartEntryModified = targetCartFacade.restoreSavedCart();
        assertThat(isCartEntryModified).isFalse();
        verify(mockTargetCommerceCartService).restoreCart(Mockito.any(CommerceCartParameter.class));
    }

    @Test
    public void testRestoreSavedCartWithModifiedQuantity() throws CommerceCartRestorationException {
        final CommerceCartRestoration restoreCart = setupCartModifications(1);

        given(mockUserService.getCurrentUser()).willReturn(customer);
        given(targetLaybyCartService.getMostRecentCartForCustomer(customer)).willReturn(cartModel);
        given(mockTargetCommerceCartService.restoreCart(Mockito.any(CommerceCartParameter.class)))
                .willReturn(restoreCart);

        final boolean isCartEntryModified = targetCartFacade.restoreSavedCart();
        assertThat(isCartEntryModified).isTrue();
        verify(mockTargetCommerceCartService).restoreCart(Mockito.any(CommerceCartParameter.class));
    }


    @Test
    public void testRestoreCartForCustomerWithNoSavedCart() throws CommerceCartRestorationException {
        given(mockUserService.getCurrentUser()).willReturn(customer);
        given(mockBaseSiteService.getCurrentBaseSite()).willReturn(baseSiteModel);
        given(mockTargetCommerceCartService.getCartForGuidAndSite(Mockito.anyString(),
                        Mockito.any(BaseSiteModel.class)))
                                .willReturn(null);

        targetCartFacade.restoreCart(DUMMY_GUID);
        verify(mockTargetCommerceCartService, Mockito.never()).restoreCart(
                Mockito.any(CommerceCartParameter.class));
    }


    @Test
    public void testRestoreCartForCustomerWithSavedCart() throws CommerceCartRestorationException {
        final CommerceCartRestoration restoreCart = setupCartModifications(0);
        given(mockUserService.getCurrentUser()).willReturn(customer);
        given(mockBaseSiteService.getCurrentBaseSite()).willReturn(baseSiteModel);
        given(mockTargetCommerceCartService.getCartForGuidAndSite(Mockito.anyString(),
                Mockito.any(BaseSiteModel.class))).willReturn(cartModel);
        given(mockTargetCommerceCartService.restoreCart(Mockito.any(CommerceCartParameter.class)))
                .willReturn(restoreCart);
        targetCartFacade.restoreCart(DUMMY_GUID);
        verify(mockTargetCommerceCartService).restoreCart(Mockito.any(CommerceCartParameter.class));
    }


    @SuppressWarnings("boxing")
    @Test
    public void testAddToCartSuccessfulWithProductCode() throws CommerceCartModificationException {

        given(product.getUnit()).willReturn(unitModelMock);
        given(productService.getProductForCode("P1000_Black_L")).willReturn(product);
        given(targetLaybyCartService.getSessionCart()).willReturn(cartModel);
        given(flybuysDiscountFacade.reassessFlybuysDiscountOnCheckoutCart()).willReturn(true);
        given(mockTargetCommerceCartService.addToCart(cartModel, product, 1, unitModelMock))
                .willReturn(cartModificationMock);
        given(cartModificationConverter.convert(cartModificationMock))
                .willReturn(cartModificationDataMock);

        final CartModificationData cartModificationResult = targetCartFacade.addToCart("P1000_Black_L", 1);
        assertThat(cartModificationDataMock).isEqualTo(cartModificationResult);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testAddToCartSuccessfulWithGiftRecipient() throws CommerceCartModificationException {

        given(product.getUnit()).willReturn(unitModelMock);
        given(productService.getProductForCode("P1000_Black_L")).willReturn(product);
        given(targetLaybyCartService.getSessionCart()).willReturn(cartModel);
        given(new Boolean(flybuysDiscountFacade.reassessFlybuysDiscountOnCheckoutCart())).willReturn(
                Boolean.TRUE);
        given(mockTargetCommerceCartService.addToCart(cartModel, product, 1, unitModelMock))
                .willReturn(cartModificationMock);
        given(cartModificationConverter.convert(cartModificationMock))
                .willReturn(cartModificationDataMock);

        given(giftCardService.isProductADigitalGiftCard(product))
                .willReturn(Boolean.TRUE);

        final CartModificationData cartModificationResult = targetCartFacade.addToCart("P1000_Black_L", 1,
                new GiftRecipientDTO());
        assertThat(cartModificationDataMock).isEqualTo(cartModificationResult);
    }

    @SuppressWarnings("boxing")
    @Test(expected = CommerceCartModificationException.class)
    public void testAddToCartSuccessfulFailForAGiftCardWithoutRecipientDetails()
            throws CommerceCartModificationException {

        given(product.getUnit()).willReturn(unitModelMock);
        given(productService.getProductForCode("P1000_Black_L")).willReturn(product);
        given(targetLaybyCartService.getSessionCart()).willReturn(cartModel);
        given(new Boolean(flybuysDiscountFacade.reassessFlybuysDiscountOnCheckoutCart())).willReturn(
                Boolean.TRUE);
        given(mockTargetCommerceCartService.addToCart(cartModel, product, 1, unitModelMock))
                .willReturn(cartModificationMock);
        given(cartModificationConverter.convert(cartModificationMock))
                .willReturn(cartModificationDataMock);
        given(giftCardService.isProductADigitalGiftCard(product))
                .willReturn(Boolean.TRUE);
        final GiftRecipientDTO giftRecipient = null;
        final CartModificationData cartModificationResult = targetCartFacade.addToCart("P1000_Black_L", 1,
                giftRecipient);
        assertThat(cartModificationDataMock).isEqualTo(cartModificationResult);
    }

    @SuppressWarnings("boxing")
    @Test(expected = CommerceCartModificationException.class)
    public void testAddToCartSuccessfulFailForNonGiftCardWithRecipientDetails()
            throws CommerceCartModificationException {

        given(product.getUnit()).willReturn(unitModelMock);
        given(productService.getProductForCode("P1000_Black_L")).willReturn(product);
        given(targetLaybyCartService.getSessionCart()).willReturn(cartModel);
        given(new Boolean(flybuysDiscountFacade.reassessFlybuysDiscountOnCheckoutCart())).willReturn(
                Boolean.TRUE);
        given(mockTargetCommerceCartService.addToCart(cartModel, product, 1, unitModelMock))
                .willReturn(cartModificationMock);
        given(cartModificationConverter.convert(cartModificationMock))
                .willReturn(cartModificationDataMock);
        given(giftCardService.isProductAGiftCard(product))
                .willReturn(Boolean.FALSE);
        final CartModificationData cartModificationResult = targetCartFacade.addToCart("P1000_Black_L", 1,
                new GiftRecipientDTO());
        assertThat(cartModificationDataMock).isEqualTo(cartModificationResult);
    }

    @Test
    public void testAddToCartSuccessfulWithNoGiftRecipient() throws CommerceCartModificationException {

        given(product.getUnit()).willReturn(unitModelMock);
        given(productService.getProductForCode("P1000_Black_L")).willReturn(product);
        given(targetLaybyCartService.getSessionCart()).willReturn(cartModel);
        given(new Boolean(flybuysDiscountFacade.reassessFlybuysDiscountOnCheckoutCart())).willReturn(
                Boolean.TRUE);
        given(mockTargetCommerceCartService.addToCart(cartModel, product, 1, unitModelMock))
                .willReturn(cartModificationMock);
        given(cartModificationConverter.convert(cartModificationMock))
                .willReturn(cartModificationDataMock);

        final GiftRecipientDTO recipient = null;
        final CartModificationData cartModificationResult = targetCartFacade.addToCart("P1000_Black_L", 1,
                recipient);
        assertThat(cartModificationDataMock).isEqualTo(cartModificationResult);
    }

    @Test(expected = ProductNotFoundCommerceCartModificationException.class)
    public void testAddToCartWithProductCodeNotFound() throws CommerceCartModificationException {

        final UnknownIdentifierException unknownIdentifierExceptionMock = mock(UnknownIdentifierException.class);
        given(productService.getProductForCode("NOT_DEFINED")).willThrow(unknownIdentifierExceptionMock);
        targetCartFacade.addToCart("NOT_DEFINED", 1);
    }

    /**
     * Test get postal code from session.
     */
    @Test
    public void testGetPostalCodeFromSession() {
        final TargetCartFacadeImpl targetFacade = mock(TargetCartFacadeImpl.class);
        given(targetFacade.getPostalCodeFromCartOrSession(mockCartData)).willCallRealMethod();
        given(targetFacade.getSessionCart()).willReturn(mockCartData);
        given(targetFacade.getSessionService()).willReturn(sessionService);
        given(sessionService.getAttribute(Mockito.anyString())).willReturn("2020");

        assertThat(targetCartFacade.getPostalCodeFromCartOrSession(mockCartData)).isEqualTo("2020");
    }

    @Test
    public void testGetPostalCodeFromAddressDataWithoutUpdateDM() {
        final TargetCartFacadeImpl targetFacade = mock(TargetCartFacadeImpl.class);
        given(targetFacade.getPostalCodeFromCartOrSession(mockCartData)).willCallRealMethod();
        given(addressData.getPostalCode()).willReturn("3030");
        given(mockCartData.getDeliveryAddress()).willReturn(addressData);
        given(targetFacade.getTheSessionCart()).willReturn(cartModel);
        given(sessionService.getAttribute(Mockito.anyString())).willReturn(StringUtils.EMPTY);
        given(targetFacade.getSessionService()).willReturn(sessionService);
        given(targetFacade.getPostCodeFormPreferredDeliveryMode()).willReturn("7070");
        given(targetFacade.getTargetCheckoutFacade()).willReturn(checkoutFacade);
        given(targetFacade.preferredDeliveryMode(cartModel)).willReturn(mockDeliveryMode);
        given(targetFacade.getModelService()).willReturn(modelService);
        given(targetFacade.getTargetDeliveryModeHelper()).willReturn(targetDeliveryModeHelper);
        given(Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                mockDeliveryMode))).willReturn(Boolean.TRUE);
        given(cartService.getSessionCart()).willReturn(cartModel);
        given(Boolean.valueOf(targetUserFacade.isAnonymousUser())).willReturn(Boolean.TRUE);

        assertThat(targetFacade.getPostalCodeFromCartOrSession(mockCartData)).isEqualTo("3030");
        verify(sessionService).getAttribute(Mockito.anyString());
    }

    /**
     * Test set postal code in session from cart data.
     */
    @Test
    public void testGetPostalCodeFromCart() {
        final TargetCartFacadeImpl targetFacade = mock(TargetCartFacadeImpl.class);
        given(targetFacade.getPostalCodeFromCartOrSession(mockCartData)).willCallRealMethod();
        given(mockCartData.getDeliveryAddress()).willReturn(addressData);
        given(mockCartData.getDeliveryAddress().getPostalCode()).willReturn("2020");
        given(targetFacade.getSessionService()).willReturn(sessionService);
        given(sessionService.getAttribute(Mockito.anyString())).willReturn("3030");
        mockCartData.setDeliveryAddress(addressData);

        assertThat(targetFacade.getPostalCodeFromCartOrSession(mockCartData)).isEqualTo("2020");
        verify(sessionService).setAttribute(Mockito.anyString(), Mockito.anyString());
    }

    /**
     * Test get empty postal code.
     */
    @Test
    public void testGetPostalCodeIsEmpty() {
        final TargetCartFacadeImpl targetFacade = mock(TargetCartFacadeImpl.class);
        given(targetFacade.getPostalCodeFromCartOrSession(mockCartData)).willCallRealMethod();
        given(mockCartData.getDeliveryAddress()).willReturn(null);
        given(targetFacade.getTheSessionCart()).willReturn(cartModel);
        given(mockCartData.getDeliveryAddress()).willReturn(null);
        given(sessionService.getAttribute(Mockito.anyString())).willReturn(StringUtils.EMPTY);
        given(targetFacade.getSessionService()).willReturn(sessionService);
        given(targetFacade.getPostCodeFormPreferredDeliveryMode()).willReturn(StringUtils.EMPTY);
        given(cartService.getSessionCart()).willReturn(cartModel);
        given(targetFacade.getModelService()).willReturn(modelService);
        given(targetFacade.preferredDeliveryMode(cartModel)).willReturn(mockDeliveryMode);
        given(Boolean.valueOf(targetUserFacade.isAnonymousUser())).willReturn(Boolean.TRUE);
        given(targetFacade.getTargetCheckoutFacade()).willReturn(checkoutFacade);
        given(targetFacade.getTargetDeliveryModeHelper()).willReturn(targetDeliveryModeHelper);
        given(Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                mockDeliveryMode))).willReturn(Boolean.FALSE);

        assertThat(targetFacade.getPostalCodeFromCartOrSession(mockCartData)).isEmpty();
        verify(sessionService).getAttribute(Mockito.anyString());
    }

    @Test
    public void testGetPostalCodeFromPreferredDM() {
        final TargetCartFacadeImpl targetFacade = mock(TargetCartFacadeImpl.class);

        given(targetFacade.getPostalCodeFromCartOrSession(mockCartData)).willCallRealMethod();
        given(mockCartData.getDeliveryAddress()).willReturn(null);
        given(targetFacade.getTheSessionCart()).willReturn(cartModel);
        given(sessionService.getAttribute(Mockito.anyString())).willReturn(StringUtils.EMPTY);
        given(targetFacade.getSessionService()).willReturn(sessionService);
        given(targetFacade.getPostCodeFormPreferredDeliveryMode()).willReturn("7070");
        given(targetFacade.getTargetCheckoutFacade()).willReturn(checkoutFacade);
        given(targetFacade.preferredDeliveryMode(cartModel)).willReturn(mockDeliveryMode);
        given(targetFacade.getModelService()).willReturn(modelService);
        given(targetFacade.getTargetDeliveryModeHelper()).willReturn(targetDeliveryModeHelper);
        given(Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                mockDeliveryMode))).willReturn(Boolean.TRUE);
        given(cartService.getSessionCart()).willReturn(cartModel);
        given(Boolean.valueOf(targetUserFacade.isAnonymousUser())).willReturn(Boolean.TRUE);

        assertThat(targetFacade.getPostalCodeFromCartOrSession(mockCartData)).isEqualTo("7070");
        verify(sessionService).getAttribute(Mockito.anyString());
        verify(checkoutFacade).updateDeliveryMode(Mockito.anyString());
    }

    @Test
    public void testGetPostalCodeFromPreferredDMWithOutUpdatingDM() {
        final TargetCartFacadeImpl targetFacade = mock(TargetCartFacadeImpl.class);
        given(targetFacade.getPostalCodeFromCartOrSession(mockCartData)).willCallRealMethod();
        given(mockCartData.getDeliveryAddress()).willReturn(null);
        given(targetFacade.getTheSessionCart()).willReturn(cartModel);
        given(sessionService.getAttribute(Mockito.anyString())).willReturn(StringUtils.EMPTY);
        given(targetFacade.getSessionService()).willReturn(sessionService);
        given(targetFacade.getPostCodeFormPreferredDeliveryMode()).willReturn("7070");
        given(targetFacade.getTargetCheckoutFacade()).willReturn(checkoutFacade);
        given(targetFacade.preferredDeliveryMode(cartModel)).willReturn(mockDeliveryMode);
        given(targetFacade.getModelService()).willReturn(modelService);
        given(targetFacade.getTargetDeliveryModeHelper()).willReturn(targetDeliveryModeHelper);
        given(Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                mockDeliveryMode))).willReturn(Boolean.FALSE);
        given(cartService.getSessionCart()).willReturn(cartModel);
        given(Boolean.valueOf(targetUserFacade.isAnonymousUser())).willReturn(Boolean.TRUE);

        assertThat(targetFacade.getPostalCodeFromCartOrSession(mockCartData)).isEqualTo("7070");
        verify(sessionService).getAttribute(Mockito.anyString());
        verify(checkoutFacade, never()).updateDeliveryMode(Mockito.anyString());
    }

    @Test
    public void testGetPostCodeFromSavedDeliveryModeAnonymousUser() {
        given(cartService.getSessionCart()).willReturn(cartModel);
        given(Boolean.valueOf(targetUserFacade.isAnonymousUser())).willReturn(Boolean.TRUE);
        assertThat(targetCartFacade.getPostCodeFormPreferredDeliveryMode()).isEmpty();
    }

    @Test
    public void testGetPostCodeFromSavedDeliveryModeRegisteredUserWithOutOrder() {
        given(cartModel.getUser()).willReturn(customerModel);
        given(cartService.getSessionCart()).willReturn(cartModel);
        given(Boolean.valueOf(targetUserFacade.isAnonymousUser())).willReturn(Boolean.FALSE);
        given(targetOrderService.findLatestOrderForUser(customer)).willReturn(null);
        assertThat(targetCartFacade.getPostCodeFormPreferredDeliveryMode()).isEmpty();
    }


    @Test
    public void testGetPostCodeFromSavedDeliveryModeRegisteredUserWithOrder() {
        given(cartModel.getUser()).willReturn(customerModel);
        given(cartService.getSessionCart()).willReturn(cartModel);
        given(cartService.getSessionCart()).willReturn(cartModel);
        given(Boolean.valueOf(targetUserFacade.isAnonymousUser())).willReturn(Boolean.FALSE);
        given(addressModel.getPostalcode()).willReturn("2323");
        given(orderModel.getDeliveryAddress()).willReturn(addressModel);
        given(targetOrderService.findLatestOrderForUser(customerModel)).willReturn(orderModel);

        assertThat(targetCartFacade.getPostCodeFormPreferredDeliveryMode()).isEqualTo("2323");
    }

    @Test
    public void testGetPreferredDeliveryMode() {
        given(mockDeliveryMode.getCode()).willReturn("asdsa");
        given(Boolean.valueOf(targetUserFacade.isAnonymousUser())).willReturn(Boolean.FALSE);
        given(targetOrderService.findLatestOrderForUser(customerModel)).willReturn(orderModel);
        given(orderModel.getDeliveryAddress()).willReturn(addressModel);
        given(orderModel.getDeliveryMode()).willReturn(mockDeliveryMode);
        given(cartModel.getUser()).willReturn(customerModel);
        given(targetCartFacade.getTheSessionCart()).willReturn(cartModel);
        assertThat(targetCartFacade.preferredDeliveryMode(cartModel)).isNotNull();
    }

    @Test
    public void testGetPreferredDeliveryModeForGuestUser() {
        given(Boolean.valueOf(targetUserFacade.isAnonymousUser())).willReturn(Boolean.TRUE);
        given(cartModel.getUser()).willReturn(customerModel);
        given(targetCartFacade.getTheSessionCart()).willReturn(cartModel);
        assertThat(targetCartFacade.preferredDeliveryMode(cartModel)).isNull();
    }

    @Test
    public void testGetPreferredDeliveryModeWithOutUserModel() {
        given(Boolean.valueOf(targetUserFacade.isAnonymousUser())).willReturn(Boolean.FALSE);
        given(targetCartFacade.getTheSessionCart()).willReturn(cartModel);
        assertThat(targetCartFacade.preferredDeliveryMode(cartModel)).isNull();
    }

    @Test
    public void testGetPreferredDeliveryModeForNewUser() {
        given(Boolean.valueOf(targetUserFacade.isAnonymousUser())).willReturn(Boolean.FALSE);
        given(targetOrderService.findLatestOrderForUser(customerModel)).willReturn(null);
        given(cartModel.getUser()).willReturn(customerModel);
        given(targetCartFacade.getTheSessionCart()).willReturn(cartModel);
        assertThat(targetCartFacade.preferredDeliveryMode(cartModel)).isNull();
    }

    @Test
    public void testGetCatchmentAreaByPostcodeWithEmpayPostcodeGroup() {
        final String postcode = "1234";
        final Collection<PostCodeGroupModel> postcodeGroups = new ArrayList<>();
        given(targetPostCodeService.getPostCodeGroupByPostcode(postcode)).willReturn(postcodeGroups);
        final String result = targetCartFacade.getCatchmentAreaByPostcode(postcode);
        assertThat("unallocated").isEqualTo(result);
    }

    @Test
    public void testGetCatchmentAreaByPostcodeWithEmpayPostcode() {
        final String postcode = StringUtils.EMPTY;
        final String result = targetCartFacade.getCatchmentAreaByPostcode(postcode);
        assertThat(result).isEmpty();
    }

    @Test
    public void testGetCatchmentAreaByPostcode() {
        final String postcode = "1234";
        final Collection<PostCodeGroupModel> postcodeGroups = new ArrayList<>();
        final PostCodeGroupModel model1 = new PostCodeGroupModel();
        model1.setCatchmentArea("testCatchmentArea1");
        postcodeGroups.add(model1);
        given(targetPostCodeService.getPostCodeGroupByPostcode(postcode)).willReturn(postcodeGroups);
        final String result = targetCartFacade.getCatchmentAreaByPostcode(postcode);
        assertThat("testCatchmentArea1").isEqualTo(result);
    }

    @Test
    public void testGetCatchmentAreaByPostcodeWithMultiCatchmentArea() {
        final String postcode = "1234";
        final Collection<PostCodeGroupModel> postcodeGroups = new ArrayList<>();
        final PostCodeGroupModel model1 = new PostCodeGroupModel();
        model1.setCatchmentArea("testCatchmentArea1");
        postcodeGroups.add(model1);
        final PostCodeGroupModel model2 = new PostCodeGroupModel();
        model2.setCatchmentArea("testCatchmentArea2");
        postcodeGroups.add(model2);
        given(targetPostCodeService.getPostCodeGroupByPostcode(postcode)).willReturn(postcodeGroups);
        final String result = targetCartFacade.getCatchmentAreaByPostcode(postcode);
        assertThat("testCatchmentArea1/testCatchmentArea2").isEqualTo(result);
    }

    @Test(expected = GiftCardValidationException.class)
    public void testValidateGiftCardQuantityAllowed() throws GiftCardValidationException, ProductNotFoundException {
        final String productCode = "PGC1000_iTunes_100";
        final long newQty = 2;

        final MaxGiftCardQtyValidator validator = mock(MaxGiftCardQtyValidator.class);
        final List<BaseGiftCardValidator> validators = new ArrayList<>();
        validators.add(validator);
        doThrow(new GiftCardValidationException("Giftcard ERROR", GiftCardValidationErrorType.QUANTITY))
                .when(validator)
                .validateGiftCards(Mockito.anyString(), Mockito.anyLong());
        targetCartFacade.setGiftCardValidators(validators);
        targetCartFacade.validateGiftCards(productCode, newQty);
    }

    @Test(expected = CommerceCartModificationException.class)
    public void testRemoveGiftCardRecipientEntryThrowsException() throws CommerceCartModificationException {
        final String productCode = "PGC1000_iTunes_100";
        final int recipientEntryToRemove = 2;
        given(productService.getProductForCode(productCode)).willReturn(product);
        given(targetLaybyCartService.getSessionCart()).willReturn(cartModel);
        given(mockTargetCommerceCartService.removeGiftCardRecipient(cartModel, product,
                recipientEntryToRemove, PK_STR)).willThrow(
                        new CommerceCartModificationException("Exception has occured"));

        targetCartFacade.removeGiftCardRecipientEntry(productCode, recipientEntryToRemove, PK_STR);
        verify(productService).getProductForCode(productCode);
        verifyNoMoreInteractions(productService);
        verify(targetLaybyCartService).getSessionCart();
        verifyNoMoreInteractions(targetLaybyCartService);
        verifyZeroInteractions(cartModificationConverter);
    }

    @Test
    public void testRemoveGiftCardRecipientEntryHappyPath() throws CommerceCartModificationException {
        final String productCode = "PGC1000_iTunes_100";
        final int recipientEntryToRemove = 2;
        given(productService.getProductForCode(productCode)).willReturn(product);
        given(targetLaybyCartService.getSessionCart()).willReturn(cartModel);
        given(mockTargetCommerceCartService.removeGiftCardRecipient(cartModel, product,
                recipientEntryToRemove, PK_STR)).willReturn(cartModificationMock);
        given(cartModificationConverter.convert(cartModificationMock)).willReturn(new CartModificationData());

        targetCartFacade.removeGiftCardRecipientEntry(productCode, recipientEntryToRemove, PK_STR);
        verify(productService).getProductForCode(productCode);
        verifyNoMoreInteractions(productService);
        verify(targetLaybyCartService).getSessionCart();
        verifyNoMoreInteractions(targetLaybyCartService);
        verify(cartModificationConverter).convert(cartModificationMock);
        verifyNoMoreInteractions(cartModificationConverter);
    }

    @Test(expected = CommerceCartModificationException.class)
    public void testUpdateGiftCardRecipientEntryThrowsException() throws CommerceCartModificationException {
        final String productCode = "PGC1000_iTunes_100";
        final GiftRecipientDTO recipientDto = mock(GiftRecipientDTO.class);
        final int recipientEntryToUpdate = 2;
        given(productService.getProductForCode(productCode)).willReturn(product);
        given(targetLaybyCartService.getSessionCart()).willReturn(cartModel);
        given(mockTargetCommerceCartService.updateGiftcardRecipientDetails(cartModel, product,
                recipientEntryToUpdate, PK_STR, recipientDto)).willThrow(
                        new CommerceCartModificationException("Exception has occured"));

        targetCartFacade.updateGiftCardDetails(productCode, recipientDto, recipientEntryToUpdate, PK_STR);
        verify(productService).getProductForCode(productCode);
        verifyNoMoreInteractions(productService);
        verify(targetLaybyCartService).getSessionCart();
        verifyNoMoreInteractions(targetLaybyCartService);
        verifyZeroInteractions(cartModificationConverter);
    }

    @Test
    public void testUpdateGiftCardRecipientEntrySuccess() throws CommerceCartModificationException {
        final String productCode = "PGC1000_iTunes_100";
        final GiftRecipientDTO recipientDto = mock(GiftRecipientDTO.class);
        final int recipientEntryToUpdate = 2;
        given(productService.getProductForCode(productCode)).willReturn(product);
        given(targetLaybyCartService.getSessionCart()).willReturn(cartModel);
        given(mockTargetCommerceCartService.updateGiftcardRecipientDetails(cartModel, product,
                recipientEntryToUpdate, PK_STR, recipientDto)).willReturn(cartModificationMock);
        given(cartModificationConverter.convert(cartModificationMock)).willReturn(new CartModificationData());

        targetCartFacade.updateGiftCardDetails(productCode, recipientDto,
                recipientEntryToUpdate, PK_STR);
        verify(productService).getProductForCode(productCode);
        verifyNoMoreInteractions(productService);
        verify(targetLaybyCartService).getSessionCart();
        verifyNoMoreInteractions(targetLaybyCartService);
        verify(cartModificationConverter).convert(cartModificationMock);
        verifyNoMoreInteractions(cartModificationConverter);
    }

    private List<PaymentTransactionModel> setupPaymentForStatus(final TransactionStatus status) {

        final List<PaymentTransactionModel> ptmList = new ArrayList<>();
        final PaymentTransactionModel ptm = new PaymentTransactionModel();
        final List<PaymentTransactionEntryModel> ptemList = new ArrayList<>();
        final PaymentTransactionEntryModel ptem = new PaymentTransactionEntryModel();
        ptem.setTransactionStatus(status.toString());
        ptem.setType(PaymentTransactionType.CAPTURE);
        ptemList.add(ptem);
        ptm.setEntries(ptemList);
        ptmList.add(ptm);

        return ptmList;
    }

    private CommerceCartRestoration setupCartModifications(final long qty) {
        final CommerceCartRestoration restoreCart = new CommerceCartRestoration();
        final CommerceCartModification commarceCartModification = new CommerceCartModification();
        commarceCartModification.setQuantityAdded(qty);
        restoreCart.setModifications(Arrays.asList(commarceCartModification));
        return restoreCart;
    }

    @Test
    public void testGetCartsColorVariantProductCodesWhenCartEntriesAreNull() {
        given(targetCartFacade.getTheSessionCart()).willReturn(cartModel);
        given(cartModel.getEntries()).willReturn(null);
        assertThat(targetCartFacade.getCartsColorVariantProductCodes()).isNull();
    }

    @Test
    public void testGetCartsColorVariantProductCodesWhenCartEntriesAreEmpty() {
        given(targetCartFacade.getTheSessionCart()).willReturn(cartModel);
        given(cartModel.getEntries()).willReturn(Collections.EMPTY_LIST);
        assertThat(targetCartFacade.getCartsColorVariantProductCodes()).isNull();
    }

    @Test
    public void testGetCartsColorVariantProductCodesWhenCartEntriesWithSizeVariantAndNonEmptyProdReferences() {
        given(targetCartFacade.getTheSessionCart()).willReturn(cartModel);

        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final AbstractOrderEntryModel entryOne = new AbstractOrderEntryModel();
        final TargetColourVariantProductModel entryOnesProduct = new TargetColourVariantProductModel();
        entryOne.setProduct(entryOnesProduct);
        entries.add(entryOne);

        final AbstractOrderEntryModel entryTwo = new AbstractOrderEntryModel();
        final TargetSizeVariantProductModel entryTwosProduct = new TargetSizeVariantProductModel();
        final TargetColourVariantProductModel entryTwosBaseProduct = new TargetColourVariantProductModel();
        entryTwosProduct.setBaseProduct(entryTwosBaseProduct);
        entryTwo.setProduct(entryTwosProduct);
        entries.add(entryTwo);

        given(cartModel.getEntries()).willReturn(entries);
        final List<String> productCodes = targetCartFacade.getCartsColorVariantProductCodes();
        assertThat(productCodes).hasSize(2);
    }

    @Test
    public void testGetCartsColorVariantProductCodesWhenCartEntriesWithSizeVariantAndEmptyProdReferences() {
        given(targetCartFacade.getTheSessionCart()).willReturn(cartModel);

        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final AbstractOrderEntryModel entryOne = new AbstractOrderEntryModel();
        final TargetColourVariantProductModel entryOnesProduct = new TargetColourVariantProductModel();
        entryOne.setProduct(entryOnesProduct);
        entries.add(entryOne);

        final AbstractOrderEntryModel entryTwo = new AbstractOrderEntryModel();
        final TargetSizeVariantProductModel entryTwosProduct = new TargetSizeVariantProductModel();
        final TargetColourVariantProductModel entryTwosBaseProduct = new TargetColourVariantProductModel();
        entryTwosProduct.setBaseProduct(entryTwosBaseProduct);
        entryTwo.setProduct(entryTwosProduct);
        entries.add(entryTwo);

        given(cartModel.getEntries()).willReturn(entries);
        final List<String> productCodes = targetCartFacade.getCartsColorVariantProductCodes();
        assertThat(productCodes).hasSize(2);
    }

    @Test
    public void testHasVariantsForProductWithVariantTypeAndVariants() {
        given(productService.getProductForCode("P1")).willReturn(product);
        given(product.getVariantType()).willReturn(variantType);
        given(product.getVariants()).willReturn(Arrays.asList(variantProductModel));

        assertThat(targetCartFacade.hasVariantsForProduct("P1")).isTrue();
    }

    @Test
    public void testHasVariantsForProductWithVariantTypeAndNoVariants() {
        given(productService.getProductForCode("P1")).willReturn(product);
        given(product.getVariantType()).willReturn(variantType);

        assertThat(targetCartFacade.hasVariantsForProduct("P1")).isFalse();
    }

    @Test
    public void testHasVariantsForProductWithNoVariants() {
        given(productService.getProductForCode("P1")).willReturn(product);
        assertThat(targetCartFacade.hasVariantsForProduct("P1")).isFalse();
    }

    @Test
    public void testHasSizeVariantsForProductWithSizeVariants() {
        given(productService.getProductForCode("P1")).willReturn(product);
        given(product.getVariantType()).willReturn(variantType);
        given(variantType.getCode()).willReturn("TargetSizeVariantProduct");

        assertThat(targetCartFacade.hasSizeVariantsForProduct("P1")).isTrue();
    }

    @Test
    public void testHasSizeVariantsForProductWithColourVariantsAndNoSizeVariants() {
        given(productService.getProductForCode("P1")).willReturn(product);
        given(product.getVariantType()).willReturn(variantType);
        given(variantType.getCode()).willReturn("TargetColourVariantProduct");

        assertThat(targetCartFacade.hasSizeVariantsForProduct("P1")).isFalse();
    }

    @Test
    public void testHasSizeVariantsForProductWithColourVariantsAndSizeVariants() {
        given(productService.getProductForCode("P1")).willReturn(product);
        given(product.getVariantType()).willReturn(variantType);
        given(variantType.getCode()).willReturn("TargetColourVariantProduct");
        given(product.getVariants()).willReturn(Arrays.asList(variantProductModel));

        assertThat(targetCartFacade.hasSizeVariantsForProduct("P1")).isTrue();
    }

}
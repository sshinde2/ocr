/**
 * 
 */
package au.com.target.tgtfacades.ordersplitting.converters;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;



/**
 * @author rmcalave
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetConsignmentPopulatorTest {
    private static final String TEST_TRACKING_URL = "http://testUrl/";

    @InjectMocks
    private final TargetConsignmentPopulator consignmentConverter = new TargetConsignmentPopulator();

    @Mock
    private EnumerationService mockEnumerationService;

    @Mock
    private TargetStoreConsignmentService mockTargetStoreConsignmentService;

    @Mock
    private Converter<ConsignmentEntryModel, ConsignmentEntryData> mockConsignmentEntryConverter;

    @Mock
    private TargetCarrierModel carrier;

    @Mock
    private DeliveryModeModel deliveryModeModelMock;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Before
    public void setUp() {
        given(carrier.getTrackingUrl()).willReturn(TEST_TRACKING_URL);
    }

    @Test
    public void testPopulate() {
        final ConsignmentStatus consignmentStatus = ConsignmentStatus.SHIPPED;
        final String consignmentCode = "23568974";
        final String consignmentTrackingId = "784512784512";

        final ConsignmentEntryModel mockConsignmentEntryModel = mock(ConsignmentEntryModel.class);

        final ConsignmentEntryData consignmentEntry = new ConsignmentEntryData();
        given(mockConsignmentEntryConverter.convert(mockConsignmentEntryModel)).willReturn(consignmentEntry);

        given(mockEnumerationService.getEnumerationName(consignmentStatus)).willReturn(consignmentStatus.getCode());

        final OrderModel mockOrderModel = Mockito.mock(OrderModel.class);
        given(mockOrderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);

        final TargetConsignmentModel mockConsignmentModel = mock(TargetConsignmentModel.class);
        given(mockConsignmentModel.getConsignmentEntries())
                .willReturn(Collections.singleton(mockConsignmentEntryModel));
        given(mockConsignmentModel.getStatus()).willReturn(consignmentStatus);
        given(mockConsignmentModel.getCode()).willReturn(consignmentCode);
        given(mockConsignmentModel.getTrackingID()).willReturn(consignmentTrackingId);
        given(mockConsignmentModel.getOrder()).willReturn(mockOrderModel);
        given(mockConsignmentModel.getTargetCarrier()).willReturn(carrier);

        final ConsignmentData result = new ConsignmentData();
        consignmentConverter.populate(mockConsignmentModel, result);

        assertThat(result.getCode()).isEqualTo(consignmentCode);
        assertThat(result.getStatus()).isEqualTo(consignmentStatus);
        assertThat(result.getStatusDisplay()).isEqualTo(consignmentStatus.getCode());
        assertThat(result.getEntries()).hasSize(1);
        assertThat(result.getEntries().get(0)).isEqualTo(consignmentEntry);
        assertThat(result.getTrackingID()).isEqualTo(TEST_TRACKING_URL + consignmentTrackingId);
    }

    @Test
    public void testPopulateWithNoEntries() {
        final TargetConsignmentModel mockConsignmentModel = mock(TargetConsignmentModel.class);

        final ConsignmentData mockResult = mock(ConsignmentData.class);
        consignmentConverter.populate(mockConsignmentModel, mockResult);

        verifyZeroInteractions(mockResult);
    }

    @Test
    public void testPopulateWithNoStatus() {
        final String consignmentCode = "23568974";
        final String consignmentTrackingId = "784512784512";

        final ConsignmentEntryModel mockConsignmentEntryModel = mock(ConsignmentEntryModel.class);

        final ConsignmentEntryData consignmentEntry = new ConsignmentEntryData();
        given(mockConsignmentEntryConverter.convert(mockConsignmentEntryModel)).willReturn(consignmentEntry);

        final OrderModel mockOrderModel = Mockito.mock(OrderModel.class);
        given(mockOrderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);

        final TargetConsignmentModel mockConsignmentModel = mock(TargetConsignmentModel.class);
        given(mockConsignmentModel.getConsignmentEntries())
                .willReturn(Collections.singleton(mockConsignmentEntryModel));
        given(mockConsignmentModel.getCode()).willReturn(consignmentCode);
        given(mockConsignmentModel.getTrackingID()).willReturn(consignmentTrackingId);
        given(mockConsignmentModel.getOrder()).willReturn(mockOrderModel);
        given(mockConsignmentModel.getTargetCarrier()).willReturn(carrier);

        final ConsignmentData result = new ConsignmentData();
        consignmentConverter.populate(mockConsignmentModel, result);

        assertThat(result.getCode()).isEqualTo(consignmentCode);
        assertThat(result.getStatus()).isNull();
        assertThat(result.getStatusDisplay()).isNull();
        assertThat(result.getEntries()).hasSize(1);
        assertThat(result.getEntries().get(0)).isEqualTo(consignmentEntry);
        assertThat(result.getTrackingID()).isEqualTo(TEST_TRACKING_URL + consignmentTrackingId);
        verifyZeroInteractions(mockEnumerationService);
    }

    @Test
    public void testPopulateCreatedWithNoTrackingId() {
        final ConsignmentStatus consignmentStatus = ConsignmentStatus.CREATED;
        final String consignmentCode = "23568974";

        final ConsignmentEntryModel mockConsignmentEntryModel = mock(ConsignmentEntryModel.class);

        final ConsignmentEntryData consignmentEntry = new ConsignmentEntryData();
        given(mockConsignmentEntryConverter.convert(mockConsignmentEntryModel)).willReturn(consignmentEntry);

        given(mockEnumerationService.getEnumerationName(consignmentStatus)).willReturn(consignmentStatus.getCode());

        final TargetConsignmentModel mockConsignmentModel = mock(TargetConsignmentModel.class);
        given(mockConsignmentModel.getConsignmentEntries())
                .willReturn(Collections.singleton(mockConsignmentEntryModel));
        given(mockConsignmentModel.getStatus()).willReturn(consignmentStatus);
        given(mockConsignmentModel.getCode()).willReturn(consignmentCode);
        given(mockConsignmentModel.getTargetCarrier()).willReturn(carrier);

        final ConsignmentData result = new ConsignmentData();
        consignmentConverter.populate(mockConsignmentModel, result);

        assertThat(result.getCode()).isEqualTo(consignmentCode);
        assertThat(result.getStatus()).isEqualTo(consignmentStatus);
        assertThat(result.getStatusDisplay()).isEqualTo(consignmentStatus.getCode());
        assertThat(result.getEntries()).hasSize(1);
        assertThat(result.getEntries().get(0)).isEqualTo(consignmentEntry);
        assertThat(result.getTrackingID()).isNull();
    }

    @Test
    public void testPopulateShippedWithNoTrackingIdLayby() {
        final ConsignmentStatus consignmentStatus = ConsignmentStatus.SHIPPED;
        final String consignmentCode = "23568974";

        final ConsignmentEntryModel mockConsignmentEntryModel = mock(ConsignmentEntryModel.class);

        final ConsignmentEntryData consignmentEntry = new ConsignmentEntryData();
        given(mockConsignmentEntryConverter.convert(mockConsignmentEntryModel)).willReturn(consignmentEntry);

        given(mockEnumerationService.getEnumerationName(consignmentStatus)).willReturn(consignmentStatus.getCode());

        final TargetConsignmentModel mockConsignmentModel = mock(TargetConsignmentModel.class);
        given(mockConsignmentModel.getConsignmentEntries())
                .willReturn(Collections.singleton(mockConsignmentEntryModel));
        given(mockConsignmentModel.getStatus()).willReturn(consignmentStatus);
        given(mockConsignmentModel.getCode()).willReturn(consignmentCode);
        given(mockConsignmentModel.getTargetCarrier()).willReturn(carrier);

        given(Boolean.valueOf(mockTargetStoreConsignmentService.isConsignmentAssignedToAnyStore(mockConsignmentModel)))
                .willReturn(Boolean.FALSE);

        final ConsignmentData result = new ConsignmentData();
        consignmentConverter.populate(mockConsignmentModel, result);

        assertThat(result.getCode()).isEqualTo(consignmentCode);
        assertThat(result.getStatus()).isNull();
        assertThat(result.getStatusDisplay()).isNull();
        assertThat(result.getEntries()).hasSize(1);
        assertThat(result.getEntries().get(0)).isEqualTo(consignmentEntry);
        assertThat(result.getTrackingID()).isNull();
    }

    @Test
    public void testPopulateShippedWithNoTrackingIdInstore() {
        final ConsignmentStatus consignmentStatus = ConsignmentStatus.SHIPPED;
        final String consignmentCode = "23568974";

        final ConsignmentEntryModel mockConsignmentEntryModel = mock(ConsignmentEntryModel.class);

        final ConsignmentEntryData consignmentEntry = new ConsignmentEntryData();
        given(mockConsignmentEntryConverter.convert(mockConsignmentEntryModel)).willReturn(consignmentEntry);

        given(mockEnumerationService.getEnumerationName(consignmentStatus)).willReturn(consignmentStatus.getCode());

        final TargetConsignmentModel mockConsignmentModel = mock(TargetConsignmentModel.class);
        given(mockConsignmentModel.getConsignmentEntries())
                .willReturn(Collections.singleton(mockConsignmentEntryModel));
        given(mockConsignmentModel.getStatus()).willReturn(consignmentStatus);
        given(mockConsignmentModel.getCode()).willReturn(consignmentCode);
        given(mockConsignmentModel.getTargetCarrier()).willReturn(carrier);

        given(Boolean.valueOf(mockTargetStoreConsignmentService.isConsignmentAssignedToAnyStore(mockConsignmentModel)))
                .willReturn(Boolean.TRUE);

        final ConsignmentData result = new ConsignmentData();
        consignmentConverter.populate(mockConsignmentModel, result);

        assertThat(result.getCode()).isEqualTo(consignmentCode);
        assertThat(result.getStatus()).isEqualTo(consignmentStatus);
        assertThat(result.getStatusDisplay()).isEqualTo(consignmentStatus.getCode());
        assertThat(result.getEntries()).hasSize(1);
        assertThat(result.getEntries().get(0)).isEqualTo(consignmentEntry);
        assertThat(result.getTrackingID()).isNull();
    }


    @Test
	public void testPopulateShippedDigitalDeliveryConsignments() {
		final ConsignmentStatus consignmentStatus = ConsignmentStatus.SHIPPED;
		final String consignmentCode = "1122";

		final ConsignmentEntryModel mockConsignmentEntryModel = mock(ConsignmentEntryModel.class);

		final ConsignmentEntryData consignmentEntry = new ConsignmentEntryData();

		given(mockConsignmentEntryConverter.convert(mockConsignmentEntryModel)).willReturn(consignmentEntry);

		given(mockEnumerationService.getEnumerationName(consignmentStatus)).willReturn(consignmentStatus.getCode());

		final TargetConsignmentModel mockConsignmentModel = mock(TargetConsignmentModel.class);
		given(mockConsignmentModel.getConsignmentEntries())
				.willReturn(Collections.singleton(mockConsignmentEntryModel));
		given(mockConsignmentModel.getStatus()).willReturn(consignmentStatus);
		given(mockConsignmentModel.getCode()).willReturn(consignmentCode);
		given(mockConsignmentModel.getTargetCarrier()).willReturn(carrier);
		given(mockConsignmentModel.getDeliveryMode()).willReturn(deliveryModeModelMock);
		given(targetDeliveryModeHelper.isDeliveryModeDigital(deliveryModeModelMock)).willReturn(Boolean.TRUE);
		given(Boolean.valueOf(mockTargetStoreConsignmentService.isConsignmentAssignedToAnyStore(mockConsignmentModel)))
				.willReturn(Boolean.FALSE);

		final ConsignmentData result = new ConsignmentData();
		consignmentConverter.populate(mockConsignmentModel, result);

		assertThat(result.getCode()).isEqualTo(consignmentCode);
		assertThat(result.getStatus()).isEqualTo(consignmentStatus);

	}

    @Test
    public void testPopulateWithCancelledOrderStatus() {
        final ConsignmentStatus consignmentStatus = ConsignmentStatus.SHIPPED;
        final String consignmentCode = "23568974";
        final String consignmentTrackingId = "784512784512";

        final ConsignmentEntryModel mockConsignmentEntryModel = mock(ConsignmentEntryModel.class);

        final ConsignmentEntryData consignmentEntry = new ConsignmentEntryData();
        given(mockConsignmentEntryConverter.convert(mockConsignmentEntryModel)).willReturn(consignmentEntry);

        given(mockEnumerationService.getEnumerationName(consignmentStatus)).willReturn(consignmentStatus.getCode());

        final OrderModel mockOrderModel = Mockito.mock(OrderModel.class);
        given(mockOrderModel.getStatus()).willReturn(OrderStatus.CANCELLED);

        final TargetConsignmentModel mockConsignmentModel = mock(TargetConsignmentModel.class);
        given(mockConsignmentModel.getConsignmentEntries())
                .willReturn(Collections.singleton(mockConsignmentEntryModel));
        given(mockConsignmentModel.getStatus()).willReturn(consignmentStatus);
        given(mockConsignmentModel.getCode()).willReturn(consignmentCode);
        given(mockConsignmentModel.getTrackingID()).willReturn(consignmentTrackingId);
        given(mockConsignmentModel.getOrder()).willReturn(mockOrderModel);

        final ConsignmentData result = new ConsignmentData();
        consignmentConverter.populate(mockConsignmentModel, result);


        assertThat(result.getCode()).isEqualTo(consignmentCode);
        assertThat(result.getStatus()).isEqualTo(consignmentStatus);
        assertThat(result.getStatusDisplay()).isEqualTo(consignmentStatus.getCode());
        assertThat(result.getEntries()).hasSize(1);
        assertThat(result.getEntries().get(0)).isEqualTo(consignmentEntry);
        assertThat(result.getTrackingID()).isNull();
    }

    @Test
    public void testPopulateWithCancelledConsignmentStatus() {
        final ConsignmentStatus consignmentStatus = ConsignmentStatus.CANCELLED;
        final String consignmentCode = "23568974";
        final String consignmentTrackingId = "784512784512";

        final ConsignmentEntryModel mockConsignmentEntryModel = mock(ConsignmentEntryModel.class);

        final ConsignmentEntryData consignmentEntry = new ConsignmentEntryData();
        given(mockConsignmentEntryConverter.convert(mockConsignmentEntryModel)).willReturn(consignmentEntry);

        given(mockEnumerationService.getEnumerationName(consignmentStatus)).willReturn(consignmentStatus.getCode());

        final OrderModel mockOrderModel = Mockito.mock(OrderModel.class);
        given(mockOrderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);

        final TargetConsignmentModel mockConsignmentModel = mock(TargetConsignmentModel.class);
        given(mockConsignmentModel.getConsignmentEntries())
                .willReturn(Collections.singleton(mockConsignmentEntryModel));
        given(mockConsignmentModel.getStatus()).willReturn(consignmentStatus);
        given(mockConsignmentModel.getCode()).willReturn(consignmentCode);
        given(mockConsignmentModel.getTrackingID()).willReturn(consignmentTrackingId);
        given(mockConsignmentModel.getOrder()).willReturn(mockOrderModel);

        final ConsignmentData result = new ConsignmentData();
        consignmentConverter.populate(mockConsignmentModel, result);

        assertThat(result.getCode()).isEqualTo(consignmentCode);
        assertThat(result.getStatus()).isEqualTo(consignmentStatus);
        assertThat(result.getStatusDisplay()).isEqualTo(consignmentStatus.getCode());
        assertThat(result.getEntries()).hasSize(1);
        assertThat(result.getEntries().get(0)).isEqualTo(consignmentEntry);
        assertThat(result.getTrackingID()).isNull();
    }

    /**
     * For click and collect delivery mode no tracking id will be set.
     */
    @Test
    public void testPopulateNoTrackingIdForCncDeliveryMode() {
        final TargetConsignmentModel mockConsignmentModel = settingMockData();
        given(mockConsignmentModel.getDeliveryMode()).willReturn(deliveryModeModelMock);
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper)
                .isDeliveryModeStoreDelivery(deliveryModeModelMock);
        final ConsignmentData result = new ConsignmentData();
        consignmentConverter.populate(mockConsignmentModel, result);
        assertThat(result.getTrackingID()).isEmpty();
    }

    /**
     * Verify if the order Consignment has Picked Up Date.
     *
     * @throws ParseException
     */
    @Test
    public void testPopulateWithPickedUpDate() throws ParseException {
        final TargetConsignmentModel mockConsignmentModel = settingMockData();
        given(mockConsignmentModel.getDeliveryMode()).willReturn(deliveryModeModelMock);
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper)
                .isDeliveryModeStoreDelivery(deliveryModeModelMock);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("M-dd-yyyy hh:mm:ss");
        final Date pickedUpDate = simpleDateFormat.parse("04-17-1983 11:59:59");
        given(mockConsignmentModel.getPickedUpDate()).willReturn(pickedUpDate);
        final ConsignmentData result = new ConsignmentData();
        consignmentConverter.populate(mockConsignmentModel, result);
        assertThat(result.getPickedUpDate()).as("Picked Up Date").isEqualTo(pickedUpDate);
    }

    /**
     * Verify if the order Consignment does not have Picked Up Date.
     *
     * @throws ParseException
     */
    @Test
    public void testPopulateWithoutPickedUpDate() throws ParseException {
        final TargetConsignmentModel mockConsignmentModel = settingMockData();
        given(mockConsignmentModel.getDeliveryMode()).willReturn(deliveryModeModelMock);
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper)
                .isDeliveryModeStoreDelivery(deliveryModeModelMock);
        final ConsignmentData result = new ConsignmentData();
        consignmentConverter.populate(mockConsignmentModel, result);
        assertThat(result.getPickedUpDate()).as("Picked Up Date").isNull();
    }

    /**
     * Verify if the order Consignment has Ready for Pick Up Date.
     *
     * @throws ParseException
     */
    @Test
    public void testPopulateWithReadyToPickUpDate() throws ParseException {
        final TargetConsignmentModel mockConsignmentModel = settingMockData();
        given(mockConsignmentModel.getDeliveryMode()).willReturn(deliveryModeModelMock);
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper)
                .isDeliveryModeStoreDelivery(deliveryModeModelMock);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("M-dd-yyyy hh:mm:ss");
        final Date readyForPickUpDate = simpleDateFormat.parse("04-17-1983 11:59:59");
        given(mockConsignmentModel.getReadyForPickUpDate()).willReturn(readyForPickUpDate);
        final ConsignmentData result = new ConsignmentData();
        consignmentConverter.populate(mockConsignmentModel, result);
        assertThat(result.getReadyForPickUpDate()).as("Ready for Pick Up Date").isEqualTo(readyForPickUpDate);
    }

    /**
     * Verify if the order Consignment does not have Ready for Pick Up Date.
     *
     * @throws ParseException
     */
    @Test
    public void testPopulateWithoutReadyToPickUpDate() throws ParseException {
        final TargetConsignmentModel mockConsignmentModel = settingMockData();
        given(mockConsignmentModel.getDeliveryMode()).willReturn(deliveryModeModelMock);
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper)
                .isDeliveryModeStoreDelivery(deliveryModeModelMock);
        final ConsignmentData result = new ConsignmentData();
        consignmentConverter.populate(mockConsignmentModel, result);
        assertThat(result.getReadyForPickUpDate()).as("Ready for Pick Up Date").isNull();
    }

    /**
     * Method will set mock data for the order.
     * 
     * @return TargetConsignmentModel
     */
    private TargetConsignmentModel settingMockData() {
        final ConsignmentEntryModel mockConsignmentEntryModel = mock(ConsignmentEntryModel.class);
        final ConsignmentEntryData consignmentEntry = new ConsignmentEntryData();
        given(mockConsignmentEntryConverter.convert(mockConsignmentEntryModel)).willReturn(consignmentEntry);
        final ConsignmentStatus consignmentStatus = ConsignmentStatus.SHIPPED;
        given(mockEnumerationService.getEnumerationName(consignmentStatus)).willReturn(consignmentStatus.getCode());
        final OrderModel mockOrderModel = mock(OrderModel.class);
        given(mockOrderModel.getStatus()).willReturn(OrderStatus.INPROGRESS);
        final TargetConsignmentModel mockConsignmentModel = mock(TargetConsignmentModel.class);
        given(mockConsignmentModel.getConsignmentEntries())
                .willReturn(Collections.singleton(mockConsignmentEntryModel));
        given(mockConsignmentModel.getStatus()).willReturn(consignmentStatus);
        given(mockConsignmentModel.getCode()).willReturn("23568974");
        given(mockConsignmentModel.getTrackingID()).willReturn("784512784512");
        given(mockConsignmentModel.getOrder()).willReturn(mockOrderModel);
        given(mockConsignmentModel.getTargetCarrier()).willReturn(carrier);
        return mockConsignmentModel;
    }

}

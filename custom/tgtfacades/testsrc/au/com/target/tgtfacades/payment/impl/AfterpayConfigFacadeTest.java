/**
 * 
 */
package au.com.target.tgtfacades.payment.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.payment.AfterpayConfigFacade;
import au.com.target.tgtfacades.payment.data.AfterpayConfigData;
import au.com.target.tgtpayment.model.AfterpayConfigModel;
import au.com.target.tgtpayment.service.AfterpayConfigService;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AfterpayConfigFacadeTest {

    @Mock
    private AfterpayConfigService afterpayConfigService;

    @Mock
    private Converter<AfterpayConfigModel, AfterpayConfigData> afterpayConfigConverter;


    @InjectMocks
    private final AfterpayConfigFacade afterpayConfigFacade = new AfterpayConfigFacadeImpl();

    @Test
    public void testFetchAfterpayConfig() {
        final AfterpayConfigModel afterpayConfigExpected = mock(AfterpayConfigModel.class);
        given(afterpayConfigService.getAfterpayConfig()).willReturn(afterpayConfigExpected);
        final AfterpayConfigData afterpayConfigDataExpected = mock(AfterpayConfigData.class);
        given(afterpayConfigConverter.convert(afterpayConfigExpected)).willReturn(afterpayConfigDataExpected);

        final AfterpayConfigData afterpayConfig = afterpayConfigFacade.getAfterpayConfig();
        assertThat(afterpayConfig).isEqualTo(afterpayConfigDataExpected);
    }

    @Test
    public void testFetchAfterpayConfigNull() {
        final AfterpayConfigData afterpayConfig = afterpayConfigFacade.getAfterpayConfig();
        assertThat(afterpayConfig).isNull();
    }
}

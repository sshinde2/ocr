/**
 * 
 */
package au.com.target.tgtfacades.payment.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import au.com.target.tgtfacades.payment.data.AfterpayConfigData;
import au.com.target.tgtpayment.model.AfterpayConfigModel;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AfterpayConfigPopulatorTest {

    @Mock
    private PriceDataFactory priceDataFactory;

    @InjectMocks
    private final AfterpayConfigPopulator afterpayConfigPopulator = new AfterpayConfigPopulator();

    @Mock
    private AfterpayConfigModel afterpayConfigModel;

    @Before
    public void setup() {
        given(afterpayConfigModel.getPaymentType()).willReturn("PAY_BY_INSTALLMENT");
        given(afterpayConfigModel.getDescription()).willReturn("Pay over time");
        given(afterpayConfigModel.getMinimumAmount()).willReturn(BigDecimal.valueOf(50));
        given(afterpayConfigModel.getMaximumAmount()).willReturn(BigDecimal.valueOf(1000));
        given(priceDataFactory.create(Mockito.any(PriceDataType.class), Mockito.any(BigDecimal.class),
                Mockito.anyString())).willAnswer(new Answer<PriceData>() {

                    @Override
                    public PriceData answer(final InvocationOnMock invocation) throws Throwable {
                        final PriceData priceData = mock(PriceData.class);
                        given(priceData.getValue()).willReturn((BigDecimal)invocation.getArguments()[1]);
                        return priceData;
                    }
                });
    }

    @Test
    public void testPopulate() {
        final AfterpayConfigData afterpayConfigData = new AfterpayConfigData();
        afterpayConfigPopulator.populate(afterpayConfigModel, afterpayConfigData);
        assertThat(afterpayConfigData.getPaymentType()).isEqualTo("PAY_BY_INSTALLMENT");
        assertThat(afterpayConfigData.getDescription()).isEqualTo("Pay over time");
        assertThat(afterpayConfigData.getMinimumAmount().getValue()).isEqualByComparingTo(BigDecimal.valueOf(50));
        assertThat(afterpayConfigData.getMaximumAmount().getValue()).isEqualByComparingTo(BigDecimal.valueOf(1000));
    }

}

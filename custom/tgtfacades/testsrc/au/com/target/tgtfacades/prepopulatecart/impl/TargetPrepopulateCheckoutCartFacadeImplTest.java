/**
 * 
 */



package au.com.target.tgtfacades.prepopulatecart.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.cart.TargetCartService;
import au.com.target.tgtcore.customer.TargetCustomerAccountService;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.SavedCncStoreDetailsModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.user.TargetUserFacade;


/**
 * @author pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPrepopulateCheckoutCartFacadeImplTest {

    private static final String PREFERRED_DELIVERY_MODE_CODE = "home-delivery";

    private static final String NON_PREFERRED_DELIVERY_MODE_CODE = "click-and-collect";

    @InjectMocks
    private final TargetPrepopulateCheckoutCartFacadeImpl targetPrepopulateCheckoutCartFacadeImpl = new TargetPrepopulateCheckoutCartFacadeImpl();

    @Mock
    private CartModel cartModel;

    @Mock
    private TargetSalesApplicationService targetSalesApplicationService;

    @Mock
    private TargetOrderService targetOrderService;

    @Mock
    private List<CreditCardPaymentInfoModel> paymentInfos;

    @Mock
    private AddressModel paymentAddress;

    @Mock
    private AddressModel shippingAddress;

    @Mock
    private TargetZoneDeliveryModeModel preferredDeliveryMode;

    @Mock
    private OrderModel previousOrder;

    @Mock
    private TargetCheckoutFacade targetCheckoutFacade;

    @Mock
    private TargetUserFacade targetUserFacade;

    @Mock
    private UserModel userModel;

    @Mock
    private TargetCustomerModel customerModel;

    @Mock
    private TargetCustomerAccountService targetCustomerAccountService;

    @Mock
    private TargetZoneDeliveryModeModel previousOrderDeliveryMode;

    @Mock
    private CreditCardPaymentInfoModel creditCardInfo;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetPointOfServiceService targetPointOfServiceService;

    @Mock
    private TargetPointOfServiceModel targetPointOfServiceModel;

    @Mock
    private TargetCartService targetCartService;

    @Mock
    private Collection<SavedCncStoreDetailsModel> cncDetails;

    @Mock
    private SavedCncStoreDetailsModel storeDetail;

    @Mock
    private PaymentModeModel paymentMode;

    @Mock
    private TargetDeliveryService targetDeliveryService;

    @Mock
    private List<AbstractTargetZoneDeliveryModeValueModel> deliveryValues;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Before
    public void setUp() throws Exception {
        given(targetCheckoutFacade.getCart()).willReturn(cartModel);
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        given(Integer.valueOf(paymentInfos.size())).willReturn(Integer.valueOf(1));
        given(cartModel.getDeliveryMode()).willReturn(null);
        given(cartModel.getDeliveryAddress()).willReturn(null);
        given(cartModel.getPaymentMode()).willReturn(null);
        given(Boolean.valueOf(targetUserFacade.isAnonymousUser())).willReturn(Boolean.FALSE);
        given(cartModel.getUser()).willReturn(customerModel);
        given(targetCustomerAccountService.getCreditCardPaymentInfos(customerModel, true)).willReturn(paymentInfos);
        given(targetOrderService.findLatestOrderForUser(customerModel)).willReturn(previousOrder);
        given(previousOrder.getDeliveryMode()).willReturn(previousOrderDeliveryMode);
        given(customerModel.getDefaultPaymentAddress()).willReturn(paymentAddress);
        given(customerModel.getDefaultShipmentAddress()).willReturn(shippingAddress);
        given(paymentInfos.get(0)).willReturn(creditCardInfo);
        given(customerModel.getPreferredDeliveryMode()).willReturn(preferredDeliveryMode);
        given(Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                preferredDeliveryMode))).willReturn(Boolean.TRUE);
        given(preferredDeliveryMode.getActive()).willReturn(Boolean.TRUE);
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(preferredDeliveryMode, cartModel))
                .willReturn(deliveryValues);
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(previousOrderDeliveryMode,
                cartModel)).willReturn(deliveryValues);
        given(Integer.valueOf(deliveryValues.size())).willReturn(Integer.valueOf(2));
        given(Boolean.valueOf(targetFeatureSwitchFacade.shouldPrepopulateCheckoutFields()))
                .willReturn(Boolean.TRUE);
    }

    /**
     * | del-mode-set - yes | kiosk - yes| result - no|
     */
    @Test
    public void testIsCartPrePopulatedWhenCartHasDeliveryModeSet() {
        given(cartModel.getDeliveryAddress()).willReturn(shippingAddress);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout()).isFalse();
    }

    /**
     * | del-mode-set - no | payment-mode - yes | kiosk - yes| result - no|
     */
    @Test
    public void testIsCartPrePopulatedWhenCartHasPaymentModeSet() {
        given(cartModel.getPaymentMode()).willReturn(paymentMode);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout()).isFalse();
    }

    /**
     * | kiosk - yes| result - no|
     */
    @Test
    public void testIsCartPrePopulatedForCheckoutForKioskApp() {
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.KIOSK);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout()).isFalse();
    }

    /**
     * | kiosk - no | user-Anonymous - yes | result - no |
     */
    @Test
    public void testIsCartPrePopulatedForCheckoutForAnonymousUser() {
        given(Boolean.valueOf(targetUserFacade.isAnonymousUser())).willReturn(Boolean.TRUE);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout());
    }

    /**
     * | kiosk - no | user-Anonymous - no | non-target-user - yes | result - no |
     */
    @Test
    public void testIsCartPrePopulatedForCheckoutNonTargetCustomer() {
        given(cartModel.getUser()).willReturn(userModel);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout()).isFalse();
    }

    /**
     * | kiosk - no | user-Anonymous - no | non-target-user - no | payment-info - no | result - no |
     */
    @Test
    public void testIsCartPrePopulatedForCheckoutWithNoPaymentInfo() {
        given(targetCustomerAccountService.getCreditCardPaymentInfos(customerModel, true)).willReturn(null);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout()).isFalse();
    }

    /**
     * | kiosk - no | user-Anonymous - no | non-target-user - no | payment-info - yes | preferred-del-mode - no |
     * previous-order - no | result - no |
     */
    @Test
    public void testIsCartPrePopulatedForCheckoutForUserWithMissingPreferredDeliveryModeAndPreviousOrder() {
        given(customerModel.getPreferredDeliveryMode()).willReturn(null);
        given(targetOrderService.findLatestOrderForUser(customerModel)).willReturn(null);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout()).isFalse();
    }

    /**
     * | kiosk - no | user-Anonymous - no | non-target-user - no | payment-info - yes | preferred-del-mode - yes |
     * previous-order - no | result - no |
     */
    @Test
    public void testIsCartPrePopulatedWhenPreferredDeliveryModeIsInactive() {
        given(cartModel.getDeliveryMode()).willReturn(preferredDeliveryMode);
        given(preferredDeliveryMode.getCode()).willReturn(PREFERRED_DELIVERY_MODE_CODE);
        given(cartModel.getDeliveryMode()).willReturn(preferredDeliveryMode);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout()).isFalse();
    }

    /**
     * | kiosk - no | user-Anonymous - no | non-target-user - no | payment-info - yes | preferred-del-mode - no |
     * previous-order - no | conflicting-products - Yes | result - no |
     */
    @Test
    public void testIsCartPrePopulatedForCheckoutForUserConflictionProducts() {
        given(Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                preferredDeliveryMode))).willReturn(Boolean.FALSE);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout()).isFalse();
    }

    /**
     * | kiosk - no | user-Anonymous - no | non-target-user - no | payment-info - yes | preferred-del-mode - yes |
     * del-mode-enabled - no | previous-order - no | conflicting-products - no | order-type - non-cnc | result - yes |
     */
    @Test
    public void testIsCartPrePopulatedForCheckoutForUserWithPreferredDeliveryModeEnabled() {
        given(targetOrderService.findLatestOrderForUser(customerModel)).willReturn(null);
        given(preferredDeliveryMode.getIsEligibleForSinglePageCheckout()).willReturn(Boolean.TRUE);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout());
        verify(targetCartService).prePopulateCart(cartModel, preferredDeliveryMode, customerModel,
                null, null);
    }

    /**
     * | kiosk - no | user-Anonymous - no | non-target-user - no | payment-info - yes | preferred-del-mode - yes |
     * del-mode-enabled - yes | previous-order - no | conflicting-products - no | order-type - non-cnc | result - yes |
     */
    @Test
    public void testIsCartPrePopulatedForCheckoutForUserWithPreferredDeliveryModeDisabled() {
        given(targetOrderService.findLatestOrderForUser(customerModel)).willReturn(null);
        given(preferredDeliveryMode.getIsEligibleForSinglePageCheckout()).willReturn(Boolean.FALSE);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout()).isFalse();
    }

    /**
     * | kiosk - no | user-Anonymous - no | non-target-user - no | payment-info - yes | preferred-del-mode - no |
     * del-mode-enabled - yes | previous-order - yes | conflicting-products - no | order-type - cnc | result - yes |
     */
    @Test
    public void testIsCartPrePopulatedForCheckoutForCnCOrder() {
        given(preferredDeliveryMode.getIsEligibleForSinglePageCheckout()).willReturn(Boolean.TRUE);
        given(customerModel.getPreferredDeliveryMode()).willReturn(preferredDeliveryMode);
        given(preferredDeliveryMode.getIsDeliveryToStore()).willReturn(Boolean.TRUE);
        given(customerModel.getSavedCncStoreDetails()).willReturn(cncDetails);
        given(storeDetail.getStore()).willReturn(targetPointOfServiceModel);
        given(targetPointOfServiceModel.getStoreNumber()).willReturn(Integer.valueOf(100));
        given(storeDetail.getPhoneNumber()).willReturn("123");
        given(Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                previousOrderDeliveryMode))).willReturn(Boolean.TRUE);
        given(previousOrder.getCncStoreNumber()).willReturn(Integer.valueOf(100));
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout()).isTrue();
        verify(targetCartService).prePopulateCart(cartModel, preferredDeliveryMode, customerModel,
                null, previousOrder);
    }

    /**
     * | kiosk - no | user-Anonymous - no | non-target-user - no | payment-info - yes | preferred-del-mode - yes | cnc -
     * no | del-mode-enabled - Yes | previous-order - yes | conflicting-products - no | order-type - non-cnc | result -
     * yes |
     */
    @Test
    public void testIsCartPrePopulatedForCheckoutForNonCnCOrder() {
        given(previousOrderDeliveryMode.getIsEligibleForSinglePageCheckout()).willReturn(Boolean.TRUE);
        given(previousOrderDeliveryMode.getActive()).willReturn(Boolean.TRUE);
        given(customerModel.getPreferredDeliveryMode()).willReturn(null);
        given(Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                previousOrderDeliveryMode))).willReturn(Boolean.TRUE);
        given(previousOrder.getCncStoreNumber()).willReturn(null);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout()).isTrue();
        verify(targetCartService).prePopulateCart(cartModel, previousOrderDeliveryMode, customerModel,
                null, previousOrder);
    }

    /**
     * | kiosk - no | user-Anonymous - no | non-target-user - no | payment-info - yes | preferred-del-mode - yes | cnc -
     * no | del-mode-enabled - Yes | previous-order - yes | conflicting-products - no | order-type - non-cnc | del-mode
     * - disabled | result - yes |
     */
    @Test
    public void testIsCartPrePopulatedForCheckoutForDisabledDeliveryMode() {
        given(previousOrderDeliveryMode.getIsEligibleForSinglePageCheckout()).willReturn(Boolean.TRUE);
        given(previousOrderDeliveryMode.getActive()).willReturn(Boolean.FALSE);
        given(customerModel.getPreferredDeliveryMode()).willReturn(null);
        given(Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                previousOrderDeliveryMode))).willReturn(Boolean.TRUE);
        given(previousOrder.getCncStoreNumber()).willReturn(null);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout()).isFalse();
    }

    /**
     * | kiosk - no | user-Anonymous - no | non-target-user - no | payment-info - yes | preferred-del-mode - no |
     * del-mode-enabled - no | previous-order - yes | conflicting-products - no | order-type - cnc | result - yes |
     */
    @Test
    public void testIsCartPrePopulatedForCheckoutForCnCOrderWithNoPreferredDelMode() {
        given(previousOrderDeliveryMode.getIsEligibleForSinglePageCheckout()).willReturn(Boolean.TRUE);
        given(previousOrderDeliveryMode.getActive()).willReturn(Boolean.TRUE);
        given(customerModel.getPreferredDeliveryMode()).willReturn(null);
        given(previousOrderDeliveryMode.getIsDeliveryToStore()).willReturn(Boolean.TRUE);
        given(previousOrder.getCncStoreNumber()).willReturn(Integer.valueOf(100));
        given(previousOrder.getDeliveryAddress()).willReturn(shippingAddress);
        given(shippingAddress.getPhone1()).willReturn("123");
        given(Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                previousOrderDeliveryMode))).willReturn(Boolean.TRUE);
        given(previousOrder.getCncStoreNumber()).willReturn(Integer.valueOf(100));
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout()).isTrue();
        verify(targetCartService).prePopulateCart(cartModel, previousOrderDeliveryMode, customerModel,
                null, previousOrder);
    }

    /**
     * | kiosk - no | delivery-mode - yes - preferred |user-Anonymous - no | non-target-user - no | payment-info - yes |
     * preferred-del-mode - no | del-mode-enabled - no | previous-order - yes | conflicting-products - no | order-type -
     * cnc | result - yes |
     */
    @Test
    public void testIsCartPrePopulatedForCheckoutWhenDeliveryModeIsSet() {
        given(previousOrderDeliveryMode.getIsEligibleForSinglePageCheckout()).willReturn(Boolean.TRUE);
        given(previousOrderDeliveryMode.getActive()).willReturn(Boolean.TRUE);
        given(customerModel.getPreferredDeliveryMode()).willReturn(null);
        given(previousOrderDeliveryMode.getIsDeliveryToStore()).willReturn(Boolean.TRUE);
        given(previousOrder.getCncStoreNumber()).willReturn(Integer.valueOf(100));
        given(previousOrder.getDeliveryAddress()).willReturn(shippingAddress);
        given(shippingAddress.getPhone1()).willReturn("123");
        given(Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                previousOrderDeliveryMode))).willReturn(Boolean.TRUE);
        given(previousOrder.getCncStoreNumber()).willReturn(Integer.valueOf(100));
        given(cartModel.getDeliveryMode()).willReturn(preferredDeliveryMode);
        given(preferredDeliveryMode.getCode()).willReturn(PREFERRED_DELIVERY_MODE_CODE);
        given(previousOrderDeliveryMode.getCode()).willReturn(PREFERRED_DELIVERY_MODE_CODE);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout()).isTrue();
        verify(targetCartService).prePopulateCart(cartModel, previousOrderDeliveryMode, customerModel,
                null, previousOrder);
    }

    /**
     * | kiosk - no | delivery-mode - yes - non-preferred |user-Anonymous - no | non-target-user - no | payment-info -
     * yes | preferred-del-mode - no | del-mode-enabled - no | previous-order - yes | conflicting-products - no |
     * order-type - cnc | result - yes |
     */
    @Test
    public void testIsCartPrePopulatedForCheckoutWhenDeliveryModeIsSetButIsNotPreferred() {
        given(previousOrderDeliveryMode.getIsEligibleForSinglePageCheckout()).willReturn(Boolean.TRUE);
        given(previousOrderDeliveryMode.getActive()).willReturn(Boolean.TRUE);
        given(customerModel.getPreferredDeliveryMode()).willReturn(null);
        given(previousOrderDeliveryMode.getIsDeliveryToStore()).willReturn(Boolean.TRUE);
        given(previousOrder.getCncStoreNumber()).willReturn(Integer.valueOf(100));
        given(previousOrder.getDeliveryAddress()).willReturn(shippingAddress);
        given(shippingAddress.getPhone1()).willReturn("123");
        given(Boolean.valueOf(targetDeliveryModeHelper.isDeliveryModeApplicableForOrder(cartModel,
                previousOrderDeliveryMode))).willReturn(Boolean.TRUE);
        given(previousOrder.getCncStoreNumber()).willReturn(Integer.valueOf(100));
        given(cartModel.getDeliveryMode()).willReturn(preferredDeliveryMode);
        given(preferredDeliveryMode.getCode()).willReturn(PREFERRED_DELIVERY_MODE_CODE);
        given(previousOrderDeliveryMode.getCode()).willReturn(NON_PREFERRED_DELIVERY_MODE_CODE);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout()).isFalse();
    }

    /**
     * | kiosk - no | delivery-mode - yes - preferred |user-Anonymous - no | non-target-user - no | payment-info - yes |
     * preferred-del-mode - yes | del-mode-enabled - yes | previous-order - yes | conflicting-products - no | order-type
     * - cnc | delivery-costs - no | result - no |
     */
    @Test
    public void tesIsCartPrePopulatedForCheckoutWhenDeliveryCostForModeDoesntExist() {
        given(preferredDeliveryMode.getIsEligibleForSinglePageCheckout()).willReturn(Boolean.TRUE);
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(preferredDeliveryMode, cartModel))
                .willReturn(null);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout()).isFalse();
    }


    @Test
    public void testIsCartPrePopulatedForCheckoutWhenNoPostCodeExceptionIsThrown() {
        given(preferredDeliveryMode.getIsEligibleForSinglePageCheckout()).willReturn(Boolean.TRUE);
        given(targetDeliveryService.getAllApplicableDeliveryValuesForModeAndOrder(preferredDeliveryMode, cartModel))
                .willThrow(new TargetNoPostCodeException("no Postcode"));
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout()).isTrue();
    }

    @Test
    public void testIsCartNotPrePopulatedWhenFeatureIsNotEnabled() {
        given(Boolean.valueOf(targetFeatureSwitchFacade.shouldPrepopulateCheckoutFields())).willReturn(Boolean.FALSE);
        given(targetCheckoutFacade.getCart()).willReturn(null);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.isCartPrePopulatedForCheckout()).isFalse();
    }

    @Test
    public void testGetPreferredDeliveryModeFromCustomer() {
        given(customerModel.getPreferredDeliveryMode()).willReturn(preferredDeliveryMode);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.getPreferredDeliveryMode(customerModel, previousOrder))
                .isEqualTo(preferredDeliveryMode);
    }

    @Test
    public void testGetPreferredDeliveryModeFromCustomerWithNoPriorOrder() {
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.getPreferredDeliveryMode(customerModel, null))
                .isEqualTo(preferredDeliveryMode);
    }

    @Test
    public void testGetPreferredDeliveryModeFromPreviousOrder() {
        given(customerModel.getPreferredDeliveryMode()).willReturn(null);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.getPreferredDeliveryMode(customerModel, previousOrder))
                .isEqualTo(previousOrderDeliveryMode);
    }

    @Test
    public void testGetPreferredDeliveryModeNull() {
        given(customerModel.getPreferredDeliveryMode()).willReturn(null);
        assertThat(targetPrepopulateCheckoutCartFacadeImpl.getPreferredDeliveryMode(customerModel, null))
                .isNull();
    }

}

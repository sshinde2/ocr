/**
 * 
 */
package au.com.target.tgtfacades.process.email.context;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.spy;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.urlencoder.UrlEncoderService;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commons.model.DocumentModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.email.context.AbstractTargetEmailContext;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.taxinvoice.TaxInvoiceService;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.process.email.data.ClickAndCollectEmailData;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CncNotificationEmailContextTest {

    @Mock
    private OrderProcessModel businessProcessModel;

    @Mock
    private EmailPageModel emailPageModel;

    @InjectMocks
    private final CncNotificationEmailContext context = new CncNotificationEmailContext();

    @Mock
    private TaxInvoiceOrderEmailContext taxInvoiceOrderEmailContext;

    @Mock
    private BaseSiteModel siteModel;

    @Mock
    private OrderModel orderModel;

    @Mock
    private AbstractTargetEmailContext abstractEmailContext;

    @Mock
    private CustomerEmailContext customerEmailContext;

    @Mock
    private UrlEncoderService urlEncoderService;

    @Mock
    private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

    @Mock
    private Converter<UserModel, CustomerData> customerConverter;

    @Mock
    private Converter<OrderModel, TargetOrderData> orderModelConverter;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Mock
    private TaxInvoiceService taxInvoiceService;

    @Mock
    private DocumentModel taxInvoiceMedia;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private ClickAndCollectEmailData cncNotificationData;

    @Mock
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Mock
    private TargetPointOfServiceData selectedStore;

    @Mock
    private ConsignmentData consignmentData1;

    @Mock
    private ConsignmentData consignmentData2;

    @Mock
    private TargetOrderData order;

    @Mock
    private ProductData product1, product2;

    @Mock
    private OrderEntryData orderEntry1;

    @Mock
    private OrderEntryData orderEntry2;

    @Mock
    private OrderEntryData orderEntry3;

    @Mock
    private OrderEntryData orderEntry4;

    private final List<ConsignmentData> consignmentDatas = spy(new ArrayList<ConsignmentData>());

    private final List<ConsignmentEntryData> consignmentEntries = spy(new ArrayList<ConsignmentEntryData>());

    private final List<ConsignmentEntryData> consignmentEntries1 = spy(new ArrayList<ConsignmentEntryData>());

    @Mock
    private ConsignmentEntryData consignmentEntryData4;

    private final List<ConsignmentEntryData> consignmentEntryDataList2 = spy(new ArrayList<ConsignmentEntryData>());

    @Mock
    private ConsignmentEntryData consignmentEntryData1, consignmentEntryData2, consignmentEntryData3;

    private final List<ConsignmentEntryData> consignmentEntryDataList = spy(new ArrayList<ConsignmentEntryData>());

    @Before
    public void setUp() {
        context.setUrlEncoderService(urlEncoderService);
        context.setSiteBaseUrlResolutionService(siteBaseUrlResolutionService);
        context.setOrderConverter(orderModelConverter);
        context.setTargetDeliveryModeHelper(targetDeliveryModeHelper);
        context.setOrderProcessParameterHelper(orderProcessParameterHelper);
        context.setTargetStoreLocatorFacade(targetStoreLocatorFacade);
        context.setSelectedStore(selectedStore);

        createOrderData();

        given(businessProcessModel.getOrder()).willReturn(orderModel);
        given(businessProcessModel.getOrder().getSite()).willReturn(siteModel);
        given(urlEncoderService.getUrlEncodingPatternForEmail(businessProcessModel)).willReturn("");
        given(orderModelConverter.convert(orderModel)).willReturn(order);
        given(taxInvoiceService.getTaxInvoiceForOrder(orderModel)).willReturn(taxInvoiceMedia);
        given(context.getOrderProcessParameterHelper().getCncNotificationData(businessProcessModel))
                .willReturn(cncNotificationData);
        given(cncNotificationData.getStoreNumber()).willReturn("252");
        given(targetStoreLocatorFacade.getPointOfService(new Integer(252)))
                .willReturn(selectedStore);
    }

    @Test
    public void testConsolidateOrderPerProduct() {
        context.init(businessProcessModel, emailPageModel);
        assertThat(consignmentDatas.size()).isEqualTo(2);

        final ConsignmentData consolidatedConsignmentData1 = consignmentDatas.get(0);
        assertThat(consolidatedConsignmentData1.getStatus()).isEqualTo(ConsignmentStatus.SHIPPED);
        assertThat(consolidatedConsignmentData1.getCode()).isEqualTo("code1");
        assertThat(consolidatedConsignmentData1.getEntries()).hasSize(2);

        final ConsignmentEntryData extractedConsignmentEntryData1 = consolidatedConsignmentData1.getEntries().get(0);
        assertThat(extractedConsignmentEntryData1.getShippedQuantity()).isEqualTo(2);

        final OrderEntryData extractedOrderEntry = extractedConsignmentEntryData1.getOrderEntry();
        assertThat(extractedOrderEntry.getProduct().getCode()).isEqualTo("p2");

        final ConsignmentEntryData extractedConsignmentEntryData2 = consignmentData1.getEntries().get(1);
        assertThat(extractedConsignmentEntryData2.getShippedQuantity()).isEqualTo(1);

        final ConsignmentData consolidateedConsignmentData2 = consignmentDatas.get(1);
        assertThat(consolidateedConsignmentData2.getStatus()).isEqualTo(ConsignmentStatus.SHIPPED);

        assertThat(consolidateedConsignmentData2.getStatus()).isEqualTo(ConsignmentStatus.SHIPPED);
        assertThat(consolidateedConsignmentData2.getCode()).isEqualTo("code1");
        assertThat(consolidateedConsignmentData2.getEntries()).hasSize(0);
    }

    private void createOrderData() {

        given(product1.getCode()).willReturn("p1");
        given(product1.getCode()).willReturn("p2");

        //Prepare Consignment 1
        orderEntry1.setEntryNumber(1);
        orderEntry1.setQuantity(2L);
        given(orderEntry1.getProduct()).willReturn(product1);

        orderEntry2.setEntryNumber(1);
        orderEntry2.setQuantity(1L);
        orderEntry2.setProduct(product2);
        given(orderEntry2.getProduct()).willReturn(product2);

        final Calendar cal1 = Calendar.getInstance();
        cal1.set(Calendar.HOUR_OF_DAY, 8);

        given(order.getCncStoreNumber()).willReturn(new Integer(252));
        given(consignmentEntryData1.getShippedQuantity()).willReturn(new Long(2L));
        given(consignmentEntryData1.getOrderEntry()).willReturn(orderEntry1);
        consignmentEntryDataList.add(consignmentEntryData1);

        given(consignmentEntryData2.getShippedQuantity()).willReturn(new Long(1L));
        given(consignmentEntryData2.getOrderEntry()).willReturn(orderEntry2);

        consignmentEntryDataList.add(consignmentEntryData2);

        given(consignmentData1.getCode()).willReturn("code1");
        cal1.set(Calendar.HOUR_OF_DAY, 9);
        given(consignmentData1.getStatusDate()).willReturn(cal1.getTime());
        given(consignmentData1.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        given(consignmentData1.getEntries()).willReturn(consignmentEntryDataList);

        consignmentEntries.add(consignmentEntryData1);
        consignmentEntries.add(consignmentEntryData2);
        consignmentDatas.add(consignmentData1);

        //Prepare Consignment 2
        orderEntry3.setEntryNumber(1);
        orderEntry3.setQuantity(2L);
        orderEntry3.setProduct(product1);
        given(orderEntry3.getProduct()).willReturn(product1);

        orderEntry4.setEntryNumber(1);
        orderEntry4.setQuantity(1L);
        orderEntry4.setProduct(product2);
        given(orderEntry4.getProduct()).willReturn(product2);

        final Calendar cal2 = Calendar.getInstance();
        cal2.set(Calendar.HOUR_OF_DAY, 10);

        consignmentEntryData3.setShippedQuantity(2L);
        consignmentEntryData3.setOrderEntry(orderEntry3);

        consignmentEntryData4.setShippedQuantity(1L);
        consignmentEntryData4.setOrderEntry(orderEntry4);

        given(consignmentData2.getCode()).willReturn("code1");

        cal1.set(Calendar.HOUR_OF_DAY, 11);
        given(consignmentData2.getStatusDate()).willReturn(cal1.getTime());
        given(consignmentData2.getStatus()).willReturn(ConsignmentStatus.SHIPPED);

        consignmentEntryDataList2.add(consignmentEntryData3);
        consignmentEntryDataList2.add(consignmentEntryData4);

        consignmentData2.setEntries(consignmentEntryDataList2);

        consignmentEntries1.add(consignmentEntryData1);
        consignmentEntries1.add(consignmentEntryData2);

        consignmentDatas.add(consignmentData2);
        order.setConsignment(consignmentDatas);
        given(order.getConsignment()).willReturn(consignmentDatas);
    }
}

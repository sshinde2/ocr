/**
 * 
 */
package au.com.target.tgtfacades.process.email.context;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.CustomerEmailResolutionService;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CustomerEmailContextTest {

    @InjectMocks
    @Spy
    private final CustomerEmailContext context = new CustomerEmailContext();
    @Mock
    private BusinessProcessModel businessProcessModel;
    @Mock
    private StoreFrontCustomerProcessModel customerProcessModel;
    @Mock
    private EmailPageModel emailPageModel;
    @Mock
    private CustomerModel customerModel;
    @Mock
    private CustomerData customerData;
    @Mock
    private Converter<UserModel, CustomerData> customerConverter;
    @Mock
    private CustomerEmailResolutionService customerEmailResolutionService;

    @Before
    public void setUp() {
        when(customerProcessModel.getCustomer()).thenReturn(customerModel);
        when(context.getCustomerConverter()).thenReturn(customerConverter);
        when(customerConverter.convert(customerModel)).thenReturn(customerData);
    }

    @Test
    public void testInitWithNormalProcess() {
        context.init(businessProcessModel, emailPageModel);
        verifyZeroInteractions(customerConverter);
    }

    @Test
    public void testInitWithCustomerProcess() {
        context.init(customerProcessModel, emailPageModel);
        verify(customerConverter).convert(customerModel);
    }
}

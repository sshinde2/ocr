/**
 * 
 */
package au.com.target.tgtfacades.process.email.context;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontProcessModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtpayment.dto.GiftCardReversalData;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GiftCardReversalEmailContextTest {

    @InjectMocks
    @Spy
    private final GiftCardReversalEmailContext context = new GiftCardReversalEmailContext();

    @Mock
    private StoreFrontProcessModel processModel;
    @Mock
    private EmailPageModel emailPageModel;
    @Mock
    private OrderProcessParameterHelper helper;
    @Mock
    private GiftCardReversalData reversalData;

    @Before
    public void setUp() {
        doReturn(helper).when(context).getOrderProcessParameterHelper();
    }

    @Test
    public void itShouldGetEmailFromReversalData() {
        when(helper.getGiftCardReversalData(processModel)).thenReturn(reversalData);
        context.init(processModel, emailPageModel);
        assertEquals(reversalData, context.getReversalData());
    }
}

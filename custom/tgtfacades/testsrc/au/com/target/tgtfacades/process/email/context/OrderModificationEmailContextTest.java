/**
 * 
 */
package au.com.target.tgtfacades.process.email.context;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import java.math.BigDecimal;
import java.util.Collections;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtfacades.order.converters.OrderEntryModificationRecordEntryConverter;
import au.com.target.tgtfacades.order.data.OrderModificationData;
import au.com.target.tgtfacades.order.data.TargetOrderData;


/**
 * Unit test for {@link OrderModificationEmailContext}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderModificationEmailContextTest {

    @Mock
    private OrderEntryModificationRecordEntryConverter orderEntryModificationRecordEntryConverter;

    @Mock
    private PriceDataFactory priceDataFactory;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    // Don't want all the mocks injected, otherwise the prices get set by PriceData
    private final OrderModificationEmailContext context = new OrderCancellationEmailContext();

    @Mock
    private OrderModificationRecordEntryModel orderCancelRecordEntry;

    @Mock
    private OrderEntryModificationRecordEntryModel orderEntryModificationRecordEntryModel;

    @Mock
    private OrderModificationData orderModificationData;

    @Mock
    private OrderProcessModel process;

    @Mock
    private CurrencyModel currency;

    @Mock
    private PriceData priceData;

    @Mock
    private TargetOrderData order;


    @Before
    public void setup() {

        context.setPriceDataFactory(priceDataFactory);
        context.setOrderEntryModificationRecordEntryConverter(orderEntryModificationRecordEntryConverter);
        context.setOrderProcessParameterHelper(orderProcessParameterHelper);

        Mockito.when(orderCancelRecordEntry.getOrderEntriesModificationEntries()).thenReturn(
                Collections.singletonList(orderEntryModificationRecordEntryModel));

        // Mock order for logging
        Mockito.when(process.getCode()).thenReturn("123");

        // Converter will return one modification entry in the converted data
        Mockito.when(orderEntryModificationRecordEntryConverter.convert(orderEntryModificationRecordEntryModel))
                .thenReturn(orderModificationData);

        // Mock PriceData from the factory
        Mockito.when(
                priceDataFactory.create(Mockito.any(PriceDataType.class), Mockito.any(BigDecimal.class),
                        Mockito.anyString()))
                .thenReturn(priceData);

    }

    @Test
    public void testReadOrderEntryModificationDataNullOrderEntry() {
        Mockito.when(orderEntryModificationRecordEntryModel.getOrderEntry()).thenReturn(null);

        context.readOrderEntryModificationData(orderCancelRecordEntry);
        Assert.assertTrue(CollectionUtils.isEmpty(context.getOrderModificationDataList()));
    }

    @Test
    public void testReadOrderEntryModificationData() {
        final OrderEntryModel entry = Mockito.mock(OrderEntryModel.class);
        Mockito.when(orderEntryModificationRecordEntryModel.getOrderEntry()).thenReturn(entry);

        context.readOrderEntryModificationData(orderCancelRecordEntry);
        Assert.assertTrue(CollectionUtils.isNotEmpty(context.getOrderModificationDataList()));
        Assert.assertEquals(orderModificationData, context.getOrderModificationDataList().get(0));
    }

    @Test
    public void testReadRefundNotInProcess() {
        Mockito.when(orderProcessParameterHelper.getRefundAmount(process)).thenReturn(null);
        context.readRefundAmount(process, currency);
        Assert.assertNull(context.getRefundAmount());
    }

    @Test
    public void testReadRefund() {
        Mockito.when(orderProcessParameterHelper.getRefundAmount(process)).thenReturn(BigDecimal.valueOf(10d));
        context.readRefundAmount(process, currency);
        Assert.assertEquals(priceData, context.getRefundAmount());
    }

}

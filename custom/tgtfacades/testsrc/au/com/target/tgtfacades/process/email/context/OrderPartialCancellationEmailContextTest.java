/**
 * 
 */
package au.com.target.tgtfacades.process.email.context;

import static org.mockito.BDDMockito.given;
import static org.fest.assertions.Assertions.assertThat;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtfacades.order.data.OrderModificationData;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.urlencoder.UrlEncoderService;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.CustomerEmailResolutionService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;


/**
 * Unit test for {@link OrderModificationEmailContext}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderPartialCancellationEmailContextTest {

    @Mock
    private PriceDataFactory priceDataFactory;
    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;
    @Mock
    private OrderModificationRecordEntryModel orderCancelRecordEntry;
    @Mock
    private OrderEntryModificationRecordEntryModel orderEntryModificationRecordEntryModel;
    @Mock
    private OrderProcessModel process;
    @Mock
    private OrderModel orderModel;
    @Mock
    private CurrencyModel currency;
    @Mock
    private PriceData priceData;
    @Mock
    private Converter<OrderModel, TargetOrderData> orderConverter;
    @Mock
    private TargetOrderData order;
    @Mock
    private EmailPageModel emailPageModel;
    @Mock
    private UrlEncoderService urlEncoderService;
    @Mock
    private SiteBaseUrlResolutionService siteBaseUrlResolutionService;
    @Mock
    private CustomerEmailResolutionService customerEmailResolutionService;
    @Mock
    private Converter<UserModel, CustomerData> customerConverter;
    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;
    @Mock
    private TargetStoreLocatorFacade targetStoreLocatorFacade;
    @Mock
    private TargetPointOfServiceData selectedStore;
    @Mock
    private BaseSiteModel siteModel;

    private OrderModificationDataHelper orderModificationDataHelper;
    private final OrderPartialCancellationEmailContext context = new OrderPartialCancellationEmailContext();


    @Before
    public void setup() {
        given(orderModel.getCurrency()).willReturn(currency);
        given(process.getOrder()).willReturn(orderModel);
        given(orderConverter.convert(orderModel)).willReturn(order);
        context.setOrderConverter(orderConverter);
        orderModificationDataHelper = new OrderModificationDataHelper();
        orderModificationDataHelper.setPriceDataFactory(priceDataFactory);
        orderModificationDataHelper.setOrderProcessParameterHelper(orderProcessParameterHelper);
        context.setOrderModificationDataHelper(orderModificationDataHelper);
        context.setOrderProcessParameterHelper(orderProcessParameterHelper);
        context.setCustomerConverter(customerConverter);
        context.setSelectedStore(selectedStore);
        context.setTargetStoreLocatorFacade(targetStoreLocatorFacade);
        context.setTargetDeliveryModeHelper(targetDeliveryModeHelper);
        context.setCustomerEmailResolutionService(customerEmailResolutionService);
        context.setOrderProcessParameterHelper(orderProcessParameterHelper);
        context.setSiteBaseUrlResolutionService(siteBaseUrlResolutionService);
        given(urlEncoderService.getUrlEncodingPatternForEmail(process)).willReturn("");
        context.setUrlEncoderService(urlEncoderService);
        given(process.getOrder().getSite()).willReturn(siteModel);
        given(context.getSite(process)).willReturn(siteModel);
        given(orderCancelRecordEntry.getOrderEntriesModificationEntries()).willReturn(
                Collections.singletonList(orderEntryModificationRecordEntryModel));

        // Mock order for logging
        given(process.getCode()).willReturn("123");

        // Mock PriceData from the factory
        given(priceDataFactory.create(Mockito.any(PriceDataType.class), Mockito.any(BigDecimal.class),
                Mockito.anyString())).willReturn(priceData);

        final Collection<OrderEntryModificationRecordEntryModel> entries = new ArrayList<>();
        final OrderEntryModificationRecordEntryModel entry1 = new OrderEntryModificationRecordEntryModel();
        entry1.setCode("1");
        entry1.setOrderEntry( new OrderEntryModel());
        entries.add(entry1);


        final OrderCancelRecordEntryModel orderCancelRecordEntryModel = new OrderCancelRecordEntryModel();
        orderCancelRecordEntryModel.setOrderEntriesModificationEntries(entries);
        final OrderModificationRecordModel record = new OrderModificationRecordModel();
        record.setOrder(orderModel);
        orderCancelRecordEntryModel.setModificationRecord(record);
        orderCancelRecordEntryModel.setRefundedShippingAmount(2d);

        given(orderProcessParameterHelper.getOrderCancelRequest(process)).willReturn(orderCancelRecordEntryModel);

        final List<ConsignmentData> shippedConsignments = new ArrayList<>();
        final ConsignmentData c1 = new ConsignmentData();
        c1.setStatus(ConsignmentStatus.SHIPPED);
        final ConsignmentData c2 = new ConsignmentData();
        c2.setStatus(ConsignmentStatus.SHIPPED);
        final ConsignmentData c3 = new ConsignmentData();
        c3.setStatus(ConsignmentStatus.SHIPPED);
        c3.setIsDigitalDelivery(true);

        shippedConsignments.add(c1);
        shippedConsignments.add(c2);
        shippedConsignments.add(c3);
        given(order.getConsignment()).willReturn(shippedConsignments);

    }




    @Test
    public void testPopulatePreviouslyCancelledItems() throws ParseException {

        final List<OrderModificationData> allCancelledItems = new ArrayList<>();
        final OrderModificationData cancelledData1 = new OrderModificationData();
        final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        cancelledData1.setCode("1");
        cancelledData1.setDate(sdf.parse("30/10/2018 10:40:11"));
        allCancelledItems.add(cancelledData1);

        final OrderModificationData cancelledData2 = new OrderModificationData();
        cancelledData2.setCode("2");
        cancelledData2.setDate(sdf.parse("30/10/2018 10:40:12"));
        allCancelledItems.add(cancelledData2);

        final OrderModificationData cancelledData3 = new OrderModificationData();
        cancelledData3.setCode("3");
        cancelledData3.setDate(sdf.parse("31/10/2018 11:40:11"));
        allCancelledItems.add(cancelledData3);

        given(order.getCancelledProducts()).willReturn(allCancelledItems);

        context.init(process,emailPageModel);
        assertThat(context.getOrder().getCancelledProducts()).hasSize(2);
        assertThat(context.getOrder().getCancelledProducts().get(0)).isEqualTo(cancelledData2);
        assertThat(context.getOrder().getCancelledProducts().get(1)).isEqualTo(cancelledData1);
        assertThat(context.getCurrentlyCancelledItems()).hasSize(1);
        assertThat(context.getCurrentlyCancelledItems().get(0)).isEqualTo(cancelledData3);



    }

    @Test
    public void testPopulateCurrentlyCancelledItemDetails() {

        given(orderProcessParameterHelper.getRefundAmount(process)).willReturn(BigDecimal.valueOf(10d));
        context.init(process,emailPageModel);
        assertThat(context.getRefundAmount()).isEqualTo(priceData);
        assertThat(context.getDeliveryRefundAmount()).isEqualTo(priceData);
    }

    @Test
    public void testCnCOrderShippedConsignments(){
        given(order.getCncStoreNumber()).willReturn(123);
        context.init(process,emailPageModel);
        assertThat(context.getShippedConsignments()).isEmpty();
        assertThat(context.getSentDigitalConsignments()).hasSize(1);

    }

    @Test
    public void testNonCncOrderShippedConsignments(){
        given(order.getCncStoreNumber()).willReturn(null);
        context.init(process,emailPageModel);
        assertThat(context.getShippedConsignments()).hasSize(2);
        assertThat(context.getSentDigitalConsignments()).hasSize(1);

    }
}

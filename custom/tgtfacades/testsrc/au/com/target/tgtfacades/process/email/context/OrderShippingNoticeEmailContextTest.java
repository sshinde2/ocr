package au.com.target.tgtfacades.process.email.context;

import static de.hybris.platform.basecommerce.enums.ConsignmentStatus.CREATED;
import static de.hybris.platform.basecommerce.enums.ConsignmentStatus.PICKED;
import static de.hybris.platform.basecommerce.enums.ConsignmentStatus.SENT_TO_WAREHOUSE;
import static de.hybris.platform.basecommerce.enums.ConsignmentStatus.SHIPPED;
import static java.lang.Boolean.valueOf;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.urlencoder.UrlEncoderService;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.CustomerEmailResolutionService;
import de.hybris.platform.commons.model.DocumentModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.taxinvoice.TaxInvoiceService;
import au.com.target.tgtfacades.order.data.OrderModificationData;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderShippingNoticeEmailContextTest {

    @Mock
    private OrderProcessModel businessProcessModel;

    @Mock
    private OrderModel orderModel;

    @Mock
    private BaseSiteModel siteModel;

    @Mock
    private TargetCustomerModel customerModel;

    @Mock
    private EmailPageModel emailPageModel;

    @Mock
    private ConsignmentModel consignmentModel;

    @Mock
    private DocumentModel taxInvoiceMedia;

    @Mock
    private Converter<ConsignmentModel, ConsignmentData> consignmentConverter;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private UrlEncoderService urlEncoderService;

    @Mock
    private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

    @Mock
    private CustomerEmailResolutionService customerEmailResolutionService;

    @Mock
    private Converter<UserModel, CustomerData> customerConverter;

    @Mock
    private Converter<OrderModel, TargetOrderData> orderConverter;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Mock
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Mock
    private TargetPointOfServiceData selectedStore;

    @Mock
    private TaxInvoiceService taxInvoiceService;

    @Mock
    private ConsignmentData consignment1;

    @Mock
    private ConsignmentData consignment2;

    @Mock
    private ConsignmentData consignment3;

    @Mock
    private ConsignmentData consignment4;

    @Mock
    private ConsignmentData consignment5;

    @Mock
    private ConsignmentData currentConsignment;

    @Mock
    private TargetOrderData order;

    @Mock
    private ProductData product1;

    @Mock
    private ProductData product2;

    @Mock
    private ProductData product3;

    @Mock
    private ProductData product4;

    @Mock
    private ProductData product5;

    @Mock
    private ProductData product6;

    @Mock
    private OrderEntryData orderEntry1;

    @Mock
    private OrderEntryData orderEntry2;

    @Mock
    private OrderEntryData orderEntry3;

    @Mock
    private OrderEntryData orderEntry4;

    @Mock
    private OrderEntryData orderEntry5;

    @Mock
    private OrderEntryData orderEntry6;

    @Mock
    private OrderModificationData orderModificationData1;

    @Mock
    private OrderModificationData orderModificationData2;

    private final List<OrderModificationData> cancelledProducts = spy(new ArrayList());

    @Mock
    private ConsignmentEntryData consignment1Entry;

    @Mock
    private ConsignmentEntryData consignment2Entry;

    @Mock
    private ConsignmentEntryData consignment3Entry;

    @Mock
    private ConsignmentEntryData consignment4Entry;

    @Mock
    private ConsignmentEntryData consignment5Entry;

    private OrderShippingNoticeEmailContext contextSetUp() {

        final OrderShippingNoticeEmailContext context = new OrderShippingNoticeEmailContext();
        context.setCustomerConverter(customerConverter);
        context.setOrderConverter(orderConverter);
        context.setOrderProcessParameterHelper(orderProcessParameterHelper);
        context.setSelectedStore(selectedStore);
        context.setTargetStoreLocatorFacade(targetStoreLocatorFacade);
        context.setTargetDeliveryModeHelper(targetDeliveryModeHelper);
        context.setCustomerEmailResolutionService(customerEmailResolutionService);
        context.setOrderProcessParameterHelper(orderProcessParameterHelper);
        context.setSiteBaseUrlResolutionService(siteBaseUrlResolutionService);
        given(urlEncoderService.getUrlEncodingPatternForEmail(businessProcessModel)).willReturn("");
        context.setUrlEncoderService(urlEncoderService);
        given(businessProcessModel.getOrder()).willReturn(orderModel);
        given(businessProcessModel.getOrder().getSite()).willReturn(siteModel);
        given(context.getSite(businessProcessModel)).willReturn(siteModel);

        given(orderModel.getUser()).willReturn(customerModel);
        final CustomerData customerData = new CustomerData();
        given(customerConverter.convert(customerModel)).willReturn(customerData);
        given(taxInvoiceService.getTaxInvoiceForOrder(orderModel)).willReturn(taxInvoiceMedia);
        given(context.getOrderProcessParameterHelper().getConsignment(businessProcessModel))
                .willReturn(consignmentModel);

        createOrderData();
        given(consignmentConverter.convert(consignmentModel)).willReturn(currentConsignment);
        given(orderConverter.convert((OrderModel)any())).willReturn(order);

        return context;
    }

    private void createOrderData() {
        given(product1.getCode()).willReturn("p1");
        given(orderEntry1.getEntryNumber()).willReturn(new Integer(1));
        given(orderEntry1.getProduct()).willReturn(product1);
        given(orderEntry1.getQuantity()).willReturn(new Long(2L));
        given(consignment1.getCode()).willReturn("code1");

        final Calendar cal1 = Calendar.getInstance();
        cal1.set(Calendar.HOUR_OF_DAY, 10);
        given(consignment1.getStatusDate()).willReturn(cal1.getTime());

        final List<ConsignmentEntryData> consignment1Entries = new ArrayList<>();

        given(consignment1Entry.getOrderEntry()).willReturn(orderEntry1);
        given(consignment1Entry.getShippedQuantity()).willReturn(new Long(2L));
        consignment1Entries.add(consignment1Entry);
        given(consignment1.getEntries()).willReturn(consignment1Entries);
        given(valueOf(consignment1.isIsDigitalDelivery())).willReturn(valueOf(true));
        given(consignment1.getStatus()).willReturn(SHIPPED);
        given(product2.getCode()).willReturn("p2");

        given(orderEntry2.getEntryNumber()).willReturn(new Integer(2));
        given(orderEntry2.getProduct()).willReturn(product2);
        given(orderEntry2.getQuantity()).willReturn(new Long(2L));
        final Calendar cal2 = Calendar.getInstance();
        cal2.set(Calendar.HOUR_OF_DAY, 11);
        given(consignment2.getCode()).willReturn("code2");
        given(consignment2.getStatusDate()).willReturn(cal2.getTime());
        given(consignment2.getStatus()).willReturn(PICKED);
        given(consignment2Entry.getOrderEntry()).willReturn(orderEntry2);
        final List<ConsignmentEntryData> consignment2Entries = new ArrayList<>();
        consignment2Entries.add(consignment2Entry);
        given(consignment2.getEntries()).willReturn(consignment2Entries);
        given(product3.getCode()).willReturn("p3");
        given(orderEntry3.getEntryNumber()).willReturn(new Integer(3));
        given(orderEntry3.getProduct()).willReturn(product3);
        given(orderEntry3.getQuantity()).willReturn(new Long(2L));
        final Calendar cal3 = Calendar.getInstance();
        cal3.set(Calendar.HOUR_OF_DAY, 12);
        given(consignment3.getCode()).willReturn("code3");
        given(consignment3.getStatusDate()).willReturn(cal3.getTime());
        given(consignment3.getStatus()).willReturn(SHIPPED);

        given(consignment3Entry.getOrderEntry()).willReturn(orderEntry3);
        given(consignment3Entry.getShippedQuantity()).willReturn(new Long(2L));

        final List<ConsignmentEntryData> consignment3Entries = new ArrayList<>();
        consignment3Entries.add(consignment3Entry);
        consignment3.setEntries(consignment3Entries);
        given(product4.getCode()).willReturn("p4");
        given(orderEntry4.getEntryNumber()).willReturn(new Integer(4));
        given(orderEntry4.getProduct()).willReturn(product4);
        given(orderEntry4.getQuantity()).willReturn(new Long(2L));

        final Calendar cal4 = Calendar.getInstance();
        cal4.set(Calendar.HOUR_OF_DAY, 13);
        given(consignment4.getCode()).willReturn("code4");
        given(consignment4.getStatusDate()).willReturn(cal4.getTime());
        given(consignment4.getStatus()).willReturn(SENT_TO_WAREHOUSE);
        given(consignment4Entry.getOrderEntry()).willReturn(orderEntry4);

        final List<ConsignmentEntryData> consignment4Entries = new ArrayList<>();
        consignment4Entries.add(consignment4Entry);
        consignment4.setEntries(consignment4Entries);

        given(product5.getCode()).willReturn("p5");
        given(orderEntry5.getEntryNumber()).willReturn(new Integer(5));
        given(orderEntry5.getProduct()).willReturn(product5);
        given(orderEntry5.getQuantity()).willReturn(new Long(2L));

        final Calendar cal5 = Calendar.getInstance();
        cal5.set(Calendar.HOUR_OF_DAY, 14);
        given(consignment4.getCode()).willReturn("code5");
        given(consignment4.getStatusDate()).willReturn(cal5.getTime());
        given(consignment4.getStatus()).willReturn(CREATED);

        given(consignment5Entry.getOrderEntry()).willReturn(orderEntry5);
        given(consignment5Entry.getShippedQuantity()).willReturn(new Long(2L));

        final List<ConsignmentEntryData> consignment5Entries = new ArrayList<>();
        consignment5Entries.add(consignment5Entry);
        consignment5.setEntries(consignment5Entries);

        given(product6.getCode()).willReturn("p6");
        given(orderEntry6.getEntryNumber()).willReturn(new Integer(5));
        given(orderEntry6.getProduct()).willReturn(product6);
        given(orderEntry6.getQuantity()).willReturn(new Long(1L));

        final List<OrderEntryData> orderEntries = new ArrayList<>();
        orderEntries.add(orderEntry1);
        orderEntries.add(orderEntry2);
        orderEntries.add(orderEntry3);
        orderEntries.add(orderEntry4);
        orderEntries.add(orderEntry5);
        orderEntries.add(orderEntry6);
        given(order.getEntries()).willReturn(orderEntries);

        given(orderModificationData1.getProduct()).willReturn(product5);
        given(orderModificationData1.getQuantity()).willReturn(new Long(2L));
        cancelledProducts.add(orderModificationData1);

        given(orderModificationData2.getProduct()).willReturn(product6);
        given(orderModificationData2.getQuantity()).willReturn(new Long(1L));
        cancelledProducts.add(orderModificationData2);

        given(order.getCancelledProducts()).willReturn(cancelledProducts);
        currentConsignment = consignment5;
        given(consignmentConverter.convert(consignmentModel)).willReturn(currentConsignment);
        final List<ConsignmentData> consignments = new ArrayList<>();
        consignments.add(consignment2);
        consignments.add(consignment1);
        consignments.add(consignment4);
        consignments.add(consignment3);
        consignments.add(consignment5);

        given(order.getConsignment()).willReturn(consignments);
    }

    @Test
    public void testOrderShippingNoticeEmailContextStatus() {

        final OrderShippingNoticeEmailContext context = contextSetUp();
        context.init(businessProcessModel, emailPageModel);

        final List<ConsignmentData> expectedConsignmentList = asList(consignment1, consignment2, consignment3,
                consignment4, consignment5);
        assertThat(context.getOrder()).isNotNull();
        assertThat(context.getOrder().getConsignment()).isEqualTo(expectedConsignmentList);
        final List<OrderEntryData> expectedInProgressEntries = asList(orderEntry2, orderEntry3, orderEntry4,
                orderEntry5, orderEntry6);
        final List<ConsignmentData> expectedShippedConsignmentList = singletonList(consignment3);
        final List<ConsignmentData> expectedDigitallySentConsignmentList = singletonList(consignment1);

        assertThat(context.getShippedConsignments()).isEqualTo(expectedShippedConsignmentList);
        assertThat(context.getSentDigitalConsignments()).isEqualTo(expectedDigitallySentConsignmentList);
        assertThat(context.getInProgressEntries()).isEqualTo(expectedInProgressEntries);
        assertThat(context.getOrder().getCancelledProducts()).isEqualTo(cancelledProducts);
        assertThat(context.getOrder().getConsignment()).isEqualTo(order.getConsignment());
    }
}
package au.com.target.tgtfacades.product.converters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductVariantListerDataConverterTest {

    @Mock
    private Map<String, List<Populator<VariantProductModel, TargetVariantProductListerData>>> variantSpecificPopulator;

    @InjectMocks
    private final TargetProductVariantListerDataConverter targetProductVariantListerDataConverter = new TargetProductVariantListerDataConverter();



    @Test
    public void testPopulateWhenVariantModelDoesNotContainFurtherVariants() throws Exception {

        final VariantProductModel source = Mockito.mock(VariantProductModel.class);
        BDDMockito.given(source.getItemtype()).willReturn("TargetColourVariantProduct");
        final Populator<VariantProductModel, TargetVariantProductListerData> colourVariantSpecificPopulator1 = Mockito
                .mock(Populator.class);
        BDDMockito.given(variantSpecificPopulator.get("TargetColourVariantProduct"))
                .willReturn(Arrays.asList(colourVariantSpecificPopulator1));

        final List<Populator<VariantProductModel, TargetVariantProductListerData>> genericVariantListerPopulators = new ArrayList<Populator<VariantProductModel, TargetVariantProductListerData>>();
        final Populator<VariantProductModel, TargetVariantProductListerData> genericPopulator = Mockito
                .mock(Populator.class);
        genericVariantListerPopulators.add(genericPopulator);
        targetProductVariantListerDataConverter.setGenericVariantListerPopulators(genericVariantListerPopulators);


        final TargetVariantProductListerData target = new TargetVariantProductListerData();
        targetProductVariantListerDataConverter.populate(source, target);
        Assertions.assertThat(target.getVariants()).isEmpty();
        Assertions.assertThat(targetProductVariantListerDataConverter.getPopulators())
                .contains(colourVariantSpecificPopulator1);
        Assertions.assertThat(targetProductVariantListerDataConverter.getPopulators())
                .contains(genericPopulator);

    }

    @Test
    public void testPopulateWhenVariantModelHavingMultipleLevelVariants() throws Exception {


        final List<Populator<VariantProductModel, TargetVariantProductListerData>> genericVariantListerPopulators = new ArrayList<Populator<VariantProductModel, TargetVariantProductListerData>>();
        final Populator<VariantProductModel, TargetVariantProductListerData> genericPopulator = Mockito
                .mock(Populator.class);
        genericVariantListerPopulators.add(genericPopulator);
        targetProductVariantListerDataConverter.setGenericVariantListerPopulators(genericVariantListerPopulators);



        final VariantProductModel sourceVariantLevel1 = Mockito.mock(VariantProductModel.class);
        BDDMockito.given(sourceVariantLevel1.getItemtype()).willReturn("TargetColourVariantProduct");

        final Populator<VariantProductModel, TargetVariantProductListerData> colourVariantSpecificPopulator = Mockito
                .mock(Populator.class);
        BDDMockito.given(variantSpecificPopulator.get("TargetColourVariantProduct"))
                .willReturn(Arrays.asList(colourVariantSpecificPopulator));

        final VariantProductModel sourceVariantLevel2 = Mockito.mock(VariantProductModel.class);
        BDDMockito.given(sourceVariantLevel2.getItemtype()).willReturn("TargetSizeVariantProduct");

        final Populator<VariantProductModel, TargetVariantProductListerData> sizeVariantSpecificPopulator = Mockito
                .mock(Populator.class);
        BDDMockito.given(variantSpecificPopulator.get("TargetSizeVariantProduct"))
                .willReturn(Arrays.asList(sizeVariantSpecificPopulator));

        BDDMockito.given(sourceVariantLevel1.getVariants()).willReturn((Arrays.asList(sourceVariantLevel2)));


        final VariantProductModel sourceVariantLevel3 = Mockito.mock(VariantProductModel.class);
        BDDMockito.given(sourceVariantLevel3.getItemtype()).willReturn("TargetNewDummyVariantProduct");

        final Populator<VariantProductModel, TargetVariantProductListerData> dummyVariantSpecificPopulator = Mockito
                .mock(Populator.class);
        BDDMockito.given(variantSpecificPopulator.get("TargetNewDummyVariantProduct"))
                .willReturn(Arrays.asList(dummyVariantSpecificPopulator));

        BDDMockito.given(sourceVariantLevel2.getVariants()).willReturn((Arrays.asList(sourceVariantLevel3)));


        final TargetVariantProductListerData target = new TargetVariantProductListerData();
        targetProductVariantListerDataConverter.populate(sourceVariantLevel1, target);
        Assertions.assertThat(target.getVariants()).isNotEmpty();
        final TargetVariantProductListerData targetLevel2 = target.getVariants().get(0);
        Assertions.assertThat(targetLevel2.getVariants()).isNotEmpty();
        final TargetVariantProductListerData targetLevel3 = targetLevel2.getVariants().get(0);
        Assertions.assertThat(targetLevel3.getVariants()).isEmpty();

    }


}

package au.com.target.tgtfacades.product.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commerceservices.product.CommerceProductService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.product.data.TargetProductData;


/**
 * Unit Test for {@link TargetProductCategoriesPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductCategoriesPopulatorTest {

    @InjectMocks
    private final TargetProductCategoriesPopulator targetProductCategoriesPopulator = new TargetProductCategoriesPopulator();

    @Mock
    private Converter<CategoryModel, CategoryData> categoryConverter;

    @Mock
    private CommerceProductService commerceProductService;

    @Test
    public void testPopulateForBaseProduct() {
        final ProductModel product = Mockito.mock(ProductModel.class);
        final CategoryModel category = Mockito.mock(CategoryModel.class);
        final TargetProductData targetProductData = Mockito.mock(TargetProductData.class);

        BDDMockito.given(commerceProductService.getSuperCategoriesExceptClassificationClassesForProduct(product))
                .willReturn(Collections.singletonList(category));

        targetProductCategoriesPopulator.populate(product, targetProductData);

        Mockito.verify(commerceProductService).getSuperCategoriesExceptClassificationClassesForProduct(product);
        Mockito.verify(categoryConverter).convert(category);
    }

    @Test
    public void testPopulateForVariantProductWithCategoryHierachy() {
        final ProductModel baseProduct = Mockito.mock(ProductModel.class);
        final VariantProductModel variantProduct = Mockito.mock(VariantProductModel.class);
        final CategoryModel category = Mockito.mock(CategoryModel.class);
        final CategoryData categoryData = new CategoryData();
        final TargetProductData targetProductData = new TargetProductData();

        BDDMockito.given(variantProduct.getBaseProduct()).willReturn(baseProduct);
        BDDMockito.given(categoryConverter.convert(category)).willReturn(categoryData);
        BDDMockito
                .given(commerceProductService.getSuperCategoriesExceptClassificationClassesForProduct(variantProduct))
                .willReturn(Collections.singletonList(category));

        targetProductCategoriesPopulator.populate(variantProduct, targetProductData);

        Mockito.verify(commerceProductService).getSuperCategoriesExceptClassificationClassesForProduct(variantProduct);
        Mockito.verify(categoryConverter).convert(category);

        Mockito.verify(variantProduct, Mockito.never()).getBaseProduct();
    }

    @Test
    public void testPopulateForVariantProductWithoutCategoryHierachy() {
        final ProductModel baseProduct = Mockito.mock(ProductModel.class);
        final VariantProductModel variantProduct = Mockito.mock(VariantProductModel.class);
        final CategoryModel category = Mockito.mock(CategoryModel.class);
        final CategoryData categoryData = new CategoryData();
        final TargetProductData targetProductData = new TargetProductData();

        BDDMockito.given(variantProduct.getBaseProduct()).willReturn(baseProduct);
        BDDMockito.given(categoryConverter.convert(category)).willReturn(categoryData);
        BDDMockito.doReturn(null).when(commerceProductService)
                .getSuperCategoriesExceptClassificationClassesForProduct(variantProduct);
        BDDMockito
                .given(commerceProductService.getSuperCategoriesExceptClassificationClassesForProduct(baseProduct))
                .willReturn(Collections.singletonList(category));


        targetProductCategoriesPopulator.populate(variantProduct, targetProductData);

        Mockito.verify(commerceProductService).getSuperCategoriesExceptClassificationClassesForProduct(variantProduct);
        Mockito.verify(variantProduct).getBaseProduct();
        Mockito.verify(commerceProductService).getSuperCategoriesExceptClassificationClassesForProduct(baseProduct);
        Mockito.verify(categoryConverter).convert(category);
    }

    @Test
    public void testPopulateForSecondLevelVariantWithoutCategoryHierachy() {
        final ProductModel baseProduct = Mockito.mock(ProductModel.class);
        final VariantProductModel colourVariantProduct = Mockito.mock(VariantProductModel.class);
        final VariantProductModel sizeVariantProduct = Mockito.mock(VariantProductModel.class);
        final CategoryModel category = Mockito.mock(CategoryModel.class);
        final CategoryData categoryData = new CategoryData();
        final TargetProductData targetProductData = new TargetProductData();

        BDDMockito.given(colourVariantProduct.getBaseProduct()).willReturn(baseProduct);
        BDDMockito.given(sizeVariantProduct.getBaseProduct()).willReturn(colourVariantProduct);
        BDDMockito.given(categoryConverter.convert(category)).willReturn(categoryData);
        BDDMockito.doReturn(null).when(commerceProductService)
                .getSuperCategoriesExceptClassificationClassesForProduct(colourVariantProduct);
        BDDMockito.doReturn(null).when(commerceProductService)
                .getSuperCategoriesExceptClassificationClassesForProduct(sizeVariantProduct);
        BDDMockito
                .given(commerceProductService.getSuperCategoriesExceptClassificationClassesForProduct(baseProduct))
                .willReturn(Collections.singletonList(category));


        targetProductCategoriesPopulator.populate(sizeVariantProduct, targetProductData);

        Mockito.verify(commerceProductService).getSuperCategoriesExceptClassificationClassesForProduct(
                sizeVariantProduct);
        Mockito.verify(sizeVariantProduct).getBaseProduct();
        Mockito.verify(commerceProductService).getSuperCategoriesExceptClassificationClassesForProduct(
                colourVariantProduct);
        Mockito.verify(colourVariantProduct).getBaseProduct();
        Mockito.verify(commerceProductService).getSuperCategoriesExceptClassificationClassesForProduct(
                baseProduct);
        Mockito.verify(categoryConverter).convert(category);
    }
}

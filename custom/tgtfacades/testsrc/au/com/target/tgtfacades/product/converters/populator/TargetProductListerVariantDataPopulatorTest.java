package au.com.target.tgtfacades.product.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductListerVariantDataPopulatorTest {

    @Mock
    private Converter<VariantProductModel, TargetVariantProductListerData> variantPdtListerConverter;

    @InjectMocks
    private final TargetProductListerVariantDataPopulator targetProductListerVariantDataPopulator = new TargetProductListerVariantDataPopulator();



    @Test
    public void testPopulateWhenThereAreVariants() throws Exception {
        final TargetProductModel source = BDDMockito.mock(TargetProductModel.class);
        final TargetColourVariantProductModel variantProductModel = BDDMockito
                .mock(TargetColourVariantProductModel.class);
        final List<VariantProductModel> variants = new ArrayList<>();
        variants.add(variantProductModel);
        BDDMockito.given(source.getVariants()).willReturn(variants);

        final TargetVariantProductListerData targetVariantProductListerData = BDDMockito
                .mock(TargetVariantProductListerData.class);

        BDDMockito.when(variantPdtListerConverter.convert(Mockito.any(VariantProductModel.class),
                Mockito.any(TargetVariantProductListerData.class))).thenReturn(targetVariantProductListerData);

        final TargetProductListerData target = new TargetProductListerData();
        targetProductListerVariantDataPopulator.populate(source, target);
        Assertions.assertThat(target.getTargetVariantProductListerData()).contains(targetVariantProductListerData);

    }

    @Test
    public void testPopulateWhenThereAreNoVariants() throws Exception {
        final TargetProductModel source = BDDMockito.mock(TargetProductModel.class);
        final TargetColourVariantProductModel variantProductModel = BDDMockito
                .mock(TargetColourVariantProductModel.class);
        final List<VariantProductModel> variants = new ArrayList<>();
        variants.add(variantProductModel);
        BDDMockito.given(source.getVariants()).willReturn(null);

        final TargetProductListerData target = new TargetProductListerData();
        targetProductListerVariantDataPopulator.populate(source, target);
        Assertions.assertThat(target.getTargetVariantProductListerData()).isNull();

    }

}

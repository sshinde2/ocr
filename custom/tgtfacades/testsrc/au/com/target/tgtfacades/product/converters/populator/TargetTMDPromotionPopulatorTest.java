package au.com.target.tgtfacades.product.converters.populator;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.ProductPromotionModel;

import org.junit.Test;
import org.mockito.Mockito;

import au.com.target.tgtcore.model.TMDiscountProductPromotionModel;
import au.com.target.tgtcore.model.TMDiscountPromotionModel;
import au.com.target.tgtfacades.product.data.TargetPromotionData;


@UnitTest
public class TargetTMDPromotionPopulatorTest {

    private final TargetTMDPromotionPopulator<ProductPromotionModel, TargetPromotionData> targetTMDPromotionPopulator = new TargetTMDPromotionPopulator<ProductPromotionModel, TargetPromotionData>();

    @Test
    public void testPopulateWithTMDPromotion() throws Exception {
        final TMDiscountPromotionModel mockTMDPromotion = Mockito.mock(TMDiscountPromotionModel.class);
        final TargetPromotionData targetPromotion = new TargetPromotionData();

        targetTMDPromotionPopulator.populate(mockTMDPromotion, targetPromotion);

        assertThat(targetPromotion.isTmdPromotion()).isTrue();
    }

    @Test
    public void testPopulateWithTMDProductPromotion() throws Exception {
        final TMDiscountProductPromotionModel mockTMDProductPromotion = Mockito
                .mock(TMDiscountProductPromotionModel.class);
        final TargetPromotionData targetPromotion = new TargetPromotionData();

        targetTMDPromotionPopulator.populate(mockTMDProductPromotion, targetPromotion);

        assertThat(targetPromotion.isTmdPromotion()).isTrue();
    }

    @Test
    public void testPopulateWithNonTMDPromotion() throws Exception {
        final ProductPromotionModel mockProductPromotion = Mockito.mock(ProductPromotionModel.class);
        final TargetPromotionData targetPromotion = new TargetPromotionData();

        targetTMDPromotionPopulator.populate(mockProductPromotion, targetPromotion);

        assertThat(targetPromotion.isTmdPromotion()).isFalse();
    }

}

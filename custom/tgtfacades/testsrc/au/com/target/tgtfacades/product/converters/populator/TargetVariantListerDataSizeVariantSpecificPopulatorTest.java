package au.com.target.tgtfacades.product.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;

import au.com.target.tgtcore.model.TargetProductSizeModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;


@UnitTest
public class TargetVariantListerDataSizeVariantSpecificPopulatorTest {

    @Test
    public void testPopulate() throws Exception {

        final TargetSizeVariantProductModel source = mock(TargetSizeVariantProductModel.class);
        final TargetProductSizeModel productSize = mock(TargetProductSizeModel.class);
        final TargetVariantProductListerData target = new TargetVariantProductListerData();
        given(source.getSize()).willReturn("size");
        given(source.getSizeType()).willReturn("sizetype");
        given(productSize.getPosition()).willReturn(Integer.valueOf(10));
        given(source.getProductSize()).willReturn(productSize);
        final TargetVariantListerDataSizeVariantSpecificPopulator populator = new TargetVariantListerDataSizeVariantSpecificPopulator();
        populator.populate(source, target);
        assertThat(target.getSize()).isEqualTo("size");
        assertThat(target.getSizeType()).isEqualTo("sizetype");
        assertThat(target.getSizePosition()).isEqualTo(Integer.valueOf(10));
    }

}

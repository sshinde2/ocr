/**
 * 
 */
package au.com.target.tgtfacades.product.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.price.TargetCommercePriceService;
import au.com.target.tgtcore.price.data.PriceRangeInformation;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.util.TargetPriceHelper;
import au.com.target.tgtfacades.util.TargetPriceInformation;


/**
 * Class to test price data populator
 * 
 * @author bbaral1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetVariantProductListerPriceDataPopulatorTest {

    @Mock
    private TargetPriceHelper targetPriceHelper;

    @Mock
    private TargetCommercePriceService targetCommercePriceService;

    @InjectMocks
    private final TargetVariantProductListerPriceDataPopulator targetVariantProductListerPriceDataPopulator = new TargetVariantProductListerPriceDataPopulator();

    @Mock
    private PriceRangeInformation priceRangeInformation;

    @Mock
    private TargetPriceInformation targetPriceInformation;

    /**
     * Method to test populate price range data.
     * 
     * @throws Exception
     */
    @Test
    public void testPopulatePriceRangeData() throws Exception {

        final AbstractTargetVariantProductModel source = mock(
                AbstractTargetVariantProductModel.class);
        final TargetVariantProductListerData target = new TargetVariantProductListerData();

        final PriceRangeData priceRangeData = new PriceRangeData();
        final PriceData maxPrice = new PriceData();
        maxPrice.setCurrencyIso("CurrencyISO-1");
        maxPrice.setFormattedValue("FormattedValue-1");
        maxPrice.setPriceType(PriceDataType.BUY);
        maxPrice.setValue(BigDecimal.valueOf(3.4));
        maxPrice.setMaxQuantity(new Long(1000L));
        maxPrice.setMinQuantity(new Long(10L));

        final PriceData minPrice = new PriceData();
        minPrice.setCurrencyIso("CurrencyISO-2");
        minPrice.setFormattedValue("FormattedValue-2");
        minPrice.setPriceType(PriceDataType.BUY);
        minPrice.setValue(BigDecimal.valueOf(2.4));
        minPrice.setMaxQuantity(new Long(500L));
        minPrice.setMinQuantity(new Long(5L));

        priceRangeData.setMaxPrice(maxPrice);
        priceRangeData.setMinPrice(minPrice);

        given(targetCommercePriceService.getPriceRangeInfoForProduct(source)).willReturn(priceRangeInformation);
        given(targetPriceHelper.populatePricesFromRangeInformation(priceRangeInformation))
                .willReturn(targetPriceInformation);
        given(targetPriceInformation.getPriceRangeData()).willReturn(priceRangeData);

        targetVariantProductListerPriceDataPopulator.populate(source,
                target);

        verify(targetCommercePriceService).getPriceRangeInfoForProduct(source);
        verify(targetPriceHelper).populatePricesFromRangeInformation(priceRangeInformation);

        assertThat(target.getPriceRange()).isNotNull();
        assertThat(target.getPriceRange()).isEqualTo(priceRangeData);
    }

    /**
     * Method to test for null price range data.
     * 
     * @throws Exception
     */
    @Test
    public void populateNullPriceRangeData() throws Exception {
        final AbstractTargetVariantProductModel source = mock(
                AbstractTargetVariantProductModel.class);
        final TargetVariantProductListerData target = new TargetVariantProductListerData();

        final PriceRangeData priceRangeData = null;

        given(targetCommercePriceService.getPriceRangeInfoForProduct(source)).willReturn(priceRangeInformation);
        given(targetPriceHelper.populatePricesFromRangeInformation(priceRangeInformation))
                .willReturn(targetPriceInformation);

        given(targetPriceInformation.getPriceRangeData()).willReturn(priceRangeData);

        targetVariantProductListerPriceDataPopulator.populate(source,
                target);

        verify(targetCommercePriceService).getPriceRangeInfoForProduct(source);
        verify(targetPriceHelper).populatePricesFromRangeInformation(priceRangeInformation);

        assertThat(target.getPriceRange()).isNull();
    }

    /**
     * Method to test populate price data.
     * 
     * @throws Exception
     */
    @Test
    public void populatePriceData() throws Exception {
        final AbstractTargetVariantProductModel source = mock(
                AbstractTargetVariantProductModel.class);
        final TargetVariantProductListerData target = new TargetVariantProductListerData();

        final PriceData priceData = new PriceData();
        priceData.setCurrencyIso("CurrencyISO-3");
        priceData.setFormattedValue("FormattedValue-3");
        priceData.setPriceType(PriceDataType.BUY);
        priceData.setValue(BigDecimal.valueOf(4.4));
        priceData.setMaxQuantity(new Long(4000L));
        priceData.setMinQuantity(new Long(20L));

        given(targetCommercePriceService.getPriceRangeInfoForProduct(source)).willReturn(priceRangeInformation);
        given(targetPriceHelper.populatePricesFromRangeInformation(priceRangeInformation))
                .willReturn(targetPriceInformation);
        given(targetPriceInformation.getPriceData()).willReturn(priceData);

        targetVariantProductListerPriceDataPopulator.populate(source,
                target);

        verify(targetCommercePriceService).getPriceRangeInfoForProduct(source);
        verify(targetPriceHelper).populatePricesFromRangeInformation(priceRangeInformation);

        assertThat(target.getPrice()).isNotNull();
        assertThat(target.getPrice()).isEqualTo(priceData);
    }

    /**
     * Method to test null price data.
     * 
     * @throws Exception
     */
    @Test
    public void populateNullPriceData() throws Exception {
        final AbstractTargetVariantProductModel source = mock(
                AbstractTargetVariantProductModel.class);
        final TargetVariantProductListerData target = new TargetVariantProductListerData();

        final PriceData priceData = null;

        given(targetCommercePriceService.getPriceRangeInfoForProduct(source)).willReturn(priceRangeInformation);
        given(targetPriceHelper.populatePricesFromRangeInformation(priceRangeInformation))
                .willReturn(targetPriceInformation);
        given(targetPriceInformation.getPriceData()).willReturn(priceData);

        targetVariantProductListerPriceDataPopulator.populate(source,
                target);

        verify(targetCommercePriceService).getPriceRangeInfoForProduct(source);
        verify(targetPriceHelper).populatePricesFromRangeInformation(priceRangeInformation);

        assertThat(target.getPrice()).isNull();
    }


}

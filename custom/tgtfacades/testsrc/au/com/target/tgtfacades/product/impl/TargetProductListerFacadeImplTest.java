package au.com.target.tgtfacades.product.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.converter.ConfigurablePopulator;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtfacades.product.data.TargetProductListerData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductListerFacadeImplTest {
    @Mock
    private Converter<TargetProductModel, TargetProductListerData> targetProductListerConverter;

    @Mock
    private TargetProductService targetProductService;

    @InjectMocks
    private final TargetProductListerFacadeImpl targetProductListerFacadeImpl = new TargetProductListerFacadeImpl();

    @Mock
    private ConfigurablePopulator<TargetProductModel, TargetProductListerData, ProductOption> configurablePopulator;

    @Test
    public void testGetBaseProductDataByCodeNoProduct() {
        final String productCode = "12345678";

        given(targetProductService.getProductForCode(productCode)).willReturn(null);

        final TargetProductListerData targetProductListerData = targetProductListerFacadeImpl
                .getBaseProductDataByCode("12345678", productOptions());
        assertThat(targetProductListerData).isNull();
    }

    @Test
    public void testGetBaseProductDataByCodeUnknownIdentifierException() {
        final String productCode = "12345678";

        given(targetProductService.getProductForCode(productCode)).willThrow(
                new UnknownIdentifierException("Not found!"));

        final TargetProductListerData targetProductListerData = targetProductListerFacadeImpl
                .getBaseProductDataByCode("12345678", productOptions());
        assertThat(targetProductListerData).isNull();
    }

    @Test
    public void testGetBaseProductDataByCodeAmbiguousIdentifierException() {
        final String productCode = "12345678";

        given(targetProductService.getProductForCode(productCode)).willThrow(
                new AmbiguousIdentifierException("Not found!"));

        final TargetProductListerData targetProductListerData = targetProductListerFacadeImpl
                .getBaseProductDataByCode("12345678", productOptions());
        assertThat(targetProductListerData).isNull();
    }

    @Test
    public void testGetBaseProductDataByCodeForBaseProduct() {
        final String productCode = "12345678";

        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);

        given(targetProductService.getProductForCode(productCode)).willReturn(mockTargetProduct);

        final TargetProductListerData result = targetProductListerFacadeImpl
                .getBaseProductDataByCode("12345678", productOptions());

        assertThat(result).isNotNull();
    }

    @Test
    public void testGetBaseProductDataByCodeForColourVariant() {
        final String productCode = "12345678";

        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);

        final TargetColourVariantProductModel mockColourVariant = mock(TargetColourVariantProductModel.class);
        given(mockColourVariant.getBaseProduct()).willReturn(mockTargetProduct);

        given(targetProductService.getProductForCode(productCode)).willReturn(mockColourVariant);

        final TargetProductListerData result = targetProductListerFacadeImpl
                .getBaseProductDataByCode("12345678", productOptions());

        assertThat(result).isNotNull();
    }

    @Test
    public void testGetBaseProductDataByCodeForSizeVariant() {
        final String productCode = "12345678";

        final TargetProductModel mockTargetProduct = mock(TargetProductModel.class);

        final TargetColourVariantProductModel mockColourVariant = mock(TargetColourVariantProductModel.class);
        given(mockColourVariant.getBaseProduct()).willReturn(mockTargetProduct);

        final TargetSizeVariantProductModel mockSizeVariant = mock(TargetSizeVariantProductModel.class);
        given(mockSizeVariant.getBaseProduct()).willReturn(mockColourVariant);

        given(targetProductService.getProductForCode(productCode)).willReturn(mockSizeVariant);

        final TargetProductListerData result = targetProductListerFacadeImpl
                .getBaseProductDataByCode("12345678", productOptions());

        assertThat(result).isNotNull();
    }

    private List<ProductOption> productOptions() {
        final List<ProductOption> productOptions = new ArrayList<>();

        productOptions.add(ProductOption.BASIC);
        productOptions.add(ProductOption.BULKY_PRODUCT_DETAIL);
        productOptions.add(ProductOption.DELIVERY_MODE_AVAILABILITY);
        productOptions.add(ProductOption.VARIANT_FULL);
        productOptions.add(ProductOption.DELIVERY_PROMOTIONAL_MESSAGE);

        return productOptions;
    }
}

/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;


/**
 * @author htan3
 *
 */
@UnitTest
public class CartDetailTMDPopulatorTest {

    private final CartDetailTMDPopulator populator = new CartDetailTMDPopulator();
    private TargetCartData source;
    private CartDetailResponseData target;

    @Before
    public void setUp() {
        source = new TargetCartData();
        target = new CartDetailResponseData();
    }

    @Test
    public void testSetTmdNumber() {
        source.setTmdNumber("3422");
        populator.populate(source, target);
        assertThat(target.getTmdNumber()).isEqualTo("3422");
    }
}

/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.affiliate.data.AffiliateOrderData;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.order.data.TargetOrderEntryData;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.response.data.OrderDetailGaData;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderDetailGaPopulatorTest {

    @Mock
    private TargetOrderData source;

    @Mock
    private PriceData totalPrice;

    @Mock
    private PriceData totalTax;

    @Mock
    private PriceData deliveryCost;

    @Mock
    private AddressData addressData;

    @Mock
    private CountryData countryData;

    @Mock
    private DeliveryModeData deliveryMode;

    @Mock
    private AffiliateOrderData affiliateOrderData;

    @Mock
    private TargetOrderEntryData orderEntry1;

    @Mock
    private TargetOrderEntryData orderEntry2;

    @Mock
    private TargetProductData targetProduct1;

    @Mock
    private TargetProductData targetProduct2;

    @Mock
    private PriceData orderEntry1Price;

    @Mock
    private PriceData orderEntry2Price;

    @Mock
    private CMSSiteService cmsSiteService;

    private OrderDetailGaData target;

    @InjectMocks
    private final OrderDetailGaPopulator orderDetailGaPopulator = new OrderDetailGaPopulator();

    @Before
    public void setUp() {
        final CMSSiteModel site = mock(CMSSiteModel.class);
        given(cmsSiteService.getCurrentSite()).willReturn(site);
        given(site.getName()).willReturn("target");
        given(source.getTotalPrice()).willReturn(totalPrice);
        given(totalPrice.getCurrencyIso()).willReturn("AUD");
        given(totalPrice.getValue()).willReturn(new BigDecimal(10));
        given(source.getTotalTax()).willReturn(totalTax);
        given(totalTax.getValue()).willReturn(new BigDecimal(1));
        given(source.getCode()).willReturn("orderCode");
        given(source.getDeliveryCost()).willReturn(deliveryCost);
        given(deliveryCost.getValue()).willReturn(new BigDecimal(5));
        given(source.getDeliveryAddress()).willReturn(addressData);
        given(addressData.getTown()).willReturn("town");
        given(addressData.getPostalCode()).willReturn("1234");
        given(addressData.getCountry()).willReturn(countryData);
        given(countryData.getName()).willReturn("Australia");
        given(source.getDeliveryMode()).willReturn(deliveryMode);
        given(deliveryMode.getName()).willReturn("CnC");
        given(source.getAppliedVoucherCodes()).willReturn(Arrays.asList("Voucher1", "Voucher2"));
        given(source.getAffiliateOrderData()).willReturn(affiliateOrderData);
        given(affiliateOrderData.getCodes()).willReturn("affiliateOrderCodes");
        given(affiliateOrderData.getQuantities()).willReturn("affiliateOrderQuantities");
        given(affiliateOrderData.getPrices()).willReturn("affiliateOrderPrices");
        final List<OrderEntryData> orderEntries = new ArrayList<>();
        orderEntries.add(orderEntry1);
        orderEntries.add(orderEntry2);
        given(source.getEntries()).willReturn(orderEntries);
        given(orderEntry1.getProduct()).willReturn(targetProduct1);
        given(orderEntry1.getDepartmentCode()).willReturn("355");
        given(targetProduct1.getBaseProductCode()).willReturn("baseProductCode1");
        given(targetProduct1.getCode()).willReturn("productCode1");
        given(targetProduct1.getName()).willReturn("Product 1");
        given(targetProduct1.getBaseName()).willReturn("Base Product 1");
        given(targetProduct1.getTopLevelCategory()).willReturn("Top Category 1");
        given(targetProduct1.getBrand()).willReturn("Brand 1");
        willReturn(Boolean.TRUE).given(targetProduct1).isAssorted();
        final List<CategoryData> categories = new ArrayList<>();
        final CategoryData category1 = mock(CategoryData.class);
        given(category1.getCode()).willReturn("C1");
        given(category1.getName()).willReturn("Category 1");
        categories.add(category1);
        final CategoryData category2 = mock(CategoryData.class);
        given(category2.getCode()).willReturn("C2");
        given(category2.getName()).willReturn("Category 2");
        categories.add(category2);
        given(targetProduct1.getCategories()).willReturn(categories);
        given(orderEntry1.getBasePrice()).willReturn(orderEntry1Price);
        given(orderEntry2.getProduct()).willReturn(targetProduct2);
        given(orderEntry2.getDepartmentCode()).willReturn("455");
        given(targetProduct2.getBaseProductCode()).willReturn("baseProductCode2");
        given(targetProduct2.getCode()).willReturn("productCode2");
        given(targetProduct2.getName()).willReturn("Product 2");
        given(targetProduct2.getBaseName()).willReturn("Base Product 2");
        given(targetProduct2.getTopLevelCategory()).willReturn("Top Category 2");
        given(targetProduct2.getBrand()).willReturn("Brand 2");
        willReturn(Boolean.FALSE).given(targetProduct2).isAssorted();
        given(orderEntry2.getBasePrice()).willReturn(orderEntry2Price);
        target = new OrderDetailGaData();
    }

    @Test
    public void testPopulate() {
        orderDetailGaPopulator.populate(source, target);
        assertThat(target.getEcommCurrency()).isEqualTo("AUD");
        assertThat(target.getEcommTotalValue()).isEqualTo(new BigDecimal(10));
        assertThat(target.getEcommTotalTax()).isEqualTo(new BigDecimal(1));
        assertThat(target.getEcommSite()).isEqualTo("target");
        assertThat(target.getEcommCode()).isEqualTo("orderCode");
        assertThat(target.getEcommDeliveryValue()).isEqualTo(new BigDecimal(5));
        assertThat(target.getEcommDeliveryTown()).isEqualTo("town");
        assertThat(target.getEcommDeliveryPostCode()).isEqualTo("1234");
        assertThat(target.getEcommDeliveryCountry()).isEqualTo("Australia");
        assertThat(target.getEcommDeliveryMode()).isEqualTo("CnC");
        assertThat(target.getEcommVoucher()).isEqualTo("Voucher1");
        assertThat(target.getAffiliateCodes()).isEqualTo("affiliateOrderCodes");
        assertThat(target.getAffiliateQuantities()).isEqualTo("affiliateOrderQuantities");
        assertThat(target.getAffiliatePrices()).isEqualTo("affiliateOrderPrices");
        assertThat(target.getEcommEntries()).onProperty("baseCode").containsExactly("baseProductCode1",
                "baseProductCode2");
        assertThat(target.getEcommEntries()).onProperty("code").containsExactly("productCode1", "productCode2");
        assertThat(target.getEcommEntries()).onProperty("name").containsExactly("Product 1", "Product 2");
        assertThat(target.getEcommEntries()).onProperty("category").containsExactly("C2/Category 2", null);
        assertThat(target.getEcommEntries()).onProperty("baseName").containsExactly("Base Product 1", "Base Product 2");
        assertThat(target.getEcommEntries()).onProperty("topLevelCategory").containsExactly("Top Category 1",
                "Top Category 2");
        assertThat(target.getEcommEntries()).onProperty("departmentCode").containsExactly("355",
                "455");
        assertThat(target.getEcommEntries()).onProperty("brand").containsExactly("Brand 1", "Brand 2");
        assertThat(target.getEcommEntries()).onProperty("assorted").containsExactly(Boolean.TRUE, Boolean.FALSE);
    }

    @Test
    public void testPopulateWithoutTotalPrice() {
        given(source.getTotalPrice()).willReturn(null);
        orderDetailGaPopulator.populate(source, target);
        assertThat(target.getEcommCurrency()).isEqualTo(null);
        assertThat(target.getEcommTotalValue()).isEqualTo(null);
        assertThat(target.getEcommTotalTax()).isEqualTo(new BigDecimal(1));
        assertThat(target.getEcommSite()).isEqualTo("target");
        assertThat(target.getEcommCode()).isEqualTo("orderCode");
        assertThat(target.getEcommDeliveryValue()).isEqualTo(new BigDecimal(5));
        assertThat(target.getEcommDeliveryTown()).isEqualTo("town");
        assertThat(target.getEcommDeliveryPostCode()).isEqualTo("1234");
        assertThat(target.getEcommDeliveryCountry()).isEqualTo("Australia");
        assertThat(target.getEcommDeliveryMode()).isEqualTo("CnC");
        assertThat(target.getEcommVoucher()).isEqualTo("Voucher1");
        assertThat(target.getAffiliateCodes()).isEqualTo("affiliateOrderCodes");
        assertThat(target.getAffiliateQuantities()).isEqualTo("affiliateOrderQuantities");
        assertThat(target.getAffiliatePrices()).isEqualTo("affiliateOrderPrices");
        assertThat(target.getEcommEntries()).onProperty("baseCode").containsExactly("baseProductCode1",
                "baseProductCode2");
        assertThat(target.getEcommEntries()).onProperty("code").containsExactly("productCode1", "productCode2");
        assertThat(target.getEcommEntries()).onProperty("name").containsExactly("Product 1", "Product 2");
        assertThat(target.getEcommEntries()).onProperty("category").containsExactly("C2/Category 2", null);
        assertThat(target.getEcommEntries()).onProperty("baseName").containsExactly("Base Product 1", "Base Product 2");
        assertThat(target.getEcommEntries()).onProperty("topLevelCategory").containsExactly("Top Category 1",
                "Top Category 2");
        assertThat(target.getEcommEntries()).onProperty("departmentCode").containsExactly("355",
                "455");
        assertThat(target.getEcommEntries()).onProperty("brand").containsExactly("Brand 1", "Brand 2");
        assertThat(target.getEcommEntries()).onProperty("assorted").containsExactly(Boolean.TRUE, Boolean.FALSE);
    }

    @Test
    public void testPopulateWithoutTotalTax() {
        given(source.getTotalTax()).willReturn(null);
        orderDetailGaPopulator.populate(source, target);
        assertThat(target.getEcommCurrency()).isEqualTo("AUD");
        assertThat(target.getEcommTotalValue()).isEqualTo(new BigDecimal(10));
        assertThat(target.getEcommTotalTax()).isEqualTo(null);
        assertThat(target.getEcommSite()).isEqualTo("target");
        assertThat(target.getEcommCode()).isEqualTo("orderCode");
        assertThat(target.getEcommDeliveryValue()).isEqualTo(new BigDecimal(5));
        assertThat(target.getEcommDeliveryTown()).isEqualTo("town");
        assertThat(target.getEcommDeliveryPostCode()).isEqualTo("1234");
        assertThat(target.getEcommDeliveryCountry()).isEqualTo("Australia");
        assertThat(target.getEcommDeliveryMode()).isEqualTo("CnC");
        assertThat(target.getEcommVoucher()).isEqualTo("Voucher1");
        assertThat(target.getAffiliateCodes()).isEqualTo("affiliateOrderCodes");
        assertThat(target.getAffiliateQuantities()).isEqualTo("affiliateOrderQuantities");
        assertThat(target.getAffiliatePrices()).isEqualTo("affiliateOrderPrices");
        assertThat(target.getEcommEntries()).onProperty("baseCode").containsExactly("baseProductCode1",
                "baseProductCode2");
        assertThat(target.getEcommEntries()).onProperty("code").containsExactly("productCode1", "productCode2");
        assertThat(target.getEcommEntries()).onProperty("name").containsExactly("Product 1", "Product 2");
        assertThat(target.getEcommEntries()).onProperty("category").containsExactly("C2/Category 2", null);
        assertThat(target.getEcommEntries()).onProperty("baseName").containsExactly("Base Product 1", "Base Product 2");
        assertThat(target.getEcommEntries()).onProperty("topLevelCategory").containsExactly("Top Category 1",
                "Top Category 2");
        assertThat(target.getEcommEntries()).onProperty("departmentCode").containsExactly("355",
                "455");
        assertThat(target.getEcommEntries()).onProperty("brand").containsExactly("Brand 1", "Brand 2");
        assertThat(target.getEcommEntries()).onProperty("assorted").containsExactly(Boolean.TRUE, Boolean.FALSE);
    }

    @Test
    public void testPopulateWithoutDeliveryCost() {
        given(source.getDeliveryCost()).willReturn(null);
        orderDetailGaPopulator.populate(source, target);
        assertThat(target.getEcommCurrency()).isEqualTo("AUD");
        assertThat(target.getEcommTotalValue()).isEqualTo(new BigDecimal(10));
        assertThat(target.getEcommTotalTax()).isEqualTo(new BigDecimal(1));
        assertThat(target.getEcommSite()).isEqualTo("target");
        assertThat(target.getEcommCode()).isEqualTo("orderCode");
        assertThat(target.getEcommDeliveryValue()).isEqualTo(null);
        assertThat(target.getEcommDeliveryTown()).isEqualTo("town");
        assertThat(target.getEcommDeliveryPostCode()).isEqualTo("1234");
        assertThat(target.getEcommDeliveryCountry()).isEqualTo("Australia");
        assertThat(target.getEcommDeliveryMode()).isEqualTo("CnC");
        assertThat(target.getEcommVoucher()).isEqualTo("Voucher1");
        assertThat(target.getAffiliateCodes()).isEqualTo("affiliateOrderCodes");
        assertThat(target.getAffiliateQuantities()).isEqualTo("affiliateOrderQuantities");
        assertThat(target.getAffiliatePrices()).isEqualTo("affiliateOrderPrices");
        assertThat(target.getEcommEntries()).onProperty("baseCode").containsExactly("baseProductCode1",
                "baseProductCode2");
        assertThat(target.getEcommEntries()).onProperty("code").containsExactly("productCode1", "productCode2");
        assertThat(target.getEcommEntries()).onProperty("name").containsExactly("Product 1", "Product 2");
        assertThat(target.getEcommEntries()).onProperty("category").containsExactly("C2/Category 2", null);
        assertThat(target.getEcommEntries()).onProperty("baseName").containsExactly("Base Product 1", "Base Product 2");
        assertThat(target.getEcommEntries()).onProperty("topLevelCategory").containsExactly("Top Category 1",
                "Top Category 2");
        assertThat(target.getEcommEntries()).onProperty("brand").containsExactly("Brand 1", "Brand 2");
        assertThat(target.getEcommEntries()).onProperty("assorted").containsExactly(Boolean.TRUE, Boolean.FALSE);
    }

    @Test
    public void testPopulateWithoutDeliveryAddress() {
        given(source.getDeliveryAddress()).willReturn(null);
        orderDetailGaPopulator.populate(source, target);
        assertThat(target.getEcommCurrency()).isEqualTo("AUD");
        assertThat(target.getEcommTotalValue()).isEqualTo(new BigDecimal(10));
        assertThat(target.getEcommTotalTax()).isEqualTo(new BigDecimal(1));
        assertThat(target.getEcommCode()).isEqualTo("orderCode");
        assertThat(target.getEcommDeliveryValue()).isEqualTo(new BigDecimal(5));
        assertThat(target.getEcommDeliveryTown()).isEqualTo(null);
        assertThat(target.getEcommDeliveryPostCode()).isEqualTo(null);
        assertThat(target.getEcommDeliveryCountry()).isEqualTo(null);
        assertThat(target.getEcommDeliveryMode()).isEqualTo("CnC");
        assertThat(target.getEcommVoucher()).isEqualTo("Voucher1");
        assertThat(target.getAffiliateCodes()).isEqualTo("affiliateOrderCodes");
        assertThat(target.getAffiliateQuantities()).isEqualTo("affiliateOrderQuantities");
        assertThat(target.getAffiliatePrices()).isEqualTo("affiliateOrderPrices");
        assertThat(target.getEcommEntries()).onProperty("baseCode").containsExactly("baseProductCode1",
                "baseProductCode2");
        assertThat(target.getEcommEntries()).onProperty("code").containsExactly("productCode1", "productCode2");
        assertThat(target.getEcommEntries()).onProperty("name").containsExactly("Product 1", "Product 2");
        assertThat(target.getEcommEntries()).onProperty("category").containsExactly("C2/Category 2", null);
        assertThat(target.getEcommEntries()).onProperty("baseName").containsExactly("Base Product 1", "Base Product 2");
        assertThat(target.getEcommEntries()).onProperty("topLevelCategory").containsExactly("Top Category 1",
                "Top Category 2");
        assertThat(target.getEcommEntries()).onProperty("brand").containsExactly("Brand 1", "Brand 2");
        assertThat(target.getEcommEntries()).onProperty("assorted").containsExactly(Boolean.TRUE, Boolean.FALSE);
    }

    @Test
    public void testPopulateWithoutDeliveryAddressCountry() {
        given(source.getDeliveryAddress().getCountry()).willReturn(null);
        orderDetailGaPopulator.populate(source, target);
        assertThat(target.getEcommCurrency()).isEqualTo("AUD");
        assertThat(target.getEcommTotalValue()).isEqualTo(new BigDecimal(10));
        assertThat(target.getEcommTotalTax()).isEqualTo(new BigDecimal(1));
        assertThat(target.getEcommSite()).isEqualTo("target");
        assertThat(target.getEcommCode()).isEqualTo("orderCode");
        assertThat(target.getEcommDeliveryValue()).isEqualTo(new BigDecimal(5));
        assertThat(target.getEcommDeliveryTown()).isEqualTo("town");
        assertThat(target.getEcommDeliveryPostCode()).isEqualTo("1234");
        assertThat(target.getEcommDeliveryCountry()).isEqualTo(null);
        assertThat(target.getEcommDeliveryMode()).isEqualTo("CnC");
        assertThat(target.getEcommVoucher()).isEqualTo("Voucher1");
        assertThat(target.getAffiliateCodes()).isEqualTo("affiliateOrderCodes");
        assertThat(target.getAffiliateQuantities()).isEqualTo("affiliateOrderQuantities");
        assertThat(target.getAffiliatePrices()).isEqualTo("affiliateOrderPrices");
        assertThat(target.getEcommEntries()).onProperty("baseCode").containsExactly("baseProductCode1",
                "baseProductCode2");
        assertThat(target.getEcommEntries()).onProperty("code").containsExactly("productCode1", "productCode2");
        assertThat(target.getEcommEntries()).onProperty("name").containsExactly("Product 1", "Product 2");
        assertThat(target.getEcommEntries()).onProperty("category").containsExactly("C2/Category 2", null);
        assertThat(target.getEcommEntries()).onProperty("baseName").containsExactly("Base Product 1", "Base Product 2");
        assertThat(target.getEcommEntries()).onProperty("topLevelCategory").containsExactly("Top Category 1",
                "Top Category 2");
        assertThat(target.getEcommEntries()).onProperty("brand").containsExactly("Brand 1", "Brand 2");
        assertThat(target.getEcommEntries()).onProperty("assorted").containsExactly(Boolean.TRUE, Boolean.FALSE);
    }

    @Test
    public void testPopulateWithoutDeliveryMode() {
        given(source.getDeliveryMode()).willReturn(null);
        orderDetailGaPopulator.populate(source, target);
        assertThat(target.getEcommCurrency()).isEqualTo("AUD");
        assertThat(target.getEcommTotalValue()).isEqualTo(new BigDecimal(10));
        assertThat(target.getEcommTotalTax()).isEqualTo(new BigDecimal(1));
        assertThat(target.getEcommCode()).isEqualTo("orderCode");
        assertThat(target.getEcommDeliveryValue()).isEqualTo(new BigDecimal(5));
        assertThat(target.getEcommDeliveryTown()).isEqualTo("town");
        assertThat(target.getEcommDeliveryPostCode()).isEqualTo("1234");
        assertThat(target.getEcommDeliveryCountry()).isEqualTo("Australia");
        assertThat(target.getEcommDeliveryMode()).isEqualTo(null);
        assertThat(target.getEcommVoucher()).isEqualTo("Voucher1");
        assertThat(target.getAffiliateCodes()).isEqualTo("affiliateOrderCodes");
        assertThat(target.getAffiliateQuantities()).isEqualTo("affiliateOrderQuantities");
        assertThat(target.getAffiliatePrices()).isEqualTo("affiliateOrderPrices");
        assertThat(target.getEcommEntries()).onProperty("baseCode").containsExactly("baseProductCode1",
                "baseProductCode2");
        assertThat(target.getEcommEntries()).onProperty("code").containsExactly("productCode1", "productCode2");
        assertThat(target.getEcommEntries()).onProperty("name").containsExactly("Product 1", "Product 2");
        assertThat(target.getEcommEntries()).onProperty("category").containsExactly("C2/Category 2", null);
        assertThat(target.getEcommEntries()).onProperty("baseName").containsExactly("Base Product 1", "Base Product 2");
        assertThat(target.getEcommEntries()).onProperty("topLevelCategory").containsExactly("Top Category 1",
                "Top Category 2");
        assertThat(target.getEcommEntries()).onProperty("brand").containsExactly("Brand 1", "Brand 2");
        assertThat(target.getEcommEntries()).onProperty("assorted").containsExactly(Boolean.TRUE, Boolean.FALSE);
    }

    @Test
    public void testPopulateWithoutVoucher() {
        given(source.getAppliedVoucherCodes()).willReturn(null);
        orderDetailGaPopulator.populate(source, target);
        assertThat(target.getEcommCurrency()).isEqualTo("AUD");
        assertThat(target.getEcommTotalValue()).isEqualTo(new BigDecimal(10));
        assertThat(target.getEcommTotalTax()).isEqualTo(new BigDecimal(1));
        assertThat(target.getEcommCode()).isEqualTo("orderCode");
        assertThat(target.getEcommDeliveryValue()).isEqualTo(new BigDecimal(5));
        assertThat(target.getEcommDeliveryTown()).isEqualTo("town");
        assertThat(target.getEcommDeliveryPostCode()).isEqualTo("1234");
        assertThat(target.getEcommDeliveryCountry()).isEqualTo("Australia");
        assertThat(target.getEcommDeliveryMode()).isEqualTo("CnC");
        assertThat(target.getEcommVoucher()).isEqualTo(null);
        assertThat(target.getAffiliateCodes()).isEqualTo("affiliateOrderCodes");
        assertThat(target.getAffiliateQuantities()).isEqualTo("affiliateOrderQuantities");
        assertThat(target.getAffiliatePrices()).isEqualTo("affiliateOrderPrices");
        assertThat(target.getEcommEntries()).onProperty("baseCode").containsExactly("baseProductCode1",
                "baseProductCode2");
        assertThat(target.getEcommEntries()).onProperty("code").containsExactly("productCode1", "productCode2");
        assertThat(target.getEcommEntries()).onProperty("name").containsExactly("Product 1", "Product 2");
        assertThat(target.getEcommEntries()).onProperty("category").containsExactly("C2/Category 2", null);
        assertThat(target.getEcommEntries()).onProperty("baseName").containsExactly("Base Product 1", "Base Product 2");
        assertThat(target.getEcommEntries()).onProperty("topLevelCategory").containsExactly("Top Category 1",
                "Top Category 2");
        assertThat(target.getEcommEntries()).onProperty("brand").containsExactly("Brand 1", "Brand 2");
        assertThat(target.getEcommEntries()).onProperty("assorted").containsExactly(Boolean.TRUE, Boolean.FALSE);
    }

    @Test
    public void testPopulateWithoutAffiliateOrderData() {
        given(source.getAffiliateOrderData()).willReturn(null);
        orderDetailGaPopulator.populate(source, target);
        assertThat(target.getEcommCurrency()).isEqualTo("AUD");
        assertThat(target.getEcommTotalValue()).isEqualTo(new BigDecimal(10));
        assertThat(target.getEcommTotalTax()).isEqualTo(new BigDecimal(1));
        assertThat(target.getEcommSite()).isEqualTo("target");
        assertThat(target.getEcommCode()).isEqualTo("orderCode");
        assertThat(target.getEcommDeliveryValue()).isEqualTo(new BigDecimal(5));
        assertThat(target.getEcommDeliveryTown()).isEqualTo("town");
        assertThat(target.getEcommDeliveryPostCode()).isEqualTo("1234");
        assertThat(target.getEcommDeliveryCountry()).isEqualTo("Australia");
        assertThat(target.getEcommDeliveryMode()).isEqualTo("CnC");
        assertThat(target.getEcommVoucher()).isEqualTo("Voucher1");
        assertThat(target.getAffiliateCodes()).isEqualTo(null);
        assertThat(target.getAffiliateQuantities()).isEqualTo(null);
        assertThat(target.getAffiliatePrices()).isEqualTo(null);
        assertThat(target.getEcommEntries()).onProperty("baseCode").containsExactly("baseProductCode1",
                "baseProductCode2");
        assertThat(target.getEcommEntries()).onProperty("code").containsExactly("productCode1", "productCode2");
        assertThat(target.getEcommEntries()).onProperty("name").containsExactly("Product 1", "Product 2");
        assertThat(target.getEcommEntries()).onProperty("category").containsExactly("C2/Category 2", null);
        assertThat(target.getEcommEntries()).onProperty("baseName").containsExactly("Base Product 1", "Base Product 2");
        assertThat(target.getEcommEntries()).onProperty("topLevelCategory").containsExactly("Top Category 1",
                "Top Category 2");
        assertThat(target.getEcommEntries()).onProperty("brand").containsExactly("Brand 1", "Brand 2");
        assertThat(target.getEcommEntries()).onProperty("assorted").containsExactly(Boolean.TRUE, Boolean.FALSE);
    }

    @Test
    public void testPopulateWithoutEntries() {
        given(source.getEntries()).willReturn(null);
        orderDetailGaPopulator.populate(source, target);
        assertThat(target.getEcommCurrency()).isEqualTo("AUD");
        assertThat(target.getEcommTotalValue()).isEqualTo(new BigDecimal(10));
        assertThat(target.getEcommTotalTax()).isEqualTo(new BigDecimal(1));
        assertThat(target.getEcommCode()).isEqualTo("orderCode");
        assertThat(target.getEcommDeliveryValue()).isEqualTo(new BigDecimal(5));
        assertThat(target.getEcommDeliveryTown()).isEqualTo("town");
        assertThat(target.getEcommDeliveryPostCode()).isEqualTo("1234");
        assertThat(target.getEcommDeliveryCountry()).isEqualTo("Australia");
        assertThat(target.getEcommDeliveryMode()).isEqualTo("CnC");
        assertThat(target.getEcommVoucher()).isEqualTo("Voucher1");
        assertThat(target.getAffiliateCodes()).isEqualTo("affiliateOrderCodes");
        assertThat(target.getAffiliateQuantities()).isEqualTo("affiliateOrderQuantities");
        assertThat(target.getAffiliatePrices()).isEqualTo("affiliateOrderPrices");
        assertThat(target.getEcommEntries()).isNull();
    }

    @Test
    public void testPopulateWithoutProductInEntry() {
        given(source.getEntries().get(1).getProduct()).willReturn(null);
        orderDetailGaPopulator.populate(source, target);
        assertThat(target.getEcommCurrency()).isEqualTo("AUD");
        assertThat(target.getEcommTotalValue()).isEqualTo(new BigDecimal(10));
        assertThat(target.getEcommTotalTax()).isEqualTo(new BigDecimal(1));
        assertThat(target.getEcommSite()).isEqualTo("target");
        assertThat(target.getEcommCode()).isEqualTo("orderCode");
        assertThat(target.getEcommDeliveryValue()).isEqualTo(new BigDecimal(5));
        assertThat(target.getEcommDeliveryTown()).isEqualTo("town");
        assertThat(target.getEcommDeliveryPostCode()).isEqualTo("1234");
        assertThat(target.getEcommDeliveryCountry()).isEqualTo("Australia");
        assertThat(target.getEcommDeliveryMode()).isEqualTo("CnC");
        assertThat(target.getEcommVoucher()).isEqualTo("Voucher1");
        assertThat(target.getAffiliateCodes()).isEqualTo("affiliateOrderCodes");
        assertThat(target.getAffiliateQuantities()).isEqualTo("affiliateOrderQuantities");
        assertThat(target.getAffiliatePrices()).isEqualTo("affiliateOrderPrices");
        assertThat(target.getEcommEntries()).onProperty("baseCode").containsExactly("baseProductCode1", null);
        assertThat(target.getEcommEntries()).onProperty("code").containsExactly("productCode1", null);
        assertThat(target.getEcommEntries()).onProperty("name").containsExactly("Product 1", null);
        assertThat(target.getEcommEntries()).onProperty("category").containsExactly("C2/Category 2", null);
        assertThat(target.getEcommEntries()).onProperty("baseName").containsExactly("Base Product 1", null);
        assertThat(target.getEcommEntries()).onProperty("topLevelCategory").containsExactly("Top Category 1", null);
        assertThat(target.getEcommEntries()).onProperty("brand").containsExactly("Brand 1", null);
        assertThat(target.getEcommEntries()).onProperty("assorted").containsExactly(Boolean.TRUE, Boolean.FALSE);
    }
}

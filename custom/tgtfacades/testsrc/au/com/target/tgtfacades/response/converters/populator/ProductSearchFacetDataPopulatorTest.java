/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.data.TargetFacetData;
import au.com.target.tgtfacades.response.data.FacetOptionResponseData;
import au.com.target.tgtfacades.response.data.FacetResponseData;
import au.com.target.tgtfacades.response.data.ProductSearchResponseData;
import au.com.target.tgtfacades.search.data.TargetProductCategorySearchPageData;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductSearchFacetDataPopulatorTest {

    /**
     * 
     */
    private static final String ISOLATED_CODE = "code2";

    private final ProductSearchFacetDataPopulator populator = new ProductSearchFacetDataPopulator();

    @Before
    public void setUp() {
        populator.setIsolatedFacets(Arrays.asList(ISOLATED_CODE));
    }

    @Test
    public void testPopulate() {
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> source = new TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData>();
        final ProductSearchResponseData target = new ProductSearchResponseData();
        final List<TargetFacetData<EndecaSearchStateData>> facetDataList = new ArrayList<>();
        final TargetFacetData<EndecaSearchStateData> targetFacetData1 = new TargetFacetData<EndecaSearchStateData>();
        final String targetFacetData1Code = "code1";
        final String targetFacetData1Name = "name1";
        final boolean targetFacetData1Multi = true;
        final boolean targetFacetData1Expand = true;

        final FacetValueData<EndecaSearchStateData> facetOption1 = new FacetValueData<EndecaSearchStateData>();
        final String facetOption1Code = "option1Code";
        final long facetOption1Count = 21;
        final boolean facetOption1IsSelected = true;
        final String facetOption1Name = "option1Name";
        final EndecaSearchStateData facetOption1Query = new EndecaSearchStateData();
        final String facetOption1QueryUrl = "facetOption1QueryUrl";
        final String facetOption1QueryWsUrl = "facetOption1QueryWsUrl";
        facetOption1Query.setUrl(facetOption1QueryUrl);
        facetOption1Query.setWsUrl(facetOption1QueryWsUrl);
        facetOption1.setCode(facetOption1Code);
        facetOption1.setCount(facetOption1Count);
        facetOption1.setSelected(facetOption1IsSelected);
        facetOption1.setName(facetOption1Name);
        facetOption1.setQuery(facetOption1Query);

        final FacetValueData<EndecaSearchStateData> facetOption2 = new FacetValueData<EndecaSearchStateData>();
        final String facetOption2Code = "option2Code";
        final long facetOption2Count = 22;
        final boolean facetOption2IsSelected = false;
        final String facetOption2Name = "option2Name";
        final EndecaSearchStateData facetOption2Query = new EndecaSearchStateData();
        final String facetOption2QueryUrl = "facetOption2QueryUrl";
        final String facetOption2QueryWsUrl = "facetOption2QueryWsUrl";
        facetOption2Query.setUrl(facetOption2QueryUrl);
        facetOption2Query.setWsUrl(facetOption2QueryWsUrl);
        facetOption2.setCode(facetOption2Code);
        facetOption2.setCount(facetOption2Count);
        facetOption2.setSelected(facetOption2IsSelected);
        facetOption2.setName(facetOption2Name);
        facetOption2.setQuery(facetOption2Query);

        final List<FacetValueData<EndecaSearchStateData>> facetOptionList1 = new ArrayList<>(
                Arrays.asList(facetOption1, facetOption2));

        final TargetFacetData<EndecaSearchStateData> targetFacetData2 = new TargetFacetData<EndecaSearchStateData>();
        final String targetFacetData2Name = "name2";
        final boolean targetFacetData2Multi = true;
        final boolean targetFacetData2Expand = true;

        final TargetFacetData<EndecaSearchStateData> subFacetResponseData = new TargetFacetData<EndecaSearchStateData>();
        final String subFacetResponseDataId = "subFacetResponseDataId";
        final String subFacetResponseDataName = "subFacetResponseDataName";
        final boolean subFacetResponseDataMulti = false;
        final List<FacetValueData<EndecaSearchStateData>> subFacetResponseDataValues = new ArrayList<>();
        final FacetValueData<EndecaSearchStateData> subFacetResponseDataValues1 = new FacetValueData<>();
        final String subFacetResponseDataValues1Name = "subFacetResponseDataValues1Name";
        final String subFacetResponseDataValues1Code = "subFacetResponseDataValues1Code";
        final long subFacetResponseDataValues1Count = 12;
        final boolean subFacetResponseDataValues1IsSelected = false;
        subFacetResponseDataValues1.setCode(subFacetResponseDataValues1Code);
        subFacetResponseDataValues1.setName(subFacetResponseDataValues1Name);
        subFacetResponseDataValues1.setCount(subFacetResponseDataValues1Count);
        subFacetResponseDataValues1.setSelected(subFacetResponseDataValues1IsSelected);

        subFacetResponseDataValues.add(subFacetResponseDataValues1);
        subFacetResponseData.setCode(subFacetResponseDataId);
        subFacetResponseData.setName(subFacetResponseDataName);
        subFacetResponseData.setMultiSelect(subFacetResponseDataMulti);
        subFacetResponseData.setValues(subFacetResponseDataValues);
        final List<TargetFacetData<EndecaSearchStateData>> subFacetResponseDataList = new ArrayList<>(
                Arrays.asList(subFacetResponseData));

        targetFacetData1.setCode(targetFacetData1Code);
        targetFacetData1.setName(targetFacetData1Name);
        targetFacetData1.setMultiSelect(targetFacetData1Multi);
        targetFacetData1.setExpand(targetFacetData1Expand);
        targetFacetData1.setValues(facetOptionList1);
        targetFacetData1.setFacets(subFacetResponseDataList);
        targetFacetData1.setSeoFollowEnabled(true);

        targetFacetData2.setCode(ISOLATED_CODE);
        targetFacetData2.setName(targetFacetData2Name);
        targetFacetData2.setMultiSelect(targetFacetData2Multi);
        targetFacetData2.setExpand(targetFacetData2Expand);
        targetFacetData2.setValues(facetOptionList1);
        targetFacetData2.setIsolated(true);
        targetFacetData2.setFacets(subFacetResponseDataList);
        targetFacetData2.setSeoFollowEnabled(false);

        facetDataList.add(targetFacetData1);
        facetDataList.add(targetFacetData2);
        source.setFacets(facetDataList);

        populator.populate(source, target);

        assertThat(target.getFacetList()).isNotEmpty().hasSize(2);
        final List<FacetResponseData> facetResponseDataList = target.getFacetList();
        final FacetResponseData firstFacetResponseData = facetResponseDataList.get(0);
        assertThat(firstFacetResponseData).isNotNull();
        assertThat(firstFacetResponseData.getId()).isEqualTo(targetFacetData1Code);
        assertThat(firstFacetResponseData.getName()).isEqualTo(targetFacetData1Name);
        assertThat(firstFacetResponseData.getFacetOptionList()).isNotEmpty();
        assertThat(firstFacetResponseData.isExpand()).isEqualTo(targetFacetData1Expand);
        if (firstFacetResponseData.getId().equals(ISOLATED_CODE)) {
            assertThat(firstFacetResponseData.isIsolated()).isTrue();
        }
        else {
            assertThat(firstFacetResponseData.isIsolated()).isFalse();
        }
        assertThat(firstFacetResponseData.isIsolated()).isFalse();
        assertThat(firstFacetResponseData.isSeoFollowEnabled()).isTrue();
        final List<FacetOptionResponseData> firstFacetResponseDataOptionResponseDataList = firstFacetResponseData
                .getFacetOptionList();
        assertThat(firstFacetResponseDataOptionResponseDataList).isNotEmpty();
        final FacetOptionResponseData facetOptionResponseData1 = firstFacetResponseDataOptionResponseDataList.get(0);
        assertThat(facetOptionResponseData1.getId()).isEqualTo(facetOption1Code);
        assertThat(facetOptionResponseData1.getName()).isEqualTo(facetOption1Name);
        assertThat(facetOptionResponseData1.getCount()).isEqualTo(facetOption1Count);
        assertThat(facetOptionResponseData1.isSelected()).isEqualTo(facetOption1IsSelected);
        assertThat(facetOptionResponseData1.getUrl()).isEqualTo(facetOption1QueryUrl);
        assertThat(facetOptionResponseData1.getQueryUrl()).isEqualTo(facetOption1QueryWsUrl);
        final FacetOptionResponseData facetOptionResponseData2 = firstFacetResponseDataOptionResponseDataList.get(1);
        assertThat(facetOptionResponseData2.getId()).isEqualTo(facetOption2Code);
        assertThat(facetOptionResponseData2.getName()).isEqualTo(facetOption2Name);
        assertThat(facetOptionResponseData2.getCount()).isEqualTo(facetOption2Count);
        assertThat(facetOptionResponseData2.isSelected()).isEqualTo(facetOption2IsSelected);
        assertThat(facetOptionResponseData2.getUrl()).isEqualTo(facetOption2QueryUrl);
        assertThat(facetOptionResponseData2.getQueryUrl()).isEqualTo(facetOption2QueryWsUrl);

        assertThat(firstFacetResponseData.getFacetList()).isNotEmpty();
        final FacetResponseData subFacetResponseDataResult = firstFacetResponseData.getFacetList().get(0);
        assertThat(subFacetResponseDataResult.getName()).isEqualTo(subFacetResponseDataName);
        assertThat(subFacetResponseDataResult.getId()).isEqualTo(subFacetResponseDataId);
        assertThat(subFacetResponseDataResult.getFacetOptionList()).isNotEmpty();
        assertThat(subFacetResponseDataResult.getFacetOptionList().get(0).getName()).isEqualTo(
                subFacetResponseDataValues1Name);
        assertThat(subFacetResponseDataResult.getFacetOptionList().get(0).getId()).isEqualTo(
                subFacetResponseDataValues1Code);
        assertThat(subFacetResponseDataResult.getFacetOptionList().get(0).getCount()).isEqualTo(
                subFacetResponseDataValues1Count);
        assertThat(subFacetResponseDataResult.getFacetOptionList().get(0).isSelected()).isEqualTo(
                subFacetResponseDataValues1IsSelected);

        final FacetResponseData firstFacetResponseData1 = facetResponseDataList.get(1);
        if (firstFacetResponseData1.getId().equals(ISOLATED_CODE)) {
            assertThat(firstFacetResponseData1.isIsolated()).isTrue();
        }
        else {
            assertThat(firstFacetResponseData1.isIsolated()).isFalse();
        }
        assertThat(firstFacetResponseData1.isSeoFollowEnabled()).isFalse();
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.response.converters.populator;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.tgtcore.product.data.ProductDisplayType;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.product.data.enums.TargetPromotionStatusEnum;
import au.com.target.tgtfacades.response.data.ProductResponseData;
import au.com.target.tgtfacades.response.data.ProductSearchResponseData;
import au.com.target.tgtfacades.response.data.ProductVariantResponseData;
import au.com.target.tgtfacades.search.data.TargetProductCategorySearchPageData;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductSearchProductDataPopulatorTest {
    private final String variantColourVariantCode = "variantColourVariantCode";
    private final String variantUrl = "variantUrl";
    private final boolean variantInStock = true;
    private final String variantCoulourName = "variantCoulourName";
    private final String variantSwatchColour = "variantSwatchColour";
    private final String variantDisplayPrice = "variantDisplayPrice";
    private final String variantThumbImageUrl = "/variantThumbImageUrl";
    private final String primaryImageUrl = "/primaryImageUrl";
    private final String secondaryImageUrl = "/secoundaryImageUrl";
    private final Boolean assorted = Boolean.TRUE;
    private final boolean displayOnly = true;
    private final Integer bulkyBoardNumbmer = Integer.valueOf(5012);
    private final String code = "code";
    private final String name = "name";
    private final boolean inStock = true;
    private final boolean onlineExclusive = true;
    private final String baseProductCode = "baseProductCode";
    private final String baseName = "baseName";
    private final String brand = "brand";
    private final String topLevelCategory = "topLevelCategory";
    private final String url = "url";
    private final String productPromotionalDeliverySticker = "productPromotionalDeliverySticker";
    private final boolean newArrived = false;
    private final boolean giftCard = false;
    private final String description = "description";
    private final boolean showWasPrice = true;
    private final boolean preview = false;
    private final Integer numberOfReview = Integer.valueOf(10);
    private final Double averageRating = Double.valueOf(4.5d);
    private final String dealDescription = "dealDescription";
    private final PriceRangeData priceRange = new PriceRangeData();
    private final PriceRangeData waspriceRange = new PriceRangeData();
    private final PriceData wasPrice = new PriceData();
    private final PriceData price = new PriceData();
    private final Integer maxAvailOnlineQty = Integer.valueOf(3);
    private final Integer maxAvailStoreQty = Integer.valueOf(6);
    private final String promotionStatus = "clearance";
    private final ProductDisplayType prdDisplayType = ProductDisplayType.AVAILABLE_FOR_SALE;
    private final Calendar normalSaleStartDate = Calendar.getInstance();


    private final ProductSearchProductDataPopulator populator = new ProductSearchProductDataPopulator();

    @Test
    public void testPopulate() {
        final TargetVariantProductListerData targetVariantProductListerData = createTestVariantProductListerData();

        final TargetProductListerData targetProductListerData = createTestProductListerData();
        targetProductListerData.setTargetVariantProductListerData(Arrays.asList(targetVariantProductListerData));

        final List<TargetProductListerData> productListerDataList = new ArrayList<>(
                Arrays.asList(targetProductListerData));

        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> source = new TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData>();

        source.setResults(productListerDataList);
        source.setBulkyBoardNumber(bulkyBoardNumbmer);


        final ProductSearchResponseData target = new ProductSearchResponseData();
        populator.populate(source, target);

        verifyResult(targetProductListerData, targetVariantProductListerData, target);
    }

    private TargetVariantProductListerData createTestVariantProductListerData() {
        final TargetVariantProductListerData targetVariantProductListerData = new TargetVariantProductListerData();
        targetVariantProductListerData
                .setGridImageUrls(Arrays.asList("01**" + primaryImageUrl, "01**" + secondaryImageUrl));
        targetVariantProductListerData.setAssorted(assorted);
        targetVariantProductListerData.setDisplayOnly(displayOnly);
        targetVariantProductListerData.setThumbImageUrl(Arrays.asList("01**" + variantThumbImageUrl));
        targetVariantProductListerData.setColourVariantCode(variantColourVariantCode);
        targetVariantProductListerData.setUrl(variantUrl);
        targetVariantProductListerData.setInStock(variantInStock);
        targetVariantProductListerData.setColourName(variantCoulourName);
        targetVariantProductListerData.setSwatchColour(variantSwatchColour);
        targetVariantProductListerData.setDisplayPrice(variantDisplayPrice);
        targetVariantProductListerData.setProductDisplayType(prdDisplayType);
        normalSaleStartDate.set(2018, Calendar.MAY, 17);
        targetVariantProductListerData.setNormalSaleStartDate(normalSaleStartDate.getTime());

        return targetVariantProductListerData;
    }

    private TargetProductListerData createTestProductListerData() {
        final TargetProductListerData targetProductListerData = new TargetProductListerData();
        targetProductListerData.setCode(code);
        targetProductListerData.setName(name);
        targetProductListerData.setInStock(inStock);
        targetProductListerData.setOnlineExclusive(onlineExclusive);
        targetProductListerData.setBaseName(baseName);
        targetProductListerData.setBaseProductCode(baseProductCode);
        targetProductListerData.setBrand(brand);
        targetProductListerData.setTopLevelCategory(topLevelCategory);
        targetProductListerData.setUrl(url);
        targetProductListerData.setProductPromotionalDeliverySticker(productPromotionalDeliverySticker);
        targetProductListerData.setNewArrived(newArrived);
        targetProductListerData.setGiftCard(giftCard);
        targetProductListerData.setDescription(description);
        targetProductListerData.setShowWasPrice(showWasPrice);
        targetProductListerData.setPreview(preview);
        targetProductListerData.setNumberOfReviews(numberOfReview);
        targetProductListerData.setAverageRating(averageRating);
        targetProductListerData.setDealDescription(dealDescription);
        targetProductListerData.setPromotionStatuses(Arrays.asList(TargetPromotionStatusEnum.CLEARANCE));
        targetProductListerData.setPriceRange(priceRange);
        targetProductListerData.setWasPriceRange(waspriceRange);
        targetProductListerData.setPrice(price);
        targetProductListerData.setWasPrice(wasPrice);
        targetProductListerData.setMaxAvailOnlineQty(maxAvailOnlineQty);
        targetProductListerData.setMaxAvailStoreQty(maxAvailStoreQty);

        return targetProductListerData;
    }

    private void verifyResult(final TargetProductListerData targetProductListerData,
            final TargetVariantProductListerData targetVariantProductListerData,
            final ProductSearchResponseData target) {
        assertThat(target.getProductDataList()).isNotEmpty();
        final ProductResponseData productResponseData = target.getProductDataList().get(0);
        assertThat(productResponseData.getCode()).isEqualTo(targetProductListerData.getCode());
        assertThat(productResponseData.getName()).isEqualTo(targetProductListerData.getName());
        assertThat(productResponseData.isInStock()).isEqualTo(targetProductListerData.isInStock());
        assertThat(productResponseData.isOnlineExclusive()).isEqualTo(targetProductListerData.isOnlineExclusive());
        assertThat(productResponseData.getPrimaryImageUrl()).isEqualTo(primaryImageUrl);
        assertThat(productResponseData.getSecondaryImageUrl()).isEqualTo(secondaryImageUrl);
        assertThat(productResponseData.getBaseProductCode()).isEqualTo(targetProductListerData.getBaseProductCode());
        assertThat(productResponseData.getBaseName()).isEqualTo(targetProductListerData.getBaseName());
        assertThat(productResponseData.getBrand()).isEqualTo(targetProductListerData.getBrand());
        assertThat(productResponseData.getTopLevelCategory()).isEqualTo(targetProductListerData.getTopLevelCategory());
        assertThat(productResponseData.getUrl()).isEqualTo(url + "?bbsn=" + bulkyBoardNumbmer);
        assertThat(productResponseData.getProductPromotionalDeliverySticker()).isEqualTo(
                targetProductListerData.getProductPromotionalDeliverySticker());
        assertThat(productResponseData.isNewArrived()).isEqualTo(targetProductListerData.isNewArrived());
        assertThat(productResponseData.isGiftCard()).isEqualTo(targetProductListerData.isGiftCard());
        assertThat(productResponseData.getDescription()).isEqualTo(targetProductListerData.getDescription());
        assertThat(productResponseData.isShowWasPrice()).isEqualTo(targetProductListerData.isShowWasPrice());
        assertThat(productResponseData.isPreview()).isEqualTo(targetProductListerData.isPreview());
        assertThat(productResponseData.getNumberOfReviews()).isEqualTo(targetProductListerData.getNumberOfReviews());
        assertThat(productResponseData.getAverageRating()).isEqualTo(targetProductListerData.getAverageRating());
        assertThat(productResponseData.getDealDescription()).isEqualTo(targetProductListerData.getDealDescription());
        assertThat(productResponseData.getAssorted()).isEqualTo(assorted);
        assertThat(productResponseData.getDisplayOnly()).isEqualTo(displayOnly);
        assertThat(productResponseData.getPromotionStatus()).isEqualTo(promotionStatus);
        assertThat(productResponseData.getPriceRange()).isEqualTo(targetProductListerData.getPriceRange());
        assertThat(productResponseData.getWasPriceRange()).isEqualTo(targetProductListerData.getWasPriceRange());
        assertThat(productResponseData.getWasPrice()).isEqualTo(targetProductListerData.getWasPrice());
        assertThat(productResponseData.getPrice()).isEqualTo(targetProductListerData.getPrice());
        assertThat(productResponseData.getMaxAvailOnlineQty())
                .isEqualTo(targetProductListerData.getMaxAvailOnlineQty());
        assertThat(productResponseData.getMaxAvailStoreQty()).isEqualTo(targetProductListerData.getMaxAvailStoreQty());
        assertThat(productResponseData.getProductVariantDataList()).isNotEmpty();
        final ProductVariantResponseData productVariantResponseData = productResponseData.getProductVariantDataList()
                .get(0);
        assertThat(productVariantResponseData.getColourVariantCode())
                .isEqualTo(targetVariantProductListerData.getColourVariantCode());
        assertThat(productVariantResponseData.getUrl()).isEqualTo(variantUrl + "?bbsn=" + bulkyBoardNumbmer);
        assertThat(productVariantResponseData.isInStock()).isEqualTo(targetVariantProductListerData.isInStock());
        assertThat(productVariantResponseData.getColourName())
                .isEqualTo(targetVariantProductListerData.getColourName());
        assertThat(productVariantResponseData.getSwatchColour())
                .isEqualTo(targetVariantProductListerData.getSwatchColour());
        assertThat(productVariantResponseData.getDisplayPrice())
                .isEqualTo(targetVariantProductListerData.getDisplayPrice());
        assertThat(productVariantResponseData.getAssorted()).isEqualTo(assorted);
        assertThat(productVariantResponseData.getDisplayOnly()).isEqualTo(displayOnly);
        assertThat(productVariantResponseData.getThumbImageUrl()).isEqualTo(variantThumbImageUrl);
        assertThat(productVariantResponseData.getGridImageUrl()).isEqualTo(primaryImageUrl);
        assertThat(productVariantResponseData.getProductDisplayType()).isEqualTo(prdDisplayType);
        assertThat(productVariantResponseData.getNormalSaleStartDate()).isEqualTo("17 May");
    }
}

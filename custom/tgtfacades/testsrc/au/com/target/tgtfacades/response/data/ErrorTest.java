/**
 * 
 */
package au.com.target.tgtfacades.response.data;

import de.hybris.bootstrap.annotations.UnitTest;

import org.fest.assertions.Assertions;
import org.junit.Test;


/**
 * @author htan3
 *
 */
@UnitTest
public class ErrorTest {

    @Test
    public void testToStringWithCodeOnly() {
        final Error error = new Error("code");
        Assertions.assertThat(error.toString()).isEqualTo("ERROR=code(null)  , requestUri=null ,\n StackTrace:null");
    }

    @Test
    public void testToStringWithCodeAndMsg() {
        final Error error = new Error("code");
        error.setMessage("msg");
        Assertions.assertThat(error.toString()).isEqualTo("ERROR=code(msg)  , requestUri=null ,\n StackTrace:null");
    }
}

package au.com.target.tgtfacades.search.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.StockData;

import junit.framework.Assert;

import org.junit.Test;

import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.search.TargetProductHideStrategy;


@UnitTest
public class OutOfStockHideStrategyTest {
    private final TargetProductHideStrategy stategy = new OutOfStockHideStrategy();
    private final StockData stockData = new StockData();

    @Test
    public void testHideThisProductWhenOutOfStock() {
        final TargetProductData productData = new TargetProductData();
        stockData.setStockLevelStatus(StockLevelStatus.OUTOFSTOCK);
        productData.setStock(stockData);

        Assert.assertTrue(stategy.hideThisProduct(productData));
    }

    @Test
    public void testHideThisProductWhenInStock() {
        final TargetProductData productData = new TargetProductData();
        stockData.setStockLevelStatus(StockLevelStatus.INSTOCK);
        productData.setStock(stockData);

        Assert.assertFalse(stategy.hideThisProduct(productData));
    }

    @Test
    public void testHideThisProductWhenLowStock() {
        final TargetProductData productData = new TargetProductData();
        stockData.setStockLevelStatus(StockLevelStatus.LOWSTOCK);
        productData.setStock(stockData);

        Assert.assertFalse(stategy.hideThisProduct(productData));
    }

    @Test
    public void testHideThisProductVariantWhenOutOfStock() {
        final TargetVariantProductListerData productData = new TargetVariantProductListerData();
        productData.setInStock(false);

        Assert.assertTrue(stategy.hideThisProduct(productData));
    }

    @Test
    public void testHideThisProductVariantWhenInStock() {
        final TargetVariantProductListerData productData = new TargetVariantProductListerData();
        productData.setInStock(true);

        Assert.assertFalse(stategy.hideThisProduct(productData));
    }

    @Test
    public void testHideThisProductVariantWhenLowStock() {
        final TargetVariantProductListerData productData = new TargetVariantProductListerData();
        productData.setInStock(true);

        Assert.assertFalse(stategy.hideThisProduct(productData));
    }
}

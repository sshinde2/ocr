package au.com.target.tgtfacades.stl.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtfacades.stl.TargetShopTheLookFacade;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;
import au.com.target.tgtmarketing.model.TargetLookModel;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetLookDetailsDataPopulatorTest {

    @Mock
    private TargetShopTheLookFacade targetShopTheLookFacade;

    @InjectMocks
    private final TargetLookDetailsDataPopulator populator = new TargetLookDetailsDataPopulator();

    @Test
    public void testPopluator() {

        final TargetLookModel source = mock(TargetLookModel.class);
        given(source.getId()).willReturn("id");
        given(source.getName()).willReturn("name");
        given(source.getDescription()).willReturn("description");
        given(source.getUrl()).willReturn("url");
        final MediaModel gridImageModel = mock(MediaModel.class);
        given(gridImageModel.getURL()).willReturn("gridUrl");
        given(source.getGridImage()).willReturn(gridImageModel);
        final TargetLookCollectionModel collectionModel = mock(TargetLookCollectionModel.class);
        given(collectionModel.getId()).willReturn("collectionId");
        given(collectionModel.getName()).willReturn("collectionName");
        given(collectionModel.getUrl()).willReturn("collectionUrl");
        given(source.getCollection()).willReturn(collectionModel);
        final LookDetailsData target = new LookDetailsData();

        final MediaModel mediaGrid = mock(MediaModel.class);
        final MediaModel mediaHero = mock(MediaModel.class);
        final MediaModel mediaSwatch = mock(MediaModel.class);
        final MediaModel mediaList = mock(MediaModel.class);
        final MediaModel mediaLarge = mock(MediaModel.class);
        final MediaModel mediaThumb = mock(MediaModel.class);
        final MediaModel mediaFull = mock(MediaModel.class);

        final MediaFormatModel mediaGridFormat = mock(MediaFormatModel.class);
        final MediaFormatModel mediaHeroFormat = mock(MediaFormatModel.class);
        final MediaFormatModel mediaSwatchFormat = mock(MediaFormatModel.class);
        final MediaFormatModel mediaListFormat = mock(MediaFormatModel.class);
        final MediaFormatModel mediaLargeFormat = mock(MediaFormatModel.class);
        final MediaFormatModel mediaThumbFormat = mock(MediaFormatModel.class);
        final MediaFormatModel mediaFullFormat = mock(MediaFormatModel.class);

        given(mediaGridFormat.getQualifier()).willReturn("grid");
        given(mediaHeroFormat.getQualifier()).willReturn("hero");
        given(mediaSwatchFormat.getQualifier()).willReturn("swatch");
        given(mediaListFormat.getQualifier()).willReturn("list");
        given(mediaLargeFormat.getQualifier()).willReturn("large");
        given(mediaThumbFormat.getQualifier()).willReturn("thumb");
        given(mediaFullFormat.getQualifier()).willReturn("full");

        given(mediaGrid.getMediaFormat()).willReturn(mediaGridFormat);
        given(mediaHero.getMediaFormat()).willReturn(mediaHeroFormat);
        given(mediaSwatch.getMediaFormat()).willReturn(mediaSwatchFormat);
        given(mediaList.getMediaFormat()).willReturn(mediaListFormat);
        given(mediaLarge.getMediaFormat()).willReturn(mediaLargeFormat);
        given(mediaThumb.getMediaFormat()).willReturn(mediaThumbFormat);
        given(mediaFull.getMediaFormat()).willReturn(mediaFullFormat);

        given(mediaGrid.getURL()).willReturn("mediaGrid");
        given(mediaHero.getURL()).willReturn("mediaHero");
        given(mediaSwatch.getURL()).willReturn("mediaSwatch");
        given(mediaList.getURL()).willReturn("mediaList");
        given(mediaLarge.getURL()).willReturn("mediaLarge");
        given(mediaThumb.getURL()).willReturn("mediaThumb");
        given(mediaFull.getURL()).willReturn("mediaFull");

        final List<MediaModel> medias = Arrays.asList(mediaGrid, mediaHero, mediaSwatch, mediaList, mediaLarge,
                mediaThumb, mediaFull);
        final MediaContainerModel mediaContainerModel = mock(MediaContainerModel.class);
        given(mediaContainerModel.getMedias()).willReturn(medias);

        given(source.getImages()).willReturn(mediaContainerModel);

        final List<String> productCodeList = Arrays.asList("product1", "product2");
        given(targetShopTheLookFacade.getProductCodeListByLook("id")).willReturn(productCodeList);

        populator.populate(source, target);
        assertThat("id").isEqualTo(target.getId());
        assertThat("name").isEqualTo(target.getName());
        assertThat("description").isEqualTo(target.getDescription());
        assertThat("url").isEqualTo(target.getUrl());
        assertThat("collectionId").isEqualTo(target.getCollectionId());
        assertThat("collectionName").isEqualTo(target.getCollectionName());
        assertThat("collectionUrl").isEqualTo(target.getCollectionUrl());

        assertThat("mediaGrid").isEqualTo(target.getImages().getGridImageUrl());
        assertThat("mediaHero").isEqualTo(target.getImages().getHeroImageUrl());
        assertThat("mediaSwatch").isEqualTo(target.getImages().getSwatchImageUrl());
        assertThat("mediaList").isEqualTo(target.getImages().getListImageUrl());
        assertThat("mediaLarge").isEqualTo(target.getImages().getLargeImageUrl());
        assertThat("mediaThumb").isEqualTo(target.getImages().getThumbImageUrl());
        assertThat("mediaFull").isEqualTo(target.getImages().getFullImageUrl());
        assertThat(productCodeList).isEqualTo(target.getProductCodeList());

    }

    @Test
    public void testPopluatorWithNullModel() {
        final TargetLookModel source = null;
        final LookDetailsData target = new LookDetailsData();
        populator.populate(source, target);
        assertThat(target.getId()).isNull();
        assertThat(target.getName()).isNull();
        assertThat(target.getDescription()).isNull();
        assertThat(target.getUrl()).isNull();
        assertThat(target.getImages()).isNull();
        assertThat(target.getCollectionId()).isNull();
        assertThat(target.getCollectionUrl()).isNull();
        assertThat(target.getProductCodeList()).isNull();
    }

}

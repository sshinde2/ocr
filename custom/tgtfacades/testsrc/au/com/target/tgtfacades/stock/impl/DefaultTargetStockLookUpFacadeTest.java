/**
 * 
 */
package au.com.target.tgtfacades.stock.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.querybuilder.EndecaProductQueryBuilder;
import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.product.stock.StockAvailabilitiesData;
import au.com.target.tgtfacades.product.stock.StockAvailabilityData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.response.data.StockResponseData;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.data.StockDatum;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.service.FluentStockLookupService;
import au.com.target.tgtsale.stock.service.TargetStoreStockService;

import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERec;
import com.endeca.navigation.ERecList;
import com.endeca.navigation.MERecList;
import com.endeca.navigation.Navigation;
import com.endeca.navigation.PropertyMap;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultTargetStockLookUpFacadeTest {

    @Mock
    private TargetStockService targetStockService;

    @Mock
    private TargetStoreStockService targetStoreStockService;

    @Mock
    private TargetProductFacade targetProductFacade;

    @Mock
    private FluentStockLookupService fluentStockLookupService;

    @Mock
    private EndecaProductQueryBuilder endecaProductQueryBuilder;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private TargetSharedConfigService targetSharedConfigService;

    @Mock
    private WarehouseModel warehouseModel;

    @InjectMocks
    @Spy
    private final DefaultTargetStockLookUpFacade defaultTargetStockLookUpFacade = new DefaultTargetStockLookUpFacade();

    @Test
    public void testGetStockFromWarehouseWhenStockAvailabilitiesIsEmpty() {
        final StockAvailabilitiesData stockAvailabilitiesData = new StockAvailabilitiesData();
        assertThat(defaultTargetStockLookUpFacade.getStockFromWarehouse(stockAvailabilitiesData, "ebay", null))
                .isEqualTo(stockAvailabilitiesData);
    }

    @Test
    public void testGetStockFromWarehouseWhenItemCodeIsNull() {
        final StockAvailabilitiesData stockAvailabilitiesData = new StockAvailabilitiesData();
        final StockAvailabilityData stockAvailabilityData = new StockAvailabilityData();
        stockAvailabilityData.setFulfillmentType("SHIP_STORE");
        stockAvailabilitiesData.setAvailabilities(Arrays.asList(stockAvailabilityData));
        assertThat(defaultTargetStockLookUpFacade.getStockFromWarehouse(stockAvailabilitiesData, "ebay", null))
                .isEqualTo(stockAvailabilitiesData);
    }

    @Test
    public void testGetStockFromWarehouseWhenItemCodeIsNotFoundInHybris() {
        final StockAvailabilitiesData stockAvailabilitiesData = new StockAvailabilitiesData();
        final StockAvailabilityData stockAvailabilityData = new StockAvailabilityData();
        stockAvailabilityData.setFulfillmentType("SHIP_STORE");
        stockAvailabilityData.setItemCode("2223");
        given(targetStockService.getStockForProductsInWarehouse(Arrays.asList("2223"), null))
                .willReturn(new HashMap<String, Integer>());
        stockAvailabilitiesData.setAvailabilities(Arrays.asList(stockAvailabilityData));
        assertThat(defaultTargetStockLookUpFacade.getStockFromWarehouse(stockAvailabilitiesData, "ebay", null))
                .isEqualTo(stockAvailabilitiesData);
        final StockAvailabilityData stockAvailabilityDataResult = stockAvailabilitiesData.getAvailabilities().get(0);
        assertThat(stockAvailabilityDataResult.getItemCode()).isEqualTo("2223");
        assertThat(stockAvailabilityDataResult.getTotalAvailableQuantity()).isEqualTo(0);
        assertThat(stockAvailabilityDataResult.isIsAvailable()).isEqualTo(false);
    }

    @Test
    public void testGetStockFromWarehouseWhenItemCodeIsLowInStock() {
        final StockAvailabilitiesData stockAvailabilitiesData = new StockAvailabilitiesData();
        final StockAvailabilityData stockAvailabilityData = new StockAvailabilityData();
        stockAvailabilityData.setFulfillmentType("SHIP_STORE");
        stockAvailabilityData.setItemCode("p1000_black_l");
        stockAvailabilityData.setRequestedQuantity(20);
        final Map<String, Integer> availMap = new HashMap<String, Integer>();
        availMap.put("p1000_black_l", Integer.valueOf(10));
        given(
                targetStockService.getStockForProductsInWarehouse(Arrays.asList("p1000_black_l"), null))
                .willReturn(availMap);
        stockAvailabilitiesData.setAvailabilities(Arrays.asList(stockAvailabilityData));
        assertThat(defaultTargetStockLookUpFacade.getStockFromWarehouse(stockAvailabilitiesData, "ebay", null))
                .isEqualTo(stockAvailabilitiesData);
        final StockAvailabilityData stockAvailabilityDataResult = stockAvailabilitiesData.getAvailabilities().get(0);
        assertThat(stockAvailabilityDataResult.getItemCode()).isEqualTo("p1000_black_l");
        assertThat(stockAvailabilityDataResult.getTotalAvailableQuantity()).isEqualTo(10);
        assertThat(stockAvailabilityDataResult.isIsAvailable()).isEqualTo(false);
    }

    @Test
    public void testGetStockFromWarehouseWhenItemCodeIsInStock() {
        final StockAvailabilitiesData stockAvailabilitiesData = new StockAvailabilitiesData();
        final StockAvailabilityData stockAvailabilityData = new StockAvailabilityData();
        stockAvailabilityData.setFulfillmentType("SHIP_STORE");
        stockAvailabilityData.setItemCode("p1000_black_l");
        stockAvailabilityData.setRequestedQuantity(15);
        final Map<String, Integer> availMap = new HashMap<String, Integer>();
        availMap.put("p1000_black_l", Integer.valueOf(20));
        given(
                targetStockService.getStockForProductsInWarehouse(Arrays.asList("p1000_black_l"), null))
                .willReturn(availMap);
        stockAvailabilitiesData.setAvailabilities(Arrays.asList(stockAvailabilityData));
        assertThat(defaultTargetStockLookUpFacade.getStockFromWarehouse(stockAvailabilitiesData, "ebay", null))
                .isEqualTo(stockAvailabilitiesData);
        final StockAvailabilityData stockAvailabilityDataResult = stockAvailabilitiesData.getAvailabilities().get(0);
        assertThat(stockAvailabilityDataResult.getItemCode()).isEqualTo("p1000_black_l");
        assertThat(stockAvailabilityDataResult.getTotalAvailableQuantity()).isEqualTo(20);
        assertThat(stockAvailabilityDataResult.isIsAvailable()).isEqualTo(true);
    }

    @Test
    public void testGetStockFromWarehouseWhenItemCodeHasNegativeAvailableStock() {
        final StockAvailabilitiesData stockAvailabilitiesData = new StockAvailabilitiesData();
        final StockAvailabilityData stockAvailabilityData = new StockAvailabilityData();
        stockAvailabilityData.setFulfillmentType("SHIP_STORE");
        stockAvailabilityData.setItemCode("p1000_black_l");
        stockAvailabilityData.setRequestedQuantity(15);
        final Map<String, Integer> availMap = new HashMap<String, Integer>();
        availMap.put("p1000_black_l", Integer.valueOf(-20));
        given(targetStockService.getStockForProductsInWarehouse(Arrays.asList("p1000_black_l"), null))
                .willReturn(availMap);
        stockAvailabilitiesData.setAvailabilities(Arrays.asList(stockAvailabilityData));
        assertThat(defaultTargetStockLookUpFacade.getStockFromWarehouse(stockAvailabilitiesData, "ebay", null))
                .isEqualTo(stockAvailabilitiesData);
        final StockAvailabilityData stockAvailabilityDataResult = stockAvailabilitiesData.getAvailabilities().get(0);
        assertThat(stockAvailabilityDataResult.getItemCode()).isEqualTo("p1000_black_l");
        assertThat(stockAvailabilityDataResult.getTotalAvailableQuantity()).isEqualTo(0);
        assertThat(stockAvailabilityDataResult.isIsAvailable()).isEqualTo(false);
    }

    @Test
    public void testGetStockFromFluentWhenItemCodeIsLowInStock() throws FluentClientException {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isFluentEnabled();
        final StockAvailabilitiesData stockAvailabilitiesData = new StockAvailabilitiesData();
        final StockAvailabilityData stockAvailabilityData = new StockAvailabilityData();
        stockAvailabilityData.setFulfillmentType("SHIP_STORE");
        stockAvailabilityData.setItemCode("p1000_black_l");
        stockAvailabilityData.setRequestedQuantity(20);
        final StockDatum stockDatum = mock(StockDatum.class);
        willReturn("p1000_black_l").given(stockDatum).getVariantCode();
        willReturn(Integer.valueOf(10)).given(stockDatum).getAtsCcQty();
        willReturn(Arrays.asList(stockDatum)).given(fluentStockLookupService)
                .lookupStock(Arrays.asList("p1000_black_l"), Arrays.asList("CC"), null);
        stockAvailabilitiesData.setAvailabilities(Arrays.asList(stockAvailabilityData));

        assertThat(defaultTargetStockLookUpFacade.getStockFromWarehouse(stockAvailabilitiesData, "ebay", null))
                .isEqualTo(stockAvailabilitiesData);
        final StockAvailabilityData stockAvailabilityDataResult = stockAvailabilitiesData.getAvailabilities().get(0);
        assertThat(stockAvailabilityDataResult.getItemCode()).isEqualTo("p1000_black_l");
        assertThat(stockAvailabilityDataResult.getTotalAvailableQuantity()).isEqualTo(10);
        assertThat(stockAvailabilityDataResult.isIsAvailable()).isEqualTo(false);
        verifyNoMoreInteractions(targetStockService);
    }

    @Test
    public void testGetStockFromFluentWhenItemCodeIsInStock() throws FluentClientException {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isFluentEnabled();
        final StockAvailabilitiesData stockAvailabilitiesData = new StockAvailabilitiesData();
        final StockAvailabilityData stockAvailabilityData = new StockAvailabilityData();
        stockAvailabilityData.setFulfillmentType("SHIP_STORE");
        stockAvailabilityData.setItemCode("p1000_black_l");
        stockAvailabilityData.setRequestedQuantity(15);
        final StockDatum stockDatum = mock(StockDatum.class);
        willReturn("p1000_black_l").given(stockDatum).getVariantCode();
        willReturn(Integer.valueOf(20)).given(stockDatum).getAtsCcQty();
        willReturn(Arrays.asList(stockDatum)).given(fluentStockLookupService)
                .lookupStock(Arrays.asList("p1000_black_l"), Arrays.asList("CC"), null);
        stockAvailabilitiesData.setAvailabilities(Arrays.asList(stockAvailabilityData));

        assertThat(defaultTargetStockLookUpFacade.getStockFromWarehouse(stockAvailabilitiesData, "ebay", null))
                .isEqualTo(stockAvailabilitiesData);
        final StockAvailabilityData stockAvailabilityDataResult = stockAvailabilitiesData.getAvailabilities().get(0);
        assertThat(stockAvailabilityDataResult.getItemCode()).isEqualTo("p1000_black_l");
        assertThat(stockAvailabilityDataResult.getTotalAvailableQuantity()).isEqualTo(20);
        assertThat(stockAvailabilityDataResult.isIsAvailable()).isEqualTo(true);
        verifyNoMoreInteractions(targetStockService);
    }

    @Test
    public void testGetStockFromFluentWhenItemCodeHasNegativeAvailableStock() throws FluentClientException {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isFluentEnabled();
        final StockAvailabilitiesData stockAvailabilitiesData = new StockAvailabilitiesData();
        final StockAvailabilityData stockAvailabilityData = new StockAvailabilityData();
        stockAvailabilityData.setFulfillmentType("SHIP_STORE");
        stockAvailabilityData.setItemCode("p1000_black_l");
        stockAvailabilityData.setRequestedQuantity(15);
        final StockDatum stockDatum = mock(StockDatum.class);
        willReturn("p1000_black_l").given(stockDatum).getVariantCode();
        willReturn(Integer.valueOf(-10)).given(stockDatum).getAtsCcQty();
        willReturn(Arrays.asList(stockDatum)).given(fluentStockLookupService)
                .lookupStock(Arrays.asList("p1000_black_l"), Arrays.asList("CC"), null);
        stockAvailabilitiesData.setAvailabilities(Arrays.asList(stockAvailabilityData));

        assertThat(defaultTargetStockLookUpFacade.getStockFromWarehouse(stockAvailabilitiesData, "ebay", null))
                .isEqualTo(stockAvailabilitiesData);
        final StockAvailabilityData stockAvailabilityDataResult = stockAvailabilitiesData.getAvailabilities().get(0);
        assertThat(stockAvailabilityDataResult.getItemCode()).isEqualTo("p1000_black_l");
        assertThat(stockAvailabilityDataResult.getTotalAvailableQuantity()).isEqualTo(0);
        assertThat(stockAvailabilityDataResult.isIsAvailable()).isEqualTo(false);
        verifyNoMoreInteractions(targetStockService);
    }

    @Test
    public void testGetStockFromFluentWhenLookupFails() throws FluentClientException {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isFluentEnabled();
        final StockAvailabilitiesData stockAvailabilitiesData = new StockAvailabilitiesData();
        final StockAvailabilityData stockAvailabilityData = new StockAvailabilityData();
        stockAvailabilityData.setFulfillmentType("SHIP_STORE");
        stockAvailabilityData.setItemCode("p1000_black_l");
        stockAvailabilityData.setRequestedQuantity(15);
        willThrow(new FluentClientException(new Error())).given(fluentStockLookupService)
                .lookupStock(Arrays.asList("p1000_black_l"), Arrays.asList("CC"), null);
        stockAvailabilitiesData.setAvailabilities(Arrays.asList(stockAvailabilityData));

        assertThat(defaultTargetStockLookUpFacade.getStockFromWarehouse(stockAvailabilitiesData, "ebay", null))
                .isEqualTo(stockAvailabilitiesData);
        final StockAvailabilityData stockAvailabilityDataResult = stockAvailabilitiesData.getAvailabilities().get(0);
        assertThat(stockAvailabilityDataResult.getItemCode()).isEqualTo("p1000_black_l");
        assertThat(stockAvailabilityDataResult.getTotalAvailableQuantity()).isEqualTo(0);
        assertThat(stockAvailabilityDataResult.isIsAvailable()).isEqualTo(false);
        verifyNoMoreInteractions(targetStockService);
    }

    @Test
    public void testLookupStockWithNoVariants() {
        final Response response = defaultTargetStockLookUpFacade.lookupStock(Collections.<String> emptyList(),
                Arrays.asList("CC"), Collections.<String> emptyList());
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_EMPTY_VARIANTS");
    }

    @Test
    public void testLookupStockWithNoDeliveryTypesNorLocations() {
        final Response response = defaultTargetStockLookUpFacade.lookupStock(Arrays.asList("P1000_red_L"),
                Collections.<String> emptyList(), Collections.<String> emptyList());
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_EMPTY_OPTIONS");
    }

    @Test
    public void testLookupStockWithFluent() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isFluentEnabled();
        final StockDatum stockDatum = mock(StockDatum.class);
        willReturn(Arrays.asList(stockDatum)).given(defaultTargetStockLookUpFacade).fluentLookupStock(
                anyList(),
                anyList(), anyList());

        final Response response = defaultTargetStockLookUpFacade.lookupStock(Arrays.asList("P1000_red_L"),
                Arrays.asList("CC"), Collections.<String> emptyList());
        assertThat(response.getData()).isInstanceOf(StockResponseData.class);
        assertThat(((StockResponseData)response.getData()).getStockData()).isNotEmpty().hasSize(1);
        assertThat(((StockResponseData)response.getData()).getAvailabilityTime()).isInstanceOf(Date.class);
        verify(defaultTargetStockLookUpFacade).fluentLookupStock(Arrays.asList("P1000_red_L"),
                Arrays.asList("CC"), Collections.<String> emptyList());
        verify(defaultTargetStockLookUpFacade, never()).legacyLookupStock(anyList(), anyList(),
                anyList());
    }

    @Test
    public void testLookupStockWithFluentEmptyResponse() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isFluentEnabled();
        willReturn(Collections.emptyList()).given(defaultTargetStockLookUpFacade).fluentLookupStock(anyList(),
                anyList(), anyList());

        final Response response = defaultTargetStockLookUpFacade.lookupStock(Arrays.asList("P1000_red_L"),
                Arrays.asList("CC"), Collections.<String> emptyList());
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_STOCK_LOOKUP");
        verify(defaultTargetStockLookUpFacade).fluentLookupStock(Arrays.asList("P1000_red_L"),
                Arrays.asList("CC"), Collections.<String> emptyList());
        verify(defaultTargetStockLookUpFacade, never()).legacyLookupStock(anyList(), anyList(),
                anyList());
    }

    @Test
    public void testLookupStockWithLegacy() {
        final StockDatum stockDatum = mock(StockDatum.class);
        willReturn(Arrays.asList(stockDatum)).given(defaultTargetStockLookUpFacade).legacyLookupStock(
                anyList(),
                anyList(),
                anyList());

        final Response response = defaultTargetStockLookUpFacade.lookupStock(Arrays.asList("P1000_red_L"),
                Arrays.asList("CC"), Collections.<String> emptyList());
        assertThat(response.getData()).isInstanceOf(StockResponseData.class);
        assertThat(((StockResponseData)response.getData()).getStockData()).isNotEmpty().hasSize(1);
        assertThat(((StockResponseData)response.getData()).getAvailabilityTime()).isInstanceOf(Date.class);
        verify(defaultTargetStockLookUpFacade).legacyLookupStock(Arrays.asList("P1000_red_L"),
                Arrays.asList("CC"), Collections.<String> emptyList());
        verify(defaultTargetStockLookUpFacade, never()).fluentLookupStock(anyList(), anyList(),
                anyList());
    }

    @Test
    public void testLookupStockWithLegacyEmptyResponse() {
        willReturn(Collections.emptyList()).given(defaultTargetStockLookUpFacade).legacyLookupStock(anyList(),
                anyList(),
                anyList());

        final Response response = defaultTargetStockLookUpFacade.lookupStock(Arrays.asList("P1000_red_L"),
                Arrays.asList("CC"), Collections.<String> emptyList());
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_STOCK_LOOKUP");
        verify(defaultTargetStockLookUpFacade).legacyLookupStock(Arrays.asList("P1000_red_L"),
                Arrays.asList("CC"), Collections.<String> emptyList());
        verify(defaultTargetStockLookUpFacade, never()).fluentLookupStock(anyList(), anyList(),
                anyList());
    }

    @Test
    public void testFluentLookupStock() throws FluentClientException {
        final StockDatum stockDatum = mock(StockDatum.class);
        willReturn(Arrays.asList(stockDatum)).given(fluentStockLookupService).lookupStock(anyList(),
                anyList(), anyList());

        final List<StockDatum> stockData = defaultTargetStockLookUpFacade.fluentLookupStock(
                Arrays.asList("P1000_red_L"),
                Arrays.asList("CC"), Collections.<String> emptyList());
        assertThat(stockData).isNotEmpty().hasSize(1);
    }

    @Test
    public void testFluentLookupStockFail() throws FluentClientException {
        willThrow(new FluentClientException(new Error())).given(fluentStockLookupService).lookupStock(
                anyList(),
                anyList(), anyList());

        final List<StockDatum> stockData = defaultTargetStockLookUpFacade.fluentLookupStock(
                Arrays.asList("P1000_red_L"),
                Arrays.asList("CC"), Collections.<String> emptyList());
        assertThat(stockData).isEmpty();
    }

    @Test
    public void testLegacyLookupStock() {
        Map<String, StockLevelStatus> productStockMap = new HashMap();
        productStockMap.put("P1000_red_L", StockLevelStatus.OUTOFSTOCK);
        productStockMap.put("P1000_green_M", StockLevelStatus.LOWSTOCK);
        willReturn(productStockMap).given(targetStoreStockService).getStoreStockLevelStatusMapForProducts(
                Integer.valueOf(5599), Arrays.asList("P1000_red_L", "P1000_green_M"));
        productStockMap = new HashMap();
        productStockMap.put("P1000_red_L", StockLevelStatus.LOWSTOCK);
        productStockMap.put("P1000_green_M", StockLevelStatus.INSTOCK);
        willReturn(productStockMap).given(targetStoreStockService).getStoreStockLevelStatusMapForProducts(
                Integer.valueOf(5612), Arrays.asList("P1000_red_L", "P1000_green_M"));
        productStockMap = new HashMap();
        productStockMap.put("P1000_red_L", StockLevelStatus.LOWSTOCK);
        productStockMap.put("P1000_green_M", StockLevelStatus.INSTOCK);
        willReturn(productStockMap).given(defaultTargetStockLookUpFacade).getConsolidatedSohByVariantMap(
                Arrays.asList("P1000_red_L", "P1000_green_M"),
                Arrays.asList("CC", "ED", "PO", "CONSOLIDATED_STORES_SOH"));

        ProductModel productModel = mock(ProductModel.class);
        willReturn(productModel).given(targetProductFacade).getProductForCode("P1000_red_L");
        StockData stock = mock(StockData.class);
        willReturn(StockLevelStatus.OUTOFSTOCK).given(stock).getStockLevelStatus();
        willReturn(stock).given(targetStockService)
                .getStockLevelAndStatusAmount(productModel);

        productModel = mock(ProductModel.class);
        willReturn(productModel).given(targetProductFacade).getProductForCode("P1000_green_M");
        stock = mock(StockData.class);
        willReturn(StockLevelStatus.LOWSTOCK).given(stock).getStockLevelStatus();
        willReturn(stock).given(targetStockService)
                .getStockLevelAndStatusAmount(productModel);

        final List<StockDatum> stockData = defaultTargetStockLookUpFacade.legacyLookupStock(
                Arrays.asList("P1000_red_L", "P1000_green_M"),
                Arrays.asList("CC", "ED", "PO", "CONSOLIDATED_STORES_SOH"), Arrays.asList("5599", "5612"));
        assertThat(stockData).isNotEmpty().hasSize(2);
        assertThat(stockData).onProperty("variantCode").containsExactly("P1000_red_L", "P1000_green_M");
        assertThat(stockData).onProperty("atsCc").containsExactly(StockLevelStatus.OUTOFSTOCK,
                StockLevelStatus.LOWSTOCK);
        assertThat(stockData).onProperty("atsEd").containsExactly(StockLevelStatus.OUTOFSTOCK,
                StockLevelStatus.LOWSTOCK);
        assertThat(stockData).onProperty("atsPo").containsExactly(StockLevelStatus.OUTOFSTOCK,
                StockLevelStatus.LOWSTOCK);
        assertThat(stockData).onProperty("consolidatedStoreStock").containsExactly(StockLevelStatus.LOWSTOCK,
                StockLevelStatus.INSTOCK);
        assertThat(stockData.get(0).getStoreSoh().get("5599")).isEqualTo(StockLevelStatus.OUTOFSTOCK);
        assertThat(stockData.get(0).getStoreSoh().get("5612")).isEqualTo(StockLevelStatus.LOWSTOCK);
        assertThat(stockData.get(1).getStoreSoh().get("5599")).isEqualTo(StockLevelStatus.LOWSTOCK);
        assertThat(stockData.get(1).getStoreSoh().get("5612")).isEqualTo(StockLevelStatus.INSTOCK);
    }

    @Test
    public void testLegacyLookupStockWithException() {
        Map<String, StockLevelStatus> productStockMap = new HashMap();
        productStockMap.put("P1000_red_L", StockLevelStatus.OUTOFSTOCK);
        productStockMap.put("P1000_green_M", StockLevelStatus.LOWSTOCK);
        willReturn(productStockMap).given(targetStoreStockService).getStoreStockLevelStatusMapForProducts(
                Integer.valueOf(5599), Arrays.asList("P1000_red_L", "P1000_green_M"));
        productStockMap = new HashMap();
        productStockMap.put("P1000_red_L", StockLevelStatus.LOWSTOCK);
        productStockMap.put("P1000_green_M", StockLevelStatus.INSTOCK);
        willReturn(productStockMap).given(targetStoreStockService).getStoreStockLevelStatusMapForProducts(
                Integer.valueOf(5612), Arrays.asList("P1000_red_L", "P1000_green_M"));
        productStockMap = new HashMap();
        productStockMap.put("P1000_red_L", StockLevelStatus.LOWSTOCK);
        productStockMap.put("P1000_green_M", StockLevelStatus.INSTOCK);
        willReturn(productStockMap).given(defaultTargetStockLookUpFacade).getConsolidatedSohByVariantMap(
                Arrays.asList("P1000_red_L", "P1000_green_M"), Arrays.asList("CC", "ED", "CONSOLIDATED_STORES_SOH"));

        ProductModel productModel = mock(ProductModel.class);
        willThrow(new UnknownIdentifierException(StringUtils.EMPTY)).given(targetProductFacade)
                .getProductForCode("P1000_red_L");
        StockData stock = mock(StockData.class);
        willReturn(StockLevelStatus.OUTOFSTOCK).given(stock).getStockLevelStatus();
        willReturn(stock).given(targetStockService)
                .getStockLevelAndStatusAmount(productModel);

        productModel = mock(ProductModel.class);
        willReturn(productModel).given(targetProductFacade).getProductForCode("P1000_green_M");
        stock = mock(StockData.class);
        willReturn(StockLevelStatus.LOWSTOCK).given(stock).getStockLevelStatus();
        willReturn(stock).given(targetStockService)
                .getStockLevelAndStatusAmount(productModel);

        final List<StockDatum> stockData = defaultTargetStockLookUpFacade.legacyLookupStock(
                Arrays.asList("P1000_red_L", "P1000_green_M"),
                Arrays.asList("CC", "ED", "CONSOLIDATED_STORES_SOH"), Arrays.asList("5599", "5612"));
        assertThat(stockData).isEmpty();
    }

    @Test
    public void testLegacyLookupStockWithoutLocations() {
        final Map<String, StockLevelStatus> productStockMap = new HashMap();
        productStockMap.put("P1000_red_L", StockLevelStatus.LOWSTOCK);
        productStockMap.put("P1000_green_M", StockLevelStatus.INSTOCK);
        willReturn(productStockMap).given(defaultTargetStockLookUpFacade).getConsolidatedSohByVariantMap(
                Arrays.asList("P1000_red_L", "P1000_green_M"), Arrays.asList("CC", "ED", "CONSOLIDATED_STORES_SOH"));

        ProductModel productModel = mock(ProductModel.class);
        willReturn(productModel).given(targetProductFacade).getProductForCode("P1000_red_L");
        StockData stock = mock(StockData.class);
        willReturn(StockLevelStatus.OUTOFSTOCK).given(stock).getStockLevelStatus();
        willReturn(stock).given(targetStockService)
                .getStockLevelAndStatusAmount(productModel);

        productModel = mock(ProductModel.class);
        willReturn(productModel).given(targetProductFacade).getProductForCode("P1000_green_M");
        stock = mock(StockData.class);
        willReturn(StockLevelStatus.LOWSTOCK).given(stock).getStockLevelStatus();
        willReturn(stock).given(targetStockService)
                .getStockLevelAndStatusAmount(productModel);

        final List<StockDatum> stockData = defaultTargetStockLookUpFacade.legacyLookupStock(
                Arrays.asList("P1000_red_L", "P1000_green_M"),
                Arrays.asList("CC", "ED", "CONSOLIDATED_STORES_SOH"), Collections.<String> emptyList());
        assertThat(stockData).isNotEmpty().hasSize(2);
        assertThat(stockData).onProperty("variantCode").containsExactly("P1000_red_L", "P1000_green_M");
        assertThat(stockData).onProperty("atsCc").containsExactly(StockLevelStatus.OUTOFSTOCK,
                StockLevelStatus.LOWSTOCK);
        assertThat(stockData).onProperty("atsHd").containsExactly(null, null);
        assertThat(stockData).onProperty("atsEd").containsExactly(StockLevelStatus.OUTOFSTOCK,
                StockLevelStatus.LOWSTOCK);
        assertThat(stockData).onProperty("consolidatedStoreStock").containsExactly(StockLevelStatus.LOWSTOCK,
                StockLevelStatus.INSTOCK);
        assertThat(stockData.get(0).getStoreSoh()).isNull();
        assertThat(stockData.get(1).getStoreSoh()).isNull();
        verify(targetStoreStockService, never()).getStoreStockLevelStatusMapForProducts(Mockito.any(Integer.class),
                anyList());
    }

    @Test
    public void testLegacyLookupStockWithoutConsolidatedStock() {
        Map<String, StockLevelStatus> productStockMap = new HashMap();
        productStockMap.put("P1000_red_L", StockLevelStatus.OUTOFSTOCK);
        productStockMap.put("P1000_green_M", StockLevelStatus.LOWSTOCK);
        willReturn(productStockMap).given(targetStoreStockService).getStoreStockLevelStatusMapForProducts(
                Integer.valueOf(5599), Arrays.asList("P1000_red_L", "P1000_green_M"));
        productStockMap = new HashMap();
        productStockMap.put("P1000_red_L", StockLevelStatus.LOWSTOCK);
        productStockMap.put("P1000_green_M", StockLevelStatus.INSTOCK);
        willReturn(productStockMap).given(targetStoreStockService).getStoreStockLevelStatusMapForProducts(
                Integer.valueOf(5612), Arrays.asList("P1000_red_L", "P1000_green_M"));

        ProductModel productModel = mock(ProductModel.class);
        willReturn(productModel).given(targetProductFacade).getProductForCode("P1000_red_L");
        StockData stock = mock(StockData.class);
        willReturn(StockLevelStatus.OUTOFSTOCK).given(stock).getStockLevelStatus();
        willReturn(stock).given(targetStockService).getStockLevelAndStatusAmount(productModel);

        productModel = mock(ProductModel.class);
        willReturn(productModel).given(targetProductFacade).getProductForCode("P1000_green_M");
        stock = mock(StockData.class);
        willReturn(StockLevelStatus.LOWSTOCK).given(stock).getStockLevelStatus();
        willReturn(stock).given(targetStockService).getStockLevelAndStatusAmount(productModel);

        final List<StockDatum> stockData = defaultTargetStockLookUpFacade.legacyLookupStock(
                Arrays.asList("P1000_red_L", "P1000_green_M"),
                Arrays.asList("CC", "HD", "ED"), Arrays.asList("5599", "5612"));
        assertThat(stockData).isNotEmpty().hasSize(2);
        assertThat(stockData).onProperty("variantCode").containsExactly("P1000_red_L", "P1000_green_M");
        assertThat(stockData).onProperty("atsCc").containsExactly(StockLevelStatus.OUTOFSTOCK,
                StockLevelStatus.LOWSTOCK);
        assertThat(stockData).onProperty("atsHd").containsExactly(StockLevelStatus.OUTOFSTOCK,
                StockLevelStatus.LOWSTOCK);
        assertThat(stockData).onProperty("atsEd").containsExactly(StockLevelStatus.OUTOFSTOCK,
                StockLevelStatus.LOWSTOCK);
        assertThat(stockData).onProperty("consolidatedStoreStock").containsExactly(null, null);
        assertThat(stockData.get(0).getStoreSoh().get("5599")).isEqualTo(StockLevelStatus.OUTOFSTOCK);
        assertThat(stockData.get(0).getStoreSoh().get("5612")).isEqualTo(StockLevelStatus.LOWSTOCK);
        assertThat(stockData.get(1).getStoreSoh().get("5599")).isEqualTo(StockLevelStatus.LOWSTOCK);
        assertThat(stockData.get(1).getStoreSoh().get("5612")).isEqualTo(StockLevelStatus.INSTOCK);
    }

    @Test
    public void testLegacyLookupStockWithJustLocations() {
        Map<String, StockLevelStatus> productStockMap = new HashMap();
        productStockMap.put("P1000_red_L", StockLevelStatus.OUTOFSTOCK);
        productStockMap.put("P1000_green_M", StockLevelStatus.LOWSTOCK);
        willReturn(productStockMap).given(targetStoreStockService).getStoreStockLevelStatusMapForProducts(
                Integer.valueOf(5599), Arrays.asList("P1000_red_L", "P1000_green_M"));
        productStockMap = new HashMap();
        productStockMap.put("P1000_red_L", StockLevelStatus.LOWSTOCK);
        productStockMap.put("P1000_green_M", StockLevelStatus.INSTOCK);
        willReturn(productStockMap).given(targetStoreStockService).getStoreStockLevelStatusMapForProducts(
                Integer.valueOf(5612), Arrays.asList("P1000_red_L", "P1000_green_M"));

        final List<StockDatum> stockData = defaultTargetStockLookUpFacade.legacyLookupStock(
                Arrays.asList("P1000_red_L", "P1000_green_M"),
                Collections.<String> emptyList(), Arrays.asList("5599", "5612"));
        assertThat(stockData).isNotEmpty().hasSize(2);
        assertThat(stockData).onProperty("variantCode").containsExactly("P1000_red_L", "P1000_green_M");
        assertThat(stockData).onProperty("atsCc").containsExactly(null, null);
        assertThat(stockData).onProperty("atsHd").containsExactly(null, null);
        assertThat(stockData).onProperty("atsEd").containsExactly(null, null);
        assertThat(stockData).onProperty("consolidatedStoreStock").containsExactly(null, null);
        assertThat(stockData.get(0).getStoreSoh().get("5599")).isEqualTo(StockLevelStatus.OUTOFSTOCK);
        assertThat(stockData.get(0).getStoreSoh().get("5612")).isEqualTo(StockLevelStatus.LOWSTOCK);
        assertThat(stockData.get(1).getStoreSoh().get("5599")).isEqualTo(StockLevelStatus.LOWSTOCK);
        assertThat(stockData.get(1).getStoreSoh().get("5612")).isEqualTo(StockLevelStatus.INSTOCK);
        verify(targetProductFacade, never()).getProductForCodeAndOptions(Mockito.anyString(),
                anyList());
    }

    @Test
    public void testLegacyLookupStockWithoutOnlineStock() {
        Map<String, StockLevelStatus> productStockMap = new HashMap();
        productStockMap.put("P1000_red_L", StockLevelStatus.OUTOFSTOCK);
        productStockMap.put("P1000_green_M", StockLevelStatus.LOWSTOCK);
        willReturn(productStockMap).given(targetStoreStockService).getStoreStockLevelStatusMapForProducts(
                Integer.valueOf(5599), Arrays.asList("P1000_red_L", "P1000_green_M"));
        productStockMap = new HashMap();
        productStockMap.put("P1000_red_L", StockLevelStatus.LOWSTOCK);
        productStockMap.put("P1000_green_M", StockLevelStatus.INSTOCK);
        willReturn(productStockMap).given(targetStoreStockService).getStoreStockLevelStatusMapForProducts(
                Integer.valueOf(5612), Arrays.asList("P1000_red_L", "P1000_green_M"));

        productStockMap = new HashMap();
        productStockMap.put("P1000_red_L", StockLevelStatus.OUTOFSTOCK);
        productStockMap.put("P1000_green_M", StockLevelStatus.LOWSTOCK);
        willReturn(productStockMap).given(defaultTargetStockLookUpFacade).getConsolidatedSohByVariantMap(
                Arrays.asList("P1000_red_L", "P1000_green_M"), Arrays.asList("CONSOLIDATED_STORES_SOH"));

        final List<StockDatum> stockData = defaultTargetStockLookUpFacade.legacyLookupStock(
                Arrays.asList("P1000_red_L", "P1000_green_M"),
                Arrays.asList("CONSOLIDATED_STORES_SOH"), Arrays.asList("5599", "5612"));
        assertThat(stockData).isNotEmpty().hasSize(2);
        assertThat(stockData).onProperty("variantCode").containsExactly("P1000_red_L", "P1000_green_M");
        assertThat(stockData).onProperty("atsCc").containsExactly(null, null);
        assertThat(stockData).onProperty("atsHd").containsExactly(null, null);
        assertThat(stockData).onProperty("atsEd").containsExactly(null, null);
        assertThat(stockData).onProperty("consolidatedStoreStock").containsExactly(StockLevelStatus.OUTOFSTOCK,
                StockLevelStatus.LOWSTOCK);
        assertThat(stockData.get(0).getStoreSoh().get("5599")).isEqualTo(StockLevelStatus.OUTOFSTOCK);
        assertThat(stockData.get(0).getStoreSoh().get("5612")).isEqualTo(StockLevelStatus.LOWSTOCK);
        assertThat(stockData.get(1).getStoreSoh().get("5599")).isEqualTo(StockLevelStatus.LOWSTOCK);
        assertThat(stockData.get(1).getStoreSoh().get("5612")).isEqualTo(StockLevelStatus.INSTOCK);
    }

    @Test
    public void testLookupStockWithNoBaseProductCode() {
        final Response response = defaultTargetStockLookUpFacade.lookupStock("", Arrays.asList("CC"),
                Collections.<String> emptyList());
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_EMPTY_BASE_PRODUCT");
    }

    @Test
    public void testLookupStockBaseProductWithNoDeliveryTypesNorLocations() {
        final Response response = defaultTargetStockLookUpFacade.lookupStock("P1000_red_L",
                Collections.<String> emptyList(), Collections.<String> emptyList());
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_EMPTY_OPTIONS");
    }

    @Test
    public void testLookupStockOnlineEmptyMap() {
        given(targetProductFacade.getProductForCode("P1000")).willReturn(null);
        final Map<String, Boolean> stockMap = defaultTargetStockLookUpFacade.lookupStockOnline("P1000");
        assertThat(stockMap).isEmpty();
    }

    @Test
    public void testLookupStockOnlineLegacy() throws FluentClientException {
        final ProductModel productModel = mock(AbstractTargetVariantProductModel.class);
        final TargetProductModel targetProductModel = mock(TargetProductModel.class);

        given(targetProductFacade.getProductForCode("P1000")).willReturn(productModel);
        given(targetProductFacade.getBaseTargetProduct(Mockito.any(AbstractTargetVariantProductModel.class)))
                .willReturn(targetProductModel);

        final Collection<VariantProductModel> expectedSellables = new ArrayList();
        final VariantProductModel colourVariant1 = mock(TargetColourVariantProductModel.class);
        final VariantProductModel colourVariant2 = mock(TargetColourVariantProductModel.class);
        final VariantProductModel colourVariant3 = mock(TargetColourVariantProductModel.class);

        given(colourVariant1.getCode()).willReturn("P1000_black");
        given(colourVariant2.getCode()).willReturn("P1000_blue");
        given(colourVariant3.getCode()).willReturn("P1000_red");

        expectedSellables.add(colourVariant1);
        expectedSellables.add(colourVariant2);
        expectedSellables.add(colourVariant3);

        given(targetProductModel.getVariants()).willReturn(expectedSellables);

        defaultTargetStockLookUpFacade.lookupStockOnline("P1000");
        final ArgumentCaptor<Collection> variantsCaptor = ArgumentCaptor.forClass(Collection.class);
        verify(defaultTargetStockLookUpFacade).getLegacyStockMap(variantsCaptor.capture());
        verify(fluentStockLookupService, never()).lookupAts(Mockito.anyString());
        assertThat(variantsCaptor.getValue()).containsOnly("P1000_black", "P1000_blue", "P1000_red");
    }

    @Test
    public void testLookupStockOnlineFluent() throws FluentClientException {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isFluentEnabled();
        final ProductModel productModel = mock(AbstractTargetVariantProductModel.class);
        final TargetProductModel targetProductModel = mock(TargetProductModel.class);

        given(targetProductFacade.getProductForCode("P1000")).willReturn(productModel);
        given(targetProductFacade.getBaseTargetProduct(Mockito.any(AbstractTargetVariantProductModel.class)))
                .willReturn(targetProductModel);

        final Collection<VariantProductModel> expectedSellables = new ArrayList();
        final VariantProductModel colourVariant1 = mock(TargetColourVariantProductModel.class);
        final VariantProductModel colourVariant2 = mock(TargetColourVariantProductModel.class);
        final VariantProductModel colourVariant3 = mock(TargetColourVariantProductModel.class);

        given(colourVariant1.getCode()).willReturn("P1000_black");
        given(colourVariant2.getCode()).willReturn("P1000_blue");
        given(colourVariant3.getCode()).willReturn("P1000_red");

        expectedSellables.add(colourVariant1);
        expectedSellables.add(colourVariant2);
        expectedSellables.add(colourVariant3);

        given(targetProductModel.getVariants()).willReturn(expectedSellables);

        defaultTargetStockLookUpFacade.lookupStockOnline("P1000");

        final ArgumentCaptor<String> blackColourVariantCaptor = ArgumentCaptor.forClass(String.class);
        final ArgumentCaptor<String> blueColourVariantCaptor = ArgumentCaptor.forClass(String.class);
        final ArgumentCaptor<String> redColourVariantCaptor = ArgumentCaptor.forClass(String.class);

        verify(fluentStockLookupService).lookupAts(blackColourVariantCaptor.capture(),
                blueColourVariantCaptor.capture(),
                redColourVariantCaptor.capture());
        verify(defaultTargetStockLookUpFacade, never()).getLegacyStockMap(Mockito.anyCollection());
        assertThat(blackColourVariantCaptor.getValue()).isEqualTo("P1000_black");
        assertThat(blueColourVariantCaptor.getValue()).isEqualTo("P1000_blue");
        assertThat(redColourVariantCaptor.getValue()).isEqualTo("P1000_red");
    }

    @Test
    public void testGetLegacyStockMapWithStock() {
        final Collection<String> variants = new ArrayList();
        variants.add("P1000_black");
        final ProductModel product = mock(ProductModel.class);
        final StockData stockData = mock(StockData.class);
        given(stockData.getStockLevelStatus()).willReturn(StockLevelStatus.INSTOCK);
        given(targetProductFacade.getProductForCode("P1000_black")).willReturn(product);
        given(targetStockService.getStockLevelAndStatusAmount(product)).willReturn(stockData);
        final Map<String, Boolean> resultMap = new HashMap();
        resultMap.put("P1000_black", Boolean.TRUE);
        final Map stockMap = (defaultTargetStockLookUpFacade.getLegacyStockMap(variants));
        assertThat(stockMap).isEqualTo(resultMap);
    }

    @Test
    public void testGetLegacyStockMapWithLowStock() {
        final Collection<String> variants = new ArrayList();
        variants.add("P1000_black");
        final ProductModel product = mock(ProductModel.class);
        final StockData stockData = mock(StockData.class);
        given(stockData.getStockLevelStatus()).willReturn(StockLevelStatus.LOWSTOCK);
        given(targetProductFacade.getProductForCode("P1000_black")).willReturn(product);
        given(targetStockService.getStockLevelAndStatusAmount(product)).willReturn(stockData);
        final Map<String, Boolean> resultMap = new HashMap();
        resultMap.put("P1000_black", Boolean.TRUE);
        final Map stockMap = (defaultTargetStockLookUpFacade.getLegacyStockMap(variants));
        assertThat(stockMap).isEqualTo(resultMap);
    }

    @Test
    public void testGetLegacyStockMapWithNoStock() {
        final Collection<String> variants = new ArrayList();
        variants.add("P1000_black");
        final ProductModel product = mock(ProductModel.class);
        final StockData stockData = mock(StockData.class);
        given(stockData.getStockLevelStatus()).willReturn(StockLevelStatus.OUTOFSTOCK);
        given(targetProductFacade.getProductForCode("P1000_black")).willReturn(product);
        given(targetStockService.getStockLevelAndStatusAmount(product)).willReturn(stockData);
        final Map<String, Boolean> resultMap = new HashMap();
        resultMap.put("P1000_black", Boolean.FALSE);
        final Map stockMap = (defaultTargetStockLookUpFacade.getLegacyStockMap(variants));
        assertThat(stockMap).isEqualTo(resultMap);
    }

    @Test
    public void testGetLegacyStockMapWithoutStockData() {
        final Collection<String> variants = new ArrayList();
        variants.add("P1000_black");
        final ProductModel product = mock(ProductModel.class);
        given(targetProductFacade.getProductForCode("P1000_black")).willReturn(product);
        given(targetStockService.getStockLevelAndStatusAmount(product)).willReturn(null);
        final Map<String, Boolean> resultMap = new HashMap();
        final Map stockMap = (defaultTargetStockLookUpFacade.getLegacyStockMap(variants));
        assertThat(stockMap).isEqualTo(resultMap);
    }

    @Test
    public void testGetLegacyStockMapWithoutVariants() {
        final Collection<String> variants = new ArrayList();
        variants.add("P1000_black");
        given(targetProductFacade.getProductForCode("P1000_black")).willReturn(null);
        final Map<String, Integer> resultMap = new HashMap();
        final Map stockMap = (defaultTargetStockLookUpFacade.getLegacyStockMap(variants));
        assertThat(stockMap).isEqualTo(resultMap);
    }

    @Test
    public void testConvertStockLevelToBool() {
        final Map<String, Integer> stockMap = new HashMap();
        stockMap.put("P1000_black", Integer.valueOf(0));
        stockMap.put("P1000_red", Integer.valueOf(2));
        stockMap.put("P1000_blue", Integer.valueOf(100));
        final Map<String, Boolean> resultMap = defaultTargetStockLookUpFacade.convertStockLevelToBool(stockMap);
        assertThat(resultMap.get("P1000_black")).isFalse();
        assertThat(resultMap.get("P1000_red")).isTrue();
        assertThat(resultMap.get("P1000_blue")).isTrue();
    }

    @Test
    public void testConvertStockLevelToBoolNullStock() {
        final Map<String, Integer> stockMap = new HashMap();
        stockMap.put("P1000_black", null);
        final Map<String, Boolean> resultMap = defaultTargetStockLookUpFacade.convertStockLevelToBool(stockMap);
        assertThat(resultMap.get("P1000_black")).isFalse();
    }

    @Test
    public void testLookupStockWithBaseProductCode() {
        final ProductModel product = mock(ProductModel.class);
        final VariantProductModel colourVariant1 = mock(TargetColourVariantProductModel.class);
        final VariantProductModel colourVariant2 = mock(TargetColourVariantProductModel.class);

        given(colourVariant1.getCode()).willReturn("P1000_black");
        given(colourVariant2.getCode()).willReturn("P1000_red");

        final Collection<VariantProductModel> expectedColours = new ArrayList();
        final Collection<String> expectedColourCodes = new ArrayList();
        expectedColours.add(colourVariant1);
        expectedColours.add(colourVariant2);

        given(product.getVariants()).willReturn(expectedColours);

        expectedColourCodes.add("P1000_black");
        expectedColourCodes.add("P1000_red");

        willReturn(product).given(targetProductFacade).getProductForCode("P1000");

        defaultTargetStockLookUpFacade.lookupStock("P1000", Arrays.asList("CC"),
                Collections.<String> emptyList());

        final ArgumentCaptor<Collection> variantsCaptor = ArgumentCaptor.forClass(Collection.class);
        final ArgumentCaptor<Collection> deliveryTypesCaptor = ArgumentCaptor.forClass(Collection.class);
        final ArgumentCaptor<Collection> locationsCaptor = ArgumentCaptor.forClass(Collection.class);

        verify(defaultTargetStockLookUpFacade).lookupStock(variantsCaptor.capture(), deliveryTypesCaptor.capture(),
                locationsCaptor.capture());
        assertThat(variantsCaptor.getValue()).containsOnly("P1000_black", "P1000_red");
        assertThat(deliveryTypesCaptor.getValue()).containsOnly(Arrays.asList("CC").toArray());
        assertThat(locationsCaptor.getValue()).containsOnly(Collections.<String> emptyList().toArray());
    }

    @Test
    public void testLookupStockWithUnknownBaseProductCode() {
        willThrow(new UnknownIdentifierException(new String())).given(targetProductFacade)
                .getProductForCode("NO_PRODUCT");
        final Response response = defaultTargetStockLookUpFacade.lookupStock("NO_PRODUCT", Arrays.asList("CC"),
                Collections.<String> emptyList());
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_BASE_PRODUCT_UNKNOWN");
    }

    @Test
    public void testLookupStockWithAmbiguousBaseProductCode() {
        willThrow(new AmbiguousIdentifierException(new String())).given(targetProductFacade)
                .getProductForCode("NO_PRODUCT");
        final Response response = defaultTargetStockLookUpFacade.lookupStock("NO_PRODUCT", Arrays.asList("CC"),
                Collections.<String> emptyList());
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_BASE_PRODUCT_UNKNOWN");
    }

    @Test
    public void testGetConsolidatedSohByVariantMap() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isConsolidatedStoreStockAvailable();
        List<ERec> eRecs = new ArrayList<>();
        ERec eRec = mock(ERec.class);
        PropertyMap properties = mock(PropertyMap.class);
        willReturn(properties).given(eRec).getProperties();
        willReturn("P1000_red_L").given(properties)
                .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE);
        willReturn(StockLevelStatus.INSTOCK).given(defaultTargetStockLookUpFacade)
                .getConsolidatedStoreStockLevel(properties);
        eRecs.add(eRec);
        eRec = mock(ERec.class);
        properties = mock(PropertyMap.class);
        willReturn(properties).given(eRec).getProperties();
        willReturn("P1000_green_M").given(properties)
                .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE);
        willReturn(StockLevelStatus.OUTOFSTOCK).given(defaultTargetStockLookUpFacade)
                .getConsolidatedStoreStockLevel(properties);
        eRecs.add(eRec);
        willReturn(eRecs).given(defaultTargetStockLookUpFacade)
                .searchEndeca(Arrays.asList("P1000_red_L", "P1000_green_M", "P_without_variants"),
                        EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE);

        eRecs = new ArrayList<>();
        eRec = mock(ERec.class);
        properties = mock(PropertyMap.class);
        willReturn(properties).given(eRec).getProperties();
        willReturn("P_without_variants").given(properties)
                .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        willReturn(StockLevelStatus.INSTOCK).given(defaultTargetStockLookUpFacade)
                .getConsolidatedStoreStockLevel(properties);
        eRecs.add(eRec);
        willReturn(eRecs).given(defaultTargetStockLookUpFacade)
                .searchEndeca(Arrays.asList("P1000_red_L", "P1000_green_M", "P_without_variants"),
                        EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);

        final Map<String, StockLevelStatus> consolidatedSohByVariantMap = defaultTargetStockLookUpFacade
                .getConsolidatedSohByVariantMap(Arrays.asList("P1000_red_L", "P1000_green_M", "P_without_variants"),
                        Arrays.asList("CC", "HD", "ED", "CONSOLIDATED_STORES_SOH"));
        assertThat(consolidatedSohByVariantMap).isNotEmpty().hasSize(3);
        assertThat(consolidatedSohByVariantMap.get("P1000_red_L")).isEqualTo(StockLevelStatus.INSTOCK);
        assertThat(consolidatedSohByVariantMap.get("P1000_green_M")).isEqualTo(StockLevelStatus.OUTOFSTOCK);
        assertThat(consolidatedSohByVariantMap.get("P_without_variants")).isEqualTo(StockLevelStatus.INSTOCK);
    }

    @Test
    public void testGetConsolidatedSohByVariantMapWithEmptyDeliveryTypes() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isConsolidatedStoreStockAvailable();

        final Map<String, StockLevelStatus> consolidatedSohByVariantMap = defaultTargetStockLookUpFacade
                .getConsolidatedSohByVariantMap(Arrays.asList("P1000_red_L", "P1000_green_M"),
                        Collections.<String> emptyList());
        assertThat(consolidatedSohByVariantMap).isEmpty();
    }

    @Test
    public void testGetConsolidatedSohByVariantMapWithDeliveryTypesMissingConsolidated() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isConsolidatedStoreStockAvailable();

        final Map<String, StockLevelStatus> consolidatedSohByVariantMap = defaultTargetStockLookUpFacade
                .getConsolidatedSohByVariantMap(Arrays.asList("P1000_red_L", "P1000_green_M"),
                        Arrays.asList("CC", "HD", "ED"));
        assertThat(consolidatedSohByVariantMap).isEmpty();
    }

    @Test
    public void testGetConsolidatedSohByVariantMapWithFeatureSwitchOff() {
        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isConsolidatedStoreStockAvailable();

        final Map<String, StockLevelStatus> consolidatedSohByVariantMap = defaultTargetStockLookUpFacade
                .getConsolidatedSohByVariantMap(Arrays.asList("P1000_red_L", "P1000_green_M"),
                        Arrays.asList("CC", "HD", "ED", "CONSOLIDATED_STORES_SOH"));
        assertThat(consolidatedSohByVariantMap).isEmpty();
    }

    @Test
    public void testSearchEndeca() {
        final EndecaSearchStateData searchStateData = mock(EndecaSearchStateData.class);
        willReturn(searchStateData).given(defaultTargetStockLookUpFacade)
                .populateEndecaSearchStateData(Arrays.asList("P1000_red_L", "P1000_green_M"), "sizeVariantCode");
        final ENEQueryResults eNEQueryResults = mock(ENEQueryResults.class);
        final Navigation navigation = mock(Navigation.class);
        final ERecList eRecs = new MERecList();
        eRecs.addAll(Arrays.asList(mock(ERec.class), mock(ERec.class)));
        willReturn(eRecs).given(navigation).getERecs();
        willReturn(navigation).given(eNEQueryResults).getNavigation();
        willReturn(eNEQueryResults).given(endecaProductQueryBuilder).getQueryResults(searchStateData, null, 2);

        assertThat(defaultTargetStockLookUpFacade.searchEndeca(Arrays.asList("P1000_red_L", "P1000_green_M"),
                "sizeVariantCode"))
                .isNotEmpty().hasSize(2);
    }

    @Test
    public void testSearchEndecaWithNavigationNull() {
        final EndecaSearchStateData searchStateData = mock(EndecaSearchStateData.class);
        willReturn(searchStateData).given(defaultTargetStockLookUpFacade)
                .populateEndecaSearchStateData(Arrays.asList("P1000_red_L", "P1000_green_M"), "sizeVariantCode");
        final ENEQueryResults eNEQueryResults = mock(ENEQueryResults.class);
        willReturn(null).given(eNEQueryResults).getNavigation();
        willReturn(eNEQueryResults).given(endecaProductQueryBuilder).getQueryResults(searchStateData, null, 2);

        assertThat(defaultTargetStockLookUpFacade.searchEndeca(Arrays.asList("P1000_red_L", "P1000_green_M"),
                "sizeVariantCode"))
                .isEmpty();
    }

    @Test
    public void testSearchEndecaWithResultNull() {
        final EndecaSearchStateData searchStateData = mock(EndecaSearchStateData.class);
        willReturn(searchStateData).given(defaultTargetStockLookUpFacade)
                .populateEndecaSearchStateData(Arrays.asList("P1000_red_L", "P1000_green_M"), "sizeVariantCode");
        willReturn(null).given(endecaProductQueryBuilder).getQueryResults(searchStateData, null, 2);

        assertThat(defaultTargetStockLookUpFacade.searchEndeca(Arrays.asList("P1000_red_L", "P1000_green_M"),
                "sizeVariantCode"))
                .isEmpty();
    }

    @Test
    public void testPopulateEndecaSearchStateData() {
        final EndecaSearchStateData searchStateData = defaultTargetStockLookUpFacade
                .populateEndecaSearchStateData(Arrays.asList("P1000_red_L", "P1000_green_M"), "sizeVariantCode");
        assertThat(searchStateData).isNotNull();
        assertThat(searchStateData.isSkipInStockFilter()).isFalse();
        assertThat(searchStateData.isSkipDefaultSort()).isTrue();
        assertThat(searchStateData.getMatchMode()).isEqualTo(EndecaConstants.MATCH_MODE_ANY);
        assertThat(searchStateData.getNpSearchState()).isEqualTo(EndecaConstants.ENDECA_N_0);
        assertThat(searchStateData.getSearchField())
                .isEqualTo(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE);
        assertThat(searchStateData.getSearchTerm()).containsExactly("P1000_red_L", "P1000_green_M");
        assertThat(searchStateData.getNuRollupField()).isNull();
        assertThat(searchStateData.getFieldListConfig()).isEqualTo(EndecaConstants.FieldList.STORESTOCK_FIELDLIST);
    }

    @Test
    public void testPopulateEndecaSearchStateDataWithFeatureSwitchOn() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isFindInStoreStockVisibilityEnabled();

        final EndecaSearchStateData searchStateData = defaultTargetStockLookUpFacade
                .populateEndecaSearchStateData(Arrays.asList("P1000_red_L", "P1000_green_M"), "sizeVariantCode");
        assertThat(searchStateData).isNotNull();
        assertThat(searchStateData.isSkipInStockFilter()).isTrue();
        assertThat(searchStateData.isSkipDefaultSort()).isTrue();
        assertThat(searchStateData.getMatchMode()).isEqualTo(EndecaConstants.MATCH_MODE_ANY);
        assertThat(searchStateData.getNpSearchState()).isEqualTo(EndecaConstants.ENDECA_N_0);
        assertThat(searchStateData.getSearchField())
                .isEqualTo(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE);
        assertThat(searchStateData.getSearchTerm()).containsExactly("P1000_red_L", "P1000_green_M");
        assertThat(searchStateData.getNuRollupField()).isNull();
        assertThat(searchStateData.getFieldListConfig()).isEqualTo(EndecaConstants.FieldList.STORESTOCK_FIELDLIST);
    }

    @Test
    public void testGetConsolidatedStoreStockLevel() {
        final PropertyMap properties = mock(PropertyMap.class);
        willReturn("100").given(properties).get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY_IN_STORE);
        assertThat(defaultTargetStockLookUpFacade.getConsolidatedStoreStockLevel(properties))
                .isEqualTo(StockLevelStatus.INSTOCK);
    }

    @Test
    public void testGetConsolidatedStoreStockLevelForLowValue() {
        final PropertyMap properties = mock(PropertyMap.class);
        willReturn("1").given(properties).get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY_IN_STORE);
        assertThat(defaultTargetStockLookUpFacade.getConsolidatedStoreStockLevel(properties))
                .isEqualTo(StockLevelStatus.INSTOCK);
    }

    @Test
    public void testGetConsolidatedStoreStockLevelForZero() {
        final PropertyMap properties = mock(PropertyMap.class);
        willReturn("0").given(properties).get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY_IN_STORE);
        assertThat(defaultTargetStockLookUpFacade.getConsolidatedStoreStockLevel(properties))
                .isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    @Test
    public void testGetConsolidatedStoreStockLevelForNoValue() {
        final PropertyMap properties = mock(PropertyMap.class);
        assertThat(defaultTargetStockLookUpFacade.getConsolidatedStoreStockLevel(properties))
                .isEqualTo(StockLevelStatus.OUTOFSTOCK);
    }

    @Test
    public void testGetConsolidatedStoreStockLevelWithFeatureSwitchOn() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isFindInStoreStockVisibilityEnabled();
        final PropertyMap properties = mock(PropertyMap.class);
        willReturn("0").given(properties).get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_AVAIL_QTY_IN_STORE);
        willReturn("1").given(properties)
                .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_ACTUAL_CONSOLIDATED_STORE_STOCK);
        assertThat(defaultTargetStockLookUpFacade.getConsolidatedStoreStockLevel(properties))
                .isEqualTo(StockLevelStatus.INSTOCK);
    }

    @Test
    public void testGetStockFromWarehouseWhenItemCodeIsInStockAndFalconDialOn() {
        final StockAvailabilitiesData stockAvailabilitiesData = new StockAvailabilitiesData();
        final StockAvailabilityData stockAvailabilityData = new StockAvailabilityData();
        stockAvailabilityData.setFulfillmentType("SHIP_STORE");
        stockAvailabilityData.setItemCode("p1000_black_l");
        stockAvailabilityData.setRequestedQuantity(3);
        final Map<String, Integer> availMap = new HashMap<String, Integer>();
        availMap.put("p1000_black_l", Integer.valueOf(20));
        given(
                targetStockService.getStockForProductsInWarehouse(Arrays.asList("p1000_black_l"), null))
                .willReturn(availMap);
        stockAvailabilitiesData.setAvailabilities(Arrays.asList(stockAvailabilityData));
        willReturn(Integer.valueOf(6)).given(targetSharedConfigService)
                .getInt(TgtCoreConstants.Config.MAX_QTY_FASTLINE_FULFILLED_PER_DAY, 100);

        willReturn(warehouseModel).given(targetWarehouseService).getDefaultOnlineWarehouse();
        willReturn(Integer.valueOf(2)).given(warehouseModel).getTotalQtyFulfilledAtFastlineToday();

        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON);

        assertThat(defaultTargetStockLookUpFacade.getStockFromWarehouse(stockAvailabilitiesData, "ebay", null))
                .isEqualTo(stockAvailabilitiesData);
        final StockAvailabilityData stockAvailabilityDataResult = stockAvailabilitiesData.getAvailabilities().get(0);
        assertThat(stockAvailabilityDataResult.getItemCode()).isEqualTo("p1000_black_l");
        assertThat(stockAvailabilityDataResult.getTotalAvailableQuantity()).isEqualTo(0);
        assertThat(stockAvailabilityDataResult.isIsAvailable()).isEqualTo(false);
    }

}

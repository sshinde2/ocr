/**
 *
 */
package au.com.target.tgtfacades.storelocator.converters.populator;


import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.util.ConverterFactory;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.TargetOpeningDayModel;
import au.com.target.tgtfacades.storelocator.data.TargetOpeningDayData;


/**
 * @author rmcalave
 *
 */
@UnitTest
public class TargetOpeningDayPopulatorTest {
    private final TargetOpeningDayPopulator targetOpeningDayPopulator = new TargetOpeningDayPopulator();
    private AbstractPopulatingConverter<TargetOpeningDayModel, TargetOpeningDayData> targetOpeningDayConverter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        targetOpeningDayConverter = new ConverterFactory<TargetOpeningDayModel, TargetOpeningDayData, TargetOpeningDayPopulator>()
                .create(TargetOpeningDayData.class, targetOpeningDayPopulator);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateWithStandardHours() {
        final DateTime openingDateTime = DateTime.now().withTime(8, 0, 0, 0); // 8:00AM today
        final DateTime closingDateTime = DateTime.now().withTime(17, 0, 0, 0); // 5:00PM today

        final TargetOpeningDayModel mockTargetOpeningDayModel = mock(TargetOpeningDayModel.class);
        given(mockTargetOpeningDayModel.getOpeningTime()).willReturn(openingDateTime.toDate());
        given(mockTargetOpeningDayModel.getClosingTime()).willReturn(closingDateTime.toDate());
        given(mockTargetOpeningDayModel.isClosed()).willReturn(Boolean.FALSE);
        given(mockTargetOpeningDayModel.getMessage()).willReturn("");

        final TargetOpeningDayData result = targetOpeningDayConverter.convert(mockTargetOpeningDayModel);

        assertThat(result.getOpeningDateTime()).isEqualTo(openingDateTime);
        assertThat(result.getClosingDateTime()).isEqualTo(closingDateTime);

        assertThat(result.getFormattedDay()).isEqualTo(openingDateTime.dayOfWeek().getAsText());
        assertThat(result.getFormattedRelativeDay()).isEqualTo("Today");
        assertThat(result.getFormattedOpeningTime()).isEqualTo("8:00 AM");
        assertThat(result.getFormattedClosingTime()).isEqualTo("5:00 PM");

        assertThat(result.isClosed()).isFalse();
        assertThat(result.isAllDayTrade()).isFalse();
        assertThat(result.getMessage()).isEmpty();
    }

    @Test
    public void testPopulateWithStandardHoursOpeningTomorrow() {
        final DateTime openingDateTime = DateTime.now().plusDays(1).withTime(8, 0, 0, 0); // 8:00AM today
        final DateTime closingDateTime = DateTime.now().plusDays(1).withTime(17, 0, 0, 0); // 5:00PM today

        final TargetOpeningDayModel mockTargetOpeningDayModel = mock(TargetOpeningDayModel.class);
        given(mockTargetOpeningDayModel.getOpeningTime()).willReturn(openingDateTime.toDate());
        given(mockTargetOpeningDayModel.getClosingTime()).willReturn(closingDateTime.toDate());
        doReturn(Boolean.FALSE).when(mockTargetOpeningDayModel).isClosed();
        given(mockTargetOpeningDayModel.getMessage()).willReturn("");

        final TargetOpeningDayData result = targetOpeningDayConverter.convert(mockTargetOpeningDayModel);

        Assert.assertEquals(openingDateTime, result.getOpeningDateTime());
        Assert.assertEquals(closingDateTime, result.getClosingDateTime());

        Assert.assertEquals(openingDateTime.dayOfWeek().getAsText(), result.getFormattedDay());
        assertThat(result.getFormattedRelativeDay()).isEqualTo("Tomorrow");
        Assert.assertEquals("8:00 AM", result.getFormattedOpeningTime());
        Assert.assertEquals("5:00 PM", result.getFormattedClosingTime());

        Assert.assertFalse(result.isClosed());
        Assert.assertFalse(result.isAllDayTrade());
        Assert.assertTrue(StringUtils.isBlank(result.getMessage()));
    }

    @Test
    public void testPopulateWithStandardHoursOpeningInTwoDays() {
        final DateTime openingDateTime = DateTime.now().plusDays(2).withTime(8, 0, 0, 0); // 8:00AM today
        final DateTime closingDateTime = DateTime.now().plusDays(2).withTime(17, 0, 0, 0); // 5:00PM today

        final TargetOpeningDayModel mockTargetOpeningDayModel = mock(TargetOpeningDayModel.class);
        given(mockTargetOpeningDayModel.getOpeningTime()).willReturn(openingDateTime.toDate());
        given(mockTargetOpeningDayModel.getClosingTime()).willReturn(closingDateTime.toDate());
        doReturn(Boolean.FALSE).when(mockTargetOpeningDayModel).isClosed();
        given(mockTargetOpeningDayModel.getMessage()).willReturn("");

        final TargetOpeningDayData result = targetOpeningDayConverter.convert(mockTargetOpeningDayModel);

        Assert.assertEquals(openingDateTime, result.getOpeningDateTime());
        Assert.assertEquals(closingDateTime, result.getClosingDateTime());

        Assert.assertEquals(openingDateTime.dayOfWeek().getAsText(), result.getFormattedDay());
        assertThat(result.getFormattedRelativeDay()).isNull();
        Assert.assertEquals("8:00 AM", result.getFormattedOpeningTime());
        Assert.assertEquals("5:00 PM", result.getFormattedClosingTime());

        Assert.assertFalse(result.isClosed());
        Assert.assertFalse(result.isAllDayTrade());
        Assert.assertTrue(StringUtils.isBlank(result.getMessage()));
    }

    @Test
    public void testPopulateWithStandardHoursOpenedYesterday() {
        final DateTime openingDateTime = DateTime.now().minusDays(1).withTime(8, 0, 0, 0); // 8:00AM today
        final DateTime closingDateTime = DateTime.now().minusDays(1).withTime(17, 0, 0, 0); // 5:00PM today

        final TargetOpeningDayModel mockTargetOpeningDayModel = mock(TargetOpeningDayModel.class);
        given(mockTargetOpeningDayModel.getOpeningTime()).willReturn(openingDateTime.toDate());
        given(mockTargetOpeningDayModel.getClosingTime()).willReturn(closingDateTime.toDate());
        doReturn(Boolean.FALSE).when(mockTargetOpeningDayModel).isClosed();
        given(mockTargetOpeningDayModel.getMessage()).willReturn("");

        final TargetOpeningDayData result = targetOpeningDayConverter.convert(mockTargetOpeningDayModel);

        Assert.assertEquals(openingDateTime, result.getOpeningDateTime());
        Assert.assertEquals(closingDateTime, result.getClosingDateTime());

        Assert.assertEquals(openingDateTime.dayOfWeek().getAsText(), result.getFormattedDay());
        assertThat(result.getFormattedRelativeDay()).isNull();
        Assert.assertEquals("8:00 AM", result.getFormattedOpeningTime());
        Assert.assertEquals("5:00 PM", result.getFormattedClosingTime());

        Assert.assertFalse(result.isClosed());
        Assert.assertFalse(result.isAllDayTrade());
        Assert.assertTrue(StringUtils.isBlank(result.getMessage()));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateWithStandardValuesOpeningMidnight() {
        final DateTime openingDateTime = DateTime.now().withTime(0, 0, 0, 0); // Midnight today
        final DateTime closingDateTime = DateTime.now().withTime(17, 0, 0, 0); // 5:00PM today

        final TargetOpeningDayModel mockTargetOpeningDayModel = mock(TargetOpeningDayModel.class);
        given(mockTargetOpeningDayModel.getOpeningTime()).willReturn(openingDateTime.toDate());
        given(mockTargetOpeningDayModel.getClosingTime()).willReturn(closingDateTime.toDate());
        given(mockTargetOpeningDayModel.isClosed()).willReturn(Boolean.FALSE);
        given(mockTargetOpeningDayModel.getMessage()).willReturn("");

        final TargetOpeningDayData result = targetOpeningDayConverter.convert(mockTargetOpeningDayModel);

        Assert.assertEquals(openingDateTime, result.getOpeningDateTime());
        Assert.assertEquals(closingDateTime, result.getClosingDateTime());

        Assert.assertEquals(openingDateTime.dayOfWeek().getAsText(), result.getFormattedDay());
        assertThat(result.getFormattedRelativeDay()).isEqualTo("Today");
        Assert.assertEquals("Midnight", result.getFormattedOpeningTime());
        Assert.assertEquals("5:00 PM", result.getFormattedClosingTime());

        Assert.assertFalse(result.isClosed());
        Assert.assertFalse(result.isAllDayTrade());
        Assert.assertTrue(StringUtils.isBlank(result.getMessage()));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateWithStandardValuesClosingMidnight() {
        final DateTime openingDateTime = DateTime.now().withTime(8, 0, 0, 0); // 8:00AM today
        final DateTime closingDateTime = DateTime.now().plusDays(1).withTime(0, 0, 0, 0); // Midnight tomorrow

        final TargetOpeningDayModel mockTargetOpeningDayModel = mock(TargetOpeningDayModel.class);
        given(mockTargetOpeningDayModel.getOpeningTime()).willReturn(openingDateTime.toDate());
        given(mockTargetOpeningDayModel.getClosingTime()).willReturn(closingDateTime.toDate());
        given(mockTargetOpeningDayModel.isClosed()).willReturn(Boolean.FALSE);
        given(mockTargetOpeningDayModel.getMessage()).willReturn("");

        final TargetOpeningDayData result = targetOpeningDayConverter.convert(mockTargetOpeningDayModel);

        Assert.assertEquals(openingDateTime, result.getOpeningDateTime());
        Assert.assertEquals(closingDateTime, result.getClosingDateTime());

        Assert.assertEquals(openingDateTime.dayOfWeek().getAsText(), result.getFormattedDay());
        assertThat(result.getFormattedRelativeDay()).isEqualTo("Today");
        Assert.assertEquals("8:00 AM", result.getFormattedOpeningTime());
        Assert.assertEquals("Midnight", result.getFormattedClosingTime());

        Assert.assertFalse(result.isClosed());
        Assert.assertFalse(result.isAllDayTrade());
        Assert.assertTrue(StringUtils.isBlank(result.getMessage()));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateWithStandardValuesOpeningAtNearlyMidnight() {
        final DateTime openingDateTime = DateTime.now().withTime(0, 1, 0, 0); // 12:01AM today
        final DateTime closingDateTime = DateTime.now().withTime(17, 0, 0, 0); // 5:00PM today

        final TargetOpeningDayModel mockTargetOpeningDayModel = mock(TargetOpeningDayModel.class);
        given(mockTargetOpeningDayModel.getOpeningTime()).willReturn(openingDateTime.toDate());
        given(mockTargetOpeningDayModel.getClosingTime()).willReturn(closingDateTime.toDate());
        given(mockTargetOpeningDayModel.isClosed()).willReturn(Boolean.FALSE);
        given(mockTargetOpeningDayModel.getMessage()).willReturn("");

        final TargetOpeningDayData result = targetOpeningDayConverter.convert(mockTargetOpeningDayModel);

        Assert.assertEquals(openingDateTime, result.getOpeningDateTime());
        Assert.assertEquals(closingDateTime, result.getClosingDateTime());

        Assert.assertEquals(openingDateTime.dayOfWeek().getAsText(), result.getFormattedDay());
        assertThat(result.getFormattedRelativeDay()).isEqualTo("Today");
        Assert.assertEquals("Midnight", result.getFormattedOpeningTime());
        Assert.assertEquals("5:00 PM", result.getFormattedClosingTime());

        Assert.assertFalse(result.isClosed());
        Assert.assertFalse(result.isAllDayTrade());
        Assert.assertTrue(StringUtils.isBlank(result.getMessage()));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateWithStandardValuesClosingAtNearlyMidnight() {
        final DateTime openingDateTime = DateTime.now().withTime(8, 0, 0, 0); // 8:00AM today
        final DateTime closingDateTime = DateTime.now().withTime(23, 59, 0, 0); // 11:59PM today

        final TargetOpeningDayModel mockTargetOpeningDayModel = mock(TargetOpeningDayModel.class);
        given(mockTargetOpeningDayModel.getOpeningTime()).willReturn(openingDateTime.toDate());
        given(mockTargetOpeningDayModel.getClosingTime()).willReturn(closingDateTime.toDate());
        given(mockTargetOpeningDayModel.isClosed()).willReturn(Boolean.FALSE);
        given(mockTargetOpeningDayModel.getMessage()).willReturn("");

        final TargetOpeningDayData result = targetOpeningDayConverter.convert(mockTargetOpeningDayModel);

        Assert.assertEquals(openingDateTime, result.getOpeningDateTime());
        Assert.assertEquals(closingDateTime, result.getClosingDateTime());

        Assert.assertEquals(openingDateTime.dayOfWeek().getAsText(), result.getFormattedDay());
        assertThat(result.getFormattedRelativeDay()).isEqualTo("Today");
        Assert.assertEquals("8:00 AM", result.getFormattedOpeningTime());
        Assert.assertEquals("Midnight", result.getFormattedClosingTime());

        Assert.assertFalse(result.isClosed());
        Assert.assertFalse(result.isAllDayTrade());
        Assert.assertTrue(StringUtils.isBlank(result.getMessage()));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateWithStandardValuesOpeningMidday() {
        final DateTime openingDateTime = DateTime.now().withTime(12, 0, 0, 0); // Midday today
        final DateTime closingDateTime = DateTime.now().withTime(17, 0, 0, 0); // 5:00PM today

        final TargetOpeningDayModel mockTargetOpeningDayModel = mock(TargetOpeningDayModel.class);
        given(mockTargetOpeningDayModel.getOpeningTime()).willReturn(openingDateTime.toDate());
        given(mockTargetOpeningDayModel.getClosingTime()).willReturn(closingDateTime.toDate());
        given(mockTargetOpeningDayModel.isClosed()).willReturn(Boolean.FALSE);
        given(mockTargetOpeningDayModel.getMessage()).willReturn("");

        final TargetOpeningDayData result = targetOpeningDayConverter.convert(mockTargetOpeningDayModel);

        Assert.assertEquals(openingDateTime, result.getOpeningDateTime());
        Assert.assertEquals(closingDateTime, result.getClosingDateTime());

        Assert.assertEquals(openingDateTime.dayOfWeek().getAsText(), result.getFormattedDay());
        assertThat(result.getFormattedRelativeDay()).isEqualTo("Today");
        Assert.assertEquals("Midday", result.getFormattedOpeningTime());
        Assert.assertEquals("5:00 PM", result.getFormattedClosingTime());

        Assert.assertFalse(result.isClosed());
        Assert.assertFalse(result.isAllDayTrade());
        Assert.assertTrue(StringUtils.isBlank(result.getMessage()));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateWithStandardValuesClosingMidday() {
        final DateTime openingDateTime = DateTime.now().withTime(8, 0, 0, 0); // 8:00AM today
        final DateTime closingDateTime = DateTime.now().withTime(12, 0, 0, 0); // Midday today

        final TargetOpeningDayModel mockTargetOpeningDayModel = mock(TargetOpeningDayModel.class);
        given(mockTargetOpeningDayModel.getOpeningTime()).willReturn(openingDateTime.toDate());
        given(mockTargetOpeningDayModel.getClosingTime()).willReturn(closingDateTime.toDate());
        given(mockTargetOpeningDayModel.isClosed()).willReturn(Boolean.FALSE);
        given(mockTargetOpeningDayModel.getMessage()).willReturn("");

        final TargetOpeningDayData result = targetOpeningDayConverter.convert(mockTargetOpeningDayModel);

        Assert.assertEquals(openingDateTime, result.getOpeningDateTime());
        Assert.assertEquals(closingDateTime, result.getClosingDateTime());

        Assert.assertEquals(openingDateTime.dayOfWeek().getAsText(), result.getFormattedDay());
        assertThat(result.getFormattedRelativeDay()).isEqualTo("Today");
        Assert.assertEquals("8:00 AM", result.getFormattedOpeningTime());
        Assert.assertEquals("Midday", result.getFormattedClosingTime());

        Assert.assertFalse(result.isClosed());
        Assert.assertFalse(result.isAllDayTrade());
        Assert.assertTrue(StringUtils.isBlank(result.getMessage()));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateWithStandardValuesOpeningAtNearlyMidday() {
        final DateTime openingDateTime = DateTime.now().withTime(12, 1, 0, 0); // 12:01PM today
        final DateTime closingDateTime = DateTime.now().withTime(17, 0, 0, 0); // 5:00PM today

        final TargetOpeningDayModel mockTargetOpeningDayModel = mock(TargetOpeningDayModel.class);
        given(mockTargetOpeningDayModel.getOpeningTime()).willReturn(openingDateTime.toDate());
        given(mockTargetOpeningDayModel.getClosingTime()).willReturn(closingDateTime.toDate());
        given(mockTargetOpeningDayModel.isClosed()).willReturn(Boolean.FALSE);
        given(mockTargetOpeningDayModel.getMessage()).willReturn("");

        final TargetOpeningDayData result = targetOpeningDayConverter.convert(mockTargetOpeningDayModel);

        Assert.assertEquals(openingDateTime, result.getOpeningDateTime());
        Assert.assertEquals(closingDateTime, result.getClosingDateTime());

        Assert.assertEquals(openingDateTime.dayOfWeek().getAsText(), result.getFormattedDay());
        assertThat(result.getFormattedRelativeDay()).isEqualTo("Today");
        Assert.assertEquals("Midday", result.getFormattedOpeningTime());
        Assert.assertEquals("5:00 PM", result.getFormattedClosingTime());

        Assert.assertFalse(result.isClosed());
        Assert.assertFalse(result.isAllDayTrade());
        Assert.assertTrue(StringUtils.isBlank(result.getMessage()));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateWithStandardValuesClosingAtNearlyMidday() {
        final DateTime openingDateTime = DateTime.now().withTime(8, 0, 0, 0); // 8:00AM today
        final DateTime closingDateTime = DateTime.now().withTime(11, 59, 0, 0); // 11:59AM today

        final TargetOpeningDayModel mockTargetOpeningDayModel = mock(TargetOpeningDayModel.class);
        given(mockTargetOpeningDayModel.getOpeningTime()).willReturn(openingDateTime.toDate());
        given(mockTargetOpeningDayModel.getClosingTime()).willReturn(closingDateTime.toDate());
        given(mockTargetOpeningDayModel.isClosed()).willReturn(Boolean.FALSE);
        given(mockTargetOpeningDayModel.getMessage()).willReturn("");

        final TargetOpeningDayData result = targetOpeningDayConverter.convert(mockTargetOpeningDayModel);

        Assert.assertEquals(openingDateTime, result.getOpeningDateTime());
        Assert.assertEquals(closingDateTime, result.getClosingDateTime());

        Assert.assertEquals(openingDateTime.dayOfWeek().getAsText(), result.getFormattedDay());
        assertThat(result.getFormattedRelativeDay()).isEqualTo("Today");
        Assert.assertEquals("8:00 AM", result.getFormattedOpeningTime());
        Assert.assertEquals("Midday", result.getFormattedClosingTime());

        Assert.assertFalse(result.isClosed());
        Assert.assertFalse(result.isAllDayTrade());
        Assert.assertTrue(StringUtils.isBlank(result.getMessage()));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateWithAllDayTrade() {
        final DateTime openingDateTime = DateTime.now().withTime(0, 1, 0, 0); // 12:01AM today
        final DateTime closingDateTime = DateTime.now().withTime(23, 59, 0, 0); // 11:59PM today

        final TargetOpeningDayModel mockTargetOpeningDayModel = mock(TargetOpeningDayModel.class);
        given(mockTargetOpeningDayModel.getOpeningTime()).willReturn(openingDateTime.toDate());
        given(mockTargetOpeningDayModel.getClosingTime()).willReturn(closingDateTime.toDate());
        given(mockTargetOpeningDayModel.isClosed()).willReturn(Boolean.FALSE);
        given(mockTargetOpeningDayModel.getMessage()).willReturn("");

        final TargetOpeningDayData result = targetOpeningDayConverter.convert(mockTargetOpeningDayModel);

        Assert.assertEquals(openingDateTime, result.getOpeningDateTime());
        Assert.assertEquals(closingDateTime, result.getClosingDateTime());

        Assert.assertEquals(openingDateTime.dayOfWeek().getAsText(), result.getFormattedDay());
        assertThat(result.getFormattedRelativeDay()).isEqualTo("Today");
        Assert.assertEquals("Midnight", result.getFormattedOpeningTime());
        Assert.assertEquals("Midnight", result.getFormattedClosingTime());

        Assert.assertFalse(result.isClosed());
        Assert.assertTrue(result.isAllDayTrade());
        Assert.assertTrue(StringUtils.isBlank(result.getMessage()));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPopulateWithStandardHoursAndIsClosedWithMessage() {
        final String message = "Robots destroyed the building.";

        final DateTime openingDateTime = DateTime.now().withTime(8, 0, 0, 0); // 8:00AM today
        final DateTime closingDateTime = DateTime.now().withTime(17, 0, 0, 0); // 5:00PM today

        final TargetOpeningDayModel mockTargetOpeningDayModel = mock(TargetOpeningDayModel.class);
        given(mockTargetOpeningDayModel.getOpeningTime()).willReturn(openingDateTime.toDate());
        given(mockTargetOpeningDayModel.getClosingTime()).willReturn(closingDateTime.toDate());
        given(mockTargetOpeningDayModel.isClosed()).willReturn(Boolean.TRUE);
        given(mockTargetOpeningDayModel.getMessage()).willReturn(message);

        final TargetOpeningDayData result = targetOpeningDayConverter.convert(mockTargetOpeningDayModel);

        Assert.assertEquals(openingDateTime, result.getOpeningDateTime());
        Assert.assertEquals(closingDateTime, result.getClosingDateTime());

        Assert.assertEquals(openingDateTime.dayOfWeek().getAsText(), result.getFormattedDay());
        assertThat(result.getFormattedRelativeDay()).isEqualTo("Today");
        Assert.assertEquals("8:00 AM", result.getFormattedOpeningTime());
        Assert.assertEquals("5:00 PM", result.getFormattedClosingTime());

        Assert.assertTrue(result.isClosed());
        Assert.assertFalse(result.isAllDayTrade());
        Assert.assertEquals(message, result.getMessage());
    }

    @Test
    public void testFormatEndOfDayTradingHourNormalTrading() {
        final DateTime open = new DateTime(2015, 1, 1, 9, 0, 0);
        final DateTime close = new DateTime(2015, 1, 1, 17, 30, 0);
        final String result = targetOpeningDayPopulator.formatEndOfDayTradingHour(open, close);
        Assert.assertEquals("5:30 PM", result);
    }

    @Test
    public void testFormatEndOfDayTradingHourMidday() {
        final DateTime open = new DateTime(2015, 1, 1, 9, 0, 0);
        final DateTime close = new DateTime(2015, 1, 1, 12, 0, 0);
        final String result = targetOpeningDayPopulator.formatEndOfDayTradingHour(open, close);
        Assert.assertEquals("Midday", result);
    }

    @Test
    public void testFormatEndOfDayTradingHourMidnight() {
        final DateTime open = new DateTime(2015, 1, 1, 9, 0, 0);
        final DateTime close = new DateTime(2015, 1, 1, 23, 59, 59);
        final String result = targetOpeningDayPopulator.formatEndOfDayTradingHour(open, close);
        Assert.assertEquals("Midnight", result);
    }

    @Test
    public void testFormatEndOfDayTradingHourNormalTradingDLSBefore() {
        final DateTime open = new DateTime(2015, 4, 4, 9, 0, 0);
        final DateTime close = new DateTime(2015, 4, 4, 17, 30, 0);
        final String result = targetOpeningDayPopulator.formatEndOfDayTradingHour(open, close);
        Assert.assertEquals("5:30 PM", result);
    }

    @Test
    public void testFormatEndOfDayTradingHourMiddayDLSBefore() {
        final DateTime open = new DateTime(2015, 4, 4, 9, 0, 0);
        final DateTime close = new DateTime(2015, 4, 4, 12, 0, 0);
        final String result = targetOpeningDayPopulator.formatEndOfDayTradingHour(open, close);
        Assert.assertEquals("Midday", result);
    }

    @Test
    public void testFormatEndOfDayTradingHourMidnightDLSBefore() {
        final DateTime open = new DateTime(2015, 4, 4, 9, 0, 0);
        final DateTime close = new DateTime(2015, 4, 4, 23, 59, 59);
        final String result = targetOpeningDayPopulator.formatEndOfDayTradingHour(open, close);
        Assert.assertEquals("Midnight", result);
    }

    @Test
    public void testFormatEndOfDayTradingHourOpenEarlyMorningDLSBefore() {
        final DateTime open = new DateTime(2015, 4, 4, 0, 30, 0);
        final DateTime close = new DateTime(2015, 4, 4, 23, 59, 59);
        final String result = targetOpeningDayPopulator.formatEndOfDayTradingHour(open, close);
        Assert.assertEquals("Midnight", result);
    }

    @Test
    public void testFormatEndOfDayTradingHourNormalTradingDLSAfter() {
        final DateTime open = new DateTime(2015, 4, 5, 9, 0, 0);
        final DateTime close = new DateTime(2015, 4, 5, 17, 30, 0);
        final String result = targetOpeningDayPopulator.formatEndOfDayTradingHour(open, close);
        Assert.assertEquals("5:30 PM", result);
    }

    @Test
    public void testFormatEndOfDayTradingHourMiddayDLSAfter() {
        final DateTime open = new DateTime(2015, 4, 5, 9, 0, 0);
        final DateTime close = new DateTime(2015, 4, 5, 12, 0, 0);
        final String result = targetOpeningDayPopulator.formatEndOfDayTradingHour(open, close);
        Assert.assertEquals("Midday", result);
    }

    @Test
    public void testFormatEndOfDayTradingHourMidnightDLSAfter() {
        final DateTime open = new DateTime(2015, 4, 5, 9, 0, 0);
        final DateTime close = new DateTime(2015, 4, 5, 23, 59, 59);
        final String result = targetOpeningDayPopulator.formatEndOfDayTradingHour(open, close);
        Assert.assertEquals("Midnight", result);
    }

    @Test
    public void testFormatEndOfDayTradingHourOpenEarlyMorningDLSAfter() {
        final DateTime open = new DateTime(2015, 4, 5, 0, 30, 0);
        final DateTime close = new DateTime(2015, 4, 5, 23, 59, 59);
        final String result = targetOpeningDayPopulator.formatEndOfDayTradingHour(open, close);
        Assert.assertEquals("Midnight", result);
    }

}

/**
 *
 */
package au.com.target.tgtfacades.storelocator.converters.populator;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.util.ConverterFactory;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.storelocator.model.OpeningDayModel;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.fest.assertions.Assertions;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.TargetOpeningDayModel;
import au.com.target.tgtfacades.storelocator.data.TargetOpeningDayData;
import au.com.target.tgtfacades.storelocator.data.TargetOpeningScheduleData;
import au.com.target.tgtfacades.storelocator.data.TargetOpeningWeekData;


/**
 * @author rmcalave
 *
 */
@UnitTest
public class TargetOpeningSchedulePopulatorTest {
    @InjectMocks
    private final TargetOpeningSchedulePopulator targetOpeningSchedulePopulator = new TargetOpeningSchedulePopulator();

    @Mock
    private AbstractPopulatingConverter<TargetOpeningDayModel, TargetOpeningDayData> mockTargetOpeningDayConverter;

    private AbstractPopulatingConverter<OpeningScheduleModel, TargetOpeningScheduleData> mockTargetOpeningScheduleConverter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockTargetOpeningScheduleConverter = new ConverterFactory<OpeningScheduleModel, TargetOpeningScheduleData, TargetOpeningSchedulePopulator>()
                .create(TargetOpeningScheduleData.class, targetOpeningSchedulePopulator);
    }

    @Test
    public void testPopulateWithNoOpeningDaysReturnEmptyWeeks() {
        final OpeningScheduleModel mockOpeningScheduleModel = mock(OpeningScheduleModel.class,
                Mockito.RETURNS_DEFAULTS);
        final TargetOpeningScheduleData mockTargetOpeningScheduleData = new TargetOpeningScheduleData();

        mockTargetOpeningScheduleConverter.populate(mockOpeningScheduleModel, mockTargetOpeningScheduleData);

        verify(mockOpeningScheduleModel).getOpeningDays();
        verifyZeroInteractions(mockTargetOpeningDayConverter);
        Assertions.assertThat(mockTargetOpeningScheduleData.getTargetOpeningWeeks()).isNotNull();
        Assertions.assertThat(mockTargetOpeningScheduleData.getTargetOpeningWeeks()).isEmpty();

    }

    @Test
    public void testPopulateWithStandardTwoWeeksData() {
        final LocalDate startOfFirstWeek = LocalDate.now().withDayOfWeek(DateTimeConstants.MONDAY);
        final LocalDate endOfFirstWeek = startOfFirstWeek.withDayOfWeek(DateTimeConstants.SUNDAY);
        final LocalDate startOfSecondWeek = startOfFirstWeek.plusWeeks(1);
        final LocalDate endOfSecondWeek = startOfSecondWeek.withDayOfWeek(DateTimeConstants.SUNDAY);
        final int numberOfDays = 14;

        final List<OpeningDayModel> mockOpeningDayModels = createMockOpeningDayModels(numberOfDays);
        final List<TargetOpeningDayData> mockOpeningDayData = createMockOpeningDays(startOfFirstWeek, numberOfDays);

        setUpMockTargetOpeningDayConverter(mockOpeningDayModels, mockOpeningDayData);

        final OpeningScheduleModel mockOpeningScheduleModel = mock(OpeningScheduleModel.class,
                Mockito.RETURNS_DEFAULTS);
        given(mockOpeningScheduleModel.getOpeningDays()).willReturn(mockOpeningDayModels);

        final TargetOpeningScheduleData result = new TargetOpeningScheduleData();
        mockTargetOpeningScheduleConverter.populate(mockOpeningScheduleModel, result);


        final List<TargetOpeningWeekData> openingWeeks = result.getTargetOpeningWeeks();
        Assert.assertEquals(2, openingWeeks.size());

        final TargetOpeningWeekData firstWeek = openingWeeks.get(0);
        Assert.assertEquals(startOfFirstWeek, firstWeek.getStartDate());
        Assert.assertEquals(endOfFirstWeek, firstWeek.getEndDate());
        Assert.assertEquals(createFormattedDateString(startOfFirstWeek), firstWeek.getFormattedStartDate());
        Assert.assertEquals(createFormattedDateString(endOfFirstWeek), firstWeek.getFormattedEndDate());
        validateOpeningDayList(firstWeek.getTargetOpeningDayList(), startOfFirstWeek);

        final TargetOpeningWeekData secondWeek = openingWeeks.get(1);
        Assert.assertEquals(startOfSecondWeek, secondWeek.getStartDate());
        Assert.assertEquals(endOfSecondWeek, secondWeek.getEndDate());
        Assert.assertEquals(createFormattedDateString(startOfSecondWeek), secondWeek.getFormattedStartDate());
        Assert.assertEquals(createFormattedDateString(endOfSecondWeek), secondWeek.getFormattedEndDate());
        validateOpeningDayList(secondWeek.getTargetOpeningDayList(), startOfSecondWeek);
    }

    @Test
    public void testPopulateWithShuffledTwoWeeksData() {
        final LocalDate startOfFirstWeek = LocalDate.now().withDayOfWeek(DateTimeConstants.MONDAY);
        final LocalDate endOfFirstWeek = startOfFirstWeek.withDayOfWeek(DateTimeConstants.SUNDAY);
        final LocalDate startOfSecondWeek = startOfFirstWeek.plusWeeks(1);
        final LocalDate endOfSecondWeek = startOfSecondWeek.withDayOfWeek(DateTimeConstants.SUNDAY);
        final int numberOfDays = 14;

        final List<OpeningDayModel> mockOpeningDayModels = createMockOpeningDayModels(numberOfDays);
        final List<TargetOpeningDayData> mockOpeningDayData = createMockOpeningDays(startOfFirstWeek, numberOfDays);

        Collections.shuffle(mockOpeningDayData);

        setUpMockTargetOpeningDayConverter(mockOpeningDayModels, mockOpeningDayData);

        final OpeningScheduleModel mockOpeningScheduleModel = mock(OpeningScheduleModel.class,
                Mockito.RETURNS_DEFAULTS);
        given(mockOpeningScheduleModel.getOpeningDays()).willReturn(mockOpeningDayModels);

        final TargetOpeningScheduleData result = new TargetOpeningScheduleData();
        mockTargetOpeningScheduleConverter.populate(mockOpeningScheduleModel, result);


        final List<TargetOpeningWeekData> openingWeeks = result.getTargetOpeningWeeks();
        Assert.assertEquals(2, openingWeeks.size());

        final TargetOpeningWeekData firstWeek = openingWeeks.get(0);
        Assert.assertEquals(startOfFirstWeek, firstWeek.getStartDate());
        Assert.assertEquals(endOfFirstWeek, firstWeek.getEndDate());

        Assert.assertEquals(createFormattedDateString(startOfFirstWeek), firstWeek.getFormattedStartDate());
        Assert.assertEquals(createFormattedDateString(endOfFirstWeek), firstWeek.getFormattedEndDate());
        validateOpeningDayList(firstWeek.getTargetOpeningDayList(), startOfFirstWeek);

        final TargetOpeningWeekData secondWeek = openingWeeks.get(1);
        Assert.assertEquals(startOfSecondWeek, secondWeek.getStartDate());
        Assert.assertEquals(endOfSecondWeek, secondWeek.getEndDate());
        Assert.assertEquals(createFormattedDateString(startOfSecondWeek), secondWeek.getFormattedStartDate());
        Assert.assertEquals(createFormattedDateString(endOfSecondWeek), secondWeek.getFormattedEndDate());
        validateOpeningDayList(secondWeek.getTargetOpeningDayList(), startOfSecondWeek);
    }

    @Test
    public void testPopulateWithOldData() {
        final LocalDate startOfFirstWeek = LocalDate.now().withDayOfWeek(DateTimeConstants.MONDAY);
        final int numberOfDays = 14;

        final List<OpeningDayModel> mockOpeningDayModels = createMockOpeningDayModels(numberOfDays);
        final List<TargetOpeningDayData> mockOpeningDayData = createMockOpeningDays(startOfFirstWeek.minusDays(14),
                numberOfDays);

        setUpMockTargetOpeningDayConverter(mockOpeningDayModels, mockOpeningDayData);

        final OpeningScheduleModel mockOpeningScheduleModel = mock(OpeningScheduleModel.class,
                Mockito.RETURNS_DEFAULTS);
        given(mockOpeningScheduleModel.getOpeningDays()).willReturn(mockOpeningDayModels);

        final TargetOpeningScheduleData result = new TargetOpeningScheduleData();
        mockTargetOpeningScheduleConverter.populate(mockOpeningScheduleModel, result);


        Assert.assertTrue(result.getTargetOpeningWeeks().isEmpty());
    }

    @Test
    public void testPopulateWithIncorrectOpeningDaysType() {
        final OpeningDayModel openingDay = mock(OpeningDayModel.class);

        final OpeningScheduleModel mockOpeningScheduleModel = mock(OpeningScheduleModel.class,
                Mockito.RETURNS_DEFAULTS);
        given(mockOpeningScheduleModel.getOpeningDays()).willReturn(Collections.singletonList(openingDay));

        final TargetOpeningScheduleData mockTargetOpeningScheduleData = mock(TargetOpeningScheduleData.class);

        mockTargetOpeningScheduleConverter.populate(mockOpeningScheduleModel, mockTargetOpeningScheduleData);

        verify(mockOpeningScheduleModel).getOpeningDays();
        verifyZeroInteractions(mockTargetOpeningDayConverter);

    }

    private void validateOpeningDayList(final List<TargetOpeningDayData> openingDayList, final LocalDate startOfWeek) {
        Assert.assertEquals(7, openingDayList.size());

        for (int i = 0; i < openingDayList.size(); i++) {
            final TargetOpeningDayData openingDay = openingDayList.get(i);

            Assert.assertEquals(openingDay.getOpeningDateTime().toLocalDate(), startOfWeek.plusDays(i));
        }
    }

    private void setUpMockTargetOpeningDayConverter(final List<OpeningDayModel> mockOpeningDayModels,
            final List<TargetOpeningDayData> mockOpeningDayData) {
        for (int i = 0; i < mockOpeningDayModels.size(); i++) {
            given(mockTargetOpeningDayConverter.convert((TargetOpeningDayModel)mockOpeningDayModels.get(i)))
                    .willReturn(mockOpeningDayData.get(i));
        }
    }

    private List<OpeningDayModel> createMockOpeningDayModels(final int numberOfElements) {
        final List<OpeningDayModel> mockOpeningDays = new ArrayList<>();

        for (int i = 0; i < numberOfElements; i++) {
            final TargetOpeningDayModel mockTargetOpeningDay = mock(TargetOpeningDayModel.class);
            mockOpeningDays.add(mockTargetOpeningDay);
        }

        return mockOpeningDays;
    }

    private List<TargetOpeningDayData> createMockOpeningDays(final LocalDate startDate, final int numberOfDays) {
        final List<TargetOpeningDayData> mockOpeningDays = new ArrayList<>();

        for (int i = 0; i < numberOfDays; i++) {
            final TargetOpeningDayData mockOpeningDay = mock(TargetOpeningDayData.class);

            final LocalTime openingTime = new LocalTime(8, 0, 0);
            final DateTime openingDateTime = startDate.plusDays(i).toDateTime(openingTime);
            given(mockOpeningDay.getOpeningDateTime()).willReturn(openingDateTime);

            final LocalTime closingTime = new LocalTime(17, 0, 0);
            final DateTime closingDateTime = startDate.plusDays(i).toDateTime(closingTime);
            given(mockOpeningDay.getClosingDateTime()).willReturn(closingDateTime);

            mockOpeningDays.add(mockOpeningDay);
        }

        return mockOpeningDays;
    }

    private String createFormattedDateString(final LocalDate date) {
        final StringBuilder formattedDateString = new StringBuilder();

        formattedDateString.append(date.dayOfWeek().getAsShortText());
        formattedDateString.append(", ");
        formattedDateString.append(date.getDayOfMonth());
        formattedDateString.append(' ');
        formattedDateString.append(date.monthOfYear().getAsShortText());

        return formattedDateString.toString();
    }
}

package au.com.target.tgtfacades.storelocator.converters.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.storelocator.model.OpeningDayModel;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import au.com.target.tgtcore.model.TargetOpeningDayModel;
import au.com.target.tgtfacades.storelocator.data.TargetOpeningDayData;
import au.com.target.tgtfacades.storelocator.data.TargetShortOpeningScheduleData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetShortOpeningSchedulePopulatorTest {

    @Mock
    private Converter<TargetOpeningDayModel, TargetOpeningDayData> targetOpeningDayConverter;

    @InjectMocks
    private final TargetShortOpeningSchedulePopulator targetShortOpeningSchedulePopulator = new TargetShortOpeningSchedulePopulator();

    @Before
    public void setUp() {
        targetShortOpeningSchedulePopulator.setNumberOfDays(3);
    }

    @Test
    public void testPopulateWithFirstDateToday() throws Exception {
        final List<OpeningDayModel> openingDays = new ArrayList<>();

        for (int i = 0; i < 7; i++) {
            final TargetOpeningDayModel mockOpeningDay = mock(TargetOpeningDayModel.class);
            given(mockOpeningDay.getOpeningTime()).willReturn(DateTime.now().plusDays(i).withTime(8, 0, 0, 0).toDate());
            given(mockOpeningDay.getClosingTime())
                    .willReturn(DateTime.now().plusDays(i).withTime(22, 0, 0, 0).toDate());

            openingDays.add(mockOpeningDay);
        }

        final OpeningScheduleModel mockOpeningSchedule = mock(OpeningScheduleModel.class);
        given(mockOpeningSchedule.getOpeningDays()).willReturn(openingDays);

        given(targetOpeningDayConverter.convert(any(TargetOpeningDayModel.class)))
                .willAnswer(createTargetOpeningDayDataAnswer());

        final TargetShortOpeningScheduleData data = new TargetShortOpeningScheduleData();
        targetShortOpeningSchedulePopulator.populate(mockOpeningSchedule, data);

        assertThat(data.getOpeningDays()).hasSize(3);

        assertThat(data.getOpeningDays().get(0).getOpeningDateTime()).isEqualTo(DateTime.now().withTime(8, 0, 0, 0));
        assertThat(data.getOpeningDays().get(1).getOpeningDateTime())
                .isEqualTo(DateTime.now().plusDays(1).withTime(8, 0, 0, 0));
        assertThat(data.getOpeningDays().get(2).getOpeningDateTime())
                .isEqualTo(DateTime.now().plusDays(2).withTime(8, 0, 0, 0));
    }

    @Test
    public void testPopulateWithTodayMidWeek() throws Exception {
        final List<OpeningDayModel> openingDays = new ArrayList<>();

        for (int i = -3; i < 4; i++) {
            final TargetOpeningDayModel mockOpeningDay = mock(TargetOpeningDayModel.class);
            given(mockOpeningDay.getOpeningTime()).willReturn(DateTime.now().plusDays(i).withTime(8, 0, 0, 0).toDate());
            given(mockOpeningDay.getClosingTime())
                    .willReturn(DateTime.now().plusDays(i).withTime(22, 0, 0, 0).toDate());

            openingDays.add(mockOpeningDay);
        }

        final OpeningScheduleModel mockOpeningSchedule = mock(OpeningScheduleModel.class);
        given(mockOpeningSchedule.getOpeningDays()).willReturn(openingDays);

        given(targetOpeningDayConverter.convert(any(TargetOpeningDayModel.class)))
                .willAnswer(createTargetOpeningDayDataAnswer());

        final TargetShortOpeningScheduleData data = new TargetShortOpeningScheduleData();
        targetShortOpeningSchedulePopulator.populate(mockOpeningSchedule, data);

        assertThat(data.getOpeningDays()).hasSize(3);

        assertThat(data.getOpeningDays().get(0).getOpeningDateTime()).isEqualTo(DateTime.now().withTime(8, 0, 0, 0));
        assertThat(data.getOpeningDays().get(1).getOpeningDateTime())
                .isEqualTo(DateTime.now().plusDays(1).withTime(8, 0, 0, 0));
        assertThat(data.getOpeningDays().get(2).getOpeningDateTime())
                .isEqualTo(DateTime.now().plusDays(2).withTime(8, 0, 0, 0));
    }

    @Test
    public void testPopulateWithTodayLastDayNotEnoughData() throws Exception {
        final List<OpeningDayModel> openingDays = new ArrayList<>();

        for (int i = -6; i < 1; i++) {
            final TargetOpeningDayModel mockOpeningDay = mock(TargetOpeningDayModel.class);
            given(mockOpeningDay.getOpeningTime()).willReturn(DateTime.now().plusDays(i).withTime(8, 0, 0, 0).toDate());
            given(mockOpeningDay.getClosingTime())
                    .willReturn(DateTime.now().plusDays(i).withTime(22, 0, 0, 0).toDate());

            openingDays.add(mockOpeningDay);
        }

        final OpeningScheduleModel mockOpeningSchedule = mock(OpeningScheduleModel.class);
        given(mockOpeningSchedule.getOpeningDays()).willReturn(openingDays);

        given(targetOpeningDayConverter.convert(any(TargetOpeningDayModel.class)))
                .willAnswer(createTargetOpeningDayDataAnswer());

        final TargetShortOpeningScheduleData data = new TargetShortOpeningScheduleData();
        targetShortOpeningSchedulePopulator.populate(mockOpeningSchedule, data);

        assertThat(data.getOpeningDays()).hasSize(1);

        assertThat(data.getOpeningDays().get(0).getOpeningDateTime()).isEqualTo(DateTime.now().withTime(8, 0, 0, 0));
    }

    @Test
    public void testPopulateWithAllStaleData() throws Exception {
        final List<OpeningDayModel> openingDays = new ArrayList<>();

        for (int i = -8; i < -1; i++) {
            final TargetOpeningDayModel mockOpeningDay = mock(TargetOpeningDayModel.class);
            given(mockOpeningDay.getOpeningTime()).willReturn(DateTime.now().plusDays(i).withTime(8, 0, 0, 0).toDate());
            given(mockOpeningDay.getClosingTime())
                    .willReturn(DateTime.now().plusDays(i).withTime(22, 0, 0, 0).toDate());

            openingDays.add(mockOpeningDay);
        }

        final OpeningScheduleModel mockOpeningSchedule = mock(OpeningScheduleModel.class);
        given(mockOpeningSchedule.getOpeningDays()).willReturn(openingDays);

        given(targetOpeningDayConverter.convert(any(TargetOpeningDayModel.class)))
                .willAnswer(createTargetOpeningDayDataAnswer());

        final TargetShortOpeningScheduleData data = new TargetShortOpeningScheduleData();
        targetShortOpeningSchedulePopulator.populate(mockOpeningSchedule, data);

        assertThat(data.getOpeningDays()).isNull();
    }

    private Answer<TargetOpeningDayData> createTargetOpeningDayDataAnswer() {
        return new Answer<TargetOpeningDayData>() {

            @Override
            public TargetOpeningDayData answer(final InvocationOnMock invocation) throws Throwable {
                final TargetOpeningDayData data = new TargetOpeningDayData();

                data.setOpeningDateTime(
                        new DateTime(((TargetOpeningDayModel)invocation.getArguments()[0]).getOpeningTime()));

                return data;
            }

        };
    }
}

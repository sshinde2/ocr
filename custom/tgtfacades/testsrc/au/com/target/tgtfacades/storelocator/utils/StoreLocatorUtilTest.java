/**
 * 
 */
package au.com.target.tgtfacades.storelocator.utils;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.storelocator.data.OpeningScheduleData;
import de.hybris.platform.commercefacades.storelocator.data.TimeData;
import de.hybris.platform.commercefacades.storelocator.data.WeekdayOpeningDayData;
import de.hybris.platform.cronjob.enums.DayOfWeek;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;


@UnitTest
public class StoreLocatorUtilTest {

    @Test
    public void testGetFormattedStoreOpeningScheduleClosed() {

        final Map<String, String> returnSchdule = StoreLocatorUtil
                .getFormattedStoreOpeningSchedule(getTargetPointOfServiceData(true));
        Assert.assertTrue(returnSchdule.entrySet().iterator().next().getKey().contains("MONDAY"));
        Assert.assertTrue(returnSchdule.entrySet().iterator().next().getValue().contains("Closed"));
    }

    @Test
    public void testGetFormattedStoreOpeningSchedule() {
        final Map<String, String> returnSchdule = StoreLocatorUtil
                .getFormattedStoreOpeningSchedule(getTargetPointOfServiceData(false));
        Assert.assertTrue(returnSchdule.entrySet().iterator().next().getKey().contains("MONDAY"));
        Assert.assertTrue(!returnSchdule.entrySet().iterator().next().getValue().contains("Closed"));
        Assert.assertTrue(returnSchdule.entrySet().iterator().next().getValue().contains("9:00 am - 9:00 pm"));
    }

    private TargetPointOfServiceData getTargetPointOfServiceData(final boolean isClosed) {
        final TargetPointOfServiceData pointOfService = new TargetPointOfServiceData();
        final OpeningScheduleData openingScheduleData = new OpeningScheduleData();

        final WeekdayOpeningDayData weekdayOpeningDayData = new WeekdayOpeningDayData();
        weekdayOpeningDayData.setClosed(isClosed);
        weekdayOpeningDayData.setWeekDay(DayOfWeek.MONDAY.toString());
        final TimeData open = new TimeData();
        open.setFormattedHour("9:00 am");
        weekdayOpeningDayData.setOpeningTime(open);

        final TimeData close = new TimeData();
        close.setFormattedHour("9:00 pm");
        weekdayOpeningDayData.setClosingTime(close);

        final List<WeekdayOpeningDayData> openingDays = new ArrayList<>();
        openingDays.add(weekdayOpeningDayData);
        openingScheduleData.setWeekDayOpeningList(openingDays);
        pointOfService.setOpeningHours(openingScheduleData);

        return pointOfService;
    }
}

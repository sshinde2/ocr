/**
 * 
 */
package au.com.target.tgtfacades.url.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtfacades.url.transform.TargetUrlTokenTransformer;
import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;

import com.endeca.infront.cartridge.model.DimensionSearchValue;
import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;
import com.google.common.collect.ImmutableList;


/**
 * @author bhuang3
 *
 */
@UnitTest
public class TargetBrandDimensionSearchUrlResolverTest {

    @InjectMocks
    private final TargetBrandDimensionSearchUrlResolver resolver = new TargetBrandDimensionSearchUrlResolver();

    private final UrlTokenTransformer urlTokenTransformer = new TargetUrlTokenTransformer();


    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
        resolver.setUrlTokenTransformers(ImmutableList.of(urlTokenTransformer));
    }

    @Test
    public void testResolveInternal() {
        final DimensionSearchValue dimensionSearchValue = new DimensionSearchValue();
        dimensionSearchValue.setNavigationState("?NTT=testNavigation");
        dimensionSearchValue.setLabel("testbrandname");
        final String result = resolver.resolveInternal(dimensionSearchValue);
        Assert.assertEquals("/b/testbrandname?NTT=testNavigation", result);
    }

    @Test
    public void testResolveInternalWithNullDimensionSearchValue() {
        final DimensionSearchValue dimensionSearchValue = null;
        final String result = resolver.resolveInternal(dimensionSearchValue);
        Assert.assertEquals(StringUtils.EMPTY, result);
    }

    @Test
    public void testResolveInternalWithSpaceInBrandName() {
        final DimensionSearchValue dimensionSearchValue = new DimensionSearchValue();
        dimensionSearchValue.setNavigationState("?NTT=testNavigation");
        dimensionSearchValue.setLabel("test brand name");
        final String result = resolver.resolveInternal(dimensionSearchValue);
        Assert.assertEquals("/b/testbrandname?NTT=testNavigation", result);
    }

    @Test
    public void testResolveInternalWithSpaceAndCapitalInBrandName() {
        final DimensionSearchValue dimensionSearchValue = new DimensionSearchValue();
        dimensionSearchValue.setNavigationState("?NTT=testNavigation");
        dimensionSearchValue.setLabel("test Brand Name");
        final String result = resolver.resolveInternal(dimensionSearchValue);
        Assert.assertEquals("/b/testbrandname?NTT=testNavigation", result);
    }
}

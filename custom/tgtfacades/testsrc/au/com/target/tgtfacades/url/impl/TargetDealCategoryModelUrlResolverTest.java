/**
 * 
 */
package au.com.target.tgtfacades.url.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.deals.TargetDealService;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import au.com.target.tgtfacades.url.transform.TargetUrlTokenTransformer;
import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;


/**
 * @author bhuang3
 * 
 */
@UnitTest
public class TargetDealCategoryModelUrlResolverTest {


    @InjectMocks
    private final TargetDealCategoryModelUrlResolver resolver = new TargetDealCategoryModelUrlResolver();

    private final UrlTokenTransformer urlTokenTransformer = new TargetUrlTokenTransformer();

    @Mock
    private TargetDealService targetDealService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        resolver.setUrlTokenTransformers(ImmutableList.of(urlTokenTransformer));
    }

    @Test
    public void testResolveInternalWithNullModel() {
        final TargetDealCategoryModel targetDealCategoryModel = null;
        final String result = resolver.resolveInternal(targetDealCategoryModel);
        Assert.assertNull(result);
    }

    @Test
    public void testResolveInternalWithNullDealModel() {
        final AbstractSimpleDealModel abstractSimpleDealModel = null;
        final TargetDealCategoryModel targetDealCategoryModel = new TargetDealCategoryModel();
        targetDealCategoryModel.setCode("testCode");
        given(targetDealService.getDealByCategory(targetDealCategoryModel)).willReturn(abstractSimpleDealModel);
        final String result = resolver.resolveInternal(targetDealCategoryModel);
        Assert.assertNull(result);
    }

    @Test
    public void testResolveInternalWithOutShortTitle() {
        final AbstractSimpleDealModel abstractSimpleDealModel = mock(AbstractSimpleDealModel.class);
        final TargetDealCategoryModel targetDealCategoryModel = new TargetDealCategoryModel();
        targetDealCategoryModel.setCode("testCode");
        given(targetDealService.getDealByCategory(targetDealCategoryModel)).willReturn(abstractSimpleDealModel);
        final String result = resolver.resolveInternal(targetDealCategoryModel);
        Assert.assertEquals("/d/testCode", result);
    }

    @Test
    public void testResolveInternalWithShortTitleWithUpperCase() {
        final AbstractSimpleDealModel abstractSimpleDealModel = mock(AbstractSimpleDealModel.class);
        final TargetDealCategoryModel targetDealCategoryModel = new TargetDealCategoryModel();
        targetDealCategoryModel.setCode("testCode");
        given(targetDealService.getDealByCategory(targetDealCategoryModel)).willReturn(abstractSimpleDealModel);
        given(abstractSimpleDealModel.getFacetTitle()).willReturn("testFacetTitle");
        final String result = resolver.resolveInternal(targetDealCategoryModel);
        Assert.assertEquals("/d/testfacettitle/testCode", result);
    }

    @Test
    public void testResolveInternalWithShortTitleWithSpace() {
        final AbstractSimpleDealModel abstractSimpleDealModel = mock(AbstractSimpleDealModel.class);
        final TargetDealCategoryModel targetDealCategoryModel = new TargetDealCategoryModel();
        targetDealCategoryModel.setCode("testCode");
        given(targetDealService.getDealByCategory(targetDealCategoryModel)).willReturn(abstractSimpleDealModel);
        given(abstractSimpleDealModel.getFacetTitle()).willReturn("test facet title");
        final String result = resolver.resolveInternal(targetDealCategoryModel);
        Assert.assertEquals("/d/test-facet-title/testCode", result);
    }


}

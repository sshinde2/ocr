/**
 * 
 */
package au.com.target.tgtfacades.url.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.AddressModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;


/**
 * @author mjanarth
 * 
 */
@UnitTest
public class TargetPointOfServiceUrlResolverTest {



    @InjectMocks
    private final TargetPointOfServiceUrlResolver resolver = new TargetPointOfServiceUrlResolver();

    @Mock
    private UrlTokenTransformer urlTokenTransformer;
    @Mock
    private TargetPointOfServiceModel pointOfServiceModel;
    @Mock
    private AddressModel addressModel;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        resolver.setUrlTokenTransformers(ImmutableList.of(urlTokenTransformer));
        when(pointOfServiceModel.getStoreNumber()).thenReturn(Integer.valueOf(100));
        when(pointOfServiceModel.getAddress()).thenReturn(addressModel);
        when(pointOfServiceModel.getAddress().getDistrict()).thenReturn("NSW");

    }

    @Test
    public void testresolveUrlWithDot() {
        final String name = "Mt.Druitt";
        when(pointOfServiceModel.getName()).thenReturn(name);
        when(urlTokenTransformer.transform(name)).thenReturn("mt-druitt");
        final String url = resolver.resolveInternal(pointOfServiceModel);
        verify(urlTokenTransformer).transform("Mt.Druitt");
        Assert.assertTrue(url.contains("mt-druitt"));
    }


    @Test
    public void testresolveUrlWithSpace() {
        final String name = "Mt   Druitt";
        when(pointOfServiceModel.getName()).thenReturn(name);
        when(urlTokenTransformer.transform(name)).thenReturn("mt-druitt");
        final String url = resolver.resolveInternal(pointOfServiceModel);
        verify(urlTokenTransformer).transform(name);
        Assert.assertTrue(url.contains("mt-druitt"));
    }


    @Test
    public void testresolveUrlWithSpecialCharacters() {
        final String name = "Mt\\'&****Druitt";
        when(pointOfServiceModel.getName()).thenReturn(name);
        when(urlTokenTransformer.transform(name)).thenReturn("mt-druitt");
        final String url = resolver.resolveInternal(pointOfServiceModel);
        verify(urlTokenTransformer).transform(name);
        Assert.assertTrue(url.contains("mt-druitt"));
    }

    @Test
    public void testresolveUrlWithSpecialCharacters1() {
        final String name = "Mt<>{}|||+$##@@Druitt";
        when(pointOfServiceModel.getName()).thenReturn(name);
        when(urlTokenTransformer.transform(name)).thenReturn("mt-druitt");
        final String url = resolver.resolveInternal(pointOfServiceModel);
        verify(urlTokenTransformer).transform(name);
        Assert.assertTrue(url.contains("mt-druitt"));
    }
}

/**
 * 
 */
package au.com.target.tgtfacades.url.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.site.BaseSiteService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtfacades.url.transform.UrlTokenTransformer;




/**
 * @author pthoma20
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductDataUrlResolverTest {

    private static final String URL_PATTERN = "/p/{product-name}/{product-code}";

    @InjectMocks
    private final TargetProductDataUrlResolver resolver = new TargetProductDataUrlResolver();

    @Mock
    private CommerceCategoryService commerceCategoryService;
    @Mock
    private BaseSiteService baseSiteService;
    @Mock
    private UrlTokenTransformer urlTokenTransformer;


    /**
     * Initializes resolver by setting specific url pattern and applied transformers.
     */
    @Before
    public void setUp() {
        resolver.setDefaultPattern(URL_PATTERN);
        resolver.setBaseSiteService(baseSiteService);
        resolver.setCommerceCategoryService(commerceCategoryService);
        resolver.setUrlTokenTransformers(ImmutableList.of(urlTokenTransformer));

        given(baseSiteService.getCurrentBaseSite()).willReturn(null);
    }

    @Test
    public void testUrlResolve() {
        final String productName = "baby jackets";
        given(urlTokenTransformer.transform(productName)).willReturn(productName);
        final String resolvedUrl = resolver.resolve(productName, "P1060_Green");
        assertThat(resolvedUrl).isEqualTo("/p/baby-jackets/P1060_Green");
    }

    @Test
    public void testUrlResolveUppserCase() {
        final String productName = "Baby Jackets";
        given(urlTokenTransformer.transform(productName)).willReturn(productName);
        final String resolvedUrl = resolver.resolve(productName, "P1060_Green");
        assertThat(resolvedUrl).isEqualTo("/p/baby-jackets/P1060_Green");
    }

    @Test
    public void testUrlResolveSpecialChar() {
        final String productName = "baby+jackets";
        final String replacedProductName = "baby-jackets";
        given(urlTokenTransformer.transform(productName)).willReturn(replacedProductName);
        final String resolvedUrl = resolver.resolve(productName, "P1060_Green");
        assertThat(resolvedUrl).isEqualTo("/p/" + replacedProductName + "/P1060_Green");
    }

    @Test
    public void testUrlResolveEmpty() {
        final String productName = "";
        final String replacedProductName = "";
        final String resolvedUrl = resolver.resolve(productName, "P1060_Green");
        verify(urlTokenTransformer, never()).transform(productName);
        assertThat(resolvedUrl).isEqualTo("/p/" + replacedProductName + "/P1060_Green");
    }

}

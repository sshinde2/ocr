/**
 *
 */
package au.com.target.tgtfacades.user.converters.populator;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.user.AddressModel;

import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtutility.format.PhoneFormat;


/**
 * @author rmcalave
 *
 */
@UnitTest
public class TargetAddressPopulatorTest {

    private final TargetAddressPopulator targetAddressPopulator = new TargetAddressPopulator();

    @Test
    public void testPopulate() {
        final String pkString = "12345678";
        final String district = "VIC";
        final String phone = "+614123123123";

        final PK pk = PK.parse(pkString);

        final TargetAddressModel mockAddress = Mockito.mock(TargetAddressModel.class);
        BDDMockito.given(mockAddress.getPk()).willReturn(pk);
        BDDMockito.given(mockAddress.getDistrict()).willReturn(district);
        BDDMockito.given(mockAddress.getPhone1()).willReturn(phone);

        final TargetAddressData addressData = new TargetAddressData();

        targetAddressPopulator.populate(mockAddress, addressData);

        // Basic check to ensure the super class populate is performed.
        assertThat(addressData.getState(), equalTo(district));
        assertThat(addressData.getPhone(), equalTo(PhoneFormat.formatNumber(phone)));
    }

    @Test
    public void testPopulateWithNonTargetSource() {
        final String pkString = "12345678";

        final PK pk = PK.parse(pkString);

        final AddressModel mockAddress = Mockito.mock(AddressModel.class);
        BDDMockito.given(mockAddress.getPk()).willReturn(pk);

        final TargetAddressData addressData = new TargetAddressData();

        targetAddressPopulator.populate(mockAddress, addressData);

        // Basic check to ensure the super class populate is performed.
        assertThat(addressData.getState(), nullValue());
    }

    @Test
    public void testPopulateWithNonTargetTarget() {

        final TargetAddressModel mockAddress = Mockito.mock(TargetAddressModel.class);

        final TargetAddressData addressData = new TargetAddressData();

        targetAddressPopulator.populate(mockAddress, addressData);

        Mockito.verify(mockAddress, Mockito.never()).getFirstname();
        Mockito.verify(mockAddress, Mockito.never()).getLastname();
    }

    @Test(expected = NullPointerException.class)
    public void testPopulateWithNullSource() {
        final TargetAddressData addressData = new TargetAddressData();

        targetAddressPopulator.populate(null, addressData);
    }

    @Test(expected = NullPointerException.class)
    public void testPopulateWithNullTarget() {
        final TargetAddressModel mockAddress = Mockito.mock(TargetAddressModel.class);

        targetAddressPopulator.populate(mockAddress, null);
    }
}

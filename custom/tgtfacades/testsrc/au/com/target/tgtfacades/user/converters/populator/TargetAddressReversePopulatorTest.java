/**
 * 
 */
package au.com.target.tgtfacades.user.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;

import org.junit.Test;
import org.mockito.Mockito;

import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtfacades.user.data.TargetAddressData;


/**
 * @author rmcalave
 * 
 */
@UnitTest
public class TargetAddressReversePopulatorTest {
    private final TargetAddressReversePopulator targetAddressReversePopulator = new TargetAddressReversePopulator();

    @Test
    public void testPopulate() {
        final String firstName = "Marty";
        final String state = "VIC";

        final TargetAddressData addressData = new TargetAddressData();
        addressData.setFirstName(firstName);
        addressData.setState(state);

        final TargetAddressModel mockAddress = Mockito.mock(TargetAddressModel.class);

        targetAddressReversePopulator.populate(addressData, mockAddress);

        Mockito.verify(mockAddress).setFirstname(firstName);
        Mockito.verify(mockAddress).setDistrict(state);
    }

    @Test
    public void testPopulateWithNonTargetSource() {
        final String firstName = "Marty";

        final AddressData addressData = new AddressData();
        addressData.setFirstName(firstName);

        final TargetAddressModel mockAddress = Mockito.mock(TargetAddressModel.class);

        targetAddressReversePopulator.populate(addressData, mockAddress);

        Mockito.verify(mockAddress).setFirstname(firstName);
        Mockito.verify(mockAddress, Mockito.never()).setDistrict(Mockito.anyString());
    }

    @Test
    public void testPopulateWithNonTargetTarget() {
        final String firstName = "Marty";

        final TargetAddressData addressData = new TargetAddressData();
        addressData.setFirstName(firstName);

        final AddressModel mockAddress = Mockito.mock(AddressModel.class);

        targetAddressReversePopulator.populate(addressData, mockAddress);

        Mockito.verify(mockAddress).setFirstname(firstName);
        Mockito.verify(mockAddress, Mockito.never()).setDistrict(Mockito.anyString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPopulateWithNullSource() {
        final TargetAddressModel mockAddress = Mockito.mock(TargetAddressModel.class);

        targetAddressReversePopulator.populate(null, mockAddress);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPopulateWithNullTarget() {
        final TargetAddressData addressData = new TargetAddressData();

        targetAddressReversePopulator.populate(addressData, null);
    }
}

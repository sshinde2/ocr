/**
 * 
 */
package au.com.target.tgtfacades.user.converters.populator;


import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.storesession.data.CurrencyData;
import de.hybris.platform.commercefacades.storesession.data.LanguageData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.user.data.TargetCustomerData;


/**
 * @author ayushman
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCustomerPopulatorTest {

    @InjectMocks
    private final TargetCustomerPopulator testTargetCustomerPopulator = new TargetCustomerPopulator();

    @Mock
    private Converter<CurrencyModel, CurrencyData> currencyConverter;
    @Mock
    private Converter<LanguageModel, LanguageData> languageConverter;
    @Mock
    private Converter<AddressModel, AddressData> addressConverter;
    @Mock
    private EmployeeModel mockEmployee;
    @Mock
    private AddressModel mockAddress;
    @Mock
    private AddressData mockAddressData;
    @Mock
    private LanguageModel mockLanguageModel;
    @Mock
    private LanguageData mockLanguageData;
    @Mock
    private CurrencyModel mockCurrencyModel;
    @Mock
    private CurrencyData mockCurrenceyData;
    @Mock
    private TargetCustomerModel mockTargetCustomer;
    @Mock
    private TitleModel mockTitle;
    @Mock
    private CustomerModel mockCustomer;

    @Test(expected = IllegalArgumentException.class)
    public void testPopulateForNull() {
        testTargetCustomerPopulator.populate(mockTargetCustomer, null);
        testTargetCustomerPopulator.populate(mockEmployee, null);
    }

    @Test
    public void testPopulateAllForTargetCustomer() {
        final String pkString = "12345678";
        final PK pk = PK.parse(pkString);

        testTargetCustomerPopulator.setCurrencyConverter(currencyConverter);
        testTargetCustomerPopulator.setAddressConverter(addressConverter);
        testTargetCustomerPopulator.setLanguageConverter(languageConverter);

        given(mockTargetCustomer.getPk()).willReturn(pk);
        given(mockTargetCustomer.getUid()).willReturn("userUid");
        given(mockTargetCustomer.getDefaultPaymentAddress()).willReturn(mockAddress);
        given(mockTargetCustomer.getDefaultShipmentAddress()).willReturn(mockAddress);
        given(mockTargetCustomer.getSessionCurrency()).willReturn(mockCurrencyModel);
        given(mockTargetCustomer.getSessionLanguage()).willReturn(mockLanguageModel);
        given(mockTargetCustomer.getUid()).willReturn("userUid");
        given(mockTargetCustomer.getTrackingGuid()).willReturn("trackingGuid");
        given(mockTargetCustomer.getAllowTracking()).willReturn(Boolean.TRUE);
        given(mockTitle.getCode()).willReturn("titleCode");
        given(mockTargetCustomer.getTitle()).willReturn(mockTitle);
        given(mockTargetCustomer.getFirstname()).willReturn("testFirstName");
        given(mockTargetCustomer.getLastname()).willReturn("testLastName");

        given(currencyConverter.convert(mockTargetCustomer.getSessionCurrency())).willReturn(mockCurrenceyData);
        given(addressConverter.convert(mockTargetCustomer.getDefaultPaymentAddress())).willReturn(mockAddressData);
        given(languageConverter.convert(mockTargetCustomer.getSessionLanguage())).willReturn(mockLanguageData);

        final TargetCustomerData targetCustomerData = new TargetCustomerData();
        testTargetCustomerPopulator.populate(mockTargetCustomer, targetCustomerData);

        Assert.assertEquals("testFirstName", targetCustomerData.getFirstName());
        Assert.assertEquals("testLastName", targetCustomerData.getLastName());
        Assert.assertEquals("userUid", targetCustomerData.getUid());
        Assert.assertEquals(mockAddressData, targetCustomerData.getDefaultBillingAddress());
        Assert.assertEquals(mockAddressData, targetCustomerData.getDefaultShippingAddress());
        Assert.assertEquals(mockCurrenceyData, targetCustomerData.getCurrency());
        Assert.assertEquals("titleCode", targetCustomerData.getTitleCode());
        Assert.assertEquals("trackingGuid", targetCustomerData.getTrackingGuid());
    }

    @Test
    public void testPopulateAllForTargetEmployee() {
        final String pkString = "12345678";
        final PK pk = PK.parse(pkString);
        final String testUserName = "testFirstName testLastname";

        testTargetCustomerPopulator.setCurrencyConverter(currencyConverter);
        testTargetCustomerPopulator.setAddressConverter(addressConverter);
        testTargetCustomerPopulator.setLanguageConverter(languageConverter);

        given(mockEmployee.getPk()).willReturn(pk);
        given(mockEmployee.getUid()).willReturn("empId");
        given(mockEmployee.getName()).willReturn(testUserName);
        given(mockEmployee.getDefaultPaymentAddress()).willReturn(mockAddress);
        given(mockEmployee.getDefaultShipmentAddress()).willReturn(mockAddress);
        given(mockEmployee.getSessionCurrency()).willReturn(mockCurrencyModel);
        given(mockEmployee.getSessionLanguage()).willReturn(mockLanguageModel);
        given(currencyConverter.convert(mockEmployee.getSessionCurrency())).willReturn(mockCurrenceyData);
        given(addressConverter.convert(mockEmployee.getDefaultPaymentAddress())).willReturn(mockAddressData);
        given(languageConverter.convert(mockEmployee.getSessionLanguage())).willReturn(mockLanguageData);

        final TargetCustomerData targetCustomerData = new TargetCustomerData();
        testTargetCustomerPopulator.populate(mockEmployee, targetCustomerData);

        Assert.assertEquals("empId", targetCustomerData.getUid());
        Assert.assertEquals(testUserName, targetCustomerData.getName());
        Assert.assertEquals(mockAddressData, targetCustomerData.getDefaultBillingAddress());
        Assert.assertEquals(mockAddressData, targetCustomerData.getDefaultShippingAddress());
        Assert.assertEquals(mockCurrenceyData, targetCustomerData.getCurrency());
        Assert.assertEquals(mockLanguageData, targetCustomerData.getLanguage());

    }

    @Test
    public void testPopulateEssential() {
        given(mockTargetCustomer.getSessionCurrency()).willReturn(null);
        given(mockTargetCustomer.getDefaultPaymentAddress()).willReturn(null);
        given(mockTargetCustomer.getDefaultShipmentAddress()).willReturn(null);
        given(mockTargetCustomer.getSessionLanguage()).willReturn(null);
        given(mockTitle.getCode()).willReturn("titleCode");
        given(mockTargetCustomer.getUid()).willReturn("userUid");
        given(mockTargetCustomer.getName()).willReturn("testUserName");

        final TargetCustomerData targetCustomerData = new TargetCustomerData();
        testTargetCustomerPopulator.populate(mockTargetCustomer, targetCustomerData);

        Assert.assertNotNull(targetCustomerData);
        Assert.assertNull(targetCustomerData.getCurrency());
        Assert.assertNull(targetCustomerData.getDefaultBillingAddress());
        Assert.assertNull(targetCustomerData.getDefaultShippingAddress());
        Assert.assertNull(targetCustomerData.getLanguage());
        Assert.assertNull(targetCustomerData.getTitleCode());
        Assert.assertEquals("testUserName", targetCustomerData.getName());
        Assert.assertEquals("userUid", targetCustomerData.getUid());
    }

    @Test
    public void testPopulateForNonTargetCustomer() {
        final String pkString = "12345678";
        final PK pk = PK.parse(pkString);

        testTargetCustomerPopulator.setCurrencyConverter(currencyConverter);
        testTargetCustomerPopulator.setAddressConverter(addressConverter);
        testTargetCustomerPopulator.setLanguageConverter(languageConverter);

        given(mockCustomer.getPk()).willReturn(pk);
        given(mockCustomer.getUid()).willReturn("userUid");
        given(mockCustomer.getDefaultPaymentAddress()).willReturn(mockAddress);
        given(mockCustomer.getDefaultShipmentAddress()).willReturn(mockAddress);
        given(mockCustomer.getSessionCurrency()).willReturn(mockCurrencyModel);
        given(mockCustomer.getSessionLanguage()).willReturn(mockLanguageModel);
        given(mockCustomer.getUid()).willReturn("userUid");
        given(mockTitle.getCode()).willReturn("titleCode");
        given(mockCustomer.getTitle()).willReturn(mockTitle);
        given(mockTargetCustomer.getFirstname()).willReturn("testFirstName");
        given(mockTargetCustomer.getLastname()).willReturn("testLastName");
        given(mockTargetCustomer.getAllowTracking()).willReturn(Boolean.valueOf(true));

        given(currencyConverter.convert(mockCustomer.getSessionCurrency())).willReturn(mockCurrenceyData);
        given(addressConverter.convert(mockCustomer.getDefaultPaymentAddress())).willReturn(mockAddressData);
        given(languageConverter.convert(mockCustomer.getSessionLanguage())).willReturn(mockLanguageData);

        final TargetCustomerData targetCustomerData = new TargetCustomerData();
        testTargetCustomerPopulator.populate(mockCustomer, targetCustomerData);

        Assert.assertNull(targetCustomerData.getFirstName());
        Assert.assertNull(targetCustomerData.getLastName());
        Assert.assertEquals(Boolean.valueOf(false), Boolean.valueOf(targetCustomerData.isAllowTracking()));
        Assert.assertEquals("userUid", targetCustomerData.getUid());
        Assert.assertEquals(mockAddressData, targetCustomerData.getDefaultBillingAddress());
        Assert.assertEquals(mockAddressData, targetCustomerData.getDefaultShippingAddress());
        Assert.assertEquals(mockCurrenceyData, targetCustomerData.getCurrency());
        Assert.assertEquals("titleCode", targetCustomerData.getTitleCode());

    }

}

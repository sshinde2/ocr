package au.com.target.tgtfacades.util;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.commercefacades.product.data.PriceData;

import java.math.BigDecimal;

import org.junit.Test;


@UnitTest
public class PriceFormatUtilsTest {

    @Test
    public void testFormatPriceWithZeroCents() throws Exception {
        final String price = "$11.00";

        final String result = PriceFormatUtils.formatPrice(price);

        assertThat(result).isEqualTo("$11");
    }

    @Test
    public void testFormatPriceWithNonZeroCents() throws Exception {
        final String price = "$11.35";

        final String result = PriceFormatUtils.formatPrice(price);

        assertThat(result).isEqualTo(price);
    }

    @Test
    public void testFormatPriceDataZeroCents() {
        final String result = PriceFormatUtils.formatPrice(buildPriceData("11.00"));
        assertThat(result).isNotNull().isNotEmpty().isEqualTo("$11");
    }

    @Test
    public void testFormatPriceRangeData() {
        final String result = PriceFormatUtils.formatPrice(buildPriceRange("11.99", "39.99"));
        assertThat(result).isNotNull().isNotEmpty().isEqualTo("$11.99 - $39.99");
    }

    @Test
    public void testFormatPriceRangeDataZeroCents() {
        final String result = PriceFormatUtils.formatPrice(buildPriceRange("11.00", "39.00"));
        assertThat(result).isNotNull().isNotEmpty().isEqualTo("$11 - $39");
    }

    @Test
    public void testFormatPriceData() {
        final String result = PriceFormatUtils.formatPrice(buildPriceData("11.99"));
        assertThat(result).isNotNull().isNotEmpty().isEqualTo("$11.99");
    }

    public PriceRangeData buildPriceRange(final String min, final String max) {
        final PriceData minPrice = buildPriceData(min);
        final PriceData maxPrice = buildPriceData(max);

        final PriceRangeData priceRange = new PriceRangeData();
        priceRange.setMaxPrice(maxPrice);
        priceRange.setMinPrice(minPrice);

        return priceRange;
    }

    public PriceData buildPriceData(final String price) {
        final PriceData priceData = new PriceData();
        priceData.setValue(new BigDecimal(price));
        priceData.setFormattedValue("$" + price);
        return priceData;
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.util;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.storelocator.TargetGoogleMapTools;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceStockData;



/**
 * @author mjanarth
 *
 */
@UnitTest
public class TargetPointOfServiceHelperTest {

    @Mock
    private TargetGoogleMapTools targetGoogleMapTools;
    @Mock
    private SalesApplicationFacade salesApplicationFacade;
    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    private final TargetPointOfServiceData targetPointOfServiceData = new TargetPointOfServiceData();

    private final TargetPointOfServiceStockData targetPointOfServiceStockData = new TargetPointOfServiceStockData();

    @Mock
    private AddressData address;
    @Mock
    private CountryData countryData;


    @InjectMocks
    private final TargetPointOfServiceHelper helper = new TargetPointOfServiceHelper();


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        given(address.getCountry()).willReturn(countryData);
    }

    @Test
    public void doNotPopulateDirectionUrlIfFeatureIsDisabled() {
        doReturn(Boolean.FALSE).when(targetFeatureSwitchFacade).isFeatureEnabled(
                TgtCoreConstants.FeatureSwitch.SHOW_GOOGLE_DIRECTION_URL);
        doReturn(Boolean.FALSE).when(salesApplicationFacade).isKioskApplication();
        helper.populateDirectionUrl("", targetPointOfServiceStockData);
        assertThat(targetPointOfServiceStockData.getDirectionUrl()).isNull();
    }

    @Test
    public void doNotPopulateDirectionUrlForKiosk() {
        doReturn(Boolean.TRUE).when(salesApplicationFacade).isKioskApplication();
        doReturn(Boolean.TRUE).when(targetFeatureSwitchFacade).isFeatureEnabled(
                TgtCoreConstants.FeatureSwitch.SHOW_GOOGLE_DIRECTION_URL);
        helper.populateDirectionUrl("", targetPointOfServiceData);
        assertThat(targetPointOfServiceData.getDirectionUrl()).isNull();
    }

    @Test
    public void verifyDirectionUrlIsPopulatedWithStockData() {
        doReturn(Boolean.FALSE).when(salesApplicationFacade).isKioskApplication();
        doReturn(Boolean.TRUE).when(targetFeatureSwitchFacade).isFeatureEnabled(
                TgtCoreConstants.FeatureSwitch.SHOW_GOOGLE_DIRECTION_URL);
        given(targetGoogleMapTools.getDirectionUrl("test", "Target+Geelong+line1+Geelong+3025+Australia"))
                .willReturn("directionUrl");
        targetPointOfServiceStockData.setName("Geelong");
        given(address.getLine1()).willReturn("line1");
        given(address.getTown()).willReturn("Geelong");
        given(address.getPostalCode()).willReturn("3025");
        given(countryData.getName()).willReturn("Australia");
        targetPointOfServiceStockData.setAddress(address);
        helper.populateDirectionUrl("test", targetPointOfServiceStockData);
        assertThat(targetPointOfServiceStockData.getDirectionUrl()).isEqualTo("directionUrl");
    }

    @Test
    public void verifyDirectionUrlIsPopulatedWithNullLocationText() {
        doReturn(Boolean.FALSE).when(salesApplicationFacade).isKioskApplication();
        doReturn(Boolean.TRUE).when(targetFeatureSwitchFacade).isFeatureEnabled(
                TgtCoreConstants.FeatureSwitch.SHOW_GOOGLE_DIRECTION_URL);
        given(targetGoogleMapTools.getDirectionUrl("Current+Location", "Target+Geelong+line1+Geelong+3025+Australia"))
                .willReturn("directionUrl");
        targetPointOfServiceStockData.setName("Geelong");
        given(address.getLine1()).willReturn("line1");
        given(address.getTown()).willReturn("Geelong");
        given(address.getPostalCode()).willReturn("3025");
        given(countryData.getName()).willReturn("Australia");
        targetPointOfServiceStockData.setAddress(address);
        helper.populateDirectionUrl(null, targetPointOfServiceStockData);
        assertThat(targetPointOfServiceStockData.getDirectionUrl()).isEqualTo("directionUrl");
    }

    @Test
    public void verifyDirectionUrlIsPopulatedWithPointOfServiceData() {
        doReturn(Boolean.FALSE).when(salesApplicationFacade).isKioskApplication();
        doReturn(Boolean.TRUE).when(targetFeatureSwitchFacade).isFeatureEnabled(
                TgtCoreConstants.FeatureSwitch.SHOW_GOOGLE_DIRECTION_URL);
        given(targetGoogleMapTools.getDirectionUrl("Current+Location", "Target+Geelong+line1+Geelong+3025+Australia"))
                .willReturn("directionUrl");
        targetPointOfServiceData.setName("Geelong");
        given(address.getLine1()).willReturn("line1");
        given(address.getTown()).willReturn("Geelong");
        given(address.getPostalCode()).willReturn("3025");
        given(countryData.getName()).willReturn("Australia");
        targetPointOfServiceData.setAddress(address);
        helper.populateDirectionUrl("", targetPointOfServiceData);
        assertThat(targetPointOfServiceData.getDirectionUrl()).isEqualTo("directionUrl");
    }
}

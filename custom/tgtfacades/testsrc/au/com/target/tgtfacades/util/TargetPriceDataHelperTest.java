/**
 * 
 */
package au.com.target.tgtfacades.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author ayushman
 *
 */
@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class TargetPriceDataHelperTest {

    @Mock
    private CommonI18NService commonI18NService;

    @Mock
    private CurrencyModel currencyModel;

    @Mock
    private PriceDataFactory priceDataFactory;

    @Mock
    private PriceData priceData;

    @InjectMocks
    private final TargetPriceDataHelper helper = new TargetPriceDataHelper();

    @Before
    public void setup() {

        BDDMockito.when(commonI18NService.getCurrentCurrency()).thenReturn(currencyModel);
        BDDMockito.when(currencyModel.getIsocode()).thenReturn("$");
    }

    @Test
    public void testPopulatePriceDataWthNullPrice() {
        final PriceData data = helper.populatePriceData(null, PriceDataType.FROM);
        Assert.assertNull(data);
    }

    @Test
    public void testPopulatePriceDataWthNullPriceType() {
        final PriceData data = helper.populatePriceData(Double.valueOf(10.55), null);
        Assert.assertNull(data);
    }

    @Test
    public void testPopulatePriceDataWthValidPrice() {
        helper.populatePriceData(Double.valueOf(10.55), PriceDataType.FROM);
        Mockito.verify(priceDataFactory).create(PriceDataType.FROM, BigDecimal.valueOf(10.55), "$");
    }



}

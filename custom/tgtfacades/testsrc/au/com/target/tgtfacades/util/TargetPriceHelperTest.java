/**
 * 
 */
package au.com.target.tgtfacades.util;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.price.data.PriceRangeInformation;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPriceHelperTest {

    @Mock
    private CommonI18NService commonI18NService;

    @Mock
    private PriceDataFactory priceDataFactory;

    @InjectMocks
    private final TargetPriceHelper targetPriceHelper = new TargetPriceHelper();

    @Mock
    private CurrencyModel currencyModel;

    @Before
    public void setup() {
        targetPriceHelper.setCommonI18NService(commonI18NService);
        targetPriceHelper.setPriceDataFactory(priceDataFactory);
        given(commonI18NService.getCurrentCurrency()).willReturn(currencyModel);
        given(currencyModel.getIsocode()).willReturn("$");

    }


    public PriceData createPriceData(final PriceDataType type, final BigDecimal value, final String iso) {
        final PriceData priceData = new PriceData();
        priceData.setCurrencyIso(iso);
        priceData.setPriceType(type);
        priceData.setValue(value);
        return priceData;
    }

    /**
     * Test method for
     * {@link au.com.target.tgtfacades.util.TargetPriceHelper#populatePricesFromRangeInformation(au.com.target.tgtcore.price.data.PriceRangeInformation)}
     * .
     */
    @Test
    public void testPopulatePricesFromRangeInformationWhenPriceRangePresent() {

        final PriceRangeInformation priceRangeInfo = new PriceRangeInformation(Double.valueOf(10), Double.valueOf(20),
                Double.valueOf(15), Double.valueOf(25));

        final PriceData fromPrice = createPriceData(PriceDataType.BUY, BigDecimal.valueOf(10), "$");
        final PriceData toPrice = createPriceData(PriceDataType.BUY, BigDecimal.valueOf(20), "$");
        final PriceData fromWasPrice = createPriceData(PriceDataType.BUY, BigDecimal.valueOf(15), "$");
        final PriceData toWasPrice = createPriceData(PriceDataType.BUY, BigDecimal.valueOf(25), "$");

        given(priceDataFactory.create(PriceDataType.BUY,
                BigDecimal.valueOf(priceRangeInfo.getFromPrice().doubleValue()), "$")).willReturn(fromPrice);
        given(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(priceRangeInfo.getToPrice().doubleValue()),
                "$")).willReturn(toPrice);
        given(priceDataFactory.create(PriceDataType.BUY,
                BigDecimal.valueOf(priceRangeInfo.getFromWasPrice().doubleValue()), "$")).willReturn(fromWasPrice);
        given(priceDataFactory.create(PriceDataType.BUY,
                BigDecimal.valueOf(priceRangeInfo.getToWasPrice().doubleValue()), "$")).willReturn(toWasPrice);

        final TargetPriceInformation targetPriceInformation = targetPriceHelper
                .populatePricesFromRangeInformation(priceRangeInfo);

        assertThat(targetPriceInformation.getPriceRangeData().getMinPrice().getValue().doubleValue())
                .isEqualTo(priceRangeInfo.getFromPrice().doubleValue());
        assertThat(targetPriceInformation.getPriceRangeData()
                .getMaxPrice().getValue().doubleValue()).isEqualTo(priceRangeInfo.getToPrice().doubleValue());
        assertThat(targetPriceInformation.getWasPriceRangeData().getMinPrice().getValue().doubleValue())
                .isEqualTo(priceRangeInfo.getFromWasPrice().doubleValue());
        assertThat(targetPriceInformation.getWasPriceRangeData()
                .getMaxPrice().getValue().doubleValue()).isEqualTo(priceRangeInfo.getToWasPrice().doubleValue());
    }


    /**
     * Test method for
     * {@link au.com.target.tgtfacades.util.TargetPriceHelper#populatePricesFromRangeInformation(au.com.target.tgtcore.price.data.PriceRangeInformation)}
     * .
     */
    @Test
    public void testPopulatePricesFromRangeInformationWhenPriceRangeIsNotPresent() {

        final PriceRangeInformation priceRangeInfo = new PriceRangeInformation(Double.valueOf(20), Double.valueOf(20),
                Double.valueOf(25), Double.valueOf(25));

        final PriceData fromPrice = createPriceData(PriceDataType.BUY, BigDecimal.valueOf(20), "$");
        final PriceData toPrice = createPriceData(PriceDataType.BUY, BigDecimal.valueOf(20), "$");
        final PriceData fromWasPrice = createPriceData(PriceDataType.BUY, BigDecimal.valueOf(25), "$");
        final PriceData toWasPrice = createPriceData(PriceDataType.BUY, BigDecimal.valueOf(25), "$");

        given(priceDataFactory.create(PriceDataType.BUY,
                BigDecimal.valueOf(priceRangeInfo.getFromPrice().doubleValue()), "$")).willReturn(fromPrice);
        given(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(priceRangeInfo.getToPrice().doubleValue()),
                "$")).willReturn(toPrice);
        given(priceDataFactory.create(PriceDataType.BUY,
                BigDecimal.valueOf(priceRangeInfo.getFromWasPrice().doubleValue()), "$")).willReturn(fromWasPrice);
        given(priceDataFactory.create(PriceDataType.BUY,
                BigDecimal.valueOf(priceRangeInfo.getToWasPrice().doubleValue()), "$")).willReturn(toWasPrice);

        final TargetPriceInformation targetPriceInformation = targetPriceHelper
                .populatePricesFromRangeInformation(priceRangeInfo);

        assertThat(targetPriceInformation.getPriceRangeData()).isNull();
        assertThat(targetPriceInformation.getWasPriceRangeData()).isNull();
        assertThat(targetPriceInformation.getPriceData().getValue()).isEqualTo(BigDecimal.valueOf(20));
        assertThat(targetPriceInformation.getWasPriceData().getValue()).isEqualTo(BigDecimal.valueOf(25));
    }

    /**
     * Test method for
     * {@link au.com.target.tgtfacades.util.TargetPriceHelper#createFormattedPriceRange(de.hybris.platform.acceleratorfacades.order.data.PriceRangeData)}
     * .
     */
    @Test
    public void testCreateFormattedPriceRange() {
        final PriceRangeData priceRangeData = new PriceRangeData();
        final PriceData maxPriceData = Mockito.mock(PriceData.class);
        given(maxPriceData.getFormattedValue()).willReturn("$15");
        final PriceData minPriceData = Mockito.mock(PriceData.class);
        given(minPriceData.getFormattedValue()).willReturn("$12");
        priceRangeData.setMaxPrice(maxPriceData);
        priceRangeData.setMinPrice(minPriceData);
        final String formattedRangePrice = targetPriceHelper.createFormattedPriceRange(priceRangeData);
        assertThat(formattedRangePrice).isEqualTo("$12 - $15");

    }

    /**
     * Test method for {@link au.com.target.tgtfacades.util.TargetPriceHelper#createPriceData(java.lang.Double)}.
     */
    @Test
    public void testCreatePriceData() {
        final PriceData priceData = Mockito.mock(PriceData.class);
        given(priceData.getFormattedValue()).willReturn("$15");
        final Double price = Double.valueOf(15);
        given(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(price.doubleValue()), "$"))
                .willReturn(priceData);

        final PriceData priceDataResult = targetPriceHelper.createPriceData(price);
        assertThat(priceDataResult.getFormattedValue()).isEqualTo("$15");
    }

    @Test
    public void testCreatePriceDataBigDecimal() {
        final PriceData priceData = Mockito.mock(PriceData.class);
        given(priceData.getFormattedValue()).willReturn("$15");
        final BigDecimal price = BigDecimal.valueOf(15);
        given(priceDataFactory.create(PriceDataType.BUY, price, "$")).willReturn(priceData);

        final PriceData priceDataResult = targetPriceHelper.createPriceData(price);
        assertThat(priceDataResult.getFormattedValue()).isEqualTo("$15");
    }

    /**
     * Test method for
     * {@link au.com.target.tgtfacades.util.TargetPriceHelper#calculateAfterpayInstallmentPrice(de.hybris.platform.commercefacades.product.data.PriceData)}
     */
    @Test
    public void testCalculateAfterpayInstallmentPrice() {
        final BigDecimal price = BigDecimal.valueOf(20);
        final PriceData priceData = createPriceData(PriceDataType.BUY, price, "$");
        given(priceDataFactory.create(any(PriceDataType.class), any(BigDecimal.class), Mockito.anyString()))
                .willReturn(null);
        targetPriceHelper.calculateAfterpayInstallmentPrice(priceData);
        final ArgumentCaptor<BigDecimal> priceCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        verify(priceDataFactory).create(any(PriceDataType.class), priceCaptor.capture(), Mockito.anyString());
        assertThat(priceCaptor.getValue().doubleValue()).isEqualTo(5);
    }

    @Test
    public void testCalculateAfterpayInstallmentPriceRoundingUp() {
        final BigDecimal price = BigDecimal.valueOf(20.03);
        final PriceData priceData = createPriceData(PriceDataType.BUY, price, "$");
        given(priceDataFactory.create(any(PriceDataType.class), any(BigDecimal.class), Mockito.anyString()))
                .willReturn(null);
        targetPriceHelper.calculateAfterpayInstallmentPrice(priceData);
        final ArgumentCaptor<BigDecimal> priceCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        verify(priceDataFactory).create(any(PriceDataType.class), priceCaptor.capture(), Mockito.anyString());
        assertThat(priceCaptor.getValue().doubleValue()).isEqualTo(5.01);
    }

    @Test
    public void testCalculateAfterpayInstallmentPriceRoundingDown() {
        final BigDecimal price = BigDecimal.valueOf(20.01);
        final PriceData priceData = createPriceData(PriceDataType.BUY, price, "$");
        given(priceDataFactory.create(any(PriceDataType.class), any(BigDecimal.class), Mockito.anyString()))
                .willReturn(null);
        targetPriceHelper.calculateAfterpayInstallmentPrice(priceData);
        final ArgumentCaptor<BigDecimal> priceCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        verify(priceDataFactory).create(any(PriceDataType.class), priceCaptor.capture(), Mockito.anyString());
        assertThat(priceCaptor.getValue().doubleValue()).isEqualTo(5.00);
    }

    @Test
    public void testCalculateAfterpayInstallmentPriceRoundingHalfGoesUp() {
        final BigDecimal price = BigDecimal.valueOf(20.02);
        final PriceData priceData = createPriceData(PriceDataType.BUY, price, "$");
        given(priceDataFactory.create(any(PriceDataType.class), any(BigDecimal.class), Mockito.anyString()))
                .willReturn(null);
        targetPriceHelper.calculateAfterpayInstallmentPrice(priceData);
        final ArgumentCaptor<BigDecimal> priceCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        verify(priceDataFactory).create(any(PriceDataType.class), priceCaptor.capture(), Mockito.anyString());
        assertThat(priceCaptor.getValue().doubleValue()).isEqualTo(5.01);
    }
}

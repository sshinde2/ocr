/**
 * 
 */
package au.com.target.tgtfacades.util;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantTypeModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.jalo.TargetColourVariantProduct;
import au.com.target.tgtcore.jalo.TargetSizeVariantProduct;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;


/**
 * @author Rahul
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductDataHelperTest {
    @Mock
    private TargetColourVariantProductModel colourVariant;

    @Mock
    private TargetSizeVariantProductModel sizeVariant;

    @Mock
    private ProductModel product;

    @Mock
    private VariantTypeModel variant;

    @Mock
    private TargetProductListerData listerData;

    @Mock
    private TargetVariantProductListerData colourVariant1;

    @Mock
    private TargetVariantProductListerData colourVariant2;

    @Mock
    private TargetVariantProductListerData sizeVariant1;

    @Mock
    private TargetVariantProductListerData sizeVariant2;

    @Mock
    private TargetVariantProductListerData sizeVariant3;

    @Mock
    private TargetVariantProductListerData sizeVariant4;

    private List<String> variantCodes;

    private final List<TargetVariantProductListerData> colourVariants = new ArrayList<TargetVariantProductListerData>();

    private final List<TargetVariantProductListerData> sizeVariantList1 = new ArrayList<TargetVariantProductListerData>();

    private final List<TargetVariantProductListerData> sizeVariantList2 = new ArrayList<TargetVariantProductListerData>();



    @Before
    public void setUp() {
        given(colourVariant1.getCode()).willReturn("CP2001");
        given(colourVariant2.getCode()).willReturn("CP2002");
        given(sizeVariant1.getCode()).willReturn("SIZE2001");
        given(sizeVariant2.getCode()).willReturn("SIZE2002");
        given(sizeVariant3.getCode()).willReturn("SIZE2003");
        given(sizeVariant4.getCode()).willReturn("SIZE2004");

    }


    @Test
    public void testGetBaseProductForColourVaraint() {
        given(colourVariant.getBaseProduct()).willReturn(product);
        assertThat(product).isEqualTo(TargetProductDataHelper.getBaseProduct(colourVariant));
    }

    @Test
    public void testGetBaseProductForSizeVaraint() {
        given(sizeVariant.getBaseProduct()).willReturn(colourVariant);
        given(colourVariant.getBaseProduct()).willReturn(product);
        assertThat(product).isEqualTo(TargetProductDataHelper.getBaseProduct(sizeVariant));
    }

    @Test
    public void testGetBaseProductForNullProduct() {
        assertThat(TargetProductDataHelper.getBaseProduct(null)).isNull();
    }

    @Test
    public void testGetBaseProductForProductModel() {
        assertThat(product).isEqualTo(TargetProductDataHelper.getBaseProduct(product));
    }

    @Test
    public void testIsBaseProductForColourVariant() {
        given(product.getVariantType()).willReturn(variant);
        given(variant.getCode()).willReturn(TargetColourVariantProduct.class.getSimpleName());
        assertThat(TargetProductDataHelper.isBaseProduct(product)).isTrue();
    }

    @Test
    public void testIsBaseProductForSizeVariant() {
        given(product.getVariantType()).willReturn(variant);
        given(variant.getCode()).willReturn(TargetSizeVariantProduct.class.getSimpleName());
        assertThat(TargetProductDataHelper.isBaseProduct(product)).isFalse();
    }

    @Test
    public void testIsBaseProductForNullVariant() {
        given(product.getVariantType()).willReturn(null);
        assertThat(TargetProductDataHelper.isBaseProduct(product)).isFalse();
    }

    @Test
    public void testIsBaseProductForInvalidCode() {
        given(product.getVariantType()).willReturn(variant);
        given(variant.getCode()).willReturn("sample");
        assertThat(TargetProductDataHelper.isBaseProduct(product)).isFalse();
    }

    @Test
    public void testIsBaseProductForNullProductCode() {
        given(product.getVariantType()).willReturn(variant);
        given(variant.getCode()).willReturn(null);
        assertThat(TargetProductDataHelper.isBaseProduct(product)).isFalse();
    }

    @Test
    public void testPopulateSellableVariantsWithNull() {
        given(listerData.getTargetVariantProductListerData()).willReturn(null);
        variantCodes = TargetProductDataHelper.populateSellableVariantsFromListerData(listerData);
        assertThat(variantCodes).isEmpty();
    }

    @Test
    public void testPopulateSellableVariantsWithEmptyColourVariants() {
        given(listerData.getTargetVariantProductListerData()).willReturn(Collections.EMPTY_LIST);
        final List<String> variantCodes = TargetProductDataHelper.populateSellableVariantsFromListerData(listerData);
        assertThat(variantCodes).isEmpty();
    }

    @Test
    public void testPopulateSellableVariantsWithOnlyColourVariants() {
        colourVariants.add(colourVariant1);
        colourVariants.add(colourVariant2);
        given(listerData.getTargetVariantProductListerData()).willReturn(colourVariants);
        variantCodes = TargetProductDataHelper.populateSellableVariantsFromListerData(listerData);
        assertThat(variantCodes.size()).isEqualTo(2);
        assertThat(variantCodes).containsExactly("CP2001", "CP2002");
    }

    @Test
    public void testPopulateSellableVariantsWithColourVariantsAndSize() {
        colourVariants.add(colourVariant1);
        colourVariants.add(colourVariant2);
        sizeVariantList1.add(sizeVariant1);
        sizeVariantList1.add(sizeVariant2);
        sizeVariantList2.add(sizeVariant3);
        sizeVariantList2.add(sizeVariant4);
        given(listerData.getTargetVariantProductListerData()).willReturn(colourVariants);
        given(colourVariant1.getVariants()).willReturn(sizeVariantList1);
        given(colourVariant2.getVariants()).willReturn(sizeVariantList2);
        variantCodes = TargetProductDataHelper.populateSellableVariantsFromListerData(listerData);
        assertThat(variantCodes.size()).isEqualTo(4);
        assertThat(variantCodes).containsExactly("SIZE2001", "SIZE2002", "SIZE2003", "SIZE2004");
    }

    @Test
    public void testPopulateSellableVariantsWithOneColourAndMulitpleSizes() {
        colourVariants.add(colourVariant1);
        sizeVariantList1.add(sizeVariant1);
        sizeVariantList1.add(sizeVariant2);
        sizeVariantList1.add(sizeVariant3);
        given(colourVariant1.getVariants()).willReturn(sizeVariantList1);
        given(listerData.getTargetVariantProductListerData()).willReturn(colourVariants);
        variantCodes = TargetProductDataHelper.populateSellableVariantsFromListerData(listerData);
        assertThat(variantCodes.size()).isEqualTo(3);
        assertThat(variantCodes).containsExactly("SIZE2001", "SIZE2002", "SIZE2003");
    }

}

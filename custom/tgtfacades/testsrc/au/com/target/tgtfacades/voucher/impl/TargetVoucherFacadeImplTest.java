/**
 * 
 */
package au.com.target.tgtfacades.voucher.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.voucher.VoucherModelService;
import de.hybris.platform.voucher.model.DateRestrictionModel;
import de.hybris.platform.voucher.model.PromotionVoucherModel;
import de.hybris.platform.voucher.model.RestrictionModel;
import de.hybris.platform.voucher.model.VoucherModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.voucher.service.TargetVoucherService;
import au.com.target.tgtfacades.deals.data.TargetVouchersData;
import au.com.target.tgtfacades.offer.TargetMobileOfferHeadingFacade;
import au.com.target.tgtfacades.offers.data.TargetMobileOfferHeadingData;
import au.com.target.tgtfacades.voucher.TargetVoucherFacade;
import au.com.target.tgtfacades.voucher.data.TargetPromotionVoucherData;
import au.com.target.tgtmarketing.voucher.TargetMobileVoucherService;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetVoucherFacadeImplTest {


    @InjectMocks
    private final TargetVoucherFacade targetVoucherFacade = new TargetVoucherFacadeImpl();

    @Mock
    private TargetMobileVoucherService targetMobileVoucherService;

    @Mock
    private TargetMobileOfferHeadingFacade targetMobileOfferHeadingFacade;

    @Mock
    private Converter<PromotionVoucherModel, TargetPromotionVoucherData> targetPromotionVoucherConverter;

    @Mock
    private TargetVoucherService targetVoucherService;

    @Mock
    private VoucherModelService voucherModelService;

    @Mock
    private TimeService timeService;

    private List<TargetMobileOfferHeadingData> targetMobileOfferHeadingList;

    @Before
    public void setup() {
        final TargetMobileOfferHeadingData targetMobileOfferHeadingData1 = Mockito
                .mock(TargetMobileOfferHeadingData.class);
        final TargetMobileOfferHeadingData targetMobileOfferHeadingData2 = Mockito
                .mock(TargetMobileOfferHeadingData.class);
        targetMobileOfferHeadingList = new ArrayList<TargetMobileOfferHeadingData>(Arrays.asList(
                targetMobileOfferHeadingData1,
                targetMobileOfferHeadingData2));
        BDDMockito.given(targetMobileOfferHeadingFacade.getTargetMobileOfferHeadings()).willReturn(
                targetMobileOfferHeadingList);
    }

    @Test
    public void testAllMobileActiveVouchersWhenNoVouchersAvailableWithOfferHeading() {
        BDDMockito.given(targetMobileVoucherService.getAllActiveMobileVouchers()).willReturn(
                new ArrayList<PromotionVoucherModel>());
        final TargetVouchersData targetVouchersData = targetVoucherFacade
                .getAllMobileActivePromotionVouchersWithOfferHeadings();
        Assertions.assertThat(targetVouchersData).isNotNull();
        Assertions.assertThat(targetVouchersData.getTargetPromotionVouchers()).isEmpty();
        Assertions.assertThat(targetVouchersData.getTargetMobileOfferHeadings()).isNotEmpty();
        Assertions.assertThat(targetVouchersData.getTargetMobileOfferHeadings())
                .isEqualTo(targetMobileOfferHeadingList);
    }

    @Test
    public void testAllMobileActiveVouchersWhenVouchersAvailableWithOfferHeading() {
        final PromotionVoucherModel promotionVoucherModel1 = Mockito.mock(PromotionVoucherModel.class);
        final PromotionVoucherModel promotionVoucherModel2 = Mockito.mock(PromotionVoucherModel.class);
        final TargetPromotionVoucherData targetPromotionVoucherData1 = Mockito.mock(TargetPromotionVoucherData.class);
        final TargetPromotionVoucherData targetPromotionVoucherData2 = Mockito.mock(TargetPromotionVoucherData.class);
        BDDMockito.given(targetMobileVoucherService.getAllActiveMobileVouchers()).willReturn(
                new ArrayList<PromotionVoucherModel>(Arrays.asList(promotionVoucherModel1, promotionVoucherModel2)));

        BDDMockito.given(targetPromotionVoucherConverter.convert(promotionVoucherModel1)).willReturn(
                targetPromotionVoucherData1);
        BDDMockito.given(targetPromotionVoucherConverter.convert(promotionVoucherModel2)).willReturn(
                targetPromotionVoucherData2);

        final TargetVouchersData targetVouchersData = targetVoucherFacade
                .getAllMobileActivePromotionVouchersWithOfferHeadings();
        Assertions.assertThat(targetVouchersData).isNotNull();
        Assertions.assertThat(targetVouchersData.getTargetPromotionVouchers()).isNotEmpty();
        Assertions.assertThat(targetVouchersData.getTargetPromotionVouchers()).contains(targetPromotionVoucherData1);
        Assertions.assertThat(targetVouchersData.getTargetPromotionVouchers()).contains(targetPromotionVoucherData2);
        Assertions.assertThat(targetVouchersData.getTargetMobileOfferHeadings()).isNotEmpty();
        Assertions.assertThat(targetVouchersData.getTargetMobileOfferHeadings())
                .isEqualTo(targetMobileOfferHeadingList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void doesExistWhenVoucherCodeNull() {
        targetVoucherFacade.doesExist(null);
    }

    @Test
    public void doesExistWhenVoucherCodeNotExist() {
        final String voucherCode = "A1234";
        BDDMockito.given(targetVoucherService.getVoucher(voucherCode)).willReturn(null);
        Assertions.assertThat(targetVoucherFacade.doesExist(voucherCode)).isFalse();
    }

    @Test
    public void doesExistWhenVoucherCodeExist() {
        final String voucherCode = "A1234";
        BDDMockito.given(targetVoucherService.getVoucher(voucherCode)).willReturn(new VoucherModel());
        Assertions.assertThat(targetVoucherFacade.doesExist(voucherCode)).isTrue();
    }

    @Test
    public void isExpiredForNotExistingVoucher() throws ParseException {
        final String voucherCode = "A1234";
        final SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        final Date currentDate = formatter.parse("29-Feb-2016");
        BDDMockito.given(timeService.getCurrentTime()).willReturn(currentDate);
        BDDMockito.given(targetVoucherService.getVoucher(voucherCode)).willReturn(null);
        Assertions.assertThat(targetVoucherFacade.isExpired(voucherCode)).isFalse();
    }

    @Test
    public void isExpiredForVouchersWithoutRestrictions() throws ParseException {
        final String voucherCode = "A1234";
        final SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        final Date currentDate = formatter.parse("29-Feb-2016");
        BDDMockito.given(timeService.getCurrentTime()).willReturn(currentDate);
        final VoucherModel voucher = Mockito.mock(VoucherModel.class);
        BDDMockito.given(targetVoucherService.getVoucher(voucherCode)).willReturn(voucher);
        BDDMockito.given(voucher.getRestrictions()).willReturn(null);
        Assertions.assertThat(targetVoucherFacade.isExpired(voucherCode)).isFalse();
    }

    @Test
    public void isExpiredForVouchersWithEmptyRestrictions() throws ParseException {
        final String voucherCode = "A1234";
        final SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        final Date currentDate = formatter.parse("29-Feb-2016");
        BDDMockito.given(timeService.getCurrentTime()).willReturn(currentDate);
        final VoucherModel voucher = Mockito.mock(VoucherModel.class);
        BDDMockito.given(targetVoucherService.getVoucher(voucherCode)).willReturn(voucher);
        BDDMockito.given(voucher.getRestrictions()).willReturn(new HashSet());
        Assertions.assertThat(targetVoucherFacade.isExpired(voucherCode)).isFalse();
    }

    @Test
    public void isExpiredForVoucherHasNoExpirationDate() throws ParseException {
        final String voucherCode = "A1234";
        final SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        final Date currentDate = formatter.parse("29-Feb-2016");
        BDDMockito.given(timeService.getCurrentTime()).willReturn(currentDate);
        final VoucherModel voucher = Mockito.mock(VoucherModel.class);
        final Set<RestrictionModel> restrictions = new HashSet<>();
        final RestrictionModel timeRestriction = Mockito.mock(DateRestrictionModel.class);
        restrictions.add(timeRestriction);
        BDDMockito.given(targetVoucherService.getVoucher(voucherCode)).willReturn(voucher);
        BDDMockito.given(voucher.getRestrictions()).willReturn(restrictions);
        Assertions.assertThat(targetVoucherFacade.isExpired(voucherCode)).isFalse();
    }

    @Test(expected = IllegalArgumentException.class)
    public void isExpiredForVoucherCurentTimeNull() {
        final String voucherCode = "A1234";
        BDDMockito.given(timeService.getCurrentTime()).willReturn(null);
        Assertions.assertThat(targetVoucherFacade.isExpired(voucherCode)).isFalse();
    }

    @Test
    public void isExpiredForVoucherEndTimeAfterCurrentTime() throws ParseException {
        final SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        final Date currentDate = formatter.parse("29-Feb-2016");
        final Date endDate = formatter.parse("1-Mar-2016");
        final String voucherCode = "A1234";
        final VoucherModel voucher = Mockito.mock(VoucherModel.class);
        final Set<RestrictionModel> restrictions = new HashSet<>();
        final DateRestrictionModel timeRestriction = Mockito.mock(DateRestrictionModel.class);
        restrictions.add(timeRestriction);
        BDDMockito.given(targetVoucherService.getVoucher(voucherCode)).willReturn(voucher);
        BDDMockito.given(voucher.getRestrictions()).willReturn(restrictions);
        BDDMockito.given(timeRestriction.getEndDate()).willReturn(endDate);
        BDDMockito.given(timeService.getCurrentTime()).willReturn(currentDate);
        Assertions.assertThat(targetVoucherFacade.isExpired(voucherCode)).isFalse();
    }

    @Test
    public void isExpiredForVoucherEndTimeBeforeCurrentTime() throws ParseException {
        final SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        final Date currentDate = formatter.parse("29-Feb-2016");
        final Date endDate = formatter.parse("28-Feb-2016");
        final String voucherCode = "A1234";
        final VoucherModel voucher = Mockito.mock(VoucherModel.class);
        final Set<RestrictionModel> restrictions = new HashSet<>();
        final DateRestrictionModel timeRestriction = Mockito.mock(DateRestrictionModel.class);
        restrictions.add(timeRestriction);
        BDDMockito.given(targetVoucherService.getVoucher(voucherCode)).willReturn(voucher);
        BDDMockito.given(voucher.getRestrictions()).willReturn(restrictions);
        BDDMockito.given(timeRestriction.getEndDate()).willReturn(endDate);
        BDDMockito.given(timeService.getCurrentTime()).willReturn(currentDate);
        Assertions.assertThat(targetVoucherFacade.isExpired(voucherCode)).isTrue();
    }
}

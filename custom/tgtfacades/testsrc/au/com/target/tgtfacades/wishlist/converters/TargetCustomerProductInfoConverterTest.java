/**
 * 
 */
package au.com.target.tgtfacades.wishlist.converters;

import de.hybris.bootstrap.annotations.UnitTest;

import org.fest.assertions.Assertions;
import org.junit.Test;

import au.com.target.tgtfacades.product.data.CustomerProductInfo;
import au.com.target.tgtwishlist.data.TargetWishListEntryData;


/**
 * @author rsamuel3
 *
 */
@UnitTest
public class TargetCustomerProductInfoConverterTest {

    private static final String TIMESTAMP = "Jan15";
    private static final String VARIANT_CODE = "56790";
    private static final String BASE_PRODUCT = "W12345";

    private final TargetCustomerProductInfoConverter converter = new TargetCustomerProductInfoConverter();

    @Test(expected = IllegalArgumentException.class)
    public void testConvertNullSource() {
        converter.convert(null);
    }

    @Test
    public void testConvertSource() {
        final CustomerProductInfo customerProductInfo = populateCustomerProductInfo();
        final TargetWishListEntryData targetWishListEntryData = converter.convert(customerProductInfo);
        verifyTargetWishListEntryData(targetWishListEntryData);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertSourceTargetSourceNull() {
        converter.convert(null, new TargetWishListEntryData());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertSourceTargetNullTarget() {
        converter.convert(new CustomerProductInfo(), null);
    }

    @Test
    public void testConvertSourceTarget() {
        final CustomerProductInfo data = populateCustomerProductInfo();
        final TargetWishListEntryData targetWishListEntryData = converter.convert(data, new TargetWishListEntryData());
        verifyTargetWishListEntryData(targetWishListEntryData);
    }

    /**
     * @param targetWishListEntryData
     */
    private void verifyTargetWishListEntryData(final TargetWishListEntryData targetWishListEntryData) {
        Assertions.assertThat(targetWishListEntryData).isNotNull();
        Assertions.assertThat(targetWishListEntryData.getBaseProductCode()).isNotNull().isNotEmpty()
                .isEqualTo(BASE_PRODUCT);
        Assertions.assertThat(targetWishListEntryData.getSelectedVariantCode()).isNotNull().isNotEmpty()
                .isEqualTo(VARIANT_CODE);
        Assertions.assertThat(targetWishListEntryData.getTimeStamp()).isNotNull().isNotEmpty().isEqualTo(TIMESTAMP);
    }

    /**
     * @return TargetWishListEntryData
     */
    protected CustomerProductInfo populateCustomerProductInfo() {
        final CustomerProductInfo data = new CustomerProductInfo();
        data.setBaseProductCode(BASE_PRODUCT);
        data.setSelectedVariantCode(VARIANT_CODE);
        data.setTimeStamp(TIMESTAMP);
        return data;
    }

}

/**
 * 
 */
package au.com.target.tgtfacades.wishlist.converters;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Date;

import org.fest.assertions.Assertions;
import org.junit.Test;

import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.product.data.CustomerProductInfo;
import au.com.target.tgtwishlist.model.TargetWishListEntryModel;


/**
 * @author rsamuel3
 *
 */
@UnitTest
public class TargetWishListEntryModelConverterTest {
    /**
     * 
     */
    private static final String VARIANT_CODE = "56790";
    /**
     * 
     */
    private static final String BASE_PRODUCT = "W12345";

    private final Date currentDate = new Date();

    private final TargetWishListEntryModelConverter converter = new TargetWishListEntryModelConverter();

    @Test(expected = IllegalArgumentException.class)
    public void testConvertNullSource() {
        converter.convert(null);
    }

    @Test
    public void testConvertSource() {
        final TargetWishListEntryModel data = populateTargetWishListEntryModel();
        final CustomerProductInfo customerProductInfo = converter.convert(data);
        verifyCustomerProductInfo(customerProductInfo);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertSourceTargetSourceNull() {
        converter.convert(null, new CustomerProductInfo());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertSourceTargetNullTarget() {
        converter.convert(new TargetWishListEntryModel(), null);
    }

    @Test
    public void testConvertSourceTarget() {
        final TargetWishListEntryModel data = populateTargetWishListEntryModel();
        final CustomerProductInfo customerProductInfo = converter.convert(data, new CustomerProductInfo());
        verifyCustomerProductInfo(customerProductInfo);
    }

    /**
     * @param customerProductInfo
     */
    private void verifyCustomerProductInfo(final CustomerProductInfo customerProductInfo) {
        Assertions.assertThat(customerProductInfo).isNotNull();
        Assertions.assertThat(customerProductInfo.getBaseProductCode()).isNotNull().isNotEmpty()
                .isEqualTo(BASE_PRODUCT);
        Assertions.assertThat(customerProductInfo.getSelectedVariantCode()).isNotNull().isNotEmpty()
                .isEqualTo(VARIANT_CODE);
        Assertions.assertThat(customerProductInfo.getTimeStamp()).isNotNull().isNotEmpty()
                .isEqualTo(String.valueOf(currentDate.getTime()));
    }

    /**
     * @return TargetWishListEntryData
     */
    protected TargetWishListEntryModel populateTargetWishListEntryModel() {
        final TargetWishListEntryModel entryModel = new TargetWishListEntryModel();
        final TargetProductModel productModel = new TargetProductModel();
        productModel.setCode(BASE_PRODUCT);
        entryModel.setProduct(productModel);
        entryModel.setSelectedVariant(VARIANT_CODE);
        entryModel.setAddedDate(currentDate);
        return entryModel;
    }
}

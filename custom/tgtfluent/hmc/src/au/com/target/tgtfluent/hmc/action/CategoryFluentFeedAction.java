/**
 * 
 */
package au.com.target.tgtfluent.hmc.action;

import de.hybris.platform.core.Registry;
import de.hybris.platform.hmc.HMCHelper;
import de.hybris.platform.hmc.util.action.ActionResult;

import au.com.target.tgtcore.jalo.TargetProductCategory;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtfluent.model.CategoryFluentUpdateStatusModel;
import au.com.target.tgtfluent.service.FluentCategoryUpsertService;


/**
 * @author bhuang3
 *
 */
public class CategoryFluentFeedAction extends
        AbstractFluentFeedAction<TargetProductCategory, TargetProductCategoryModel> {

    private static final FluentCategoryUpsertService FLUENT_CATEGORY_UPSERT_SERVICE = (FluentCategoryUpsertService)Registry
            .getApplicationContext().getBean("fluentCategoryUpsertService");

    @Override
    protected ActionResult executeAction(final TargetProductCategoryModel model) {
        try {
            FLUENT_CATEGORY_UPSERT_SERVICE.upsertCategory(model);
            if (checkCategoryupdateStatus(model.getCode())) {
                return new ActionResult(0, HMCHelper.getLocalizedString("action.fluentfeed.success"), true);
            }
            else {
                return new ActionResult(1, HMCHelper.getLocalizedString("action.fluentfeed.failure"), false);
            }
        }
        catch (final Exception e) {
            return new ActionResult(1, HMCHelper.getLocalizedString("action.fluentfeed.failure"), false);
        }

    }

    private boolean checkCategoryupdateStatus(final String code) {
        final CategoryFluentUpdateStatusModel fluentUpdateStatus = getFluentUpdateStatusService()
                .getFluentUpdateStatus(CategoryFluentUpdateStatusModel.class,
                        CategoryFluentUpdateStatusModel._TYPECODE, code);
        return fluentUpdateStatus != null && fluentUpdateStatus.isLastRunSuccessful();
    }

}

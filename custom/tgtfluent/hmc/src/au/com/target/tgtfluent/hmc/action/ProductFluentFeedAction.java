/**
 * 
 */
package au.com.target.tgtfluent.hmc.action;

import de.hybris.platform.core.Registry;
import de.hybris.platform.hmc.HMCHelper;
import de.hybris.platform.hmc.util.action.ActionResult;

import java.text.MessageFormat;
import java.util.List;

import au.com.target.tgtcore.jalo.TargetProduct;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfluent.model.AbstractFluentUpdateStatusModel;
import au.com.target.tgtfluent.service.FluentProductUpsertService;
import au.com.target.tgtfluent.service.FluentSkuUpsertService;
import au.com.target.tgtutility.util.TargetProductUtils;


/**
 * @author bhuang3
 *
 */
public class ProductFluentFeedAction extends AbstractFluentFeedAction<TargetProduct, TargetProductModel> {

    private static final FluentProductUpsertService FLUENT_PRODUCT_UPSERT_SERVICE = (FluentProductUpsertService)Registry
            .getApplicationContext().getBean("fluentProductUpsertService");

    private static final FluentSkuUpsertService FLUENT_SKU_UPSERT_SERVICE = (FluentSkuUpsertService)Registry
            .getApplicationContext().getBean(
                    "fluentSkuUpsertService");

    @Override
    protected ActionResult executeAction(final TargetProductModel model) {
        try {
            FLUENT_PRODUCT_UPSERT_SERVICE.upsertProduct(model);
            if (checkFluentUpdateStatus(model.getCode())) {
                final String skuFeedResult = upsertSkus(model);
                return new ActionResult(0, MessageFormat.format(
                        HMCHelper.getLocalizedString("action.productfluentfeed.success"), model.getCode()) + "\n"
                        + skuFeedResult, true);
            }
            else {
                return new ActionResult(1, MessageFormat.format(
                        HMCHelper.getLocalizedString("action.productfluentfeed.failure"), model.getCode()), false);
            }
        }
        catch (final Exception e) {
            return new ActionResult(1, HMCHelper.getLocalizedString("action.fluentfeed.failure"), false);
        }
    }


    private String upsertSkus(final TargetProductModel model) {
        final List<AbstractTargetVariantProductModel> variantList = TargetProductUtils.getAllSellableVariants(model);
        final StringBuilder sb = new StringBuilder();
        for (final AbstractTargetVariantProductModel variant : variantList) {
            if (upsertSku(variant)) {
                sb.append(MessageFormat.format(HMCHelper.getLocalizedString("action.skufluentfeed.success"),
                        variant.getCode())).append("\n");
            }
            else {
                sb.append(MessageFormat.format(HMCHelper.getLocalizedString("action.skufluentfeed.failure"),
                        variant.getCode())).append("\n");
            }
        }
        return sb.toString();
    }

    private boolean upsertSku(final AbstractTargetVariantProductModel variant) {
        try {
            FLUENT_SKU_UPSERT_SERVICE.upsertSku(variant);
            return checkFluentUpdateStatus(variant.getCode());
        }
        catch (final Exception e) {
            return false;
        }

    }

    private boolean checkFluentUpdateStatus(final String code) {
        final AbstractFluentUpdateStatusModel fluentUpdateStatus = getFluentUpdateStatusService()
                .getFluentUpdateStatus(AbstractFluentUpdateStatusModel.class,
                        AbstractFluentUpdateStatusModel._TYPECODE, code);
        return fluentUpdateStatus != null && fluentUpdateStatus.isLastRunSuccessful();
    }

}

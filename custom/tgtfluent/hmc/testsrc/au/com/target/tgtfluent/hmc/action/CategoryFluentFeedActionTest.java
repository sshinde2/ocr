/**
 * 
 */
package au.com.target.tgtfluent.hmc.action;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.Registry;
import de.hybris.platform.hmc.HMCHelper;
import de.hybris.platform.hmc.jalo.AccessManager;
import de.hybris.platform.hmc.util.action.ActionEvent;
import de.hybris.platform.hmc.util.action.ActionResult;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.servicelayer.model.ModelService;

import java.lang.reflect.Field;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.ApplicationContext;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.jalo.TargetProductCategory;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtfluent.model.CategoryFluentUpdateStatusModel;
import au.com.target.tgtfluent.service.FluentCategoryUpsertService;
import au.com.target.tgtfluent.service.FluentUpdateStatusService;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({ "au.com.target.tgtfluent.hmc.action.AbstractFluentFeedAction",
        "au.com.target.tgtfluent.hmc.action.CategoryFluentFeedAction" })
@PrepareForTest({ Registry.class, HMCHelper.class, AccessManager.class, CategoryFluentFeedAction.class })
public class CategoryFluentFeedActionTest {

    private final CategoryFluentFeedAction categoryFluentFeedAction = new CategoryFluentFeedAction();

    @Mock
    private ActionEvent actionEvent;

    @Mock
    private TargetProductCategory category;

    @Mock
    private TargetProductCategoryModel categorymodel;

    @Mock
    private ApplicationContext context;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private ModelService modelService;

    @Mock
    private FluentUpdateStatusService fluentUpdateStatusService;

    @Mock
    private FluentCategoryUpsertService fluentCategoryUpsertService;

    @Mock
    private AccessManager accessManager;

    @Mock
    private CategoryFluentUpdateStatusModel fluentUpdateStatus;

    private final String notCreated = "notCreated";

    private final String cannotChange = "cannotChange";

    private final String feedFailure = "feedFailure";

    private final String feedSuccess = "feedSuccess";

    private final String categoryCode = "testCode";

    @Before
    public void setup() throws IllegalArgumentException, IllegalAccessException {
        given(Boolean.valueOf(fluentUpdateStatus.isLastRunSuccessful())).willReturn(Boolean.TRUE);
        given(categorymodel.getCode()).willReturn(categoryCode);

        final Field fieldModelService = PowerMockito.field(CategoryFluentFeedAction.class, "MODEL_SERVICE");
        fieldModelService.set(CategoryFluentFeedAction.class, modelService);

        final Field fieldUpdateStatusService = PowerMockito.field(CategoryFluentFeedAction.class,
                "FLUENT_UPDATE_STATUS_SERVICE");
        fieldUpdateStatusService.set(CategoryFluentFeedAction.class, fluentUpdateStatusService);

        final Field fieldUpsertService = PowerMockito.field(CategoryFluentFeedAction.class,
                "FLUENT_CATEGORY_UPSERT_SERVICE");
        fieldUpsertService.set(CategoryFluentFeedAction.class, fluentCategoryUpsertService);

        final Field fieldFeatureService = PowerMockito.field(CategoryFluentFeedAction.class,
                "TARGET_FEATURE_SWITCH_SERVICES");
        fieldFeatureService.set(CategoryFluentFeedAction.class, targetFeatureSwitchService);

        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)))
                .willReturn(Boolean.TRUE);

        given(fluentUpdateStatusService.getFluentUpdateStatus(CategoryFluentUpdateStatusModel.class,
                CategoryFluentUpdateStatusModel._TYPECODE, categoryCode)).willReturn(fluentUpdateStatus);

        given(actionEvent.getData()).willReturn(category);
        PowerMockito.mockStatic(HMCHelper.class, Mockito.RETURNS_MOCKS);
        given(HMCHelper.getLocalizedString("action.notcreatedyet")).willReturn(notCreated);
        given(HMCHelper.getLocalizedString("action.cannotchangetype")).willReturn(cannotChange);
        given(HMCHelper.getLocalizedString("action.fluentfeed.failure")).willReturn(feedFailure);
        given(HMCHelper.getLocalizedString("action.fluentfeed.success")).willReturn(feedSuccess);

        given(modelService.get(category)).willReturn(categorymodel);
        given(context.getBean("fluentCategoryUpsertService")).willReturn(fluentCategoryUpsertService);

        PowerMockito.mockStatic(AccessManager.class, Mockito.RETURNS_MOCKS);
        given(AccessManager.getInstance()).willReturn(accessManager);
        given(Boolean.valueOf(accessManager.canChange(any(ComposedType.class)))).willReturn(Boolean.TRUE);
    }

    @Test
    public void testPerform() throws JaloBusinessException {
        final ActionResult result = categoryFluentFeedAction.perform(actionEvent);
        assertThat(result.getMessage()).isEqualTo(feedSuccess + '\n');
        assertThat(result.getResult()).isEqualTo(0);
        verify(fluentCategoryUpsertService).upsertCategory(categorymodel);
    }

    @Test
    public void testPerformWithNullItem() throws JaloBusinessException {
        given(actionEvent.getData()).willReturn(null);
        final ActionResult result = categoryFluentFeedAction.perform(actionEvent);
        assertThat(result.getMessage()).isEqualTo(notCreated + '\n');
        assertThat(result.getResult()).isEqualTo(1);
    }

    @Test
    public void testPerformWithNoPermission() throws JaloBusinessException {
        given(Boolean.valueOf(accessManager.canChange(any(ComposedType.class)))).willReturn(Boolean.FALSE);
        final ActionResult result = categoryFluentFeedAction.perform(actionEvent);
        assertThat(result.getMessage()).isEqualTo(cannotChange + '\n');
        assertThat(result.getResult()).isEqualTo(1);
    }

    @Test
    public void testPerformWithException() throws JaloBusinessException {
        doThrow(new RuntimeException()).when(fluentCategoryUpsertService).upsertCategory(categorymodel);
        final ActionResult result = categoryFluentFeedAction.perform(actionEvent);
        assertThat(result.getMessage()).isEqualTo(feedFailure + '\n');
        assertThat(result.getResult()).isEqualTo(1);
    }
}

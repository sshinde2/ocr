/**
 * 
 */
package au.com.target.tgtfluent.hmc.action;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.Registry;
import de.hybris.platform.hmc.HMCHelper;
import de.hybris.platform.hmc.jalo.AccessManager;
import de.hybris.platform.hmc.util.action.ActionEvent;
import de.hybris.platform.hmc.util.action.ActionResult;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.jalo.TargetProduct;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfluent.model.AbstractFluentUpdateStatusModel;
import au.com.target.tgtfluent.model.SkuFluentUpdateStatusModel;
import au.com.target.tgtfluent.service.FluentProductUpsertService;
import au.com.target.tgtfluent.service.FluentSkuUpsertService;
import au.com.target.tgtfluent.service.FluentUpdateStatusService;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({ "au.com.target.tgtfluent.hmc.action.AbstractFluentFeedAction",
        "au.com.target.tgtfluent.hmc.action.ProductFluentFeedAction" })
@PrepareForTest({ Registry.class, HMCHelper.class, AccessManager.class, ProductFluentFeedAction.class })
public class ProductFluentFeedActionTest {

    private final ProductFluentFeedAction productFluentFeedAction = new ProductFluentFeedAction();

    @Mock
    private ActionEvent actionEvent;

    @Mock
    private TargetProduct targetProduct;

    @Mock
    private TargetProductModel targetProductModel;

    @Mock
    private TargetColourVariantProductModel colour1;

    @Mock
    private TargetColourVariantProductModel colour2;

    @Mock
    private TargetSizeVariantProductModel size11;

    @Mock
    private TargetSizeVariantProductModel size12;

    @Mock
    private TargetSizeVariantProductModel size21;

    @Mock
    private TargetSizeVariantProductModel size22;

    @Mock
    private ModelService modelService;

    @Mock
    private FluentUpdateStatusService fluentUpdateStatusService;

    @Mock
    private FluentProductUpsertService fluentProductUpsertService;

    @Mock
    private FluentSkuUpsertService fluentSkuUpsertService;

    @Mock
    private AccessManager accessManager;

    @Mock
    private AbstractFluentUpdateStatusModel fluentUpdateStatus;

    @Mock
    private SkuFluentUpdateStatusModel skuFluentUpdateStatusModel;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    private final String notCreated = "notCreated";

    private final String cannotChange = "cannotChange";

    private final String feedFailure = "feedFailure";

    private final String feedSuccess = "feedSuccess";

    private final String productfluentfeedSuccess = "product feed success {0}";

    private final String productfluentfeedFailure = "product feed failure {0}";

    private final String skufluentfeedSuccess = "product feed success {0}";

    private final String skufluentfeedFailure = "product feed failure {0}";

    private final String productCode = "productCode";

    private final String variantCode = "variantCode";


    @Before
    public void setup() throws IllegalArgumentException, IllegalAccessException {
        given(Boolean.valueOf(fluentUpdateStatus.isLastRunSuccessful())).willReturn(Boolean.TRUE);

        given(targetProductModel.getCode()).willReturn(productCode);

        final Field fieldModelService = PowerMockito.field(ProductFluentFeedAction.class, "MODEL_SERVICE");
        fieldModelService.set(ProductFluentFeedAction.class, modelService);

        final Field fieldUpdateStatusService = PowerMockito.field(ProductFluentFeedAction.class,
                "FLUENT_UPDATE_STATUS_SERVICE");
        fieldUpdateStatusService.set(ProductFluentFeedAction.class, fluentUpdateStatusService);

        final Field fieldUpsertService = PowerMockito.field(ProductFluentFeedAction.class,
                "FLUENT_PRODUCT_UPSERT_SERVICE");
        fieldUpsertService.set(ProductFluentFeedAction.class, fluentProductUpsertService);

        final Field fieldSkuUpsertService = PowerMockito.field(ProductFluentFeedAction.class,
                "FLUENT_SKU_UPSERT_SERVICE");
        fieldSkuUpsertService.set(ProductFluentFeedAction.class, fluentSkuUpsertService);

        final Field fieldFeatureService = PowerMockito.field(CategoryFluentFeedAction.class,
                "TARGET_FEATURE_SWITCH_SERVICES");
        fieldFeatureService.set(CategoryFluentFeedAction.class, targetFeatureSwitchService);

        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)))
                .willReturn(Boolean.TRUE);

        given(fluentUpdateStatusService.getFluentUpdateStatus(AbstractFluentUpdateStatusModel.class,
                AbstractFluentUpdateStatusModel._TYPECODE, productCode)).willReturn(fluentUpdateStatus);

        given(fluentUpdateStatusService.getFluentUpdateStatus(
                AbstractFluentUpdateStatusModel.class, AbstractFluentUpdateStatusModel._TYPECODE, variantCode))
                        .willReturn(
                                fluentUpdateStatus);

        given(actionEvent.getData()).willReturn(targetProduct);
        PowerMockito.mockStatic(HMCHelper.class, Mockito.RETURNS_MOCKS);
        given(HMCHelper.getLocalizedString("action.notcreatedyet")).willReturn(notCreated);
        given(HMCHelper.getLocalizedString("action.cannotchangetype")).willReturn(cannotChange);
        given(HMCHelper.getLocalizedString("action.fluentfeed.failure")).willReturn(feedFailure);
        given(HMCHelper.getLocalizedString("action.fluentfeed.success")).willReturn(feedSuccess);
        given(HMCHelper.getLocalizedString("action.productfluentfeed.success")).willReturn(productfluentfeedSuccess);
        given(HMCHelper.getLocalizedString("action.productfluentfeed.failure")).willReturn(productfluentfeedFailure);
        given(HMCHelper.getLocalizedString("action.skufluentfeed.success")).willReturn(skufluentfeedSuccess);
        given(HMCHelper.getLocalizedString("action.skufluentfeed.failure")).willReturn(skufluentfeedFailure);

        given(modelService.get(targetProduct)).willReturn(targetProductModel);

        PowerMockito.mockStatic(AccessManager.class, Mockito.RETURNS_MOCKS);
        given(AccessManager.getInstance()).willReturn(accessManager);
        given(Boolean.valueOf(accessManager.canChange(any(ComposedType.class)))).willReturn(Boolean.TRUE);
    }

    @Test
    public void testPerform() throws JaloBusinessException {
        final ActionResult result = productFluentFeedAction.perform(actionEvent);
        assertThat(result.getMessage()).isEqualTo("product feed success productCode\n" + '\n');
        assertThat(result.getResult()).isEqualTo(0);
        verify(fluentProductUpsertService).upsertProduct(targetProductModel);
    }

    @Test
    public void testPerformWithNullItem() throws JaloBusinessException {
        given(actionEvent.getData()).willReturn(null);
        final ActionResult result = productFluentFeedAction.perform(actionEvent);
        assertThat(result.getMessage()).isEqualTo(notCreated + '\n');
        assertThat(result.getResult()).isEqualTo(1);
    }

    @Test
    public void testPerformWithNoPermission() throws JaloBusinessException {
        given(Boolean.valueOf(accessManager.canChange(any(ComposedType.class)))).willReturn(Boolean.FALSE);
        final ActionResult result = productFluentFeedAction.perform(actionEvent);
        assertThat(result.getMessage()).isEqualTo(cannotChange + '\n');
        assertThat(result.getResult()).isEqualTo(1);
    }

    @Test
    public void testPerformWithException() throws JaloBusinessException {
        doThrow(new RuntimeException()).when(fluentProductUpsertService).upsertProduct(targetProductModel);
        final ActionResult result = productFluentFeedAction.perform(actionEvent);
        assertThat(result.getMessage()).isEqualTo(feedFailure + '\n');
        assertThat(result.getResult()).isEqualTo(1);
    }

    @Test
    public void testPerformWitVariants() throws JaloBusinessException {
        createVariants();
        final ActionResult result = productFluentFeedAction.perform(actionEvent);
        assertThat(result.getMessage())
                .isEqualTo(
                        "product feed success productCode\n"
                                + "product feed success variantCode\n"
                                + "product feed success variantCode\n"
                                + "product feed success variantCode\n"
                                + "product feed success variantCode\n" + '\n');
        assertThat(result.getResult()).isEqualTo(0);
        verify(fluentProductUpsertService).upsertProduct(targetProductModel);
        verify(fluentSkuUpsertService).upsertSku(size11);
        verify(fluentSkuUpsertService).upsertSku(size12);
        verify(fluentSkuUpsertService).upsertSku(size21);
        verify(fluentSkuUpsertService).upsertSku(size22);
    }

    private void createVariants() {
        given(size11.getCode()).willReturn(variantCode);
        given(size12.getCode()).willReturn(variantCode);
        given(size21.getCode()).willReturn(variantCode);
        given(size22.getCode()).willReturn(variantCode);
        final Collection<VariantProductModel> colourVariants = new ArrayList<>();
        colourVariants.add(colour1);
        colourVariants.add(colour2);
        given(targetProductModel.getVariants()).willReturn(colourVariants);
        final Collection<VariantProductModel> sizeVariants1 = new ArrayList<>();
        sizeVariants1.add(size11);
        sizeVariants1.add(size12);
        given(colour1.getVariants()).willReturn(sizeVariants1);
        final Collection<VariantProductModel> sizeVariants2 = new ArrayList<>();
        sizeVariants2.add(size21);
        sizeVariants2.add(size22);
        given(colour2.getVariants()).willReturn(sizeVariants2);
    }
}

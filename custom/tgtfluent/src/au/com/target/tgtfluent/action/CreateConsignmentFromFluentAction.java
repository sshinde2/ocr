/**
 * 
 */
package au.com.target.tgtfluent.action;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;
import au.com.target.tgtfluent.exception.FluentFulfilmentException;
import au.com.target.tgtfluent.service.FluentFulfilmentService;


/**
 * @author bhuang3
 *
 */
public class CreateConsignmentFromFluentAction extends RetryActionWithSpringConfig<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(CreateConsignmentFromFluentAction.class);

    private static final String FLUENT_ERROR = "FLUENT_FULFILMENT_CREATION_ERROR: ";

    private FluentFulfilmentService fluentFulfilmentService;

    public enum Transition {
        OK, NOK, RETRYEXCEEDED;

        public static Set<String> getStringValues() {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values()) {
                res.add(t.toString());
            }
            return res;
        }
    }


    @Override
    protected String executeInternal(final OrderProcessModel process) throws RetryLaterException, Exception {
        final OrderModel order = process.getOrder();
        Assert.notNull(order, "Order cannot be null");
        Transition result = Transition.OK;
        try {
            fluentFulfilmentService.createConsignmentsByOrderId(order);
        }
        catch (final FluentFulfilmentException e) {
            LOG.error(
                    FLUENT_ERROR
                            + " Trigger retry for creating consignments error because of the service unavailable "
                            + "or empty response for order "
                            + "or status in fluent is not correct for order" + order.getCode(),
                    e);
            throw new RetryLaterException(
                    "Trigger retry for creating consignments error because of the service unavailable "
                            + "or empty response for order "
                            + "or status in fluent is not correct for order" + order.getCode());
        }
        catch (final Exception e1) {
            LOG.error(FLUENT_ERROR + " orderCode=" + order.getCode() + " CreateConsignmentFromFluentAction error: ",
                    e1);
            result = Transition.NOK;
        }

        return result.toString();
    }


    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }



    /**
     * @param fluentFulfilmentService
     *            the fluentFulfilmentService to set
     */
    @Required
    public void setFluentFulfilmentService(final FluentFulfilmentService fluentFulfilmentService) {
        this.fluentFulfilmentService = fluentFulfilmentService;
    }

}

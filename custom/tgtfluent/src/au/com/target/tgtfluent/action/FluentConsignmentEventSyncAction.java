/**
 * 
 */
package au.com.target.tgtfluent.action;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfluent.constants.TgtFluentConstants;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.exception.FluentFulfilmentException;
import au.com.target.tgtfluent.service.FluentConsignmentService;
import au.com.target.tgtfluent.service.FluentFulfilmentService;
import au.com.target.tgtfulfilment.enums.OfcOrderType;


/**
 * @author pvarghe2
 *
 */
public class FluentConsignmentEventSyncAction extends RetryActionWithSpringConfig<OrderProcessModel> {

    private static final String FLUENT_OFC_SYNC_ERROR = "FLUENT_OFC_SYNC_ERROR : ConsignmentId={0}, exceptionMessage={1}";

    private static final Logger LOG = Logger.getLogger(FluentConsignmentEventSyncAction.class);

    private FluentConsignmentService fluentConsignmentService;

    private FluentFulfilmentService fluentFulfilmentService;

    public enum Transition {
        OK, NOK, RETRYEXCEEDED;

        public static Set<String> getStringValues() {
            final Set<String> res = new HashSet<>();
            for (final Transition t : Transition.values()) {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    protected String executeInternal(final OrderProcessModel process) throws RetryLaterException, Exception {
        final TargetConsignmentModel consignment = (TargetConsignmentModel)getOrderProcessParameterHelper()
                .getConsignment(process);
        Assert.notNull(consignment, "Consignment cannot be null");
        try {
            if (fluentConsignmentService.isAFluentConsignment(consignment)) {
                fluentFulfilmentService.sendConsignmentEventToFluent(consignment,
                        consignment.getOfcOrderType() == OfcOrderType.INSTORE_PICKUP
                                ? TgtFluentConstants.TargetOfcEvent.OFC_SS_CNC_SHIPPED
                                : TgtFluentConstants.TargetOfcEvent.OFC_SHIPPED,
                        true);
            }
        }
        catch (final FluentFulfilmentException ex) {
            LOG.error(MessageFormat.format(FLUENT_OFC_SYNC_ERROR, consignment.getCode(), ex.getMessage()));
            return Transition.NOK.toString();
        }
        catch (final FluentClientException ex) {
            LOG.error(MessageFormat.format(FLUENT_OFC_SYNC_ERROR, consignment.getCode(), ex.getMessage()));
            throw new RetryLaterException(
                    MessageFormat.format(FLUENT_OFC_SYNC_ERROR, consignment.getCode(), ex.getMessage()));
        }
        return Transition.OK.toString();
    }

    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }

    /**
     * @param fluentConsignmentService
     *            the fluentConsignmentService to set
     */
    @Required
    public void setFluentConsignmentService(final FluentConsignmentService fluentConsignmentService) {
        this.fluentConsignmentService = fluentConsignmentService;
    }

    /**
     * @param fluentFulfilmentService
     *            the fluentFulfilmentService to set
     */
    @Required
    public void setFluentFulfilmentService(final FluentFulfilmentService fluentFulfilmentService) {
        this.fluentFulfilmentService = fluentFulfilmentService;
    }
}

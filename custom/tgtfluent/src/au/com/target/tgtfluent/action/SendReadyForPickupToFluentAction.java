/**
 * 
 */
package au.com.target.tgtfluent.action;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfluent.exception.FluentBaseException;
import au.com.target.tgtfluent.service.FluentFulfilmentService;


/**
 * @author cbi
 *
 */
public class SendReadyForPickupToFluentAction extends AbstractProceduralAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(SendReadyForPickupToFluentAction.class);

    private static final String FLUENT_READYFORPICKUP_FEATURESWITCH_OFF = "FLUENT READYFORPICKUP FEATURESWITCH is OFF OR NOT A FLUENT ORDER.";

    private static final String FLUENT_READYFORPICKUP_ORDER_FAILED = "FLUENT READYFORPICKUP ORDER FAILED: ";

    private static final String FLUENT_READYFORPICKUP_CONSIGNMENTS_NULL = "FLUENT READYFORPICKUP CONSIGNMENTS IS NULL";

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private FluentFulfilmentService fluentFulfilmentService;

    @Override
    public void executeAction(final OrderProcessModel orderProcessModel) throws RetryLaterException, Exception {
        if (!(targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)
                && StringUtils.isNotEmpty(orderProcessModel.getOrder().getFluentId()))) {
            LOG.info(FLUENT_READYFORPICKUP_FEATURESWITCH_OFF);
            return;
        }
        try {
            final OrderModel orderModel = orderProcessModel.getOrder();
            final List<TargetConsignmentModel> consignments = getShippedAndNotEGiftCardConsignmentsForOrder(orderModel);
            if (CollectionUtils.isEmpty(consignments)) {
                LOG.warn(FLUENT_READYFORPICKUP_CONSIGNMENTS_NULL);
                return;
            }
            for (final TargetConsignmentModel consignment : consignments) {
                fluentFulfilmentService.sendReadyForPickup(consignment);
            }
        }
        catch (final FluentBaseException e) {
            LOG.error(FLUENT_READYFORPICKUP_ORDER_FAILED, e);
        }
    }

    /**
     * @param fluentFulfilmentService
     *            the fluentFulfilmentService to set
     */
    public void setFluentFulfilmentService(final FluentFulfilmentService fluentFulfilmentService) {
        this.fluentFulfilmentService = fluentFulfilmentService;
    }

    /**
     * 
     * @param orderModel
     * @return List<TargetConsignmentModel>
     */
    private List<TargetConsignmentModel> getShippedAndNotEGiftCardConsignmentsForOrder(final OrderModel orderModel) {
        if (CollectionUtils.isEmpty(orderModel.getConsignments())) {
            return Collections.emptyList();
        }
        else {
            final List<TargetConsignmentModel> activeOnes = new ArrayList<>();
            for (final ConsignmentModel cons : orderModel.getConsignments()) {
                if (cons instanceof TargetConsignmentModel
                        && ConsignmentStatus.SHIPPED.equals(cons.getStatus())
                        && !checkIsDigital(cons)
                        && checkIsDeliveryToStore(cons)) {
                    activeOnes.add((TargetConsignmentModel)cons);
                }
            }
            return activeOnes;
        }

    }

    private boolean checkIsDeliveryToStore(final ConsignmentModel cons) {
        boolean isDeliveryToStore = false;
        if (cons != null && cons.getDeliveryMode() instanceof TargetZoneDeliveryModeModel) {
            isDeliveryToStore = BooleanUtils.isTrue(((TargetZoneDeliveryModeModel)cons.getDeliveryMode())
                    .getIsDeliveryToStore());
        }
        return isDeliveryToStore;

    }

    private boolean checkIsDigital(final ConsignmentModel cons) {
        boolean isDigital = false;
        if (cons != null && cons.getDeliveryMode() instanceof TargetZoneDeliveryModeModel) {
            isDigital = BooleanUtils.isTrue(((TargetZoneDeliveryModeModel)cons.getDeliveryMode()).getIsDigital());
        }
        return isDigital;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }


}

package au.com.target.tgtfluent.constants;

@SuppressWarnings("deprecation")
public class TgtFluentConstants extends GeneratedTgtFluentConstants {
    public static final String EXTENSIONNAME = "tgtfluent";

    private TgtFluentConstants() {
        //empty 
    }

    public interface Category {
        String ALL_PRODUCTS = "AP01";
    }

    public interface Event {
        String CANCEL_ORDER = "CANCEL_ORDER";
        String RESUME_ORDER = "RESUME_ORDER";
        String PREORDER_RELEASE = "PREORDER_RELEASE";
        String READY_FOR_PICKUP = "READY_FOR_PICKUP";
        String PICKED_UP = "PICKED_UP";
        String RETURNED_TO_FLOOR = "RETURNED_TO_FLOOR";
        String PREORDER_LOAD_MAX_QTY = "PREORDER_LOAD_MAX_QTY";
        String CUSTOMER_CANCEL = "CUSTOMER_CANCEL";
    }

    public interface EntityType {
        String ORDER = "ORDER";
        String FULFILMENT = "FULFILMENT";
        String INVENTORY_CATALOGUE = "INVENTORY_CATALOGUE";
    }

    public interface EntityRef {
        String DEFAULT = "DEFAULT:{retailerId}";
    }

    public interface EntitySubtype {
        String CC = "CC";
        String HD = "HD";
        String DEFAULT = "DEFAULT";
    }

    public interface DeliveryMode {
        String HOME_DELIVERY = "home-delivery";
        String EXPRESS_DELIVERY = "express-delivery";
        String CLICK_AND_COLLECT = "click-and-collect";
        String EBAY_HOME_DELIVERY = "eBay-delivery";
        String EBAY_EXPRESS_DELIVERY = "ebay-express-delivery";
        String EBAY_CLICK_COLLECT = "ebay-click-and-collect";
        String DIGITAL_GIFT_CARD = "digital-gift-card";
    }

    public interface FluentFulfilmentStatus {

        String CREATED = "CREATED";
        String AWAITING_WAVE = "AWAITING_WAVE";
        String SENT_TO_WAREHOUSE = "SENT_TO_WAREHOUSE";
        String CONFIRMED_BY_WAREHOUSE = "CONFIRMED_BY_WAREHOUSE";
        String ASSIGNED = "ASSIGNED";
        String PICK = "PICK";
        String PACK = "PACK";
        String FULFILLED = "FULFILLED";
        String PARTIALLY_FULFILLED = "PARTIALLY_FULFILLED";
        String SHIPPED = "SHIPPED";
        String REJECTED = "REJECTED";
        String SYSTEM_REJECTED = "SYSTEM_REJECTED";
        String SYSTEM_EXPIRED = "SYSTEM_EXPIRED";
        String EXPIRED = "EXPIRED";
        String CANCELLED_NO_STOCK = "CANCELLED_NO_STOCK";
    }

    public interface FluentCarrierName {
        String AUSTRALIA_POST = "AUSTRALIA_POST";
        String STAR_TRACK = "STAR_TRACK";
        String TOLL = "TOLL";
        String TOLL_CC = "TOLL_CC";
        String NA = "NA";
    }


    public interface TargetCarrierName {

        String AUSTRALIA_POST = "AustraliaPost";
        String AUSTRALIA_POST_EBAY = "AustraliaPostEbay";
        String AUSTRALIA_POST_EBAY_INSTORE_HD = "AustraliaPostEbayInstoreHD";
        String AUSTRALIA_POST_EBAY_INSTORE_CNC = "AustraliaPostEbayInstoreCNC";
        String AUSTRALIA_POST_INSTORE_CNC = "AustraliaPostInstoreCNC";
        String AUSTRALIA_POST_INSTORE_HD = "AustraliaPostInstoreHD";
        String NULL_CNC = "NullCnC";
        String NULL_DIG_GIFT = "NullDigGift";
        String NULL_EBAY_CNC = "NullEbayCnC";
        String STAR_TRACK = "StarTrack";
        String STAR_TRACK_EBAY = "StarTrackEbay";
        String TOLL = "Toll";
        String TOLL_CNC = "TollCnC";
        String TOLL_EBAY = "TollEbay";
        String TOLL_EBAY_CNC = "TollEbayCNC";
    }

    public interface TargetOfcEvent {
        String OFC_WAVED = "OFC_WAVED";
        String OFC_PICK_PACK = "OFC_PICK_PACK";
        String OFC_SS_CNC_SHIPPED = "OFC_SS_CNC_SHIPPED";
        String OFC_SHIPPED = "OFC_SHIPPED";
    }

    public interface FluentEventStatus {
        String FLUENT_EVENT_STATUS_FAILED = "FAILED";
        String FLUENT_EVENT_STATUS_NO_MATCH = "NO_MATCH";
    }

}

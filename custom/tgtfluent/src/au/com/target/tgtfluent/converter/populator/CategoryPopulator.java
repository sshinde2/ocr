/**
 * 
 */
package au.com.target.tgtfluent.converter.populator;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.MessageFormat;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.fest.util.Collections;

import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtfluent.data.Category;
import au.com.target.tgtfluent.exception.CategoryFluentFeedException;


/**
 * @author bpottass
 *
 */
public class CategoryPopulator implements Populator<TargetProductCategoryModel, Category> {

    private static final Logger LOG = Logger.getLogger(CategoryPopulator.class);

    private static final String LOG_PREFIX = "FLUENT CATEGORY FEED POPULATOR: ";

    private static final String SUPER_CATEGORY_MISSING_FLUENTID = "Category={0} with super category={1} has no fluent id";

    private static final String NO_FLUENT_ID_FOR_SUPER_CATEGORY = "NO_FLUENT_ID_FOR_SUPER_CATEGORY";

    @Override
    public void populate(final TargetProductCategoryModel source, final Category target) throws ConversionException {
        target.setCategoryRef(source.getCode());
        target.setName(source.getName());
        target.setId(source.getFluentId());
        target.setParentCategoryId(getParentCategoryFluentId(source));
    }

    /**
     * @param source
     */
    private String getParentCategoryFluentId(final TargetProductCategoryModel source) {

        final Collection<CategoryModel> parents = source.getSupercategories();
        if (isRootCategory(parents)) {
            return null;
        }
        String parentCategoryFluentId = null;
        for (final CategoryModel parentCategory : parents) {
            if (parentCategory instanceof TargetProductCategoryModel) {
                parentCategoryFluentId = ((TargetProductCategoryModel)parentCategory)
                        .getFluentId();
                if (parentCategoryFluentId == null) {
                    LOG.error(MessageFormat.format(LOG_PREFIX + SUPER_CATEGORY_MISSING_FLUENTID, source.getCode(),
                            parentCategory.getCode()));
                    throw new CategoryFluentFeedException(NO_FLUENT_ID_FOR_SUPER_CATEGORY,
                            MessageFormat.format(SUPER_CATEGORY_MISSING_FLUENTID, source.getCode(),
                                    parentCategory.getCode()));
                }
                break;
            }
        }
        return parentCategoryFluentId;
    }


    private boolean isRootCategory(final Collection<CategoryModel> parents) {
        return Collections.isEmpty(parents);
    }
}

/**
 * 
 */
package au.com.target.tgtfluent.converter.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfluent.data.Article;
import au.com.target.tgtfluent.data.Fulfilment;
import au.com.target.tgtfluent.data.FulfilmentItem;
import au.com.target.tgtfluent.util.FluentCarrierMap;
import au.com.target.tgtfulfilment.model.ConsignmentParcelModel;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;


/**
 * @author gsing236
 *
 */
public class FluentConsignmentPopulator implements Populator<TargetConsignmentModel, Fulfilment> {

    @Override
    public void populate(final TargetConsignmentModel consignmentModel, final Fulfilment fulfilment)
            throws ConversionException {
        populateCarrierDetails(consignmentModel, fulfilment);
        fulfilment.setItems(createFulfilmentItems(consignmentModel));
        fulfilment.setArticles(createArticles(consignmentModel));
        final Integer parcelCount = consignmentModel.getParcelCount();
        fulfilment.setParcelCount(parcelCount == null ? 0 : parcelCount.intValue());
    }

    private void populateCarrierDetails(final TargetConsignmentModel consignmentModel, final Fulfilment fulfilment) {
        final TargetCarrierModel carrierModel = consignmentModel.getTargetCarrier();
        if (carrierModel != null) {
            fulfilment.setCarrierName(FluentCarrierMap.getFluentCarrierName(carrierModel.getCode()));
            final String trackingID = consignmentModel.getTrackingID();
            final String trackingUrl = carrierModel.getTrackingUrl();
            fulfilment.setCarrierTrackingId(trackingID);
            fulfilment.setTrackingUrl((StringUtils.isNotBlank(trackingID) && StringUtils.isNotBlank(trackingUrl))
                    ? trackingUrl.concat(trackingID) : null);
        }
    }

    private List<Article> createArticles(final TargetConsignmentModel consignmentModel) {
        List<Article> articles = null;
        final Set<ConsignmentParcelModel> parcels = consignmentModel.getParcelsDetails();
        if (CollectionUtils.isNotEmpty(parcels)) {
            articles = new ArrayList<>();
            for (final ConsignmentParcelModel consignmentParcelModel : parcels) {
                final Article article = new Article();
                article.setHeight(String.valueOf(consignmentParcelModel.getHeight()));
                article.setLength(String.valueOf(consignmentParcelModel.getLength()));
                article.setWidth(String.valueOf(consignmentParcelModel.getWidth()));
                article.setWeight(String.valueOf(consignmentParcelModel.getActualWeight()));
                articles.add(article);
            }
        }
        return articles;
    }

    protected List<FulfilmentItem> createFulfilmentItems(final TargetConsignmentModel consignmentModel) {
        final List<FulfilmentItem> items = new ArrayList<>();
        for (final ConsignmentEntryModel entry : consignmentModel.getConsignmentEntries()) {
            items.add(createItem(entry));
        }
        return items;
    }

    protected FulfilmentItem createItem(final ConsignmentEntryModel entry) {
        final FulfilmentItem fulfilmentItem = new FulfilmentItem();
        fulfilmentItem.setOrderItemRef(entry.getOrderEntry().getProduct().getCode());
        final int requestedQty = entry.getQuantity().intValue();
        final int shippedQty = entry.getShippedQuantity().intValue();
        final int rejectedQty = requestedQty - shippedQty;
        fulfilmentItem.setRequestedQty(Integer.valueOf(requestedQty));
        fulfilmentItem.setConfirmedQty(Integer.valueOf(shippedQty));
        fulfilmentItem.setRejectedQty(Integer.valueOf(rejectedQty));
        return fulfilmentItem;
    }
}

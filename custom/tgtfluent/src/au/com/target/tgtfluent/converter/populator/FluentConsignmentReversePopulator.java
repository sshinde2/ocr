/**
 * 
 */
package au.com.target.tgtfluent.converter.populator;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.order.ZoneDeliveryModeService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.orderEntry.TargetOrderEntryService;
import au.com.target.tgtcore.util.ProductUtil;
import au.com.target.tgtfluent.constants.FulfilmentStatus.Vendor;
import au.com.target.tgtfluent.constants.TgtFluentConstants.DeliveryMode;
import au.com.target.tgtfluent.constants.TgtFluentConstants.FluentFulfilmentStatus;
import au.com.target.tgtfluent.dao.FluentCarrierDao;
import au.com.target.tgtfluent.data.Article;
import au.com.target.tgtfluent.data.Fulfilment;
import au.com.target.tgtfluent.data.FulfilmentItem;
import au.com.target.tgtfluent.util.FluentCarrierMap;
import au.com.target.tgtfluent.util.FluentDataMap;
import au.com.target.tgtfluent.util.FluentDeliveryType;
import au.com.target.tgtfluent.util.FluentFulfilmentType;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.model.ConsignmentParcelModel;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;


/**
 * @author bhuang3
 *
 */
public class FluentConsignmentReversePopulator implements Populator<Fulfilment, TargetConsignmentModel> {

    private static final Logger LOG = Logger.getLogger(FluentConsignmentReversePopulator.class);

    private static final String FLUENT_CONSIGNMENT_POPULATION_ERROR = "FLUENT_CONSIGNMENT_POPULATION_ERROR: message={0}, orderCode={1}, fulfilmentStatus={2}, occurredOn={3}, fulfilmentId={4}";

    private static final String FLUENT_CONSIGNMENT_POPULATION_WARN = "FLUENT_CONSIGNMENT_POPULATION_WARN: message={0}, orderCode={1}, fulfilmentStatus={2}, occurredOn={3}, fulfilmentId={4}";

    private static final List<SalesApplication> WEB_SALES_APPLICATIONS = ImmutableList.of(SalesApplication.WEB,
            SalesApplication.KIOSK, SalesApplication.MOBILEAPP);

    private TargetOrderEntryService targetOrderEntryService;

    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    private ModelService modelService;

    private ZoneDeliveryModeService zoneDeliveryModeService;

    private FluentCarrierDao fluentCarrierDao;

    @Override
    public void populate(final Fulfilment fulfilment, final TargetConsignmentModel consignment)
            throws ConversionException {
        final OrderModel orderModel = (OrderModel)consignment.getOrder();
        consignment.setStatus(FluentDataMap.getConsignmentStatus(fulfilment.getStatus()));
        this.populateConsignmentEntries(fulfilment.getItems(), consignment, orderModel);
        this.populateDeliveryMode(orderModel, fulfilment, consignment);
        this.populateRejectReason(fulfilment, consignment);
        this.populateShippingAddress(orderModel, fulfilment, consignment);
        this.populateConsignmentStatusDate(fulfilment, consignment);
        this.populateCarrier(fulfilment, consignment);
        this.populateArticle(fulfilment, consignment);
    }

    /**
     * @param fulfilment
     * @param consignment
     */
    protected void populateCarrier(final Fulfilment fulfilment, final TargetConsignmentModel consignment) {
        if (FluentFulfilmentStatus.CREATED.equals(fulfilment.getStatus())
                || FluentDataMap.SYSTEM_TERMINAL_STATUS.contains(fulfilment.getStatus())) {
            return;
        }
        populateTargetCarrier(fulfilment, consignment);
        consignment.setTrackingID(fulfilment.getCarrierTrackingId());
        consignment.setCarrierIntegratorTrackingId(fulfilment.getCarrierIntegrationTrackingId());
        consignment.setParcelCount(Integer.valueOf(fulfilment.getParcelCount()));
    }

    protected void populateArticle(final Fulfilment fulfilment, final TargetConsignmentModel consignment) {

        if (CollectionUtils.isEmpty(fulfilment.getArticles())) {
            return;
        }
        final List<String> parcelIdsPresent = getAllParcelIdAndParcelsPresent(consignment);
        final List<ConsignmentParcelModel> parcelModelsToSave = new ArrayList<>();
        for (final Article article : fulfilment.getArticles()) {
            if (CollectionUtils.isEmpty(parcelIdsPresent) || !parcelIdsPresent.contains(article.getArticleId())) {
                final ConsignmentParcelModel consignmentParcelModel = modelService.create(ConsignmentParcelModel.class);
                consignmentParcelModel.setActualWeight(getDoubleValue(article.getWeight()));
                consignmentParcelModel.setWidth(getDoubleValue(article.getWidth()));
                consignmentParcelModel.setLength(getDoubleValue(article.getLength()));
                consignmentParcelModel.setHeight(getDoubleValue(article.getHeight()));
                consignmentParcelModel.setParcelId(article.getArticleId());
                consignmentParcelModel.setActualWeight(Double.valueOf(article.getWeight()));
                consignmentParcelModel.setConsignment(consignment);
                parcelModelsToSave.add(consignmentParcelModel);
            }
        }
        if (CollectionUtils.isNotEmpty(parcelModelsToSave)) {
            modelService.saveAll(parcelModelsToSave);
        }
    }



    private Double getDoubleValue(final String value) {
        try {
            return Double.valueOf(value);
        }
        catch (final NumberFormatException ne) {
            return null;
        }

    }

    private List<String> getAllParcelIdAndParcelsPresent(
            final TargetConsignmentModel consignment) {
        final Set<ConsignmentParcelModel> consignmentParcels = consignment.getParcelsDetails();
        if (CollectionUtils.isEmpty(consignmentParcels)) {
            return ListUtils.EMPTY_LIST;
        }
        final List<String> parcelIdsPresent = new ArrayList();
        for (final ConsignmentParcelModel consignmentParcel : consignmentParcels) {
            if (StringUtils.isNotBlank(consignmentParcel.getParcelId())) {
                parcelIdsPresent.add(consignmentParcel.getParcelId());
            }
        }
        return parcelIdsPresent;
    }

    /**
     * 
     * @param fulfilment
     * @param consignment
     */
    private void populateTargetCarrier(final Fulfilment fulfilment, final TargetConsignmentModel consignment) {
        if (consignment.getTargetCarrier() != null) {
            return;
        }
        try {
            if (StringUtils.isEmpty(fulfilment.getCarrierName())) {
                throw new IllegalArgumentException(" carrierName is null");
            }
            final String targetCarrierCode = getTargetCarrierCode(fulfilment, consignment);
            final TargetCarrierModel targetCarrierModel = fluentCarrierDao
                    .getTargetCarrierByCode(targetCarrierCode);
            consignment.setTargetCarrier(targetCarrierModel);
            consignment.setCarrier(targetCarrierModel.getWarehouseCode());
        }
        catch (final Exception e) {
            LOG.error(MessageFormat.format(FLUENT_CONSIGNMENT_POPULATION_ERROR, e.getMessage(),
                    consignment.getOrder().getCode(),
                    consignment.getStatus().getCode(), null, fulfilment.getFulfilmentId()), e);
        }
    }

    /**
     * @param fulfilment
     * @param targetConsignment
     */
    protected void populateConsignmentStatusDate(final Fulfilment fulfilment,
            final TargetConsignmentModel targetConsignment) {

        if (isConsignmentStatusAndOccurredOnDateValid(fulfilment, targetConsignment)) {
            final ConsignmentStatus consignmentStatus = targetConsignment.getStatus();
            final Date occurredOn = fulfilment.getOccurredOn();

            if (isStatusConfirmedOrSentToWarehouse(consignmentStatus)) {
                targetConsignment.setSentToWarehouseDate(occurredOn);
            }
            else if (isStatusWavedAndWavedDateNull(targetConsignment, consignmentStatus)) {
                targetConsignment.setWavedDate(occurredOn);
            }
            else if (isStatusPickedAndPickDateNull(targetConsignment, consignmentStatus)) {
                targetConsignment.setPickConfirmDate(occurredOn);
            }
            else if (isStatusPackedAndPackedDateNull(targetConsignment, consignmentStatus)) {
                targetConsignment.setPackedDate(occurredOn);
            }
            else if (isStatusShippedAndShippedDateNull(targetConsignment, consignmentStatus)) {
                targetConsignment.setShippingDate(occurredOn);
                targetConsignment.setShipConfReceived(Boolean.TRUE);
            }
            else if (isStatusCancelledAndCancelledDateNull(targetConsignment, consignmentStatus)) {
                targetConsignment.setCancelDate(occurredOn);
            }
        }
    }


    /**
     * 
     * @param consignment
     * @return reject reason for fulfilment status REJECTED, ZERO_PICK_BY_STORE for store and ZERO_PICK_BY_WAREHOUSE for
     *         others
     */
    private ConsignmentRejectReason getReasonForRejectStatus(final TargetConsignmentModel consignment) {
        if (isWarehouseVendorStore(consignment.getWarehouse().getVendor().getCode())) {
            return ConsignmentRejectReason.ZERO_PICK_BY_STORE;
        }
        else {
            return ConsignmentRejectReason.ZERO_PICK_BY_WAREHOUSE;
        }
    }

    /**
     * @param orderModel
     * @param fulfilment
     * @param consignment
     */
    protected void populateShippingAddress(final OrderModel orderModel, final Fulfilment fulfilment,
            final TargetConsignmentModel consignment) {
        if (consignment.getShippingAddress() != null) {
            // shippingAddress already exists, don't try to recreate
            return;
        }
        if (CollectionUtils.isNotEmpty(fulfilment.getItems())) {
            final FulfilmentItem item = fulfilment.getItems().get(0);
            if (item != null && StringUtils.isNotEmpty(item.getOrderItemRef())) {
                final AbstractOrderEntryModel orderEntry = targetOrderEntryService
                        .getOrderEntryByProductCode(orderModel, item.getOrderItemRef());
                final ProductModel product = orderEntry.getProduct();
                if (!targetDeliveryModeHelper.hasOnlyDigitalDeliveryMode(product)) {
                    consignment.setShippingAddress(orderModel.getDeliveryAddress());
                }
            }
        }
    }

    /**
     * @param orderModel
     * @param fulfilment
     * @param consignment
     */
    protected void populateDeliveryMode(final OrderModel orderModel, final Fulfilment fulfilment,
            final TargetConsignmentModel consignment) {
        if (consignment.getDeliveryMode() != null) {
            // deliveryMode already exists, don't try to recreate
            return;
        }
        if (this.containsDigitalItem(orderModel, fulfilment.getItems())) {
            //we don't need to set the shipping address for digital delivery
            if (!this.containsAllDigitalItem(orderModel, fulfilment.getItems())) {
                throw new ConversionException(MessageFormat.format(FLUENT_CONSIGNMENT_POPULATION_ERROR,
                        " fulfilment doesn't contain all digital items",
                        orderModel.getCode(), fulfilment.getStatus(), fulfilment.getOccurredOn(),
                        fulfilment.getFulfilmentId()));
            }
            consignment.setDeliveryMode(this.getDigitalDeliveryModeFromItems(orderModel, fulfilment.getItems()));
        }
        else {
            this.populateNonDigitalDeliveryMode(fulfilment, consignment, orderModel);
        }
    }

    /**
     * @param fulfilment
     * @param consignment
     * @param orderModel
     */
    private void populateNonDigitalDeliveryMode(final Fulfilment fulfilment, final TargetConsignmentModel consignment,
            final OrderModel orderModel) {
        final FluentFulfilmentType fluentFulfilmentType = FluentFulfilmentType.get(fulfilment.getFulfilmentType());
        final FluentDeliveryType fluentDeliveryType = FluentDeliveryType.get(fulfilment.getDeliveryType());
        final SalesApplication salesApplication = orderModel.getSalesApplication();
        final String deliveryMode = this.mapDeliveryMode(fluentFulfilmentType, fluentDeliveryType, salesApplication);
        if (StringUtils.isEmpty(deliveryMode)) {
            throw new ConversionException(
                    MessageFormat.format(FLUENT_CONSIGNMENT_POPULATION_ERROR, " cannot map to any delivery mode",
                            orderModel.getCode(), fulfilment.getStatus(), fulfilment.getOccurredOn(),
                            fulfilment.getFulfilmentId()));
        }
        try {
            final DeliveryModeModel deliveryModeModel = zoneDeliveryModeService.getDeliveryModeForCode(deliveryMode);
            consignment.setDeliveryMode(deliveryModeModel);
        }
        catch (final Exception e) {
            throw new ConversionException(MessageFormat.format(FLUENT_CONSIGNMENT_POPULATION_ERROR,
                    " cannot find delivery mode by code=" + deliveryMode,
                    orderModel.getCode(), fulfilment.getStatus(), fulfilment.getOccurredOn(),
                    fulfilment.getFulfilmentId()), e);
        }

    }

    /**
     * @param fluentFulfilmentType
     * @param fluentDeliveryType
     * @param salesApplication
     * @return deliveryMode
     */
    private String mapDeliveryMode(final FluentFulfilmentType fluentFulfilmentType,
            final FluentDeliveryType fluentDeliveryType, final SalesApplication salesApplication) {
        String deliveryMode = null;
        if (WEB_SALES_APPLICATIONS.contains(salesApplication)) {
            if (isHomeDelivery(fluentFulfilmentType, fluentDeliveryType)) {
                deliveryMode = DeliveryMode.HOME_DELIVERY;
            }
            else if (isExpressDelivery(fluentFulfilmentType, fluentDeliveryType)) {
                deliveryMode = DeliveryMode.EXPRESS_DELIVERY;
            }
            else if (isCnc(fluentFulfilmentType, fluentDeliveryType)) {
                deliveryMode = DeliveryMode.CLICK_AND_COLLECT;
            }
        }
        else if (SalesApplication.EBAY.equals(salesApplication)) {
            if (isHomeDelivery(fluentFulfilmentType, fluentDeliveryType)) {
                deliveryMode = DeliveryMode.EBAY_HOME_DELIVERY;
            }
            else if (isExpressDelivery(fluentFulfilmentType, fluentDeliveryType)) {
                deliveryMode = DeliveryMode.EBAY_EXPRESS_DELIVERY;
            }
            else if (isCnc(fluentFulfilmentType, fluentDeliveryType)) {
                deliveryMode = DeliveryMode.EBAY_CLICK_COLLECT;
            }
        }
        return deliveryMode;
    }

    /**
     * @param items
     * @param consignment
     * @param orderModel
     */
    protected void populateConsignmentEntries(final List<FulfilmentItem> items,
            final TargetConsignmentModel consignment,
            final AbstractOrderModel orderModel) {
        if (CollectionUtils.isNotEmpty(consignment.getConsignmentEntries())) {
            // entries already exists, don't try to recreate
            if (CollectionUtils.isNotEmpty(items)) {
                updateConsignmentEntries(items, consignment, orderModel);
            }
            return;
        }
        consignment.setConsignmentEntries(new HashSet<ConsignmentEntryModel>());
        if (CollectionUtils.isNotEmpty(items)) {
            for (final FulfilmentItem item : items) {
                final ConsignmentEntryModel entry = createConsignmentEntryModelByFulfilmentItem(item, orderModel,
                        consignment);
                if (entry != null) {
                    consignment.getConsignmentEntries().add(entry);
                }
            }
        }
    }

    /**
     * @param items
     * @param consignment
     * @param orderModel
     */
    protected void updateConsignmentEntries(final List<FulfilmentItem> items,
            final TargetConsignmentModel consignment,
            final AbstractOrderModel orderModel) {
        for (final FulfilmentItem item : items) {
            updateConsignmentEntry(item, orderModel, consignment);
        }
    }

    /**
     * @param item
     * @param orderModel
     * @param consignment
     */
    protected void updateConsignmentEntry(final FulfilmentItem item,
            final AbstractOrderModel orderModel, final TargetConsignmentModel consignment) {
        final AbstractOrderEntryModel orderEntry = targetOrderEntryService
                .getOrderEntryByProductCode(orderModel, item.getOrderItemRef());
        for (final ConsignmentEntryModel entry : consignment.getConsignmentEntries()) {
            if (entry.getOrderEntry().equals(orderEntry)) {
                final int filledQty = item.getFilledQty() != null ? item.getFilledQty().intValue() : 0;
                entry.setShippedQuantity(Long.valueOf(filledQty));
            }
            modelService.save(entry);
        }
    }

    /**
     * @param item
     * @param orderModel
     * @param consignment
     * @return {@link ConsignmentEntryModel}
     */
    private ConsignmentEntryModel createConsignmentEntryModelByFulfilmentItem(final FulfilmentItem item,
            final AbstractOrderModel orderModel, final TargetConsignmentModel consignment) {
        ConsignmentEntryModel entry = null;
        if (item != null && StringUtils.isNotEmpty(item.getOrderItemRef())) {
            final AbstractOrderEntryModel orderEntry = targetOrderEntryService
                    .getOrderEntryByProductCode(orderModel, item.getOrderItemRef());
            if (orderEntry != null) {
                entry = modelService.create(ConsignmentEntryModel.class);
                entry.setConsignment(consignment);
                entry.setOrderEntry(orderEntry);
                final int quantity = item.getRequestedQty() != null ? item.getRequestedQty().intValue() : 0;
                final int filledQty = item.getFilledQty() != null ? item.getFilledQty().intValue() : 0;
                entry.setQuantity(Long.valueOf(quantity));
                entry.setShippedQuantity(Long.valueOf(filledQty));
            }
        }
        return entry;
    }

    /**
     * @param orderModel
     * @param items
     * @return whether fulfilment contains digital item
     */
    private boolean containsDigitalItem(final AbstractOrderModel orderModel, final List<FulfilmentItem> items) {
        boolean result = false;
        if (CollectionUtils.isNotEmpty(items)) {
            for (final FulfilmentItem item : items) {
                if (item != null && StringUtils.isNotEmpty(item.getOrderItemRef())
                        && this.isProductTypeDigitalByFulfilmentItem(item, orderModel)) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * @param orderModel
     * @param items
     * @return whether fulfilment contains only digital items
     */
    private boolean containsAllDigitalItem(final AbstractOrderModel orderModel, final List<FulfilmentItem> items) {
        boolean result = true;
        if (CollectionUtils.isNotEmpty(items)) {
            for (final FulfilmentItem item : items) {
                if (item != null && StringUtils.isNotEmpty(item.getOrderItemRef())
                        && !this.isProductTypeDigitalByFulfilmentItem(item, orderModel)) {
                    result = false;
                    break;
                }
            }
        }
        else {
            result = false;
        }
        return result;
    }

    /**
     * @param fulfilment
     * @param consignment
     */
    protected void populateRejectReason(final Fulfilment fulfilment, final TargetConsignmentModel consignment) {
        if (FluentFulfilmentStatus.EXPIRED.equals(fulfilment.getStatus())
                && fulfilment.getFromAddress() != null) {
            consignment.setRejectReason(ConsignmentRejectReason.EXPIRED);
        }
        else if (FluentFulfilmentStatus.SYSTEM_REJECTED.equals(fulfilment.getStatus())) {
            consignment.setRejectReason(ConsignmentRejectReason.SYSTEM_REJECT);
        }
        else if (FluentFulfilmentStatus.REJECTED.equals(fulfilment.getStatus())) {
            consignment.setRejectReason(getReasonForRejectStatus(consignment));
        }
        else if (FluentFulfilmentStatus.SYSTEM_EXPIRED.equals(fulfilment.getStatus())) {
            consignment.setRejectReason(ConsignmentRejectReason.SYSTEM_EXPIRED);
        }
        else if (FluentFulfilmentStatus.CANCELLED_NO_STOCK.equals(fulfilment.getStatus())) {
            consignment.setRejectReason(ConsignmentRejectReason.UNIT_OUT_OF_STOCK_AT_LOCATION);
        }
    }

    /**
     * @param fulfilment
     * @param consignment
     * @return target carrier name code
     */
    private String getTargetCarrierCode(final Fulfilment fulfilment, final TargetConsignmentModel consignment) {
        return FluentCarrierMap.getTargetCarrierName(fulfilment.getCarrierName(),
                getLocationKeyForCarrierMap(consignment),
                consignment.getDeliveryMode().getCode());
    }

    /**
     * 
     * @param consignment
     * @return 'store' if warehouseVendor is store otherwise 'non-store'
     */
    private String getLocationKeyForCarrierMap(final TargetConsignmentModel consignment) {
        if (isWarehouseVendorStore(consignment.getWarehouse().getVendor().getCode())) {
            return FluentCarrierMap.STORE;
        }
        else {
            return FluentCarrierMap.NON_STORE;
        }
    }

    /**
     * @param orderModel
     * @param items
     * @return {@link TargetZoneDeliveryModeModel}
     */
    private TargetZoneDeliveryModeModel getDigitalDeliveryModeFromItems(final AbstractOrderModel orderModel,
            final List<FulfilmentItem> items) {
        TargetZoneDeliveryModeModel digitalDeliveryMode = null;
        if (CollectionUtils.isEmpty(items)) {
            return digitalDeliveryMode;
        }

        for (final FulfilmentItem item : items) {
            if (item != null && StringUtils.isNotEmpty(item.getOrderItemRef())
                    && this.isProductTypeDigitalByFulfilmentItem(item, orderModel)) {
                digitalDeliveryMode = targetDeliveryModeHelper.getDigitalDeliveryMode(targetOrderEntryService
                        .getOrderEntryByProductCode(orderModel, item.getOrderItemRef()).getProduct());
                if (digitalDeliveryMode != null) {
                    break;
                }
            }
        }
        return digitalDeliveryMode;
    }

    /**
     * @param item
     * @param orderModel
     * @return whether fulfilment item contains digital product
     */
    private boolean isProductTypeDigitalByFulfilmentItem(final FulfilmentItem item,
            final AbstractOrderModel orderModel) {
        final AbstractOrderEntryModel orderEntry = targetOrderEntryService
                .getOrderEntryByProductCode(orderModel, item.getOrderItemRef());
        return ProductUtil.isProductTypeDigital(orderEntry.getProduct());
    }

    /**
     * @param fluentFulfilmentType
     * @param fluentDeliveryType
     * @return is fulfilment home delivery
     */
    private boolean isHomeDelivery(final FluentFulfilmentType fluentFulfilmentType,
            final FluentDeliveryType fluentDeliveryType) {
        return FluentDeliveryType.STANDARD.equals(fluentDeliveryType)
                && FluentFulfilmentType.HOME_DELIVERY_TYPE.contains(fluentFulfilmentType);
    }

    /**
     * @param fluentFulfilmentType
     * @param fluentDeliveryType
     * @return is fulfilment cnc
     */
    private boolean isCnc(final FluentFulfilmentType fluentFulfilmentType,
            final FluentDeliveryType fluentDeliveryType) {
        return FluentDeliveryType.STANDARD.equals(fluentDeliveryType)
                && FluentFulfilmentType.CC_TYPE.contains(fluentFulfilmentType);
    }

    /**
     * @param fluentFulfilmentType
     * @param fluentDeliveryType
     * @return is fulfilment express delivery
     */
    private boolean isExpressDelivery(final FluentFulfilmentType fluentFulfilmentType,
            final FluentDeliveryType fluentDeliveryType) {
        return FluentDeliveryType.EXPRESS.equals(fluentDeliveryType)
                && FluentFulfilmentType.HOME_DELIVERY_TYPE.contains(fluentFulfilmentType);
    }

    /**
     * @param targetConsignment
     * @param consignmentStatus
     * @return true if status is PICKED and picked date is null
     */
    private boolean isStatusPickedAndPickDateNull(final TargetConsignmentModel targetConsignment,
            final ConsignmentStatus consignmentStatus) {
        return ConsignmentStatus.PICKED.equals(consignmentStatus)
                && targetConsignment.getPickConfirmDate() == null;
    }

    /**
     * @param targetConsignment
     * @param consignmentStatus
     * @return true if status is CANCELLED and cancelled date is null
     */
    private boolean isStatusCancelledAndCancelledDateNull(final TargetConsignmentModel targetConsignment,
            final ConsignmentStatus consignmentStatus) {
        return ConsignmentStatus.CANCELLED.equals(consignmentStatus)
                && targetConsignment.getCancelDate() == null;
    }

    /**
     * @param targetConsignment
     * @param consignmentStatus
     * @return true is status is SHIPPED and shipped date is null
     */
    private boolean isStatusShippedAndShippedDateNull(final TargetConsignmentModel targetConsignment,
            final ConsignmentStatus consignmentStatus) {
        return ConsignmentStatus.SHIPPED.equals(consignmentStatus)
                && targetConsignment.getShippingDate() == null;
    }

    /**
     * @param targetConsignment
     * @param consignmentStatus
     * @return true if status is PACKED and packed date is null
     */
    private boolean isStatusPackedAndPackedDateNull(final TargetConsignmentModel targetConsignment,
            final ConsignmentStatus consignmentStatus) {
        return ConsignmentStatus.PACKED.equals(consignmentStatus) && targetConsignment.getPackedDate() == null;
    }

    /**
     * @param targetConsignment
     * @param consignmentStatus
     * @return true if status is WAVED and waved date is null
     */
    private boolean isStatusWavedAndWavedDateNull(final TargetConsignmentModel targetConsignment,
            final ConsignmentStatus consignmentStatus) {
        return ConsignmentStatus.WAVED.equals(consignmentStatus)
                && targetConsignment.getWavedDate() == null;
    }

    /**
     * @param consignmentStatus
     * @return true if status is CONFIRMED_BY_WAREHOUSE or SENT_TO_WAREHOUSE
     */
    private boolean isStatusConfirmedOrSentToWarehouse(final ConsignmentStatus consignmentStatus) {
        return ConsignmentStatus.CONFIRMED_BY_WAREHOUSE.equals(consignmentStatus)
                || ConsignmentStatus.SENT_TO_WAREHOUSE.equals(consignmentStatus);
    }

    /**
     * 
     * @param fulfilment
     * @param targetConsignment
     * @return true if consignment status is not null and occurredOn is present
     */
    private boolean isConsignmentStatusAndOccurredOnDateValid(final Fulfilment fulfilment,
            final TargetConsignmentModel targetConsignment) {

        if (targetConsignment.getStatus() == null) {
            throw new ConversionException(
                    MessageFormat.format(FLUENT_CONSIGNMENT_POPULATION_ERROR, " consignmentStatus is null",
                            targetConsignment.getOrder().getCode(),
                            null, fulfilment.getOccurredOn(), fulfilment.getFulfilmentId()));
        }
        else if (fulfilment.getOccurredOn() == null) {
            LOG.warn(MessageFormat.format(FLUENT_CONSIGNMENT_POPULATION_WARN, " occurredOn date is null",
                    targetConsignment.getOrder().getCode(),
                    targetConsignment.getStatus().getCode(), null, fulfilment.getFulfilmentId()));
            return false;
        }
        return true;
    }

    /**
     * 
     * @param vendorCode
     * @return true if vendor code equals Target
     */
    private boolean isWarehouseVendorStore(final String vendorCode) {
        return Vendor.Target.equals(Vendor.valueOf(vendorCode));
    }

    @Required
    public void setTargetOrderEntryService(final TargetOrderEntryService targetOrderEntryService) {
        this.targetOrderEntryService = targetOrderEntryService;
    }

    @Required
    public void setTargetDeliveryModeHelper(final TargetDeliveryModeHelper targetDeliveryModeHelper) {
        this.targetDeliveryModeHelper = targetDeliveryModeHelper;
    }

    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    @Required
    public void setZoneDeliveryModeService(final ZoneDeliveryModeService zoneDeliveryModeService) {
        this.zoneDeliveryModeService = zoneDeliveryModeService;
    }

    @Required
    public void setFluentCarrierDao(final FluentCarrierDao fluentCarrierDao) {
        this.fluentCarrierDao = fluentCarrierDao;
    }

}

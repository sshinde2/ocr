/**
 * 
 */
package au.com.target.tgtfluent.converter.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.MessageFormat;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfluent.data.Fulfilment;
import au.com.target.tgtfluent.util.FluentDataMap;


/**
 * @author bpottass
 *
 */
public class FluentConsignmentWarehouseReversePopulator implements Populator<Fulfilment, TargetConsignmentModel> {

    private static final Logger LOG = Logger.getLogger(FluentConsignmentWarehouseReversePopulator.class);

    private static final String FLUENT_CONSIGNMENT_WAREHOUSE_REVERSE_POPULATION_ERROR = "FLUENT_CONSIGNMENT_WAREHOUSE_REVERSE_POPULATION_ERROR: message={0}, orderCode={1}, fulfilmentId={2},locationRef={3}";

    private TargetWarehouseService targetWarehouseService;

    private TargetPointOfServiceService targetPointOfServiceService;

    @Override
    public void populate(final Fulfilment fulfilment, final TargetConsignmentModel consignment)
            throws ConversionException {
        final String orderCode = consignment.getOrder().getCode();
        this.populateWarehouse(fulfilment, consignment, orderCode);
    }

    /**
     * @param fulfilment
     * @param consignment
     * @param orderCode
     */
    protected void populateWarehouse(final Fulfilment fulfilment, final TargetConsignmentModel consignment,
            final String orderCode) {
        if (consignment.getWarehouse() != null) {
            // Warehouse is not editable
            return;
        }

        if (this.isCancellationWarehouse(fulfilment)) {
            //set cancellation warehouse if fromAddress is null, which means this is a system level reject.
            consignment
                    .setWarehouse(targetWarehouseService.getWarehouseForCode(TgtCoreConstants.CANCELLATION_WAREHOUSE));
        }
        else {
            final WarehouseModel warehouseModel = warehouseFromLocationRefFinder(fulfilment, orderCode);
            consignment.setWarehouse(warehouseModel);
        }
    }

    /**
     * @param fulfilment
     * @param orderCode
     * @return warehouseModel
     */
    private WarehouseModel warehouseFromLocationRefFinder(final Fulfilment fulfilment, final String orderCode) {
        final String locationRef = fulfilment.getFromAddress().getLocationRef();
        WarehouseModel warehouseModel = null;
        try {
            warehouseModel = targetWarehouseService.getWarehouseForFluentLocationRef(locationRef);
        }
        catch (final Exception e) {
            // locationRef is not a warehouse, find the warehouse by targetPointOfService
            try {
                warehouseModel = targetPointOfServiceService.getWarehouseByStoreNumber(Integer.valueOf(locationRef));
            }
            catch (final NumberFormatException | TargetUnknownIdentifierException
                    | TargetAmbiguousIdentifierException e1) {
                LOG.error(MessageFormat.format(FLUENT_CONSIGNMENT_WAREHOUSE_REVERSE_POPULATION_ERROR,
                        " Cannot find the warehouse model",
                        orderCode, fulfilment.getFulfilmentId(), locationRef, fulfilment.getFulfilmentId()));
            }
        }
        if (warehouseModel == null) {
            throw new ConversionException(MessageFormat.format(FLUENT_CONSIGNMENT_WAREHOUSE_REVERSE_POPULATION_ERROR,
                    " Cannot find the warehouse model",
                    orderCode, fulfilment.getFulfilmentId(), locationRef, fulfilment.getFulfilmentId()));
        }
        return warehouseModel;
    }

    /**
     * @param fulfilment
     * @return is fulfilment cancelled
     */
    private boolean isCancellationWarehouse(final Fulfilment fulfilment) {
        return FluentDataMap.SYSTEM_REFUND_STATUS.contains(fulfilment.getStatus());
    }

    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }

    @Required
    public void setTargetPointOfServiceService(final TargetPointOfServiceService targetPointOfServiceService) {
        this.targetPointOfServiceService = targetPointOfServiceService;
    }
}

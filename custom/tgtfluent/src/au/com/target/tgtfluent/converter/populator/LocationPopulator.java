/**
 * 
 */
package au.com.target.tgtfluent.converter.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.storelocator.model.OpeningDayModel;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetOpeningDayModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfluent.data.Address;
import au.com.target.tgtfluent.data.Location;
import au.com.target.tgtfluent.data.LocationStatus;
import au.com.target.tgtfluent.data.LocationType;
import au.com.target.tgtfluent.data.OpeningHours;


/**
 * @author mgazal
 *
 */
public class LocationPopulator implements Populator<TargetPointOfServiceModel, Location> {

    private static final Logger LOG = Logger.getLogger(LocationPopulator.class);

    private static final String COMPANY_NAME = "Target";

    private static final String CLOSED_HOURS = "-1";

    private final DateTimeFormatter openingHoursFormatter = DateTimeFormat.forPattern("HHmm");

    private String defaultEmail;

    private String defaultPhone;

    private String defaultCountry;

    private OpeningHours defaultOpeningHours;

    private Map<String, String> timezoneStateMap;

    private Map<String, String> timezoneCityMap;

    @Override
    public void populate(final TargetPointOfServiceModel source, final Location target) throws ConversionException {
        target.setLocationRef(source.getStoreNumber().toString());
        target.setStatus(LocationStatus.get(source).name());
        target.setName(source.getName());
        target.setEmail(source.getAddress() != null
                ? StringUtils.defaultIfBlank(source.getAddress().getEmail(), defaultEmail) : defaultEmail);
        target.setSupportPhone(source.getAddress() != null
                ? StringUtils.defaultIfBlank(source.getAddress().getPhone1(), defaultPhone) : defaultPhone);
        target.setType(LocationType.get(source).name());
        if (source.getAddress() != null) {
            target.setTimeZone(StringUtils.defaultIfBlank(timezoneCityMap.get(source.getAddress().getTown()),
                    timezoneStateMap.get(source.getAddress().getDistrict())));
        }
        target.setAddress(getAddress(source));
        target.setOpeningHours(getOpeningHours(source));
    }

    protected Address getAddress(final TargetPointOfServiceModel source) {
        final Address address = new Address();
        address.setLocationRef(source.getStoreNumber().toString());
        address.setCompanyName(COMPANY_NAME);
        address.setName(source.getName());
        if (source.getAddress() != null) {
            address.setCity(source.getAddress().getTown());
            address.setCountry(
                    source.getAddress().getCountry() != null ? source.getAddress().getCountry().getName()
                            : defaultCountry);
            address.setPostcode(source.getAddress().getPostalcode());
            address.setState(source.getAddress().getDistrict());
            address.setStreet(source.getAddress().getStreetname());
        }
        address.setLatitude(source.getLatitude() != null ? source.getLatitude().toString() : null);
        address.setLongitude(source.getLongitude() != null ? source.getLongitude().toString() : null);
        return address;
    }

    /**
     * Get OpeningHours object from pointOfService OpeningSchedule
     * 
     * @param source
     * @return {@link OpeningHours}
     */
    protected OpeningHours getOpeningHours(final TargetPointOfServiceModel source) {
        OpeningHours openingHours;
        if (source.getOpeningSchedule() == null
                || CollectionUtils.isEmpty(source.getOpeningSchedule().getOpeningDays())) {
            LOG.info(MessageFormat.format("FLUENT: openingHours missing for store:{0} with status:{1}",
                    source.getStoreNumber().toString(), LocationStatus.get(source).name()));
            openingHours = defaultOpeningHours;
        }
        else {
            openingHours = new OpeningHours();
            for (final OpeningDayModel openingDay : getFilteredOpeningDays(source.getOpeningSchedule())) {
                setDayMins(openingHours, openingDay);
            }
        }
        return openingHours;
    }

    /**
     * @param schedule
     * @return List of {@link OpeningDayModel}
     */
    protected List<OpeningDayModel> getFilteredOpeningDays(final OpeningScheduleModel schedule) {
        List<OpeningDayModel> openingDays = new ArrayList<>(schedule.getOpeningDays());
        Collections.sort(openingDays, new Comparator<OpeningDayModel>() {

            @Override
            public int compare(final OpeningDayModel o1, final OpeningDayModel o2) {
                return o1.getOpeningTime().compareTo(o2.getOpeningTime());
            }
        });

        final Date now = getNow();
        final OpeningDayModel futureOpeningDay = (OpeningDayModel)CollectionUtils.find(openingDays, new Predicate() {

            @Override
            public boolean evaluate(final Object arg0) {
                return ((OpeningDayModel)arg0).getOpeningTime().after(now);
            }
        });

        if (futureOpeningDay == null) {
            openingDays = openingDays.subList(openingDays.size() - 7, openingDays.size());
        }
        else {
            final int futureIndex = openingDays.indexOf(futureOpeningDay);
            if (futureIndex + 7 > openingDays.size()) {
                openingDays = openingDays.subList(openingDays.size() - 7, openingDays.size());
            }
            else {
                openingDays = openingDays.subList(futureIndex, futureIndex + 7);
            }
        }

        return openingDays;
    }

    protected void setDayMins(final OpeningHours openingHours, final OpeningDayModel openingDay) {
        final String startMin;
        final String endMin;

        if (openingDay instanceof TargetOpeningDayModel && ((TargetOpeningDayModel)openingDay).isClosed()) {
            startMin = CLOSED_HOURS;
            endMin = CLOSED_HOURS;
        }
        else {
            startMin = getMin(openingDay.getOpeningTime());
            endMin = getMin(openingDay.getClosingTime());
        }

        final DateTime dateTime = new DateTime(openingDay.getOpeningTime().getTime());
        switch (dateTime.getDayOfWeek()) {
            case DateTimeConstants.MONDAY:
                openingHours.setMonStartMin(startMin);
                openingHours.setMonEndMin(endMin);
                break;
            case DateTimeConstants.TUESDAY:
                openingHours.setTueStartMin(startMin);
                openingHours.setTueEndMin(endMin);
                break;
            case DateTimeConstants.WEDNESDAY:
                openingHours.setWedStartMin(startMin);
                openingHours.setWedEndMin(endMin);
                break;
            case DateTimeConstants.THURSDAY:
                openingHours.setThuStartMin(startMin);
                openingHours.setThuEndMin(endMin);
                break;
            case DateTimeConstants.FRIDAY:
                openingHours.setFriStartMin(startMin);
                openingHours.setFriEndMin(endMin);
                break;
            case DateTimeConstants.SATURDAY:
                openingHours.setSatStartMin(startMin);
                openingHours.setSatEndMin(endMin);
                break;
            case DateTimeConstants.SUNDAY:
                openingHours.setSunStartMin(startMin);
                openingHours.setSunEndMin(endMin);
                break;
            default:
                break;
        }
    }

    /**
     * @return now
     */
    protected Date getNow() {
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return calendar.getTime();
    }

    /**
     * Gets opening or closing hours in fluent minutes syntax
     * 
     * @param time
     * @return minutes
     */
    protected String getMin(final Date time) {
        return openingHoursFormatter.print(time.getTime());
    }

    /**
     * @param defaultEmail
     *            the defaultEmail to set
     */
    @Required
    public void setDefaultEmail(final String defaultEmail) {
        this.defaultEmail = defaultEmail;
    }

    /**
     * @param defaultPhone
     *            the defaultPhone to set
     */
    @Required
    public void setDefaultPhone(final String defaultPhone) {
        this.defaultPhone = defaultPhone;
    }

    /**
     * @param defaultCountry
     *            the defaultCountry to set
     */
    @Required
    public void setDefaultCountry(final String defaultCountry) {
        this.defaultCountry = defaultCountry;
    }

    /**
     * @param defaultOpeningHours
     *            the defaultOpeningHours to set
     */
    @Required
    public void setDefaultOpeningHours(final OpeningHours defaultOpeningHours) {
        this.defaultOpeningHours = defaultOpeningHours;
    }

    /**
     * @param timezoneStateMap
     *            the timezoneStateMap to set
     */
    @Required
    public void setTimezoneStateMap(final Map<String, String> timezoneStateMap) {
        this.timezoneStateMap = timezoneStateMap;
    }

    /**
     * @param timezoneCityMap
     *            the timezoneCityMap to set
     */
    @Required
    public void setTimezoneCityMap(final Map<String, String> timezoneCityMap) {
        this.timezoneCityMap = timezoneCityMap;
    }

}

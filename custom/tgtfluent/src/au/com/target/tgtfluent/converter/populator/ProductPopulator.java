/**
 * 
 */
package au.com.target.tgtfluent.converter.populator;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfluent.data.Attribute;
import au.com.target.tgtfluent.data.AttributeType;
import au.com.target.tgtfluent.data.Product;
import au.com.target.tgtfluent.data.ProductStatus;
import au.com.target.tgtfluent.exception.ProductFluentFeedException;


/**
 * @author bhuang3
 *
 */
public class ProductPopulator implements Populator<TargetProductModel, Product> {

    private static final String PRODUCT_TYPE = "PRODUCT_TYPE";

    private static final String DISPLAY_ONLY = "DISPLAY_ONLY";

    private static final String PREVIEW = "PREVIEW";

    private static final String CLICK_AND_COLLECT = "CLICK_AND_COLLECT";

    private static final String HOME_DELIVERY = "HOME_DELIVERY";

    private static final String EXPRESS_DELIVERY = "EXPRESS_DELIVERY";

    private static final String EMAIL_DELIVERY = "EMAIL_DELIVERY";

    private static final String CLICK_AND_COLLECT_CODE = "click-and-collect";

    private static final String HOME_DELIVERY_CODE = "home-delivery";

    private static final String EXPRESS_DELIVERY_CODE = "express-delivery";

    private static final String DIGITAL_DELIVERY_CODE = "digital-gift-card";

    private static final String NO_SUPER_CATEGORY_HAS_FLUENTID_ERROR_CODE = "NO SUPER CATEGORY HAS FLUENTID";

    private static final String NO_CATEGORY_HAS_FLUENTID = "Product={0} with no category has fluent id";

    @Override
    public void populate(final TargetProductModel model, final Product product)
            throws ConversionException {
        populateCategories(model, product);
        populateProduct(model, product);
    }

    /**
     * @param model
     * @param product
     */
    private void populateProduct(final TargetProductModel model, final Product product) {

        product.setName(model.getName());
        product.setProductRef(model.getCode());
        product.setStatus(ArticleApprovalStatus.APPROVED.equals(model.getApprovalStatus()) ? ProductStatus.ACTIVE
                : ProductStatus.INACTIVE);
        final List<Attribute> attributes = new ArrayList<>();
        product.setAttributes(attributes);

        final String productType = model.getProductType() != null ? model.getProductType().getCode() : null;
        populateAttribute(PRODUCT_TYPE, AttributeType.STRING, productType, attributes);
        final String displayOnly = model.getDisplayOnly() != null ? String.valueOf(model.getDisplayOnly()
                .booleanValue()).toUpperCase() : null;
        populateAttribute(DISPLAY_ONLY, AttributeType.STRING, displayOnly, attributes);
        final String preview = model.getPreview() != null ? String.valueOf(model.getPreview()
                .booleanValue()).toUpperCase() : null;
        populateAttribute(PREVIEW, AttributeType.STRING, preview, attributes);
        populateDeliveryModes(model, attributes);

    }

    /**
     * @param model
     * @param attributes
     */
    private void populateDeliveryModes(final TargetProductModel model, final List<Attribute> attributes) {
        boolean isCnc = false;
        boolean isHomeDelivery = false;
        boolean isExpressDelivery = false;
        boolean isEmailDelivery = false;

        if (CollectionUtils.isNotEmpty(model.getDeliveryModes())) {
            for (final DeliveryModeModel deliveryMode : model.getDeliveryModes()) {
                final String deliveryModeCode = deliveryMode.getCode();
                isCnc |= CLICK_AND_COLLECT_CODE.equals(deliveryModeCode);
                isHomeDelivery |= HOME_DELIVERY_CODE.equals(deliveryModeCode);
                isExpressDelivery |= EXPRESS_DELIVERY_CODE.equals(deliveryModeCode);
                isEmailDelivery |= DIGITAL_DELIVERY_CODE.equals(deliveryModeCode);
            }
        }

        populateAttribute(CLICK_AND_COLLECT, AttributeType.STRING, String.valueOf(isCnc).toUpperCase(), attributes);
        populateAttribute(HOME_DELIVERY, AttributeType.STRING, String.valueOf(isHomeDelivery).toUpperCase(),
                attributes);
        populateAttribute(EXPRESS_DELIVERY, AttributeType.STRING, String.valueOf(isExpressDelivery).toUpperCase(),
                attributes);
        populateAttribute(EMAIL_DELIVERY, AttributeType.STRING, String.valueOf(isEmailDelivery).toUpperCase(),
                attributes);
    }


    /**
     * @param model
     * @param product
     */
    private void populateCategories(final TargetProductModel model, final Product product) {
        final String categoryFluentId = model.getPrimarySuperCategory() != null
                ? model.getPrimarySuperCategory().getFluentId()
                : null;
        if (StringUtils.isBlank(categoryFluentId)) {
            throw new ProductFluentFeedException(NO_SUPER_CATEGORY_HAS_FLUENTID_ERROR_CODE, MessageFormat.format(
                    NO_CATEGORY_HAS_FLUENTID, model.getCode()));
        }
        product.setCategories(Arrays.asList(categoryFluentId));
    }

    /**
     * @param attributeName
     * @param type
     * @param value
     * @param attributes
     */
    private void populateAttribute(final String attributeName, final AttributeType type, final String value,
            final List<Attribute> attributes) {
        if (StringUtils.isNotEmpty(value)) {
            attributes.add(createAttribute(attributeName, type, value));
        }
    }

    /**
     * @param name
     * @param type
     * @param value
     * @return {@link Attribute}
     */
    private Attribute createAttribute(final String name, final AttributeType type, final String value) {
        final Attribute attribute = new Attribute();
        attribute.setName(name);
        attribute.setType(type);
        attribute.setValue(value);
        return attribute;
    }
}

/**
 * 
 */
package au.com.target.tgtfluent.converter.populator;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfluent.data.Attribute;
import au.com.target.tgtfluent.data.AttributeType;
import au.com.target.tgtfluent.data.Reference;
import au.com.target.tgtfluent.data.Sku;
import au.com.target.tgtfluent.data.SkuStatus;
import au.com.target.tgtutility.util.TargetDateUtil;
import au.com.target.tgtutility.util.TimeZoneUtils;


/**
 * @author mgazal
 *
 */
public class SkuPopulator implements Populator<AbstractTargetVariantProductModel, Sku> {

    private static final String LARGE_MEDIA = "/large/";

    private static final String BARCODE = "BARCODE";

    private static final String SWATCH = "SWATCH";

    private static final String SIZE = "SIZE";

    private static final String DEPARTMENT = "DEPARTMENT";

    private static final String ASSORTED = "ASSORTED";

    private static final String MERCH_PRODUCT_STATUS = "MERCH_PRODUCT_STATUS";

    private static final String WEIGHT = "WEIGHT";

    private static final String HEIGHT = "HEIGHT";

    private static final String LENGTH = "LENGTH";

    private static final String WIDTH = "WIDTH";

    private static final String EBAY = "EBAY";

    private static final String DENOMINATION = "DENOMINATION";

    private static final String BRAND_ID = "BRAND_ID";

    private static final String GIFT_CARD_STYLE = "GIFT_CARD_STYLE";

    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    private String imageBaseUrl;

    @Override
    public void populate(final AbstractTargetVariantProductModel source, final Sku target) throws ConversionException {
        target.setSkuRef(source.getCode());
        final TargetProductModel baseProduct = getBaseProduct(source);
        target.setProductRef(baseProduct.getCode());
        target.setStatus(getSkuStatus(source));
        target.setImageUrlRef(getImageUrlRef(source));
        target.setName(source.getName());
        target.setReferences(getReferences(source));
        target.setAttributes(getAttributes(source, baseProduct));
    }

    /**
     * @param source
     * @return baseProductCode
     */
    protected TargetProductModel getBaseProduct(final AbstractTargetVariantProductModel source) {
        if (source.getBaseProduct() instanceof AbstractTargetVariantProductModel) {
            return getBaseProduct((AbstractTargetVariantProductModel)source.getBaseProduct());
        }
        if (source.getBaseProduct() instanceof TargetProductModel) {
            return (TargetProductModel)source.getBaseProduct();
        }
        return null;
    }

    /**
     * @param source
     * @return {@link SkuStatus}
     */
    protected SkuStatus getSkuStatus(final AbstractTargetVariantProductModel source) {
        if (ArticleApprovalStatus.APPROVED.equals(source.getApprovalStatus())) {
            return SkuStatus.ACTIVE;
        }
        return SkuStatus.INACTIVE;
    }

    /**
     * @param source
     * @return imageUrlRef
     */
    protected String getImageUrlRef(final AbstractTargetVariantProductModel source) {
        if (CollectionUtils.isNotEmpty(source.getGalleryImages())
                && CollectionUtils.isNotEmpty(source.getGalleryImages().get(0).getMedias())) {
            for (final MediaModel media : source.getGalleryImages().get(0).getMedias()) {
                if (media != null && media.getCode() != null && media.getCode().startsWith(LARGE_MEDIA)) {
                    return imageBaseUrl + media.getURL();
                }
            }
        }
        else if (source.getBaseProduct() instanceof AbstractTargetVariantProductModel) {
            return getImageUrlRef((AbstractTargetVariantProductModel)source.getBaseProduct());
        }
        return null;
    }

    /**
     * @param source
     * @return references
     */
    protected List<Reference> getReferences(final AbstractTargetVariantProductModel source) {
        if (StringUtils.isNotBlank(source.getEan())) {
            final Reference reference = new Reference();
            reference.setType(BARCODE);
            reference.setValue(source.getEan());
            return Arrays.asList(reference);
        }
        return Collections.emptyList();
    }

    /**
     * @param source
     * @param product
     * @return attributes
     */
    protected List<Attribute> getAttributes(final AbstractTargetVariantProductModel source,
            final TargetProductModel product) {
        final List<Attribute> attributes = new ArrayList<>();

        populateAttribute(attributes, SWATCH, AttributeType.STRING, getSwatch(source));

        if (source instanceof TargetSizeVariantProductModel) {
            populateAttribute(attributes, SIZE, AttributeType.STRING,
                    ((TargetSizeVariantProductModel)source).getSize());
        }

        if (product.getMerchDepartment() != null) {
            populateAttribute(attributes, DEPARTMENT, AttributeType.STRING, product.getMerchDepartment().getName());
        }
        populateAttribute(attributes, ASSORTED, AttributeType.STRING, getAssorted(source));
        populateAttribute(attributes, MERCH_PRODUCT_STATUS, AttributeType.STRING, source.getMerchProductStatus());

        if (source.getProductPackageDimensions() != null) {
            populateAttribute(attributes, WEIGHT, AttributeType.STRING,
                    getDimensionValue(source.getProductPackageDimensions().getWeight()));
            populateAttribute(attributes, HEIGHT, AttributeType.STRING,
                    getDimensionValue(source.getProductPackageDimensions().getHeight()));
            populateAttribute(attributes, LENGTH, AttributeType.STRING,
                    getDimensionValue(source.getProductPackageDimensions().getLength()));
            populateAttribute(attributes, WIDTH, AttributeType.STRING,
                    getDimensionValue(source.getProductPackageDimensions().getWidth()));
        }

        populateAttribute(attributes, EBAY, AttributeType.STRING, String.valueOf(source.getAvailableOnEbay())
                .toUpperCase());
        //poplates preorder attributes(Start,End,normal sales date
        populatePreOrderAttributes(source, attributes);

        if (source.getProductType() != null) {
            final String productType = source.getProductType().getName();
            // if it is gift card, add DENOMINATION BRAND_ID and GIFT_CARD_STYLE attributes!  
            if (productType != null
                    && (TgtCoreConstants.DIGITAL.equalsIgnoreCase(productType)
                            || TgtCoreConstants.PHYSICAL_GIFTCARD.equalsIgnoreCase(productType))) {
                final String denomination = getDenomination(source);
                populateGiftCardAttribute(attributes, DENOMINATION, AttributeType.STRING, denomination);

                final String brandId = getBrandId(product, productType);
                populateGiftCardAttribute(attributes, BRAND_ID, AttributeType.STRING, brandId);

                final String giftCardStyle = getGiftCardStyle(source);
                populateGiftCardAttribute(attributes, GIFT_CARD_STYLE, AttributeType.STRING, giftCardStyle);
            }
        }

        return attributes;
    }

    /**
     * @param attributes
     * @param attributeName
     * @param type
     * @param value
     */
    private void populateAttribute(final List<Attribute> attributes, final String attributeName,
            final AttributeType type, final Object value) {
        if (value != null) {
            attributes.add(createAttribute(attributeName, type, value));
        }

    }

    /**
     * GiftCard Attribute
     * 
     * @param attributes
     * @param attributeName
     * @param type
     * @param value
     */

    private void populateGiftCardAttribute(final List<Attribute> attributes, final String attributeName,
            final AttributeType type, final Object value) {
        if (value != null && StringUtils.isNotBlank(value.toString())) {
            attributes.add(createAttribute(attributeName, type, value));
        }
        else {
            attributes.add(createAttribute(attributeName, type, StringUtils.EMPTY));
        }
    }

    /**
     * Populate the Preorder date attributes
     * 
     * @param variant
     * @param attributes
     */
    private void populatePreOrderAttributes(final AbstractTargetVariantProductModel variant,
            final List<Attribute> attributes) {

        TargetColourVariantProductModel colourVariant;
        if (variant instanceof TargetSizeVariantProductModel) {
            colourVariant = (TargetColourVariantProductModel)variant.getBaseProduct();
        }
        else {
            colourVariant = (TargetColourVariantProductModel)variant;
        }
        if (null != colourVariant) {
            populateAttribute(attributes, "PREORDER_START_DATE", AttributeType.STRING,
                    TargetDateUtil.getDateAsString(colourVariant.getPreOrderStartDateTime(), DATE_FORMAT,
                            TimeZoneUtils.TIMEZONE_UTC));
            populateAttribute(attributes, "PREORDER_END_DATE", AttributeType.STRING,
                    TargetDateUtil.getDateAsString(colourVariant.getPreOrderEndDateTime(), DATE_FORMAT,
                            TimeZoneUtils.TIMEZONE_UTC));
            populateAttribute(attributes, "SALE_START_DATE", AttributeType.STRING,
                    TargetDateUtil.getDateAsString(colourVariant.getNormalSaleStartDateTime(), DATE_FORMAT,
                            TimeZoneUtils.TIMEZONE_UTC));
        }
    }

    /**
     * @param name
     * @param type
     * @param value
     * @return {@link Attribute}
     */
    private Attribute createAttribute(final String name, final AttributeType type, final Object value) {
        final Attribute attribute = new Attribute();
        attribute.setName(name);
        attribute.setType(type);
        attribute.setValue(value);
        return attribute;
    }

    /**
     * @param source
     * @return swatch
     */
    protected String getSwatch(final AbstractTargetVariantProductModel source) {
        if (source.getBaseProduct() instanceof TargetColourVariantProductModel) {
            return getSwatch((AbstractTargetVariantProductModel)source.getBaseProduct());
        }
        if (source instanceof TargetColourVariantProductModel) {
            return ((TargetColourVariantProductModel)source).getSwatchName();
        }
        return null;
    }

    /**
     * @param source
     * @return assorted
     */
    protected String getAssorted(final AbstractTargetVariantProductModel source) {
        if (source.getBaseProduct() instanceof TargetColourVariantProductModel) {
            return getAssorted((AbstractTargetVariantProductModel)source.getBaseProduct());
        }
        if (source instanceof TargetColourVariantProductModel) {
            return String.valueOf(((TargetColourVariantProductModel)source).isAssorted()).toUpperCase();
        }
        return null;
    }

    /**
     * @param source
     * @return giftCardStyle
     */
    protected String getGiftCardStyle(final AbstractTargetVariantProductModel source) {
        if (source.getBaseProduct() instanceof TargetColourVariantProductModel) {
            return getGiftCardStyle((AbstractTargetVariantProductModel)source.getBaseProduct());
        }
        if (source instanceof TargetColourVariantProductModel) {
            return ((TargetColourVariantProductModel)source).getGiftCardStyle();
        }
        return null;
    }

    /**
     * @param product
     * @param productType
     * @return brandId
     */
    protected String getBrandId(final TargetProductModel product, final String productType) {
        if (product.getGiftCard() == null) {
            throw new IllegalStateException(
                    "missing [" + TargetProductModel.GIFTCARD + "] in product of type [" + productType + "]");
        }
        return product.getGiftCard().getBrandId();
    }

    /**
     * @param source
     * @return denomination
     */
    protected String getDenomination(final AbstractTargetVariantProductModel source) {
        if (source instanceof TargetSizeVariantProductModel) {
            final Double value = ((TargetSizeVariantProductModel)source).getDenomination();
            return value != null ? value.toString() : null;
        }
        return null;
    }

    /**
     * @param value
     * @return dimensionValue
     */
    protected String getDimensionValue(final Double value) {
        return value != null ? value.toString() : null;
    }

    /**
     * @param imageBaseUrl
     *            the imageBaseUrl to set
     */
    @Required
    public void setImageBaseUrl(final String imageBaseUrl) {
        this.imageBaseUrl = imageBaseUrl;
    }

}

/**
 * 
 */
package au.com.target.tgtfluent.converter.populator.factory;

import de.hybris.platform.converters.Populator;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfluent.constants.TgtFluentConstants;
import au.com.target.tgtfluent.converter.populator.FluentConsignmentPopulator;
import au.com.target.tgtfluent.converter.populator.FluentWavedConsignmentPopulator;



/**
 * @author pvarghe2
 *
 */
public class FluentConsignmentPopulatorFactory {

    private FluentConsignmentPopulator fluentConsignmentPopulator;
    private FluentWavedConsignmentPopulator fluentWavedConsignmentPopulator;

    public Populator getConsignmentPopulator(final String eventName) {
        if (TgtFluentConstants.TargetOfcEvent.OFC_WAVED.equals(eventName)) {
            return fluentWavedConsignmentPopulator;
        }
        return fluentConsignmentPopulator;
    }


    /**
     * @param fluentConsignmentPopulator
     *            the fluentBaseConsignmentPopulator to set
     */
    @Required
    public void setFluentConsignmentPopulator(final FluentConsignmentPopulator fluentConsignmentPopulator) {
        this.fluentConsignmentPopulator = fluentConsignmentPopulator;
    }

    /**
     * @param fluentWavedConsignmentPopulator
     *            the fluentWavedConsignmentPopulator to set
     */
    @Required
    public void setFluentWavedConsignmentPopulator(
            final FluentWavedConsignmentPopulator fluentWavedConsignmentPopulator) {
        this.fluentWavedConsignmentPopulator = fluentWavedConsignmentPopulator;
    }
}

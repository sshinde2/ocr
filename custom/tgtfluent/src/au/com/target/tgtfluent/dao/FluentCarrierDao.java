/**
 * 
 */
package au.com.target.tgtfluent.dao;

import de.hybris.platform.servicelayer.internal.dao.GenericDao;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;


/**
 * @author bpottass
 *
 */
public interface FluentCarrierDao extends GenericDao<TargetCarrierModel> {

    /**
     * Get the carrier using the code
     * 
     * @param carrierCode
     * @return TargetCarrierModel - carrierModel for the code
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    TargetCarrierModel getTargetCarrierByCode(final String carrierCode)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;

}

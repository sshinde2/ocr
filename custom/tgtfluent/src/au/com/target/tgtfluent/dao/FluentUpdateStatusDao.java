/**
 * 
 */
package au.com.target.tgtfluent.dao;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtfluent.model.AbstractFluentUpdateStatusModel;


/**
 * @author mgazal
 *
 */
public interface FluentUpdateStatusDao {

    /**
     * Find the UpdateStatusModel for given typecode and code
     * 
     * @param typecode
     * @param code
     * @return {@link AbstractFluentUpdateStatusModel}
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    AbstractFluentUpdateStatusModel find(String typecode, String code)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;
}

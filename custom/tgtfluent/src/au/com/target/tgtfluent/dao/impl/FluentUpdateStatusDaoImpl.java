/**
 * 
 */
package au.com.target.tgtfluent.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.util.TargetServicesUtil;
import au.com.target.tgtfluent.dao.FluentUpdateStatusDao;
import au.com.target.tgtfluent.model.AbstractFluentUpdateStatusModel;


/**
 * @author mgazal
 *
 */
public class FluentUpdateStatusDaoImpl extends DefaultGenericDao<AbstractFluentUpdateStatusModel>
        implements FluentUpdateStatusDao {

    public FluentUpdateStatusDaoImpl() {
        super(AbstractFluentUpdateStatusModel._TYPECODE);
    }

    @Override
    public AbstractFluentUpdateStatusModel find(final String typecode, final String code)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final FlexibleSearchQuery query = createFlexibleSearchQuery(typecode, code);
        final FlexibleSearchService fss = getFlexibleSearchService();
        final SearchResult searchResult = fss.search(query);
        final List<AbstractFluentUpdateStatusModel> result = searchResult.getResult();
        TargetServicesUtil.validateIfSingleResult(result, AbstractFluentUpdateStatusModel.class,
                AbstractFluentUpdateStatusModel.CODE, code);
        return result.get(0);
    }

    private FlexibleSearchQuery createFlexibleSearchQuery(final String typecode, final String code) {
        final StringBuilder builder = createQueryString(typecode);
        final FlexibleSearchQuery query = new FlexibleSearchQuery(builder.toString());
        query.addQueryParameter("code", code);
        query.setNeedTotal(true);
        return query;
    }

    private StringBuilder createQueryString(final String typecode) {
        final StringBuilder builder = new StringBuilder();
        builder.append("SELECT {c:").append("pk").append("} ");
        builder.append("FROM {").append(typecode).append(" AS c} ");
        builder.append("WHERE {c:" + AbstractFluentUpdateStatusModel.CODE + "} = ?code");
        return builder;
    }

}

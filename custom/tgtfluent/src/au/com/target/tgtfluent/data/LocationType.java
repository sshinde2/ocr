/**
 * 
 */
package au.com.target.tgtfluent.data;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;


/**
 * @author mgazal
 *
 */
public enum LocationType {
    STORE, WAREHOUSE;

    /**
     * @param pointOfServiceModel
     */
    public static LocationType get(final TargetPointOfServiceModel pointOfServiceModel) {
        // TODO use pointOfServiceModel?
        return STORE;
    }
}

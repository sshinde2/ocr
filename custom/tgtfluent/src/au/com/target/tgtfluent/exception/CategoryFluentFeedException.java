/**
 * 
 */
package au.com.target.tgtfluent.exception;

/**
 * @author bpottass
 *
 */
public class CategoryFluentFeedException extends FluentFeedException {

    private final String errorCode;

    public CategoryFluentFeedException(final String errorCode) {
        super();
        this.errorCode = errorCode;
    }

    /**
     * @param message
     * @param cause
     */
    public CategoryFluentFeedException(final String code, final String message, final Throwable cause) {
        super(message, cause);
        this.errorCode = code;
    }


    /**
     * @param message
     */
    public CategoryFluentFeedException(final String code, final String message) {
        super(message);
        this.errorCode = code;
    }

    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }
}

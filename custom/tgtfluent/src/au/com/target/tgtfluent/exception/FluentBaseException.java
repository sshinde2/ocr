/**
 * 
 */
package au.com.target.tgtfluent.exception;

/**
 * @author pvarghe2
 *
 */
public class FluentBaseException extends Exception {

    /**
     * 
     */
    public FluentBaseException() {
        super();
    }

    /**
     * 
     * @param message
     */
    public FluentBaseException(final String message) {
        super(message);
    }

    /**
     * @param cause
     *            - Root cause
     */
    public FluentBaseException(final Exception cause) {
        super(cause);
    }

    /**
     * @param message
     *            - Descriptive message
     * @param cause
     *            - Root cause
     */
    public FluentBaseException(final String message, final Exception cause) {
        super(message, cause);
    }

}

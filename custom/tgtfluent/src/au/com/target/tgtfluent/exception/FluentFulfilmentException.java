/**
 * 
 */
package au.com.target.tgtfluent.exception;



/**
 * @author bhuang3
 *
 */
public class FluentFulfilmentException extends FluentBaseException {

    /**
     * @param message
     *            - Descriptive message
     */
    public FluentFulfilmentException(final String message) {
        super(message);
    }

    /**
     * @param cause
     *            - Root cause
     */
    public FluentFulfilmentException(final Exception cause) {
        super(cause);
    }

    /**
     * @param message
     *            - Descriptive message
     * @param cause
     *            - Root cause
     */
    public FluentFulfilmentException(final String message, final Exception cause) {
        super(message, cause);
    }

}

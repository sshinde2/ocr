/**
 * 
 */
package au.com.target.tgtfluent.exception;



/**
 * @author bhuang3
 *
 */
public class ProductFluentFeedException extends FluentFeedException {

    private final String errorCode;

    public ProductFluentFeedException(final String errorCode) {
        super();
        this.errorCode = errorCode;
    }

    /**
     * @param message
     * @param cause
     */
    public ProductFluentFeedException(final String code, final String message, final Throwable cause) {
        super(message, cause);
        this.errorCode = code;
    }


    /**
     * @param message
     */
    public ProductFluentFeedException(final String code, final String message) {
        super(message);
        this.errorCode = code;
    }

    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }
}

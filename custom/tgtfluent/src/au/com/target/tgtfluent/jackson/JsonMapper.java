/**
 * 
 */
package au.com.target.tgtfluent.jackson;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;


/**
 * Setup Deserialization config
 * 
 * @author pthoma20
 * 
 */
public class JsonMapper extends ObjectMapper {

    /**
     * 
     */
    public JsonMapper() {
        super();
        this.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

}

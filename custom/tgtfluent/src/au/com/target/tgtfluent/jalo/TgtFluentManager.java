package au.com.target.tgtfluent.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;

import au.com.target.tgtfluent.constants.TgtFluentConstants;


public class TgtFluentManager extends GeneratedTgtFluentManager {

    public static final TgtFluentManager getInstance() {
        final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
        return (TgtFluentManager)em.getExtension(TgtFluentConstants.EXTENSIONNAME);
    }

}

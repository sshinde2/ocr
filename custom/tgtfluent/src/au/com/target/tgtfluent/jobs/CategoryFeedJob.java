/**
 * 
 */
package au.com.target.tgtfluent.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfluent.service.CategoryFeedService;


/**
 * @author bpottass
 *
 */
public class CategoryFeedJob extends AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(CategoryFeedJob.class);

    private static final String LOGGER_PREFIX = "FLUENT CATEGORY_BULK_LOAD: ";

    private CategoryFeedService categoryFeedService;

    @Override
    public PerformResult perform(final CronJobModel arg0) {
        try {
            LOG.info(LOGGER_PREFIX + "Job Starting");
            categoryFeedService.feedCategoriesToFluent();
            LOG.info(LOGGER_PREFIX + "Job Finished Successfully");
            return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

        }
        catch (final Exception e) {
            LOG.error(LOGGER_PREFIX + "Job Failed, Unhandled Exception occurred, Aborting", e);
            return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
        }
    }


    /**
     * @param categoryFeedService
     *            the categoryFeedService to set
     */
    @Required
    public void setCategoryFeedService(final CategoryFeedService categoryFeedService) {
        this.categoryFeedService = categoryFeedService;
    }


}

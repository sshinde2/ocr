/**
 * 
 */
package au.com.target.tgtfluent.service;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;

import au.com.target.tgtfluent.client.FluentClient;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.model.AbstractFluentUpdateStatusModel;
import au.com.target.tgtfluent.model.FluentUpdateFailureCauseModel;


/**
 * @author mgazal
 *
 */
public abstract class AbstractFluentUpsertService<J extends ItemModel, K, L extends AbstractFluentUpdateStatusModel> {

    private static final Logger LOG = Logger.getLogger(AbstractFluentUpsertService.class);

    private static final String LOG_TEMPLATE = "FLUENT: action=upsert{0}: {1}";

    private static final String LOG_ERROR = "FLUENT_UPSERT_ERROR: Entity code={0} updateStatusType={1}: PK={2}";

    protected FluentClient fluentClient;

    protected ModelService modelService;

    protected FluentUpdateStatusService fluentUpdateStatusService;

    private final Class<L> updateStatusClass;

    private Converter<J, K> converter;

    /**
     * @param updateStatusClass
     */
    public AbstractFluentUpsertService(final Class<L> updateStatusClass) {
        this.updateStatusClass = updateStatusClass;
    }

    /**
     * generic method to upsert an entity to fluent.
     * 
     * @param model
     */
    protected void upsertEntity(final J model) {
        FluentResponse fluentResponse;
        final K entity = convert(model);
        if (!isValid(entity, model)) {
            return;
        }
        String fluentId = getFluentId(model);
        if (StringUtils.isNotBlank(fluentId)) {
            fluentResponse = fluentUpdateApi(entity, fluentId, model);
            if (hasError(fluentResponse, getUpdateFailureStatus())) {
                log(entity, "update failed for fluentId:" + fluentId + " going to try create");
                fluentResponse = fluentCreateApi(entity, model);
            }
        }
        else {
            fluentResponse = fluentCreateApi(entity, model);
            if (hasError(fluentResponse, getCreateFailureStatus())) {
                log(entity, "create failed for code:" + getCode(model) + " going to search");
                fluentId = fluentSearchApi(entity);
                if (StringUtils.isNotBlank(fluentId)) {
                    fluentResponse = fluentUpdateApi(entity, fluentId, model);
                }
                else {
                    log(entity, "search failed for code:" + getCode(model));
                }
            }
        }
        updateUpdateStatus(model, fluentResponse);
    }

    protected K convert(final J model) {
        return converter.convert(model);
    }

    /**
     * @param entity
     * @param model
     * @return false if entity and/or model is invalid for upsert
     */
    protected boolean isValid(final K entity, final J model) {
        return entity != null && model != null;
    }

    /**
     * @param entity
     * @param message
     */
    protected void log(final K entity, final String message) {
        LOG.info(MessageFormat.format(LOG_TEMPLATE, entity.getClass().getSimpleName(), message));
    }

    /**
     * Updates the fluentUpdateStatus
     * 
     * @param model
     * @param fluentResponse
     */
    protected void updateUpdateStatus(final J model, final FluentResponse fluentResponse) {
        final List<ItemModel> toSave = new ArrayList<>();
        final L fluentUpdateStatus = fluentUpdateStatusService.getFluentUpdateStatus(updateStatusClass,
                getUpdateStatusTypeCode(), getCode(model));
        final boolean success = CollectionUtils.isEmpty(fluentResponse.getErrors());
        fluentUpdateStatus.setLastRunSuccessful(success);
        if (success) {
            fluentUpdateStatus.setFluentId(fluentResponse.getId());
            setFluentId(model, fluentResponse.getId());
            toSave.add(model);
        }
        else if (CollectionUtils.isNotEmpty(fluentResponse.getErrors())) {
            LOG.error(MessageFormat.format(LOG_ERROR, getCode(model), getUpdateStatusTypeCode(),
                    fluentUpdateStatus.getPk()));
            FluentUpdateFailureCauseModel failureCause;
            for (final Error error : fluentResponse.getErrors()) {
                failureCause = modelService.create(FluentUpdateFailureCauseModel.class);
                failureCause.setCode(error.getCode());
                failureCause.setMessage(error.getMessage());
                failureCause.setUpdateStatus(fluentUpdateStatus);
                toSave.add(failureCause);
            }
        }
        toSave.add(fluentUpdateStatus);
        modelService.saveAll(toSave);
    }

    /**
     * @return the _TYPECODE for the updateStatus model
     */
    protected abstract String getUpdateStatusTypeCode();

    /**
     * Get the fluentId parameter from the model
     * 
     * @param model
     * @return fluentId
     */
    protected abstract String getFluentId(J model);

    /**
     * Set fluentId on the model
     * 
     * @param model
     * @param fluentId
     */
    protected abstract void setFluentId(J model, String fluentId);

    /**
     * Call the create API for the given entity on Fluent
     * 
     * @param entity
     * @param model
     * @return {@link FluentResponse}
     */
    protected abstract FluentResponse fluentCreateApi(K entity, J model);

    /**
     * Call the update API for the given entity on Fluent
     * 
     * @param entity
     * @param fluentId
     * @param model
     * @return {@link FluentResponse}
     */
    protected abstract FluentResponse fluentUpdateApi(K entity, String fluentId, J model);

    /**
     * Call the search API for the given entity on Fluent and return the fluentId
     * 
     * @param entity
     * @return fluentId
     */
    protected abstract String fluentSearchApi(K entity);

    /**
     * Return the unique code for the model to be stored against updateStatus
     * 
     * @param model
     * @return code
     */
    protected abstract String getCode(J model);

    /**
     * the {@link HttpStatus} that indicates create failed because entity already exists
     * 
     * @return httpStatus string
     */
    protected String getCreateFailureStatus() {
        return String.valueOf(HttpStatus.CONFLICT.value());
    }

    /**
     * the {@link HttpStatus} that indicates update failed because entity does not exist
     * 
     * @return httpStatus string
     */
    protected String getUpdateFailureStatus() {
        return String.valueOf(HttpStatus.NOT_FOUND.value());
    }

    /**
     * @param fluentResponse
     * @param errorCode
     * @return whether the response has the given error
     */
    private boolean hasError(final FluentResponse fluentResponse, final String errorCode) {
        return CollectionUtils.find(fluentResponse.getErrors(), new Predicate() {

            @Override
            public boolean evaluate(final Object object) {
                return errorCode.equals(((Error)object).getCode());
            }
        }) != null;
    }

    /**
     * @param fluentClient
     *            the fluentClient to set
     */
    @Required
    public void setFluentClient(final FluentClient fluentClient) {
        this.fluentClient = fluentClient;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param fluentUpdateStatusService
     *            the fluentUpdateStatusService to set
     */
    @Required
    public void setFluentUpdateStatusService(final FluentUpdateStatusService fluentUpdateStatusService) {
        this.fluentUpdateStatusService = fluentUpdateStatusService;
    }

    /**
     * @param converter
     *            the converter to set
     */
    @Required
    public void setConverter(final Converter<J, K> converter) {
        this.converter = converter;
    }
}

/**
 * 
 */
package au.com.target.tgtfluent.service;

/**
 * @author bpottass
 *
 */
public interface CategoryFeedService {

    /**
     * Feed all categories to fluent.
     */
    public void feedCategoriesToFluent();

}

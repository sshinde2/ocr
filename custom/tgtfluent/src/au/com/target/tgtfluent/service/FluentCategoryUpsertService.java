/**
 * 
 */
package au.com.target.tgtfluent.service;

import au.com.target.tgtcore.model.TargetProductCategoryModel;


/**
 * @author bpottass
 *
 */
public interface FluentCategoryUpsertService {


    /**
     * Create/Update category in Fluent
     * 
     * @param categoryModel
     */
    void upsertCategory(TargetProductCategoryModel categoryModel);

}

/**
 * 
 */
package au.com.target.tgtfluent.service;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import au.com.target.tgtcore.model.TargetConsignmentModel;


/**
 * @author mgazal
 *
 */
public interface FluentConsignmentService {

    /**
     * Gets and existing consignment against the given code or create a new one
     * 
     * @param consignmentCode
     * @return {@link TargetConsignmentModel}
     */
    public TargetConsignmentModel getOrCreateConsignmentForCode(final String consignmentCode);


    /**
     * Creates a consignment and set the consignmentCode
     * 
     * @param consignmentCode
     * @return {@link TargetConsignmentModel}
     */
    public TargetConsignmentModel createConsignmentForCode(final String consignmentCode);


    /**
     * @param consignment
     * @return
     */
    boolean isConsignmentServicedByFluentServicePoint(TargetConsignmentModel consignment);

    /**
     * checks whether order associated to consignment is a fluent order or not. If fluent order, then return true else
     * false.
     * 
     * @return true/false
     */
    boolean isAFluentConsignment(final ConsignmentModel consignment);
}

/**
 * 
 */
package au.com.target.tgtfluent.service;

import java.util.List;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfluent.model.AbstractFluentBatchResponseModel;


/**
 * @author mgazal
 *
 */
public interface FluentFeedService {

    /**
     * Feed locations in batch to fluent
     * 
     * @param targetPointOfServiceModels
     */
    void feedLocation(List<TargetPointOfServiceModel> targetPointOfServiceModels);

    /**
     * Check batch status and update current status
     * 
     * @param batchResponse
     */
    void checkBatchStatus(AbstractFluentBatchResponseModel batchResponse);
}

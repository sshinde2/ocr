/**
 * 
 */
package au.com.target.tgtfluent.service;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfluent.data.Fulfilment;
import au.com.target.tgtfluent.exception.FluentBaseException;
import au.com.target.tgtfluent.exception.FluentFulfilmentException;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntryList;


/**
 * @author bhuang3
 *
 */
public interface FluentFulfilmentService {

    /**
     * create consignments in order by polling fluent fulfilments
     * 
     * @param orderModel
     */
    void createConsignmentsByOrderId(OrderModel orderModel) throws FluentFulfilmentException;

    /**
     * create TargetOrderCancelEntryList by cancellation consignment(consignment contains the cancellationWarehouse)
     * 
     * @param orderModel
     * @return TargetOrderCancelEntryList
     */
    TargetOrderCancelEntryList createTargetOrderCancelEntryList(OrderModel orderModel);

    /**
     * check if any assigned consignment
     * 
     * @param orderModel
     * @return true or false
     */
    boolean isAnyAssignedConsignment(final OrderModel orderModel);

    /**
     * send ReadyForPickup to fluent
     * 
     * @param consignment
     * @throws FluentBaseException
     */
    void sendReadyForPickup(final TargetConsignmentModel consignment)
            throws FluentBaseException;

    /**
     * send ReturnedToFloor to fluent
     * 
     * @param consignment
     * @throws FluentBaseException
     */
    void sendConsignmentEventToFluent(final ConsignmentModel consignment, String eventName,
            boolean isCreateFulfilment)
            throws FluentBaseException;

    /**
     * @param fulfilment
     * @throws FluentFulfilmentException
     */
    void upsertFulfilment(Fulfilment fulfilment) throws FluentFulfilmentException;

}

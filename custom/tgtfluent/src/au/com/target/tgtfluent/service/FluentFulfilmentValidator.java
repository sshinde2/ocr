/**
 * 
 */
package au.com.target.tgtfluent.service;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfluent.data.Fulfilment;
import au.com.target.tgtfluent.exception.FluentFulfilmentException;


/**
 * @author bpottass
 *
 */
public interface FluentFulfilmentValidator {

    /**
     * 
     * @param fulfilment
     * @param consignment
     * @return true if fulfilment is valid fulfilment
     */
    boolean validateFulfilment(final Fulfilment fulfilment, final TargetConsignmentModel consignment);

    /**
     * 
     * @param fulfilment
     * @param consignment
     * @return true if status transition is valid
     * @throws FluentFulfilmentException
     */
    boolean validateStateTransition(final Fulfilment fulfilment, final TargetConsignmentModel consignment)
            throws FluentFulfilmentException;

}

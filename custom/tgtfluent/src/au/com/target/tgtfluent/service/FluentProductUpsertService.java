/**
 * 
 */
package au.com.target.tgtfluent.service;

import au.com.target.tgtcore.model.TargetProductModel;


/**
 * @author mgazal
 *
 */
public interface FluentProductUpsertService {
    /**
     * Feed Product to fluent
     * 
     * @param targetProductModel
     */
    void upsertProduct(TargetProductModel targetProductModel);
}

/**
 * 
 */
package au.com.target.tgtfluent.service;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;


/**
 * @author mgazal
 *
 */
public interface FluentSkuUpsertService {
    /**
     * Feed SKU to fluent
     * 
     * @param variantProduct
     */
    void upsertSku(AbstractTargetVariantProductModel variantProduct);


    /**
     * Publish PreOrderMaxQtyInventory to Fluent
     * 
     * @param variantProduct
     */
    void sendMaxPreOrderInventoryEvent(AbstractTargetVariantProductModel variantProduct);
}

/**
 * 
 */
package au.com.target.tgtfluent.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.fluent.FluentStockService;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtfluent.data.FluentStock;
import au.com.target.tgtfluent.data.StockDatum;
import au.com.target.tgtfluent.exception.FluentClientException;


/**
 * @author mgazal
 *
 */
public interface FluentStockLookupService extends FluentStockService {

    /**
     * Fetch stock using Fluent Stock Lookup API for given baseProduct and storeNumber
     * 
     * @param baseProductCode
     * @param storeNumber
     * @return map of SKU codes to {@link FluentStock}
     * @throws FluentClientException
     */
    Map<String, FluentStock> lookupStock(String baseProductCode, String storeNumber) throws FluentClientException;

    /**
     * Fetch stock using Fluent Stock Lookup API for given skus
     * 
     * @param skuCodes
     * @return map of SKU codes to stockLevel
     * @throws FluentClientException
     */
    Map<String, Integer> lookupStock(Collection<String> skuCodes) throws FluentClientException;

    /**
     * Fetch stock using Fluent Stock Lookup API for given skus, deliveryTypes and locations
     * 
     * @param skuRefs
     * @param deliveryTypes
     * @param locations
     * @return list of {@link StockDatum}
     * @throws FluentClientException
     */
    List<StockDatum> lookupStock(final Collection<String> skuRefs, final Collection<String> deliveryTypes,
            final Collection<String> locations) throws FluentClientException;

    /**
     * @param skuRefs
     * @return map of skuCode to maximum of ats values for it
     * @throws FluentClientException
     */
    Map<String, Integer> lookupAts(final String... skuRefs) throws FluentClientException;

    /**
     * Fetch stock using Fluent Stock Lookup API for given skus and locations/stores
     * 
     * @param skuRefs
     * @param locations
     * @return StockVisibilityItemLookupResponseDto
     * @throws FluentClientException
     */
    StockVisibilityItemLookupResponseDto lookupStock(final Collection<String> skuRefs,
            final Collection<String> locations) throws FluentClientException;
}

/**
 * 
 */
package au.com.target.tgtfluent.service;

/**
 * @author mgazal
 *
 */
public interface ProductFeedService {

    public void feedProducts();
}

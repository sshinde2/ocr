/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtfluent.dao.FluentBatchResponseDao;
import au.com.target.tgtfluent.enums.FluentBatchStatus;
import au.com.target.tgtfluent.model.AbstractFluentBatchResponseModel;
import au.com.target.tgtfluent.service.FluentBatchResponseService;


/**
 * @author mgazal
 *
 */
public class FluentBatchResponseServiceImpl extends AbstractBusinessService implements FluentBatchResponseService {

    private FluentBatchResponseDao fluentBatchResponseDao;

    @Override
    public AbstractFluentBatchResponseModel find(final String typecode, final String jobId, final String batchId) {
        try {
            return fluentBatchResponseDao.find(typecode, jobId, batchId);
        }
        catch (TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
            return null;
        }
    }

    @Override
    public List<AbstractFluentBatchResponseModel> find(final String typecode,
            final Collection<FluentBatchStatus> statuses) {
        return fluentBatchResponseDao.find(typecode, statuses);
    }

    /**
     * @param fluentBatchResponseDao
     *            the fluentBatchResponseDao to set
     */
    @Required
    public void setFluentBatchResponseDao(final FluentBatchResponseDao fluentBatchResponseDao) {
        this.fluentBatchResponseDao = fluentBatchResponseDao;
    }
}

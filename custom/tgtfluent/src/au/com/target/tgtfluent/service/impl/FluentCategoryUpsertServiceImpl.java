/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import java.util.Arrays;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtfluent.data.Category;
import au.com.target.tgtfluent.data.CategoryResponse;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.exception.CategoryFluentFeedException;
import au.com.target.tgtfluent.model.CategoryFluentUpdateStatusModel;
import au.com.target.tgtfluent.service.AbstractFluentUpsertService;
import au.com.target.tgtfluent.service.FluentCategoryUpsertService;


/**
 * @author bpottass
 *
 */
public class FluentCategoryUpsertServiceImpl
        extends AbstractFluentUpsertService<TargetProductCategoryModel, Category, CategoryFluentUpdateStatusModel>
        implements FluentCategoryUpsertService {

    public FluentCategoryUpsertServiceImpl() {
        super(CategoryFluentUpdateStatusModel.class);
    }

    @Override
    public void upsertCategory(final TargetProductCategoryModel targetProductCategoryModel) {
        upsertEntity(targetProductCategoryModel);
    }

    @Override
    protected Category convert(final TargetProductCategoryModel model) {
        FluentResponse fluentResponse;
        try {
            return super.convert(model);
        }
        catch (final CategoryFluentFeedException e) {
            final Error error = new Error();
            error.setCode(e.getErrorCode());
            error.setMessage(e.getMessage());
            fluentResponse = new FluentResponse();
            fluentResponse.setErrors(Arrays.asList(error));
            updateUpdateStatus(model, fluentResponse);
            return null;
        }
    }

    @Override
    protected String getUpdateStatusTypeCode() {
        return CategoryFluentUpdateStatusModel._TYPECODE;
    }

    @Override
    protected String getFluentId(final TargetProductCategoryModel model) {
        return model.getFluentId();
    }

    @Override
    protected void setFluentId(final TargetProductCategoryModel model, final String fluentId) {
        model.setFluentId(fluentId);
    }

    @Override
    protected FluentResponse fluentCreateApi(final Category category, final TargetProductCategoryModel model) {
        return fluentClient.createCategory(category);
    }

    @Override
    protected FluentResponse fluentUpdateApi(final Category category, final String fluentId,
            final TargetProductCategoryModel model) {
        return fluentClient.updateCategory(fluentId, category);
    }

    @Override
    protected String fluentSearchApi(final Category category) {
        final CategoryResponse categoryResponse = fluentClient.searchCategory(category.getCategoryRef());
        if (categoryResponse != null && CollectionUtils.isNotEmpty(categoryResponse.getResults())) {
            return categoryResponse.getResults().get(0).getId();
        }
        return null;
    }

    @Override
    protected String getCode(final TargetProductCategoryModel model) {
        return model.getCode();
    }


}

/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatus.Series;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfluent.client.FluentClient;
import au.com.target.tgtfluent.data.Batch;
import au.com.target.tgtfluent.data.BatchAction;
import au.com.target.tgtfluent.data.BatchRecordStatus;
import au.com.target.tgtfluent.data.BatchResponse;
import au.com.target.tgtfluent.data.EntityType;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.data.Job;
import au.com.target.tgtfluent.data.Location;
import au.com.target.tgtfluent.enums.FluentBatchStatus;
import au.com.target.tgtfluent.exception.FluentFeedException;
import au.com.target.tgtfluent.model.AbstractFluentBatchResponseModel;
import au.com.target.tgtfluent.model.LocationFluentBatchResponseModel;
import au.com.target.tgtfluent.service.FluentFeedService;


/**
 * @author mgazal
 *
 */
public class FluentFeedServiceImpl implements FluentFeedService {

    private static final Logger LOG = Logger.getLogger(FluentFeedServiceImpl.class);

    private static final String LOCATION_JOB_PREFIX = "LOCATION_";

    private static final String BATCH_ERROR_LOG_TEMPLATE = "Fluent Batch Error batchId:{0}, entityType:{1}, entityId:{2}, locationRef:{3}, error:{4}, message:{5}";

    private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormat.forPattern("yyyyMMdd_HHmmss");

    private Converter<TargetPointOfServiceModel, Location> locationConverter;

    private FluentClient fluentClient;

    private ModelService modelService;

    private String batchCount;

    @Override
    public void feedLocation(final List<TargetPointOfServiceModel> targetPointOfServiceModels) {
        final Job job = new Job();
        job.setName(LOCATION_JOB_PREFIX + DATE_TIME_FORMAT.print(System.currentTimeMillis()));
        final FluentResponse jobResponse = fluentClient.createJob(job);
        if (CollectionUtils.isEmpty(jobResponse.getErrors())) {
            final Batch batch = new Batch();
            batch.setAction(BatchAction.UPSERT);
            batch.setEntityType(EntityType.LOCATION);
            final List<Location> locations = new ArrayList<>(targetPointOfServiceModels.size());
            for (final TargetPointOfServiceModel targetPointOfServiceModel : targetPointOfServiceModels) {
                locations.add(locationConverter.convert(targetPointOfServiceModel));
            }
            batch.setEntities(locations);
            final FluentResponse batchResponse = fluentClient.createBatch(jobResponse.getId(), batch);
            if (CollectionUtils.isEmpty(batchResponse.getErrors())) {
                final LocationFluentBatchResponseModel fluentBatchResponse = modelService
                        .create(LocationFluentBatchResponseModel.class);
                fluentBatchResponse.setJobId(jobResponse.getId());
                fluentBatchResponse.setBatchId(batchResponse.getId());
                fluentBatchResponse.setStatus(FluentBatchStatus.PENDING);
                modelService.save(fluentBatchResponse);
            }
            else {
                throw new FluentFeedException();
            }
        }
        else {
            throw new FluentFeedException();
        }
    }

    @Override
    public void checkBatchStatus(final AbstractFluentBatchResponseModel batchResponseModel) {
        final Map<String, String> queryParams = new HashMap<>();
        queryParams.put("count", batchCount);
        final BatchResponse batchResponse = fluentClient.viewBatch(batchResponseModel.getJobId(),
                batchResponseModel.getBatchId(), queryParams);
        switch (batchResponse.getStatus()) {
            case PENDING:
                batchResponseModel.setStatus(FluentBatchStatus.PENDING);
                break;
            case RUNNING:
                batchResponseModel.setStatus(FluentBatchStatus.RUNNING);
                break;
            case COMPLETE:
                batchResponseModel.setStatus(FluentBatchStatus.COMPLETE);
                if (CollectionUtils.isNotEmpty(batchResponse.getResults())) {
                    processBatchRecords(batchResponse);
                }
                break;
            default:
                break;
        }
        modelService.save(batchResponseModel);
    }

    /**
     * @param batchResponse
     */
    protected void processBatchRecords(final BatchResponse batchResponse) {
        for (final BatchRecordStatus recordStatus : batchResponse.getResults()) {
            final Series httpSeries = HttpStatus.Series.valueOf(Integer.parseInt(recordStatus.getResponseCode()));
            if (httpSeries == Series.CLIENT_ERROR || httpSeries == Series.SERVER_ERROR) {
                logRecordStatus(Level.ERROR, batchResponse, recordStatus);
            }
            else if (httpSeries != Series.SUCCESSFUL) {
                logRecordStatus(Level.WARN, batchResponse, recordStatus);
            }
        }
    }

    /**
     * @param level
     * @param batchResponse
     * @param recordStatus
     */
    protected void logRecordStatus(final Level level, final BatchResponse batchResponse,
            final BatchRecordStatus recordStatus) {
        LOG.log(level, MessageFormat.format(BATCH_ERROR_LOG_TEMPLATE, batchResponse.getBatchId(),
                batchResponse.getEntityType(), recordStatus.getEntityId(), recordStatus.getLocationRef(),
                recordStatus.getResponseCode(), recordStatus.getMessage()));
    }

    /**
     * @param locationConverter
     *            the locationConverter to set
     */
    @Required
    public void setLocationConverter(final Converter<TargetPointOfServiceModel, Location> locationConverter) {
        this.locationConverter = locationConverter;
    }

    /**
     * @param fluentClient
     *            the fluentClient to set
     */
    @Required
    public void setFluentClient(final FluentClient fluentClient) {
        this.fluentClient = fluentClient;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param batchCount
     *            the batchCount to set
     */
    @Required
    public void setBatchCount(final String batchCount) {
        this.batchCount = batchCount;
    }
}

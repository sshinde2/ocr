/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.orderEntry.TargetOrderEntryService;
import au.com.target.tgtfluent.constants.FulfilmentStatus;
import au.com.target.tgtfluent.constants.FulfilmentStatus.Vendor;
import au.com.target.tgtfluent.data.Fulfilment;
import au.com.target.tgtfluent.data.FulfilmentItem;
import au.com.target.tgtfluent.exception.FluentFulfilmentException;
import au.com.target.tgtfluent.service.FluentFulfilmentValidator;
import au.com.target.tgtfluent.util.FluentDataMap;
import au.com.target.tgtfluent.util.FluentDeliveryType;
import au.com.target.tgtfluent.util.FluentFulfilmentType;


/**
 * @author bpottass
 *
 */
public class FluentFulfilmentValidatorImpl implements FluentFulfilmentValidator {

    private static final Logger LOG = Logger.getLogger(FluentFulfilmentValidatorImpl.class);

    private static final String FLUENT_FULFILMENT_VALIDATOR_ERROR = "FLUENT_FULFILMENT_VALIDATOR_ERROR:";
    private static final String FLUENT_FULFILMENT_VALIDATOR_ERROR_MSG = FLUENT_FULFILMENT_VALIDATOR_ERROR
            + " message={0}, orderId={1}, fulfilmentId={2}, fulfilmentStatus={3},consignmentStatus={4}, vendor={5}";

    private TargetOrderEntryService targetOrderEntryService;

    @Override
    public boolean validateFulfilment(final Fulfilment fulfilment, final TargetConsignmentModel consignment) {
        Assert.notNull(consignment, "TargetConsignmentModel cannot be null");
        Assert.notNull(fulfilment, "Fulfilment cannot be null");
        final OrderModel orderModel = consignment.getOrder() instanceof OrderModel ? (OrderModel)consignment.getOrder()
                : null;
        if (orderModel == null) {
            throw new IllegalArgumentException(FLUENT_FULFILMENT_VALIDATOR_ERROR + "Order cannot be null");
        }
        if (isConsignmentAlreadyCancelled(consignment)) {
            throw new IllegalArgumentException(FLUENT_FULFILMENT_VALIDATOR_ERROR + "Consignment is already cancelled");
        }
        if (!validateFulfilment(fulfilment, orderModel) || !validateOrderEntries(fulfilment, orderModel)
                || !validateFulfilmentTypes(fulfilment, orderModel)) {
            throw new IllegalArgumentException(MessageFormat.format(FLUENT_FULFILMENT_VALIDATOR_ERROR_MSG,
                    "fluent fulfilment data validation error", orderModel.getCode(), fulfilment.getFulfilmentId(),
                    fulfilment.getStatus(), consignment.getStatus(), null));
        }
        return true;
    }

    @Override
    public boolean validateStateTransition(final Fulfilment fulfilment, final TargetConsignmentModel consignment)
            throws FluentFulfilmentException {
        final Enum oldStatus;
        final Enum newStatus;
        try {
            oldStatus = getOldStatus(consignment);
            newStatus = getNewStatus(fulfilment, consignment);
            if (newStatus == null) {
                throw new IllegalArgumentException();
            }
        }
        catch (final IllegalArgumentException e) {
            final String vendor = consignment.getWarehouse().getVendor() != null
                    ? consignment.getWarehouse().getVendor().getCode()
                    : null;
            final String errorMessage = MessageFormat.format(FLUENT_FULFILMENT_VALIDATOR_ERROR_MSG,
                    "can't convert fulfilment or consignmentStatus", null, fulfilment.getFulfilmentId(),
                    fulfilment.getStatus(),
                    consignment.getStatus(),
                    vendor);
            LOG.warn(errorMessage, e);
            throw new IllegalArgumentException(errorMessage, e);
        }

        return validateStateTransition(oldStatus, newStatus, fulfilment.getFulfilmentId());
    }

    /**
     * @param fulfilment
     * @param orderModel
     * @return whether fulfilment is valid
     */
    protected boolean validateFulfilment(final Fulfilment fulfilment, final AbstractOrderModel orderModel) {
        boolean result = true;
        final String orderCode = orderModel.getCode();
        final String prefixError = FLUENT_FULFILMENT_VALIDATOR_ERROR + " orderCode=" + orderCode;
        if (StringUtils.isEmpty(fulfilment.getOrderId())) {
            LOG.error(prefixError + " OrderId cannot be empty");
            result = false;
        }

        if (!fulfilment.getOrderId().equalsIgnoreCase(orderModel.getFluentId())) {
            LOG.error(prefixError + " OrderId doesn't match");
            result = false;
        }
        if (fulfilment.getFulfilmentId() == null) {
            LOG.error(prefixError + " FulfilmentId cannot be empty");
            result = false;
        }
        if (StringUtils.isEmpty(fulfilment.getStatus())) {
            LOG.error(prefixError + " Fulfilment status cannot be empty");
            result = false;
        }
        if (!FluentDataMap.getAllAvailableConsignmentStatus().contains(fulfilment.getStatus())) {
            LOG.error(prefixError + " Fulfilment status is not valid. status=" + fulfilment.getStatus());
            result = false;
        }

        if (!FluentDeliveryType.getAllAvailableTypes().contains(fulfilment.getDeliveryType())) {
            LOG.error(prefixError + " DeliveryType is not valid. DeliveryType=" + fulfilment.getDeliveryType());
            result = false;
        }

        if (fulfilment.getFromAddress() != null && StringUtils.isEmpty(fulfilment.getFromAddress().getLocationRef())) {
            LOG.error(prefixError + " Fulfilment from address location ref cannot be empty");
            result = false;
        }

        return result;
    }

    /**
     * @param fulfilment
     * @param orderModel
     * @return whether fulfilment type is valid
     */
    protected boolean validateFulfilmentTypes(final Fulfilment fulfilment, final AbstractOrderModel orderModel) {
        boolean result = true;
        final String orderCode = orderModel.getCode();
        final String prefixError = FLUENT_FULFILMENT_VALIDATOR_ERROR + " orderCode=" + orderCode;
        if (StringUtils.isEmpty(fulfilment.getFulfilmentType())) {
            LOG.error(prefixError + " FulfilmentType cannot be empty");
            result = false;
        }
        if (!FluentFulfilmentType.getAllAvailableTypes().contains(fulfilment.getFulfilmentType())) {
            LOG.error(prefixError + " FulfilmentType is not valid. fulfilmentType=" + fulfilment.getFulfilmentType());
            result = false;
        }
        return result;
    }

    /**
     * @param fulfilment
     * @param orderModel
     * @return whether order entries are valid
     */
    protected boolean validateOrderEntries(final Fulfilment fulfilment, final AbstractOrderModel orderModel) {
        boolean result = true;
        final String orderCode = orderModel.getCode();
        final String prefixError = FLUENT_FULFILMENT_VALIDATOR_ERROR + " orderCode=" + orderCode;
        final List<String> productListInOrderEntries = targetOrderEntryService
                .getAllProductCodeFromOrderEntries(orderModel);
        final List<String> productListInFulfilment = getAllItemCodeFromFulfilment(fulfilment);
        if (CollectionUtils.isEmpty(productListInFulfilment)) {
            LOG.error(prefixError + " Fulfilment item cannot be empty");
            result = false;
        }
        if (!productListInOrderEntries.containsAll(productListInFulfilment)) {
            LOG.error(prefixError + " Fulfilment item is not in order entries. product list in order entry="
                    + StringUtils.join(productListInOrderEntries, ",") + " , product list in fulfilment="
                    + StringUtils.join(productListInFulfilment, ","));
            result = false;
        }
        return result;
    }

    /**
     * @param fulfilment
     * @return list of item codes from fulfilment
     */
    private List<String> getAllItemCodeFromFulfilment(final Fulfilment fulfilment) {
        final List<String> itemCodes = new ArrayList<>();
        if (fulfilment != null && CollectionUtils.isNotEmpty(fulfilment.getItems())) {
            for (final FulfilmentItem item : fulfilment.getItems()) {
                if (item != null && StringUtils.isNotEmpty(item.getOrderItemRef())) {
                    itemCodes.add(item.getOrderItemRef());
                }
            }
        }
        return itemCodes;
    }

    /**
     * @param oldStatus
     * @param newStatus
     * @param fulfilmentId
     * @return whether transition is valid
     * @throws FluentFulfilmentException
     */
    protected boolean validateStateTransition(final Enum oldStatus, final Enum newStatus, final String fulfilmentId)
            throws FluentFulfilmentException {
        if (oldStatus == null) {
            if (newStatus.ordinal() > 0) {
                throw new FluentFulfilmentException(
                        "invalid starting status= " + newStatus + " for fulfilmentId=" + fulfilmentId);
            }
            else {
                return true;
            }
        }
        if (newStatus.compareTo(oldStatus) <= 0) {
            LOG.warn("drop status transition from=" + oldStatus + " to=" + newStatus + " for fulfilmentId="
                    + fulfilmentId);
            return false;
        }
        if (newStatus.ordinal() - oldStatus.ordinal() > 1) {
            LOG.warn("invalid status transition from=" + oldStatus + " to=" + newStatus + " for fulfilmentId="
                    + fulfilmentId);
            throw new FluentFulfilmentException(
                    "invalid status transition from=" + oldStatus + " to=" + newStatus + " for fulfilmentId="
                            + fulfilmentId);
        }
        return true;
    }


    /**
     * @param consignment
     * @return oldStatus
     */
    protected Enum getOldStatus(final TargetConsignmentModel consignment) {
        switch (Vendor.valueOf(consignment.getWarehouse().getVendor().getCode())) {
            case Target:
                return FulfilmentStatus.Store.get(consignment.getStatus());
            case Fastline:
                return FulfilmentStatus.Dc.get(consignment.getStatus());
            case Incomm:
                if (consignment.getShippingAddress() == null) {
                    return FulfilmentStatus.Incomm.get(consignment.getStatus());
                }
                else {
                    return FulfilmentStatus.IncommPhysical.get(consignment.getStatus());
                }
            default:
                throw new IllegalArgumentException();
        }
    }

    /**
     * @param consignment
     * @return newStatus
     */
    protected Enum getNewStatus(final Fulfilment fulfilment, final TargetConsignmentModel consignment) {
        switch (Vendor.valueOf(consignment.getWarehouse().getVendor().getCode())) {
            case Target:
                return FulfilmentStatus.Store.get(fulfilment.getStatus());
            case Fastline:
                return FulfilmentStatus.Dc.get(fulfilment.getStatus());
            case Incomm:
                if (fulfilment.getToAddress() == null) {
                    return FulfilmentStatus.Incomm.get(fulfilment.getStatus());
                }
                else {
                    return FulfilmentStatus.IncommPhysical.get(fulfilment.getStatus());
                }
            default:
                return null;
        }
    }

    /**
     * 
     * @param consignment
     * @return true if consignment status is cancelled
     */
    private boolean isConsignmentAlreadyCancelled(final TargetConsignmentModel consignment) {
        return ConsignmentStatus.CANCELLED.equals(consignment.getStatus());
    }

    /**
     * @param targetOrderEntryService
     *            the targetOrderEntryService to set
     */
    @Required
    public void setTargetOrderEntryService(final TargetOrderEntryService targetOrderEntryService) {
        this.targetOrderEntryService = targetOrderEntryService;
    }
}

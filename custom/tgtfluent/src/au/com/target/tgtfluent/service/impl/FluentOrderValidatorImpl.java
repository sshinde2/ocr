/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtfluent.data.Order;
import au.com.target.tgtfluent.service.FluentOrderValidator;

/**
 * @author bpottass
 *
 */
public class FluentOrderValidatorImpl implements FluentOrderValidator {

    @Override
    public boolean validateIfAnyConsignmentsAreOpen(final OrderModel orderModel) {
        if (CollectionUtils.isNotEmpty(orderModel.getConsignments())) {
            for (final ConsignmentModel conModel : orderModel.getConsignments()) {
                if (ConsignmentStatus.CANCELLED.equals(conModel.getStatus())
                        || ConsignmentStatus.SHIPPED.equals(conModel.getStatus())) {
                    continue;
                }
                else {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean validateIfAnyItemInOrderIsNotShipped(final OrderModel orderModel) {
        return getTotalQtyOfAllEntriesInOrder(
                orderModel) != getTotalQtyOfAllEntriesInShippedConsignments(orderModel);
    }


    @Override
    public boolean isOrderDataInValid(final Order order) {
        return order.getOrderId() == null || order.getStatus() == null;
    }

    /**
     * 
     * @param orderModel
     * @return total quantity of all entries in consignments with SHIPPED status
     */
    private long getTotalQtyOfAllEntriesInShippedConsignments(final OrderModel orderModel) {
        long total = 0;
        if (CollectionUtils.isNotEmpty(orderModel.getConsignments())) {
            for (final ConsignmentModel conModel : orderModel.getConsignments()) {
                if (ConsignmentStatus.SHIPPED.equals(conModel.getStatus())) {
                    total += getTotalQtyOfAllEntriesInConsignment(conModel);
                }
            }
        }
        return total;
    }

    /**
     * 
     * @param conModel
     * @return total quantity of all entries in the consignment
     */
    private long getTotalQtyOfAllEntriesInConsignment(final ConsignmentModel conModel) {
        long total = 0;
        for (final ConsignmentEntryModel consignmentEntry : conModel.getConsignmentEntries()) {
            total += consignmentEntry.getShippedQuantity() != null ? consignmentEntry.getShippedQuantity().longValue()
                    : 0;
        }
        return total;
    }

    /**
     * @param orderModel
     * @return total quantity of all Entries in the Order
     */
    private long getTotalQtyOfAllEntriesInOrder(final OrderModel orderModel) {
        long totalQuantity = 0;
        for (final AbstractOrderEntryModel orderEntry : orderModel.getEntries()) {
            totalQuantity += orderEntry.getQuantity() != null ? orderEntry.getQuantity().longValue() : 0;
        }
        return totalQuantity;
    }

}

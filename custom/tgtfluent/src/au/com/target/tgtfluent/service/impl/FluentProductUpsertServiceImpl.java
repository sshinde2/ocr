/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import java.util.Arrays;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.data.Product;
import au.com.target.tgtfluent.data.ProductsResponse;
import au.com.target.tgtfluent.exception.ProductFluentFeedException;
import au.com.target.tgtfluent.model.ProductFluentUpdateStatusModel;
import au.com.target.tgtfluent.service.AbstractFluentUpsertService;
import au.com.target.tgtfluent.service.FluentProductUpsertService;


/**
 * @author mgazal
 *
 */
public class FluentProductUpsertServiceImpl
        extends AbstractFluentUpsertService<TargetProductModel, Product, ProductFluentUpdateStatusModel>
        implements FluentProductUpsertService {

    public FluentProductUpsertServiceImpl() {
        super(ProductFluentUpdateStatusModel.class);
    }

    @Override
    public void upsertProduct(final TargetProductModel targetProductModel) {
        upsertEntity(targetProductModel);
    }

    @Override
    protected Product convert(final TargetProductModel model) {
        FluentResponse fluentResponse;
        try {
            return super.convert(model);
        }
        catch (final ProductFluentFeedException e) {
            final Error error = new Error();
            error.setCode(e.getErrorCode());
            error.setMessage(e.getMessage());
            fluentResponse = new FluentResponse();
            fluentResponse.setErrors(Arrays.asList(error));
            updateUpdateStatus(model, fluentResponse);
            return null;
        }
    }

    @Override
    protected String getUpdateStatusTypeCode() {
        return ProductFluentUpdateStatusModel._TYPECODE;
    }

    @Override
    protected String getFluentId(final TargetProductModel model) {
        return model.getFluentId();
    }

    @Override
    protected void setFluentId(final TargetProductModel model, final String fluentId) {
        model.setFluentId(fluentId);
    }

    @Override
    protected FluentResponse fluentCreateApi(final Product entity, final TargetProductModel model) {
        return fluentClient.createProduct(entity);
    }

    @Override
    protected FluentResponse fluentUpdateApi(final Product entity, final String fluentId,
            final TargetProductModel model) {
        return fluentClient.updateProduct(fluentId, entity);
    }

    @Override
    protected String fluentSearchApi(final Product entity) {
        final ProductsResponse productsResponse = fluentClient.searchProduct(entity.getProductRef());
        if (productsResponse != null && CollectionUtils.isNotEmpty(productsResponse.getResults())) {
            return productsResponse.getResults().get(0).getProductId();
        }
        return null;
    }

    @Override
    protected String getCode(final TargetProductModel model) {
        return model.getCode();
    }

}

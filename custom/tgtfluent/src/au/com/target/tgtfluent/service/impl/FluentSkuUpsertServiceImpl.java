/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import java.util.Arrays;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.order.FluentErrorException;
import au.com.target.tgtfluent.constants.TgtFluentConstants;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.data.Event;
import au.com.target.tgtfluent.data.EventResponse;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.data.PreOrderInventory;
import au.com.target.tgtfluent.data.PreOrderItemInventory;
import au.com.target.tgtfluent.data.Sku;
import au.com.target.tgtfluent.data.SkusResponse;
import au.com.target.tgtfluent.model.SkuFluentUpdateStatusModel;
import au.com.target.tgtfluent.service.AbstractFluentUpsertService;
import au.com.target.tgtfluent.service.FluentSkuUpsertService;
import au.com.target.tgtutility.util.TargetProductUtils;


/**
 * @author mgazal
 *
 */
public class FluentSkuUpsertServiceImpl
        extends AbstractFluentUpsertService<AbstractTargetVariantProductModel, Sku, SkuFluentUpdateStatusModel>
        implements FluentSkuUpsertService {

    public FluentSkuUpsertServiceImpl() {
        super(SkuFluentUpdateStatusModel.class);
    }

    @Override
    public void upsertSku(final AbstractTargetVariantProductModel variantProduct) {
        upsertEntity(variantProduct);
        if (publishMaxPreOrderInventoryEvent(variantProduct)) {
            sendMaxPreOrderInventoryEvent(variantProduct);
        }
    }

    @Override
    protected boolean isValid(final Sku entity, final AbstractTargetVariantProductModel model) {
        if (StringUtils.isBlank(getBaseProductId(model))) {
            log(entity, "fluentId absent on baseProduct of " + model.getCode());
            return false;
        }
        return super.isValid(entity, model);
    }

    @Override
    protected String getUpdateStatusTypeCode() {
        return SkuFluentUpdateStatusModel._TYPECODE;
    }

    @Override
    protected String getFluentId(final AbstractTargetVariantProductModel model) {
        return model.getFluentId();
    }

    @Override
    protected void setFluentId(final AbstractTargetVariantProductModel model, final String fluentId) {
        model.setFluentId(fluentId);
    }

    @Override
    protected FluentResponse fluentCreateApi(final Sku sku, final AbstractTargetVariantProductModel model) {
        return fluentClient.createSku(getBaseProductId(model), sku);
    }

    @Override
    protected FluentResponse fluentUpdateApi(final Sku sku, final String fluentId,
            final AbstractTargetVariantProductModel model) {
        return fluentClient.updateSku(getBaseProductId(model), fluentId, sku);
    }

    @Override
    protected String fluentSearchApi(final Sku sku) {
        final SkusResponse skusResponse = fluentClient.searchSkus(sku.getSkuRef());
        if (CollectionUtils.isNotEmpty(skusResponse.getResults())) {
            return skusResponse.getResults().get(0).getSkuId();
        }
        return null;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfluent.service.FluentSkuUpsertService#sendMaxPreOrderQtyEvent(au.com.target.tgtcore.model.AbstractTargetVariantProductModel)
     */
    @Override
    public void sendMaxPreOrderInventoryEvent(final AbstractTargetVariantProductModel variantProduct) {
        final Event event = new Event();
        event.setName(TgtFluentConstants.Event.PREORDER_LOAD_MAX_QTY);
        event.setEntityType(TgtFluentConstants.EntityType.INVENTORY_CATALOGUE);
        event.setEntitySubtype(TgtFluentConstants.EntitySubtype.DEFAULT);
        event.setRootEntityType(TgtFluentConstants.EntityType.INVENTORY_CATALOGUE);
        event.setAttributes(getMaxPreQtyInventory(variantProduct));
        event.setEntityRef(TgtFluentConstants.EntityRef.DEFAULT);
        event.setRootEntityRef(TgtFluentConstants.EntityRef.DEFAULT);
        final FluentResponse fluentResponse = fluentClient.eventSync(event);
        if (CollectionUtils.isNotEmpty(fluentResponse.getErrors())) {
            final Error error = fluentResponse.getErrors().iterator().next();
            throw new FluentErrorException("variant=" + variantProduct.getCode() + ", code=" + error.getCode()
                    + ", message="
                    + error.getMessage());
        }
        if (fluentResponse instanceof EventResponse
                && StringUtils.isNotBlank(((EventResponse)fluentResponse).getErrorMessage())) {
            throw new FluentErrorException("Variant=" + variantProduct.getCode() + ", eventStatus="
                    + ((EventResponse)fluentResponse).getEventStatus()
                    + ", errorMessage=" + ((EventResponse)fluentResponse).getErrorMessage());
        }

    }

    @Override
    protected String getCode(final AbstractTargetVariantProductModel model) {
        return model.getCode();
    }

    @Override
    protected String getCreateFailureStatus() {
        return String.valueOf(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * @param abstractTargetVariantProduct
     * @return fluentId of the baseProduct
     */
    protected String getBaseProductId(final AbstractTargetVariantProductModel abstractTargetVariantProduct) {
        if (abstractTargetVariantProduct.getBaseProduct() instanceof AbstractTargetVariantProductModel) {
            return getBaseProductId((AbstractTargetVariantProductModel)abstractTargetVariantProduct.getBaseProduct());
        }
        if (abstractTargetVariantProduct.getBaseProduct() instanceof TargetProductModel) {
            return ((TargetProductModel)abstractTargetVariantProduct.getBaseProduct()).getFluentId();
        }
        return null;
    }

    /**
     * 
     * @param variant
     */
    protected PreOrderInventory getMaxPreQtyInventory(final AbstractTargetVariantProductModel variant) {
        final int maxPreOrderQty = variant.getMaxPreOrderQuantity() != null ? variant.getMaxPreOrderQuantity()
                .intValue() : 0;
        final PreOrderItemInventory item = new PreOrderItemInventory();
        item.setSkuRef(variant.getCode());
        item.setQuantity(maxPreOrderQty);
        final PreOrderInventory inventory = new PreOrderInventory();
        inventory.setItems(Arrays.asList(item));
        return inventory;
    }

    /**
     * Checks whether the product is embargoed ,Preorder enddate and maxPreOrderQuantity are available
     * 
     * @param variant
     * @return boolean
     */
    protected boolean publishMaxPreOrderInventoryEvent(final AbstractTargetVariantProductModel variant) {
        return variant.getMaxPreOrderQuantity() != null
                && TargetProductUtils.getPreOrderEndDate(variant) != null
                && TargetProductUtils.isProductEmbargoed(variant);
    }
}

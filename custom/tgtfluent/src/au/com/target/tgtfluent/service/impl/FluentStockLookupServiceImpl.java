/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.stock.strategy.StockLevelStatusStrategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtcore.stock.TargetStockLevelStatusStrategy;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemResponseDto;
import au.com.target.tgtfluent.client.FluentClient;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.data.FluentStock;
import au.com.target.tgtfluent.data.Fulfilment;
import au.com.target.tgtfluent.data.FulfilmentItem;
import au.com.target.tgtfluent.data.FulfilmentOptionAttribute;
import au.com.target.tgtfluent.data.FulfilmentOptionAttributeType;
import au.com.target.tgtfluent.data.FulfilmentOptionItem;
import au.com.target.tgtfluent.data.FulfilmentOptionRequest;
import au.com.target.tgtfluent.data.FulfilmentOptionResponse;
import au.com.target.tgtfluent.data.FulfilmentPlan;
import au.com.target.tgtfluent.data.StockAttribute;
import au.com.target.tgtfluent.data.StockDatum;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.service.FluentStockLookupService;
import au.com.target.tgtutility.util.JsonConversionUtil;


/**
 * @author mgazal
 *
 */
public class FluentStockLookupServiceImpl extends AbstractBusinessService implements FluentStockLookupService {

    private static final Logger LOG = Logger.getLogger(FluentStockLookupServiceImpl.class);

    private static final String DELIVERY_TYPES = "deliveryTypes";

    private static final String LOCATION_REFS = "locationRefs";

    private FluentClient fluentClient;

    private StockLevelStatusStrategy onlineStocklevelStatusStrategy;

    private TargetStockLevelStatusStrategy storeStockLevelStatusStrategy;

    @Override
    public Map<String, FluentStock> lookupStock(final String baseProductCode, final String storeNumber)
            throws FluentClientException {
        final FluentResponse fluentResponse = fluentClient
                .createFulfilmentOption(createRequest(baseProductCode, storeNumber));
        if (CollectionUtils.isNotEmpty(fluentResponse.getErrors())) {
            throw new FluentClientException(fluentResponse.getErrors().get(0));
        }
        if (fluentResponse instanceof FulfilmentOptionResponse) {
            return createProductResponse((FulfilmentOptionResponse)fluentResponse);
        }
        return Collections.emptyMap();
    }

    @Override
    public Map<String, Integer> lookupStock(final Collection<String> skuCodes) throws FluentClientException {
        final FluentResponse fluentResponse = fluentClient.createFulfilmentOption(createRequest(skuCodes));
        if (CollectionUtils.isNotEmpty(fluentResponse.getErrors())) {
            throw new FluentClientException(fluentResponse.getErrors().get(0));
        }
        if (fluentResponse instanceof FulfilmentOptionResponse) {
            return createSkusResponse((FulfilmentOptionResponse)fluentResponse);
        }
        return Collections.emptyMap();
    }

    @Override
    public List<StockDatum> lookupStock(final Collection<String> skuRefs, final Collection<String> deliveryTypes,
            final Collection<String> locations) throws FluentClientException {
        final FluentResponse fluentResponse = fluentClient
                .createFulfilmentOption(createRequest(skuRefs, deliveryTypes, locations));
        if (CollectionUtils.isNotEmpty(fluentResponse.getErrors())) {
            throw new FluentClientException(fluentResponse.getErrors().get(0));
        }
        if (fluentResponse instanceof FulfilmentOptionResponse) {
            return createResponse((FulfilmentOptionResponse)fluentResponse);
        }
        return Collections.emptyList();
    }

    @Override
    public Map<String, Integer> lookupAts(final String... skuRefs) throws FluentClientException {
        final List<StockDatum> stockData = lookupStock(Arrays.asList(skuRefs),
                Arrays.asList(TgtCoreConstants.DELIVERY_TYPE.CC, TgtCoreConstants.DELIVERY_TYPE.HD,
                        TgtCoreConstants.DELIVERY_TYPE.ED, TgtCoreConstants.DELIVERY_TYPE.PO),
                null);
        final Map<String, Integer> stockMap = new HashMap<>();
        for (final StockDatum stockDatum : stockData) {
            stockMap.put(stockDatum.getVariantCode(), Integer.valueOf(findAts(stockDatum)));
        }
        return stockMap;
    }

    @Override
    public Map<String, Integer> lookupPlaceOrderAts(final Collection<String> skuRefs, final String deliveryMode) {
        List<StockDatum> stockData;
        try {
            stockData = lookupStock(skuRefs, Arrays.asList(deliveryMode), null);
            final Map<String, Integer> stockMap = new HashMap<>();
            for (final StockDatum stockDatum : stockData) {
                stockMap.put(stockDatum.getVariantCode(), Integer.valueOf(findAts(stockDatum)));
            }
            return stockMap;
        }
        catch (final FluentClientException e) {
            throw new FluentOrderException(e.getLocalizedMessage());
        }
    }


    @Override
    public StockLevelStatus getProductOnlineStockStatus(final String skuCode) {
        try {
            final Map<String, Integer> stockMap = lookupAts(skuCode);
            return getOnlineStockLevel(stockMap.get(skuCode));
        }
        catch (final FluentClientException e) {
            LOG.error("FLUENT STOCK_LOOKUP_ERROR", e);
            return StockLevelStatus.OUTOFSTOCK;
        }
    }

    @Override
    public StockVisibilityItemLookupResponseDto lookupStock(final Collection<String> skuRefs,
            final Collection<String> locations) throws FluentClientException {

        final List<StockDatum> stockData = lookupStock(skuRefs, Collections.<String> emptyList(), locations);
        return getStockResultFromStockData(stockData);
    }

    /**
     * @param stockData
     * @return {@link StockVisibilityItemLookupResponseDto}
     */
    private StockVisibilityItemLookupResponseDto getStockResultFromStockData(final List<StockDatum> stockData) {
        if (CollectionUtils.isEmpty(stockData)) {
            return null;
        }
        final StockVisibilityItemLookupResponseDto stockResult = new StockVisibilityItemLookupResponseDto();
        stockResult.setItems(new ArrayList<StockVisibilityItemResponseDto>());
        StockVisibilityItemResponseDto item;
        for (final StockDatum stockDatum : stockData) {
            for (final Entry<String, Integer> storeStock : stockDatum.getStoreSohQty().entrySet()) {
                item = new StockVisibilityItemResponseDto();
                item.setCode(stockDatum.getVariantCode());
                item.setStoreNumber(storeStock.getKey());
                item.setSoh(String.valueOf(storeStock.getValue()));
                stockResult.getItems().add(item);
            }
        }
        return stockResult;
    }

    /**
     * @param baseProductCode
     * @param storeNumber
     * @return {@link FulfilmentOptionRequest}
     */
    protected FulfilmentOptionRequest createRequest(final String baseProductCode, final String storeNumber) {
        final FulfilmentOptionRequest fulfilmentOptionRequest = new FulfilmentOptionRequest();
        fulfilmentOptionRequest.setLocationRef(storeNumber);
        final List<FulfilmentOptionItem> items = new ArrayList<>();
        final FulfilmentOptionItem item = new FulfilmentOptionItem();
        item.setSkuRef(baseProductCode);
        // TODO requestQuantity = 1 ?
        item.setRequestedQuantity(Integer.valueOf(1));
        items.add(item);
        fulfilmentOptionRequest.setItems(items);
        final List<FulfilmentOptionAttribute> attributes = new ArrayList<>();
        final FulfilmentOptionAttribute attribute = new FulfilmentOptionAttribute();
        attribute.setName("type");
        attribute.setType(FulfilmentOptionAttributeType.STRING);
        attribute.setValue("StockLookUpUsingProduct");
        attributes.add(attribute);
        fulfilmentOptionRequest.setAttributes(attributes);
        return fulfilmentOptionRequest;
    }

    protected FulfilmentOptionRequest createRequest(final Collection<String> skus) {
        final FulfilmentOptionRequest fulfilmentOptionRequest = new FulfilmentOptionRequest();
        final List<FulfilmentOptionItem> items = new ArrayList<>();
        FulfilmentOptionItem item;
        for (final String sku : skus) {
            item = new FulfilmentOptionItem();
            item.setSkuRef(sku);
            // TODO requestQuantity = 1 ?
            item.setRequestedQuantity(Integer.valueOf(1));
            items.add(item);
        }
        fulfilmentOptionRequest.setItems(items);
        final List<FulfilmentOptionAttribute> attributes = new ArrayList<>();
        final FulfilmentOptionAttribute attribute = new FulfilmentOptionAttribute();
        attribute.setName("type");
        attribute.setType(FulfilmentOptionAttributeType.STRING);
        attribute.setValue("STOCK_LOOKUP_USING_SKU");
        attributes.add(attribute);
        fulfilmentOptionRequest.setAttributes(attributes);
        return fulfilmentOptionRequest;
    }

    /**
     * @param skus
     * @param deliveryTypes
     * @param locations
     * @return {@link FulfilmentOptionRequest}
     */
    protected FulfilmentOptionRequest createRequest(final Collection<String> skus,
            final Collection<String> deliveryTypes, final Collection<String> locations) {
        final FulfilmentOptionRequest fulfilmentOptionRequest = new FulfilmentOptionRequest();
        final List<FulfilmentOptionItem> items = new ArrayList<>();
        FulfilmentOptionItem item;
        for (final String sku : skus) {
            item = new FulfilmentOptionItem();
            item.setSkuRef(sku);
            items.add(item);
        }
        fulfilmentOptionRequest.setItems(items);
        final List<FulfilmentOptionAttribute> attributes = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(deliveryTypes)) {
            final FulfilmentOptionAttribute deliveryTypesAttribute = new FulfilmentOptionAttribute();
            deliveryTypesAttribute.setName(DELIVERY_TYPES);
            deliveryTypesAttribute.setType(FulfilmentOptionAttributeType.ARRAY);
            deliveryTypesAttribute.setValue(deliveryTypes);
            attributes.add(deliveryTypesAttribute);
        }

        if (CollectionUtils.isNotEmpty(locations)) {
            final FulfilmentOptionAttribute locationsAttribute = new FulfilmentOptionAttribute();
            locationsAttribute.setName(LOCATION_REFS);
            locationsAttribute.setType(FulfilmentOptionAttributeType.ARRAY);
            locationsAttribute.setValue(locations);
            attributes.add(locationsAttribute);
        }

        fulfilmentOptionRequest.setAttributes(attributes);
        return fulfilmentOptionRequest;
    }

    /**
     * @param fluentResponse
     * @return {@link Map}
     */
    protected List<StockDatum> createResponse(final FulfilmentOptionResponse fluentResponse) {
        final Map<String, StockDatum> stockDataMap = new LinkedHashMap<>();

        if (CollectionUtils.isNotEmpty(fluentResponse.getPlans())) {
            final FulfilmentPlan plan = fluentResponse.getPlans().iterator().next();
            for (final Fulfilment fulfilment : plan.getFulfilments()) {
                populateStockDatum(plan, fulfilment, stockDataMap);
            }
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("stockData:" + JsonConversionUtil.convertToJsonString(stockDataMap.values()));
        }
        return new ArrayList<>(stockDataMap.values());
    }

    /**
     * @param plan
     * @param fulfilment
     * @param stockDataMap
     */
    protected void populateStockDatum(final FulfilmentPlan plan, final Fulfilment fulfilment,
            final Map<String, StockDatum> stockDataMap) {
        StockDatum stockDatum;
        StockLevelStatus onlineStockLevelStatus;

        for (final FulfilmentItem item : fulfilment.getItems()) {
            stockDatum = getStockDatum(stockDataMap, item.getSkuRef());
            onlineStockLevelStatus = getOnlineStockLevel(item.getAvailableQty());
            switch (fulfilment.getFulfilmentType()) {
                case TgtCoreConstants.DELIVERY_TYPE.CC:
                    stockDatum.setAtsCc(onlineStockLevelStatus);
                    stockDatum.setAtsCcQty(item.getAvailableQty());
                    break;
                case TgtCoreConstants.DELIVERY_TYPE.HD:
                    stockDatum.setAtsHd(onlineStockLevelStatus);
                    stockDatum.setAtsHdQty(item.getAvailableQty());
                    break;
                case TgtCoreConstants.DELIVERY_TYPE.ED:
                    stockDatum.setAtsEd(onlineStockLevelStatus);
                    stockDatum.setAtsEdQty(item.getAvailableQty());
                    break;
                case TgtCoreConstants.DELIVERY_TYPE.PO:
                    stockDatum.setAtsPo(onlineStockLevelStatus);
                    stockDatum.setAtsPoQty(item.getAvailableQty());
                    break;
                case TgtCoreConstants.DELIVERY_TYPE.CONSOLIDATED_STORES_SOH:
                    stockDatum.setConsolidatedStoreStock(
                            (item.getAvailableQty() != null && item.getAvailableQty().intValue() > 0)
                                    ? StockLevelStatus.INSTOCK
                                    : StockLevelStatus.OUTOFSTOCK);
                    stockDatum.setConsolidatedStoreStockQty(item.getAvailableQty());
                    break;
                case TgtCoreConstants.DELIVERY_TYPE.STORE_SOH:
                    initStoreSohMap(stockDatum);
                    stockDatum.getStoreSoh().put(fulfilment.getLocationRef(), storeStockLevelStatusStrategy
                            .checkStatus(String.valueOf(item.getAvailableQty())));
                    stockDatum.getStoreSohQty().put(fulfilment.getLocationRef(), item.getAvailableQty());
                    break;
                default:
                    LOG.warn("unrecongnised fulfilmentType=" + fulfilment.getFulfilmentType() + " in plan="
                            + plan.getPlanId());
                    break;
            }
        }
    }

    /**
     * 
     * @param stockDataMap
     * @param skuRef
     * @return {@link StockDatum}
     */
    private StockDatum getStockDatum(final Map<String, StockDatum> stockDataMap, final String skuRef) {
        StockDatum stockDatum = stockDataMap.get(skuRef);
        if (stockDatum == null) {
            stockDatum = new StockDatum();
            stockDatum.setVariantCode(skuRef);
            stockDataMap.put(skuRef, stockDatum);
        }

        return stockDatum;
    }

    /**
     * @param stockDatum
     */
    private void initStoreSohMap(final StockDatum stockDatum) {
        if (stockDatum == null) {
            return;
        }
        if (stockDatum.getStoreSoh() == null) {
            stockDatum.setStoreSoh(new LinkedHashMap<String, StockLevelStatus>());
        }
        if (stockDatum.getStoreSohQty() == null) {
            stockDatum.setStoreSohQty(new LinkedHashMap<String, Integer>());
        }
    }

    /**
     * @param fluentResponse
     * @return {@link Map}
     */
    protected Map<String, FluentStock> createProductResponse(final FulfilmentOptionResponse fluentResponse) {
        final Map<String, FluentStock> stockMap = new HashMap<>();
        FluentStock fluentStock;
        StockData stockData;
        for (final FulfilmentOptionAttribute attribute : fluentResponse.getAttributes()) {
            if (!(attribute.getValue() instanceof StockAttribute)) {
                continue;
            }
            final StockAttribute fluentStockAttribute = (StockAttribute)attribute.getValue();
            fluentStock = new FluentStock();

            stockData = new StockData();
            // TODO ATS = ats.hd?
            stockData.setStockLevel(Long.valueOf(fluentStockAttribute.getAts().getHd()));
            stockData.setStockLevelStatus(getOnlineStockLevel(Integer.valueOf(fluentStockAttribute.getAts().getHd())));
            fluentStock.setAts(stockData);

            stockData = new StockData();
            stockData.setStockLevel(Long.valueOf(fluentStockAttribute.getConsolidatedStock()));
            stockData.setStockLevelStatus(fluentStockAttribute.getConsolidatedStock() > 0 ? StockLevelStatus.INSTOCK
                    : StockLevelStatus.OUTOFSTOCK);
            fluentStock.setConsolidated(stockData);

            stockData = new StockData();
            stockData.setStockLevel(Long.valueOf(fluentStockAttribute.getStoreStock()));
            stockData.setStockLevelStatus(storeStockLevelStatusStrategy
                    .checkStatus(String.valueOf(fluentStockAttribute.getStoreStock())));
            fluentStock.setStore(stockData);

            stockMap.put(fluentStockAttribute.getSkuRef(), fluentStock);
        }
        return stockMap;
    }

    /**
     * @param fluentResponse
     * @return map of sku to stockLevel
     */
    protected Map<String, Integer> createSkusResponse(final FulfilmentOptionResponse fluentResponse) {
        final Map<String, Integer> skuStockMap = new HashMap<>();
        for (final FulfilmentOptionItem item : fluentResponse.getItems()) {
            skuStockMap.put(item.getSkuRef(), item.getAvailableQuantity());
        }
        return skuStockMap;
    }

    /**
     * @param stockValue
     * @return {@link StockLevelStatus}
     */
    private StockLevelStatus getOnlineStockLevel(final Integer stockValue) {
        StockLevelModel stockLevel;
        if (stockValue == null) {
            stockLevel = null;
        }
        else {
            stockLevel = new StockLevelModel();
            stockLevel.setAvailable(stockValue.intValue());
        }
        return onlineStocklevelStatusStrategy.checkStatus(stockLevel);
    }

    /**
     * @param stockDatum
     * @return ats
     */
    private int findAts(final StockDatum stockDatum) {
        final int maxQty = NumberUtils.max(getQtyAsInt(stockDatum.getAtsCcQty()),
                getQtyAsInt(stockDatum.getAtsHdQty()), getQtyAsInt(stockDatum.getAtsEdQty()));
        return (Math.max(maxQty, getQtyAsInt(stockDatum.getAtsPoQty())));
    }

    /**
     * @param qty
     * @return qty as long
     */
    private int getQtyAsInt(final Integer qty) {
        return qty != null ? qty.intValue() : 0;
    }

    /**
     * @param fluentClient
     *            the fluentClient to set
     */
    @Required
    public void setFluentClient(final FluentClient fluentClient) {
        this.fluentClient = fluentClient;
    }

    /**
     * @param onlineStocklevelStatusStrategy
     *            the onlineStocklevelStatusStrategy to set
     */
    @Required
    public void setOnlineStocklevelStatusStrategy(final StockLevelStatusStrategy onlineStocklevelStatusStrategy) {
        this.onlineStocklevelStatusStrategy = onlineStocklevelStatusStrategy;
    }

    /**
     * @param storeStockLevelStatusStrategy
     *            the storeStockLevelStatusStrategy to set
     */
    @Required
    public void setStoreStockLevelStatusStrategy(final TargetStockLevelStatusStrategy storeStockLevelStatusStrategy) {
        this.storeStockLevelStatusStrategy = storeStockLevelStatusStrategy;
    }
}

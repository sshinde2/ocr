/**
 * 
 */
package au.com.target.tgtfluent.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;


/**
 * @author bhuang3
 *
 */
public enum FluentDeliveryType {
    STANDARD("STANDARD"),
    EXPRESS("EXPRESS"),
    OVERNIGHT("OVERNIGHT"),
    THREEHOURS("3HOURS");

    private static final Map<String, FluentDeliveryType> LOOKUP = new HashMap<>();

    private final String value;

    static {
        for (final FluentDeliveryType fluentDeliveryType : FluentDeliveryType.values()) {
            LOOKUP.put(fluentDeliveryType.value, fluentDeliveryType);
        }
    }

    private FluentDeliveryType(final String value) {
        this.value = value;
    }

    public static FluentDeliveryType get(final String code) {
        return LOOKUP.get(code);
    }

    public static Set<String> getAllAvailableTypes() {
        return LOOKUP.keySet();
    }
}

/**
 * 
 */
package au.com.target.tgtfluent.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.ImmutableList;


/**
 * @author bhuang3
 *
 */
public enum FluentFulfilmentType {
    CC_PFS("CC_PFS"),
    CC_PFDC("CC_PFDC"),
    HD_PFS("HD_PFS"),
    HD_PFDC("HD_PFDC");

    public static final List<FluentFulfilmentType> HOME_DELIVERY_TYPE = ImmutableList.of(HD_PFS, HD_PFDC);

    public static final List<FluentFulfilmentType> CC_TYPE = ImmutableList.of(CC_PFS, CC_PFDC);

    private static final Map<String, FluentFulfilmentType> LOOKUP = new HashMap<>();

    private final String value;

    static {
        for (final FluentFulfilmentType fluentFulfilmentType : FluentFulfilmentType.values()) {
            LOOKUP.put(fluentFulfilmentType.value, fluentFulfilmentType);
        }
    }

    private FluentFulfilmentType(final String value) {
        this.value = value;
    }

    public static FluentFulfilmentType get(final String code) {
        return LOOKUP.get(code);
    }

    public static Set<String> getAllAvailableTypes() {
        return LOOKUP.keySet();
    }
}

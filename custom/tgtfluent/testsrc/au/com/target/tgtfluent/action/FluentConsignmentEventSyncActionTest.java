package au.com.target.tgtfluent.action;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfluent.constants.TgtFluentConstants;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.exception.FluentFulfilmentException;
import au.com.target.tgtfluent.service.FluentConsignmentService;
import au.com.target.tgtfluent.service.FluentFulfilmentService;
import au.com.target.tgtfulfilment.enums.OfcOrderType;


/**
 * 
 * @author pvarghe2
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentConsignmentEventSyncActionTest {

    @InjectMocks
    private final FluentConsignmentEventSyncAction fluentConsignmentEventSyncAction = new FluentConsignmentEventSyncAction();

    @Mock
    private FluentConsignmentService fluentConsignmentService;

    @Mock
    private FluentFulfilmentService fluentFulfilmentService;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Test
    public void testExecuteInternalNullProcess() throws RetryLaterException, Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Consignment cannot be null");
        fluentConsignmentEventSyncAction.executeInternal(null);
        verifyZeroInteractions(fluentFulfilmentService);
    }

    @Test
    public void testExecuteInternalNullConsignment() throws RetryLaterException, Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Consignment cannot be null");

        final OrderProcessModel process = mock(OrderProcessModel.class);
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(null);
        fluentConsignmentEventSyncAction.executeInternal(process);
        verifyZeroInteractions(fluentFulfilmentService);
    }

    @Test
    public void testExecuteInternalNonFluentConsignment() throws RetryLaterException, Exception {
        final OrderProcessModel process = mock(OrderProcessModel.class);
        final TargetConsignmentModel consignmentModel = mock(TargetConsignmentModel.class);
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignmentModel);
        final String transitionResult = fluentConsignmentEventSyncAction.executeInternal(process);
        verifyZeroInteractions(fluentFulfilmentService);
        assertThat(transitionResult).isNotEmpty().isEqualTo("OK");
    }

    @Test
    public void testExecuteInternalFluentConsignment() throws RetryLaterException, Exception {
        final OrderProcessModel process = mock(OrderProcessModel.class);
        final TargetConsignmentModel consignmentModel = mock(TargetConsignmentModel.class);
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignmentModel);
        willReturn(Boolean.TRUE).given(fluentConsignmentService).isAFluentConsignment(consignmentModel);
        final String transitionResult = fluentConsignmentEventSyncAction.executeInternal(process);
        verify(fluentFulfilmentService).sendConsignmentEventToFluent(consignmentModel,
                TgtFluentConstants.TargetOfcEvent.OFC_SHIPPED, true);
        assertThat(transitionResult).isNotEmpty().isEqualTo("OK");
    }

    @Test
    public void testExecuteInternalCNCSameStoreFluentConsignment() throws RetryLaterException, Exception {
        final OrderProcessModel process = mock(OrderProcessModel.class);
        final TargetConsignmentModel consignmentModel = mock(TargetConsignmentModel.class);
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignmentModel);
        given(consignmentModel.getOfcOrderType()).willReturn(OfcOrderType.INSTORE_PICKUP);
        willReturn(Boolean.TRUE).given(fluentConsignmentService).isAFluentConsignment(consignmentModel);
        final String transitionResult = fluentConsignmentEventSyncAction.executeInternal(process);
        verify(fluentFulfilmentService).sendConsignmentEventToFluent(consignmentModel,
                TgtFluentConstants.TargetOfcEvent.OFC_SS_CNC_SHIPPED, true);
        assertThat(transitionResult).isNotEmpty().isEqualTo("OK");
    }

    @Test
    public void testExecuteInternalThrowsFluentFulfilmentException() throws RetryLaterException, Exception {
        expectedException.expect(RetryLaterException.class);
        expectedException
                .expectMessage(
                        "FLUENT_OFC_SYNC_ERROR : ConsignmentId=1234, exceptionMessage=code=code123, message=some message");

        final OrderProcessModel process = mock(OrderProcessModel.class);
        final TargetConsignmentModel consignmentModel = mock(TargetConsignmentModel.class);
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignmentModel);
        given(consignmentModel.getCode()).willReturn("1234");
        willReturn(Boolean.TRUE).given(fluentConsignmentService).isAFluentConsignment(consignmentModel);
        final Error error = new Error();
        error.setMessage("some message");
        error.setCode("code123");
        willThrow(new FluentClientException(error)).given(fluentFulfilmentService)
                .sendConsignmentEventToFluent(consignmentModel, TgtFluentConstants.TargetOfcEvent.OFC_SHIPPED, true);
        fluentConsignmentEventSyncAction.executeInternal(process);
        verify(fluentFulfilmentService).sendConsignmentEventToFluent(consignmentModel,
                TgtFluentConstants.TargetOfcEvent.OFC_SHIPPED, true);
    }

    @Test
    public void testExecuteInternalThrowsFluentClientException() throws RetryLaterException, Exception {
        final OrderProcessModel process = mock(OrderProcessModel.class);
        final TargetConsignmentModel consignmentModel = mock(TargetConsignmentModel.class);
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignmentModel);
        given(consignmentModel.getCode()).willReturn("1234");
        willReturn(Boolean.TRUE).given(fluentConsignmentService).isAFluentConsignment(consignmentModel);
        willThrow(new FluentFulfilmentException("some Exception")).given(fluentFulfilmentService)
                .sendConsignmentEventToFluent(consignmentModel, TgtFluentConstants.TargetOfcEvent.OFC_SHIPPED, true);
        final String transitionResult = fluentConsignmentEventSyncAction.executeInternal(process);
        verify(fluentFulfilmentService).sendConsignmentEventToFluent(consignmentModel,
                TgtFluentConstants.TargetOfcEvent.OFC_SHIPPED, true);
        assertThat(transitionResult).isNotEmpty().isEqualTo("NOK");

    }
}

/**
 * 
 */
package au.com.target.tgtfluent.converter.populator;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfluent.data.Address;
import au.com.target.tgtfluent.data.Fulfilment;


/**
 * @author bpottass
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentConsignmentWarehouseReversePopulatorTest {

    @InjectMocks
    @Spy
    private final FluentConsignmentWarehouseReversePopulator fluentConsignmentWarehouseReversePopulator = new FluentConsignmentWarehouseReversePopulator();

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private TargetPointOfServiceService targetPointOfServiceService;

    @Mock
    private OrderModel orderModel;

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private AddressModel shippingAddress;

    @Mock
    private WarehouseModel warehouseModel;

    private final String orderId = "testOrderId";

    @Before
    public void setup() {
        given(consignment.getOrder()).willReturn(orderModel);
        given(orderModel.getCode()).willReturn(orderId);
    }

    @Test
    public void testPopulateWarehouseForRejectedFulfilment() throws ParseException {
        final Fulfilment fulfilment = new Fulfilment();
        fulfilment.setStatus("SYSTEM_REJECTED");
        fluentConsignmentWarehouseReversePopulator.populate(fulfilment, consignment);

        verify(targetWarehouseService).getWarehouseForCode(TgtCoreConstants.CANCELLATION_WAREHOUSE);
    }

    @Test
    public void testPopulateWarehouseForStore() throws ParseException, NumberFormatException,
            TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final Fulfilment fulfilment = new Fulfilment();
        fulfilment.setFromAddress(createAddress("1234"));
        given(targetWarehouseService.getWarehouseForFluentLocationRef("1234")).willThrow(new RuntimeException("test"));
        given(targetPointOfServiceService.getWarehouseByStoreNumber(Integer.valueOf("1234")))
                .willReturn(warehouseModel);
        fluentConsignmentWarehouseReversePopulator.populate(fulfilment, consignment);

        verify(targetWarehouseService).getWarehouseForFluentLocationRef("1234");
        verify(consignment).setWarehouse(warehouseModel);
    }

    private Address createAddress(final String locationRef) {
        final Address address = new Address();
        address.setLocationRef(locationRef);
        return address;
    }
}

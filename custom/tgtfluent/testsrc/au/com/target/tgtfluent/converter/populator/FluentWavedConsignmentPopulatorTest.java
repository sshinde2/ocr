package au.com.target.tgtfluent.converter.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;

import org.junit.Test;

import au.com.target.tgtfluent.data.FulfilmentItem;


@UnitTest
public class FluentWavedConsignmentPopulatorTest {

    private final FluentWavedConsignmentPopulator populator = new FluentWavedConsignmentPopulator();

    @Test
    public void testCreateItem() {
        final ConsignmentEntryModel entry = mock(ConsignmentEntryModel.class);
        final AbstractOrderEntryModel entryModel = mock(AbstractOrderEntryModel.class);
        final ProductModel product = mock(ProductModel.class);
        final String productId = "P1234";
        given(entry.getOrderEntry()).willReturn(entryModel);
        given(entryModel.getProduct()).willReturn(product);
        given(product.getCode()).willReturn("P1234");
        given(entry.getQuantity()).willReturn(Long.valueOf(2l));
        final FulfilmentItem fulfilmentItem = populator.createItem(entry);
        assertThat(fulfilmentItem).isNotNull();
        assertThat(fulfilmentItem.getOrderItemRef()).isEqualTo(productId);
        assertThat(fulfilmentItem.getRequestedQty()).isEqualTo(2);
    }
}

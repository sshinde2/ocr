/**
 * 
 */
package au.com.target.tgtfluent.converter.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.storelocator.model.OpeningDayModel;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetOpeningDayModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfluent.data.Address;
import au.com.target.tgtfluent.data.Location;
import au.com.target.tgtfluent.data.OpeningHours;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class LocationPopulatorTest {

    @Mock
    private TargetPointOfServiceModel source;

    @Mock
    private AddressModel address;

    @Mock
    private OpeningScheduleModel openingSchedule;

    @Mock
    private OpeningHours defaultOpeningHours;

    @Spy
    @InjectMocks
    private final LocationPopulator locationPopulator = new LocationPopulator();

    @Before
    public void setup() {
        locationPopulator.setDefaultEmail("store@target.com.au");
        locationPopulator.setDefaultPhone("1300753567");
        locationPopulator.setDefaultCountry("Australia");
        final Map<String, String> timezoneStateMap = new HashMap<>();
        timezoneStateMap.put("VIC", "Australia/Melbourne");
        timezoneStateMap.put("NSW", "Australia/Sydney");
        locationPopulator.setTimezoneStateMap(timezoneStateMap);
        final Map<String, String> timezoneCityMap = new HashMap<>();
        timezoneCityMap.put("Broken Hill", "Australia/Broken_Hill");
        locationPopulator.setTimezoneCityMap(timezoneCityMap);

        given(source.getStoreNumber()).willReturn(Integer.valueOf(5423));
        given(source.getName()).willReturn("Werribee");
        given(source.getClosed()).willReturn(Boolean.FALSE);
        given(source.getAddress()).willReturn(address);
        given(source.getOpeningSchedule()).willReturn(openingSchedule);
        given(source.getLatitude()).willReturn(Double.valueOf(-37.876364));
        given(source.getLongitude()).willReturn(Double.valueOf(144.678964));

        final CountryModel country = mock(CountryModel.class);
        given(country.getName()).willReturn("Australia");
        given(address.getCountry()).willReturn(country);
        given(address.getTown()).willReturn("Hoppers Crossing");
        given(address.getPostalcode()).willReturn("3029");
        given(address.getDistrict()).willReturn("VIC");
        given(address.getStreetname()).willReturn("Cnr Heaths and Derrimut Roads");
    }

    @Test
    public void testPopulate() {
        given(address.getEmail()).willReturn("waurnpondsstore@target.com.au");
        given(address.getPhone1()).willReturn("0345678912");
        given(address.getDistrict()).willReturn("VIC");
        final Location target = new Location();
        final Address address = mock(Address.class);
        willReturn(address).given(locationPopulator).getAddress(source);
        final OpeningHours openingHours = mock(OpeningHours.class);
        willReturn(openingHours).given(locationPopulator).getOpeningHours(source);

        locationPopulator.populate(source, target);
        assertThat(target.getLocationRef()).isEqualTo("5423");
        assertThat(target.getName()).isEqualTo("Werribee");
        assertThat(target.getStatus()).isEqualTo("ACTIVE");
        assertThat(target.getEmail()).isEqualTo("waurnpondsstore@target.com.au");
        assertThat(target.getSupportPhone()).isEqualTo("0345678912");
        assertThat(target.getType()).isEqualTo("STORE");
        assertThat(target.getTimeZone()).isEqualTo("Australia/Melbourne");
        assertThat(target.getAddress()).isEqualTo(address);
        assertThat(target.getOpeningHours()).isEqualTo(openingHours);
    }

    @Test
    public void testPopulateWhenAddressNull() {
        given(source.getAddress()).willReturn(null);
        final Location target = new Location();
        final Address address = mock(Address.class);
        willReturn(address).given(locationPopulator).getAddress(source);
        final OpeningHours openingHours = mock(OpeningHours.class);
        willReturn(openingHours).given(locationPopulator).getOpeningHours(source);

        locationPopulator.populate(source, target);
        assertThat(target.getLocationRef()).isEqualTo("5423");
        assertThat(target.getName()).isEqualTo("Werribee");
        assertThat(target.getStatus()).isEqualTo("ACTIVE");
        assertThat(target.getEmail()).isEqualTo("store@target.com.au");
        assertThat(target.getSupportPhone()).isEqualTo("1300753567");
        assertThat(target.getType()).isEqualTo("STORE");
        assertThat(target.getTimeZone()).isNull();
        assertThat(target.getAddress()).isEqualTo(address);
        assertThat(target.getOpeningHours()).isEqualTo(openingHours);
    }

    @Test
    public void testPopulateDefaults() {
        given(address.getDistrict()).willReturn("NSW");
        final Location target = new Location();
        final Address address = mock(Address.class);
        willReturn(address).given(locationPopulator).getAddress(source);
        final OpeningHours openingHours = mock(OpeningHours.class);
        willReturn(openingHours).given(locationPopulator).getOpeningHours(source);

        locationPopulator.populate(source, target);
        assertThat(target.getLocationRef()).isEqualTo("5423");
        assertThat(target.getName()).isEqualTo("Werribee");
        assertThat(target.getStatus()).isEqualTo("ACTIVE");
        assertThat(target.getEmail()).isEqualTo("store@target.com.au");
        assertThat(target.getSupportPhone()).isEqualTo("1300753567");
        assertThat(target.getType()).isEqualTo("STORE");
        assertThat(target.getTimeZone()).isEqualTo("Australia/Sydney");
        assertThat(target.getAddress()).isEqualTo(address);
        assertThat(target.getOpeningHours()).isEqualTo(openingHours);
    }

    @Test
    public void testPopulateTimeZoneUsingTown() {
        given(address.getDistrict()).willReturn("NSW");
        given(address.getTown()).willReturn("Broken Hill");
        final Location target = new Location();
        final Address address = mock(Address.class);
        willReturn(address).given(locationPopulator).getAddress(source);
        final OpeningHours openingHours = mock(OpeningHours.class);
        willReturn(openingHours).given(locationPopulator).getOpeningHours(source);

        locationPopulator.populate(source, target);
        assertThat(target.getLocationRef()).isEqualTo("5423");
        assertThat(target.getName()).isEqualTo("Werribee");
        assertThat(target.getStatus()).isEqualTo("ACTIVE");
        assertThat(target.getEmail()).isEqualTo("store@target.com.au");
        assertThat(target.getSupportPhone()).isEqualTo("1300753567");
        assertThat(target.getType()).isEqualTo("STORE");
        assertThat(target.getTimeZone()).isEqualTo("Australia/Broken_Hill");
        assertThat(target.getAddress()).isEqualTo(address);
        assertThat(target.getOpeningHours()).isEqualTo(openingHours);
    }

    @Test
    public void testGetAddress() {
        final Address address = locationPopulator.getAddress(source);
        assertThat(address).isNotNull();
        assertThat(address.getLocationRef()).isEqualTo("5423");
        assertThat(address.getCompanyName()).isEqualTo("Target");
        assertThat(address.getName()).isEqualTo("Werribee");
        assertThat(address.getCity()).isEqualTo("Hoppers Crossing");
        assertThat(address.getCountry()).isEqualTo("Australia");
        assertThat(address.getPostcode()).isEqualTo("3029");
        assertThat(address.getState()).isEqualTo("VIC");
        assertThat(address.getStreet()).isEqualTo("Cnr Heaths and Derrimut Roads");
        assertThat(address.getLatitude()).isEqualTo("-37.876364");
        assertThat(address.getLongitude()).isEqualTo("144.678964");
    }

    @Test
    public void testGetAddressWhenSourceAddressNull() {
        given(source.getAddress()).willReturn(null);

        final Address address = locationPopulator.getAddress(source);
        assertThat(address).isNotNull();
        assertThat(address.getLocationRef()).isEqualTo("5423");
        assertThat(address.getCompanyName()).isEqualTo("Target");
        assertThat(address.getName()).isEqualTo("Werribee");
        assertThat(address.getCity()).isNull();
        assertThat(address.getCountry()).isNull();
        assertThat(address.getPostcode()).isNull();
        assertThat(address.getState()).isNull();
        assertThat(address.getStreet()).isNull();
        assertThat(address.getLatitude()).isEqualTo("-37.876364");
        assertThat(address.getLongitude()).isEqualTo("144.678964");
    }

    @Test
    public void testGetAddressWhenLatLongNull() {
        given(source.getLatitude()).willReturn(null);
        given(source.getLongitude()).willReturn(null);

        final Address address = locationPopulator.getAddress(source);
        assertThat(address).isNotNull();
        assertThat(address.getLocationRef()).isEqualTo("5423");
        assertThat(address.getCompanyName()).isEqualTo("Target");
        assertThat(address.getName()).isEqualTo("Werribee");
        assertThat(address.getCity()).isEqualTo("Hoppers Crossing");
        assertThat(address.getCountry()).isEqualTo("Australia");
        assertThat(address.getPostcode()).isEqualTo("3029");
        assertThat(address.getState()).isEqualTo("VIC");
        assertThat(address.getStreet()).isEqualTo("Cnr Heaths and Derrimut Roads");
        assertThat(address.getLatitude()).isNull();
        assertThat(address.getLongitude()).isNull();
    }

    @Test
    public void testGetAddressWithDefaultCountry() {
        given(address.getCountry()).willReturn(null);

        final Address address = locationPopulator.getAddress(source);
        assertThat(address).isNotNull();
        assertThat(address.getLocationRef()).isEqualTo("5423");
        assertThat(address.getCompanyName()).isEqualTo("Target");
        assertThat(address.getName()).isEqualTo("Werribee");
        assertThat(address.getCity()).isEqualTo("Hoppers Crossing");
        assertThat(address.getCountry()).isEqualTo("Australia");
        assertThat(address.getPostcode()).isEqualTo("3029");
        assertThat(address.getState()).isEqualTo("VIC");
        assertThat(address.getStreet()).isEqualTo("Cnr Heaths and Derrimut Roads");
        assertThat(address.getLatitude()).isEqualTo("-37.876364");
        assertThat(address.getLongitude()).isEqualTo("144.678964");
    }

    @Test
    public void testGetOpeningHours() {
        OpeningDayModel openingDayModel;
        final Collection<OpeningDayModel> openingDays = new ArrayList<>();
        given(openingSchedule.getOpeningDays()).willReturn(openingDays);
        final Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 8, 10, 1, 0);
        for (int i = 0; i < 14; i++) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            calendar.add(Calendar.HOUR, 1);
            openingDayModel = mock(OpeningDayModel.class);
            given(openingDayModel.getOpeningTime()).willReturn(calendar.getTime());
            calendar.add(Calendar.HOUR, 10);
            given(openingDayModel.getClosingTime()).willReturn(calendar.getTime());
            openingDays.add(openingDayModel);
            calendar.add(Calendar.HOUR, -10);
        }
        calendar.set(Calendar.DAY_OF_MONTH, 14);
        willReturn(calendar.getTime()).given(locationPopulator).getNow();

        final OpeningHours openingHours = locationPopulator.getOpeningHours(source);
        assertThat(openingHours.getMonStartMin()).isEqualTo("0900");
        assertThat(openingHours.getMonEndMin()).isEqualTo("1900");
        assertThat(openingHours.getTueStartMin()).isEqualTo("1000");
        assertThat(openingHours.getTueEndMin()).isEqualTo("2000");
        assertThat(openingHours.getWedStartMin()).isEqualTo("1100");
        assertThat(openingHours.getWedEndMin()).isEqualTo("2100");
        assertThat(openingHours.getThuStartMin()).isEqualTo("1200");
        assertThat(openingHours.getThuEndMin()).isEqualTo("2200");
        assertThat(openingHours.getFriStartMin()).isEqualTo("0600");
        assertThat(openingHours.getFriEndMin()).isEqualTo("1600");
        assertThat(openingHours.getSatStartMin()).isEqualTo("0700");
        assertThat(openingHours.getSatEndMin()).isEqualTo("1700");
        assertThat(openingHours.getSunStartMin()).isEqualTo("0800");
        assertThat(openingHours.getSunEndMin()).isEqualTo("1800");
    }

    @Test
    public void testGetOpeningHoursDefault() {
        given(openingSchedule.getOpeningDays()).willReturn(null);

        final OpeningHours openingHours = locationPopulator.getOpeningHours(source);
        assertThat(openingHours).isEqualTo(defaultOpeningHours);
    }

    @Test
    public void testGetOpeningHoursForClosedStore() {
        final TargetOpeningDayModel openingDay = mock(TargetOpeningDayModel.class);
        willReturn(Boolean.TRUE).given(openingDay).isClosed();
        final Calendar sunday = Calendar.getInstance();
        sunday.set(2017, 9, 22, 0, 0);
        willReturn(sunday.getTime()).given(openingDay).getOpeningTime();
        final OpeningHours openingHours = new OpeningHours();

        locationPopulator.setDayMins(openingHours, openingDay);
        assertThat(openingHours.getSunStartMin()).isEqualTo("-1");
        assertThat(openingHours.getSunEndMin()).isEqualTo("-1");
    }

    @Test
    public void testGetFilteredOpeningDaysPast() {
        final DateTimeFormatter openingHoursFormatter = DateTimeFormat.forPattern("MMddHHmm");
        OpeningDayModel openingDayModel;
        final Collection<OpeningDayModel> openingDays = new ArrayList<>();
        given(openingSchedule.getOpeningDays()).willReturn(openingDays);
        final Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 8, 10, 1, 0);
        for (int i = 0; i < 14; i++) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            calendar.add(Calendar.HOUR, 1);
            openingDayModel = mock(OpeningDayModel.class);
            given(openingDayModel.getOpeningTime()).willReturn(calendar.getTime());
            calendar.add(Calendar.HOUR, 10);
            given(openingDayModel.getClosingTime()).willReturn(calendar.getTime());
            openingDays.add(openingDayModel);
            calendar.add(Calendar.HOUR, -10);
        }
        willReturn(calendar.getTime()).given(locationPopulator).getNow();

        final List<OpeningDayModel> sortedOpeningDays = locationPopulator.getFilteredOpeningDays(openingSchedule);
        assertThat(sortedOpeningDays).isNotNull().hasSize(7);
        assertThat(openingHoursFormatter.print(sortedOpeningDays.get(0).getOpeningTime().getTime()))
                .isEqualTo("09180900");
        assertThat(openingHoursFormatter.print(sortedOpeningDays.get(6).getOpeningTime().getTime()))
                .isEqualTo("09241500");
    }

    @Test
    public void testGetFilteredOpeningDaysMix1() {
        final DateTimeFormatter openingHoursFormatter = DateTimeFormat.forPattern("MMddHHmm");
        OpeningDayModel openingDayModel;
        final Collection<OpeningDayModel> openingDays = new ArrayList<>();
        given(openingSchedule.getOpeningDays()).willReturn(openingDays);
        final Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 8, 10, 1, 0);
        for (int i = 0; i < 14; i++) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            calendar.add(Calendar.HOUR, 1);
            openingDayModel = mock(OpeningDayModel.class);
            given(openingDayModel.getOpeningTime()).willReturn(calendar.getTime());
            calendar.add(Calendar.HOUR, 10);
            given(openingDayModel.getClosingTime()).willReturn(calendar.getTime());
            openingDays.add(openingDayModel);
            calendar.add(Calendar.HOUR, -10);
        }
        calendar.set(Calendar.DAY_OF_MONTH, 14);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        willReturn(calendar.getTime()).given(locationPopulator).getNow();

        final List<OpeningDayModel> sortedOpeningDays = locationPopulator.getFilteredOpeningDays(openingSchedule);
        assertThat(sortedOpeningDays).isNotNull().hasSize(7);
        assertThat(openingHoursFormatter.print(sortedOpeningDays.get(0).getOpeningTime().getTime()))
                .isEqualTo("09140500");
        assertThat(openingHoursFormatter.print(sortedOpeningDays.get(6).getOpeningTime().getTime()))
                .isEqualTo("09201100");
    }

    @Test
    public void testGetFilteredOpeningDaysMix2() {
        final DateTimeFormatter openingHoursFormatter = DateTimeFormat.forPattern("MMddHHmm");
        OpeningDayModel openingDayModel;
        final Collection<OpeningDayModel> openingDays = new ArrayList<>();
        given(openingSchedule.getOpeningDays()).willReturn(openingDays);
        final Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 8, 10, 1, 0);
        for (int i = 0; i < 14; i++) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            calendar.add(Calendar.HOUR, 1);
            openingDayModel = mock(OpeningDayModel.class);
            given(openingDayModel.getOpeningTime()).willReturn(calendar.getTime());
            calendar.add(Calendar.HOUR, 10);
            given(openingDayModel.getClosingTime()).willReturn(calendar.getTime());
            openingDays.add(openingDayModel);
            calendar.add(Calendar.HOUR, -10);
        }
        calendar.set(Calendar.DAY_OF_MONTH, 18);
        willReturn(calendar.getTime()).given(locationPopulator).getNow();

        final List<OpeningDayModel> sortedOpeningDays = locationPopulator.getFilteredOpeningDays(openingSchedule);
        assertThat(sortedOpeningDays).isNotNull().hasSize(7);
        assertThat(openingHoursFormatter.print(sortedOpeningDays.get(0).getOpeningTime().getTime()))
                .isEqualTo("09180900");
        assertThat(openingHoursFormatter.print(sortedOpeningDays.get(6).getOpeningTime().getTime()))
                .isEqualTo("09241500");
    }

    @Test
    public void testGetFilteredOpeningDaysFuture() {
        final DateTimeFormatter openingHoursFormatter = DateTimeFormat.forPattern("MMddHHmm");
        OpeningDayModel openingDayModel;
        final Collection<OpeningDayModel> openingDays = new ArrayList<>();
        given(openingSchedule.getOpeningDays()).willReturn(openingDays);
        final Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 8, 10, 1, 0);
        for (int i = 0; i < 14; i++) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            calendar.add(Calendar.HOUR, 1);
            openingDayModel = mock(OpeningDayModel.class);
            given(openingDayModel.getOpeningTime()).willReturn(calendar.getTime());
            calendar.add(Calendar.HOUR, 10);
            given(openingDayModel.getClosingTime()).willReturn(calendar.getTime());
            openingDays.add(openingDayModel);
            calendar.add(Calendar.HOUR, -10);
        }
        calendar.set(Calendar.DAY_OF_MONTH, 5);
        willReturn(calendar.getTime()).given(locationPopulator).getNow();

        final List<OpeningDayModel> sortedOpeningDays = locationPopulator.getFilteredOpeningDays(openingSchedule);
        assertThat(sortedOpeningDays).isNotNull().hasSize(7);
        assertThat(openingHoursFormatter.print(sortedOpeningDays.get(0).getOpeningTime().getTime()))
                .isEqualTo("09110200");
        assertThat(openingHoursFormatter.print(sortedOpeningDays.get(6).getOpeningTime().getTime()))
                .isEqualTo("09170800");
    }

    @Test
    public void testGetNow() {
        final Calendar reference = Calendar.getInstance();
        reference.add(Calendar.DAY_OF_MONTH, -1);
        final Date now = locationPopulator.getNow();
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        assertThat(calendar.get(Calendar.HOUR_OF_DAY)).isEqualTo(23);
        assertThat(calendar.get(Calendar.MINUTE)).isEqualTo(59);
        assertThat(calendar.get(Calendar.SECOND)).isEqualTo(59);
        assertThat(calendar.get(Calendar.DAY_OF_MONTH)).isEqualTo(reference.get(Calendar.DAY_OF_MONTH));
        assertThat(calendar.get(Calendar.MONTH)).isEqualTo(reference.get(Calendar.MONTH));
        assertThat(calendar.get(Calendar.YEAR)).isEqualTo(reference.get(Calendar.YEAR));
    }

}

package au.com.target.tgtfluent.converter.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.model.GiftRecipientModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfluent.data.Attribute;
import au.com.target.tgtfluent.data.AttributeType;
import au.com.target.tgtfluent.data.FulfilmentChoice;
import au.com.target.tgtfluent.data.FulfilmentChoiceType;
import au.com.target.tgtfluent.data.GiftCardRecipient;
import au.com.target.tgtfluent.data.Order;
import au.com.target.tgtfluent.data.OrderType;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderPopulatorTest {

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Mock
    private OrderModel orderModel;

    @Mock
    private DeliveryModeModel deliveryMode;

    @Mock
    private CurrencyModel currency;

    @Mock
    private AddressModel address;

    @Mock
    private AbstractOrderEntryModel orderEntry;

    @Mock
    private AbstractOrderEntryModel orderEntry2;

    @Mock
    private ProductTypeModel productTypeModel;

    @Mock
    private ProductTypeModel productTypeModel2;

    @Mock
    private TargetProductModel targetProductModel;

    @Mock
    private TargetProductModel targetProductModel2;

    @Mock
    private GiftRecipientModel giftRecipientModel;

    @Mock
    private GiftRecipientModel giftRecipientModel2;

    @Mock
    private TargetCustomerModel customer;

    @Mock
    private SalesApplication salesApplication;

    @Mock
    private CountryModel country;

    @InjectMocks
    private final OrderPopulator orderPopulator = new OrderPopulator();

    @Before
    public void setUp() throws Exception {

        given(orderModel.getCode()).willReturn("123456");
        given(orderModel.getDeliveryMode()).willReturn(deliveryMode);
        willReturn(Boolean.FALSE).given(targetDeliveryModeHelper).isDeliveryModeStoreDelivery(deliveryMode);

        given(orderModel.getCurrency()).willReturn(currency);
        given(currency.getIsocode()).willReturn("AUD");

        given(orderModel.getDeliveryAddress()).willReturn(address);
        given(address.getFirstname()).willReturn("Peter");
        given(address.getLastname()).willReturn("Jackson");
        given(orderModel.getCncStoreNumber()).willReturn(Integer.valueOf(5009));
        given(address.getStreetname()).willReturn("12 Thomson Road");
        given(address.getPostalcode()).willReturn("3216");
        given(address.getTown()).willReturn("Geelong");
        given(address.getDistrict()).willReturn("VIC");

        given(address.getCountry()).willReturn(country);
        given(country.getIsocode()).willReturn("AU");

        given(deliveryMode.getCode()).willReturn("express-delivery");
        given(orderModel.getDeliveryCost()).willReturn(Double.valueOf(10));

        given(orderModel.getEntries()).willReturn(Collections.singletonList(orderEntry));

        given(orderEntry.getProduct()).willReturn(targetProductModel);
        given(targetProductModel.getCode()).willReturn("P100_BLACK");

        given(orderEntry.getQuantity()).willReturn(Long.valueOf(2));
        given(orderEntry.getBasePrice()).willReturn(Double.valueOf(10));
        given(orderEntry.getTotalPrice()).willReturn(Double.valueOf(20));

        given(productTypeModel.getCode()).willReturn("giftCard");
        given(targetProductModel.getProductType()).willReturn(productTypeModel);

        given(orderModel.getTotalPrice()).willReturn(Double.valueOf(30));

        given(orderModel.getUser()).willReturn(customer);
        given(customer.getFirstname()).willReturn("Movyn");
        given(customer.getLastname()).willReturn("John");
        given(customer.getCustomerID()).willReturn("123");
        given(customer.getContactEmail()).willReturn("movyn.john@retail.com");
        given(customer.getMobileNumber()).willReturn("0431759049");

        given(orderModel.getSalesApplication()).willReturn(salesApplication);
        given(salesApplication.getCode()).willReturn("web");

    }

    @Test
    public void testPopulateHdOrder() {
        final Order orderRequest = new Order();
        orderPopulator.populate(orderModel, orderRequest);

        assertThat(orderRequest.getOrderRef()).isEqualTo("123456");
        assertThat(orderRequest.getTotalPrice()).isEqualTo(30);
        assertThat(orderRequest.getType()).isEqualTo(OrderType.HD);
        assertThat(orderRequest.getFulfilmentChoice()).isNotNull();
        assertThat(orderRequest.getItems()).isNotEmpty();
        assertThat(orderRequest.getAttributes()).isNotEmpty();
        assertThat(orderRequest.getCustomer()).isNotNull();

        assertThat(orderRequest.getFulfilmentChoice()).isInstanceOf(FulfilmentChoice.class);
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getName()).isEqualTo("Peter Jackson");
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getStreet()).isEqualTo("12 Thomson Road");
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getPostcode()).isEqualTo("3216");
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getCity()).isEqualTo("Geelong");
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getState()).isEqualTo("VIC");
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getCountry()).isEqualTo("AU");
        assertThat(orderRequest.getTotalPrice()).isEqualTo(Double.valueOf(30));
        assertThat(orderRequest.getFulfilmentChoice().getFulfilmentType()).isEqualTo(FulfilmentChoiceType.HD_PFS);

        assertThat(orderRequest.getItems()).onProperty("skuRef").containsExactly("P100_BLACK");
        assertThat(orderRequest.getItems()).onProperty("requestedQty").containsExactly(Integer.valueOf(2));
        assertThat(orderRequest.getItems()).onProperty("totalPrice").containsExactly(Double.valueOf(20));
        assertThat(orderRequest.getItems()).onProperty("currency").containsExactly("AUD");

        assertThat(orderRequest.getCustomer().getCustomerRef()).isEqualTo("123");
        assertThat(orderRequest.getCustomer().getFirstName()).isEqualTo("Movyn");
        assertThat(orderRequest.getCustomer().getLastName()).isEqualTo("John");
        assertThat(orderRequest.getCustomer().getMobile()).isEqualTo("0431759049");
        assertThat(orderRequest.getCustomer().getEmail()).isEqualTo("movyn.john@retail.com");
    }

    @Test
    public void testPopulateHdOrderWithoutCustomerMobile() {
        given(customer.getMobileNumber()).willReturn(null);
        final Order orderRequest = new Order();
        orderPopulator.populate(orderModel, orderRequest);

        assertThat(orderRequest.getOrderRef()).isEqualTo("123456");
        assertThat(orderRequest.getTotalPrice()).isEqualTo(30);
        assertThat(orderRequest.getType()).isEqualTo(OrderType.HD);
        assertThat(orderRequest.getFulfilmentChoice()).isNotNull();
        assertThat(orderRequest.getItems()).isNotEmpty();
        assertThat(orderRequest.getAttributes()).isNotEmpty();
        assertThat(orderRequest.getCustomer()).isNotNull();

        assertThat(orderRequest.getFulfilmentChoice()).isInstanceOf(FulfilmentChoice.class);
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getName()).isEqualTo("Peter Jackson");
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getStreet()).isEqualTo("12 Thomson Road");
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getPostcode()).isEqualTo("3216");
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getCity()).isEqualTo("Geelong");
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getState()).isEqualTo("VIC");
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getCountry()).isEqualTo("AU");
        assertThat(orderRequest.getTotalPrice()).isEqualTo(Double.valueOf(30));
        assertThat(orderRequest.getFulfilmentChoice().getFulfilmentType()).isEqualTo(FulfilmentChoiceType.HD_PFS);

        assertThat(orderRequest.getItems()).onProperty("skuRef").containsExactly("P100_BLACK");
        assertThat(orderRequest.getItems()).onProperty("requestedQty").containsExactly(Integer.valueOf(2));
        assertThat(orderRequest.getItems()).onProperty("totalPrice").containsExactly(Double.valueOf(20));
        assertThat(orderRequest.getItems()).onProperty("currency").containsExactly("AUD");

        assertThat(orderRequest.getCustomer().getCustomerRef()).isEqualTo("123");
        assertThat(orderRequest.getCustomer().getFirstName()).isEqualTo("Movyn");
        assertThat(orderRequest.getCustomer().getLastName()).isEqualTo("John");
        assertThat(orderRequest.getCustomer().getMobile()).isEqualTo(null);
        assertThat(orderRequest.getCustomer().getEmail()).isEqualTo("movyn.john@retail.com");
    }

    @Test
    public void testPopulateCnCOrderWithDeliveryUpdates() {
        final Order orderRequest = new Order();
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper).isDeliveryModeStoreDelivery(deliveryMode);
        given(deliveryMode.getCode()).willReturn("click-and-collect");
        given(orderModel.getDeliveryCost()).willReturn(Double.valueOf(0));
        given(orderModel.getDeliveryAddress().getPhone1()).willReturn("0422523151");

        orderPopulator.populate(orderModel, orderRequest);

        assertThat(orderRequest.getOrderRef()).isEqualTo("123456");
        assertThat(orderRequest.getTotalPrice()).isEqualTo(30);
        assertThat(orderRequest.getType()).isEqualTo(OrderType.CC);

        assertThat(orderRequest.getFulfilmentChoice()).isNotNull();
        assertThat(orderRequest.getItems()).isNotEmpty();
        assertThat(orderRequest.getAttributes()).isNotEmpty();
        assertThat(orderRequest.getCustomer()).isNotNull();

        assertThat(orderRequest.getFulfilmentChoice()).isInstanceOf(FulfilmentChoice.class);
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getLocationRef()).isEqualTo("5009");
        assertThat(orderRequest.getTotalPrice()).isEqualTo(Double.valueOf(30));
        assertThat(orderRequest.getFulfilmentChoice().getFulfilmentType()).isEqualTo(FulfilmentChoiceType.CC_PFS);

        assertThat(orderRequest.getItems()).onProperty("skuRef").containsExactly("P100_BLACK");
        assertThat(orderRequest.getItems()).onProperty("requestedQty").containsExactly(Integer.valueOf(2));
        assertThat(orderRequest.getItems()).onProperty("totalPrice").containsExactly(Double.valueOf(20));
        assertThat(orderRequest.getItems()).onProperty("currency").containsExactly("AUD");

        assertThat(orderRequest.getCustomer().getCustomerRef()).isEqualTo("123");
        assertThat(orderRequest.getCustomer().getFirstName()).isEqualTo(address.getFirstname());
        assertThat(orderRequest.getCustomer().getLastName()).isEqualTo(address.getLastname());
        assertThat(orderRequest.getCustomer().getMobile()).isEqualTo("0422523151");
    }

    @Test
    public void testPopulateCnCOrderWithCellPhone() {
        final Order orderRequest = new Order();
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper).isDeliveryModeStoreDelivery(deliveryMode);
        given(deliveryMode.getCode()).willReturn("click-and-collect");
        given(orderModel.getDeliveryAddress().getCellphone()).willReturn("0422523152");

        orderPopulator.populate(orderModel, orderRequest);

        assertThat(orderRequest.getCustomer().getMobile()).isEqualTo("0422523152");
    }

    @Test
    public void testPopulateCnCOrderWithoutCustomerMobile() {
        given(customer.getMobileNumber()).willReturn(null);
        final Order orderRequest = new Order();
        willReturn(Boolean.TRUE).given(targetDeliveryModeHelper).isDeliveryModeStoreDelivery(deliveryMode);
        given(deliveryMode.getCode()).willReturn("click-and-collect");
        given(orderModel.getDeliveryCost()).willReturn(Double.valueOf(0));
        orderPopulator.populate(orderModel, orderRequest);

        assertThat(orderRequest.getOrderRef()).isEqualTo("123456");
        assertThat(orderRequest.getTotalPrice()).isEqualTo(30);
        assertThat(orderRequest.getType()).isEqualTo(OrderType.CC);

        assertThat(orderRequest.getFulfilmentChoice()).isNotNull();
        assertThat(orderRequest.getItems()).isNotEmpty();
        assertThat(orderRequest.getAttributes()).isNotEmpty();
        assertThat(orderRequest.getCustomer()).isNotNull();

        assertThat(orderRequest.getFulfilmentChoice()).isInstanceOf(FulfilmentChoice.class);
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getLocationRef()).isEqualTo("5009");
        assertThat(orderRequest.getTotalPrice()).isEqualTo(Double.valueOf(30));
        assertThat(orderRequest.getFulfilmentChoice().getFulfilmentType()).isEqualTo(FulfilmentChoiceType.CC_PFS);

        assertThat(orderRequest.getItems()).onProperty("skuRef").containsExactly("P100_BLACK");
        assertThat(orderRequest.getItems()).onProperty("requestedQty").containsExactly(Integer.valueOf(2));
        assertThat(orderRequest.getItems()).onProperty("totalPrice").containsExactly(Double.valueOf(20));
        assertThat(orderRequest.getItems()).onProperty("currency").containsExactly("AUD");

        assertThat(orderRequest.getCustomer().getCustomerRef()).isEqualTo("123");
        assertThat(orderRequest.getCustomer().getFirstName()).isEqualTo(address.getFirstname());
        assertThat(orderRequest.getCustomer().getLastName()).isEqualTo(address.getLastname());
        assertThat(orderRequest.getCustomer().getMobile()).isEqualTo(null);
    }

    @Test
    public void testPopulateGiftCardOrderForOneSkuTwoRecipient() {
        final Order orderRequest = new Order();
        given(orderModel.getDeliveryAddress()).willReturn(null);
        given(deliveryMode.getCode()).willReturn("digital-gift-card");
        given(orderModel.getDeliveryCost()).willReturn(Double.valueOf(0));
        given(productTypeModel.getCode()).willReturn("digital");

        given(giftRecipientModel.getFirstName()).willReturn("Steve");
        given(giftRecipientModel.getLastName()).willReturn("Jobs");
        given(giftRecipientModel.getEmail()).willReturn("steve.jobs@apple.com");
        given(giftRecipientModel.getMessageText()).willReturn("Enjoy xbox gold membership");

        given(giftRecipientModel2.getFirstName()).willReturn("Bill");
        given(giftRecipientModel2.getLastName()).willReturn("Gates");
        given(giftRecipientModel2.getEmail()).willReturn("bill.gates@microsoft.com");
        given(giftRecipientModel2.getMessageText()).willReturn("Welcome to itunes");

        given(orderEntry.getGiftRecipients()).willReturn(Arrays.asList(giftRecipientModel, giftRecipientModel2));

        orderPopulator.populate(orderModel, orderRequest);

        assertThat(orderRequest.getOrderRef()).isEqualTo("123456");
        assertThat(orderRequest.getTotalPrice()).isEqualTo(30);
        assertThat(orderRequest.getType()).isEqualTo(OrderType.HD);

        assertThat(orderRequest.getFulfilmentChoice()).isNotNull();
        assertThat(orderRequest.getItems()).isNotEmpty();
        assertThat(orderRequest.getAttributes()).isNotEmpty();
        assertThat(orderRequest.getCustomer()).isNotNull();

        assertThat(orderRequest.getFulfilmentChoice()).isInstanceOf(FulfilmentChoice.class);
        assertThat(orderRequest.getFulfilmentChoice().getAddress()).isNull();
        assertThat(orderRequest.getTotalPrice()).isEqualTo(Double.valueOf(30));
        assertThat(orderRequest.getFulfilmentChoice().getFulfilmentType()).isEqualTo(FulfilmentChoiceType.HD_PFS);

        assertThat(orderRequest.getItems()).onProperty("skuRef").containsExactly("P100_BLACK");
        assertThat(orderRequest.getItems()).onProperty("requestedQty").containsExactly(Integer.valueOf(2));
        assertThat(orderRequest.getItems()).onProperty("totalPrice").containsExactly(Double.valueOf(20));
        assertThat(orderRequest.getItems()).onProperty("currency").containsExactly("AUD");

        assertThat(orderRequest.getCustomer().getCustomerRef()).isEqualTo("123");
        assertThat(orderRequest.getCustomer().getFirstName()).isEqualTo("Movyn");
        assertThat(orderRequest.getCustomer().getLastName()).isEqualTo("John");
        assertThat(orderRequest.getCustomer().getMobile()).isEqualTo("0431759049");
        assertThat(orderRequest.getCustomer().getEmail()).isEqualTo("movyn.john@retail.com");

        assertThat(orderRequest.getAttributes()).onProperty("name").containsExactly("SALES_APPLICATION",
                "SHIP_TO_PHONE", "PREORDER", "GIFTCARD_RECIPIENT");
        assertThat(orderRequest.getAttributes()).onProperty("type").containsExactly(AttributeType.STRING,
                AttributeType.STRING,
                AttributeType.STRING, AttributeType.JSON);

        final List<Attribute> attributes = orderRequest.getAttributes();
        final Attribute giftCardAttribute = attributes.get(3);
        final List<GiftCardRecipient> giftCardRecipients = (List<GiftCardRecipient>)giftCardAttribute.getValue();
        assertThat(giftCardRecipients.get(0).getSkuRef()).isEqualTo("P100_BLACK");
        assertThat(giftCardRecipients.get(0).getRecipients()).onProperty("firstName").contains("Steve", "Bill");
        assertThat(giftCardRecipients.get(0).getRecipients()).onProperty("lastName").contains("Jobs", "Gates");
        assertThat(giftCardRecipients.get(0).getRecipients()).onProperty("email").contains("steve.jobs@apple.com",
                "bill.gates@microsoft.com");
        assertThat(giftCardRecipients.get(0).getRecipients()).onProperty("message")
                .contains("Enjoy xbox gold membership", "Welcome to itunes");

    }

    @Test
    public void testPopulateGiftCardOrderForTwoSkus() {
        final Order orderRequest = new Order();
        given(orderModel.getDeliveryAddress()).willReturn(null);
        given(deliveryMode.getCode()).willReturn("digital-gift-card");
        given(orderModel.getDeliveryCost()).willReturn(Double.valueOf(0));
        given(productTypeModel.getCode()).willReturn("digital");

        given(orderModel.getEntries()).willReturn(Arrays.asList(orderEntry, orderEntry2));

        given(giftRecipientModel.getFirstName()).willReturn("Steve");
        given(giftRecipientModel.getLastName()).willReturn("Jobs");
        given(giftRecipientModel.getEmail()).willReturn("steve.jobs@apple.com");
        given(giftRecipientModel.getMessageText()).willReturn("Enjoy xbox gold membership");

        given(giftRecipientModel2.getFirstName()).willReturn("Bill");
        given(giftRecipientModel2.getLastName()).willReturn("Gates");
        given(giftRecipientModel2.getEmail()).willReturn("bill.gates@microsoft.com");
        given(giftRecipientModel2.getMessageText()).willReturn("Welcome to itunes");

        given(orderEntry.getGiftRecipients()).willReturn(Arrays.asList(giftRecipientModel, giftRecipientModel2));

        given(orderEntry2.getProduct()).willReturn(targetProductModel2);
        given(targetProductModel2.getCode()).willReturn("889691_nocolour");
        given(productTypeModel2.getCode()).willReturn("digital");
        given(targetProductModel2.getProductType()).willReturn(productTypeModel2);

        given(orderEntry2.getQuantity()).willReturn(Long.valueOf(1));
        given(orderEntry2.getBasePrice()).willReturn(Double.valueOf(20));
        given(orderEntry2.getTotalPrice()).willReturn(Double.valueOf(20));

        final GiftRecipientModel giftRecipientModel21 = mock(GiftRecipientModel.class);
        given(giftRecipientModel21.getFirstName()).willReturn("Tim");
        given(giftRecipientModel21.getLastName()).willReturn("Cook");
        given(giftRecipientModel21.getEmail()).willReturn("tim.cook@apple.com");
        given(giftRecipientModel21.getMessageText()).willReturn("Enjoy netflix");

        given(orderEntry2.getGiftRecipients()).willReturn(Arrays.asList(giftRecipientModel21));

        orderPopulator.populate(orderModel, orderRequest);

        assertThat(orderRequest.getOrderRef()).isEqualTo("123456");
        assertThat(orderRequest.getTotalPrice()).isEqualTo(30);
        assertThat(orderRequest.getType()).isEqualTo(OrderType.HD);

        assertThat(orderRequest.getFulfilmentChoice()).isNotNull();
        assertThat(orderRequest.getItems()).isNotEmpty();
        assertThat(orderRequest.getAttributes()).isNotEmpty();
        assertThat(orderRequest.getCustomer()).isNotNull();

        assertThat(orderRequest.getFulfilmentChoice()).isInstanceOf(FulfilmentChoice.class);
        assertThat(orderRequest.getFulfilmentChoice().getAddress()).isNull();
        assertThat(orderRequest.getTotalPrice()).isEqualTo(Double.valueOf(30));
        assertThat(orderRequest.getFulfilmentChoice().getFulfilmentType()).isEqualTo(FulfilmentChoiceType.HD_PFS);

        assertThat(orderRequest.getItems()).onProperty("skuRef").contains("P100_BLACK", "889691_nocolour");
        assertThat(orderRequest.getItems()).onProperty("requestedQty").contains(Integer.valueOf(2),
                Integer.valueOf(1));
        assertThat(orderRequest.getItems()).onProperty("totalPrice").containsExactly(Double.valueOf(20),
                Double.valueOf(20));
        assertThat(orderRequest.getItems()).onProperty("currency").containsExactly("AUD", "AUD");

        assertThat(orderRequest.getCustomer().getCustomerRef()).isEqualTo("123");
        assertThat(orderRequest.getCustomer().getFirstName()).isEqualTo("Movyn");
        assertThat(orderRequest.getCustomer().getLastName()).isEqualTo("John");
        assertThat(orderRequest.getCustomer().getMobile()).isEqualTo("0431759049");
        assertThat(orderRequest.getCustomer().getEmail()).isEqualTo("movyn.john@retail.com");

        assertThat(orderRequest.getAttributes()).onProperty("name").containsExactly("SALES_APPLICATION",
                "SHIP_TO_PHONE", "PREORDER", "GIFTCARD_RECIPIENT");
        assertThat(orderRequest.getAttributes()).onProperty("type").containsExactly(AttributeType.STRING,
                AttributeType.STRING,
                AttributeType.STRING, AttributeType.JSON);

        final List<Attribute> attributes = orderRequest.getAttributes();
        final Attribute giftCardAttribute = attributes.get(3);
        final List<GiftCardRecipient> giftCardRecipients = (List<GiftCardRecipient>)giftCardAttribute.getValue();
        assertThat(giftCardRecipients.get(0).getSkuRef()).isEqualTo("P100_BLACK");
        assertThat(giftCardRecipients.get(0).getRecipients()).onProperty("firstName").contains("Steve", "Bill");
        assertThat(giftCardRecipients.get(0).getRecipients()).onProperty("lastName").contains("Jobs", "Gates");
        assertThat(giftCardRecipients.get(0).getRecipients()).onProperty("email").contains("steve.jobs@apple.com",
                "bill.gates@microsoft.com");
        assertThat(giftCardRecipients.get(0).getRecipients()).onProperty("message")
                .contains("Enjoy xbox gold membership", "Welcome to itunes");

        assertThat(giftCardRecipients.get(1).getSkuRef()).isEqualTo("889691_nocolour");
        assertThat(giftCardRecipients.get(1).getRecipients()).onProperty("firstName").contains("Tim");
        assertThat(giftCardRecipients.get(1).getRecipients()).onProperty("lastName").contains("Cook");
        assertThat(giftCardRecipients.get(1).getRecipients()).onProperty("email").contains("tim.cook@apple.com");
        assertThat(giftCardRecipients.get(1).getRecipients()).onProperty("message").contains("Enjoy netflix");
    }

    @Test
    public void testPopulateAddressWhenStreetNumberFeildIsNotEmpty() {

        final Order orderRequest = new Order();
        given(address.getStreetname()).willReturn("71");
        given(address.getStreetnumber()).willReturn("Osborne Av");

        orderPopulator.populate(orderModel, orderRequest);

        assertThat(orderRequest.getFulfilmentChoice()).isInstanceOf(FulfilmentChoice.class);
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getName()).isEqualTo("Peter Jackson");
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getStreet()).isEqualTo("Osborne Av");
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getCompanyName()).isEqualTo("71");
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getPostcode()).isEqualTo("3216");
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getCity()).isEqualTo("Geelong");
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getState()).isEqualTo("VIC");
        assertThat(orderRequest.getFulfilmentChoice().getAddress().getCountry()).isEqualTo("AU");
        assertThat(orderRequest.getTotalPrice()).isEqualTo(Double.valueOf(30));
        assertThat(orderRequest.getFulfilmentChoice().getFulfilmentType()).isEqualTo(FulfilmentChoiceType.HD_PFS);
    }

}

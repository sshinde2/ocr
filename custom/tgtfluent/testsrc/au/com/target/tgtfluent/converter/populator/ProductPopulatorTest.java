/**
 * 
 */
package au.com.target.tgtfluent.converter.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Sets;

import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfluent.data.AttributeType;
import au.com.target.tgtfluent.data.Product;
import au.com.target.tgtfluent.data.ProductStatus;
import au.com.target.tgtfluent.exception.ProductFluentFeedException;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductPopulatorTest {

    private final ProductPopulator productPopulator = new ProductPopulator();

    @Mock
    private TargetProductModel model;


    @Mock
    private ProductTypeModel productType;

    @Mock
    private TargetProductCategoryModel primaryCategory;

    @Mock
    private DeliveryModeModel homeDelivery;

    @Mock
    private DeliveryModeModel cncDelivery;

    @Mock
    private DeliveryModeModel expressDelivery;

    @Mock
    private DeliveryModeModel digitalDelivery;

    @Mock
    private DeliveryModeModel ebayDelivery;

    @Mock
    private DeliveryModeModel ebayCncDelivery;

    @Mock
    private DeliveryModeModel ebayExpressDelivery;

    @Mock
    private ConfigurationService configurationService;

    private final Set<DeliveryModeModel> deliveryModes = new HashSet<>();

    @Before
    public void setup() {
        given(model.getCode()).willReturn("W1000");
        given(productType.getCode()).willReturn("normal");
        given(primaryCategory.getFluentId()).willReturn("primaryCategory_fid");
        given(homeDelivery.getCode()).willReturn("home-delivery");
        given(cncDelivery.getCode()).willReturn("click-and-collect");
        given(expressDelivery.getCode()).willReturn("express-delivery");
        given(digitalDelivery.getCode()).willReturn("digital-gift-card");
        given(ebayDelivery.getCode()).willReturn("eBay-delivery");
        given(ebayCncDelivery.getCode()).willReturn("ebay-click-and-collect");
        given(ebayExpressDelivery.getCode()).willReturn("ebay-express-delivery");
        deliveryModes.addAll(Sets.newHashSet(homeDelivery, cncDelivery, expressDelivery, digitalDelivery, ebayDelivery,
                ebayCncDelivery, ebayExpressDelivery));
        given(model.getPrimarySuperCategory()).willReturn(primaryCategory);
    }

    @Test(expected = ProductFluentFeedException.class)
    public void testPopulateWithNoPrimaryCategory() {
        given(model.getPrimarySuperCategory()).willReturn(null);
        final Product product = new Product();
        productPopulator.populate(model, product);
    }

    @Test(expected = ProductFluentFeedException.class)
    public void testPopulateWithPrimaryCategoryWithoutFluentId() {
        given(primaryCategory.getFluentId()).willReturn(null);

        final Product product = new Product();
        productPopulator.populate(model, product);
    }

    @Test
    public void testPopulateWithProductAttributes() {
        given(model.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(model.getProductType()).willReturn(productType);
        given(model.getPrimarySuperCategory()).willReturn(primaryCategory);
        given(model.getDisplayOnly()).willReturn(Boolean.TRUE);
        given(model.getPreview()).willReturn(Boolean.TRUE);
        given(model.getDeliveryModes()).willReturn(deliveryModes);

        final Product product = new Product();
        productPopulator.populate(model, product);

        assertThat(product.getCategories().size()).isEqualTo(1);
        assertThat(product.getCategories().get(0)).isEqualTo("primaryCategory_fid");
        assertThat(product.getStatus()).isEqualTo(ProductStatus.ACTIVE);
        assertThat(product.getAttributes()).onProperty("name").containsExactly("PRODUCT_TYPE", "DISPLAY_ONLY",
                "PREVIEW", "CLICK_AND_COLLECT", "HOME_DELIVERY", "EXPRESS_DELIVERY", "EMAIL_DELIVERY");
        assertThat(product.getAttributes()).onProperty("type").containsExactly(AttributeType.STRING,
                AttributeType.STRING, AttributeType.STRING, AttributeType.STRING, AttributeType.STRING,
                AttributeType.STRING, AttributeType.STRING);
        assertThat(product.getAttributes()).onProperty("value").containsExactly("normal", "TRUE", "TRUE",
                "TRUE", "TRUE", "TRUE", "TRUE");
    }

}

/**
 * 
 */
package au.com.target.tgtfluent.converter.populator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.GiftCardModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfluent.data.Attribute;
import au.com.target.tgtfluent.data.AttributeType;
import au.com.target.tgtfluent.data.Reference;
import au.com.target.tgtfluent.data.Sku;
import au.com.target.tgtfluent.data.SkuStatus;
import au.com.target.tgtutility.util.TargetDateUtil;
import au.com.target.tgtutility.util.TimeZoneUtils;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SkuPopulatorTest {

    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    @Mock
    private TargetSizeVariantProductModel sizeVariantProduct;

    @Mock
    private TargetColourVariantProductModel colourVariantProduct;

    @Mock
    private TargetProductModel product;

    @Mock
    private TargetMerchDepartmentModel merchDepartment;

    @Spy
    @InjectMocks
    private final SkuPopulator skuPopulator = new SkuPopulator();

    @Before
    public void setup() {
        skuPopulator.setImageBaseUrl("https://fe-qat.target.com.au");
        given(sizeVariantProduct.getCode()).willReturn("SV01");
        given(sizeVariantProduct.getName()).willReturn("Size Variant 1");

        given(colourVariantProduct.getCode()).willReturn("CV01");
        given(colourVariantProduct.getName()).willReturn("Colour Variant 1");

        given(product.getCode()).willReturn("B01");
    }

    @Test
    public void testPopulateWithSizeVariant() {
        willReturn(product).given(skuPopulator).getBaseProduct(sizeVariantProduct);
        willReturn(SkuStatus.ACTIVE).given(skuPopulator).getSkuStatus(sizeVariantProduct);
        willReturn("/images/SV01").given(skuPopulator).getImageUrlRef(sizeVariantProduct);
        final List<Reference> sReferences = mock(List.class);
        willReturn(sReferences).given(skuPopulator).getReferences(sizeVariantProduct);
        final List<Attribute> sAttributes = mock(List.class);
        willReturn(sAttributes).given(skuPopulator).getAttributes(sizeVariantProduct, product);
        final Sku target = new Sku();
        skuPopulator.populate(sizeVariantProduct, target);

        assertThat(target.getSkuRef()).isEqualTo("SV01");
        assertThat(target.getProductRef()).isEqualTo("B01");
        assertThat(target.getStatus()).isEqualTo(SkuStatus.ACTIVE);
        assertThat(target.getImageUrlRef()).isEqualTo("/images/SV01");
        assertThat(target.getName()).isEqualTo("Size Variant 1");
        assertThat(target.getReferences()).isEqualTo(sReferences);
        assertThat(target.getAttributes()).isEqualTo(sAttributes);
    }

    @Test
    public void testPopulateWithColourVariant() {
        given(product.getCode()).willReturn("B02");
        willReturn(product).given(skuPopulator).getBaseProduct(colourVariantProduct);
        willReturn(SkuStatus.ACTIVE).given(skuPopulator).getSkuStatus(colourVariantProduct);
        willReturn("/images/CV01").given(skuPopulator).getImageUrlRef(colourVariantProduct);
        final List<Reference> cReferences = mock(List.class);
        willReturn(cReferences).given(skuPopulator).getReferences(colourVariantProduct);
        final List<Attribute> cAttributes = mock(List.class);
        willReturn(cAttributes).given(skuPopulator).getAttributes(colourVariantProduct, product);
        final Sku target = new Sku();
        skuPopulator.populate(colourVariantProduct, target);

        assertThat(target.getSkuRef()).isEqualTo("CV01");
        assertThat(target.getProductRef()).isEqualTo("B02");
        assertThat(target.getStatus()).isEqualTo(SkuStatus.ACTIVE);
        assertThat(target.getImageUrlRef()).isEqualTo("/images/CV01");
        assertThat(target.getName()).isEqualTo("Colour Variant 1");
        assertThat(target.getReferences()).isEqualTo(cReferences);
        assertThat(target.getAttributes()).isEqualTo(cAttributes);
    }

    @Test
    public void getBaseProductWithSizeVariant() {
        given(sizeVariantProduct.getBaseProduct()).willReturn(colourVariantProduct);
        given(colourVariantProduct.getBaseProduct()).willReturn(product);
        given(product.getCode()).willReturn("B01");

        assertThat(skuPopulator.getBaseProduct(sizeVariantProduct)).isEqualTo(product);
    }

    @Test
    public void getBaseProductWithColourVariant() {
        given(colourVariantProduct.getBaseProduct()).willReturn(product);
        given(product.getCode()).willReturn("B01");

        assertThat(skuPopulator.getBaseProduct(colourVariantProduct)).isEqualTo(product);
    }

    @Test
    public void testGetSkuStatusApproved() {
        given(sizeVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);

        assertThat(skuPopulator.getSkuStatus(sizeVariantProduct)).isEqualTo(SkuStatus.ACTIVE);
    }

    @Test
    public void testGetSkuStatusCheck() {
        given(sizeVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.CHECK);

        assertThat(skuPopulator.getSkuStatus(sizeVariantProduct)).isEqualTo(SkuStatus.INACTIVE);
    }

    @Test
    public void testGetSkuStatusUnApproved() {
        given(sizeVariantProduct.getApprovalStatus()).willReturn(ArticleApprovalStatus.UNAPPROVED);

        assertThat(skuPopulator.getSkuStatus(sizeVariantProduct)).isEqualTo(SkuStatus.INACTIVE);
    }

    @Test
    public void testGetImageUrlRefForSizeVariant() {
        given(sizeVariantProduct.getBaseProduct()).willReturn(colourVariantProduct);
        final List<MediaContainerModel> mediaContainerList = new ArrayList<>();
        final MediaContainerModel mediaContainer = mock(MediaContainerModel.class);

        final Collection<MediaModel> medias = new ArrayList<>();
        final MediaModel media1 = mock(MediaModel.class);
        given(media1.getCode()).willReturn("/large/1");
        given(media1.getURL()).willReturn("/large/1");
        medias.add(media1);
        given(mediaContainer.getMedias()).willReturn(medias);
        mediaContainerList.add(mediaContainer);
        given(colourVariantProduct.getGalleryImages()).willReturn(mediaContainerList);

        assertThat(skuPopulator.getImageUrlRef(sizeVariantProduct)).isEqualTo("https://fe-qat.target.com.au/large/1");
    }

    @Test
    public void testGetImageUrlRefForColourVariant() {
        given(sizeVariantProduct.getBaseProduct()).willReturn(colourVariantProduct);
        final List<MediaContainerModel> mediaContainerList = new ArrayList<>();
        final MediaContainerModel mediaContainer = mock(MediaContainerModel.class);

        final Collection<MediaModel> medias = new ArrayList<>();
        final MediaModel media1 = mock(MediaModel.class);
        given(media1.getCode()).willReturn("/large/1");
        given(media1.getURL()).willReturn("/large/1");
        medias.add(media1);
        given(mediaContainer.getMedias()).willReturn(medias);
        mediaContainerList.add(mediaContainer);
        given(colourVariantProduct.getGalleryImages()).willReturn(mediaContainerList);

        assertThat(skuPopulator.getImageUrlRef(colourVariantProduct)).isEqualTo("https://fe-qat.target.com.au/large/1");
    }

    @Test
    public void testGetReferences() {
        given(colourVariantProduct.getEan()).willReturn("123123");

        final List<Reference> references = skuPopulator.getReferences(colourVariantProduct);
        assertThat(references).hasSize(1);
        assertThat(references.get(0).getType()).isEqualTo("BARCODE");
        assertThat(references.get(0).getValue()).isEqualTo("123123");
    }

    @Test
    public void testGetReferencesEmpty() {
        final List<Reference> references = skuPopulator.getReferences(colourVariantProduct);
        assertThat(references).isEmpty();
    }

    @Test
    public void testGetAttributes() {
        willReturn("black").given(skuPopulator).getSwatch(sizeVariantProduct);
        given(sizeVariantProduct.getSize()).willReturn("M");
        given(product.getMerchDepartment()).willReturn(merchDepartment);
        given(merchDepartment.getName()).willReturn("Boys 1-7");
        given(sizeVariantProduct.getMerchProductStatus()).willReturn("Active");
        willReturn("TRUE").given(skuPopulator).getAssorted(sizeVariantProduct);
        final TargetProductDimensionsModel dimensions = mock(TargetProductDimensionsModel.class);
        given(dimensions.getWeight()).willReturn(Double.valueOf(2.1));
        given(dimensions.getHeight()).willReturn(Double.valueOf(10.9));
        given(dimensions.getLength()).willReturn(Double.valueOf(5.5));
        given(dimensions.getWidth()).willReturn(Double.valueOf(3.4));
        given(sizeVariantProduct.getProductPackageDimensions()).willReturn(dimensions);

        final List<Attribute> attributes = skuPopulator.getAttributes(sizeVariantProduct, product);
        assertThat(attributes).hasSize(10);
        assertThat(attributes).onProperty("name").containsExactly("SWATCH", "SIZE", "DEPARTMENT", "ASSORTED",
                "MERCH_PRODUCT_STATUS", "WEIGHT",
                "HEIGHT", "LENGTH", "WIDTH", "EBAY");
        assertThat(attributes).onProperty("type").containsExactly(AttributeType.STRING, AttributeType.STRING,
                AttributeType.STRING, AttributeType.STRING, AttributeType.STRING, AttributeType.STRING,
                AttributeType.STRING,
                AttributeType.STRING, AttributeType.STRING, AttributeType.STRING);
        assertThat(attributes).onProperty("value").containsExactly("black", "M", "Boys 1-7", "TRUE", "Active", "2.1",
                "10.9",
                "5.5", "3.4", "FALSE");
    }

    @Test
    public void testGetAttributesWithNulls() {
        given(sizeVariantProduct.getBaseProduct()).willReturn(colourVariantProduct);
        willReturn("black").given(skuPopulator).getSwatch(sizeVariantProduct);
        given(sizeVariantProduct.getSize()).willReturn("M");
        given(sizeVariantProduct.getDepartment()).willReturn(null);
        willReturn("FALSE").given(skuPopulator).getAssorted(sizeVariantProduct);
        final TargetProductDimensionsModel dimensions = mock(TargetProductDimensionsModel.class);
        given(dimensions.getWeight()).willReturn(Double.valueOf(2.1));
        given(dimensions.getHeight()).willReturn(Double.valueOf(10.9));
        given(dimensions.getLength()).willReturn(null);
        given(dimensions.getWidth()).willReturn(null);
        given(colourVariantProduct.getPreOrderEndDateTime()).willReturn(null);
        given(colourVariantProduct.getPreOrderStartDateTime()).willReturn(null);
        given(colourVariantProduct.getNormalSaleStartDateTime()).willReturn(null);
        given(sizeVariantProduct.getProductPackageDimensions()).willReturn(dimensions);
        final List<Attribute> attributes = skuPopulator.getAttributes(sizeVariantProduct, product);
        assertThat(attributes).hasSize(6);
        assertThat(attributes).onProperty("name").containsExactly("SWATCH", "SIZE", "ASSORTED", "WEIGHT", "HEIGHT",
                "EBAY");
        assertThat(attributes).onProperty("type").containsExactly(AttributeType.STRING, AttributeType.STRING,
                AttributeType.STRING, AttributeType.STRING, AttributeType.STRING, AttributeType.STRING);
        assertThat(attributes).onProperty("value").containsExactly("black", "M", "FALSE", "2.1", "10.9", "FALSE");
    }

    @Test
    public void testGetAttributesForGiftCard() {
        final ProductTypeModel productType = mock(ProductTypeModel.class);
        willReturn("giftCard").given(productType).getName();
        willReturn(productType).given(sizeVariantProduct).getProductType();
        final GiftCardModel giftCardReference = mock(GiftCardModel.class);
        willReturn("brandId").given(giftCardReference).getBrandId();
        willReturn(giftCardReference).given(product).getGiftCard();
        willReturn(Double.valueOf(2.0)).given(sizeVariantProduct).getDenomination();
        willReturn("disco").given(colourVariantProduct).getGiftCardStyle();
        willReturn(colourVariantProduct).given(sizeVariantProduct).getBaseProduct();
        willReturn("black").given(skuPopulator).getSwatch(sizeVariantProduct);
        given(sizeVariantProduct.getSize()).willReturn("M");
        given(product.getMerchDepartment()).willReturn(merchDepartment);
        given(merchDepartment.getName()).willReturn("Boys 1-7");
        given(sizeVariantProduct.getMerchProductStatus()).willReturn("Active");
        willReturn("TRUE").given(skuPopulator).getAssorted(sizeVariantProduct);
        final TargetProductDimensionsModel dimensions = mock(TargetProductDimensionsModel.class);
        given(dimensions.getWeight()).willReturn(Double.valueOf(2.1));
        given(dimensions.getHeight()).willReturn(Double.valueOf(10.9));
        given(dimensions.getLength()).willReturn(Double.valueOf(5.5));
        given(dimensions.getWidth()).willReturn(Double.valueOf(3.4));
        given(sizeVariantProduct.getProductPackageDimensions()).willReturn(dimensions);

        final List<Attribute> attributes = skuPopulator.getAttributes(sizeVariantProduct, product);
        assertThat(attributes).hasSize(13);
        assertThat(attributes).onProperty("name").containsExactly("SWATCH", "SIZE", "DEPARTMENT", "ASSORTED",
                "MERCH_PRODUCT_STATUS", "WEIGHT", "HEIGHT", "LENGTH", "WIDTH", "EBAY", "DENOMINATION", "BRAND_ID",
                "GIFT_CARD_STYLE");
        assertThat(attributes).onProperty("type").containsExactly(AttributeType.STRING, AttributeType.STRING,
                AttributeType.STRING, AttributeType.STRING, AttributeType.STRING, AttributeType.STRING,
                AttributeType.STRING, AttributeType.STRING, AttributeType.STRING, AttributeType.STRING,
                AttributeType.STRING, AttributeType.STRING, AttributeType.STRING);
        assertThat(attributes).onProperty("value").containsExactly("black", "M", "Boys 1-7", "TRUE", "Active", "2.1",
                "10.9", "5.5", "3.4", "FALSE", "2.0", "brandId", "disco");
    }

    /**
     * Test the preorder attributes for sizeVariant
     */
    @Test
    public void testGetAttributesWithPreorderAttribiutesSizeVariant() {
        given(sizeVariantProduct.getBaseProduct()).willReturn(colourVariantProduct);
        given(sizeVariantProduct.getDepartment()).willReturn(null);
        final Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 3, calendar.get(Calendar.DATE));
        final Date embargoDate = new Date(calendar.getTimeInMillis());
        final Calendar start = Calendar.getInstance();
        final Calendar end = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), start.get(Calendar.MONTH) - 1, start.get(Calendar.DATE));
        final Date startDate = new Date(start.getTimeInMillis());
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 1, end.get(Calendar.DATE));
        final Date endDate = new Date(end.getTimeInMillis());
        given(colourVariantProduct.getPreOrderStartDateTime()).willReturn(startDate);
        given(colourVariantProduct.getPreOrderEndDateTime()).willReturn(endDate);
        given(colourVariantProduct.getNormalSaleStartDateTime()).willReturn(embargoDate);

        final List<Attribute> attributes = skuPopulator.getAttributes(sizeVariantProduct, product);
        assertThat(attributes).hasSize(5);
        assertThat(attributes).onProperty("name").containsExactly("ASSORTED", "EBAY", "PREORDER_START_DATE",
                "PREORDER_END_DATE",
                "SALE_START_DATE");
        assertThat(attributes).onProperty("type").containsExactly(AttributeType.STRING, AttributeType.STRING,
                AttributeType.STRING, AttributeType.STRING, AttributeType.STRING);
        assertThat(attributes).onProperty("value").containsExactly("FALSE", "FALSE",
                TargetDateUtil.getDateAsString(startDate, DATE_FORMAT, TimeZoneUtils.TIMEZONE_UTC),
                TargetDateUtil.getDateAsString(endDate, DATE_FORMAT, TimeZoneUtils.TIMEZONE_UTC),
                TargetDateUtil.getDateAsString(embargoDate, DATE_FORMAT, TimeZoneUtils.TIMEZONE_UTC));
    }

    /**
     * Test the preorder attributes for colourvariant
     */
    @Test
    public void testGetAttributesWithPreorderAttribiutesColourVariant() {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 3, calendar.get(Calendar.DATE));
        final Date embargoDate = new Date(calendar.getTimeInMillis());
        final Calendar start = Calendar.getInstance();
        final Calendar end = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), start.get(Calendar.MONTH) - 1, start.get(Calendar.DATE));
        final Date startDate = new Date(start.getTimeInMillis());
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 1, end.get(Calendar.DATE));
        final Date endDate = new Date(end.getTimeInMillis());
        given(colourVariantProduct.getPreOrderStartDateTime()).willReturn(startDate);
        given(colourVariantProduct.getPreOrderEndDateTime()).willReturn(endDate);
        given(colourVariantProduct.getNormalSaleStartDateTime()).willReturn(embargoDate);
        given(colourVariantProduct.getDepartment()).willReturn(null);
        final List<Attribute> attributes = skuPopulator.getAttributes(colourVariantProduct, product);

        assertThat(attributes).hasSize(5);
        assertThat(attributes).onProperty("name").containsExactly("ASSORTED", "EBAY", "PREORDER_START_DATE",
                "PREORDER_END_DATE",
                "SALE_START_DATE");
        assertThat(attributes).onProperty("type").containsExactly(AttributeType.STRING, AttributeType.STRING,
                AttributeType.STRING, AttributeType.STRING, AttributeType.STRING);
        assertThat(attributes).onProperty("value").containsExactly("FALSE", "FALSE",
                TargetDateUtil.getDateAsString(startDate, DATE_FORMAT, TimeZoneUtils.TIMEZONE_UTC),
                TargetDateUtil.getDateAsString(endDate, DATE_FORMAT, TimeZoneUtils.TIMEZONE_UTC),
                TargetDateUtil.getDateAsString(embargoDate, DATE_FORMAT, TimeZoneUtils.TIMEZONE_UTC));
    }

    @Test
    public void testGetSwatchForSizeVariant() {
        given(sizeVariantProduct.getBaseProduct()).willReturn(colourVariantProduct);
        given(colourVariantProduct.getSwatchName()).willReturn("black");

        assertThat(skuPopulator.getSwatch(sizeVariantProduct)).isEqualTo("black");
    }

    @Test
    public void testGetSwatchForColourVariant() {
        given(colourVariantProduct.getSwatchName()).willReturn("black");

        assertThat(skuPopulator.getSwatch(colourVariantProduct)).isEqualTo("black");
    }

    @Test
    public void testGetAssortedForSizeVariant() {
        given(sizeVariantProduct.getBaseProduct()).willReturn(colourVariantProduct);
        willReturn(Boolean.TRUE).given(colourVariantProduct).isAssorted();

        assertThat(skuPopulator.getAssorted(sizeVariantProduct)).isEqualTo("TRUE");
    }

    @Test
    public void testGetAssortedForColourVariant() {
        willReturn(Boolean.TRUE).given(colourVariantProduct).isAssorted();

        assertThat(skuPopulator.getAssorted(colourVariantProduct)).isEqualTo("TRUE");
    }

    @Test
    public void testGetBrandIdFromBaseProduct() {
        final GiftCardModel giftCardReference = mock(GiftCardModel.class);
        willReturn("brandId").given(giftCardReference).getBrandId();
        willReturn(giftCardReference).given(product).getGiftCard();

        assertThat(skuPopulator.getBrandId(product, "giftCard")).isEqualTo("brandId");
    }

    @Test(expected = IllegalStateException.class)
    public void testGetBrandIdFail() {
        skuPopulator.getBrandId(product, "giftCard");
    }
}

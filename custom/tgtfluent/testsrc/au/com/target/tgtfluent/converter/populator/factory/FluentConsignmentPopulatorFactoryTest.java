package au.com.target.tgtfluent.converter.populator.factory;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import au.com.target.tgtfluent.constants.TgtFluentConstants;
import au.com.target.tgtfluent.converter.populator.FluentConsignmentPopulator;
import au.com.target.tgtfluent.converter.populator.FluentWavedConsignmentPopulator;


public class FluentConsignmentPopulatorFactoryTest {

    @InjectMocks
    private final FluentConsignmentPopulatorFactory fluentConsignmentPopulatorFactory = new FluentConsignmentPopulatorFactory();

    @Mock
    private FluentConsignmentPopulator fluentConsignmentPopulator;

    @Mock
    private FluentWavedConsignmentPopulator fluentWavedConsignmentPopulator;


    @Test
    public void testGetConsignmentPopulatorWaved() {
        assertThat(
                fluentConsignmentPopulatorFactory.getConsignmentPopulator(TgtFluentConstants.TargetOfcEvent.OFC_WAVED))
                        .isEqualTo(fluentWavedConsignmentPopulator);
    }

    @Test
    public void testGetConsignmentPopulator() {
        assertThat(fluentConsignmentPopulatorFactory.getConsignmentPopulator("")).isEqualTo(fluentConsignmentPopulator);
    }

}

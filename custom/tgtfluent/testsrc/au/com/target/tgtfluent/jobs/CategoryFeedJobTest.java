package au.com.target.tgtfluent.jobs;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfluent.service.CategoryFeedService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CategoryFeedJobTest {

    @Mock
    private CategoryFeedService categoryFeedService;
    @InjectMocks
    private final CategoryFeedJob categoryFeedJob = new CategoryFeedJob();


    @Test
    public void testPerformSuccess() {
        final PerformResult result = categoryFeedJob.perform(null);

        verify(categoryFeedService).feedCategoriesToFluent();
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }

    @Test
    public void testPerformFailure() {
        willThrow(new IllegalStateException()).given(categoryFeedService).feedCategoriesToFluent();
        final PerformResult result = categoryFeedJob.perform(null);

        assertThat(result.getResult()).isEqualTo(CronJobResult.FAILURE);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.ABORTED);
    }

}

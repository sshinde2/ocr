/**
 * 
 */
package au.com.target.tgtfluent.jobs;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfluent.service.ProductFeedService;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductFeedJobTest {

    @Mock
    private ProductFeedService productFeedService;

    @InjectMocks
    private final ProductFeedJob productFeedJob = new ProductFeedJob();

    @Test
    public void testPerform() {
        final PerformResult result = productFeedJob.perform(null);

        verify(productFeedService).feedProducts();
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }

    @Test
    public void testPerformFail() {
        willThrow(new IllegalStateException()).given(productFeedService).feedProducts();
        final PerformResult result = productFeedJob.perform(null);

        assertThat(result.getResult()).isEqualTo(CronJobResult.FAILURE);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.ABORTED);
    }

    @Test
    public void testIsAbortable() {
        assertThat(productFeedJob.isAbortable()).isTrue();
    }
}

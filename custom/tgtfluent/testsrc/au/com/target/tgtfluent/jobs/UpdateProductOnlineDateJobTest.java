/**
 * 
 */
package au.com.target.tgtfluent.jobs;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.product.TargetProductService;


/**
 * @author fkhan4
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UpdateProductOnlineDateJobTest {
    @Mock
    private TargetProductService targetProductService;

    @InjectMocks
    private final UpdateProductOnlineDateJob updateProductOnlineDateJob = new UpdateProductOnlineDateJob();

    @Test
    public void testPerform() {
        final PerformResult result = updateProductOnlineDateJob.perform(null);

        verify(targetProductService).setOnlineDateForApplicableProducts();
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }

    @Test
    public void testPerformFail() {
        willThrow(new IllegalStateException()).given(targetProductService).setOnlineDateForApplicableProducts();
        final PerformResult result = updateProductOnlineDateJob.perform(null);

        assertThat(result.getResult()).isEqualTo(CronJobResult.FAILURE);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.ABORTED);
    }

    @Test
    public void testIsAbortable() {
        assertThat(updateProductOnlineDateJob.isAbortable()).isTrue();
    }

}

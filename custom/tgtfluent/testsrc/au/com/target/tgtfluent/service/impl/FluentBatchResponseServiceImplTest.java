/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtfluent.dao.FluentBatchResponseDao;
import au.com.target.tgtfluent.enums.FluentBatchStatus;
import au.com.target.tgtfluent.model.AbstractFluentBatchResponseModel;
import au.com.target.tgtfluent.model.LocationFluentBatchResponseModel;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentBatchResponseServiceImplTest {

    @Mock
    private FluentBatchResponseDao fluentBatchResponseDao;

    @InjectMocks
    private final FluentBatchResponseServiceImpl fluentBatchResponseService = new FluentBatchResponseServiceImpl();

    @Test
    public void testFind() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final AbstractFluentBatchResponseModel batchResponse = mock(AbstractFluentBatchResponseModel.class);
        given(fluentBatchResponseDao.find(LocationFluentBatchResponseModel._TYPECODE, "1", "1"))
                .willReturn(batchResponse);

        assertThat(fluentBatchResponseService.find(LocationFluentBatchResponseModel._TYPECODE, "1", "1"))
                .isEqualTo(batchResponse);
    }

    @Test
    public void testFindFail() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        given(fluentBatchResponseDao.find(LocationFluentBatchResponseModel._TYPECODE, "1", "1"))
                .willThrow(new TargetUnknownIdentifierException(""));

        assertThat(fluentBatchResponseService.find(LocationFluentBatchResponseModel._TYPECODE, "1", "1")).isNull();
    }

    @Test
    public void testFindByStatus() {
        final List<AbstractFluentBatchResponseModel> batchResponses = new ArrayList<>();
        given(fluentBatchResponseDao.find(LocationFluentBatchResponseModel._TYPECODE,
                Arrays.asList(FluentBatchStatus.PENDING, FluentBatchStatus.RUNNING))).willReturn(batchResponses);

        assertThat(fluentBatchResponseService.find(LocationFluentBatchResponseModel._TYPECODE,
                Arrays.asList(FluentBatchStatus.PENDING, FluentBatchStatus.RUNNING))).isEqualTo(batchResponses);
    }
}

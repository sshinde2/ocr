/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtfluent.client.FluentClient;
import au.com.target.tgtfluent.data.Category;
import au.com.target.tgtfluent.data.CategoryResponse;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.exception.CategoryFluentFeedException;
import au.com.target.tgtfluent.model.CategoryFluentUpdateStatusModel;
import au.com.target.tgtfluent.model.FluentUpdateFailureCauseModel;
import au.com.target.tgtfluent.service.FluentUpdateStatusService;


/**
 * @author bpottass
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentCategoryUpsertServiceImplTest {

    @Mock
    protected FluentClient fluentClient;

    @Mock
    private ModelService modelService;

    @Mock
    private FluentUpdateStatusService fluentUpdateStatusService;

    @Mock
    private Converter<TargetProductCategoryModel, Category> converter;

    @InjectMocks
    @Spy
    private final FluentCategoryUpsertServiceImpl fluentCategoryUpsertServiceImpl = new FluentCategoryUpsertServiceImpl();

    @Mock
    private TargetProductCategoryModel categoryModel;

    @Mock
    private Category category;

    @Mock
    private CategoryFluentUpdateStatusModel categoryUpdateStatus;

    @Mock
    private FluentUpdateFailureCauseModel updateFailureCause;

    private final String fluentId = "Test fluentId";

    @Before
    public void setup() {
        given(converter.convert(categoryModel)).willReturn(category);
        given(fluentUpdateStatusService.getFluentUpdateStatus(CategoryFluentUpdateStatusModel.class,
                CategoryFluentUpdateStatusModel._TYPECODE, categoryModel.getCode())).willReturn(categoryUpdateStatus);
        final List<FluentUpdateFailureCauseModel> failureCauses = mock(List.class);
        given(categoryUpdateStatus.getFailureCauses()).willReturn(failureCauses);
        given(modelService.create(FluentUpdateFailureCauseModel.class))
                .willReturn(updateFailureCause);
    }

    @Test
    public void testUpsertCategoryWithNoFluentIdForSuperCategory() {
        given(converter.convert(categoryModel)).willThrow(
                new CategoryFluentFeedException("NO_FLUENT_ID_FOR_SUPER_CATEGORY",
                        "Super category doesn't have fluent id"));

        fluentCategoryUpsertServiceImpl.upsertCategory(categoryModel);
        verify(modelService).create(FluentUpdateFailureCauseModel.class);
        verify(updateFailureCause).setCode("NO_FLUENT_ID_FOR_SUPER_CATEGORY");
        verify(updateFailureCause).setMessage("Super category doesn't have fluent id");
        verify(categoryUpdateStatus).setLastRunSuccessful(false);
        verify(modelService).saveAll(Arrays.asList(updateFailureCause, categoryUpdateStatus));
        verifyZeroInteractions(fluentClient);
        verifyNoMoreInteractions(modelService);
    }


    @Test
    public void testUpsertCategoryCreateSuccess() {
        given(categoryModel.getFluentId()).willReturn(StringUtils.EMPTY);
        final FluentResponse fluentResponse = mock(FluentResponse.class);
        given(fluentResponse.getId()).willReturn(fluentId);
        given(fluentClient.createCategory(category)).willReturn(fluentResponse);

        fluentCategoryUpsertServiceImpl.upsertCategory(categoryModel);
        verify(fluentClient).createCategory(category);
        verifyNoMoreInteractions(fluentClient);
        verify(categoryUpdateStatus).setLastRunSuccessful(true);
        verify(categoryUpdateStatus).setFluentId(fluentId);
        verify(modelService).saveAll(Arrays.asList(categoryModel, categoryUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testUpsertCategoryCreateFail() {
        given(categoryModel.getFluentId()).willReturn(StringUtils.EMPTY);
        final FluentResponse fluentResponse = mock(FluentResponse.class);
        final List<Error> errors = new ArrayList<>();
        final Error error = mock(Error.class);
        given(error.getCode()).willReturn("500");
        given(error.getMessage()).willReturn("Status 500 Error");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.createCategory(category)).willReturn(fluentResponse);

        fluentCategoryUpsertServiceImpl.upsertCategory(categoryModel);
        verify(fluentClient).createCategory(category);
        verifyNoMoreInteractions(fluentClient);
        verify(categoryUpdateStatus).setLastRunSuccessful(false);
        verify(categoryUpdateStatus, never()).setFluentId(anyString());
        verify(modelService).create(FluentUpdateFailureCauseModel.class);
        verify(updateFailureCause).setCode("500");
        verify(updateFailureCause).setMessage("Status 500 Error");
        verify(updateFailureCause).setUpdateStatus(categoryUpdateStatus);
        verify(modelService).saveAll(Arrays.asList(updateFailureCause, categoryUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testUpsertCategoryUpdateOnConflict() {
        given(category.getCategoryRef()).willReturn("Cat1");
        given(categoryModel.getFluentId()).willReturn(StringUtils.EMPTY);
        FluentResponse fluentResponse = mock(FluentResponse.class);
        final List<Error> errors = new ArrayList<>();
        final Error error = mock(Error.class);
        given(error.getCode()).willReturn("409");
        given(error.getMessage())
                .willReturn("Category already exists");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.createCategory(category)).willReturn(fluentResponse);
        final CategoryResponse categoryResponse = mock(CategoryResponse.class);
        final List<Category> categoryies = new ArrayList<>();
        final Category categoryResult = mock(Category.class);
        given(categoryResult.getId()).willReturn(fluentId);
        categoryies.add(categoryResult);
        given(categoryResponse.getResults()).willReturn(categoryies);
        given(fluentClient.searchCategory("Cat1")).willReturn(categoryResponse);
        fluentResponse = mock(FluentResponse.class);
        given(fluentResponse.getId()).willReturn(fluentId);
        given(fluentClient.updateCategory(fluentId, category)).willReturn(fluentResponse);

        fluentCategoryUpsertServiceImpl.upsertCategory(categoryModel);
        verify(fluentClient).createCategory(category);
        verify(fluentClient).searchCategory("Cat1");
        verify(fluentClient).updateCategory(fluentId, category);
        verifyNoMoreInteractions(fluentClient);
        verify(categoryUpdateStatus).setLastRunSuccessful(true);
        verify(categoryUpdateStatus).setFluentId(fluentId);
        verify(modelService).saveAll(Arrays.asList(categoryModel, categoryUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testUpsertCategoryCreateAndUpdateFail() {
        given(category.getCategoryRef()).willReturn("Cat1");
        given(categoryModel.getFluentId()).willReturn(StringUtils.EMPTY);
        FluentResponse fluentResponse = mock(FluentResponse.class);
        List<Error> errors = new ArrayList<>();
        Error error = mock(Error.class);
        given(error.getCode()).willReturn("409");
        given(error.getMessage())
                .willReturn("Category already exists");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.createCategory(category)).willReturn(fluentResponse);
        final CategoryResponse categoryResponse = mock(CategoryResponse.class);
        final List<Category> categories = new ArrayList<>();
        final Category categoryResult = mock(Category.class);
        given(categoryResult.getId()).willReturn(fluentId);
        categories.add(categoryResult);
        given(categoryResponse.getResults()).willReturn(categories);
        given(fluentClient.searchCategory("Cat1")).willReturn(categoryResponse);
        fluentResponse = mock(FluentResponse.class);
        errors = new ArrayList<>();
        error = mock(Error.class);
        given(error.getCode()).willReturn("500");
        given(error.getMessage()).willReturn("Status 500 Error");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.updateCategory(fluentId, category)).willReturn(fluentResponse);

        fluentCategoryUpsertServiceImpl.upsertCategory(categoryModel);
        verify(fluentClient).createCategory(category);
        verify(fluentClient).searchCategory("Cat1");
        verify(fluentClient).updateCategory(fluentId, category);
        verifyNoMoreInteractions(fluentClient);
        verify(categoryUpdateStatus).setLastRunSuccessful(false);
        verify(categoryUpdateStatus, never()).setFluentId(anyString());
        verify(modelService).create(FluentUpdateFailureCauseModel.class);
        verify(updateFailureCause).setCode("500");
        verify(updateFailureCause).setMessage("Status 500 Error");
        verify(updateFailureCause).setUpdateStatus(categoryUpdateStatus);
        verify(modelService).saveAll(Arrays.asList(updateFailureCause, categoryUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testUpsertCategoryUpdateSuccess() {
        given(categoryModel.getFluentId()).willReturn(fluentId);
        final FluentResponse fluentResponse = mock(FluentResponse.class);
        given(fluentResponse.getId()).willReturn(fluentId);
        given(fluentClient.updateCategory(fluentId, category)).willReturn(fluentResponse);

        fluentCategoryUpsertServiceImpl.upsertCategory(categoryModel);
        verify(fluentClient).updateCategory(fluentId, category);
        verifyNoMoreInteractions(fluentClient);
        verify(categoryUpdateStatus).setLastRunSuccessful(true);
        verify(categoryUpdateStatus).setFluentId(fluentId);
        verify(modelService).saveAll(Arrays.asList(categoryModel, categoryUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testUpsertCategoryCreateOnNotFound() {
        given(categoryModel.getFluentId()).willReturn(fluentId);
        FluentResponse fluentResponse = mock(FluentResponse.class);
        final List<Error> errors = new ArrayList<>();
        final Error error = mock(Error.class);
        given(error.getCode()).willReturn("404");
        given(error.getMessage()).willReturn("Object of type [Category] was not found using id : [5]");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.updateCategory(fluentId, category)).willReturn(fluentResponse);
        fluentResponse = mock(FluentResponse.class);
        given(fluentResponse.getId()).willReturn(fluentId);
        given(fluentClient.createCategory(category)).willReturn(fluentResponse);

        fluentCategoryUpsertServiceImpl.upsertCategory(categoryModel);
        verify(fluentClient).updateCategory(fluentId, category);
        verify(fluentClient).createCategory(category);
        verifyNoMoreInteractions(fluentClient);
        verify(categoryUpdateStatus).setLastRunSuccessful(true);
        verify(categoryUpdateStatus).setFluentId(fluentId);
        verify(modelService).saveAll(Arrays.asList(categoryModel, categoryUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testUpsertCategoryUpdateAndCreateFail() {
        given(categoryModel.getFluentId()).willReturn(fluentId);
        FluentResponse fluentResponse = mock(FluentResponse.class);
        List<Error> errors = new ArrayList<>();
        Error error = mock(Error.class);
        given(error.getCode()).willReturn("404");
        given(error.getMessage()).willReturn("Object of type [Category] was not found using id : [5]");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.updateCategory(fluentId, category)).willReturn(fluentResponse);
        fluentResponse = mock(FluentResponse.class);
        errors = new ArrayList<>();
        error = mock(Error.class);
        given(error.getCode()).willReturn("500");
        given(error.getMessage()).willReturn("Status 500 Error");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.createCategory(category)).willReturn(fluentResponse);

        fluentCategoryUpsertServiceImpl.upsertCategory(categoryModel);
        verify(fluentClient).updateCategory(fluentId, category);
        verify(fluentClient).createCategory(category);
        verifyNoMoreInteractions(fluentClient);
        verify(categoryUpdateStatus).setLastRunSuccessful(false);
        verify(categoryUpdateStatus, never()).setFluentId(anyString());
        verify(modelService).create(FluentUpdateFailureCauseModel.class);
        verify(updateFailureCause).setCode("500");
        verify(updateFailureCause).setMessage("Status 500 Error");
        verify(updateFailureCause).setUpdateStatus(categoryUpdateStatus);
        verify(modelService).saveAll(Arrays.asList(updateFailureCause, categoryUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

}

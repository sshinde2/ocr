package au.com.target.tgtfluent.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.VendorModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.orderEntry.TargetOrderEntryService;
import au.com.target.tgtfluent.constants.FulfilmentStatus;
import au.com.target.tgtfluent.constants.FulfilmentStatus.Dc;
import au.com.target.tgtfluent.data.Address;
import au.com.target.tgtfluent.data.Fulfilment;
import au.com.target.tgtfluent.exception.FluentFulfilmentException;


/**
 * @author bpottass
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentFulfilmentValidatorImplTest {

    @Mock
    private TargetOrderEntryService targetOrderEntryService;

    @Mock
    private Fulfilment fulfilment;

    @Mock
    private OrderModel orderModel;

    @Mock
    private VendorModel vendor;

    @Mock
    private WarehouseModel warehouse;

    @Mock
    private TargetConsignmentModel consignment;

    @InjectMocks
    @Spy
    private final FluentFulfilmentValidatorImpl fluentFulfilmentValidatorImpl = new FluentFulfilmentValidatorImpl();

    @Test(expected = IllegalArgumentException.class)
    public void testValidateStateTransitionUnkownVendor() throws FluentFulfilmentException {
        willReturn(vendor).given(warehouse).getVendor();
        willReturn(warehouse).given(consignment).getWarehouse();
        willThrow(new IllegalArgumentException()).given(fluentFulfilmentValidatorImpl).getOldStatus(consignment);
        willReturn(Dc.CREATED).given(fluentFulfilmentValidatorImpl).getNewStatus(fulfilment, consignment);

        fluentFulfilmentValidatorImpl.validateStateTransition(fulfilment, consignment);
    }

    @Test
    public void testValidateStateTransitionNullOldStatusAndCreatedNewStatus() throws FluentFulfilmentException {
        willReturn(vendor).given(warehouse).getVendor();
        willReturn(warehouse).given(consignment).getWarehouse();
        willReturn(null).given(fluentFulfilmentValidatorImpl).getOldStatus(consignment);
        willReturn(Dc.CREATED).given(fluentFulfilmentValidatorImpl).getNewStatus(fulfilment, consignment);

        fluentFulfilmentValidatorImpl.validateStateTransition(fulfilment, consignment);
        assertThat(fluentFulfilmentValidatorImpl.validateStateTransition(fulfilment, consignment)).isTrue();
    }

    @Test(expected = FluentFulfilmentException.class)
    public void testValidateStateTransitionNullOldStatusAndNotCreatedNewStatus() throws FluentFulfilmentException {
        willReturn(vendor).given(warehouse).getVendor();
        willReturn(warehouse).given(consignment).getWarehouse();
        willReturn(null).given(fluentFulfilmentValidatorImpl).getOldStatus(consignment);
        willReturn(Dc.SENT_TO_WAREHOUSE).given(fluentFulfilmentValidatorImpl).getNewStatus(fulfilment, consignment);

        fluentFulfilmentValidatorImpl.validateStateTransition(fulfilment, consignment);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateStateTransitionNullNewStatus() throws FluentFulfilmentException {
        willReturn(vendor).given(warehouse).getVendor();
        willReturn(warehouse).given(consignment).getWarehouse();
        willReturn(Dc.CREATED).given(fluentFulfilmentValidatorImpl).getOldStatus(consignment);
        willReturn(null).given(fluentFulfilmentValidatorImpl).getNewStatus(fulfilment, consignment);

        fluentFulfilmentValidatorImpl.validateStateTransition(fulfilment, consignment);
    }

    @Test
    public void testValidateStateTransitionNewStatusOlderThanOldStatus() throws FluentFulfilmentException {
        willReturn(vendor).given(warehouse).getVendor();
        willReturn(warehouse).given(consignment).getWarehouse();
        willReturn(Dc.PICKED).given(fluentFulfilmentValidatorImpl).getOldStatus(consignment);
        willReturn(Dc.CREATED).given(fluentFulfilmentValidatorImpl).getNewStatus(fulfilment, consignment);

        assertThat(fluentFulfilmentValidatorImpl.validateStateTransition(fulfilment, consignment)).isFalse();
    }

    @Test
    public void testValidateStateTransition() throws FluentFulfilmentException {
        willReturn(vendor).given(warehouse).getVendor();
        willReturn(warehouse).given(consignment).getWarehouse();
        willReturn(Dc.PICKED).given(fluentFulfilmentValidatorImpl).getOldStatus(consignment);
        willReturn(Dc.SHIPPED).given(fluentFulfilmentValidatorImpl).getNewStatus(fulfilment, consignment);

        assertThat(fluentFulfilmentValidatorImpl.validateStateTransition(fulfilment, consignment)).isTrue();
    }

    @Test(expected = FluentFulfilmentException.class)
    public void testValidateStateTransitionSkippedTransition() throws FluentFulfilmentException {
        willReturn(vendor).given(warehouse).getVendor();
        willReturn(warehouse).given(consignment).getWarehouse();
        willReturn(Dc.CONFIRMED_BY_WAREHOUSE).given(fluentFulfilmentValidatorImpl).getOldStatus(consignment);
        willReturn(Dc.SHIPPED).given(fluentFulfilmentValidatorImpl).getNewStatus(fulfilment, consignment);

        fluentFulfilmentValidatorImpl.validateStateTransition(fulfilment, consignment);
    }

    @Test
    public void testGetOldStatusForStore() {
        willReturn("Target").given(vendor).getCode();
        willReturn(vendor).given(warehouse).getVendor();
        willReturn(warehouse).given(consignment).getWarehouse();
        willReturn(ConsignmentStatus.CREATED).given(consignment).getStatus();

        assertThat(fluentFulfilmentValidatorImpl.getOldStatus(consignment)).isEqualTo(FulfilmentStatus.Store.CREATED);
    }

    @Test
    public void testGetOldStatusForDc() {
        willReturn("Fastline").given(vendor).getCode();
        willReturn(vendor).given(warehouse).getVendor();
        willReturn(warehouse).given(consignment).getWarehouse();
        willReturn(ConsignmentStatus.CREATED).given(consignment).getStatus();

        assertThat(fluentFulfilmentValidatorImpl.getOldStatus(consignment)).isEqualTo(FulfilmentStatus.Dc.CREATED);
    }

    @Test
    public void testGetOldStatusForIncomm() {
        willReturn("Incomm").given(vendor).getCode();
        willReturn(vendor).given(warehouse).getVendor();
        willReturn(warehouse).given(consignment).getWarehouse();
        willReturn(ConsignmentStatus.CREATED).given(consignment).getStatus();

        assertThat(fluentFulfilmentValidatorImpl.getOldStatus(consignment)).isEqualTo(FulfilmentStatus.Incomm.CREATED);
    }

    @Test
    public void testGetOldStatusForIncommPhysical() {
        willReturn("Incomm").given(vendor).getCode();
        willReturn(vendor).given(warehouse).getVendor();
        willReturn(warehouse).given(consignment).getWarehouse();
        final AddressModel address = mock(AddressModel.class);
        willReturn(address).given(consignment).getShippingAddress();
        willReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE).given(consignment).getStatus();

        assertThat(fluentFulfilmentValidatorImpl.getOldStatus(consignment))
                .isEqualTo(FulfilmentStatus.IncommPhysical.CONFIRMED_BY_WAREHOUSE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetOldStatusForUnknown() {
        willReturn("Unknown").given(vendor).getCode();
        willReturn(vendor).given(warehouse).getVendor();
        willReturn(warehouse).given(consignment).getWarehouse();
        willReturn(ConsignmentStatus.CREATED).given(consignment).getStatus();

        fluentFulfilmentValidatorImpl.getOldStatus(consignment);
    }

    @Test
    public void testGetNewStatusForStore() {
        willReturn("Target").given(vendor).getCode();
        willReturn(vendor).given(warehouse).getVendor();
        willReturn(warehouse).given(consignment).getWarehouse();
        willReturn("CREATED").given(fulfilment).getStatus();

        assertThat(fluentFulfilmentValidatorImpl.getNewStatus(fulfilment, consignment))
                .isEqualTo(FulfilmentStatus.Store.CREATED);
    }

    @Test
    public void testGetNewStatusForDc() {
        willReturn("Fastline").given(vendor).getCode();
        willReturn(vendor).given(warehouse).getVendor();
        willReturn(warehouse).given(consignment).getWarehouse();
        willReturn("CREATED").given(fulfilment).getStatus();

        assertThat(fluentFulfilmentValidatorImpl.getNewStatus(fulfilment, consignment))
                .isEqualTo(FulfilmentStatus.Dc.CREATED);
    }

    @Test
    public void testGetNewStatusForIncomm() {
        willReturn("Incomm").given(vendor).getCode();
        willReturn(vendor).given(warehouse).getVendor();
        willReturn(warehouse).given(consignment).getWarehouse();
        willReturn("CREATED").given(fulfilment).getStatus();

        assertThat(fluentFulfilmentValidatorImpl.getNewStatus(fulfilment, consignment))
                .isEqualTo(FulfilmentStatus.Incomm.CREATED);
    }

    @Test
    public void testGetNewStatusForIncommPhysical() {
        willReturn("Incomm").given(vendor).getCode();
        willReturn(vendor).given(warehouse).getVendor();
        willReturn(warehouse).given(consignment).getWarehouse();
        final Address toAddress = mock(Address.class);
        willReturn(toAddress).given(fulfilment).getToAddress();
        willReturn("CONFIRMED_BY_WAREHOUSE").given(fulfilment).getStatus();

        assertThat(fluentFulfilmentValidatorImpl.getNewStatus(fulfilment, consignment))
                .isEqualTo(FulfilmentStatus.IncommPhysical.CONFIRMED_BY_WAREHOUSE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetnewStatusForUnknown() {
        willReturn("Unknown").given(vendor).getCode();
        willReturn(vendor).given(warehouse).getVendor();
        willReturn(warehouse).given(consignment).getWarehouse();
        willReturn("CREATED").given(fulfilment).getStatus();
        fluentFulfilmentValidatorImpl.getNewStatus(fulfilment, consignment);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateAlreadyCancelled() {
        given(consignment.getOrder()).willReturn(orderModel);
        willReturn(ConsignmentStatus.CANCELLED).given(consignment).getStatus();
        fluentFulfilmentValidatorImpl.validateFulfilment(fulfilment, consignment);
    }

}

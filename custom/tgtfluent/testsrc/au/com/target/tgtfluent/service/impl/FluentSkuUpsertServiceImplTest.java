/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.order.FluentErrorException;
import au.com.target.tgtfluent.client.FluentClient;
import au.com.target.tgtfluent.constants.TgtFluentConstants;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.data.Event;
import au.com.target.tgtfluent.data.EventResponse;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.data.PreOrderInventory;
import au.com.target.tgtfluent.data.PreOrderItemInventory;
import au.com.target.tgtfluent.data.Sku;
import au.com.target.tgtfluent.data.SkusResponse;
import au.com.target.tgtfluent.model.FluentUpdateFailureCauseModel;
import au.com.target.tgtfluent.model.SkuFluentUpdateStatusModel;
import au.com.target.tgtfluent.service.FluentUpdateStatusService;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentSkuUpsertServiceImplTest {

    @Mock
    protected FluentClient fluentClient;

    @Mock
    private ModelService modelService;

    @Mock
    private FluentUpdateStatusService fluentUpdateStatusService;

    @Mock
    private Converter<AbstractTargetVariantProductModel, Sku> converter;

    @InjectMocks
    @Spy
    private final FluentSkuUpsertServiceImpl fluentProductUpsertServiceImpl = new FluentSkuUpsertServiceImpl();

    @Mock
    private AbstractTargetVariantProductModel variantProduct;

    @Mock
    private Sku sku;

    @Mock
    private SkuFluentUpdateStatusModel skuUpdateStatus;

    @Mock
    private FluentUpdateFailureCauseModel updateFailureCause;

    @Mock
    private EventResponse eventResponse;

    @Mock
    private TargetColourVariantProductModel colourVariant;

    @Mock
    private TargetSizeVariantProductModel sizeVariant;

    private final String fluentId = "4567";

    @Before
    public void setup() {
        given(converter.convert(variantProduct)).willReturn(sku);
        given(converter.convert(colourVariant)).willReturn(sku);
        given(fluentUpdateStatusService.getFluentUpdateStatus(SkuFluentUpdateStatusModel.class,
                SkuFluentUpdateStatusModel._TYPECODE, variantProduct.getCode())).willReturn(skuUpdateStatus);
        final List<FluentUpdateFailureCauseModel> failureCauses = mock(List.class);
        given(skuUpdateStatus.getFailureCauses()).willReturn(failureCauses);
        given(modelService.create(FluentUpdateFailureCauseModel.class))
                .willReturn(updateFailureCause);
    }

    @Test
    public void testUpsertSkuNotValid() {
        fluentProductUpsertServiceImpl.upsertSku(variantProduct);
        verifyZeroInteractions(fluentClient);
        verifyZeroInteractions(modelService);
    }

    @Test
    public void testUpsertSkuCreateSuccess() {
        willReturn(Boolean.valueOf(false)).given(fluentProductUpsertServiceImpl).publishMaxPreOrderInventoryEvent(
                variantProduct);
        given(variantProduct.getFluentId()).willReturn(StringUtils.EMPTY);
        final FluentResponse fluentResponse = mock(FluentResponse.class);
        given(fluentResponse.getId()).willReturn(fluentId);
        given(fluentClient.createSku("1234", sku)).willReturn(fluentResponse);
        willReturn("1234").given(fluentProductUpsertServiceImpl).getBaseProductId(variantProduct);
        fluentProductUpsertServiceImpl.upsertSku(variantProduct);
        verify(fluentClient).createSku("1234", sku);
        verifyNoMoreInteractions(fluentClient);
        verify(skuUpdateStatus).setLastRunSuccessful(true);
        verify(skuUpdateStatus).setFluentId(fluentId);
        verify(modelService).saveAll(Arrays.asList(variantProduct, skuUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

    /**
     * When publishPreorderEvent Returns true
     */
    @Test
    public void testUpsertSkuCreateSuccessForPreOrder() {
        given(colourVariant.getFluentId()).willReturn(StringUtils.EMPTY);
        given(colourVariant.getMaxPreOrderQuantity()).willReturn(Integer.valueOf(100));
        given(colourVariant.getCode()).willReturn("P1000");
        given(fluentUpdateStatusService.getFluentUpdateStatus(SkuFluentUpdateStatusModel.class,
                SkuFluentUpdateStatusModel._TYPECODE, colourVariant.getCode())).willReturn(skuUpdateStatus);
        final ArgumentCaptor<Event> eventCaptor = ArgumentCaptor.forClass(Event.class);
        final FluentResponse fluentResponse = mock(FluentResponse.class);
        given(fluentResponse.getId()).willReturn(fluentId);
        given(fluentClient.createSku("1234", sku)).willReturn(fluentResponse);
        willReturn(new EventResponse()).given(fluentClient).eventSync(eventCaptor.capture());
        willReturn("1234").given(fluentProductUpsertServiceImpl).getBaseProductId(colourVariant);
        willReturn(Boolean.valueOf(true)).given(fluentProductUpsertServiceImpl).publishMaxPreOrderInventoryEvent(
                colourVariant);
        fluentProductUpsertServiceImpl.upsertSku(colourVariant);
        verify(fluentClient).createSku("1234", sku);
        verify(fluentClient).eventSync(eventCaptor.capture());
        verifyNoMoreInteractions(fluentClient);
        verify(skuUpdateStatus).setLastRunSuccessful(true);
        verify(skuUpdateStatus).setFluentId(fluentId);
        verify(modelService).saveAll(Arrays.asList(colourVariant, skuUpdateStatus));
        verifyNoMoreInteractions(modelService);
        final Event event = eventCaptor.getValue();
        assertThat(event.getName()).isEqualTo(TgtFluentConstants.Event.PREORDER_LOAD_MAX_QTY);
        assertThat(event.getEntityType()).isEqualTo(TgtFluentConstants.EntityType.INVENTORY_CATALOGUE);
        assertThat(event.getEntitySubtype()).isEqualTo(TgtFluentConstants.EntitySubtype.DEFAULT);
        assertThat(event.getRootEntityType()).isEqualTo(TgtFluentConstants.EntityType.INVENTORY_CATALOGUE);
        assertThat(event.getRootEntityRef()).isEqualTo(TgtFluentConstants.EntityRef.DEFAULT);
        assertThat(event.getEntityRef()).isEqualTo(TgtFluentConstants.EntityRef.DEFAULT);
        assertThat(event.getAttributes() instanceof PreOrderInventory).isTrue();
        final PreOrderInventory inventory = (PreOrderInventory)event.getAttributes();
        final List<PreOrderItemInventory> attributes = inventory.getItems();
        assertThat(attributes).hasSize(1);
        assertThat(attributes).onProperty("quantity").containsExactly(Integer.valueOf(100));
        assertThat(attributes).onProperty("skuRef").containsExactly("P1000");
    }


    @Test
    public void testUpsertSkuCreateFail() {
        given(variantProduct.getFluentId()).willReturn(StringUtils.EMPTY);
        final FluentResponse fluentResponse = mock(FluentResponse.class);
        final List<Error> errors = new ArrayList<>();
        final Error error = mock(Error.class);
        given(error.getCode()).willReturn("500");
        given(error.getMessage()).willReturn("Disaster");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.createSku("1234", sku)).willReturn(fluentResponse);
        willReturn("1234").given(fluentProductUpsertServiceImpl).getBaseProductId(variantProduct);

        fluentProductUpsertServiceImpl.upsertSku(variantProduct);
        verify(fluentClient).createSku("1234", sku);
        verifyNoMoreInteractions(fluentClient);
        verify(skuUpdateStatus).setLastRunSuccessful(false);
        verify(skuUpdateStatus, never()).setFluentId(anyString());
        verify(modelService).create(FluentUpdateFailureCauseModel.class);
        verify(updateFailureCause).setCode("500");
        verify(updateFailureCause).setMessage("Disaster");
        verify(updateFailureCause).setUpdateStatus(skuUpdateStatus);
        verify(modelService).saveAll(Arrays.asList(updateFailureCause, skuUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testUpsertSkuUpdateOnConflict() {
        given(sku.getSkuRef()).willReturn("V01");
        given(variantProduct.getFluentId()).willReturn(StringUtils.EMPTY);
        FluentResponse fluentResponse = mock(FluentResponse.class);
        final List<Error> errors = new ArrayList<>();
        final Error error = mock(Error.class);
        given(error.getCode()).willReturn("400");
        given(error.getMessage())
                .willReturn("Not allowed to upsert sku, the status of the existing sku must be 'UNKNOWN'");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.createSku("1234", sku)).willReturn(fluentResponse);
        final SkusResponse skusResponse = mock(SkusResponse.class);
        final List<Sku> skus = new ArrayList<>();
        final Sku skuResult = mock(Sku.class);
        given(skuResult.getSkuId()).willReturn(fluentId);
        skus.add(skuResult);
        given(skusResponse.getResults()).willReturn(skus);
        given(fluentClient.searchSkus("V01")).willReturn(skusResponse);
        fluentResponse = mock(FluentResponse.class);
        given(fluentResponse.getId()).willReturn(fluentId);
        given(fluentClient.updateSku("1234", fluentId, sku)).willReturn(fluentResponse);
        willReturn("1234").given(fluentProductUpsertServiceImpl).getBaseProductId(variantProduct);

        fluentProductUpsertServiceImpl.upsertSku(variantProduct);
        verify(fluentClient).createSku("1234", sku);
        verify(fluentClient).searchSkus("V01");
        verify(fluentClient).updateSku("1234", fluentId, sku);
        verifyNoMoreInteractions(fluentClient);
        verify(skuUpdateStatus).setLastRunSuccessful(true);
        verify(skuUpdateStatus).setFluentId(fluentId);
        verify(modelService).saveAll(Arrays.asList(variantProduct, skuUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testUpsertSkuCreateAndUpdateFail() {
        given(sku.getSkuRef()).willReturn("V01");
        given(variantProduct.getFluentId()).willReturn(StringUtils.EMPTY);
        FluentResponse fluentResponse = mock(FluentResponse.class);
        List<Error> errors = new ArrayList<>();
        Error error = mock(Error.class);
        given(error.getCode()).willReturn("400");
        given(error.getMessage())
                .willReturn("Not allowed to upsert sku, the status of the existing sku must be 'UNKNOWN'");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.createSku("1234", sku)).willReturn(fluentResponse);
        final SkusResponse skusResponse = mock(SkusResponse.class);
        final List<Sku> skus = new ArrayList<>();
        final Sku skuResult = mock(Sku.class);
        given(skuResult.getSkuId()).willReturn(fluentId);
        skus.add(skuResult);
        given(skusResponse.getResults()).willReturn(skus);
        given(fluentClient.searchSkus("V01")).willReturn(skusResponse);
        fluentResponse = mock(FluentResponse.class);
        errors = new ArrayList<>();
        error = mock(Error.class);
        given(error.getCode()).willReturn("500");
        given(error.getMessage()).willReturn("Disaster");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.updateSku("1234", fluentId, sku)).willReturn(fluentResponse);
        willReturn("1234").given(fluentProductUpsertServiceImpl).getBaseProductId(variantProduct);

        fluentProductUpsertServiceImpl.upsertSku(variantProduct);
        verify(fluentClient).createSku("1234", sku);
        verify(fluentClient).searchSkus("V01");
        verify(fluentClient).updateSku("1234", fluentId, sku);
        verifyNoMoreInteractions(fluentClient);
        verify(skuUpdateStatus).setLastRunSuccessful(false);
        verify(skuUpdateStatus, never()).setFluentId(anyString());
        verify(modelService).create(FluentUpdateFailureCauseModel.class);
        verify(updateFailureCause).setCode("500");
        verify(updateFailureCause).setMessage("Disaster");
        verify(updateFailureCause).setUpdateStatus(skuUpdateStatus);
        verify(modelService).saveAll(Arrays.asList(updateFailureCause, skuUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testUpsertSkuUpdateSuccess() {
        given(variantProduct.getFluentId()).willReturn(fluentId);
        final FluentResponse fluentResponse = mock(FluentResponse.class);
        given(fluentResponse.getId()).willReturn(fluentId);
        given(fluentClient.updateSku("1234", fluentId, sku)).willReturn(fluentResponse);
        willReturn("1234").given(fluentProductUpsertServiceImpl).getBaseProductId(variantProduct);

        fluentProductUpsertServiceImpl.upsertSku(variantProduct);
        verify(fluentClient).updateSku("1234", fluentId, sku);
        verifyNoMoreInteractions(fluentClient);
        verify(skuUpdateStatus).setLastRunSuccessful(true);
        verify(skuUpdateStatus).setFluentId(fluentId);
        verify(modelService).saveAll(Arrays.asList(variantProduct, skuUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testUpsertSkuCreateOnNotFound() {
        given(variantProduct.getFluentId()).willReturn(fluentId);
        FluentResponse fluentResponse = mock(FluentResponse.class);
        final List<Error> errors = new ArrayList<>();
        final Error error = mock(Error.class);
        given(error.getCode()).willReturn("404");
        given(error.getMessage()).willReturn("Object of type [Sku] was not found using id : [67]");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.updateSku("1234", fluentId, sku)).willReturn(fluentResponse);
        fluentResponse = mock(FluentResponse.class);
        given(fluentResponse.getId()).willReturn(fluentId);
        given(fluentClient.createSku("1234", sku)).willReturn(fluentResponse);
        willReturn("1234").given(fluentProductUpsertServiceImpl).getBaseProductId(variantProduct);

        fluentProductUpsertServiceImpl.upsertSku(variantProduct);
        verify(fluentClient).updateSku("1234", fluentId, sku);
        verify(fluentClient).createSku("1234", sku);
        verifyNoMoreInteractions(fluentClient);
        verify(skuUpdateStatus).setLastRunSuccessful(true);
        verify(skuUpdateStatus).setFluentId(fluentId);
        verify(modelService).saveAll(Arrays.asList(variantProduct, skuUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testUpsertSkuUpdateAndCreateFail() {
        given(variantProduct.getFluentId()).willReturn(fluentId);
        FluentResponse fluentResponse = mock(FluentResponse.class);
        List<Error> errors = new ArrayList<>();
        Error error = mock(Error.class);
        given(error.getCode()).willReturn("404");
        given(error.getMessage()).willReturn("Object of type [Sku] was not found using id : [67]");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.updateSku("1234", fluentId, sku)).willReturn(fluentResponse);
        fluentResponse = mock(FluentResponse.class);
        errors = new ArrayList<>();
        error = mock(Error.class);
        given(error.getCode()).willReturn("500");
        given(error.getMessage()).willReturn("Disaster");
        errors.add(error);
        given(fluentResponse.getErrors()).willReturn(errors);
        given(fluentClient.createSku("1234", sku)).willReturn(fluentResponse);
        willReturn("1234").given(fluentProductUpsertServiceImpl).getBaseProductId(variantProduct);

        fluentProductUpsertServiceImpl.upsertSku(variantProduct);
        verify(fluentClient).updateSku("1234", fluentId, sku);
        verify(fluentClient).createSku("1234", sku);
        verifyNoMoreInteractions(fluentClient);
        verify(skuUpdateStatus).setLastRunSuccessful(false);
        verify(skuUpdateStatus, never()).setFluentId(anyString());
        verify(modelService).create(FluentUpdateFailureCauseModel.class);
        verify(updateFailureCause).setCode("500");
        verify(updateFailureCause).setMessage("Disaster");
        verify(updateFailureCause).setUpdateStatus(skuUpdateStatus);
        verify(modelService).saveAll(Arrays.asList(updateFailureCause, skuUpdateStatus));
        verifyNoMoreInteractions(modelService);
    }

    @Test
    public void testGetMaxPreOrderQtyInventory() {
        given(variantProduct.getMaxPreOrderQuantity()).willReturn(Integer.valueOf(100));
        given(variantProduct.getCode()).willReturn("P1000");
        final PreOrderInventory inventory = fluentProductUpsertServiceImpl.getMaxPreQtyInventory(variantProduct);
        assertThat(inventory.getItems()).hasSize(1);
        assertThat(inventory.getItems().get(0).getSkuRef()).isEqualTo("P1000");
        assertThat(inventory.getItems().get(0).getQuantity()).isEqualTo(100);
    }

    @Test
    public void testGetMaxPreOrderQtyInventoryNullPreOrderQty() {
        given(variantProduct.getMaxPreOrderQuantity()).willReturn(null);
        given(variantProduct.getCode()).willReturn("P1000");
        final PreOrderInventory inventory = fluentProductUpsertServiceImpl.getMaxPreQtyInventory(variantProduct);
        final List<PreOrderItemInventory> attributes = inventory.getItems();
        assertThat(attributes).hasSize(1);
        assertThat(attributes).onProperty("skuRef").containsExactly("P1000");
        assertThat(attributes).onProperty("quantity").containsExactly(Integer.valueOf(0));
    }

    @Test
    public void testSendMaxPreQtyInventory() {
        given(variantProduct.getMaxPreOrderQuantity()).willReturn(Integer.valueOf(500));
        given(variantProduct.getCode()).willReturn("P1000");
        final ArgumentCaptor<Event> eventCaptor = ArgumentCaptor.forClass(Event.class);
        willReturn(new EventResponse()).given(fluentClient).eventSync(eventCaptor.capture());
        fluentProductUpsertServiceImpl.sendMaxPreOrderInventoryEvent(variantProduct);
        verify(fluentClient).eventSync(eventCaptor.capture());
        final Event event = eventCaptor.getValue();
        assertThat(event.getName()).isEqualTo(TgtFluentConstants.Event.PREORDER_LOAD_MAX_QTY);
        assertThat(event.getEntityType()).isEqualTo(TgtFluentConstants.EntityType.INVENTORY_CATALOGUE);
        assertThat(event.getEntitySubtype()).isEqualTo(TgtFluentConstants.EntitySubtype.DEFAULT);
        assertThat(event.getRootEntityType()).isEqualTo(TgtFluentConstants.EntityType.INVENTORY_CATALOGUE);
        assertThat(event.getRootEntityRef()).isEqualTo(TgtFluentConstants.EntityRef.DEFAULT);
        assertThat(event.getEntityRef()).isEqualTo(TgtFluentConstants.EntityRef.DEFAULT);
        assertThat(event.getAttributes() instanceof PreOrderInventory).isTrue();
        final PreOrderInventory inventory = (PreOrderInventory)event.getAttributes();
        final List<PreOrderItemInventory> attributes = inventory.getItems();
        assertThat(attributes).hasSize(1);
        assertThat(attributes).onProperty("skuRef").containsExactly("P1000");
        assertThat(attributes).onProperty("quantity").containsExactly(Integer.valueOf(500));

    }

    @Test(expected = FluentErrorException.class)
    public void testSendMaxPreQtyInventoryFail() {
        willReturn("send pre order event failed").given(eventResponse).getErrorMessage();
        given(variantProduct.getMaxPreOrderQuantity()).willReturn(Integer.valueOf(500));
        given(variantProduct.getCode()).willReturn("P1000");
        final ArgumentCaptor<Event> eventCaptor = ArgumentCaptor.forClass(Event.class);
        willReturn(eventResponse).given(fluentClient).eventSync(eventCaptor.capture());
        fluentProductUpsertServiceImpl.sendMaxPreOrderInventoryEvent(variantProduct);
        verify(fluentClient).eventSync(eventCaptor.capture());
    }

    @Test
    public void testPublishMaxPreOrderInventoryEventNotEmbargoed() {
        given(colourVariant.getNormalSaleStartDateTime()).willReturn(getDate(true, false));
        given(colourVariant.getPreOrderEndDateTime()).willReturn(getDate(true, false));
        given(colourVariant.getMaxPreOrderQuantity()).willReturn(Integer.valueOf(500));
        final boolean publicEvent = fluentProductUpsertServiceImpl.publishMaxPreOrderInventoryEvent(variantProduct);
        assertThat(publicEvent).isFalse();
    }

    @Test
    public void testPublishMaxPreOrderInventoryEventNullQty() {
        given(colourVariant.getNormalSaleStartDateTime()).willReturn(getDate(false, true));
        given(colourVariant.getPreOrderEndDateTime()).willReturn(getDate(false, true));
        given(colourVariant.getMaxPreOrderQuantity()).willReturn(null);
        final boolean publicEvent = fluentProductUpsertServiceImpl.publishMaxPreOrderInventoryEvent(colourVariant);
        assertThat(publicEvent).isFalse();
    }

    @Test
    public void testPublishMaxPreOrderInventoryEventWithNoEndDate() {
        final Calendar embargoDate = Calendar.getInstance();
        embargoDate.set(embargoDate.get(Calendar.YEAR), embargoDate.get(Calendar.MONTH) + 4, 25, 0, 0, 0);
        given(colourVariant.getNormalSaleStartDateTime()).willReturn(getDate(false, true));
        given(colourVariant.getPreOrderEndDateTime()).willReturn(null);
        given(colourVariant.getMaxPreOrderQuantity()).willReturn(Integer.valueOf(500));
        final boolean publicEvent = fluentProductUpsertServiceImpl.publishMaxPreOrderInventoryEvent(colourVariant);
        assertThat(publicEvent).isFalse();
    }

    /**
     * Product embargoed,not null enddate and qty available
     */
    @Test
    public void testPublishMaxPreOrderInventoryEvent() {
        given(colourVariant.getNormalSaleStartDateTime()).willReturn(getDate(false, true));
        given(colourVariant.getPreOrderEndDateTime()).willReturn(getDate(false, true));
        given(colourVariant.getMaxPreOrderQuantity()).willReturn(Integer.valueOf(500));
        final boolean publicEvent = fluentProductUpsertServiceImpl.publishMaxPreOrderInventoryEvent(colourVariant);
        assertThat(publicEvent).isTrue();
    }

    /**
     * Product embargoed,not null enddate and qty available(size variant)
     */
    @Test
    public void testPublishMaxPreOrderInventoryEventWithSize() {
        given(colourVariant.getNormalSaleStartDateTime()).willReturn(getDate(false, true));
        given(colourVariant.getPreOrderEndDateTime()).willReturn(getDate(false, true));
        given(sizeVariant.getMaxPreOrderQuantity()).willReturn(Integer.valueOf(500));
        given(sizeVariant.getBaseProduct()).willReturn(colourVariant);
        final boolean publicEvent = fluentProductUpsertServiceImpl.publishMaxPreOrderInventoryEvent(sizeVariant);
        assertThat(publicEvent).isTrue();
    }

    /**
     * Product embargoed,not null enddate and qty null(size variant)
     */
    @Test
    public void testPublishMaxPreOrderInventoryEventWithSizeNullQty() {
        given(colourVariant.getNormalSaleStartDateTime()).willReturn(getDate(false, true));
        given(colourVariant.getPreOrderEndDateTime()).willReturn(getDate(false, true));
        given(sizeVariant.getMaxPreOrderQuantity()).willReturn(null);
        given(sizeVariant.getBaseProduct()).willReturn(colourVariant);
        final boolean publicEvent = fluentProductUpsertServiceImpl.publishMaxPreOrderInventoryEvent(sizeVariant);
        assertThat(publicEvent).isFalse();
    }

    /**
     * Product embargoed, null enddate and qty available(size variant)
     */
    @Test
    public void testPublishMaxPreOrderInventoryEventWithSizeNoEndDate() {
        given(colourVariant.getNormalSaleStartDateTime()).willReturn(getDate(false, true));
        given(colourVariant.getPreOrderEndDateTime()).willReturn(null);
        given(sizeVariant.getMaxPreOrderQuantity()).willReturn(null);
        given(sizeVariant.getBaseProduct()).willReturn(colourVariant);
        final boolean publicEvent = fluentProductUpsertServiceImpl.publishMaxPreOrderInventoryEvent(sizeVariant);
        assertThat(publicEvent).isFalse();
    }

    private Date getDate(final boolean past, final boolean future) {
        final Calendar calendar = Calendar.getInstance();
        if (past) {
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) - 4, 25, 0, 0, 0);
        }
        if (future) {
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 4, 25, 0, 0, 0);
        }
        return calendar.getTime();

    }
}

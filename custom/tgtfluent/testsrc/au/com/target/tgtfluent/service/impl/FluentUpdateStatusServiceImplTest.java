/**
 * 
 */
package au.com.target.tgtfluent.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtfluent.dao.FluentUpdateStatusDao;
import au.com.target.tgtfluent.model.ProductFluentUpdateStatusModel;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentUpdateStatusServiceImplTest {

    @InjectMocks
    private final FluentUpdateStatusServiceImpl service = new FluentUpdateStatusServiceImpl();

    @Mock
    private FluentUpdateStatusDao fluentUpdateStatusDao;

    @Mock
    private ModelService modelService;

    @Test
    public void testGetFluentUpdateStatus()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final Class<ProductFluentUpdateStatusModel> searchClass = ProductFluentUpdateStatusModel.class;
        final String code = "code";
        final ProductFluentUpdateStatusModel expected = mock(ProductFluentUpdateStatusModel.class);
        given(fluentUpdateStatusDao.find(ProductFluentUpdateStatusModel._TYPECODE, code)).willReturn(expected);

        final ProductFluentUpdateStatusModel result = service.getFluentUpdateStatus(searchClass,
                ProductFluentUpdateStatusModel._TYPECODE, code);
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void testGetFluentUpdateStatusWithCreateNew() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final Class<ProductFluentUpdateStatusModel> searchClass = ProductFluentUpdateStatusModel.class;
        final String code = "code";
        given(fluentUpdateStatusDao.find(ProductFluentUpdateStatusModel._TYPECODE, code))
                .willThrow(new TargetUnknownIdentifierException("error"));
        final ProductFluentUpdateStatusModel expected = mock(ProductFluentUpdateStatusModel.class);
        given(modelService.create(searchClass)).willReturn(expected);

        final ProductFluentUpdateStatusModel result = service.getFluentUpdateStatus(searchClass,
                ProductFluentUpdateStatusModel._TYPECODE, code);
        assertThat(result).isEqualTo(expected);
    }
}

/**
 * 
 */
package au.com.target.tgtfluent.util;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import au.com.target.tgtfluent.constants.TgtFluentConstants.FluentCarrierName;
import au.com.target.tgtfluent.constants.TgtFluentConstants.TargetCarrierName;


/**
 * @author gsing236
 *
 */
public class FluentCarrierMapTest {

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testGetFluentCarrierNameAP() throws Exception {
        final String targetCarrierName = TargetCarrierName.AUSTRALIA_POST_INSTORE_CNC;

        final String fluentCarrierName = FluentCarrierMap.getFluentCarrierName(targetCarrierName);
        assertThat(fluentCarrierName).isEqualTo(FluentCarrierName.AUSTRALIA_POST);
    }

    @Test
    public void testGetFluentCarrierNameToll() throws Exception {
        final String targetCarrierName = TargetCarrierName.TOLL_CNC;

        final String fluentCarrierName = FluentCarrierMap.getFluentCarrierName(targetCarrierName);
        assertThat(fluentCarrierName).isEqualTo(FluentCarrierName.TOLL_CC);
    }

    @Test
    public void testGetFluentCarrierNameException() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("no value found for the targetCarrierName= WrongCarrier");

        FluentCarrierMap.getFluentCarrierName("WrongCarrier");
    }
}

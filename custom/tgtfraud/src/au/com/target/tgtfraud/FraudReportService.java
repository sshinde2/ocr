/**
 * 
 */
package au.com.target.tgtfraud;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.fraud.model.FraudReportModel;


/**
 * Service for managing fraud reports.
 * 
 */
public interface FraudReportService {

    /**
     * Create, save and return a fraud report on the given order, for the given response.
     * 
     * @param response
     * @param order
     * @return FraudReportModel
     */
    FraudReportModel createFraudReport(FraudServiceResponse response, OrderModel order);

}

/**
 * 
 */
package au.com.target.tgtfraud;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.fraud.FraudService;
import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import java.util.List;


/**
 * @author vmuthura
 * 
 */
public interface TargetFraudService extends FraudService
{

    /**
     * Submit given payment details
     * 
     * @param orderModel
     * @return status
     */
    FraudServiceResponse submitPayment(final String providerName, OrderModel orderModel);

    /**
     * Submit refund details on shortpick or cancel.
     * 
     * @param orderModificationRecordEntryModel
     * @return status
     */
    FraudServiceResponse submitRefund(final String providerName,
            OrderModificationRecordEntryModel orderModificationRecordEntryModel,
            List<PaymentTransactionEntryModel> refundEntries);

    /**
     * Detects if an {@code order} in its current state is eligible for fraud review, i.e. if agent can accept or reject
     * it.
     *
     * @param order
     *            the order to check state for
     * @return {@code true} if order is eligible for review, {@code false otherwise}
     */
    boolean isOrderReviewable(final OrderModel order);


}

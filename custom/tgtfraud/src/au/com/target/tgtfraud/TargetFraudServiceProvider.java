/**
 * 
 */
package au.com.target.tgtfraud;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.fraud.FraudServiceProvider;
import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import java.util.List;


/**
 * @author vmuthura
 * 
 */
public interface TargetFraudServiceProvider extends FraudServiceProvider
{
    /**
     * Submit given payment details
     * 
     * @param orderModel
     * @return status
     */
    FraudServiceResponse submitPayment(OrderModel orderModel);

    /**
     * Submit refund details on shortpick or cancel.
     * 
     * @param orderModificationRecordEntryModel
     * @return status
     */
    FraudServiceResponse submitRefund(OrderModificationRecordEntryModel orderModificationRecordEntryModel,
            final List<PaymentTransactionEntryModel> refundEntries);

}

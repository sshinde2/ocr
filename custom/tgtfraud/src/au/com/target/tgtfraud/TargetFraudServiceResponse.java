package au.com.target.tgtfraud;

import de.hybris.platform.fraud.impl.FraudServiceResponse;

import au.com.target.tgtfraud.provider.data.RecommendationTypeEnum;


/**
 * @author vmuthura
 * 
 */
public class TargetFraudServiceResponse extends FraudServiceResponse
{
    private RecommendationTypeEnum recommendation;

    /**
     * @param providerName
     */
    public TargetFraudServiceResponse(final String providerName)
    {
        super(providerName);
    }

    /**
     * @param description
     * @param providerName
     */
    public TargetFraudServiceResponse(final String description, final String providerName)
    {
        super(description, providerName);
    }

    /**
     * @return the recommendation
     */
    public RecommendationTypeEnum getRecommendation()
    {
        return recommendation;
    }

    /**
     * @param recommendation
     *            the recommendation to set
     */
    public void setRecommendation(final RecommendationTypeEnum recommendation)
    {
        this.recommendation = recommendation;
    }

}

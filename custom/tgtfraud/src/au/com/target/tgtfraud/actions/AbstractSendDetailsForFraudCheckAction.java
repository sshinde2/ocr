/**
 * 
 */
package au.com.target.tgtfraud.actions;

import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;
import au.com.target.tgtfraud.TargetFraudService;
import au.com.target.tgtfraud.TargetFraudServiceResponse;
import au.com.target.tgtfraud.exception.FraudCheckServiceUnavailableException;
import au.com.target.tgtfraud.provider.data.RecommendationTypeEnum;


/**
 * Base action for sending details to fraud check service.<br/>
 * For these actions we ignore the response from the service and always continue.
 * 
 */
public abstract class AbstractSendDetailsForFraudCheckAction extends RetryActionWithSpringConfig<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(AbstractSendDetailsForFraudCheckAction.class);

    protected TargetFraudService targetFraudService;

    protected String providerName;

    public enum Transition
    {
        OK;

        public static Set<String> getStringValues()
        {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values())
            {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }


    /**
     * Send details based on process data, to be overridden in subclasses.
     * 
     * @param process
     * @return FraudServiceResponse
     * @throws FraudCheckServiceUnavailableException
     */
    protected abstract FraudServiceResponse sendDetails(OrderProcessModel process)
            throws FraudCheckServiceUnavailableException;


    @Override
    protected String executeInternal(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process, "process may not be null");

        // If there is a problem, then just log a warning but continue -
        // it's not important to the process that this succeeds.
        // We don't create a fraud check report for this action.

        String msg = "Process:" + process.getCode();
        if (process.getOrder() != null) {
            msg += " Order:" + process.getOrder();
        }

        try {
            final FraudServiceResponse response = sendDetails(process);


            if (response == null) {
                LOG.warn(msg + " null response returned");
            }
            else if (!(response instanceof TargetFraudServiceResponse)) {
                LOG.warn(msg + "  response returned is not TargetFraudServiceResponse");
            }
            else {
                final TargetFraudServiceResponse targetResponse = (TargetFraudServiceResponse)response;
                final RecommendationTypeEnum rec = targetResponse.getRecommendation();
                if (!rec.equals(RecommendationTypeEnum.ACCEPT)) {
                    LOG.warn(msg + " recommendation returned=" + rec);
                }
            }
        }
        catch (final FraudCheckServiceUnavailableException fcsue) {
            LOG.warn(msg + " fraud check service unavailable");
        }

        return Transition.OK.toString();
    }


    /**
     * @param targetFraudService
     *            the targetFraudService to set
     */
    @Required
    public void setTargetFraudService(final TargetFraudService targetFraudService) {
        this.targetFraudService = targetFraudService;
    }

    /**
     * @param providerName
     *            the providerName to set
     */
    @Required
    public void setProviderName(final String providerName) {
        this.providerName = providerName;
    }

    /**
     * @return the providerName
     */
    public String getProviderName() {
        return providerName;
    }


}

/**
 * 
 */
package au.com.target.tgtfraud.actions;

import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.fraud.model.FraudReportModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.paymentstandard.model.StandardPaymentModeModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;
import au.com.target.tgtfraud.FraudReportService;
import au.com.target.tgtfraud.TargetFraudService;
import au.com.target.tgtfraud.exception.FraudCheckServiceUnavailableException;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


public class FraudCheckAction extends RetryActionWithSpringConfig<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(FraudCheckAction.class);

    private TargetFraudService targetFraudService;

    private FraudReportService fraudReportService;

    private TargetVoucherService targetVoucherService;

    private TargetSalesApplicationConfigService salesApplicationConfigService;

    private String providerName;

    public enum Transition {
        ACCEPT, REJECT, REVIEW;

        public static Set<String> getStringValues() {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values()) {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }

    @Override
    protected String executeInternal(final OrderProcessModel process) throws RetryLaterException, Exception {

        final OrderModel order = process.getOrder();
        Assert.notNull(order, "Order cannot be null");

        final String orderCode = order.getCode();
        final SalesApplication salesApp = order.getSalesApplication();

        if (salesApplicationConfigService.isSkipFraudCheck(salesApp)) {
            LOG.info("Skipping fraud check as skipFraudCheck is marked as true for the salesApplication="
                    + salesApp.toString() + ", order=" + orderCode);
            return Transition.ACCEPT.toString();
        }

        // Run the fraud check
        FraudServiceResponse response = null;
        try {
            response = targetFraudService.recognizeOrderSymptoms(providerName, order);
        }
        catch (final FraudCheckServiceUnavailableException fcsue) {

            // Service unavailable - RETRY
            throw new RetryLaterException("Fraud check service unavailable for order: " + orderCode, fcsue);
        }

        Assert.notNull(response, "response from targetFraudService cannot be null");

        // Create a fraud report from the response
        final FraudReportModel fraudReport = fraudReportService.createFraudReport(response, order);
        FraudStatus status = fraudReport.getStatus();

        final StandardPaymentModeModel standardPaymentModeModel = (StandardPaymentModeModel)order
                .getPaymentMode();
        if (null != standardPaymentModeModel
                && BooleanUtils.isTrue(standardPaymentModeModel.getSkipFraudRespValidation())) {
            return Transition.ACCEPT.toString();
        }
        if (status.equals(FraudStatus.CHECK) && null != targetVoucherService.getFlybuysDiscountForOrder(order)) {
            LOG.warn(SplunkLogFormatter.formatMessage(
                    "In FraudCheckAction setting order status to REJECT since order contains a flybuys discount",
                    TgtutilityConstants.ErrorCode.WARN_PAYMENT));
            status = FraudStatus.FRAUD;
        }
        // Update the order status
        updateOrder(status, order);

        return fraudStatusToTransition(status);
    }

    /**
     * Map the fraud status to the transition to return
     * 
     * @param status
     * @return transition name
     */
    protected String fraudStatusToTransition(final FraudStatus status) {

        Transition result = Transition.ACCEPT;

        if (status != null) {
            if (status.equals(FraudStatus.FRAUD)) {
                result = Transition.REJECT;
            }
            else if (status.equals(FraudStatus.CHECK)) {
                result = Transition.REVIEW;
            }
        }
        return result.toString();
    }

    /**
     * Update the order status and fraud flags
     * 
     * @param status
     * @param order
     */
    protected void updateOrder(final FraudStatus status, final OrderModel order) {

        order.setFraudulent(Boolean.FALSE);
        order.setPotentiallyFraudulent(Boolean.FALSE);

        if (status == FraudStatus.FRAUD) {
            order.setFraudulent(Boolean.TRUE);
        }
        else if (status == FraudStatus.CHECK) {
            order.setPotentiallyFraudulent(Boolean.TRUE);
        }

        modelService.save(order);
    }

    @Override
    public Set getTransitions() {
        return Transition.getStringValues();
    }

    /**
     * @param targetFraudService
     *            the targetFraudService to set
     */
    @Required
    public void setTargetFraudService(final TargetFraudService targetFraudService) {
        this.targetFraudService = targetFraudService;
    }

    /**
     * @param fraudReportService
     *            the fraudReportService to set
     */
    @Required
    public void setFraudReportService(final FraudReportService fraudReportService) {
        this.fraudReportService = fraudReportService;
    }

    /**
     * @param providerName
     *            the providerName to set
     */
    @Required
    public void setProviderName(final String providerName) {
        this.providerName = providerName;
    }

    /**
     * @param targetVoucherService
     *            the targetVoucherService to set
     */
    @Required
    public void setTargetVoucherService(final TargetVoucherService targetVoucherService) {
        this.targetVoucherService = targetVoucherService;
    }

    /**
     * @param salesApplicationConfigService
     *            the salesApplicationConfigService to set
     */
    @Required
    public void setSalesApplicationConfigService(
            final TargetSalesApplicationConfigService salesApplicationConfigService) {
        this.salesApplicationConfigService = salesApplicationConfigService;
    }

}

package au.com.target.tgtfraud.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtfraud.ordercancel.RejectCancelService;


/**
 * 
 * Action to kick off the reject procedure
 * 
 */
public class RejectOrderAction extends AbstractProceduralAction<OrderProcessModel> {

    private RejectCancelService rejectCancelService;

    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {

        Assert.notNull(process, "Orderprocess cannot be null");
        final OrderModel orderModel = process.getOrder();
        Assert.notNull(orderModel, "Ordermodel cannot be null");

        rejectCancelService.startRejectCancelProcess(orderModel);
    }

    /**
     * @param rejectCancelService
     *            the rejectCancelService to set
     */
    @Required
    public void setRejectCancelService(final RejectCancelService rejectCancelService) {
        this.rejectCancelService = rejectCancelService;
    }


}

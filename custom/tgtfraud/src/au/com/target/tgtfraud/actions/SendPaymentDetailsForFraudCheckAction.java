/**
 * 
 */
package au.com.target.tgtfraud.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import org.springframework.util.Assert;

import au.com.target.tgtfraud.exception.FraudCheckServiceUnavailableException;


/**
 * Sending payment details to fraud service (eg accertify)
 * 
 */
public class SendPaymentDetailsForFraudCheckAction extends AbstractSendDetailsForFraudCheckAction {

    /* 
     * Override the sendDetails to send the payment details for the order.
     * 
     * @see au.com.target.tgtfraud.actions.AbstractSendDetailsForFraudCheckAction#sendDetails(de.hybris.platform.orderprocessing.model.OrderProcessModel)
     */
    @Override
    protected FraudServiceResponse sendDetails(final OrderProcessModel process)
            throws FraudCheckServiceUnavailableException {

        final OrderModel order = process.getOrder();
        Assert.notNull(order, "order may not be null");

        try {
            return targetFraudService.submitPayment(getProviderName(), order);
        }
        catch (final FraudCheckServiceUnavailableException e) {
            throw e;
        }
        //CHECKSTYLE:OFF really don't want fraud check to terminate business process
        catch (final Throwable t) {
            throw new FraudCheckServiceUnavailableException("Unexpected error processing fraud verification", t);
        }
        //CHECKSTYLE:ON
    }

}

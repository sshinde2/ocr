/**
 * 
 */
package au.com.target.tgtfraud.dao;

import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.Date;
import java.util.List;


public interface FraudDao {

    /**
     * get all orders that have been in a status for longer then specified hours
     * 
     * @return list
     */
    List<OrderModel> getOrdersForAutoAccept(OrderStatus orderStatus, Date timestamp, FraudStatus fraudStatus);

}

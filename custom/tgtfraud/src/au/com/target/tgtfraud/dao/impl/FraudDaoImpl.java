/**
 * 
 */
package au.com.target.tgtfraud.dao.impl;

import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtfraud.dao.FraudDao;


public class FraudDaoImpl extends AbstractItemDao implements FraudDao {

    // get orders in status greater than a specified hours old
    private static final String QUERY_GET_ORDERS_FOR_STATUS = "SELECT  {"
            + OrderModel.PK
            + "} FROM {"
            + OrderModel._TYPECODE
            + " AS O "
            + " JOIN FRAUDREPORT AS FR ON {FR.ORDER} = {O.PK} } WHERE {O.STATUS} = ?status AND {FR.STATUS} = ?fraudStatus AND {FR.TIMESTAMP} < ?timestamp";


    @Override
    public List<OrderModel> getOrdersForAutoAccept(final OrderStatus status, final Date timestamp,
            final FraudStatus fraudStatus) {
        final Map<String, Object> queryParamMap = new HashMap<String, Object>();
        queryParamMap.put("status", status);
        queryParamMap.put("timestamp", timestamp);
        queryParamMap.put("fraudStatus", fraudStatus);
        final SearchResult<OrderModel> searchResult = search(QUERY_GET_ORDERS_FOR_STATUS,
                queryParamMap);
        final List<OrderModel> result = searchResult.getResult();
        return result == null ? Collections.EMPTY_LIST : result;
    }
}

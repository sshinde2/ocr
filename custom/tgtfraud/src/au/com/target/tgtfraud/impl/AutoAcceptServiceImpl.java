package au.com.target.tgtfraud.impl;

import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtfraud.AutoAcceptService;
import au.com.target.tgtfraud.FraudReportService;
import au.com.target.tgtfraud.TargetFraudServiceResponse;
import au.com.target.tgtfraud.dao.FraudDao;
import au.com.target.tgtfraud.provider.data.RecommendationTypeEnum;


public class AutoAcceptServiceImpl extends AbstractBusinessService implements AutoAcceptService {

    private TargetBusinessProcessService targetBusinessProcessService;
    private FraudReportService fraudReportService;
    private FraudDao fraudDao;

    private int autoAcceptHourValue;
    private String providerName;
    private String approvalExplanation;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Override
    public void triggerAcceptProcessForAutoAcceptReviewOrders() {

        //get the list of orders that are in review status and have been in that status for more than the configured number of hours
        final List<OrderModel> autoAcceptOrdersList = fraudDao.getOrdersForAutoAccept(OrderStatus.REVIEW,
                DateTime.now().minusHours(autoAcceptHourValue).toDate(), FraudStatus.CHECK);
        if (CollectionUtils.isEmpty(autoAcceptOrdersList)) {
            return;
        }
        for (final OrderModel orderModel : autoAcceptOrdersList) {
            startAcceptOrderProcess(orderModel);
            fraudReportService.createFraudReport(createFraudServiceResponse(), orderModel);
        }
    }

    /**
     * @param orderModel
     */
    private void startAcceptOrderProcess(final OrderModel orderModel) {
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)
                && StringUtils.isNotEmpty(orderModel.getFluentId())) {
            startAcceptOrderProcessWithFluent(orderModel);
        }
        else {
            if (BooleanUtils.isTrue(orderModel.getContainsPreOrderItems())) {
                targetBusinessProcessService.startPreOrderAcceptProcess(orderModel);
            }
            else {
                targetBusinessProcessService.startOrderAcceptProcess(orderModel);
            }
        }
    }

    /**
     * This method decides which business process to call based on three scenarios an order can go to review. 1. After
     * capturing initial deposit for a preorder - containsPreOrderItems: true and normalSalesStartDateTime is present 2.
     * After final payment is captured for a preorder - containsPreOrderItems: false and normalSalesStartDateTime is
     * present 3. After payment is captured for a normal order - containsPreOrderItems: false and
     * normalSalesStartDateTime is null
     * 
     * @param orderModel
     */
    private void startAcceptOrderProcessWithFluent(final OrderModel orderModel) {
        if (BooleanUtils.isTrue(orderModel.getContainsPreOrderItems())) {
            targetBusinessProcessService.startFluentAcceptPreOrderProcess(orderModel);
        }
        else if (orderModel.getNormalSaleStartDateTime() != null) {
            targetBusinessProcessService.startFluentPreOrderReleaseAcceptProcess(orderModel);
        }
        else {
            targetBusinessProcessService.startFluentOrderAcceptProcess(orderModel);
        }
    }

    private TargetFraudServiceResponse createFraudServiceResponse() {
        final TargetFraudServiceResponse fraudServiceResponse = new TargetFraudServiceResponse(
                approvalExplanation, providerName);
        fraudServiceResponse.setRecommendation(RecommendationTypeEnum.ACCEPT);
        return fraudServiceResponse;
    }

    /**
     * @param autoAcceptHourValue
     *            the autoAcceptHourValue to set
     */
    @Required
    public void setAutoAcceptHourValue(final int autoAcceptHourValue) {
        this.autoAcceptHourValue = autoAcceptHourValue;
    }

    /**
     * @param fraudDao
     *            the fraudDao to set
     */
    @Required
    public void setFraudDao(final FraudDao fraudDao) {
        this.fraudDao = fraudDao;
    }

    /**
     * @param targetBusinessProcessService
     *            the targetBusinessProcessService to set
     */
    @Required
    public void setTargetBusinessProcessService(final TargetBusinessProcessService targetBusinessProcessService) {
        this.targetBusinessProcessService = targetBusinessProcessService;
    }

    /**
     * @param providerName
     *            the providerName to set
     */
    @Required
    public void setProviderName(final String providerName) {
        this.providerName = providerName;
    }

    /**
     * @param approvalExplanation
     *            the approvalExplanation to set
     */
    @Required
    public void setApprovalExplanation(final String approvalExplanation) {
        this.approvalExplanation = approvalExplanation;
    }

    /**
     * @param fraudReportService
     *            the fraudReportService to set
     */
    @Required
    public void setFraudReportService(final FraudReportService fraudReportService) {
        this.fraudReportService = fraudReportService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }



}

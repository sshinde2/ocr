/**
 * 
 */
package au.com.target.tgtfraud.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.fraud.impl.DefaultFraudService;
import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfraud.TargetFraudService;
import au.com.target.tgtfraud.TargetFraudServiceProvider;
import au.com.target.tgtfraud.strategy.ReviewableAfterFraudCheck;


/**
 * @author vmuthura
 * 
 */
public class DefaultTargetFraudService extends DefaultFraudService implements TargetFraudService
{

    private List<ReviewableAfterFraudCheck> reviewableAfterFraudChecks;

    @Override
    public TargetFraudServiceProvider getProvider(final String name)
    {
        for (final Iterator iterator = getProviders().iterator(); iterator.hasNext();)
        {
            final TargetFraudServiceProvider p = (TargetFraudServiceProvider)iterator.next();
            if (name.equalsIgnoreCase(p.getProviderName())) {
                return p;
            }
        }

        throw new IllegalArgumentException((new StringBuilder("got no configured provider ")).append(name)
                .append(" within ").append(getProviders()).toString());
    }

    @Override
    public FraudServiceResponse submitPayment(final String providerName, final OrderModel orderModel)
    {
        return getProvider(providerName).submitPayment(orderModel);
    }

    @Override
    public FraudServiceResponse submitRefund(final String providerName,
            final OrderModificationRecordEntryModel orderModificationRecordEntryModel,
            final List<PaymentTransactionEntryModel> refundEntries)
    {
        return getProvider(providerName).submitRefund(orderModificationRecordEntryModel, refundEntries);
    }

    @Override
    public boolean isOrderReviewable(final OrderModel order) {
        for (final ReviewableAfterFraudCheck check : reviewableAfterFraudChecks) {
            if (!check.perform(order)) {
                return false;
            }
        }
        return true;
    }


    /**
     * Returns the list of checks which determine if an order is eligible for review after fraud.
     *
     * @return the list of checks
     */
    public List<ReviewableAfterFraudCheck> getReviewableAfterFraudChecks() {
        return reviewableAfterFraudChecks;
    }

    /**
     * Sets the list of checks which determine if an order is eligible for review after fraud.
     *
     * @param reviewableAfterFraudChecks
     *            the list of checks to set
     */
    @Required
    public void setReviewableAfterFraudChecks(final List<ReviewableAfterFraudCheck> reviewableAfterFraudChecks) {
        this.reviewableAfterFraudChecks = reviewableAfterFraudChecks;
    }
}

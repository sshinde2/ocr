package au.com.target.tgtfraud.impl;

import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.fraud.model.FraudReportModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfraud.FraudReportService;
import au.com.target.tgtfraud.TargetFraudServiceResponse;
import au.com.target.tgtfraud.provider.data.RecommendationTypeEnum;
import au.com.target.tgtfraud.strategy.TargetFraudReportCreationStrategy;


public class FraudReportServiceImpl extends AbstractBusinessService implements FraudReportService {

    private static final Logger LOG = Logger.getLogger(FraudReportServiceImpl.class);
    private TargetFraudReportCreationStrategy targetFraudReportCreationStrategy;

    /* 
     * The code in this method is largely lifted from the default Hybris AbstractFraudCheckAction, <br/>
     * but here we read the recommendation out of the customised response.
     * 
     * @see au.com.target.tgtfraud.FraudReportService#createFraudReport(java.lang.String, de.hybris.platform.fraud.impl.FraudServiceResponse, de.hybris.platform.core.model.order.OrderModel, de.hybris.platform.basecommerce.enums.FraudStatus)
     */
    @Override
    public FraudReportModel createFraudReport(final FraudServiceResponse response, final OrderModel order)
    {
        // Default to ok
        RecommendationTypeEnum recommendation = RecommendationTypeEnum.ACCEPT;

        if (response instanceof TargetFraudServiceResponse) {
            recommendation = ((TargetFraudServiceResponse)response).getRecommendation();
        }
        else {
            LOG.warn("response should be instanceof TargetFraudServiceResponse, order=" + order.getCode());
        }
        final FraudReportModel fraudReport = targetFraudReportCreationStrategy.createFraudReport(response, order,
                getStatusForRecommendation(recommendation));
        getModelService().save(fraudReport);
        getModelService().refresh(order);
        return fraudReport;
    }

    /**
     * Map a RecommendationTypeEnum to a FraudStatus
     * 
     * @param recommendation
     * @return FraudStatus
     */
    protected FraudStatus getStatusForRecommendation(final RecommendationTypeEnum recommendation) {

        if (recommendation == RecommendationTypeEnum.ACCEPT) {
            return FraudStatus.OK;
        }
        if (recommendation == RecommendationTypeEnum.REJECT) {
            return FraudStatus.FRAUD;
        }
        if (recommendation == RecommendationTypeEnum.REVIEW) {
            return FraudStatus.CHECK;
        }

        LOG.warn("Unrecognised value for RecommendationTypeEnum, setting FraudStatus to OK: " + recommendation);

        return FraudStatus.OK;
    }

    /**
     * @param targetFraudReportCreationStrategy
     *            the targetFraudReportCreationStrategy to set
     */
    @Required
    public void setTargetFraudReportCreationStrategy(
            final TargetFraudReportCreationStrategy targetFraudReportCreationStrategy) {
        this.targetFraudReportCreationStrategy = targetFraudReportCreationStrategy;
    }

}

/**
 * 
 */
package au.com.target.tgtfraud.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.apache.log4j.Logger;

import au.com.target.tgtfraud.AutoAcceptService;


public class TargetAutoAcceptReviewOrdersJob extends AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(TargetAutoAcceptReviewOrdersJob.class);

    private AutoAcceptService autoAcceptService;

    @Override
    public PerformResult perform(final CronJobModel cronJob) {

        try {
            // if there are any orders in review status for more then 72 hours then trigger accept business process for those orders
            autoAcceptService.triggerAcceptProcessForAutoAcceptReviewOrders();
        }

        catch (final Exception e)
        {
            LOG.error("Failed to perform TargetAutoReviewOrderProcessCronJob job", e);
            return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
        }

        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    /**
     * @param autoAcceptService
     *            the autoAcceptService to set
     */
    public void setAutoAcceptService(final AutoAcceptService autoAcceptService) {
        this.autoAcceptService = autoAcceptService;
    }


}

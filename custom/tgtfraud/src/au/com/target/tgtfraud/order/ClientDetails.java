package au.com.target.tgtfraud.order;

/**
 * Holds client/browser details requried for fraud detection
 */
public class ClientDetails {

    private String ipAddress;

    private String browserCookies;

    /**
     * @return the ipAddress
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * @param ipAddress
     *            the ipAddress to set
     */
    public void setIpAddress(final String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * @return the browserCookies
     */
    public String getBrowserCookies() {
        return browserCookies;
    }

    /**
     * @param browserCookies
     *            the browserCookies to set
     */
    public void setBrowserCookies(final String browserCookies) {
        this.browserCookies = browserCookies;
    }

}

package au.com.target.tgtfraud.order;

//CHECKSTYLE:OFF
import de.hybris.platform.core.model.order.CartModel;

import au.com.target.tgtfraud.model.FraudOrderDetailsModel;
import au.com.target.tgtlayby.order.TargetLaybyCommerceCheckoutService;


//CHECKSTYLE:ON

/**
 * Extended interface to support populating details requried for fraud detection on the {@link CartModel}
 */
public interface TargetFraudCommerceCheckoutService extends TargetLaybyCommerceCheckoutService {


    /**
     * Populate {@link FraudOrderDetailsModel} on the {@link CartModel} using {@link ClientDetails}
     * 
     * @param cartModel
     * @param clientDetails
     */
    void populateClientDetailsForFraudDetection(CartModel cartModel, ClientDetails clientDetails);

}

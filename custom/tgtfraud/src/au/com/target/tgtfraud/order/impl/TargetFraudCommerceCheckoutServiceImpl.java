package au.com.target.tgtfraud.order.impl;

import de.hybris.platform.core.model.order.CartModel;

import org.springframework.util.Assert;

import au.com.target.tgtfraud.model.FraudOrderDetailsModel;
import au.com.target.tgtfraud.order.ClientDetails;
import au.com.target.tgtfraud.order.TargetFraudCommerceCheckoutService;
import au.com.target.tgtlayby.order.impl.TargetLaybyCommerceCheckoutServiceImpl;


/**
 * Default implementation of {@link TargetFraudCommerceCheckoutService}
 */
public class TargetFraudCommerceCheckoutServiceImpl extends TargetLaybyCommerceCheckoutServiceImpl implements
        TargetFraudCommerceCheckoutService {


    @Override
    public void populateClientDetailsForFraudDetection(final CartModel cartModel, final ClientDetails clientDetails) {

        Assert.notNull(cartModel, "cartModel cannot be null");
        Assert.notNull(clientDetails, "clientDetails cannot be null");

        final FraudOrderDetailsModel fraudOrderDetails = getModelService().create(FraudOrderDetailsModel.class);
        fraudOrderDetails.setCustomerIP(clientDetails.getIpAddress());
        fraudOrderDetails.setBrowserCookies(clientDetails.getBrowserCookies());

        cartModel.setFraudOrderDetails(fraudOrderDetails);

        getModelService().saveAll(fraudOrderDetails, cartModel);
    }

}

/**
 * 
 */
package au.com.target.tgtfraud.ordercancel;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.exceptions.OrderCancelRecordsHandlerException;


/**
 * Service for creating a cancel request for a reject and running it
 * 
 */
public interface RejectCancelService {

    /**
     * Start the cancel procedure for this fraud rejected order
     * 
     * @param order
     * @throws OrderCancelRecordsHandlerException
     * @throws OrderCancelException
     */
    void startRejectCancelProcess(OrderModel order) throws OrderCancelRecordsHandlerException,
            OrderCancelException;
}

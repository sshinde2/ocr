/**
 * 
 */
package au.com.target.tgtfraud.ordercancel.impl;

import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.ordercancel.TargetCancelService;
import au.com.target.tgtcore.ordercancel.TargetOrderCancelRequest;
import au.com.target.tgtcore.ordercancel.discountstrategy.VoucherCorrectionStrategy;
import au.com.target.tgtcore.ordercancel.util.OrderCancelHelper;
import au.com.target.tgtfraud.ordercancel.RejectCancelService;


public class RejectCancelServiceImpl implements RejectCancelService {

    protected static final String CANCEL_REASON = "Order Rejected on Fraud Check";

    private OrderCancelHelper orderCancelHelper;

    private TargetCancelService targetCancelService;

    private ModelService modelService;

    private VoucherCorrectionStrategy voucherCorrectionStrategy;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Override
    public void startRejectCancelProcess(final OrderModel orderModel)
            throws OrderCancelException {

        // Create a cancel record and entry for cancelling the full order
        final TargetOrderCancelRequest orderCancelRequest = orderCancelHelper.createOrderFullCancelRecord(orderModel,
                CANCEL_REASON, CancelReason.FRAUDCHECKREJECTED);

        final OrderCancelRecordEntryModel cancelRequestRecordEntry = orderCancelHelper
                .createOrderFullCancelRecordEntry(orderCancelRequest);

        if (!(targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)
                && StringUtils.isNotEmpty(orderModel.getFluentId()))) {
            // This is up front since it uses the existing consignment statuses before they get modified below.
            targetCancelService.releaseStock(orderCancelRequest);
        }

        // modify order and consignment entries
        targetCancelService.modifyOrderAccordingToRequest(orderCancelRequest);

        modelService.refresh(orderModel);

        // Clear layby fee so it doesn't show up in order details
        orderModel.setLayByFee(Double.valueOf(0));
        targetCancelService.recalculateOrder(orderModel);

        voucherCorrectionStrategy.correctAppliedVouchers(orderModel);

        targetCancelService.updateRecordEntry(orderCancelRequest);

        getTargetBusinessProcessService().startOrderRejectProcess(orderModel, cancelRequestRecordEntry);
    }

    /**
     * Lookup for TargetBusinessProcessService
     * 
     * @return TargetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

    /**
     * @param orderCancelHelper
     *            the orderCancelHelper to set
     */
    @Required
    public void setOrderCancelHelper(final OrderCancelHelper orderCancelHelper) {
        this.orderCancelHelper = orderCancelHelper;
    }

    /**
     * @param targetCancelService
     *            the targetCancelService to set
     */
    @Required
    public void setTargetCancelService(final TargetCancelService targetCancelService) {
        this.targetCancelService = targetCancelService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param voucherCorrectionStrategy
     *            the voucherCorrectionStrategy to set
     */
    @Required
    public void setVoucherCorrectionStrategy(final VoucherCorrectionStrategy voucherCorrectionStrategy) {
        this.voucherCorrectionStrategy = voucherCorrectionStrategy;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }
}

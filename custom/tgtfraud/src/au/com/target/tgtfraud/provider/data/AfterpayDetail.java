/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * @author gsing236
 *
 */
@XmlRootElement
@XmlType(propOrder = {
        "failedAfterpayRequestId",
        "failedAfterpayAmount",
        "failedAfterpayStatus",
})
public class AfterpayDetail {

    private String failedAfterpayRequestId;
    private String failedAfterpayAmount;
    private String failedAfterpayStatus;

    /**
     * @return the failedAfterpayRequestId
     */
    public String getFailedAfterpayRequestId() {
        return failedAfterpayRequestId;
    }

    /**
     * @param failedAfterpayRequestId
     *            the failedAfterpayRequestId to set
     */
    public void setFailedAfterpayRequestId(final String failedAfterpayRequestId) {
        this.failedAfterpayRequestId = failedAfterpayRequestId;
    }

    /**
     * @return the failedAfterpayAmount
     */
    public String getFailedAfterpayAmount() {
        return failedAfterpayAmount;
    }

    /**
     * @param failedAfterpayAmount
     *            the failedAfterpayAmount to set
     */
    public void setFailedAfterpayAmount(final String failedAfterpayAmount) {
        this.failedAfterpayAmount = failedAfterpayAmount;
    }

    /**
     * @return the failedAfterpayStatus
     */
    public String getFailedAfterpayStatus() {
        return failedAfterpayStatus;
    }

    /**
     * @param failedAfterpayStatus
     *            the failedAfterpayStatus to set
     */
    public void setFailedAfterpayStatus(final String failedAfterpayStatus) {
        this.failedAfterpayStatus = failedAfterpayStatus;
    }
}

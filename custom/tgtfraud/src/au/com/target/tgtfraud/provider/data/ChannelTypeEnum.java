/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * @author vmuthura
 * 
 */
@XmlEnum(String.class)
public enum ChannelTypeEnum
{
    @XmlEnumValue("Automation")
    AUTOMATION("Automation"),

    @XmlEnumValue("Phone")
    PHONE("Phone"),

    @XmlEnumValue("Web")
    WEB("Web"),

    @XmlEnumValue("Kiosk")
    KIOSK("Kiosk"),

    @XmlEnumValue("B2B")
    B2B("B2B");
    
    private String code;

    /**
     * @param code
     *            - The channel code
     */
    private ChannelTypeEnum(final String code)
    {
        this.code = code;
    }

    /**
     * @return - The channel code
     */
    public String getCode()
    {
        return this.code;
    }

}

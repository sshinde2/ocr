/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement
@XmlType(propOrder = {
        "failedcardNumber",
        "failedcardType",
        "failedcardExpireDate",
        "failedbillingFullName",
        "failedbillingFirstName",
        "failedbillingLastName",
        "failedbillingAddressLine1",
        "failedbillingAddressLine2",
        "failedbillingCity",
        "failedbillingRegion",
        "failedbillingPostalCode",
        "failedbillingCountry",
        "failedbillingPhone"
})
public class CreditCardDetail
{
    private String failedcardNumber;
    private String failedcardType;
    private String failedcardExpireDate;
    private String failedbillingFullName;
    private String failedbillingFirstName;
    private String failedbillingLastName;
    private String failedbillingAddressLine1;
    private String failedbillingAddressLine2;
    private String failedbillingCity;
    private String failedbillingRegion;
    private String failedbillingPostalCode;
    private String failedbillingCountry;
    private String failedbillingPhone;

    /**
     * @return the failedcardNumber
     */
    public String getFailedcardNumber()
    {
        return failedcardNumber;
    }

    /**
     * @param failedcardNumber
     *            the failedcardNumber to set
     */
    public void setFailedcardNumber(final String failedcardNumber)
    {
        this.failedcardNumber = failedcardNumber;
    }

    /**
     * @return the failedcardType
     */
    public String getFailedcardType()
    {
        return failedcardType;
    }

    /**
     * @param failedcardType
     *            the failedcardType to set
     */
    public void setFailedcardType(final String failedcardType)
    {
        this.failedcardType = failedcardType;
    }

    /**
     * @return the failedcardExpireDate
     */
    public String getFailedcardExpireDate()
    {
        return failedcardExpireDate;
    }

    /**
     * @param failedcardExpireDate
     *            the failedcardExpireDate to set
     */
    public void setFailedcardExpireDate(final String failedcardExpireDate)
    {
        this.failedcardExpireDate = failedcardExpireDate;
    }

    /**
     * @return the failedbillingFullName
     */
    public String getFailedbillingFullName()
    {
        return failedbillingFullName;
    }

    /**
     * @param failedbillingFullName
     *            the failedbillingFullName to set
     */
    public void setFailedbillingFullName(final String failedbillingFullName)
    {
        this.failedbillingFullName = failedbillingFullName;
    }

    /**
     * @return the failedbillingFirstName
     */
    public String getFailedbillingFirstName()
    {
        return failedbillingFirstName;
    }

    /**
     * @param failedbillingFirstName
     *            the failedbillingFirstName to set
     */
    public void setFailedbillingFirstName(final String failedbillingFirstName)
    {
        this.failedbillingFirstName = failedbillingFirstName;
    }

    /**
     * @return the failedbillingLastName
     */
    public String getFailedbillingLastName()
    {
        return failedbillingLastName;
    }

    /**
     * @param failedbillingLastName
     *            the failedbillingLastName to set
     */
    public void setFailedbillingLastName(final String failedbillingLastName)
    {
        this.failedbillingLastName = failedbillingLastName;
    }

    /**
     * @return the failedbillingAddressLine1
     */
    public String getFailedbillingAddressLine1()
    {
        return failedbillingAddressLine1;
    }

    /**
     * @param failedbillingAddressLine1
     *            the failedbillingAddressLine1 to set
     */
    public void setFailedbillingAddressLine1(final String failedbillingAddressLine1)
    {
        this.failedbillingAddressLine1 = failedbillingAddressLine1;
    }

    /**
     * @return the failedbillingAddressLine2
     */
    public String getFailedbillingAddressLine2()
    {
        return failedbillingAddressLine2;
    }

    /**
     * @param failedbillingAddressLine2
     *            the failedbillingAddressLine2 to set
     */
    public void setFailedbillingAddressLine2(final String failedbillingAddressLine2)
    {
        this.failedbillingAddressLine2 = failedbillingAddressLine2;
    }

    /**
     * @return the failedbillingCity
     */
    public String getFailedbillingCity()
    {
        return failedbillingCity;
    }

    /**
     * @param failedbillingCity
     *            the failedbillingCity to set
     */
    public void setFailedbillingCity(final String failedbillingCity)
    {
        this.failedbillingCity = failedbillingCity;
    }

    /**
     * @return the failedbillingRegion
     */
    public String getFailedbillingRegion()
    {
        return failedbillingRegion;
    }

    /**
     * @param failedbillingRegion
     *            the failedbillingRegion to set
     */
    public void setFailedbillingRegion(final String failedbillingRegion)
    {
        this.failedbillingRegion = failedbillingRegion;
    }

    /**
     * @return the failedbillingPostalCode
     */
    public String getFailedbillingPostalCode()
    {
        return failedbillingPostalCode;
    }

    /**
     * @param failedbillingPostalCode
     *            the failedbillingPostalCode to set
     */
    public void setFailedbillingPostalCode(final String failedbillingPostalCode)
    {
        this.failedbillingPostalCode = failedbillingPostalCode;
    }

    /**
     * @return the failedbillingCountry
     */
    public String getFailedbillingCountry()
    {
        return failedbillingCountry;
    }

    /**
     * @param failedbillingCountry
     *            the failedbillingCountry to set
     */
    public void setFailedbillingCountry(final String failedbillingCountry)
    {
        this.failedbillingCountry = failedbillingCountry;
    }

    /**
     * @return the failedbillingPhone
     */
    public String getFailedbillingPhone()
    {
        return failedbillingPhone;
    }

    /**
     * @param failedbillingPhone
     *            the failedbillingPhone to set
     */
    public void setFailedbillingPhone(final String failedbillingPhone)
    {
        this.failedbillingPhone = failedbillingPhone;
    }

}

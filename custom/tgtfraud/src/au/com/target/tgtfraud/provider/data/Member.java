/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import au.com.target.tgtfraud.provider.util.MemberShipDateAdapter;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement
@XmlType(propOrder = {
        "memberID",
        "membershipDate",
        "memberFullName",
        "memberFirstName",
        "memberLastName",
        "memberEmail",
        "memberPhone",
        "targetStaffDiscountCard",
        "loyaltyNumber",
        "internetMemberRegisteredStatus",
})
public class Member
{
    private String memberID;
    private Date membershipDate;
    private String memberFullName;
    private String memberFirstName;
    private String memberLastName;
    private String memberEmail;
    private String memberPhone;
    private String targetStaffDiscountCard;
    private String loyaltyNumber;
    private String internetMemberRegisteredStatus;

    /**
     * @return the memberID
     */
    public String getMemberID()
    {
        return memberID;
    }

    /**
     * @param memberID
     *            the memberID to set
     */
    public void setMemberID(final String memberID)
    {
        this.memberID = memberID;
    }

    /**
     * @return the membershipDate
     */
    @XmlJavaTypeAdapter(MemberShipDateAdapter.class)
    public Date getMembershipDate()
    {
        return membershipDate;
    }

    /**
     * @param membershipDate
     *            the membershipDate to set
     */
    public void setMembershipDate(final Date membershipDate)
    {
        this.membershipDate = membershipDate;
    }

    /**
     * @return the memberFullName
     */
    public String getMemberFullName()
    {
        return memberFullName;
    }

    /**
     * @param memberFullName
     *            the memberFullName to set
     */
    public void setMemberFullName(final String memberFullName)
    {
        this.memberFullName = memberFullName;
    }

    /**
     * @return the memberFirstName
     */
    public String getMemberFirstName()
    {
        return memberFirstName;
    }

    /**
     * @param memberFirstName
     *            the memberFirstName to set
     */
    public void setMemberFirstName(final String memberFirstName)
    {
        this.memberFirstName = memberFirstName;
    }

    /**
     * @return the memberLastName
     */
    public String getMemberLastName()
    {
        return memberLastName;
    }

    /**
     * @param memberLastName
     *            the memberLastName to set
     */
    public void setMemberLastName(final String memberLastName)
    {
        this.memberLastName = memberLastName;
    }

    /**
     * @return the memberEmail
     */
    public String getMemberEmail()
    {
        return memberEmail;
    }

    /**
     * @param memberEmail
     *            the memberEmail to set
     */
    public void setMemberEmail(final String memberEmail)
    {
        this.memberEmail = memberEmail;
    }

    /**
     * @return the memberPhone
     */
    public String getMemberPhone()
    {
        return memberPhone;
    }

    /**
     * @param memberPhone
     *            the memberPhone to set
     */
    public void setMemberPhone(final String memberPhone)
    {
        this.memberPhone = memberPhone;
    }

    /**
     * @return the targetStaffDiscountCard
     */
    public String getTargetStaffDiscountCard()
    {
        return targetStaffDiscountCard;
    }

    /**
     * @param targetStaffDiscountCard
     *            the targetStaffDiscountCard to set
     */
    public void setTargetStaffDiscountCard(final String targetStaffDiscountCard)
    {
        this.targetStaffDiscountCard = targetStaffDiscountCard;
    }

    /**
     * @return the loyaltyNumber
     */
    public String getLoyaltyNumber()
    {
        return loyaltyNumber;
    }

    /**
     * @param loyaltyNumber
     *            the loyaltyNumber to set
     */
    public void setLoyaltyNumber(final String loyaltyNumber)
    {
        this.loyaltyNumber = loyaltyNumber;
    }

    /**
     * @return the internetMemberRegisteredStatus
     */
    public String getInternetMemberRegisteredStatus()
    {
        return internetMemberRegisteredStatus;
    }

    /**
     * @param internetMemberRegisteredStatus
     *            the internetMemberRegisteredStatus to set
     */
    public void setInternetMemberRegisteredStatus(final String internetMemberRegisteredStatus)
    {
        this.internetMemberRegisteredStatus = internetMemberRegisteredStatus;
    }

}

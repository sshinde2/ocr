/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement
@XmlType(propOrder = {
        "itemNumber",
        "itemDescription",
        "unitPrice",
        "quantity",
        "brandname",
        "category",
        "tax",
        "departmentNumber",
        "recipientemailAddress"
})
public class OrderItem
{
    private String itemNumber;
    private String itemDescription;
    private BigDecimal unitPrice;
    private long quantity;
    private String brandname;
    private String category;
    private BigDecimal tax;
    private int departmentNumber;
    private List<String> recipientemailAddress;

    /**
     * @return the itemNumber
     */
    public String getItemNumber()
    {
        return itemNumber;
    }

    /**
     * @param itemNumber
     *            the itemNumber to set
     */
    public void setItemNumber(final String itemNumber)
    {
        this.itemNumber = itemNumber;
    }

    /**
     * @return the itemDescription
     */
    public String getItemDescription()
    {
        return itemDescription;
    }

    /**
     * @param itemDescription
     *            the itemDescription to set
     */
    public void setItemDescription(final String itemDescription)
    {
        this.itemDescription = itemDescription;
    }

    /**
     * @return the unitPrice
     */
    public BigDecimal getUnitPrice()
    {
        return unitPrice;
    }

    /**
     * @param unitPrice
     *            the unitPrice to set
     */
    public void setUnitPrice(final BigDecimal unitPrice)
    {
        this.unitPrice = unitPrice;
    }

    /**
     * @return the quantity
     */
    public long getQuantity()
    {
        return quantity;
    }

    /**
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(final long quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the brandname
     */
    public String getBrandname()
    {
        return brandname;
    }

    /**
     * @param brandname
     *            the brandname to set
     */
    public void setBrandname(final String brandname)
    {
        this.brandname = brandname;
    }

    /**
     * @return the category
     */
    public String getCategory()
    {
        return category;
    }

    /**
     * @param category
     *            the category to set
     */
    public void setCategory(final String category)
    {
        this.category = category;
    }

    /**
     * @return the tax
     */
    public BigDecimal getTax()
    {
        return tax;
    }

    /**
     * @param tax
     *            the tax to set
     */
    public void setTax(final BigDecimal tax)
    {
        this.tax = tax;
    }

    /**
     * @return the departmentNumber
     */
    public int getDepartmentNumber()
    {
        return departmentNumber;
    }

    /**
     * @param departmentNumber
     *            the departmentNumber to set
     */
    public void setDepartmentNumber(final int departmentNumber)
    {
        this.departmentNumber = departmentNumber;
    }

    /**
     * @return the recipientemailAddress
     */
    public List<String> getRecipientemailAddress() {
        return recipientemailAddress;
    }

    /**
     * @param recipientemailAddress
     *            the recipientemailAddress to set
     */
    public void setRecipientemailAddress(final List<String> recipientemailAddress) {
        this.recipientemailAddress = recipientemailAddress;
    }

}

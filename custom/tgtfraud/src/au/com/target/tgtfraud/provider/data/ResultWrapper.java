/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement
public class ResultWrapper
{

    private List<Result> results;
    private String error;

    /**
     * @return the results
     */
    @XmlElement(name = "transaction-results")
    public List<Result> getResults()
    {
        return results;
    }

    /**
     * @param results
     *            the results to set
     */
    public void setResults(final List<Result> results)
    {
        this.results = results;
    }

    /**
     * @return the error
     */
    @XmlElement(name = "error")
    public String getError()
    {
        return error;
    }

    /**
     * @param error
     *            the error to set
     */
    public void setError(final String error)
    {
        this.error = error;
    }



}

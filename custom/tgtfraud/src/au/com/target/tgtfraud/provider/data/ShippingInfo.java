/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement
@XmlType(propOrder = {
        "shippingFullName",
        "shippingFirstName",
        "shippingLastName",
        "shippingAddressLine1",
        "shippingAddressLine2",
        "shippingCity",
        "shippingRegion",
        "shippingPostalCode",
        "shippingCountry",
        "shippingPhone",
        "shippingMethod"
})
public class ShippingInfo
{
    private String shippingFullName;
    private String shippingFirstName;
    private String shippingLastName;
    private String shippingAddressLine1;
    private String shippingAddressLine2;
    private String shippingCity;
    private String shippingRegion;
    private String shippingPostalCode;
    private String shippingCountry;
    private String shippingPhone;
    private String shippingMethod;

    /**
     * @return the shippingFullName
     */
    @XmlElement(name = "shipppingFullName")
    public String getShippingFullName()
    {
        return shippingFullName;
    }

    /**
     * @param shippingFullName
     *            the shippingFullName to set
     */
    public void setShippingFullName(final String shippingFullName)
    {
        this.shippingFullName = shippingFullName;
    }

    /**
     * @return the shippingFirstName
     */
    public String getShippingFirstName()
    {
        return shippingFirstName;
    }

    /**
     * @param shippingFirstName
     *            the shippingFirstName to set
     */
    public void setShippingFirstName(final String shippingFirstName)
    {
        this.shippingFirstName = shippingFirstName;
    }

    /**
     * @return the shippingLastName
     */
    public String getShippingLastName()
    {
        return shippingLastName;
    }

    /**
     * @param shippingLastName
     *            the shippingLastName to set
     */
    public void setShippingLastName(final String shippingLastName)
    {
        this.shippingLastName = shippingLastName;
    }

    /**
     * @return the shippingAddressLine1
     */
    public String getShippingAddressLine1()
    {
        return shippingAddressLine1;
    }

    /**
     * @param shippingAddressLine1
     *            the shippingAddressLine1 to set
     */
    public void setShippingAddressLine1(final String shippingAddressLine1)
    {
        this.shippingAddressLine1 = shippingAddressLine1;
    }

    /**
     * @return the shippingAddressLine2
     */
    public String getShippingAddressLine2()
    {
        return shippingAddressLine2;
    }

    /**
     * @param shippingAddressLine2
     *            the shippingAddressLine2 to set
     */
    public void setShippingAddressLine2(final String shippingAddressLine2)
    {
        this.shippingAddressLine2 = shippingAddressLine2;
    }

    /**
     * @return the shippingCity
     */
    public String getShippingCity()
    {
        return shippingCity;
    }

    /**
     * @param shippingCity
     *            the shippingCity to set
     */
    public void setShippingCity(final String shippingCity)
    {
        this.shippingCity = shippingCity;
    }

    /**
     * @return the shippingRegion
     */
    public String getShippingRegion()
    {
        return shippingRegion;
    }

    /**
     * @param shippingRegion
     *            the shippingRegion to set
     */
    public void setShippingRegion(final String shippingRegion)
    {
        this.shippingRegion = shippingRegion;
    }

    /**
     * @return the shippingPostalCode
     */
    public String getShippingPostalCode()
    {
        return shippingPostalCode;
    }

    /**
     * @param shippingPostalCode
     *            the shippingPostalCode to set
     */
    public void setShippingPostalCode(final String shippingPostalCode)
    {
        this.shippingPostalCode = shippingPostalCode;
    }

    /**
     * @return the shippingCountry
     */
    public String getShippingCountry()
    {
        return shippingCountry;
    }

    /**
     * @param shippingCountry
     *            the shippingCountry to set
     */
    public void setShippingCountry(final String shippingCountry)
    {
        this.shippingCountry = shippingCountry;
    }

    /**
     * @return the shippingPhone
     */
    public String getShippingPhone()
    {
        return shippingPhone;
    }

    /**
     * @param shippingPhone
     *            the shippingPhone to set
     */
    public void setShippingPhone(final String shippingPhone)
    {
        this.shippingPhone = shippingPhone;
    }

    /**
     * @return the shippingMethod
     */
    public String getShippingMethod()
    {
        return shippingMethod;
    }

    /**
     * @param shippingMethod
     *            the shippingMethod to set
     */
    public void setShippingMethod(final String shippingMethod)
    {
        this.shippingMethod = shippingMethod;
    }

}

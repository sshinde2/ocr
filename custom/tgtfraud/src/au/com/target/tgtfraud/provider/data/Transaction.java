/**
 * 
 */
package au.com.target.tgtfraud.provider.data;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement(name = "transactions")
public class Transaction
{
    private List<Order> orders;

    /**
     * @return the order
     */
    @XmlElement(name = "order")
    public List<Order> getOrders()
    {
        return orders;
    }

    /**
     * @param orders
     *            the order to set
     */
    public void setOrders(final List<Order> orders)
    {
        this.orders = orders;
    }
}

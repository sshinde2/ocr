/**
 * 
 */
package au.com.target.tgtfraud.provider.util;

import java.io.IOException;

import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;


/**
 * @author vmuthura
 * 
 */
public class AccertifyErrorHandler implements ResponseErrorHandler
{

    @Override
    public boolean hasError(final ClientHttpResponse clienthttpresponse) throws IOException
    {
        return false;
    }

    @Override
    public void handleError(final ClientHttpResponse clienthttpresponse) throws IOException
    {
        //
    }

}

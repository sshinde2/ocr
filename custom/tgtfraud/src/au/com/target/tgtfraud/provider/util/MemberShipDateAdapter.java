/**
 * 
 */
package au.com.target.tgtfraud.provider.util;

import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


/**
 * @author vmuthura
 * 
 */
public class MemberShipDateAdapter extends XmlAdapter<String, Date>
{

    private final DateTimeFormatter dateTimeFormat = DateTimeFormat.forPattern("yyyy/MM/dd");

    @Override
    public String marshal(final Date v) throws Exception
    {
        return dateTimeFormat.print(new DateTime(v));
    }

    @Override
    public Date unmarshal(final String v) throws Exception
    {
        return dateTimeFormat.parseDateTime(v).toDate();
    }

}

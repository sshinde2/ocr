package au.com.target.tgtfraud.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;

import java.util.List;

import au.com.target.tgtfraud.constants.TgtfraudConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 * 
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = TgtfraudConstants.EXTENSIONNAME)
public class TgtFraudSetup extends AbstractSystemSetup {

    @Override
    public List<SystemSetupParameter> getInitializationOptions() {
        return null;
    }

    /**
     * This method will be called during the system initialization.
     * 
     * @param context
     *            the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.PROJECT, process = Process.ALL)
    public void createProjectData(final SystemSetupContext context)
    {
        importImpexFile(context, "/tgtfraud/import/common/job-triggers.impex");
        importImpexFile(context, "/tgtfraud/import/fraudreviewgroup-users.impex");
    }

}

package au.com.target.tgtfraud.strategy;

import de.hybris.platform.core.model.order.OrderModel;

/**
 * Interface that designates a check for review eligibility after a fraud.
 */
public interface ReviewableAfterFraudCheck {

    /**
     * Verifies whether given {@code order} in its current state is eligible
     * for review after a fraud was detected.
     *
     * @param order the order to check
     * @return {@code true} if order is eligible for review, {@code false otherwise}
     */
    boolean perform(final OrderModel order);
}

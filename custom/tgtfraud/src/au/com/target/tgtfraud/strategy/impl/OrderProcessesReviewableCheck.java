package au.com.target.tgtfraud.strategy.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.enums.ProcessState;

import java.util.EnumSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfraud.strategy.ReviewableAfterFraudCheck;


/**
 * Strategy that checks if an order has any pending business processes(except excluded ones) before allowing or
 * disallowing a review action related to fraud.
 */
public class OrderProcessesReviewableCheck implements ReviewableAfterFraudCheck {

    private static final EnumSet<ProcessState> TERMINAL_PROCESS_STATES = EnumSet.of(
            ProcessState.SUCCEEDED, ProcessState.FAILED, ProcessState.ERROR);

    private List<String> excludedBusinessProcesses;

    @Override
    public boolean perform(final OrderModel order) {

        if (CollectionUtils.isNotEmpty(order.getOrderProcess())) {
            for (final OrderProcessModel process : order.getOrderProcess()) {

                // excluding configured processes from denying cancellation
                if (CollectionUtils.isEmpty(excludedBusinessProcesses)
                        || !excludedBusinessProcesses.contains(process.getProcessDefinitionName())) {

                    if (!TERMINAL_PROCESS_STATES.contains(process.getState())) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * @param excludedBusinessProcesses
     *            the excludedBusinessProcesses to set
     */
    @Required
    public void setExcludedBusinessProcesses(final List<String> excludedBusinessProcesses) {
        this.excludedBusinessProcesses = excludedBusinessProcesses;
    }

}

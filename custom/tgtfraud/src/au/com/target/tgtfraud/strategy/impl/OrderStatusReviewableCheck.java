package au.com.target.tgtfraud.strategy.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.Set;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfraud.strategy.ReviewableAfterFraudCheck;

/**
 * Strategy that checks order status before allowing or disallowing
 * a review action related to fraud.
 */
public class OrderStatusReviewableCheck implements ReviewableAfterFraudCheck {

    private Set<OrderStatus> aptStatuses;

    @Override
    public boolean perform(final OrderModel order) {
        return getAptStatuses().contains(order.getStatus());
    }

    /**
     * Returns the set of apt order statuses.
     *
     * @return the set of order statuses which will pass this check
     */
    public Set<OrderStatus> getAptStatuses() {
        return aptStatuses;
    }

    /**
     * Sets the set of apt order statuses.
     *
     * @param aptStatuses the set of order statuses which will pass this check
     */
    @Required
    public void setAptStatuses(final Set<OrderStatus> aptStatuses) {
        this.aptStatuses = aptStatuses;
    }
}

/**
 * 
 */
package au.com.target.tgtfraud.strategy.impl;

import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.fraud.impl.FraudSymptom;
import de.hybris.platform.fraud.model.FraudReportModel;
import de.hybris.platform.fraud.model.FraudSymptomScoringModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtfraud.strategy.TargetFraudReportCreationStrategy;


/**
 * @author mjanarth
 * 
 */
public class TargetFraudReportCreationStrategyImpl extends AbstractBusinessService implements
        TargetFraudReportCreationStrategy {


    @Override
    public FraudReportModel createFraudReport(final FraudServiceResponse response, final OrderModel orderModel,
            final FraudStatus status) {

        final FraudReportModel fraudReport = getModelService().create(FraudReportModel.class);
        fraudReport.setOrder(orderModel);
        fraudReport.setProvider(response.getProviderName());
        fraudReport.setTimestamp(new Date());
        fraudReport.setExplanation(response.getDescription());
        fraudReport.setStatus(status);
        int reportNumber = 0;
        if (CollectionUtils.isNotEmpty(orderModel.getFraudReports()))
        {
            reportNumber = orderModel.getFraudReports().size();
        }
        fraudReport.setCode(orderModel.getCode() + "_FR" + reportNumber);
        if (response.getSymptoms() != null) {
            List<FraudSymptomScoringModel> symptoms = null;
            for (final FraudSymptom symptom : response.getSymptoms())
            {
                if (symptoms == null)
                {
                    symptoms = new ArrayList<>();
                }
                final FraudSymptomScoringModel symptomScoring = getModelService()
                        .create(FraudSymptomScoringModel.class);
                symptomScoring.setFraudReport(fraudReport);
                symptomScoring.setName(symptom.getSymptom());
                symptomScoring.setExplanation(symptom.getExplanation());
                symptomScoring.setScore(symptom.getScore());
                symptoms.add(symptomScoring);
            }
            fraudReport.setFraudSymptomScorings(symptoms);
        }
        return fraudReport;

    }



}

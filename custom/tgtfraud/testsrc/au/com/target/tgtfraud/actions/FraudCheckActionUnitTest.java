/**
 * 
 */
package au.com.target.tgtfraud.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.fraud.model.FraudReportModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.paymentstandard.model.StandardPaymentModeModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;
import au.com.target.tgtfraud.FraudReportService;
import au.com.target.tgtfraud.TargetFraudService;
import au.com.target.tgtfraud.actions.FraudCheckAction.Transition;
import au.com.target.tgtfraud.exception.FraudCheckServiceUnavailableException;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FraudCheckActionUnitTest {

    private static final int RETRIES_ALLOWED = 3;
    private static final int RETRY_INTERVAL = 3000;
    private static final String ACCERTIFY = "ACCERTIFY";

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Mock
    private FraudReportService fraudReportService;

    @Mock
    private TargetFraudService targetFraudCheckService;

    @Mock
    private ModelService modelService;

    @Mock
    private FraudReportModel fraudReport;

    @Mock
    private FraudServiceResponse response;

    @Mock
    private StandardPaymentModeModel standardPaymentModel;

    @Mock
    private TargetVoucherService targetVoucherService;

    @Mock
    private TargetSalesApplicationConfigService salesApplicationConfigService;

    @InjectMocks
    private final FraudCheckAction fraudCheckAction = new FraudCheckAction();


    @Before
    public void setup() {
        fraudCheckAction.setRetryInterval(RETRY_INTERVAL);
        fraudCheckAction.setAllowedRetries(RETRIES_ALLOWED);
        fraudCheckAction.setProviderName(ACCERTIFY);
        BDDMockito.given(process.getOrder()).willReturn(order);

        BDDMockito.given(order.getPaymentMode()).willReturn(standardPaymentModel);
        BDDMockito.given(order.getSalesApplication()).willReturn(SalesApplication.WEB);

        BDDMockito.given(standardPaymentModel.getSkipFraudRespValidation()).willReturn(Boolean.FALSE);

        // Fraud report
        BDDMockito.given(fraudReportService.createFraudReport(response, order)).willReturn(fraudReport);
        BDDMockito.given(fraudReport.getStatus()).willReturn(FraudStatus.OK);
    }

    @Test
    public void testFraudStatusToTransition() {

        Assert.assertEquals(FraudCheckAction.Transition.ACCEPT.toString(),
                fraudCheckAction.fraudStatusToTransition(null));

        Assert.assertEquals(FraudCheckAction.Transition.ACCEPT.toString(),
                fraudCheckAction.fraudStatusToTransition(FraudStatus.OK));

        Assert.assertEquals(FraudCheckAction.Transition.REVIEW.toString(),
                fraudCheckAction.fraudStatusToTransition(FraudStatus.CHECK));

        Assert.assertEquals(FraudCheckAction.Transition.REJECT.toString(),
                fraudCheckAction.fraudStatusToTransition(FraudStatus.FRAUD));

    }


    @Test(expected = RetryLaterException.class)
    public void testExecuteServiceUnavailable() throws Exception {

        BDDMockito.given(
                targetFraudCheckService.recognizeOrderSymptoms(ACCERTIFY, order))
                .willThrow(new FraudCheckServiceUnavailableException("Test"));

        fraudCheckAction.execute(process);
    }

    @Test
    public void testExecuteOk() throws Exception {

        BDDMockito.given(targetFraudCheckService.recognizeOrderSymptoms(ACCERTIFY, order)).willReturn(response);

        final String result = fraudCheckAction.execute(process);
        Assert.assertEquals(Transition.ACCEPT.toString(), result);
    }

    @Test
    public void testSkipFraudValidation() throws Exception {

        BDDMockito.given(standardPaymentModel.getSkipFraudRespValidation()).willReturn(Boolean.TRUE);
        BDDMockito.given(targetFraudCheckService.recognizeOrderSymptoms(ACCERTIFY, order)).willReturn(response);
        BDDMockito.given(fraudReport.getStatus()).willReturn(FraudStatus.FRAUD);

        final String result = fraudCheckAction.execute(process);
        Assert.assertEquals(Transition.ACCEPT.toString(), result);
    }

    @Test
    public void testNotSkipFraudValidation() throws Exception {

        BDDMockito.given(standardPaymentModel.getSkipFraudRespValidation()).willReturn(Boolean.FALSE);
        BDDMockito.given(targetFraudCheckService.recognizeOrderSymptoms(ACCERTIFY, order)).willReturn(response);
        BDDMockito.given(fraudReport.getStatus()).willReturn(FraudStatus.FRAUD);

        final String result = fraudCheckAction.execute(process);
        Assert.assertEquals(Transition.REJECT.toString(), result);
    }

    @Test
    public void testFraudValidationWithFlybuys() throws Exception {
        final FlybuysDiscountModel flybuysDiscountModel = Mockito.mock(FlybuysDiscountModel.class);
        BDDMockito.given(fraudReportService.createFraudReport(response, order)).willReturn(fraudReport);
        BDDMockito.given(fraudReport.getStatus()).willReturn(FraudStatus.CHECK);
        BDDMockito.given(targetFraudCheckService.recognizeOrderSymptoms(ACCERTIFY, order)).willReturn(response);
        BDDMockito.given(targetVoucherService.getFlybuysDiscountForOrder(order)).willReturn(flybuysDiscountModel);

        final String result = fraudCheckAction.execute(process);
        Assert.assertEquals(Transition.REJECT.toString(), result);
    }

    @Test
    public void testFraudValidationWithoutFlybuys() throws Exception {
        BDDMockito.given(fraudReportService.createFraudReport(response, order)).willReturn(fraudReport);
        BDDMockito.given(fraudReport.getStatus()).willReturn(FraudStatus.CHECK);
        BDDMockito.given(targetFraudCheckService.recognizeOrderSymptoms(ACCERTIFY, order)).willReturn(response);
        BDDMockito.given(targetVoucherService.getFlybuysDiscountForOrder(order)).willReturn(null);

        final String result = fraudCheckAction.execute(process);
        Assert.assertEquals(Transition.REVIEW.toString(), result);
    }

    @Test
    public void testSkipFraudValidationWithSalesAppConfigMarkedAsNotSkip() throws Exception {

        final SalesApplication salesApp = SalesApplication.WEB;
        Mockito.doReturn(Boolean.FALSE).when(salesApplicationConfigService).isSkipFraudCheck(salesApp);
        BDDMockito.given(order.getSalesApplication()).willReturn(salesApp);
        BDDMockito.given(fraudReportService.createFraudReport(response, order)).willReturn(fraudReport);
        BDDMockito.given(fraudReport.getStatus()).willReturn(FraudStatus.CHECK);
        BDDMockito.given(targetFraudCheckService.recognizeOrderSymptoms(ACCERTIFY, order)).willReturn(response);
        BDDMockito.given(targetVoucherService.getFlybuysDiscountForOrder(order)).willReturn(null);

        final String result = fraudCheckAction.execute(process);
        Assert.assertEquals(Transition.REVIEW.toString(), result);
    }

    @Test
    public void testSkipFraudValidationWithSalesAppConfigMarkedAsSkip() throws Exception {

        final SalesApplication salesApp = SalesApplication.TRADEME;
        Mockito.doReturn(Boolean.TRUE).when(salesApplicationConfigService).isSkipFraudCheck(salesApp);
        BDDMockito.given(order.getSalesApplication()).willReturn(salesApp);
        final String result = fraudCheckAction.execute(process);

        Assert.assertEquals(Transition.ACCEPT.toString(), result);
    }

}

/**
 * 
 */
package au.com.target.tgtfraud.actions;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtfraud.TargetFraudService;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendCancelRefundForFraudCheckActionTest {

    @InjectMocks
    private final SendCancelRefundForFraudCheckAction action = new SendCancelRefundForFraudCheckAction();

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;
    @Mock
    private OrderProcessModel process;
    @Mock
    private List refundList;
    @Mock
    private TargetFraudService targetFraudService;
    @Mock
    private OrderCancelRecordEntryModel cancelEntry;

    @Before
    public void setUp() {
        when(orderProcessParameterHelper.getOrderCancelRequest(process)).thenReturn(cancelEntry);
        when(orderProcessParameterHelper.getRefundPaymentEntryList(process)).thenReturn(refundList);
    }

    @Test
    public void testSendDetails() {
        action.sendDetails(process);
        verify(targetFraudService).submitRefund(null, cancelEntry, refundList);
    }

    @Test
    public void testShouldNotResetRefundAmountIfItIsNull() {
        when(orderProcessParameterHelper.getRefundAmount(process)).thenReturn(null);
        action.sendDetails(process);
        verify(targetFraudService).submitRefund(null, cancelEntry, refundList);
        verify(cancelEntry, times(0)).setRefundedAmount(any(Double.class));
    }

    @Test
    public void testShouldNotResetRefundAmountIfItIsZero() {
        when(orderProcessParameterHelper.getRefundAmount(process)).thenReturn(BigDecimal.ZERO);
        action.sendDetails(process);
        verify(targetFraudService).submitRefund(null, cancelEntry, refundList);
        verify(cancelEntry, times(0)).setRefundedAmount(any(Double.class));
    }

    @Test
    public void testShouldSetRefundedAmountFromProcessHelper() {
        when(cancelEntry.getRefundedAmount()).thenReturn(Double.valueOf(0d));
        when(orderProcessParameterHelper.getRefundAmount(process)).thenReturn(BigDecimal.TEN);
        action.sendDetails(process);
        verify(targetFraudService).submitRefund(null, cancelEntry, refundList);
        verify(cancelEntry).setRefundedAmount(Double.valueOf(10d));
    }
}

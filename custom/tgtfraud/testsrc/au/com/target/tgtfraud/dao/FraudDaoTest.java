/**
 * 
 */
package au.com.target.tgtfraud.dao;

import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.fraud.model.FraudReportModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;

import au.com.target.tgtfraud.dao.impl.FraudDaoImpl;


public class FraudDaoTest extends ServicelayerTransactionalTest {

    @Resource
    private FraudDaoImpl fraudDao;

    @Resource
    private ModelService modelService;

    @Test
    public void testGetOrdersForAutoAccept() {

        createOrderForCode("testOrder");
        final List<OrderModel> list = fraudDao.getOrdersForAutoAccept(OrderStatus.REVIEW,
                DateTime.now().minusHours(72).toDate(),
                FraudStatus.CHECK);

        Assert.assertEquals(1, list.size());
        Assert.assertEquals("testOrder", list.get(0).getCode());
    }

    @Test
    public void testGetZeroOrdersForAutoAccept() {

        createOrderForCode("testOrder");
        final List<OrderModel> list = fraudDao.getOrdersForAutoAccept(OrderStatus.REVIEW,
                DateTime.now().minusHours(90).toDate(),
                FraudStatus.CHECK);

        Assert.assertEquals(0, list.size());

    }

    private OrderModel createOrderForCode(final String code) {

        final OrderModel order = new OrderModel();
        order.setStatus(OrderStatus.REVIEW);

        order.setCode(code);
        order.setCurrency(getCurrencyTestData());
        order.setUser(getUser());
        order.setDate(new Date());
        modelService.save(order);

        final FraudReportModel fraudReport = modelService.create(FraudReportModel.class);
        fraudReport.setTimestamp(DateTime.now().minusHours(80).toDate());
        fraudReport.setStatus(FraudStatus.CHECK);
        fraudReport.setCode("test");
        fraudReport.setOrder(order);
        modelService.save(fraudReport);
        final Set<FraudReportModel> set = new HashSet<>();
        set.add(fraudReport);

        order.setFraudReports(set);
        modelService.save(order);
        return order;

    }

    private CurrencyModel getCurrencyTestData() {
        final CurrencyModel currency = modelService.create(CurrencyModel.class);
        currency.setIsocode("TEST");
        currency.setSymbol("TEST");
        modelService.save(currency);

        return currency;
    }

    private UserModel getUser() {
        final UserModel user = modelService.create(UserModel.class);
        user.setUid("user");
        modelService.save(user);
        return user;
    }

}

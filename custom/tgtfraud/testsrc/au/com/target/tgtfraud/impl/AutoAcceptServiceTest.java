package au.com.target.tgtfraud.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtfraud.FraudReportService;
import au.com.target.tgtfraud.TargetFraudServiceResponse;
import au.com.target.tgtfraud.dao.FraudDao;
import au.com.target.tgtfraud.provider.data.RecommendationTypeEnum;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AutoAcceptServiceTest {

    private static final String APPROVAL_EXPLANATION = "Testing with Mockito";
    private static final String PROVIDER_NAME = "Junit Test";

    @Mock
    private FraudDao fraudDao;

    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;

    @Mock
    private FraudReportService fraudReportService;

    @InjectMocks
    private final AutoAcceptServiceImpl autoAcceptService = new AutoAcceptServiceImpl();

    @Mock
    private OrderModel order;

    @Mock
    private OrderModel order2;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Before
    public void setUp() {
        autoAcceptService.setApprovalExplanation(APPROVAL_EXPLANATION);
        autoAcceptService.setProviderName(PROVIDER_NAME);
    }

    @Test
    public void testAutoAcceptReviewOrdersNoOrders() {
        final List<OrderModel> list = new ArrayList<>();
        given(fraudDao.getOrdersForAutoAccept(OrderStatus.REVIEW, new Date(), FraudStatus.CHECK))
                .willReturn(list);
        autoAcceptService.triggerAcceptProcessForAutoAcceptReviewOrders();
        verifyZeroInteractions(targetBusinessProcessService);
    }

    @Test
    public void testAutoAcceptReviewOrdersHas1Order() {
        final List<OrderModel> list = new ArrayList<>();
        list.add(order);
        final Date date = any(Date.class);
        given(fraudDao.getOrdersForAutoAccept(any(OrderStatus.class),
                date,
                any(FraudStatus.class)))
                        .willReturn(list);
        given(order.getCode()).willReturn("test");

        autoAcceptService.triggerAcceptProcessForAutoAcceptReviewOrders();

        verify(targetBusinessProcessService).startOrderAcceptProcess(order);

        final ArgumentCaptor<TargetFraudServiceResponse> fraudServiceResponseCaptor = ArgumentCaptor
                .forClass(TargetFraudServiceResponse.class);
        verify(fraudReportService).createFraudReport(fraudServiceResponseCaptor.capture(), Mockito.eq(order));


        assertThat(fraudServiceResponseCaptor.getValue().getProviderName()).isEqualTo(PROVIDER_NAME);
        assertThat(fraudServiceResponseCaptor.getValue().getDescription()).isEqualTo(APPROVAL_EXPLANATION);
        assertThat(fraudServiceResponseCaptor.getValue().getRecommendation()).isEqualTo(RecommendationTypeEnum.ACCEPT);

    }

    @Test
    public void testAutoAcceptReviewOrdersWithFluentFeatureSwitchOn() {
        final List<OrderModel> list = new ArrayList<>();
        list.add(order);
        final Date date = any(Date.class);
        given(fraudDao.getOrdersForAutoAccept(any(OrderStatus.class),
                date,
                any(FraudStatus.class)))
                        .willReturn(list);
        given(order.getCode()).willReturn("test");
        given(order.getFluentId()).willReturn("123");
        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)))
                .willReturn(Boolean.TRUE);
        autoAcceptService.triggerAcceptProcessForAutoAcceptReviewOrders();
        verify(targetBusinessProcessService).startFluentOrderAcceptProcess(order);
    }

    @Test
    public void testAutoAcceptReviewOrdersForPreOrderWithFluentFeatureSwitchOn() {
        final List<OrderModel> list = new ArrayList<>();
        list.add(order);
        final Date date = any(Date.class);
        given(fraudDao.getOrdersForAutoAccept(any(OrderStatus.class),
                date,
                any(FraudStatus.class)))
                        .willReturn(list);
        given(order.getFluentId()).willReturn("123");
        given(order.getCode()).willReturn("test");
        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)))
                .willReturn(Boolean.TRUE);
        given(order.getContainsPreOrderItems()).willReturn(Boolean.TRUE);

        autoAcceptService.triggerAcceptProcessForAutoAcceptReviewOrders();
        verify(targetBusinessProcessService, never()).startFluentOrderAcceptProcess(order);
        verify(targetBusinessProcessService, never()).startFluentPreOrderReleaseAcceptProcess(order);
        verify(targetBusinessProcessService).startFluentAcceptPreOrderProcess(order);

    }

    @Test
    public void testAutoAcceptReviewOrdersForPreOrderRelease() {
        final List<OrderModel> list = new ArrayList<>();
        list.add(order);
        final Date date = any(Date.class);
        given(fraudDao.getOrdersForAutoAccept(any(OrderStatus.class),
                date,
                any(FraudStatus.class)))
                        .willReturn(list);
        given(order.getCode()).willReturn("test");
        given(order.getFluentId()).willReturn("123");
        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)))
                .willReturn(Boolean.TRUE);
        given(order.getContainsPreOrderItems()).willReturn(Boolean.FALSE);
        final Date salesDateMock = mock(Date.class);
        given(order.getNormalSaleStartDateTime()).willReturn(salesDateMock);

        autoAcceptService.triggerAcceptProcessForAutoAcceptReviewOrders();
        verify(targetBusinessProcessService, never()).startFluentOrderAcceptProcess(order);
        verify(targetBusinessProcessService, never()).startFluentAcceptPreOrderProcess(order);
        verify(targetBusinessProcessService).startFluentPreOrderReleaseAcceptProcess(order);

    }

    @Test
    public void testAutoAcceptReviewOrdersForPreOrderWithFluentFeatureSwitchOff() {
        final List<OrderModel> list = new ArrayList<>();
        list.add(order);
        final Date date = any(Date.class);
        given(fraudDao.getOrdersForAutoAccept(any(OrderStatus.class),
                date,
                any(FraudStatus.class)))
                        .willReturn(list);
        given(order.getCode()).willReturn("test");
        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)))
                .willReturn(Boolean.FALSE);
        given(order.getContainsPreOrderItems()).willReturn(Boolean.TRUE);
        final Date salesDateMock = mock(Date.class);
        given(order.getNormalSaleStartDateTime()).willReturn(salesDateMock);

        autoAcceptService.triggerAcceptProcessForAutoAcceptReviewOrders();
        verify(targetBusinessProcessService, never()).startFluentOrderAcceptProcess(order);
        verify(targetBusinessProcessService, never()).startFluentAcceptPreOrderProcess(order);
        verify(targetBusinessProcessService, never()).startFluentPreOrderReleaseAcceptProcess(order);
        verify(targetBusinessProcessService, never()).startOrderAcceptProcess(order);
        verify(targetBusinessProcessService).startPreOrderAcceptProcess(order);

    }

    @Test
    public void testAutoAcceptReviewOrdersHas2Orders() {
        final List<OrderModel> list = new ArrayList<>();
        list.add(order);
        list.add(order2);
        final Date date = any(Date.class);
        given(fraudDao.getOrdersForAutoAccept(any(OrderStatus.class),
                date,
                any(FraudStatus.class)))
                        .willReturn(list);
        given(order.getCode()).willReturn("test");
        given(order2.getCode()).willReturn("test");

        autoAcceptService.triggerAcceptProcessForAutoAcceptReviewOrders();

        verify(targetBusinessProcessService).startOrderAcceptProcess(order);
        verify(targetBusinessProcessService).startOrderAcceptProcess(order2);

        final ArgumentCaptor<TargetFraudServiceResponse> fraudServiceResponseCaptor = ArgumentCaptor
                .forClass(TargetFraudServiceResponse.class);

        verify(fraudReportService, times(2)).createFraudReport(fraudServiceResponseCaptor.capture(),
                any(OrderModel.class));

        assertThat(fraudServiceResponseCaptor.getValue().getProviderName()).isEqualTo(PROVIDER_NAME);
        assertThat(fraudServiceResponseCaptor.getValue().getDescription()).isEqualTo(APPROVAL_EXPLANATION);
        assertThat(fraudServiceResponseCaptor.getValue().getRecommendation()).isEqualTo(RecommendationTypeEnum.ACCEPT);

    }

    @Test
    public void testAutoAcceptReviewOldOrdersWithFluentFeatureSwitchOn() {
        final List<OrderModel> list = new ArrayList<>();
        list.add(order);
        final Date date = any(Date.class);
        given(fraudDao.getOrdersForAutoAccept(any(OrderStatus.class),
                date,
                any(FraudStatus.class)))
                        .willReturn(list);
        given(order.getCode()).willReturn("test");
        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)))
                .willReturn(Boolean.TRUE);
        autoAcceptService.triggerAcceptProcessForAutoAcceptReviewOrders();
        verify(targetBusinessProcessService).startOrderAcceptProcess(order);
    }

}

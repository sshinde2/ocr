package au.com.target.tgtfraud.impl;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfraud.strategy.ReviewableAfterFraudCheck;

import com.google.common.collect.ImmutableList;


/**
 * Test suite for {@link DefaultTargetFraudService}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultTargetFraudServiceTest {

    @InjectMocks
    private final DefaultTargetFraudService fraudService = new DefaultTargetFraudService();

    @Mock
    private OrderModel order;

    /**
     * Verifies that {@link DefaultTargetFraudService#isOrderReviewable(OrderModel)} will correctly aggregate decisions
     * taken by each particular {@link ReviewableAfterFraudCheck}.
     */
    @SuppressWarnings("boxing")
    @Test
    public void testChecksToDetectIfOrderIsReviewable() {
        final ReviewableAfterFraudCheck check1 = mock(ReviewableAfterFraudCheck.class);
        final ReviewableAfterFraudCheck check2 = mock(ReviewableAfterFraudCheck.class);
        final ReviewableAfterFraudCheck check3 = mock(ReviewableAfterFraudCheck.class);
        when(check1.perform(order)).thenReturn(true);
        when(check2.perform(order)).thenReturn(false);
        when(check3.perform(order)).thenReturn(true);

        fraudService.setReviewableAfterFraudChecks(ImmutableList.of(check1, check2, check3));

        assertFalse(fraudService.isOrderReviewable(order));
    }
}

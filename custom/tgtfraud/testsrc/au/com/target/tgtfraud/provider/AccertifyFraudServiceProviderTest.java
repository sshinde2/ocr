/**
 * 
 */
package au.com.target.tgtfraud.provider;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.order.TargetDiscountService;
import au.com.target.tgtfraud.TargetFraudServiceResponse;
import au.com.target.tgtfraud.exception.FraudCheckServiceUnavailableException;
import au.com.target.tgtfraud.provider.data.Order;
import au.com.target.tgtfraud.provider.data.OrderTypeEnum;
import au.com.target.tgtfraud.provider.data.PaymentInfo;
import au.com.target.tgtfraud.provider.data.RecommendationTypeEnum;
import au.com.target.tgtfraud.provider.data.Result;
import au.com.target.tgtfraud.provider.data.Transaction;
import au.com.target.tgtfraud.provider.util.AccertifyUtil;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;


/**
 * @author vmuthura
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AccertifyFraudServiceProviderTest {

    @Mock
    private RestOperations restOperations;
    @Mock
    private TargetDiscountService targetDiscountService;
    @Mock
    private AccertifyUtil accertifyUtil;
    @Mock
    private OrderModel orderModel;
    @Mock
    private Order order;
    @Mock
    private PaymentInfo paymentInfo;
    @Mock
    private Result response;
    @Mock
    private Transaction transaction;

    private final String url = "/accertify";

    @InjectMocks
    private final AccertifyFraudServiceProvider targetFraudServiceProvider = new AccertifyFraudServiceProvider();

    @Before
    @SuppressWarnings("deprecation")
    public void setUp() {
        final Date date = new Date();
        final DeliveryModeModel deliveryMode = mock(DeliveryModeModel.class);
        given(deliveryMode.getName()).willReturn("AUS POST");
        final TargetCustomerModel customer = mock(TargetCustomerModel.class);
        given(customer.getCustomerID()).willReturn("00006001");
        given(customer.getCreationtime()).willReturn(date);
        given(customer.getDisplayName()).willReturn("Jack Sparrow");
        given(customer.getFirstname()).willReturn("Jack");
        given(customer.getLastname()).willReturn("Sparrow");
        given(customer.getContactEmail()).willReturn("jsparrow@target.com.au");
        given(customer.getMobileNumber()).willReturn("0469242341");
        given(customer.getType()).willReturn(CustomerType.GUEST);
        final OrderEntryModel orderEntryModel = mock(OrderEntryModel.class);
        final TargetColourVariantProductModel productModel = mock(TargetColourVariantProductModel.class);
        final BrandModel brandModel = mock(BrandModel.class);
        final TargetProductCategoryModel categoryModel = mock(TargetProductCategoryModel.class);
        final TargetProductModel targetProduct = mock(TargetProductModel.class);
        given(orderEntryModel.getBasePrice()).willReturn(Double.valueOf(20.00d));
        given(orderEntryModel.getQuantity()).willReturn(Long.valueOf(1l));
        given(orderEntryModel.getProduct()).willReturn(productModel);

        given(productModel.getBrand()).willReturn(brandModel);
        given(orderEntryModel.getProduct().getDescription()).willReturn("Blake Cushion");
        given(orderEntryModel.getProduct().getCode()).willReturn("51584764");
        given(productModel.getDepartment()).willReturn(Integer.valueOf(500));
        given(productModel.getBrand()).willReturn(brandModel);
        given(productModel.getBaseProduct()).willReturn(targetProduct);
        given(targetProduct.getPrimarySuperCategory()).willReturn(categoryModel);
        given(categoryModel.getName()).willReturn("Bed Cushions");
        given(brandModel.getName()).willReturn("Grandeur");

        final AddressModel addressModel = mock(AddressModel.class);
        given(addressModel.getFirstname()).willReturn("Jack");
        given(addressModel.getLastname()).willReturn("Rabbit");
        given(addressModel.getLine1()).willReturn("Delivery St");
        given(addressModel.getLine2()).willReturn("");
        given(addressModel.getTown()).willReturn("Geelong");
        given(addressModel.getDistrict()).willReturn("VIC");
        given(addressModel.getCountry()).willReturn(new CountryModel("AU"));
        given(addressModel.getPostalcode()).willReturn("3220");
        given(addressModel.getCellphone()).willReturn("048484848");

        final CreditCardPaymentInfoModel ccPaymentInfoModel = mock(CreditCardPaymentInfoModel.class);
        given(ccPaymentInfoModel.getNumber()).willReturn("5123 45XX XXXX X346");
        given(ccPaymentInfoModel.getBillingAddress()).willReturn(addressModel);
        given(ccPaymentInfoModel.getType()).willReturn(CreditCardType.MASTER);
        given(ccPaymentInfoModel.getValidToMonth()).willReturn("07");
        given(ccPaymentInfoModel.getValidToYear()).willReturn("2017");
        given(ccPaymentInfoModel.getCcOwner()).willReturn("Vijay");

        final PaypalPaymentInfoModel paypalPaymentInfoModel = mock(PaypalPaymentInfoModel.class);
        given(paypalPaymentInfoModel.getPayerId()).willReturn("Jack@rabbit.com");
        given(paypalPaymentInfoModel.getBillingAddress()).willReturn(addressModel);

        final PaymentTransactionEntryModel ccTxnEntry = mock(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTxnModel = mock(PaymentTransactionModel.class);
        given(ccTxnEntry.getPaymentTransaction()).willReturn(paymentTxnModel);
        given(paymentTxnModel.getInfo()).willReturn(ccPaymentInfoModel);
        given(ccTxnEntry.getRequestId()).willReturn("654321");
        given(ccTxnEntry.getAmount()).willReturn(BigDecimal.valueOf(29.00d));
        given(ccTxnEntry.getTransactionStatus()).willReturn("ACCEPTED");

        final PaymentTransactionEntryModel paypalTxnEntry = mock(PaymentTransactionEntryModel.class);
        given(paypalTxnEntry.getRequestId()).willReturn("123456");
        given(paypalTxnEntry.getAmount()).willReturn(BigDecimal.valueOf(100.50d));
        given(paypalTxnEntry.getTransactionStatus()).willReturn("REJECTED");

        final PaymentTransactionModel ccModel = mock(PaymentTransactionModel.class);
        given(ccModel.getInfo()).willReturn(ccPaymentInfoModel);
        given(ccModel.getEntries()).willReturn(new LinkedList() {
            {
                add(ccTxnEntry);
            }
        });

        final PaymentTransactionModel paypalModel = mock(PaymentTransactionModel.class);
        given(paypalModel.getInfo()).willReturn(paypalPaymentInfoModel);
        given(paypalModel.getEntries()).willReturn(new LinkedList() {
            {
                add(paypalTxnEntry);
            }
        });

        given(orderModel.getCode()).willReturn("09223039");
        given(orderModel.getCreationtime()).willReturn(date);
        given(orderModel.getTotalPrice()).willReturn(Double.valueOf("29.00"));
        given(orderModel.getDeliveryCost()).willReturn(Double.valueOf("9.00"));
        given(orderModel.getTotalTax()).willReturn(Double.valueOf("0.82"));
        given(orderModel.getSalesApplication()).willReturn(SalesApplication.WEB);
        given(orderModel.getTotalDiscounts()).willReturn(Double.valueOf("0.00"));
        given(orderModel.getDeliveryAddress()).willReturn(addressModel);
        given(orderModel.getDeliveryMode()).willReturn(deliveryMode);
        given(orderModel.getEntries()).willReturn(new LinkedList<AbstractOrderEntryModel>() {
            {
                add(orderEntryModel);
            }
        });
        given(orderModel.getUser()).willReturn(customer);
        given(orderModel.getPaymentTransactions()).willReturn(new LinkedList() {
            {
                add(paypalModel);
                add(ccModel);
            }
        });

        given(
                accertifyUtil.buildTransaction(any(OrderModel.class), any(OrderModificationRecordEntryModel.class),
                        any(OrderTypeEnum.class), any(List.class)))
                .willReturn(transaction);
        given(order.getPaymentInformation()).willReturn(new LinkedList<PaymentInfo>() {
            {
                add(paymentInfo);
            }
        });
        targetFraudServiceProvider.setUrl("uri");
        given(restOperations.postForObject("uri", transaction, Result.class)).willReturn(response);
    }

    @Test(expected = FraudCheckServiceUnavailableException.class)
    public void testAccertifyServiceException() {
        given(
                restOperations.postForObject(Mockito.anyString(), Mockito.anyObject(), Mockito.any(Class.class)))
                .willThrow(new RestClientException("Internal Server error"));
        targetFraudServiceProvider.recognizeOrderFraudSymptoms(orderModel);
    }


    @Test(expected = FraudCheckServiceUnavailableException.class)
    public void testAccertifyServiceExceptionWhenPerformFraudCheck() {
        given(
                restOperations.postForObject(Mockito.anyString(), Mockito.anyObject(), Mockito.any(Class.class)))
                .willThrow(new RestClientException("Internal Server error"));
        targetFraudServiceProvider.performFraudCheck(transaction);
    }

    @Test
    public void testAccertifyResponse() {
        final Result result = mock(Result.class);
        given(result.getRecommendation()).willReturn(RecommendationTypeEnum.ACCEPT);
        given(
                restOperations.postForObject(Mockito.anyString(), Mockito.anyObject(), Mockito.any(Class.class)))
                .willReturn(result);
        final TargetFraudServiceResponse serviceResponse = (TargetFraudServiceResponse)targetFraudServiceProvider
                .recognizeOrderFraudSymptoms(orderModel);
        assertThat(serviceResponse.getRecommendation()).isEqualTo(RecommendationTypeEnum.ACCEPT);
    }

    @Test
    public void testAccertifyResponseForPreOrder() {
        final Result result = mock(Result.class);
        given(result.getRecommendation()).willReturn(RecommendationTypeEnum.ACCEPT);
        given(orderModel.getContainsPreOrderItems()).willReturn(Boolean.TRUE);
        targetFraudServiceProvider.setUrl(url);
        given(accertifyUtil.buildTransaction(orderModel, null, OrderTypeEnum.PREORDER, null)).willReturn(transaction);
        given(
                restOperations.postForObject(url, transaction, Result.class))
                .willReturn(result);
        final TargetFraudServiceResponse serviceResponse = (TargetFraudServiceResponse)targetFraudServiceProvider
                .recognizeOrderFraudSymptoms(orderModel);
        verify(accertifyUtil).buildTransaction(orderModel, null, OrderTypeEnum.PREORDER, null);
        assertThat(serviceResponse.getRecommendation()).isEqualTo(RecommendationTypeEnum.ACCEPT);
    }

    @Test
    public void testAccertifyResponseForNormalOrder() {
        final Result result = mock(Result.class);
        given(result.getRecommendation()).willReturn(RecommendationTypeEnum.ACCEPT);
        given(orderModel.getContainsPreOrderItems()).willReturn(Boolean.FALSE);
        targetFraudServiceProvider.setUrl(url);
        given(accertifyUtil.buildTransaction(orderModel, null, OrderTypeEnum.SALE, null)).willReturn(transaction);
        given(
                restOperations.postForObject(url, transaction, Result.class))
                .willReturn(result);
        final TargetFraudServiceResponse serviceResponse = (TargetFraudServiceResponse)targetFraudServiceProvider
                .recognizeOrderFraudSymptoms(orderModel);
        verify(accertifyUtil).buildTransaction(orderModel, null, OrderTypeEnum.SALE, null);
        assertThat(serviceResponse.getRecommendation()).isEqualTo(RecommendationTypeEnum.ACCEPT);
    }

    @Test
    public void testAccertifyResponsNormalOrder() {
        final Result result = mock(Result.class);
        given(result.getRecommendation()).willReturn(RecommendationTypeEnum.ACCEPT);
        given(orderModel.getContainsPreOrderItems()).willReturn(null);
        targetFraudServiceProvider.setUrl(url);
        given(accertifyUtil.buildTransaction(orderModel, null, OrderTypeEnum.PREORDER, null)).willReturn(transaction);
        given(
                restOperations.postForObject(url, transaction, Result.class))
                .willReturn(result);
        final TargetFraudServiceResponse serviceResponse = (TargetFraudServiceResponse)targetFraudServiceProvider
                .recognizeOrderFraudSymptoms(orderModel);
        verify(accertifyUtil).buildTransaction(orderModel, null, OrderTypeEnum.SALE, null);
        assertThat(serviceResponse.getRecommendation()).isEqualTo(RecommendationTypeEnum.ACCEPT);
    }

    @Test
    public void testAccertifyResponseWithBadSymptom() {
        final Result result = mock(Result.class);
        given(result.getRecommendation()).willReturn(RecommendationTypeEnum.ACCEPT);
        given(result.getRulesTripped()).willReturn("123;123;123");
        given(
                restOperations.postForObject(Mockito.anyString(), Mockito.anyObject(), Mockito.any(Class.class)))
                .willReturn(result);
        final TargetFraudServiceResponse serviceResponse = (TargetFraudServiceResponse)targetFraudServiceProvider
                .recognizeOrderFraudSymptoms(orderModel);
        assertThat(serviceResponse.getRecommendation()).isEqualTo(RecommendationTypeEnum.ACCEPT);
        assertThat(CollectionUtils.isEmpty(serviceResponse.getSymptoms())).isTrue();
    }

    @Test
    public void testAccertifyResponseWith2Symptoms() {
        final Result result = mock(Result.class);
        given(result.getRecommendation()).willReturn(RecommendationTypeEnum.ACCEPT);
        given(result.getRulesTripped()).willReturn("123:123:123;12:12:12");
        given(
                restOperations.postForObject(Mockito.anyString(), Mockito.anyObject(), Mockito.any(Class.class)))
                .willReturn(result);
        final TargetFraudServiceResponse serviceResponse = (TargetFraudServiceResponse)targetFraudServiceProvider
                .recognizeOrderFraudSymptoms(orderModel);
        assertThat(serviceResponse.getRecommendation()).isEqualTo(RecommendationTypeEnum.ACCEPT);
        assertThat(serviceResponse.getSymptoms().size()).isEqualTo(2);
    }

    @Test
    public void testPerformFraudCheckForRefund() {
        final OrderModificationRecordEntryModel orderModRecordEntryModel = mock(
                OrderModificationRecordEntryModel.class);
        final OrderModificationRecordModel modRecord = mock(OrderModificationRecordModel.class);
        given(modRecord.getOrder()).willReturn(orderModel);
        given(orderModRecordEntryModel.getModificationRecord()).willReturn(modRecord);
        final List<PaymentTransactionEntryModel> refundList = mock(List.class);
        targetFraudServiceProvider.submitRefund(orderModRecordEntryModel, refundList);
        verify(accertifyUtil).buildTransaction(orderModel, orderModRecordEntryModel, OrderTypeEnum.REFUND, refundList);
    }


    @Test
    public void testPerformFraudCheckForRefundWithNoRefundEntries() {
        final OrderModificationRecordEntryModel orderModRecordEntryModel = mock(
                OrderModificationRecordEntryModel.class);
        final OrderModificationRecordModel modRecord = mock(OrderModificationRecordModel.class);
        given(modRecord.getOrder()).willReturn(orderModel);
        given(orderModRecordEntryModel.getModificationRecord()).willReturn(modRecord);
        assertThat((targetFraudServiceProvider.submitRefund(orderModRecordEntryModel, null))).isNull();
        verify(accertifyUtil, times(0)).buildTransaction(orderModel, orderModRecordEntryModel, OrderTypeEnum.REFUND,
                null);
    }
}

package au.com.target.tgtfraud.strategy.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.enums.ProcessState;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;


/**
 * Test suite for {@link OrderProcessesReviewableCheck}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderProcessesReviewableCheckTest {

    @InjectMocks
    private final OrderProcessesReviewableCheck check = new OrderProcessesReviewableCheck();

    @Mock
    private OrderModel order;

    /**
     * Verifies that check for an order that does not have any business processes assigned will return {@code true}.
     */
    @Test
    public void testCheckWithEmptyOrderProcesses() {
        assertTrue(check.perform(order));
    }

    /**
     * Verifies that check for an order that has business processes, any of which have non-terminal state, will return
     * {@code false}.
     */
    @Test
    public void testProcessStatusDecisionAggregation() {
        final OrderProcessModel process1 = mock(OrderProcessModel.class);
        final OrderProcessModel process2 = mock(OrderProcessModel.class);
        final OrderProcessModel process3 = mock(OrderProcessModel.class);
        when(process1.getState()).thenReturn(ProcessState.FAILED);
        when(process2.getState()).thenReturn(ProcessState.SUCCEEDED);
        when(process3.getState()).thenReturn(ProcessState.CREATED);
        when(order.getOrderProcess()).thenReturn(ImmutableList.of(process1, process2, process3));
        assertFalse(check.perform(order));
    }

    /**
     * Verifies that any terminal business processes state related to order that is being inspected will cause this
     * check to return {@code true}.
     */
    @Test
    public void testHappyScenario() {
        final OrderProcessModel process1 = mock(OrderProcessModel.class);
        final OrderProcessModel process2 = mock(OrderProcessModel.class);
        final OrderProcessModel process3 = mock(OrderProcessModel.class);
        when(process1.getState()).thenReturn(ProcessState.SUCCEEDED);
        when(process2.getState()).thenReturn(ProcessState.FAILED);
        when(process3.getState()).thenReturn(ProcessState.ERROR);
        when(order.getOrderProcess()).thenReturn(ImmutableList.of(process1, process2, process3));
        assertTrue(check.perform(order));
    }

    @Test
    public void testProcessIsRunningButExcluded() {
        final OrderProcessModel process = new OrderProcessModel();
        process.setProcessDefinitionName("excluded");
        process.setState(ProcessState.RUNNING);

        Mockito.when(order.getOrderProcess()).thenReturn(Collections.singleton(process));

        check.setExcludedBusinessProcesses(Arrays.asList("excluded"));

        assertTrue(check.perform(order));
    }

}

/**
 * 
 */
package au.com.target.tgtfraud.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.fraud.model.FraudReportModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author mjanarth
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFraudReportCreationStrategyImplTest {


    private static final int FRAUD_REPORT_NUMBER = 2;
    private static final String ORDER_CODE = "orderCode";
    private static final String FRAUD_NOTE = "fraudNote";
    @Mock
    private ModelService modelService;
    @Mock
    private UserService userService;
    @Mock
    private UserModel user;
    private FraudReportModel fraudReportModel;
    @InjectMocks
    private final TargetFraudReportCreationStrategyImpl strategy = new TargetFraudReportCreationStrategyImpl();

    private FraudServiceResponse response;

    @Before
    public void init() {

        fraudReportModel = new FraudReportModel();
        user = Mockito.mock(UserModel.class);
        response = new FraudServiceResponse(FRAUD_NOTE, "admin");

        Mockito.when(user.getUid()).thenReturn("admin");
        Mockito.when(userService.getCurrentUser()).thenReturn(user);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddFraudReportToOrder() {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        Mockito.when(orderModel.getCode()).thenReturn(ORDER_CODE);
        BDDMockito.given(modelService.create(FraudReportModel.class)).willReturn(fraudReportModel);
        strategy.createFraudReport(response, orderModel, FraudStatus.OK);
        Assert.assertEquals(FraudStatus.OK, fraudReportModel.getStatus());
        Assert.assertEquals(FRAUD_NOTE, fraudReportModel.getExplanation());
        Assert.assertEquals(ORDER_CODE + "_FR0", fraudReportModel.getCode());
    }

    /**
     * Testing saving new fraud report model when order already has FraudReports
     */

    @SuppressWarnings("boxing")
    @Test
    public void testAddFraudReportToOrderWithFraudReports() {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        BDDMockito.given(modelService.create(FraudReportModel.class)).willReturn(fraudReportModel);
        final Set<FraudReportModel> setOfReports = Mockito.mock(Set.class);
        Mockito.when(setOfReports.isEmpty()).thenReturn(Boolean.FALSE);
        Mockito.when(setOfReports.size()).thenReturn(FRAUD_REPORT_NUMBER);
        Mockito.when(orderModel.getFraudReports()).thenReturn(setOfReports);
        Mockito.when(orderModel.getCode()).thenReturn(ORDER_CODE);
        strategy.createFraudReport(response, orderModel, FraudStatus.CHECK);
        Assert.assertEquals(orderModel, fraudReportModel.getOrder());
        Assert.assertEquals(FraudStatus.CHECK, fraudReportModel.getStatus());
        Assert.assertEquals(FRAUD_NOTE, fraudReportModel.getExplanation());
        Assert.assertEquals(ORDER_CODE + "_FR" + FRAUD_REPORT_NUMBER, fraudReportModel.getCode());
    }

    /**
     * Test setting current user id to fraudReportModel
     */
    @SuppressWarnings("boxing")
    @Test
    public void testCheckIsUserSetToFraudReport()
    {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final Set<FraudReportModel> setOfReports = Mockito.mock(Set.class);
        Mockito.when(setOfReports.isEmpty()).thenReturn(Boolean.FALSE);
        Mockito.when(setOfReports.size()).thenReturn(FRAUD_REPORT_NUMBER);
        BDDMockito.given(modelService.create(FraudReportModel.class)).willReturn(fraudReportModel);
        Mockito.when(orderModel.getFraudReports()).thenReturn(setOfReports);
        Mockito.when(orderModel.getCode()).thenReturn(ORDER_CODE);
        strategy.createFraudReport(response, orderModel, FraudStatus.CHECK);
        Assert.assertEquals("admin", fraudReportModel.getProvider());
    }



}

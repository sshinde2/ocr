/**
 * 
 */
package au.com.target.tgtfulfilment;

/**
 * Reason that a consignment is sent to warehouse
 * 
 */
public enum ConsignmentSentToWarehouseReason
{
    NEW("N"), UPDATED("U"), CANCELLED("C");

    private String code;

    private ConsignmentSentToWarehouseReason(final String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtfulfilment.service.SendToWarehouseService;


/**
 * Send new order to warehouse
 * 
 */
public class ExtractNewOrderToWarehouseAction extends AbstractProceduralAction<OrderProcessModel> {

    private SendToWarehouseService sendToWarehouseService;

    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process);
        final OrderModel order = process.getOrder();
        Assert.notNull(order);

        sendToWarehouseService.sendNewOrder(order);
    }

    /**
     * @param sendToWarehouseService
     *            the sendToWarehouseService to set
     */
    @Required
    public void setSendToWarehouseService(final SendToWarehouseService sendToWarehouseService) {
        this.sendToWarehouseService = sendToWarehouseService;
    }


}

/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;

import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;


/**
 * @author bhuang3
 *
 */
public class ForceDefaultWarehouseToOnlinePOSDecisionAction extends AbstractSimpleDecisionAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(ForceDefaultWarehouseToOnlinePOSDecisionAction.class);

    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Override
    public Transition executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process, "Order ProcessModel cannot be null");
        final ConsignmentModel currentConsignment = orderProcessParameterHelper.getConsignment(process);
        Assert.notNull(currentConsignment, "current consignment cannot be null");
        final WarehouseModel warehouse = currentConsignment.getWarehouse();
        Assert.notNull(warehouse, "warehouse cannot be null");
        if (BooleanUtils.isTrue(warehouse.getDefault())) {
            LOG.info("ForceDefaultWarehouseToOnlinePOS: Received the fastline consignment in instore picked process,"
                    + "and will adjust stock on fastline warehouse and skip the interStoreTransferAction");
            return Transition.OK;
        }
        else {
            return Transition.NOK;
        }
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.exceptions.InventoryAdjustmentFailedException;
import au.com.target.tgtfulfilment.inventory.dto.InventoryAdjustmentData;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;
import au.com.target.tgtfulfilment.stock.client.InventoryAdjustmentClient;


/**
 * @author pthoma20
 *
 */
public class InventoryAdjustmentAction extends AbstractProceduralAction<OrderProcessModel> {


    private static final Logger LOG = Logger.getLogger(InventoryAdjustmentAction.class);
    private static final String PREFIX = "Inventory-Adjustment: ";
    private static final int DEFAULT_TO_STORE = 5599;

    private OrderProcessParameterHelper orderProcessParameterHelper;
    private InventoryAdjustmentClient inventoryAdjustmentClient;
    private TargetWarehouseService targetWarehouseService;
    private TargetFeatureSwitchService featureSwitchService;
    private TargetStoreConsignmentService targetStoreConsignmentService;
    private TargetSharedConfigService targetSharedConfigService;


    @Override
    public void executeAction(final OrderProcessModel orderProcess) throws RetryLaterException, Exception {

        if (!featureSwitchService.isFeatureEnabled("inventoryAdjustmentUsingWM")) {
            return;
        }

        Assert.notNull(orderProcess, "order process can not be null");

        final ConsignmentModel consignment = orderProcessParameterHelper.getConsignment(orderProcess);
        Assert.notNull(consignment, "consignment can not be null");

        final List<InventoryAdjustmentData> inventoryAdjustmentDataList = new ArrayList<>();
        final WarehouseModel warehouse = consignment.getWarehouse();
        final TargetPointOfServiceModel fromStore = targetStoreConsignmentService
                .getAssignedStoreForWarehouse(warehouse);

        if (fromStore == null) {
            final String message = PREFIX + "From Store Not Found ";
            LOG.error(message);
            throw new InventoryAdjustmentFailedException(message);
        }


        TargetPointOfServiceModel toStore = targetStoreConsignmentService
                .getAssignedStoreForWarehouse(targetWarehouseService.getDefaultOnlineWarehouse());

        if (toStore == null) {
            final String message = PREFIX + "To Store Not Found ";
            LOG.error(message);
            toStore = new TargetPointOfServiceModel();
            toStore.setStoreNumber(Integer.valueOf(targetSharedConfigService
                    .getInt(TgtCoreConstants.Config.CONSIGNMENT_DEFAULT_TO_STORE, DEFAULT_TO_STORE)));
        }

        final Set<ConsignmentEntryModel> consignmentEntries = consignment.getConsignmentEntries();
        for (final ConsignmentEntryModel consignmentEntry : consignmentEntries) {
            if (null != consignmentEntry.getShippedQuantity() && consignmentEntry.getShippedQuantity().intValue() > 0) {
                inventoryAdjustmentDataList
                        .add(getInventoryAdjustmentData(consignmentEntry.getOrderEntry().getProduct().getCode(),
                                consignmentEntry.getShippedQuantity().longValue() * -1, fromStore));
                inventoryAdjustmentDataList.add(
                        getInventoryAdjustmentData(consignmentEntry.getOrderEntry().getProduct().getCode(),
                                consignmentEntry.getShippedQuantity().longValue(), toStore));
            }
        }

        if (!inventoryAdjustmentClient.sendInventoryAdjustment(inventoryAdjustmentDataList)) {
            final String message = PREFIX + "Failed to send inventory Adjustment ";
            LOG.error(message);
            throw new InventoryAdjustmentFailedException(message);
        }

    }

    private InventoryAdjustmentData getInventoryAdjustmentData(final String productCode, final long quantity,
            final TargetPointOfServiceModel store) {
        final InventoryAdjustmentData inventoryAdjustmentData = new InventoryAdjustmentData();
        inventoryAdjustmentData.setCode(productCode);
        inventoryAdjustmentData.setAdjustmentQty(String.valueOf(quantity));
        inventoryAdjustmentData.setStoreNum(store.getStoreNumber().toString());
        return inventoryAdjustmentData;

    }



    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }

    /**
     * @param inventoryAdjustmentClient
     *            the inventoryAdjustmentClient to set
     */
    @Required
    public void setInventoryAdjustmentClient(final InventoryAdjustmentClient inventoryAdjustmentClient) {
        this.inventoryAdjustmentClient = inventoryAdjustmentClient;
    }

    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */
    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }

    /**
     * @param featureSwitchService
     *            the featureSwitchService to set
     */
    @Required
    public void setFeatureSwitchService(final TargetFeatureSwitchService featureSwitchService) {
        this.featureSwitchService = featureSwitchService;
    }


    /**
     * @param targetStoreConsignmentService
     *            the targetStoreConsignmentService to set
     */
    @Required
    public void setTargetStoreConsignmentService(final TargetStoreConsignmentService targetStoreConsignmentService) {
        this.targetStoreConsignmentService = targetStoreConsignmentService;
    }

    /**
     * @param targetSharedConfigService
     *            the targetSharedConfigService to set
     */
    @Required
    public void setTargetSharedConfigService(final TargetSharedConfigService targetSharedConfigService) {
        this.targetSharedConfigService = targetSharedConfigService;
    }


}

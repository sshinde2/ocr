/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgtcore.postcode.service.TargetPostCodeService;


/**
 * Log the Unallocated postcode's
 *
 */
public class LogUnknownPostCodeAction extends AbstractProceduralAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(LogUnknownPostCodeAction.class);

    private TargetPostCodeService targetPostCodeService;

    /* (non-Javadoc)
     * @see de.hybris.platform.processengine.action.AbstractProceduralAction#executeAction(de.hybris.platform.processengine.model.BusinessProcessModel)
     */
    @Override
    public void executeAction(final OrderProcessModel process) {
        try {
            final OrderModel orderModel = process.getOrder();
            if (orderModel.getDeliveryMode() instanceof TargetZoneDeliveryModeModel) {
                final TargetZoneDeliveryModeModel deliveryMode = (TargetZoneDeliveryModeModel)orderModel
                        .getDeliveryMode();
                if (BooleanUtils.isFalse(deliveryMode.getIsDeliveryToStore())
                        && orderModel.getDeliveryAddress() != null) {
                    final String postCode = orderModel.getDeliveryAddress().getPostalcode();
                    final PostCodeModel postCodeModel = targetPostCodeService
                            .getPostCode(postCode);
                    if (postCodeModel == null || CollectionUtils.isEmpty(postCodeModel.getPostCodeGroups())) {
                        LOG.info("NotifyingPostCodeEvent:action=NotifyUnknownPostCode, reason=UnknownPostcodeInHomeDeliveryAddress, order="
                                + orderModel.getCode()
                                + ", deliveryMode="
                                + deliveryMode.getCode()
                                + ", postcode="
                                + postCode + ", timestamp=" + orderModel.getDate());
                    }
                }
            }
        }
        catch (final Exception e) {
            LOG.info("Something went wrong while running LogUnknownPostCodeAction, Error:" + e);
        }
    }

    /**
     * @param targetPostCodeService
     *            the targetPostCodeService to set
     */
    @Required
    public void setTargetPostCodeService(final TargetPostCodeService targetPostCodeService) {
        this.targetPostCodeService = targetPostCodeService;
    }


}

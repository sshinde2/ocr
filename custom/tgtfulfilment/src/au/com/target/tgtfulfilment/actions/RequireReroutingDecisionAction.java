/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;


public class RequireReroutingDecisionAction extends AbstractAction<OrderProcessModel> {

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private TargetConsignmentService targetConsignmentService;

    private OrderProcessParameterHelper orderProcessParameterHelper;

    private enum Transition {
        TRUE, FALSE;

        public static Set<String> getStringValues() {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values()) {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    public String execute(final OrderProcessModel process) throws RetryLaterException, Exception {

        final boolean isfalconEnabled = targetFeatureSwitchService
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON);
        if (isfalconEnabled) {
            final ConsignmentModel consignment = orderProcessParameterHelper.getConsignment(process);
            if (targetConsignmentService.isConsignmentToDefaultWarehouse(consignment)
                    || targetConsignmentService.isAFluentConsignment(consignment)) {
                return Transition.FALSE.toString();
            }
            return Transition.TRUE.toString();
        }
        return Transition.FALSE.toString();
    }

    @Override
    public Set<String> getTransitions() {
        return Transition.getStringValues();
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }


    /**
     * @return the targetConsignmentService
     */
    public TargetConsignmentService getTargetConsignmentService() {
        return targetConsignmentService;
    }

    /**
     * @param targetConsignmentService
     *            the targetConsignmentService to set
     */
    public void setTargetConsignmentService(final TargetConsignmentService targetConsignmentService) {
        this.targetConsignmentService = targetConsignmentService;
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }
}

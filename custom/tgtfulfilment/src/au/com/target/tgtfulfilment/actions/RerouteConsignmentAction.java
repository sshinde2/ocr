package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.ConsignmentCreationException;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.task.RetryLaterException;

import java.text.MessageFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetStoreStockCheckException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtfulfilment.service.SendToWarehouseService;
import au.com.target.tgtfulfilment.service.impl.ConsignmentReroutingService;


/**
 * Re-route consignment if store rejects the order.
 * 
 */
public class RerouteConsignmentAction extends RetryActionWithSpringConfig<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(RerouteConsignmentAction.class);

    private static final String ERR_WHILE_REROUTING_CONS = "ERROR-CONSIGNMENT-REROUTING: Error while re-routing consignment, consignmentCode={0}, orderCode={1}";

    private static final String ERR_CONS_TO_REROUTE_MISSING = "ERROR-CONSIGNMENT-MISSING-TO-REROUTE: No consignments found to reroute for orderCode={0}";

    private static final String INFO_SKIP_REROUTE = "INFO-CONSIGNMENT-REROUTING: Skip rerouting consignment, because of all items fulfulled,  consignmentCode={0}, orderCode={1}";

    private ConsignmentReroutingService consignmentReroutingService;

    private SendToWarehouseService sendToWarehouseService;

    private TargetFeatureSwitchService targetFeatureSwitchService;


    public enum Transition {
        OK;

        public static Set<String> getStringValues() {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values()) {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String executeInternal(final OrderProcessModel process) throws Exception {
        Assert.notNull(process, "OrderProcessModel cannot be null");
        // Get the consignment and update object out of the process parameters
        final ConsignmentModel consignment = getOrderProcessParameterHelper().getConsignment(process);
        Assert.notNull(consignment, "Consignment cannot be null");
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)) {
            if (consignmentReroutingService.isRequireRerouting(consignment)) {
                rerouteConsignment(consignment);
            }
            else {
                LOG.info(MessageFormat.format(INFO_SKIP_REROUTE, consignment.getCode(),
                        consignment.getOrder() != null ? consignment.getOrder()
                                .getCode() : StringUtils.EMPTY));
            }
        }
        else {
            rerouteConsignment(consignment);
        }

        return Transition.OK.toString();

    }

    private void rerouteConsignment(final ConsignmentModel consignment)
            throws ConsignmentCreationException, RetryLaterException {
        try {
            final List<ConsignmentModel> consignments = consignmentReroutingService.rerouteConsignment(consignment);
            if (CollectionUtils.isEmpty(consignments)) {
                throw new IllegalStateException(
                        MessageFormat.format(ERR_CONS_TO_REROUTE_MISSING, consignment.getOrder().getCode()));
            }
            sendToWarehouse(consignments);
        }
        catch (final TargetStoreStockCheckException ex) {
            final String message = MessageFormat.format(
                    "Retrying RerouteConsignmentAction  reason=stock_check_failed order={0}, consignment={1}",
                    consignment.getOrder().getCode(), consignment.getCode());
            LOG.error(message, ex);
            throw new RetryLaterException(message, ex);
        }
    }

    private void sendToWarehouse(final List<ConsignmentModel> consignments) {
        for (final ConsignmentModel cons : consignments) {
            try {
                sendToWarehouseService.sendConsignment(cons);
            }
            catch (final Exception e) {
                LOG.error(MessageFormat.format(ERR_WHILE_REROUTING_CONS, cons.getCode(), cons.getOrder().getCode()), e);
            }
        }
    }

    /**
     * @param sendToWarehouseService
     *            the sendToWarehouseService to set
     */
    @Required
    public void setSendToWarehouseService(final SendToWarehouseService sendToWarehouseService) {
        this.sendToWarehouseService = sendToWarehouseService;
    }

    /**
     * @param consignmentReroutingService
     *            the consignmentReroutingService to set
     */
    @Required
    public void setConsignmentReroutingService(final ConsignmentReroutingService consignmentReroutingService) {
        this.consignmentReroutingService = consignmentReroutingService;
    }


    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

}

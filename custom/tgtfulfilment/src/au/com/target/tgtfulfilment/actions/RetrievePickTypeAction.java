package au.com.target.tgtfulfilment.actions;

import static au.com.target.tgtbusproc.util.OrderProcessParameterHelper.PickTypeEnum;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;

import java.util.EnumSet;
import java.util.Set;

import org.apache.log4j.Logger;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import com.google.common.base.Functions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

/**
 * Actions that returns currently set "pick type" for consignment as a transaction.
 * In case if "pick type" is not set, it returns {@link #PICK_TYPE_NOT_SET}.
 */
public class RetrievePickTypeAction extends AbstractAction<OrderProcessModel> {

    /**
     * Constant return by this action in case of "pick type"
     * variable is not set for the current business process.
     */
    public static final String PICK_TYPE_NOT_SET = "NOT_SET";

    private static final Logger LOG = Logger.getLogger(RetrievePickTypeAction.class);


    @Override
    public String execute(final OrderProcessModel process) throws Exception {
        final PickTypeEnum pickType = getOrderProcessParameterHelper().getPickType(process);
        if (pickType == null) {
            LOG.warn("Pick type is not set, process #" + process.getCode());
            return PICK_TYPE_NOT_SET;
        }
        return pickType.toString();
    }

    @Override
    public Set<String> getTransitions() {
        return ImmutableSet.<String>builder()
                .addAll(Iterables.transform(
                        EnumSet.allOf(PickTypeEnum.class),
                        Functions.toStringFunction()))
                .add(PICK_TYPE_NOT_SET)
                .build();
    }

    /**
     * Returns the order process parameter helper.
     *
     * @return the order process parameter helper.
     */
    public OrderProcessParameterHelper getOrderProcessParameterHelper() {
        return (OrderProcessParameterHelper)getProcessParameterHelper();
    }

}

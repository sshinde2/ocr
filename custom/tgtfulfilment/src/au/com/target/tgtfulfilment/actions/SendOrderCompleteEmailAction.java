/**
 *
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.actions.SendEmailAction;

/**
 * @author ramsatish
 *
 */
public class SendOrderCompleteEmailAction extends SendEmailAction {

	private static final Logger LOG = Logger.getLogger(SendOrderCompleteEmailAction.class);

	@Override
	public Transition executeAction(final BusinessProcessModel process) throws RetryLaterException, Exception {
		Assert.notNull(process, "Process cannot be null");
		if (process instanceof OrderProcessModel) {
			final OrderModel order = ((OrderProcessModel) process).getOrder();
			if (order != null && order.getCncStoreNumber() == null) {

				LOG.info("Sending order complete email with tax invoice for order"+ order.getCode());
				super.executeAction(process);
			}
		}

		return Transition.OK;

	}

}

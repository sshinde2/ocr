package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.ConsignmentCreationException;
import de.hybris.platform.ordersplitting.OrderSplittingService;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.actions.RetryActionWithSpringConfig;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.exception.TargetStoreStockCheckException;


/**
 * Split the order/order entries into consignments
 */
public class SplitOrderAction extends RetryActionWithSpringConfig<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(SplitOrderAction.class);

    private OrderSplittingService orderSplittingService;

    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    public enum Transition {
        OK;

        public static Set<String> getStringValues() {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values()) {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }

    @Override
    protected String executeInternal(final OrderProcessModel process)
            throws RetryLaterException, ConsignmentCreationException {
        Assert.notNull(process, "OrderProcess cannot be null");

        final OrderModel orderModel = process.getOrder();
        Assert.notNull(orderModel, "Order cannot be null");
        assignDeliveryModesToOrderEntries(orderModel);

        try {
            final List<ConsignmentModel> consignments = orderSplittingService.splitOrderForConsignment(
                    orderModel, orderModel.getEntries());
            if (LOG.isDebugEnabled()) {
                LOG.debug("Splitting order into " + consignments.size() + " consignments.");
            }
        }
        catch (final TargetStoreStockCheckException ex) {
            final String message = "Retrying SplitOrderAction  reason=stock_check_failed  order="
                    + orderModel.getCode();
            throw new RetryLaterException(message, ex);
        }


        setOrderStatus(process.getOrder(), OrderStatus.INPROGRESS);

        return Transition.OK.toString();
    }

    /**
     * Assign delivery modes to order entries based on the product.
     * 
     * @param orderModel
     */
    private void assignDeliveryModesToOrderEntries(final OrderModel orderModel) {
        final List<AbstractOrderEntryModel> orderEntries = orderModel.getEntries();
        if (CollectionUtils.isNotEmpty(orderEntries)) {
            for (final AbstractOrderEntryModel orderEntry : orderEntries) {
                final ProductModel product = orderEntry.getProduct();
                if (product != null) {
                    if (targetDeliveryModeHelper.hasOnlyDigitalDeliveryMode(product)) {
                        final TargetZoneDeliveryModeModel digitalDeliveryMode = targetDeliveryModeHelper
                                .getDigitalDeliveryMode(product);
                        orderEntry.setDeliveryMode(digitalDeliveryMode);
                        orderEntry.setDeliveryAddress(null);
                    }
                    else {
                        orderEntry.setDeliveryMode(orderModel.getDeliveryMode());
                        orderEntry.setDeliveryAddress(orderModel.getDeliveryAddress());
                    }
                    getModelService().save(orderEntry);
                }
            }
        }
    }

    @Required
    public void setOrderSplittingService(final OrderSplittingService orderSplittingService) {
        this.orderSplittingService = orderSplittingService;
    }

    /**
     * @param targetDeliveryModeHelper
     *            the targetDeliveryModeHelper to set
     */
    @Required
    public void setTargetDeliveryModeHelper(final TargetDeliveryModeHelper targetDeliveryModeHelper) {
        this.targetDeliveryModeHelper = targetDeliveryModeHelper;
    }


}

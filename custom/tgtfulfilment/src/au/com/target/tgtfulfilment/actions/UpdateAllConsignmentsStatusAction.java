/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;

import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;


/**
 * Updates the status of all consignments belonging to the order that is being processed.
 */
public class UpdateAllConsignmentsStatusAction extends AbstractProceduralAction<OrderProcessModel> {

    private ConsignmentStatus consignmentStatus;
    private List<ConsignmentStatus> onlyIfPreviousStatusList = null;

    @Override
    public void executeAction(final OrderProcessModel process) throws Exception {
        Assert.notNull(process);
        Assert.notNull(process.getOrder());

        final Set<ConsignmentModel> consignments = process.getOrder().getConsignments();
        if (consignments != null) {
            for (final ConsignmentModel consignment : consignments) {
                if (CollectionUtils.isEmpty(onlyIfPreviousStatusList)
                        || onlyIfPreviousStatusList.contains(consignment.getStatus())) {
                    consignment.setStatus(getConsignmentStatus());
                }
            }
            getModelService().saveAll(consignments);
        }
    }

    /**
     * Returns the status to update all consignments with.
     * 
     * @return the consignment status
     */
    public ConsignmentStatus getConsignmentStatus() {
        return consignmentStatus;
    }

    /**
     * Sets the status to be set for all order consignments in this business process.
     * 
     * @param consignmentStatus
     *            the consignment status to test
     */
    @Required
    public void setConsignmentStatus(final ConsignmentStatus consignmentStatus) {
        this.consignmentStatus = consignmentStatus;
    }

    /**
     * If the status is only to be set if the consignment has an existing status, set this. Defaults to null, so not
     * setting will result in the status always updating.
     * 
     * @param onlyIfPreviousStatusList
     *            the onlyIfPreviousStatusList to set, null for no previous status restriction
     */
    public void setOnlyIfPreviousStatusList(final List<ConsignmentStatus> onlyIfPreviousStatusList) {
        this.onlyIfPreviousStatusList = onlyIfPreviousStatusList;
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.Date;

import org.springframework.util.Assert;


/**
 * @author bhuang3
 * 
 */
public class UpdateClickAndCollectNotificationTimeAction extends AbstractProceduralAction<OrderProcessModel> {

    @Override
    public void executeAction(final OrderProcessModel processModel) throws RetryLaterException, Exception {
        Assert.notNull(processModel, "Orderprocess cannot be null");
        final OrderModel orderModel = processModel.getOrder();
        Assert.notNull(orderModel, "orderModel cannot be null");
        orderModel.setCncLastNotification(new Date());
        getModelService().save(orderModel);
    }


}

/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;


public class UpdateConsignmentStatusAction extends AbstractProceduralAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(UpdateConsignmentStatusAction.class);
    private static final String CONSIGNMENT_LOGGING_PREFIX = "UpdateConsignmentStatusAction - ChangeStatus :";

    private ConsignmentStatus consignmentStatus;
    private OrderProcessParameterHelper orderProcessParameterHelper;
    private List<ConsignmentStatus> onlyIfPreviousStatusList = null;


    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process);

        // Get the consignment out of the process parameters
        final ConsignmentModel consignment = orderProcessParameterHelper.getConsignment(process);
        Assert.notNull(consignment);

        final ConsignmentStatus existingConsignmentStatus = consignment.getStatus();

        final String logMessageStem = CONSIGNMENT_LOGGING_PREFIX
                + " consignmentCode=" + consignment.getCode()
                + " originalStatus=" + existingConsignmentStatus
                + " newStatus=" + consignmentStatus
                + " message=";

        if (consignmentStatus.equals(existingConsignmentStatus)) {

            LOG.info(logMessageStem + "already in status");
        }
        else if (CollectionUtils.isEmpty(onlyIfPreviousStatusList)
                || onlyIfPreviousStatusList.contains(consignment.getStatus())) {

            consignment.setStatus(consignmentStatus);
            if (consignmentStatus == ConsignmentStatus.CANCELLED) {
                this.updateConsignmentShippedQtyForCancellation(consignment);
            }
            save(consignment);

            LOG.info(logMessageStem + "status updated");
        }
        else {
            LOG.warn(logMessageStem + "incorrect originalStatus");
        }
    }


    private void updateConsignmentShippedQtyForCancellation(final ConsignmentModel consignment) {
        if (CollectionUtils.isNotEmpty(consignment.getConsignmentEntries())) {
            for (final ConsignmentEntryModel entry : consignment.getConsignmentEntries()) {
                entry.setShippedQuantity(Long.valueOf(0l));
            }
        }
    }

    /**
     * @param consignmentStatus
     *            the consignmentStatus to set
     */
    @Required
    public void setConsignmentStatus(final ConsignmentStatus consignmentStatus) {
        this.consignmentStatus = consignmentStatus;
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }

    /**
     * If the status is only to be set if the consignment has an existing status, set this. Defaults to null, so not
     * setting will result in the status always updating.
     * 
     * @param onlyIfPreviousStatusList
     *            the onlyIfPreviousStatusList to set, null for no previous status restriction
     */
    public void setOnlyIfPreviousStatusList(final List<ConsignmentStatus> onlyIfPreviousStatusList) {
        this.onlyIfPreviousStatusList = onlyIfPreviousStatusList;
    }

}

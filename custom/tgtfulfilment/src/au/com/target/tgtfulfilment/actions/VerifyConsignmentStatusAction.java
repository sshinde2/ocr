package au.com.target.tgtfulfilment.actions;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;

import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;

/**
 * Verifies that current consignment status is equal to the {@link #getConsignmentStatus()}.
 */
public class VerifyConsignmentStatusAction extends AbstractSimpleDecisionAction<OrderProcessModel> {

    private ConsignmentStatus consignmentStatus;

    @Override
    public Transition executeAction(final OrderProcessModel process) throws Exception {
        final ConsignmentModel consignment = getOrderProcessParameterHelper().getConsignment(process);
        Assert.notNull(consignment);

        return consignment.getStatus() == getConsignmentStatus() ? Transition.OK : Transition.NOK;
    }

    /**
     * Returns the parameter helper extension.
     *
     * @return the parameter helper
     */
    public OrderProcessParameterHelper getOrderProcessParameterHelper() {
        return (OrderProcessParameterHelper)getProcessParameterHelper();
    }

    /**
     * Returns the consignment status that is used for check.
     *
     * @return the consignment status
     */
    public ConsignmentStatus getConsignmentStatus() {
        return consignmentStatus;
    }

    /**
     * Sets the consignment status to be used for check.
     *
     * @param consignmentStatus the consignment status
     */
    public void setConsignmentStatus(final ConsignmentStatus consignmentStatus) {
        this.consignmentStatus = consignmentStatus;
    }
}
/**
 * 
 */
package au.com.target.tgtfulfilment.attributehandler.impl;

import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.model.TargetManifestModel;


/**
 * Attribute handler to calculate total number of parcels in a manifest
 * 
 * @author Vivek
 *
 */
public class ManifestTotalParcelsAttributeHandler extends AbstractDynamicAttributeHandler<Integer, TargetManifestModel> {

    @Override
    public Integer get(final TargetManifestModel manifest) {
        int parcels = 0;
        if (null != manifest) {
            final Set<TargetConsignmentModel> consignments = manifest.getConsignments();
            if (CollectionUtils.isNotEmpty(consignments)) {
                for (final TargetConsignmentModel consignment : consignments) {
                    parcels += null == consignment.getParcelCount() ? 0 : consignment.getParcelCount().intValue();
                }
            }
        }
        return Integer.valueOf(parcels);
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.attributehandler.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;


/**
 * @author Rahul
 *
 */
public class OfcOrderTypeAttributeHandler extends
        AbstractDynamicAttributeHandler<OfcOrderType, TargetConsignmentModel> {

    private TargetStoreConsignmentService targetStoreConsignmentService;

    @Override
    public OfcOrderType get(final TargetConsignmentModel consignment) {

        Assert.notNull(consignment, "consignment can't be null");

        // consignment not fulfilled instore
        if (!targetStoreConsignmentService.isConsignmentAssignedToAnyStore(consignment)) {
            return null;
        }

        final AbstractOrderModel order = consignment.getOrder();
        Assert.notNull(order, "order can't be null");

        final DeliveryModeModel deliveryMode = order.getDeliveryMode();
        Assert.notNull(deliveryMode, "delivery mode can't be null");
        Assert.isInstanceOf(TargetZoneDeliveryModeModel.class, deliveryMode,
                "deliveryMode must be instance of TargetZoneDeliveryModeModel");

        final Integer orderStoreNumber = order.getCncStoreNumber();
        final TargetZoneDeliveryModeModel targetDeliveryMode = (TargetZoneDeliveryModeModel)deliveryMode;

        // if not cnc
        if (null == orderStoreNumber || BooleanUtils.isFalse(targetDeliveryMode.getIsDeliveryToStore())) {
            return OfcOrderType.CUSTOMER_DELIVERY;
        }

        // same store cnc
        if (targetStoreConsignmentService.isConsignmentAssignedToStore(consignment, orderStoreNumber.intValue())) {
            return OfcOrderType.INSTORE_PICKUP;
        }

        return OfcOrderType.INTERSTORE_DELIVERY;
    }

    /**
     * @param targetStoreConsignmentService
     *            the targetStoreConsignmentService to set
     */
    @Required
    public void setTargetStoreConsignmentService(final TargetStoreConsignmentService targetStoreConsignmentService) {
        this.targetStoreConsignmentService = targetStoreConsignmentService;
    }

}

/**
 *
 */
package au.com.target.tgtfulfilment.client;

import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseRequest;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseResponse;


/**
 * Rest client to poll warehouse for status of shipments
 *
 * @author gsing236
 *
 */
public interface PollConsignmentWarehouseRestClient {

    /**
     * poll consignment numbers
     *
     * @param request
     *            - {@link PollWarehouseRequest}
     * @return {@link PollWarehouseResponse}
     */
    PollWarehouseResponse pollConsignments(PollWarehouseRequest request);
}

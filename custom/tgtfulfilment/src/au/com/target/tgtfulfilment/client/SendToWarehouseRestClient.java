/**
 * 
 */
package au.com.target.tgtfulfilment.client;

import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseRequest;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseResponse;


/**
 * SendToWarehouseRestClient interface
 * 
 * @author jjayawa1
 *
 */
public interface SendToWarehouseRestClient {

    /**
     * Sends the consignment information to a service.
     * 
     * @param sendToWarehouseRequest
     * @return BaseResponseDTO
     */
    SendToWarehouseResponse sendConsignment(SendToWarehouseRequest sendToWarehouseRequest);
}

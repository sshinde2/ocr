/**
 * 
 */
package au.com.target.tgtfulfilment.client.impl;

import au.com.target.tgtfulfilment.client.SendToWarehouseRestClient;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseRequest;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseResponse;


/**
 * Default implementation for SendToWarehouseRestClient
 * 
 * @author jjayawa1
 *
 */
public class DefaultSendToWarehouseRestClient implements SendToWarehouseRestClient {

    /**
     * {@inheritDoc}
     */
    @Override
    public SendToWarehouseResponse sendConsignment(final SendToWarehouseRequest sendToWarehouseRequest) {
        return SendToWarehouseResponse.getSuccessRespose();
    }

}

/**
 * 
 */
package au.com.target.tgtfulfilment.consignment.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.product.dao.TargetProductSearchServiceDao;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.stock.TargetFulfilmentStockService;


public class ReservedQtyReconcilationJob extends AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(ReservedQtyReconcilationJob.class);

    private TargetProductSearchServiceDao targetProductSearchServiceDao;
    private TargetWarehouseService targetWarehouseService;
    private TargetFulfilmentStockService targetFulfilmentStockService;

    @Override
    public PerformResult perform(final CronJobModel cronJob) {
        LOG.info("Start ReservedQuantityReconciliationJob");

        int startCount = 0;
        final String query = getQuery();
        final WarehouseModel warehouse = targetWarehouseService.getDefaultOnlineWarehouse();
        List<AbstractTargetVariantProductModel> productVariants = targetProductSearchServiceDao.getAllProductVariants(
                query, startCount);
        while (CollectionUtils.isNotEmpty(productVariants)) {
            LOG.info("ReservedQuantityReconciliationJob found [" + productVariants.size()
                    + "] product variants in Staged catalog.");

            for (final AbstractTargetVariantProductModel variant : productVariants) {
                try {
                    targetFulfilmentStockService.reconcileStockLevelReservedQuantity(variant, warehouse);
                }
                catch (final Exception e) {
                    LOG.error(MessageFormat.format("ReconcilingStock : Failed for variant={0} for the warehouse={1}",
                            variant.getCode(), warehouse.getCode()), e);
                }
            }
            startCount = startCount + productVariants.size();
            productVariants = targetProductSearchServiceDao.getAllProductVariants(query, startCount);
        }
        LOG.info("Done ReservedQuantityReconciliationJob");
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    private String getQuery() {
        final StringBuilder query = new StringBuilder();
        query.append("SELECT {").append(Item.PK).append("} ");
        query.append("FROM {").append(AbstractTargetVariantProductModel._TYPECODE).append("} ");
        query.append("WHERE {").append(AbstractTargetVariantProductModel.CATALOGVERSION).append("} = ?catalogVersion");
        query.append(" ORDER BY  " + "{").append(Item.PK).append("} ");
        return query.toString();
    }

    /**
     * @param targetFulfilmentStockService
     *            the targetFulfilmentStockService to set
     */
    @Required
    public void setTargetFulfilmentStockService(final TargetFulfilmentStockService targetFulfilmentStockService) {
        this.targetFulfilmentStockService = targetFulfilmentStockService;
    }

    /**
     * @param targetProductSearchServiceDao
     *            the targetProductSearchServiceDao to set
     */
    @Required
    public void setTargetProductSearchServiceDao(final TargetProductSearchServiceDao targetProductSearchServiceDao) {
        this.targetProductSearchServiceDao = targetProductSearchServiceDao;
    }

    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */
    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }
}

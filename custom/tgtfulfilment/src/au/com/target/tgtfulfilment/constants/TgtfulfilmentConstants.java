/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtfulfilment.constants;

@SuppressWarnings("deprecation")
public final class TgtfulfilmentConstants extends GeneratedTgtfulfilmentConstants {
    public static final String EXTENSIONNAME = "tgtfulfilment";

    public static final String EVENT_INVOICING_FINISHED_SUFFIX = "_INVOICING_FINISHED";

    // only for testing
    public static final String CONSIGNMENT_COUNTER = "CONSIGNMENT_COUNTER";
    public static final String PARENT_PROCESS = "PARENT_PROCESS";

    public static final String FEATURE_WEBMETHOD_ORDEREXTRACT_PRODUCT_ID = "incomm.orderextract.productid";

    public static final String GIFTCARD_SHIPPING_METHOD_SMALL = "AusPostSmallNormalRegistered";

    public static final String GIFTCARD_SHIPPING_METHOD_LARGE = "AusPostLargeRegularRegistered";

    public static final String GIFTCARD_SHIPPING_METHOD_EMAIL = "Email";

    public interface Config {
        String GIFTCARDS_SMALL_SHIPMENT_LIMIT = "giftcards.shipping.small.limit";
        String STORE_DISTANCE_LIMIT = "fulfilment.store.distance.limit";
    }

    private TgtfulfilmentConstants() {
        //prevent construction
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.converters;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfulfilment.dto.ConsignmentParcelDTO;
import au.com.target.tgtfulfilment.model.ConsignmentParcelModel;


/**
 * Converter to convert parcel information to a parcel model
 * 
 * @author jjayawa1
 *
 */
public class ConsignmentParcelModelConverter implements Converter<ConsignmentParcelDTO, ConsignmentParcelModel> {

    private ModelService modelService;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.dto.converter.Converter#convert(java.lang.Object)
     */
    @Override
    public ConsignmentParcelModel convert(final ConsignmentParcelDTO source) throws ConversionException {
        final ConsignmentParcelModel consignmentParcel = modelService.create(ConsignmentParcelModel.class);
        return convert(source, consignmentParcel);
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.dto.converter.Converter#convert(java.lang.Object, java.lang.Object)
     */
    @Override
    public ConsignmentParcelModel convert(final ConsignmentParcelDTO source, final ConsignmentParcelModel target)
            throws ConversionException {
        target.setHeight(Double.valueOf(source.getHeight()));
        target.setLength(Double.valueOf(source.getLength()));
        target.setWidth(Double.valueOf(source.getWidth()));
        target.setActualWeight(Double.valueOf(source.getWeight()));

        return target;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

}

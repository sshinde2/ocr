/**
 * 
 */
package au.com.target.tgtfulfilment.dao;

import java.util.List;

import au.com.target.tgtfulfilment.model.TargetStandardParcelModel;


/**
 * @author smishra1
 *
 */
public interface TargetStandardParcelDao {
    /**
     * Gets All the available Standard Parcel details.
     * 
     * @return List of All Standard Parcel details
     */
    List<TargetStandardParcelModel> getEnabledTargetStandardParcels();
}

package au.com.target.tgtfulfilment.dao.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.dbformatter.TargetDBSpecificQueryFactory;
import au.com.target.tgtcore.dbformatter.TargetDBSpecificQueryFormatter;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.util.TargetServicesUtil;
import au.com.target.tgtfulfilment.dao.TargetConsignmentDao;
import au.com.target.tgtfulfilment.dao.util.TargetFulfilmentDaoUtil;
import au.com.target.tgtfulfilment.dto.TargetConsignmentPageResult;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;
import au.com.target.tgtfulfilment.warehouseservices.TargetFulfilmentWarehouseService;


/**
 * Default Implementation of {@link TargetConsignmentDao}
 */

/**
 * @author smudumba
 *
 */
public class TargetConsignmentDaoImpl extends DefaultGenericDao<TargetConsignmentModel>
        implements TargetConsignmentDao {

    private static final Logger LOG = Logger.getLogger(TargetConsignmentDaoImpl.class);

    private static final int DEFAULT_RECS_PER_PAGE = 20;

    private static final String FIND_CONSIGNMENT_BY_CUTOFF_TIME = "SELECT {con." + TargetConsignmentModel.PK
            + "} FROM { pos2warehouserel AS pw JOIN " + TargetPointOfServiceModel._TYPECODE + " AS tpos ON {tpos."
            + TargetPointOfServiceModel.PK + "} = {pw.source} JOIN " + TargetConsignmentModel._TYPECODE
            + " AS con ON {con." + TargetConsignmentModel.WAREHOUSE + "} = {pw.target} JOIN " + OrderModel._TYPECODE
            + " as o ON {o." + OrderModel.PK + "} = {con." + TargetConsignmentModel.ORDER + "}}" + " WHERE {tpos."
            + TargetPointOfServiceModel.STORENUMBER + "} = ?storeNumber AND ({o." + OrderModel.CNCSTORENUMBER
            + "} IS NULL OR {o." + OrderModel.CNCSTORENUMBER + "} != ?storeNumber) AND {con."
            + TargetConsignmentModel.CREATIONTIME + "} > ?filterDate";

    private static final String FIND_CONSIGNMENT_BY_CODE_QUERY = "SELECT {" + ItemModel.PK + "} FROM {"
            + TargetConsignmentModel._TYPECODE + "} WHERE {" + TargetConsignmentModel.CODE + "} = ?consignmentCode";

    private static final String CONSIGNMENTS_WAITING_FOR_SHIP_CONFIRM = "SELECT {" + ItemModel.PK + "} FROM {"
            + TargetConsignmentModel._TYPECODE + "} WHERE {" + TargetConsignmentModel.SHIPADVICESENTDATE
            + "} < ?shipConfirmWaitLimit AND {" + TargetConsignmentModel.STATUS + "} != ?status";


    private static final String FIND_CONSIGNMENT_BY_CODES_QUERY = "SELECT {" + ItemModel.PK + "} FROM {"
            + TargetConsignmentModel._TYPECODE + "} WHERE {" + TargetConsignmentModel.CODE
            + "} IN (?consignmentCodes)";

    private static final String FIND_CONSIGNMENTS_BY_STATUS_FOR_ALL_STORE = "SELECT {con." + ItemModel.PK
            + "} FROM  { pos2warehouserel AS pw join " + TargetPointOfServiceModel._TYPECODE + " AS tpos on {tpos."
            + TargetPointOfServiceModel.PK + "} = {pw.source}  join " + TargetConsignmentModel._TYPECODE
            + " AS con on {con." + TargetConsignmentModel.WAREHOUSE + "} = {pw.target}  } "
            + "WHERE {con:" + TargetConsignmentModel.STATUS + "} =?status  AND {tpos. "
            + TargetPointOfServiceModel.PK + "} IS NOT NULL ";

    private static final String FIND_CONSIGNMENTS_BY_STATUS_FOR_STORES_STEM = "SELECT {con." + ItemModel.PK
            + "} FROM { pos2warehouserel AS pw join " + TargetPointOfServiceModel._TYPECODE + " AS tpos on {tpos."
            + TargetPointOfServiceModel.PK + "} = {pw.source} join " + TargetConsignmentModel._TYPECODE
            + " AS con on {con." + TargetConsignmentModel.WAREHOUSE + "} = {pw.target} join " + AddressModel._TYPECODE
            + " AS addr on {addr.pk} = {tpos.address} } WHERE {con:" + TargetConsignmentModel.STATUS
            + "} IN (?statuses)  AND {tpos." + TargetPointOfServiceModel.PK + "} IS NOT NULL";

    private static final String FIND_CONSIGNMENTS_BY_STATUS_FOR_STORES_IN_STATE = FIND_CONSIGNMENTS_BY_STATUS_FOR_STORES_STEM
            + " AND {addr.district} IN (?includedStates)";

    private static final String FIND_CONSIGNMENTS_BY_STATUS_FOR_STORES_NOT_IN_STATE = FIND_CONSIGNMENTS_BY_STATUS_FOR_STORES_STEM
            + " AND {addr.district} NOT IN (?excludedStates)";

    private static final String FIND_CONSIGNMENTS_BY_STATUS_AND_WAREHOUSE = "SELECT {" + TargetConsignmentModel.PK
            + "} FROM {" + TargetConsignmentModel._TYPECODE + " as cons JOIN " + TargetCarrierModel._TYPECODE
            + " as car ON {car:"
            + TargetCarrierModel.PK + "} = {cons:" + TargetConsignmentModel.TARGETCARRIER + "}} WHERE {"
            + TargetConsignmentModel.STATUS
            + "} = ?status AND {" + TargetConsignmentModel.WAREHOUSE + "} = ?warehouse AND   @@DATEDIFFQUERY@@ > {car."
            + TargetCarrierModel.RESENDORDEXTRACTTIMEOUT + "}";

    private static final String FIND_CONSIGNMENTS_BY_STATUS_AND_EXTERNAL_WAREHOUSE = "SELECT {"
            + TargetConsignmentModel.PK
            + "} FROM {" + TargetConsignmentModel._TYPECODE + " as cons JOIN " + TargetCarrierModel._TYPECODE
            + " as car ON {car:"
            + TargetCarrierModel.PK + "} = {cons:" + TargetConsignmentModel.TARGETCARRIER + "} JOIN "
            + WarehouseModel._TYPECODE + " as war ON {war: " + WarehouseModel.PK + "} = {cons:"
            + TargetConsignmentModel.WAREHOUSE + "}} WHERE {"
            + TargetConsignmentModel.STATUS
            + "} = ?status AND {war." + WarehouseModel.EXTERNALWAREHOUSE
            + "} = ?external AND   @@DATEDIFFQUERY@@ > {car."
            + TargetCarrierModel.RESENDORDEXTRACTTIMEOUT + "}";


    /**
     * Query to get the consignments for given store and status sort by pick date which are not to be collected from
     * given store.
     */
    private static final String FIND_CONSIGNMENTS_UNMANIFESTED_IN_STORE_LEGACY = "SELECT {con."
            + TargetConsignmentModel.PK
            + "} FROM { pos2warehouserel AS pw JOIN " + TargetPointOfServiceModel._TYPECODE + " AS tpos ON {tpos."
            + TargetPointOfServiceModel.PK + "} = {pw.source} JOIN " + TargetConsignmentModel._TYPECODE
            + " AS con ON {con." + TargetConsignmentModel.WAREHOUSE + "} = {pw.target} JOIN " + OrderModel._TYPECODE
            + " as o ON {o." + OrderModel.PK + "} = {con." + TargetConsignmentModel.ORDER + "}}"
            + " WHERE {con." + TargetConsignmentModel.STATUS + "} = ?status AND {tpos."
            + TargetPointOfServiceModel.STORENUMBER + "} = ?storeNumber ORDER BY {con."
            + TargetConsignmentModel.PACKEDDATE
            + "}";

    /**
     * Query to get the consignments for given store and status sort by pick date which are not to be collected from
     * given store. Add the manifest model check only return the consignment which doesn't have the manifest model
     */
    private static final String FIND_CONSIGNMENTS_UNMANIFESTED_IN_STORE = "SELECT {con."
            + TargetConsignmentModel.PK
            + "} FROM { pos2warehouserel AS pw JOIN " + TargetPointOfServiceModel._TYPECODE + " AS tpos ON {tpos."
            + TargetPointOfServiceModel.PK + "} = {pw.source} JOIN " + TargetConsignmentModel._TYPECODE
            + " AS con ON {con." + TargetConsignmentModel.WAREHOUSE + "} = {pw.target} JOIN " + OrderModel._TYPECODE
            + " as o ON {o." + OrderModel.PK + "} = {con." + TargetConsignmentModel.ORDER + "}}"
            + " WHERE {con." + TargetConsignmentModel.STATUS + "} = ?status AND {tpos."
            + TargetPointOfServiceModel.STORENUMBER + "} = ?storeNumber AND {con." + TargetConsignmentModel.MANIFEST
            + "} IS NULL ORDER BY {con."
            + TargetConsignmentModel.PACKEDDATE
            + "}";

    /** Query prefix to get the consignements for instore fulfilment. */
    private static final String FIND_CONSIGNMENT_FOR_STORE_PREFIX = "SELECT {con."
            + TargetConsignmentModel.PK + "} FROM {" + TargetConsignmentModel._TYPECODE
            + " AS con JOIN pos2warehouserel AS pw ON {pw.target} = {con." + TargetConsignmentModel.WAREHOUSE
            + "} JOIN " + TargetPointOfServiceModel._TYPECODE + " AS tpos ON {tpos." + TargetPointOfServiceModel.PK
            + "} = {pw.source}} WHERE {tpos." + TargetPointOfServiceModel.PK + "} = ?store";


    /** Query to get the consignments not processed for given store */
    private static final String FIND_CONSIGNMENTS_NOT_PROCESSED_FOR_STORE = FIND_CONSIGNMENT_FOR_STORE_PREFIX
            + " AND {con." + TargetConsignmentModel.STATUS + "} NOT IN (?notProcessedStatuses)";

    /** Query to get the consignments for given store, completed instore during given time */
    private static final String FIND_CONSIGNMENTS_COMPLETED_FOR_STORE = FIND_CONSIGNMENT_FOR_STORE_PREFIX
            + " AND {con." + TargetConsignmentModel.STATUS
            + "} IN (?completeStatuses) AND @@DATEDIFFQUERY@@ = ?noOfDays";

    /** Query to get the consignments for given store, rejected instore during given time */
    private static final String FIND_CONSIGNMENTS_REJECTED_FOR_STORE = FIND_CONSIGNMENT_FOR_STORE_PREFIX + " AND {con."
            + TargetConsignmentModel.STATUS + "} IN (?rejectStatuses) AND ({con." + TargetConsignmentModel.REJECTREASON
            + "} IS NULL OR {con." + TargetConsignmentModel.REJECTREASON
            + "} NOT IN (?invalidRejectReasons)) AND @@DATEDIFFQUERY@@ = ?noOfDays";

    /** Query to get list of open consignments for given store */
    private static final String FIND_OPEN_CONSIGNMENTS_FOR_STORE = FIND_CONSIGNMENT_FOR_STORE_PREFIX
            + " AND {con." + TargetConsignmentModel.STATUS
            + "} IN (?openStatuses)";


    /** Base Query to get the count of consignments by status */
    private static final String FIND_COUNTOF_CONSIGNMENT_FOR_STORE_BY_STATUS_PREFIX = "SELECT {con."
            + TargetConsignmentModel.STATUS + "}, count(1) FROM {" + TargetConsignmentModel._TYPECODE
            + " AS con JOIN pos2warehouserel AS pw ON {pw.target} = {con." + TargetConsignmentModel.WAREHOUSE
            + "} JOIN " + TargetPointOfServiceModel._TYPECODE + " AS tpos ON {tpos." + TargetPointOfServiceModel.PK
            + "} = {pw.source}} WHERE {tpos." + TargetPointOfServiceModel.PK + "} = ?store";

    /** Query to get the count of consignments by no processed status */
    private static final String FIND_COUNTOF_CONSIGNMENTS_NOT_PROCESSED_FOR_STORE_BYSTATUS = FIND_COUNTOF_CONSIGNMENT_FOR_STORE_BY_STATUS_PREFIX
            + " AND {con." + TargetConsignmentModel.STATUS + "} NOT IN (?notProcessedStatuses) GROUP BY {con."
            + TargetConsignmentModel.STATUS + "} ";

    /** Query to get the count of consignments by which are in completed state */
    private static final String FIND_COUNTOF_CONSIGNMENTS_COMPLETED_FOR_STORE = FIND_COUNTOF_CONSIGNMENT_FOR_STORE_BY_STATUS_PREFIX
            + " AND {con." + TargetConsignmentModel.STATUS
            + "} IN (?completeStatuses) AND @@DATEDIFFQUERY@@ = ?noOfDays GROUP BY {con."
            + TargetConsignmentModel.STATUS + "} ";

    /** Query to get the count of consignments by which are in rejected state */
    private static final String FIND_COUNTOF_CONSIGNMENTS_REJECTED_FOR_STORE = FIND_COUNTOF_CONSIGNMENT_FOR_STORE_BY_STATUS_PREFIX
            + " AND {con."
            + TargetConsignmentModel.STATUS + "} IN (?rejectStatuses) AND ({con." + TargetConsignmentModel.REJECTREASON
            + "} IS NULL OR {con." + TargetConsignmentModel.REJECTREASON
            + "} NOT IN (?invalidRejectReasons)) AND @@DATEDIFFQUERY@@ = ?noOfDays GROUP BY {con."
            + TargetConsignmentModel.STATUS + "} ";



    /** Query to get quantity of products allocated to a given store */
    private static final String FIND_ALLOCATED_PRODUCT_COUNT_FOR_STORE = "SELECT SUM({ce.quantity}) FROM {"
            + ConsignmentEntryModel._TYPECODE + " AS ce JOIN "
            + OrderEntryModel._TYPECODE + " AS oe ON ({ce." + ConsignmentEntryModel.ORDERENTRY
            + "}={oe.pk} AND {oe." + OrderEntryModel.PRODUCT + "}=?product ) JOIN "
            + ConsignmentModel._TYPECODE + " AS con ON {ce." + ConsignmentEntryModel.CONSIGNMENT + "}={con.pk} JOIN "
            + WarehouseModel._TYPECODE + " AS wh ON {con." + ConsignmentModel.WAREHOUSE
            + "}={wh.pk} JOIN pos2warehouserel AS pw ON {wh."
            + WarehouseModel.PK + "}={pw.target} JOIN "
            + TargetPointOfServiceModel._TYPECODE + " AS tpos ON ({pw.source}={tpos." + TargetPointOfServiceModel.PK
            + "} AND {tpos." + TargetPointOfServiceModel.STORENUMBER + "} = ?storeNumber}) WHERE ({con."
            + ConsignmentModel.STATUS + "}=?completedStatus AND {con."
            + ConsignmentModel.SHIPPINGDATE
            + "} > ?filterDate) OR {con." + ConsignmentModel.STATUS
            + "} IN (?nonCompletedStatusesForProductCount)";

    /** Query to get the consignments for Auto update partner picked up job during given time */
    private static final String FIND_CONSIGNMENT_FOR_AUTO_UPDATE_PARTNER_PICKEDUP = "SELECT {con."
            + TargetConsignmentModel.PK + "} FROM {" + TargetConsignmentModel._TYPECODE
            + " AS con JOIN DeliveryMode AS del ON{con." + TargetConsignmentModel.DELIVERYMODE + "}={del."
            + DeliveryModeModel.PK + "}}"
            + "WHERE {del." + DeliveryModeModel.CODE + "}=?cncDeliveryMode AND {con."
            + TargetConsignmentModel.PICKEDUPDATE + "} IS NULL AND {con." + TargetConsignmentModel.PICKEDUPAUTODATE
            + "} IS NULL AND {con." + TargetConsignmentModel.READYFORPICKUPDATE
            + "} IS NOT NULL AND {con." + TargetConsignmentModel.STATUS
            + "}!=?status AND @@DATEDIFFQUERY@@ >?noOfDays";

    private static final String FIND_ALL_CONSIGNMENTS_BY_STATUS_WAREHOUSE = "SELECT "
            + "{tc." + TargetConsignmentModel.PK + "}"
            + " FROM {" + TargetConsignmentModel._TYPECODE + " as tc "
            + " JOIN " + OrderModel._TYPECODE + " as ord "
            + " ON {tc." + TargetConsignmentModel.ORDER + "}= {ord." + OrderModel.PK + "} }"
            + " WHERE {tc." + TargetConsignmentModel.STATUS + "} = ?status "
            + " AND {tc." + TargetConsignmentModel.WAREHOUSE + "} = ?warehouse "
            + " AND { ord." + OrderModel.FLUENTID + "}  is  NULL"
            + " AND {ord." + OrderModel.ORIGINALVERSION + "} IS NULL";

    /** Query to get consignments from all stores based on multiple statuses */
    private static final String FIND_CONSIGNMENTS_BY_MULTIPLE_STATUSES_FOR_ALL_STORE = "SELECT {con." + ItemModel.PK
            + "} FROM  { pos2warehouserel AS pw join " + TargetPointOfServiceModel._TYPECODE + " AS tpos on {tpos."
            + TargetPointOfServiceModel.PK + "} = {pw.source}  join " + TargetConsignmentModel._TYPECODE
            + " AS con on {con." + TargetConsignmentModel.WAREHOUSE + "} = {pw.target}  } "
            + "WHERE {con:" + TargetConsignmentModel.STATUS + "} IN (?statuses)  AND {tpos. "
            + TargetPointOfServiceModel.PK + "} IS NOT NULL ";

    private List<ConsignmentStatus> nonCompletedStatusesForProductCount;

    private TargetDBSpecificQueryFactory targetDBSpecificQueryFactory;

    private TargetFulfilmentDaoUtil targetFulfilmentDaoUtil;

    private TargetFulfilmentWarehouseService targetFulfilmentWarehouseService;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    public TargetConsignmentDaoImpl() {
        super(TargetConsignmentModel._TYPECODE);
    }

    private static final String getConsignmentsForStoreQuery(final boolean lastXDaysCreatedDateFilter) {
        return "SELECT {con." + ItemModel.PK
                + "} FROM  { pos2warehouserel AS pw join " + TargetPointOfServiceModel._TYPECODE + " AS tpos on {tpos."
                + TargetPointOfServiceModel.PK + "} = {pw.source}  join " + TargetConsignmentModel._TYPECODE
                + " AS con on {con." + TargetConsignmentModel.WAREHOUSE + "} = {pw.target} } where {tpos. "
                + TargetPointOfServiceModel.STORENUMBER + "} = ?storeNumber AND ({con."
                + TargetConsignmentModel.REJECTREASON + "} IS NULL OR {con."
                + TargetConsignmentModel.REJECTREASON + "} NOT IN (?rejectReasons))"
                + (lastXDaysCreatedDateFilter
                        ? " AND {con." + TargetConsignmentModel.CREATIONTIME + " } >= ?lastCreatedDate " : "")
                + " ORDER BY {con." + TargetConsignmentModel.CREATIONTIME + "} DESC";
    }

    private FlexibleSearchQuery getConsignmentsNotInSpecifiedStatusForStoreQuery(
            final Integer storeNumber, final String searchText,
            final int lastXDays, final List<ConsignmentStatus> statusFilter, final String sortKey,
            final String sortDirection) {

        final List<ConsignmentRejectReason> rejectReasons = new ArrayList<>();
        rejectReasons.add(ConsignmentRejectReason.CANCELLED_BY_CUSTOMER);
        final boolean lastXDaysCreatedDateFilter = lastXDays > 0 ? true : false;

        final Map<String, Object> queryParams = new HashMap<>();

        final StringBuilder queryBuilder = new StringBuilder("SELECT {con." + ItemModel.PK
                + "} FROM  { pos2warehouserel AS pw join " + TargetPointOfServiceModel._TYPECODE + " AS tpos on {tpos."
                + TargetPointOfServiceModel.PK + "} = {pw.source}  join " + TargetConsignmentModel._TYPECODE
                + " AS con on {con." + TargetConsignmentModel.WAREHOUSE + "} = {pw.target} ");

        boolean sortByOrderCreationDate = false;
        boolean sortByDeliveryType = false;
        boolean searchByOrderNumber = false;

        // Set sortByOrderCreationDate to true if sorting is by orderCreationDate     
        if (StringUtils.isEmpty(sortKey) || StringUtils.equals("orderCreationDate", sortKey)) {
            sortByOrderCreationDate = true;
        }
        // Set sortByDeliveryType to true if sorting is by deliveryType 
        else if (StringUtils.equals("deliveryType", sortKey)) {
            sortByDeliveryType = true;
        }

        final String cleanSearchText = cleanSearchText(searchText);
        if (StringUtils.isNotEmpty(cleanSearchText)) {
            if (isOrderNumberSearch(cleanSearchText)) {
                // Set sortByOrderCreationDate to true if sorting is by orderCreationDate
                sortByOrderCreationDate = true;
                // Search By Order Number
                searchByOrderNumber = true;
            }
            else {
                // Search By Customer Name
                queryBuilder
                        .append(" JOIN "
                                + AddressModel._TYPECODE
                                + " as addr on {addr.pk} = {con.shippingAddress} and (CONCAT({addr.firstName},' ',{addr.lastName}) = ?customerName OR {addr.firstName} = ?customerName OR {addr.lastName} = ?customerName) ");
                queryParams.put("customerName", cleanSearchText);
            }
        }

        // Adding Joins based on the active sort columns
        if (sortByOrderCreationDate) {
            queryBuilder.append(" JOIN " + OrderModel._TYPECODE
                    + " as ord on {ord.pk} = {con.order} ");
        }
        else if (sortByDeliveryType) {
            queryBuilder.append(" JOIN "
                    + DeliveryModeModel._TYPECODE
                    + " as dm on {dm.pk} = {con." + TargetConsignmentModel.DELIVERYMODE + "}");
        }

        if (searchByOrderNumber) {
            queryBuilder.append(" AND {ord.code} = ?orderCode ");
            queryParams.put("orderCode", cleanSearchText);
        }

        queryBuilder.append(" } where {tpos. "
                + TargetPointOfServiceModel.STORENUMBER + "} = ?storeNumber AND {con."
                + TargetConsignmentModel.STATUS + "} != ?status AND ({con."
                + TargetConsignmentModel.REJECTREASON + "} IS NULL OR {con."
                + TargetConsignmentModel.REJECTREASON + "} NOT IN (?rejectReasons))"
                + (lastXDaysCreatedDateFilter
                        ? " AND {con." + TargetConsignmentModel.CREATIONTIME + " } >= ?lastCreatedDate " : ""));

        if (CollectionUtils.isNotEmpty(statusFilter)) {
            queryBuilder.append(" AND {con."
                    + TargetConsignmentModel.STATUS + "} in (?filterStatus) ");
            queryParams.put("filterStatus", statusFilter);
        }

        // Sorting by Order Creation Date based on Direction
        if (StringUtils.isEmpty(sortKey) || sortByOrderCreationDate) {
            final String direction = StringUtils.isNotEmpty(sortDirection) ? sortDirection : "ASC";
            queryBuilder.append(" ORDER BY {ord." + OrderModel.DATE + "} " + direction);
        }
        // Sorting by Consignment Creation Date based on Direction
        else if (StringUtils.equals("consignmentCreationDate", sortKey)) {
            queryBuilder.append(" ORDER BY {con." + TargetConsignmentModel.CREATIONTIME + "} " + sortDirection);
        }
        // Sorting by Delivery Mode based on Direction
        else if (sortByDeliveryType) {
            queryBuilder.append(" ORDER BY {dm." + DeliveryModeModel.CODE + "} " + sortDirection);
        }

        queryParams.put("storeNumber", storeNumber);
        queryParams.put("rejectReasons", rejectReasons);
        queryParams.put("status", ConsignmentStatus.CREATED);

        if (lastXDaysCreatedDateFilter) {
            DateTime dateTime = new DateTime();
            dateTime = dateTime.minusDays(lastXDays);
            //Optimize DB query by rounding it to date
            queryParams.put("lastCreatedDate", getRoundedDateToXMinutes(dateTime, 5).toDate());

        }

        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryBuilder.toString(), queryParams);
        return query;
    }

    @Override
    public List<TargetConsignmentModel> getConsignmentsWaitingForShipConfirm(final Integer shipConfirmWaitLimit) {
        final DateTime shipConfWaitLimit = DateTime.now().minusHours(shipConfirmWaitLimit.intValue());

        final Map<String, Object> params = new HashMap<String, Object>(2);
        params.put("shipConfirmWaitLimit", shipConfWaitLimit.toDate());
        params.put("status", ConsignmentStatus.SHIPPED);

        final SearchResult<TargetConsignmentModel> searchResult = getFlexibleSearchService().search(
                CONSIGNMENTS_WAITING_FOR_SHIP_CONFIRM, params);
        return targetFulfilmentDaoUtil.refreshModelList(searchResult.getResult());
    }

    @Override
    public List<TargetConsignmentModel> getAllConsignmentsNotAcknowlegedWithinTimeLimit() {

        final String timeFormatDateDiffQuery = targetDBSpecificQueryFactory
                .getDBSpecificQueryFormatter().formatDateDiff(TargetDBSpecificQueryFormatter.MINUTES,
                        "{" + TargetConsignmentModel.SENTTOWAREHOUSEDATE + "}", "?currentDate");

        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_CONSIGNMENTS_BY_STATUS_AND_WAREHOUSE.replace(
                "@@DATEDIFFQUERY@@", timeFormatDateDiffQuery));

        query.addQueryParameter("status", ConsignmentStatus.SENT_TO_WAREHOUSE);
        query.addQueryParameter("warehouse", targetFulfilmentWarehouseService.getDefaultOnlineWarehouse());
        query.addQueryParameter("currentDate", new Date());

        LOG.debug("Resend Order Extract Selection Query" + query);

        final SearchResult<TargetConsignmentModel> searchResult = getFlexibleSearchService().search(query);
        final List<TargetConsignmentModel> result = searchResult.getResult();
        if (CollectionUtils.isNotEmpty(result)) {
            targetFulfilmentDaoUtil.refreshModelList(result);
        }
        return result;
    }

    @Override
    public List<TargetConsignmentModel> getAllExternalConsignmentsInCreatedStateWithinTimeLimit() {
        final String timeFormatDateDiffQuery = targetDBSpecificQueryFactory
                .getDBSpecificQueryFormatter().formatDateDiff(TargetDBSpecificQueryFormatter.MINUTES,
                        "{" + TargetConsignmentModel.CREATIONTIME + "}", "?currentDate");
        final FlexibleSearchQuery query = new FlexibleSearchQuery(
                FIND_CONSIGNMENTS_BY_STATUS_AND_EXTERNAL_WAREHOUSE.replace(
                        "@@DATEDIFFQUERY@@", timeFormatDateDiffQuery));

        query.addQueryParameter("status", ConsignmentStatus.CREATED);
        query.addQueryParameter("external", Boolean.TRUE);
        query.addQueryParameter("currentDate", new Date());

        final SearchResult<TargetConsignmentModel> searchResult = getFlexibleSearchService()
                .search(query);

        return targetFulfilmentDaoUtil.refreshModelList(searchResult.getResult());
    }

    @Override
    public TargetConsignmentModel getConsignmentBycode(final String consignmentCode)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        final Map<String, Object> params = new HashMap<String, Object>(1, 1);
        params.put("consignmentCode", consignmentCode);

        final SearchResult<TargetConsignmentModel> searchResult = getFlexibleSearchService()
                .search(FIND_CONSIGNMENT_BY_CODE_QUERY, params);

        final List<TargetConsignmentModel> models = targetFulfilmentDaoUtil.refreshModelList(searchResult
                .getResult());

        TargetServicesUtil.validateIfSingleResult(models, TargetConsignmentModel.class,
                TargetConsignmentModel.CODE, consignmentCode);

        return models.get(0);
    }


    /**
     * Method to find consignments by store number
     * 
     * @param storeNumber
     * @return targetConsignmentModel
     */
    @Override
    public List<TargetConsignmentModel> getConsignmentsForStore(final Integer storeNumber) {

        final List<ConsignmentRejectReason> rejectReasons = new ArrayList<>();
        rejectReasons.add(ConsignmentRejectReason.CANCELLED_BY_CUSTOMER);
        final Map<String, Object> params = new HashMap<String, Object>(2);
        params.put("storeNumber", storeNumber);
        params.put("rejectReasons", rejectReasons);

        final SearchResult<TargetConsignmentModel> searchResult = getFlexibleSearchService()
                .search(getConsignmentsForStoreQuery(false), params);

        return targetFulfilmentDaoUtil.refreshModelList(searchResult.getResult());

    }



    /* Method to find consignments by store number
     * @param storeNumber
     * @param offset
     * @param recsPerPage
     * @return targetConsignmentModel
     */

    @Override
    public TargetConsignmentPageResult getConsignmentsForStore(final Integer storeNumber, final int offset,
            final int recsPerPage, final int lastXDays, final String searchText, final List<ConsignmentStatus> status,
            final String sortKey, final String sortDirection) {


        final TargetConsignmentPageResult targetConsignmentPageResult = new TargetConsignmentPageResult();

        final FlexibleSearchQuery query = getConsignmentsNotInSpecifiedStatusForStoreQuery(storeNumber, searchText,
                lastXDays, status, sortKey, sortDirection);

        query.setNeedTotal(true);
        //check for 0 and negative values
        LOG.info("Retrieving Consingments : Offset=" + offset + " recsPerPage=" + recsPerPage);
        query.setCount((0 == recsPerPage) || (0 > recsPerPage) ? DEFAULT_RECS_PER_PAGE : recsPerPage);
        query.setStart(offset);
        final SearchResult<TargetConsignmentModel> searchResult = getFlexibleSearchService().search(query);
        targetConsignmentPageResult.setConsignments(targetFulfilmentDaoUtil.refreshModelList(searchResult
                .getResult()));
        targetConsignmentPageResult.setTotal(searchResult.getTotalCount());
        LOG.info("Total Consignments Retrieved =" + searchResult.getTotalCount() + " Consignment in current batch="
                + searchResult.getCount());

        return targetConsignmentPageResult;
    }



    /**
     * @param searchText
     * @return
     */
    private boolean isOrderNumberSearch(final String searchText) {
        return NumberUtils.isNumber(searchText) || StringUtils.startsWithIgnoreCase(searchText, "autox11");
    }

    /**
     * @param searchText
     */
    private String cleanSearchText(final String searchText) {
        if (StringUtils.isNotEmpty(searchText)) {
            return searchText.trim().replaceAll(" +", " ");
        }
        return searchText;
    }

    /**
     * @param consignmentCodes
     *            List of consignment codes
     * @return ConsignmentModels Consignment Models
     */
    @Override
    public List<TargetConsignmentModel> getConsignmentsByCodes(final List<String> consignmentCodes) {

        final Map<String, Object> params = new HashMap<String, Object>(1, 1);
        params.put("consignmentCodes", consignmentCodes);

        final SearchResult<TargetConsignmentModel> searchResult = getFlexibleSearchService()
                .search(FIND_CONSIGNMENT_BY_CODES_QUERY, params);

        return targetFulfilmentDaoUtil.refreshModelList(searchResult.getResult());
    }

    @Override
    public List<TargetConsignmentModel> getConsignmentsUnmanifestedInStore(final ConsignmentStatus status,
            final int storeNumber) {
        String queryString = StringUtils.EMPTY;
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)) {
            queryString = FIND_CONSIGNMENTS_UNMANIFESTED_IN_STORE;
        }
        else {
            queryString = FIND_CONSIGNMENTS_UNMANIFESTED_IN_STORE_LEGACY;
        }
        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
        query.addQueryParameter("status", status);
        query.addQueryParameter("storeNumber", Integer.valueOf(storeNumber));

        return getRefreshedConsignments(query);
    }

    @Override
    public List<TargetConsignmentModel> getConsignmentsByStatusForAllStore(final ConsignmentStatus status) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_CONSIGNMENTS_BY_STATUS_FOR_ALL_STORE);
        query.addQueryParameter("status", status);
        return getRefreshedConsignments(query);
    }

    @Override
    public List<TargetConsignmentModel> getConsignmentsByStatusForStoreInState(final List<ConsignmentStatus> statuses,
            final List<String> includedStates) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_CONSIGNMENTS_BY_STATUS_FOR_STORES_IN_STATE);
        query.addQueryParameter("statuses", statuses);
        query.addQueryParameter("includedStates", includedStates);
        return getRefreshedConsignments(query);
    }

    @Override
    public List<TargetConsignmentModel> getConsignmentsByStatusForStoreNotInState(
            final List<ConsignmentStatus> statuses,
            final List<String> excludedStates) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_CONSIGNMENTS_BY_STATUS_FOR_STORES_NOT_IN_STATE);
        query.addQueryParameter("statuses", statuses);
        query.addQueryParameter("excludedStates", excludedStates);
        return getRefreshedConsignments(query);
    }

    @Override
    public List<TargetConsignmentModel> getConsignmentsNotProcessedForStore(final TargetPointOfServiceModel store) {
        final Map<String, Object> params = new HashMap<String, Object>(2);
        params.put("store", store);
        params.put("notProcessedStatuses",
                Arrays.asList(ConsignmentStatus.SHIPPED, ConsignmentStatus.CANCELLED, ConsignmentStatus.CREATED));

        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_CONSIGNMENTS_NOT_PROCESSED_FOR_STORE, params);
        return getRefreshedConsignments(query);
    }

    @Override
    public List<TargetConsignmentModel> getOpenConsignmentsForStore(final TargetPointOfServiceModel store) {
        final Map<String, Object> params = new HashMap<String, Object>(2);
        params.put("store", store);
        params.put("openStatuses",
                Arrays.asList(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, ConsignmentStatus.SENT_TO_WAREHOUSE));

        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_OPEN_CONSIGNMENTS_FOR_STORE, params);
        return getRefreshedConsignments(query);
    }



    @Override
    public Map<ConsignmentStatus, Integer> getCountOfConsignmentsNotProcessedForStoreByStatus(
            final TargetPointOfServiceModel store) {

        final Map<ConsignmentStatus, Integer> consignmentStatusMap = new HashMap<>();
        final Map<String, Object> params = new HashMap<>(2);
        params.put("store", store);
        params.put("notProcessedStatuses",
                Arrays.asList(ConsignmentStatus.SHIPPED, ConsignmentStatus.CANCELLED, ConsignmentStatus.CREATED));

        final FlexibleSearchQuery query = new FlexibleSearchQuery(
                FIND_COUNTOF_CONSIGNMENTS_NOT_PROCESSED_FOR_STORE_BYSTATUS, params);
        query.setResultClassList(Arrays.asList(EnumerationValueModel.class, Integer.class));
        final SearchResult<List<?>> result = getFlexibleSearchService().search(query);
        final List<List<?>> resultValues = result.getResult();

        for (final List<?> row : resultValues) {
            final String consignmentStatusCode = ((EnumerationValueModel)row.get(0)).getCode();
            if (StringUtils.isNotEmpty(consignmentStatusCode)) {
                consignmentStatusMap.put(ConsignmentStatus.valueOf(consignmentStatusCode), (Integer)row.get(1));
            }
        }
        return consignmentStatusMap;
    }

    @Override
    public Map<ConsignmentStatus, Integer> getCountOfConsignmentsCompletedByDayForStoreByStatus(
            final TargetPointOfServiceModel store,
            final int noOfDays) {
        final Map<ConsignmentStatus, Integer> consignmentStatusMap = new HashMap<>();
        final String timeFormatDateDiffQuery = targetDBSpecificQueryFactory
                .getDBSpecificQueryFormatter().formatDateDiff(TargetDBSpecificQueryFormatter.DAYS,
                        "{" + TargetConsignmentModel.SHIPPINGDATE + "}", "?currentDate");

        final FlexibleSearchQuery query = new FlexibleSearchQuery(
                FIND_COUNTOF_CONSIGNMENTS_COMPLETED_FOR_STORE.replace(
                        "@@DATEDIFFQUERY@@", timeFormatDateDiffQuery));

        query.addQueryParameter("store", store);
        query.addQueryParameter("completeStatuses", Arrays.asList(ConsignmentStatus.SHIPPED));

        //Optimize DB query by rounding it to date
        query.addQueryParameter("currentDate", getRoundedDateToXMinutes(new DateTime(), 5).toDate()); //Database Optimization to round to 5 minutes.
        query.addQueryParameter("noOfDays", Integer.valueOf(noOfDays));

        query.setResultClassList(Arrays.asList(EnumerationValueModel.class, Integer.class));
        final SearchResult<List<?>> result = getFlexibleSearchService().search(query);
        final List<List<?>> resultValues = result.getResult();

        for (final List<?> row : resultValues) {
            final String consignmentStatusCode = ((EnumerationValueModel)row.get(0)).getCode();
            if (StringUtils.isNotEmpty(consignmentStatusCode)) {
                consignmentStatusMap.put(ConsignmentStatus.valueOf(consignmentStatusCode), (Integer)row.get(1));
            }
        }
        return consignmentStatusMap;
    }


    @Override
    public Map<ConsignmentStatus, Integer> getCountOfConsignmentsRejectedByDayForStoreByStatus(
            final TargetPointOfServiceModel store,
            final int noOfDays) {

        final Map<ConsignmentStatus, Integer> consignmentStatusMap = new HashMap<>();
        final String timeFormatDateDiffQuery = targetDBSpecificQueryFactory
                .getDBSpecificQueryFormatter().formatDateDiff(TargetDBSpecificQueryFormatter.DAYS,
                        "{" + TargetConsignmentModel.CANCELDATE + "}", "?currentDate");

        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_COUNTOF_CONSIGNMENTS_REJECTED_FOR_STORE.replace(
                "@@DATEDIFFQUERY@@", timeFormatDateDiffQuery));

        query.addQueryParameter("store", store);
        query.addQueryParameter("rejectStatuses", Arrays.asList(ConsignmentStatus.CANCELLED));
        query.addQueryParameter("invalidRejectReasons", Arrays.asList(ConsignmentRejectReason.CANCELLED_BY_CUSTOMER));

        //Optimize DB query by rounding it to date
        query.addQueryParameter("currentDate", getRoundedDateToXMinutes(new DateTime(), 5).toDate()); //Database Optimization to round to 5 minutes.
        query.addQueryParameter("noOfDays", Integer.valueOf(noOfDays));

        query.setResultClassList(Arrays.asList(EnumerationValueModel.class, Integer.class));
        final SearchResult<List<?>> result = getFlexibleSearchService().search(query);
        final List<List<?>> resultValues = result.getResult();

        for (final List<?> row : resultValues) {
            final String consignmentStatusCode = ((EnumerationValueModel)row.get(0)).getCode();
            if (StringUtils.isNotEmpty(consignmentStatusCode)) {
                consignmentStatusMap.put(ConsignmentStatus.valueOf(consignmentStatusCode), (Integer)row.get(1));
            }
        }
        return consignmentStatusMap;
    }



    @Override
    public List<TargetConsignmentModel> getConsignmentsCompletedByDayForStore(final TargetPointOfServiceModel store,
            final int noOfDays) {
        final String timeFormatDateDiffQuery = targetDBSpecificQueryFactory
                .getDBSpecificQueryFormatter().formatDateDiff(TargetDBSpecificQueryFormatter.DAYS,
                        "{" + TargetConsignmentModel.SHIPPINGDATE + "}", "?currentDate");

        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_CONSIGNMENTS_COMPLETED_FOR_STORE.replace(
                "@@DATEDIFFQUERY@@", timeFormatDateDiffQuery));

        query.addQueryParameter("store", store);
        query.addQueryParameter("completeStatuses", Arrays.asList(ConsignmentStatus.SHIPPED));

        //Optimize DB query by rounding it to date
        query.addQueryParameter("currentDate", getRoundedDateToXMinutes(new DateTime(), 5).toDate()); //Database Optimization to round to 5 minutes.
        query.addQueryParameter("noOfDays", Integer.valueOf(noOfDays));

        return getRefreshedConsignments(query);
    }

    @Override
    public List<TargetConsignmentModel> getConsignmentsRejectedByDayForStore(final TargetPointOfServiceModel store,
            final int noOfDays) {

        final String timeFormatDateDiffQuery = targetDBSpecificQueryFactory
                .getDBSpecificQueryFormatter().formatDateDiff(TargetDBSpecificQueryFormatter.DAYS,
                        "{" + TargetConsignmentModel.CANCELDATE + "}", "?currentDate");

        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_CONSIGNMENTS_REJECTED_FOR_STORE.replace(
                "@@DATEDIFFQUERY@@", timeFormatDateDiffQuery));

        query.addQueryParameter("store", store);
        query.addQueryParameter("rejectStatuses", Arrays.asList(ConsignmentStatus.CANCELLED));
        query.addQueryParameter("invalidRejectReasons", Arrays.asList(ConsignmentRejectReason.CANCELLED_BY_CUSTOMER));

        //Optimize DB query by rounding it to date
        query.addQueryParameter("currentDate", getRoundedDateToXMinutes(new DateTime(), 5).toDate()); //Database Optimization to round to 5 minutes.
        query.addQueryParameter("noOfDays", Integer.valueOf(noOfDays));

        return getRefreshedConsignments(query);
    }

    private DateTime getRoundedDateToXMinutes(final DateTime dateTime, final int minutes) {
        if (minutes < 1 || 60 % minutes != 0) {
            throw new IllegalArgumentException("minutes must be a factor of 60");
        }

        final DateTime hour = dateTime.hourOfDay().roundFloorCopy();
        final long millisSinceHour = new Duration(hour, dateTime).getMillis();
        final int roundedMinutes = ((int)Math.round(
                millisSinceHour / 60000.0 / minutes)) * minutes;
        return hour.minusMinutes(roundedMinutes);
    }

    @Override
    public int getConsignmentCountForStoreByMaxCutoffPeriod(final Integer storeNumber,
            final Date filterDate) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_CONSIGNMENT_BY_CUTOFF_TIME);

        query.addQueryParameter("storeNumber", storeNumber);
        query.addQueryParameter("filterDate", filterDate);
        final SearchResult<TargetConsignmentModel> resultSet = getFlexibleSearchService().search(query);
        if (null != resultSet) {
            return resultSet.getTotalCount();
        }
        return 0;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.dao.TargetConsignmentDao#getQuantityAssignedToStoreForProduct(java.lang.String, java.util.Date)
     */
    @Override
    public int getQuantityAssignedToStoreForProduct(final Integer storeNumber,
            final AbstractTargetVariantProductModel product,
            final Date filterDate) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_ALLOCATED_PRODUCT_COUNT_FOR_STORE);
        query.addQueryParameter("storeNumber", storeNumber);
        query.addQueryParameter("completedStatus", ConsignmentStatus.SHIPPED);
        query.addQueryParameter("product", product);
        query.addQueryParameter("filterDate", filterDate);
        query.addQueryParameter("nonCompletedStatusesForProductCount", nonCompletedStatusesForProductCount);

        final List resultClassList = new ArrayList();
        resultClassList.add(Integer.class);
        query.setResultClassList(resultClassList);

        final SearchResult<Integer> resultSet = getFlexibleSearchService().search(query);
        if (resultSet != null) {
            if (CollectionUtils.isNotEmpty(resultSet.getResult())) {
                final Integer sum = resultSet.getResult().get(0);
                if (sum != null) {
                    return sum.intValue();
                }
            }
        }
        return 0;
    }

    /**
     * Gets the refreshed consignments.
     *
     * @param query
     *            the query
     * @return the refreshed consignments
     */
    private List<TargetConsignmentModel> getRefreshedConsignments(final FlexibleSearchQuery query) {
        final SearchResult<TargetConsignmentModel> result = getFlexibleSearchService().search(query);
        final List<TargetConsignmentModel> resultValues = result.getResult();

        if (CollectionUtils.isEmpty(resultValues)) {
            return Collections.emptyList();
        }

        return targetFulfilmentDaoUtil.refreshModelList(resultValues);
    }

    @Override
    public List<TargetConsignmentModel> getConsignmentsForAutoUpdatePartnerPickedup(final String deliveryModeCode,
            final int noOfDays) {

        final String timeFormatDateDiffQuery = targetDBSpecificQueryFactory
                .getDBSpecificQueryFormatter().formatDateDiff(TargetDBSpecificQueryFormatter.DAYS,
                        "{" + TargetConsignmentModel.READYFORPICKUPDATE + "}", "?currentDate");

        final FlexibleSearchQuery query = new FlexibleSearchQuery(
                FIND_CONSIGNMENT_FOR_AUTO_UPDATE_PARTNER_PICKEDUP.replace(
                        "@@DATEDIFFQUERY@@", timeFormatDateDiffQuery));

        query.addQueryParameter("cncDeliveryMode", deliveryModeCode);
        query.addQueryParameter("noOfDays", Integer.valueOf(noOfDays));
        query.addQueryParameter("currentDate", Calendar.getInstance().getTime());
        query.addQueryParameter("status", ConsignmentStatus.CANCELLED);

        final SearchResult<TargetConsignmentModel> searchResult = getFlexibleSearchService().search(query);
        final List<TargetConsignmentModel> result = searchResult.getResult();
        if (CollectionUtils.isNotEmpty(result)) {
            targetFulfilmentDaoUtil.refreshModelList(result);
        }
        return result;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.dao.TargetConsignmentDao#getAllUnShippedConsignments()
     */
    @Override
    public List<TargetConsignmentModel> getAllConsignmentsByStatusAndWarehouse(final WarehouseModel warehouse,
            final ConsignmentStatus status) {

        final Map params = new HashMap<String, Object>(2, 2);
        params.put("status", status);
        params.put("warehouse", warehouse);
        final FlexibleSearchQuery query = new FlexibleSearchQuery(
                FIND_ALL_CONSIGNMENTS_BY_STATUS_WAREHOUSE, params);
        return getRefreshedConsignments(query);
    }

    @Override
    public List<TargetConsignmentModel> getConsignmentsOfMultiplesStatusesForAllStores(
            final List<ConsignmentStatus> statuses) {

        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_CONSIGNMENTS_BY_MULTIPLE_STATUSES_FOR_ALL_STORE);
        query.addQueryParameter("statuses", statuses);
        return getRefreshedConsignments(query);
    }

    /**
     * @param targetDBSpecificQueryFactory
     *            the targetDBSpecificQueryFactory to set
     */
    @Required
    public void setTargetDBSpecificQueryFactory(final TargetDBSpecificQueryFactory targetDBSpecificQueryFactory) {
        this.targetDBSpecificQueryFactory = targetDBSpecificQueryFactory;
    }

    /**
     * @param nonCompletedStatusesForProductCount
     *            the nonCompletedStatusesForProductCount to set
     */
    @Required
    public void setNonCompletedStatusesForProductCount(
            final List<ConsignmentStatus> nonCompletedStatusesForProductCount) {
        this.nonCompletedStatusesForProductCount = nonCompletedStatusesForProductCount;
    }

    /**
     * @param targetFulfilmentDaoUtil
     *            the targetFulfilmentDaoUtil to set
     */
    @Required
    public void setTargetFulfilmentDaoUtil(final TargetFulfilmentDaoUtil targetFulfilmentDaoUtil) {
        this.targetFulfilmentDaoUtil = targetFulfilmentDaoUtil;
    }

    /**
     * @param targetFulfilmentWarehouseService
     *            the targetFulfilmentWarehouseService to set
     */
    @Required
    public void setTargetFulfilmentWarehouseService(
            final TargetFulfilmentWarehouseService targetFulfilmentWarehouseService) {
        this.targetFulfilmentWarehouseService = targetFulfilmentWarehouseService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }
}

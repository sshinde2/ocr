/**
 * 
 */
package au.com.target.tgtfulfilment.dao.impl;

import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.dao.impl.TargetPointOfServiceDaoImpl;
import au.com.target.tgtfulfilment.dao.TargetFulfilmentPointOfServiceDao;
import au.com.target.tgtfulfilment.model.NTLFulfilmentCapabilitiesModel;


/**
 * @author Nandini
 *
 */
public class TargetFulfilmentPointOfServiceDaoImpl extends TargetPointOfServiceDaoImpl
        implements TargetFulfilmentPointOfServiceDao {

    private static final Logger LOG = Logger.getLogger(TargetFulfilmentPointOfServiceDaoImpl.class);

    private static final String QUERY_TARGET_POS_NTLFULFILMENT_ENABLED = "SELECT {pos." + TargetPointOfServiceModel.PK
            + "} FROM {" + TargetPointOfServiceModel._TYPECODE
            + " AS pos},{" + NTLFulfilmentCapabilitiesModel._TYPECODE
            + " AS ntl},{" + RegionModel._TYPECODE + " AS reg} where {pos."
            + TargetPointOfServiceModel.NTLFULFILMENTCAPABILITIES + "} = {ntl."
            + NTLFulfilmentCapabilitiesModel.PK + "} and {reg." + RegionModel.ISOCODE + "} = ?isocode and {ntl."
            + NTLFulfilmentCapabilitiesModel.PK + "}={reg." + RegionModel.NTLFULFILMENTCAPABILITIES
            + "} and {ntl." + NTLFulfilmentCapabilitiesModel.ENABLED + "} = ?enabled";


    @Override
    public TargetPointOfServiceModel getNTLByRegion(final RegionModel region) {
        Assert.notNull(region, "Region model can't be null.");

        final String isocode = region.getIsocode();
        Assert.notNull("Region's isocode can't be null.", isocode);

        final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_TARGET_POS_NTLFULFILMENT_ENABLED);
        query.addQueryParameter("enabled", Boolean.TRUE);
        query.addQueryParameter("isocode", isocode);

        try {
            return getFlexibleSearchService().searchUnique(query);
        }
        catch (final ModelNotFoundException e) {
            // this exception is fine as we can have regions without any NTL
            LOG.info("No NTL POS found for region with isocode as: " + isocode);
            return null;
        }
        catch (final AmbiguousIdentifierException e) {
            LOG.warn("More than one POS found for region with isocode as: " + isocode, e);
            return null;
        }
    }

}

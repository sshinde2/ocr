package au.com.target.tgtfulfilment.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.List;

import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.dao.TargetGlobalStoreFulfilmentCapabilitiesDao;


/**
 * The Class TargetGlobalStoreFulfilmentCapabilitiesDaoImpl.
 * 
 * @author ajit
 */
public class TargetGlobalStoreFulfilmentCapabilitiesDaoImpl extends
        DefaultGenericDao<GlobalStoreFulfilmentCapabilitiesModel> implements
        TargetGlobalStoreFulfilmentCapabilitiesDao {

    public TargetGlobalStoreFulfilmentCapabilitiesDaoImpl() {
        super(GlobalStoreFulfilmentCapabilitiesModel._TYPECODE);
    }

    @Override
    public GlobalStoreFulfilmentCapabilitiesModel getGlobalStoreFulfilmentCapabilities() {

        final List<GlobalStoreFulfilmentCapabilitiesModel> result = find();
        return result.size() == 0 ? null : result.get(0);
    }

}
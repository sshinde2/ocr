/**
 * 
 */
package au.com.target.tgtfulfilment.dto;

/**
 * Parcel DTO for facades and services.
 * 
 * @author jjayawa1
 *
 */
public class ConsignmentParcelDTO {
    private String height;
    private String length;
    private String width;
    private String weight;

    /**
     * @return the weight
     */
    public String getWeight() {
        return weight;
    }

    /**
     * @param weight
     *            the weight to set
     */
    public void setWeight(final String weight) {
        this.weight = weight;
    }

    /**
     * @return the height
     */
    public String getHeight() {
        return height;
    }

    /**
     * @param height
     *            the height to set
     */
    public void setHeight(final String height) {
        this.height = height;
    }

    /**
     * @return the length
     */
    public String getLength() {
        return length;
    }

    /**
     * @param length
     *            the length to set
     */
    public void setLength(final String length) {
        this.length = length;
    }

    /**
     * @return the width
     */
    public String getWidth() {
        return width;
    }

    /**
     * @param width
     *            the width to set
     */
    public void setWidth(final String width) {
        this.width = width;
    }


}

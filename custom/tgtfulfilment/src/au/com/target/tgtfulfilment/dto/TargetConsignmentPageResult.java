/**
 * 
 */
package au.com.target.tgtfulfilment.dto;

import java.util.List;

import au.com.target.tgtcore.model.TargetConsignmentModel;


/**
 * @author smudumba
 *
 */
public class TargetConsignmentPageResult {

    private int total;
    private List<TargetConsignmentModel> consignments;

    /**
     * @return the total
     */
    public int getTotal() {
        return total;
    }

    /**
     * @param total
     *            the total to set
     */
    public void setTotal(final int total) {
        this.total = total;
    }

    /**
     * @return the consignments
     */
    public List<TargetConsignmentModel> getConsignments() {
        return consignments;
    }

    /**
     * @param consignments
     *            the consignments to set
     */
    public void setConsignments(final List<TargetConsignmentModel> consignments) {
        this.consignments = consignments;
    }



}

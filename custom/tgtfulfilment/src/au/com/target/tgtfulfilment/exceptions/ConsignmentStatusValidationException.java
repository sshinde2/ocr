/**
 * 
 */
package au.com.target.tgtfulfilment.exceptions;

/**
 * @author sbryan6
 *
 */
public class ConsignmentStatusValidationException extends Exception {

    public ConsignmentStatusValidationException(final String message) {
        super(message);
    }

}

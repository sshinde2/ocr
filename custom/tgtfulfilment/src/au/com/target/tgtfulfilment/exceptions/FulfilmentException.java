package au.com.target.tgtfulfilment.exceptions;

import de.hybris.platform.servicelayer.exceptions.BusinessException;


/**
 * Base exception for all fulfilment related processing errors
 * 
 */
public class FulfilmentException extends BusinessException {

    /**
     * @param message
     * @param cause
     */
    public FulfilmentException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     */
    public FulfilmentException(final String message) {
        super(message);
    }

}

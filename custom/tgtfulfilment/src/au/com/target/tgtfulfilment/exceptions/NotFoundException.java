/**
 * 
 */
package au.com.target.tgtfulfilment.exceptions;

import de.hybris.platform.servicelayer.exceptions.BusinessException;


/**
 * @author jjayawa1
 *
 */
public class NotFoundException extends BusinessException {

    /**
     * @param message
     * @param cause
     */
    public NotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     */
    public NotFoundException(final String message) {
        super(message);
    }

}

/**
 * 
 */
package au.com.target.tgtfulfilment.exceptions;

/**
 * TargetSplitOrderException. When the warehouse is null, this exception will be thrown.
 *
 */
public class TargetSplitOrderException extends Exception {
    public TargetSplitOrderException(final String message) {
        super(message);
    }

    public TargetSplitOrderException(final Throwable cause) {
        super(cause);
    }

    public TargetSplitOrderException(final String message, final Throwable cause) {
        super(message, cause);
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.exceptions;

/**
 * @author smishra1
 *
 */
public class TrackingIdGenerationException extends RuntimeException {
    /**
     * @param message
     *            the exception message
     * @param cause
     *            the cause of the exception
     */
    public TrackingIdGenerationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     *
     * @param message
     *            the exception message
     */
    public TrackingIdGenerationException(final String message) {
        super(message);
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice;

import java.util.List;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.dto.ConsignmentParcelDTO;


/**
 * Utility class for updating consignment with parcel data.
 * 
 * @author jjayawa1
 *
 */
public interface ConsignmentParcelUpdater {
    /**
     * Just updates the parcel count.
     * 
     * @param consignment
     * @param parcelCount
     */
    void updateParcelCount(TargetConsignmentModel consignment, Integer parcelCount);

    /**
     * Update parcel details.
     * 
     * @param parcels
     */
    void updateParcelDetails(TargetConsignmentModel consignment, List<ConsignmentParcelDTO> parcels);
}

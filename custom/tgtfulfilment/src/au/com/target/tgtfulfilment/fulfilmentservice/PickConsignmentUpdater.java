/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.dto.PickConfirmEntry;


/**
 * Utility class for updating consignment shipped quantities according to a pick.
 * 
 */
public interface PickConsignmentUpdater {

    /**
     * Update the consignment entry ship quantities based on the given pick list.
     * 
     * @param consignment
     * @param pickConfirmEntries
     */
    void updateConsignmentShipQuantities(ConsignmentModel consignment, List<PickConfirmEntry> pickConfirmEntries);

    /**
     * Update all the consignment details.
     * 
     * @param consignment
     * @param parcelCount
     * @param trackingNumber
     * @param carrier
     */
    void updateConsignmentDetails(TargetConsignmentModel consignment, Integer parcelCount, String trackingNumber,
            String carrier);
}

/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;

import au.com.target.tgtfulfilment.dto.PickConfirmEntry;


/**
 * Methods for validating the contents of a pick to determine invalid or over pick
 * 
 */
public interface PickValidator {

    /**
     * Determine if this is an invalid pick.
     * 
     * @param consignment
     * @param pickConfirmEntries
     * @return error message
     */
    String isInvalidPick(ConsignmentModel consignment, List<PickConfirmEntry> pickConfirmEntries);

    /**
     * Determine if this is an over pick.
     * 
     * @param consignment
     * @param pickConfirmEntries
     * @return true if over pick
     */
    boolean isOverPick(ConsignmentModel consignment, List<PickConfirmEntry> pickConfirmEntries);

}

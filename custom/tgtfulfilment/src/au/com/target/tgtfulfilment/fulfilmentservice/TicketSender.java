/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;


/**
 * @author Nandini
 *
 */
public interface TicketSender {

    /**
     * Raise cs ticket when stock is unavailable in defalut warehouse
     * 
     * @param oeg
     */
    void sendStockNotInDefaultWarehouse(OrderEntryGroup oeg);

    /**
     * Raise cs ticket when exception occured in routing logic
     * 
     * @param oeg
     */
    void sendRoutingException(OrderEntryGroup oeg);
}

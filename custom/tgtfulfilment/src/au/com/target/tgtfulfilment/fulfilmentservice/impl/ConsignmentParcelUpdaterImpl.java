/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice.impl;

import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.converters.ConsignmentParcelModelConverter;
import au.com.target.tgtfulfilment.dto.ConsignmentParcelDTO;
import au.com.target.tgtfulfilment.fulfilmentservice.ConsignmentParcelUpdater;
import au.com.target.tgtfulfilment.model.ConsignmentParcelModel;


/**
 * @author jjayawa1
 *
 */
public class ConsignmentParcelUpdaterImpl extends AbstractBusinessService implements ConsignmentParcelUpdater {

    private ConsignmentParcelModelConverter consignmentParcelModelConverter;

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.ConsignmentParcelUpdater#updateParcelCount(au.com.target.tgtcore.model.TargetConsignmentModel, java.lang.Integer)
     */
    @Override
    public void updateParcelCount(final TargetConsignmentModel consignment, final Integer parcelCount) {
        consignment.setParcelCount(parcelCount);
        getModelService().save(consignment);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.ConsignmentParcelUpdater#updateParcelDetails(au.com.target.tgtcore.model.TargetConsignmentModel, au.com.target.tgtfulfilment.dto.ConsignmentParcelDTO)
     */
    @Override
    public void updateParcelDetails(final TargetConsignmentModel consignment,
            final List<ConsignmentParcelDTO> parcels) {

        if (CollectionUtils.isNotEmpty(parcels)) {

            for (final ConsignmentParcelDTO parcel : parcels) {

                final ConsignmentParcelModel consignmentParcel = consignmentParcelModelConverter.convert(parcel);
                consignmentParcel.setConsignment(consignment);
                getModelService().save(consignmentParcel);
            }
            getModelService().refresh(consignment);
        }
    }


    /**
     * @param consignmentParcelModelConverter
     *            the consignmentParcelModelConverter to set
     */
    @Required
    public void setConsignmentParcelModelConverter(final ConsignmentParcelModelConverter consignmentParcelModelConverter) {
        this.consignmentParcelModelConverter = consignmentParcelModelConverter;
    }
}

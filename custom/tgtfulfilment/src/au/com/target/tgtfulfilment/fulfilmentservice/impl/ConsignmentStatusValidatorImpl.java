/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtfulfilment.exceptions.ConsignmentStatusValidationException;
import au.com.target.tgtfulfilment.fulfilmentservice.ConsignmentStatusValidator;


/**
 * @author sbryan6
 *
 */
public class ConsignmentStatusValidatorImpl implements ConsignmentStatusValidator {

    // Map of new status vs allowed old states
    private Map<ConsignmentStatus, List<ConsignmentStatus>> consignmentStatusMap;


    @Override
    public void validateConsignmentStatus(final ConsignmentStatus oldStatus, final ConsignmentStatus newStatus)
            throws ConsignmentStatusValidationException {
        Assert.notNull(oldStatus, "oldStatus cannot be null");
        Assert.notNull(newStatus, "newStatus cannot be null");

        final List<ConsignmentStatus> allowedStatuses = consignmentStatusMap.get(newStatus);

        if (CollectionUtils.isEmpty(allowedStatuses)) {
            throw new ConsignmentStatusValidationException(
                    "Consignment status validation unrecognized status: oldStatus="
                            + oldStatus + ", newStatus=" + newStatus);
        }

        if (!allowedStatuses.contains(oldStatus)) {
            throw new ConsignmentStatusValidationException("Consignment status validation failed: oldStatus="
                    + oldStatus
                    + ", newStatus=" + newStatus + ", allowedStatuses=" + StringUtils.join(allowedStatuses, ','));
        }
    }


    /**
     * @param consignmentStatusMap
     *            the consignmentStatusMap to set
     */
    @Required
    public void setConsignmentStatusMap(final Map<ConsignmentStatus, List<ConsignmentStatus>> consignmentStatusMap) {
        this.consignmentStatusMap = consignmentStatusMap;
    }


}

/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice.impl;

import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.PickTypeEnum;
import au.com.target.tgtcore.util.DealUtils;
import au.com.target.tgtfulfilment.dto.PickConsignmentUpdateData;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.fulfilmentservice.PickTypeAscertainerStrategy;
import au.com.target.tgtfulfilment.helper.TargetFulfilmentHelper;


/**
 * Strategy for determining the pick type of a consignment
 * 
 */
public class PickTypeAscertainerStrategyImpl implements PickTypeAscertainerStrategy {

    private static final Logger LOG = Logger.getLogger(PickTypeAscertainerStrategyImpl.class);
    private static final String DEAL_SHORT_PICK_LOG_MESSAGE = "Short pick on order with deal. Order:[ %s ], Items:[ %s ]";

    private TargetFulfilmentHelper targetFulfilmentHelper;

    @Override
    public PickTypeEnum ascertainPickType(final ConsignmentModel consignment, final PickConsignmentUpdateData updateData)
            throws FulfilmentException {
        Assert.notNull(consignment, "consignment cannot be null");
        Assert.notNull(updateData, "updateData cannot be null");

        final Set<ConsignmentEntryModel> consignmentEntries = consignment.getConsignmentEntries();

        if (CollectionUtils.isEmpty(consignmentEntries)) {
            throw new FulfilmentException(String.format("Consignment Entries shouldn't be empty on Consignment - {%s}",
                    consignment.getCode()));
        }

        // FullPick is the happy scenario, only if any shippedQty < originalQty consider short pick
        // If all shippedQty = 0 then it is a zero pick

        boolean isZeroPick = true;
        boolean isShortPick = false;
        final List<String> shortPickedDealItemCodes = new ArrayList<>();

        for (final ConsignmentEntryModel entry : consignmentEntries) {

            if (entry == null) {
                continue;
            }

            final Long originQuantity = entry.getQuantity();
            if (originQuantity == null) {
                continue;
            }

            final String itemCode = targetFulfilmentHelper.getConsignmentEntryItemCode(entry);
            if (itemCode == null) {
                throw new FulfilmentException(String.format("No item code for Consignment entry in - {%s}",
                        consignment.getCode()));
            }

            final long shippedQuantity = updateData.getShippedQuantity(itemCode);

            // If any pick is non-zero then this is not a zero pick
            if (shippedQuantity > 0) {
                isZeroPick = false;
            }

            // Check for short pick
            if (shippedQuantity < originQuantity.longValue()) {
                if (DealUtils.isEntryPartOfDeal(entry.getOrderEntry())) {
                    shortPickedDealItemCodes.add(entry.getOrderEntry().getProduct().getCode());
                }

                isShortPick = true;
            }
        }

        if (!shortPickedDealItemCodes.isEmpty()) {
            LOG.info(String.format(DEAL_SHORT_PICK_LOG_MESSAGE, consignment.getOrder().getCode(),
                    StringUtils.join(shortPickedDealItemCodes.toArray(), ',')));
        }

        // Default to full
        PickTypeEnum pickType = PickTypeEnum.FULL;

        if (isZeroPick) {
            pickType = PickTypeEnum.ZERO;
        }
        else if (isShortPick) {
            pickType = PickTypeEnum.SHORT;
        }

        return pickType;
    }

    /**
     * @param targetFulfilmentHelper
     *            the targetFulfilmentHelper to set
     */
    @Required
    public void setTargetFulfilmentHelper(final TargetFulfilmentHelper targetFulfilmentHelper) {
        this.targetFulfilmentHelper = targetFulfilmentHelper;
    }


}

/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice.impl;

import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtfulfilment.dto.PickConfirmEntry;
import au.com.target.tgtfulfilment.fulfilmentservice.PickValidator;
import au.com.target.tgtfulfilment.helper.TargetFulfilmentHelper;


/**
 * Checks if a pick is an invalid pick
 * 
 */
public class PickValidatorImpl implements PickValidator {

    private static final Logger LOG = Logger.getLogger(PickValidatorImpl.class);

    private TargetFulfilmentHelper targetFulfilmentHelper;



    /**
     * Invalid pick if the pick list has any items not in the consignment, <br/>
     * or if the pick list has null values.
     * 
     * @param consignment
     * @param pickConfirmEntries
     * @return error message
     */
    @Override
    public String isInvalidPick(final ConsignmentModel consignment,
            final List<PickConfirmEntry> pickConfirmEntries) {
        Assert.notNull(consignment, "consignment cannot be null");

        if (CollectionUtils.isEmpty(pickConfirmEntries)) {
            LOG.warn("Pick Entries are Empty");
            return "Pick Entries are Empty";
        }

        // As we go check for repeated entries
        final Set<String> seenCodes = new HashSet<>();

        for (final PickConfirmEntry pickEntry : pickConfirmEntries) {

            final String itemCode = pickEntry.getItemCode();
            if (itemCode == null) {
                LOG.error("Pick entry has null code for consignment " + consignment.getCode());
                return "Pick entry has null code for consignment " + consignment.getCode();
            }

            if (pickEntry.getQuantityShipped() == null) {
                LOG.error("Pick entry has null quantity for item code " + itemCode + " for consignment "
                        + consignment.getCode());
                return ("Pick entry has null quantity for item code " + itemCode + " for consignment "
                + consignment.getCode());
            }

            if (seenCodes.contains(itemCode)) {
                LOG.error("Repeated pick entry for item code " + itemCode + " for consignment "
                        + consignment.getCode());
                return ("Repeated pick entry for item code " + itemCode + " for consignment "
                + consignment.getCode());
            }

            seenCodes.add(itemCode);
        }

        return null;
    }

    /**
     * Over pick if the pick list has any items with greater quantities than the consignment.
     * 
     * @param consignment
     * @param pickConfirmEntries
     * @return true if this is an over pick
     */
    @Override
    public boolean isOverPick(final ConsignmentModel consignment,
            final List<PickConfirmEntry> pickConfirmEntries) {
        Assert.notNull(consignment, "consignment cannot be null");

        if (CollectionUtils.isEmpty(pickConfirmEntries)) {
            return false;
        }

        for (final PickConfirmEntry pickEntry : pickConfirmEntries) {

            final String itemCode = pickEntry.getItemCode();

            if (getConsignmentItemQuantity(consignment, itemCode) < pickEntry.getQuantityShipped().intValue()) {
                LOG.error("Pick entry has item code " + itemCode + " not in consignment "
                        + consignment.getCode());
                return true;
            }
        }

        return false;
    }


    /**
     * Check if consignment contains given item
     * 
     * @param consignment
     * @param itemCode
     * @return true if consignment contains given item
     */
    protected boolean consignmentContainsItem(final ConsignmentModel consignment, final String itemCode) {
        Assert.notNull(consignment, "consignment cannot be null");
        Assert.notNull(itemCode, "itemCode cannot be null");

        final ConsignmentEntryModel consignmentEntry = targetFulfilmentHelper.getConsignmentEntryForItemCode(
                consignment, itemCode);
        return (consignmentEntry != null);
    }


    /**
     * Return the consignment entry quantity for the given item code
     * 
     * @param consignment
     * @param itemCode
     * @return quantity of item in the consignment
     */
    protected long getConsignmentItemQuantity(final ConsignmentModel consignment, final String itemCode) {
        Assert.notNull(consignment, "consignment cannot be null");
        Assert.notNull(itemCode, "itemCode cannot be null");

        final ConsignmentEntryModel consignmentEntry = targetFulfilmentHelper.getConsignmentEntryForItemCode(
                consignment, itemCode);

        if (consignmentEntry != null && consignmentEntry.getQuantity() != null) {
            return consignmentEntry.getQuantity().longValue();
        }

        // Not found so return 0
        return 0;
    }


    /**
     * @param targetFulfilmentHelper
     *            the targetFulfilmentHelper to set
     */
    @Required
    public void setTargetFulfilmentHelper(final TargetFulfilmentHelper targetFulfilmentHelper) {
        this.targetFulfilmentHelper = targetFulfilmentHelper;
    }


}

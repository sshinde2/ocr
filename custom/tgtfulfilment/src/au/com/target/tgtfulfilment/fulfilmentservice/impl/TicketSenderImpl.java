/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;
import au.com.target.tgtfulfilment.fulfilmentservice.TicketSender;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;


/**
 * @author Nandini
 *
 */
public class TicketSenderImpl implements TicketSender {

    private static final String STOCK_UNAVAILABLE_TICKET_MESSAGE = "Stock is not available in default online warehouse";
    private static final String STOCK_UNAVAILABLE_TICKET_HEAD = "Stock is not available in default online warehouse";

    private static final String REROUTING_TICKET_MESSAGE = "Exception occurred in routing logic, defaulting to fastline";
    private static final String REROUTING_TICKET_HEAD = "Exception occurred in routing logic, defaulting to fastline";

    private OEGParameterHelper oegParameterHelper;
    private TargetTicketBusinessService targetTicketBusinessService;

    @Override
    public void sendStockNotInDefaultWarehouse(final OrderEntryGroup oeg) {

        final OrderModel order = oegParameterHelper.getOrderFromGivenGroup(oeg);
        targetTicketBusinessService.createCsAgentTicket(STOCK_UNAVAILABLE_TICKET_HEAD,
                STOCK_UNAVAILABLE_TICKET_HEAD, STOCK_UNAVAILABLE_TICKET_MESSAGE, order);
    }

    @Override
    public void sendRoutingException(final OrderEntryGroup oeg) {

        final OrderModel order = oegParameterHelper.getOrderFromGivenGroup(oeg);
        targetTicketBusinessService.createCsAgentTicket(REROUTING_TICKET_HEAD,
                REROUTING_TICKET_HEAD, REROUTING_TICKET_MESSAGE, order);
    }

    /**
     * @param oegParameterHelper
     *            the oegParameterHelper to set
     */
    @Required
    public void setOegParameterHelper(final OEGParameterHelper oegParameterHelper) {
        this.oegParameterHelper = oegParameterHelper;
    }

    /**
     * @param targetTicketBusinessService
     *            the targetTicketBusinessService to set
     */

    @Required
    public void setTargetTicketBusinessService(final TargetTicketBusinessService targetTicketBusinessService) {
        this.targetTicketBusinessService = targetTicketBusinessService;
    }

}

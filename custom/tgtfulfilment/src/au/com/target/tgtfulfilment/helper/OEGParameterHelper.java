/**
 * 
 */
package au.com.target.tgtfulfilment.helper;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.logger.RoutingLogger;


/**
 * The Class OEGParameterHelper.
 *
 * @author ayushman
 */
public class OEGParameterHelper {

    private static final RoutingLogger LOG = new RoutingLogger(OEGParameterHelper.class);

    public enum ParameterName {
        DELIVERY_MODE, CNC_STORE_NUMBER, DELIVERY_ADDRESS, ORDER_CODE, FULFILMENT_WAREHOUSE, FULFILMENT_TPOS, REQUIRED_QTY, ASSIGNED_QTY, IS_NOT_ASSIGNED_OEG
    }

    private TargetWarehouseService targetWarehouseService;


    /**
     * Populate the oeg parameters from the order
     * 
     * @param oeg
     */
    public void populateParamsFromOrder(final OrderEntryGroup oeg) {
        if (CollectionUtils.isNotEmpty(oeg)) {
            final OrderModel order = getOrderFromGivenGroup(oeg);
            Assert.notNull(order, "Order can't be null.");
            populateOEGParamsFromOrder(oeg, order);
        }

    }


    /**
     * Populate the oeg parameters from the order for list of orderEntryGroups
     * 
     * @param oegs
     */
    public void populateParamsFromOrder(final List<OrderEntryGroup> oegs) {

        if (CollectionUtils.isNotEmpty(oegs)) {
            final OrderModel order = getOrderFromGivenGroup(oegs.get(0));
            Assert.notNull(order, "Order can't be null.");
            for (final OrderEntryGroup oeg : oegs) {
                populateOEGParamsFromOrder(oeg, order);
            }
        }
    }



    /**
     * Populate oeg params from given order.
     *
     * @param oeg
     *            the oeg
     * @param order
     *            the order
     */
    protected void populateOEGParamsFromOrder(final OrderEntryGroup oeg, final OrderModel order) {

        Assert.notNull(order, "Order can't be null.");

        final AddressModel deliveryAddress = order.getDeliveryAddress();

        final DeliveryModeModel deliveryMode = order.getDeliveryMode();
        Assert.isInstanceOf(TargetZoneDeliveryModeModel.class, deliveryMode,
                "Delivery mode must be type of TargetZoneDeliveryModeModel.");

        final TargetZoneDeliveryModeModel tgtDeliveryMode = (TargetZoneDeliveryModeModel)deliveryMode;
        final Integer cncStoreNumber = order.getCncStoreNumber();
        final String orderCode = order.getCode();

        setDeliveryAddress(oeg, deliveryAddress);
        setDeliveryMode(oeg, tgtDeliveryMode);
        setCncStoreNumber(oeg, cncStoreNumber);
        setOrderCode(oeg, orderCode);
    }

    /**
     * Gets the order for given group.
     *
     * @param orderEntryGroup
     *            the order entry group
     * @return the order for given group
     */
    public OrderModel getOrderFromGivenGroup(final OrderEntryGroup orderEntryGroup) {
        final Iterator<AbstractOrderEntryModel> orderEntries = orderEntryGroup.iterator();

        if (null != orderEntries && orderEntries.hasNext()) {
            final AbstractOrderEntryModel orderEntry = orderEntries.next();

            if (null != orderEntry.getOrder()) {
                return (OrderModel)orderEntry.getOrder();
            }
        }
        return null;
    }

    /**
     * Sets the delivery mode.
     *
     * @param oeg
     *            the oeg
     * @param deliveryModeModel
     *            the delivery mode model
     */
    public void setDeliveryMode(final OrderEntryGroup oeg, final TargetZoneDeliveryModeModel deliveryModeModel) {
        oeg.setParameter(ParameterName.DELIVERY_MODE.name(), deliveryModeModel);
    }

    /**
     * Gets the delivery mode.
     *
     * @param oeg
     *            the oeg
     * @return deliveryMode
     */
    public TargetZoneDeliveryModeModel getDeliveryMode(final OrderEntryGroup oeg) {
        if (oeg != null) {
            final TargetZoneDeliveryModeModel deliveryMode = (TargetZoneDeliveryModeModel)oeg
                    .getParameter(ParameterName.DELIVERY_MODE.name());
            return deliveryMode;
        }
        return null;
    }

    /**
     * Sets the cnc store number.
     *
     * @param oeg
     *            the oeg
     * @param cncStoreNumber
     *            the cnc store number
     */
    public void setCncStoreNumber(final OrderEntryGroup oeg, final Integer cncStoreNumber) {
        oeg.setParameter(ParameterName.CNC_STORE_NUMBER.name(), cncStoreNumber);
    }

    /**
     * Gets the cnc store number.
     *
     * @param oeg
     *            the oeg
     * @return cncStoreNumber
     */
    public Integer getCncStoreNumber(final OrderEntryGroup oeg) {
        if (oeg != null) {
            final Integer cncStoreNumber = (Integer)oeg.getParameter(ParameterName.CNC_STORE_NUMBER.name());
            return cncStoreNumber;
        }
        return null;
    }

    /**
     * Sets the delivery address.
     *
     * @param oeg
     *            the oeg
     * @param deliveryAddress
     *            the address
     */
    public void setDeliveryAddress(final OrderEntryGroup oeg, final AddressModel deliveryAddress) {
        oeg.setParameter(ParameterName.DELIVERY_ADDRESS.name(), deliveryAddress);
    }

    /**
     * Gets the delivery address.
     *
     * @param oeg
     *            the oeg
     * @return address
     */
    public AddressModel getDeliveryAddress(final OrderEntryGroup oeg) {
        final AddressModel address = (AddressModel)oeg.getParameter(ParameterName.DELIVERY_ADDRESS.name());
        return address;
    }

    /**
     * Sets the order code.
     *
     * @param oeg
     *            the oeg
     * @param orderCode
     *            the order code
     */
    public void setOrderCode(final OrderEntryGroup oeg, final String orderCode) {
        oeg.setParameter(ParameterName.ORDER_CODE.name(), orderCode);
    }

    /**
     * Gets the order code.
     *
     * @param oeg
     *            the oeg
     * @return orderCode
     */
    public String getOrderCode(final OrderEntryGroup oeg) {
        final String orderCode = (String)oeg.getParameter(ParameterName.ORDER_CODE.name());
        return orderCode;
    }

    /**
     * Set the fulfilment warehouse
     * 
     * @param oeg
     * @param warehouse
     */
    private void setFulfilmentWarehouse(final OrderEntryGroup oeg, final WarehouseModel warehouse) {
        oeg.setParameter(ParameterName.FULFILMENT_WAREHOUSE.name(), warehouse);
    }

    public void setTpos(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos) {
        oeg.setParameter(ParameterName.FULFILMENT_TPOS.name(), tpos);
    }

    /**
     * get the TargetPointOfServiceModel
     * 
     * @param oeg
     * @return TargetPointOfServiceModel
     */
    public TargetPointOfServiceModel getTpos(final OrderEntryGroup oeg) {
        if (oeg != null) {
            return (TargetPointOfServiceModel)oeg.getParameter(ParameterName.FULFILMENT_TPOS.name());
        }
        return null;
    }

    /**
     * Get the fulfilment warehouse
     * 
     * @param oeg
     * @return warehouse
     */
    public WarehouseModel getFulfilmentWarehouse(final OrderEntryGroup oeg) {
        final WarehouseModel warehouse = (WarehouseModel)oeg
                .getParameter(ParameterName.FULFILMENT_WAREHOUSE.name());
        return warehouse;
    }

    /**
     * Return true if this oeg is assigned to a warehouse
     * 
     * @param oeg
     * @return true if assigned
     */
    public boolean isOEGAssigned(final OrderEntryGroup oeg) {

        return (getFulfilmentWarehouse(oeg) != null);
    }

    /**
     * Assign the tpos to this OEG, which will also assign the warehouse
     * 
     * @param oeg
     * @param tpos
     */
    public void assignTposToOEG(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos) {

        if (tpos != null) {
            assignWarehouseToOEG(oeg, targetWarehouseService.getWarehouseForPointOfService(tpos));
            setTpos(oeg, tpos);
        }
    }

    /**
     * Assign the warehouse to the OEG
     * 
     * @param oeg
     * @param fulfillingWarehouse
     */
    public void assignWarehouseToOEG(final OrderEntryGroup oeg, final WarehouseModel fulfillingWarehouse) {
        setFulfilmentWarehouse(oeg, fulfillingWarehouse);
    }

    /**
     * Set not assigned oeg flag
     * 
     * @param notAssignedOeg
     */
    public void setNotAssignedOegFlag(final OrderEntryGroup notAssignedOeg) {
        if (notAssignedOeg != null) {
            notAssignedOeg.setParameter(ParameterName.IS_NOT_ASSIGNED_OEG.name(), Boolean.TRUE);
        }
    }

    /**
     * get NotAssignedOeg Flag
     * 
     * @param oeg
     * @return boolean
     */
    public boolean getNotAssignedOegFlag(final OrderEntryGroup oeg) {
        final boolean isNotAssigned = false;
        if (oeg != null) {
            return BooleanUtils.isTrue((Boolean)oeg.getParameter(ParameterName.IS_NOT_ASSIGNED_OEG.name()));
        }
        return isNotAssigned;
    }

    /**
     * chekc if splittedOEGs contains fastline assignment
     * 
     * @param splittedOEGs
     * @return true if contains
     */
    public boolean containsFastlineOEG(final List<OrderEntryGroup> splittedOEGs) {
        if (CollectionUtils.isNotEmpty(splittedOEGs)) {
            for (final OrderEntryGroup oeg : splittedOEGs) {
                if (getFulfilmentWarehouse(oeg) == null) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Get OFC order type for the oeg
     * 
     * @param oeg
     * @param fulfillingStoreNum
     * @return type
     */
    public OfcOrderType getOfcOrderType(final OrderEntryGroup oeg, final Integer fulfillingStoreNum) {

        final TargetZoneDeliveryModeModel deliveryMode = getDeliveryMode(oeg);
        final String orderCode = getOrderCode(oeg);
        final Integer cncStoreNumber = getCncStoreNumber(oeg);

        return getOfcOrderType(deliveryMode, fulfillingStoreNum, cncStoreNumber, orderCode);
    }

    private OfcOrderType getOfcOrderType(final TargetZoneDeliveryModeModel deliveryMode,
            final Integer fulfillingStoreNum,
            final Integer cncStoreNum,
            final String orderCode) {

        if (deliveryMode == null) {
            LOG.logWarnMessage("Trying to get OFC order type but delivery mode is null, order=" + orderCode);
            return null;
        }

        if (BooleanUtils.isTrue(deliveryMode.getIsDeliveryToStore())) {

            if (cncStoreNum == null || fulfillingStoreNum == null) {
                LOG.logWarnMessage("Trying to get OFC order type but store number is null, order=" + orderCode);
                return null;
            }

            if (cncStoreNum.equals(fulfillingStoreNum)) {
                return OfcOrderType.INSTORE_PICKUP;
            }

            return OfcOrderType.INTERSTORE_DELIVERY;
        }

        return OfcOrderType.CUSTOMER_DELIVERY;
    }

    private Map<String, Long> getRequiredQtyMapByOrderEntries(final OrderEntryGroup oeg) {
        final Map<String, Long> reqiredQtyEntries = new HashMap<>();
        if (CollectionUtils.isNotEmpty(oeg)) {
            for (final AbstractOrderEntryModel entry : oeg) {
                if (entry.getProduct() != null) {
                    reqiredQtyEntries.put(entry.getProduct().getCode(), entry.getQuantity());
                }
            }
        }
        return reqiredQtyEntries;
    }

    /**
     * set required qty into the parameter for checking stock
     * 
     * @param oeg
     */
    public void setRequiredQtyByOrderEntries(final OrderEntryGroup oeg) {
        if (CollectionUtils.isNotEmpty(oeg)) {
            oeg.setParameter(ParameterName.REQUIRED_QTY.name(), getRequiredQtyMapByOrderEntries(oeg));
        }

    }

    /**
     * set required qty
     * 
     * @param oeg
     * @param requiredQty
     */
    public void setRequiredQty(final OrderEntryGroup oeg, final Map<String, Long> requiredQty) {
        if (oeg != null) {
            oeg.setParameter(ParameterName.REQUIRED_QTY.name(), requiredQty);
        }
    }

    /**
     * get required qty
     * 
     * @param oeg
     */
    public Map<String, Long> getRequiredQty(final OrderEntryGroup oeg) {
        if (oeg != null) {
            return (Map<String, Long>)(oeg.getParameter(ParameterName.REQUIRED_QTY.name()));
        }
        return null;
    }

    /**
     * set assigned Qty from order entries
     * 
     * @param oeg
     */
    public void setAssignedQtyByOrderEntries(final OrderEntryGroup oeg) {
        if (CollectionUtils.isNotEmpty(oeg)) {
            oeg.setParameter(ParameterName.ASSIGNED_QTY.name(), getRequiredQtyMapByOrderEntries(oeg));
        }
    }

    /**
     * set assigned qty
     * 
     * @param oeg
     * @param assignedQty
     */
    public void setAssignedQty(final OrderEntryGroup oeg, final Map<String, Long> assignedQty) {
        if (oeg != null) {
            oeg.setParameter(ParameterName.ASSIGNED_QTY.name(), assignedQty);
        }
    }

    /**
     * set assigned qty to list of oeg by order entry qty
     * 
     * @param oegs
     */
    public void setAssignedQtyByOrderEntries(final List<OrderEntryGroup> oegs) {
        if (CollectionUtils.isNotEmpty(oegs)) {
            for (final OrderEntryGroup oeg : oegs) {
                setAssignedQtyByOrderEntries(oeg);
            }
        }
    }

    /**
     * get assigned qty
     * 
     * @param oeg
     * @return Map<String, Long>
     */
    public Map<String, Long> getAssignedQty(final OrderEntryGroup oeg) {
        if (oeg != null) {
            final Map<String, Long> assigedQty = (Map<String, Long>)(oeg
                    .getParameter(ParameterName.ASSIGNED_QTY.name()));
            return assigedQty;
        }
        return null;
    }

    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */
    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }


}

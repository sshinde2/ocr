/**
 * 
 */
package au.com.target.tgtfulfilment.helper;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;


/**
 * @author asingh78
 * 
 */
public class TargetFulfilmentHelper {
    public static final String DISABLE_RESTRICTIONS = "disableRestrictions";
    private SessionService sessionService;

    /**
     * Return the ConsignmentEntry for the given item code, or null if not found.<br/>
     * We are disabling restriction because we want to access non approve product.
     * 
     * @param consignment
     * @param itemCode
     * @return ConsignmentEntryModel or null if not found
     */
    public ConsignmentEntryModel getConsignmentEntryForItemCode(final ConsignmentModel consignment,
            final String itemCode) {
        Assert.notNull(consignment, "consignment cannot be null");
        Assert.notNull(itemCode, "itemCode cannot be null");
        final ConsignmentEntryModel consignmentEntryModel = sessionService
                .executeInLocalView(new SessionExecutionBody() {
                    @Override
                    public Object execute()
                    {
                        if (CollectionUtils.isEmpty(consignment.getConsignmentEntries())) {
                            return null;
                        }
                        // we are disabling restriction because we want to access non approved product
                        sessionService.setAttribute(DISABLE_RESTRICTIONS, Boolean.TRUE);
                        for (final ConsignmentEntryModel consignmentEntry : consignment.getConsignmentEntries()) {
                            final AbstractOrderEntryModel orderEntry = consignmentEntry.getOrderEntry();

                            if (orderEntry != null && orderEntry.getProduct() != null
                                    && itemCode.equals(orderEntry.getProduct().getCode())) {
                                return consignmentEntry;
                            }
                        }
                        return null;
                    }
                });

        return consignmentEntryModel;
    }


    /**
     * Return the item code for the given consignment entry.<br/>
     * We are disabling restriction because we want to access non approve product.
     * 
     * @param consignmentEntry
     * @return item code
     */
    public String getConsignmentEntryItemCode(final ConsignmentEntryModel consignmentEntry) {
        Assert.notNull(consignmentEntry, "consignmentEntry cannot be null");

        final String item = sessionService
                .executeInLocalView(new SessionExecutionBody() {
                    @Override
                    public Object execute()
                    {
                        // we are disabling restriction because we want to access non approved product
                        sessionService.setAttribute(DISABLE_RESTRICTIONS, Boolean.TRUE);
                        final AbstractOrderEntryModel orderEntry = consignmentEntry.getOrderEntry();

                        if (orderEntry != null && orderEntry.getProduct() != null) {

                            return orderEntry.getProduct().getCode();
                        }

                        return null;
                    }
                });

        return item;
    }


    /**
     * @param sessionService
     *            the sessionService to set
     */
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

}

/**
 * 
 */
package au.com.target.tgtfulfilment.helper;

import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;


/**
 * Helper class for working with warehouses.
 * 
 * @author jjayawa1
 *
 */
public class WarehouseHelper {

    /**
     * Return the point of service for the given warehouse if there is one
     * 
     * @param warehouse
     * @return tpos or null
     */
    public TargetPointOfServiceModel getAssignedStoreForWarehouse(final WarehouseModel warehouse) {

        if (warehouse != null) {
            final Collection<PointOfServiceModel> pointsOfService = warehouse.getPointsOfService();
            if (CollectionUtils.isNotEmpty(pointsOfService)) {

                for (final PointOfServiceModel pos : pointsOfService) {

                    if (pos instanceof TargetPointOfServiceModel) {

                        return ((TargetPointOfServiceModel)pos);
                    }
                }
            }
        }

        return null;
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.impl;

import de.hybris.platform.core.model.order.OrderModel;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.order.strategies.TaxInvoiceGenerationDenialStrategy;
import au.com.target.tgtfulfilment.salesapplication.TargetFulfilmentSalesApplicationConfigService;


/**
 * @author Vivek
 *
 */
public class SalesChannelTaxInvoiceGenerationDenialStrategy implements TaxInvoiceGenerationDenialStrategy {

    private TargetFulfilmentSalesApplicationConfigService salesApplicationConfigService;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.order.strategies.TaxInvoiceGenerationDenialStrategy#isDenied(de.hybris.platform.core.model.order.OrderModel)
     */
    @Override
    public boolean isDenied(final OrderModel order) {
        Assert.notNull(order, "Order can't be null!");
        return salesApplicationConfigService.isSuppressTaxInvoice(order.getSalesApplication());
    }

    /**
     * @param salesApplicationConfigService
     *            the salesApplicationConfigService to set
     */
    @Required
    public void setSalesApplicationConfigService(
            final TargetFulfilmentSalesApplicationConfigService salesApplicationConfigService) {
        this.salesApplicationConfigService = salesApplicationConfigService;
    }

}

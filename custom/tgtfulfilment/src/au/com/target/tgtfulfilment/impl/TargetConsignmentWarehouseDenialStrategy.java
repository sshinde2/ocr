/**
 * 
 */
package au.com.target.tgtfulfilment.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.returns.strategy.ReturnableCheck;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.util.Assert;


/**
 * @author pratik
 *
 */
public class TargetConsignmentWarehouseDenialStrategy implements ReturnableCheck {

    @Override
    public boolean perform(final OrderModel order, final AbstractOrderEntryModel orderEntry, final long quantity) {
        Assert.notNull(order, "Order can't be null!");
        Assert.notNull(orderEntry, "Order Entry can't be null!");

        final WarehouseModel warehouse = getActiveWarehouseForOrderEntry(orderEntry);

        if (warehouse == null || BooleanUtils.isTrue(Boolean.valueOf(warehouse.isDenyRefund()))) {
            return false;
        }
        return true;
    }

    /**
     * Method to get the warehouse from active consignment for the order entry.
     * 
     * @param orderEntry
     * @return warehouse
     */
    private WarehouseModel getActiveWarehouseForOrderEntry(final AbstractOrderEntryModel orderEntry) {
        if (CollectionUtils.isNotEmpty(orderEntry.getConsignmentEntries())) {
            for (final ConsignmentEntryModel consignmentEntry : orderEntry.getConsignmentEntries()) {
                final ConsignmentModel consignment = consignmentEntry.getConsignment();
                if (consignment != null
                        && ConsignmentStatus.SHIPPED.getCode().equals(consignment.getStatus().getCode())) {
                    return consignment.getWarehouse();
                }
            }
        }
        return null;
    }
}

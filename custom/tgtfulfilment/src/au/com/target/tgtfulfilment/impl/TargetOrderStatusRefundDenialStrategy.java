/**
 * 
 */
package au.com.target.tgtfulfilment.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.strategy.ReturnableCheck;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;


public class TargetOrderStatusRefundDenialStrategy extends AbstractBusinessService implements
        ReturnableCheck {

    private List<OrderStatus> allowedOrderStatuses;

    /* (non-Javadoc)
     * @see de.hybris.platform.returns.strategy.ReturnableCheck#perform(de.hybris.platform.core.model.order.OrderModel, de.hybris.platform.core.model.order.AbstractOrderEntryModel, long)
     */
    @Override
    public boolean perform(final OrderModel order, final AbstractOrderEntryModel entry, final long returnQuantity) {
        return allowedOrderStatuses.contains(order.getStatus());
    }

    /**
     * @param allowedOrderStatuses
     *            the allowedOrderStatuses to set
     */
    @Required
    public void setAllowedOrderStatuses(final List<OrderStatus> allowedOrderStatuses) {
        this.allowedOrderStatuses = allowedOrderStatuses;
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.integration.converters.sendtowarehouse;

import de.hybris.platform.core.model.user.AddressModel;

import au.com.target.tgtcore.converters.TargetConverter;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.AddressDTO;


/**
 * @author Vivek
 *
 */
public class SendToWarehouseAddressConverter implements TargetConverter<AddressModel, AddressDTO> {

    /**
     * {@inheritDoc}
     */
    @Override
    public AddressDTO convert(final AddressModel source) {

        if (null != source) {
            final AddressDTO shippingAddress = new AddressDTO();

            shippingAddress.setName(source.getFirstname() + " " + source.getLastname());
            final String[] address = { source.getLine1(), source.getLine2() };
            shippingAddress.setAddressLine(address);
            shippingAddress.setCity(source.getTown());
            shippingAddress.setState(source.getDistrict());
            if (null != source.getCountry()) {
                shippingAddress.setCountry(source.getCountry().getName());
            }
            shippingAddress.setPostcode(source.getPostalcode());
            shippingAddress.setPhone(source.getPhone1());

            return shippingAddress;
        }

        return null;
    }

}

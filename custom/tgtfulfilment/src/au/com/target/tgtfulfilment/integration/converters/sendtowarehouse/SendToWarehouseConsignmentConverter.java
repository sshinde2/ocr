/**
 * 
 */
package au.com.target.tgtfulfilment.integration.converters.sendtowarehouse;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.converters.TargetConverter;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.util.ProductUtil;
import au.com.target.tgtfulfilment.constants.TgtfulfilmentConstants;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ConsignmentDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ConsignmentEntryDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.OrderDTO;


/**
 * ConsignmentModel to DTO converter.
 * 
 * @author jjayawa1
 *
 */
public class SendToWarehouseConsignmentConverter implements TargetConverter<ConsignmentModel, ConsignmentDTO> {

    private SendToWarehouseConsignmentEntryConverter sendToWarehouseConsignmentEntryConverter;
    private SendToWarehouseOrderConverter sendToWarehouseOrderConverter;
    private TargetSharedConfigService targetSharedConfigService;



    /**
     * {@inheritDoc}
     */
    @Override
    public ConsignmentDTO convert(final ConsignmentModel source) {
        Assert.notNull(source, "ConsignmentModel cannot be null");
        final ConsignmentDTO consignment = new ConsignmentDTO();
        consignment.setId(source.getCode());

        final WarehouseModel warehouse = source.getWarehouse();
        if (warehouse != null) {
            consignment.setWarehouseId(warehouse.getCode());
        }

        final AbstractOrderModel order = source.getOrder();
        if (order != null) {
            if (order instanceof OrderModel) {
                final OrderDTO orderDTO = sendToWarehouseOrderConverter.convert((OrderModel)order);
                consignment.setOrder(orderDTO);
            }
        }

        final Set<ConsignmentEntryModel> consignmentEntries = source.getConsignmentEntries();
        final List<ConsignmentEntryDTO> entries = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(consignmentEntries)) {
            //Get the first consignment entry to check the productype
            final ConsignmentEntryModel consignmentEntry = consignmentEntries.iterator().next();
            String productType = null;
            long quantitySum = 0;
            if (null != consignmentEntry.getOrderEntry()) {
                productType = ProductUtil.getProductTypeCode(consignmentEntry.getOrderEntry()
                        .getProduct());
            }
            for (final ConsignmentEntryModel entry : consignmentEntries) {
                entries.add(sendToWarehouseConsignmentEntryConverter.convert(entry));
                quantitySum = quantitySum + entry.getQuantity().longValue();
            }
            setShippingMethod(consignment, quantitySum, productType);
        }
        consignment.setConsignmentEntries(entries);
        return consignment;
    }


    private void setShippingMethod(final ConsignmentDTO consignment, final long quantity, final String productType) {
        if (ProductTypeModel.DIGITAL.equalsIgnoreCase(productType)) {
            consignment.setShippingMethod(TgtfulfilmentConstants.GIFTCARD_SHIPPING_METHOD_EMAIL);
        }
        if (TgtCoreConstants.PHYSICAL_GIFTCARD.equalsIgnoreCase(productType)) {
            if (quantity > getSmallShipmentLimit()) {
                consignment.setShippingMethod(TgtfulfilmentConstants.GIFTCARD_SHIPPING_METHOD_LARGE);
            }
            else {
                consignment.setShippingMethod(TgtfulfilmentConstants.GIFTCARD_SHIPPING_METHOD_SMALL);
            }
        }

    }

    private int getSmallShipmentLimit() {
        return targetSharedConfigService
                .getInt(TgtfulfilmentConstants.Config.GIFTCARDS_SMALL_SHIPMENT_LIMIT, 2);
    }


    /**
     * @param sendToWarehouseConsignmentEntryConverter
     *            the sendToWarehouseConsignmentEntryConverter to set
     */
    @Required
    public void setSendToWarehouseConsignmentEntryConverter(
            final SendToWarehouseConsignmentEntryConverter sendToWarehouseConsignmentEntryConverter) {
        this.sendToWarehouseConsignmentEntryConverter = sendToWarehouseConsignmentEntryConverter;
    }

    /**
     * @param sendToWarehouseOrderConverter
     *            the sendToWarehouseOrderConverter to set
     */
    @Required
    public void setSendToWarehouseOrderConverter(final SendToWarehouseOrderConverter sendToWarehouseOrderConverter) {
        this.sendToWarehouseOrderConverter = sendToWarehouseOrderConverter;
    }


    /**
     * @param targetSharedConfigService
     *            the targetSharedConfigService to set
     */
    @Required
    public void setTargetSharedConfigService(final TargetSharedConfigService targetSharedConfigService) {
        this.targetSharedConfigService = targetSharedConfigService;
    }

}

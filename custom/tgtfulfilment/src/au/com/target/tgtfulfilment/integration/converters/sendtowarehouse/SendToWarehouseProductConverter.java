/**
 * 
 */
package au.com.target.tgtfulfilment.integration.converters.sendtowarehouse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.converters.TargetConverter;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.GiftCardModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfulfilment.constants.TgtfulfilmentConstants;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ProductDTO;
import au.com.target.tgtutility.util.TargetProductUtils;


/**
 * Convert ProductModel to DTO
 * 
 * @author jjayawa1
 *
 */
public class SendToWarehouseProductConverter implements TargetConverter<AbstractTargetVariantProductModel, ProductDTO> {

    private TargetFeatureSwitchService targetFeatureSwitchService;

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductDTO convert(final AbstractTargetVariantProductModel source) {
        Assert.notNull(source, "TargetSizeVariantProductModel cannot be null");
        final ProductDTO product = new ProductDTO();
        final AbstractTargetVariantProductModel variantProduct = source;

        // Get gift card brand id from the base product
        final TargetProductModel baseProduct = TargetProductUtils.getBaseTargetProduct(variantProduct);
        if (baseProduct != null) {
            final GiftCardModel giftCardModel = baseProduct.getGiftCard();
            if (giftCardModel != null) {
                product.setBrandId(giftCardModel.getBrandId());
            }
        }

        // Get gift card style from the colour variant
        final TargetColourVariantProductModel colourProduct = TargetProductUtils
                .getColourVariantProduct(variantProduct);
        if (colourProduct != null) {
            product.setGiftCardStyle(colourProduct.getGiftCardStyle());
        }

        // Rest from the size
        product.setId(variantProduct.getCode());
        final String ean = variantProduct.getEan();
        if (StringUtils.isNotBlank(ean)) {
            product.setEan(ean);
        }
        product.setName(variantProduct.getName());

        if (variantProduct instanceof TargetSizeVariantProductModel) {
            final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)variantProduct;
            final Double denomination = sizeVariant.getDenomination();
            if (null != denomination) {
                product.setDenomination(String.valueOf(denomination));
            }
            if (targetFeatureSwitchService
                    .isFeatureEnabled(TgtfulfilmentConstants.FEATURE_WEBMETHOD_ORDEREXTRACT_PRODUCT_ID)) {
                product.setProductId(sizeVariant.getGiftCardProductId());
            }
        }

        return product;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }
}

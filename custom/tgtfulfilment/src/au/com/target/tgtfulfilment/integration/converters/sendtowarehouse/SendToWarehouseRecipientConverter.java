/**
 * 
 */
package au.com.target.tgtfulfilment.integration.converters.sendtowarehouse;


import org.springframework.util.Assert;

import au.com.target.tgtcore.converters.TargetConverter;
import au.com.target.tgtcore.model.GiftRecipientModel;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.RecipientDTO;
import au.com.target.tgtfulfilment.util.EscapeSpecialCharactersUtil;


/**
 * Convert GiftRecipientModel to RecipientDTO.
 * 
 * @author jjayawa1
 *
 */
public class SendToWarehouseRecipientConverter implements TargetConverter<GiftRecipientModel, RecipientDTO> {

    /**
     * {@inheritDoc}
     */
    @Override
    public RecipientDTO convert(final GiftRecipientModel source) {
        Assert.notNull(source, "GiftRecipientModel cannot be null");
        final RecipientDTO recipient = new RecipientDTO();
        recipient.setEmailAddress(source.getEmail());
        recipient.setFirstName(EscapeSpecialCharactersUtil.escapeSpecialXMLCharacters(source.getFirstName()));
        recipient.setLastName(EscapeSpecialCharactersUtil.escapeSpecialXMLCharacters(source.getLastName()));
        recipient.setMessage(source.getMessageText());
        return recipient;
    }

}

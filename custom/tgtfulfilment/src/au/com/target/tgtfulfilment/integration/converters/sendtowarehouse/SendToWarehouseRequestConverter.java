/**
 * 
 */
package au.com.target.tgtfulfilment.integration.converters.sendtowarehouse;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.converters.TargetConverter;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ConsignmentDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseRequest;


/**
 * ConsignmentModel to SendToWarehouseRequest converter.
 * 
 * @author jjayawa1
 *
 */
public class SendToWarehouseRequestConverter implements
        TargetConverter<ConsignmentModel, SendToWarehouseRequest> {

    private SendToWarehouseConsignmentConverter sendToWarehouseConsignmentConverter;

    /**
     * {@inheritDoc}
     */
    @Override
    public SendToWarehouseRequest convert(final ConsignmentModel source) {
        Assert.notNull(source, "ConsignmentModel cannot be null");
        final SendToWarehouseRequest request = new SendToWarehouseRequest();
        final ConsignmentDTO consignment = sendToWarehouseConsignmentConverter.convert(source);
        request.setConsignment(consignment);
        return request;
    }

    /**
     * @param sendToWarehouseConsignmentConverter
     *            the sendToWarehouseConsignmentConverter to set
     */
    @Required
    public void setSendToWarehouseConsignmentConverter(
            final SendToWarehouseConsignmentConverter sendToWarehouseConsignmentConverter) {
        this.sendToWarehouseConsignmentConverter = sendToWarehouseConsignmentConverter;
    }

}

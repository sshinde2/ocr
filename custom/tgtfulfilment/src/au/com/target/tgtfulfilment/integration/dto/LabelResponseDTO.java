/**
 * 
 */
package au.com.target.tgtfulfilment.integration.dto;

/**
 * Response for pdf request.
 * 
 * @author jjayawa1
 *
 */
public class LabelResponseDTO extends BaseResponseDTO {
    private byte[] data;

    /**
     * @return the data
     */
    public byte[] getData() {
        return data;
    }

    /**
     * @param data
     *            the data to set
     */
    public void setData(final byte[] data) {
        this.data = data;
    }

}

/**
 * 
 */
package au.com.target.tgtfulfilment.integration.dto.pollwarehouse;

import au.com.target.tgtfulfilment.integration.dto.BaseResponseDTO;


/**
 * poll warehouse response DTO
 * 
 * @author gsing236
 *
 */
public class PollWarehouseResponse extends BaseResponseDTO {

    /**
     * Convenience method to get a success response.
     * 
     * @return SendToWarehouseResponse
     */
    public static PollWarehouseResponse getSuccessResponse() {
        final PollWarehouseResponse response = new PollWarehouseResponse();
        response.setSuccess(true);
        return response;
    }

}

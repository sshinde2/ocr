/**
 * 
 */
package au.com.target.tgtfulfilment.integration.dto.sendtowarehouse;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * @author Vivek
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class AddressDTO {
    private String name;
    private String[] addressLine;
    private String city;
    private String state;
    private String country;
    private String postcode;
    private String phone;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the addressLine
     */
    public String[] getAddressLine() {
        return addressLine;
    }

    /**
     * @param addressLine
     *            the addressLine to set
     */
    public void setAddressLine(final String[] addressLine) {
        this.addressLine = addressLine;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city
     *            the city to set
     */
    public void setCity(final String city) {
        this.city = city;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(final String state) {
        this.state = state;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country
     *            the country to set
     */
    public void setCountry(final String country) {
        this.country = country;
    }

    /**
     * @return the postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * @param postcode
     *            the postcode to set
     */
    public void setPostcode(final String postcode) {
        this.postcode = postcode;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     *            the phone to set
     */
    public void setPhone(final String phone) {
        this.phone = phone;
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.integration.dto.sendtowarehouse;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * ConsignmentEntry DTO
 * 
 * @author jjayawa1
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ConsignmentEntryDTO {
    private ProductDTO product;
    private List<RecipientDTO> recipients;
    private String quantity;


    /**
     * @return the quantity
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(final String quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the product
     */
    public ProductDTO getProduct() {
        return product;
    }

    /**
     * @param product
     *            the product to set
     */
    public void setProduct(final ProductDTO product) {
        this.product = product;
    }

    /**
     * @return the recipients
     */
    public List<RecipientDTO> getRecipients() {
        return recipients;
    }

    /**
     * @param recipients
     *            the recipients to set
     */
    public void setRecipients(final List<RecipientDTO> recipients) {
        this.recipients = recipients;
    }

}

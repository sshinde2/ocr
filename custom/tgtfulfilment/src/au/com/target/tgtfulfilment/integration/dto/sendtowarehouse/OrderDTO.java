/**
 * 
 */
package au.com.target.tgtfulfilment.integration.dto.sendtowarehouse;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Order DTO
 * 
 * @author jjayawa1
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class OrderDTO {
    private String id;
    private CustomerDTO customer;
    private AddressDTO shippingAddress;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * @return the customer
     */
    public CustomerDTO getCustomer() {
        return customer;
    }

    /**
     * @param customer
     *            the customer to set
     */
    public void setCustomer(final CustomerDTO customer) {
        this.customer = customer;
    }

    /**
     * @return the shippingAddress
     */
    public AddressDTO getShippingAddress() {
        return shippingAddress;
    }

    /**
     * @param shippingAddress
     *            the shippingAddress to set
     */
    public void setShippingAddress(final AddressDTO shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

}

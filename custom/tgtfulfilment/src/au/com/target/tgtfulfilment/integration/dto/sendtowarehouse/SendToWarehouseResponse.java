/**
 * 
 */
package au.com.target.tgtfulfilment.integration.dto.sendtowarehouse;

import au.com.target.tgtfulfilment.integration.dto.BaseResponseDTO;


/**
 * SendToWarehouseResponse DTO
 * 
 * @author jjayawa1
 *
 */
public class SendToWarehouseResponse extends BaseResponseDTO {

    /**
     * Convenience method to get a success response.
     * 
     * @return SendToWarehouseResponse
     */
    public static SendToWarehouseResponse getSuccessRespose() {
        final SendToWarehouseResponse response = new SendToWarehouseResponse();
        response.setSuccess(true);
        return response;
    }
}

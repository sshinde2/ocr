/**
 * 
 */

package au.com.target.tgtfulfilment.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.storeblackout.StoreBlackOutService;


/**
 * Interceptor to validate the start date and end date values for 'GlobalStoreFulfilmentCapabilities' model
 * 
 * @author Pratik
 *
 */
public class GlobalStoreFulfilmentCapabilitiesValidateInterceptor implements ValidateInterceptor {

    private static final String MAX_ORDER_PERIOD_TIME_PATTERN = "^\\d\\d:\\d\\d$";
    private StoreBlackOutService storeBlackOutService;

    /**
     * Method to validate is start date of blackout period is after end date of blackout period. <br \>
     * NOTE : If both the dates are equal then no exception is thrown by the interceptor.
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof GlobalStoreFulfilmentCapabilitiesModel) {
            final GlobalStoreFulfilmentCapabilitiesModel globalFulfilmentModel = (GlobalStoreFulfilmentCapabilitiesModel)model;
            storeBlackOutService.validateDates(globalFulfilmentModel.getBlackoutStart(),
                    globalFulfilmentModel.getBlackoutEnd());
            final String maxOrderPeriodStartTime = globalFulfilmentModel.getMaxOrderPeriodStartTime();
            if (null != maxOrderPeriodStartTime) {
                final Pattern pattern = Pattern.compile(MAX_ORDER_PERIOD_TIME_PATTERN);
                if (pattern.matcher(maxOrderPeriodStartTime).matches()) {
                    final String[] parsedInput = maxOrderPeriodStartTime.split(":");
                    if (Integer.valueOf(parsedInput[0]).intValue() < 0
                            || Integer.valueOf(parsedInput[0]).intValue() >= 24) {
                        throw new InterceptorException("HH should be between 0-23 and not " + maxOrderPeriodStartTime);
                    }
                    if (Integer.valueOf(parsedInput[1]).intValue() < 0
                            || Integer.valueOf(parsedInput[1]).intValue() >= 60) {
                        throw new InterceptorException("MM should be between 0-59 and not " + maxOrderPeriodStartTime);
                    }
                }
                else {
                    throw new InterceptorException("Input should be of type HH:MM - 24 HOURS format and not "
                            + maxOrderPeriodStartTime);
                }
            }
            else {
                throw new InterceptorException("maxOrderPeriodStartTime must not be null");
            }
        }
    }

    /**
     * @param storeBlackOutService
     *            the storeBlackOutService to set
     */
    @Required
    public void setStoreBlackOutService(final StoreBlackOutService storeBlackOutService) {
        this.storeBlackOutService = storeBlackOutService;
    }
}

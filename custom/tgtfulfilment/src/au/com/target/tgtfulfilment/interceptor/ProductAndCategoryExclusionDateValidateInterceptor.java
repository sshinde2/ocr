/**
 * 
 */
package au.com.target.tgtfulfilment.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.text.MessageFormat;
import java.util.Date;

import au.com.target.tgtcore.model.AbstractExclusionsModel;


/**
 * @author bhuang3
 *
 */
public class ProductAndCategoryExclusionDateValidateInterceptor implements
        ValidateInterceptor<AbstractExclusionsModel> {


    @Override
    public void onValidate(final AbstractExclusionsModel exclusionModel, final InterceptorContext context)
            throws InterceptorException {
        Date startDate = null;
        Date endDate = null;
        startDate = exclusionModel.getExclusionStartDate();
        endDate = exclusionModel.getExclusionEndDate();
        if (startDate != null && endDate != null) {
            if (endDate.before(startDate)) {
                throw new InterceptorException(MessageFormat.format(
                        "Exclude start date {0} should be before exclude end date {1}", startDate.toString(),
                        endDate.toString()));
            }
        }
    }
}

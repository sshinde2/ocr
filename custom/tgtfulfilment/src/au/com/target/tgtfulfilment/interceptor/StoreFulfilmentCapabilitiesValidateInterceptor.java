/**
 * 
 */
package au.com.target.tgtfulfilment.interceptor;

import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.Date;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.joda.time.DateTimeComparator;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.storeblackout.StoreBlackOutService;
import au.com.target.tgtutility.util.PhoneValidationUtils;


/**
 * Interceptor to validate the Mandatory Entries for 'StoreFulfilmentCapabilities' model
 * 
 * @author siddharam
 *
 */
public class StoreFulfilmentCapabilitiesValidateInterceptor implements ValidateInterceptor {
    private static final String INTER_STORE_DELIVERY_ALLOWED = "Inter Store Delivery Enabled Store";
    private static final String HOME_DELIVERY_ENABLED_STORE = "Customer Delivery Enabled Store";

    private StoreBlackOutService storeBlackOutService;

    /**
     * Method to validate is start date of blackout period is after end date of blackout period. <br \>
     * NOTE : If both the dates are equal then no exception is thrown by the interceptor.
     * 
     * @throws InterceptorException
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof StoreFulfilmentCapabilitiesModel) {
            final StoreFulfilmentCapabilitiesModel storeFulfilmentModel = (StoreFulfilmentCapabilitiesModel)model;

            final String mobileNumber = storeFulfilmentModel.getMobileNumber();
            if (mobileNumber != null && !PhoneValidationUtils.validatePhoneNumber(mobileNumber)) {
                throw new InterceptorException("Phone number must be valid");
            }
            storeBlackOutService.validateDates(storeFulfilmentModel.getBlackoutStart(),
                    storeFulfilmentModel.getBlackoutEnd());

            final Date notificationStartTime = storeFulfilmentModel.getNotificationStartTime();
            final Date notificationEndTime = storeFulfilmentModel.getNotificationEndTime();

            if (BooleanUtils.isTrue(storeFulfilmentModel.getNotificationsEnabled())) {
                if (null == notificationStartTime || null == notificationEndTime) {
                    throw new InterceptorException(
                            "Notification start time and end time must not be empty when notifications are enabled");
                }
                if (DateTimeComparator.getTimeOnlyInstance().compare(notificationStartTime, notificationEndTime) > 0) {
                    throw new InterceptorException(
                            "Notification start time must be less than the notification end time");
                }
            }
            if (BooleanUtils.isTrue(storeFulfilmentModel.getAllowDeliveryToAnotherStore())
                    && null == storeFulfilmentModel.getMaxConsignmentsForDeliveryPerDay()) {
                throw new InterceptorException(
                        "MaxOrderForDeliveryPerDay cannot be null for : " + INTER_STORE_DELIVERY_ALLOWED);
            }
            else {
                final Set<TargetZoneDeliveryModeModel> deliveryModelList = storeFulfilmentModel
                        .getDeliveryModes();
                if (CollectionUtils.isNotEmpty(deliveryModelList)) {
                    for (final TargetZoneDeliveryModeModel deliveryType : deliveryModelList) {
                        if (BooleanUtils.isFalse(deliveryType.getIsDeliveryToStore())
                                && null == storeFulfilmentModel.getMaxConsignmentsForDeliveryPerDay()) {
                            throw new InterceptorException(
                                    "MaxOrderForDeliveryPerDay cannot be null for : " + HOME_DELIVERY_ENABLED_STORE);
                        }
                    }
                }
            }
            if (null != storeFulfilmentModel.getMaxAllowedProductWeightForDelivery() && storeFulfilmentModel
                    .getMaxAllowedProductWeightForDelivery().doubleValue() < 0) {
                throw new InterceptorException(
                        "Invalid : Value for allowed Weight should not be less than 0");
            }
            if (null != storeFulfilmentModel.getMaxConsignmentsForDeliveryPerDay() && storeFulfilmentModel
                    .getMaxConsignmentsForDeliveryPerDay().intValue() < 0) {
                throw new InterceptorException(
                        "Invalid : Value of max consignments should not be less than 0");
            }
        }
    }

    /**
     * @param storeBlackOutService
     *            the storeBlackOutService to set
     */
    @Required
    public void setStoreBlackOutService(final StoreBlackOutService storeBlackOutService) {
        this.storeBlackOutService = storeBlackOutService;
    }
}

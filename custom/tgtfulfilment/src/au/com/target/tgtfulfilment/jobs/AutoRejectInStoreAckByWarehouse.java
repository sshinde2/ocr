/**
 * 
 */
package au.com.target.tgtfulfilment.jobs;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.fulfilmentservice.TargetFulfilmentService;
import au.com.target.tgtfulfilment.logger.InstoreFulfilmentLogger;
import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;


/**
 * This cron job will re-route the orders to fastline those not accepted by the store and those created outside the
 * allowed tolerance time
 * 
 * @author ajit
 *
 */
public class AutoRejectInStoreAckByWarehouse extends AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(AutoRejectInStoreAckByWarehouse.class);
    private static final InstoreFulfilmentLogger INSTORE_LOGGER = new InstoreFulfilmentLogger(
            AutoRejectInStoreAckByWarehouse.class);


    private TargetFulfilmentService targetFulfilmentService;

    private TargetStoreConsignmentService targetStoreConsignmentService;

    private TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService;

    private List<String> excludedStates;

    private List<String> includedStates;

    private TargetConsignmentService targetConsignmentService;

    /**
     * (non-Javadoc)
     * 
     * @see de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.CronJobModel)
     */
    @Override
    public PerformResult perform(final CronJobModel paramT) {

        final List<TargetConsignmentModel> consignmentList;
        final List<ConsignmentStatus> statusList = Arrays.asList(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE,
                ConsignmentStatus.SENT_TO_WAREHOUSE);

        if (CollectionUtils.isNotEmpty(excludedStates)) {
            consignmentList = targetStoreConsignmentService.getConsignmentsForStoreNotInState(
                    statusList,
                    excludedStates);
        }
        else if (CollectionUtils.isNotEmpty(includedStates)) {
            consignmentList = targetStoreConsignmentService.getConsignmentsForStoreInState(
                    statusList,
                    includedStates);
        }
        else {
            consignmentList = targetStoreConsignmentService
                    .getConsignmentsOfMultiplesStatusesForAllStores(statusList);
        }

        LOG.info("Count of Confirmed by warehouse Consignments" + consignmentList.size());
        final Date currentDate = new Date();
        if (CollectionUtils.isNotEmpty(consignmentList)) {
            final int pullBackTolerance = getPullBackTolerance();
            for (final TargetConsignmentModel consignmentModel : consignmentList) {
                //Don't do auto reject for default warehouse
                // or if consignment serviced by Fluent Service Point 
                if (isDefaultWarehouse(consignmentModel)
                        || targetConsignmentService.isConsignmentServicedByFluentServicePoint(consignmentModel)) {
                    continue;
                }
                // Checking if consignment was created outside the pull back tolerance time
                if ((TimeUnit.MILLISECONDS.toMinutes(currentDate.getTime()
                        - consignmentModel.getCreationtime().getTime()) > pullBackTolerance)) {
                    final Integer storeNumber = targetStoreConsignmentService
                            .getAssignedStoreForConsignmentAsInteger(consignmentModel);
                    try {
                        INSTORE_LOGGER.logStoreConsignmentAction("AutoRejectConsignment",
                                storeNumber,
                                targetStoreConsignmentService.getOrderCodeFromConsignment(consignmentModel),
                                consignmentModel.getCode(),
                                targetStoreConsignmentService.getDeliverFromStateFromConsignment(consignmentModel),
                                targetStoreConsignmentService.getDeliverToStoreNumberFromConsignment(consignmentModel),
                                targetStoreConsignmentService.getDeliverToStateFromConsignment(consignmentModel));
                        targetFulfilmentService.processRerouteAutoTimeoutConsignment(consignmentModel,
                                ConsignmentRejectReason.ACCEPT_TIMEOUT);
                    }
                    catch (final Exception e) {
                        INSTORE_LOGGER.logStoreConsignmentActionError("AutoRejectConsignment", storeNumber,
                                consignmentModel.getCode(), e);
                    }
                }
            }
        }

        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    /**
     * Check default warehouse for the consignment
     * 
     * @param consignmentModel
     * @return boolean
     */
    private boolean isDefaultWarehouse(final TargetConsignmentModel consignmentModel) {
        final WarehouseModel warehouse = consignmentModel.getWarehouse();
        return warehouse != null ? (warehouse.getDefault() != null ? warehouse.getDefault().booleanValue() : false)
                : false;
    }

    /**
     * Method to get pull back tolerance from GlobalFulfilmentCapabilities model, if the model doesn't exist then pull
     * back tolerance is 0
     * 
     * @return pullBackTolerance
     */
    private int getPullBackTolerance() {
        final GlobalStoreFulfilmentCapabilitiesModel globalCapabilities = targetGlobalStoreFulfilmentService
                .getGlobalStoreFulfilmentCapabilites();
        int pullBackTolerance = 0;
        if (globalCapabilities != null) {
            if (globalCapabilities.getPullBackTolerance() != null) {
                pullBackTolerance = globalCapabilities.getPullBackTolerance().intValue();
            }
        }
        return pullBackTolerance;
    }

    /**
     * @param targetFulfilmentService
     *            the targetFulfilmentService to set
     */
    @Required
    public void setTargetFulfilmentService(final TargetFulfilmentService targetFulfilmentService) {
        this.targetFulfilmentService = targetFulfilmentService;
    }

    /**
     * @param targetStoreConsignmentService
     *            the targetStoreConsignmentService to set
     */
    @Required
    public void setTargetStoreConsignmentService(final TargetStoreConsignmentService targetStoreConsignmentService) {
        this.targetStoreConsignmentService = targetStoreConsignmentService;
    }

    /**
     * @param targetGlobalStoreFulfilmentService
     *            the targetGlobalStoreFulfilmentService to set
     */
    @Required
    public void setTargetGlobalStoreFulfilmentService(
            final TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService) {
        this.targetGlobalStoreFulfilmentService = targetGlobalStoreFulfilmentService;
    }

    /**
     * @param excludedStates
     *            the list of states from which pullback should be excluded
     */
    public void setExcludedStates(final List<String> excludedStates) {
        this.excludedStates = excludedStates;
    }

    /**
     * @param includedStates
     *            the list of states from which pullback should be included
     */
    public void setIncludedStates(final List<String> includedStates) {
        this.includedStates = includedStates;
    }

    /**
     * @param targetConsignmentService
     *            the targetConsignmentService to set
     */
    @Required
    public void setTargetConsignmentService(final TargetConsignmentService targetConsignmentService) {
        this.targetConsignmentService = targetConsignmentService;
    }

}

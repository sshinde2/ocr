/**
 * 
 */
package au.com.target.tgtfulfilment.jobs;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.fulfilmentservice.TargetFulfilmentService;
import au.com.target.tgtfulfilment.logger.InstoreFulfilmentLogger;
import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;


/**
 * This cron job is used to re-route the consignments to central warehouse those not accepted by the store
 * 
 * @author siddharam
 *
 */
public class AutoRejectInStoreWavedJob extends AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(AutoRejectInStoreWavedJob.class);
    private static final InstoreFulfilmentLogger INSTORE_LOGGER = new InstoreFulfilmentLogger(
            AutoRejectInStoreWavedJob.class);

    private TargetStoreConsignmentService targetStoreConsignmentService;

    private TargetFulfilmentService targetFulfilmentService;

    private TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService;

    private TargetConsignmentService targetConsignmentService;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.CronJobModel)
     */
    @Override
    public PerformResult perform(final CronJobModel cron) {
        final int maxTimeToPick = getMaxTimeToPickUp();
        final Date currentDate = new Date();
        final List<TargetConsignmentModel> consignments = targetStoreConsignmentService
                .getConsignmentsForAllStore(ConsignmentStatus.WAVED);
        LOG.info("Count of Waved Consignments" + consignments.size());
        LOG.info("Max time to Pick" + maxTimeToPick);
        if (maxTimeToPick > 0) {
            for (final TargetConsignmentModel consignment : consignments) {

                //Skip the default warehouse or if consignment is serviced by Fluent ServicePoint
                if (isDefaultWarehouse(consignment)
                        || targetConsignmentService.isConsignmentServicedByFluentServicePoint(consignment)) {
                    continue;
                }

                final Integer storeNumber = targetStoreConsignmentService
                        .getAssignedStoreForConsignmentAsInteger(consignment);

                if (consignment.getWavedDate() != null) {
                    if ((TimeUnit.MILLISECONDS.toMinutes(currentDate.getTime()
                            - consignment.getWavedDate().getTime()) > maxTimeToPick)) {

                        try {
                            INSTORE_LOGGER.logStoreConsignmentAction("AutoRejectWavedConsignment", storeNumber,
                                    targetStoreConsignmentService.getOrderCodeFromConsignment(consignment),
                                    consignment.getCode(),
                                    targetStoreConsignmentService.getDeliverFromStateFromConsignment(consignment),
                                    targetStoreConsignmentService.getDeliverToStoreNumberFromConsignment(consignment),
                                    targetStoreConsignmentService.getDeliverToStateFromConsignment(consignment));
                            targetFulfilmentService.processRerouteAutoTimeoutConsignment(consignment,
                                    ConsignmentRejectReason.PICK_TIMEOUT);
                        }
                        catch (final Exception e) {
                            INSTORE_LOGGER.logStoreConsignmentActionError("AutoRejectWavedConsignment", storeNumber,
                                    consignment.getCode(), e);
                        }
                    }
                }
                else {
                    INSTORE_LOGGER.logStoreConsignmentActionError("AutoRejectWavedConsignment", storeNumber,
                            consignment.getCode(), "Empty waved date");
                }
            }
        }

        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    /**
     * Check default warehouse for the consignment
     * 
     * @param consignmentModel
     * @return boolean
     */
    private boolean isDefaultWarehouse(final TargetConsignmentModel consignmentModel) {
        final WarehouseModel warehouse = consignmentModel.getWarehouse();
        return warehouse != null ? (warehouse.getDefault() != null ? warehouse.getDefault().booleanValue() : false)
                : false;
    }


    /**
     * @return the minutes
     */
    private int getMaxTimeToPickUp() {
        final GlobalStoreFulfilmentCapabilitiesModel globalStoreFulfilmentCapabilitiesModel = targetGlobalStoreFulfilmentService
                .getGlobalStoreFulfilmentCapabilites();
        int maxTimeToPick = 0;
        if (globalStoreFulfilmentCapabilitiesModel != null) {
            maxTimeToPick = globalStoreFulfilmentCapabilitiesModel
                    .getMaxTimeToPick().intValue();
        }
        return maxTimeToPick;
    }

    /**
     * @param targetFulfilmentService
     *            the targetFulfilmentService to set
     */
    @Required
    public void setTargetFulfilmentService(final TargetFulfilmentService targetFulfilmentService) {
        this.targetFulfilmentService = targetFulfilmentService;
    }

    /**
     * @param targetGlobalStoreFulfilmentService
     *            the targetGlobalStoreFulfilmentService to set
     */
    @Required
    public void setTargetGlobalStoreFulfilmentService(
            final TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService) {
        this.targetGlobalStoreFulfilmentService = targetGlobalStoreFulfilmentService;
    }

    /**
     * @param targetStoreConsignmentService
     *            the targetStoreConsignmentService to set
     */
    @Required
    public void setTargetStoreConsignmentService(final TargetStoreConsignmentService targetStoreConsignmentService) {
        this.targetStoreConsignmentService = targetStoreConsignmentService;
    }

    /**
     * @param targetConsignmentService
     *            the targetConsignmentService to set
     */
    @Required
    public void setTargetConsignmentService(final TargetConsignmentService targetConsignmentService) {
        this.targetConsignmentService = targetConsignmentService;
    }

}

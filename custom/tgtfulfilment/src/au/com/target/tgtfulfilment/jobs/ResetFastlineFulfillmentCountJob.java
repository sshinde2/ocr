/**
 * 
 */
package au.com.target.tgtfulfilment.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;


/**
 * @author fkhan4
 *
 */
public class ResetFastlineFulfillmentCountJob extends AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(ResetFastlineFulfillmentCountJob.class);

    private TargetWarehouseService targetWarehouseService;
    private ModelService modelService;


    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.CronJobModel)
     */
    @Override
    public PerformResult perform(final CronJobModel arg0) {
        final WarehouseModel warehouseModel = targetWarehouseService
                .getWarehouseForCode(TgtCoreConstants.FASTLINE_WAREHOUSE);
        warehouseModel.setTotalQtyFulfilledAtFastlineToday(0);
        modelService.save(warehouseModel);
        modelService.refresh(warehouseModel);
        LOG.info(" :: Successfully reset total quantity fulfilled at Fastline to :: "
                + warehouseModel.getTotalQtyFulfilledAtFastlineToday());
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */
    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Override
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

}

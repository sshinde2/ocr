/**
 * 
 */
package au.com.target.tgtfulfilment.logger;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectState;


/**
 * @author sbryan6
 *
 */
public class InstoreFulfilmentLogger {

    private static final String LOGGING_PREFIX = "InstoreFulfilment: ";

    private Logger log = null;


    public InstoreFulfilmentLogger(final Class classname) {
        log = Logger.getLogger(classname);
    }

    public void logActionWarn(final String action, final String code) {

        log.warn(LOGGING_PREFIX + " action=" + action + ", code=" + code);
    }

    public void logActionInfo(final String action, final String code) {

        log.info(LOGGING_PREFIX + "action=" + action + ", code=" + code);
    }

    public void logActionError(final String action, final String code, final Exception e) {

        log.error(LOGGING_PREFIX + "action=" + action + ", code=" + code, e);
    }

    public void logStoreAction(final String action, final Integer storeNumber) {

        log.info(LOGGING_PREFIX + "action=" + action + ", store=" + storeNumber);
    }

    public void logStoreActionInfo(final String action, final Integer storeNumber, final String code) {

        log.info(LOGGING_PREFIX + "action=" + action + ", store=" + storeNumber + ", code=" + code);
    }

    public void logStoreActionInfoError(final String action, final Integer storeNumber, final String code,
            final Exception e) {

        log.error(LOGGING_PREFIX + "action=" + action + ", store=" + storeNumber + ", code=" + code, e);
    }

    public void logStoreConsignmentAction(final String action, final Integer storeNumber, final String orderCode,
            final String consignment,
            final String deliverFromState, final String deliverToStoreNumber, final String deliverToState) {
        log.info(LOGGING_PREFIX + "action=" + action + ", store=" + storeNumber + ", order=" + orderCode
                + ", consignment=" + consignment
                + ", deliverFromState=" + deliverFromState
                + ", deliveryToStore=" + deliverToStoreNumber
                + ", deliverToState=" + deliverToState);
    }

    public void logStoreConsignmentRejectReason(final ConsignmentRejectReason reason, final Integer storeNumber,
            final String orderCode,
            final String consignment, final String deliverFromState, final String deliverToStoreNumber,
            final String deliverToState) {
        log.info(LOGGING_PREFIX + "rejectReason=" + reason + ", store=" + storeNumber + ", order="
                + orderCode + ", consignment=" + consignment + ", deliverFromState="
                + deliverFromState
                + ", deliveryToStore="
                + deliverToStoreNumber
                + ", deliverToState="
                + deliverToState);
    }

    public void logStoreConsignmentInstoreRejectReason(final ConsignmentRejectReason reason, final Integer storeNumber,
            final String orderCode,
            final String consignment, final String instoreRejectReason, final ConsignmentRejectState rejectState,
            final String deliverFromState, final String deliverToStoreNumber, final String deliverToState) {
        String productCode = StringUtils.EMPTY;
        String rejectReason = StringUtils.EMPTY;
        if (StringUtils.isNotEmpty(instoreRejectReason)) {
            final String[] results = instoreRejectReason.split("\\|\\|", 2);
            if (results.length == 2) {
                productCode = results[0];
                rejectReason = results[1];
            }
            else {
                rejectReason = instoreRejectReason;
            }
        }
        log.info(LOGGING_PREFIX
                + "rejectReason="
                + reason
                + ", store="
                + storeNumber
                + ", consignment="
                + consignment
                + ", order="
                + orderCode
                + ", deliverFromState="
                + deliverFromState
                + ", deliveryToStore="
                + deliverToStoreNumber
                + ", deliverToState="
                + deliverToState
                + (StringUtils.isNotEmpty(productCode) ? (", RejectedProduct=" + productCode) : StringUtils.EMPTY)
                + (StringUtils.isNotEmpty(rejectReason) ? (", instoreRejectReason=" + rejectReason) : StringUtils.EMPTY)
                + (StringUtils.isNotEmpty(rejectReason) ? (", rejectState=" + rejectState) : StringUtils.EMPTY));
    }

    public void logStoreConsignmentInfo(final String action, final Integer storeNumber, final String consignment,
            final String code) {

        log.info(LOGGING_PREFIX + "action=" + action + ", store=" + storeNumber + ", consignment=" + consignment
                + ", code=" + code);
    }

    public void logStoreActionError(final String action, final Integer storeNumber, final Exception e) {

        log.error(LOGGING_PREFIX + "action=" + action + ", store=" + storeNumber, e);
    }

    public void logStoreActionError(final String action, final Integer storeNumber, final String message) {

        log.error(LOGGING_PREFIX + "action=" + action + ", store=" + storeNumber + ", message=" + message);
    }

    public void logStoreConsignmentActionError(final String action, final Integer storeNumber,
            final String consignment,
            final Exception e) {

        log.error(LOGGING_PREFIX + "action=" + action + ", store=" + storeNumber + ", consignment=" + consignment, e);
    }

    public void logStoreConsignmentActionError(final String action, final Integer storeNumber,
            final String consignment,
            final String message) {

        log.error(LOGGING_PREFIX + "action=" + action + ", store=" + storeNumber + ", consignment=" + consignment
                + ", message=" + message);
    }

}

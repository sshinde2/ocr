/**
 * 
 */
package au.com.target.tgtfulfilment.logger;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper.ParameterName;
import au.com.target.tgtfulfilment.logger.data.EntryData;
import au.com.target.tgtfulfilment.logger.data.OrderEntryGroupData;
import au.com.target.tgtfulfilment.logger.data.SplitResultData;


/**
 * Logger class created to create the relevant logger messages
 * 
 * @author rsamuel3
 *
 */
public class RoutingLogger {
    private static final String LOGGING_PREFIX = "RoutingRules: ";

    private Logger log = null;


    public RoutingLogger(final Class classname) {
        log = Logger.getLogger(classname);
    }

    /**
     * Log entry to routing rules
     * 
     * @param orderCode
     */
    public void logRulesEntryMessage(final String orderCode) {

        log.info(LOGGING_PREFIX + "action=RoutingRulesEntry, order=" + orderCode);
    }

    /**
     * creates a log message for why the routing rules are exited
     * 
     * @param orderCode
     * @param reason
     */
    public void logRulesExitMessage(final String orderCode, final String reason) {

        log.info(LOGGING_PREFIX + "action=RoutingRulesExit, order=" + orderCode + ", reason=" + reason);
    }

    /**
     * creates a log message for why the global routing rules are exited
     * 
     * @param orderCode
     * @param reason
     */
    public void logGlobalRulesExitMessage(final String orderCode, final String reason) {

        log.info(LOGGING_PREFIX + "action=GlobalRoutingRulesExit, order=" + orderCode + ", reason=" + reason);
    }

    /**
     * log General Action Message
     * 
     * @param orderCode
     * @param action
     * @param reason
     */
    public void logGeneralActionMessage(final String orderCode, final String action, final String reason) {

        log.info(LOGGING_PREFIX + "action=" + action + ", order=" + orderCode + ", reason=" + reason);
    }

    /**
     * creates a log message for why the delivery address validation rules exited
     * 
     * @param orderCode
     * @param reason
     */
    public void logDeliveryAddressRulesExitMessage(final String orderCode, final String reason) {

        log.info(LOGGING_PREFIX + "action=DeliveryAddressRoutingRulesExit, order=" + orderCode + ", reason=" + reason);
    }

    /**
     * creates a log message for why the NTL rules exited
     * 
     * @param orderCode
     * @param reason
     */
    public void logNTLRulesExitMessage(final String orderCode, final String reason) {

        log.info(LOGGING_PREFIX + "action=NTLRoutingRulesExit, order=" + orderCode + ", reason=" + reason);
    }

    /**
     * creates a log message when NTL rules exited because of out of stock
     * 
     * @param orderCode
     * @param storeNumber
     * @param reason
     */
    public void logNTLRulesExitMessage(final String orderCode, final int storeNumber, final String reason) {

        log.info(LOGGING_PREFIX + "action=NTLRoutingRulesExit, order=" + orderCode + ", store=" + storeNumber
                + ", reason=" + reason);
    }

    /**
     * creates a log message when there are no giftcards or error with giftcard stock and the rule exits
     * 
     * @param orderCode
     * @param reason
     */
    public void logGiftCardRulesExitMessage(final String orderCode, final String reason) {

        log.info(LOGGING_PREFIX + "action=GiftCardRulesExit, order=" + orderCode + ", reason=" + reason);
    }

    /**
     * creates a log message for why the routing rules are exited, with exception
     * 
     * @param orderCode
     * @param reason
     * @param e
     */
    public void logRulesExitException(final String orderCode, final String reason, final Exception e) {

        logRulesExitMessage(orderCode, reason);
        log.error("Exception in routing rules", e);
    }

    /**
     * create an info log for when a store is excluded
     * 
     * @param orderCode
     * @param storeNumber
     * @param reason
     */
    public void logStoreExcludedMessage(final String orderCode, final int storeNumber, final String reason) {

        log.info(LOGGING_PREFIX + "action=StoreExcluded, order=" + orderCode + ", store=" + storeNumber
                + ", reason=" + reason);
    }

    /**
     * Log skip of same store cnc rules
     * 
     * @param orderCode
     * @param reason
     */
    public void logSameStoreCncRulesSkipMessage(final String orderCode, final String reason) {

        log.info(LOGGING_PREFIX + "action=SameStoreCNCRulesSkip, order=" + orderCode + ", reason=" + reason);
    }

    /**
     * Log skip of same store cnc rules
     * 
     * @param orderCode
     * @param reason
     */
    public void logStoreRulesExcludedMessage(final String action, final String orderCode, final String reason,
            final String storeNumber, final String storeState) {

        log.info(LOGGING_PREFIX + "action=" + action + ", order=" + orderCode + ", reason=" + reason + ", store="
                + storeNumber + ", storeState=" + storeState);
    }

    /**
     * Log skip of ntl cnc rule
     *
     * @param orderCode
     *            the order code
     */
    public void logNTLCncSkipMessage(final String orderCode, final String reason) {

        log.info(LOGGING_PREFIX + "action=NTLCNCRulesSkip, order=" + orderCode + ", reason=" + reason);
    }

    /**
     * Log entry to same store cnc rules
     * 
     * @param orderCode
     * @param storeNumber
     */
    public void logSameStoreCncRulesEntryMessage(final String orderCode, final int storeNumber) {

        log.info(LOGGING_PREFIX + "action=SameStoreCNCRulesEntry, order=" + orderCode + ", store=" + storeNumber);
    }

    /**
     * Log entry to dispatch rules
     * 
     * @param orderCode
     */
    public void logDispatchRulesEntryMessage(final String orderCode) {

        log.info(LOGGING_PREFIX + "action=DispatchRulesEntry, order=" + orderCode);
    }

    /**
     * Log entry to NTL rules
     * 
     * @param orderCode
     */
    public void logNTLRulesEntryMessage(final String orderCode) {

        log.info(LOGGING_PREFIX + "action=NTLRulesEntry, order=" + orderCode);
    }

    /**
     * log info for when the cnc same store rules are exited
     * 
     * @param orderCode
     * @param storeNumber
     * @param reason
     */
    public void logSameStoreCncRulesExitMessage(final String orderCode, final int storeNumber, final String reason) {

        log.info(LOGGING_PREFIX + "action=SameStoreCNCRulesExit, order=" + orderCode + ", store=" + storeNumber
                + ", reason=" + reason);
    }

    /**
     * create an info log for when it is assigned to store
     * 
     * @param orderCode
     * @param storeNumber
     * @param reason
     */
    public void logStoreAssignMessage(final String orderCode, final int storeNumber, final String reason) {

        log.info(LOGGING_PREFIX + "action=AssignOrderToStore, order=" + orderCode + ", store=" + storeNumber
                + ", reason=" + reason);
    }

    /**
     * create an info log for when full order assigned to NTL
     * 
     * @param orderCode
     * @param storeNumber
     */
    public void logNTLAssignMessage(final String orderCode, final int storeNumber) {
        log.info(LOGGING_PREFIX + "action=AssignOrderToNTL, order=" + orderCode + ", store=" + storeNumber);
    }

    /**
     * create an info log for when part order assigned to NTL
     * 
     * @param orderCode
     * @param storeNumber
     */
    public void logNTLPartAssignMessage(final String orderCode, final int storeNumber) {
        log.info(LOGGING_PREFIX + "action=AssignPartOrderToNTL, order=" + orderCode + ", store=" + storeNumber);
    }

    /**
     * create an info log when an order is going to be split
     *
     * @param orderCode
     *            the order code
     */
    public void logSplitOrderMessage(final String orderCode) {

        log.info(LOGGING_PREFIX + "action=SplittingOrder, order=" + orderCode);
    }

    /**
     * create an info log when an order is going to be split by store
     * 
     * @param orderCode
     */
    public void logSplitByStoreEntryMessage(final String orderCode) {
        log.info(LOGGING_PREFIX + "action=splittingOrderByStore, order=" + orderCode);
    }

    /**
     * Log warning message caused by routing rules
     * 
     * @param message
     */
    public void logWarnMessage(final String message) {

        log.warn(LOGGING_PREFIX + message);
    }

    /**
     * create an info log When all the products are External products
     * 
     * @param orderCode
     * @param warehouse
     */
    public void logAssignedToWarehouseMessage(final String orderCode, final String warehouse) {
        log.info(LOGGING_PREFIX + "action=AssignOrderToWarehouse, order=" + orderCode + ", warehouse="
                + warehouse);
    }

    /**
     * create an info log when there are Fastline products in the order
     * 
     * @param orderCode
     * @param warehouse
     */
    public void logAssignedToFastlineMessage(final String orderCode, final String warehouse) {
        log.info(LOGGING_PREFIX + "action=AssignOrderToFastline, order=" + orderCode + ", warehouse="
                + warehouse);
    }

    /**
     * create an info log when no product found in the order for external warehouses
     * 
     * @param orderCode
     */
    public void logNoExternalProductFoundInTheOrder(final String orderCode) {
        log.info(LOGGING_PREFIX + "action=NoExternalWarehouseProductFoundInTheOrder, order=" + orderCode);
    }

    /**
     * create an info log when no external warehouse found in the order
     * 
     * @param orderCode
     */
    public void logNoExternalWarehouseFound(final String orderCode) {
        log.info(LOGGING_PREFIX + "action=NoExternalWarehousesFound, order=" + orderCode);
    }

    /**
     * log for split soh optimization enable.
     * 
     * @param orderCode
     * @param message
     */
    public void logSplitOptimisationEnable(final String message, final String orderCode) {
        log.info(MessageFormat.format(LOGGING_PREFIX + "action={0},for Order ={1}", message, orderCode));
    }

    /**
     * LOG unexpected exceptions occuring in routing rules
     * 
     * @param orderCode
     * @param message
     * @param e
     */
    public void logUnexpectedExceptionMessage(final String orderCode, final String message, final Exception e) {

        log.info(LOGGING_PREFIX + "action=UnexpectedException, order=" + orderCode + ", reason="
                + message);
        log.error("Exception in routing rules", e);
    }

    private List<OrderEntryGroupData> generateAssignedOegsData(final List<OrderEntryGroup> assignedOegs) {

        final List<OrderEntryGroupData> orderEntryGroupDataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(assignedOegs)) {
            for (final OrderEntryGroup oeg : assignedOegs) {
                final OrderEntryGroupData oegData = new OrderEntryGroupData();
                final List<EntryData> entryDatas = new ArrayList<>();
                if (CollectionUtils.isEmpty(oeg)) {
                    continue;
                }
                final Map<String, Long> assignedQtyMap = (Map<String, Long>)oeg
                        .getParameter(OEGParameterHelper.ParameterName.ASSIGNED_QTY.name());
                if (MapUtils.isEmpty(assignedQtyMap)) {
                    continue;
                }
                // Set warehouseCode
                final WarehouseModel assignedWarehouse = (WarehouseModel)oeg
                        .getParameter(OEGParameterHelper.ParameterName.FULFILMENT_WAREHOUSE.name());
                if (assignedWarehouse == null) {
                    oegData.setWarehouseCode("fastlineWarehouse");
                }
                else {
                    oegData.setWarehouseCode(assignedWarehouse.getCode());
                }
                // Set store number
                final TargetPointOfServiceModel tpos = (TargetPointOfServiceModel)oeg
                        .getParameter(ParameterName.FULFILMENT_TPOS.name());
                if (tpos != null && tpos.getStoreNumber() != null) {
                    oegData.setStoreNumber(tpos.getStoreNumber().toString());
                }

                for (final AbstractOrderEntryModel entry : oeg) {
                    final String productCode = entry.getProduct().getCode();
                    final Long assignedQty = assignedQtyMap.get(productCode);
                    final EntryData entryData = new EntryData();
                    entryData.setProductCode(productCode);
                    entryData.setQuantity(assignedQty != null ? assignedQty.toString() : "0");
                    entryDatas.add(entryData);

                }
                oegData.setEntries(entryDatas);
                orderEntryGroupDataList.add(oegData);
            }
        }
        return orderEntryGroupDataList;
    }

    private OrderEntryGroupData generateNotAssignedOeg(final OrderEntryGroup notAssignedOeg) {
        final OrderEntryGroupData orderEntryGroupData = new OrderEntryGroupData();
        orderEntryGroupData.setWarehouseCode("CancellationWarehouse");
        if (CollectionUtils.isNotEmpty(notAssignedOeg)) {
            final Map<String, Long> requiredQtyMap = (Map<String, Long>)notAssignedOeg
                    .getParameter(OEGParameterHelper.ParameterName.REQUIRED_QTY.name());
            if (MapUtils.isNotEmpty(requiredQtyMap)) {
                final List<EntryData> entryDatas = new ArrayList<>();
                for (final AbstractOrderEntryModel entry : notAssignedOeg) {
                    final String productCode = entry.getProduct().getCode();
                    final Long requiredQty = requiredQtyMap.get(productCode);
                    final EntryData entryData = new EntryData();
                    entryData.setProductCode(productCode);
                    entryData.setQuantity(requiredQty != null ? requiredQty.toString() : "0");
                    entryDatas.add(entryData);
                }
                orderEntryGroupData.setEntries(entryDatas);
            }
        }

        return orderEntryGroupData;

    }

    /**
     * log splitOeg result
     * 
     * @param orderCode
     * @param action
     * @param assignedOegs
     * @param notAssignedOeg
     */
    public void logSplitOegResult(final String orderCode, final String deliveryModeCode, final String cncStoreNumber,
            final String action,
            final List<OrderEntryGroup> assignedOegs, final OrderEntryGroup notAssignedOeg) {
        final SplitResultData splitResultData = new SplitResultData();
        splitResultData.setOrderCode(orderCode);
        splitResultData.setCncStoreNumber(cncStoreNumber);
        splitResultData.setDeliveryModeCode(deliveryModeCode);
        splitResultData.setAssignedOrderEntryGroupList(generateAssignedOegsData(assignedOegs));
        splitResultData.setNotAssignedOrderEntryGroup(generateNotAssignedOeg(notAssignedOeg));
        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationConfig.Feature.WRAP_ROOT_VALUE, true);
        String jsonString;
        try {
            jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(splitResultData);
        }
        catch (final IOException e) {
            jsonString = e.getMessage();
        }

        log.info(LOGGING_PREFIX + "action=" + action + ", order=" + orderCode + ", SplitOegResult="
                + jsonString);

    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.logger.data;

import java.util.List;


/**
 * @author bhuang3
 *
 */
public class OrderEntryGroupData {

    private String warehouseCode;

    private String storeNumber;

    private List<EntryData> entries;

    /**
     * @return the warehouseCode
     */
    public String getWarehouseCode() {
        return warehouseCode;
    }

    /**
     * @param warehouseCode
     *            the warehouseCode to set
     */
    public void setWarehouseCode(final String warehouseCode) {
        this.warehouseCode = warehouseCode;
    }

    /**
     * @return the entries
     */
    public List<EntryData> getEntries() {
        return entries;
    }

    /**
     * @param entries
     *            the entries to set
     */
    public void setEntries(final List<EntryData> entries) {
        this.entries = entries;
    }

    /**
     * @return the storeNumber
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

}

/**
 * 
 */
package au.com.target.tgtfulfilment.logger.data;

import java.util.List;


/**
 * @author bhuang3
 *
 */
public class SplitResultData {

    private String orderCode;

    private String deliveryModeCode;

    private String cncStoreNumber;

    private List<OrderEntryGroupData> assignedOrderEntryGroupList;

    private OrderEntryGroupData notAssignedOrderEntryGroup;

    /**
     * @return the orderCode
     */
    public String getOrderCode() {
        return orderCode;
    }

    /**
     * @param orderCode
     *            the orderCode to set
     */
    public void setOrderCode(final String orderCode) {
        this.orderCode = orderCode;
    }

    /**
     * @return the assignedOrderEntryGroupList
     */
    public List<OrderEntryGroupData> getAssignedOrderEntryGroupList() {
        return assignedOrderEntryGroupList;
    }

    /**
     * @param assignedOrderEntryGroupList
     *            the assignedOrderEntryGroupList to set
     */
    public void setAssignedOrderEntryGroupList(final List<OrderEntryGroupData> assignedOrderEntryGroupList) {
        this.assignedOrderEntryGroupList = assignedOrderEntryGroupList;
    }

    /**
     * @return the notAssignedOrderEntryGroup
     */
    public OrderEntryGroupData getNotAssignedOrderEntryGroup() {
        return notAssignedOrderEntryGroup;
    }

    /**
     * @param notAssignedOrderEntryGroup
     *            the notAssignedOrderEntryGroup to set
     */
    public void setNotAssignedOrderEntryGroup(final OrderEntryGroupData notAssignedOrderEntryGroup) {
        this.notAssignedOrderEntryGroup = notAssignedOrderEntryGroup;
    }

    /**
     * @return the deliveryModeCode
     */
    public String getDeliveryModeCode() {
        return deliveryModeCode;
    }

    /**
     * @param deliveryModeCode
     *            the deliveryModeCode to set
     */
    public void setDeliveryModeCode(final String deliveryModeCode) {
        this.deliveryModeCode = deliveryModeCode;
    }

    /**
     * @return the cncStoreNumber
     */
    public String getCncStoreNumber() {
        return cncStoreNumber;
    }

    /**
     * @param cncStoreNumber
     *            the cncStoreNumber to set
     */
    public void setCncStoreNumber(final String cncStoreNumber) {
        this.cncStoreNumber = cncStoreNumber;
    }

}

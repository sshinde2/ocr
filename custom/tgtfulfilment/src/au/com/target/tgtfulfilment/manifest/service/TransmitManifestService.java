/**
 * 
 */
package au.com.target.tgtfulfilment.manifest.service;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.integration.dto.ManifestResponseDTO;
import au.com.target.tgtfulfilment.model.TargetManifestModel;


/**
 * Service interface to transmit manifest to carrier using middleware.
 * 
 * @author jjayawa1
 *
 */
public interface TransmitManifestService {

    /**
     * Transmit the given manifest using to carrier.
     * 
     * @param pos
     * @param manifest
     * @return boolean
     */
    ManifestResponseDTO transmitManifest(TargetPointOfServiceModel pos, TargetManifestModel manifest);

    /**
     * Re transmits manifests that are not transmitted.
     */
    public void reTransmitManifests();

}

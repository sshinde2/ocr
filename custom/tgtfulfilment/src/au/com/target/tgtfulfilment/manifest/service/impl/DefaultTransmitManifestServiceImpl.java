/**
 * 
 */
package au.com.target.tgtfulfilment.manifest.service.impl;

import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.dao.TargetManifestDao;
import au.com.target.tgtfulfilment.integration.dto.ManifestResponseDTO;
import au.com.target.tgtfulfilment.logger.InstoreFulfilmentLogger;
import au.com.target.tgtfulfilment.manifest.service.TransmitManifestService;
import au.com.target.tgtfulfilment.model.TargetManifestModel;


/**
 * Default implementation of TransmitManifestService
 * 
 * @author jjayawa1
 *
 */
public class DefaultTransmitManifestServiceImpl implements TransmitManifestService {
    private static final InstoreFulfilmentLogger INSTORE_LOGGER = new InstoreFulfilmentLogger(
            DefaultTransmitManifestServiceImpl.class);
    private TargetManifestDao targetManifestDao;

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.service.TransmitManifestService#transmitManifest(au.com.target.tgtfulfilment.model.TargetManifestModel)
     */
    @Override
    public ManifestResponseDTO transmitManifest(final TargetPointOfServiceModel pos, final TargetManifestModel manifest) {
        INSTORE_LOGGER.logActionInfo("No transmission of manifest: ", manifest.getCode());
        return new ManifestResponseDTO();
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.manifest.service.TransmitManifestService#reTransmitManifests()
     */
    @Override
    public void reTransmitManifests() {
        final List<TargetManifestModel> notTransmittedManifests = targetManifestDao.getNotTransmittedManifests();

        if (CollectionUtils.isNotEmpty(notTransmittedManifests)) {
            for (final TargetManifestModel notTransmittedManifest : notTransmittedManifests) {
                final TargetPointOfServiceModel tpos = getPOS(notTransmittedManifest);
                if (tpos != null) {
                    final ManifestResponseDTO transmitResponse = transmitManifest(tpos,
                            notTransmittedManifest);

                    if (!transmitResponse.isSuccess()) {
                        INSTORE_LOGGER.logStoreActionError("Re-TransmitManifestFailure error="
                                + transmitResponse.getErrorCode(), tpos.getStoreNumber(),
                                notTransmittedManifest.getCode());
                    }
                    else {
                        INSTORE_LOGGER.logStoreActionInfo("Re-TransmitManifestSuccess",
                                tpos.getStoreNumber(),
                                notTransmittedManifest.getCode());
                    }
                }
            }
        }
        else {
            INSTORE_LOGGER.logActionInfo("Re-TransmitManifest", "NoFailedManifests");
        }
    }

    /**
     * @param targetManifestDao
     *            the targetManifestDao to set
     */
    @Required
    public void setTargetManifestDao(final TargetManifestDao targetManifestDao) {
        this.targetManifestDao = targetManifestDao;
    }

    private final TargetPointOfServiceModel getPOS(final TargetManifestModel manifest) {
        TargetPointOfServiceModel tpos = null;
        if (CollectionUtils.isNotEmpty(manifest.getConsignments())) {
            final TargetConsignmentModel consignment = manifest.getConsignments().iterator()
                    .next();
            final WarehouseModel storeWarehouse = consignment.getWarehouse();
            if (storeWarehouse != null) {
                if (CollectionUtils.isNotEmpty(storeWarehouse.getPointsOfService())) {
                    final PointOfServiceModel pos = storeWarehouse.getPointsOfService().iterator().next();
                    if (pos instanceof TargetPointOfServiceModel) {
                        tpos = (TargetPointOfServiceModel)pos;
                    }
                }
                else {
                    INSTORE_LOGGER
                            .logActionWarn("Re-TransmitManifestFailure error=NoPOSInWarehouse "
                                    , storeWarehouse.getCode());
                }
            }
            else {
                INSTORE_LOGGER
                        .logActionWarn("Re-TransmitManifestFailure error=NoWarehouseInConsignment ",
                                consignment.getCode());
            }
        }
        else {
            INSTORE_LOGGER
                    .logActionWarn("Re-TransmitManifestFailure error=NoConsignmentsInManifest "
                            , manifest.getCode());
        }
        return tpos;
    }
}

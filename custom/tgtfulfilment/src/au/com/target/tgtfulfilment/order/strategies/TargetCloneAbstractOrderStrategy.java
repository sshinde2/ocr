/**
 * 
 */
package au.com.target.tgtfulfilment.order.strategies;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.AbstractOrderEntryTypeService;
import de.hybris.platform.order.strategies.ordercloning.impl.DefaultCloneAbstractOrderStrategy;
import de.hybris.platform.servicelayer.internal.model.impl.ItemModelCloneCreator;
import de.hybris.platform.servicelayer.type.TypeService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.GiftRecipientModel;


/**
 * @author sbryan6
 *
 */
public class TargetCloneAbstractOrderStrategy extends DefaultCloneAbstractOrderStrategy {

    private final ItemModelCloneCreator itemModelCloneCreator;

    /**
     * @param typeService
     * @param itemModelCloneCreator
     * @param abstractOrderEntryTypeService
     */
    public TargetCloneAbstractOrderStrategy(final TypeService typeService,
            final ItemModelCloneCreator itemModelCloneCreator,
            final AbstractOrderEntryTypeService abstractOrderEntryTypeService) {
        super(typeService, itemModelCloneCreator, abstractOrderEntryTypeService);
        this.itemModelCloneCreator = itemModelCloneCreator;
    }

    @Override
    protected void postProcess(final AbstractOrderModel original, final AbstractOrderModel copy)
    {
        if (original != null && copy != null) {
            copyGiftRecipients(original, copy);
        }
    }

    private void copyGiftRecipients(final AbstractOrderModel original, final AbstractOrderModel copy) {

        final List<AbstractOrderEntryModel> originalEntries = original.getEntries();
        final List<AbstractOrderEntryModel> copyEntries = copy.getEntries();

        // The superclass method postProcess actually ensures the sizes are the same
        // so just adding a basic check here
        if (CollectionUtils.isNotEmpty(originalEntries) && CollectionUtils.isNotEmpty(copyEntries)
                && originalEntries.size() == copyEntries.size()) {

            final Iterator<AbstractOrderEntryModel> itOriginal = originalEntries.iterator();
            final Iterator<AbstractOrderEntryModel> itCopy = copyEntries.iterator();

            while (itOriginal.hasNext())
            {
                final AbstractOrderEntryModel originalEntry = itOriginal.next();
                final AbstractOrderEntryModel copyEntry = itCopy.next();
                copyEntry.setGiftRecipients(createCopyGiftRecipients(originalEntry));
            }
        }
    }

    private List<GiftRecipientModel> createCopyGiftRecipients(final AbstractOrderEntryModel originalEntry) {

        if (originalEntry == null) {
            return null;
        }

        final List<GiftRecipientModel> originalGiftRecipients = originalEntry.getGiftRecipients();

        if (CollectionUtils.isEmpty(originalGiftRecipients)) {
            return null;
        }

        final List<GiftRecipientModel> copyRecipients = new ArrayList<>();
        for (final GiftRecipientModel orig : originalGiftRecipients) {

            final GiftRecipientModel copy = (GiftRecipientModel)itemModelCloneCreator.copy(orig);
            copyRecipients.add(copy);
        }

        return copyRecipients;
    }

}

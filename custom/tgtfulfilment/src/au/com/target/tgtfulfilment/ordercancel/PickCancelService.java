/**
 * 
 */
package au.com.target.tgtfulfilment.ordercancel;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelException;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.PickTypeEnum;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.dto.PickConsignmentUpdateData;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;


/**
 * Service for creating a cancel request for a pick and running it
 * 
 */
public interface PickCancelService {

    /**
     * Start the cancel procedure for this short or zero pick
     * 
     * @param order
     * @param consignment
     * @param updateData
     * @param pickType
     *            should be SHORT or ZERO
     * @throws FulfilmentException
     */
    void startPickCancelProcess(OrderModel order, TargetConsignmentModel consignment,
            PickConsignmentUpdateData updateData, PickTypeEnum pickType)
            throws OrderCancelException, FulfilmentException;

}

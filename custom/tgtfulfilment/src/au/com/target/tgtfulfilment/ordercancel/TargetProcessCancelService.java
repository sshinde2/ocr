/**
 * 
 */
package au.com.target.tgtfulfilment.ordercancel;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelException;

import au.com.target.tgtcore.ordercancel.data.CancelRequestResponse;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntryList;


/**
 * @author bhuang3
 *
 */
public interface TargetProcessCancelService {

    CancelRequestResponse processCancel(OrderModel order,
            TargetOrderCancelEntryList targetOrderCancelEntryList)
            throws OrderCancelException, FulfilmentException;
}

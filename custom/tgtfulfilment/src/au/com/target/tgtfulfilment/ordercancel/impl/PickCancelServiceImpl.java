/**
 * 
 */
package au.com.target.tgtfulfilment.ordercancel.impl;

import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.CancelTypeEnum;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.PickTypeEnum;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.ordercancel.TargetOrderCancelRequest;
import au.com.target.tgtcore.ordercancel.data.CancelRequestResponse;
import au.com.target.tgtcore.ordercancel.data.RefundInfoDTO;
import au.com.target.tgtcore.ordercancel.enums.RequestOrigin;
import au.com.target.tgtfulfilment.dto.PickConsignmentUpdateData;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.helper.TargetFulfilmentHelper;
import au.com.target.tgtfulfilment.ordercancel.PickCancelService;


/**
 * @author sbryan5
 * 
 */
public class PickCancelServiceImpl extends TargetProcessCancelServiceImpl implements PickCancelService {

    private static final Logger LOG = Logger.getLogger(PickCancelServiceImpl.class);

    private static final CancelReason PICK_CANCEL_REASON = CancelReason.OUTOFSTOCK;

    private TargetFulfilmentHelper targetFulfilmentHelper;




    @Override
    public void startPickCancelProcess(final OrderModel order, final TargetConsignmentModel consignment,
            final PickConsignmentUpdateData updateData, final PickTypeEnum pickType) throws OrderCancelException,
            FulfilmentException {

        final String formattedCancelNote = String.format("Zero or Short Pick happened on Consignment - {%s}",
                consignment.getCode());

        final CancelTypeEnum cancelType = PickTypeEnum.ZERO.equals(pickType) ? CancelTypeEnum.ZERO_PICK
                : CancelTypeEnum.SHORT_PICK;

        final TargetOrderCancelRequest cancelRequest = createCancelRequest(order, consignment, updateData, cancelType,
                formattedCancelNote);

        if (cancelRequest == null) {
            throw new OrderCancelException(order.getCode(), "Could not create cancel request for updated consignment");
        }

        final CancelRequestResponse response = processCancelRequest(cancelRequest);

        final RefundInfoDTO refundedInfo = targetRefundPaymentService.createRefundedInfo(
                response.getFollowOnRefundPaymentDataList(), response.getRefundAmount());
        final BigDecimal refundAmountRemaining = refundedInfo.getRefundAmountRemaining();

        // Raise ticket if refund failed
        this.raiseCsTicket(order, cancelRequest, refundedInfo);

        // The business process will be responsible for accertify, emails, tlog
        getTargetBusinessProcessService().startConsignmentPickedCancelProcess(order, consignment, updateData,
                (OrderCancelRecordEntryModel)response.getOrderModificationRecordEntryModel(),
                response.getRefundAmount(), cancelType, response.getFollowOnRefundPaymentList(),
                refundAmountRemaining);
        LOG.info("Short/zero pick cancel process started successfully for order:" + order.getCode() + " consignment:"
                + consignment.getCode());

    }


    /**
     * Create an order cancel request pojo for the given order and consignment update data
     * 
     * @param order
     * @param updateData
     * @param cancelType
     * @param formattedCancelNote
     * @throws FulfilmentException
     */
    protected TargetOrderCancelRequest createCancelRequest(final OrderModel order,
            final ConsignmentModel consignment,
            final PickConsignmentUpdateData updateData,
            final CancelTypeEnum cancelType,
            final String formattedCancelNote) throws FulfilmentException {

        final List<OrderCancelEntry> orderCancelEntries = generateCancelEntries(consignment, updateData,
                order.getCode(),
                formattedCancelNote);

        if (CollectionUtils.isNotEmpty(orderCancelEntries)) {
            final TargetOrderCancelRequest orderCancelRequest = new TargetOrderCancelRequest(order, orderCancelEntries,
                    formattedCancelNote);
            orderCancelRequest.setCancelReason(PICK_CANCEL_REASON);
            orderCancelRequest.setRequestOrigin(RequestOrigin.WAREHOUSE);

            return orderCancelRequest;
        }
        return null;
    }


    /**
     * Generate OrderCancelEntry list based on the consignment and update data.
     * 
     * @param consignment
     * @param updateData
     * @param orderCode
     * @param formattedCancelNote
     * @return list of OrderCancelEntry
     * @throws FulfilmentException
     */
    protected List<OrderCancelEntry> generateCancelEntries(final ConsignmentModel consignment,
            final PickConsignmentUpdateData updateData,
            final String orderCode,
            final String formattedCancelNote) throws FulfilmentException {

        final Set<ConsignmentEntryModel> consignmentEntries = consignment.getConsignmentEntries();

        if (CollectionUtils.isEmpty(consignmentEntries)) {
            throw new FulfilmentException(String.format("Consignment Entries shouldn't be empty on Consignment - {%s}",
                    consignment.getCode()));
        }

        final List<OrderCancelEntry> orderCancelEntries = new ArrayList<>();
        for (final ConsignmentEntryModel entry : consignmentEntries) {

            if (entry == null || entry.getQuantity() == null) {
                continue;
            }

            final Long originQuantity = entry.getQuantity();

            final String itemCode = targetFulfilmentHelper.getConsignmentEntryItemCode(entry);
            if (itemCode == null) {
                throw new FulfilmentException(String.format("No item code for Consignment entry in - {%s}",
                        consignment.getCode()));
            }

            final long shippedQuantity = updateData.getShippedQuantity(itemCode);

            if (shippedQuantity < originQuantity.longValue()) {
                final long cancelledQty = originQuantity.longValue() - shippedQuantity;
                orderCancelEntries.add(new OrderCancelEntry(entry.getOrderEntry(), cancelledQty, formattedCancelNote,
                        PICK_CANCEL_REASON));
            }

        }

        return orderCancelEntries;
    }


    /**
     * Lookup for TargetBusinessProcessService
     * 
     * @return TargetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }


    /**
     * @param targetFulfilmentHelper
     *            the targetFulfilmentHelper to set
     */
    @Required
    public void setTargetFulfilmentHelper(final TargetFulfilmentHelper targetFulfilmentHelper) {
        this.targetFulfilmentHelper = targetFulfilmentHelper;
    }

}

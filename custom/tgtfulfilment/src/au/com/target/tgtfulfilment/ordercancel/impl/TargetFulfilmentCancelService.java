/**
 * 
 */
package au.com.target.tgtfulfilment.ordercancel.impl;

import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.ordercancel.TargetOrderCancelRequest;
import au.com.target.tgtcore.ordercancel.enums.RequestOrigin;
import au.com.target.tgtcore.ordercancel.impl.TargetCancelServiceImpl;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;

import com.google.common.collect.ImmutableSet;


/**
 * @author dcwillia
 * 
 */
public class TargetFulfilmentCancelService extends TargetCancelServiceImpl {

    private static final Logger LOG = Logger.getLogger(TargetFulfilmentCancelService.class);

    private static final Set<ConsignmentStatus> CON_STATUS_REQUIRE_EXTRACT =
            new ImmutableSet.Builder<ConsignmentStatus>()
                    .add(ConsignmentStatus.SENT_TO_WAREHOUSE)
                    .add(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE)
                    .add(ConsignmentStatus.PICKED)
                    .build();

    private TargetWarehouseService targetWarehouseService;

    private TargetConsignmentService targetConsignmentService;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Override
    public void releaseStock(final OrderCancelRequest cancelRequest) throws OrderCancelException {
        for (final OrderCancelEntry oce : cancelRequest.getEntriesToCancel())
        {
            final AbstractOrderEntryModel orderEntry = oce.getOrderEntry();

            if (orderEntryRequiresStockRelease(orderEntry)) {
                final ProductModel product = orderEntry.getProduct();
                final int amount = (int)oce.getCancelQuantity();
                if (amount > 0) {
                    try {
                        getTargetStockService().release(product, amount, amount
                                + " stock has been released for " + product.getName() + " in orderEntry pk of "
                                + orderEntry.getPk());
                    }
                    catch (final Exception e) {
                        // Treat failure to release stock as non fatal, but log a formatted warning
                        LOG.warn(SplunkLogFormatter.formatOrderMessage("releaseStock failed: " + e.getMessage(),
                                TgtutilityConstants.ErrorCode.WARN_TGTLAYBY,
                                orderEntry.getOrder().getCode()));
                    }
                }
            }
        }
    }


    /**
     * Return true if a stock release is required for the given order Entry
     * 
     * @param orderEntry
     * @return true if required
     */
    protected boolean orderEntryRequiresStockRelease(final AbstractOrderEntryModel orderEntry) {
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)) {
            return true;
        }

        final Set<ConsignmentEntryModel> consignmentEntries = orderEntry.getConsignmentEntries();
        if (CollectionUtils.isNotEmpty(consignmentEntries)) {
            for (final ConsignmentEntryModel consignmentEntry : consignmentEntries) {

                final ConsignmentModel consignment = consignmentEntry.getConsignment();
                if (consignment != null && consignment instanceof TargetConsignmentModel) {
                    final TargetConsignmentModel targetCon = (TargetConsignmentModel)consignment;
                    if (targetCon.getPickConfirmDate() != null) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    @Override
    public boolean isExtractCancelToWarehouseRequired(final OrderModel order) {

        final List<TargetConsignmentModel> consignments = targetConsignmentService.getActiveConsignmentsForOrder(order);

        if (CollectionUtils.isEmpty(consignments)) {
            return false;
        }

        // Will want to extract the cancel to warehouse if any consignment is at the warehouse
        for (final TargetConsignmentModel consignment : consignments) {
            if (CON_STATUS_REQUIRE_EXTRACT.contains(consignment.getStatus())) {
                return true;
            }
        }

        return false;
    }

    @Override
    protected WarehouseModel getOnlineWarehouse() {
        return targetWarehouseService.getDefaultOnlineWarehouse();

    }

    @Override
    public void updateConsignmentStatus(final OrderCancelRequest cancelRequest) {

        boolean updateConsignmentForPartialCancel = true;
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)) {
            updateConsignmentForPartialCancel = false;
        }

        final List<TargetConsignmentModel> consignments = targetConsignmentService
                .getActiveConsignmentsForOrder(cancelRequest
                        .getOrder());

        if (CollectionUtils.isNotEmpty(consignments)) {
            if (!cancelRequest.isPartialCancel()) {
                for (final TargetConsignmentModel consignment : consignments) {
                    updateConsignmentForCancellation(cancelRequest, consignment);
                }
                getModelService().saveAll(consignments);
            }
            else if (updateConsignmentForPartialCancel) {
                checkOrderEntriesAndUpdateConsignmentStatus(cancelRequest, consignments);
                adjustConsignmentsForPartialCancel(cancelRequest.getOrder(), consignments);
            }
        }
    }

    /**
     * @param cancelRequest
     * @param consignment
     */
    protected void updateConsignmentForCancellation(final OrderCancelRequest cancelRequest,
            final TargetConsignmentModel consignment) {
        consignment.setStatus(ConsignmentStatus.CANCELLED);
        consignment.setCancelDate(Calendar.getInstance().getTime());
        this.updateConsignmentShippedQtyForCancellation(consignment);
        if (CancelReason.OUTOFSTOCK.equals(cancelRequest.getCancelReason())
                && RequestOrigin.WAREHOUSE.equals(((TargetOrderCancelRequest)cancelRequest).getRequestOrigin())) {
            // zero-pick
            consignment.setRejectReason(ConsignmentRejectReason.ZERO_PICK_BY_WAREHOUSE);
        }
        else {
            // cancelled by cs agent
            consignment.setRejectReason(ConsignmentRejectReason.CANCELLED_BY_CUSTOMER);
        }
    }


    private void updateConsignmentShippedQtyForCancellation(final TargetConsignmentModel consignment) {
        if (CollectionUtils.isNotEmpty(consignment.getConsignmentEntries())) {
            for (final ConsignmentEntryModel entry : consignment.getConsignmentEntries()) {
                entry.setShippedQuantity(Long.valueOf(0l));
            }
        }
    }

    /**
     * Checks orderEntries in consignment entries. If at least one of the order entries has quantity > 0 don't set the
     * consignment to CANCELLED state.
     * 
     * @param consignments
     */
    private void checkOrderEntriesAndUpdateConsignmentStatus(final OrderCancelRequest cancelRequest,
            final List<TargetConsignmentModel> consignments) {
        for (final TargetConsignmentModel consignment : consignments) {
            boolean cancelConsignment = true;
            final Set<ConsignmentEntryModel> consignmentEntries = consignment.getConsignmentEntries();
            if (CollectionUtils.isNotEmpty(consignmentEntries)) {
                for (final ConsignmentEntryModel consignmentEntry : consignmentEntries) {
                    final AbstractOrderEntryModel orderEntry = consignmentEntry.getOrderEntry();
                    if (orderEntry != null) {
                        if (orderEntry.getQuantity() != null) {
                            if (orderEntry.getQuantity().longValue() > 0) {
                                cancelConsignment = false;
                                break;
                            }
                        }
                    }
                }
            }

            if (cancelConsignment) {
                updateConsignmentForCancellation(cancelRequest, consignment);
                getModelService().save(consignment);
            }
        }
    }

    /**
     * Drop the existing consignment entries as we cannot adjust the quantity(initial) attribute.Clone it and adjust the
     * quantity alone to match the order entry
     * 
     * @param order
     * @param consignments
     * 
     */
    private void adjustConsignmentsForPartialCancel(final OrderModel order,
            final List<TargetConsignmentModel> consignments) {

        final List<ConsignmentEntryModel> consignmentEntriesToRemove = new ArrayList<>();
        final List<ConsignmentEntryModel> newConsignmentEntries = new ArrayList<>();

        for (final AbstractOrderEntryModel orderEntry : order.getEntries()) {
            final ConsignmentEntryModel consignmentEntry = getConsignmentEntryFromActiveConsignments(orderEntry,
                    consignments);
            if (null != consignmentEntry) {
                if (orderEntry.getQuantity().longValue() > 0) {
                    // As there should be only one consignment entry per order entry. May need to re-split the order in future
                    final ConsignmentEntryModel newConsignmentEntry = getModelService().clone(consignmentEntry);
                    newConsignmentEntry.setQuantity(orderEntry.getQuantity());
                    newConsignmentEntries.add(newConsignmentEntry);
                }
                consignmentEntriesToRemove.add(consignmentEntry);
            }
        }

        if (CollectionUtils.isNotEmpty(consignmentEntriesToRemove)) {
            getModelService().removeAll(consignmentEntriesToRemove);
            getModelService().saveAll(newConsignmentEntries);
        }
    }

    /**
     * Gets the consignment entry from active consignments.
     *
     * @param orderEntry
     *            the order entry
     * @param consignments
     *            the consignment
     * @return the consignment entry from active consignments
     */
    private ConsignmentEntryModel getConsignmentEntryFromActiveConsignments(final AbstractOrderEntryModel orderEntry,
            final List<TargetConsignmentModel> consignments) {

        for (final ConsignmentEntryModel consEntryModel : orderEntry.getConsignmentEntries()) {
            if (consignments.contains(consEntryModel.getConsignment())) {
                return consEntryModel;
            }
        }

        return null;
    }

    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */
    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }

    /**
     * @param targetConsignmentService
     *            the targetConsignmentService to set
     */
    @Required
    public void setTargetConsignmentService(final TargetConsignmentService targetConsignmentService) {
        this.targetConsignmentService = targetConsignmentService;
    }


    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

}
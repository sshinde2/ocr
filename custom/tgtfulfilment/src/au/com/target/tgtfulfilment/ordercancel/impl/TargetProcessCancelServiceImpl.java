/**
 * 
 */
package au.com.target.tgtfulfilment.ordercancel.impl;

import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRecordsHandler;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.CancelTypeEnum;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.flybuys.service.FlybuysDiscountService;
import au.com.target.tgtcore.order.impl.TargetFindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtcore.ordercancel.TargetCancelService;
import au.com.target.tgtcore.ordercancel.TargetOrderCancelRequest;
import au.com.target.tgtcore.ordercancel.TargetRefundPaymentService;
import au.com.target.tgtcore.ordercancel.data.CancelRequestResponse;
import au.com.target.tgtcore.ordercancel.data.PaymentTransactionEntryDTO;
import au.com.target.tgtcore.ordercancel.data.RefundInfoDTO;
import au.com.target.tgtcore.ordercancel.discountstrategy.VoucherCorrectionStrategy;
import au.com.target.tgtcore.ordercancel.enums.RequestOrigin;
import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.exceptions.RefundFailedException;
import au.com.target.tgtfulfilment.ordercancel.TargetProcessCancelService;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntry;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntryList;
import au.com.target.tgtfulfilment.salesapplication.TargetFulfilmentSalesApplicationConfigService;
import au.com.target.tgtpayment.exceptions.PaymentException;
import au.com.target.tgtpayment.util.PriceCalculator;


/**
 * @author bhuang3
 *
 */
public class TargetProcessCancelServiceImpl implements TargetProcessCancelService {

    private static final Logger LOG = Logger.getLogger(TargetProcessCancelServiceImpl.class);

    private static final CancelReason PICK_CANCEL_REASON = CancelReason.OUTOFSTOCK;

    private static final String CANCEL_NOTE = "system cancellation";

    private static final String REFUND_REJECT_MESSAGE = "refund rejected: refund amount:{0}, and failure message:{1} ";

    private static final String REFUND_TICKET_MESSAGE = " Order Short/Zero pick is failed due to failure of refund | Refund information: {0}.";

    private static final String SHORTPICK_PRODUCT_MSG = " The following products were not fulfilled due to a short/Zero-pick:{0}";

    private static final String REFUND_TICKET_HEAD = "Short or Zero pick refund failed";

    protected TargetFulfilmentSalesApplicationConfigService salesApplicationConfigService;

    protected TargetRefundPaymentService targetRefundPaymentService;

    protected TargetTicketBusinessService targetTicketBusinessService;

    private TransactionTemplate transactionTemplate;

    private FlybuysDiscountService flybuysDiscountService;

    private VoucherCorrectionStrategy voucherCorrectionStrategy;

    private ModelService modelService;

    private TargetFindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    private TargetCancelService targetCancelService;

    private OrderCancelRecordsHandler orderCancelRecordsHandler;

    private TargetFeatureSwitchService targetFeatureSwitchService;


    @Override
    public CancelRequestResponse processCancel(final OrderModel order,
            final TargetOrderCancelEntryList targetOrderCancelEntryList) throws OrderCancelException,
            FulfilmentException {
        final TargetOrderCancelRequest cancelRequest = this.createCancelRequest(order, targetOrderCancelEntryList);
        final CancelRequestResponse response = processCancelRequest(cancelRequest);
        final RefundInfoDTO refundedInfo = targetRefundPaymentService.createRefundedInfo(
                response.getFollowOnRefundPaymentDataList(), response.getRefundAmount());
        response.setRefundAmountRemaining(refundedInfo.getRefundAmountRemaining());
        final CancelTypeEnum cancelType = cancelRequest.isPartialCancel() ? CancelTypeEnum.SHORT_PICK
                : CancelTypeEnum.ZERO_PICK;
        response.setCancelType(cancelType);
        this.raiseCsTicket(order, cancelRequest, refundedInfo);
        return response;
    }

    protected TargetOrderCancelRequest createCancelRequest(final OrderModel order,
            final TargetOrderCancelEntryList targetOrderCancelEntryList) {
        TargetOrderCancelRequest orderCancelRequest = null;

        final List<OrderCancelEntry> orderCancelEntryList = this.generateCancelEntries(targetOrderCancelEntryList);
        if (CollectionUtils.isNotEmpty(orderCancelEntryList)) {
            orderCancelRequest = new TargetOrderCancelRequest(order, orderCancelEntryList, CANCEL_NOTE);
            orderCancelRequest.setCancelReason(PICK_CANCEL_REASON);
            orderCancelRequest.setRequestOrigin(RequestOrigin.SYSTEM);
        }
        return orderCancelRequest;
    }

    private List<OrderCancelEntry> generateCancelEntries(final TargetOrderCancelEntryList targetOrderCancelEntryList) {
        final List<OrderCancelEntry> orderCancelEntries = new ArrayList<>();
        if (targetOrderCancelEntryList != null
                && CollectionUtils.isNotEmpty(targetOrderCancelEntryList.getEntriesToCancel())) {
            for (final TargetOrderCancelEntry cancelEntry : targetOrderCancelEntryList.getEntriesToCancel()) {
                orderCancelEntries.add(new OrderCancelEntry(cancelEntry.getOrderEntry(),
                        cancelEntry.getCancelQuantity() != null ? cancelEntry.getCancelQuantity().longValue() : 0l,
                        CANCEL_NOTE,
                        PICK_CANCEL_REASON));
            }
        }
        return orderCancelEntries;
    }

    protected void raiseCsTicket(final OrderModel order, final TargetOrderCancelRequest cancelRequest,
            final RefundInfoDTO refundedInfo) {
        // Raise ticket if refund failed
        if (!salesApplicationConfigService.isSuppressAutoRefund(order.getSalesApplication())) {
            if (StringUtils.isNotEmpty(refundedInfo.getMessage())) {
                final String refundMessage = MessageFormat.format(REFUND_TICKET_MESSAGE, refundedInfo.getMessage());
                final String ticketMsg = MessageFormat.format(REFUND_TICKET_MESSAGE, refundedInfo.getMessage())
                        + MessageFormat.format(SHORTPICK_PRODUCT_MSG,
                                populateCancelledProductCodes(cancelRequest.getEntriesToCancel()));
                targetTicketBusinessService.createCsAgentTicket(REFUND_TICKET_HEAD, REFUND_TICKET_HEAD, ticketMsg,
                        order, TgtCoreConstants.CsAgentGroup.REFUND_FAILED_CS_AGENT_GROUP);
                LOG.info("Refund failed for order:" + order.getCode() + refundMessage
                        + " CS ticket has been raised");
            }
        }
    }

    protected CancelRequestResponse processCancelRequest(final OrderCancelRequest orderCancelRequest)
            throws OrderCancelException {

        // We want a transaction block here, because if the refund fails we want to undo all of the
        // changes we make to the order and remove the history and cancel request model.  
        // That way when we log a ticket to the cs agent they can get a new credit card from the customer,
        // and perform a (partial) cancellation.  
        // NOTE: the consignment itself has already been updated with quantities, tracking number, parcel count, carrier.
        // Those updates will not be undone, but we assume that will not hinder future pick or partial cancellation.

        final CancelRequestResponse response = new CancelRequestResponse();

        final OrderCancelException orderCancelException = transactionTemplate.execute(
                new TransactionCallback<OrderCancelException>() {

                    @Override
                    public OrderCancelException doInTransaction(final TransactionStatus status) {

                        try {
                            processCancelRequestInternal(orderCancelRequest, response);

                            // If the refund payment failed then throw an exception to force the rollback
                            final String refundFailureMessage = response.getRefundFailureMessage();
                            if (refundFailureMessage != null) {

                                throw new RefundFailedException(MessageFormat
                                        .format(REFUND_REJECT_MESSAGE,
                                                response.getRefundAmount(), refundFailureMessage, orderCancelRequest
                                                        .getOrder().getCode()));
                            }

                        }
                        //The order needs to be progressed further,even if there is a refund failed
                        catch (final RefundFailedException | PaymentException e) {
                            LOG.info("Refund Failed for order"
                                    + orderCancelRequest.getOrder().getCode());
                            return null;
                        }
                        catch (final OrderCancelException e) {
                            status.setRollbackOnly();
                            return e;
                        }
                        catch (final Exception e) {
                            status.setRollbackOnly();
                            return new OrderCancelException(orderCancelRequest.getOrder().getCode(), MessageFormat
                                    .format(REFUND_REJECT_MESSAGE,
                                            response.getRefundAmount(), e.getMessage()));
                        }

                        return null;

                    }

                });

        // Handle failure
        if (orderCancelException != null) {

            // If there was a refund payment attempt then recreate the payment models, now the transaction has rolled back
            if (CollectionUtils.isNotEmpty(response.getFollowOnRefundPaymentDataList())) {
                targetRefundPaymentService.createFollowOnRefundPaymentTransactionEntries(orderCancelRequest.getOrder(),
                        response.getFollowOnRefundPaymentDataList());
            }

            throw orderCancelException;
        }

        return response;
    }

    /**
     * Do the steps required for the order cancel.
     * 
     * @param orderCancelRequest
     * @throws OrderCancelException
     */
    private void processCancelRequestInternal(final OrderCancelRequest orderCancelRequest,
            final CancelRequestResponse response)
            throws OrderCancelException {

        // Create the OrderCancelRecordEntryModel. This also creates an order history snapshot
        final OrderCancelRecordEntryModel cancelRequestRecordEntry = orderCancelRecordsHandler
                .createRecordEntry(orderCancelRequest);

        if (!targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)) {
            // This is up front since it uses the existing consignment statuses before they get modified below.
            targetCancelService.releaseStock(orderCancelRequest);
        }

        // modify order 
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)) {
            // we don't need to modify the consignment for fluent
            targetCancelService.modifyOrderAccordingToRequest(orderCancelRequest, orderCancelRequest.getOrder());
        }
        else {
            targetCancelService.modifyOrderAccordingToRequest(orderCancelRequest);
        }

        final OrderModel order = orderCancelRequest.getOrder();
        modelService.refresh(order);

        targetCancelService.recalculateOrderPreventingIncrease(order, cancelRequestRecordEntry);

        final Double orderTotalBeforeVoucherUpdate = order.getTotalPrice();

        voucherCorrectionStrategy.correctAppliedVouchers(order);

        targetCancelService.updateOrderStatus(cancelRequestRecordEntry, order);

        //update record entries
        targetCancelService.updateRecordEntry(orderCancelRequest);

        final BigDecimal refundAmount = PriceCalculator.subtract(order.getTotalPrice(),
                findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(order));

        cancelRequestRecordEntry.setRefundedAmount(Double.valueOf(refundAmount.doubleValue()));

        flybuysDiscountService
                .setFlybuysRefundDetailsInModificationEntry(cancelRequestRecordEntry, order,
                        orderTotalBeforeVoucherUpdate);
        modelService.save(cancelRequestRecordEntry);

        // Pass some data out in the response
        response.setRefundAmount(refundAmount);
        response.setOrderModificationRecordEntryModel(cancelRequestRecordEntry);

        if (refundAmount.compareTo(BigDecimal.ZERO) > 0
                && !salesApplicationConfigService.isSuppressAutoRefund(order.getSalesApplication())) {
            // Do the refund at the end to minimise chance of rollback afterwards
            // Note we save the PTEM data in response in case we roll everything back so we can recreate them
            final List<PaymentTransactionEntryModel> refundedEntryList = targetRefundPaymentService
                    .performFollowOnRefund(order, refundAmount);
            final List<PaymentTransactionEntryDTO> refundedEntryDtoList = targetRefundPaymentService
                    .convertPaymentTransactionEntries(refundedEntryList);
            response.setFollowOnRefundPaymentList(refundedEntryList);
            response.setFollowOnRefundPaymentDataList(refundedEntryDtoList);
            response.setRefundedAmount(targetRefundPaymentService
                    .findRefundedAmountAfterRefund(refundedEntryDtoList));
        }
    }


    /**
     * 
     * @param cancelEntries
     * @return list
     */
    protected List<String> populateCancelledProductCodes(final List<OrderCancelEntry> cancelEntries) {
        final ArrayList<String> productCodes = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(cancelEntries)) {
            for (final OrderCancelEntry entry : cancelEntries) {
                final AbstractOrderEntryModel orderEntryModel = entry.getOrderEntry();
                if (null != orderEntryModel && null != orderEntryModel.getProduct()) {
                    productCodes.add(orderEntryModel.getProduct().getCode());
                }
            }
        }
        return productCodes;
    }

    /**
     * @param transactionTemplate
     *            the transactionTemplate to set
     */
    @Required
    public void setTransactionTemplate(final TransactionTemplate transactionTemplate) {
        this.transactionTemplate = transactionTemplate;
    }

    /**
     * @param targetRefundPaymentService
     *            the targetRefundPaymentService to set
     */
    @Required
    public void setTargetRefundPaymentService(final TargetRefundPaymentService targetRefundPaymentService) {
        this.targetRefundPaymentService = targetRefundPaymentService;
    }

    /**
     * @param targetTicketBusinessService
     *            the targetTicketBusinessService to set
     */
    @Required
    public void setTargetTicketBusinessService(final TargetTicketBusinessService targetTicketBusinessService) {
        this.targetTicketBusinessService = targetTicketBusinessService;
    }

    /**
     * @param orderCancelRecordsHandler
     *            the orderCancelRecordsHandler to set
     */
    @Required
    public void setOrderCancelRecordsHandler(final OrderCancelRecordsHandler orderCancelRecordsHandler) {
        this.orderCancelRecordsHandler = orderCancelRecordsHandler;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param findOrderTotalPaymentMadeStrategy
     *            the findOrderTotalPaymentMadeStrategy to set
     */
    @Required
    public void setFindOrderTotalPaymentMadeStrategy(
            final TargetFindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy) {
        this.findOrderTotalPaymentMadeStrategy = findOrderTotalPaymentMadeStrategy;
    }

    /**
     * @param targetCancelService
     *            the targetCancelService to set
     */
    @Required
    public void setTargetCancelService(final TargetCancelService targetCancelService) {
        this.targetCancelService = targetCancelService;
    }

    /**
     * @param voucherCorrectionStrategy
     *            the voucherCorrectionStrategy to set
     */
    @Required
    public void setVoucherCorrectionStrategy(final VoucherCorrectionStrategy voucherCorrectionStrategy) {
        this.voucherCorrectionStrategy = voucherCorrectionStrategy;
    }

    /**
     * @param flybuysDiscountService
     *            the flybuysDiscountService to set
     */
    @Required
    public void setFlybuysDiscountService(final FlybuysDiscountService flybuysDiscountService) {
        this.flybuysDiscountService = flybuysDiscountService;
    }

    /**
     * @param salesApplicationConfigService
     *            the salesApplicationConfigService to set
     */
    @Required
    public void setSalesApplicationConfigService(
            final TargetFulfilmentSalesApplicationConfigService salesApplicationConfigService) {
        this.salesApplicationConfigService = salesApplicationConfigService;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

}

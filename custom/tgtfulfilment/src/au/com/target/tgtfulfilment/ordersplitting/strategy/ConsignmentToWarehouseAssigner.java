/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy;

import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.enums.OfcOrderType;


/**
 * @author sbryan6
 *
 */
public interface ConsignmentToWarehouseAssigner {

    /**
     * Assign consignment to target store
     * 
     * @param consignment
     * @param warehouse
     * @param orderType
     * @param deliveryMode
     * @param orderCode
     */
    void assignConsignmentToStore(TargetConsignmentModel consignment, WarehouseModel warehouse, OfcOrderType orderType,
            TargetZoneDeliveryModeModel deliveryMode, String orderCode);

    /**
     * Assign consignment to an NTL
     * 
     * @param consignment
     * @param warehouse
     */
    void assignConsignmentToNTL(TargetConsignmentModel consignment, WarehouseModel warehouse);

    /**
     * Assign consignment to given warehouse with no tpos
     * 
     * @param consignment
     * @param warehouse
     */
    void assignConsignmentToExternalWarehouse(TargetZoneDeliveryModeModel deliveryMode,
            TargetConsignmentModel consignment, WarehouseModel warehouse);

    /**
     * Assign consignment to default warehouse
     * 
     * @param consignment
     */
    void assignDefaultWarehouse(TargetConsignmentModel consignment);

    /**
     * assign Cancellation Warehouse And Update ConsignmentStatus
     * 
     * @param consignment
     */
    void assignCancellationWarehouseAndUpdateConsignmentStatus(TargetConsignmentModel consignment);

}

/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * Strategy to be implemented to decide the routing rules
 * 
 * @author rsamuel3
 *
 */
public interface GlobalFulfilmentDenialStrategy {

    /**
     * Checks if all the routing rules are passed as part of this strategy
     * 
     * @param orderEntryGroup
     * @return DenialResponse which contains the error string and boolean indicating whether it is denied
     */
    DenialResponse isDenied(final OrderEntryGroup orderEntryGroup);

}

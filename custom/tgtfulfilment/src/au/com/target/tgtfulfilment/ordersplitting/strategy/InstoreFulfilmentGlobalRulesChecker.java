/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;


/**
 * @author sbryan6
 *
 */
public interface InstoreFulfilmentGlobalRulesChecker {


    /**
     * Check all the global rules for instore fulfilment
     * 
     * @param oeg
     * @return true if all rules passed
     */
    boolean areAllGlobalRulesPassed(final OrderEntryGroup oeg);


    /**
     * Check all the global dispatch rules for instore fulfilment PPD
     * 
     * @param oeg
     * @return true if all rules passed
     */
    boolean areAllDispatchRulesPassed(final OrderEntryGroup oeg);

}

package au.com.target.tgtfulfilment.ordersplitting.strategy;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;


/**
 * @author rsamuel3
 *
 */
public interface StoreSelectorStrategy {

    /**
     * Select the store to route the order to
     * 
     * @param oeg
     * @return TargetPointOfServiceModel
     */
    TargetPointOfServiceModel selectStore(OrderEntryGroup oeg);
}

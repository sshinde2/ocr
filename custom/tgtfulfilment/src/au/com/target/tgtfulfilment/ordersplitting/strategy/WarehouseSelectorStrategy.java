/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import au.com.target.tgtfulfilment.exceptions.TargetSplitOrderException;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.SplitOegResult;


/**
 * @author Nandini
 *
 */
public interface WarehouseSelectorStrategy {

    /**
     * Split the oeg if required for NTL
     * 
     * @param oeg
     * @return List of OrderEntryGroup
     * @throws TargetSplitOrderException
     */
    SplitOegResult split(OrderEntryGroup oeg) throws TargetSplitOrderException;
}

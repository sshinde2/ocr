/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.bean;

/**
 * Response from denial strategy
 * 
 * @author sbryan6
 *
 */
public class DenialResponse {

    private final boolean isDenied;
    private final String reason;

    /**
     * Private Constructor - use factory methods
     */
    private DenialResponse(final boolean isDenied, final String reason) {
        super();
        this.isDenied = isDenied;
        this.reason = reason;
    }

    /**
     * Create a not denied DenialReponse
     * 
     * @return DenialReponse
     */
    public static DenialResponse createNotDenied() {
        return new DenialResponse(false, null);
    }

    /**
     * Create a denied DenialReponse
     * 
     * @param reason
     * @returnDenialResponse
     */
    public static DenialResponse createDenied(final String reason) {
        return new DenialResponse(true, reason);
    }

    /**
     * @return the isDenied
     */
    public boolean isDenied() {
        return isDenied;
    }

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }


}

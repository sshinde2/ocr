/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.bean;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.List;


/**
 * @author smishra1
 *
 */
public class SplitOegResult {

    private List<OrderEntryGroup> assignedOEGsToWarehouse;
    private OrderEntryGroup notAssignedToWarehouse;


    /**
     * @return the assignedOEGsToWarehouse
     */
    public List<OrderEntryGroup> getAssignedOEGsToWarehouse() {
        return assignedOEGsToWarehouse;
    }

    /**
     * @param assignedOEGsToWarehouse
     *            the assignedOEGsToWarehouse to set
     */
    public void setAssignedOEGsToWarehouse(final List<OrderEntryGroup> assignedOEGsToWarehouse) {
        this.assignedOEGsToWarehouse = assignedOEGsToWarehouse;
    }

    /**
     * @return the notAssignedToWarehouse
     */
    public OrderEntryGroup getNotAssignedToWarehouse() {
        return notAssignedToWarehouse;
    }

    /**
     * @param notAssignedToWarehouse
     *            the notAssignedToWarehouse to set
     */
    public void setNotAssignedToWarehouse(final OrderEntryGroup notAssignedToWarehouse) {
        this.notAssignedToWarehouse = notAssignedToWarehouse;
    }

}

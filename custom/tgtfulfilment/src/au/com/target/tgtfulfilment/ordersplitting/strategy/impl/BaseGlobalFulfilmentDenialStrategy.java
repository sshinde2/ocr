/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.ordersplitting.strategy.GlobalFulfilmentDenialStrategy;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;


/**
 * @author rsamuel3
 *
 */
public abstract class BaseGlobalFulfilmentDenialStrategy implements GlobalFulfilmentDenialStrategy {

    private OEGParameterHelper oegParameterHelper;

    private TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService;

    /**
     * @return the oegParameterHelper
     */
    protected OEGParameterHelper getOegParameterHelper() {
        return oegParameterHelper;
    }

    /**
     * @param oegParameterHelper
     *            the oegParameterHelper to set
     */
    @Required
    public void setOegParameterHelper(final OEGParameterHelper oegParameterHelper) {
        this.oegParameterHelper = oegParameterHelper;
    }

    /**
     * @return the targetGlobalStoreFulfilmentService
     */
    protected TargetGlobalStoreFulfilmentService getTargetGlobalStoreFulfilmentService() {
        return targetGlobalStoreFulfilmentService;
    }

    /**
     * @param targetGlobalStoreFulfilmentService
     *            the targetGlobalStoreFulfilmentService to set
     */
    @Required
    public void setTargetGlobalStoreFulfilmentService(
            final TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService) {
        this.targetGlobalStoreFulfilmentService = targetGlobalStoreFulfilmentService;
    }

}

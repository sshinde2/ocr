/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.ordersplitting.strategy.SpecificStoreFulfilmentDenialStrategy;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;


/**
 * Base for a SpecificStoreFulfilmentDenialStrategy
 * 
 * @author sbryan6
 *
 */
public abstract class BaseSpecificStoreFulfilmentDenialStrategy implements SpecificStoreFulfilmentDenialStrategy {

    private OEGParameterHelper oegParameterHelper;

    private TargetStoreFulfilmentCapabilitiesService storeFulfilmentCapabilitiesService;

    /**
     * @return the oegParameterHelper
     */
    protected OEGParameterHelper getOegParameterHelper() {
        return oegParameterHelper;
    }

    /**
     * @param oegParameterHelper
     *            the oegParameterHelper to set
     */
    @Required
    public void setOegParameterHelper(final OEGParameterHelper oegParameterHelper) {
        this.oegParameterHelper = oegParameterHelper;
    }

    /**
     * @return the storeFulfilmentCapabilitiesService
     */
    protected TargetStoreFulfilmentCapabilitiesService getStoreFulfilmentCapabilitiesService() {
        return storeFulfilmentCapabilitiesService;
    }

    /**
     * @param storeFulfilmentCapabilitiesService
     *            the storeFulfilmentCapabilitiesService to set
     */
    @Required
    public void setStoreFulfilmentCapabilitiesService(
            final TargetStoreFulfilmentCapabilitiesService storeFulfilmentCapabilitiesService) {
        this.storeFulfilmentCapabilitiesService = storeFulfilmentCapabilitiesService;
    }

}

/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.SplittingStrategy;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.helper.WarehouseHelper;
import au.com.target.tgtfulfilment.logger.RoutingLogger;
import au.com.target.tgtfulfilment.ordersplitting.strategy.ConsignmentToWarehouseAssigner;
import au.com.target.tgtfulfilment.ordersplitting.strategy.InstoreFulfilmentGlobalRulesChecker;
import au.com.target.tgtfulfilment.ordersplitting.strategy.StoreSelectorStrategy;


/**
 * Strategy to re-route a consignment if a store is not able to fulfill
 * 
 * @author jjayawa1
 *
 */
public class ConsignmentReroutingStrategy implements SplittingStrategy {
    private static final RoutingLogger LOG = new RoutingLogger(ConsignmentReroutingStrategy.class);

    private WarehouseHelper warehouseHelper;

    private StoreSelectorStrategy storeSelectorStrategy;

    private OEGParameterHelper oegParameterHelper;

    private InstoreFulfilmentGlobalRulesChecker instoreFulfilmentGlobalRulesChecker;

    private ConsignmentToWarehouseAssigner consignmentToWarehouseAssigner;

    @Override
    public List<OrderEntryGroup> perform(final List<OrderEntryGroup> orderEntryGroups) {

        // order entry group is empty, returning it without doing anything
        if (CollectionUtils.isEmpty(orderEntryGroups)) {
            return orderEntryGroups;
        }

        Assert.isTrue(orderEntryGroups.size() == 1,
                "Only one entry should be available for re-routing");

        final OrderEntryGroup originalOEG = orderEntryGroups.get(0);
        oegParameterHelper.populateParamsFromOrder(originalOEG);
        // must have this step to set the required qty on OEG, stock look up will rely on this
        oegParameterHelper.setRequiredQtyByOrderEntries(originalOEG);
        oegParameterHelper.setAssignedQtyByOrderEntries(originalOEG);

        final String orderCode = oegParameterHelper.getOrderCode(originalOEG);

        // For any exception, handle it and go to default warehouse
        try {
            LOG.logRulesEntryMessage(orderCode);

            // In-store fulfilment 
            if (instoreFulfilmentGlobalRulesChecker.areAllGlobalRulesPassed(originalOEG)) {
                if (instoreFulfilmentGlobalRulesChecker.areAllDispatchRulesPassed(originalOEG)) {

                    final TargetPointOfServiceModel tpos = storeSelectorStrategy.selectStore(originalOEG);
                    if (tpos != null) {
                        oegParameterHelper.assignTposToOEG(originalOEG, tpos);
                    }
                }
            }
        }
        catch (final Exception e) {
            LOG.logRulesExitException(orderCode, "UnexpectedException", e);
        }

        return Collections.singletonList(originalOEG);
    }

    @Override
    public void afterSplitting(final OrderEntryGroup orderEntryGroup, final ConsignmentModel createdOne) {
        Assert.isTrue(createdOne instanceof TargetConsignmentModel, "Consignment must be TargetConsignmentModel");
        final TargetConsignmentModel targetConsignment = (TargetConsignmentModel)createdOne;
        assignConsignmentToWarehouse(orderEntryGroup, targetConsignment);
    }

    /**
     * Given the oeg.warehouse assign the consignment to warehouse and carrier
     * 
     * @param orderEntryGroup
     * @param targetConsignment
     */
    private void assignConsignmentToWarehouse(final OrderEntryGroup orderEntryGroup,
            final TargetConsignmentModel targetConsignment) {

        final WarehouseModel warehouse = oegParameterHelper.getFulfilmentWarehouse(orderEntryGroup);
        final TargetZoneDeliveryModeModel deliveryMode = oegParameterHelper.getDeliveryMode(orderEntryGroup);
        final TargetPointOfServiceModel tpos = warehouseHelper.getAssignedStoreForWarehouse(warehouse);

        if (warehouse == null || tpos == null) {
            consignmentToWarehouseAssigner.assignDefaultWarehouse(targetConsignment);
            return;
        }

        // Target store
        final OfcOrderType orderType = oegParameterHelper.getOfcOrderType(orderEntryGroup, tpos.getStoreNumber());
        final String orderCode = oegParameterHelper.getOrderCode(orderEntryGroup);
        consignmentToWarehouseAssigner.assignConsignmentToStore(targetConsignment, warehouse, orderType, deliveryMode,
                orderCode);
    }

    /**
     * @param warehouseHelper
     *            the warehouseHelper to set
     */
    @Required
    public void setWarehouseHelper(final WarehouseHelper warehouseHelper) {
        this.warehouseHelper = warehouseHelper;
    }

    /**
     * @param oegParameterHelper
     *            the oegParameterHelper to set
     */
    @Required
    public void setOegParameterHelper(final OEGParameterHelper oegParameterHelper) {
        this.oegParameterHelper = oegParameterHelper;
    }

    /**
     * @param storeSelectorStrategy
     *            the storeSelectorStrategy to set
     */
    @Required
    public void setStoreSelectorStrategy(final StoreSelectorStrategy storeSelectorStrategy) {
        this.storeSelectorStrategy = storeSelectorStrategy;
    }

    /**
     * @param instoreFulfilmentGlobalRulesChecker
     *            the instoreFulfilmentGlobalRulesChecker to set
     */
    @Required
    public void setInstoreFulfilmentGlobalRulesChecker(
            final InstoreFulfilmentGlobalRulesChecker instoreFulfilmentGlobalRulesChecker) {
        this.instoreFulfilmentGlobalRulesChecker = instoreFulfilmentGlobalRulesChecker;
    }

    /**
     * @param consignmentToWarehouseAssigner
     *            the consignmentToWarehouseAssigner to set
     */
    @Required
    public void setConsignmentToWarehouseAssigner(final ConsignmentToWarehouseAssigner consignmentToWarehouseAssigner) {
        this.consignmentToWarehouseAssigner = consignmentToWarehouseAssigner;
    }

}

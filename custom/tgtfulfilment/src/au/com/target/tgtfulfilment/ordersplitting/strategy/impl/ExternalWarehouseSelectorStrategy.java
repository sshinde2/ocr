/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;
import de.hybris.platform.stock.exception.StockLevelNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.util.ProductUtil;
import au.com.target.tgtcore.warehouse.dao.TargetWarehouseDao;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.logger.RoutingLogger;
import au.com.target.tgtfulfilment.ordersplitting.strategy.WarehouseSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.SplitOegResult;


/**
 * 
 * @author vivek
 *
 */
public class ExternalWarehouseSelectorStrategy implements WarehouseSelectorStrategy {
    private static final RoutingLogger ROUTING_LOG = new RoutingLogger(ExternalWarehouseSelectorStrategy.class);

    private TargetStockService targetStockService;
    private OEGParameterHelper oegParameterHelper;
    private TargetWarehouseDao targetWarehouseDao;

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.ordersplitting.strategy.WarehouseSelectorStrategy#split(de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup, java.util.List)
     */
    @Override
    public SplitOegResult split(final OrderEntryGroup oeg) {
        final SplitOegResult result = new SplitOegResult();
        OrderEntryGroup notAssignedToWarehouse = oeg;
        final String orderCode = oegParameterHelper.getOrderCode(oeg);
        final List<OrderEntryGroup> assignedOEGs = new ArrayList<>();
        try {
            Assert.notEmpty(oeg, "OEG cannot be empty");


            final List<WarehouseModel> warehouses = targetWarehouseDao.getExternalWarehouses(true);

            if (CollectionUtils.isNotEmpty(warehouses)) {
                for (final WarehouseModel warehouse : warehouses) {
                    notAssignedToWarehouse = assignEntriesToWarehouse(orderCode, notAssignedToWarehouse, warehouse,
                            assignedOEGs);
                }
            }
            else {
                ROUTING_LOG.logNoExternalWarehouseFound(orderCode);
            }

            if (CollectionUtils.isEmpty(assignedOEGs)) {
                ROUTING_LOG.logNoExternalProductFoundInTheOrder(orderCode);
            }
            else {
                populateParametersToAssignedOegs(assignedOEGs);
            }
        }

        catch (final Exception e) {
            ROUTING_LOG.logUnexpectedExceptionMessage(orderCode, "ExternalWarehouseRoutingException", e);
        }
        result.setAssignedOEGsToWarehouse(assignedOEGs);
        populateParametersToNotAssignedOeg(notAssignedToWarehouse);
        result.setNotAssignedToWarehouse(notAssignedToWarehouse);
        final String deliveryModeCode = oegParameterHelper.getDeliveryMode(oeg) != null ? oegParameterHelper
                .getDeliveryMode(oeg).getCode() : StringUtils.EMPTY;
        final String cncStoreNumber = oegParameterHelper.getCncStoreNumber(oeg) != null ? oegParameterHelper
                .getCncStoreNumber(oeg).toString() : StringUtils.EMPTY;
        ROUTING_LOG.logSplitOegResult(orderCode, deliveryModeCode, cncStoreNumber, "ExternalWarehouseSelectorStrategy",
                result.getAssignedOEGsToWarehouse(), result.getNotAssignedToWarehouse());
        return result;
    }

    private void populateParametersToNotAssignedOeg(final OrderEntryGroup notAssignedToWarehouse) {
        if (CollectionUtils.isNotEmpty(notAssignedToWarehouse)) {
            oegParameterHelper.populateParamsFromOrder(notAssignedToWarehouse);
        }
    }

    private void populateParametersToAssignedOegs(final List<OrderEntryGroup> assignedOEGs) {
        if (CollectionUtils.isNotEmpty(assignedOEGs)) {
            oegParameterHelper.populateParamsFromOrder(assignedOEGs);
        }
    }

    private final OrderEntryGroup assignEntriesToWarehouse(final String orderCode, final OrderEntryGroup oeg,
            final WarehouseModel warehouse,
            final List<OrderEntryGroup> assignedOEGs) {
        final Map<String, Long> requiredQtyMap = oegParameterHelper.getRequiredQty(oeg);
        final OrderEntryGroup notAssignedToWarehouse = new OrderEntryGroup();
        final Map<String, Long> notAssignedRequiredQtyMap = new HashMap<>();
        oegParameterHelper.setRequiredQty(notAssignedToWarehouse, notAssignedRequiredQtyMap);

        final OrderEntryGroup assignedToWarehouseDigitalGiftCard = new OrderEntryGroup();
        final Map<String, Long> assignedToWarehouseDigitalGiftCardQtyMap = new HashMap<>();
        oegParameterHelper.setAssignedQty(assignedToWarehouseDigitalGiftCard, assignedToWarehouseDigitalGiftCardQtyMap);

        final OrderEntryGroup assignedToWarehousePhysicalGiftCard = new OrderEntryGroup();
        final Map<String, Long> assignedToWarehousePhysicalGiftCardQtyMap = new HashMap<>();
        oegParameterHelper.setAssignedQty(assignedToWarehousePhysicalGiftCard,
                assignedToWarehousePhysicalGiftCardQtyMap);

        final OrderEntryGroup assignedToWarehouse = new OrderEntryGroup();
        final Map<String, Long> assignedToWarehouseQtyMap = new HashMap<>();
        oegParameterHelper.setAssignedQty(assignedToWarehouse, assignedToWarehouseQtyMap);

        StockLevelModel stockLevel = null;
        for (final AbstractOrderEntryModel entry : oeg) {
            final ProductModel product = entry.getProduct();
            try {
                stockLevel = targetStockService.checkAndGetStockLevel(product, warehouse);
            }
            catch (final StockLevelNotFoundException e) {
                stockLevel = null;
            }
            if (null == stockLevel) {
                notAssignedToWarehouse.add(entry);
                notAssignedRequiredQtyMap.put(product.getCode(), requiredQtyMap.get(product.getCode()));
            }
            else {
                if (ProductUtil.isProductTypeDigital(product)) {
                    assignedToWarehouseDigitalGiftCard.add(entry);
                    assignedToWarehouseDigitalGiftCardQtyMap.put(product.getCode(),
                            requiredQtyMap.get(product.getCode()));
                }
                else if (ProductUtil.isProductTypePhysicalGiftcard(product)) {
                    assignedToWarehousePhysicalGiftCard.add(entry);
                    assignedToWarehousePhysicalGiftCardQtyMap.put(product.getCode(),
                            requiredQtyMap.get(product.getCode()));
                }
                else {
                    assignedToWarehouse.add(entry);
                    assignedToWarehouseQtyMap.put(product.getCode(),
                            requiredQtyMap.get(product.getCode()));
                }
            }
        }
        //normal product
        assignDetailsToWarehouse(orderCode, warehouse, assignedOEGs, assignedToWarehouse);
        // digital giftcard
        assignDetailsToWarehouse(orderCode, warehouse, assignedOEGs, assignedToWarehouseDigitalGiftCard);
        // physical giftcard
        assignDetailsToWarehouse(orderCode, warehouse, assignedOEGs, assignedToWarehousePhysicalGiftCard);

        return notAssignedToWarehouse;
    }

    /**
     * @param orderCode
     * @param warehouse
     * @param assignedOEGs
     * @param assignedToWarehouse
     */
    private void assignDetailsToWarehouse(final String orderCode, final WarehouseModel warehouse,
            final List<OrderEntryGroup> assignedOEGs, final OrderEntryGroup assignedToWarehouse) {
        if (CollectionUtils.isNotEmpty(assignedToWarehouse)) {
            ROUTING_LOG.logAssignedToWarehouseMessage(orderCode, warehouse.getCode());
            oegParameterHelper.assignWarehouseToOEG(assignedToWarehouse, warehouse);
            assignedOEGs.add(assignedToWarehouse);
        }
    }

    /**
     * @param targetStockService
     *            the targetStockService to set
     */
    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }

    /**
     * @param oegParameterHelper
     *            the oegParameterHelper to set
     */
    @Required
    public void setOegParameterHelper(final OEGParameterHelper oegParameterHelper) {
        this.oegParameterHelper = oegParameterHelper;
    }

    /**
     * @param targetWarehouseDao
     *            the targetWarehouseDao to set
     */
    @Required
    public void setTargetWarehouseDao(final TargetWarehouseDao targetWarehouseDao) {
        this.targetWarehouseDao = targetWarehouseDao;
    }

}

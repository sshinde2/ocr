/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;
import de.hybris.platform.stock.exception.StockLevelNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.ordersplitting.strategy.WarehouseSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.SplitOegResult;


/**
 * 
 * @author bhuang3
 *
 */
public class FastlineSplitConsignmentStrategy implements WarehouseSelectorStrategy {

    private static final Logger LOG = Logger.getLogger(FastlineSplitConsignmentStrategy.class);

    private OEGParameterHelper oegParameterHelper;

    private TargetWarehouseService targetWarehouseService;

    private TargetStockService targetStockService;

    private TargetPointOfServiceService targetPointOfServiceService;

    private TargetSharedConfigService targetSharedConfigService;

    @Override
    public SplitOegResult split(final OrderEntryGroup oeg) {
        Assert.notEmpty(oeg, "OEG cannot be empty");
        final SplitOegResult result = new SplitOegResult();
        final List<OrderEntryGroup> assignedOEGs = new ArrayList<>();
        final OrderEntryGroup assignedOeg = new OrderEntryGroup();
        oegParameterHelper.setAssignedQty(assignedOeg, new HashMap<String, Long>());
        final OrderEntryGroup notAssignedOeg = new OrderEntryGroup();
        oegParameterHelper.setRequiredQty(notAssignedOeg, new HashMap<String, Long>());
        this.splitOegByFastlineStock(oeg, assignedOeg, notAssignedOeg);
        checkFulfilledByFastlinePerDayCount(assignedOeg, notAssignedOeg);
        if (CollectionUtils.isNotEmpty(assignedOeg)) {
            oegParameterHelper.setTpos(assignedOeg, targetPointOfServiceService.getTposOfDefaultWarehouse());
            oegParameterHelper.populateParamsFromOrder(assignedOeg);
            assignedOEGs.add(assignedOeg);

        }
        result.setAssignedOEGsToWarehouse(assignedOEGs);
        oegParameterHelper.populateParamsFromOrder(notAssignedOeg);
        result.setNotAssignedToWarehouse(notAssignedOeg);
        return result;
    }

    protected void checkFulfilledByFastlinePerDayCount(final OrderEntryGroup assignedOeg,
            final OrderEntryGroup notAssignedOeg) {

        int currentQtyAvailableToFulfill = getCurrentQuantityAvailableToFulfill();

        final Map<String, Long> assignedQtyMap = oegParameterHelper.getAssignedQty(assignedOeg);
        final Map<String, Long> requiredQtyMap = oegParameterHelper.getRequiredQty(notAssignedOeg);

        final Iterator<AbstractOrderEntryModel> orderEntryIterator = assignedOeg.iterator();
        while (orderEntryIterator.hasNext()) {
            final AbstractOrderEntryModel entry = orderEntryIterator.next();
            final String productCode = entry.getProduct().getCode();
            final Long assignedQty = assignedQtyMap.get(productCode);
            if (currentQtyAvailableToFulfill <= 0) {
                if (!notAssignedOeg.contains(entry)) {
                    notAssignedOeg.add(entry);
                    requiredQtyMap.put(entry.getProduct().getCode(),
                            assignedQtyMap.get(entry.getProduct().getCode()));
                    assignedQtyMap.remove(entry.getProduct().getCode());
                    orderEntryIterator.remove();
                }
                else {
                    final Long requiredQty = requiredQtyMap.get(productCode);
                    requiredQtyMap.put(productCode,
                            Long.valueOf(requiredQty.longValue() + assignedQty.longValue()));
                    assignedQtyMap.remove(entry.getProduct().getCode());
                    orderEntryIterator.remove();
                }
                continue;
            }

            currentQtyAvailableToFulfill -= assignedQty.intValue();
            if (currentQtyAvailableToFulfill < 0) {
                assignedQtyMap.put(productCode,
                        Long.valueOf(assignedQty.longValue() + currentQtyAvailableToFulfill));
                if (requiredQtyMap.containsKey(productCode)) {
                    requiredQtyMap.put(productCode, Long
                            .valueOf(requiredQtyMap.get(productCode).longValue() - currentQtyAvailableToFulfill));
                }
                else {
                    requiredQtyMap.put(productCode, Long.valueOf(-currentQtyAvailableToFulfill));
                    notAssignedOeg.add(entry);
                }

            }
        }


        oegParameterHelper.setAssignedQty(assignedOeg, assignedQtyMap);
        oegParameterHelper.setRequiredQty(notAssignedOeg, requiredQtyMap);

    }


    /**
     * @return currentAvailableQuntity to fulfill
     */
    private int getCurrentQuantityAvailableToFulfill() {
        final int maxQtyFastlineFulfillPerDay = targetSharedConfigService
                .getInt(TgtCoreConstants.Config.MAX_QTY_FASTLINE_FULFILLED_PER_DAY, 100);
        final WarehouseModel warehouse = targetWarehouseService.getDefaultOnlineWarehouse();
        final int currentFulfilledQuantity = warehouse.getTotalQtyFulfilledAtFastlineToday();
        return maxQtyFastlineFulfillPerDay - currentFulfilledQuantity;
    }

    protected void splitOegByFastlineStock(final OrderEntryGroup oeg,
            final OrderEntryGroup assigned, final OrderEntryGroup notAssigned) {
        final Map<String, Long> requiredQtyMap = oegParameterHelper.getRequiredQty(oeg);
        final WarehouseModel warehouse = targetWarehouseService.getDefaultOnlineWarehouse();
        final Map<String, Long> requiredQtyForNonAssignedOegMap = oegParameterHelper.getRequiredQty(notAssigned);
        final Map<String, Long> assignedQtyMap = oegParameterHelper.getAssignedQty(assigned);
        for (final AbstractOrderEntryModel entry : oeg) {
            if (entry.getProduct() != null) {
                final String productCode = entry.getProduct().getCode();
                final ProductModel productModel = entry.getProduct();
                final Long requiredQty = requiredQtyMap.get(productCode);
                final Long sohQty = Long.valueOf(getFastlineStock(productModel, warehouse));
                if (requiredQty == null || requiredQty.longValue() <= 0 || sohQty == null || sohQty.longValue() <= 0) {
                    notAssigned.add(entry);
                    requiredQtyForNonAssignedOegMap.put(productCode, requiredQty);
                }
                else {
                    final long diff = requiredQty.longValue() - sohQty.longValue();
                    if (diff <= 0) {
                        assigned.add(entry);
                        assignedQtyMap.put(productCode, requiredQty);
                    }
                    else {
                        // split the order entry into 2, partial assigned and partial not
                        notAssigned.add(entry);
                        requiredQtyForNonAssignedOegMap.put(productCode, Long.valueOf(diff));
                        assigned.add(entry);
                        assignedQtyMap.put(productCode, sohQty);
                    }
                }
            }

        }

    }

    private int getFastlineStock(final ProductModel productModel, final WarehouseModel warehouse) {
        int qty = 0;
        try {
            qty = targetStockService.getStockLevelAmount(productModel, warehouse);
            if (qty > 0) {
                final int pendingQuantity = targetPointOfServiceService
                        .getFulfilmentPendingQuantity(TgtCoreConstants.ONLINE_STORE_NUMBER, productModel);
                qty = qty - pendingQuantity;
                LOG.info("fastline stock=" + qty + " pendingQty=" + pendingQuantity + " for product="
                        + productModel.getCode());
            }
        }
        catch (final StockLevelNotFoundException e) {
            qty = 0;
        }
        return qty;
    }

    /**
     * @param oegParameterHelper
     *            the oegParameterHelper to set
     */
    @Required
    public void setOegParameterHelper(final OEGParameterHelper oegParameterHelper) {
        this.oegParameterHelper = oegParameterHelper;
    }


    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */
    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }

    /**
     * @param targetStockService
     *            the targetStockService to set
     */
    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }

    /**
     * @param targetPointOfServiceService
     *            the targetPointOfServiceService to set
     */
    @Required
    public void setTargetPointOfServiceService(final TargetPointOfServiceService targetPointOfServiceService) {
        this.targetPointOfServiceService = targetPointOfServiceService;
    }

    /**
     * @param targetSharedConfigService
     *            the targetSharedConfigService to set
     */
    @Required
    public void setTargetSharedConfigService(final TargetSharedConfigService targetSharedConfigService) {
        this.targetSharedConfigService = targetSharedConfigService;
    }

}

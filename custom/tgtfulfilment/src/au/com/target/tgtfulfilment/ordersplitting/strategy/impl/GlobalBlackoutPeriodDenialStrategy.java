/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.Date;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.storeblackout.StoreBlackOutService;


/**
 * @author rsamuel3
 *
 */
public class GlobalBlackoutPeriodDenialStrategy extends BaseGlobalFulfilmentDenialStrategy {

    private StoreBlackOutService storeBlackOutService;

    @Override
    public DenialResponse isDenied(final OrderEntryGroup orderEntryGroup) {
        final GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel = getTargetGlobalStoreFulfilmentService()
                .getGlobalStoreFulfilmentCapabilites();
        if (isNotInGlobalBlackoutPeriod(capabilitiesModel.getBlackoutStart(), capabilitiesModel.getBlackoutEnd())) {
            return DenialResponse.createNotDenied();
        }
        return DenialResponse.createDenied("GlobalInBlackoutPeriod");
    }

    /**
     * Method to check if Global blackout exist
     * 
     * @param startDate
     * @param endDate
     * @return boolean
     */
    private boolean isNotInGlobalBlackoutPeriod(final Date startDate, final Date endDate) {
        return !storeBlackOutService.doesBlackOutPeriodApply(startDate, endDate);
    }

    /**
     * @param storeBlackOutService
     *            the storeBlackOutService to set
     */
    @Required
    public void setStoreBlackOutService(final StoreBlackOutService storeBlackOutService) {
        this.storeBlackOutService = storeBlackOutService;
    }

}

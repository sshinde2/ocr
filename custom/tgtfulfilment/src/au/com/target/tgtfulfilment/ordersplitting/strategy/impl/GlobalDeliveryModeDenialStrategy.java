/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * @author rsamuel3
 *
 */
public class GlobalDeliveryModeDenialStrategy extends BaseGlobalFulfilmentDenialStrategy {

    @Override
    public DenialResponse isDenied(final OrderEntryGroup orderEntryGroup) {
        final GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel = getTargetGlobalStoreFulfilmentService()
                .getGlobalStoreFulfilmentCapabilites();
        if (isDeliveryModeEnabledForInStoreCapability(getOegParameterHelper().getDeliveryMode(orderEntryGroup),
                capabilitiesModel.getDeliveryModes())) {
            return DenialResponse.createNotDenied();
        }
        return DenialResponse.createDenied("GlobalDeliveryModeDisabled");
    }

    /**
     * Method to check if delivery mode is enabled for In-Store fulfilment
     * 
     * @param ordersDeliveryMode
     * @param allowedDeliveryModes
     * @return boolean
     */
    private boolean isDeliveryModeEnabledForInStoreCapability(final DeliveryModeModel ordersDeliveryMode,
            final Set<TargetZoneDeliveryModeModel> allowedDeliveryModes) {
        if (CollectionUtils.isNotEmpty(allowedDeliveryModes)) {
            for (final TargetZoneDeliveryModeModel zoneDeliveryMode : allowedDeliveryModes) {
                if (StringUtils.equals(ordersDeliveryMode.getCode(), zoneDeliveryMode.getCode())) {
                    return true;
                }
            }
        }
        return false;
    }
}

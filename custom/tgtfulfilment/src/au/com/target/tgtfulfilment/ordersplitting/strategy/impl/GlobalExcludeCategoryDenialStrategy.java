/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.FilterExclusionListService;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;


/**
 * @author bhuang3
 *
 */
public class GlobalExcludeCategoryDenialStrategy extends BaseGlobalFulfilmentDenialStrategy {

    private TargetStoreFulfilmentCapabilitiesService storeFulfilmentCapabilitiesService;

    private FilterExclusionListService filterExclusionListService;

    @Override
    public DenialResponse isDenied(final OrderEntryGroup orderEntryGroup) {

        final String orderCode = getOegParameterHelper().getOrderCode(orderEntryGroup);
        Assert.notEmpty(orderEntryGroup, "orderEntryGroup cannot be empty" + orderCode);
        final List<CategoryModel> excludeCategories = getExcludeCategories();

        if (CollectionUtils.isNotEmpty(excludeCategories) && storeFulfilmentCapabilitiesService
                .isOrderContainingProductsFromExcludedCategories(orderEntryGroup, excludeCategories)) {
            return DenialResponse.createDenied("GlobalExcludeCategoryDenialStrategy");
        }
        return DenialResponse.createNotDenied();
    }

    private List<CategoryModel> getExcludeCategories() {
        final GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel = getTargetGlobalStoreFulfilmentService()
                .getGlobalStoreFulfilmentCapabilites();

        List<CategoryModel> excludeCategories = null;
        if (capabilitiesModel != null && capabilitiesModel.getExcludeCategories() != null) {
            excludeCategories = filterExclusionListService.filterActiveCategoryModelsByDateRange(capabilitiesModel
                    .getExcludeCategories());
        }
        return excludeCategories;
    }

    /**
     * @param storeFulfilmentCapabilitiesService
     *            the storeFulfilmentCapabilitiesService to set
     */
    @Required
    public void setStoreFulfilmentCapabilitiesService(
            final TargetStoreFulfilmentCapabilitiesService storeFulfilmentCapabilitiesService) {
        this.storeFulfilmentCapabilitiesService = storeFulfilmentCapabilitiesService;
    }

    /**
     * @param filterExclusionListService
     *            the filterExclusionListService to set
     */
    @Required
    public void setFilterExclusionListService(final FilterExclusionListService filterExclusionListService) {
        this.filterExclusionListService = filterExclusionListService;
    }
}

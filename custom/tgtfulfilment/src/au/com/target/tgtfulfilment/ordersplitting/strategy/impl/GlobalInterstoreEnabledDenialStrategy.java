/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * @author smishra1
 *
 */
public class GlobalInterstoreEnabledDenialStrategy extends BaseGlobalFulfilmentDenialStrategy {

    @Override
    public DenialResponse isDenied(final OrderEntryGroup orderEntryGroup) {
        final GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel = getTargetGlobalStoreFulfilmentService()
                .getGlobalStoreFulfilmentCapabilites();
        if (CollectionUtils.isNotEmpty(orderEntryGroup)) {
            if (null != getOegParameterHelper().getCncStoreNumber(orderEntryGroup)
                    && !capabilitiesModel.isAllowInterstoreDelivery()) {
                return DenialResponse.createDenied("InterStoreDeliveryDisabled");
            }
        }
        return DenialResponse.createNotDenied();
    }
}

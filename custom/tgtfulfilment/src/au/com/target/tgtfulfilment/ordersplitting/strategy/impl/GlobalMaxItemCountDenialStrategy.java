/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * Check if the maximum count of items in order is more than allowed for the store
 * 
 * @author rsamuel3
 *
 */
public class GlobalMaxItemCountDenialStrategy extends BaseGlobalFulfilmentDenialStrategy {

    @Override
    public DenialResponse isDenied(final OrderEntryGroup orderEntryGroup) {
        final GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel = getTargetGlobalStoreFulfilmentService()
                .getGlobalStoreFulfilmentCapabilites();
        int numberOfItemsInOrder = 0;
        if (CollectionUtils.isNotEmpty(orderEntryGroup)) {
            numberOfItemsInOrder = getNumberOfItemsInOrder(orderEntryGroup);
        }
        if (isItemCountLessThanMaximumAllowed(numberOfItemsInOrder,
                capabilitiesModel.getMaxItemsPerConsignment().intValue())) {
            return DenialResponse.createNotDenied();
        }
        return DenialResponse.createDenied("GlobalMaxItemCountExceeded");
    }

    /**
     * Method to get number of items in the order (sum of entry quantity)
     * 
     * @param orderEntryGroup
     * @return numberOfItemsInOrder
     */
    private int getNumberOfItemsInOrder(final OrderEntryGroup orderEntryGroup) {
        int numberOfItemsInOrder = 0;
        for (final AbstractOrderEntryModel orderEntry : orderEntryGroup) {
            if ((orderEntry != null) && (orderEntry.getQuantity() != null)) {
                numberOfItemsInOrder += orderEntry.getQuantity().intValue();
            }
        }
        return numberOfItemsInOrder;
    }

    /**
     * Method to check if the number of items in order are less than or equal to maximum number of items allowed
     * 
     * @param numberOfItemsInOrder
     * @param maximumAllowedQuantity
     * @return boolean
     */
    private boolean isItemCountLessThanMaximumAllowed(final int numberOfItemsInOrder,
            final int maximumAllowedQuantity) {
        return numberOfItemsInOrder <= maximumAllowedQuantity;
    }
}

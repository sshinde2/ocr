/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;


/**
 * @author smishra1
 *
 */
public class GlobalMaxRoutingAttemptsDenialStrategy extends BaseGlobalFulfilmentDenialStrategy {

    private TargetStoreConsignmentService targetStoreConsignmentService;

    /**
     * Checks whether already reached number of maximum routing attempts.
     * 
     * @param orderEntryGroup
     * @return DenialResponse
     */
    @Override
    public DenialResponse isDenied(final OrderEntryGroup orderEntryGroup) {
        AbstractOrderModel order = null;
        if (CollectionUtils.isNotEmpty(orderEntryGroup)) {
            order = orderEntryGroup.get(0).getOrder();
        }
        if (isNumberOfRoutingAttemptsExceeded(order)) {
            return DenialResponse.createDenied("MaxRoutingAttemptsExceeded");
        }
        return DenialResponse.createNotDenied();
    }

    /**
     * Method to check if maximum number of re-routing attempts has exceeded the allowed attempts
     * 
     * @param order
     * @return boolean
     */
    private boolean isNumberOfRoutingAttemptsExceeded(final AbstractOrderModel order) {
        final Set<ConsignmentModel> consignments = order.getConsignments();
        final GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel = getTargetGlobalStoreFulfilmentService()
                .getGlobalStoreFulfilmentCapabilites();
        int routingCount = 0;
        if (null != capabilitiesModel && null != capabilitiesModel.getNumberOfRoutingAttempts()) {
            final int maxRoutingAttempts = capabilitiesModel.getNumberOfRoutingAttempts().intValue();

            if (maxRoutingAttempts <= 0 && CollectionUtils.isEmpty(consignments)) {
                return true;
            }

            for (final ConsignmentModel consignment : consignments) {
                if (consignment.getStatus().equals(ConsignmentStatus.CANCELLED)
                        && targetStoreConsignmentService.isConsignmentAssignedToAnyStore(consignment)) {
                    ++routingCount;
                }
            }
            return routingCount >= maxRoutingAttempts;
        }
        return true;
    }

    /**
     * @param targetStoreConsignmentService
     *            the new target store consignment service
     */
    @Required
    public void setTargetStoreConsignmentService(final TargetStoreConsignmentService targetStoreConsignmentService) {
        this.targetStoreConsignmentService = targetStoreConsignmentService;
    }

}

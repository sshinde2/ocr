/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * @author bhuang1
 *
 */
public class GlobalMaxRoutingPeriodDenialStrategy extends BaseGlobalFulfilmentDenialStrategy {


    /**
     * Checks whether the time science order creation already reached maximum routing period.
     * 
     * 
     * @param orderEntryGroup
     * @return DenialResponse
     */
    @Override
    public DenialResponse isDenied(final OrderEntryGroup orderEntryGroup) {
        final OrderModel orderModel = getOegParameterHelper().getOrderFromGivenGroup(orderEntryGroup);
        if (isMaxRoutingPeriodExceeded(orderModel)) {
            return DenialResponse.createDenied("MaxRoutingPeriodExceeded");
        }
        return DenialResponse.createNotDenied();
    }

    private boolean isMaxRoutingPeriodExceeded(final OrderModel order) {
        boolean exceeded = false;
        final GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel = getTargetGlobalStoreFulfilmentService()
                .getGlobalStoreFulfilmentCapabilites();
        final Date orderCreationDate = order.getCreationtime();
        if (capabilitiesModel != null && orderCreationDate != null) {
            final Integer maxRoutingPeriod = capabilitiesModel.getMaxRoutingPeriod();
            if (maxRoutingPeriod != null) {
                final int maxRoutingPeriodMinutes = maxRoutingPeriod.intValue();
                final long currentDate = getCurrentDate().getTime();
                final long orderCreationTime = orderCreationDate.getTime();
                final long diff = TimeUnit.MILLISECONDS.toMinutes(currentDate - orderCreationTime);
                if (diff > maxRoutingPeriodMinutes) {
                    exceeded = true;
                }
            }
        }
        return exceeded;
    }

    private Date getCurrentDate() {
        return new Date();
    }

}

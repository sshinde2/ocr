/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.model.ProductExclusionsModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.FilterExclusionListService;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;


/**
 * @author smishra1
 *
 */
public class GlobalProductExclusionDenialStrategy extends BaseGlobalFulfilmentDenialStrategy {
    private FilterExclusionListService filterExclusionListService;
    private TargetStoreFulfilmentCapabilitiesService storeFulfilmentCapabilitiesService;

    @Override
    public DenialResponse isDenied(final OrderEntryGroup orderEntryGroup) {
        Assert.notNull(orderEntryGroup, "orderEntryGroup cannot be null");
        final GlobalStoreFulfilmentCapabilitiesModel globalFulfillmentCapabilitiesModel = getTargetGlobalStoreFulfilmentService()
                .getGlobalStoreFulfilmentCapabilites();
        if (CollectionUtils.isEmpty(globalFulfillmentCapabilitiesModel.getProductExclusions())) {
            return DenialResponse.createNotDenied();
        }
        final Set<ProductExclusionsModel> productExclusionList = filterExclusionListService
                .filterActiveProductExclusions(globalFulfillmentCapabilitiesModel.getProductExclusions());
        if (storeFulfilmentCapabilitiesService.isProductInExclusionList(orderEntryGroup, productExclusionList)) {
            return DenialResponse.createDenied("GlobalProductExclusionDenial");
        }
        return DenialResponse.createNotDenied();
    }


    /**
     * @param filterExclusionListService
     *            the filterExclusionListService to set
     */
    @Required
    public void setFilterExclusionListService(final FilterExclusionListService filterExclusionListService) {
        this.filterExclusionListService = filterExclusionListService;
    }

    /**
     * @param storeFulfilmentCapabilitiesService
     *            the storeFulfilmentCapabilitiesService to set
     */
    @Required
    public void setStoreFulfilmentCapabilitiesService(
            final TargetStoreFulfilmentCapabilitiesService storeFulfilmentCapabilitiesService) {
        this.storeFulfilmentCapabilitiesService = storeFulfilmentCapabilitiesService;
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.logger.RoutingLogger;
import au.com.target.tgtfulfilment.ordersplitting.strategy.GlobalFulfilmentDenialStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.InstoreFulfilmentGlobalRulesChecker;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * @author sbryan6
 *
 */
public class InstoreFulfilmentGlobalRulesCheckerImpl implements InstoreFulfilmentGlobalRulesChecker {

    private static final RoutingLogger LOG = new RoutingLogger(TargetSplitWithRoutingStrategy.class);

    private List<GlobalFulfilmentDenialStrategy> globalStoreFulfilmentDenialStrategies;

    private List<GlobalFulfilmentDenialStrategy> globalDispatchFulfilmentDenialStrategies;

    private List<GlobalFulfilmentDenialStrategy> legacyGlobalStoreFulfilmentDenialStrategies;

    private OEGParameterHelper oegParameterHelper;

    private TargetFeatureSwitchService targetFeatureSwitchService;


    @Override
    public boolean areAllGlobalRulesPassed(final OrderEntryGroup oeg) {
        Assert.notNull(oeg, "oeg cannot be null");

        final String orderCode = oegParameterHelper.getOrderCode(oeg);

        for (final GlobalFulfilmentDenialStrategy globalDenialStrategy : getGlobalDenialStrategies()) {
            final DenialResponse denialResponse = globalDenialStrategy.isDenied(oeg);
            if (denialResponse.isDenied()) {
                LOG.logGlobalRulesExitMessage(orderCode, denialResponse.getReason());
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean areAllDispatchRulesPassed(final OrderEntryGroup oeg) {
        Assert.notNull(oeg, "oeg cannot be null");

        final String orderCode = oegParameterHelper.getOrderCode(oeg);

        for (final GlobalFulfilmentDenialStrategy globalDenialStrategy : globalDispatchFulfilmentDenialStrategies) {
            final DenialResponse denialResponse = globalDenialStrategy.isDenied(oeg);
            if (denialResponse.isDenied()) {
                LOG.logGlobalRulesExitMessage(orderCode, denialResponse.getReason());
                return false;
            }
        }
        return true;
    }

    private List<GlobalFulfilmentDenialStrategy> getGlobalDenialStrategies() {
        List<GlobalFulfilmentDenialStrategy> denialStrategies = new ArrayList<>();
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)) {
            denialStrategies = globalStoreFulfilmentDenialStrategies;
        }
        else {
            denialStrategies = legacyGlobalStoreFulfilmentDenialStrategies;
        }
        return denialStrategies;
    }

    /**
     * @param globalStoreFulfilmentDenialStrategies
     *            the globalStoreFulfilmentDenialStrategies to set
     */
    @Required
    public void setGlobalStoreFulfilmentDenialStrategies(
            final List<GlobalFulfilmentDenialStrategy> globalStoreFulfilmentDenialStrategies) {
        this.globalStoreFulfilmentDenialStrategies = globalStoreFulfilmentDenialStrategies;
    }

    /**
     * @param globalDispatchFulfilmentDenialStrategies
     *            the globalDispatchFulfilmentDenialStrategies to set
     */
    @Required
    public void setGlobalDispatchFulfilmentDenialStrategies(
            final List<GlobalFulfilmentDenialStrategy> globalDispatchFulfilmentDenialStrategies) {
        this.globalDispatchFulfilmentDenialStrategies = globalDispatchFulfilmentDenialStrategies;
    }

    /**
     * @param oegParameterHelper
     *            the oegParameterHelper to set
     */
    @Required
    public void setOegParameterHelper(final OEGParameterHelper oegParameterHelper) {
        this.oegParameterHelper = oegParameterHelper;
    }

    /**
     * @param legacyGlobalStoreFulfilmentDenialStrategies
     *            the legacyGlobalStoreFulfilmentDenialStrategies to set
     */
    @Required
    public void setLegacyGlobalStoreFulfilmentDenialStrategies(
            final List<GlobalFulfilmentDenialStrategy> legacyGlobalStoreFulfilmentDenialStrategies) {
        this.legacyGlobalStoreFulfilmentDenialStrategies = legacyGlobalStoreFulfilmentDenialStrategies;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

}

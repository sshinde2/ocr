/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgtcore.postcode.service.impl.TargetPostCodeServiceImpl;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * @author rsamuel3
 *
 */
public class InvalidDeliveryAddressDenialStrategy extends BaseGlobalFulfilmentDenialStrategy {

    private TargetPostCodeServiceImpl targetPostCodeService;

    @Override
    public DenialResponse isDenied(final OrderEntryGroup orderEntryGroup) {
        Assert.notNull(orderEntryGroup, "OrderEntryGroup  cannot be null");
        final AddressModel deliveryAddress = getOegParameterHelper().getDeliveryAddress(orderEntryGroup);
        return validateDeliveryAddress(deliveryAddress);
    }

    /**
     * validate that the delivery address has a state and is of the type that is expected and has a valid postcode
     * 
     * @param deliveryAddress
     * @return DenialResponse
     */
    protected DenialResponse validateDeliveryAddress(final AddressModel deliveryAddress) {

        //validate the state
        if (null == deliveryAddress || deliveryAddress.getDistrict() == null) {
            return DenialResponse.createDenied("InvalidOrderDeliveryAddress");
        }

        //check if valid postcode in system

        final String postcode = deliveryAddress.getPostalcode();

        if (StringUtils.isBlank(postcode)) {
            return DenialResponse.createDenied("EmptyPostcodeInDeliveryAddress postcode=" + postcode);
        }

        final PostCodeModel postcodeModel = targetPostCodeService.getPostCode(postcode.trim());

        return validatePostcode(postcode, postcodeModel);
    }

    /**
     * validate if the postcode model is null for a given postcode
     * 
     * @param postcode
     * @param postcodeModel
     * @return DenialResponse
     */
    protected DenialResponse validatePostcode(final String postcode,
            final PostCodeModel postcodeModel) {
        if (postcodeModel == null) {
            return DenialResponse.createDenied("UnknownPostcodeInDeliveryAddress postcode=" + postcode);
        }

        return DenialResponse.createNotDenied();
    }

    /**
     * @param targetPostCodeService
     *            the targetPostCodeService to set
     */
    @Required
    public void setTargetPostCodeService(final TargetPostCodeServiceImpl targetPostCodeService) {
        this.targetPostCodeService = targetPostCodeService;
    }

}

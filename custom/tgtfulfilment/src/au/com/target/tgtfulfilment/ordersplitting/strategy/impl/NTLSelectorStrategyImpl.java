/**
 * 
 */

package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.logger.RoutingLogger;
import au.com.target.tgtfulfilment.ordersplitting.strategy.WarehouseSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.SplitOegResult;
import au.com.target.tgtfulfilment.service.TargetFulfilmentPointOfServiceService;


/**
 * @author Nandini
 *
 */

public class NTLSelectorStrategyImpl implements WarehouseSelectorStrategy {

    private static final RoutingLogger ROUTING_LOG = new RoutingLogger(NTLSelectorStrategyImpl.class);

    private TargetFulfilmentPointOfServiceService targetFulfilmentPOSService;
    private TargetWarehouseService targetWarehouseService;
    private TargetStockService targetStockService;
    private OEGParameterHelper oegParameterHelper;


    @Override
    public SplitOegResult split(final OrderEntryGroup oeg) {
        Assert.notEmpty(oeg, "OEG cannot be empty");

        final String orderCode = oegParameterHelper.getOrderCode(oeg);
        final AddressModel deliveryAddress = oegParameterHelper.getDeliveryAddress(oeg);
        final Map<String, Long> requiredQty = oegParameterHelper.getRequiredQty(oeg);
        if (MapUtils.isEmpty(requiredQty)) {
            ROUTING_LOG.logNTLRulesExitMessage(orderCode, "NoRequiredQtyOnOeg");
            return createSingleUnassignedOeg(oeg);
        }

        if (null == deliveryAddress) {
            ROUTING_LOG.logNTLRulesExitMessage(orderCode, "NoDeliveryAddressInOrder");
            return createSingleUnassignedOeg(oeg);
        }

        final TargetPointOfServiceModel tpos = targetFulfilmentPOSService.getNTLByAddress(deliveryAddress);

        if (null == tpos) {
            ROUTING_LOG.logNTLRulesExitMessage(orderCode, "NoEnabledNTLInState");
            return createSingleUnassignedOeg(oeg);
        }

        final WarehouseModel warehouse = targetWarehouseService.getWarehouseForPointOfService(tpos);

        if (null == warehouse) {
            ROUTING_LOG.logNTLRulesExitMessage(orderCode, "NoWarehouseFoundForSelectedNTL");
            return createSingleUnassignedOeg(oeg);
        }

        final OrderEntryGroup ntlOEG = new OrderEntryGroup();
        final OrderEntryGroup nonNtlOEG = new OrderEntryGroup();
        final SplitOegResult result = new SplitOegResult();
        final ArrayList<OrderEntryGroup> assignedOEGsToWarehouse = new ArrayList<>();

        final Map<String, Long> nonNtlRequiredQty = new HashMap<>();
        final Map<String, Long> assignedQty = new HashMap<>();

        // try to split consignment for partial NTL
        for (final AbstractOrderEntryModel entry : oeg) {
            final String productCode = entry.getProduct().getCode();
            final Long qty = requiredQty.get(productCode);
            if (targetStockService.isOrderEntryInStockAtWarehouse(entry, warehouse, qty)) {
                ntlOEG.add(entry);
                assignedQty.put(productCode, qty);
            }
            else {
                nonNtlOEG.add(entry);
                nonNtlRequiredQty.put(productCode, qty);
            }
        }
        assignedOEGsToWarehouse.add(ntlOEG);

        if (CollectionUtils.isNotEmpty(ntlOEG)) {
            oegParameterHelper.setAssignedQty(ntlOEG, assignedQty);
            if (CollectionUtils.isEmpty(nonNtlOEG)) {
                // whole order assigned to NTL
                ROUTING_LOG.logNTLAssignMessage(orderCode, tpos.getStoreNumber().intValue());
                oegParameterHelper.assignTposToOEG(ntlOEG, tpos);
                result.setAssignedOEGsToWarehouse(assignedOEGsToWarehouse);
            }
            else {
                // order splitted
                ROUTING_LOG.logSplitOrderMessage(orderCode);
                ROUTING_LOG.logNTLPartAssignMessage(orderCode, tpos.getStoreNumber().intValue());
                oegParameterHelper.assignTposToOEG(ntlOEG, tpos);
                result.setAssignedOEGsToWarehouse(assignedOEGsToWarehouse);
                result.setNotAssignedToWarehouse(nonNtlOEG);
            }
        }
        else {
            ROUTING_LOG.logNTLRulesExitMessage(orderCode, tpos.getStoreNumber().intValue(), "ProductsNotInStockAtNTL");
            result.setNotAssignedToWarehouse(nonNtlOEG);
        }
        if (CollectionUtils.isNotEmpty(nonNtlOEG)) {
            oegParameterHelper.setRequiredQty(nonNtlOEG, nonNtlRequiredQty);
        }
        final String deliveryModeCode = oegParameterHelper.getDeliveryMode(oeg) != null ? oegParameterHelper
                .getDeliveryMode(oeg).getCode() : StringUtils.EMPTY;
        final String cncStoreNumber = oegParameterHelper.getCncStoreNumber(oeg) != null ? oegParameterHelper
                .getCncStoreNumber(oeg).toString() : StringUtils.EMPTY;
        ROUTING_LOG.logSplitOegResult(orderCode, deliveryModeCode, cncStoreNumber, "NTLSelectorStrategy",
                result.getAssignedOEGsToWarehouse(),
                result.getNotAssignedToWarehouse());
        return result;
    }

    private SplitOegResult createSingleUnassignedOeg(final OrderEntryGroup oeg) {
        final SplitOegResult result = new SplitOegResult();
        result.setNotAssignedToWarehouse(oeg);
        return result;
    }

    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */

    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }

    /**
     * @param targetStockService
     *            the targetStockService to set
     */

    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }

    /**
     * @param oegParameterHelper
     *            the oegParameterHelper to set
     */

    @Required
    public void setOegParameterHelper(final OEGParameterHelper oegParameterHelper) {
        this.oegParameterHelper = oegParameterHelper;
    }

    /**
     * @param targetFulfilmentPOSService
     *            the targetFulfilmentPOSService to set
     */
    @Required
    public void setTargetFulfilmentPOSService(final TargetFulfilmentPointOfServiceService targetFulfilmentPOSService) {
        this.targetFulfilmentPOSService = targetFulfilmentPOSService;
    }

}

/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.logger.RoutingLogger;
import au.com.target.tgtfulfilment.ordersplitting.strategy.SameStoreCncSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.SpecificStoreFulfilmentDenialStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * @author sbryan6
 *
 */
public class SameStoreCncSelectorStrategyImpl implements SameStoreCncSelectorStrategy {

    private static final RoutingLogger LOG = new RoutingLogger(SameStoreCncSelectorStrategyImpl.class);

    private List<SpecificStoreFulfilmentDenialStrategy> specificStoreFulfilmentDenialStrategies;

    private OEGParameterHelper oegParameterHelper;

    private TargetPointOfServiceService targetPointOfServiceService;


    @Override
    public TargetPointOfServiceModel isValidForSameStoreCnc(final OrderEntryGroup oeg, final Integer cncStoreNumber) {

        final String orderCode = oegParameterHelper.getOrderCode(oeg);

        if (null == cncStoreNumber) {
            LOG.logSameStoreCncRulesSkipMessage(orderCode, "NotCncOrder");
            return null;
        }

        LOG.logSameStoreCncRulesEntryMessage(orderCode, cncStoreNumber.intValue());

        final TargetPointOfServiceModel tpos = lookupTargetPointOfService(cncStoreNumber);

        if (null == tpos) {
            LOG.logSameStoreCncRulesSkipMessage(orderCode, "TPOSNotFound");
            return null;
        }

        if (CollectionUtils.isNotEmpty(specificStoreFulfilmentDenialStrategies)) {
            for (final SpecificStoreFulfilmentDenialStrategy denialStrategy : specificStoreFulfilmentDenialStrategies) {
                final DenialResponse denialResponse = denialStrategy.isDenied(oeg, tpos);
                if (denialResponse.isDenied()) {
                    LOG.logSameStoreCncRulesExitMessage(orderCode, cncStoreNumber.intValue(),
                            denialResponse.getReason());
                    return null;
                }
            }
        }

        LOG.logStoreAssignMessage(orderCode, cncStoreNumber.intValue(), "SameStoreCnc");

        return tpos;
    }


    /**
     * Lookup target point of service.
     *
     * @param storeNumber
     *            the store number
     * @return the target point of service model
     */
    protected TargetPointOfServiceModel lookupTargetPointOfService(final Integer storeNumber) {
        try {
            return targetPointOfServiceService.getPOSByStoreNumber(storeNumber);
        }
        catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException ex) {
            return null;
        }
    }


    /**
     * @param specificStoreFulfilmentDenialStrategies
     *            the specificStoreFulfilmentDenialStrategies to set
     */
    @Required
    public void setSpecificStoreFulfilmentDenialStrategies(
            final List<SpecificStoreFulfilmentDenialStrategy> specificStoreFulfilmentDenialStrategies) {
        this.specificStoreFulfilmentDenialStrategies = specificStoreFulfilmentDenialStrategies;
    }

    /**
     * @param oegParameterHelper
     *            the oegParameterHelper to set
     */
    @Required
    public void setOegParameterHelper(final OEGParameterHelper oegParameterHelper) {
        this.oegParameterHelper = oegParameterHelper;
    }

    /**
     * @param targetPointOfServiceService
     *            the targetPointOfServiceService to set
     */
    @Required
    public void setTargetPointOfServiceService(final TargetPointOfServiceService targetPointOfServiceService) {
        this.targetPointOfServiceService = targetPointOfServiceService;
    }


}

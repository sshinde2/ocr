/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * Check if the store can deal with the order delivery mode
 * 
 * @author sbryan6
 *
 */
public class SpecificStoreDeliveryModeFulfilmentDenialStrategy extends BaseSpecificStoreFulfilmentDenialStrategy {

    private static final Logger LOG = Logger.getLogger(SpecificStoreDeliveryModeFulfilmentDenialStrategy.class);

    @Override
    public DenialResponse isDenied(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos) {
        Assert.notEmpty(oeg, "order entry group must not be null");
        Assert.notNull(tpos, "tpos must not be null");

        final String orderCode = getOegParameterHelper().getOrderCode(oeg);
        if (LOG.isDebugEnabled()) {
            LOG.debug(" START order=" + orderCode + " store=" + tpos.getStoreNumber());
        }

        final Integer cncStoreNumber = getOegParameterHelper().getCncStoreNumber(oeg);
        final TargetZoneDeliveryModeModel targetZoneDeliveryMode = getOegParameterHelper().getDeliveryMode(oeg);

        if (tpos.getStoreNumber() == null) {

            return DenialResponse.createDenied("StoreNumberNull");
        }

        final int storeNumber = tpos.getStoreNumber().intValue();

        if (targetZoneDeliveryMode == null) {

            return DenialResponse.createDenied("DeliveryModeNullInOrder");
        }

        if (!(getStoreFulfilmentCapabilitiesService().isDeliveryModeAssociatedToStore(tpos, targetZoneDeliveryMode))) {

            return DenialResponse.createDenied("DeliveryModeNotAccepted: "
                    + targetZoneDeliveryMode.getCode());
        }

        final StoreFulfilmentCapabilitiesModel storeFulfilmentCapabilitiesModel = tpos.getFulfilmentCapability();
        if (storeFulfilmentCapabilitiesModel == null) {
            return DenialResponse.createDenied("StoreNotCapableToFulfillOrder");
        }

        final Boolean isDeliveryToStore = targetZoneDeliveryMode.getIsDeliveryToStore();
        if (BooleanUtils.isNotTrue(isDeliveryToStore)) {
            return DenialResponse.createNotDenied();
        }

        if (cncStoreNumber == null) {
            return DenialResponse.createDenied("OrderCncStoreNumberMissing");
        }

        if (cncStoreNumber.intValue() == storeNumber) {
            //same store delivery
            final Boolean allowDeliveryToThisStore = storeFulfilmentCapabilitiesModel.getAllowDeliveryToThisStore();
            if (BooleanUtils.isNotTrue(allowDeliveryToThisStore)) {
                return DenialResponse.createDenied("NotAllowedToDeliverToThisStore");
            }
        }
        else {
            // inter store delivery
            final Boolean allowDeliveryToAnotherStore = storeFulfilmentCapabilitiesModel
                    .getAllowDeliveryToAnotherStore();
            if (BooleanUtils.isNotTrue(allowDeliveryToAnotherStore)) {
                return DenialResponse.createDenied("NotAllowedToDeliverToAnotherStore: "
                        + cncStoreNumber.intValue());
            }
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug(" END order=" + orderCode + " store=" + tpos.getStoreNumber());
        }

        return DenialResponse.createNotDenied();
    }

}

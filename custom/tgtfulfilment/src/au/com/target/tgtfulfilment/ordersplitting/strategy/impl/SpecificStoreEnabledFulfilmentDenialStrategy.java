/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * Check if store is enabled for fulfilment
 * 
 * @author sbryan6
 *
 */
public class SpecificStoreEnabledFulfilmentDenialStrategy extends BaseSpecificStoreFulfilmentDenialStrategy {

    private static final Logger LOG = Logger.getLogger(SpecificStoreEnabledFulfilmentDenialStrategy.class);

    @Override
    public DenialResponse isDenied(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos) {

        Assert.notEmpty(oeg, "order entry group must not be null");
        Assert.notNull(tpos, "tpos must not be null");

        final String orderCode = getOegParameterHelper().getOrderCode(oeg);

        if (LOG.isDebugEnabled()) {
            LOG.debug(" START order=" + orderCode + " store=" + tpos.getStoreNumber());
        }

        if (getStoreFulfilmentCapabilitiesService().isStoreEnabledForFulfilment(tpos)) {
            return DenialResponse.createNotDenied();
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug(" END order=" + orderCode + " store=" + tpos.getStoreNumber());
        }

        return DenialResponse.createDenied("StoreNotCapableToFulfillOrder");
    }

}

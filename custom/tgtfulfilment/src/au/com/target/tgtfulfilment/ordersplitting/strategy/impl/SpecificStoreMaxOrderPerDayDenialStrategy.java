/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * @author smishra1
 *
 */
public class SpecificStoreMaxOrderPerDayDenialStrategy extends BaseSpecificStoreFulfilmentDenialStrategy {

    private static final Logger LOG = Logger.getLogger(SpecificStoreMaxOrderPerDayDenialStrategy.class);

    @Override
    public DenialResponse isDenied(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos) {
        Assert.notNull(oeg, "oeg must not be null");

        Assert.notNull(tpos, "tpos must not be null");

        final String orderCode = getOegParameterHelper().getOrderCode(oeg);

        if (LOG.isDebugEnabled()) {
            LOG.debug(" START order=" + orderCode + " store=" + tpos.getStoreNumber());
        }


        if (getStoreFulfilmentCapabilitiesService().isMaxNumberOfOrdersExceeded(tpos)) {
            return DenialResponse.createDenied("MaxNumberOfOrdersForTheDayExceededForTheStore");
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug(" END order=" + orderCode + " store=" + tpos.getStoreNumber());
        }


        return DenialResponse.createNotDenied();
    }

}

/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtsale.stock.service.TargetStoreStockService;


/**
 * @author sbryan6
 *
 */
public class SpecificStoreStockFulfilmentDenialStrategy extends BaseSpecificStoreFulfilmentDenialStrategy {

    private TargetStoreStockService targetStoreStockService;

    private static final Logger LOG = Logger.getLogger(SpecificStoreStockFulfilmentDenialStrategy.class);

    @Override
    public DenialResponse isDenied(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos) {
        Assert.notNull(oeg, "oeg must not be null");
        Assert.notNull(tpos, "POS object must not be null");

        final String orderCode = getOegParameterHelper().getOrderCode(oeg);

        if (LOG.isDebugEnabled()) {
            LOG.debug(" START order=" + orderCode + " store=" + tpos.getStoreNumber());
        }


        if (tpos == null
                || !targetStoreStockService.isOrderEntriesInStockAtStore(oeg, tpos.getStoreNumber(),
                        getOegParameterHelper().getOrderCode(oeg))) {

            return DenialResponse.createDenied("OrderNotInStockAtStore");
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug(" END order=" + orderCode + " store=" + tpos.getStoreNumber());
        }

        return DenialResponse.createNotDenied();
    }

    @Required
    public void setTargetStoreStockService(final TargetStoreStockService targetStoreStockService) {
        this.targetStoreStockService = targetStoreStockService;
    }

}

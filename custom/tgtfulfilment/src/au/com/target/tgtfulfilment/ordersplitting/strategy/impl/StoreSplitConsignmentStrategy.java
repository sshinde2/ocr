/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;
import de.hybris.platform.storelocator.exception.GeoServiceWrapperException;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Table;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetStoreStockCheckException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.constants.TgtfulfilmentConstants;
import au.com.target.tgtfulfilment.logger.RoutingLogger;
import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.InstoreFulfilmentGlobalRulesChecker;
import au.com.target.tgtfulfilment.ordersplitting.strategy.NearestStoreSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.SpecificStoreFulfilmentDenialStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.SplitOegResult;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;
import au.com.target.tgtfulfilment.service.TargetStoreLocatorService;


/**
 * 
 * @author bhuang3
 *
 */
public class StoreSplitConsignmentStrategy extends SameStoreCncSplitConsignmentStrategy {

    private static final RoutingLogger ROUTING_LOG = new RoutingLogger(StoreSplitConsignmentStrategy.class);

    private List<SpecificStoreFulfilmentDenialStrategy> storeFulfilmentDenialStrategies;

    private List<SpecificStoreFulfilmentDenialStrategy> storeWithEntryLevelFulfilmentDenialStrategies;

    private InstoreFulfilmentGlobalRulesChecker instoreFulfilmentGlobalRulesChecker;

    private TargetStoreLocatorService targetStoreLocatorService;

    private NearestStoreSelectorStrategy nearestStoreSelectorStrategy;

    private TargetSharedConfigService targetSharedConfigService;

    private TargetConsignmentService targetConsignmentService;

    private TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private FastlineSplitConsignmentStrategy fastlineSplitConsignmentStrategy;


    @Override
    public SplitOegResult split(final OrderEntryGroup oeg) {
        Assert.notEmpty(oeg, "OEG cannot be empty");
        final AddressModel deliveryAddress = getOegParameterHelper().getDeliveryAddress(oeg);
        Assert.notNull(deliveryAddress, "Delivery Address cannot be null");
        final String orderCode = getOegParameterHelper().getOrderCode(oeg);
        final OrderModel orderModel = getOegParameterHelper().getOrderFromGivenGroup(oeg);
        ROUTING_LOG.logSplitByStoreEntryMessage(orderCode);

        final SplitOegResult result = new SplitOegResult();
        final List<OrderEntryGroup> assignedOEGs = new ArrayList<>();
        OrderEntryGroup notAssignedToStore = oeg;
        result.setNotAssignedToWarehouse(notAssignedToStore);
        result.setAssignedOEGsToWarehouse(assignedOEGs);
        //check the gloabal rules
        if (!instoreFulfilmentGlobalRulesChecker.areAllDispatchRulesPassed(oeg)) {
            return result;
        }

        final List<PointOfServiceDistanceData> distanceDataList = new ArrayList<>();
        final List<TargetPointOfServiceModel> validTposList = new ArrayList<>();

        this.generateDistanceAndTposList(deliveryAddress, orderCode, oeg, distanceDataList, validTposList);

        if (CollectionUtils.isEmpty(validTposList)) {
            ROUTING_LOG.logGeneralActionMessage(orderCode, "Exit StoreSplitConsignmentStrategy",
                    "no valid tpos for routing");
            return result;
        }
        //need the List<OrderEntryGroup> assignedOEGs and OrderEntryGroup notAssignedToStore
        notAssignedToStore = assignedToStores(orderModel, validTposList, distanceDataList, assignedOEGs,
                notAssignedToStore);
        result.setNotAssignedToWarehouse(notAssignedToStore);
        result.setAssignedOEGsToWarehouse(assignedOEGs);
        getOegParameterHelper().populateParamsFromOrder(notAssignedToStore);
        final String deliveryModeCode = getOegParameterHelper().getDeliveryMode(oeg) != null ? getOegParameterHelper()
                .getDeliveryMode(oeg).getCode() : StringUtils.EMPTY;
        final String cncStoreNumber = getOegParameterHelper().getCncStoreNumber(oeg) != null ? getOegParameterHelper()
                .getCncStoreNumber(oeg).toString() : StringUtils.EMPTY;
        ROUTING_LOG.logSplitOegResult(orderCode, deliveryModeCode, cncStoreNumber, "StoreSplitConsignmentStrategy",
                result.getAssignedOEGsToWarehouse(), result.getNotAssignedToWarehouse());
        return result;
    }

    private void generateDistanceAndTposList(final AddressModel deliveryAddress, final String orderCode,
            final OrderEntryGroup oeg, final List<PointOfServiceDistanceData> distanceDataList,
            final List<TargetPointOfServiceModel> validTposList) {
        try {
            final List<PointOfServiceDistanceData> storeDistanceDataList = this.getStoreDistanceData(deliveryAddress);
            final int storeCount = storeDistanceDataList != null ? storeDistanceDataList.size() : 0;
            ROUTING_LOG.logGeneralActionMessage(orderCode, "StoreSplitConsignmentStrategy",
                    "Found number of stores, StoreCount=" + storeCount);
            if (CollectionUtils.isNotEmpty(storeDistanceDataList)) {
                distanceDataList.addAll(storeDistanceDataList);
            }
        }
        catch (final GeoServiceWrapperException e) {
            ROUTING_LOG.logDeliveryAddressRulesExitMessage(orderCode,
                    "CantGeocodeOrderDeliveryAddress, error=" + e.getLocalizedMessage());
        }


        final List<TargetPointOfServiceModel> tposList = getValidTopsList(distanceDataList, oeg, orderCode);
        if (CollectionUtils.isNotEmpty(tposList)) {
            validTposList.addAll(tposList);
        }

        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASTLINE_IN_STORE_SPLITTING)) {
            try {
                final List<PointOfServiceDistanceData> defaultWarehouseDistanceDataList = this
                        .getDefaultWarehouseDistanceData(deliveryAddress);
                if (CollectionUtils.isNotEmpty(defaultWarehouseDistanceDataList)) {
                    distanceDataList.addAll(defaultWarehouseDistanceDataList);
                }
            }
            catch (final GeoServiceWrapperException e) {
                ROUTING_LOG.logDeliveryAddressRulesExitMessage(orderCode,
                        "CantGeocodeOrderDeliveryAddress, error=" + e.getLocalizedMessage());
            }

            final TargetPointOfServiceModel tposOfDefaultWarehouse = getTargetPointOfServiceService()
                    .getTposOfDefaultWarehouse();
            if (tposOfDefaultWarehouse != null) {
                validTposList.add(tposOfDefaultWarehouse);
            }
        }
        final int validTposCount = validTposList != null ? validTposList.size() : 0;
        ROUTING_LOG.logGeneralActionMessage(orderCode, "StoreSplitConsignmentStrategy",
                "Found number of valid stores after running denial strategies, StoreCount=" + validTposCount);
    }

    /**
     * filter the list by denial strategies and convert the List<PointOfServiceDistanceData> to List
     * <TargetPointOfServiceModel>
     * 
     * @param distanceDataList
     * @param oeg
     * @param orderCode
     * @return List<TargetPointOfServiceModel>
     */
    private List<TargetPointOfServiceModel> getValidTopsList(final List<PointOfServiceDistanceData> distanceDataList,
            final OrderEntryGroup oeg, final String orderCode) {
        List<TargetPointOfServiceModel> validTposList = new ArrayList<>();
        if (CollectionUtils.isEmpty(distanceDataList)) {
            ROUTING_LOG.logGeneralActionMessage(orderCode, "StoreSplitConsignmentStrategy",
                    "No store available in range for routing");
            return validTposList;
        }

        final List<TargetPointOfServiceModel> targetPointOfServiceList = generateTposListByDistanceDataList(
                distanceDataList);

        validTposList = filterPointOfServiceListByDenialStrategies(
                targetPointOfServiceList, oeg);

        if (CollectionUtils.isEmpty(validTposList)) {
            ROUTING_LOG.logGeneralActionMessage(orderCode, "StoreSplitConsignmentStrategy",
                    "No store pass store denial strategies for routing");
        }
        return validTposList;
    }

    private List<TargetPointOfServiceModel> generateTposListByDistanceDataList(
            final List<PointOfServiceDistanceData> distanceDataList) {
        final List<TargetPointOfServiceModel> tposList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(distanceDataList)) {
            for (final PointOfServiceDistanceData data : distanceDataList) {
                if (data.getPointOfService() instanceof TargetPointOfServiceModel) {
                    tposList.add((TargetPointOfServiceModel)data.getPointOfService());
                }
            }
        }
        return tposList;
    }

    private List<PointOfServiceDistanceData> getStoreDistanceData(final AddressModel deliveryAddress) {
        // Get the list of tpos
        List<TargetPointOfServiceModel> targetPointOfServiceList;
        if (targetFeatureSwitchService
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SELECT_STORE_WITH_DISTANCE_RESTRICTION)) {
            targetPointOfServiceList = targetStoreLocatorService
                    .getAllFulfilmentStores();
            return nearestStoreSelectorStrategy
                    .getSortedStoreListByDistanceWithDistanceRestriction(targetPointOfServiceList, deliveryAddress,
                            getStoreDistanceLimit());

        }
        else {
            targetPointOfServiceList = targetStoreLocatorService
                    .getAllFulfilmentStoresInState(deliveryAddress.getDistrict().trim());
            // set the big distance to ignore the distance restriction
            return nearestStoreSelectorStrategy
                    .getSortedStoreListByDistanceWithDistanceRestriction(targetPointOfServiceList, deliveryAddress,
                            10000d);

        }


    }

    private List<PointOfServiceDistanceData> getDefaultWarehouseDistanceData(final AddressModel deliveryAddress) {
        List<PointOfServiceDistanceData> pointOfServiceDistanceDataList = new ArrayList<>();
        final TargetPointOfServiceModel tpos = getTargetPointOfServiceService().getTposOfDefaultWarehouse();
        if (tpos != null) {
            final List<TargetPointOfServiceModel> targetPointOfServiceList = ImmutableList.of(tpos);
            pointOfServiceDistanceDataList = nearestStoreSelectorStrategy
                    .getSortedStoreListByDistanceWithDistanceRestriction(targetPointOfServiceList, deliveryAddress,
                            10000d);
        }
        return pointOfServiceDistanceDataList;
    }

    private List<TargetPointOfServiceModel> filterPointOfServiceListByDenialStrategies(
            final List<TargetPointOfServiceModel> targetPointOfServiceList, final OrderEntryGroup oeg) {
        final List<TargetPointOfServiceModel> filteredList = new ArrayList<>();
        for (final TargetPointOfServiceModel tpos : targetPointOfServiceList) {
            if (storePassesDenialStrategies(oeg, tpos)) {
                filteredList.add(tpos);
            }
        }
        return filteredList;
    }



    private boolean storePassesDenialStrategies(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos) {
        if (CollectionUtils.isNotEmpty(storeFulfilmentDenialStrategies)) {
            final String orderCode = getOegParameterHelper().getOrderCode(oeg);
            for (final SpecificStoreFulfilmentDenialStrategy denialStrategy : storeFulfilmentDenialStrategies) {
                final DenialResponse denialResponse = denialStrategy.isDenied(oeg, tpos);
                if (denialResponse.isDenied()) {
                    ROUTING_LOG.logStoreRulesExcludedMessage("StoreExcluded", orderCode, denialResponse.getReason(),
                            getStoreNumberForLogging(tpos), getStoreStateForLogging(tpos));
                    return false;
                }
            }
        }
        return true;
    }

    private void errorOnStockCheckFail(final String orderCode, final String message, final Exception ex) {
        if (targetFeatureSwitchService
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.ERROR_ON_STOCKCHECK_FAILURE)) {
            final TargetStoreStockCheckException stockException = new TargetStoreStockCheckException(
                    message + " orderCode=" + orderCode, ex);
            ROUTING_LOG.logUnexpectedExceptionMessage(orderCode, message, stockException);
            throw stockException;
        }
        ROUTING_LOG.logGeneralActionMessage(orderCode, "SkipStockCheckException",
                TgtCoreConstants.FeatureSwitch.ERROR_ON_STOCKCHECK_FAILURE + "-switch_is_off");
    }


    private Table<Integer, String, Long> doBatchedStoreCheck(final String orderCode,
            final List<TargetPointOfServiceModel> targetPointOfServiceList, final OrderEntryGroup oeg) {

        Table<Integer, String, Long> sohStoreTable = null;
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPLIT_ORDER_OPTIMISATION)) {
            try {
                ROUTING_LOG.logSplitOptimisationEnable("Fetching all the SOH data related to the order starts",
                        orderCode);
                sohStoreTable = getTargetStoreStockService()
                        .fetchProductsSohInStore(targetPointOfServiceList, getProductModelsFromOrderEntries(oeg));
                ROUTING_LOG.logSplitOptimisationEnable("Fetching all the SOH data related to the order ends",
                        orderCode);
            }
            catch (final Exception ex) {
                errorOnStockCheckFail(orderCode, "Stock Check Failed reason=EXCEPTION", ex);
            }

            if (sohStoreTable == null || sohStoreTable.isEmpty()) {
                errorOnStockCheckFail(orderCode, "Stock Check Failed reason=NO_RESULTS_RETURNED", null);
            }

        }
        return sohStoreTable;
    }

    private OrderEntryGroup assignedToStores(final OrderModel orderModel,
            final List<TargetPointOfServiceModel> targetPointOfServiceList,
            final List<PointOfServiceDistanceData> distanceDataList, final List<OrderEntryGroup> assignedOEGs,
            final OrderEntryGroup oeg) {
        final String orderCode = orderModel != null ? orderModel.getCode() : StringUtils.EMPTY;
        int activeConsignmentCount = 0;
        final int maxConsignmentSplits = this.getMaxConsignmentSplits();
        if (maxConsignmentSplits > 0) {
            activeConsignmentCount = this.getActiveConsignmentCount(orderModel);
        }
        OrderEntryGroup notAssignedOeg = oeg;
        final Table<Integer, String, Long> sohStoreTable = doBatchedStoreCheck(orderCode, targetPointOfServiceList,
                oeg);

        while (CollectionUtils.isNotEmpty(notAssignedOeg)) {
            if (this.exceedMaxNumberOfStoreConsignmentSplits(activeConsignmentCount, maxConsignmentSplits,
                    assignedOEGs)) {
                //finish
                ROUTING_LOG
                        .logGeneralActionMessage(
                                orderModel.getCode(),
                                "Exit StoreSplitConsignmentStrategy",
                                MessageFormat
                                        .format(
                                                "Exceed the max number of store consignment splits, maxNumberOfStoreConsignmentSplits={0}",
                                                Integer.valueOf(maxConsignmentSplits)));
                return notAssignedOeg;
            }
            final List<SplitOegResult> results = createSplitOegResultList(targetPointOfServiceList, notAssignedOeg,
                    orderCode, sohStoreTable);

            filterOutZeroAssignedResult(results);
            if (CollectionUtils.isEmpty(results)) {
                //finish
                return notAssignedOeg;
            }
            else {
                sortByTotalAssignedQuantity(results);
                final List<SplitOegResult> maxQtyResults = createMaxQtyResults(results);
                final Map<TargetPointOfServiceModel, SplitOegResult> tposResultsMap = createTposListBySplitOegResultMap(
                        maxQtyResults);
                final List<TargetPointOfServiceModel> tposList = new ArrayList<>(tposResultsMap.keySet());

                // sort by distance
                final List<PointOfServiceDistanceData> generatedDistanceDataList = generateDistanceDataByTposList(
                        distanceDataList, tposList);
                nearestStoreSelectorStrategy.sortByDistance(generatedDistanceDataList);
                final TargetPointOfServiceModel assignedTpos = (TargetPointOfServiceModel)generatedDistanceDataList
                        .get(0)
                        .getPointOfService();
                final SplitOegResult assignedSplitOegResult = tposResultsMap.get(assignedTpos);
                assignedOEGs.addAll(assignedSplitOegResult.getAssignedOEGsToWarehouse());
                notAssignedOeg = assignedSplitOegResult.getNotAssignedToWarehouse();
                targetPointOfServiceList.remove(assignedTpos);
            }
        }
        return notAssignedOeg;

    }

    private boolean exceedMaxNumberOfStoreConsignmentSplits(final int activeConsignmentCount,
            final int maxConsignmentSplits,
            final List<OrderEntryGroup> assignedOEGs) {
        if (maxConsignmentSplits > 0) {
            int alreadyAssignedSize = activeConsignmentCount;
            if (assignedOEGs != null) {
                alreadyAssignedSize += assignedOEGs.size();
            }
            return alreadyAssignedSize >= maxConsignmentSplits;
        }

        return false;
    }

    private int getMaxConsignmentSplits() {
        int maxNumberOfStoreConsignmentSplits = 0;
        final GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel = targetGlobalStoreFulfilmentService
                .getGlobalStoreFulfilmentCapabilites();
        if (capabilitiesModel != null && capabilitiesModel.getMaxNumberOfStoreConsignmentSplits() != null) {
            maxNumberOfStoreConsignmentSplits = capabilitiesModel.getMaxNumberOfStoreConsignmentSplits().intValue();
        }
        return maxNumberOfStoreConsignmentSplits;
    }

    private int getActiveConsignmentCount(final OrderModel orderModel) {
        final List<TargetConsignmentModel> consignmentList = targetConsignmentService
                .getActivePPDStoreConsignments(orderModel);
        return consignmentList.size();
    }

    private List<PointOfServiceDistanceData> generateDistanceDataByTposList(
            final List<PointOfServiceDistanceData> distanceDataList,
            final List<TargetPointOfServiceModel> tposList) {
        final List<PointOfServiceDistanceData> distanceData = new ArrayList<>();
        for (final TargetPointOfServiceModel tpos : tposList) {
            final PointOfServiceDistanceData searchedData = (PointOfServiceDistanceData)CollectionUtils.find(
                    distanceDataList, new Predicate() {
                        @Override
                        public boolean evaluate(final Object obj) {
                            final PointOfServiceModel pos = ((PointOfServiceDistanceData)obj).getPointOfService();
                            return tpos == pos;
                        }
                    });
            if (searchedData != null) {
                distanceData.add(searchedData);
            }

        }
        return distanceData;
    }

    private Map<TargetPointOfServiceModel, SplitOegResult> createTposListBySplitOegResultMap(
            final List<SplitOegResult> results) {
        final Map<TargetPointOfServiceModel, SplitOegResult> tposMap = new HashMap<>();
        if (CollectionUtils.isNotEmpty(results)) {
            for (final SplitOegResult result : results) {
                final List<OrderEntryGroup> assignedOegs = result.getAssignedOEGsToWarehouse();
                if (CollectionUtils.isEmpty(assignedOegs)) {
                    continue;
                }
                final OrderEntryGroup oeg = assignedOegs.get(0);
                final TargetPointOfServiceModel tpos = getOegParameterHelper().getTpos(oeg);
                if (tpos == null) {
                    continue;
                }
                tposMap.put(tpos, result);
            }
        }
        return tposMap;
    }

    private List<SplitOegResult> createMaxQtyResults(final List<SplitOegResult> results) {
        final int maxQty = getTotalAssignedQuantityFromSplitOegResult(results.get(0));
        final List<SplitOegResult> filteredResult = new ArrayList<>();
        for (final SplitOegResult result : results) {
            if (getTotalAssignedQuantityFromSplitOegResult(result) == maxQty) {
                filteredResult.add(result);
            }
        }
        return filteredResult;
    }

    private void sortByTotalAssignedQuantity(final List<SplitOegResult> results) {
        Collections.sort(results, new Comparator<SplitOegResult>() {
            @Override
            public int compare(final SplitOegResult r1, final SplitOegResult r2) {
                return getTotalAssignedQuantityFromSplitOegResult(r2) - getTotalAssignedQuantityFromSplitOegResult(r1);
            }
        });
    }

    private int getTotalAssignedQuantityFromSplitOegResult(final SplitOegResult result) {
        if (result != null && CollectionUtils.isNotEmpty(result.getAssignedOEGsToWarehouse())
                && CollectionUtils.isNotEmpty(result.getAssignedOEGsToWarehouse().get(0))) {
            final OrderEntryGroup assignedOeg = result.getAssignedOEGsToWarehouse().get(0);
            final Map<String, Long> assignedQtyMap = getOegParameterHelper().getAssignedQty(assignedOeg);
            if (MapUtils.isEmpty(assignedQtyMap)) {
                return 0;
            }
            else {
                int totalQty = 0;
                for (final AbstractOrderEntryModel entry : assignedOeg) {
                    final Long qty = assignedQtyMap.get(entry.getProduct().getCode());
                    final long quantity = qty != null ? qty.longValue() : 0;
                    totalQty += quantity;
                }
                return totalQty;
            }

        }
        else {
            return 0;
        }
    }

    private void filterOutZeroAssignedResult(final List<SplitOegResult> results) {
        CollectionUtils.filter(results, new Predicate() {
            @Override
            public boolean evaluate(final Object result) {
                final List<OrderEntryGroup> assignedOegs = ((SplitOegResult)result).getAssignedOEGsToWarehouse();
                // only one OrderEntryGroup at this moment
                return CollectionUtils.isNotEmpty(assignedOegs) && CollectionUtils.isNotEmpty(assignedOegs.get(0));
            }

        });
    }



    private List<SplitOegResult> createSplitOegResultList(
            final List<TargetPointOfServiceModel> targetPointOfServiceList,
            final OrderEntryGroup oeg, final String orderCode, final Table<Integer, String, Long> sohStoreTable) {
        final List<SplitOegResult> results = new ArrayList<>();
        for (final TargetPointOfServiceModel tpos : targetPointOfServiceList) {
            SplitOegResult splitOegResult;
            if (isDefaultWarehouse(tpos)) {
                splitOegResult = fastlineSplitConsignmentStrategy.split(oeg);
            }
            else {
                splitOegResult = createSplitOegResult(oeg, tpos, orderCode, sohStoreTable);
            }
            results.add(splitOegResult);
        }
        return results;
    }

    /**
     * check if the top has the default warehouse (fastline)
     * 
     * @param tpos
     * @return true if tops has the default warehouse
     */
    private boolean isDefaultWarehouse(final TargetPointOfServiceModel tpos) {
        boolean isDefaultWarehouse = false;
        if (tpos != null && CollectionUtils.isNotEmpty(tpos.getWarehouses())) {
            for (final WarehouseModel warehouse : tpos.getWarehouses()) {
                if (BooleanUtils.isTrue(warehouse.getDefault())) {
                    isDefaultWarehouse = true;
                    break;
                }
            }
        }
        return isDefaultWarehouse;
    }

    private SplitOegResult createSplitOegResult(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos,
            final String orderCode, final Table<Integer, String, Long> sohStoreTable) {
        final SplitOegResult result = new SplitOegResult();
        final List<OrderEntryGroup> assignedOEGs = new ArrayList<>();
        OrderEntryGroup notAssignedToStore = new OrderEntryGroup();
        try {
            final OrderEntryGroup assignedToStore = new OrderEntryGroup();
            getOegParameterHelper().setAssignedQty(assignedToStore, new HashMap<String, Long>());

            getOegParameterHelper().setRequiredQty(notAssignedToStore, new HashMap<String, Long>());

            final OrderEntryGroup validOeg = this.splitOegByDenialStrategies(tpos, oeg,
                    notAssignedToStore);

            if (CollectionUtils.isNotEmpty(validOeg)) {
                if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPLIT_ORDER_OPTIMISATION)
                        && sohStoreTable != null && !sohStoreTable.isEmpty()) {
                    this.splitOegByStoreStock(tpos, validOeg, assignedToStore, notAssignedToStore, orderCode,
                            sohStoreTable);
                }
                else {
                    this.splitOegByStoreStock(tpos, validOeg, assignedToStore, notAssignedToStore, orderCode);
                }
            }
            if (CollectionUtils.isNotEmpty(assignedToStore)) {
                getOegParameterHelper().assignTposToOEG(assignedToStore, tpos);
                getOegParameterHelper().populateParamsFromOrder(assignedToStore);
                assignedOEGs.add(assignedToStore);
            }
            getOegParameterHelper().populateParamsFromOrder(notAssignedToStore);
        }
        catch (final Exception e) {
            ROUTING_LOG.logUnexpectedExceptionMessage(orderCode, "createStoreSplitOegResult", e);
            notAssignedToStore = oeg;
        }

        result.setAssignedOEGsToWarehouse(assignedOEGs);
        result.setNotAssignedToWarehouse(notAssignedToStore);
        return result;
    }

    private double getStoreDistanceLimit() {
        return targetSharedConfigService.getDouble(TgtfulfilmentConstants.Config.STORE_DISTANCE_LIMIT, 10000d);
    }

    @Override
    protected boolean checkByDenialStrategies(final OrderEntryGroup oeg, final TargetPointOfServiceModel tpos) {
        return checkBySpecificStoreFulfilmentDenialStrategy(oeg, tpos, storeWithEntryLevelFulfilmentDenialStrategies,
                "StoreExcluded");
    }

    /**
     * @param storeFulfilmentDenialStrategies
     *            the storeFulfilmentDenialStrategies to set
     */
    @Required
    public void setStoreFulfilmentDenialStrategies(
            final List<SpecificStoreFulfilmentDenialStrategy> storeFulfilmentDenialStrategies) {
        this.storeFulfilmentDenialStrategies = storeFulfilmentDenialStrategies;
    }


    @Required
    public void setInstoreFulfilmentGlobalRulesChecker(
            final InstoreFulfilmentGlobalRulesChecker instoreFulfilmentGlobalRulesChecker) {
        this.instoreFulfilmentGlobalRulesChecker = instoreFulfilmentGlobalRulesChecker;
    }

    @Required
    public void setTargetStoreLocatorService(final TargetStoreLocatorService targetStoreLocatorService) {
        this.targetStoreLocatorService = targetStoreLocatorService;
    }


    /**
     * @param storeWithEntryLevelFulfilmentDenialStrategies
     *            the storeWithEntryLevelFulfilmentDenialStrategies to set
     */
    @Required
    public void setStoreWithEntryLevelFulfilmentDenialStrategies(
            final List<SpecificStoreFulfilmentDenialStrategy> storeWithEntryLevelFulfilmentDenialStrategies) {
        this.storeWithEntryLevelFulfilmentDenialStrategies = storeWithEntryLevelFulfilmentDenialStrategies;
    }


    /**
     * @param nearestStoreSelectorStrategy
     *            the nearestStoreSelectorStrategy to set
     */
    @Required
    public void setNearestStoreSelectorStrategy(final NearestStoreSelectorStrategy nearestStoreSelectorStrategy) {
        this.nearestStoreSelectorStrategy = nearestStoreSelectorStrategy;
    }

    /**
     * @param targetSharedConfigService
     *            the targetSharedConfigService to set
     */
    @Required
    public void setTargetSharedConfigService(final TargetSharedConfigService targetSharedConfigService) {
        this.targetSharedConfigService = targetSharedConfigService;
    }

    /**
     * @return the storeFulfilmentDenialStrategies
     */
    public List<SpecificStoreFulfilmentDenialStrategy> getStoreFulfilmentDenialStrategies() {
        return storeFulfilmentDenialStrategies;
    }

    /**
     * @return the storeWithEntryLevelFulfilmentDenialStrategies
     */
    public List<SpecificStoreFulfilmentDenialStrategy> getStoreWithEntryLevelFulfilmentDenialStrategies() {
        return storeWithEntryLevelFulfilmentDenialStrategies;
    }

    /**
     * @param targetConsignmentService
     *            the targetConsignmentService to set
     */
    @Required
    public void setTargetConsignmentService(final TargetConsignmentService targetConsignmentService) {
        this.targetConsignmentService = targetConsignmentService;
    }

    @Required
    public void setTargetGlobalStoreFulfilmentService(
            final TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService) {
        this.targetGlobalStoreFulfilmentService = targetGlobalStoreFulfilmentService;
    }

    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    @Required
    public void setFastlineSplitConsignmentStrategy(
            final FastlineSplitConsignmentStrategy fastlineSplitConsignmentStrategy) {
        this.fastlineSplitConsignmentStrategy = fastlineSplitConsignmentStrategy;
    }


}

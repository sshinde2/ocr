/**
 * 
 */
package au.com.target.tgtfulfilment.salesapplication;

import de.hybris.platform.commerceservices.enums.SalesApplication;

import au.com.target.sale.salesapplication.TargetSalesSalesApplicationConfigService;


/**
 * @author ragarwa3
 *
 */
public interface TargetFulfilmentSalesApplicationConfigService extends TargetSalesSalesApplicationConfigService {

    /**
     * Checks if given sales application is configured to deny short picks.
     *
     * @param salesApp
     *            the sales app
     * @return true, if configured as denyShortPicks otherwise false.
     */
    boolean isDenyShortPicks(final SalesApplication salesApp);

    /**
     * Checks if given sales application is configured to suppress auto refund.
     * 
     * @param salesApp
     * @return true, if configured as suppressAutoRefund otherwise false.
     */
    boolean isSuppressAutoRefund(final SalesApplication salesApp);

}

/**
 * 
 */
package au.com.target.tgtfulfilment.service;

import de.hybris.platform.category.model.CategoryModel;

import java.util.List;
import java.util.Set;

import au.com.target.tgtfulfilment.model.CategoryExclusionsModel;
import au.com.target.tgtfulfilment.model.ProductExclusionsModel;


/**
 * @author smishra1
 *
 */
public interface FilterExclusionListService {

    /**
     * Method to return a Set of ProductExclusionsModel entries active on the current date
     * 
     * @param productExclusionList
     * 
     * */
    Set<ProductExclusionsModel> filterActiveProductExclusions(
            final Set<ProductExclusionsModel> productExclusionList);

    /**
     * Method to return a Set of excluded categories active on the current date
     * 
     * @param categoryExclusionList
     * @return list of excluded categories filtered based on date
     */
    List<CategoryModel> filterActiveCategoryModelsByDateRange(
            Set<CategoryExclusionsModel> categoryExclusionList);
}

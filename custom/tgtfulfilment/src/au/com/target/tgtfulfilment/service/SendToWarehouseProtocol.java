/**
 * 
 */
package au.com.target.tgtfulfilment.service;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.ConsignmentSentToWarehouseReason;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseResponse;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseResponse;


/**
 * This interface deals with sending consignments to a warehouse
 * 
 * @author mmaki
 * 
 */
public interface SendToWarehouseProtocol {

    /**
     * Extracts this consignment to the warehouse
     * 
     * @param consignment
     *            consignment to be extracted
     * @param reason
     *            (NEW, UDPATED, CANCELLED)
     * @return SendToWarehouseResponse
     */
    SendToWarehouseResponse sendConsignmentExtract(ConsignmentModel consignment,
            ConsignmentSentToWarehouseReason reason);


    /**
     * Extract and send the unshipped consignments to warehouse
     * 
     * @param consignments
     * @return {@link PollWarehouseResponse}
     */
    PollWarehouseResponse sendUnshippedConsignments(List<TargetConsignmentModel> consignments);
}
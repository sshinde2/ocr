/**
 * 
 */
package au.com.target.tgtfulfilment.service;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;


/**
 * Strategy for selecting the right service for sending to warehouse
 * 
 * @author sbryan6
 *
 */
public interface SendToWarehouseSelectorStrategy {

    /**
     * Select the service for sending to warehouse depending on consignment
     * 
     * @param consignment
     * @return SendToWarehouseProtocol
     */
    SendToWarehouseProtocol getSendToWarehouseProtocolForConsignment(ConsignmentModel consignment);

}

/**
 * 
 */
package au.com.target.tgtfulfilment.service;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.exceptions.ConsignmentStatusValidationException;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseResponse;


/**
 * Service for sending a consignment to the warehouse using an appropriate send method.
 * 
 * @author sbryan6
 *
 */
public interface SendToWarehouseService {

    /**
     * Send the consignments for a new order to the right warehouses.
     * 
     * @param order
     */
    void sendNewOrder(final OrderModel order) throws FulfilmentException, ConsignmentStatusValidationException;

    /**
     * Send cancel extracts to warehouse for cancelled or part cancelled order.
     * 
     * @param order
     */
    void sendOrderCancellation(OrderModel order);

    /**
     * Resend the given consignment to the right warehouse.
     * 
     * @param consignment
     */
    void sendConsignment(final ConsignmentModel consignment) throws FulfilmentException,
            ConsignmentStatusValidationException;

    /**
     * Send all the unhsipped consignments based on the warehouse to the warehouse
     * 
     * @param consignments
     */
    PollWarehouseResponse sendUnshippedConsignemnts(List<TargetConsignmentModel> consignments);

}

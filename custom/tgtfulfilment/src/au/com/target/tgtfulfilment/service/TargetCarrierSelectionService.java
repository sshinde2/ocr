/**
 * 
 */
package au.com.target.tgtfulfilment.service;

import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;


/**
 * @author Vivek
 *
 */
public interface TargetCarrierSelectionService {

    /**
     * Gets Preferred carrier based on Total Dead Weight.
     * 
     * @param consignment
     * @param bulky
     * @param enabledInStore
     * @return TargetCarrierModel
     */
    public TargetCarrierModel getPreferredCarrier(final TargetConsignmentModel consignment, final boolean bulky,
            final boolean enabledInStore);


    /**
     * Gets the Null carrier for the given delivery mode
     * 
     * @return null carrier
     */
    TargetCarrierModel getNullCarrier(TargetZoneDeliveryModeModel deliveryMode);

}
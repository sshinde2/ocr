/**
 * 
 */
package au.com.target.tgtfulfilment.service;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.ConsignmentService;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfluent.service.FluentConsignmentService;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;


/**
 * @author rsamuel3
 *
 */
public interface TargetConsignmentService extends ConsignmentService, FluentConsignmentService {

    /**
     * Get the first consignment that match one of the status in the list. The order of precedence is the order in which
     * the status is set in the List
     * 
     * @param status
     *            List of status in the order of precedence
     * @param order
     *            order from where the consignment needs to be found
     * @return ConsignmentModel that matches one of the status passed in or null if it could not find one
     */
    ConsignmentModel findConsignmentFromOrderWithStatus(AbstractOrderModel order, List<ConsignmentStatus> status);

    /**
     * Returns the active consignments, ie non-cancelled ones
     * 
     * @param order
     * @return list ConsignmentModel
     */
    List<TargetConsignmentModel> getActiveConsignmentsForOrder(AbstractOrderModel order);

    /**
     * Returns the active consignments which are for delivery to store (eg for cnc extract)
     * 
     * @param order
     * @return list ConsignmentModel
     */
    List<TargetConsignmentModel> getActiveDeliverToStoreConsignmentsForOrder(AbstractOrderModel order);

    /**
     * Find the consignment assigned to the default warehouse for an order.
     * 
     * @param orderCode
     * @return TargetConsignmentModel
     */
    TargetConsignmentModel findConsignmentForDefaultWarehouse(String orderCode);

    /**
     * Find the consignment based on consignmentId.
     * 
     * @param consignmentCode
     * @return TargetConsignmentModel
     * @throws NotFoundException
     */
    TargetConsignmentModel getConsignmentForCode(final String consignmentCode) throws NotFoundException;

    /**
     * Update the ready for pick up date on consignment
     * 
     * @param consignmentModel
     * @return true, if ready for pickup date is updated
     */
    boolean updateReadyForPickupDate(final TargetConsignmentModel consignmentModel);

    /**
     * Method to update picked up date in the consignment
     * 
     * @param consignment
     * @return true, if picked up date is updated
     */
    boolean updatePickedupDate(final TargetConsignmentModel consignment);

    /**
     * Find active consignment to be delivered to store for order.
     * 
     * @param order
     * @return TargetConsignmentModel, null if doesn't exist
     */
    TargetConsignmentModel getActiveDeliverToStoreConsignmentForOrder(final OrderModel order);

    /**
     * Method to update consignment returned to floor date
     * 
     * @param consignment
     */
    void updateReturnedToFloorDate(final TargetConsignmentModel consignment);

    /**
     * Get all consignments based on the consignment status and warehousecode
     * 
     * @param consignmentStatus
     * @param warehouseCode
     * @return List<TargetConsignmentModel>
     * @throws NotFoundException
     */
    List<TargetConsignmentModel> getAllConsignmentByStatusAndWarehouse(final ConsignmentStatus consignmentStatus,
            String warehouseCode) throws NotFoundException;

    /**
     * @param order
     * @return list target consignment model
     */
    List<TargetConsignmentModel> getActivePPDStoreConsignments(AbstractOrderModel order);

    /**
     * Check whether the consignment warehouse is online default warehouse.
     * 
     * @param consignment
     * @return true if consigment warehouse is same as online Warehouse
     */
    boolean isConsignmentToDefaultWarehouse(ConsignmentModel consignment);
}

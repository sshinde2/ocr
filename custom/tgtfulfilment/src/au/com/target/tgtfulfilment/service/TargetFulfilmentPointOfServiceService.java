/**
 * 
 */
package au.com.target.tgtfulfilment.service;

import de.hybris.platform.core.model.user.AddressModel;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;


/**
 * @author Nandini
 *
 */
public interface TargetFulfilmentPointOfServiceService extends TargetPointOfServiceService {

    /**
     * Return a single TargetPointOfServiceModel identified by region.
     * 
     * @param address
     *            the Address
     * @return TargetPointOfServiceModel
     */
    TargetPointOfServiceModel getNTLByAddress(final AddressModel address);

}

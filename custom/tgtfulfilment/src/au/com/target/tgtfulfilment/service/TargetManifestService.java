/**
 * 
 */
package au.com.target.tgtfulfilment.service;

import java.util.List;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;
import au.com.target.tgtfulfilment.model.TargetManifestModel;


/**
 * Provides service for creating Manifest
 * 
 * @author Vivek
 *
 */
public interface TargetManifestService {

    /**
     * Creates a Manifest with all the un-manifested picked consignments
     * 
     * @param pointOfService
     * @return TargetManifestModel
     */
    TargetManifestModel createManifest(TargetPointOfServiceModel pointOfService);

    /**
     * Gets the manifest by code.
     *
     * @param manifestCode
     *            the manifest code
     * @return the manifest by code
     * @throws NotFoundException
     */
    TargetManifestModel getManifestByCode(final String manifestCode) throws NotFoundException;

    /**
     * Gets the manifests history for a given store.
     *
     * @param store
     *            the store
     * @return the manifests history by date
     */
    List<TargetManifestModel> getManifestsHistoryForStore(final TargetPointOfServiceModel store);

    /**
     * Complete all the consignments in the manifest
     * 
     * @param manifestModel
     */
    void completeConsignmentsInManifest(TargetManifestModel manifestModel, Integer storeNumber);

}

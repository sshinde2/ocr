/**
 * 
 */
package au.com.target.tgtfulfilment.service;

/**
 * @author pvarghe2
 *
 */
public interface TargetPreOrderFulfilmentService {

    /**
     * Method to process the fulfilment for all the parked pre-orders with specific release date.
     */
    void processPreOrderFulfilment();
}
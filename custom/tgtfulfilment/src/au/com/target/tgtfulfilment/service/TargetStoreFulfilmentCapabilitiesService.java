/**
 * 
 */
package au.com.target.tgtfulfilment.service;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.List;
import java.util.Set;

import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.model.ProductExclusionsModel;


/**
 * TargetStoreFulfilmentCapabilitiesService to get capability info from store.
 * 
 * @author jjayawa1
 *
 */
public interface TargetStoreFulfilmentCapabilitiesService {

    /**
     * Check whether store is enabled to fulfill
     * 
     * @param tpos
     * @return boolean
     */
    boolean isStoreEnabledForFulfilment(TargetPointOfServiceModel tpos);

    /**
     * Check whether the delivery mode is available for store.
     * 
     * @param tpos
     * @param deliveryMode
     * @return boolean
     */
    boolean isDeliveryModeAssociatedToStore(TargetPointOfServiceModel tpos, DeliveryModeModel deliveryMode);

    /**
     * * Checks if is target blackout exist for a store.
     *
     * @param tpos
     * @return true, if is target store blackout exist
     */
    boolean isTargetStoreBlackoutExist(TargetPointOfServiceModel tpos);

    /**
     * * returns the Max allowed weight for being eligible for delivery.
     *
     * @param tpos
     *            the Point of service object
     * @return Max Weight Allowed
     */
    Double getMaxAllowedProductWeightForDelivery(final TargetPointOfServiceModel tpos);

    /**
     * * checks if the max number of orders accepted per day has exceeded.
     *
     * @param tpos
     *            the Point of service object
     * @return true if the order count exceeds the max order per day
     */
    boolean isMaxNumberOfOrdersExceeded(final TargetPointOfServiceModel tpos);

    /**
     * @param orderEntryGroup
     * @param excludeCategories
     * @return true if order contains any product which belong to excluded categories
     */
    boolean isOrderContainingProductsFromExcludedCategories(final OrderEntryGroup orderEntryGroup,
            final List<CategoryModel> excludeCategories);

    /**
     * check if products in order belongs to the configured exclusion list
     * 
     * @param oeg
     * @param productExclusionList
     * @return True/False
     */
    boolean isProductInExclusionList(OrderEntryGroup oeg, Set<ProductExclusionsModel> productExclusionList);

    /**
     * Returns the list of restricted product types for a specified store
     * 
     * @param tpos
     *            the Point of service object
     * @return List of product types
     */

    Set<ProductTypeModel> getRestrictedProductTypes(final TargetPointOfServiceModel tpos);

    /**
     * Returns the bufferStock defined for the store
     * 
     * @param tpos
     * @return bufferStock
     */
    Long getBufferStock(TargetPointOfServiceModel tpos);
}
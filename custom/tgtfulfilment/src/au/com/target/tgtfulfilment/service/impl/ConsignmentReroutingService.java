/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.numberseries.NumberSeriesManager;
import de.hybris.platform.ordersplitting.ConsignmentCreationException;
import de.hybris.platform.ordersplitting.impl.DefaultOrderSplittingService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.strategy.SplittingStrategy;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;


/**
 * Re-route after a consignment is assigned to a store.
 * 
 * @author jjayawa1
 *
 */
public class ConsignmentReroutingService extends DefaultOrderSplittingService {

    private static final Logger LOG = Logger.getLogger(ConsignmentReroutingService.class);

    private TargetConsignmentService targetConsignmentService;

    private OEGParameterHelper oegParameterHelper;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private List<SplittingStrategy> legacyStrategiesList;

    /**
     * Re-route consignment
     * 
     * @param consignment
     * @return List
     * @throws ConsignmentCreationException
     */
    public List<ConsignmentModel> rerouteConsignment(final ConsignmentModel consignment)
            throws ConsignmentCreationException {
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)) {
            return rerouteConsignmentByShippedQty(consignment);
        }
        return rerouteConsignmentByOrderEntry(consignment);
    }

    private List<ConsignmentModel> rerouteConsignmentByOrderEntry(final ConsignmentModel consignment)
            throws ConsignmentCreationException {
        Assert.notNull(consignment, "Consignment model cannot be null");

        final AbstractOrderModel order = consignment.getOrder();
        Assert.notNull(order, "Consignment must have an order");

        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();

        final Set<ConsignmentEntryModel> consignmentEntries = consignment.getConsignmentEntries();

        Assert.isTrue(CollectionUtils.isNotEmpty(consignmentEntries), "Consignment must have consignment entries");
        for (final ConsignmentEntryModel consignmentEntry : consignmentEntries) {
            final AbstractOrderEntryModel orderEntryModel = consignmentEntry.getOrderEntry();
            Assert.notNull(orderEntryModel, "ConsignmentEntry must have an OrderEntry");
            orderEntries.add(orderEntryModel);
        }
        return splitOrderForConsignment(order, orderEntries);
    }

    /**
     * check if the consignment require rerouting or not
     * 
     * @param consignment
     * @return true or false
     */
    public boolean isRequireRerouting(final ConsignmentModel consignment) {
        boolean isRequire = false;
        final Set<ConsignmentEntryModel> consignmentEntries = consignment.getConsignmentEntries();
        if (CollectionUtils.isNotEmpty(consignmentEntries)) {
            for (final ConsignmentEntryModel consignmentEntry : consignmentEntries) {
                final long quantity = consignmentEntry.getQuantity() != null ? consignmentEntry.getQuantity()
                        .longValue() : 0;
                final long shippedQuantity = consignmentEntry.getShippedQuantity() != null ? consignmentEntry
                        .getShippedQuantity().longValue() : 0;
                final long diff = quantity - shippedQuantity;
                if (diff > 0) {
                    isRequire = true;
                    break;
                }
            }
        }
        return isRequire;
    }

    /**
     * find item not shipped and reroute
     * 
     * @param consignment
     * @return List<ConsignmentModel>
     * @throws ConsignmentCreationException
     */
    private List<ConsignmentModel> rerouteConsignmentByShippedQty(final ConsignmentModel consignment)
            throws ConsignmentCreationException {
        List<ConsignmentModel> consignmentModelList = new ArrayList<>();
        final OrderEntryGroup oeg = new OrderEntryGroup();
        final Map<String, Long> requiredQtyMap = new HashMap<>();
        oegParameterHelper.setRequiredQty(oeg, requiredQtyMap);
        this.populateOrderEntryGroupWithRequiredQty(consignment, oeg, requiredQtyMap);
        if (CollectionUtils.isNotEmpty(oeg)) {
            consignmentModelList = rerouteConsignmentByOrderEntryGroup(oeg);
        }
        return consignmentModelList;

    }

    protected void populateOrderEntryGroupWithRequiredQty(final ConsignmentModel consignment,
            final OrderEntryGroup oeg, final Map<String, Long> requiredQtyMap) {
        final Set<ConsignmentEntryModel> consignmentEntries = consignment.getConsignmentEntries();
        if (CollectionUtils.isNotEmpty(consignmentEntries)) {
            for (final ConsignmentEntryModel consignmentEntry : consignmentEntries) {
                final long quantity = consignmentEntry.getQuantity() != null ? consignmentEntry.getQuantity()
                        .longValue() : 0;
                final long shippedQuantity = consignmentEntry.getShippedQuantity() != null ? consignmentEntry
                        .getShippedQuantity().longValue() : 0;
                final long diff = quantity - shippedQuantity;
                if (diff > 0) {
                    final AbstractOrderEntryModel orderEntry = consignmentEntry.getOrderEntry();
                    final ProductModel productModel = consignmentEntry.getOrderEntry().getProduct();
                    oeg.add(orderEntry);
                    requiredQtyMap.put(productModel.getCode(), Long.valueOf(diff));
                }
            }
        }
    }

    private List<ConsignmentModel> rerouteConsignmentByOrderEntryGroup(final OrderEntryGroup oeg)
            throws ConsignmentCreationException {
        Assert.isTrue(CollectionUtils.isNotEmpty(oeg), "OrderEntryGroup must have consignment entries");
        final OrderModel order = oegParameterHelper.getOrderFromGivenGroup(oeg);
        Assert.notNull(order, "Consignment must have an order");
        return splitOrderForConsignment(order, oeg);
    }

    @Override
    public List<ConsignmentModel> splitOrderForConsignmentNotPersist(final AbstractOrderModel order,
            final List<AbstractOrderEntryModel> orderEntryList) throws ConsignmentCreationException {
        List<OrderEntryGroup> splitedList = new ArrayList<>();

        OrderEntryGroup tmpOrderEntryList = new OrderEntryGroup();

        if (orderEntryList instanceof OrderEntryGroup) {
            tmpOrderEntryList = (OrderEntryGroup)orderEntryList;
        }
        else {
            tmpOrderEntryList.addAll(orderEntryList);
        }

        splitedList.add(tmpOrderEntryList);

        List<SplittingStrategy> strategies = new ArrayList<>();
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)) {
            strategies = this.getStrategiesList();
        }
        else {
            strategies = legacyStrategiesList;
        }

        if (CollectionUtils.isEmpty(strategies)) {
            LOG.warn("No splitting strategies were configured!");
        }
        for (final SplittingStrategy strategy : strategies) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Applying order splitting strategy : [" + strategy.getClass().getName() + "]");
            }
            splitedList = strategy.perform(splitedList);
        }
        final String orderCode;
        if (order == null) {
            orderCode = getUniqueNumber("ORDER", 10,
                    "GEN0001");
        }
        else {
            orderCode = order.getCode();
        }

        final Object consignmentList = new ArrayList();
        char prefixCode = 'a';
        for (final OrderEntryGroup orderEntryResultList : splitedList) {
            final ConsignmentModel cons = targetConsignmentService.createConsignment(order, prefixCode + orderCode,
                    orderEntryResultList);
            prefixCode = (char)(extractPrefixCodeFromConsignment(cons.getCode(), orderCode) + '\1');

            for (final SplittingStrategy strategy : this.getStrategiesList()) {
                strategy.afterSplitting(orderEntryResultList, cons);
            }

            ((List)consignmentList).add(cons);
        }
        return ((List<ConsignmentModel>)consignmentList);
    }

    private char extractPrefixCodeFromConsignment(final String consignmentCode, final String orderCode) {
        final String strPrefix = StringUtils.removeEnd(consignmentCode, orderCode);
        return strPrefix.charAt(0);
    }


    @SuppressWarnings("deprecation")
    private String getUniqueNumber(final String code, final int digits, final String startValue)
    {
        try
        {
            NumberSeriesManager.getInstance().getNumberSeries(code);
        }
        catch (final JaloInvalidParameterException localJaloInvalidParameterException)
        {
            NumberSeriesManager.getInstance().createNumberSeries(code, startValue,
                    1, digits);
        }

        return NumberSeriesManager.getInstance().getUniqueNumber(code, digits);
    }


    /**
     * @param targetConsignmentService
     *            the targetConsignmentService to set
     */
    @Required
    public void setTargetConsignmentService(final TargetConsignmentService targetConsignmentService) {
        this.targetConsignmentService = targetConsignmentService;
    }

    /**
     * @param oegParameterHelper
     *            the oegParameterHelper to set
     */
    @Required
    public void setOegParameterHelper(final OEGParameterHelper oegParameterHelper) {
        this.oegParameterHelper = oegParameterHelper;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * @param legacyStrategiesList
     *            the legacyStrategiesList to set
     */
    @Required
    public void setLegacyStrategiesList(final List<SplittingStrategy> legacyStrategiesList) {
        this.legacyStrategiesList = legacyStrategiesList;
    }



}

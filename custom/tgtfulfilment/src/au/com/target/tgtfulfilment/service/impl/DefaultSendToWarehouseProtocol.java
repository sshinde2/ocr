/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;

import org.apache.log4j.Logger;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.ConsignmentSentToWarehouseReason;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseResponse;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseResponse;
import au.com.target.tgtfulfilment.service.SendToWarehouseProtocol;


/**
 * default implementation
 * 
 * @author rsamuel3
 * 
 */
public class DefaultSendToWarehouseProtocol implements SendToWarehouseProtocol {

    private static final Logger LOG = Logger.getLogger(DefaultSendToWarehouseProtocol.class);

    @Override
    public SendToWarehouseResponse sendConsignmentExtract(final ConsignmentModel consignment,
            final ConsignmentSentToWarehouseReason reason) {

        LOG.info("No action fulfilment for consignment: " + consignment.getCode());
        return SendToWarehouseResponse.getSuccessRespose();
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.service.SendToWarehouseProtocol#sendUnshippedConsignments(java.util.List)
     */
    @Override
    public PollWarehouseResponse sendUnshippedConsignments(final List<TargetConsignmentModel> consignments) {
        LOG.info("No action fulfilment for consignments");
        return PollWarehouseResponse.getSuccessResponse();
    }


}

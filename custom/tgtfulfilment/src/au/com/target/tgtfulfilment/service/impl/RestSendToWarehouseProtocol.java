/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.ConsignmentSentToWarehouseReason;
import au.com.target.tgtfulfilment.client.PollConsignmentWarehouseRestClient;
import au.com.target.tgtfulfilment.client.SendToWarehouseRestClient;
import au.com.target.tgtfulfilment.integration.converters.sendtowarehouse.SendToWarehouseRequestConverter;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseRequest;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseResponse;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseRequest;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseResponse;
import au.com.target.tgtfulfilment.service.SendToWarehouseProtocol;


/**
 * SendToWarehouseProtocol implementation for REST.
 * 
 * @author jjayawa1
 *
 */
public class RestSendToWarehouseProtocol implements SendToWarehouseProtocol {

    private static final Logger LOG = Logger.getLogger(RestSendToWarehouseProtocol.class);
    private static final String CONSIGNMENT_FOUND_MSG = "{0} consignment(s) found:";

    private SendToWarehouseRestClient sendToWarehouseRestClient;
    private SendToWarehouseRequestConverter sendToWarehouseRequestConverter;
    private PollConsignmentWarehouseRestClient pollWarehouseRestClient;

    /**
     * {@inheritDoc}
     */
    @Override
    public SendToWarehouseResponse sendConsignmentExtract(final ConsignmentModel consignment,
            final ConsignmentSentToWarehouseReason reason) {
        final SendToWarehouseRequest request = sendToWarehouseRequestConverter.convert(consignment);
        return sendToWarehouseRestClient.sendConsignment(request);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.service.SendToWarehouseProtocol#sendUnshippedConsignments(java.util.List)
     */
    @Override
    public PollWarehouseResponse sendUnshippedConsignments(final List<TargetConsignmentModel> consignments) {
        final List<String> consignmentCodes = populateConsigmnetCodeList(consignments);
        LOG.info(MessageFormat.format(CONSIGNMENT_FOUND_MSG, String.valueOf(consignmentCodes.size()))
                + consignmentCodes);
        final PollWarehouseRequest request = new PollWarehouseRequest();
        request.setConsignmentRequestNumbers(consignmentCodes);
        return pollWarehouseRestClient.pollConsignments(request);
    }

    /**
     * @param sendToWarehouseRestClient
     *            the sendToWarehouseRestClient to set
     */
    @Required
    public void setSendToWarehouseRestClient(final SendToWarehouseRestClient sendToWarehouseRestClient) {
        this.sendToWarehouseRestClient = sendToWarehouseRestClient;
    }

    /**
     * @param sendToWarehouseRequestConverter
     *            the sendToWarehouseRequestConverter to set
     */
    @Required
    public void setSendToWarehouseRequestConverter(
            final SendToWarehouseRequestConverter sendToWarehouseRequestConverter) {
        this.sendToWarehouseRequestConverter = sendToWarehouseRequestConverter;
    }

    /**
     * Populate the consignmentCodes for the list of consignments
     * 
     * @param consignmentList
     * @return List
     */
    private List populateConsigmnetCodeList(final List<TargetConsignmentModel> consignmentList) {
        final List consignmentCodesList = new ArrayList<String>();
        if (CollectionUtils.isNotEmpty(consignmentList)) {
            for (final TargetConsignmentModel consignment : consignmentList) {
                consignmentCodesList.add(consignment.getCode());

            }
        }
        return consignmentCodesList;

    }

    /**
     * @param pollWarehouseRestClient
     *            the pollWarehouseRestClient to set
     */
    @Required
    public void setPollWarehouseRestClient(final PollConsignmentWarehouseRestClient pollWarehouseRestClient) {
        this.pollWarehouseRestClient = pollWarehouseRestClient;
    }

}

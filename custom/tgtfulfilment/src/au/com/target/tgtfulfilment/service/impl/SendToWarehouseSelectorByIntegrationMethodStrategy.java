/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.Map;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtfulfilment.enums.IntegrationMethod;
import au.com.target.tgtfulfilment.service.SendToWarehouseProtocol;
import au.com.target.tgtfulfilment.service.SendToWarehouseSelectorStrategy;


/**
 * Strategy for getting the right service for sending to warehouse, <br/>
 * depending on the integration method on the warehouse.
 * 
 * @author sbryan6
 *
 */
public class SendToWarehouseSelectorByIntegrationMethodStrategy implements SendToWarehouseSelectorStrategy {

    private Map<String, SendToWarehouseProtocol> sendToWarehouseProtocols;

    private String defaultIntegrationMethod;

    private TargetFeatureSwitchService targetFeatureSwitchService;


    @Override
    public SendToWarehouseProtocol getSendToWarehouseProtocolForConsignment(final ConsignmentModel consignment) {
        Assert.notNull(consignment, "consignment must not be null");

        final IntegrationMethod method = getIntegrationMethod(consignment);

        // Map should contain the integration method - if not then there is a config error
        Assert.isTrue(sendToWarehouseProtocols.containsKey(method.toString()),
                "Unsupported integration method: " + method + " for consignment: " + consignment.getCode());

        final SendToWarehouseProtocol fulfilmentProcessService = sendToWarehouseProtocols
                .get(method.toString());
        return fulfilmentProcessService;
    }

    /**
     * Return the integrationMethod for the given consignment
     * 
     * @return IntegrationMethod
     */
    protected IntegrationMethod getIntegrationMethod(final ConsignmentModel consignment) {
        Assert.notNull(consignment, "consignment must not be null");

        // Default to fallback method
        IntegrationMethod method = IntegrationMethod.valueOf(defaultIntegrationMethod);

        final WarehouseModel warehouse = consignment.getWarehouse();
        if (warehouse != null && warehouse.getIntegrationMethod() != null) {
            if (targetFeatureSwitchService
                    .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FORCE_DEFAULT_WAREHOUSE_TO_ONLINE_POS)
                    && BooleanUtils.isTrue(warehouse.getDefault())) {
                method = IntegrationMethod.NONE;
            }
            else {
                method = warehouse.getIntegrationMethod();
            }
        }

        return method;
    }

    /**
     * @param sendToWarehouseProtocols
     *            the sendToWarehouseProtocols to set
     */
    @Required
    public void setSendToWarehouseProtocols(final Map<String, SendToWarehouseProtocol> sendToWarehouseProtocols) {
        this.sendToWarehouseProtocols = sendToWarehouseProtocols;
    }


    /**
     * @param defaultIntegrationMethod
     *            the defaultIntegrationMethod to set
     */
    @Required
    public void setDefaultIntegrationMethod(final String defaultIntegrationMethod) {
        this.defaultIntegrationMethod = defaultIntegrationMethod;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }


}

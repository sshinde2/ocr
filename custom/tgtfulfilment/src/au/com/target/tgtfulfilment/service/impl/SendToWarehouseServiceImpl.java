/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.ConsignmentSentToWarehouseReason;
import au.com.target.tgtfulfilment.exceptions.ConsignmentStatusValidationException;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.fulfilmentservice.TargetFulfilmentService;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseResponse;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseResponse;
import au.com.target.tgtfulfilment.service.SendToWarehouseProtocol;
import au.com.target.tgtfulfilment.service.SendToWarehouseSelectorStrategy;
import au.com.target.tgtfulfilment.service.SendToWarehouseService;


/**
 * @author sbryan6
 *
 */
public class SendToWarehouseServiceImpl implements SendToWarehouseService {

    /**
     * 
     */
    private static final String SEND_CONSIGNMENT_ERROR = "SendToWarehouse of an action={0} consignment with code={1} for the order={2} has failed with errorCodes={3}, errorMessages={4} and errorType={5}";

    private static final Logger LOG = Logger.getLogger(SendToWarehouseServiceImpl.class);

    private SendToWarehouseSelectorStrategy sendToWarehouseSelectorStrategy;

    private TargetFulfilmentService targetFulfilmentService;

    private ModelService modelService;

    @Override
    public void sendNewOrder(final OrderModel order) throws FulfilmentException, ConsignmentStatusValidationException {

        Assert.notNull(order, "order must not be null");

        // Send all the consignments to warehouse
        final Set<ConsignmentModel> consignments = order.getConsignments();
        Assert.notNull(consignments, "consignments cannot be null");
        Assert.notEmpty(consignments, "There are no consignments to send");
        for (final ConsignmentModel consignment : consignments) {

            final TargetConsignmentModel targetConsignment = checkConsignment(consignment, order.getCode());
            if (targetConsignment == null) {
                continue;
            }

            if (!validateNewConsignmentCurrentStatus(targetConsignment)) {
                LOG.warn("Consignment is in wrong status for send to warehouse for new order: " + order.getCode());
                continue;
            }

            final SendToWarehouseResponse response = sendConsignmentExtract(targetConsignment,
                    ConsignmentSentToWarehouseReason.NEW);
            if (response.isSuccess()) {
                setNewOrderConsignmentStatus(targetConsignment);
            }
            else {
                logConsignmentSendError(ConsignmentSentToWarehouseReason.NEW, order, targetConsignment, response);
            }
        }

    }


    @Override
    public void sendOrderCancellation(final OrderModel order) {

        Assert.notNull(order, "order must not be null");

        // Send all the consignment cancellations to warehouse
        final Set<ConsignmentModel> consignments = order.getConsignments();

        Assert.notNull(consignments, "consignments cannot be null");
        Assert.notEmpty(consignments, "There are no consignments to send");

        final String orderCode = order.getCode();
        for (final ConsignmentModel consignment : consignments) {

            final TargetConsignmentModel targetConsignment = checkConsignment(consignment, orderCode);
            if (targetConsignment == null) {
                LOG.warn("Consignment is null for order: "
                        + orderCode);
                continue;
            }

            if (!validateCancelConsignmentCurrentStatus(targetConsignment)) {
                LOG.warn("Consignment is in wrong status for send cancellation to warehouse for order: "
                        + orderCode);
                continue;
            }
            if (!shouldCancellationOfConsignmentBeSentToWarehouse(targetConsignment)) {
                LOG.info(MessageFormat.format("Cancel Consignment {0} doesn't need to be send to warehouse {1}",
                        consignment.getCode(),
                        targetConsignment.getWarehouse() != null ? targetConsignment.getWarehouse().getCode() : null));
                continue;
            }

            final ConsignmentSentToWarehouseReason cancelReason = getCancelReason(targetConsignment);
            final SendToWarehouseResponse response = sendConsignmentExtract(targetConsignment,
                    cancelReason);

            if (!response.isSuccess()) {
                logConsignmentSendError(cancelReason, order, targetConsignment, response);
            }

        }

    }

    private boolean shouldCancellationOfConsignmentBeSentToWarehouse(final ConsignmentModel consignment) {
        if (null != consignment.getWarehouse() && consignment.getWarehouse().isExternalWarehouse()) {
            return false;
        }
        return true;
    }


    @Override
    public void sendConsignment(final ConsignmentModel consignment) throws FulfilmentException,
            ConsignmentStatusValidationException {

        final TargetConsignmentModel targetConsignment = checkConsignment(consignment, null);
        Assert.notNull(consignment, "Consignment cannot be null");
        if (!TgtCoreConstants.CANCELLATION_WAREHOUSE.equals(targetConsignment.getWarehouse().getCode())) {
            final SendToWarehouseResponse response = sendConsignmentExtract(targetConsignment,
                    ConsignmentSentToWarehouseReason.NEW);
            if (response.isSuccess()) {
                setNewOrderConsignmentStatus(targetConsignment);
            }
            else {
                final AbstractOrderModel order = consignment.getOrder();
                logConsignmentSendError(ConsignmentSentToWarehouseReason.NEW, order, targetConsignment, response);
            }
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.service.SendToWarehouseService#sendUnshippedConsignemnts(java.util.List)
     */
    @Override
    public PollWarehouseResponse sendUnshippedConsignemnts(final List<TargetConsignmentModel> consignments) {
        Assert.notNull(consignments, "consignment List cannot be null");
        Assert.notEmpty(consignments, "There are no consignments to send");
        final SendToWarehouseProtocol fulfilmentProcessService = sendToWarehouseSelectorStrategy
                .getSendToWarehouseProtocolForConsignment(consignments.get(0));
        return fulfilmentProcessService.sendUnshippedConsignments(consignments);

    }

    /**
     * Validate the consignment is in correct state for a new order
     * 
     * @param consignment
     * @return true if in right state
     */
    protected boolean validateNewConsignmentCurrentStatus(final ConsignmentModel consignment) {

        if (!ConsignmentStatus.CREATED.equals(consignment.getStatus())) {
            LOG.warn("consignment expected in state CREATED, was in " + consignment.getStatus());
            return false;
        }

        return true;
    }


    /**
     * Validate the consignment is in correct state for a cancellation
     * 
     * @param consignment
     * @return true if in right state
     */
    protected boolean validateCancelConsignmentCurrentStatus(final ConsignmentModel consignment) {
        Assert.notNull(consignment, "consignment must not be null");

        // For a cancel situation, consignment is expected to be at the warehouse or it may have been cancelled
        if (!(ConsignmentStatus.SENT_TO_WAREHOUSE.equals(consignment.getStatus())
                || ConsignmentStatus.CONFIRMED_BY_WAREHOUSE.equals(consignment.getStatus())
                || ConsignmentStatus.PICKED.equals(consignment.getStatus())
                || ConsignmentStatus.CANCELLED.equals(consignment.getStatus()))) {
            LOG.warn("Attempt to do cancel extract to warehouse for Consignment " + consignment.getCode()
                    + " in state " + consignment.getStatus());
            return false;
        }

        return true;
    }


    /**
     * Get the ConsignmentSentToWarehouseReason for a cancelled or part-cancelled consignment
     * 
     * @param consignment
     * @return ConsignmentSentToWarehouseReason
     */
    protected ConsignmentSentToWarehouseReason getCancelReason(final ConsignmentModel consignment) {

        if (ConsignmentStatus.CANCELLED.equals(consignment.getStatus())) {
            return ConsignmentSentToWarehouseReason.CANCELLED;
        }

        return ConsignmentSentToWarehouseReason.UPDATED;
    }


    /**
     * Send the consignment extract using process from selection strategy
     * 
     * @param consignment
     * @param reason
     */
    protected SendToWarehouseResponse sendConsignmentExtract(final ConsignmentModel consignment,
            final ConsignmentSentToWarehouseReason reason) {

        final SendToWarehouseProtocol fulfilmentProcessService = sendToWarehouseSelectorStrategy
                .getSendToWarehouseProtocolForConsignment(consignment);

        return fulfilmentProcessService.sendConsignmentExtract(consignment, reason);
    }


    private void setNewOrderConsignmentStatus(final TargetConsignmentModel targetConsignment)
            throws FulfilmentException, ConsignmentStatusValidationException {
        setConsignmentStatus(targetConsignment, ConsignmentStatus.SENT_TO_WAREHOUSE);
        targetFulfilmentService.processConsignmentAutoActions(targetConsignment);
    }

    private void setConsignmentStatus(final TargetConsignmentModel consignment, final ConsignmentStatus status) {

        consignment.setStatus(status);
        consignment.setSentToWarehouseDate(new Date());
        modelService.save(consignment);
    }


    private TargetConsignmentModel checkConsignment(final ConsignmentModel consignment, final String orderCode) {

        if (consignment == null) {
            LOG.warn("Consignment shouldn't be null at this point for order: " + orderCode);
            return null;
        }

        if (!(consignment instanceof TargetConsignmentModel)) {
            LOG.warn("Consignment is not of type 'TargetConsignment' for order: " + orderCode);
            return null;
        }

        return (TargetConsignmentModel)consignment;
    }

    /**
     * @param order
     * @param targetConsignment
     * @param response
     */
    private void logConsignmentSendError(final ConsignmentSentToWarehouseReason reason, final AbstractOrderModel order,
            final TargetConsignmentModel targetConsignment,
            final SendToWarehouseResponse response) {
        final String errorType = response.getErrorType() != null ? response.getErrorType().toString()
                : "UNKNOWN_ERROR_TYPE";
        LOG.warn(MessageFormat.format(SEND_CONSIGNMENT_ERROR, reason.toString(), targetConsignment.getCode(),
                order.getCode(),
                response.getErrorCode(), response.getErrorValue(), errorType));
    }


    /**
     * @param sendToWarehouseSelectorStrategy
     *            the sendToWarehouseSelectorStrategy to set
     */
    @Required
    public void setSendToWarehouseSelectorStrategy(
            final SendToWarehouseSelectorStrategy sendToWarehouseSelectorStrategy) {
        this.sendToWarehouseSelectorStrategy = sendToWarehouseSelectorStrategy;
    }


    /**
     * @param targetFulfilmentService
     *            the targetFulfilmentService to set
     */
    @Required
    public void setTargetFulfilmentService(final TargetFulfilmentService targetFulfilmentService) {
        this.targetFulfilmentService = targetFulfilmentService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }



}

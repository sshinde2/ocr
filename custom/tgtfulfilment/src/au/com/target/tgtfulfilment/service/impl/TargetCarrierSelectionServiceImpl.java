/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.PostCodeGroupModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.postcode.service.TargetPostCodeService;
import au.com.target.tgtfulfilment.dao.TargetCarrierDao;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;
import au.com.target.tgtfulfilment.model.TargetPostCodeAwareCarrierModel;
import au.com.target.tgtfulfilment.service.TargetCarrierSelectionService;


/**
 * @author Vivek
 *
 */
public class TargetCarrierSelectionServiceImpl implements TargetCarrierSelectionService {

    private TargetCarrierDao targetCarrierDao;
    private TargetPostCodeService targetPostCodeService;

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.service.TargetCarrierSelectionService#selectCarrier()
     */
    @Override
    public TargetCarrierModel getPreferredCarrier(final TargetConsignmentModel consignment, final boolean bulky,
            final boolean enabledInStore) {

        final List<TargetCarrierModel> carrierModels = targetCarrierDao.getApplicableCarriers(
                (TargetZoneDeliveryModeModel)consignment.getDeliveryMode(), bulky, enabledInStore);

        final Double consignmentWeight = consignment.getTotalWeight();

        for (final TargetCarrierModel carrier : carrierModels) {
            final Double maxAllowedWeight = carrier.getMaxWeight();

            if (null == maxAllowedWeight) {
                return carrier;
            }

            if (null != consignmentWeight && consignmentWeight.doubleValue() < maxAllowedWeight.doubleValue()) {
                if (carrier instanceof TargetPostCodeAwareCarrierModel) {

                    final String consPostCode = (null != consignment.getShippingAddress() && null != consignment
                            .getShippingAddress().getPostalcode()) ? consignment.getShippingAddress().getPostalcode()
                            : null;

                    final Collection<PostCodeGroupModel> carrierPCGroups = ((TargetPostCodeAwareCarrierModel)carrier)
                            .getPostCodeGroups();

                    if (targetPostCodeService.doesPostCodeBelongsToGroups(consPostCode, carrierPCGroups)) {
                        return carrier;
                    }
                }
                else {
                    return carrier;
                }
            }
        }
        return null;
    }


    @Override
    public TargetCarrierModel getNullCarrier(final TargetZoneDeliveryModeModel deliveryMode) {

        return targetCarrierDao.getNullCarrier(deliveryMode);
    }

    /**
     * @param targetCarrierDao
     *            the targetCarrierDao to set
     */
    @Required
    public void setTargetCarrierDao(final TargetCarrierDao targetCarrierDao) {
        this.targetCarrierDao = targetCarrierDao;
    }

    /**
     * @param targetPostCodeService
     *            the targetPostCodeService to set
     */
    @Required
    public void setTargetPostCodeService(final TargetPostCodeService targetPostCodeService) {
        this.targetPostCodeService = targetPostCodeService;
    }

}
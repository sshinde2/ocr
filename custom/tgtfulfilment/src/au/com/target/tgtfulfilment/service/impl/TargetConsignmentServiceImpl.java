/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.ConsignmentCreationException;
import de.hybris.platform.ordersplitting.impl.DefaultConsignmentService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.dao.TargetConsignmentDao;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.enums.StorePortal;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;
import au.com.target.tgtfulfilment.warehouseservices.TargetFulfilmentWarehouseService;


public class TargetConsignmentServiceImpl extends DefaultConsignmentService implements TargetConsignmentService {

    private static final Logger LOG = Logger.getLogger(TargetConsignmentServiceImpl.class);
    private static final String WARN_MULTIPLE_DELIVER_TO_STORE_CONS = "More than one active deliver to store consignment found for order with orderNumber={0}";
    private static final String WARN_NO_CONSIGNMENT_FOUND = "No Consignment found for consignmentstatus and warehouseCode:{0} and warehouse {1}";
    private static final String WARN_NO_WAREHOUSE_FOUND = "No warehouse found for warehouseCode:{0}";

    private ModelService modelService;
    private TargetWarehouseService targetWarehouseService;
    private TargetOrderService targetOrderService;
    private TargetConsignmentDao targetConsignmentDao;
    private int currentConsignmentVersion;
    private TargetFulfilmentWarehouseService targetFulfilmentWarehouseService;
    private OEGParameterHelper oegParameterHelper;
    private TargetFeatureSwitchService targetFeatureSwitchService;


    /* (non-Javadoc)
     * @see de.hybris.platform.ordersplitting.impl.DefaultConsignmentService#createConsignment(de.hybris.platform.core.model.order.AbstractOrderModel, java.lang.String, java.util.List)
     */
    @Override
    public ConsignmentModel createConsignment(final AbstractOrderModel order, final String code,
            final List<AbstractOrderEntryModel> orderEntries) throws ConsignmentCreationException {

        Assert.notNull(order, "Order cannot be null");
        Assert.isTrue(CollectionUtils.isNotEmpty(orderEntries), "OrderEntries cannot be null or Empty!");

        Map<String, Long> assignedQty = null;
        if (orderEntries instanceof OrderEntryGroup) {
            final OrderEntryGroup oeg = (OrderEntryGroup)orderEntries;
            assignedQty = oegParameterHelper.getAssignedQty(oeg);
        }
        Assert.notNull(assignedQty, "assigned qty cannot be null");

        final TargetConsignmentModel cons = modelService.create(TargetConsignmentModel.class);

        cons.setStatus(ConsignmentStatus.READY);
        cons.setConsignmentEntries(new HashSet<ConsignmentEntryModel>());
        cons.setCode(getUniqueConsignmentCode(order, code));
        cons.setConsignmentVersion(Integer.valueOf(currentConsignmentVersion));

        final AbstractOrderEntryModel abstractOrderEntry = orderEntries.get(0);
        if (null != abstractOrderEntry && null != abstractOrderEntry.getDeliveryMode()) {
            cons.setDeliveryMode(abstractOrderEntry.getDeliveryMode());
            cons.setShippingAddress(abstractOrderEntry.getDeliveryAddress());
        }
        else {
            cons.setShippingAddress(order.getDeliveryAddress());
            cons.setDeliveryMode(order.getDeliveryMode());
        }

        for (final AbstractOrderEntryModel orderEntry : orderEntries) {
            // read the assigned quantity from parameter ASSIGNED_QTY in OrderEntryGroup
            if (orderEntry != null) {
                final Long qty = assignedQty.get(orderEntry.getProduct().getCode());
                if (qty == null) {
                    LOG.error("product assigned qty cannot be null");
                    continue;
                }
                final ConsignmentEntryModel entry = modelService.create(ConsignmentEntryModel.class);
                entry.setOrderEntry(orderEntry);
                entry.setQuantity(qty);
                entry.setConsignment(cons);
                cons.getConsignmentEntries().add(entry);
            }
        }

        final List<WarehouseModel> warehouses = targetWarehouseService.getWarehouses(orderEntries);
        if (warehouses.isEmpty()) {
            throw new ConsignmentCreationException("No default warehouse found for consignment");
        }
        final WarehouseModel warehouse = warehouses.iterator().next();
        cons.setWarehouse(warehouse);
        cons.setOrder(order);

        return cons;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.service.TargetConsignmentService#findConsignmentFromOrderWithStatus(de.hybris.platform.core.model.order.AbstractOrderModel, java.util.List)
     */
    @Override
    public ConsignmentModel findConsignmentFromOrderWithStatus(final AbstractOrderModel order,
            final List<ConsignmentStatus> status) {
        Assert.notNull(order, "Order cannot be null");
        Assert.notEmpty(status, "Consignment status needs to be set");
        final Set<ConsignmentModel> consignments = order.getConsignments();
        for (final ConsignmentStatus consStatus : status) {
            for (final ConsignmentModel cons : consignments) {
                if (cons.getStatus().equals(consStatus)) {
                    return cons;
                }
            }
        }
        return null;
    }

    @Override
    public List<TargetConsignmentModel> getActiveConsignmentsForOrder(final AbstractOrderModel order) {
        Assert.notNull(order, "Order cannot be null");

        final List<TargetConsignmentModel> activeOnes = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(order.getConsignments())) {
            for (final ConsignmentModel cons : order.getConsignments()) {
                if (cons.getStatus() != null) {
                    if (!cons.getStatus().equals(ConsignmentStatus.CANCELLED)
                            && cons instanceof TargetConsignmentModel) {
                        activeOnes.add((TargetConsignmentModel)cons);
                    }
                }
            }
        }
        return activeOnes;
    }

    @Override
    public List<TargetConsignmentModel> getActivePPDStoreConsignments(final AbstractOrderModel order) {
        Assert.notNull(order, "Order cannot be null");

        final List<TargetConsignmentModel> activeCons = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(order.getConsignments())) {
            for (final ConsignmentModel cons : order.getConsignments()) {
                if (checkConsignmentStatusWarehouseAndOFCType(cons)) {
                    activeCons.add((TargetConsignmentModel)cons);
                }

            }
        }
        return activeCons;
    }

    /**
     * check the consignment is not cancelled, and not same store instore pickup, and not fastlinewarehouse, not
     * external warehouse
     * 
     * @param cons
     * @return true of false
     */
    private boolean checkConsignmentStatusWarehouseAndOFCType(final ConsignmentModel cons) {
        if (cons instanceof TargetConsignmentModel) {
            final TargetConsignmentModel targetCons = (TargetConsignmentModel)cons;
            return targetCons.getStatus() != null && !targetCons.getStatus().equals(ConsignmentStatus.CANCELLED)
                    && targetCons.getOfcOrderType() != null
                    && targetCons.getOfcOrderType() != OfcOrderType.INSTORE_PICKUP
                    && targetCons.getWarehouse() != null && !targetCons.getWarehouse().isExternalWarehouse()
                    && !TgtCoreConstants.FASTLINE_WAREHOUSE.equalsIgnoreCase(targetCons.getWarehouse().getCode());
        }
        return false;
    }

    @Override
    public List<TargetConsignmentModel> getActiveDeliverToStoreConsignmentsForOrder(final AbstractOrderModel order) {
        Assert.notNull(order, "Order cannot be null");

        final List<TargetConsignmentModel> activeOnes = getActiveConsignmentsForOrder(order);
        final List<TargetConsignmentModel> cncOnes = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(activeOnes)) {
            for (final ConsignmentModel cons : activeOnes) {
                final DeliveryModeModel delMode = cons.getDeliveryMode();
                if (delMode instanceof TargetZoneDeliveryModeModel) {
                    final TargetZoneDeliveryModeModel tzdm = (TargetZoneDeliveryModeModel)delMode;
                    if (BooleanUtils.isTrue(tzdm.getIsDeliveryToStore())) {
                        cncOnes.add((TargetConsignmentModel)cons);
                    }
                }
            }
        }
        return cncOnes;
    }

    @Override
    public TargetConsignmentModel findConsignmentForDefaultWarehouse(final String orderCode) {
        final OrderModel order = targetOrderService.findOrderModelForOrderId(orderCode);
        final List<TargetConsignmentModel> activeConsignments = getActiveConsignmentsForOrder(order);
        for (final TargetConsignmentModel consignment : activeConsignments) {
            final WarehouseModel warehouse = consignment.getWarehouse();
            if (warehouse != null) {
                if (BooleanUtils.isTrue(warehouse.getDefault())) {
                    modelService.refresh(consignment);
                    return consignment;
                }
            }
        }
        return null;
    }

    /**
     * generate a consignment code that is unique
     * 
     * @param order
     * @param code
     * @return String which is the valid code
     */
    protected String getUniqueConsignmentCode(final AbstractOrderModel order, final String code) {
        String validCode = code;
        final Set<ConsignmentModel> consignments = order.getConsignments();
        if (CollectionUtils.isEmpty(consignments)) {
            return validCode;
        }
        final String orderCode = order.getCode();
        final String strPrefix = StringUtils.removeEnd(code, orderCode);
        char prefix = strPrefix.charAt(0);
        ConsignmentModel consignment = null;
        do {
            validCode = prefix + orderCode;
            consignment = (ConsignmentModel)CollectionUtils.find(consignments, new ConsignmentCodePredicate(validCode));
            prefix++;
        }
        while (consignment != null);
        return validCode;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Override
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param targetWarehouseService
     *            the warehouseService to set
     */
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }

    /**
     * predicate that compares the consignment codes
     * 
     * @author rsamuel3
     *
     */
    class ConsignmentCodePredicate implements Predicate {

        private final String code;

        ConsignmentCodePredicate(final String code) {
            this.code = code;
        }

        /* (non-Javadoc)
         * @see org.apache.commons.collections.Predicate#evaluate(java.lang.Object)
         */
        @Override
        public boolean evaluate(final Object arg0) {
            if (arg0 instanceof ConsignmentModel) {
                final ConsignmentModel cons = (ConsignmentModel)arg0;
                return cons.getCode().equals(code);
            }
            return false;
        }

    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.service.TargetConsignmentService#getConsignmentForCode(java.lang.String)
     */
    @Override
    public TargetConsignmentModel getConsignmentForCode(final String consignmentCode) throws NotFoundException {
        TargetConsignmentModel consignment = null;
        try {
            consignment = targetConsignmentDao.getConsignmentBycode(consignmentCode);
        }
        catch (final TargetAmbiguousIdentifierException e) {
            throw new IllegalArgumentException(
                    "Error getting consignment details for consignment : " + consignmentCode,
                    e);
        }
        catch (final TargetUnknownIdentifierException e) {
            throw new NotFoundException("Error consignment details not found for consignment : " + consignmentCode, e);
        }
        if (consignment == null) {
            throw new NotFoundException("Error consignment details not found for consignment : " + consignmentCode);
        }
        return consignment;
    }

    @Override
    public boolean updatePickedupDate(final TargetConsignmentModel consignment) {
        if (consignment != null && consignment.getPickedUpDate() == null) {
            consignment.setPickedUpDate(new Date());
            modelService.save(consignment);
            return true;
        }
        return false;
    }

    @Override
    public boolean updateReadyForPickupDate(final TargetConsignmentModel consignmentModel) {
        if (consignmentModel != null && consignmentModel.getReadyForPickUpDate() == null) {
            consignmentModel.setReadyForPickUpDate(new Date());
            modelService.save(consignmentModel);
            return true;
        }
        return false;
    }

    @Override
    public void updateReturnedToFloorDate(final TargetConsignmentModel consignment) {
        if (consignment != null && consignment.getReturnedToFloorDate() == null) {
            consignment.setReturnedToFloorDate(new Date());
            modelService.save(consignment);
        }
    }

    @Override
    public TargetConsignmentModel getActiveDeliverToStoreConsignmentForOrder(final OrderModel orderModel) {
        final List<TargetConsignmentModel> consignments = getActiveDeliverToStoreConsignmentsForOrder(orderModel);
        if (CollectionUtils.isNotEmpty(consignments)) {
            if (consignments.size() > 1) {
                LOG.warn(MessageFormat.format(WARN_MULTIPLE_DELIVER_TO_STORE_CONS, orderModel.getCode()));
            }
            return consignments.get(0);
        }
        return null;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.service.TargetConsignmentService#getAllConsignmentByStatusAndWarehouse(de.hybris.platform.basecommerce.enums.ConsignmentStatus, java.lang.String)
     */
    @Override
    public List<TargetConsignmentModel> getAllConsignmentByStatusAndWarehouse(
            final ConsignmentStatus consignmentStatus,
            final String warehouseCode) throws NotFoundException {
        List<TargetConsignmentModel> consignmentList = new ArrayList<>();
        try {
            final WarehouseModel warehouse = targetFulfilmentWarehouseService.getWarehouseForCode(warehouseCode);
            consignmentList = targetConsignmentDao.getAllConsignmentsByStatusAndWarehouse(warehouse,
                    consignmentStatus);
            if (consignmentList.isEmpty()) {
                LOG.info(MessageFormat.format(WARN_NO_CONSIGNMENT_FOUND, consignmentStatus,
                        warehouseCode));
            }

        }
        catch (final UnknownIdentifierException e) {
            LOG.warn(MessageFormat.format(WARN_NO_WAREHOUSE_FOUND,
                    warehouseCode));
            throw new NotFoundException(MessageFormat.format(WARN_NO_WAREHOUSE_FOUND,
                    warehouseCode), e);
        }
        return consignmentList;
    }

    @Override
    public TargetConsignmentModel getOrCreateConsignmentForCode(final String consignmentCode) {
        try {
            return getConsignmentForCode(consignmentCode);
        }
        catch (final NotFoundException e) {
            return createConsignmentForCode(consignmentCode);
        }
    }

    @Override
    public TargetConsignmentModel createConsignmentForCode(final String consignmentCode) {
        final TargetConsignmentModel consignment = modelService.create(TargetConsignmentModel.class);
        consignment.setCode(consignmentCode);
        consignment.setStatus(ConsignmentStatus.CREATED);
        consignment.setConsignmentVersion(Integer.valueOf(currentConsignmentVersion));
        return consignment;
    }

    @Override
    public boolean isConsignmentToDefaultWarehouse(final ConsignmentModel consignment) {
        if (consignment != null) {
            return consignment.getWarehouse() != null
                    && consignment.getWarehouse().equals(targetWarehouseService.getDefaultOnlineWarehouse());
        }
        return false;
    }

    /**
     * checks whether order associated to consignment is a fluent order or not. If fluent order, then return true else
     * false.
     */
    @Override
    public boolean isAFluentConsignment(final ConsignmentModel consignment) {
        Assert.notNull(consignment, "consignment cannot be null");
        final AbstractOrderModel order = consignment.getOrder();
        Assert.notNull(order, "order cannot be null");

        return targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)
                && StringUtils.isNotBlank(order.getFluentId());
    }

    @Override
    public boolean isConsignmentServicedByFluentServicePoint(final TargetConsignmentModel consignment) {

        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)) {
            final WarehouseModel warehouse = consignment.getWarehouse();
            final Collection<PointOfServiceModel> pointsOfService = warehouse.getPointsOfService();
            // we only have one POS for each warehouse
            if (CollectionUtils.isNotEmpty(pointsOfService) && pointsOfService.size() == 1) {
                final PointOfServiceModel posModel = pointsOfService.iterator().next();
                if (posModel instanceof TargetPointOfServiceModel) {
                    final StorePortal storePortal = ((TargetPointOfServiceModel)posModel).getStorePortal();
                    return StorePortal.SERVICE_POINT == storePortal;
                }
            }
        }
        return false;
    }

    /**
     * @param targetConsignmentDao
     *            the targetConsignmentDao to set
     */
    @Required
    public void setTargetConsignmentDao(final TargetConsignmentDao targetConsignmentDao) {
        this.targetConsignmentDao = targetConsignmentDao;
    }

    /**
     * @param targetOrderService
     *            the targetOrderService to set
     */
    @Required
    public void setTargetOrderService(final TargetOrderService targetOrderService) {
        this.targetOrderService = targetOrderService;
    }

    /**
     * @param currentConsignmentVersion
     *            the currentConsignmentVersion to set
     */
    @Required
    public void setCurrentConsignmentVersion(final int currentConsignmentVersion) {
        this.currentConsignmentVersion = currentConsignmentVersion;
    }

    /**
     * @param targetFulfilmentWarehouseService
     *            the targetFulfilmentWarehouseService to set
     */
    @Required
    public void setTargetFulfilmentWarehouseService(
            final TargetFulfilmentWarehouseService targetFulfilmentWarehouseService) {
        this.targetFulfilmentWarehouseService = targetFulfilmentWarehouseService;
    }

    @Required
    public void setOegParameterHelper(final OEGParameterHelper oegParameterHelper) {
        this.oegParameterHelper = oegParameterHelper;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

}

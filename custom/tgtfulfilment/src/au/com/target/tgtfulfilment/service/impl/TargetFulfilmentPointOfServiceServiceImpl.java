/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.impl.TargetPointOfServiceServiceImpl;
import au.com.target.tgtfulfilment.dao.TargetFulfilmentPointOfServiceDao;
import au.com.target.tgtfulfilment.service.TargetFulfilmentPointOfServiceService;


/**
 * @author Nandini
 *
 */
public class TargetFulfilmentPointOfServiceServiceImpl extends TargetPointOfServiceServiceImpl
        implements TargetFulfilmentPointOfServiceService {

    private TargetFulfilmentPointOfServiceDao targetFulfilmentPointOfServiceDao;

    @Override
    public TargetPointOfServiceModel getNTLByAddress(final AddressModel address) {
        Assert.notNull(address, "Address model can't be null.");

        RegionModel region = null;
        if (null != address.getRegion()) {
            region = address.getRegion();
        }
        else if (StringUtils.isNotBlank(address.getDistrict())) {
            region = getTargetPointOfServiceDao().getRegionByAbbreviation(address.getDistrict());
        }

        if (region == null) {
            return null;
        }

        return targetFulfilmentPointOfServiceDao.getNTLByRegion(region);
    }

    /**
     * @param targetFulfilmentPointOfServiceDao
     *            the targetfulfilmentPointOfServiceDao to set
     */
    @Required
    public void setTargetFulfilmentPointOfServiceDao(
            final TargetFulfilmentPointOfServiceDao targetFulfilmentPointOfServiceDao) {
        this.targetFulfilmentPointOfServiceDao = targetFulfilmentPointOfServiceDao;
    }

}

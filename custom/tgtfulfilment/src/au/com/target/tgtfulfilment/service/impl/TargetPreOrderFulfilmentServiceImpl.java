/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.order.FluentErrorException;
import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtcore.order.FluentOrderService;
import au.com.target.tgtcore.order.TargetPreOrderService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfluent.constants.FluentOrderStatus;
import au.com.target.tgtfluent.data.OrderResponse;
import au.com.target.tgtfluent.data.StockDatum;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.service.FluentStockLookupService;
import au.com.target.tgtfulfilment.service.TargetPreOrderFulfilmentService;


/**
 * @author pvarghe2
 *
 */
public class TargetPreOrderFulfilmentServiceImpl implements TargetPreOrderFulfilmentService {

    private static final Logger LOG = Logger.getLogger(TargetPreOrderFulfilmentServiceImpl.class);

    private int daysToAdvance;

    private TargetPreOrderService targetPreOrderService;

    private FluentOrderService fluentOrderService;

    private FluentStockLookupService fluentStockLookupService;

    private TargetWarehouseService targetWarehouseService;

    private TargetBusinessProcessService targetBusinessProcessService;


    @Override
    public void processPreOrderFulfilment() {
        final List<OrderModel> orders = targetPreOrderService.getParkedPreOrdersForReleaseDate(new Date(),
                daysToAdvance);
        if (CollectionUtils.isEmpty(orders)) {
            LOG.info("PRE-ORDER-FULFILMENT: No Parked Pre-orders retreived for fulfilment");
            return;
        }
        final List<String> noStockSKUList = new ArrayList<>();
        for (final OrderModel order : orders) {
            final String fluentOrderId = order.getFluentId();
            LOG.info(MessageFormat.format(
                    "PRE-ORDER-FULFILMENT : Starting Fulfilment process for Order with, orderNumber={0} and fluentOrderId={1}",
                    order.getCode(), fluentOrderId));
            try {
                final OrderResponse orderResponse = fluentOrderService.getOrder(fluentOrderId);
                final String fluentOrderStatus = orderResponse != null ? orderResponse.getStatus() : StringUtils.EMPTY;
                if (FluentOrderStatus.PARKED.equals(FluentOrderStatus.get(fluentOrderStatus))) {
                    checkSOHAndRelease(order, noStockSKUList);
                }
                else if (FluentOrderStatus.PENDING.equals(FluentOrderStatus.get(fluentOrderStatus))) {
                    LOG.warn(MessageFormat.format(
                            "PRE-ORDER-FULFILMENT: Starting PreOrder Release Business Process since the order status is PENDING in fluent for order with orderNumber={0} and fluentOrderId={1}",
                            order.getCode(), fluentOrderId));
                    targetBusinessProcessService.startFluentReleasePreOrderProcess(order);
                }
                else {
                    LOG.error(MessageFormat.format(
                            "PRE-ORDER-FULFILMENT-FAILED: Attempted to process the order with status={0}, orderNumber={1}",
                            fluentOrderStatus, order.getCode()));
                }
            }
            catch (final FluentOrderException | FluentErrorException | FluentClientException e) {
                LOG.error(MessageFormat.format(
                        "PRE-ORDER-FULFILMENT-FAILED: Fluent error:  message={0} happened while processing the order,orderNumber={1} and fluentOrderId={2}",
                        e.getMessage(),
                        order.getCode(), fluentOrderId), e);
            }
        }
    }


    private void checkSOHAndRelease(final OrderModel order, final List<String> noStockSKUList)
            throws FluentClientException {
        final List<AbstractOrderEntryModel> entries = order.getEntries();
        if (CollectionUtils.isEmpty(entries)) {
            return;
        }
        final AbstractOrderEntryModel entry = entries.get(0);
        final String skuId = entry.getProduct().getCode();
        if (noStockSKUList.contains(skuId)) {
            LOG.warn(MessageFormat.format(
                    "PRE-ORDER-FULFILMENT-FAILED: Skipped the order with insufficient stock for skuNumber={0}, orderNumber={1}",
                    skuId, order.getCode()));
            return;
        }
        final int skuQty = entry.getQuantity().intValue();
        final int sohQty = getFastlineSoh(skuId);
        if (sohQty >= skuQty) {
            final boolean isOrderReleased = fluentOrderService.releaseOrder(order);
            if (isOrderReleased) {
                LOG.info(MessageFormat.format(
                        "PRE-ORDER-FULFILMENT: Starting PreOrder Release Business Process for order with orderNumber={0}",
                        order.getCode()));
                targetBusinessProcessService.startFluentReleasePreOrderProcess(order);
            }
        }
        else {
            if (sohQty == 0) {
                noStockSKUList.add(skuId);
            }
            LOG.error(MessageFormat.format(
                    "PRE-ORDER-FULFILMENT-FAILED: Attempted to process the order with insufficent stock for skuNumber={0}, orderNumber={1}",
                    skuId,
                    order.getCode()));
        }
    }


    private int getFastlineSoh(final String skuRef) throws FluentClientException {
        int sohQty = 0;
        final String defaultLocation = getDefaultOnlineWarehouse();
        final List<StockDatum> stockData = fluentStockLookupService.lookupStock(Arrays.asList(skuRef),
                null, Arrays.asList(defaultLocation));
        if (CollectionUtils.isNotEmpty(stockData)) {
            final Map<String, Integer> sohQtyMap = stockData.get(0).getStoreSohQty();
            if (sohQtyMap.containsKey(defaultLocation)) {
                sohQty = sohQtyMap.get(defaultLocation).intValue();
            }
        }
        return sohQty;
    }

    private String getDefaultOnlineWarehouse() {
        final WarehouseModel warehouse = targetWarehouseService.getDefaultOnlineWarehouse();
        return warehouse == null ? StringUtils.EMPTY : warehouse.getFluentLocationRef();
    }

    /**
     * @param fluentOrderService
     *            the fluentOrderService to set
     */
    @Required
    public void setFluentOrderService(final FluentOrderService fluentOrderService) {
        this.fluentOrderService = fluentOrderService;
    }

    /**
     * @param targetPreOrderService
     *            the targetPreOrderService to set
     */
    @Required
    public void setTargetPreOrderService(final TargetPreOrderService targetPreOrderService) {
        this.targetPreOrderService = targetPreOrderService;
    }

    /**
     * @param fluentStockLookupService
     *            the fluentStockLookupService to set
     */
    @Required
    public void setFluentStockLookupService(final FluentStockLookupService fluentStockLookupService) {
        this.fluentStockLookupService = fluentStockLookupService;
    }


    /**
     * @param targetWarehouseService
     *            the targetWarehouseService to set
     */
    @Required
    public void setTargetWarehouseService(final TargetWarehouseService targetWarehouseService) {
        this.targetWarehouseService = targetWarehouseService;
    }


    /**
     * @param daysToAdvance
     *            the daysToAdvance to set
     */
    @Required
    public void setDaysToAdvance(final int daysToAdvance) {
        this.daysToAdvance = daysToAdvance;
    }

    /**
     * @param targetBusinessProcessService
     *            the targetBusinessProcessService to set
     */
    @Required
    public void setTargetBusinessProcessService(final TargetBusinessProcessService targetBusinessProcessService) {
        this.targetBusinessProcessService = targetBusinessProcessService;
    }
}

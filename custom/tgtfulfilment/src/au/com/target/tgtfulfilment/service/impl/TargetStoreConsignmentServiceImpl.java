/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTimeComparator;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.dao.TargetPointOfServiceDao;
import au.com.target.tgtfulfilment.dao.TargetConsignmentDao;
import au.com.target.tgtfulfilment.dto.TargetConsignmentPageResult;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;


/**
 * Implementation of services for Instore consignments
 * 
 * @author sbryan6
 *
 */
public class TargetStoreConsignmentServiceImpl implements TargetStoreConsignmentService {

    private TargetConsignmentDao targetConsignmentDao;
    private TargetPointOfServiceDao targetPointOfServiceDao;
    private TargetStoreFulfilmentCapabilitiesService storeFulfilmentCapabilitiesService;

    /**
     * Method to fetch consignments for a store number
     * 
     * @param storeNumber
     * @return ConsignmentModel
     *
     */
    @Override
    public List<TargetConsignmentModel> getConsignmentsForStore(final Integer storeNumber) {
        if (null != storeNumber) {

            return targetConsignmentDao.getConsignmentsForStore(storeNumber);
        }

        return null;
    }

    /**
     * Method to fetch consignments for a store number
     * 
     * @param storeNumber
     * @return targetConsignmentPageResult
     */
    @Override
    public TargetConsignmentPageResult getConsignmentsForStore(final Integer storeNumber, final int offset,
            final int recsPerPage, final int lastXDays, final String searchText, final List<ConsignmentStatus> status,
            final String sortKey, final String sortDirection) {

        if (null != storeNumber) {

            return targetConsignmentDao.getConsignmentsForStore(storeNumber, offset, recsPerPage, lastXDays,
                    searchText, status, sortKey, sortDirection);
        }

        return null;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.service.TargetStoreConsignmentService#isConsignmentAssignedToStore(de.hybris.platform.ordersplitting.model.ConsignmentModel)
     */
    @Override
    public boolean isConsignmentAssignedToAnyStore(final ConsignmentModel consignment) {

        final TargetPointOfServiceModel tpos = getAssignedStoreForConsignment(consignment);

        if (null != tpos && !PointOfServiceTypeEnum.WAREHOUSE.equals(tpos.getType())) {
            return true;
        }

        return false;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.service.TargetStoreConsignmentService#isConsignmentAssignedToAnyNTL(de.hybris.platform.ordersplitting.model.ConsignmentModel)
     */
    @Override
    public boolean isConsignmentAssignedToAnyNTL(final ConsignmentModel consignment) {

        final TargetPointOfServiceModel tpos = getAssignedStoreForConsignment(consignment);

        if (null != tpos && PointOfServiceTypeEnum.WAREHOUSE.equals(tpos.getType())) {
            return true;
        }

        return false;
    }

    @Override
    public boolean isConsignmentAssignedToStore(final ConsignmentModel consignment, final int store) {

        final Integer consStoreNum = getAssignedStoreForConsignmentAsInteger(consignment);

        return (consStoreNum != null && consStoreNum.intValue() == store);
    }

    @Override
    public TargetPointOfServiceModel getAssignedStoreForConsignment(final ConsignmentModel consignment) {

        if (consignment != null) {
            final WarehouseModel warehouse = consignment.getWarehouse();
            return getAssignedStoreForWarehouse(warehouse);
        }

        return null;
    }

    /**
     * Return the point of service for the given warehouse if there is one
     * 
     * @param warehouse
     * @return tpos or null
     */
    @Override
    public TargetPointOfServiceModel getAssignedStoreForWarehouse(final WarehouseModel warehouse) {

        if (warehouse != null) {
            final Collection<PointOfServiceModel> pointsOfService = warehouse.getPointsOfService();
            if (CollectionUtils.isNotEmpty(pointsOfService)) {

                for (final PointOfServiceModel pos : pointsOfService) {

                    if (pos instanceof TargetPointOfServiceModel) {

                        return ((TargetPointOfServiceModel)pos);
                    }
                }
            }
        }

        return null;
    }



    @Override
    public Integer getAssignedStoreForConsignmentAsInteger(final ConsignmentModel consignment) {

        final TargetPointOfServiceModel tpos = getAssignedStoreForConsignment(consignment);
        if (tpos == null) {
            return null;
        }

        return tpos.getStoreNumber();
    }

    @Override
    public List<TargetConsignmentModel> getConsignmentsByCodes(final List<String> consignmentCodes) {
        if (CollectionUtils.isNotEmpty(consignmentCodes)) {

            return targetConsignmentDao.getConsignmentsByCodes(consignmentCodes);
        }

        return null;
    }

    @Override
    public TargetConsignmentModel getConsignmentByCode(final String consignmentCode) {

        if (StringUtils.isNotEmpty(consignmentCode)) {

            final List<TargetConsignmentModel> consList = getConsignmentsByCodes(
                    Collections.singletonList(consignmentCode));
            if (CollectionUtils.isNotEmpty(consList)) {
                return consList.iterator().next();
            }
        }

        return null;
    }

    @Override
    public List<TargetConsignmentModel> getConsignmentsByDayForStore(final TargetPointOfServiceModel store,
            final int noOfDays) {
        Assert.notNull(store, "TargetPointOfService can't be null");

        final List<TargetConsignmentModel> consignments = new ArrayList<>();
        if (noOfDays == 0) {
            consignments.addAll(targetConsignmentDao.getConsignmentsNotProcessedForStore(store));
        }
        consignments.addAll(targetConsignmentDao.getConsignmentsCompletedByDayForStore(store, noOfDays));
        consignments.addAll(targetConsignmentDao.getConsignmentsRejectedByDayForStore(store, noOfDays));
        return consignments;
    }


    @Override
    public Map<ConsignmentStatus, Integer> getConsignmentCountByStatus(final TargetPointOfServiceModel store,
            final int noOfDays) {
        Assert.notNull(store, "TargetPointOfService can't be null");
        final Map<ConsignmentStatus, Integer> combinedConsignmentStatusByCount = new HashMap<>();
        if (noOfDays == 0) {
            combinedConsignmentStatusByCount.putAll(targetConsignmentDao
                    .getCountOfConsignmentsNotProcessedForStoreByStatus(store));
        }
        combinedConsignmentStatusByCount.putAll(targetConsignmentDao
                .getCountOfConsignmentsCompletedByDayForStoreByStatus(store, noOfDays));
        combinedConsignmentStatusByCount.putAll(targetConsignmentDao
                .getCountOfConsignmentsRejectedByDayForStoreByStatus(store, noOfDays));

        return combinedConsignmentStatusByCount;
    }

    @Override
    public Map<TargetPointOfServiceModel, Integer> getPosAndOpenOrdersForStores() {
        final Map<TargetPointOfServiceModel, Integer> posWithOpenOrders = new HashMap<TargetPointOfServiceModel, Integer>();
        final List<TargetPointOfServiceModel> posList = targetPointOfServiceDao.getAllFulfilmentEnabledPOS();
        for (final TargetPointOfServiceModel pos : posList) {
            final StoreFulfilmentCapabilitiesModel fulfilmentCapability = pos.getFulfilmentCapability();
            if (fulfilmentCapability != null) {
                if ((!storeFulfilmentCapabilitiesService.isTargetStoreBlackoutExist(pos))
                        && BooleanUtils.isTrue(fulfilmentCapability.getNotificationsEnabled())
                        && (isStoreInNotificationEnabledPeriod(fulfilmentCapability.getNotificationStartTime(),
                                fulfilmentCapability.getNotificationEndTime()))
                        && (fulfilmentCapability.getMobileNumber() != null)) {
                    final List<TargetConsignmentModel> con = targetConsignmentDao.getOpenConsignmentsForStore(pos);
                    if (CollectionUtils.isNotEmpty(con)) {
                        posWithOpenOrders.put(pos, Integer.valueOf(con.size()));
                    }
                }
            }
        }
        return posWithOpenOrders;
    }

    /**
     * Method to get Not manifested consignments
     * 
     * @param storeNumber
     * @return TargetConsignmentModel List
     */
    @Override
    public List<TargetConsignmentModel> getConsignmentsNotManifestedForStore(final int storeNumber) {
        return targetConsignmentDao
                .getConsignmentsUnmanifestedInStore(ConsignmentStatus.PACKED, storeNumber);
    }

    /**
     * (non-Javadoc)
     * 
     * @see au.com.target.tgtfulfilment.service.TargetStoreConsignmentService#getConsignmentsForAllStore(de.hybris.platform.basecommerce.enums.ConsignmentStatus)
     */
    @Override
    public List<TargetConsignmentModel> getConsignmentsForAllStore(final ConsignmentStatus status) {
        return targetConsignmentDao.getConsignmentsByStatusForAllStore(status);
    }

    /**
     * (non-Javadoc)
     * 
     * @see au.com.target.tgtfulfilment.service.TargetStoreConsignmentService#getConsignmentsForStoreInState(de.hybris.platform.basecommerce.enums.ConsignmentStatus,
     *      java.util.List)
     */
    @Override
    public List<TargetConsignmentModel> getConsignmentsForStoreInState(final List<ConsignmentStatus> statuses,
            final List<String> includedStates) {
        return targetConsignmentDao.getConsignmentsByStatusForStoreInState(statuses, includedStates);
    }

    /**
     * (non-Javadoc)
     * 
     * @see au.com.target.tgtfulfilment.service.TargetStoreConsignmentService#getConsignmentsForStoreNotInState(de.hybris.platform.basecommerce.enums.ConsignmentStatus,
     *      java.util.List)
     */
    @Override
    public List<TargetConsignmentModel> getConsignmentsForStoreNotInState(final List<ConsignmentStatus> statuses,
            final List<String> excludedStates) {
        return targetConsignmentDao.getConsignmentsByStatusForStoreNotInState(statuses, excludedStates);
    }


    @Override
    public String getOrderCodeFromConsignment(final TargetConsignmentModel targetConsignmentModel) {
        if (targetConsignmentModel != null && targetConsignmentModel.getOrder() != null) {
            return targetConsignmentModel.getOrder().getCode();
        }
        return StringUtils.EMPTY;
    }

    @Override
    public String getDeliverToStateFromConsignment(final TargetConsignmentModel targetConsignmentModel) {
        String deliverToState = StringUtils.EMPTY;
        if (targetConsignmentModel != null && targetConsignmentModel.getShippingAddress() != null) {
            deliverToState = targetConsignmentModel.getShippingAddress().getDistrict();
        }
        return deliverToState;
    }

    @Override
    public String getDeliverToStoreNumberFromConsignment(final TargetConsignmentModel targetConsignmentModel) {
        String deliverToStoreNumber = StringUtils.EMPTY;
        if (targetConsignmentModel != null && targetConsignmentModel.getOrder() != null
                && targetConsignmentModel.getOrder().getCncStoreNumber() != null) {
            deliverToStoreNumber = targetConsignmentModel.getOrder().getCncStoreNumber().toString();
        }
        return deliverToStoreNumber;
    }

    @Override
    public String getDeliverFromStateFromConsignment(final TargetConsignmentModel targetConsignmentModel) {
        String deliverFromState = StringUtils.EMPTY;
        if (targetConsignmentModel != null && targetConsignmentModel.getWarehouse() != null) {
            final TargetPointOfServiceModel tpos = getAssignedStoreForWarehouse(targetConsignmentModel.getWarehouse());
            if (tpos != null && tpos.getAddress() != null) {
                deliverFromState = tpos.getAddress().getDistrict();
            }

        }
        return deliverFromState;
    }

    @Override
    public List<TargetConsignmentModel> getConsignmentsOfMultiplesStatusesForAllStores(
            final List<ConsignmentStatus> statuses) {
        return targetConsignmentDao.getConsignmentsOfMultiplesStatusesForAllStores(statuses);
    }

    /**
     * Returns true for below conditions 1)current date in between startDate and EndDate. 2)start date null and current
     * date is before end Date. 3)end date null and current date is after start date.
     * 
     * @param startDate
     * @param endDate
     * @return isNotificationPeriod
     */
    protected boolean isStoreInNotificationEnabledPeriod(final Date startDate, final Date endDate) {
        if (startDate == null && endDate == null) {
            return false;
        }
        final Date currentDate = getCurrentDate();
        final DateTimeComparator timeComparator = DateTimeComparator.getTimeOnlyInstance();

        return ((timeComparator.compare(currentDate, startDate) >= 0)
                && (timeComparator.compare(currentDate, endDate) <= 0));
    }

    /**
     * Gets the current date.
     *
     * @return the current date
     */
    protected Date getCurrentDate() {
        return new Date();
    }

    /**
     * @param targetConsignmentDao
     *            the targetConsignmentDao to set
     */
    @Required
    public void setTargetConsignmentDao(final TargetConsignmentDao targetConsignmentDao) {
        this.targetConsignmentDao = targetConsignmentDao;
    }

    /**
     * @param targetPointOfServiceDao
     *            the targetPointOfServiceDao to set
     */
    @Required
    public void setTargetPointOfServiceDao(final TargetPointOfServiceDao targetPointOfServiceDao) {
        this.targetPointOfServiceDao = targetPointOfServiceDao;
    }

    /**
     * @param storeFulfilmentCapabilitiesService
     *            the storeFulfilmentCapabilitiesService to set
     */
    @Required
    public void setStoreFulfilmentCapabilitiesService(
            final TargetStoreFulfilmentCapabilitiesService storeFulfilmentCapabilitiesService) {
        this.storeFulfilmentCapabilitiesService = storeFulfilmentCapabilitiesService;
    }
}

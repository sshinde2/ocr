/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;


import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.category.TargetCategoryService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfulfilment.dao.TargetConsignmentDao;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.model.ProductExclusionsModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;
import au.com.target.tgtfulfilment.util.TgtFulfilmentUtil;
import au.com.target.tgtutility.util.TargetProductUtils;


/**
 * TargetStoreFulfilmentCapabilitiesService Implementation
 * 
 * @author jjayawa1
 *
 */
public class TargetStoreFulfilmentCapabilitiesServiceImpl implements TargetStoreFulfilmentCapabilitiesService {

    private static final Logger LOG = Logger.getLogger(TargetStoreFulfilmentCapabilitiesServiceImpl.class);
    private double defaultMaxWeightAllowedForDelivery;
    private TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService;
    private TargetConsignmentDao targetConsignmentDao;
    private OEGParameterHelper oegParameterHelper;
    private TargetCategoryService categoryService;

    @Override
    public boolean isStoreEnabledForFulfilment(final TargetPointOfServiceModel tpos) {

        final StoreFulfilmentCapabilitiesModel storeCapabilitiesModel = getStoreFulfilmentCapabilities(tpos);
        if (storeCapabilitiesModel == null) {
            return false;
        }

        final Boolean enabled = storeCapabilitiesModel.getEnabled();
        return (enabled != null && enabled.booleanValue());
    }

    @Override
    public boolean isDeliveryModeAssociatedToStore(final TargetPointOfServiceModel tpos,
            final DeliveryModeModel deliveryMode) {

        final StoreFulfilmentCapabilitiesModel storeCapabilitiesModel = getStoreFulfilmentCapabilities(tpos);
        if (storeCapabilitiesModel == null) {
            return false;
        }

        final Set<TargetZoneDeliveryModeModel> zoneDeliveryModes = storeCapabilitiesModel.getDeliveryModes();

        if (CollectionUtils.isNotEmpty(zoneDeliveryModes)) {
            for (final TargetZoneDeliveryModeModel zoneDeliveryMode : zoneDeliveryModes) {
                if (deliveryMode.getCode().equals(zoneDeliveryMode.getCode())) {
                    return true;
                }
            }
        }


        return false;
    }

    @Override
    public boolean isTargetStoreBlackoutExist(final TargetPointOfServiceModel tpos) {

        final StoreFulfilmentCapabilitiesModel storeCapabilitiesModel = getStoreFulfilmentCapabilities(tpos);
        if (storeCapabilitiesModel == null) {
            return false;
        }

        return !storeCapabilitiesModel.getCanBeUsed().booleanValue();
    }

    @Override
    public Double getMaxAllowedProductWeightForDelivery(final TargetPointOfServiceModel tpos) {
        final StoreFulfilmentCapabilitiesModel storeCapabilitiesModel = getStoreFulfilmentCapabilities(tpos);
        if (null == storeCapabilitiesModel) {
            return null;
        }
        final Double maxAllowedWeight = storeCapabilitiesModel.getMaxAllowedProductWeightForDelivery();
        if (null == maxAllowedWeight) {
            return Double.valueOf(defaultMaxWeightAllowedForDelivery);
        }
        return maxAllowedWeight;
    }

    @Override
    public Set<ProductTypeModel> getRestrictedProductTypes(final TargetPointOfServiceModel tpos) {
        final StoreFulfilmentCapabilitiesModel storeCapabilitiesModel = getStoreFulfilmentCapabilities(tpos);
        if (null == storeCapabilitiesModel) {
            return null;
        }
        return storeCapabilitiesModel.getProductTypes();
    }

    @Override
    public boolean isMaxNumberOfOrdersExceeded(final TargetPointOfServiceModel tpos) {
        final StoreFulfilmentCapabilitiesModel storeCapabilitiesModel = getStoreFulfilmentCapabilities(tpos);
        if (null == storeCapabilitiesModel) {
            LOG.warn("No Store capablity Information found");
            return false;
        }
        if (null == tpos.getStoreNumber() || null == storeCapabilitiesModel.getMaxConsignmentsForDeliveryPerDay()) {
            LOG.warn("StoreNumber or StoreOrderLimit not set");
            return false;
        }
        final int maxConsignmentsPerDay = storeCapabilitiesModel.getMaxConsignmentsForDeliveryPerDay().intValue();
        if (maxConsignmentsPerDay == 0) {
            //this should turn the rule off.
            return false;
        }
        final String maxOrderPeriodStartTime = getMaxOrderPeriodStartTime();
        Date queryDate = null;
        if (StringUtils.isNotBlank(maxOrderPeriodStartTime)) {
            queryDate = TgtFulfilmentUtil.getValidDateForTime(maxOrderPeriodStartTime);
        }
        if (queryDate == null) {
            LOG.warn("maxOrderPeriodStartTime not set");
            return false;
        }
        if (maxConsignmentsPerDay <= targetConsignmentDao.getConsignmentCountForStoreByMaxCutoffPeriod(
                tpos.getStoreNumber(), queryDate)) {
            return true;
        }

        return false;
    }

    protected String getMaxOrderPeriodStartTime() {
        final GlobalStoreFulfilmentCapabilitiesModel globalStoreFulfilmentCapabilitiesModel = targetGlobalStoreFulfilmentService
                .getGlobalStoreFulfilmentCapabilites();
        String maxOrderPeriodStartTime = null;
        if (globalStoreFulfilmentCapabilitiesModel != null) {
            maxOrderPeriodStartTime = globalStoreFulfilmentCapabilitiesModel
                    .getMaxOrderPeriodStartTime();
        }
        return maxOrderPeriodStartTime;
    }



    private StoreFulfilmentCapabilitiesModel getStoreFulfilmentCapabilities(final TargetPointOfServiceModel tpos) {

        if (tpos == null) {
            return null;
        }
        return tpos.getFulfilmentCapability();
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService#isOrderContainingProductsFromExcludedCategories(de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup, java.util.List)
     */
    @Override
    public boolean isOrderContainingProductsFromExcludedCategories(final OrderEntryGroup orderEntryGroup,
            final List<CategoryModel> excludeCategories) {
        final String orderCode = oegParameterHelper.getOrderCode(orderEntryGroup);

        Assert.notEmpty(orderEntryGroup, "orderEntryGroup cannot be empty " + orderCode);
        Assert.notEmpty(excludeCategories, "excludeCategories cannot be empty " + orderCode);

        for (final AbstractOrderEntryModel orderEntryModel : orderEntryGroup) {

            Assert.notNull(orderEntryModel, "No orderEntryModel found. Object returned was null : " + orderCode);

            final ProductModel productModel = orderEntryModel.getProduct();

            Assert.isTrue(productModel instanceof AbstractTargetVariantProductModel,
                    "No TargetVariantProductModel found. Object returned was null : " + orderCode);

            final AbstractTargetVariantProductModel variantProductModel = (AbstractTargetVariantProductModel)productModel;
            final TargetProductModel targetProduct = TargetProductUtils.getBaseTargetProduct(variantProductModel);

            Assert.notNull(targetProduct,
                    "No TargetProductModel found for VariantProduct= " + variantProductModel.getCode());

            final Set<CategoryModel> allSuperCategories = categoryService
                    .getAllSuperCategoriesForProduct(targetProduct);
            if (CollectionUtils.containsAny(excludeCategories, allSuperCategories)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isProductInExclusionList(final OrderEntryGroup oeg,
            final Set<ProductExclusionsModel> productExclusionList) {
        if (CollectionUtils.isNotEmpty(productExclusionList)) {
            for (final AbstractOrderEntryModel orderEntry : oeg) {
                if (orderEntry.getProduct() instanceof VariantProductModel) {
                    ProductModel product = ((VariantProductModel)orderEntry.getProduct()).getBaseProduct();
                    while (product instanceof VariantProductModel) {
                        product = ((VariantProductModel)product).getBaseProduct();
                    }
                    if (containsProduct(productExclusionList, product)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public Long getBufferStock(final TargetPointOfServiceModel tpos) {
        final StoreFulfilmentCapabilitiesModel storeFulfilmentCapabilities = getStoreFulfilmentCapabilities(tpos);
        if (null != storeFulfilmentCapabilities) {
            return storeFulfilmentCapabilities.getBufferStock();
        }
        return Long.valueOf(0);
    }

    /**
     * @param productExclusions
     * @param product
     * @return True/False
     */
    protected boolean containsProduct(final Set<ProductExclusionsModel> productExclusions,
            final ProductModel product) {
        for (final ProductExclusionsModel exclusionProduct : productExclusions) {
            if (null != exclusionProduct.getProduct()
                    && exclusionProduct.getProduct().getCode().equals(product.getCode())) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param defaultMaxWeightAllowedForDelivery
     *            the defaultMaxWeightAllowedForDelivery to set
     */
    @Required
    public void setDefaultMaxWeightAllowedForDelivery(final double defaultMaxWeightAllowedForDelivery) {
        this.defaultMaxWeightAllowedForDelivery = defaultMaxWeightAllowedForDelivery;
    }

    /**
     * @param targetGlobalStoreFulfilmentService
     *            the targetGlobalStoreFulfilmentService to set
     */
    @Required
    public void setTargetGlobalStoreFulfilmentService(
            final TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService) {
        this.targetGlobalStoreFulfilmentService = targetGlobalStoreFulfilmentService;
    }

    /**
     * @param targetConsignmentDao
     *            the targetConsignmentDao to set
     */
    @Required
    public void setTargetConsignmentDao(final TargetConsignmentDao targetConsignmentDao) {
        this.targetConsignmentDao = targetConsignmentDao;
    }

    /**
     * @param oegParameterHelper
     *            the oegParameterHelper to set
     */
    @Required
    public void setOegParameterHelper(final OEGParameterHelper oegParameterHelper) {
        this.oegParameterHelper = oegParameterHelper;
    }

    /**
     * @param categoryService
     *            the categoryService to set
     */
    @Required
    public void setCategoryService(final TargetCategoryService categoryService) {
        this.categoryService = categoryService;
    }

}
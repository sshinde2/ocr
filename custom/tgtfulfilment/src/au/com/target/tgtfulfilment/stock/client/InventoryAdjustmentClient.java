/**
 * 
 */
package au.com.target.tgtfulfilment.stock.client;

import java.util.List;

import au.com.target.tgtfulfilment.inventory.dto.InventoryAdjustmentData;
import au.com.target.tgtwebcore.exception.TargetIntegrationException;



/**
 * @author pthoma20
 * 
 */
public interface InventoryAdjustmentClient {

    /**
     * Publishes events to webmethods when an inventory Adjustment is required. Example - when a store other than the
     * default Online warehouse fulfills the order.
     * 
     * @param inventoryAdjustmentDataList
     * @return boolean
     * @throws TargetIntegrationException
     */
    boolean sendInventoryAdjustment(final List<InventoryAdjustmentData> inventoryAdjustmentDataList)
            throws TargetIntegrationException;

}
/**
 * 
 */
package au.com.target.tgtfulfilment.stock.client.impl;

import java.util.List;

import org.apache.log4j.Logger;

import au.com.target.tgtfulfilment.inventory.dto.InventoryAdjustmentData;
import au.com.target.tgtfulfilment.stock.client.InventoryAdjustmentClient;
import au.com.target.tgtwebcore.exception.TargetIntegrationException;


/**
 * @author pthoma20
 * 
 */
public class DefaultInventoryAdjustmentClientImpl implements InventoryAdjustmentClient {

    private static final Logger LOG = Logger.getLogger(DefaultInventoryAdjustmentClientImpl.class);

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.stock.client.TargetInventoryAdjustmentClient#sendInventoryAdjustment(java.lang.String)
     */
    @Override
    public boolean sendInventoryAdjustment(final List<InventoryAdjustmentData> inventoryAdjustmentDataList)
            throws TargetIntegrationException {
        LOG.info("Called the default noop implementation.");
        return false;
    }


}

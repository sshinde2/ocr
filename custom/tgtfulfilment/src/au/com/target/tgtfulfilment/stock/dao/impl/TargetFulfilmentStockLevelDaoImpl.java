/**
 * 
 */
package au.com.target.tgtfulfilment.stock.dao.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.stock.impl.TargetStockLevelDaoImpl;
import au.com.target.tgtfulfilment.stock.dao.TargetFulfilmentStockLevelDao;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;


/**
 * @author rahul
 */
public class TargetFulfilmentStockLevelDaoImpl extends TargetStockLevelDaoImpl implements TargetFulfilmentStockLevelDao {

    // @formatter:off
    private static final String CALCULATE_RESERVED_STOCK_QUERY = "SELECT SUM(QTY) FROM ( "
            + "{{ "
            +       "SELECT ROUND({OE." + OrderEntryModel.QUANTITY + "}, 0) AS QTY FROM {" + OrderEntryModel._TYPECODE + " AS OE "
            +       "JOIN " + OrderModel._TYPECODE + " AS O ON {OE." + OrderEntryModel.ORDER + "} = {O." + OrderModel.PK + "} "
            +       "JOIN " + PurchaseOptionConfigModel._TYPECODE + " AS POC ON {O." + OrderModel.PURCHASEOPTIONCONFIG + "} = {POC." + PurchaseOptionConfigModel.PK + "} "
            +       "JOIN " + OrderStatus._TYPECODE + " AS OS ON {O."+ OrderModel.STATUS + "} = {OS.PK}} "
            +       "WHERE {O." + OrderModel.ORIGINALVERSION + "} IS NULL "
            +       "AND {POC." + PurchaseOptionConfigModel.WAREHOUSE + "} = ?warehouse "
            +       "AND {OE." + OrderEntryModel.PRODUCT + "} = ?product "
            +       "AND {O." + OrderModel.STATUS + "} IN (?orderStatusesWithReservedStock) "
            +" }} "
            +" UNION ALL"
            +" {{ "
            +       "SELECT ROUND({OE." + OrderEntryModel.QUANTITY + "}, 0) AS QTY FROM {" + OrderEntryModel._TYPECODE + " AS OE "
            +       "JOIN " + OrderModel._TYPECODE + " AS O ON {OE." + OrderEntryModel.ORDER + "} = {O." + OrderModel.PK + "} "
            +       "JOIN " + PurchaseOptionConfigModel._TYPECODE + " AS POC ON {O." + OrderModel.PURCHASEOPTIONCONFIG + "} = {POC." + PurchaseOptionConfigModel.PK + "} "
            +       "JOIN " + OrderStatus._TYPECODE + " AS OS ON {O." + OrderModel.STATUS + "} = {OS.PK} "
            +       "JOIN " + ConsignmentModel._TYPECODE + " AS C ON {C." + ConsignmentModel.ORDER + "} = {O."+ OrderModel.PK + "} "
            +       "JOIN " + ConsignmentStatus._TYPECODE + " AS CS ON {C." + ConsignmentModel.STATUS + "} = {CS.PK}} "
            +       "WHERE {O." + OrderModel.ORIGINALVERSION + "} IS NULL "
            +       "AND {POC." + PurchaseOptionConfigModel.WAREHOUSE + "} = ?warehouse "
            +       "AND {OE." + OrderEntryModel.PRODUCT + "} = ?product "
            +       "AND {O." + OrderModel.STATUS + "} = ?inProgressOrderStatus "
            +       "AND {C."+ ConsignmentModel.STATUS + "} IN (?consignmentStatusesWithReservedStock) "
            + "}} "
            +" UNION ALL"
            +" {{ "
            +       "SELECT {SL." + StockLevelModel.RESERVED + "}*-1 AS QTY FROM {" + StockLevelModel._TYPECODE + " AS SL} "
            +       "WHERE {SL." + StockLevelModel.WAREHOUSE + "} = ?warehouse "
            +       "AND {SL." + StockLevelModel.PRODUCTCODE + "} = ?productCode "
            + "}} "
            + ") tbl";
    // @formatter:on

    private List<ConsignmentStatus> consignmentStatusesWithReservedStock;
    private List<OrderStatus> orderStatusesWithReservedStock;

    @Override
    public long calculateReservedStockAdjustment(final AbstractTargetVariantProductModel product,
            final WarehouseModel warehouse) {
        final FlexibleSearchQuery flexiSearch = new FlexibleSearchQuery(CALCULATE_RESERVED_STOCK_QUERY);
        flexiSearch.addQueryParameter("product", product);
        flexiSearch.addQueryParameter("productCode", product.getCode());
        flexiSearch.addQueryParameter("warehouse", warehouse);
        flexiSearch.addQueryParameter("inProgressOrderStatus", OrderStatus.INPROGRESS);
        flexiSearch.addQueryParameter("consignmentStatusesWithReservedStock", consignmentStatusesWithReservedStock);
        flexiSearch.addQueryParameter("orderStatusesWithReservedStock", orderStatusesWithReservedStock);
        flexiSearch.setResultClassList(Collections.singletonList(Long.class));
        final SearchResult<Long> searchResult = getFlexibleSearchService().search(flexiSearch);

        long result = 0L;
        if (searchResult.getCount() > 0) {
            final Long firstResult = searchResult.getResult().get(0);
            if (firstResult != null) {
                result = firstResult.longValue();
            }
        }
        return result;
    }

    /**
     * @param consignmentStatusesWithReservedStock
     *            the consignmentStatusesWithReservedStock to set
     */
    @Required
    public void setConsignmentStatusesWithReservedStock(
            final List<ConsignmentStatus> consignmentStatusesWithReservedStock) {
        this.consignmentStatusesWithReservedStock = consignmentStatusesWithReservedStock;
    }

    /**
     * @param orderStatusesWithReservedStock
     *            the orderStatusesWithReservedStock to set
     */
    @Required
    public void setOrderStatusesWithReservedStock(final List<OrderStatus> orderStatusesWithReservedStock) {
        this.orderStatusesWithReservedStock = orderStatusesWithReservedStock;
    }
}

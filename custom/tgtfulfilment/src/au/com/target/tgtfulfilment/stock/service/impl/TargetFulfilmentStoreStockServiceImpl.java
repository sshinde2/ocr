/**
 * 
 */
package au.com.target.tgtfulfilment.stock.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.JaloSession;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemResponseDto;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;
import au.com.target.tgtsale.stock.dto.response.StockUpdateProductResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateStoreResponseDto;
import au.com.target.tgtsale.stock.service.impl.TargetStoreStockServiceImpl;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * @author Vivek
 *
 */
public class TargetFulfilmentStoreStockServiceImpl extends TargetStoreStockServiceImpl {

    private static final Logger LOG = Logger.getLogger(TargetFulfilmentStoreStockServiceImpl.class);

    private TargetStoreFulfilmentCapabilitiesService targetStoreFulfilmentCapabilitiesService;

    @Override
    protected boolean isProductInStock(final StockUpdateResponseDto stockUpdateResponse,
            final List<AbstractOrderEntryModel> orderEntries,
            final Integer storeNumber, final String orderCode) {

        final List<StockUpdateStoreResponseDto> storeResponses = stockUpdateResponse.getStockUpdateStoreResponseDtos();

        //Response object is empty.
        if (CollectionUtils.isEmpty(storeResponses)) {
            logInfoMessage("Store stock responses is empty", storeNumber, orderCode);
            return false;
        }
        List<StockUpdateProductResponseDto> productStoreStocks = null;

        //get the response object for the store.
        for (final StockUpdateStoreResponseDto storeResponse : storeResponses) {
            final String storeNo = storeResponse.getStoreNumber();
            if (null != storeNumber) {
                if (storeNo.equals(storeNumber.toString())) {
                    productStoreStocks = storeResponse.getStockUpdateProductResponseDtos();
                    break;
                }
            }
        }

        if (CollectionUtils.isEmpty(productStoreStocks)) {
            logInfoMessage("Could not find stock response for store", storeNumber, orderCode);
            return false;
        }

        //check if the product exists in the store for the order
        if (!checkProductExistsInStore(orderEntries, productStoreStocks)) {
            logInfoMessage("Product is not in store stock response", storeNumber, orderCode);
            return false;
        }

        TargetPointOfServiceModel pointOfService = null;
        try {
            pointOfService = targetPointOfServiceService.getPOSByStoreNumber(storeNumber);
        }
        catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
            logInfoMessage("Can't find POS", storeNumber, orderCode);
            return false;
        }

        final Long storeStockBuffer = targetStoreFulfilmentCapabilitiesService.getBufferStock(pointOfService);

        for (final AbstractOrderEntryModel oem : orderEntries) {
            final ProductModel product = oem.getProduct();
            if (null == product || null == product.getCode()) {
                logInfoMessage("Product/Product Code is NULL", storeNumber, orderCode);
                return false;
            }

            for (final StockUpdateProductResponseDto productStoreStock : productStoreStocks) {
                if (null != productStoreStock) {

                    if (product.getCode().equals(productStoreStock.getItemcode())) {

                        final Long productSOH = Long.valueOf(productStoreStock.getSoh());

                        if (null == productSOH) {
                            logInfoMessage("Stock on hand value is null for itemCode=" + product.getCode(),
                                    storeNumber, orderCode);
                            return false;
                        }

                        long actualProductSOH = productSOH.longValue();

                        if (null != storeStockBuffer) {
                            actualProductSOH -= storeStockBuffer.longValue();
                        }

                        final int pendingQuantity = targetPointOfServiceService
                                .getFulfilmentPendingQuantity(storeNumber, product);

                        final long orderQuantity = oem.getQuantity().longValue();

                        final boolean outOfStock = (orderQuantity > (actualProductSOH - pendingQuantity));

                        logInfoMessage("SOH lookup for product: product=" + product.getCode()
                                + ", SOHatStore=" + productSOH.longValue()
                                + ", bufferStockSet=" + storeStockBuffer
                                + ", pendingQuantity=" + pendingQuantity
                                + ", resultSOH=" + actualProductSOH
                                + ", orderQty=" + orderQuantity
                                + ", outOfStock=" + outOfStock, storeNumber, orderCode);

                        if (outOfStock) {

                            return false;
                        }
                        break;
                    }
                }
            }
        }
        return true;
    }


    @Override
    protected boolean isProductInStock(final StockVisibilityItemLookupResponseDto stockVisibilityItemLookupResponseDto,
            final List<AbstractOrderEntryModel> orderEntries,
            final Integer storeNumber, final String orderCode) {

        final List<StockVisibilityItemResponseDto> stockVisibilityItemResponseDtoList = stockVisibilityItemLookupResponseDto
                .getItems();

        //Response object is empty.
        if (CollectionUtils.isEmpty(stockVisibilityItemResponseDtoList)) {
            logInfoMessage("Store stock responses is empty", storeNumber, orderCode);
            return false;
        }

        //get the response object for the store.
        TargetPointOfServiceModel pointOfService = null;
        try {
            pointOfService = targetPointOfServiceService.getPOSByStoreNumber(storeNumber);
        }
        catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
            logInfoMessage("Can't find POS", storeNumber, orderCode);
            return false;
        }

        //get Stock Buffer at Store level
        final Long storeStockBuffer = targetStoreFulfilmentCapabilitiesService.getBufferStock(pointOfService);

        return evaluateIsProductInStock(orderEntries, storeNumber, orderCode, stockVisibilityItemResponseDtoList,
                storeStockBuffer);

    }

    private boolean evaluateIsProductInStock(final List<AbstractOrderEntryModel> orderEntries,
            final Integer storeNumber,
            final String orderCode, final List<StockVisibilityItemResponseDto> stockVisibilityItemResponseDtoList,
            final Long storeStockBuffer) {
        //Iterate over the order entries
        for (final AbstractOrderEntryModel oem : orderEntries) {
            final ProductModel product = oem.getProduct();
            if (null == product || null == product.getCode()) {
                logInfoMessage("Product/Product Code is NULL", storeNumber, orderCode);
                return false;
            }

            boolean outOfStock = true;
            long actualProductSOH = 0;
            int pendingQuantity = 0;
            long orderQuantity = 0;

            //iterate over the response
            for (final StockVisibilityItemResponseDto stockVisibilityItemResponseDto : stockVisibilityItemResponseDtoList) {

                if (!doesTheSohResponseMatchProductAndStore(stockVisibilityItemResponseDto, storeNumber,
                        product.getCode())) {
                    continue;
                }
                if (stockVisibilityItemResponseDto.getSoh() == null
                        || Long.parseLong(stockVisibilityItemResponseDto.getSoh()) <= 0) {
                    actualProductSOH = 0;
                }
                else {
                    actualProductSOH = Long.parseLong(stockVisibilityItemResponseDto.getSoh());
                }
                pendingQuantity = targetPointOfServiceService
                        .getFulfilmentPendingQuantity(storeNumber, product);
                if (null != storeStockBuffer) {
                    actualProductSOH -= storeStockBuffer.longValue();
                }

                orderQuantity = oem.getQuantity().longValue();
                outOfStock = orderQuantity > (actualProductSOH - pendingQuantity);


            }
            logInfoMessage("SOH lookup for product: product=" + product.getCode()
                    + ", SOHatStore=" + actualProductSOH
                    + ", bufferStockSet=" + storeStockBuffer
                    + ", pendingQuantity=" + pendingQuantity
                    + ", resultSOH=" + actualProductSOH
                    + ", orderQty=" + orderQuantity
                    + ", outOfStock=" + outOfStock, storeNumber, orderCode);
            if (outOfStock) {
                return false;
            }

        }
        return true;
    }

    private boolean doesTheSohResponseMatchProductAndStore(
            final StockVisibilityItemResponseDto stockVisibilityItemResponseDto, final Integer storeNumber,
            final String productCode) {
        if (null == storeNumber || !StringUtils.equalsIgnoreCase(storeNumber.toString(),
                stockVisibilityItemResponseDto.getStoreNumber())) {
            return false;
        }
        if (!StringUtils.equalsIgnoreCase(productCode, stockVisibilityItemResponseDto.getCode())) {
            return false;
        }
        return true;
    }

    /**
     * fetch products soh in one store, consider the butter and store pending
     */
    @Override
    public Map<String, Long> fetchProductsSohInStore(final TargetPointOfServiceModel tpos,
            final List<String> products) {
        final Map<String, Long> productsSoh = new HashMap<>();
        if (tpos == null) {
            LOG.error("Targetpointofservice cannot be null when fetching the soh");
            return productsSoh;
        }
        final Integer storeNumber = tpos.getStoreNumber();
        final List<String> storeNumbers = new ArrayList<>();
        storeNumbers.add(String.valueOf(storeNumber));
        final List<StockVisibilityItemResponseDto> stockVisibilityItemResponseDtoList = this
                .querySohUsingCache(storeNumbers, products);
        return this.createSohMapData(products, stockVisibilityItemResponseDtoList, tpos);
    }

    @Override
    public Table<Integer, String, Long> fetchProductsSohInStore(final List<TargetPointOfServiceModel> tposList,
            final List<ProductModel> products) {
        if (CollectionUtils.isEmpty(tposList)) {
            LOG.error("Targetpointofservice cannot be null when fetching the soh");
            return HashBasedTable.create();
        }
        final List<String> storeNumbers = new ArrayList<>();
        for (final TargetPointOfServiceModel tpos : tposList) {
            final Integer storeNumber = tpos.getStoreNumber();
            storeNumbers.add(String.valueOf(storeNumber));
        }
        final List<String> productCodes = getProductCodeFromProductModel(products);

        final List<StockVisibilityItemResponseDto> stockVisibilityItemResponseDtoList = this
                .querySohUsingCache(storeNumbers, productCodes);
        final Map<Integer, Long> storeStockBufferMap = getStoreStockBuffer(tposList);

        String jaloSessionId = null;
        if (JaloSession.hasCurrentSession()) {
            final JaloSession jaloSession = JaloSession.getCurrentSession();
            jaloSessionId = jaloSession.getSessionID();
        }
        LOG.info(MessageFormat.format("Flexi search to fetch the consignment pending quantities starts, session={0}",
                jaloSessionId));
        final Table<Integer, String, Long> pendingQuantityTable = targetPointOfServiceService
                .getFulfilmentPendingQuantityByProductCodeForStores(storeNumbers, products);
        LOG.info(MessageFormat.format("Flexi search to fetch the consignment pending quantities ends, session={0}",
                jaloSessionId));
        return this.createSohTableData(productCodes, stockVisibilityItemResponseDtoList, storeStockBufferMap,
                pendingQuantityTable);
    }

    /**
     * @param products
     * @return list of product code;
     */
    private List<String> getProductCodeFromProductModel(final List<ProductModel> products) {
        final List<String> productCodes = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(products)) {
            for (final ProductModel product : products) {
                productCodes.add(product.getCode());
            }
        }
        return productCodes;
    }


    /**
     * 
     * @param tposList
     * @return map of store stock buffer
     */
    private Map<Integer, Long> getStoreStockBuffer(final List<TargetPointOfServiceModel> tposList) {
        final Map<Integer, Long> storeStockBufferMap = new HashMap<>();
        for (final TargetPointOfServiceModel tpos : tposList) {
            final Long storeStockBuffer = targetStoreFulfilmentCapabilitiesService.getBufferStock(tpos);
            storeStockBufferMap.put(tpos.getStoreNumber(), storeStockBuffer);
        }
        return storeStockBufferMap;
    }

    private List<StockVisibilityItemResponseDto> querySohUsingCache(final List<String> storeNumbers,
            final List<String> products) {
        final StockVisibilityItemLookupResponseDto stockVisibilityItemLookupResponseDto;
        List<StockVisibilityItemResponseDto> stockVisibilityItemResponseDtoList = new ArrayList<>();
        try {
            stockVisibilityItemLookupResponseDto = lookupStockForItemsInStores(storeNumbers, products);
        }
        catch (final Exception e) {
            LOG.error(SplunkLogFormatter.formatMessage("Error invoking Stock service ",
                    TgtutilityConstants.ErrorCode.ERR_TGTSALE), e);
            return stockVisibilityItemResponseDtoList;
        }

        stockVisibilityItemResponseDtoList = stockVisibilityItemLookupResponseDto
                .getItems();

        //Response object is empty.
        if (CollectionUtils.isEmpty(stockVisibilityItemResponseDtoList)) {
            LOG.info("Store stock responses is empty");
        }
        return stockVisibilityItemResponseDtoList;
    }

    /**
     * 
     * @param products
     * @param stockVisibilityItemResponseDtoList
     * @param storeStockBufferMap
     * @param pendingQuantityTable
     * @return table store and product stock.
     */
    private Table<Integer, String, Long> createSohTableData(final List<String> products,
            final List<StockVisibilityItemResponseDto> stockVisibilityItemResponseDtoList,
            final Map<Integer, Long> storeStockBufferMap, final Table<Integer, String, Long> pendingQuantityTable) {
        if (CollectionUtils.isEmpty(stockVisibilityItemResponseDtoList) || CollectionUtils.isEmpty(products)
                || null == pendingQuantityTable) {
            return null;
        }
        //iterate over the response
        final Table<Integer, String, Long> sohTable = HashBasedTable.create();
        for (final StockVisibilityItemResponseDto stockVisibilityItemResponseDto : stockVisibilityItemResponseDtoList) {
            final Integer storeNumber = Integer.valueOf(stockVisibilityItemResponseDto.getStoreNumber());
            final String product = stockVisibilityItemResponseDto.getCode();
            final Long storeStockBuffer = storeStockBufferMap.get(storeNumber);
            long actualProductSOH;
            final Long pendingQuantity = pendingQuantityTable.get(storeNumber, product);
            final int pendingQuantityInt = pendingQuantity == null ? 0 : pendingQuantity.intValue();

            if (stockVisibilityItemResponseDto.getSoh() == null
                    || Long.parseLong(stockVisibilityItemResponseDto.getSoh()) <= 0) {
                actualProductSOH = 0;
            }
            else {
                actualProductSOH = Long.parseLong(stockVisibilityItemResponseDto.getSoh());
            }
            if (null != storeStockBuffer) {
                actualProductSOH -= storeStockBuffer.longValue();
            }
            actualProductSOH -= pendingQuantityInt;

            logSohInfoMessage("SOH lookup for product: product=" + product
                    + ", SOHatStore=" + actualProductSOH
                    + ", bufferStockSet=" + storeStockBuffer
                    + ", pendingQuantity=" + pendingQuantityInt
                    + ", resultSOH=" + actualProductSOH, storeNumber);

            sohTable.put(storeNumber, product, Long.valueOf(actualProductSOH));
        }
        return sohTable;
    }


    private Map<String, Long> createSohMapData(final List<String> products,
            final List<StockVisibilityItemResponseDto> stockVisibilityItemResponseDtoList,
            final TargetPointOfServiceModel tpos) {
        final Map<String, Long> sohMap = new HashMap<>();
        if (CollectionUtils.isEmpty(stockVisibilityItemResponseDtoList) || CollectionUtils.isEmpty(products)) {
            return sohMap;
        }
        //get Stock Buffer at Store level
        final Long storeStockBuffer = targetStoreFulfilmentCapabilitiesService.getBufferStock(tpos);
        final Integer storeNumber = tpos.getStoreNumber();
        //Iterate over the order entries
        for (final String product : products) {
            sohMap.put(product, Long.valueOf(0));
            long actualProductSOH = 0;
            int pendingQuantity = 0;
            //iterate over the response
            for (final StockVisibilityItemResponseDto stockVisibilityItemResponseDto : stockVisibilityItemResponseDtoList) {

                if (!doesTheSohResponseMatchProductAndStore(stockVisibilityItemResponseDto, storeNumber, product)) {
                    continue;
                }
                if (stockVisibilityItemResponseDto.getSoh() == null
                        || Long.parseLong(stockVisibilityItemResponseDto.getSoh()) <= 0) {
                    actualProductSOH = 0;
                }
                else {
                    actualProductSOH = Long.parseLong(stockVisibilityItemResponseDto.getSoh());
                }
                pendingQuantity = targetPointOfServiceService
                        .getFulfilmentPendingQuantityByProductCode(storeNumber, product);
                if (null != storeStockBuffer) {
                    actualProductSOH -= storeStockBuffer.longValue();
                }
                actualProductSOH -= pendingQuantity;

                logSohInfoMessage("SOH lookup for product: product=" + product
                        + ", SOHatStore=" + actualProductSOH
                        + ", bufferStockSet=" + storeStockBuffer
                        + ", pendingQuantity=" + pendingQuantity
                        + ", resultSOH=" + actualProductSOH, storeNumber);
                sohMap.put(product, Long.valueOf(actualProductSOH));
            }
        }
        return sohMap;
    }

    /**
     * @param targetStoreFulfilmentCapabilitiesService
     *            the targetStoreFulfilmentCapabilitiesService to set
     */
    @Required
    public void setTargetStoreFulfilmentCapabilitiesService(
            final TargetStoreFulfilmentCapabilitiesService targetStoreFulfilmentCapabilitiesService) {
        this.targetStoreFulfilmentCapabilitiesService = targetStoreFulfilmentCapabilitiesService;
    }

}

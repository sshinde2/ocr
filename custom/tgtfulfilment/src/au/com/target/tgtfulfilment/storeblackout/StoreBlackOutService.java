/**
 * 
 */
package au.com.target.tgtfulfilment.storeblackout;

import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.Date;


/**
 * @author Pradeep
 *
 */
public interface StoreBlackOutService {

    /**
     * @param startDate
     * @param endDate
     * @return isBlockoutDate
     */
    boolean doesBlackOutPeriodApply(final Date startDate, final Date endDate);

    /**
     * Validate dates.
     *
     * @param startDate
     *            the start date
     * @param endDate
     *            the end date
     * @throws InterceptorException
     *             the interceptor exception
     */
    void validateDates(Date startDate, Date endDate) throws InterceptorException;
}

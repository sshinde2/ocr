/**
 * 
 */
package au.com.target.tgtfulfilment.storeblackout.impl;

import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.Date;

import org.apache.commons.lang.ObjectUtils;

import au.com.target.tgtfulfilment.storeblackout.StoreBlackOutService;


/**
 * @author Pradeep
 *
 */
public class StoreBlackOutServiceImpl implements StoreBlackOutService {

    /**
     * Returns true for below conditions 1)current date in between startDate and EndDate. 2)start date null and current
     * date is before end Date. 3)end date null and current date is after start date.
     * 
     * @param startDate
     * @param endDate
     * @return isBlockoutDate
     */
    @Override
    public boolean doesBlackOutPeriodApply(final Date startDate, final Date endDate) {
        if (startDate == null && endDate == null) {
            return false;
        }
        final Date currentDate = new Date();
        //ObjectUtils internally uses Date Comparator.
        return (ObjectUtils.compare(currentDate, startDate, false) != -1 && ObjectUtils
                .compare(endDate, currentDate, true) != -1);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.storeblackout.StoreBlackOutService#isValidDate(java.util.Date, java.util.Date)
     */
    @Override
    public void validateDates(final Date startDate, final Date endDate) throws InterceptorException {
        final Date currentDate = new Date();
        if ((null != startDate)
                && startDate.before(currentDate)) {
            throw new InterceptorException("Invalid blackout start date, it should be greater than present date.");
        }
        if ((null != endDate)
                && endDate.before(currentDate)) {
            throw new InterceptorException("Invalid blackout end date, it should be greater than present date.");
        }
        if ((null != startDate) && (null != endDate)
                && ((startDate.after(endDate)))) {
            throw new InterceptorException(
                    "Invalid blackout start date, it should be either before or equal to end date.");
        }
    }



}

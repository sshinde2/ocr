/**
 * 
 */
package au.com.target.tgtfulfilment.util;

import org.apache.commons.lang.StringEscapeUtils;


/**
 * Utility class to escape special characters.
 * 
 * @author jjayawa1
 *
 */
public final class EscapeSpecialCharactersUtil {

    private EscapeSpecialCharactersUtil() {
        // Empty constructor.
    }

    /**
     * Escapes html special characters from an input String.
     * 
     * @param inputString
     * @return escaped String
     */
    public static String escapeSpecialHtmlCharacters(final String inputString) {
        return StringEscapeUtils.escapeHtml(inputString);
    }

    /**
     * Escapes xml special characters from an input String.
     * 
     * @param inputString
     * @return escaped String
     */
    public static String escapeSpecialXMLCharacters(final String inputString) {
        return StringEscapeUtils.escapeXml(inputString);
    }
}

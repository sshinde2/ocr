/**
 * 
 */
package au.com.target.tgtfulfilment.util;

/**
 * LoggingContext to hold constants for logging.
 * 
 * @author jjayawa1
 *
 */
public enum LoggingContext {

    WAREHOUSE("WarehouseFulfilment: "), AUTO_ACK("AutoFulfilment: "), INSTORE("InstoreFulfilment: ");

    private String selectedContext;

    /**
     * @param selectedContext
     *            the selectedContext to set
     */
    private LoggingContext(final String selectedContext) {
        this.selectedContext = selectedContext;
    }

    /**
     * Return the value of the selected logging context.
     * 
     * @return the selectedContext
     */
    public static String getSelectedContext(final LoggingContext context) {
        if (context != null) {
            return context.getLoggingContext();
        }
        return WAREHOUSE.getLoggingContext();
    }

    private String getLoggingContext() {
        return this.selectedContext;
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.util;

import java.util.Calendar;
import java.util.Date;

import org.joda.time.DateTime;


/**
 * @author rsamuel3
 *
 */
public class TgtFulfilmentUtil {

    private TgtFulfilmentUtil() {
        //not implemented
    }

    /**
     * When time is in the format hh:mm this method returns a date
     * 
     * @param time
     * @return Date which indicates either the current date at this time or if the date created is in the future the
     *         date for the previous day
     */
    public static Date getValidDateForTime(final String time) {
        if (null != time) {
            final String[] timeUnits = time.split(":");
            final Calendar cal = Calendar.getInstance();
            if (timeUnits.length == 2) {
                cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeUnits[0]));
                cal.set(Calendar.MINUTE, Integer.parseInt(timeUnits[1]));
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
                DateTime validDate = new DateTime(cal.getTimeInMillis());

                if (validDate.isAfter(DateTime.now())) {
                    validDate = validDate.minusHours(24);
                }
                return validDate.toDate();
            }
        }
        return null;
    }
}

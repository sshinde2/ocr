/**
 * 
 */
package au.com.target.tgtfulfilment.warehouseservices.impl;

import de.hybris.platform.ordersplitting.model.WarehouseModel;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.warehouse.service.impl.TargetWarehouseServiceImpl;
import au.com.target.tgtfulfilment.warehouseservices.TargetFulfilmentWarehouseService;


/**
 * @author ajit
 *
 */
public class TargetFulfilmentWarehouseServiceImpl extends TargetWarehouseServiceImpl implements
        TargetFulfilmentWarehouseService {

    private String giftcardWarehouseCode;

    @Override
    public WarehouseModel getGiftcardWarehouse() {
        return getWarehouseForCode(getGiftcardWarehouseCode());
    }

    /**
     * @return the digitalGiftcardWarehouseCode
     */
    protected String getGiftcardWarehouseCode() {
        return giftcardWarehouseCode;
    }

    /**
     * @param giftcardWarehouseCode
     *            the digitalGiftcardWarehouseCode to set
     */
    @Required
    public void setGiftcardWarehouseCode(final String giftcardWarehouseCode) {
        this.giftcardWarehouseCode = giftcardWarehouseCode;
    }

}

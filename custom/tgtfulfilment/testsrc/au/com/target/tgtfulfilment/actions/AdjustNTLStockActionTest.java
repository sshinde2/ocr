package au.com.target.tgtfulfilment.actions;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;


/**
 * Unit test for {@link SplitOrderAction}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AdjustNTLStockActionTest {

    private static final String CONSIGNMENT_CODE = "testCons";

    @InjectMocks
    private final AdjustNTLStockAction adjustNTLStockAction = new AdjustNTLStockAction();

    @Mock
    private TargetStockService targetStockService;

    @Mock
    private TargetStoreConsignmentService targetStoreConsignmentService;

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteActionWithNullProcess() throws Exception {
        adjustNTLStockAction.executeAction(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteActionWithNullOrder() throws Exception {
        final OrderProcessModel orderProcessModel = Mockito.mock(OrderProcessModel.class);
        adjustNTLStockAction.executeAction(orderProcessModel);
    }

    @Test
    public void testExecuteActionForFastlineOrders() throws Exception {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final ConsignmentModel consignment = Mockito.mock(ConsignmentModel.class);
        final ConsignmentEntryModel consignmentEntry = Mockito.mock(ConsignmentEntryModel.class);
        final ProductModel productModel = Mockito.mock(ProductModel.class);
        final WarehouseModel warehouseFrom = Mockito.mock(WarehouseModel.class);
        final WarehouseModel warehouseTo = Mockito.mock(WarehouseModel.class);
        final OrderEntryModel orderEntryModel = Mockito.mock(OrderEntryModel.class);

        given(orderModel.getConsignments()).willReturn(Collections.singleton(consignment));
        given(consignment.getConsignmentEntries()).willReturn(Collections.singleton(consignmentEntry));
        given(orderModel.getConsignments()).willReturn(Collections.singleton(consignment));
        given(consignment.getConsignmentEntries()).willReturn(Collections.singleton(consignmentEntry));
        given(consignmentEntry.getOrderEntry()).willReturn(orderEntryModel);
        given(orderEntryModel.getProduct()).willReturn(productModel);
        given(orderEntryModel.getQuantity()).willReturn(Long.valueOf(3));
        given(targetWarehouseService.getDefaultOnlineWarehouse()).willReturn(warehouseFrom);
        given(consignment.getWarehouse()).willReturn(warehouseTo);
        given(consignment.getCode()).willReturn(CONSIGNMENT_CODE);

        final OrderProcessModel orderProcessModel = Mockito.mock(OrderProcessModel.class);
        BDDMockito.given(orderProcessModel.getOrder()).willReturn(orderModel);
        BDDMockito.given(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyNTL(consignment)))
                .willReturn(Boolean.valueOf(false));
        adjustNTLStockAction.executeAction(orderProcessModel);

        BDDMockito.verify(targetStoreConsignmentService).isConsignmentAssignedToAnyNTL(consignment);

        BDDMockito.verify(targetStockService, Mockito.times(0)).transferReserveAmount(productModel, 3, warehouseFrom,
                warehouseTo, CONSIGNMENT_CODE);
    }

    @Test
    public void testExecuteActionForNTLOrders() throws Exception {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final ConsignmentModel consignment = Mockito.mock(ConsignmentModel.class);
        final ConsignmentEntryModel consignmentEntry = Mockito.mock(ConsignmentEntryModel.class);
        final ProductModel productModel = Mockito.mock(ProductModel.class);
        final OrderEntryModel orderEntryModel = Mockito.mock(OrderEntryModel.class);
        final WarehouseModel warehouseFrom = Mockito.mock(WarehouseModel.class);
        final WarehouseModel warehouseTo = Mockito.mock(WarehouseModel.class);

        given(consignment.getConsignmentEntries()).willReturn(Collections.singleton(consignmentEntry));
        given(orderModel.getConsignments()).willReturn(Collections.singleton(consignment));
        given(consignmentEntry.getOrderEntry()).willReturn(orderEntryModel);
        given(orderEntryModel.getProduct()).willReturn(productModel);
        given(orderEntryModel.getQuantity()).willReturn(Long.valueOf(3));
        given(targetWarehouseService.getDefaultOnlineWarehouse()).willReturn(warehouseFrom);
        given(consignment.getWarehouse()).willReturn(warehouseTo);
        given(consignment.getCode()).willReturn(CONSIGNMENT_CODE);

        final OrderProcessModel orderProcessModel = Mockito.mock(OrderProcessModel.class);
        BDDMockito.given(orderProcessModel.getOrder()).willReturn(orderModel);
        BDDMockito.given(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyNTL(consignment)))
                .willReturn(Boolean.valueOf(true));
        adjustNTLStockAction.executeAction(orderProcessModel);

        BDDMockito.verify(targetStoreConsignmentService).isConsignmentAssignedToAnyNTL(consignment);

        BDDMockito.verify(targetStockService).transferReserveAmount(productModel, 3, warehouseFrom,
                warehouseTo, CONSIGNMENT_CODE);
    }

    @Test
    public void testExecuteActionForMultipleConsignments() throws Exception {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        final ConsignmentModel consignment1 = Mockito.mock(ConsignmentModel.class);
        final ConsignmentModel consignment2 = Mockito.mock(ConsignmentModel.class);
        final ConsignmentEntryModel consignmentEntry = Mockito.mock(ConsignmentEntryModel.class);
        final ProductModel productModel = Mockito.mock(ProductModel.class);
        final OrderEntryModel orderEntryModel = Mockito.mock(OrderEntryModel.class);
        final WarehouseModel warehouseFrom = Mockito.mock(WarehouseModel.class);
        final WarehouseModel warehouseTo = Mockito.mock(WarehouseModel.class);
        final Set<ConsignmentModel> consignments = new HashSet<>();
        consignments.add(consignment1);
        consignments.add(consignment2);
        given(orderModel.getConsignments()).willReturn(consignments);
        given(consignment1.getConsignmentEntries()).willReturn(Collections.singleton(consignmentEntry));
        given(consignment2.getConsignmentEntries()).willReturn(Collections.singleton(consignmentEntry));
        given(consignmentEntry.getOrderEntry()).willReturn(orderEntryModel);
        given(orderEntryModel.getProduct()).willReturn(productModel);
        given(orderEntryModel.getQuantity()).willReturn(Long.valueOf(3));
        given(targetWarehouseService.getDefaultOnlineWarehouse()).willReturn(warehouseFrom);
        given(consignment1.getWarehouse()).willReturn(warehouseTo);
        given(consignment1.getCode()).willReturn(CONSIGNMENT_CODE);

        final OrderProcessModel orderProcessModel = Mockito.mock(OrderProcessModel.class);
        BDDMockito.given(orderProcessModel.getOrder()).willReturn(orderModel);
        BDDMockito.given(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyNTL(consignment1)))
                .willReturn(Boolean.valueOf(true));
        BDDMockito.given(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyNTL(consignment2)))
                .willReturn(Boolean.valueOf(true));
        adjustNTLStockAction.executeAction(orderProcessModel);
        BDDMockito.verify(targetStockService).transferReserveAmount(productModel, 3, warehouseFrom,
                warehouseTo, CONSIGNMENT_CODE);
    }
}

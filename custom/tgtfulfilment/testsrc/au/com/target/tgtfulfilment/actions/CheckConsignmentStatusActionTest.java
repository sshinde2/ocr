/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import static au.com.target.tgtfulfilment.actions.CheckConsignmentStatusAction.Transition.ALL_CANCELLED;
import static au.com.target.tgtfulfilment.actions.CheckConsignmentStatusAction.Transition.ALL_DONE;
import static au.com.target.tgtfulfilment.actions.CheckConsignmentStatusAction.Transition.PENDING;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.exceptions.BusinessProcessException;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CheckConsignmentStatusActionTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Mock
    private PurchaseOptionConfigModel purchaseOptionConfig;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private ConsignmentModel consignment;

    @Mock
    private ModelService modelService;

    @Mock
    private WarehouseModel warehouse;

    @Spy
    @InjectMocks
    private final CheckConsignmentStatusAction action = new CheckConsignmentStatusAction();

    @Before
    public void setup() {
        // Default full pick on consignment in process
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignment);
        given(orderProcessParameterHelper.getPickType(process)).willReturn(
                OrderProcessParameterHelper.PickTypeEnum.FULL);

        given(process.getOrder()).willReturn(order);
        given(warehouse.getCode()).willReturn("FastlineWarehouse");
        given(consignment.getWarehouse()).willReturn(warehouse);
        given(order.getConsignments()).willReturn(Collections.singleton(consignment));
        given(order.getPurchaseOptionConfig()).willReturn(purchaseOptionConfig);
        given(purchaseOptionConfig.getAllowPaymentDues()).willReturn(Boolean.FALSE);
        final ArrayList<String> warehousesList = new ArrayList();
        warehousesList.add("FastlineWarehouse");
        warehousesList.add("IncommWarehouse");
        warehousesList.add("CnpWarehouse");
        action.setWarehousesList(warehousesList);
    }

    @Test
    public void testCombinedConsignmentStateNoConsignments() {
        expectedException.expect(BusinessProcessException.class);
        given(order.getConsignments()).willReturn(null);
        action.getCombinedConsignmentState(process);
    }

    @Test
    public void testCombinedConsignmentStateCancelledConsignment() {
        given(consignment.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        assertEquals(ALL_CANCELLED.name(), action.getCombinedConsignmentState(process));
    }

    @Test
    public void testCombinedConsignmentStateConfirmedConsignment() {
        given(consignment.getStatus()).willReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        assertEquals(PENDING.name(), action.getCombinedConsignmentState(process));
    }

    @Test
    public void testCombinedConsignmentStatePickedConsignment() {
        given(consignment.getStatus()).willReturn(ConsignmentStatus.PICKED);
        assertEquals(ALL_DONE.name(), action.getCombinedConsignmentState(process));
    }

    @Test
    public void testCombinedConsignmentStateShippedConsignment() {
        given(consignment.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        assertEquals(ALL_DONE.name(), action.getCombinedConsignmentState(process));
    }

    @Test
    public void testCombinedConsignmentStatePackedConsignment() {
        given(warehouse.getCode()).willReturn("RockhamptonWarehouse");
        given(consignment.getStatus()).willReturn(ConsignmentStatus.PACKED);
        assertEquals(ALL_DONE.name(), action.getCombinedConsignmentState(process));
    }

    @Test
    public void testMultipleConsignmentFastlineAndIncomm() {
        final WarehouseModel fastlineWarehouse = new WarehouseModel();
        fastlineWarehouse.setCode("FastlineWarehouse");
        final WarehouseModel incommWarehouse = new WarehouseModel();
        incommWarehouse.setCode("IncommWarehouse");

        final ConsignmentModel fastlineConsignment = new ConsignmentModel();
        fastlineConsignment.setStatus(ConsignmentStatus.PICKED);
        fastlineConsignment.setWarehouse(fastlineWarehouse);

        final ConsignmentModel incommConsignment = new ConsignmentModel();
        incommConsignment.setStatus(ConsignmentStatus.SHIPPED);
        incommConsignment.setWarehouse(incommWarehouse);

        final Set<ConsignmentModel> consignments = new HashSet<>();
        consignments.add(incommConsignment);
        consignments.add(fastlineConsignment);

        given(order.getConsignments()).willReturn(consignments);
        assertEquals(ALL_DONE.name(), action.getCombinedConsignmentState(process));
    }

    @Test
    public void testMultipleConsignmentFastlineAndInstoreFulfillment() {
        final WarehouseModel fastlineWarehouse = new WarehouseModel();
        fastlineWarehouse.setCode("FastlineWarehouse");
        final WarehouseModel insotreWarehouse = new WarehouseModel();
        insotreWarehouse.setCode("RockhamptonWarehouse");

        final ConsignmentModel fastlineConsignment = new ConsignmentModel();
        fastlineConsignment.setStatus(ConsignmentStatus.PICKED);
        fastlineConsignment.setWarehouse(fastlineWarehouse);

        final ConsignmentModel instoreConsignment = new ConsignmentModel();
        instoreConsignment.setStatus(ConsignmentStatus.PACKED);
        instoreConsignment.setWarehouse(insotreWarehouse);

        final Set<ConsignmentModel> consignments = new HashSet<>();
        consignments.add(instoreConsignment);
        consignments.add(fastlineConsignment);

        given(order.getConsignments()).willReturn(consignments);
        assertEquals(ALL_DONE.name(), action.getCombinedConsignmentState(process));
    }
}

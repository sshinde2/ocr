/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Sets;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.actions.CheckOrderConsignmentStatusAction.Transition;



/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CheckOrderConsignmentStatusActionTest {

    @InjectMocks
    private final CheckOrderConsignmentStatusAction action = new CheckOrderConsignmentStatusAction();

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private TargetWarehouseService warehouseService;

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Mock
    private AbstractOrderEntryModel orderEntry1;

    @Mock
    private AbstractOrderEntryModel orderEntry2;

    @Mock
    private ConsignmentModel consignment1;

    @Mock
    private ConsignmentModel consignment2;

    @Mock
    private ConsignmentEntryModel c1e1;

    @Mock
    private ConsignmentEntryModel c2e1;

    @Mock
    private WarehouseModel fastlineWarehouse;

    @Mock
    private WarehouseModel storeWarehouse;

    @Before
    public void setup() {
        given(fastlineWarehouse.getCode()).willReturn("FastlineWarehouse");
        given(storeWarehouse.getCode()).willReturn("storeWarehouse");
        final ArrayList<String> warehousesList = new ArrayList();
        warehousesList.add("FastlineWarehouse");
        warehousesList.add("IncommWarehouse");
        warehousesList.add("CnpWarehouse");
        action.setWarehousesList(warehousesList);
        given(process.getOrder()).willReturn(order);
        given(order.getEntries()).willReturn(Arrays.asList(orderEntry1, orderEntry2));
        given(order.getConsignments()).willReturn(Sets.newHashSet(consignment1, consignment2));
        given(consignment1.getWarehouse()).willReturn(storeWarehouse);
        given(consignment2.getWarehouse()).willReturn(fastlineWarehouse);
        given(consignment1.getConsignmentEntries()).willReturn(Sets.newHashSet(c1e1));
        given(consignment2.getConsignmentEntries()).willReturn(Sets.newHashSet(c2e1));
        action.setRequiredStatusListForWarehouse(Arrays.asList(ConsignmentStatus.SHIPPED, ConsignmentStatus.PICKED));
        action.setRequiredStatusListForStore(Arrays.asList(ConsignmentStatus.SHIPPED, ConsignmentStatus.PACKED));
    }

    @Test
    public void testDefaultOnlineWarehouseTreatedLikeAStoreReturnsAllDoneWithShippedAndPacked()
            throws Exception {
        given(warehouseService.getDefaultOnlineWarehouse()).willReturn(fastlineWarehouse);
        given(targetFeatureSwitchService
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FORCE_DEFAULT_WAREHOUSE_TO_ONLINE_POS))
                        .willReturn(true);
        given(consignment1.getWarehouse()).willReturn(fastlineWarehouse);
        given(consignment2.getWarehouse()).willReturn(fastlineWarehouse);
        given(consignment1.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        given(consignment2.getStatus()).willReturn(ConsignmentStatus.PACKED);
        given(c1e1.getShippedQuantity()).willReturn(Long.valueOf(5));
        given(c2e1.getShippedQuantity()).willReturn(Long.valueOf(10));
        given(orderEntry1.getQuantity()).willReturn(Long.valueOf(5));
        given(orderEntry2.getQuantity()).willReturn(Long.valueOf(10));
        final String result = action.execute(process);
        assertThat(result).isEqualTo(Transition.ALL_DONE.name());
    }

    @Test
    public void testDefaultOnlineWarehouseTreatedLikeAStoreReturnsPendingWithPickedAndPacked()
            throws Exception {
        given(warehouseService.getDefaultOnlineWarehouse()).willReturn(fastlineWarehouse);
        given(targetFeatureSwitchService
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FORCE_DEFAULT_WAREHOUSE_TO_ONLINE_POS))
                        .willReturn(true);
        given(consignment1.getWarehouse()).willReturn(fastlineWarehouse);
        given(consignment2.getWarehouse()).willReturn(fastlineWarehouse);
        given(consignment1.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(consignment2.getStatus()).willReturn(ConsignmentStatus.PACKED);
        given(c1e1.getShippedQuantity()).willReturn(Long.valueOf(5));
        given(c2e1.getShippedQuantity()).willReturn(Long.valueOf(10));
        given(orderEntry1.getQuantity()).willReturn(Long.valueOf(5));
        given(orderEntry2.getQuantity()).willReturn(Long.valueOf(10));
        final String result = action.execute(process);
        assertThat(result).isEqualTo(Transition.PENDING.name());
    }


    @Test
    public void testDefaultOnlineWarehouseNotTreatedLikeAStoreReturnsAllDoneWithShippedAndPicked()
            throws Exception {
        given(warehouseService.getDefaultOnlineWarehouse()).willReturn(fastlineWarehouse);
        given(targetFeatureSwitchService
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FORCE_DEFAULT_WAREHOUSE_TO_ONLINE_POS))
                        .willReturn(false);
        given(consignment1.getWarehouse()).willReturn(fastlineWarehouse);
        given(consignment2.getWarehouse()).willReturn(fastlineWarehouse);
        given(consignment1.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        given(consignment2.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(c1e1.getShippedQuantity()).willReturn(Long.valueOf(5));
        given(c2e1.getShippedQuantity()).willReturn(Long.valueOf(10));
        given(orderEntry1.getQuantity()).willReturn(Long.valueOf(5));
        given(orderEntry2.getQuantity()).willReturn(Long.valueOf(10));
        final String result = action.execute(process);
        assertThat(result).isEqualTo(Transition.ALL_DONE.name());
    }

    @Test
    public void testDefaultOnlineWarehouseNotTreatedLikeAStoreReturnsPendingWithPickedAndPacked()
            throws Exception {
        given(warehouseService.getDefaultOnlineWarehouse()).willReturn(fastlineWarehouse);
        given(targetFeatureSwitchService
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FORCE_DEFAULT_WAREHOUSE_TO_ONLINE_POS))
                        .willReturn(false);
        given(consignment1.getWarehouse()).willReturn(fastlineWarehouse);
        given(consignment2.getWarehouse()).willReturn(fastlineWarehouse);
        given(consignment1.getStatus()).willReturn(ConsignmentStatus.PACKED);
        given(consignment2.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(c1e1.getShippedQuantity()).willReturn(Long.valueOf(5));
        given(c2e1.getShippedQuantity()).willReturn(Long.valueOf(10));
        given(orderEntry1.getQuantity()).willReturn(Long.valueOf(5));
        given(orderEntry2.getQuantity()).willReturn(Long.valueOf(10));
        final String result = action.execute(process);
        assertThat(result).isEqualTo(Transition.PENDING.name());
    }

    @Test
    public void testExecuteWithAllCancelled() throws Exception {
        given(warehouseService.getDefaultOnlineWarehouse()).willReturn(fastlineWarehouse);
        given(orderEntry1.getQuantity()).willReturn(Long.valueOf(0));
        given(orderEntry2.getQuantity()).willReturn(Long.valueOf(0));
        final String result = action.execute(process);
        assertThat(result).isEqualTo(Transition.ALL_CANCELLED.name());
    }

    @Test
    public void testExecuteWithPendingInFastline() throws Exception {
        given(warehouseService.getDefaultOnlineWarehouse()).willReturn(fastlineWarehouse);
        given(consignment1.getStatus()).willReturn(ConsignmentStatus.SENT_TO_WAREHOUSE);
        given(consignment2.getStatus()).willReturn(ConsignmentStatus.PACKED);
        given(c1e1.getShippedQuantity()).willReturn(null);
        given(c2e1.getShippedQuantity()).willReturn(Long.valueOf(10));
        given(orderEntry1.getQuantity()).willReturn(Long.valueOf(5));
        given(orderEntry2.getQuantity()).willReturn(Long.valueOf(10));
        final String result = action.execute(process);
        assertThat(result).isEqualTo(Transition.PENDING.name());
    }

    @Test
    public void testExecuteWithPendingInStore() throws Exception {
        given(warehouseService.getDefaultOnlineWarehouse()).willReturn(fastlineWarehouse);
        given(consignment1.getStatus()).willReturn(ConsignmentStatus.SHIPPED);
        given(consignment2.getStatus()).willReturn(ConsignmentStatus.PICKED);
        given(c1e1.getShippedQuantity()).willReturn(null);
        given(c2e1.getShippedQuantity()).willReturn(Long.valueOf(10));
        given(orderEntry1.getQuantity()).willReturn(Long.valueOf(5));
        given(orderEntry2.getQuantity()).willReturn(Long.valueOf(10));
        final String result = action.execute(process);
        assertThat(result).isEqualTo(Transition.PENDING.name());
    }
}

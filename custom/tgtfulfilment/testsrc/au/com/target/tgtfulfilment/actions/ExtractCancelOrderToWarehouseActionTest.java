/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper.CancelTypeEnum;
import au.com.target.tgtfulfilment.service.SendToWarehouseService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtractCancelOrderToWarehouseActionTest {

    @Mock
    private SendToWarehouseService sendToWarehouseService;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @InjectMocks
    private final ExtractCancelOrderToWarehouseAction action = new ExtractCancelOrderToWarehouseAction();

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @SuppressWarnings("boxing")
    @Before
    public void setup() {
        BDDMockito.given(process.getOrder()).willReturn(order);
        BDDMockito.given(orderProcessParameterHelper.doCancelExtract(process)).willReturn(true);
        BDDMockito.given(orderProcessParameterHelper.getCancelType(process)).willReturn(CancelTypeEnum.COCKPIT_CANCEL);
    }

    @Test
    public void testShortPick() throws RetryLaterException, Exception {

        BDDMockito.given(orderProcessParameterHelper.getCancelType(process)).willReturn(CancelTypeEnum.SHORT_PICK);
        action.execute(process);
        Mockito.verify(sendToWarehouseService, Mockito.never()).sendOrderCancellation(order);
    }

    @Test
    public void testZeroPick() throws RetryLaterException, Exception {

        BDDMockito.given(orderProcessParameterHelper.getCancelType(process)).willReturn(CancelTypeEnum.ZERO_PICK);
        action.execute(process);
        Mockito.verify(sendToWarehouseService, Mockito.never()).sendOrderCancellation(order);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testWithDoCancelExtractFalse() throws RetryLaterException, Exception {

        BDDMockito.given(orderProcessParameterHelper.doCancelExtract(process)).willReturn(false);
        action.execute(process);
        Mockito.verify(sendToWarehouseService, Mockito.never()).sendOrderCancellation(order);
    }

    @Test
    public void testSuccess() throws RetryLaterException, Exception {

        action.execute(process);
        Mockito.verify(sendToWarehouseService).sendOrderCancellation(order);
    }


}

/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.exceptions.InventoryAdjustmentFailedException;
import au.com.target.tgtfulfilment.inventory.dto.InventoryAdjustmentData;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;
import au.com.target.tgtfulfilment.stock.client.InventoryAdjustmentClient;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class InventoryAdjustmentActionTest {


    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private InventoryAdjustmentClient inventoryAdjustmentClient;

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private TargetStoreConsignmentService targetStoreConsignmentService;

    @Mock
    private TargetFeatureSwitchService featureSwitchService;

    @Mock
    private OrderProcessModel orderProcessMock;

    @Mock
    private ConsignmentModel consignment;

    @Mock
    private WarehouseModel fromWarehouse;

    @Mock
    private TargetPointOfServiceModel fromStore;

    @Mock
    private WarehouseModel toWarehouse;

    @Mock
    private TargetPointOfServiceModel toStore;

    @Mock
    private TargetSharedConfigService targetSharedConfigService;

    private Set<ConsignmentEntryModel> consignmentEntryList;

    @Captor
    private ArgumentCaptor<List<InventoryAdjustmentData>> captorListDataSent;

    @InjectMocks
    private final InventoryAdjustmentAction inventoryAdjustmentAction = new InventoryAdjustmentAction();

    @Before
    public void setup() {
        given(Boolean.valueOf((featureSwitchService.isFeatureEnabled("inventoryAdjustmentUsingWM"))))
                .willReturn(Boolean.TRUE);
        given(orderProcessParameterHelper.getConsignment(orderProcessMock)).willReturn(consignment);

        given(consignment.getWarehouse()).willReturn(fromWarehouse);
        given(targetStoreConsignmentService.getAssignedStoreForWarehouse(fromWarehouse)).willReturn(fromStore);
        given(fromStore.getStoreNumber()).willReturn(Integer.valueOf(1));

        given(targetWarehouseService.getDefaultOnlineWarehouse()).willReturn(toWarehouse);
        given(targetStoreConsignmentService.getAssignedStoreForWarehouse(toWarehouse)).willReturn(toStore);
        given(toStore.getStoreNumber()).willReturn(Integer.valueOf(2));
        mockConsignmentEntry();
        given(consignment.getConsignmentEntries()).willReturn(consignmentEntryList);
    }

    private void mockConsignmentEntry() {
        consignmentEntryList = new HashSet<ConsignmentEntryModel>();
        consignmentEntryList.add(mockConsignmentEntry("1223", 10));
    }

    private ConsignmentEntryModel mockConsignmentEntry(final String itemCode, final long quanity) {
        final ConsignmentEntryModel entryModel = mock(ConsignmentEntryModel.class);
        given(entryModel.getShippedQuantity()).willReturn(Long.valueOf(quanity));
        final OrderEntryModel orderEntryModel = mock(OrderEntryModel.class);
        given(entryModel.getOrderEntry()).willReturn(orderEntryModel);
        final TargetSizeVariantProductModel sizeVariantModel = mock(TargetSizeVariantProductModel.class);
        given(orderEntryModel.getProduct()).willReturn(sizeVariantModel);
        given(sizeVariantModel.getCode()).willReturn(itemCode);
        return entryModel;
    }

    @Test
    public void testInventoryAdjustmentWhenFeatureIsOff() throws RetryLaterException, Exception {
        given(Boolean.valueOf((featureSwitchService.isFeatureEnabled("inventoryAdjustmentUsingWM"))))
                .willReturn(Boolean.FALSE);
        inventoryAdjustmentAction.executeAction(orderProcessMock);
        verify(orderProcessParameterHelper, never()).getConsignment(orderProcessMock);
    }




    @Test
    public void testExceptionWhenFromStoreNotFound()
            throws RetryLaterException, Exception {
        try {
            given(targetStoreConsignmentService.getAssignedStoreForWarehouse(fromWarehouse)).willReturn(null);
            inventoryAdjustmentAction.executeAction(orderProcessMock);
        }
        catch (final InventoryAdjustmentFailedException ex) {
            assertThat(ex.getMessage())
                    .isEqualTo(
                            "Inventory-Adjustment: From Store Not Found ");
        }
    }


    @Test
    public void testWhenSucessFulInventoryAdjustment() throws RetryLaterException, Exception {
        given(Boolean.valueOf(inventoryAdjustmentClient.sendInventoryAdjustment(captorListDataSent.capture())))
                .willReturn(Boolean.TRUE);
        inventoryAdjustmentAction.executeAction(orderProcessMock);
        verify(inventoryAdjustmentClient).sendInventoryAdjustment(captorListDataSent.getValue());
        assertThat(captorListDataSent.getValue()).isNotNull();
        assertThat(captorListDataSent.getValue()).hasSize(2);
        for (final InventoryAdjustmentData inventoryAdjustmentData : captorListDataSent.getValue()) {
            assertThat(inventoryAdjustmentData.getCode()).isEqualTo("1223");
            switch (inventoryAdjustmentData.getStoreNum()) {
                case "1":
                    assertThat(inventoryAdjustmentData.getAdjustmentQty()).isEqualTo("-10");
                    break;
                case "2":
                    assertThat(inventoryAdjustmentData.getAdjustmentQty()).isEqualTo("10");
                    break;
                default:
                    fail("No from Store and To Store");
                    break;
            }
        }
    }


}

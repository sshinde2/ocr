/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.ordercancel.data.CancelRequestResponse;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.fulfilmentservice.PickTicketSender;
import au.com.target.tgtfulfilment.ordercancel.TargetProcessCancelService;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntryList;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ItemCancelAndRefundActionTest {

    @InjectMocks
    private final ItemCancelAndRefundAction itemCancelAndRefundAction = new ItemCancelAndRefundAction();

    @Mock
    private TargetProcessCancelService targetProcessCancelService;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private PickTicketSender pickTicketSender;

    @Mock
    private OrderProcessModel orderProcess;

    @Mock
    private OrderModel order;

    @Mock
    private TargetOrderCancelEntryList targetOrderCancelEntryList;

    @Mock
    private CancelRequestResponse response;

    @Before
    public void setup() throws FulfilmentException, OrderCancelException {
        given(orderProcess.getOrder()).willReturn(order);
        given(orderProcessParameterHelper
                .getTargetOrderCancelEntryList(orderProcess)).willReturn(targetOrderCancelEntryList);
        given(targetProcessCancelService.processCancel(order,
                targetOrderCancelEntryList)).willReturn(response);
    }

    @Test
    public void testExecuteAction() throws RetryLaterException, Exception {
        final Transition result = itemCancelAndRefundAction.executeAction(orderProcess);
        assertThat(result).isEqualTo(Transition.OK);
        verify(orderProcessParameterHelper).setOrderCancelRequest(orderProcess,
                (OrderCancelRecordEntryModel)response.getOrderModificationRecordEntryModel());
        verify(orderProcessParameterHelper).setRefundAmount(orderProcess, response.getRefundAmount());
        verify(orderProcessParameterHelper).setCancelType(orderProcess, response.getCancelType());
        verify(orderProcessParameterHelper).setRefundPaymentEntryList(orderProcess,
                response.getFollowOnRefundPaymentList());
        verify(orderProcessParameterHelper).setRefundAmountRemaining(orderProcess, response.getRefundAmountRemaining());
    }

    @Test
    public void testExecuteActionWithNOK() throws RetryLaterException, Exception {
        given(targetProcessCancelService.processCancel(order,
                targetOrderCancelEntryList)).willThrow(new OrderCancelException("test"));
        final Transition result = itemCancelAndRefundAction.executeAction(orderProcess);
        assertThat(result).isEqualTo(Transition.NOK);
    }

}

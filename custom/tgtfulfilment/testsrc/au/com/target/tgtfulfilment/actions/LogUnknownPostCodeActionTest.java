/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.PostCodeGroupModel;
import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgtcore.postcode.service.TargetPostCodeService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class LogUnknownPostCodeActionTest {

    @InjectMocks
    private final LogUnknownPostCodeAction logUnknownPostCodeAction = new LogUnknownPostCodeAction();

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Mock
    private TargetPostCodeService targetPostCodeService;


    @Mock
    private TargetZoneDeliveryModeModel deliveryModeModel;

    @Mock
    private AddressModel addressModel;

    @Before
    public void setup() {
        BDDMockito.given(process.getOrder()).willReturn(order);
    }

    @Test
    public void testExecuteAllocated() throws RetryLaterException, Exception {
        final String postCode = "3223";
        BDDMockito.given(order.getDeliveryMode()).willReturn(deliveryModeModel);
        BDDMockito.given(order.getDeliveryAddress()).willReturn(addressModel);
        BDDMockito.given(addressModel.getPostalcode()).willReturn(postCode);
        BDDMockito.given(deliveryModeModel.getIsDeliveryToStore()).willReturn(
                Boolean.FALSE);
        final List<PostCodeGroupModel> postcodeGroups = new ArrayList<>();
        final PostCodeGroupModel postCodeGroup = new PostCodeGroupModel();
        postcodeGroups.add(postCodeGroup);
        final PostCodeModel postCodeModel = Mockito.mock(PostCodeModel.class);
        BDDMockito.given(targetPostCodeService.getPostCode(postCode)).willReturn(postCodeModel);
        BDDMockito.given(postCodeModel.getPostCodeGroups()).willReturn(postcodeGroups);
        logUnknownPostCodeAction.execute(process);
        verify(order, times(0)).getCode();
    }

    @Test
    public void testExecuteNonExist() throws RetryLaterException, Exception {
        final String postCode = "3223";
        BDDMockito.given(order.getDeliveryMode()).willReturn(deliveryModeModel);
        BDDMockito.given(order.getDeliveryAddress()).willReturn(addressModel);
        BDDMockito.given(addressModel.getPostalcode()).willReturn(postCode);
        BDDMockito.given(deliveryModeModel.getIsDeliveryToStore()).willReturn(
                Boolean.FALSE);
        BDDMockito.given(targetPostCodeService.getPostCode(postCode)).willReturn(null);
        logUnknownPostCodeAction.execute(process);
        verify(order, times(1)).getCode();
    }

    @Test
    public void testExecuteNonAllocated() throws RetryLaterException, Exception {
        final String postCode = "3223";
        BDDMockito.given(order.getDeliveryMode()).willReturn(deliveryModeModel);
        BDDMockito.given(order.getDeliveryAddress()).willReturn(addressModel);
        BDDMockito.given(addressModel.getPostalcode()).willReturn(postCode);
        BDDMockito.given(deliveryModeModel.getIsDeliveryToStore()).willReturn(
                Boolean.FALSE);
        final List<PostCodeGroupModel> postcodeGroups = new ArrayList<>();
        final PostCodeModel postCodeModel = Mockito.mock(PostCodeModel.class);
        BDDMockito.given(targetPostCodeService.getPostCode(postCode)).willReturn(postCodeModel);
        BDDMockito.given(postCodeModel.getPostCodeGroups()).willReturn(postcodeGroups);
        logUnknownPostCodeAction.execute(process);
        verify(order, times(1)).getCode();
    }

    @Test
    public void testExecuteOtherDeliveryMode() throws RetryLaterException, Exception {
        final String postCode = "3223";
        BDDMockito.given(order.getDeliveryMode()).willReturn(deliveryModeModel);
        BDDMockito.given(order.getDeliveryAddress()).willReturn(addressModel);
        BDDMockito.given(deliveryModeModel.getIsDeliveryToStore()).willReturn(
                Boolean.TRUE);
        BDDMockito.given(addressModel.getPostalcode()).willReturn(postCode);
        logUnknownPostCodeAction.execute(process);
        verify(order, times(0)).getCode();
    }
}

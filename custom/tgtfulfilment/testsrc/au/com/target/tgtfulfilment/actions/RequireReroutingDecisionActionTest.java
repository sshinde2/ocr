package au.com.target.tgtfulfilment.actions;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;


/**
 * 
 * @author pvarghe2
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class RequireReroutingDecisionActionTest {

    @Mock
    private OrderProcessModel process;

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private WarehouseModel onlineWarehouse;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private TargetConsignmentService targetConsignmentService;


    @InjectMocks
    private final RequireReroutingDecisionAction requireReroutingDecisionAction = new RequireReroutingDecisionAction();

    @Test
    public void testExecuteNullWarehouse() throws Exception {
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignment);
        willReturn(Boolean.TRUE).given(targetConsignmentService).isConsignmentToDefaultWarehouse(consignment);
        final String result = requireReroutingDecisionAction.execute(process);
        assertThat(result).isEqualTo("FALSE");
    }

    @Test
    public void testExecuteWithOnlineWarehouse() throws Exception {
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignment);
        willReturn(Boolean.TRUE).given(targetConsignmentService).isConsignmentToDefaultWarehouse(consignment);
        given(consignment.getWarehouse()).willReturn(onlineWarehouse);
        final String result = requireReroutingDecisionAction.execute(process);
        assertThat(result).isEqualTo("FALSE");
    }

    @Test
    public void testExecuteWithStoreWarehouseAndFalconSwitchOn() throws Exception {
        final WarehouseModel storeWarehouse = mock(WarehouseModel.class);
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignment);
        willReturn(Boolean.FALSE).given(targetConsignmentService).isConsignmentToDefaultWarehouse(consignment);
        given(consignment.getWarehouse()).willReturn(storeWarehouse);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON);
        final String result = requireReroutingDecisionAction.execute(process);
        assertThat(result).isEqualTo("TRUE");
    }

    @Test
    public void testExecuteWithStoreWarehouseAndFalconSwitchOff() throws Exception {
        final WarehouseModel storeWarehouse = mock(WarehouseModel.class);
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignment);
        willReturn(Boolean.TRUE).given(targetConsignmentService).isConsignmentToDefaultWarehouse(consignment);
        given(consignment.getWarehouse()).willReturn(storeWarehouse);
        willReturn(Boolean.FALSE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON);
        final String result = requireReroutingDecisionAction.execute(process);
        assertThat(result).isEqualTo("FALSE");
    }

    @Test
    public void testExecuteWithOnlineWarehouseAndFalconSwitchOn() throws Exception {
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignment);
        willReturn(Boolean.TRUE).given(targetConsignmentService).isConsignmentToDefaultWarehouse(consignment);
        given(consignment.getWarehouse()).willReturn(onlineWarehouse);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON);
        final String result = requireReroutingDecisionAction.execute(process);
        assertThat(result).isEqualTo("FALSE");
    }

    @Test
    public void testExecuteWithFluentConsignmentAndFalconSwitchOn() throws Exception {
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignment);
        willReturn(Boolean.TRUE).given(targetConsignmentService).isAFluentConsignment(consignment);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON);

        final String result = requireReroutingDecisionAction.execute(process);

        assertThat(result).isEqualTo("FALSE");
    }

}

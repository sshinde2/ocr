/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.service.SendToWarehouseService;


/**
 * Unit test for {@link ResendConsignmentToWarehouseAction}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ResendConsignmentToWarehouseActionTest {

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @Mock
    private OrderProcessModel process;

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private SendToWarehouseService sendToWarehouseService;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private OrderModel order;


    @InjectMocks
    private final ResendConsignmentToWarehouseAction action = new ResendConsignmentToWarehouseAction();

    @Test
    public void testExecuteNullProcess() throws RetryLaterException, Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("OrderProcess cannot be null");
        action.execute(null);
        Mockito.verifyZeroInteractions(order);
        Mockito.verifyZeroInteractions(process);
        Mockito.verifyZeroInteractions(sendToWarehouseService);
    }


    @Test
    public void testExecuteNullOrder() throws RetryLaterException, Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Order cannot be null");
        Mockito.when(process.getOrder()).thenReturn(null);
        action.execute(process);
        Mockito.verify(process).getOrder();
        Mockito.verifyNoMoreInteractions(process);
        Mockito.verifyZeroInteractions(order);
        Mockito.verifyZeroInteractions(sendToWarehouseService);
    }

    @Test
    public void testExecuteNullValidConsignments() throws RetryLaterException, Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Consignment cannot be null");
        Mockito.when(process.getOrder()).thenReturn(order);
        Mockito.when(orderProcessParameterHelper.getConsignment(process)).thenReturn(null);

        action.execute(process);

        Mockito.verify(orderProcessParameterHelper).getConsignment(process);
        Mockito.verifyNoMoreInteractions(orderProcessParameterHelper);
        Mockito.verifyZeroInteractions(sendToWarehouseService);
    }

    @Test
    public void testExecuteWithValidConsignments() throws RetryLaterException, Exception {
        Mockito.when(process.getOrder()).thenReturn(order);
        Mockito.when(orderProcessParameterHelper.getConsignment(process)).thenReturn(consignment);

        action.execute(process);

        Mockito.verify(orderProcessParameterHelper).getConsignment(process);
        Mockito.verify(sendToWarehouseService).sendConsignment(consignment);
        Mockito.verifyNoMoreInteractions(sendToWarehouseService);
    }
}

package au.com.target.tgtfulfilment.actions;

import static au.com.target.tgtbusproc.util.OrderProcessParameterHelper.PickTypeEnum;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;

/**
 * Test suite for {@link RetrievePickTypeAction}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class RetrievePickTypeActionTest {

    @Mock
    private OrderProcessParameterHelper processParameterHelper;

    @Spy
    @InjectMocks
    private RetrievePickTypeAction action = new RetrievePickTypeAction();

    @Mock
    private OrderProcessModel process;

    /**
     * Initializes test case before run.
     */
    @Before
    public void setUp() {
        doReturn(processParameterHelper).when(action).getOrderProcessParameterHelper();
    }

    /**
     * Verifies that {@link RetrievePickTypeAction#PICK_TYPE_NOT_SET} is returned when no
     * pick type parameter is set in the context of {@code process}.
     *
     * @throws Exception if any exception occurs
     */
    @Test
    public void testPickTypeNotSet() throws Exception {
        assertEquals(RetrievePickTypeAction.PICK_TYPE_NOT_SET, action.execute(process));
    }

    /**
     * Verifies that action returns pick type set in the context of the business process.
     *
     * @throws Exception if any exception occurs.
     */
    @Test
    public void testPickTypeSet() throws Exception {
        when(processParameterHelper.getPickType(process)).thenReturn(PickTypeEnum.FULL);
        assertEquals(PickTypeEnum.FULL.toString(), action.execute(process));
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.actions;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;
import de.hybris.platform.task.RetryLaterException;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.email.BusinessProcessEmailStrategy;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendOrderShippingNoticeEmailActionTest {

    @Mock
    private BusinessProcessEmailStrategy businessProcessEmailStrategy;

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Mock
    private TargetConsignmentService targetConsignmentService;


    @Mock
    private WarehouseModel warehouse;


    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private TargetStoreConsignmentService targetStoreConsignmentService;


    @InjectMocks
    @Spy
    private final SendOrderShippingNoticeEmailAction action = new SendOrderShippingNoticeEmailAction();

    @Before
    public void setup() {
        action.setFrontendTemplateName("TEMPLATE");
        BDDMockito.willReturn(businessProcessEmailStrategy).given(action).getBusinessProcessEmailStrategy();
        given(process.getOrder()).willReturn(order);
    }

    @Test
    public void testExecuteNoConsignments() throws RetryLaterException, Exception {
        Mockito.when(targetConsignmentService.getActiveConsignmentsForOrder(order)).thenReturn(null);
        final Transition trans = action.executeAction(process);
        Mockito.verifyZeroInteractions(businessProcessEmailStrategy);
        Assert.assertEquals(Transition.OK, trans);
    }

    @Test
    public void testExecuteWithConsignmentsForFastline() throws RetryLaterException, Exception {

        Mockito.when(targetConsignmentService.getActiveConsignmentsForOrder(order)).thenReturn(
                Collections.singletonList(consignment));
        Mockito.when(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyStore(consignment)))
                .thenReturn(
                        Boolean.FALSE);
        final Transition trans = action.executeAction(process);
        Mockito.verify(businessProcessEmailStrategy).sendEmail(process, "TEMPLATE");
        Assert.assertEquals(Transition.OK, trans);
    }

    @Test
    public void testExecuteWithConsignmentsForStore() throws RetryLaterException, Exception {
        Mockito.when(targetConsignmentService.getActiveConsignmentsForOrder(order)).thenReturn(
                Collections.singletonList(consignment));
        Mockito.when(consignment.getOfcOrderType()).thenReturn(OfcOrderType.INSTORE_PICKUP);
        final Transition trans = action.executeAction(process);
        Mockito.verifyZeroInteractions(businessProcessEmailStrategy);
        Assert.assertEquals(Transition.OK, trans);

    }

	@Test
	public void testExecuteWithConsignmentsForInterStoreDelivery() throws RetryLaterException, Exception {
		given(targetConsignmentService.getActiveConsignmentsForOrder(order)).willReturn(
			Collections.singletonList(consignment));
		given(consignment.getOfcOrderType()).willReturn(OfcOrderType.INTERSTORE_DELIVERY);
		final Transition trans = action.executeAction(process);
		Mockito.verifyZeroInteractions(businessProcessEmailStrategy);
		Assert.assertEquals(Transition.OK, trans);

	}
}

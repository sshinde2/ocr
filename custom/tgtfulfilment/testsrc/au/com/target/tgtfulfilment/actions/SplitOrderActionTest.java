package au.com.target.tgtfulfilment.actions;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.ConsignmentCreationException;
import de.hybris.platform.ordersplitting.OrderSplittingService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.exception.TargetStoreStockCheckException;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;


/**
 * Unit test for {@link SplitOrderAction}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SplitOrderActionTest {

    @InjectMocks
    private final SplitOrderAction splitOrderAction = new SplitOrderAction();

    @Mock
    private OrderSplittingService orderSplittingService;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Test
    public void testExecuteActionWithNullProcess() throws ConsignmentCreationException, RetryLaterException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("OrderProcess cannot be null");
        splitOrderAction.executeInternal(null);
    }

    @Test
    public void testExecuteActionWithNullOrder() throws ConsignmentCreationException, RetryLaterException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Order cannot be null");

        final OrderProcessModel orderProcessModel = mock(OrderProcessModel.class);
        given(orderProcessModel.getOrder()).willReturn(null);

        splitOrderAction.executeInternal(orderProcessModel);
    }

    @Test
    public void testExecuteAction() throws ConsignmentCreationException, RetryLaterException {
        final OrderModel orderModel = mock(OrderModel.class);
        final OrderProcessModel orderProcessModel = mock(OrderProcessModel.class);
        given(orderProcessModel.getOrder()).willReturn(orderModel);

        splitOrderAction.executeInternal(orderProcessModel);


        verify(orderSplittingService).splitOrderForConsignment(orderModel, orderModel.getEntries());
        verify(orderModel).setStatus(OrderStatus.INPROGRESS);
        verify(modelService).save(orderModel);
    }

    @Test(expected = RetryLaterException.class)
    public void testExecuteActionWhenStockFails() throws ConsignmentCreationException, RetryLaterException {
        final OrderModel orderModel = mock(OrderModel.class);
        final OrderProcessModel orderProcessModel = mock(OrderProcessModel.class);
        given(orderProcessModel.getOrder()).willReturn(orderModel);
        given(orderSplittingService.splitOrderForConsignment(
                orderModel, orderModel.getEntries()))
                        .willThrow(new TargetStoreStockCheckException("exception", new Exception()));

        splitOrderAction.executeInternal(orderProcessModel);

    }

    @Test
    public void testVerifyWhenOrderHasOnlyDigitalProduct() throws ConsignmentCreationException, RetryLaterException {
        final OrderModel orderModel = mock(OrderModel.class);
        final OrderProcessModel orderProcessModel = mock(OrderProcessModel.class);
        final ProductModel digitalProduct = mock(ProductModel.class);
        final AbstractOrderEntryModel digitalEntry = mock(AbstractOrderEntryModel.class);
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries.add(digitalEntry);
        given(orderProcessModel.getOrder()).willReturn(orderModel);
        given(orderModel.getEntries()).willReturn(orderEntries);
        given(digitalEntry.getProduct()).willReturn(digitalProduct);
        given(Boolean.valueOf(targetDeliveryModeHelper.hasOnlyDigitalDeliveryMode(digitalProduct)))
                .willReturn(Boolean.TRUE);
        splitOrderAction.executeInternal(orderProcessModel);
        verify(targetDeliveryModeHelper).getDigitalDeliveryMode(
                any(AbstractTargetVariantProductModel.class));
    }

    @Test
    public void testVerifyWhenOrderHasMixedProducts() throws ConsignmentCreationException, RetryLaterException {
        final OrderModel orderModel = mock(OrderModel.class);
        final OrderProcessModel orderProcessModel = mock(OrderProcessModel.class);
        final AbstractTargetVariantProductModel digitalProduct = mock(AbstractTargetVariantProductModel.class);
        final AbstractTargetVariantProductModel nonDigitalProduct = Mockito
                .mock(AbstractTargetVariantProductModel.class);
        final AbstractOrderEntryModel digitalEntry = mock(AbstractOrderEntryModel.class);
        final AbstractOrderEntryModel nonDigitalEntry = mock(AbstractOrderEntryModel.class);
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries.add(digitalEntry);
        orderEntries.add(nonDigitalEntry);
        given(orderModel.getEntries()).willReturn(orderEntries);
        given(digitalEntry.getProduct()).willReturn(digitalProduct);
        given(nonDigitalEntry.getProduct()).willReturn(nonDigitalProduct);
        given(Boolean.valueOf(targetDeliveryModeHelper.hasOnlyDigitalDeliveryMode(digitalProduct)))
                .willReturn(Boolean.TRUE);
        given(Boolean.valueOf(targetDeliveryModeHelper.hasOnlyDigitalDeliveryMode(nonDigitalProduct)))
                .willReturn(Boolean.FALSE);
        given(orderProcessModel.getOrder()).willReturn(orderModel);
        splitOrderAction.executeInternal(orderProcessModel);
        verify(targetDeliveryModeHelper).getDigitalDeliveryMode(
                any(AbstractTargetVariantProductModel.class));
    }

    @Test
    public void testVerifyWhenOrderHasOnlyNonDigitalProduct() throws ConsignmentCreationException, RetryLaterException {
        final OrderModel orderModel = mock(OrderModel.class);
        final OrderProcessModel orderProcessModel = mock(OrderProcessModel.class);
        final ProductModel nonDigitalProduct = mock(ProductModel.class);
        final AbstractOrderEntryModel nonDigitalEntry = mock(AbstractOrderEntryModel.class);
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries.add(nonDigitalEntry);
        given(orderModel.getEntries()).willReturn(orderEntries);
        given(nonDigitalEntry.getProduct()).willReturn(nonDigitalProduct);
        given(Boolean.valueOf(targetDeliveryModeHelper.hasOnlyDigitalDeliveryMode(nonDigitalProduct)))
                .willReturn(Boolean.FALSE);
        given(orderProcessModel.getOrder()).willReturn(orderModel);
        splitOrderAction.executeInternal(orderProcessModel);
        verify(targetDeliveryModeHelper, never()).getDigitalDeliveryMode(
                any(AbstractTargetVariantProductModel.class));
    }

    @Test
    public void testVerifyWhenOrderHasProductWithMixedDelModes()
            throws ConsignmentCreationException, RetryLaterException {
        final OrderModel orderModel = mock(OrderModel.class);
        final OrderProcessModel orderProcessModel = mock(OrderProcessModel.class);
        final AbstractTargetVariantProductModel digitalProduct = mock(AbstractTargetVariantProductModel.class);
        final AbstractTargetVariantProductModel nonDigitalProduct = Mockito
                .mock(AbstractTargetVariantProductModel.class);
        final AbstractOrderEntryModel digitalEntry = mock(AbstractOrderEntryModel.class);
        final AbstractOrderEntryModel nonDigitalEntry = mock(AbstractOrderEntryModel.class);
        final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
        orderEntries.add(digitalEntry);
        orderEntries.add(nonDigitalEntry);
        given(orderModel.getEntries()).willReturn(orderEntries);
        given(digitalEntry.getProduct()).willReturn(digitalProduct);
        given(nonDigitalEntry.getProduct()).willReturn(nonDigitalProduct);
        given(Boolean.valueOf(targetDeliveryModeHelper.hasOnlyDigitalDeliveryMode(digitalProduct)))
                .willReturn(Boolean.FALSE);
        given(Boolean.valueOf(targetDeliveryModeHelper.hasOnlyDigitalDeliveryMode(nonDigitalProduct)))
                .willReturn(Boolean.FALSE);
        given(orderProcessModel.getOrder()).willReturn(orderModel);
        splitOrderAction.executeInternal(orderProcessModel);
        verify(targetDeliveryModeHelper, never()).getDigitalDeliveryMode(
                any(AbstractTargetVariantProductModel.class));
    }


}

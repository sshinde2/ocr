package au.com.target.tgtfulfilment.actions;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableSet;

/**
 * Test suite for {@link UpdateAllConsignmentsStatusAction}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UpdateAllConsignmentsStatusActionTest {

    private static final ConsignmentStatus CONSIGNMENT_STATUS = ConsignmentStatus.PICKED;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private UpdateAllConsignmentsStatusAction action = new UpdateAllConsignmentsStatusAction();

    @Mock
    private OrderProcessModel process;
    @Mock
    private OrderModel order;
    @Mock
    private ConsignmentModel consignment;

    /**
     * Initializes test case before run.
     */
    @Before
    public void setUp() {
        when(process.getOrder()).thenReturn(order);
        when(order.getConsignments()).thenReturn(ImmutableSet.of(consignment));

        action.setConsignmentStatus(CONSIGNMENT_STATUS);
    }

    /**
     * Verifies that execution of action will fail if not enough information was provided.
     *
     * @throws Exception if any exception occurs
     */
    @Test
    public void testIllegalArgument() throws Exception {
        when(process.getOrder()).thenReturn(null);
        try {
            action.executeAction(process);
            fail();
        }
        catch (final IllegalArgumentException e) {
            assertTrue(e.getMessage().contains("[Assertion failed]"));
        }
    }

    /**
     * Verifies that actions sets status to all consignments for the order taken
     * from process context.
     *
     * @throws Exception if any exception occurs
     */
    @Test
    public void testSetStatus() throws Exception {
        action.executeAction(process);

        verify(consignment).setStatus(CONSIGNMENT_STATUS);
        verify(modelService).saveAll(order.getConsignments());
    }
}

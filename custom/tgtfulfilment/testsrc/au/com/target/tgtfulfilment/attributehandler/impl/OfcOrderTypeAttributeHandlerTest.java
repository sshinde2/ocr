package au.com.target.tgtfulfilment.attributehandler.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OfcOrderTypeAttributeHandlerTest {

    private static final Integer ORDER_CNC_STORE_NUMBER = Integer.valueOf(1234);

    @InjectMocks
    private final OfcOrderTypeAttributeHandler handler = new OfcOrderTypeAttributeHandler();

    @Mock
    private TargetStoreConsignmentService targetStoreConsignmentService;

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private OrderModel order;

    @Mock
    private TargetZoneDeliveryModeModel targetDeliveryMode;

    @Before
    public void setup() {
        when(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyStore(consignment)))
                .thenReturn(Boolean.TRUE);
        when(consignment.getOrder()).thenReturn(order);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetOfcOrderTypeNullConsignment() {
        handler.get(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetOfcOrderTypeNullOrder() {
        when(consignment.getOrder()).thenReturn(null);

        handler.get(consignment);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetOfcOrderTypeNullDeliveryMode() {
        when(order.getDeliveryMode()).thenReturn(null);

        handler.get(consignment);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetOfcOrderTypeDeliveryModeNotTargetType() {
        when(order.getDeliveryMode()).thenReturn(Mockito.any(ZoneDeliveryModeModel.class));

        handler.get(consignment);
    }

    @Test
    public void testGetOfcOrderTypeNotFulfilledInStore() {
        when(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToAnyStore(consignment)))
                .thenReturn(Boolean.FALSE);

        Assert.assertNull(handler.get(consignment));
    }

    @Test
    public void testGetOfcOrderTypeDeliveryWithNullOrderCncStoreNumber() {
        setupData();
        when(order.getCncStoreNumber()).thenReturn(null);

        Assert.assertEquals(OfcOrderType.CUSTOMER_DELIVERY, handler.get(consignment));
    }

    @Test
    public void testGetOfcOrderTypeDeliveryWithNonDeliveryToStoreMode() {
        setupData();
        when(targetDeliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.FALSE);

        Assert.assertEquals(OfcOrderType.CUSTOMER_DELIVERY, handler.get(consignment));
    }

    @Test
    public void testGetOfcOrderTypeCnc() {
        setupData();
        when(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToStore(consignment,
                ORDER_CNC_STORE_NUMBER.intValue()))).thenReturn(Boolean.TRUE);

        Assert.assertEquals(OfcOrderType.INSTORE_PICKUP, handler.get(consignment));
    }

    @Test
    public void testGetOfcOrderTypeInterstore() {
        setupData();
        when(Boolean.valueOf(targetStoreConsignmentService.isConsignmentAssignedToStore(consignment,
                ORDER_CNC_STORE_NUMBER.intValue()))).thenReturn(Boolean.FALSE);

        Assert.assertEquals(OfcOrderType.INTERSTORE_DELIVERY, handler.get(consignment));
    }

    private void setupData() {
        when(order.getDeliveryMode()).thenReturn(targetDeliveryMode);
        when(targetDeliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);
        when(order.getCncStoreNumber()).thenReturn(ORDER_CNC_STORE_NUMBER);
    }
}

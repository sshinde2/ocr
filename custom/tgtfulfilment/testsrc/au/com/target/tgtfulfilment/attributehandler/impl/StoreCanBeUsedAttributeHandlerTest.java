/**
 * 
 */
package au.com.target.tgtfulfilment.attributehandler.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.sql.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.storeblackout.StoreBlackOutService;


/**
 * @author Siddharam
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StoreCanBeUsedAttributeHandlerTest {

    @InjectMocks
    private final StoreCanBeUsedAttributeHandler storeCanBeUsedAttributeHandler = new StoreCanBeUsedAttributeHandler();

    @Mock
    private StoreBlackOutService storeBlackOutService;

    private final StoreFulfilmentCapabilitiesModel model = new StoreFulfilmentCapabilitiesModel();

    @Before
    public void setUp() throws Exception {
        model.setEnabled(Boolean.TRUE);
        Mockito.when(Boolean.valueOf(storeBlackOutService.doesBlackOutPeriodApply(Mockito.any(Date.class),
                Mockito.any(Date.class)))).thenReturn(Boolean.FALSE);
    }

    @Test
    public void testWhenStoreIsNotEnabled() {
        model.setEnabled(Boolean.FALSE);
        Assert.assertFalse(storeCanBeUsedAttributeHandler.get(model).booleanValue());
    }

    @Test
    public void testWhenStoreIsEnabledAndBlackOutPeriodExists() {
        Mockito.when(Boolean.valueOf(storeBlackOutService.doesBlackOutPeriodApply(Mockito.any(Date.class),
                Mockito.any(Date.class)))).thenReturn(Boolean.TRUE);
        Assert.assertFalse(storeCanBeUsedAttributeHandler.get(model).booleanValue());
    }

    @Test
    public void testWhenStoreIsEnabledAndBlackOutPeriodNotExist() {
        Mockito.when(Boolean.valueOf(storeBlackOutService.doesBlackOutPeriodApply(Mockito.any(Date.class),
                Mockito.any(Date.class)))).thenReturn(Boolean.FALSE);
        Assert.assertTrue(storeCanBeUsedAttributeHandler.get(model).booleanValue());
    }

}

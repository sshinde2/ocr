/**
 * 
 */
package au.com.target.tgtfulfilment.consignment.jobs;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.product.dao.TargetProductSearchServiceDao;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ReservedQtyReconcilationJobTest {

    private static final String QUERY = "SELECT {pk} FROM {AbstractTargetVariantProduct} WHERE {catalogVersion} = ?catalogVersion ORDER BY  {pk} ";

    @Mock
    private TargetProductSearchServiceDao targetProductSearchServiceDao;
    @Mock
    private TargetWarehouseService targetWarehouseService;
    @Mock
    private CronJobModel cj;
    @Mock
    private AbstractTargetVariantProductModel variant1;
    @Mock
    private AbstractTargetVariantProductModel variant2;
    @Mock
    private AbstractTargetVariantProductModel variant3;
    @Mock
    private WarehouseModel warehouse;

    @InjectMocks
    private final ReservedQtyReconcilationJob job = new ReservedQtyReconcilationJob();

    private List<AbstractTargetVariantProductModel> variants;

    @Before
    public void setup() {
        variants = new ArrayList<>();
        variants.add(variant1);
        variants.add(variant2);
        variants.add(variant3);

        BDDMockito.given(targetProductSearchServiceDao.getAllProductVariants(QUERY, 0)).willReturn(variants);
        BDDMockito.given(variant1.getCode()).willReturn("test");
        BDDMockito.given(variant2.getCode()).willReturn("test");
        BDDMockito.given(variant3.getCode()).willReturn("test");
        BDDMockito.given(targetWarehouseService.getDefaultOnlineWarehouse()).willReturn(warehouse);
    }

    @Test
    public void testJobSuccessful() {
        job.perform(cj);
        Mockito.verify(targetProductSearchServiceDao).getAllProductVariants(QUERY, 0);
    }

    @Test
    public void testJobFailed() {
        BDDMockito.given(targetProductSearchServiceDao.getAllProductVariants("fail", 0)).willReturn(variants);
        job.perform(cj);
        Mockito.verify(targetProductSearchServiceDao, Mockito.times(0)).getAllProductVariants("fail", 3);
    }
}

package au.com.target.tgtfulfilment.dao.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.dbformatter.TargetDBSpecificQueryFactory;
import au.com.target.tgtcore.dbformatter.TargetDBSpecificQueryFormatter;
import au.com.target.tgtcore.dbformatter.formatter.TargetHsqlDbQueryFormatter;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.dao.util.TargetFulfilmentDaoUtil;
import au.com.target.tgtfulfilment.warehouseservices.TargetFulfilmentWarehouseService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetConsignmentDaoImplTest {
    @Mock
    private FlexibleSearchService mockFlexibleSearchService;

    @Mock
    private TargetDBSpecificQueryFactory targetDBSpecificQueryFactory;

    @Mock
    private TargetHsqlDbQueryFormatter targetHsqlDbQueryFormatter;

    @Mock
    private TargetFulfilmentDaoUtil<ConsignmentModel> targetFulfilmentDaoUtil;

    @Mock
    private TargetFulfilmentWarehouseService targetFulfilmentWarehouseService;

    @Mock
    private ModelService modelService;

    private WarehouseModel warehouse;

    @InjectMocks
    private final TargetConsignmentDaoImpl targetConsignmentDaoImpl = new TargetConsignmentDaoImpl();

    @Before
    public void setup() {
        BDDMockito.given(targetDBSpecificQueryFactory.getDBSpecificQueryFormatter()).willReturn(
                new TargetHsqlDbQueryFormatter());

        BDDMockito
                .given(targetHsqlDbQueryFormatter.formatDateDiff(TargetDBSpecificQueryFormatter.MINUTES,
                        "{" + TargetConsignmentModel.SENTTOWAREHOUSEDATE + "}"
                        , "?currentDate")).willReturn(" DATEDIFF('mi',{sentToWarehouseDate},?currentDate) ");
        warehouse = new WarehouseModel();

    }

    @Test
    public void testGetAllConsignmentsNotAcknowlegedWithinTimeLimitNoConsignments() throws Exception {
        final ArgumentCaptor<FlexibleSearchQuery> queryCaptor = ArgumentCaptor.forClass(FlexibleSearchQuery.class);

        final SearchResult<TargetConsignmentModel> mockSearchResult = Mockito.mock(SearchResult.class);

        Mockito.doReturn(mockSearchResult).when(mockFlexibleSearchService).search(queryCaptor.capture());
        Mockito.when(targetFulfilmentWarehouseService.getDefaultOnlineWarehouse()).thenReturn(warehouse);

        final List<TargetConsignmentModel> result = targetConsignmentDaoImpl
                .getAllConsignmentsNotAcknowlegedWithinTimeLimit();
        Assertions.assertThat(result).isEmpty();

        final FlexibleSearchQuery query = queryCaptor.getValue();
        verifyQuery(query.getQuery());

        final Map<String, Object> queryParameters = query.getQueryParameters();
        Assertions.assertThat(queryParameters.get("currentDate")).isInstanceOf(Date.class);
        Assertions.assertThat(queryParameters.get("status")).isEqualTo(ConsignmentStatus.SENT_TO_WAREHOUSE);
    }

    @Test
    public void testPerformWithOneConsignment() throws Exception {
        final ArgumentCaptor<FlexibleSearchQuery> queryCaptor = ArgumentCaptor.forClass(FlexibleSearchQuery.class);

        final SearchResult<TargetConsignmentModel> mockSearchResult = Mockito.mock(SearchResult.class);

        final TargetConsignmentModel mockConsignment = Mockito.mock(TargetConsignmentModel.class);
        BDDMockito.given(mockSearchResult.getResult()).willReturn(Collections.singletonList(mockConsignment));
        Mockito.when(targetFulfilmentWarehouseService.getDefaultOnlineWarehouse()).thenReturn(warehouse);

        Mockito.doReturn(mockSearchResult).when(mockFlexibleSearchService).search(queryCaptor.capture());

        final List<TargetConsignmentModel> result = targetConsignmentDaoImpl
                .getAllConsignmentsNotAcknowlegedWithinTimeLimit();
        Assertions.assertThat(result).containsOnly(mockConsignment);

        final FlexibleSearchQuery query = queryCaptor.getValue();
        verifyQuery(query.getQuery());

        final Map<String, Object> queryParameters = query.getQueryParameters();
        Assertions.assertThat(queryParameters.get("currentDate")).isInstanceOf(Date.class);
        Assertions.assertThat(queryParameters.get("status")).isEqualTo(ConsignmentStatus.SENT_TO_WAREHOUSE);
    }

    @Test
    public void testPerformWithTwoConsignments() throws Exception {
        final ArgumentCaptor<FlexibleSearchQuery> queryCaptor = ArgumentCaptor.forClass(FlexibleSearchQuery.class);

        final SearchResult<TargetConsignmentModel> mockSearchResult = Mockito.mock(SearchResult.class);

        final TargetConsignmentModel mockConsignment = Mockito.mock(TargetConsignmentModel.class);
        final TargetConsignmentModel mockConsignment2 = Mockito.mock(TargetConsignmentModel.class);
        Mockito.when(targetFulfilmentWarehouseService.getDefaultOnlineWarehouse()).thenReturn(warehouse);

        final List<TargetConsignmentModel> mockConsignments = new ArrayList<>();
        mockConsignments.add(mockConsignment);
        mockConsignments.add(mockConsignment2);

        BDDMockito.given(mockSearchResult.getResult()).willReturn(mockConsignments);

        Mockito.doReturn(mockSearchResult).when(mockFlexibleSearchService).search(queryCaptor.capture());

        final List<TargetConsignmentModel> result = targetConsignmentDaoImpl
                .getAllConsignmentsNotAcknowlegedWithinTimeLimit();
        Assertions.assertThat(result).containsOnly(mockConsignment, mockConsignment2);

        final FlexibleSearchQuery query = queryCaptor.getValue();
        verifyQuery(query.getQuery());

        final Map<String, Object> queryParameters = query.getQueryParameters();
        Assertions.assertThat(queryParameters.get("currentDate")).isInstanceOf(Date.class);
        Assertions.assertThat(queryParameters.get("status")).isEqualTo(ConsignmentStatus.SENT_TO_WAREHOUSE);
    }

    private void verifyQuery(final String query) {
        Assertions
                .assertThat(query)
                .isEqualTo(
                        "SELECT {pk} FROM {TargetConsignment as cons JOIN TargetCarrier as car ON {car:pk} = {cons:targetCarrier}} "
                                + "WHERE {status} = ?status AND {warehouse} = ?warehouse AND    DATEDIFF('mi',{sentToWarehouseDate},?currentDate)  > "
                                + "{car.resendOrdExtractTimeOut}");
    }
}

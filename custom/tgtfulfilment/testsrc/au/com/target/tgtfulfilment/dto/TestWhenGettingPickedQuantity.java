/**
 * 
 */
package au.com.target.tgtfulfilment.dto;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


/**
 * Test for {@link PickConsignmentUpdateData}
 * 
 */
@UnitTest
public class TestWhenGettingPickedQuantity {

    private PickConsignmentUpdateData updateData;

    private List<PickConfirmEntry> pickEntries;

    @Before
    public void setup() {

        updateData = new PickConsignmentUpdateData();
        pickEntries = new ArrayList<>();
        updateData.setPickEntries(pickEntries);
    }

    @Test
    public void givenNullPickEntries() {

        updateData.setPickEntries(null);
        Assert.assertEquals(0, updateData.getShippedQuantity("product1"));
    }

    @Test
    public void givenEmptyPickEntries() {

        Assert.assertEquals(0, updateData.getShippedQuantity("product1"));
    }

    @Test
    public void givenPickEntryWithNullItemCode() {

        pickEntries.add(createPickConfirmEntry(null, Integer.valueOf(1)));

        Assert.assertEquals(0, updateData.getShippedQuantity("product1"));
    }

    @Test
    public void givenItemCodeNotFound() {

        pickEntries.add(createPickConfirmEntry("product2", Integer.valueOf(1)));

        Assert.assertEquals(0, updateData.getShippedQuantity("product1"));
    }

    @Test
    public void givenItemFoundWithNullQuantity() {

        pickEntries.add(createPickConfirmEntry("product1", null));

        Assert.assertEquals(0, updateData.getShippedQuantity("product1"));
    }

    @Test
    public void givenItemFoundWithPositiveQuantity() {

        pickEntries.add(createPickConfirmEntry("product2", Integer.valueOf(1)));
        pickEntries.add(createPickConfirmEntry("product1", Integer.valueOf(2)));

        Assert.assertEquals(2, updateData.getShippedQuantity("product1"));
    }


    private PickConfirmEntry createPickConfirmEntry(final String itemCode, final Integer quantity) {

        final PickConfirmEntry entry = new PickConfirmEntry();
        entry.setItemCode(itemCode);
        entry.setQuantityShipped(quantity);

        return entry;
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.converters.ConsignmentParcelModelConverter;
import au.com.target.tgtfulfilment.dto.ConsignmentParcelDTO;
import au.com.target.tgtfulfilment.model.ConsignmentParcelModel;


/**
 * Unit test for {@link ConsignmentParcelUpdaterImpl}
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ConsignmentParcelUpdaterTest {

    @Mock
    private ModelService modelService;

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private ConsignmentParcelModelConverter consignmentParcelModelConverter;

    @InjectMocks
    private final ConsignmentParcelUpdaterImpl consignmentParcelUpdater = new ConsignmentParcelUpdaterImpl();

    @Before
    public void setup() {
        consignmentParcelModelConverter.setModelService(modelService);
        consignmentParcelUpdater.setConsignmentParcelModelConverter(consignmentParcelModelConverter);
    }

    @Test
    public void testUpdateParcelCount() {
        final Integer parcelCount = Integer.valueOf(1);

        consignmentParcelUpdater.updateParcelCount(consignment, parcelCount);

        Mockito.verify(consignment).setParcelCount(parcelCount);
        Mockito.verify(modelService).save(consignment);
    }

    @Test
    public void testVerifyConvert() {
        final Set<ConsignmentParcelModel> parcelDetails = new HashSet<>();
        final TargetConsignmentModel consignmentToUpdate = new TargetConsignmentModel();
        consignmentToUpdate.setParcelsDetails(parcelDetails);
        final ConsignmentParcelModel consignmentParcelModel = new ConsignmentParcelModel();
        BDDMockito.given(modelService.create(ConsignmentParcelModel.class)).willReturn(
                consignmentParcelModel);
        final ConsignmentParcelDTO parcel = createParcelWithMandatoryValues();
        final List<ConsignmentParcelDTO> parcels = new ArrayList<>();
        parcels.add(parcel);
        BDDMockito.given(consignmentParcelModelConverter.convert(parcel)).willReturn(consignmentParcelModel);
        consignmentParcelUpdater.updateParcelDetails(consignmentToUpdate, parcels);
        Mockito.verify(consignmentParcelModelConverter).convert(parcel);
        Mockito.verify(modelService).save(consignmentParcelModel);
        Mockito.verify(modelService).refresh(consignmentToUpdate);
    }

    private ConsignmentParcelDTO createParcelWithMandatoryValues() {
        final ConsignmentParcelDTO parcel = new ConsignmentParcelDTO();
        parcel.setLength("10.00");
        parcel.setWidth("10.00");
        parcel.setHeight("10.00");
        parcel.setWeight("1001.00");
        return parcel;
    }
}

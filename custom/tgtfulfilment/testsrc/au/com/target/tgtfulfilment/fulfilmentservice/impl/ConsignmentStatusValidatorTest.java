/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtfulfilment.exceptions.ConsignmentStatusValidationException;


/**
 * @author sbryan6
 *
 */
@UnitTest
public class ConsignmentStatusValidatorTest {

    private final ConsignmentStatusValidatorImpl validator = new ConsignmentStatusValidatorImpl();

    @Before
    public void setup() {

        validator.setConsignmentStatusMap(createConsignmentStatusMap());
    }

    @Test
    public void testConfirmingAllowed() throws ConsignmentStatusValidationException {

        validator.validateConsignmentStatus(ConsignmentStatus.SENT_TO_WAREHOUSE,
                ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
    }

    @Test(expected = ConsignmentStatusValidationException.class)
    public void testConfirmingNotAllowed() throws ConsignmentStatusValidationException {

        validator.validateConsignmentStatus(ConsignmentStatus.PICKED, ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
    }

    @Test(expected = ConsignmentStatusValidationException.class)
    public void testConfirmingNotFoundInMap() throws ConsignmentStatusValidationException {

        validator.validateConsignmentStatus(ConsignmentStatus.PICKED, ConsignmentStatus.PACKED);
    }



    private Map<ConsignmentStatus, List<ConsignmentStatus>> createConsignmentStatusMap() {

        final Map<ConsignmentStatus, List<ConsignmentStatus>> map = new HashMap<ConsignmentStatus, List<ConsignmentStatus>>();

        final List<ConsignmentStatus> allowedForConfirm = new ImmutableList.Builder<ConsignmentStatus>()
                .add(ConsignmentStatus.SENT_TO_WAREHOUSE)
                .build();

        map.put(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, allowedForConfirm);

        final List<ConsignmentStatus> allowedForPick = new ImmutableList.Builder<ConsignmentStatus>()
                .add(ConsignmentStatus.SENT_TO_WAREHOUSE)
                .add(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE)
                .add(ConsignmentStatus.PICKED)
                .build();

        map.put(ConsignmentStatus.PICKED, allowedForPick);

        return map;
    }

}

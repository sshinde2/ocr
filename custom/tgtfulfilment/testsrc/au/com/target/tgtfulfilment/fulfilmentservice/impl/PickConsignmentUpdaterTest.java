/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.dto.PickConfirmEntry;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.helper.TargetFulfilmentHelper;


/**
 * Unit test for {@link PickConsignmentUpdaterImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PickConsignmentUpdaterTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Mock
    private ModelService modelService;

    @Mock
    private TargetFulfilmentHelper targetFulfilmentHelper;

    @InjectMocks
    private final PickConsignmentUpdaterImpl pickConsignmentUpdater = new PickConsignmentUpdaterImpl();

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private ConsignmentEntryModel consignmentEntry;

    @Mock
    private AbstractOrderEntryModel orderEntry;

    @Mock
    private ProductModel product;


    private List<PickConfirmEntry> pickConfirmEntries;

    private PickConfirmEntry pce;


    @Before
    public void setup() {
        pickConfirmEntries = new ArrayList<>();
    }

    private void addPickEntry(final String itemCode, final Integer quantity) {
        pce = new PickConfirmEntry();
        pce.setItemCode(itemCode);
        pce.setQuantityShipped(quantity);

        pickConfirmEntries.add(pce);
    }

    private void addConsignmentEntry() {

        // consignment has one entry with 2 x product "product1"
        BDDMockito.given(targetFulfilmentHelper.getConsignmentEntryForItemCode(consignment, "product1"))
                .willReturn(consignmentEntry);
        BDDMockito.given(consignmentEntry.getQuantity()).willReturn(new Long(2));
    }

    @Test
    public void testUpdateConsignmentShipQuantitiesEmptyConsignment() throws FulfilmentException {

        pickConsignmentUpdater.updateConsignmentShipQuantities(consignment, pickConfirmEntries);
        checkNoConsignmentEntryUpdate();
    }

    @Test
    public void testUpdateConsignmentShipQuantitiesEmptyPick() throws FulfilmentException {

        addConsignmentEntry();

        pickConsignmentUpdater.updateConsignmentShipQuantities(consignment, pickConfirmEntries);
        checkNoConsignmentEntryUpdate();
    }

    @Test
    public void testUpdateConsignmentShipQuantitiesNoMatch() throws FulfilmentException {

        addConsignmentEntry();
        addPickEntry("product2", Integer.valueOf(2));

        pickConsignmentUpdater.updateConsignmentShipQuantities(consignment, pickConfirmEntries);
        checkNoConsignmentEntryUpdate();
    }

    @Test
    public void testUpdateConsignmentShipQuantitiesOver() throws FulfilmentException {

        // We now allow consignment to have overpick values set

        addConsignmentEntry();
        addPickEntry("product1", Integer.valueOf(3));

        pickConsignmentUpdater.updateConsignmentShipQuantities(consignment, pickConfirmEntries);

        Mockito.verify(consignmentEntry).setShippedQuantity(Long.valueOf(3l));
    }

    @Test
    public void testUpdateConsignmentShipQuantities() throws FulfilmentException {

        addConsignmentEntry();
        addPickEntry("product1", Integer.valueOf(2));

        pickConsignmentUpdater.updateConsignmentShipQuantities(consignment, pickConfirmEntries);

        Mockito.verify(consignmentEntry).setShippedQuantity(Long.valueOf(2l));
    }

    private void checkNoConsignmentEntryUpdate() {
        Mockito.verify(consignmentEntry, Mockito.never()).setShippedQuantity(Mockito.any(Long.class));
    }

    @Test
    public void testUpdateConsignmentDetails() {

        final Integer parcelCount = Integer.valueOf(1);
        final String trackingNumber = "123";
        final String carrier = "Aus Post";

        pickConsignmentUpdater.updateConsignmentDetails(consignment, parcelCount, trackingNumber, carrier);

        Mockito.verify(consignment).setParcelCount(parcelCount);
        Mockito.verify(consignment).setCarrier(carrier);
        Mockito.verify(consignment).setTrackingID(trackingNumber);
        Mockito.verify(modelService).save(consignment);
    }
}

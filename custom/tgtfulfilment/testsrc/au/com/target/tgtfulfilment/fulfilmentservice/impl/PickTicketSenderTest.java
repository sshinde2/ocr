package au.com.target.tgtfulfilment.fulfilmentservice.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;


/**
 * Unit test for {@link PickTicketSenderImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PickTicketSenderTest {
    @Mock
    private TargetTicketBusinessService targetTicketBusinessService;

    @InjectMocks
    private final PickTicketSenderImpl pickTicketSender = new PickTicketSenderImpl();

    @Mock
    private OrderModel order;

    @Mock
    private ConsignmentModel consignment;

    @Test
    public void testSendTicketInvalidPick() {

        pickTicketSender.sendTicketInvalidPick(order, "ERROR");
        Mockito.verify(targetTicketBusinessService).createCsAgentTicket(Mockito.anyString(), Mockito.anyString(),
                Mockito.eq("ERROR"), Mockito.eq(order));
    }

    @Test
    public void testSendTicketOverPick() {

        final String errorMessage = "Over pick received for an order that does not allow them";

        pickTicketSender.sendTicketOverPick(order, consignment, null);
        Mockito.verify(targetTicketBusinessService).createCsAgentTicket(Mockito.anyString(), Mockito.anyString(),
                Mockito.startsWith(errorMessage), Mockito.eq(order));
    }

    @Test
    public void testSendTicketShortZeroPick() {

        final String errorMessage = "Short or Zero pick received for an order that does not allow them";

        pickTicketSender.sendTicketShortZeroPick(order, consignment, null);
        Mockito.verify(targetTicketBusinessService).createCsAgentTicket(Mockito.anyString(), Mockito.anyString(),
                Mockito.startsWith(errorMessage), Mockito.eq(order));
    }

    @Test
    public void testSendTicketPinpadShortZeroPick() {

        final String errorMessage = "Short or Zero pick received for an order that does not allow them";
        Mockito.when(order.getSalesApplication()).thenReturn(SalesApplication.KIOSK);
        pickTicketSender.sendTicketShortZeroPick(order, consignment, null);
        Mockito.verify(targetTicketBusinessService, Mockito.times(1)).createCsAgentTicket(
                Mockito.eq(PickTicketSenderImpl.SHORT_ZERO_PINPAD_TICKET_HEADLINE),
                Mockito.eq(PickTicketSenderImpl.SHORT_ZERO_PINPAD_TICKET_SUBJECT),
                Mockito.startsWith(errorMessage), Mockito.eq(order));
    }

    @Test
    public void testSendTicketTradeMeShortZeroPick() {

        final String errorMessage = "Short or Zero pick received for an order that does not allow them";
        Mockito.when(order.getSalesApplication()).thenReturn(SalesApplication.TRADEME);
        pickTicketSender.sendTicketShortZeroPick(order, consignment, null);
        Mockito.verify(targetTicketBusinessService, Mockito.times(1)).createCsAgentTicket(
                Mockito.eq(PickTicketSenderImpl.ZERO_TRADEME_TICKET_HEADLINE),
                Mockito.eq(PickTicketSenderImpl.ZERO_TRADEME_TICKET_SUBJECT),
                Mockito.startsWith(errorMessage), Mockito.eq(order));
    }

    @Test
    public void testSendTicketeBayShortZeroPick() {

        final String errorMessage = "Short or Zero pick received for an order that does not allow them";
        Mockito.when(order.getSalesApplication()).thenReturn(SalesApplication.EBAY);
        pickTicketSender.sendTicketShortZeroPick(order, consignment, null);
        Mockito.verify(targetTicketBusinessService, Mockito.times(1)).createCsAgentTicket(
                Mockito.eq(PickTicketSenderImpl.ZERO_EBAY_TICKET_HEADLINE),
                Mockito.eq(PickTicketSenderImpl.ZERO_EBAY_TICKET_SUBJECT),
                Mockito.startsWith(errorMessage), Mockito.eq(order));
    }


    @Test
    public void testSendTicketWebShortZeroPick() {
        final String errorMessage = "Short or Zero pick received for an order that does not allow them";
        Mockito.when(order.getSalesApplication()).thenReturn(SalesApplication.WEB);
        pickTicketSender.sendTicketShortZeroPick(order, consignment, null);
        Mockito.verify(targetTicketBusinessService, Mockito.times(1)).createCsAgentTicket(
                Mockito.eq(PickTicketSenderImpl.SHORT_ZERO_TICKET_HEADLINE),
                Mockito.eq(PickTicketSenderImpl.SHORT_ZERO_TICKET_SUBJECT),
                Mockito.startsWith(errorMessage), Mockito.eq(order));
    }

    @Test
    public void testSendTicketFailedRefund() {

        final String errorMessage = "Refund failed";

        pickTicketSender.sendTicketFailedRefund(order, errorMessage);
        Mockito.verify(targetTicketBusinessService).createCsAgentTicket(Mockito.anyString(), Mockito.anyString(),
                Mockito.startsWith(errorMessage), Mockito.eq(order));
    }


}

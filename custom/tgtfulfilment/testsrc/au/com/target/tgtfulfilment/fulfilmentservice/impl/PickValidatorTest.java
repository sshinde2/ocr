/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.dto.PickConfirmEntry;
import au.com.target.tgtfulfilment.helper.TargetFulfilmentHelper;


/**
 * Unit test for {@link PickValidatorImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PickValidatorTest {

    @InjectMocks
    private final PickValidatorImpl pickValidator = new PickValidatorImpl();

    @Mock
    private ConsignmentModel consignment;

    @Mock
    private ConsignmentEntryModel consignmentEntry;

    @Mock
    private AbstractOrderEntryModel orderEntry;

    @Mock
    private ProductModel product;

    @Mock
    private TargetFulfilmentHelper targetFulfilmentHelper;

    private List<PickConfirmEntry> pickConfirmEntries;

    private PickConfirmEntry pce;

    @Before
    public void setup() {
        pickConfirmEntries = new ArrayList<>();
    }

    private void addPickEntry(final String itemCode, final int quantity) {
        addPickEntry(itemCode, Integer.valueOf(quantity));
    }

    private void addPickEntry(final String itemCode, final Integer quantity) {
        pce = new PickConfirmEntry();
        pce.setItemCode(itemCode);
        pce.setQuantityShipped(quantity);

        pickConfirmEntries.add(pce);
    }


    private void addConsignmentEntry() {

        // consignment has one entry with 2 x product "product1"
        BDDMockito.given(consignment.getCode()).willReturn("consignment1");
        BDDMockito.given(consignment.getConsignmentEntries()).willReturn(Collections.singleton(consignmentEntry));

        BDDMockito.given(consignmentEntry.getOrderEntry()).willReturn(orderEntry);

        BDDMockito.given(orderEntry.getProduct()).willReturn(product);

        BDDMockito.given(consignmentEntry.getQuantity()).willReturn(new Long(2));

        BDDMockito.given(product.getCode()).willReturn("product1");

    }

    @Test
    public void testConsignmentContainsItemEmptyConsignment() {

        final boolean contains = pickValidator.consignmentContainsItem(consignment, "product1");
        Assert.assertFalse(contains);
    }

    @Test
    public void testConsignmentContainsItemDifferentProduct() {

        addConsignmentEntry();

        final boolean contains = pickValidator.consignmentContainsItem(consignment, "product2");
        Assert.assertFalse(contains);
    }

    @Test
    public void testConsignmentContainsItemNullOrderEntry() {

        addConsignmentEntry();
        BDDMockito.given(consignmentEntry.getOrderEntry()).willReturn(null);

        final boolean contains = pickValidator.consignmentContainsItem(consignment, "product1");
        Assert.assertFalse(contains);
    }

    @Test
    public void testConsignmentContainsItemNullProductCode() {

        addConsignmentEntry();
        BDDMockito.given(product.getCode()).willReturn(null);

        final boolean contains = pickValidator.consignmentContainsItem(consignment, "product1");
        Assert.assertFalse(contains);
    }


    @Test
    public void testConsignmentContainsItem() {

        addConsignmentEntry();
        BDDMockito.given(targetFulfilmentHelper.getConsignmentEntryForItemCode(consignment, "product1")).willReturn(
                consignmentEntry);
        final boolean contains = pickValidator.consignmentContainsItem(consignment, "product1");

        Assert.assertTrue(contains);
    }

    @Test
    public void testConsignmentItemQuantityEmptyConsignment() {

        final long quantity = pickValidator.getConsignmentItemQuantity(consignment, "product1");


        Assert.assertEquals(0, quantity);
    }

    @Test
    public void testConsignmentItemQuantityDifferentProduct() {

        addConsignmentEntry();

        final long quantity = pickValidator.getConsignmentItemQuantity(consignment, "product2");
        Assert.assertEquals(0, quantity);
    }

    @Test
    public void testConsignmentItemQuantity() {

        addConsignmentEntry();
        BDDMockito.given(targetFulfilmentHelper.getConsignmentEntryForItemCode(consignment, "product1")).willReturn(
                consignmentEntry);
        final long quantity = pickValidator.getConsignmentItemQuantity(consignment, "product1");
        Assert.assertEquals(2, quantity);
    }


    @Test
    public void testIsInvalidPickNullPickList() {

        addConsignmentEntry();

        final String invalidMessage = pickValidator.isInvalidPick(consignment, null);
        Assert.assertNotNull(invalidMessage);
        Assert.assertEquals("Pick Entries are Empty", invalidMessage);
    }

    @Test
    public void testIsInvalidPickEmptyPickList() {

        addConsignmentEntry();

        final String invalidMessage = pickValidator.isInvalidPick(consignment, pickConfirmEntries);
        Assert.assertNotNull(invalidMessage);
        Assert.assertEquals("Pick Entries are Empty", invalidMessage);
    }

    @Test
    public void testIsInvalidPickNullPickItem() {

        addConsignmentEntry();
        addPickEntry(null, 2);

        final String invalidMessage = pickValidator.isInvalidPick(consignment, pickConfirmEntries);
        Assert.assertNotNull(invalidMessage);
        Assert.assertTrue(invalidMessage.startsWith("Pick entry has null code"));
    }

    @Test
    public void testIsInvalidPickNullQuantity() {

        addConsignmentEntry();
        addPickEntry("product1", null);

        final String invalidMessage = pickValidator.isInvalidPick(consignment, pickConfirmEntries);
        Assert.assertNotNull(invalidMessage);
        Assert.assertTrue(invalidMessage.startsWith("Pick entry has null quantity"));
    }

    @Test
    public void testIsInvalidPickFullPickItem() {

        addConsignmentEntry();
        addPickEntry("product1", 2);
        BDDMockito.given(targetFulfilmentHelper.getConsignmentEntryForItemCode(consignment, "product1")).willReturn(
                consignmentEntry);
        final String invalidMessage = pickValidator.isInvalidPick(consignment, pickConfirmEntries);
        Assert.assertNull(invalidMessage);
    }

    @Test
    public void testIsInvalidPickRepeatedItem() {

        addConsignmentEntry();
        addPickEntry("product1", 1);
        addPickEntry("product1", 1);

        final String invalidMessage = pickValidator.isInvalidPick(consignment, pickConfirmEntries);
        Assert.assertNotNull(invalidMessage);
        Assert.assertTrue(invalidMessage.startsWith("Repeated pick entry for item code"));
    }

    @Test
    public void testIsOverPickNullPickList() {

        addConsignmentEntry();

        final boolean over = pickValidator.isOverPick(consignment, pickConfirmEntries);
        Assert.assertFalse(over);
    }

    @Test
    public void testIsOverPickEmptyPickList() {

        addConsignmentEntry();

        final boolean over = pickValidator.isOverPick(consignment, pickConfirmEntries);
        Assert.assertFalse(over);
    }

    @Test
    public void testIsOverPickWrongItem() {

        addConsignmentEntry();
        addPickEntry("product2", 2);
        BDDMockito.given(targetFulfilmentHelper.getConsignmentEntryForItemCode(consignment, "product1")).willReturn(
                consignmentEntry);

        final boolean over = pickValidator.isOverPick(consignment, pickConfirmEntries);
        Assert.assertTrue(over);
    }

    @Test
    public void testIsOverPickMatchingItemLowerQuantity() {

        addConsignmentEntry();
        addPickEntry("product1", 1);
        BDDMockito.given(targetFulfilmentHelper.getConsignmentEntryForItemCode(consignment, "product1")).willReturn(
                consignmentEntry);
        final boolean over = pickValidator.isOverPick(consignment, pickConfirmEntries);
        Assert.assertFalse(over);
    }

    @Test
    public void testIsOverPickMatchingItemEqualQuantity() {

        addConsignmentEntry();
        addPickEntry("product1", 2);
        BDDMockito.given(targetFulfilmentHelper.getConsignmentEntryForItemCode(consignment, "product1")).willReturn(
                consignmentEntry);
        final boolean over = pickValidator.isOverPick(consignment, pickConfirmEntries);
        Assert.assertFalse(over);
    }

    @Test
    public void testIsOverPickMatchingItemOverQuantity() {

        addConsignmentEntry();
        addPickEntry("product1", 3);
        BDDMockito.given(targetFulfilmentHelper.getConsignmentEntryForItemCode(consignment, "product1")).willReturn(
                consignmentEntry);
        final boolean over = pickValidator.isOverPick(consignment, pickConfirmEntries);
        Assert.assertTrue(over);
    }


}

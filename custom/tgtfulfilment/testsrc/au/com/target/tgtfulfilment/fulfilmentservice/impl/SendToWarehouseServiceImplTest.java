/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.service.SendToWarehouseSelectorStrategy;
import au.com.target.tgtfulfilment.service.impl.RestSendToWarehouseProtocol;
import au.com.target.tgtfulfilment.service.impl.SendToWarehouseServiceImpl;


/**
 * @author mjanarth
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendToWarehouseServiceImplTest {

    @Mock
    private SendToWarehouseSelectorStrategy sendToWarehouseSelectorStrategy;

    @Mock
    private RestSendToWarehouseProtocol restSendToWarehouseProtocol;

    @Mock
    private TargetConsignmentModel mockConsignment;

    @InjectMocks
    private final SendToWarehouseServiceImpl service = new SendToWarehouseServiceImpl();

    private final List<TargetConsignmentModel> consignments = new ArrayList<TargetConsignmentModel>();

    @Before
    public void setup() {
        given(sendToWarehouseSelectorStrategy.getSendToWarehouseProtocolForConsignment(mockConsignment))
                .willReturn(
                        restSendToWarehouseProtocol);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testsendUnshippedConsignemntsWithEmptyConsignments() {
        service.sendUnshippedConsignemnts(new ArrayList<TargetConsignmentModel>());
        verifyZeroInteractions(sendToWarehouseSelectorStrategy);
        verifyZeroInteractions(restSendToWarehouseProtocol);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testsendUnshippedConsignemntsWithNullConsignments() {
        service.sendUnshippedConsignemnts(null);
        verifyZeroInteractions(sendToWarehouseSelectorStrategy);
        verifyZeroInteractions(restSendToWarehouseProtocol);
    }

    @Test
    public void testsendUnshippedConsignemntsWithConsignments() {
        consignments.add(mockConsignment);
        service.sendUnshippedConsignemnts(consignments);
        verify(sendToWarehouseSelectorStrategy).getSendToWarehouseProtocolForConsignment(mockConsignment);
        verify(restSendToWarehouseProtocol).sendUnshippedConsignments(consignments);
    }


}

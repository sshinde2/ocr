/**
 * 
 */
package au.com.target.tgtfulfilment.fulfilmentservice.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;


/**
 * Unit test for {@link TicketSenderImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TicketSenderImplTest {

    @Mock
    private TargetTicketBusinessService targetTicketBusinessService;

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private OEGParameterHelper oegParameterHelper;

    @Mock
    private OrderModel order;

    @InjectMocks
    private final TicketSenderImpl ticketSender = new TicketSenderImpl();

    @Test
        public void testSendStockNotInDefaultWarehouse() {
            Mockito.when(oegParameterHelper.getOrderFromGivenGroup(oeg)).thenReturn(order);
            ticketSender.sendStockNotInDefaultWarehouse(oeg);
            Mockito.verify(targetTicketBusinessService).createCsAgentTicket(Mockito.anyString(),
                    Mockito.anyString(), Mockito.anyString(), Mockito.any(OrderModel.class));
        }


    @Test
    public void testSendRoutingException() {
        Mockito.when(oegParameterHelper.getOrderFromGivenGroup(oeg)).thenReturn(order);
        ticketSender.sendRoutingException(oeg);
        Mockito.verify(targetTicketBusinessService).createCsAgentTicket(Mockito.anyString(),
                Mockito.anyString(), Mockito.anyString(), Mockito.any(OrderModel.class));
    }

}

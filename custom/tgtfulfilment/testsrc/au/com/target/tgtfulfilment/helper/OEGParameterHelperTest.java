package au.com.target.tgtfulfilment.helper;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper.ParameterName;

import com.google.common.collect.ImmutableList;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OEGParameterHelperTest {

    private static final Integer FULFILMENT_STORE_NUMBER = Integer.valueOf(4321);
    private static final Integer CNC_STORE_NUMBER = Integer.valueOf(1234);
    private static final String ORDER_CODE = "testOrder";

    @InjectMocks
    private final OEGParameterHelper oegParameterHelper = new OEGParameterHelper();

    private final OrderEntryGroup orderEntryGroup = new OrderEntryGroup();

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private TargetPointOfServiceModel tpos;

    @Mock
    private WarehouseModel warehouse;

    @Mock
    private TargetZoneDeliveryModeModel deliveryMode;

    @Mock
    private AddressModel deliveryAddress;

    @Test
    public void testPopulateOEGParamsFromOrder() {

        final OrderModel order = new OrderModel();
        order.setCode(ORDER_CODE);
        order.setCncStoreNumber(CNC_STORE_NUMBER);
        order.setDeliveryAddress(deliveryAddress);
        order.setDeliveryMode(deliveryMode);

        oegParameterHelper.populateOEGParamsFromOrder(orderEntryGroup, order);

        Assert.assertEquals(deliveryAddress, oegParameterHelper.getDeliveryAddress(orderEntryGroup));
        Assert.assertEquals(deliveryMode, oegParameterHelper.getDeliveryMode(orderEntryGroup));
        Assert.assertEquals(CNC_STORE_NUMBER, oegParameterHelper.getCncStoreNumber(orderEntryGroup));
        Assert.assertEquals(ORDER_CODE, oegParameterHelper.getOrderCode(orderEntryGroup));
    }

    @Test
    public void testGetNullValues() {
        Assert.assertNull(oegParameterHelper.getOrderCode(orderEntryGroup));
        Assert.assertNull(oegParameterHelper.getCncStoreNumber(orderEntryGroup));
        Assert.assertNull(oegParameterHelper.getDeliveryAddress(orderEntryGroup));
        Assert.assertNull(oegParameterHelper.getDeliveryMode(orderEntryGroup));
        Assert.assertNull(oegParameterHelper.getFulfilmentWarehouse(orderEntryGroup));
    }

    @Test
    public void testIsOEGAssignedFalse() {

        oegParameterHelper.assignWarehouseToOEG(orderEntryGroup, null);
        Assert.assertFalse(oegParameterHelper.isOEGAssigned(orderEntryGroup));
    }

    @Test
    public void testIsOEGAssignedTrue() {

        oegParameterHelper.assignWarehouseToOEG(orderEntryGroup, new WarehouseModel());
        Assert.assertTrue(oegParameterHelper.isOEGAssigned(orderEntryGroup));
    }

    @Test
    public void testAssignTposToOEGWithWarehouse() {

        Mockito.when(targetWarehouseService.getWarehouseForPointOfService(tpos)).thenReturn(warehouse);
        oegParameterHelper.assignTposToOEG(orderEntryGroup, tpos);

        Assert.assertEquals(warehouse, oegParameterHelper.getFulfilmentWarehouse(orderEntryGroup));
    }

    @Test
    public void testAssignTposToOEGWithoutWarehouse() {

        Mockito.when(targetWarehouseService.getWarehouseForPointOfService(tpos)).thenReturn(null);
        oegParameterHelper.assignTposToOEG(orderEntryGroup, tpos);

        Assert.assertNull(oegParameterHelper.getFulfilmentWarehouse(orderEntryGroup));
    }

    @Test
    public void testGetOrderTypeInstorePickup() {

        populateParamsForOrderType();
        when(deliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);

        Assert.assertEquals(OfcOrderType.INSTORE_PICKUP,
                oegParameterHelper.getOfcOrderType(orderEntryGroup, CNC_STORE_NUMBER));
    }

    @Test
    public void testGetOrderTypeInterstoreDelivery() {

        populateParamsForOrderType();
        when(deliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);

        Assert.assertEquals(OfcOrderType.INTERSTORE_DELIVERY,
                oegParameterHelper.getOfcOrderType(orderEntryGroup, FULFILMENT_STORE_NUMBER));
    }

    @Test
    public void testGetOrderTypeCustomerDelivery() {

        populateParamsForOrderType();
        when(deliveryMode.getIsDeliveryToStore()).thenReturn(Boolean.FALSE);

        Assert.assertEquals(OfcOrderType.CUSTOMER_DELIVERY,
                oegParameterHelper.getOfcOrderType(orderEntryGroup, FULFILMENT_STORE_NUMBER));
    }

    @Test
    public void testGetOrderFromGivenGroupNull() {
        Assert.assertNull(oegParameterHelper.getOrderFromGivenGroup(orderEntryGroup));
    }

    @Test
    public void testGetOrderFromGivenGroupEntryButNoOrder() {
        final OrderEntryModel orderEntry = new OrderEntryModel();
        orderEntryGroup.add(orderEntry);
        Assert.assertNull(oegParameterHelper.getOrderFromGivenGroup(orderEntryGroup));
    }

    @Test
    public void testGetOrderFromGivenGroupEntryWithOrder() {
        final OrderEntryModel orderEntry = new OrderEntryModel();
        final OrderModel order = new OrderModel();
        orderEntry.setOrder(order);
        orderEntryGroup.add(orderEntry);

        final OrderModel actualOrder = oegParameterHelper.getOrderFromGivenGroup(orderEntryGroup);
        Assert.assertNotNull(actualOrder);
        Assert.assertEquals(order, actualOrder);
    }

    @Test
    public void testContainsFastlineOEG() {
        final OrderEntryGroup oeg = mock(OrderEntryGroup.class);
        given(oeg.getParameter(ParameterName.FULFILMENT_WAREHOUSE.name())).willReturn(null);
        final List<OrderEntryGroup> splittedOEGs = ImmutableList.of(oeg);

        final boolean result = oegParameterHelper.containsFastlineOEG(splittedOEGs);
        assertThat(result).isTrue();
    }

    @Test
    public void testContainsFastlineOEGWithoutFastlineAssignment() {
        final OrderEntryGroup oeg = mock(OrderEntryGroup.class);
        given(oeg.getParameter(ParameterName.FULFILMENT_WAREHOUSE.name())).willReturn(warehouse);
        final List<OrderEntryGroup> splittedOEGs = ImmutableList.of(oeg);

        final boolean result = oegParameterHelper.containsFastlineOEG(splittedOEGs);
        assertThat(result).isFalse();
    }

    private void populateParamsForOrderType() {

        oegParameterHelper.setDeliveryMode(orderEntryGroup, deliveryMode);
        oegParameterHelper.setCncStoreNumber(orderEntryGroup, CNC_STORE_NUMBER);
        oegParameterHelper.setOrderCode(orderEntryGroup, ORDER_CODE);
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.helper;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;


/**
 * @author asingh78
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFulfilmentHelperTest {
    @InjectMocks
    private final TargetFulfilmentHelper targetFulfilmentHelper = new TargetFulfilmentHelper();


    @Mock
    private ConsignmentModel consignment;

    @Mock
    private ConsignmentEntryModel consignmentEntry;

    @Mock
    private AbstractOrderEntryModel orderEntry;

    @Mock
    private SessionService sessionService;
    @Mock
    private ProductModel product;

    @Before
    public void setup() {
        BDDMockito.when(sessionService.executeInLocalView(Mockito.<SessionExecutionBody> any())).thenAnswer(
                new Answer() {

                    @Override
                    public Object answer(final InvocationOnMock invocation) {
                        final Object[] args = invocation.getArguments();
                        final SessionExecutionBody arg = (SessionExecutionBody)args[0];
                        return arg.execute();
                    }
                });
    }

    @Test
    public void testgetConsignmentEntryForItemCodeWithNullConsignmentEntry() {
        BDDMockito.given(consignment.getCode()).willReturn("consignment1");
        BDDMockito.given(consignment.getConsignmentEntries()).willReturn(null);


        final ConsignmentEntryModel contains = targetFulfilmentHelper.getConsignmentEntryForItemCode(consignment,
                "product1");
        Assert.assertNull(contains);
    }

    @Test
    public void testgetConsignmentEntryForItemCodeWithNullOrderEntery() {

        BDDMockito.given(consignment.getCode()).willReturn("consignment1");
        BDDMockito.given(consignment.getConsignmentEntries()).willReturn(Collections.singleton(consignmentEntry));

        BDDMockito.given(consignmentEntry.getOrderEntry()).willReturn(null);

        final ConsignmentEntryModel contains = targetFulfilmentHelper.getConsignmentEntryForItemCode(consignment,
                "product1");
        Assert.assertNull(contains);
    }

    @Test
    public void testgetConsignmentEntryForItemCodeWithNullProduct() {

        BDDMockito.given(consignment.getCode()).willReturn("consignment1");
        BDDMockito.given(consignment.getConsignmentEntries()).willReturn(Collections.singleton(consignmentEntry));

        BDDMockito.given(consignmentEntry.getOrderEntry()).willReturn(orderEntry);

        BDDMockito.given(orderEntry.getProduct()).willReturn(null);


        final ConsignmentEntryModel contains = targetFulfilmentHelper.getConsignmentEntryForItemCode(consignment,
                "product1");
        Assert.assertNull(contains);
    }

    @Test
    public void testgetConsignmentEntryForItemCodeWhenEveryThingFine() {

        BDDMockito.given(consignment.getCode()).willReturn("consignment1");
        BDDMockito.given(consignment.getConsignmentEntries()).willReturn(Collections.singleton(consignmentEntry));

        BDDMockito.given(consignmentEntry.getOrderEntry()).willReturn(orderEntry);

        BDDMockito.given(orderEntry.getProduct()).willReturn(product);

        BDDMockito.given(consignmentEntry.getQuantity()).willReturn(new Long(2));

        BDDMockito.given(product.getCode()).willReturn("product1");

        final ConsignmentEntryModel contains = targetFulfilmentHelper.getConsignmentEntryForItemCode(consignment,
                "product1");
        Assert.assertNotNull(contains);
    }

    @Test
    public void testGetConsignmentEntryItemCodeNullOrderEntry() {

        BDDMockito.given(consignmentEntry.getOrderEntry()).willReturn(null);

        final String itemCode = targetFulfilmentHelper.getConsignmentEntryItemCode(consignmentEntry);
        Assert.assertNull(itemCode);
    }

    @Test
    public void testGetConsignmentEntryItemCodeNullProduct() {

        BDDMockito.given(consignmentEntry.getOrderEntry()).willReturn(orderEntry);
        BDDMockito.given(orderEntry.getProduct()).willReturn(null);

        final String itemCode = targetFulfilmentHelper.getConsignmentEntryItemCode(consignmentEntry);
        Assert.assertNull(itemCode);
    }

    @Test
    public void testGetConsignmentEntryItemCode() {

        BDDMockito.given(consignmentEntry.getOrderEntry()).willReturn(orderEntry);
        BDDMockito.given(orderEntry.getProduct()).willReturn(product);
        BDDMockito.given(product.getCode()).willReturn("product1");

        final String itemCode = targetFulfilmentHelper.getConsignmentEntryItemCode(consignmentEntry);
        Assert.assertNotNull(itemCode);
        Assert.assertEquals("product1", itemCode);
    }

}

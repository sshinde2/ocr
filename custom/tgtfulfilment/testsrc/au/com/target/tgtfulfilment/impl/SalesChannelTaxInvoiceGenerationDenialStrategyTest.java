/**
 * 
 */
package au.com.target.tgtfulfilment.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.salesapplication.TargetFulfilmentSalesApplicationConfigService;


/**
 * Unit test for {@link SalesChannelTaxInvoiceGenerationDenialStrategy}
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SalesChannelTaxInvoiceGenerationDenialStrategyTest {

    @InjectMocks
    private final SalesChannelTaxInvoiceGenerationDenialStrategy strategy = new SalesChannelTaxInvoiceGenerationDenialStrategy();

    @Mock
    private TargetFulfilmentSalesApplicationConfigService targetFulfilmentSalesApplicationConfigService;

    @Test
    public void testIsDeniedSalesAppNull() {


        final OrderModel order = Mockito.mock(OrderModel.class);

        BDDMockito.given(order.getSalesApplication()).willReturn(null);
        BDDMockito.given(
                Boolean.valueOf(targetFulfilmentSalesApplicationConfigService.isSuppressTaxInvoice(order
                        .getSalesApplication()))).willReturn(Boolean.FALSE);
        Assert.assertFalse(strategy.isDenied(order));
    }

    @Test
    public void testIsDeniedSalesAppEbay() {
        final OrderModel order = Mockito.mock(OrderModel.class);

        BDDMockito.given(order.getSalesApplication()).willReturn(SalesApplication.EBAY);
        BDDMockito.given(
                Boolean.valueOf(targetFulfilmentSalesApplicationConfigService.isSuppressTaxInvoice(order
                        .getSalesApplication()))).willReturn(Boolean.FALSE);
        Assert.assertFalse(strategy.isDenied(order));
    }

    @Test
    public void testIsDeniedSalesAppTradeMe() {
        final OrderModel order = Mockito.mock(OrderModel.class);

        BDDMockito.given(order.getSalesApplication()).willReturn(SalesApplication.TRADEME);
        BDDMockito.given(
                Boolean.valueOf(targetFulfilmentSalesApplicationConfigService.isSuppressTaxInvoice(order
                        .getSalesApplication()))).willReturn(Boolean.TRUE);
        Assert.assertTrue(strategy.isDenied(order));
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetOrderStatusRefundDenialStrategyTest {

    @Mock
    private OrderModel order;

    @InjectMocks
    private final TargetOrderStatusRefundDenialStrategy strategy = new TargetOrderStatusRefundDenialStrategy();

    @Mock
    private AbstractOrderEntryModel entry;

    private final List<OrderStatus> allowedOrderStatuses = new ArrayList<>();

    @Test
    public void testInvalidOrderStatus() {
        allowedOrderStatuses.add(OrderStatus.COMPLETED);
        allowedOrderStatuses.add(OrderStatus.INVOICED);
        strategy.setAllowedOrderStatuses(allowedOrderStatuses);

        BDDMockito.given(order.getStatus()).willReturn(OrderStatus.INPROGRESS);
        final boolean result1 = strategy.perform(order, entry, 1l);
        Assert.assertFalse(result1);

        BDDMockito.given(order.getStatus()).willReturn(OrderStatus.COMPLETED);
        final boolean result2 = strategy.perform(order, entry, 1l);
        Assert.assertTrue(result2);

        BDDMockito.given(order.getStatus()).willReturn(OrderStatus.INVOICED);
        final boolean result3 = strategy.perform(order, entry, 1l);
        Assert.assertTrue(result3);
    }
}

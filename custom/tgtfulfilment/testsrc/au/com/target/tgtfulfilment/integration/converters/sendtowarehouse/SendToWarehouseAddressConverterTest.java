/**
 * 
 */
package au.com.target.tgtfulfilment.integration.converters.sendtowarehouse;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.AddressDTO;


/**
 * @author Vivek
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendToWarehouseAddressConverterTest {

    private final SendToWarehouseAddressConverter addressConverter = new SendToWarehouseAddressConverter();

    @Test
    public void testNullAddress() {
        Assert.assertNull(addressConverter.convert(null));
    }

    @Test
    public void testWithAddressData() {
        final AddressModel shippingAddress = Mockito.mock(AddressModel.class);
        final CountryModel country = Mockito.mock(CountryModel.class);

        Mockito.when(shippingAddress.getFirstname()).thenReturn("John");
        Mockito.when(shippingAddress.getLastname()).thenReturn("Snow");
        Mockito.when(shippingAddress.getLine1()).thenReturn("Address Line 1");
        Mockito.when(shippingAddress.getLine2()).thenReturn("Address Line 2");
        Mockito.when(shippingAddress.getTown()).thenReturn("Geelong");
        Mockito.when(shippingAddress.getDistrict()).thenReturn("VIC");
        Mockito.when(shippingAddress.getCountry()).thenReturn(country);
        Mockito.when(shippingAddress.getPostalcode()).thenReturn("3001");
        Mockito.when(shippingAddress.getPhone1()).thenReturn("4412121212");

        Mockito.when(country.getName()).thenReturn("Australia");

        final AddressDTO addressDTO = addressConverter.convert(shippingAddress);

        Assert.assertNotNull(addressDTO);
        Assert.assertEquals("John Snow", addressDTO.getName());
        final String address[] = addressDTO.getAddressLine();
        Assert.assertEquals("Address Line 1", address[0]);
        Assert.assertEquals("Address Line 2", address[1]);
        Assert.assertEquals("Geelong", addressDTO.getCity());
        Assert.assertEquals("VIC", addressDTO.getState());
        Assert.assertEquals("Australia", addressDTO.getCountry());
        Assert.assertEquals("3001", addressDTO.getPostcode());
        Assert.assertEquals("4412121212", addressDTO.getPhone());
    }

}

/**
 * 
 */
package au.com.target.tgtfulfilment.integration.converters.sendtowarehouse;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.AddressDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.CustomerDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.OrderDTO;


/**
 * Tests for SendToWarehouseOrderConverter
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendToWarehouseOrderConverterTest {

    @Mock
    private SendToWarehouseCustomerConverter sendToWarehouseCustomerConverter;

    @Mock
    private SendToWarehouseAddressConverter sendToWarehouseAddressConverter;

    @InjectMocks
    private final SendToWarehouseOrderConverter sendToWarehouseOrderConverter = new SendToWarehouseOrderConverter();

    @Test(expected = IllegalArgumentException.class)
    public void testNullOrder() {
        sendToWarehouseOrderConverter.convert(null);
    }

    @Test
    public void testOrderWithNullCustomer() {
        final OrderModel order = new OrderModel();
        order.setCode("1234");
        final OrderDTO orderDTO = sendToWarehouseOrderConverter.convert(order);
        Assert.assertNotNull(orderDTO);
        Assert.assertEquals(orderDTO.getId(), "1234");
        Assert.assertNull(orderDTO.getCustomer());
    }

    @Test
    public void testOrderWithNotNullCustomer() {
        final CustomerDTO customerDTO = new CustomerDTO();
        final OrderModel order = new OrderModel();
        order.setCode("1234");
        order.setUser(new TargetCustomerModel());
        BDDMockito.given(sendToWarehouseCustomerConverter.convert(Mockito.any(TargetCustomerModel.class))).willReturn(
                customerDTO);

        final OrderDTO orderDTO = sendToWarehouseOrderConverter.convert(order);
        Assert.assertNotNull(orderDTO);
        Assert.assertEquals(orderDTO.getId(), "1234");
        Assert.assertNotNull(orderDTO.getCustomer());
    }

    @Test
    public void testOrderWithNullShippingAddress() {
        final OrderModel order = new OrderModel();
        order.setCode("1234");
        final OrderDTO orderDTO = sendToWarehouseOrderConverter.convert(order);
        Assert.assertNotNull(orderDTO);
        Assert.assertEquals(orderDTO.getId(), "1234");
        Assert.assertNull(orderDTO.getShippingAddress());
    }

    @Test
    public void testOrderWithNotNullShippingAddress() {
        final AddressDTO addressDTO = new AddressDTO();
        final OrderModel order = new OrderModel();
        order.setCode("1234");
        order.setDeliveryAddress(new AddressModel());
        BDDMockito.given(sendToWarehouseAddressConverter.convert(Mockito.any(AddressModel.class))).willReturn(
                addressDTO);

        final OrderDTO orderDTO = sendToWarehouseOrderConverter.convert(order);
        Assert.assertNotNull(orderDTO);
        Assert.assertEquals(orderDTO.getId(), "1234");
        Assert.assertNotNull(orderDTO.getShippingAddress());
    }
}

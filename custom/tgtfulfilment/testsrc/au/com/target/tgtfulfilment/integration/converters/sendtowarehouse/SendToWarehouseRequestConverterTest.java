/**
 * 
 */
package au.com.target.tgtfulfilment.integration.converters.sendtowarehouse;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ConsignmentDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseRequest;


/**
 * Tests for SendToWarehouseRequestConverter
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendToWarehouseRequestConverterTest {

    @Mock
    private SendToWarehouseConsignmentConverter sendToWarehouseConsignmentConverter;

    @InjectMocks
    private final SendToWarehouseRequestConverter sendToWarehouseRequestConverter = new SendToWarehouseRequestConverter();

    @Test(expected = IllegalArgumentException.class)
    public void testNullConsignment() {
        sendToWarehouseRequestConverter.convert(null);
    }

    @Test
    public void testWithConsignment() {
        final ConsignmentModel consignment = new ConsignmentModel();
        BDDMockito.given(sendToWarehouseConsignmentConverter.convert(Mockito.any(ConsignmentModel.class))).willReturn(
                new ConsignmentDTO());
        final SendToWarehouseRequest request = sendToWarehouseRequestConverter.convert(consignment);
        Assert.assertNotNull(request);
        Assert.assertNotNull(request.getConsignment());
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.Calendar;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.storeblackout.StoreBlackOutService;


/**
 * Test class to test the interceptor which validates the blackout dates
 * 
 * @author Pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GlobalStoreFulfilmentCapabilitiesValidateInterceptorTest {

    @InjectMocks
    private final GlobalStoreFulfilmentCapabilitiesValidateInterceptor interceptor = new GlobalStoreFulfilmentCapabilitiesValidateInterceptor();

    @Mock
    private StoreBlackOutService storeBlackOutService;

    @Mock
    private InterceptorContext ctx;

    private GlobalStoreFulfilmentCapabilitiesModel model;

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    //CHECKSTYLE:ON


    /**
     * Method to test validation when start date is before end date
     */
    @Test
    public void testOnValidateWhenStartDateIsBeforeEndDate() {
        model = new GlobalStoreFulfilmentCapabilitiesModel();
        model.setMaxOrderPeriodStartTime("00:00");
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        model.setBlackoutStart(calendar.getTime());
        calendar.add(Calendar.DATE, 5);
        model.setBlackoutEnd(calendar.getTime());
        boolean isValidationSuccessful = false;
        try {
            interceptor.onValidate(model, ctx);
            Mockito.verify(storeBlackOutService).validateDates(model.getBlackoutStart(), model.getBlackoutEnd());
            isValidationSuccessful = true;
        }
        catch (final InterceptorException ie) {
            isValidationSuccessful = false;
        }
        Assert.assertTrue("Start date is before end date", isValidationSuccessful);
    }

    /**
     * Method to test validation when start date is after end date
     * 
     * @throws InterceptorException
     */
    @Test
    public void testOnValidateWhenStartDateIsAfterEndDate() throws InterceptorException {
        model = new GlobalStoreFulfilmentCapabilitiesModel();
        exception.expect(InterceptorException.class);
        final Calendar startDate = Calendar.getInstance();
        final Calendar endDate = Calendar.getInstance();
        startDate.add(Calendar.DATE, 1);
        model.setBlackoutStart(startDate.getTime());
        endDate.add(Calendar.DATE, -3);
        model.setBlackoutEnd(endDate.getTime());
        Mockito.doThrow(
                new InterceptorException(""))
                .when(storeBlackOutService).validateDates(startDate.getTime(), endDate.getTime());
        interceptor.onValidate(model, ctx);
    }


    /**
     * Method to test validation when interceptor is called on the wrong model type
     */
    @Test
    public void testOnValidateWhenModelIsNotOfExpectedType() {
        final Object sampleModel = new Object();
        boolean isValidationSuccessful = false;
        try {
            interceptor.onValidate(sampleModel, ctx);
            isValidationSuccessful = true;
        }
        catch (final InterceptorException ie) {
            isValidationSuccessful = false;
        }
        Assert.assertTrue("Validation is successful as model is not of correct type", isValidationSuccessful);
    }

    @Test
    public void testForMaxOrderPeriodStartTimeNull() throws InterceptorException {
        model = new GlobalStoreFulfilmentCapabilitiesModel();
        model.setMaxOrderPeriodStartTime(null);
        exception.expect(InterceptorException.class);
        exception.expectMessage("maxOrderPeriodStartTime must not be null");
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testForvalidPatter() throws InterceptorException {
        model = new GlobalStoreFulfilmentCapabilitiesModel();
        model.setMaxOrderPeriodStartTime("00:00");
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testForInvalidPatterWithDifferentDelimiter() throws InterceptorException {
        model = new GlobalStoreFulfilmentCapabilitiesModel();
        model.setMaxOrderPeriodStartTime("00;00");
        exception.expect(InterceptorException.class);
        exception.expectMessage("Input should be of type HH:MM - 24 HOURS format");
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testForInvalidPatterWithJustAlpabets() throws InterceptorException {
        model = new GlobalStoreFulfilmentCapabilitiesModel();
        model.setMaxOrderPeriodStartTime("AA:BB");
        exception.expect(InterceptorException.class);
        exception.expectMessage("Input should be of type HH:MM - 24 HOURS format");
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testForAlphaNumericValue() throws InterceptorException {
        model = new GlobalStoreFulfilmentCapabilitiesModel();
        model.setMaxOrderPeriodStartTime("a0:00");
        exception.expect(InterceptorException.class);
        exception.expectMessage("Input should be of type HH:MM - 24 HOURS format");
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testForInvalidPatterWithExtraAlphabet() throws InterceptorException {
        model = new GlobalStoreFulfilmentCapabilitiesModel();
        model.setMaxOrderPeriodStartTime("a00:00");
        exception.expect(InterceptorException.class);
        exception.expectMessage("Input should be of type HH:MM - 24 HOURS format");
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testForInvalidPatternWithMultipleNumbers() throws InterceptorException {
        model = new GlobalStoreFulfilmentCapabilitiesModel();
        model.setMaxOrderPeriodStartTime("000:00");
        exception.expect(InterceptorException.class);
        exception.expectMessage("Input should be of type HH:MM - 24 HOURS format");
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testForInvalidHours() throws InterceptorException {
        model = new GlobalStoreFulfilmentCapabilitiesModel();
        model.setMaxOrderPeriodStartTime("75:00");
        exception.expect(InterceptorException.class);
        exception.expectMessage("HH should be between 0-23");
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testForInvalidMinutes() throws InterceptorException {
        model = new GlobalStoreFulfilmentCapabilitiesModel();
        model.setMaxOrderPeriodStartTime("00:75");
        exception.expect(InterceptorException.class);
        exception.expectMessage("MM should be between 0-59");
        interceptor.onValidate(model, ctx);
    }
}

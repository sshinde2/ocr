/**
 * 
 */
package au.com.target.tgtfulfilment.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.Calendar;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.model.CategoryExclusionsModel;
import au.com.target.tgtfulfilment.model.ProductExclusionsModel;


/**
 * Test class to test the interceptor which validates the Product and category exclusion model end dates
 * 
 * @author smishra1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductAndCategoryExclusionDateValidateInterceptorTest {

    @InjectMocks
    private final ProductAndCategoryExclusionDateValidateInterceptor interceptor = new ProductAndCategoryExclusionDateValidateInterceptor();

    @Mock
    private InterceptorContext ctx;

    private CategoryExclusionsModel catetoryExclusion;
    private ProductExclusionsModel productExclusion;

    @Test
    public void testCategoryOnValidateWhenStartDateNull() {
        catetoryExclusion = new CategoryExclusionsModel();
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 5);
        catetoryExclusion.setExclusionEndDate(calendar.getTime());
        boolean result = false;
        try {
            interceptor.onValidate(catetoryExclusion, ctx);
            result = true;
        }
        catch (final InterceptorException ie) {
            result = false;
        }
        Assert.assertTrue("validation pass with Start date null", result);
    }

    @Test
    public void testCategoryOnValidateWhenEndDateNull() {
        catetoryExclusion = new CategoryExclusionsModel();
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 5);
        catetoryExclusion.setExclusionStartDate(calendar.getTime());
        boolean result = false;
        try {
            interceptor.onValidate(catetoryExclusion, ctx);
            result = true;
        }
        catch (final InterceptorException ie) {
            result = false;
        }
        Assert.assertTrue("validation pass with Start date null", result);
    }

    @Test
    public void testCategoryOnValidateWhenStartDateNullAndEndDateNull() {
        catetoryExclusion = new CategoryExclusionsModel();
        boolean result = false;
        try {
            interceptor.onValidate(catetoryExclusion, ctx);
            result = true;
        }
        catch (final InterceptorException ie) {
            result = false;
        }
        Assert.assertTrue("validation pass with Start date null", result);
    }

    @Test
    public void testCategoryOnValidateWhenStartDateIsBeforeEndDate() {
        catetoryExclusion = new CategoryExclusionsModel();
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        catetoryExclusion.setExclusionStartDate(calendar.getTime());
        calendar.add(Calendar.DATE, 5);
        catetoryExclusion.setExclusionEndDate(calendar.getTime());
        boolean result = false;
        try {
            interceptor.onValidate(catetoryExclusion, ctx);
            result = true;
        }
        catch (final InterceptorException ie) {
            result = false;
        }
        Assert.assertTrue("validation pass with Start date before end date", result);
    }

    @Test
    public void testCategoryOnValidateWhenStartDateIsEqualEndDate() {
        catetoryExclusion = new CategoryExclusionsModel();
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        catetoryExclusion.setExclusionStartDate(calendar.getTime());
        catetoryExclusion.setExclusionEndDate(calendar.getTime());
        boolean result = false;
        try {
            interceptor.onValidate(catetoryExclusion, ctx);
            result = true;
        }
        catch (final InterceptorException ie) {
            result = false;
        }
        Assert.assertTrue("validation pass with Start date equal end date", result);
    }

    @Test
    public void testCategoryOnValidateWhenStartDateIsAfterEndDate() {
        catetoryExclusion = new CategoryExclusionsModel();
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        catetoryExclusion.setExclusionEndDate(calendar.getTime());
        calendar.add(Calendar.DATE, 5);
        catetoryExclusion.setExclusionStartDate(calendar.getTime());
        boolean result = false;
        try {
            interceptor.onValidate(catetoryExclusion, ctx);
            result = true;
        }
        catch (final InterceptorException ie) {
            result = false;
        }
        Assert.assertFalse("validation fail with Start date is after end date", result);
    }

    @Test
    public void testProductOnValidateWhenStartDateNull() {
        productExclusion = new ProductExclusionsModel();
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 5);
        productExclusion.setExclusionEndDate(calendar.getTime());
        boolean result = false;
        try {
            interceptor.onValidate(productExclusion, ctx);
            result = true;
        }
        catch (final InterceptorException ie) {
            result = false;
        }
        Assert.assertTrue("validation pass with Start date null", result);
    }

    @Test
    public void testProductOnValidateWhenEndDateNull() {
        productExclusion = new ProductExclusionsModel();
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 5);
        productExclusion.setExclusionStartDate(calendar.getTime());
        boolean result = false;
        try {
            interceptor.onValidate(productExclusion, ctx);
            result = true;
        }
        catch (final InterceptorException ie) {
            result = false;
        }
        Assert.assertTrue("validation pass with Start date null", result);
    }

    @Test
    public void testProductOnValidateWhenStartDateNullAndEndDateNull() {
        productExclusion = new ProductExclusionsModel();
        boolean result = false;
        try {
            interceptor.onValidate(productExclusion, ctx);
            result = true;
        }
        catch (final InterceptorException ie) {
            result = false;
        }
        Assert.assertTrue("validation pass with Start date null", result);
    }

    @Test
    public void testProductOnValidateWhenStartDateIsBeforeEndDate() {
        productExclusion = new ProductExclusionsModel();
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        productExclusion.setExclusionStartDate(calendar.getTime());
        calendar.add(Calendar.DATE, 5);
        productExclusion.setExclusionEndDate(calendar.getTime());
        boolean result = false;
        try {
            interceptor.onValidate(productExclusion, ctx);
            result = true;
        }
        catch (final InterceptorException ie) {
            result = false;
        }
        Assert.assertTrue("validation pass with Start date before end date", result);
    }

    @Test
    public void testProductOnValidateWhenStartDateIsEqualEndDate() {
        productExclusion = new ProductExclusionsModel();
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        productExclusion.setExclusionStartDate(calendar.getTime());
        productExclusion.setExclusionEndDate(calendar.getTime());
        boolean result = false;
        try {
            interceptor.onValidate(productExclusion, ctx);
            result = true;
        }
        catch (final InterceptorException ie) {
            result = false;
        }
        Assert.assertTrue("validation pass with Start date equal end date", result);
    }

    @Test
    public void testProductOnValidateWhenStartDateIsAfterEndDate() {
        productExclusion = new ProductExclusionsModel();
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        productExclusion.setExclusionEndDate(calendar.getTime());
        calendar.add(Calendar.DATE, 5);
        productExclusion.setExclusionStartDate(calendar.getTime());
        boolean result = false;
        try {
            interceptor.onValidate(productExclusion, ctx);
            result = true;
        }
        catch (final InterceptorException ie) {
            result = false;
        }
        Assert.assertFalse("validation fail with Start date is after end date", result);
    }
}

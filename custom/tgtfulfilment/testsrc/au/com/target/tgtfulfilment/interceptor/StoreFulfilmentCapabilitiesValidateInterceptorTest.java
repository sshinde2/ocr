/**
 * 
 */
package au.com.target.tgtfulfilment.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.storeblackout.StoreBlackOutService;


/**
 * @author siddharam
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StoreFulfilmentCapabilitiesValidateInterceptorTest {

    private static final String INTER_STORE_DELIVERY_ALLOWED = "Inter Store Delivery Enabled Store";
    private static final String HOME_DELIVERY_ENABLED_STORE = "Customer Delivery Enabled Store";
    private static final String HOME_DELIVERY_CODE = "home-delivery";
    private static final String EXPRESS_DELIVER_CODE = "express-delivery";

    @InjectMocks
    private final StoreFulfilmentCapabilitiesValidateInterceptor interceptor = new StoreFulfilmentCapabilitiesValidateInterceptor();

    @Mock
    private StoreBlackOutService storeBlackOutService;

    @Mock
    private InterceptorContext ctx;

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    private StoreFulfilmentCapabilitiesModel model;

    @Mock
    private StoreFulfilmentCapabilitiesModel fulfilmentCapabilityModel;

    @Mock
    private TargetZoneDeliveryModeModel deliveryMode;

    /**
     * Method to test validation when start date is before end date
     */
    @Test
    public void testOnValidateWhenStartDateIsBeforeEndDate() {
        model = new StoreFulfilmentCapabilitiesModel();
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        model.setBlackoutStart(calendar.getTime());
        calendar.add(Calendar.DATE, 5);
        model.setBlackoutEnd(calendar.getTime());
        boolean isValidationSuccessful = false;
        try {
            interceptor.onValidate(model, ctx);
            Mockito.verify(storeBlackOutService).validateDates(model.getBlackoutStart(), model.getBlackoutEnd());
            isValidationSuccessful = true;
        }
        catch (final InterceptorException ie) {
            isValidationSuccessful = false;
        }
        Assert.assertTrue("Start date is before end date", isValidationSuccessful);
    }

    /**
     * Method to test validation when start date is after end date
     * 
     * @throws InterceptorException
     */
    @Test(expected = InterceptorException.class)
    public void testOnValidateWhenStartDateIsAfterEndDate() throws InterceptorException {
        model = new StoreFulfilmentCapabilitiesModel();
        final Calendar startDate = Calendar.getInstance();
        final Calendar endDate = Calendar.getInstance();
        startDate.add(Calendar.DATE, 1);
        model.setBlackoutStart(startDate.getTime());
        endDate.add(Calendar.DATE, -3);
        model.setBlackoutEnd(endDate.getTime());
        Mockito.doThrow(
                new InterceptorException(""))
                .when(storeBlackOutService).validateDates(startDate.getTime(), endDate.getTime());
        interceptor.onValidate(model, ctx);
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidateInvalidMobileNumber() throws InterceptorException {
        model = new StoreFulfilmentCapabilitiesModel();
        final Calendar startDate = Calendar.getInstance();
        final Calendar endDate = Calendar.getInstance();
        startDate.add(Calendar.DATE, 1);
        model.setBlackoutStart(startDate.getTime());
        endDate.add(Calendar.DATE, 5);
        model.setBlackoutEnd(endDate.getTime());
        model.setMobileNumber("1234");
        interceptor.onValidate(model, ctx);
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidateEmptyMobileNumber() throws InterceptorException {
        model = new StoreFulfilmentCapabilitiesModel();
        final Calendar startDate = Calendar.getInstance();
        final Calendar endDate = Calendar.getInstance();
        startDate.add(Calendar.DATE, 1);
        model.setBlackoutStart(startDate.getTime());
        endDate.add(Calendar.DATE, 5);
        model.setBlackoutEnd(endDate.getTime());
        model.setMobileNumber("");
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testOnValidateNullMobileNumber() throws InterceptorException {
        model = new StoreFulfilmentCapabilitiesModel();
        final Calendar startDate = Calendar.getInstance();
        final Calendar endDate = Calendar.getInstance();
        startDate.add(Calendar.DATE, 1);
        model.setBlackoutStart(startDate.getTime());
        endDate.add(Calendar.DATE, 5);
        model.setBlackoutEnd(endDate.getTime());
        model.setMobileNumber(null);
        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testOnValidateValidMobileNumber() throws InterceptorException {
        model = new StoreFulfilmentCapabilitiesModel();
        final Calendar startDate = Calendar.getInstance();
        final Calendar endDate = Calendar.getInstance();
        startDate.add(Calendar.DATE, 1);
        model.setBlackoutStart(startDate.getTime());
        endDate.add(Calendar.DATE, 5);
        model.setBlackoutEnd(endDate.getTime());
        model.setMobileNumber("4492055011");
        interceptor.onValidate(model, ctx);
    }

    /**
     * Method to test validation when interceptor is called on the wrong model type
     */
    @Test
    public void testOnValidateWhenModelIsNotOfExpectedType() {
        final Object sampleModel = new Object();
        boolean isValidationSuccessful = false;
        try {
            interceptor.onValidate(sampleModel, ctx);
            isValidationSuccessful = true;
        }
        catch (final InterceptorException ie) {
            isValidationSuccessful = false;
        }
        Assert.assertTrue("Validation is successful as model is not of correct type", isValidationSuccessful);
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidateNotificationsEnabledAndNullStartTime() throws InterceptorException {
        enableNotifications();
        Mockito.when(model.getNotificationStartTime()).thenReturn(null);

        interceptor.onValidate(model, ctx);
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidateNotificationsEnabledAndNullEndTime() throws InterceptorException {
        enableNotifications();
        Mockito.when(model.getNotificationStartTime()).thenReturn(getTime(10));
        Mockito.when(model.getNotificationEndTime()).thenReturn(null);

        interceptor.onValidate(model, ctx);
    }

    @Test(expected = InterceptorException.class)
    public void testOnValidateNotificationsEnabledAndStartTimeGreaterThanEndTime() throws InterceptorException {

        enableNotifications();
        Mockito.when(model.getNotificationStartTime()).thenReturn(getTime(10));
        Mockito.when(model.getNotificationEndTime()).thenReturn(getTime(9));

        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testOnValidateNotificationsEnabledAndStartTimeLessThanEndTime() throws InterceptorException {

        enableNotifications();
        Mockito.when(model.getNotificationStartTime()).thenReturn(getTime(9));
        Mockito.when(model.getNotificationEndTime()).thenReturn(getTime(16));

        interceptor.onValidate(model, ctx);
    }

    @Test
    public void testMaxOrderForDeliveryNullForInterStore() throws InterceptorException {
        Mockito.when(fulfilmentCapabilityModel.getAllowDeliveryToAnotherStore()).thenReturn(Boolean.valueOf(true));
        Mockito.when(fulfilmentCapabilityModel.getMaxConsignmentsForDeliveryPerDay()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("MaxOrderForDeliveryPerDay cannot be null for : " + INTER_STORE_DELIVERY_ALLOWED);
        interceptor.onValidate(fulfilmentCapabilityModel, ctx);
    }


    @Test
    public void testtestMaxOrderForDeliveryNullForHomeDelivery() throws InterceptorException {
        Mockito.when(fulfilmentCapabilityModel.getDeliveryModes()).thenReturn(Collections.singleton(deliveryMode));
        Mockito.when(deliveryMode.getCode()).thenReturn(HOME_DELIVERY_CODE);
        Mockito.when(fulfilmentCapabilityModel.getMaxConsignmentsForDeliveryPerDay()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("MaxOrderForDeliveryPerDay cannot be null for : "
                        + HOME_DELIVERY_ENABLED_STORE);
        interceptor.onValidate(fulfilmentCapabilityModel, ctx);
    }

    @Test
    public void testtestMaxOrderForDeliveryNullForExpressDelivery() throws InterceptorException {
        Mockito.when(fulfilmentCapabilityModel.getDeliveryModes()).thenReturn(Collections.singleton(deliveryMode));
        Mockito.when(deliveryMode.getCode()).thenReturn(EXPRESS_DELIVER_CODE);
        Mockito.when(fulfilmentCapabilityModel.getMaxConsignmentsForDeliveryPerDay()).thenReturn(null);

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("MaxOrderForDeliveryPerDay cannot be null for : "
                        + HOME_DELIVERY_ENABLED_STORE);
        interceptor.onValidate(fulfilmentCapabilityModel, ctx);
    }

    @Test
    public void testNegativeValueValidationForMaxConsignments() throws InterceptorException {
        Mockito.when(fulfilmentCapabilityModel.getMaxConsignmentsForDeliveryPerDay()).thenReturn(Integer.valueOf(-1));

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("Invalid : Value of max consignments should not be less than 0");
        interceptor.onValidate(fulfilmentCapabilityModel, ctx);
    }

    @Test
    public void testNegativeValueValidationForMaxWeight() throws InterceptorException {
        Mockito.when(fulfilmentCapabilityModel.getMaxAllowedProductWeightForDelivery()).thenReturn(Double.valueOf(-1));

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("Invalid : Value for allowed Weight should not be less than 0");
        interceptor.onValidate(fulfilmentCapabilityModel, ctx);
    }

    @Test
    public void testNegativeValueValidationForMaxWeightDoubleValue() throws InterceptorException {
        Mockito.when(fulfilmentCapabilityModel.getMaxAllowedProductWeightForDelivery()).thenReturn(
                Double.valueOf(-0.00001));

        expectedException.expect(InterceptorException.class);
        expectedException
                .expectMessage("Invalid : Value for allowed Weight should not be less than 0");
        interceptor.onValidate(fulfilmentCapabilityModel, ctx);
    }

    private void enableNotifications() {
        model = Mockito.mock(StoreFulfilmentCapabilitiesModel.class);
        Mockito.when(model.getNotificationsEnabled()).thenReturn(Boolean.TRUE);
    }

    private Date getTime(final int hour) {

        final Calendar cal = Calendar.getInstance();
        cal.set(0, 0, 0, hour, 0, 0);

        return cal.getTime();
    }
}

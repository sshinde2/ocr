/**
 * 
 */
package au.com.target.tgtfulfilment.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetConsignmentPrepareInterceptorTest {
    @Mock
    private InterceptorContext context;

    private final TargetConsignmentPrepareInterceptor interceptor = new TargetConsignmentPrepareInterceptor();

    @Mock
    private TargetConsignmentModel targetConsignment;

    @Mock
    private ConsignmentModel consignment;

    @Test
    public void testTargetConsignmentModelWithWaitingStatus() throws InterceptorException {
        Mockito.when(targetConsignment.getStatus()).thenReturn(ConsignmentStatus.WAITING);
        interceptor.onPrepare(targetConsignment, context);
        Mockito.verify(targetConsignment).getStatus();
        Mockito.verifyNoMoreInteractions(targetConsignment);
    }

    @Test
    public void testTargetConsignmentModelWithConfirmedByWarehouseStatusWithNullDate() throws InterceptorException {
        Mockito.when(targetConsignment.getStatus()).thenReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        Mockito.when(targetConsignment.getSentToWarehouseDate()).thenReturn(null);
        interceptor.onPrepare(targetConsignment, context);
        Mockito.verify(targetConsignment).getStatus();
        Mockito.verify(targetConsignment).getSentToWarehouseDate();
        Mockito.verify(targetConsignment).setSentToWarehouseDate(Mockito.any(Date.class));
        Mockito.verifyNoMoreInteractions(targetConsignment);
    }

    @Test
    public void testTargetConsignmentModelWithConfirmedByWarehouseStatusWithValidDate() throws InterceptorException {
        Mockito.when(targetConsignment.getStatus()).thenReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        Mockito.when(targetConsignment.getSentToWarehouseDate()).thenReturn(new Date());
        interceptor.onPrepare(targetConsignment, context);
        Mockito.verify(targetConsignment).getStatus();
        Mockito.verify(targetConsignment).getSentToWarehouseDate();
        Mockito.verifyNoMoreInteractions(targetConsignment);
    }

    @Test
    public void testTargetConsignmentModelWithSentToWarehouseStatusWithNullDate() throws InterceptorException {
        Mockito.when(targetConsignment.getStatus()).thenReturn(ConsignmentStatus.SENT_TO_WAREHOUSE);
        Mockito.when(targetConsignment.getSentToWarehouseDate()).thenReturn(null);
        interceptor.onPrepare(targetConsignment, context);
        Mockito.verify(targetConsignment).getStatus();
        Mockito.verify(targetConsignment).getSentToWarehouseDate();
        Mockito.verify(targetConsignment).setSentToWarehouseDate(Mockito.any(Date.class));
        Mockito.verifyNoMoreInteractions(targetConsignment);
    }

    @Test
    public void testTargetConsignmentModelWithSentToWarehouseStatusWithValidDate() throws InterceptorException {
        Mockito.when(targetConsignment.getStatus()).thenReturn(ConsignmentStatus.SENT_TO_WAREHOUSE);
        Mockito.when(targetConsignment.getSentToWarehouseDate()).thenReturn(new Date());
        interceptor.onPrepare(targetConsignment, context);
        Mockito.verify(targetConsignment).getStatus();
        Mockito.verify(targetConsignment).getSentToWarehouseDate();
        Mockito.verifyNoMoreInteractions(targetConsignment);
    }

    @Test
    public void testTargetConsignmentModelWithShippedStatusWithNullDate() throws InterceptorException {
        Mockito.when(targetConsignment.getStatus()).thenReturn(ConsignmentStatus.SHIPPED);
        Mockito.when(targetConsignment.getShippingDate()).thenReturn(null);
        interceptor.onPrepare(targetConsignment, context);
        Mockito.verify(targetConsignment).getStatus();
        Mockito.verify(targetConsignment).getShippingDate();
        Mockito.verify(targetConsignment).setShippingDate(Mockito.any(Date.class));
        Mockito.verify(targetConsignment).setShipConfReceived(Boolean.TRUE);
        Mockito.verifyNoMoreInteractions(targetConsignment);
    }

    @Test
    public void testTargetConsignmentModelWithShippedStatusWithValidDate() throws InterceptorException {
        Mockito.when(targetConsignment.getStatus()).thenReturn(ConsignmentStatus.SHIPPED);
        Mockito.when(targetConsignment.getShippingDate()).thenReturn(new Date());
        interceptor.onPrepare(targetConsignment, context);
        Mockito.verify(targetConsignment).getStatus();
        Mockito.verify(targetConsignment).getShippingDate();
        Mockito.verify(targetConsignment).setShipConfReceived(Boolean.TRUE);
        Mockito.verifyNoMoreInteractions(targetConsignment);
    }

    @Test
    public void testTargetConsignmentModelWithPackedStatusWithNullDate() throws InterceptorException {
        Mockito.when(targetConsignment.getStatus()).thenReturn(ConsignmentStatus.PACKED);
        Mockito.when(targetConsignment.getPackedDate()).thenReturn(null);
        interceptor.onPrepare(targetConsignment, context);
        Mockito.verify(targetConsignment).getStatus();
        Mockito.verify(targetConsignment).getPackedDate();
        Mockito.verify(targetConsignment).setPackedDate(Mockito.any(Date.class));
        Mockito.verifyNoMoreInteractions(targetConsignment);
    }

    @Test
    public void testTargetConsignmentModelWithPackedStatusWithValidDate() throws InterceptorException {
        Mockito.when(targetConsignment.getStatus()).thenReturn(ConsignmentStatus.PACKED);
        Mockito.when(targetConsignment.getPackedDate()).thenReturn(new Date());
        interceptor.onPrepare(targetConsignment, context);
        Mockito.verify(targetConsignment).getStatus();
        Mockito.verify(targetConsignment).getPackedDate();
        Mockito.verifyNoMoreInteractions(targetConsignment);
    }

    @Test
    public void testTargetConsignmentModelWithPickedStatusWithNullDate() throws InterceptorException {
        Mockito.when(targetConsignment.getStatus()).thenReturn(ConsignmentStatus.PICKED);
        Mockito.when(targetConsignment.getPickConfirmDate()).thenReturn(null);
        interceptor.onPrepare(targetConsignment, context);
        Mockito.verify(targetConsignment).getStatus();
        Mockito.verify(targetConsignment).getPickConfirmDate();
        Mockito.verify(targetConsignment).setPickConfirmDate(Mockito.any(Date.class));
        Mockito.verifyNoMoreInteractions(targetConsignment);
    }

    @Test
    public void testTargetConsignmentModelWithPickedStatusWithValidDate() throws InterceptorException {
        Mockito.when(targetConsignment.getStatus()).thenReturn(ConsignmentStatus.PICKED);
        Mockito.when(targetConsignment.getPickConfirmDate()).thenReturn(new Date());
        interceptor.onPrepare(targetConsignment, context);
        Mockito.verify(targetConsignment).getStatus();
        Mockito.verify(targetConsignment).getPickConfirmDate();
        Mockito.verifyNoMoreInteractions(targetConsignment);
    }

    @Test
    public void testTargetConsignmentModelWithWavedStatusWithNullDate() throws InterceptorException {
        Mockito.when(targetConsignment.getStatus()).thenReturn(ConsignmentStatus.WAVED);
        Mockito.when(targetConsignment.getWavedDate()).thenReturn(null);
        interceptor.onPrepare(targetConsignment, context);
        Mockito.verify(targetConsignment).getStatus();
        Mockito.verify(targetConsignment).getWavedDate();
        Mockito.verify(targetConsignment).setWavedDate(Mockito.any(Date.class));
        Mockito.verifyNoMoreInteractions(targetConsignment);
    }

    @Test
    public void testTargetConsignmentModelWithWavedStatusWithValidDate() throws InterceptorException {
        Mockito.when(targetConsignment.getStatus()).thenReturn(ConsignmentStatus.WAVED);
        Mockito.when(targetConsignment.getWavedDate()).thenReturn(new Date());
        interceptor.onPrepare(targetConsignment, context);
        Mockito.verify(targetConsignment).getStatus();
        Mockito.verify(targetConsignment).getWavedDate();
        Mockito.verifyNoMoreInteractions(targetConsignment);
    }

    @Test
    public void testTargetConsignmentModelWithCancelledStatusWithNullDate() throws InterceptorException {
        Mockito.when(targetConsignment.getStatus()).thenReturn(ConsignmentStatus.CANCELLED);
        Mockito.when(targetConsignment.getCancelDate()).thenReturn(null);
        interceptor.onPrepare(targetConsignment, context);
        Mockito.verify(targetConsignment).getStatus();
        Mockito.verify(targetConsignment).getCancelDate();
        Mockito.verify(targetConsignment).setCancelDate(Mockito.any(Date.class));
        Mockito.verifyNoMoreInteractions(targetConsignment);
    }

    @Test
    public void testTargetConsignmentModelWithCancelStatusWithValidDate() throws InterceptorException {
        Mockito.when(targetConsignment.getStatus()).thenReturn(ConsignmentStatus.CANCELLED);
        Mockito.when(targetConsignment.getCancelDate()).thenReturn(new Date());
        interceptor.onPrepare(targetConsignment, context);
        Mockito.verify(targetConsignment).getStatus();
        Mockito.verify(targetConsignment).getCancelDate();
        Mockito.verifyNoMoreInteractions(targetConsignment);
    }
}

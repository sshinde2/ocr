/**
 * 
 */
package au.com.target.tgtfulfilment.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.model.TargetConsignmentModel;


/**
 * @author sbryan6
 *
 */
@UnitTest
public class TargetConsignmentValidateInterceptorTest {

    private final TargetConsignmentValidateInterceptor interceptor = new TargetConsignmentValidateInterceptor();

    @Mock
    private InterceptorContext interceptorContext;

    @Mock
    private TargetConsignmentModel consignment;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testHasShippingAddress() throws InterceptorException {

        Mockito.when(consignment.getShippingAddress()).thenReturn(new AddressModel());
        interceptor.onValidate(consignment, interceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testValidateWithNoDelMode() throws InterceptorException {

        interceptor.onValidate(consignment, interceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testValidateWithWrongTypeOfDelMode() throws InterceptorException {

        final DeliveryModeModel delMode = Mockito.mock(DeliveryModeModel.class);
        Mockito.when(consignment.getDeliveryMode()).thenReturn(delMode);
        interceptor.onValidate(consignment, interceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testValidateWithDelModeWithNullIsDigital() throws InterceptorException {

        final TargetZoneDeliveryModeModel delMode = Mockito.mock(TargetZoneDeliveryModeModel.class);
        Mockito.when(consignment.getDeliveryMode()).thenReturn(delMode);
        interceptor.onValidate(consignment, interceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testValidateWithDelModeWithFalseIsDigital() throws InterceptorException {

        final TargetZoneDeliveryModeModel delMode = Mockito.mock(TargetZoneDeliveryModeModel.class);
        Mockito.when(consignment.getDeliveryMode()).thenReturn(delMode);
        Mockito.when(delMode.getIsDigital()).thenReturn(Boolean.FALSE);
        interceptor.onValidate(consignment, interceptorContext);
    }

    @Test
    public void testValidateWithDelModeWithTrueIsDigital() throws InterceptorException {

        final TargetZoneDeliveryModeModel delMode = Mockito.mock(TargetZoneDeliveryModeModel.class);
        Mockito.when(consignment.getDeliveryMode()).thenReturn(delMode);
        Mockito.when(delMode.getIsDigital()).thenReturn(Boolean.TRUE);
        interceptor.onValidate(consignment, interceptorContext);
    }


}

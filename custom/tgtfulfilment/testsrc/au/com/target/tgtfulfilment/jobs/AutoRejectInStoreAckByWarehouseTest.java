/**
 * 
 */
package au.com.target.tgtfulfilment.jobs;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.fulfilmentservice.TargetFulfilmentService;
import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;


/**
 * Unit test for the cron job AutoRejectInStoreAckByWarehouse.
 *
 * @author ajit
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AutoRejectInStoreAckByWarehouseTest {

    @InjectMocks
    private final AutoRejectInStoreAckByWarehouse cronJob = new AutoRejectInStoreAckByWarehouse();

    @Mock
    private TargetFulfilmentService targetFulfilmentService;

    @Mock
    private TargetStoreConsignmentService targetStoreConsignmentService;

    @Mock
    private CronJobModel cronJobModel;

    @Mock
    private TargetGlobalStoreFulfilmentService globalFulfilmentService;

    @Mock
    private GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel;

    @Mock
    private WarehouseModel warehouse;

    @Mock
    private TargetConsignmentService targetConsignmentService;

    @Before
    public void setUp() {
        given(globalFulfilmentService.getGlobalStoreFulfilmentCapabilites()).willReturn(capabilitiesModel);
        given(capabilitiesModel.getPullBackTolerance()).willReturn(Integer.valueOf(80));
    }

    /**
     * To test given Consignment list is empty
     */
    @Test
    public void testCronjobWithEmptyConsigment() {
        final List<TargetConsignmentModel> consignmentModels = Collections.emptyList();
        given(targetStoreConsignmentService.getConsignmentsOfMultiplesStatusesForAllStores(
                Arrays.asList(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, ConsignmentStatus.SENT_TO_WAREHOUSE)))
                .willReturn(consignmentModels);
        final PerformResult performResult = cronJob.perform(cronJobModel);
        verify(targetStoreConsignmentService).getConsignmentsOfMultiplesStatusesForAllStores(
                Arrays.asList(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, ConsignmentStatus.SENT_TO_WAREHOUSE));
        verifyZeroInteractions(targetFulfilmentService);

        assertThat(CronJobResult.SUCCESS).isEqualTo(performResult.getResult());
        assertThat(CronJobStatus.FINISHED).isEqualTo(performResult.getStatus());
    }

    /**
     * Test cronjob with pull back tolerance time as 80 minutes and 2 consignments where 1 is outside tolerance time and
     * the other is in tolerance time.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testCronJobWithConsignmentsWithPullBackTolerance() throws Exception {
        final List<TargetConsignmentModel> consignmentList = getMockConsignments();
        final TargetConsignmentModel consignment1 = consignmentList.get(0);
        given(targetStoreConsignmentService.getConsignmentsOfMultiplesStatusesForAllStores(
                Arrays.asList(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, ConsignmentStatus.SENT_TO_WAREHOUSE)))
                .willReturn(consignmentList);

        final PerformResult performResult = cronJob.perform(cronJobModel);
        verify(targetFulfilmentService).processRerouteAutoTimeoutConsignment(consignment1,
                ConsignmentRejectReason.ACCEPT_TIMEOUT);
        verifyNoMoreInteractions(targetFulfilmentService);
        assertThat(CronJobResult.SUCCESS).isEqualTo(performResult.getResult());
        assertThat(CronJobStatus.FINISHED).isEqualTo(performResult.getStatus());
    }

    /**
     * Test cronjob given pull back tolerance is not set in global fulfillment capabilities model and 2 consignments.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testCronJobWithConsignmentsWithNoPullBackTolerance() throws Exception {
        given(capabilitiesModel.getPullBackTolerance()).willReturn(null);
        testCronJobWithPullBackToleranceAsZero();
    }

    /**
     * Test cronjob given global fulfillment capabilities model doesn't exist and 2 consignments.
     * 
     * @throws Exception
     */
    @Test
    public void testCronJobWithGlobalCapabilitiesModelMissing() throws Exception {
        given(globalFulfilmentService.getGlobalStoreFulfilmentCapabilites()).willReturn(null);
        testCronJobWithPullBackToleranceAsZero();
    }

    /**
     * Test cronjob with excluded states
     */
    @Test
    public void testCronJobWithExcludedStateList() {
        final List<String> excludedStates = Arrays.asList("WA");
        cronJob.setExcludedStates(excludedStates);

        final List<TargetConsignmentModel> consignmentList = getMockConsignments();
        given(targetStoreConsignmentService
                .getConsignmentsForStoreNotInState(
                        Arrays.asList(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, ConsignmentStatus.SENT_TO_WAREHOUSE),
                        excludedStates))
                        .willReturn(consignmentList);

        final PerformResult performResult = cronJob.perform(cronJobModel);

        assertThat(CronJobResult.SUCCESS).isEqualTo(performResult.getResult());
        assertThat(CronJobStatus.FINISHED).isEqualTo(performResult.getStatus());
        verify(targetStoreConsignmentService)
                .getConsignmentsForStoreNotInState(
                        Arrays.asList(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, ConsignmentStatus.SENT_TO_WAREHOUSE),
                        excludedStates);
        verify(
                targetStoreConsignmentService, never())
                        .getConsignmentsOfMultiplesStatusesForAllStores(
                                Arrays.asList(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE,
                                        ConsignmentStatus.SENT_TO_WAREHOUSE));
        verify(
                targetStoreConsignmentService, never())
                        .getConsignmentsForStoreInState(
                                Arrays.asList(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE,
                                        ConsignmentStatus.SENT_TO_WAREHOUSE),
                                excludedStates);
    }

    /**
     * Test cronjob with included states
     */
    @Test
    public void testCronJobWithIncludedStateList() {
        final List<String> includedStates = Arrays.asList("WA");
        cronJob.setIncludedStates(includedStates);

        final List<TargetConsignmentModel> consignmentList = getMockConsignments();
        given(targetStoreConsignmentService
                .getConsignmentsForStoreInState(
                        Arrays.asList(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, ConsignmentStatus.SENT_TO_WAREHOUSE),
                        includedStates))
                        .willReturn(consignmentList);

        final PerformResult performResult = cronJob.perform(cronJobModel);

        assertThat(CronJobResult.SUCCESS).isEqualTo(performResult.getResult());
        assertThat(CronJobStatus.FINISHED).isEqualTo(performResult.getStatus());
        verify(targetStoreConsignmentService)
                .getConsignmentsForStoreInState(
                        Arrays.asList(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, ConsignmentStatus.SENT_TO_WAREHOUSE),
                        includedStates);
        verify(
                targetStoreConsignmentService, never())
                        .getConsignmentsOfMultiplesStatusesForAllStores(
                                Arrays.asList(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE,
                                        ConsignmentStatus.SENT_TO_WAREHOUSE));
        verify(
                targetStoreConsignmentService, never())
                        .getConsignmentsForStoreNotInState(Arrays.asList(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE,
                                ConsignmentStatus.SENT_TO_WAREHOUSE), includedStates);
    }

    @Test
    public void testCronJobWithConsignmentsWithDefaultWarehouse() throws Exception {
        final List<TargetConsignmentModel> consignmentList = getMockConsignments();
        final TargetConsignmentModel consignment1 = consignmentList.get(0);
        given(targetStoreConsignmentService.getConsignmentsForAllStore(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE))
                .willReturn(consignmentList);
        given(consignment1.getWarehouse()).willReturn(warehouse);
        given(warehouse.getDefault()).willReturn(Boolean.TRUE);
        final PerformResult performResult = cronJob.perform(cronJobModel);
        verifyZeroInteractions(targetFulfilmentService);
        assertThat(CronJobResult.SUCCESS).isEqualTo(performResult.getResult());
        assertThat(CronJobStatus.FINISHED).isEqualTo(performResult.getStatus());
    }

    @Test
    public void testCronJobWithConsignmentsgivenServicedByServicePoint() throws Exception {
        final List<TargetConsignmentModel> consignmentList = getMockConsignments();
        final TargetConsignmentModel consignment1 = consignmentList.get(0);
        given(targetStoreConsignmentService.getConsignmentsForAllStore(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE))
                .willReturn(consignmentList);
        given(consignment1.getWarehouse()).willReturn(warehouse);
        willReturn(Boolean.TRUE).given(targetConsignmentService)
                .isConsignmentServicedByFluentServicePoint(consignment1);

        final PerformResult performResult = cronJob.perform(cronJobModel);

        verifyZeroInteractions(targetFulfilmentService);
        assertThat(CronJobResult.SUCCESS).isEqualTo(performResult.getResult());
        assertThat(CronJobStatus.FINISHED).isEqualTo(performResult.getStatus());
    }

    /**
     * Method to test cron job given pull back time is set as 0.
     * 
     * @throws Exception
     */
    private void testCronJobWithPullBackToleranceAsZero() throws Exception {
        final List<TargetConsignmentModel> consignmentList = getMockConsignments();
        final TargetConsignmentModel consignment1 = consignmentList.get(0);
        final TargetConsignmentModel consignment2 = consignmentList.get(1);
        given(targetStoreConsignmentService.getConsignmentsOfMultiplesStatusesForAllStores(
                Arrays.asList(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, ConsignmentStatus.SENT_TO_WAREHOUSE)))
                .willReturn(consignmentList);

        final PerformResult performResult = cronJob.perform(cronJobModel);
        verify(targetFulfilmentService).processRerouteAutoTimeoutConsignment(consignment1,
                ConsignmentRejectReason.ACCEPT_TIMEOUT);
        verify(targetFulfilmentService).processRerouteAutoTimeoutConsignment(consignment2,
                ConsignmentRejectReason.ACCEPT_TIMEOUT);
        verifyNoMoreInteractions(targetFulfilmentService);
        assertThat(CronJobResult.SUCCESS).isEqualTo(performResult.getResult());
        assertThat(CronJobStatus.FINISHED).isEqualTo(performResult.getStatus());
    }

    /**
     * Method to mock consignment data
     * 
     * @param consignment
     * @param consignmentCode
     * @param minuteSinceConsignmentCreated
     */
    private void mockConsignmentData(final TargetConsignmentModel consignment, final String consignmentCode,
            final int minuteSinceConsignmentCreated) {
        given(consignment.getStatus()).willReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        given(consignment.getCode()).willReturn(consignmentCode);
        final Calendar consignmentCreationTime = Calendar.getInstance();
        consignmentCreationTime.add(Calendar.MINUTE, minuteSinceConsignmentCreated);
        given(consignment.getCreationtime()).willReturn(consignmentCreationTime.getTime());
    }

    /**
     * Method to return mock consignment list
     * 
     * @return a list of mock consignments
     */
    private List<TargetConsignmentModel> getMockConsignments() {
        final List<TargetConsignmentModel> consignmentList = new ArrayList<>();
        consignmentList.add(mock(TargetConsignmentModel.class));
        consignmentList.add(mock(TargetConsignmentModel.class));
        mockConsignmentData(consignmentList.get(0), "a12345", -81);
        mockConsignmentData(consignmentList.get(1), "a12346", -80);
        return consignmentList;
    }

}

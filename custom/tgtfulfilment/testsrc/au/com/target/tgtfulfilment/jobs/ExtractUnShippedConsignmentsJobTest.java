/**
 * 
 */
package au.com.target.tgtfulfilment.jobs;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseResponse;
import au.com.target.tgtfulfilment.service.SendToWarehouseService;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;


/**
 * @author mjanarth
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtractUnShippedConsignmentsJobTest {



    @Mock
    private TargetConsignmentService targetConsignmentService;
    @Mock
    private SendToWarehouseService sendToWarehouseService;

    @Mock
    private CronJobModel cronJobModel;

    @InjectMocks
    private final ExtractUnShippedConsignmentsJob extractUnShippedConsignmentsJob = new ExtractUnShippedConsignmentsJob();

    @Mock
    private TargetConsignmentModel mockedConsignment;

    private final List<String> warehouseCodes = new ArrayList<String>();

    private final List<TargetConsignmentModel> consignments = new ArrayList<TargetConsignmentModel>();

    private PerformResult result;


    @Test
    public void testPerformEmptyWarehouses() {
        extractUnShippedConsignmentsJob.setWarehouseCodes(warehouseCodes);
        result = extractUnShippedConsignmentsJob.perform(cronJobModel);
        verifyZeroInteractions(targetConsignmentService);
        verifyZeroInteractions(sendToWarehouseService);
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);

    }

    @Test
    public void testPerformWithWarehouseNotFound() throws NotFoundException {
        warehouseCodes.add("Incomm");
        extractUnShippedConsignmentsJob.setWarehouseCodes(warehouseCodes);
        given(
                targetConsignmentService.getAllConsignmentByStatusAndWarehouse(
                        ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, "Incomm")).willThrow(
                                new NotFoundException("exception"));
        result = extractUnShippedConsignmentsJob.perform(cronJobModel);
        verify(targetConsignmentService).getAllConsignmentByStatusAndWarehouse(
                ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, "Incomm");
        verifyZeroInteractions(sendToWarehouseService);
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);

    }

    @Test
    public void testPerformWithEmptyConsigmentList() throws NotFoundException {
        warehouseCodes.add("Incomm");
        extractUnShippedConsignmentsJob.setWarehouseCodes(warehouseCodes);
        given(
                targetConsignmentService.getAllConsignmentByStatusAndWarehouse(
                        ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, "Incomm")).willReturn(
                                new ArrayList<TargetConsignmentModel>());
        result = extractUnShippedConsignmentsJob.perform(cronJobModel);
        verify(targetConsignmentService).getAllConsignmentByStatusAndWarehouse(
                ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, "Incomm");
        verifyZeroInteractions(sendToWarehouseService);
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);

    }

    @Test
    public void testPerformWithWarehouseAndConsignmentsFound() throws NotFoundException {
        consignments.add(mockedConsignment);
        warehouseCodes.add("Incomm");
        extractUnShippedConsignmentsJob.setWarehouseCodes(warehouseCodes);
        given(
                targetConsignmentService.getAllConsignmentByStatusAndWarehouse(
                        ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, "Incomm")).willReturn(
                                consignments);
        final PollWarehouseResponse response = new PollWarehouseResponse();
        response.setSuccess(true);
        given(sendToWarehouseService.sendUnshippedConsignemnts(consignments)).willReturn(response);
        result = extractUnShippedConsignmentsJob.perform(cronJobModel);
        verify(targetConsignmentService).getAllConsignmentByStatusAndWarehouse(
                ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, "Incomm");
        verify(sendToWarehouseService).sendUnshippedConsignemnts(consignments);
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);

    }

    @Test
    public void testPerformWithMultipleWarehouseConsignmentsFound() throws NotFoundException {
        consignments.add(mockedConsignment);
        warehouseCodes.add("Incomm");
        warehouseCodes.add("fastwarehouse");
        extractUnShippedConsignmentsJob.setWarehouseCodes(warehouseCodes);
        given(
                targetConsignmentService.getAllConsignmentByStatusAndWarehouse(
                        ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, "Incomm")).willReturn(
                                consignments);
        given(
                targetConsignmentService.getAllConsignmentByStatusAndWarehouse(
                        ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, "fastwarehouse")).willThrow(
                                new NotFoundException("exception"));

        final PollWarehouseResponse response = new PollWarehouseResponse();
        response.setSuccess(true);
        given(sendToWarehouseService.sendUnshippedConsignemnts(consignments)).willReturn(response);
        result = extractUnShippedConsignmentsJob.perform(cronJobModel);
        verify(targetConsignmentService, times(1)).getAllConsignmentByStatusAndWarehouse(
                ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, "Incomm");
        verify(targetConsignmentService, times(1)).getAllConsignmentByStatusAndWarehouse(
                ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, "fastwarehouse");
        verify(sendToWarehouseService, times(1)).sendUnshippedConsignemnts(consignments);
        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);

    }

    @Test
    public void testPerformWhenPollingWarehouseRestCallFails() throws NotFoundException {
        consignments.add(mockedConsignment);
        warehouseCodes.add("Incomm");
        warehouseCodes.add("fastwarehouse");
        extractUnShippedConsignmentsJob.setWarehouseCodes(warehouseCodes);
        given(
                targetConsignmentService.getAllConsignmentByStatusAndWarehouse(
                        ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, "Incomm")).willReturn(
                                consignments);
        given(
                targetConsignmentService.getAllConsignmentByStatusAndWarehouse(
                        ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, "fastwarehouse")).willThrow(
                                new NotFoundException("exception"));

        final PollWarehouseResponse response = new PollWarehouseResponse();
        response.setSuccess(false);
        given(sendToWarehouseService.sendUnshippedConsignemnts(consignments)).willReturn(response);
        result = extractUnShippedConsignmentsJob.perform(cronJobModel);
        verify(targetConsignmentService, times(1)).getAllConsignmentByStatusAndWarehouse(
                ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, "Incomm");
        verify(sendToWarehouseService, times(1)).sendUnshippedConsignemnts(consignments);
        assertThat(result.getResult()).isEqualTo(CronJobResult.FAILURE);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.ABORTED);

    }
}

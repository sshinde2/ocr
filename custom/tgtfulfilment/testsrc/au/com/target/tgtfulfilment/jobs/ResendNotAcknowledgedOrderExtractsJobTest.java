package au.com.target.tgtfulfilment.jobs;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.dao.TargetConsignmentDao;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ResendNotAcknowledgedOrderExtractsJobTest {

    @Mock
    private TargetConsignmentDao mockTargetConsignmentDao;

    @Mock
    private TargetBusinessProcessService mockTargetBusinessProcessService;

    @Mock
    private OrderModel mockOrder;

    @Mock
    private TargetConsignmentModel mockConsignment;

    @Mock
    private TargetConsignmentModel mockConsignment2;

    @InjectMocks
    private final ResendNotAcknowledgedOrderExtractsJob resendNotAcknowledgedOrderExtractsJob = new ResendNotAcknowledgedOrderExtractsJob();

    @Before
    public void setup() {
        given(mockConsignment.getOrder()).willReturn(mockOrder);
        given(mockConsignment2.getOrder()).willReturn(mockOrder);
    }

    @Test
    public void testPerformWithNoConsignments() throws Exception {
        final PerformResult result = resendNotAcknowledgedOrderExtractsJob.perform(null);

        verify(mockTargetConsignmentDao).getAllConsignmentsNotAcknowlegedWithinTimeLimit();
        verify(mockTargetConsignmentDao).getAllExternalConsignmentsInCreatedStateWithinTimeLimit();
        verifyZeroInteractions(mockTargetBusinessProcessService);

        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }

    @Test
    public void testPerformWithGeneralException() throws Exception {
        given(mockTargetConsignmentDao.getAllConsignmentsNotAcknowlegedWithinTimeLimit()).willThrow(
                new RuntimeException("general exception"));

        final PerformResult result = resendNotAcknowledgedOrderExtractsJob.perform(null);

        assertThat(result.getResult()).isEqualTo(CronJobResult.ERROR);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.ABORTED);
    }

    @Test
    public void testPerformWithOneConsignmentNotAck() throws Exception {
        given(mockTargetConsignmentDao.getAllConsignmentsNotAcknowlegedWithinTimeLimit())
                .willReturn(Collections.singletonList(mockConsignment));

        final PerformResult result = resendNotAcknowledgedOrderExtractsJob.perform(null);

        verify(mockTargetBusinessProcessService).resendConsignmentProcess(mockConsignment);

        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }

    @Test
    public void testPerformWithTwoConsignmentsNotAck() throws Exception {
        final List<TargetConsignmentModel> mockConsignments = new ArrayList<>();
        mockConsignments.add(mockConsignment);
        mockConsignments.add(mockConsignment2);

        given(mockTargetConsignmentDao.getAllConsignmentsNotAcknowlegedWithinTimeLimit())
                .willReturn(mockConsignments);

        final PerformResult result = resendNotAcknowledgedOrderExtractsJob.perform(null);

        verify(mockTargetBusinessProcessService).resendConsignmentProcess(mockConsignment);
        verify(mockTargetBusinessProcessService).resendConsignmentProcess(mockConsignment2);

        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }

    @Test
    public void testPerformWithOneConsignmentCreated() throws Exception {
        given(mockTargetConsignmentDao.getAllExternalConsignmentsInCreatedStateWithinTimeLimit())
                .willReturn(Collections.singletonList(mockConsignment));

        final PerformResult result = resendNotAcknowledgedOrderExtractsJob.perform(null);

        verify(mockTargetBusinessProcessService).resendConsignmentProcess(mockConsignment);

        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }

    @Test
    public void testPerformWithTwoConsignmentsNotAckAndCreated() throws Exception {
        given(mockTargetConsignmentDao.getAllConsignmentsNotAcknowlegedWithinTimeLimit())
                .willReturn(Collections.singletonList(mockConsignment));
        given(mockTargetConsignmentDao.getAllExternalConsignmentsInCreatedStateWithinTimeLimit())
                .willReturn(Collections.singletonList(mockConsignment2));

        final PerformResult result = resendNotAcknowledgedOrderExtractsJob.perform(null);

        verify(mockTargetBusinessProcessService).resendConsignmentProcess(mockConsignment);
        verify(mockTargetBusinessProcessService).resendConsignmentProcess(mockConsignment2);

        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }

    @Test
    public void testPerformWithTwoConsignmentCreatedFirstFails() throws Exception {
        final List<TargetConsignmentModel> mockConsignments = new ArrayList<>();
        mockConsignments.add(mockConsignment);
        mockConsignments.add(mockConsignment2);

        given(mockTargetConsignmentDao.getAllExternalConsignmentsInCreatedStateWithinTimeLimit())
                .willReturn(mockConsignments);

        given(mockTargetBusinessProcessService.resendConsignmentProcess(mockConsignment)).willThrow(
                new RuntimeException("specific exception"));
        final PerformResult result = resendNotAcknowledgedOrderExtractsJob.perform(null);

        verify(mockTargetBusinessProcessService).resendConsignmentProcess(mockConsignment);
        verify(mockTargetBusinessProcessService).resendConsignmentProcess(mockConsignment2);

        assertThat(result.getResult()).isEqualTo(CronJobResult.ERROR);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }

    @Test
    public void testPerformWithSomeOrdersCancelled() throws Exception {
        final OrderModel mockOrder2 = mock(OrderModel.class);
        final OrderModel mockOrder3 = mock(OrderModel.class);
        final OrderModel mockOrder4 = mock(OrderModel.class);

        final TargetConsignmentModel mockConsignment3 = mock(TargetConsignmentModel.class);
        final TargetConsignmentModel mockConsignment4 = mock(TargetConsignmentModel.class);

        given(mockConsignment2.getOrder()).willReturn(mockOrder2);
        given(mockConsignment3.getOrder()).willReturn(mockOrder3);
        given(mockConsignment4.getOrder()).willReturn(mockOrder4);

        given(mockOrder.getStatus()).willReturn(OrderStatus.CREATED);
        given(mockOrder2.getStatus()).willReturn(OrderStatus.INPROGRESS);
        given(mockOrder3.getStatus()).willReturn(OrderStatus.INVOICED);
        given(mockOrder4.getStatus()).willReturn(OrderStatus.CANCELLED);

        given(mockTargetConsignmentDao.getAllExternalConsignmentsInCreatedStateWithinTimeLimit())
                .willReturn(Arrays.asList(mockConsignment, mockConsignment2, mockConsignment3, mockConsignment4));

        final PerformResult result = resendNotAcknowledgedOrderExtractsJob.perform(null);

        verify(mockTargetBusinessProcessService).resendConsignmentProcess(mockConsignment);
        verify(mockTargetBusinessProcessService).resendConsignmentProcess(mockConsignment2);
        verify(mockTargetBusinessProcessService).resendConsignmentProcess(mockConsignment3);
        verify(mockTargetBusinessProcessService, Mockito.times(0)).resendConsignmentProcess(mockConsignment4);

        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }

    @Test
    public void testPerformWithFluentOrder() {
        willReturn("fluentId").given(mockOrder).getFluentId();
        given(mockTargetConsignmentDao.getAllExternalConsignmentsInCreatedStateWithinTimeLimit())
                .willReturn(Collections.singletonList(mockConsignment));

        final PerformResult result = resendNotAcknowledgedOrderExtractsJob.perform(null);

        verify(mockTargetBusinessProcessService, never()).resendConsignmentProcess(mockConsignment);

        assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
        assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
    }
}

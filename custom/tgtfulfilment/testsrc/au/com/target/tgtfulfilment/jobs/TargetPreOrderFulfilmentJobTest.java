/**
 * 
 */
package au.com.target.tgtfulfilment.jobs;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.service.TargetPreOrderFulfilmentService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPreOrderFulfilmentJobTest {

    @Mock
    private TargetPreOrderFulfilmentService targetPreOrderFullfilmentService;

    @Mock
    private CronJobModel cronJob;

    @InjectMocks
    private final TargetPreOrderFulfilmentJob job = new TargetPreOrderFulfilmentJob();

    @Test
    public void testSuccessCronjob() {
        final PerformResult result = job.perform(cronJob);
        assertThat(CronJobStatus.FINISHED).isEqualTo(result.getStatus());
        assertThat(CronJobResult.SUCCESS).isEqualTo(result.getResult());

    }

    @Test
    public void testFailureCronjob() {
        given(job.perform(cronJob)).willThrow(new RuntimeException());
        final PerformResult result = job.perform(cronJob);
        assertThat(CronJobStatus.ABORTED).isEqualTo(result.getStatus());
        assertThat(CronJobResult.ERROR).isEqualTo(result.getResult());
    }
}

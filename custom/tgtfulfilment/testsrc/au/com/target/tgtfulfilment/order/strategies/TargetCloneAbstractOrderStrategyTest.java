/**
 * 
 */
package au.com.target.tgtfulfilment.order.strategies;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.internal.model.impl.ItemModelCloneCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.GiftRecipientModel;


/**
 * @author sbryan6
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCloneAbstractOrderStrategyTest {

    private ItemModelCloneCreator itemModelCloneCreator;

    private TargetCloneAbstractOrderStrategy strategy;

    private final AbstractOrderModel original = new AbstractOrderModel();
    private final AbstractOrderModel copy = new AbstractOrderModel();

    @Mock
    private GiftRecipientModel recipient1;

    @Mock
    private GiftRecipientModel recipient2;

    @Mock
    private GiftRecipientModel recipient3;

    @Mock
    private GiftRecipientModel recipient4;

    @Mock
    private GiftRecipientModel copyRecipient1;

    @Mock
    private GiftRecipientModel copyRecipient2;

    @Mock
    private GiftRecipientModel copyRecipient3;

    @Mock
    private GiftRecipientModel copyRecipient4;


    @Before
    public void setup() {

        itemModelCloneCreator = Mockito.mock(ItemModelCloneCreator.class);
        strategy = new TargetCloneAbstractOrderStrategy(null, itemModelCloneCreator, null);

        Mockito.when(itemModelCloneCreator.copy(recipient1)).thenReturn(copyRecipient1);
        Mockito.when(itemModelCloneCreator.copy(recipient2)).thenReturn(copyRecipient2);
        Mockito.when(itemModelCloneCreator.copy(recipient3)).thenReturn(copyRecipient3);
        Mockito.when(itemModelCloneCreator.copy(recipient4)).thenReturn(copyRecipient4);


        final List<GiftRecipientModel> origRecipients1 = new ArrayList<>();
        origRecipients1.add(recipient1);
        origRecipients1.add(recipient2);

        final List<GiftRecipientModel> origRecipients2 = new ArrayList<>();
        origRecipients2.add(recipient3);
        origRecipients2.add(recipient4);

        final AbstractOrderEntryModel entry1 = new AbstractOrderEntryModel();
        entry1.setGiftRecipients(origRecipients1);

        final AbstractOrderEntryModel entry2 = new AbstractOrderEntryModel();
        entry2.setGiftRecipients(origRecipients2);

        final List<AbstractOrderEntryModel> origEntries = new ArrayList<>();
        origEntries.add(createEntry(origRecipients1));
        origEntries.add(createEntry(origRecipients2));

        final List<AbstractOrderEntryModel> copyEntries = new ArrayList<>();
        copyEntries.add(createEntry(null));
        copyEntries.add(createEntry(null));

        original.setEntries(origEntries);
        copy.setEntries(copyEntries);
    }

    private AbstractOrderEntryModel createEntry(final List<GiftRecipientModel> list) {

        final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();
        entry.setGiftRecipients(list);
        return entry;
    }


    @Test
    public void testPostProcessNullCopy() {

        strategy.postProcess(original, null);

        Mockito.verifyZeroInteractions(itemModelCloneCreator);
    }

    @Test
    public void testPostProcessNullOriginal() {

        strategy.postProcess(null, copy);

        Mockito.verifyZeroInteractions(itemModelCloneCreator);
    }

    @Test
    public void testPostProcessEmptyOriginalEntries() {

        original.setEntries(null);
        strategy.postProcess(original, copy);

        Mockito.verifyZeroInteractions(itemModelCloneCreator);

        assertThat(copy.getEntries().get(0).getGiftRecipients()).isNullOrEmpty();
    }

    @Test
    public void testPostProcessWithEntries() {

        strategy.postProcess(original, copy);

        final List<GiftRecipientModel> recipients1 = copy.getEntries().get(0).getGiftRecipients();
        assertThat(recipients1).isNotNull().hasSize(2);
        assertThat(recipients1.get(0)).isSameAs(copyRecipient1);
        assertThat(recipients1.get(1)).isSameAs(copyRecipient2);

        final List<GiftRecipientModel> recipients2 = copy.getEntries().get(1).getGiftRecipients();
        assertThat(recipients2).isNotNull().hasSize(2);
        assertThat(recipients2.get(0)).isSameAs(copyRecipient3);
        assertThat(recipients2.get(1)).isSameAs(copyRecipient4);

    }

    @Test
    public void testPostProcessWithEntriesDifferentSizes() {

        // Change copy to only have one entry - would cause array out of bounds
        final List<AbstractOrderEntryModel> copyEntries = new ArrayList<>();
        copyEntries.add(createEntry(null));
        copy.setEntries(copyEntries);

        strategy.postProcess(original, copy);

        Mockito.verifyZeroInteractions(itemModelCloneCreator);

        assertThat(copy.getEntries().get(0).getGiftRecipients()).isNullOrEmpty();
    }



}

/**
 * 
 */
package au.com.target.tgtfulfilment.ordercancel.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections.SetUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.ordercancel.TargetOrderCancelRequest;
import au.com.target.tgtcore.ordercancel.enums.RequestOrigin;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;
import au.com.target.tgtlayby.model.PurchaseOptionModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFulfilmentCancelServiceTest {

    @InjectMocks
    private final TargetFulfilmentCancelService targetFulfilmentCancelService = new TargetFulfilmentCancelService();

    @Mock
    private TargetStockService targetStockService;

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private TargetConsignmentService targetConsignmentService;

    @Mock
    private OrderCancelRequest cancelRequest;

    @Mock
    private TargetOrderCancelRequest targetCancelRequest;

    @Mock
    private OrderCancelEntry oce;

    @Mock
    private AbstractOrderEntryModel orderEntry;

    @Mock
    private ProductModel product;

    @Mock
    private OrderModel order;

    @Mock
    private PurchaseOptionModel purchaseOption;

    @Mock
    private ConsignmentModel consignment;

    @Mock
    private TargetConsignmentModel targetConsignment;

    @Mock
    private WarehouseModel warehouseModel;

    @Mock
    private ConsignmentEntryModel consignmentEntry;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @SuppressWarnings("boxing")
    @Before
    public void setup() {

        // One cancel entry
        Mockito.when(cancelRequest.getEntriesToCancel()).thenReturn(Collections.singletonList(oce));
        Mockito.when(oce.getOrderEntry()).thenReturn(orderEntry);
        Mockito.when(orderEntry.getProduct()).thenReturn(product);
        Mockito.when(orderEntry.getOrder()).thenReturn(order);
        Mockito.when(order.getPurchaseOption()).thenReturn(purchaseOption);
        Mockito.when(orderEntry.getConsignmentEntries()).thenReturn(Collections.singleton(consignmentEntry));
        Mockito.when(consignmentEntry.getConsignment()).thenReturn(consignment);
        Mockito.when(oce.getCancelQuantity()).thenReturn(2l);
        Mockito.when(targetConsignmentService.getActiveConsignmentsForOrder(order)).thenReturn(
                Collections.singletonList(targetConsignment));
    }

    @Test
    public void testReleaseStockNotPicked() throws OrderCancelException {

        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);

        targetFulfilmentCancelService.releaseStock(cancelRequest);

        Mockito.verify(targetStockService).release(Mockito.eq(product), Mockito.eq(2),
                Mockito.anyString());
    }

    @Test
    public void testReleaseStockPicked() throws OrderCancelException {
        Mockito.when(consignmentEntry.getConsignment()).thenReturn(targetConsignment);
        Mockito.when(targetConsignment.getStatus()).thenReturn(ConsignmentStatus.PICKED);
        Mockito.when(targetConsignment.getPickConfirmDate()).thenReturn(new Date());
        targetFulfilmentCancelService.releaseStock(cancelRequest);

        Mockito.verifyZeroInteractions(targetStockService);
    }

    @Test
    public void testGetOnlineWarehouse() {
        BDDMockito.given(targetWarehouseService.getDefaultOnlineWarehouse()).willReturn(
                warehouseModel);
        Assert.assertNotNull(targetFulfilmentCancelService.getOnlineWarehouse());
    }

    @Test
    public void getIsExtractCancelToWarehouseRequiredNoConsignments() {

        Mockito.when(order.getConsignments()).thenReturn(SetUtils.EMPTY_SET);

        Assert.assertFalse(targetFulfilmentCancelService.isExtractCancelToWarehouseRequired(order));
        Mockito.verify(targetConsignmentService).getActiveConsignmentsForOrder(order);
    }

    @Test
    public void getIsExtractCancelToWarehouseRequiredInvalidConsignment() {
        consignment = targetConsignment;
        Mockito.when(order.getConsignments()).thenReturn(Collections.singleton(consignment));
        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.CANCELLED);

        Assert.assertFalse(targetFulfilmentCancelService.isExtractCancelToWarehouseRequired(order));
        Mockito.verify(targetConsignmentService).getActiveConsignmentsForOrder(order);
        Mockito.verify(consignment).getStatus();
    }

    @Test
    public void getIsExtractCancelToWarehouseRequiredValidConsignment() {
        consignment = targetConsignment;
        Mockito.when(order.getConsignments()).thenReturn(Collections.singleton(consignment));
        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);

        Assert.assertTrue(targetFulfilmentCancelService.isExtractCancelToWarehouseRequired(order));
        Mockito.verify(targetConsignmentService).getActiveConsignmentsForOrder(order);
        Mockito.verify(consignment).getStatus();
    }

    @Test
    public void testOrderEntryRequiresStockReleaseNoConsignmentEntries() {

        Mockito.when(orderEntry.getConsignmentEntries()).thenReturn(null);
        Assert.assertTrue(targetFulfilmentCancelService.orderEntryRequiresStockRelease(orderEntry));
    }

    @Test
    public void testOrderEntryRequiresStockReleaseCancelled() {

        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.CANCELLED);
        Assert.assertTrue(targetFulfilmentCancelService.orderEntryRequiresStockRelease(orderEntry));
    }

    @Test
    public void testOrderEntryRequiresStockReleaseSent() {

        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.SENT_TO_WAREHOUSE);
        Assert.assertTrue(targetFulfilmentCancelService.orderEntryRequiresStockRelease(orderEntry));
    }

    @Test
    public void testOrderEntryRequiresStockReleaseRerouted() {

        final Set<ConsignmentEntryModel> consignmentEntries = new HashSet<>();
        consignmentEntries.add(createConsignmentEntry(ConsignmentStatus.CANCELLED));
        consignmentEntries.add(createConsignmentEntry(ConsignmentStatus.SENT_TO_WAREHOUSE));

        Mockito.when(orderEntry.getConsignmentEntries()).thenReturn(consignmentEntries);

        Assert.assertTrue(targetFulfilmentCancelService.orderEntryRequiresStockRelease(orderEntry));
    }

    @Test
    public void testUpdateConsignmentStatusNoConsignment() {

        Mockito.when(targetConsignmentService.getActiveConsignmentsForOrder(order)).thenReturn(
                Collections.EMPTY_LIST);

        Mockito.when(cancelRequest.getOrder()).thenReturn(order);
        targetFulfilmentCancelService.updateConsignmentStatus(cancelRequest);
        Mockito.verify(targetConsignmentService).getActiveConsignmentsForOrder(order);

    }

    @Test
    public void testUpdateConsignmentStatusWhenOrderIsFullyCancelled() {
        Mockito.when(order.getConsignments()).thenReturn(Collections.singleton(consignment));
        Mockito.when(Boolean.valueOf(cancelRequest.isPartialCancel())).thenReturn(Boolean.FALSE);
        Mockito.when(cancelRequest.getOrder()).thenReturn(order);
        targetFulfilmentCancelService.updateConsignmentStatus(cancelRequest);

        Mockito.verify(targetConsignmentService).getActiveConsignmentsForOrder(order);
        Mockito.verify(targetConsignment).setRejectReason(ConsignmentRejectReason.CANCELLED_BY_CUSTOMER);
        Mockito.verify(targetConsignment).setCancelDate(Mockito.any(Date.class));
        Mockito.verify(targetConsignment).setStatus(ConsignmentStatus.CANCELLED);
    }

    @Test
    public void testUpdateConsignmentStatusWhenOrderIsFullyCancelledByCsAgentOtherReason() {
        Mockito.when(order.getConsignments()).thenReturn(Collections.singleton(consignment));
        Mockito.when(Boolean.valueOf(targetCancelRequest.isPartialCancel())).thenReturn(Boolean.FALSE);
        Mockito.when(targetCancelRequest.getCancelReason()).thenReturn(CancelReason.LATEDELIVERY);
        Mockito.when(targetCancelRequest.getOrder()).thenReturn(order);
        Mockito.when(targetCancelRequest.getRequestOrigin()).thenReturn(RequestOrigin.CSCOCKPIT);
        targetFulfilmentCancelService.updateConsignmentStatus(targetCancelRequest);

        Mockito.verify(targetConsignmentService).getActiveConsignmentsForOrder(order);
        Mockito.verify(targetConsignment).setRejectReason(ConsignmentRejectReason.CANCELLED_BY_CUSTOMER);
        Mockito.verify(targetConsignment).setCancelDate(Mockito.any(Date.class));
        Mockito.verify(targetConsignment).setStatus(ConsignmentStatus.CANCELLED);
    }


    @Test
    public void testUpdateConsignmentStatusWhenOrderIsFullyCancelledByCsAgentOutOfStock() {
        Mockito.when(order.getConsignments()).thenReturn(Collections.singleton(consignment));
        Mockito.when(Boolean.valueOf(targetCancelRequest.isPartialCancel())).thenReturn(Boolean.FALSE);
        Mockito.when(targetCancelRequest.getCancelReason()).thenReturn(CancelReason.OUTOFSTOCK);
        Mockito.when(targetCancelRequest.getOrder()).thenReturn(order);
        Mockito.when(targetCancelRequest.getRequestOrigin()).thenReturn(RequestOrigin.CSCOCKPIT);
        targetFulfilmentCancelService.updateConsignmentStatus(targetCancelRequest);

        Mockito.verify(targetConsignmentService).getActiveConsignmentsForOrder(order);
        Mockito.verify(targetConsignment).setRejectReason(ConsignmentRejectReason.CANCELLED_BY_CUSTOMER);
        Mockito.verify(targetConsignment).setCancelDate(Mockito.any(Date.class));
        Mockito.verify(targetConsignment).setStatus(ConsignmentStatus.CANCELLED);
    }


    @Test
    public void testUpdateConsignmentStatusWhenOrderIsZeroPicked() {
        Mockito.when(order.getConsignments()).thenReturn(Collections.singleton(consignment));
        Mockito.when(Boolean.valueOf(targetCancelRequest.isPartialCancel())).thenReturn(Boolean.FALSE);
        Mockito.when(targetCancelRequest.getCancelReason()).thenReturn(CancelReason.OUTOFSTOCK);
        Mockito.when(targetCancelRequest.getOrder()).thenReturn(order);
        Mockito.when(targetCancelRequest.getRequestOrigin()).thenReturn(RequestOrigin.WAREHOUSE);
        targetFulfilmentCancelService.updateConsignmentStatus(targetCancelRequest);

        Mockito.verify(targetConsignmentService).getActiveConsignmentsForOrder(order);
        Mockito.verify(targetConsignment).setRejectReason(ConsignmentRejectReason.ZERO_PICK_BY_WAREHOUSE);
        Mockito.verify(targetConsignment).setCancelDate(Mockito.any(Date.class));
        Mockito.verify(targetConsignment).setStatus(ConsignmentStatus.CANCELLED);
    }

    @Test
    public void testUpdateConsignmentStatusWhenOrderIsNotFullyPartiallyCancelled() {
        Mockito.when(cancelRequest.getOrder()).thenReturn(order);
        Mockito.when(targetConsignmentService
                .getActiveConsignmentsForOrder(order)).thenReturn(Collections.singletonList(targetConsignment));
        Mockito.when(Boolean.valueOf(cancelRequest.isPartialCancel())).thenReturn(Boolean.TRUE);
        Mockito.when(cancelRequest.getOrder()).thenReturn(order);
        Mockito.when(targetConsignment.getConsignmentEntries()).thenReturn(Collections.singleton(consignmentEntry));
        Mockito.when(consignmentEntry.getOrderEntry()).thenReturn(orderEntry);
        Mockito.when(orderEntry.getQuantity()).thenReturn(Long.valueOf(2));
        targetFulfilmentCancelService.updateConsignmentStatus(cancelRequest);
        Mockito.verify(targetConsignmentService).getActiveConsignmentsForOrder(order);

        Mockito.verify(targetConsignment, Mockito.times(0)).setRejectReason(
                ConsignmentRejectReason.CANCELLED_BY_CUSTOMER);
        Mockito.verify(targetConsignment, Mockito.times(0)).setCancelDate(Mockito.any(Date.class));
        Mockito.verify(targetConsignment, Mockito.times(0)).setStatus(ConsignmentStatus.CANCELLED);
    }

    @Test
    public void testVerifyNoCloneWhenAllConsignmentEntriesPartiallyCancel() {
        consignment = targetConsignment;
        Mockito.when(cancelRequest.getOrder()).thenReturn(order);
        Mockito.when(targetConsignmentService
                .getActiveConsignmentsForOrder(order)).thenReturn(Collections.singletonList(targetConsignment));
        Mockito.when(Boolean.valueOf(cancelRequest.isPartialCancel())).thenReturn(Boolean.TRUE);
        Mockito.when(order.getEntries()).thenReturn(Collections.singletonList(orderEntry));
        Mockito.when(orderEntry.getConsignmentEntries()).thenReturn(Collections.singleton(consignmentEntry));
        Mockito.when(consignmentEntry.getConsignment()).thenReturn(consignment);
        Mockito.when(orderEntry.getQuantity()).thenReturn(Long.valueOf(0));
        targetFulfilmentCancelService.updateConsignmentStatus(cancelRequest);
        Mockito.verify(modelService, Mockito.times(0)).clone(consignmentEntry);
    }

    @Test
    public void testUpdateConsignmentStatusWhenOrderIsFullyPartiallyCancelled() {
        Mockito.when(cancelRequest.getOrder()).thenReturn(order);
        Mockito.when(targetConsignmentService
                .getActiveConsignmentsForOrder(order)).thenReturn(Collections.singletonList(targetConsignment));
        Mockito.when(Boolean.valueOf(cancelRequest.isPartialCancel())).thenReturn(Boolean.TRUE);
        Mockito.when(cancelRequest.getOrder()).thenReturn(order);
        Mockito.when(targetConsignment.getConsignmentEntries()).thenReturn(Collections.singleton(consignmentEntry));
        Mockito.when(consignmentEntry.getOrderEntry()).thenReturn(orderEntry);
        Mockito.when(orderEntry.getQuantity()).thenReturn(Long.valueOf(0));
        targetFulfilmentCancelService.updateConsignmentStatus(cancelRequest);
        Mockito.verify(targetConsignmentService).getActiveConsignmentsForOrder(order);

        Mockito.verify(targetConsignment, Mockito.times(1)).setRejectReason(
                ConsignmentRejectReason.CANCELLED_BY_CUSTOMER);
        Mockito.verify(targetConsignment, Mockito.times(1)).setCancelDate(Mockito.any(Date.class));
        Mockito.verify(targetConsignment, Mockito.times(1)).setStatus(ConsignmentStatus.CANCELLED);
    }

    private ConsignmentEntryModel createConsignmentEntry(final ConsignmentStatus status) {

        final ConsignmentEntryModel mockConsignmentEntry = Mockito.mock(ConsignmentEntryModel.class);
        final ConsignmentModel mockConsignment = Mockito.mock(ConsignmentModel.class);

        Mockito.when(mockConsignmentEntry.getConsignment()).thenReturn(mockConsignment);
        Mockito.when(mockConsignment.getStatus()).thenReturn(status);

        return mockConsignmentEntry;
    }


}

/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * Test cases for AlreadyTriedStoreFilterImpl.
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AlreadyTriedStoreDenialStrategyTest {

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private AbstractOrderEntryModel entry;

    @Mock
    private AbstractOrderModel order;

    @Mock
    private TargetConsignmentModel consignment1;

    @Mock
    private TargetConsignmentModel consignment2;

    @Mock
    private WarehouseModel warehouse1;

    @Mock
    private WarehouseModel warehouse2;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @InjectMocks
    private final AlreadyTriedStoreDenialStrategy alreadyTriedStoreFilter = new AlreadyTriedStoreDenialStrategy();

    @Before
    public void setupOrderData() {
        final Collection<PointOfServiceModel> poses1 = new ArrayList<>();
        poses1.add(createPOS(1));
        poses1.add(createPOS(3));

        final Collection<PointOfServiceModel> poses2 = new ArrayList<>();
        poses2.add(createPOS(2));

        BDDMockito.given(oeg.get(0)).willReturn(entry);
        BDDMockito.given(entry.getOrder()).willReturn(order);

        BDDMockito.given(consignment1.getWarehouse()).willReturn(warehouse1);
        BDDMockito.given(consignment1.getStatus()).willReturn(ConsignmentStatus.CANCELLED);
        BDDMockito.given(warehouse1.getPointsOfService()).willReturn(poses1);

        BDDMockito.given(consignment2.getWarehouse()).willReturn(warehouse2);
        BDDMockito.given(consignment2.getStatus()).willReturn(ConsignmentStatus.PICKPACK);
        BDDMockito.given(warehouse2.getPointsOfService()).willReturn(poses2);

        final Set<ConsignmentModel> consignments = new HashSet<>();
        consignments.add(consignment1);
        consignments.add(consignment2);

        BDDMockito.given(order.getConsignments()).willReturn(consignments);
    }

    @Test
    public void testWhenStoreNumberInCanceledConsignment() {
        final DenialResponse response = alreadyTriedStoreFilter.isDenied(oeg, createPOS(1));
        Assert.assertTrue(response.isDenied());
    }

    @Test
    public void testWhenStoreNumberInAnyConsignment() {
        BDDMockito.given(
                Boolean.valueOf(targetFeatureSwitchService
                        .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON))).willReturn(Boolean.TRUE);
        final DenialResponse response = alreadyTriedStoreFilter.isDenied(oeg, createPOS(2));
        Assert.assertTrue(response.isDenied());
    }

    @Test
    public void testWhenStoreIsNull() {
        final DenialResponse response = alreadyTriedStoreFilter.isDenied(oeg, null);
        Assert.assertTrue(response.isDenied());
    }

    @Test
    public void testWhenStoreNumberInNotCanceledConsignment() {
        final DenialResponse response = alreadyTriedStoreFilter.isDenied(oeg, createPOS(2));
        Assert.assertFalse(response.isDenied());
    }

    @Test
    public void testWhenStoreNumberNotInAnyConsignment() {
        final DenialResponse response = alreadyTriedStoreFilter.isDenied(oeg, createPOS(4));
        Assert.assertFalse(response.isDenied());
    }

    @Test
    public void testWhenConsignmentsAreNull() {
        BDDMockito.given(order.getConsignments()).willReturn(null);
        final DenialResponse response = alreadyTriedStoreFilter.isDenied(oeg, createPOS(1));
        Assert.assertFalse(response.isDenied());
    }

    @Test
    public void testWhenConsignmentsStatusesAreNull() {
        BDDMockito.given(consignment1.getStatus()).willReturn(null);
        final DenialResponse response = alreadyTriedStoreFilter.isDenied(oeg, createPOS(1));
        Assert.assertFalse(response.isDenied());
    }

    @Test
    public void testWhenWarehouseForConsignmentIsNull() {
        BDDMockito.given(consignment1.getWarehouse()).willReturn(null);
        final DenialResponse response = alreadyTriedStoreFilter.isDenied(oeg, createPOS(1));
        Assert.assertFalse(response.isDenied());
    }

    @Test
    public void testWhenPosesForWarehouseIsNull() {
        BDDMockito.given(warehouse1.getPointsOfService()).willReturn(null);
        final DenialResponse response = alreadyTriedStoreFilter.isDenied(oeg, createPOS(1));
        Assert.assertFalse(response.isDenied());
    }

    @Test
    public void testWhenPosesForWarehouseSameStore() {
        BDDMockito.given(order.getCncStoreNumber()).willReturn(Integer.valueOf(1));
        final DenialResponse response = alreadyTriedStoreFilter.isDenied(oeg, createPOS(1));
        Assert.assertTrue(response.isDenied());
    }

    private TargetPointOfServiceModel createPOS(final int storeNumber)
    {
        final TargetPointOfServiceModel tpos = Mockito.mock(TargetPointOfServiceModel.class);
        BDDMockito.given(tpos.getStoreNumber()).willReturn(Integer.valueOf(storeNumber));
        return tpos;
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;
import au.com.target.tgtfulfilment.storeblackout.StoreBlackOutService;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GlobalBlackoutPeriodDenialStrategyTest {

    @Mock
    private StoreBlackOutService storeBlackoutService;

    @Mock
    private GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel;

    @Mock
    private TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService;

    @Mock
    private OrderEntryGroup oeg;

    @InjectMocks
    private final GlobalBlackoutPeriodDenialStrategy strategy = new GlobalBlackoutPeriodDenialStrategy();

    @Before
    public void setUp() {
        BDDMockito.given(targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites()).willReturn(
                capabilitiesModel);
    }

    /**
     * isNotInGlobalBlackout - No
     */
    @Test
    public void testGlobalRulesPassedWhenGlobalBlackoutIsGoingOn() {
        BDDMockito.given(
                Boolean.valueOf(storeBlackoutService.doesBlackOutPeriodApply(capabilitiesModel.getBlackoutStart(),
                        capabilitiesModel.getBlackoutEnd())))
                .willReturn(Boolean.TRUE);
        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertTrue("All global rules are satisfied",
                denialResponse.isDenied());
    }

    /**
     * isNotInGlobalBlackout - Yes
     */
    @Test
    public void testGlobalRulesPassedWhenNotInGlobalBlackout() {
        BDDMockito.given(
                Boolean.valueOf(storeBlackoutService.doesBlackOutPeriodApply(capabilitiesModel.getBlackoutStart(),
                        capabilitiesModel.getBlackoutEnd())))
                .willReturn(Boolean.FALSE);
        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertFalse("All global rules are satisfied",
                denialResponse.isDenied());
    }
}

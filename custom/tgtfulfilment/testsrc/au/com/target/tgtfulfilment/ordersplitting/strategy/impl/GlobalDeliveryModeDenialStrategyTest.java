/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GlobalDeliveryModeDenialStrategyTest {

    @Mock
    private TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService;

    @Mock
    private GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel;

    @Mock
    private TargetZoneDeliveryModeModel deliveryModesEnabledForGlobalFulfilment;

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private OEGParameterHelper oegParameterHelper;

    @InjectMocks
    private final GlobalDeliveryModeDenialStrategy strategy = new GlobalDeliveryModeDenialStrategy();

    @Before
    public void setUp() {
        BDDMockito.given(targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites()).willReturn(
                capabilitiesModel);
        final TargetZoneDeliveryModeModel deliveryMode = new TargetZoneDeliveryModeModel();
        deliveryMode.setCode("click-n-collect");
        BDDMockito.given(capabilitiesModel.getDeliveryModes()).willReturn(Collections.singleton(deliveryMode));
    }

    /**
     * isDeliveryModeEnabledForInstoreFulfilment - No |
     *
     */
    @Test
    public void testGlobalRulesPassedWhenDeliveryModeIsNotEnabled() {
        final TargetZoneDeliveryModeModel deliveryMode = new TargetZoneDeliveryModeModel();
        deliveryMode.setCode("Express-Delivery");
        BDDMockito.given(oegParameterHelper.getDeliveryMode(oeg)).willReturn(deliveryMode);
        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertTrue("All global rules are satisfied",
                denialResponse.isDenied());
    }

    /**
     * isDeliveryModeEnabledForInstoreFulfilment - No |
     * 
     */
    @Test
    public void testGlobalRulesPassedWhenNoDeliveryModeIsEnabled() {
        BDDMockito.given(capabilitiesModel.getDeliveryModes()).willReturn(null);
        final TargetZoneDeliveryModeModel deliveryMode = new TargetZoneDeliveryModeModel();
        deliveryMode.setCode("click-n-collect");
        BDDMockito.given(oegParameterHelper.getDeliveryMode(oeg)).willReturn(deliveryMode);
        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertTrue("All global rules are satisfied",
                denialResponse.isDenied());
    }

    @Test
    public void testGlobalRulesPassedWhenDeliveryModeIsEnabled() {
        final TargetZoneDeliveryModeModel deliveryMode = new TargetZoneDeliveryModeModel();
        deliveryMode.setCode("click-n-collect");
        BDDMockito.given(oegParameterHelper.getDeliveryMode(oeg)).willReturn(deliveryMode);
        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertFalse("All global rules are satisfied",
                denialResponse.isDenied());
    }
}

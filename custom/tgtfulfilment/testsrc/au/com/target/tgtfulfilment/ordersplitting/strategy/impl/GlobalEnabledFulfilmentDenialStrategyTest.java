/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GlobalEnabledFulfilmentDenialStrategyTest {

    @Mock
    private TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService;

    @Mock
    private GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel;

    @Mock
    private OrderEntryGroup oeg;

    @InjectMocks
    private final GlobalEnabledFulfilmentDenialStrategy strategy = new GlobalEnabledFulfilmentDenialStrategy();

    @Before
    public void setUp() {
        BDDMockito.given(targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites()).willReturn(
                capabilitiesModel);
    }

    /**
     * | isEnabled - No
     */
    @Test
    public void testGlobalRulesPassedWhenGlobalFulfilmentIsDisabled() {

        BDDMockito.given(Boolean.valueOf(capabilitiesModel.isEnabled())).willReturn(Boolean.FALSE);
        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertTrue("All global rules are satisfied",
                denialResponse.isDenied());
    }

    /**
     * | isEnabled - Yes
     */
    @Test
    public void testGlobalRulesPassedWhenGlobalFulfilmentIsEnabled() {
        BDDMockito.given(Boolean.valueOf(capabilitiesModel.isEnabled())).willReturn(Boolean.TRUE);
        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertFalse("All global rules are satisfied",
                denialResponse.isDenied());
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.category.TargetCategoryService;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.model.CategoryExclusionsModel;
import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.FilterExclusionListService;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GlobalExcludeCategoryDenialStrategyTest {

    @Mock
    private TargetCategoryService categoryService;

    @Mock
    private GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel;

    @Mock
    private TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService;

    @Mock
    private OEGParameterHelper oegParameterHelper;

    @Mock
    private TargetStoreFulfilmentCapabilitiesService capabilitiesService;

    @Mock
    private FilterExclusionListService filterExclusionListService;

    @Mock
    private CategoryExclusionsModel catExclusion1;

    @Mock
    private CategoryExclusionsModel catExclusion2;

    @InjectMocks
    private final GlobalExcludeCategoryDenialStrategy strategy = new GlobalExcludeCategoryDenialStrategy();

    private final CategoryModel excludeCategory1 = new CategoryModel();
    private final CategoryModel excludeCategory2 = new CategoryModel();
    private final CategoryModel excludeCategory3 = new CategoryModel();

    @Before
    public void setUp() {
        BDDMockito.given(targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites()).willReturn(
                capabilitiesModel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGlobalRulesNotDeniedWhenExcludeCategoriesIsEmpty() {
        final OrderEntryGroup oeg = null;
        BDDMockito.given(capabilitiesModel.getExcludeCategories()).willReturn(createExcludeCategoryModelList());
        strategy.isDenied(oeg);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGlobalRulesDeniedWhenOrderEntryGroupIsEmpty() {
        final OrderEntryGroup oeg = new OrderEntryGroup();
        BDDMockito.given(capabilitiesModel.getExcludeCategories()).willReturn(createExcludeCategoryModelList());
        strategy.isDenied(oeg);
    }

    @Test
    public void testGlobalRulesNotDenied() {
        final OrderEntryGroup oeg = new OrderEntryGroup();
        final OrderEntryModel entryModel1 = Mockito.mock(OrderEntryModel.class);
        oeg.add(entryModel1);

        BDDMockito.given(capabilitiesModel.getExcludeCategories()).willReturn(createExcludeCategoryModelList());
        BDDMockito.given(Boolean
                .valueOf(capabilitiesService.isOrderContainingProductsFromExcludedCategories(Mockito.eq(oeg),
                        Mockito.anyList())))
                .willReturn(Boolean.FALSE);

        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertFalse("Create not Denied", denialResponse.isDenied());
    }

    @Test
    public void testGlobalRulesDeniedWithExcludeCategory() {
        final OrderEntryGroup oeg = new OrderEntryGroup();
        final OrderEntryModel entryModel1 = Mockito.mock(OrderEntryModel.class);
        oeg.add(entryModel1);

        BDDMockito.given(Boolean
                .valueOf(capabilitiesService.isOrderContainingProductsFromExcludedCategories(Mockito.eq(oeg),
                        Mockito.anyList())))
                .willReturn(Boolean.TRUE);

        BDDMockito.given(capabilitiesModel.getExcludeCategories()).willReturn(createExcludeCategoryModelList());

        BDDMockito.given(
                filterExclusionListService.filterActiveCategoryModelsByDateRange(capabilitiesModel
                        .getExcludeCategories())).willReturn(getCategoryExclusionList());

        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertTrue("Create Denied", denialResponse.isDenied());
    }

    @Test
    public void testGlobalRulesNotDeniedWithNoneOftheProductsFromExcludeCategory() {
        final OrderEntryGroup oeg = new OrderEntryGroup();
        final OrderEntryModel entryModel1 = Mockito.mock(OrderEntryModel.class);
        oeg.add(entryModel1);

        BDDMockito.given(Boolean
                .valueOf(capabilitiesService.isOrderContainingProductsFromExcludedCategories(Mockito.eq(oeg),
                        Mockito.anyList())))
                .willReturn(Boolean.FALSE);

        BDDMockito.given(capabilitiesModel.getExcludeCategories()).willReturn(createExcludeCategoryModelList());

        BDDMockito.given(
                filterExclusionListService.filterActiveCategoryModelsByDateRange(capabilitiesModel
                        .getExcludeCategories())).willReturn(getCategoryExclusionList());

        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertFalse("Create Not Denied", denialResponse.isDenied());
    }

    @Test
    public void testGlobalRulesNotDeniedWithNullExcludeCategory() {
        final OrderEntryGroup oeg = new OrderEntryGroup();
        final OrderEntryModel entryModel1 = Mockito.mock(OrderEntryModel.class);
        oeg.add(entryModel1);

        BDDMockito.given(Boolean
                .valueOf(capabilitiesService.isOrderContainingProductsFromExcludedCategories(Mockito.eq(oeg),
                        Mockito.anyList())))
                .willReturn(Boolean.TRUE);

        BDDMockito.given(capabilitiesModel.getExcludeCategories()).willReturn(createExcludeCategoryModelList());

        BDDMockito.given(
                filterExclusionListService.filterActiveCategoryModelsByDateRange(capabilitiesModel
                        .getExcludeCategories())).willReturn(null);

        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertFalse("Create Not Denied", denialResponse.isDenied());
    }

    @Test
    public void testGlobalRulesNotDeniedWithNullCategories() {
        final OrderEntryGroup oeg = new OrderEntryGroup();
        final OrderEntryModel entryModel1 = Mockito.mock(OrderEntryModel.class);
        oeg.add(entryModel1);

        BDDMockito.given(Boolean
                .valueOf(capabilitiesService.isOrderContainingProductsFromExcludedCategories(Mockito.eq(oeg),
                        Mockito.anyList())))
                .willReturn(Boolean.TRUE);

        BDDMockito.given(capabilitiesModel.getExcludeCategories()).willReturn(null);

        BDDMockito.given(
                filterExclusionListService.filterActiveCategoryModelsByDateRange(capabilitiesModel
                        .getExcludeCategories())).willReturn(getCategoryExclusionList());

        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertFalse("Create Not Denied", denialResponse.isDenied());
    }

    private Set<CategoryExclusionsModel> createExcludeCategoryModelList() {
        final CategoryExclusionsModel excludeCate1 = new CategoryExclusionsModel();
        excludeCate1.setCategory(excludeCategory1);
        final CategoryExclusionsModel excludeCate2 = new CategoryExclusionsModel();
        excludeCate2.setCategory(excludeCategory2);
        final CategoryExclusionsModel excludeCate3 = new CategoryExclusionsModel();
        excludeCate3.setCategory(excludeCategory3);
        return Sets.newHashSet(excludeCate1, excludeCate2, excludeCate3);
    }


    private List<CategoryModel> getCategoryExclusionList() {
        return Lists.newArrayList(excludeCategory1, excludeCategory2, excludeCategory3);
    }
}

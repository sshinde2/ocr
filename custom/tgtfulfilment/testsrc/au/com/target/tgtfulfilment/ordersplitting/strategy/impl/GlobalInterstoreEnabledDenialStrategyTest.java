/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;


/**
 * @author smishra1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GlobalInterstoreEnabledDenialStrategyTest {

    @Mock
    private TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService;

    @Mock
    private GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel;

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private OEGParameterHelper oegParameterHelper;

    @InjectMocks
    private final GlobalInterstoreEnabledDenialStrategy strategy = new GlobalInterstoreEnabledDenialStrategy();

    @Before
    public void setUp() {
        BDDMockito.given(targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites()).willReturn(
                capabilitiesModel);
    }

    /**
     * Enabled - False and CnC order
     */
    @Test
    public void testAllowInterstoreFalse() {

        Mockito.when(oegParameterHelper.getCncStoreNumber(oeg)).thenReturn(Integer.valueOf(1000));
        Mockito.when(Boolean.valueOf(capabilitiesModel.isAllowInterstoreDelivery())).thenReturn(Boolean.FALSE);

        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertTrue("InterStoreDeliveryDisabled", denialResponse.isDenied());
    }

    /**
     * Enabled - False and not a CnC Order
     */
    @Test
    public void testNotCnCOrder() {

        Mockito.when(oegParameterHelper.getCncStoreNumber(oeg)).thenReturn(null);
        Mockito.when(Boolean.valueOf(capabilitiesModel.isAllowInterstoreDelivery())).thenReturn(Boolean.FALSE);

        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertFalse(denialResponse.isDenied());
    }

    /**
     * Enabled - true and CnC order
     */
    @Test
    public void testAllowInterstoreTrue() {

        Mockito.when(oegParameterHelper.getCncStoreNumber(oeg)).thenReturn(Integer.valueOf(1000));
        Mockito.when(Boolean.valueOf(capabilitiesModel.isAllowInterstoreDelivery())).thenReturn(Boolean.TRUE);

        final DenialResponse denialResponse = strategy.isDenied(oeg);
        Assert.assertFalse(denialResponse.isDenied());
    }
}

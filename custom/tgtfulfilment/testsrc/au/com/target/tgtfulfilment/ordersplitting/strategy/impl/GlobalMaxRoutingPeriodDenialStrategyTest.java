/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GlobalMaxRoutingPeriodDenialStrategyTest {

    @Mock
    private OEGParameterHelper oegParameterHelper;

    @Mock
    private TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService;

    @Mock
    private GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel;

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private OrderModel orderModel;

    @InjectMocks
    private final GlobalMaxRoutingPeriodDenialStrategy strategy = new GlobalMaxRoutingPeriodDenialStrategy();

    @Before
    public void setUp() {
        given(targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites()).willReturn(
                capabilitiesModel);
        given(oegParameterHelper.getOrderFromGivenGroup(oeg)).willReturn(orderModel);

    }

    @Test
    public void testStrategyWhenExceedMaxRoutingPeriod() {
        final Calendar creationTime = Calendar.getInstance();
        creationTime.add(Calendar.MINUTE, -5);
        final Date creationDate = creationTime.getTime();
        given(orderModel.getCreationtime()).willReturn(creationDate);
        given(capabilitiesModel.getMaxRoutingPeriod()).willReturn(Integer.valueOf(4));
        final DenialResponse result = strategy.isDenied(oeg);
        assertThat(result.isDenied()).isTrue();
    }

    @Test
    public void testStrategyWhenNotExceedMaxRoutingPeriod() {
        final Calendar creationTime = Calendar.getInstance();
        creationTime.add(Calendar.MINUTE, -3);
        final Date creationDate = creationTime.getTime();
        given(orderModel.getCreationtime()).willReturn(creationDate);
        given(capabilitiesModel.getMaxRoutingPeriod()).willReturn(Integer.valueOf(4));
        final DenialResponse result = strategy.isDenied(oeg);
        assertThat(result.isDenied()).isFalse();
    }

}

/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.model.ProductExclusionsModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.FilterExclusionListService;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;

import com.google.common.collect.Sets;


/**
 * @author smishra1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GlobalProductExclusionDenialStrategyTest {

    @Mock
    private TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService;

    @Mock
    private TargetStoreFulfilmentCapabilitiesService storeFulfilmentCapabilitiesService;

    @Mock
    private GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel;

    @Mock
    private FilterExclusionListService filterExclusionListService;

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private ProductExclusionsModel productExclusion1;

    @Mock
    private ProductExclusionsModel productExclusion2;

    @Mock
    private ProductExclusionsModel productExclusion3;


    @InjectMocks
    private final GlobalProductExclusionDenialStrategy globalProductExclusionDenialStrategy = new GlobalProductExclusionDenialStrategy();



    @Before
    public void setUp() {
        BDDMockito.given(targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites()).willReturn(
                capabilitiesModel);
        BDDMockito.given(capabilitiesModel.getProductExclusions()).willReturn(getProductExclusionList());
        BDDMockito.given(filterExclusionListService
                .filterActiveProductExclusions(capabilitiesModel.getProductExclusions())).willReturn(
                getProductExclusionList());
    }

    @Test
    public void testDeniedForProductInExcluded() {
        BDDMockito.given(capabilitiesModel.getProductExclusions()).willReturn(getProductExclusionList());
        BDDMockito
                .given(Boolean.valueOf(
                        storeFulfilmentCapabilitiesService.isProductInExclusionList(oeg, getProductExclusionList())))
                .willReturn(Boolean.TRUE);
        final DenialResponse denialResponse = globalProductExclusionDenialStrategy.isDenied(oeg);
        Assert.assertTrue("Create Denied", denialResponse.isDenied());
    }

    @Test
    public void testDeniedForProductNotExcluded() {
        BDDMockito
                .given(Boolean.valueOf(
                        storeFulfilmentCapabilitiesService.isProductInExclusionList(oeg, getProductExclusionList())))
                .willReturn(Boolean.FALSE);
        final DenialResponse denialResponse = globalProductExclusionDenialStrategy.isDenied(oeg);
        Assert.assertFalse("Create Not Denied", denialResponse.isDenied());
    }

    private Set<ProductExclusionsModel> getProductExclusionList() {
        return Sets.newHashSet(productExclusion1, productExclusion2, productExclusion3);
    }


}
/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.ordersplitting.strategy.GlobalFulfilmentDenialStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class InstoreFulfilmentGlobalRulesCheckerTest {

    private static final String ORDER_CODE = "testOrder";

    @InjectMocks
    private final InstoreFulfilmentGlobalRulesCheckerImpl checker = new InstoreFulfilmentGlobalRulesCheckerImpl();

    @Mock
    private GlobalFulfilmentDenialStrategy globalDispatchFulfilmentDenialStrategy;

    @Mock
    private GlobalFulfilmentDenialStrategy globalStoreFulfilmentDenialStrategy;

    @Mock
    private OEGParameterHelper oegParameterHelper;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    private final OrderEntryGroup orderEntryGroup = new OrderEntryGroup();

    @Before
    public void setUp() {

        checker.setGlobalStoreFulfilmentDenialStrategies(
                Collections.singletonList(globalStoreFulfilmentDenialStrategy));
        checker.setGlobalDispatchFulfilmentDenialStrategies(
                Collections.singletonList(globalDispatchFulfilmentDenialStrategy));
        checker.setLegacyGlobalStoreFulfilmentDenialStrategies(
                Collections.singletonList(globalStoreFulfilmentDenialStrategy));

        Mockito.doReturn(ORDER_CODE).when(oegParameterHelper).getOrderCode(orderEntryGroup);
    }

    @Test
    public void testAreAllGlobalRulesPassedFalse() {

        when(globalStoreFulfilmentDenialStrategy.isDenied(orderEntryGroup))
                .thenReturn(DenialResponse.createDenied(null));

        Assert.assertFalse(checker.areAllGlobalRulesPassed(orderEntryGroup));
    }

    @Test
    public void testAreAllGlobalRulesPassedTrue() {
        when(globalStoreFulfilmentDenialStrategy.isDenied(orderEntryGroup))
                .thenReturn(DenialResponse.createNotDenied());

        Assert.assertTrue(checker.areAllGlobalRulesPassed(orderEntryGroup));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAreAllGlobalRulesPassedNull() {

        checker.areAllGlobalRulesPassed(null);
    }

    @Test
    public void testAreAllDispatchRulesPassedFalse() {
        when(globalDispatchFulfilmentDenialStrategy.isDenied(orderEntryGroup))
                .thenReturn(DenialResponse.createDenied(null));

        Assert.assertFalse(checker.areAllDispatchRulesPassed(orderEntryGroup));
    }

    @Test
    public void testAreAllDispatchRulesPassedTrue() {
        when(globalDispatchFulfilmentDenialStrategy.isDenied(orderEntryGroup))
                .thenReturn(DenialResponse.createNotDenied());

        Assert.assertTrue(checker.areAllDispatchRulesPassed(orderEntryGroup));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAreAllDispatchRulesPassedNull() {

        checker.areAllDispatchRulesPassed(null);
    }

}

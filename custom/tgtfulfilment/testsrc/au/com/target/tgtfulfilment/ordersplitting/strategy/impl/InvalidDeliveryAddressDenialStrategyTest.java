/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtauspost.model.AusPostChargeZoneModel;
import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.postcode.service.impl.TargetPostCodeServiceImpl;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class InvalidDeliveryAddressDenialStrategyTest {

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    //CHECKSTYLE:ON

    @Mock
    private TargetPostCodeServiceImpl targetPostCodeService;

    @InjectMocks
    private final InvalidDeliveryAddressDenialStrategy invalidDeliveryAddressDenialStrategy = new InvalidDeliveryAddressDenialStrategy();

    @Mock
    private TargetAddressModel targetAddressModel;

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private OEGParameterHelper oegParameterHelper;

    @Mock
    private PostCodeModel postcodeModel;

    @Mock
    private AusPostChargeZoneModel ausPostChargeZoneModel;

    @Before
    public void setup() {
        // when(oeg.getCode()).thenReturn("1234");
    }


    @Test
    public void testNullOrder() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("OrderEntryGroup  cannot be null");
        invalidDeliveryAddressDenialStrategy.isDenied(null);
    }

    @Test
    public void testWithNullAddressModel() {
        when(oegParameterHelper.getDeliveryAddress(oeg)).thenReturn(null);
        assertThat(invalidDeliveryAddressDenialStrategy.isDenied(oeg).isDenied()).isTrue();
    }

    @Test
    public void testWithTargetAddressModelPostcodeNull() {
        when(oegParameterHelper.getDeliveryAddress(oeg)).thenReturn(targetAddressModel);
        when(targetAddressModel.getDistrict()).thenReturn("VIC");
        when(targetAddressModel.getPostalcode()).thenReturn(null);
        assertThat(invalidDeliveryAddressDenialStrategy.isDenied(oeg).isDenied()).isTrue();
    }

    @Test
    public void testWithTargetAddressModelPostcodeEmpty() {
        when(oegParameterHelper.getDeliveryAddress(oeg)).thenReturn(targetAddressModel);
        when(targetAddressModel.getDistrict()).thenReturn("VIC");
        when(targetAddressModel.getPostalcode()).thenReturn(StringUtils.EMPTY);
        assertThat(invalidDeliveryAddressDenialStrategy.isDenied(oeg).isDenied()).isTrue();
    }

    @Test
    public void testWithTargetAddressModelPostcodeServiceReturnsNull() {
        when(oegParameterHelper.getDeliveryAddress(oeg)).thenReturn(targetAddressModel);
        when(targetAddressModel.getDistrict()).thenReturn("VIC");
        when(targetAddressModel.getPostalcode()).thenReturn("3000");
        when(targetPostCodeService.getPostCode(Mockito.anyString())).thenReturn(null);
        assertThat(invalidDeliveryAddressDenialStrategy.isDenied(oeg).isDenied()).isTrue();
    }

    @Test
    public void testAllValidationsPass() {
        when(oegParameterHelper.getDeliveryAddress(oeg)).thenReturn(targetAddressModel);
        when(targetAddressModel.getDistrict()).thenReturn("VIC");
        when(targetAddressModel.getPostalcode()).thenReturn("3000");
        when(targetPostCodeService.getPostCode(Mockito.anyString())).thenReturn(postcodeModel);
        assertThat(invalidDeliveryAddressDenialStrategy.isDenied(oeg).isDenied()).isFalse();
    }
}

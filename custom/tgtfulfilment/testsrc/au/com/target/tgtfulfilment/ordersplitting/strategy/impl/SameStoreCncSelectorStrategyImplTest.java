/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.ordersplitting.strategy.SpecificStoreFulfilmentDenialStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;


/**
 * Unit tests for SameStoreCncSelectorStrategyImpl.
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SameStoreCncSelectorStrategyImplTest {

    private static final Integer STORE_NUMBER = Integer.valueOf(7001);
    private static final String ORDER_CODE = "testOrder";

    private List<SpecificStoreFulfilmentDenialStrategy> specificStoreFulfilmentDenialStrategies;

    @Spy
    @InjectMocks
    private final SameStoreCncSelectorStrategyImpl strategy = new SameStoreCncSelectorStrategyImpl();

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private SpecificStoreFulfilmentDenialStrategy denialStrategy;

    @Mock
    private TargetPointOfServiceModel tpos;

    @Mock
    private OEGParameterHelper oegParameterHelper;

    @Mock
    private TargetPointOfServiceService targetPointOfServiceService;


    @Before
    public void setup() {

        Mockito.doReturn(ORDER_CODE).when(oegParameterHelper).getOrderCode(oeg);

        // List with a single strategy
        specificStoreFulfilmentDenialStrategies = new ArrayList<>();
        specificStoreFulfilmentDenialStrategies.add(denialStrategy);
        strategy.setSpecificStoreFulfilmentDenialStrategies(specificStoreFulfilmentDenialStrategies);
    }

    @Test
    public void testIsValidWithNullCncStoreNumber() {

        assertThat(strategy.isValidForSameStoreCnc(oeg, null)).isNull();
    }

    @Test
    public void testIsValidWithStoreNotFound() {

        Mockito.doReturn(null).when(strategy).lookupTargetPointOfService(STORE_NUMBER);
        assertThat(strategy.isValidForSameStoreCnc(oeg, STORE_NUMBER)).isNull();
    }

    @Test
    public void testIsValidWithDenialStrategyReturningFalse() {

        Mockito.doReturn(tpos).when(strategy).lookupTargetPointOfService(STORE_NUMBER);
        Mockito.when(denialStrategy.isDenied(oeg, tpos)).thenReturn(DenialResponse.createDenied("Denied"));
        assertThat(strategy.isValidForSameStoreCnc(oeg, STORE_NUMBER)).isNull();
    }

    @Test
    public void testIsValidWithDenialStrategyReturningTrue() {

        Mockito.doReturn(tpos).when(strategy).lookupTargetPointOfService(STORE_NUMBER);
        Mockito.when(denialStrategy.isDenied(oeg, tpos)).thenReturn(DenialResponse.createNotDenied());
        assertThat(strategy.isValidForSameStoreCnc(oeg, STORE_NUMBER)).isSameAs(tpos);
    }

    @Test
    public void testIsValidWithNoDenialStrategys() {

        Mockito.doReturn(tpos).when(strategy).lookupTargetPointOfService(STORE_NUMBER);
        strategy.setSpecificStoreFulfilmentDenialStrategies(null);
        assertThat(strategy.isValidForSameStoreCnc(oeg, STORE_NUMBER)).isSameAs(tpos);
    }

    @Test
    public void testLookupTargetPointOfServiceNull() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        when(targetPointOfServiceService.getPOSByStoreNumber(STORE_NUMBER)).thenReturn(null);

        assertThat(strategy.lookupTargetPointOfService(STORE_NUMBER)).isNull();
    }

    @Test
    public void testLookupTargetPointOfServiceException() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        when(targetPointOfServiceService.getPOSByStoreNumber(STORE_NUMBER)).thenThrow(
                new TargetUnknownIdentifierException("Hi!"));

        assertThat(strategy.lookupTargetPointOfService(STORE_NUMBER)).isNull();
    }

    @Test
    public void testLookupTargetPointOfService()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        when(targetPointOfServiceService.getPOSByStoreNumber(STORE_NUMBER)).thenReturn(tpos);

        assertThat(strategy.lookupTargetPointOfService(STORE_NUMBER)).isSameAs(tpos);
    }


}

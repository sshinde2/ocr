/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.model.ProductExclusionsModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.service.FilterExclusionListService;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;


/**
 * @author ayushman
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StoreProductExclusionDenialStrategyTest {


    @Mock
    private TargetStoreFulfilmentCapabilitiesService storeFulfilmentCapabilitiesService;

    @Mock
    private StoreFulfilmentCapabilitiesModel capabilitiesModel;

    @Mock
    private FilterExclusionListService filterExclusionListService;

    @Mock
    private Set<ProductExclusionsModel> productExclusionList;

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private OEGParameterHelper oegHelper;

    @Mock
    private TargetPointOfServiceModel targetPointOfService;


    @InjectMocks
    private final StoreProductExclusionDenialStrategy storeProductExclusionDenialStrategy = new StoreProductExclusionDenialStrategy();



    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        given(targetPointOfService
                .getFulfilmentCapability()).willReturn(
                        capabilitiesModel);
        given(filterExclusionListService
                .filterActiveProductExclusions(capabilitiesModel.getProductExclusions())).willReturn(
                        productExclusionList);
        storeProductExclusionDenialStrategy.setStoreFulfilmentCapabilitiesService(storeFulfilmentCapabilitiesService);
        given(oegHelper.getOrderCode(oeg)).willReturn("O1");
    }

    @Test
    public void testDeniedForProductInExcluded() {
        BDDMockito
                .given(Boolean.valueOf(
                        storeFulfilmentCapabilitiesService.isProductInExclusionList(oeg, productExclusionList)))
                .willReturn(Boolean.TRUE);
        final DenialResponse denialResponse = storeProductExclusionDenialStrategy.isDenied(oeg, targetPointOfService);
        assertThat(denialResponse.isDenied()).isTrue();
    }

    @Test
    public void testDeniedForProductNotExcluded() {
        given(Boolean.valueOf(
                storeFulfilmentCapabilitiesService.isProductInExclusionList(oeg, productExclusionList)))
                        .willReturn(Boolean.FALSE);
        final DenialResponse denialResponse = storeProductExclusionDenialStrategy.isDenied(oeg, targetPointOfService);
        assertThat(denialResponse.isDenied()).isFalse();
    }


}

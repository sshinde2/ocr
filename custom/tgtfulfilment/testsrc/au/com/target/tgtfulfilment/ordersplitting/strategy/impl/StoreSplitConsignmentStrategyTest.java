/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetStoreStockCheckException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.constants.TgtfulfilmentConstants;
import au.com.target.tgtfulfilment.exceptions.TargetSplitOrderException;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.ordersplitting.strategy.InstoreFulfilmentGlobalRulesChecker;
import au.com.target.tgtfulfilment.ordersplitting.strategy.NearestStoreSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.SpecificStoreFulfilmentDenialStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.DenialResponse;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.SplitOegResult;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;
import au.com.target.tgtfulfilment.service.TargetStoreLocatorService;
import au.com.target.tgtsale.stock.service.TargetStoreStockService;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StoreSplitConsignmentStrategyTest {

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private TargetPointOfServiceService targetPointOfServiceService;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private TargetStoreStockService targetStoreStockService;

    @Mock
    private InstoreFulfilmentGlobalRulesChecker instoreFulfilmentGlobalRulesChecker;

    @Mock
    private TargetStoreLocatorService targetStoreLocatorService;

    @Spy
    private final NearestStoreSelectorStrategy nearestStoreSelectorStrategy = new NearestStoreSelectorStrategyImpl();

    @Mock
    private TargetSharedConfigService targetSharedConfigService;

    @InjectMocks
    private final StoreSplitConsignmentStrategy strategy = new StoreSplitConsignmentStrategy();

    @Mock
    private TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService;

    @Mock
    private TargetConsignmentService targetConsignmentService;

    @Mock
    private OrderModel orderModel;

    @Mock
    private TargetZoneDeliveryModeModel deliveryMode;

    @Mock
    private AddressModel deliveryAddress;

    private final String district = "VIC";

    private final double distanceLimit = 1000d;

    private final String orderCode = "test code";

    private final String pcode1 = "P1";
    private final String pcode2 = "P2";
    private final String pcode3 = "P3";

    private final List<String> productListFirstRound = Arrays.asList(pcode1, pcode2, pcode3);
    private final List<String> productListSecondRound = Arrays.asList(pcode1, pcode2);

    @Mock
    private WarehouseModel warehouse1;
    @Mock
    private WarehouseModel warehouse2;
    @Mock
    private WarehouseModel warehouse3;
    @Mock
    private WarehouseModel warehouse4;
    @Mock
    private WarehouseModel warehouse5;


    private final ProductModel prod1 = mock(ProductModel.class);
    private final ProductModel prod2 = mock(ProductModel.class);
    private final ProductModel prod3 = mock(ProductModel.class);
    private final List<ProductModel> productModelListFirstRound = Arrays.asList(prod1, prod2, prod3);

    private final TargetPointOfServiceModel tpos1 = mock(TargetPointOfServiceModel.class);
    private final TargetPointOfServiceModel tpos2 = mock(TargetPointOfServiceModel.class);
    private final TargetPointOfServiceModel tpos3 = mock(TargetPointOfServiceModel.class);
    private final TargetPointOfServiceModel tpos4 = mock(TargetPointOfServiceModel.class);
    private final TargetPointOfServiceModel tpos5 = mock(TargetPointOfServiceModel.class);
    private final List<TargetPointOfServiceModel> tposList = Arrays.asList(tpos1, tpos2, tpos3, tpos4, tpos5);
    private final List<TargetPointOfServiceModel> tposList1 = Arrays.asList(tpos1, tpos2, tpos4, tpos5);
    private final List<TargetPointOfServiceModel> tposList2 = Arrays.asList(tpos3);

    @Before
    public void before() {
        final OEGParameterHelper helper = new OEGParameterHelper();
        helper.setTargetWarehouseService(targetWarehouseService);
        strategy.setOegParameterHelper(helper);
        setStrategies(strategy);
        given(orderModel.getDeliveryMode()).willReturn(deliveryMode);
        given(
                Double.valueOf(targetSharedConfigService.getDouble(TgtfulfilmentConstants.Config.STORE_DISTANCE_LIMIT,
                        10000d))).willReturn(Double.valueOf(distanceLimit));
        given(orderModel.getCode()).willReturn(orderCode);
        given(orderModel.getDeliveryAddress()).willReturn(deliveryAddress);
        given(deliveryAddress.getDistrict()).willReturn(district);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService).isFeatureEnabled(
                TgtCoreConstants.FeatureSwitch.SELECT_STORE_WITH_DISTANCE_RESTRICTION);
        given(tpos1.getStoreNumber()).willReturn(Integer.valueOf(1));
        given(tpos2.getStoreNumber()).willReturn(Integer.valueOf(2));
        given(tpos3.getStoreNumber()).willReturn(Integer.valueOf(3));
        given(tpos4.getStoreNumber()).willReturn(Integer.valueOf(4));
    }

    @Test
    public void testSplit() throws TargetSplitOrderException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final long p1rqty = 8;
        final long p2rqty = 7;
        final long p3rqty = 10;
        final Integer cncStoreNumber = Integer.valueOf(1234);
        final Map<String, Long> requiredQty = new HashMap<>();
        requiredQty.put(pcode1, Long.valueOf(p1rqty));
        requiredQty.put(pcode2, Long.valueOf(p2rqty));
        requiredQty.put(pcode3, Long.valueOf(p3rqty));

        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p1 = mock(TargetProductModel.class);
        given(p1.getCode()).willReturn(pcode1);
        given(entry1.getProduct()).willReturn(p1);
        given(entry1.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p2 = mock(TargetProductModel.class);
        given(p2.getCode()).willReturn(pcode2);
        given(entry2.getProduct()).willReturn(p2);
        given(entry2.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p3 = mock(TargetProductModel.class);
        given(p3.getCode()).willReturn(pcode3);
        given(entry3.getProduct()).willReturn(p3);
        given(entry3.getOrder()).willReturn(orderModel);
        final OrderEntryGroup oeg = new OrderEntryGroup();
        oeg.setParameter("CNC_STORE_NUMBER", cncStoreNumber);
        oeg.setParameter("REQUIRED_QTY", requiredQty);
        oeg.setParameter("DELIVERY_ADDRESS", deliveryAddress);
        oeg.add(entry1);
        oeg.add(entry2);
        oeg.add(entry3);

        mockStoreWarehouseAndStock(oeg);

        final SplitOegResult result = strategy.split(oeg);

        // assert the assigned Oeg
        final OrderEntryGroup assigned1 = result.getAssignedOEGsToWarehouse().get(0);
        final Map<String, Long> assignedQtyMap1 = (Map<String, Long>)assigned1.getParameter("ASSIGNED_QTY");
        final WarehouseModel assignedWarehouse1 = (WarehouseModel)assigned1.getParameter("FULFILMENT_WAREHOUSE");
        final Map<String, Long> expectedAssignedQtyMap1 = new HashMap<>();
        expectedAssignedQtyMap1.put(pcode1, Long.valueOf(4));
        expectedAssignedQtyMap1.put(pcode2, Long.valueOf(4));
        expectedAssignedQtyMap1.put(pcode3, Long.valueOf(7));

        final List<AbstractOrderEntryModel> assignedEntries = assigned1;
        assertThat(assignedEntries).hasSize(3);
        assertThat(assignedEntries).containsExactly(entry1, entry2, entry3);
        assertThat(assignedQtyMap1).hasSize(3);
        assertThat(assignedQtyMap1).isIn(expectedAssignedQtyMap1);
        assertThat(assignedWarehouse1).isEqualTo(warehouse1);

        final OrderEntryGroup assigned2 = result.getAssignedOEGsToWarehouse().get(1);
        final Map<String, Long> assignedQtyMap2 = (Map<String, Long>)assigned2.getParameter("ASSIGNED_QTY");
        final WarehouseModel assignedWarehouse2 = (WarehouseModel)assigned2.getParameter("FULFILMENT_WAREHOUSE");
        final Map<String, Long> expectedAssignedQtyMap2 = new HashMap<>();
        expectedAssignedQtyMap2.put(pcode1, Long.valueOf(4));
        expectedAssignedQtyMap2.put(pcode2, Long.valueOf(3));
        expectedAssignedQtyMap2.put(pcode3, Long.valueOf(2));

        final List<AbstractOrderEntryModel> assignedEntries2 = assigned2;
        assertThat(assignedEntries2).hasSize(3);
        assertThat(assignedEntries2).containsExactly(entry1, entry2, entry3);
        assertThat(assignedQtyMap2).hasSize(3);
        assertThat(assignedQtyMap2).isIn(expectedAssignedQtyMap2);
        assertThat(assignedWarehouse2).isEqualTo(warehouse2);

        // assert the notAssigned oeg
        final OrderEntryGroup notAssigned = result.getNotAssignedToWarehouse();
        final List<AbstractOrderEntryModel> notAssignedEntries = notAssigned;
        final Map<String, Long> notAssignedQtyMap = (Map<String, Long>)notAssigned.getParameter("REQUIRED_QTY");
        final Map<String, Long> expectednotAssignedQtyMap = new HashMap<>();
        expectednotAssignedQtyMap.put(pcode3, Long.valueOf(1));
        assertThat(notAssignedEntries).hasSize(1);
        assertThat(notAssignedEntries).containsExactly(entry3);
        assertThat(notAssignedQtyMap).hasSize(1);
        assertThat(notAssignedQtyMap).isIn(expectednotAssignedQtyMap);

    }

    @Test
    public void testSplitWithSplitOptimisation() throws TargetSplitOrderException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final long p1rqty = 8;
        final long p2rqty = 7;
        final long p3rqty = 10;
        final Integer cncStoreNumber = Integer.valueOf(1234);
        final Map<String, Long> requiredQty = new HashMap<>();
        requiredQty.put(pcode1, Long.valueOf(p1rqty));
        requiredQty.put(pcode2, Long.valueOf(p2rqty));
        requiredQty.put(pcode3, Long.valueOf(p3rqty));

        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p1 = mock(TargetProductModel.class);
        given(p1.getCode()).willReturn(pcode1);
        given(entry1.getProduct()).willReturn(p1);
        given(entry1.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p2 = mock(TargetProductModel.class);
        given(p2.getCode()).willReturn(pcode2);
        given(entry2.getProduct()).willReturn(p2);
        given(entry2.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p3 = mock(TargetProductModel.class);
        given(p3.getCode()).willReturn(pcode3);
        given(entry3.getProduct()).willReturn(p3);
        given(entry3.getOrder()).willReturn(orderModel);
        final OrderEntryGroup oeg = new OrderEntryGroup();
        oeg.setParameter("CNC_STORE_NUMBER", cncStoreNumber);
        oeg.setParameter("REQUIRED_QTY", requiredQty);
        oeg.setParameter("DELIVERY_ADDRESS", deliveryAddress);
        oeg.add(entry1);
        oeg.add(entry2);
        oeg.add(entry3);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPLIT_ORDER_OPTIMISATION);

        mockStoreWarehouseAndStock(oeg);
        mockFetchProductsSohInstore();

        final SplitOegResult result = strategy.split(oeg);

        // assert the assigned Oeg
        final OrderEntryGroup assigned1 = result.getAssignedOEGsToWarehouse().get(0);
        final Map<String, Long> assignedQtyMap1 = (Map<String, Long>)assigned1.getParameter("ASSIGNED_QTY");
        final WarehouseModel assignedWarehouse1 = (WarehouseModel)assigned1.getParameter("FULFILMENT_WAREHOUSE");
        final Map<String, Long> expectedAssignedQtyMap1 = new HashMap<>();
        expectedAssignedQtyMap1.put(pcode1, Long.valueOf(4));
        expectedAssignedQtyMap1.put(pcode2, Long.valueOf(4));
        expectedAssignedQtyMap1.put(pcode3, Long.valueOf(7));

        final List<AbstractOrderEntryModel> assignedEntries = assigned1;
        assertThat(assignedEntries).hasSize(3);
        assertThat(assignedEntries).containsExactly(entry1, entry2, entry3);
        assertThat(assignedQtyMap1).hasSize(3);
        assertThat(assignedQtyMap1).isIn(expectedAssignedQtyMap1);
        assertThat(assignedWarehouse1).isEqualTo(warehouse1);

        final OrderEntryGroup assigned2 = result.getAssignedOEGsToWarehouse().get(1);
        final Map<String, Long> assignedQtyMap2 = (Map<String, Long>)assigned2.getParameter("ASSIGNED_QTY");
        final WarehouseModel assignedWarehouse2 = (WarehouseModel)assigned2.getParameter("FULFILMENT_WAREHOUSE");
        final Map<String, Long> expectedAssignedQtyMap2 = new HashMap<>();
        expectedAssignedQtyMap2.put(pcode1, Long.valueOf(4));
        expectedAssignedQtyMap2.put(pcode2, Long.valueOf(3));
        expectedAssignedQtyMap2.put(pcode3, Long.valueOf(2));

        final List<AbstractOrderEntryModel> assignedEntries2 = assigned2;
        assertThat(assignedEntries2).hasSize(3);
        assertThat(assignedEntries2).containsExactly(entry1, entry2, entry3);
        assertThat(assignedQtyMap2).hasSize(3);
        assertThat(assignedQtyMap2).isIn(expectedAssignedQtyMap2);
        assertThat(assignedWarehouse2).isEqualTo(warehouse2);

        // assert the notAssigned oeg
        final OrderEntryGroup notAssigned = result.getNotAssignedToWarehouse();
        final List<AbstractOrderEntryModel> notAssignedEntries = notAssigned;
        final Map<String, Long> notAssignedQtyMap = (Map<String, Long>)notAssigned.getParameter("REQUIRED_QTY");
        final Map<String, Long> expectednotAssignedQtyMap = new HashMap<>();
        expectednotAssignedQtyMap.put(pcode3, Long.valueOf(1));
        assertThat(notAssignedEntries).hasSize(1);
        assertThat(notAssignedEntries).containsExactly(entry3);
        assertThat(notAssignedQtyMap).hasSize(1);
        assertThat(notAssignedQtyMap).isIn(expectednotAssignedQtyMap);

    }



    @Test(expected = TargetStoreStockCheckException.class)
    public void testSplitWithSplitOptimisationWhenNoStockReturnedWithFeatureSwitchOn()
            throws TargetSplitOrderException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        willReturn(Boolean.TRUE).given(targetFeatureSwitchService).isFeatureEnabled(
                TgtCoreConstants.FeatureSwitch.ERROR_ON_STOCKCHECK_FAILURE);
        final long p1rqty = 8;
        final long p2rqty = 7;
        final long p3rqty = 10;
        final Integer cncStoreNumber = Integer.valueOf(1234);
        final Map<String, Long> requiredQty = new HashMap<>();
        requiredQty.put(pcode1, Long.valueOf(p1rqty));
        requiredQty.put(pcode2, Long.valueOf(p2rqty));
        requiredQty.put(pcode3, Long.valueOf(p3rqty));

        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p1 = mock(TargetProductModel.class);
        given(p1.getCode()).willReturn(pcode1);
        given(entry1.getProduct()).willReturn(p1);
        given(entry1.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p2 = mock(TargetProductModel.class);
        given(p2.getCode()).willReturn(pcode2);
        given(entry2.getProduct()).willReturn(p2);
        given(entry2.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p3 = mock(TargetProductModel.class);
        given(p3.getCode()).willReturn(pcode3);
        given(entry3.getProduct()).willReturn(p3);
        given(entry3.getOrder()).willReturn(orderModel);
        final OrderEntryGroup oeg = new OrderEntryGroup();
        oeg.setParameter("CNC_STORE_NUMBER", cncStoreNumber);
        oeg.setParameter("REQUIRED_QTY", requiredQty);
        oeg.setParameter("DELIVERY_ADDRESS", deliveryAddress);
        oeg.add(entry1);
        oeg.add(entry2);
        oeg.add(entry3);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPLIT_ORDER_OPTIMISATION);

        mockStoreWarehouseAndStock(oeg);

        final Table<Integer, String, Long> emptyStock = HashBasedTable.create();
        given(targetStoreStockService.fetchProductsSohInStore(Arrays.asList(tpos1, tpos2, tpos3),
                productModelListFirstRound))
                        .willReturn(emptyStock);

        strategy.split(oeg);

    }

    @Test(expected = TargetStoreStockCheckException.class)
    public void testSplitWithSplitOptimisationWhenErrorsWithFeatureSwitchOn()
            throws TargetSplitOrderException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        willReturn(Boolean.TRUE).given(targetFeatureSwitchService).isFeatureEnabled(
                TgtCoreConstants.FeatureSwitch.ERROR_ON_STOCKCHECK_FAILURE);
        final long p1rqty = 8;
        final long p2rqty = 7;
        final long p3rqty = 10;
        final Integer cncStoreNumber = Integer.valueOf(1234);
        final Map<String, Long> requiredQty = new HashMap<>();
        requiredQty.put(pcode1, Long.valueOf(p1rqty));
        requiredQty.put(pcode2, Long.valueOf(p2rqty));
        requiredQty.put(pcode3, Long.valueOf(p3rqty));

        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p1 = mock(TargetProductModel.class);
        given(p1.getCode()).willReturn(pcode1);
        given(entry1.getProduct()).willReturn(p1);
        given(entry1.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p2 = mock(TargetProductModel.class);
        given(p2.getCode()).willReturn(pcode2);
        given(entry2.getProduct()).willReturn(p2);
        given(entry2.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p3 = mock(TargetProductModel.class);
        given(p3.getCode()).willReturn(pcode3);
        given(entry3.getProduct()).willReturn(p3);
        given(entry3.getOrder()).willReturn(orderModel);
        final OrderEntryGroup oeg = new OrderEntryGroup();
        oeg.setParameter("CNC_STORE_NUMBER", cncStoreNumber);
        oeg.setParameter("REQUIRED_QTY", requiredQty);
        oeg.setParameter("DELIVERY_ADDRESS", deliveryAddress);
        oeg.add(entry1);
        oeg.add(entry2);
        oeg.add(entry3);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPLIT_ORDER_OPTIMISATION);

        mockStoreWarehouseAndStock(oeg);

        given(targetStoreStockService.fetchProductsSohInStore(Arrays.asList(tpos1, tpos2, tpos3),
                productModelListFirstRound))
                        .willThrow(new RuntimeException());

        strategy.split(oeg);
    }




    @Test
    public void testSplitWithFastlineInSplittingOn() throws TargetSplitOrderException,
            TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService).isFeatureEnabled(
                TgtCoreConstants.FeatureSwitch.FASTLINE_IN_STORE_SPLITTING);
        final long p1rqty = 8;
        final long p2rqty = 7;
        final long p3rqty = 10;
        final Integer cncStoreNumber = Integer.valueOf(1234);
        final Map<String, Long> requiredQty = new HashMap<>();
        requiredQty.put(pcode1, Long.valueOf(p1rqty));
        requiredQty.put(pcode2, Long.valueOf(p2rqty));
        requiredQty.put(pcode3, Long.valueOf(p3rqty));

        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p1 = mock(TargetProductModel.class);
        given(p1.getCode()).willReturn(pcode1);
        given(entry1.getProduct()).willReturn(p1);
        given(entry1.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p2 = mock(TargetProductModel.class);
        given(p2.getCode()).willReturn(pcode2);
        given(entry2.getProduct()).willReturn(p2);
        given(entry2.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p3 = mock(TargetProductModel.class);
        given(p3.getCode()).willReturn(pcode3);
        given(entry3.getProduct()).willReturn(p3);
        given(entry3.getOrder()).willReturn(orderModel);
        final OrderEntryGroup oeg = new OrderEntryGroup();
        oeg.setParameter("CNC_STORE_NUMBER", cncStoreNumber);
        oeg.setParameter("REQUIRED_QTY", requiredQty);
        oeg.setParameter("DELIVERY_ADDRESS", deliveryAddress);
        oeg.add(entry1);
        oeg.add(entry2);
        oeg.add(entry3);

        mockStoreWarehouseAndStock(oeg);

        final SplitOegResult result = strategy.split(oeg);

        // assert the assigned Oeg
        final OrderEntryGroup assigned1 = result.getAssignedOEGsToWarehouse().get(0);
        final Map<String, Long> assignedQtyMap1 = (Map<String, Long>)assigned1.getParameter("ASSIGNED_QTY");
        final WarehouseModel assignedWarehouse1 = (WarehouseModel)assigned1.getParameter("FULFILMENT_WAREHOUSE");
        final Map<String, Long> expectedAssignedQtyMap1 = new HashMap<>();
        expectedAssignedQtyMap1.put(pcode1, Long.valueOf(4));
        expectedAssignedQtyMap1.put(pcode2, Long.valueOf(4));
        expectedAssignedQtyMap1.put(pcode3, Long.valueOf(7));

        final List<AbstractOrderEntryModel> assignedEntries = assigned1;
        assertThat(assignedEntries).hasSize(3);
        assertThat(assignedEntries).containsExactly(entry1, entry2, entry3);
        assertThat(assignedQtyMap1).hasSize(3);
        assertThat(assignedQtyMap1).isIn(expectedAssignedQtyMap1);
        assertThat(assignedWarehouse1).isEqualTo(warehouse1);

        final OrderEntryGroup assigned2 = result.getAssignedOEGsToWarehouse().get(1);
        final Map<String, Long> assignedQtyMap2 = (Map<String, Long>)assigned2.getParameter("ASSIGNED_QTY");
        final WarehouseModel assignedWarehouse2 = (WarehouseModel)assigned2.getParameter("FULFILMENT_WAREHOUSE");
        final Map<String, Long> expectedAssignedQtyMap2 = new HashMap<>();
        expectedAssignedQtyMap2.put(pcode1, Long.valueOf(4));
        expectedAssignedQtyMap2.put(pcode2, Long.valueOf(3));
        expectedAssignedQtyMap2.put(pcode3, Long.valueOf(2));

        final List<AbstractOrderEntryModel> assignedEntries2 = assigned2;
        assertThat(assignedEntries2).hasSize(3);
        assertThat(assignedEntries2).containsExactly(entry1, entry2, entry3);
        assertThat(assignedQtyMap2).hasSize(3);
        assertThat(assignedQtyMap2).isIn(expectedAssignedQtyMap2);
        assertThat(assignedWarehouse2).isEqualTo(warehouse2);

        // assert the notAssigned oeg
        final OrderEntryGroup notAssigned = result.getNotAssignedToWarehouse();
        final List<AbstractOrderEntryModel> notAssignedEntries = notAssigned;
        final Map<String, Long> notAssignedQtyMap = (Map<String, Long>)notAssigned.getParameter("REQUIRED_QTY");
        final Map<String, Long> expectednotAssignedQtyMap = new HashMap<>();
        expectednotAssignedQtyMap.put(pcode3, Long.valueOf(1));
        assertThat(notAssignedEntries).hasSize(1);
        assertThat(notAssignedEntries).containsExactly(entry3);
        assertThat(notAssignedQtyMap).hasSize(1);
        assertThat(notAssignedQtyMap).isIn(expectednotAssignedQtyMap);

    }

    @Test
    public void testSplitWithFeatureDistanceRestrictionOff() throws TargetSplitOrderException,
            TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        willReturn(Boolean.FALSE).given(targetFeatureSwitchService).isFeatureEnabled(
                TgtCoreConstants.FeatureSwitch.SELECT_STORE_WITH_DISTANCE_RESTRICTION);
        final long p1rqty = 8;
        final long p2rqty = 7;
        final long p3rqty = 10;
        final Integer cncStoreNumber = Integer.valueOf(1234);
        final Map<String, Long> requiredQty = new HashMap<>();
        requiredQty.put(pcode1, Long.valueOf(p1rqty));
        requiredQty.put(pcode2, Long.valueOf(p2rqty));
        requiredQty.put(pcode3, Long.valueOf(p3rqty));

        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p1 = mock(TargetProductModel.class);
        given(p1.getCode()).willReturn(pcode1);
        given(entry1.getProduct()).willReturn(p1);
        given(entry1.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p2 = mock(TargetProductModel.class);
        given(p2.getCode()).willReturn(pcode2);
        given(entry2.getProduct()).willReturn(p2);
        given(entry2.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p3 = mock(TargetProductModel.class);
        given(p3.getCode()).willReturn(pcode3);
        given(entry3.getProduct()).willReturn(p3);
        given(entry3.getOrder()).willReturn(orderModel);
        final OrderEntryGroup oeg = new OrderEntryGroup();
        oeg.setParameter("CNC_STORE_NUMBER", cncStoreNumber);
        oeg.setParameter("REQUIRED_QTY", requiredQty);
        oeg.setParameter("DELIVERY_ADDRESS", deliveryAddress);
        oeg.add(entry1);
        oeg.add(entry2);
        oeg.add(entry3);

        mockStoreWarehouseAndStock(oeg);

        final SplitOegResult result = strategy.split(oeg);

        // assert the assigned Oeg
        final OrderEntryGroup assigned1 = result.getAssignedOEGsToWarehouse().get(0);
        final Map<String, Long> assignedQtyMap1 = (Map<String, Long>)assigned1.getParameter("ASSIGNED_QTY");
        final WarehouseModel assignedWarehouse1 = (WarehouseModel)assigned1.getParameter("FULFILMENT_WAREHOUSE");
        final Map<String, Long> expectedAssignedQtyMap1 = new HashMap<>();
        expectedAssignedQtyMap1.put(pcode1, Long.valueOf(4));
        expectedAssignedQtyMap1.put(pcode2, Long.valueOf(4));
        expectedAssignedQtyMap1.put(pcode3, Long.valueOf(7));

        final List<AbstractOrderEntryModel> assignedEntries = assigned1;
        assertThat(assignedEntries).hasSize(3);
        assertThat(assignedEntries).containsExactly(entry1, entry2, entry3);
        assertThat(assignedQtyMap1).hasSize(3);
        assertThat(assignedQtyMap1).isIn(expectedAssignedQtyMap1);
        assertThat(assignedWarehouse1).isEqualTo(warehouse1);

        final OrderEntryGroup assigned2 = result.getAssignedOEGsToWarehouse().get(1);
        final Map<String, Long> assignedQtyMap2 = (Map<String, Long>)assigned2.getParameter("ASSIGNED_QTY");
        final WarehouseModel assignedWarehouse2 = (WarehouseModel)assigned2.getParameter("FULFILMENT_WAREHOUSE");
        final Map<String, Long> expectedAssignedQtyMap2 = new HashMap<>();
        expectedAssignedQtyMap2.put(pcode1, Long.valueOf(4));
        expectedAssignedQtyMap2.put(pcode2, Long.valueOf(3));
        expectedAssignedQtyMap2.put(pcode3, Long.valueOf(2));

        final List<AbstractOrderEntryModel> assignedEntries2 = assigned2;
        assertThat(assignedEntries2).hasSize(3);
        assertThat(assignedEntries2).containsExactly(entry1, entry2, entry3);
        assertThat(assignedQtyMap2).hasSize(3);
        assertThat(assignedQtyMap2).isIn(expectedAssignedQtyMap2);
        assertThat(assignedWarehouse2).isEqualTo(warehouse2);

        // assert the notAssigned oeg
        final OrderEntryGroup notAssigned = result.getNotAssignedToWarehouse();
        final List<AbstractOrderEntryModel> notAssignedEntries = notAssigned;
        final Map<String, Long> notAssignedQtyMap = (Map<String, Long>)notAssigned.getParameter("REQUIRED_QTY");
        final Map<String, Long> expectednotAssignedQtyMap = new HashMap<>();
        expectednotAssignedQtyMap.put(pcode3, Long.valueOf(1));
        assertThat(notAssignedEntries).hasSize(1);
        assertThat(notAssignedEntries).containsExactly(entry3);
        assertThat(notAssignedQtyMap).hasSize(1);
        assertThat(notAssignedQtyMap).isIn(expectednotAssignedQtyMap);

    }

    @Test
    public void testSplitWithDispatchRulesFailed() throws TargetSplitOrderException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final long p1rqty = 8;
        final long p2rqty = 7;
        final long p3rqty = 10;
        final Integer cncStoreNumber = Integer.valueOf(1234);
        final Map<String, Long> requiredQty = new HashMap<>();
        requiredQty.put(pcode1, Long.valueOf(p1rqty));
        requiredQty.put(pcode2, Long.valueOf(p2rqty));
        requiredQty.put(pcode3, Long.valueOf(p3rqty));

        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p1 = mock(TargetProductModel.class);
        given(p1.getCode()).willReturn(pcode1);
        given(entry1.getProduct()).willReturn(p1);
        given(entry1.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p2 = mock(TargetProductModel.class);
        given(p2.getCode()).willReturn(pcode2);
        given(entry2.getProduct()).willReturn(p2);
        given(entry2.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p3 = mock(TargetProductModel.class);
        given(p3.getCode()).willReturn(pcode3);
        given(entry3.getProduct()).willReturn(p3);
        given(entry3.getOrder()).willReturn(orderModel);
        final OrderEntryGroup oeg = new OrderEntryGroup();
        oeg.setParameter("CNC_STORE_NUMBER", cncStoreNumber);
        oeg.setParameter("REQUIRED_QTY", requiredQty);
        oeg.setParameter("DELIVERY_ADDRESS", deliveryAddress);
        oeg.add(entry1);
        oeg.add(entry2);
        oeg.add(entry3);

        mockStoreWarehouseAndStock(oeg);

        given(Boolean.valueOf(instoreFulfilmentGlobalRulesChecker.areAllDispatchRulesPassed(oeg))).willReturn(
                Boolean.FALSE);

        final SplitOegResult result = strategy.split(oeg);

        // assert the assigned Oeg
        assertThat(result.getAssignedOEGsToWarehouse()).isEmpty();

        // assert the notAssigned oeg
        final OrderEntryGroup notAssigned = result.getNotAssignedToWarehouse();
        final List<AbstractOrderEntryModel> notAssignedEntries = notAssigned;
        final Map<String, Long> notAssignedQtyMap = (Map<String, Long>)notAssigned.getParameter("REQUIRED_QTY");
        final Map<String, Long> expectednotAssignedQtyMap = new HashMap<>();
        expectednotAssignedQtyMap.put(pcode1, Long.valueOf(8));
        expectednotAssignedQtyMap.put(pcode2, Long.valueOf(7));
        expectednotAssignedQtyMap.put(pcode3, Long.valueOf(10));
        assertThat(notAssignedEntries).hasSize(3);
        assertThat(notAssignedEntries).containsExactly(entry1, entry2, entry3);
        assertThat(notAssignedQtyMap).hasSize(3);
        assertThat(notAssignedQtyMap).isIn(expectednotAssignedQtyMap);

    }

    @Test
    public void testSplitWithEmptyTpos() throws TargetSplitOrderException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final long p1rqty = 8;
        final long p2rqty = 7;
        final long p3rqty = 10;
        final Integer cncStoreNumber = Integer.valueOf(1234);
        final Map<String, Long> requiredQty = new HashMap<>();
        requiredQty.put(pcode1, Long.valueOf(p1rqty));
        requiredQty.put(pcode2, Long.valueOf(p2rqty));
        requiredQty.put(pcode3, Long.valueOf(p3rqty));

        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p1 = mock(TargetProductModel.class);
        given(p1.getCode()).willReturn(pcode1);
        given(entry1.getProduct()).willReturn(p1);
        given(entry1.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p2 = mock(TargetProductModel.class);
        given(p2.getCode()).willReturn(pcode2);
        given(entry2.getProduct()).willReturn(p2);
        given(entry2.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p3 = mock(TargetProductModel.class);
        given(p3.getCode()).willReturn(pcode3);
        given(entry3.getProduct()).willReturn(p3);
        given(entry3.getOrder()).willReturn(orderModel);
        final OrderEntryGroup oeg = new OrderEntryGroup();
        oeg.setParameter("CNC_STORE_NUMBER", cncStoreNumber);
        oeg.setParameter("REQUIRED_QTY", requiredQty);
        oeg.setParameter("DELIVERY_ADDRESS", deliveryAddress);
        oeg.add(entry1);
        oeg.add(entry2);
        oeg.add(entry3);

        mockStoreWarehouseAndStock(oeg);

        doReturn(null).when(nearestStoreSelectorStrategy)
                .getSortedStoreListByDistanceWithDistanceRestriction(tposList,
                        deliveryAddress, distanceLimit);

        final SplitOegResult result = strategy.split(oeg);

        // assert the assigned Oeg
        assertThat(result.getAssignedOEGsToWarehouse()).isEmpty();

        // assert the notAssigned oeg
        final OrderEntryGroup notAssigned = result.getNotAssignedToWarehouse();
        final List<AbstractOrderEntryModel> notAssignedEntries = notAssigned;
        final Map<String, Long> notAssignedQtyMap = (Map<String, Long>)notAssigned.getParameter("REQUIRED_QTY");
        final Map<String, Long> expectednotAssignedQtyMap = new HashMap<>();
        expectednotAssignedQtyMap.put(pcode1, Long.valueOf(8));
        expectednotAssignedQtyMap.put(pcode2, Long.valueOf(7));
        expectednotAssignedQtyMap.put(pcode3, Long.valueOf(10));
        assertThat(notAssignedEntries).hasSize(3);
        assertThat(notAssignedEntries).containsExactly(entry1, entry2, entry3);
        assertThat(notAssignedQtyMap).hasSize(3);
        assertThat(notAssignedQtyMap).isIn(expectednotAssignedQtyMap);

    }

    @Test
    public void testSplitWithExceedMaxSplits() throws TargetSplitOrderException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel = mock(
                GlobalStoreFulfilmentCapabilitiesModel.class);
        given(capabilitiesModel.getMaxNumberOfStoreConsignmentSplits()).willReturn(Integer.valueOf(3));
        given(targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites()).willReturn(capabilitiesModel);

        final TargetConsignmentModel consignment1 = mock(TargetConsignmentModel.class);
        final TargetConsignmentModel consignment2 = mock(TargetConsignmentModel.class);
        final List<TargetConsignmentModel> consignmentList = Arrays.asList(consignment1, consignment2);

        given(targetConsignmentService
                .getActivePPDStoreConsignments(orderModel)).willReturn(consignmentList);

        final long p1rqty = 8;
        final long p2rqty = 7;
        final long p3rqty = 10;
        final Integer cncStoreNumber = Integer.valueOf(1234);
        final Map<String, Long> requiredQty = new HashMap<>();
        requiredQty.put(pcode1, Long.valueOf(p1rqty));
        requiredQty.put(pcode2, Long.valueOf(p2rqty));
        requiredQty.put(pcode3, Long.valueOf(p3rqty));

        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p1 = mock(TargetProductModel.class);
        given(p1.getCode()).willReturn(pcode1);
        given(entry1.getProduct()).willReturn(p1);
        given(entry1.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p2 = mock(TargetProductModel.class);
        given(p2.getCode()).willReturn(pcode2);
        given(entry2.getProduct()).willReturn(p2);
        given(entry2.getOrder()).willReturn(orderModel);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);
        final TargetProductModel p3 = mock(TargetProductModel.class);
        given(p3.getCode()).willReturn(pcode3);
        given(entry3.getProduct()).willReturn(p3);
        given(entry3.getOrder()).willReturn(orderModel);
        final OrderEntryGroup oeg = new OrderEntryGroup();
        oeg.setParameter("CNC_STORE_NUMBER", cncStoreNumber);
        oeg.setParameter("REQUIRED_QTY", requiredQty);
        oeg.setParameter("DELIVERY_ADDRESS", deliveryAddress);
        oeg.add(entry1);
        oeg.add(entry2);
        oeg.add(entry3);

        mockStoreWarehouseAndStock(oeg);

        final SplitOegResult result = strategy.split(oeg);

        assertThat(result.getAssignedOEGsToWarehouse()).hasSize(1);
        // assert the assigned Oeg
        final OrderEntryGroup assigned1 = result.getAssignedOEGsToWarehouse().get(0);
        final Map<String, Long> assignedQtyMap1 = (Map<String, Long>)assigned1.getParameter("ASSIGNED_QTY");
        final WarehouseModel assignedWarehouse1 = (WarehouseModel)assigned1.getParameter("FULFILMENT_WAREHOUSE");
        final Map<String, Long> expectedAssignedQtyMap1 = new HashMap<>();
        expectedAssignedQtyMap1.put(pcode1, Long.valueOf(4));
        expectedAssignedQtyMap1.put(pcode2, Long.valueOf(4));
        expectedAssignedQtyMap1.put(pcode3, Long.valueOf(7));

        final List<AbstractOrderEntryModel> assignedEntries = assigned1;
        assertThat(assignedEntries).hasSize(3);
        assertThat(assignedEntries).containsExactly(entry1, entry2, entry3);
        assertThat(assignedQtyMap1).hasSize(3);
        assertThat(assignedQtyMap1).isIn(expectedAssignedQtyMap1);
        assertThat(assignedWarehouse1).isEqualTo(warehouse1);


        // assert the notAssigned oeg
        final OrderEntryGroup notAssigned = result.getNotAssignedToWarehouse();
        final List<AbstractOrderEntryModel> notAssignedEntries = notAssigned;
        final Map<String, Long> notAssignedQtyMap = (Map<String, Long>)notAssigned.getParameter("REQUIRED_QTY");
        final Map<String, Long> expectednotAssignedQtyMap = new HashMap<>();
        expectednotAssignedQtyMap.put(pcode1, Long.valueOf(4));
        expectednotAssignedQtyMap.put(pcode2, Long.valueOf(3));
        expectednotAssignedQtyMap.put(pcode3, Long.valueOf(3));
        assertThat(notAssignedEntries).hasSize(3);
        assertThat(notAssignedEntries).containsExactly(entry1, entry2, entry3);
        assertThat(notAssignedQtyMap).hasSize(3);
        assertThat(notAssignedQtyMap).isIn(expectednotAssignedQtyMap);

    }

    private void mockStoreWarehouseAndStock(final OrderEntryGroup oeg) {
        if (!targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASTLINE_IN_STORE_SPLITTING)) {
            if (targetFeatureSwitchService.isFeatureEnabled(
                    TgtCoreConstants.FeatureSwitch.SELECT_STORE_WITH_DISTANCE_RESTRICTION)) {
                given(targetStoreLocatorService.getAllFulfilmentStores()).willReturn(tposList);
            }
            else {
                given(targetStoreLocatorService
                        .getAllFulfilmentStoresInState(Mockito.anyString())).willReturn(tposList);
            }
        }
        else {
            if (targetFeatureSwitchService.isFeatureEnabled(
                    TgtCoreConstants.FeatureSwitch.SELECT_STORE_WITH_DISTANCE_RESTRICTION)) {
                given(targetStoreLocatorService.getAllFulfilmentStores()).willReturn(tposList1);
            }
            else {
                given(targetStoreLocatorService
                        .getAllFulfilmentStoresInState(Mockito.anyString())).willReturn(tposList1);
            }

        }



        given(warehouse1.getCode()).willReturn("warehouse1");
        given(targetWarehouseService.getWarehouseForPointOfService(tpos1)).willReturn(warehouse1);

        given(warehouse2.getCode()).willReturn("warehouse2");
        given(targetWarehouseService.getWarehouseForPointOfService(tpos2)).willReturn(warehouse2);

        given(warehouse3.getCode()).willReturn("warehouse3");
        given(targetWarehouseService.getWarehouseForPointOfService(tpos3)).willReturn(warehouse3);

        given(warehouse4.getCode()).willReturn("warehouse4");
        given(targetWarehouseService.getWarehouseForPointOfService(tpos4)).willReturn(warehouse4);

        given(targetWarehouseService.getWarehouseForPointOfService(tpos5)).willReturn(warehouse5);

        final PointOfServiceDistanceData tposDistance1 = createNewPointOfServiceDistanceData(tpos1, 100d);
        final PointOfServiceDistanceData tposDistance2 = createNewPointOfServiceDistanceData(tpos2, 200d);
        final PointOfServiceDistanceData tposDistance3 = createNewPointOfServiceDistanceData(tpos3, 300d);
        final PointOfServiceDistanceData tposDistance4 = createNewPointOfServiceDistanceData(tpos4, 400d);
        //final PointOfServiceDistanceData tposDistance5 = createNewPointOfServiceDistanceData(tpos5, 10000d);



        //filter out the tpos5 by distance
        final List<PointOfServiceDistanceData> distanceDataList = Arrays.asList(tposDistance1, tposDistance2,
                tposDistance3, tposDistance4);

        final List<PointOfServiceDistanceData> distanceDataList1 = Arrays.asList(tposDistance1, tposDistance2,
                tposDistance4);



        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASTLINE_IN_STORE_SPLITTING)) {
            // use tpos3 as the onlineTpos
            final List<PointOfServiceDistanceData> fastlineDistanceDataList = Arrays.asList(tposDistance3);

            given(targetPointOfServiceService.getTposOfDefaultWarehouse()).willReturn(tpos3);
            doReturn(fastlineDistanceDataList).when(nearestStoreSelectorStrategy)
                    .getSortedStoreListByDistanceWithDistanceRestriction(tposList2,
                            deliveryAddress, 10000d);
            if (targetFeatureSwitchService.isFeatureEnabled(
                    TgtCoreConstants.FeatureSwitch.SELECT_STORE_WITH_DISTANCE_RESTRICTION)) {
                doReturn(distanceDataList1).when(nearestStoreSelectorStrategy)
                        .getSortedStoreListByDistanceWithDistanceRestriction(tposList1,
                                deliveryAddress, distanceLimit);
            }
            else {
                doReturn(distanceDataList1).when(nearestStoreSelectorStrategy)
                        .getSortedStoreListByDistanceWithDistanceRestriction(tposList1,
                                deliveryAddress, 10000d);
            }

        }
        else {
            if (targetFeatureSwitchService.isFeatureEnabled(
                    TgtCoreConstants.FeatureSwitch.SELECT_STORE_WITH_DISTANCE_RESTRICTION)) {
                doReturn(distanceDataList).when(nearestStoreSelectorStrategy)
                        .getSortedStoreListByDistanceWithDistanceRestriction(tposList,
                                deliveryAddress, distanceLimit);
            }
            else {
                doReturn(distanceDataList).when(nearestStoreSelectorStrategy)
                        .getSortedStoreListByDistanceWithDistanceRestriction(tposList,
                                deliveryAddress, 10000d);
            }
        }

        //filter out the tpos4 by store denial strategies
        final SpecificStoreFulfilmentDenialStrategy denialTpos4 = mock(SpecificStoreFulfilmentDenialStrategy.class);
        given(
                denialTpos4.isDenied(any(OrderEntryGroup.class),
                        Mockito.argThat(new ArgumentMatcher<TargetPointOfServiceModel>() {
                            @Override
                            public boolean matches(final Object argument) {
                                return argument != tpos4;
                            }
                        }))).willReturn(
                                DenialResponse.createNotDenied());
        given(denialTpos4.isDenied(oeg, tpos4)).willReturn(DenialResponse.createDenied("test tpos 4 denied"));
        strategy.getStoreFulfilmentDenialStrategies().add(denialTpos4);

        // set up store stock mock
        //required 8 7 10
        mockStoreStock(tpos1, productListFirstRound, 4, 4, 7); // total unit: 4+4+7=15
        mockStoreStock(tpos2, productListFirstRound, 4, 3, 2); // total unit: 4+3+2=9
        mockStoreStock(tpos3, productListFirstRound, 5, 2, 8); // total unit: 5+2+8=15
        mockStoreStock(tpos4, productListFirstRound, 7, 1, 6); // total unit: 7+1+6=14
        //tpos1 4 4 7 will be picked second round required qyt: 4 3 3
        mockStoreStock(tpos2, productListSecondRound, 4, 3, 2); // total unit: 4+3+2=9
        mockStoreStock(tpos3, productListSecondRound, 5, 2, 8); // total unit: 4+2+3=9
        mockStoreStock(tpos4, productListSecondRound, 7, 1, 6); // total unit: 4+1+3=8


        given(Boolean.valueOf(instoreFulfilmentGlobalRulesChecker.areAllDispatchRulesPassed(oeg))).willReturn(
                Boolean.TRUE);
    }

    private void setStrategies(final StoreSplitConsignmentStrategy _strategy) {
        final SpecificStoreFulfilmentDenialStrategy strategy1 = mock(SpecificStoreFulfilmentDenialStrategy.class);
        given(strategy1.isDenied(any(OrderEntryGroup.class), any(TargetPointOfServiceModel.class))).willReturn(
                DenialResponse.createNotDenied());
        final List<SpecificStoreFulfilmentDenialStrategy> storeFulfilmentDenialStrategies = new ArrayList<>();
        storeFulfilmentDenialStrategies.add(strategy1);
        _strategy.setStoreFulfilmentDenialStrategies(storeFulfilmentDenialStrategies);

        final List<SpecificStoreFulfilmentDenialStrategy> storeWithEntryLevelFulfilmentDenialStrategies = new ArrayList<>();
        storeWithEntryLevelFulfilmentDenialStrategies.add(strategy1);
        _strategy.setStoreWithEntryLevelFulfilmentDenialStrategies(storeWithEntryLevelFulfilmentDenialStrategies);
    }

    private PointOfServiceDistanceData createNewPointOfServiceDistanceData(final TargetPointOfServiceModel tpos,
            final double distance) {
        final PointOfServiceDistanceData posDistanceData = new PointOfServiceDistanceData();
        posDistanceData.setDistanceKm(distance);
        posDistanceData.setPointOfService(tpos);
        return posDistanceData;
    }



    private void mockFetchProductsSohInstore() {
        final Table<Integer, String, Long> sohTable = mockStoreStockTable(tpos1, productListFirstRound, 4, 4, 7);
        sohTable.putAll(mockStoreStockTable(tpos2, productListFirstRound, 4, 3, 2));
        sohTable.putAll(mockStoreStockTable(tpos3, productListFirstRound, 5, 2, 0));
        given(targetStoreStockService.fetchProductsSohInStore(Arrays.asList(tpos1, tpos2, tpos3),
                productModelListFirstRound))
                        .willReturn(sohTable);
    }

    private Table<Integer, String, Long> mockStoreStockTable(final TargetPointOfServiceModel tpos,
            final List<String> products, final int... qty) {
        final Table<Integer, String, Long> sohTable = HashBasedTable.create();
        for (int i = 0; i < products.size(); i++) {
            sohTable.put(tpos.getStoreNumber(), products.get(i), Long.valueOf(qty[i]));
        }
        return sohTable;
    }

    private void mockStoreStock(final TargetPointOfServiceModel tpos, final List<String> products, final int... qty) {
        final Map<String, Long> sohMap = new HashMap<>();
        for (int i = 0; i < products.size(); i++) {
            sohMap.put(products.get(i), Long.valueOf(qty[i]));
        }
        given(targetStoreStockService.fetchProductsSohInStore(tpos, products))
                .willReturn(sohMap);
    }

}

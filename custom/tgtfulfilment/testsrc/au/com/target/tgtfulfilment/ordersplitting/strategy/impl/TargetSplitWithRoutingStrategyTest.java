/**
 * 
 */
package au.com.target.tgtfulfilment.ordersplitting.strategy.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;
import de.hybris.platform.stock.exception.StockLevelNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.ListUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.exception.TargetStoreStockCheckException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.enums.OfcOrderType;
import au.com.target.tgtfulfilment.exceptions.TargetSplitOrderException;
import au.com.target.tgtfulfilment.fulfilmentservice.TicketSender;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.helper.WarehouseHelper;
import au.com.target.tgtfulfilment.ordercancel.dto.TargetOrderCancelEntryList;
import au.com.target.tgtfulfilment.ordersplitting.strategy.ConsignmentToWarehouseAssigner;
import au.com.target.tgtfulfilment.ordersplitting.strategy.InstoreFulfilmentGlobalRulesChecker;
import au.com.target.tgtfulfilment.ordersplitting.strategy.SameStoreCncSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.StoreSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.WarehouseSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.bean.SplitOegResult;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetSplitWithRoutingStrategyTest {

    private static final Integer CNC_STORE_NUMBER = Integer.valueOf(1234);

    private static final Integer FULFILMENT_STORE_NUMBER = Integer.valueOf(4321);

    private static final String ORDER_CODE = "testOrder";

    @Spy
    @InjectMocks
    private final TargetSplitWithRoutingStrategy tgtSplitWithRoutingStrategy = new TargetSplitWithRoutingStrategy();

    @Mock
    private OEGParameterHelper oegParameterHelper;

    @Mock
    private SameStoreCncSelectorStrategy sameStoreCncSelectorStrategy;

    @Mock
    private StoreSelectorStrategy storeSelectorStrategy;

    @Mock
    private WarehouseHelper consignmentReroutingHelper;

    @Mock
    private ConsignmentToWarehouseAssigner consignmentToWarehouseAssigner;

    @Mock
    private AddressModel deliveryAddress;

    @Mock
    private TargetZoneDeliveryModeModel deliveryMode;

    @Mock
    private TargetPointOfServiceModel cncStore;

    @Mock
    private TargetPointOfServiceModel fulfilmentStore;

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private WarehouseModel warehouse;

    @Mock
    private WarehouseSelectorStrategy ntlSelectorStrategy;

    @Mock
    private WarehouseSelectorStrategy externalWarehouseSelectorStrategy;

    @Mock
    private InstoreFulfilmentGlobalRulesChecker instoreFulfilmentGlobalRulesChecker;

    @Mock
    private TargetStockService targetStockService;

    @Mock
    private AbstractOrderEntryModel abstractOrderEntry;

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private SplitOegResult externalWarehouseSplitResult;

    @Mock
    private SplitOegResult sameStoreSplitResult;

    @Mock
    private SplitOegResult ntlSplitresult;

    @Mock
    private StockLevelModel stockLevel;

    @Mock
    private OrderEntryModel entry;

    @Mock
    private TargetTicketBusinessService targetTicketBusinessService;

    @Mock
    private TicketSender ticketSender;

    @Mock
    private ProductModel product;

    @Mock
    private OrderEntryGroup oeg;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private SameStoreCncSplitConsignmentStrategy sameStoreCncSplitConsignmentStrategy;

    @Mock
    private FastlineSplitConsignmentStrategy fastlineSplitConsignmentStrategy;

    @Mock
    private StoreSplitConsignmentStrategy storeSplitConsignmentStrategy;

    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;

    private final OrderModel order = new OrderModel();

    private final OrderEntryGroup orderEntryGroup = new OrderEntryGroup();

    private final List<OrderEntryGroup> orderEntryGroups = new ArrayList<>();

    private final OrderEntryModel orderEntry = new OrderEntryModel();

    private final ProductModel productModel = new ProductModel();



    @Before
    public void setUp() {
        orderEntry.setProduct(productModel);
        productModel.setCode("testProduct");
        orderEntryGroup.add(orderEntry);
        orderEntryGroups.add(orderEntryGroup);
        willDoNothing().given(oegParameterHelper).populateParamsFromOrder(orderEntryGroup);
        willReturn(ORDER_CODE).given(oegParameterHelper).getOrderCode(orderEntryGroup);
        willReturn(ORDER_CODE).given(oegParameterHelper).getOrderCode(orderEntryGroup);
        willReturn(CNC_STORE_NUMBER).given(oegParameterHelper).getCncStoreNumber(orderEntryGroup);
        willReturn(deliveryAddress).given(oegParameterHelper).getDeliveryAddress(orderEntryGroup);
    }

    @Test
    public void testPerformNullGroups() {
        assertThat(tgtSplitWithRoutingStrategy.perform(null)).isNull();
    }

    @Test
    public void testPerformEmptyGroups() {
        assertThat(tgtSplitWithRoutingStrategy.perform(ListUtils.EMPTY_LIST)).isEmpty();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPerformMultipleGroups() {
        orderEntryGroups.add(new OrderEntryGroup());
        tgtSplitWithRoutingStrategy.perform(orderEntryGroups);
    }

    @Test
    public void testPerformAssignCncToSameStore() throws TargetSplitOrderException {
        order.setDeliveryAddress(deliveryAddress);
        order.setDeliveryMode(deliveryMode);
        order.setCncStoreNumber(CNC_STORE_NUMBER);
        order.setCode(ORDER_CODE);

        willReturn(Boolean.TRUE).given(instoreFulfilmentGlobalRulesChecker)
                .areAllGlobalRulesPassed(orderEntryGroup);
        willReturn(externalWarehouseSplitResult).given(externalWarehouseSelectorStrategy).split(orderEntryGroup);
        willReturn(orderEntryGroup).given(externalWarehouseSplitResult).getNotAssignedToWarehouse();
        setupGlobalInstoreFulfilmentRulesPassed(true);
        willReturn(cncStore).given(sameStoreCncSelectorStrategy).isValidForSameStoreCnc(orderEntryGroup,
                CNC_STORE_NUMBER);

        tgtSplitWithRoutingStrategy.perform(orderEntryGroups);

        verify(oegParameterHelper).assignTposToOEG(orderEntryGroup, cncStore);
    }

    @Test
    public void testPerformAssignCncToAnotherStore() throws TargetSplitOrderException {
        order.setDeliveryAddress(deliveryAddress);
        order.setDeliveryMode(deliveryMode);
        order.setCncStoreNumber(CNC_STORE_NUMBER);
        order.setCode(ORDER_CODE);

        setupGlobalInstoreFulfilmentRulesPassed(true);
        setupDispatchInstoreFulfilmentRulesPassed(true);

        willReturn(externalWarehouseSplitResult).given(externalWarehouseSelectorStrategy).split(orderEntryGroup);
        willReturn(orderEntryGroup).given(externalWarehouseSplitResult).getNotAssignedToWarehouse();
        willReturn(null).given(sameStoreCncSelectorStrategy).isValidForSameStoreCnc(orderEntryGroup,
                CNC_STORE_NUMBER);
        willReturn(fulfilmentStore).given(storeSelectorStrategy).selectStore(orderEntryGroup);
        willReturn(OfcOrderType.INTERSTORE_DELIVERY).given(oegParameterHelper)
                .getOfcOrderType(orderEntryGroup, FULFILMENT_STORE_NUMBER);

        tgtSplitWithRoutingStrategy.perform(orderEntryGroups);

        verify(oegParameterHelper).assignTposToOEG(orderEntryGroup, fulfilmentStore);
    }

    private void verifyOrderEntryGroupResult(final List<OrderEntryGroup> resultOegs,
            final OrderEntryGroup orderEntryGroup) {
        assertThat(resultOegs).isNotNull();
        assertThat(resultOegs).hasSize(1);
        assertThat(resultOegs.get(0)).isEqualTo(orderEntryGroup);
    }

    @Test
    public void testPerformNTLNoSplitOneNTLOeg() throws TargetSplitOrderException {
        final List<OrderEntryGroup> oegs = new ArrayList<>();
        oegs.add(orderEntryGroup);
        setupNtlTestData();
        willReturn(externalWarehouseSplitResult).given(externalWarehouseSelectorStrategy).split(orderEntryGroup);
        willReturn(orderEntryGroup).given(externalWarehouseSplitResult).getNotAssignedToWarehouse();
        willReturn(ntlSplitresult).given(ntlSelectorStrategy).split(orderEntryGroup);
        willReturn(oegs).given(ntlSplitresult).getAssignedOEGsToWarehouse();

        final List<OrderEntryGroup> resultOegs = tgtSplitWithRoutingStrategy.perform(oegs);

        verifyOrderEntryGroupResult(resultOegs, orderEntryGroup);

    }

    @Test
    public void testPerformExternalSplitException() throws TargetSplitOrderException {
        final List<OrderEntryGroup> oegs = new ArrayList<>();
        oegs.add(orderEntryGroup);

        willThrow(new TargetSplitOrderException("Exception while splitting"))
                .given(externalWarehouseSelectorStrategy).split(orderEntryGroup);
        willReturn(orderEntryGroup).given(externalWarehouseSplitResult).getNotAssignedToWarehouse();

        final List<OrderEntryGroup> resultOegs = tgtSplitWithRoutingStrategy.perform(oegs);

        verifyOrderEntryGroupResult(resultOegs, orderEntryGroup);
    }


    @Test
    public void testPerformNTLNoSplitOneNonNTLOeg() throws TargetSplitOrderException {
        final ArrayList<OrderEntryGroup> oegs1 = new ArrayList<>();
        oegs1.add(orderEntryGroup);
        setupNtlTestData();
        willReturn(externalWarehouseSplitResult).given(externalWarehouseSelectorStrategy).split(orderEntryGroup);
        willReturn(orderEntryGroup).given(externalWarehouseSplitResult).getNotAssignedToWarehouse();
        willReturn(ntlSplitresult).given(ntlSelectorStrategy).split(orderEntryGroup);
        willReturn(oegs1).given(ntlSplitresult).getAssignedOEGsToWarehouse();
        willReturn(null).given(oegParameterHelper).getFulfilmentWarehouse(orderEntryGroup);

        final List<OrderEntryGroup> oegs = tgtSplitWithRoutingStrategy.perform(orderEntryGroups);

        verifyOrderEntryGroupResult(oegs, orderEntryGroup);
    }


    @Test
    public void testPerformFastlineSplitNoStockOeg() throws TargetSplitOrderException {

        final List<AbstractOrderEntryModel> refundEntries = new ArrayList<>();
        refundEntries.add(abstractOrderEntry);
        final List<OrderEntryGroup> oegs = new ArrayList<>();
        oegs.add(orderEntryGroup);
        setupNtlTestData();
        willReturn(externalWarehouseSplitResult).given(externalWarehouseSelectorStrategy).split(orderEntryGroup);
        willReturn(orderEntryGroup).given(externalWarehouseSplitResult).getNotAssignedToWarehouse();
        willReturn(ntlSplitresult).given(ntlSelectorStrategy).split(orderEntryGroup);
        willReturn(orderEntryGroup).given(ntlSplitresult).getNotAssignedToWarehouse();
        willReturn(warehouse).given(targetWarehouseService).getDefaultOnlineWarehouse();
        willThrow(new StockLevelNotFoundException("Stock not found for product in warehouse"))
                .given(targetStockService).checkAndGetStockLevel(orderEntryGroup.get(0).getProduct(), warehouse);

        assertThat(tgtSplitWithRoutingStrategy.perform(oegs)).isNotNull();
        verify(ticketSender).sendStockNotInDefaultWarehouse(orderEntryGroup);

        verifyOrderEntryGroupResult(oegs, orderEntryGroup);

    }

    @Test
    public void testPerformNTLSplitOeg() throws TargetSplitOrderException {
        final List<OrderEntryGroup> orderEntryGroupList = new ArrayList<>();
        final OrderEntryGroup originalOeg = new OrderEntryGroup();
        final OrderEntryGroup oeg1 = new OrderEntryGroup();
        final OrderEntryGroup oeg2 = new OrderEntryGroup();
        final OrderEntryModel giftCardEntry = new OrderEntryModel();
        final OrderEntryModel cncEntry = new OrderEntryModel();
        final OrderEntryModel ntlEntry = new OrderEntryModel();
        originalOeg.add(giftCardEntry);
        originalOeg.add(cncEntry);
        originalOeg.add(ntlEntry);
        oeg1.add(giftCardEntry);
        oeg2.add(cncEntry);
        oeg2.add(ntlEntry);
        final ArrayList<OrderEntryGroup> oegs = new ArrayList<>();
        oegs.add(oeg1);
        orderEntryGroupList.add(originalOeg);

        willReturn(externalWarehouseSplitResult).given(externalWarehouseSelectorStrategy).split(originalOeg);
        willReturn(oegs).given(externalWarehouseSplitResult).getAssignedOEGsToWarehouse();
        willReturn(oeg2).given(externalWarehouseSplitResult).getNotAssignedToWarehouse();
        willReturn(Boolean.TRUE).given(instoreFulfilmentGlobalRulesChecker)
                .areAllGlobalRulesPassed(oeg2);
        willReturn(Boolean.TRUE).given(instoreFulfilmentGlobalRulesChecker)
                .areAllDispatchRulesPassed(oeg2);
        willReturn(cncStore).given(sameStoreCncSelectorStrategy).isValidForSameStoreCnc(oeg2,
                CNC_STORE_NUMBER);
        willReturn(cncStore).given(storeSelectorStrategy).selectStore(oeg2);

        final List<OrderEntryGroup> resultOegs = tgtSplitWithRoutingStrategy.perform(orderEntryGroupList);
        verify(oegParameterHelper).assignTposToOEG(oeg2, cncStore);


        assertThat(resultOegs).isNotNull();
        assertThat(resultOegs).hasSize(2);
        assertThat(resultOegs.get(0)).isEqualTo(oeg1);
        assertThat(resultOegs.get(1)).isEqualTo(oeg2);
        assertThat(resultOegs.get(0).get(0)).isEqualTo(giftCardEntry);
        assertThat(resultOegs.get(1).get(0)).isEqualTo(cncEntry);
        assertThat(resultOegs.get(1).get(1)).isEqualTo(ntlEntry);
    }

    private void setupNtlTestData() {
        order.setDeliveryAddress(deliveryAddress);
        order.setDeliveryMode(deliveryMode);
        order.setCode(ORDER_CODE);

        setupGlobalInstoreFulfilmentRulesPassed(false);

        willReturn(null).given(oegParameterHelper).getCncStoreNumber(orderEntryGroup);
    }

    @Test
    public void testPerformAssignDeliveryToStore() throws TargetSplitOrderException {
        order.setDeliveryAddress(deliveryAddress);
        order.setDeliveryMode(deliveryMode);
        order.setCode(ORDER_CODE);

        setupGlobalInstoreFulfilmentRulesPassed(true);
        setupDispatchInstoreFulfilmentRulesPassed(true);

        willReturn(externalWarehouseSplitResult).given(externalWarehouseSelectorStrategy).split(orderEntryGroup);
        willReturn(orderEntryGroup).given(externalWarehouseSplitResult).getNotAssignedToWarehouse();
        willReturn(cncStore).given(storeSelectorStrategy).selectStore(orderEntryGroup);
        willReturn(OfcOrderType.CUSTOMER_DELIVERY).given(oegParameterHelper)
                .getOfcOrderType(orderEntryGroup, FULFILMENT_STORE_NUMBER);

        tgtSplitWithRoutingStrategy.perform(orderEntryGroups);

        verify(oegParameterHelper).assignTposToOEG(orderEntryGroup, cncStore);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAfterSplittingNotTargetConsignment() {
        tgtSplitWithRoutingStrategy.afterSplitting(Mockito.any(OrderEntryGroup.class),
                Mockito.any(ConsignmentModel.class));
    }

    @Test
    public void testAfterSplittingForStore() {

        willReturn(cncStore).given(consignmentReroutingHelper).getAssignedStoreForWarehouse(warehouse);
        willReturn(warehouse).given(oegParameterHelper).getFulfilmentWarehouse(orderEntryGroup);
        willReturn(deliveryMode).given(consignment).getDeliveryMode();
        willReturn(CNC_STORE_NUMBER).given(cncStore).getStoreNumber();
        willReturn(OfcOrderType.INSTORE_PICKUP).given(oegParameterHelper)
                .getOfcOrderType(orderEntryGroup, CNC_STORE_NUMBER);

        tgtSplitWithRoutingStrategy.afterSplitting(orderEntryGroup, consignment);

        verify(consignmentToWarehouseAssigner).assignConsignmentToStore(consignment, warehouse,
                OfcOrderType.INSTORE_PICKUP, deliveryMode, ORDER_CODE);
    }

    @Test
    public void testAfterSplittingNTL() {

        // Warehouse is linked to fulfilmentStore of type WAREHOUSE
        willReturn(deliveryMode).given(consignment).getDeliveryMode();
        willReturn(fulfilmentStore).given(consignmentReroutingHelper).getAssignedStoreForWarehouse(warehouse);
        willReturn(warehouse).given(oegParameterHelper).getFulfilmentWarehouse(orderEntryGroup);
        willReturn(PointOfServiceTypeEnum.WAREHOUSE).given(fulfilmentStore).getType();

        tgtSplitWithRoutingStrategy.afterSplitting(orderEntryGroup, consignment);

        verify(consignmentToWarehouseAssigner).assignConsignmentToNTL(consignment, warehouse);
    }

    @Test
    public void testAfterSplittingExternalWarehouse() {

        // No store on the warehouse
        willReturn(warehouse).given(oegParameterHelper).getFulfilmentWarehouse(orderEntryGroup);
        willReturn(null).given(consignmentReroutingHelper).getAssignedStoreForWarehouse(warehouse);
        willReturn(Boolean.TRUE).given(warehouse).isExternalWarehouse();
        willReturn(deliveryMode).given(consignment).getDeliveryMode();
        tgtSplitWithRoutingStrategy.afterSplitting(orderEntryGroup, consignment);

        verify(consignmentToWarehouseAssigner).assignConsignmentToExternalWarehouse(deliveryMode, consignment,
                warehouse);
    }

    @Test
    public void testAfterSplittingDefaultWarehouse() {

        // Warehouse is not set in oeg
        willReturn(null).given(oegParameterHelper).getFulfilmentWarehouse(orderEntryGroup);
        willReturn(deliveryMode).given(consignment).getDeliveryMode();
        tgtSplitWithRoutingStrategy.afterSplitting(orderEntryGroup, consignment);

        verify(consignmentToWarehouseAssigner).assignDefaultWarehouse(consignment);
    }

    @Test
    public void testAfterSplittingCancellationWarehouse() {
        willReturn(Boolean.TRUE).given(oegParameterHelper).getNotAssignedOegFlag(orderEntryGroup);
        final Map<String, Long> assignedQtyMap = new HashMap<>();
        willReturn(assignedQtyMap).given(oegParameterHelper).getAssignedQty(orderEntryGroup);
        tgtSplitWithRoutingStrategy.afterSplitting(orderEntryGroup, consignment);
        verify(consignmentToWarehouseAssigner).assignCancellationWarehouseAndUpdateConsignmentStatus(
                consignment);
        verify(targetBusinessProcessService).startOrderItemCancelProcess(any(OrderModel.class),
                any(TargetOrderCancelEntryList.class));
    }

    private void setupGlobalInstoreFulfilmentRulesPassed(final boolean success) {

        willReturn(Boolean.valueOf(success)).given(instoreFulfilmentGlobalRulesChecker)
                .areAllGlobalRulesPassed(orderEntryGroup);
    }

    private void setupDispatchInstoreFulfilmentRulesPassed(final boolean success) {

        willReturn(Boolean.valueOf(success)).given(instoreFulfilmentGlobalRulesChecker)
                .areAllDispatchRulesPassed(orderEntryGroup);
    }

    @Test(expected = TargetStoreStockCheckException.class)
    public void testPerformAssignSameStoreThrowsStockCheckExceptionWhenStockLookupFails()
            throws TargetSplitOrderException {
        order.setDeliveryAddress(deliveryAddress);
        order.setDeliveryMode(deliveryMode);
        order.setCode(ORDER_CODE);

        setupGlobalInstoreFulfilmentRulesPassed(true);
        setupDispatchInstoreFulfilmentRulesPassed(true);

        willReturn(externalWarehouseSplitResult).given(externalWarehouseSelectorStrategy).split(orderEntryGroup);
        willReturn(orderEntryGroup).given(externalWarehouseSplitResult).getNotAssignedToWarehouse();
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService).isFeatureEnabled("fastlineFalcon");
        willThrow(new TargetStoreStockCheckException("exception", new Exception()))
                .given(sameStoreCncSplitConsignmentStrategy).split(orderEntryGroup);

        tgtSplitWithRoutingStrategy.perform(orderEntryGroups);

    }

    @Test(expected = TargetStoreStockCheckException.class)
    public void testPerformAssignStoreSplitThrowsStockCheckExceptionWhenStockLookupFails()
            throws TargetSplitOrderException {
        order.setDeliveryAddress(deliveryAddress);
        order.setDeliveryMode(deliveryMode);
        order.setCode(ORDER_CODE);

        setupGlobalInstoreFulfilmentRulesPassed(true);
        setupDispatchInstoreFulfilmentRulesPassed(true);

        willReturn(externalWarehouseSplitResult).given(externalWarehouseSelectorStrategy).split(orderEntryGroup);
        willReturn(orderEntryGroup).given(externalWarehouseSplitResult).getNotAssignedToWarehouse();
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService).isFeatureEnabled("fastlineFalcon");
        willReturn(sameStoreSplitResult).given(sameStoreCncSplitConsignmentStrategy).split(orderEntryGroup);
        willReturn(orderEntryGroup).given(sameStoreSplitResult).getNotAssignedToWarehouse();
        willThrow(new TargetStoreStockCheckException("exception", new Exception()))
                .given(storeSplitConsignmentStrategy).split(orderEntryGroup);

        tgtSplitWithRoutingStrategy.perform(orderEntryGroups);

    }


}

package au.com.target.tgtfulfilment.salesapplication.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;

import java.util.Collections;
import java.util.Map;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.SalesApplicationConfigModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFulfilmentSalesApplicationConfigServiceImplTest {

    private static final SalesApplication EBAY_SALES_APP = SalesApplication.EBAY;

    @InjectMocks
    private final TargetFulfilmentSalesApplicationConfigServiceImpl service = new TargetFulfilmentSalesApplicationConfigServiceImpl();

    @Mock
    private GenericDao<SalesApplicationConfigModel> salesApplicationConfigDao;

    @Mock
    private SalesApplicationConfigModel salesAppConfig;

    private final Map<String, SalesApplication> params = Collections
            .singletonMap(SalesApplicationConfigModel.SALESAPPLICATION, EBAY_SALES_APP);

    @Test
    public void testIsDenyShortPicksNull() {
        Mockito.doReturn(null).when(salesApplicationConfigDao).find(params);

        Assertions.assertThat(service.isDenyShortPicks(EBAY_SALES_APP)).isFalse();
    }

    @Test
    public void testIsDenyShortPicksFalse() {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.FALSE).when(salesAppConfig).isDenyShortPicks();

        Assertions.assertThat(service.isDenyShortPicks(EBAY_SALES_APP)).isFalse();
    }

    @Test
    public void testIsDenyShortPicksTrue() {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.TRUE).when(salesAppConfig).isDenyShortPicks();

        Assertions.assertThat(service.isDenyShortPicks(EBAY_SALES_APP)).isTrue();
    }

    @Test
    public void testIsSuppressAutoRefundNull() {
        Mockito.doReturn(null).when(salesApplicationConfigDao).find(params);

        Assertions.assertThat(service.isSuppressAutoRefund(EBAY_SALES_APP)).isFalse();
    }

    @Test
    public void testIsSuppressAutoRefundFalse() {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.FALSE).when(salesAppConfig).isSuppressAutoRefund();

        Assertions.assertThat(service.isSuppressAutoRefund(EBAY_SALES_APP)).isFalse();
    }

    @Test
    public void testIsSuppressAutoRefundTrue() {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.TRUE).when(salesAppConfig).isSuppressAutoRefund();

        Assertions.assertThat(service.isSuppressAutoRefund(EBAY_SALES_APP)).isTrue();
    }

}

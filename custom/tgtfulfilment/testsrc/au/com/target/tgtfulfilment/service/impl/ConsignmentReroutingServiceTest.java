/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.ConsignmentCreationException;
import de.hybris.platform.ordersplitting.ConsignmentService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.strategy.SplittingStrategy;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtfulfilment.ordersplitting.strategy.impl.ConsignmentReroutingStrategy;


/**
 * Unit tests for ConsignmentReroutingService.
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ConsignmentReroutingServiceTest {

    @InjectMocks
    private final ConsignmentReroutingService consignmentReroutingService = new ConsignmentReroutingService();

    @Mock
    private ConsignmentService consignmentService;

    @Mock
    private ConsignmentReroutingStrategy splittingStrategy;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private ConsignmentModel consignmentModel;

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();


    //CHECKSTYLE:ON

    @Before
    public void setStratergies() {
        final List<SplittingStrategy> splittingStrategies = new ArrayList<>();
        splittingStrategies.add(splittingStrategy);
        consignmentReroutingService.setStrategiesList(splittingStrategies);
    }

    @Test
    public void testRerouteConsignmentWithNullConsignment() throws ConsignmentCreationException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Consignment model cannot be null");
        consignmentReroutingService.rerouteConsignment(null);
    }

    @Test
    public void testRerouteConsignmentWithNullOrder() throws ConsignmentCreationException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Consignment must have an order");
        final ConsignmentModel consignment = Mockito.mock(ConsignmentModel.class);
        BDDMockito.given(consignment.getOrder()).willReturn(null);
        consignmentReroutingService.rerouteConsignment(consignment);
    }

    @Test
    public void testRerouteConsignmentWithNullConsignmentEntries() throws ConsignmentCreationException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Consignment must have consignment entries");
        final ConsignmentModel consignment = Mockito.mock(ConsignmentModel.class);
        final OrderModel order = Mockito.mock(OrderModel.class);
        BDDMockito.given(consignment.getOrder()).willReturn(order);
        BDDMockito.given(consignment.getConsignmentEntries()).willReturn(null);
        consignmentReroutingService.rerouteConsignment(consignment);
    }

    @Test
    public void testRerouteConsignmentWithEmptyConsignmentEntries() throws ConsignmentCreationException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Consignment must have consignment entries");
        final ConsignmentModel consignment = Mockito.mock(ConsignmentModel.class);
        final OrderModel order = Mockito.mock(OrderModel.class);
        BDDMockito.given(consignment.getOrder()).willReturn(order);
        BDDMockito.given(consignment.getConsignmentEntries()).willReturn(new HashSet<ConsignmentEntryModel>());
        consignmentReroutingService.rerouteConsignment(consignment);
    }

    @Test
    public void testRerouteConsignmentWithANullOrderEntry() throws ConsignmentCreationException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("ConsignmentEntry must have an OrderEntry");
        final OrderEntryModel orderEntry = Mockito.mock(OrderEntryModel.class);
        final ConsignmentModel consignment = Mockito.mock(ConsignmentModel.class);
        final ConsignmentEntryModel consignmentEntry1 = Mockito.mock(ConsignmentEntryModel.class);
        final ConsignmentEntryModel consignmentEntry2 = Mockito.mock(ConsignmentEntryModel.class);
        final Set<ConsignmentEntryModel> consignmentEntries = new HashSet<>();
        consignmentEntries.add(consignmentEntry1);
        consignmentEntries.add(consignmentEntry2);
        final OrderModel order = Mockito.mock(OrderModel.class);
        BDDMockito.given(consignment.getOrder()).willReturn(order);
        BDDMockito.given(consignment.getConsignmentEntries()).willReturn(consignmentEntries);
        BDDMockito.given(consignmentEntry1.getOrderEntry()).willReturn(null);
        BDDMockito.given(consignmentEntry2.getOrderEntry()).willReturn(orderEntry);
        BDDMockito.given(splittingStrategy.perform(Mockito.anyList())).willReturn(new ArrayList<OrderEntryGroup>());
        consignmentReroutingService.rerouteConsignment(consignment);
    }

    @Test
    public void testIsRequireReroutingWithEmptyConsignment() {
        final boolean result = consignmentReroutingService.isRequireRerouting(consignmentModel);
        assertThat(result).isFalse();
    }

    @Test
    public void testIsRequireReroutingWithRequiredCase() {
        final ConsignmentEntryModel consignmentEntry1 = mock(ConsignmentEntryModel.class);
        given(consignmentEntry1.getShippedQuantity()).willReturn(Long.valueOf(5));
        given(consignmentEntry1.getQuantity()).willReturn(Long.valueOf(5));
        final ConsignmentEntryModel consignmentEntry2 = mock(ConsignmentEntryModel.class);
        given(consignmentEntry2.getShippedQuantity()).willReturn(Long.valueOf(4));
        given(consignmentEntry2.getQuantity()).willReturn(Long.valueOf(5));
        final Set<ConsignmentEntryModel> consignmentEntries = new HashSet<>();
        consignmentEntries.add(consignmentEntry1);
        consignmentEntries.add(consignmentEntry2);
        given(consignmentModel.getConsignmentEntries()).willReturn(consignmentEntries);
        final boolean result = consignmentReroutingService.isRequireRerouting(consignmentModel);
        assertThat(result).isTrue();
    }

    @Test
    public void testIsRequireReroutingWithNotRequiredCase() {
        final ConsignmentEntryModel consignmentEntry1 = mock(ConsignmentEntryModel.class);
        given(consignmentEntry1.getShippedQuantity()).willReturn(Long.valueOf(5));
        given(consignmentEntry1.getQuantity()).willReturn(Long.valueOf(5));
        final ConsignmentEntryModel consignmentEntry2 = mock(ConsignmentEntryModel.class);
        given(consignmentEntry2.getShippedQuantity()).willReturn(Long.valueOf(5));
        given(consignmentEntry2.getQuantity()).willReturn(Long.valueOf(5));
        final Set<ConsignmentEntryModel> consignmentEntries = new HashSet<>();
        consignmentEntries.add(consignmentEntry1);
        consignmentEntries.add(consignmentEntry2);
        given(consignmentModel.getConsignmentEntries()).willReturn(consignmentEntries);
        final boolean result = consignmentReroutingService.isRequireRerouting(consignmentModel);
        assertThat(result).isFalse();
    }

    @Test
    public void testPopulateOrderEntryGroupWithRequiredQty() {
        final String productCode1 = "testProduct1";
        final ProductModel product1 = mock(ProductModel.class);
        given(product1.getCode()).willReturn(productCode1);
        final AbstractOrderEntryModel orderEntry1 = mock(AbstractOrderEntryModel.class);
        given(orderEntry1.getProduct()).willReturn(product1);
        final ConsignmentEntryModel consignmentEntry1 = mock(ConsignmentEntryModel.class);
        given(consignmentEntry1.getOrderEntry()).willReturn(orderEntry1);
        given(consignmentEntry1.getShippedQuantity()).willReturn(Long.valueOf(3));
        given(consignmentEntry1.getQuantity()).willReturn(Long.valueOf(5));
        final ConsignmentEntryModel consignmentEntry2 = mock(ConsignmentEntryModel.class);
        given(consignmentEntry2.getShippedQuantity()).willReturn(Long.valueOf(5));
        given(consignmentEntry2.getQuantity()).willReturn(Long.valueOf(5));
        final Set<ConsignmentEntryModel> consignmentEntries = new HashSet<>();
        consignmentEntries.add(consignmentEntry1);
        consignmentEntries.add(consignmentEntry2);
        given(consignmentModel.getConsignmentEntries()).willReturn(consignmentEntries);
        final OrderEntryGroup oeg = new OrderEntryGroup();
        final Map<String, Long> requiredQtyMap = new HashMap<>();
        consignmentReroutingService.populateOrderEntryGroupWithRequiredQty(consignmentModel, oeg, requiredQtyMap);
        assertThat(oeg.size()).isEqualTo(1);
        assertThat(oeg.get(0)).isEqualTo(orderEntry1);
        assertThat(requiredQtyMap).hasSize(1);
        assertThat(requiredQtyMap.get(productCode1)).isEqualTo(2);
    }
}

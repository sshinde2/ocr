/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.client.PollConsignmentWarehouseRestClient;
import au.com.target.tgtfulfilment.client.SendToWarehouseRestClient;
import au.com.target.tgtfulfilment.integration.converters.sendtowarehouse.SendToWarehouseRequestConverter;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseRequest;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseResponse;


/**
 * Tests for RestSendToWarehouseProtocol
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class RestSendToWarehouseProtocolTest {

    @Mock
    private SendToWarehouseRestClient sendToWarehouseRestClient;

    @Mock
    private TargetConsignmentModel mockConsignment;

    @InjectMocks
    private final RestSendToWarehouseProtocol restSendToWarehouseProtocol = new RestSendToWarehouseProtocol();

    @Mock
    private PollConsignmentWarehouseRestClient pollWarehouseRestClient;


    @Test(expected = IllegalArgumentException.class)
    public void testNullConsignment() {
        final SendToWarehouseRequestConverter sendToWarehouseRequestConverter = new SendToWarehouseRequestConverter();
        restSendToWarehouseProtocol.setSendToWarehouseRequestConverter(sendToWarehouseRequestConverter);
        restSendToWarehouseProtocol.sendConsignmentExtract(null, null);
    }

    @Test
    public void testUnshippedConsignments() {
        given(mockConsignment.getCode()).willReturn("a2009");
        final ArgumentCaptor<PollWarehouseRequest> argumentCaptor = ArgumentCaptor.forClass(PollWarehouseRequest.class);
        given(pollWarehouseRestClient.pollConsignments(argumentCaptor.capture()))
                .willReturn(PollWarehouseResponse.getSuccessResponse());
        final PollWarehouseResponse response = restSendToWarehouseProtocol.sendUnshippedConsignments(Collections
                .singletonList(mockConsignment));
        verify(pollWarehouseRestClient).pollConsignments(argumentCaptor.capture());
        assertThat(response.isSuccess()).isTrue();
    }
}

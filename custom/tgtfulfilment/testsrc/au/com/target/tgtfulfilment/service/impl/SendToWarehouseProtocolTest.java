/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.ConsignmentSentToWarehouseReason;
import au.com.target.tgtfulfilment.exceptions.ConsignmentStatusValidationException;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.fulfilmentservice.TargetFulfilmentService;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseResponse;
import au.com.target.tgtfulfilment.service.SendToWarehouseProtocol;
import au.com.target.tgtfulfilment.service.SendToWarehouseSelectorStrategy;


/**
 * @author sbryan6
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendToWarehouseProtocolTest {

    @Mock
    private SendToWarehouseSelectorStrategy sendToWarehouseSelectorStrategy;

    @Mock
    private SendToWarehouseProtocol fulfilmentProcessService;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private final SendToWarehouseServiceImpl sendToWarehouseService = new SendToWarehouseServiceImpl();

    @Mock
    private OrderModel order;

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private TargetConsignmentModel consignment2;

    @Mock
    private TargetFulfilmentService targetFulfilmentService;

    private SendToWarehouseResponse response1;

    private SendToWarehouseResponse response2;

    private final Set<ConsignmentModel> consignments = new HashSet<>();

    @Before
    public void setup() {
        response1 = new SendToWarehouseResponse();
        response1.setSuccess(true);

        response2 = new SendToWarehouseResponse();
        response2.setSuccess(false);

        consignments.add(consignment);

        Mockito.when(order.getConsignments()).thenReturn(consignments);


        Mockito.when(sendToWarehouseSelectorStrategy.getSendToWarehouseProtocolForConsignment(consignment)).thenReturn(
                fulfilmentProcessService);

        Mockito.when(sendToWarehouseSelectorStrategy.getSendToWarehouseProtocolForConsignment(consignment2))
                .thenReturn(
                        fulfilmentProcessService);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSendNewNoConsignments() throws Exception {

        Mockito.when(order.getConsignments()).thenReturn(null);
        Mockito.when(fulfilmentProcessService.sendConsignmentExtract(consignment, ConsignmentSentToWarehouseReason.NEW))
                .thenReturn(response1);

        sendToWarehouseService.sendNewOrder(order);

        checkConsignmentIsNotSentToWarehouse();
        checkStatusNotSet(consignment);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testSendCancelNoConsignments() throws Exception {

        Mockito.when(order.getConsignments()).thenReturn(null);

        sendToWarehouseService.sendOrderCancellation(order);
        Mockito.when(fulfilmentProcessService.sendConsignmentExtract(consignment, ConsignmentSentToWarehouseReason.NEW))
                .thenReturn(response1);

        checkConsignmentIsNotSentToWarehouse();
        checkStatusNotSet(consignment);
    }


    @Test
    public void testSendNewWithConsignmentNullStatus() throws Exception {

        Mockito.when(consignment.getStatus()).thenReturn(null);

        sendToWarehouseService.sendNewOrder(order);

        checkConsignmentIsNotSentToWarehouse();
        checkStatusNotSet(consignment);
    }


    @Test
    public void testSendCancelWithConsignmentNullStatus() throws Exception {

        Mockito.when(consignment.getStatus()).thenReturn(null);

        sendToWarehouseService.sendOrderCancellation(order);

        checkConsignmentIsNotSentToWarehouse();
        checkStatusNotSet(consignment);
    }


    @Test
    public void testSendNewWithConsignmentWrongStatus() throws Exception {

        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);

        sendToWarehouseService.sendNewOrder(order);

        checkConsignmentIsNotSentToWarehouse();
        checkStatusNotSet(consignment);
    }

    @Test
    public void testSendNewWithConsignmentOneWrongStatusAnotherCorrectStatus() throws Exception {

        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        Mockito.when(consignment2.getStatus()).thenReturn(ConsignmentStatus.CREATED);
        Mockito.when(
                fulfilmentProcessService.sendConsignmentExtract(consignment2, ConsignmentSentToWarehouseReason.NEW))
                .thenReturn(response1);

        consignments.add(consignment2);

        sendToWarehouseService.sendNewOrder(order);

        checkConsignmentSent(1, consignment2);
        checkStatusNotSet(consignment);
        checkStatusChangedTo(consignment2, ConsignmentStatus.SENT_TO_WAREHOUSE);
    }

    @Test
    public void testSendCancelWithConsignmentWrongStatus() throws Exception {

        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.CREATED);

        sendToWarehouseService.sendOrderCancellation(order);

        checkConsignmentIsNotSentToWarehouse();
        checkStatusNotSet(consignment);
    }


    @Test
    public void testSendNewWithConsignmentsForNonStore() throws FulfilmentException,
            ConsignmentStatusValidationException {

        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.CREATED);
        Mockito.when(fulfilmentProcessService.sendConsignmentExtract(consignment, ConsignmentSentToWarehouseReason.NEW))
                .thenReturn(response1);

        sendToWarehouseService.sendNewOrder(order);

        checkConsignmentSent(1, consignment);
        checkStatusChangedTo(consignment, ConsignmentStatus.SENT_TO_WAREHOUSE);
    }

    @Test
    public void testSendNewWithConsignmentsForNonStoreResponseFalse() throws FulfilmentException,
            ConsignmentStatusValidationException {
        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.CREATED);
        Mockito.when(fulfilmentProcessService.sendConsignmentExtract(consignment, ConsignmentSentToWarehouseReason.NEW))
                .thenReturn(response2);

        sendToWarehouseService.sendNewOrder(order);

        checkConsignmentSent(1, consignment);
        checkStatusNotSet(consignment);
    }

    @Test
    public void testSendNewWithConsignmentsForNonStoreMultipleConsignmentsOneFailedOtherSuccess()
            throws FulfilmentException, ConsignmentStatusValidationException
    {
        consignments.add(consignment2);
        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.CREATED);
        Mockito.when(consignment2.getStatus()).thenReturn(ConsignmentStatus.CREATED);
        Mockito.when(fulfilmentProcessService.sendConsignmentExtract(consignment, ConsignmentSentToWarehouseReason.NEW))
                .thenReturn(response1);
        Mockito.when(
                fulfilmentProcessService.sendConsignmentExtract(consignment2, ConsignmentSentToWarehouseReason.NEW))
                .thenReturn(response2);

        sendToWarehouseService.sendNewOrder(order);

        checkConsignmentSent(1, consignment);
        checkConsignmentSent(1, consignment2);
        checkStatusNotSet(consignment2);
        checkStatusChangedTo(consignment, ConsignmentStatus.SENT_TO_WAREHOUSE);
    }

    @Test
    public void testSendNewWithConsignmentsForStoreResponseFalse() throws FulfilmentException,
            ConsignmentStatusValidationException {
        response1.setSuccess(false);
        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.CREATED);
        Mockito.when(fulfilmentProcessService.sendConsignmentExtract(consignment, ConsignmentSentToWarehouseReason.NEW))
                .thenReturn(response1);
        sendToWarehouseService.sendNewOrder(order);

        checkConsignmentSent(1, consignment);
        checkStatusNotSet(consignment);
    }


    @Test
    public void testSendNewWithNonTargetConsignments() throws Exception {
        final ConsignmentModel nonTargetConsignment = Mockito.mock(ConsignmentModel.class);
        consignments.add(nonTargetConsignment);
        consignments.add(consignment);

        Mockito.when(order.getConsignments()).thenReturn(consignments);
        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.CREATED);
        Mockito.when(
                fulfilmentProcessService.sendConsignmentExtract(consignment, ConsignmentSentToWarehouseReason.NEW))
                .thenReturn(response1);

        sendToWarehouseService.sendNewOrder(order);

        checkConsignmentSent(1, consignment);
        checkStatusChangedTo(consignment, ConsignmentStatus.SENT_TO_WAREHOUSE);

        Mockito.verifyZeroInteractions(nonTargetConsignment);
    }

    @Test
    public void testWithCancelledConsignment() throws Exception {

        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.CANCELLED);
        Mockito.when(
                fulfilmentProcessService
                        .sendConsignmentExtract(consignment, ConsignmentSentToWarehouseReason.CANCELLED))
                .thenReturn(response1);

        sendToWarehouseService.sendOrderCancellation(order);

        checkConsignmentSentWithCancelled();
    }

    @Test
    public void testWithCancelledConsignmentWithUnsuccessfulResponse() throws Exception {

        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.CANCELLED);
        response1.setSuccess(false);
        Mockito.when(
                fulfilmentProcessService
                        .sendConsignmentExtract(consignment, ConsignmentSentToWarehouseReason.CANCELLED))
                .thenReturn(response1);

        sendToWarehouseService.sendOrderCancellation(order);

        checkConsignmentSentWithCancelled();
    }

    @Test
    public void testWithPartCancelledConsignmentWithQuantity() throws RetryLaterException, Exception {

        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.SENT_TO_WAREHOUSE);

        Mockito.when(
                fulfilmentProcessService
                        .sendConsignmentExtract(consignment, ConsignmentSentToWarehouseReason.UPDATED))
                .thenReturn(response1);

        sendToWarehouseService.sendOrderCancellation(order);

        checkConsignmentSentWithUpdated();
    }

    @Test
    public void testResendConsignmentForNonStore() throws FulfilmentException, ConsignmentStatusValidationException {
        final WarehouseModel warehouse = mock(WarehouseModel.class);
        Mockito.when(warehouse.getCode()).thenReturn("fastlineWarehouse");
        Mockito.when(consignment.getWarehouse()).thenReturn(warehouse);

        Mockito.when(consignment.getStatus()).thenReturn(ConsignmentStatus.CREATED);
        Mockito.when(fulfilmentProcessService.sendConsignmentExtract(consignment, ConsignmentSentToWarehouseReason.NEW))
                .thenReturn(response1);

        sendToWarehouseService.sendConsignment(consignment);

        checkConsignmentSent(1, consignment);
        checkStatusChangedTo(consignment, ConsignmentStatus.SENT_TO_WAREHOUSE);
    }


    private void checkConsignmentIsNotSentToWarehouse() {

        Mockito.verifyZeroInteractions(fulfilmentProcessService);
    }

    private void checkConsignmentSent(final int times, final ConsignmentModel consignmentSent) {

        Mockito.verify(fulfilmentProcessService, Mockito.times(times)).sendConsignmentExtract(consignmentSent,
                ConsignmentSentToWarehouseReason.NEW);
    }

    private void checkConsignmentSentWithCancelled() {

        Mockito.verify(fulfilmentProcessService).sendConsignmentExtract(consignment,
                ConsignmentSentToWarehouseReason.CANCELLED);
    }

    private void checkConsignmentSentWithUpdated() {

        Mockito.verify(fulfilmentProcessService).sendConsignmentExtract(consignment,
                ConsignmentSentToWarehouseReason.UPDATED);
        Mockito.verify(consignment, Mockito.never()).setStatus(Mockito.any(ConsignmentStatus.class));
        Mockito.verifyZeroInteractions(modelService);
    }

    private void checkStatusNotSet(final ConsignmentModel consignmentNotchanged) {

        Mockito.verify(consignmentNotchanged, Mockito.never()).setStatus(Mockito.any(ConsignmentStatus.class));
    }

    private void checkStatusChangedTo(final TargetConsignmentModel consignmentChanged, final ConsignmentStatus newStatus) {

        Mockito.verify(consignmentChanged).setStatus(newStatus);
        Mockito.verify(consignmentChanged).setSentToWarehouseDate(Mockito.any(Date.class));
        Mockito.verify(modelService).save(consignmentChanged);
    }


}

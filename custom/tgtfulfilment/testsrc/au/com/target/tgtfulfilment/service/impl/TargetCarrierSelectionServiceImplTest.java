/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.PostCodeGroupModel;
import au.com.target.tgtcore.model.PostCodeModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.postcode.service.TargetPostCodeService;
import au.com.target.tgtfulfilment.dao.TargetCarrierDao;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;
import au.com.target.tgtfulfilment.model.TargetPostCodeAwareCarrierModel;


/**
 * @author Vivek
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCarrierSelectionServiceImplTest {

    private static final String TEST_POST_CODE = "7001";

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private TargetCarrierDao targetCarrierDao;

    @Mock
    private TargetPostCodeService targetPostCodeService;

    @Mock
    private TargetZoneDeliveryModeModel deliveryMode;

    @Mock
    private AddressModel shippingAddress;

    private final Collection<PostCodeGroupModel> postCodeGroups = new ArrayList<>();

    @InjectMocks
    private final TargetCarrierSelectionServiceImpl targetCarrierSelectionServiceImpl = new TargetCarrierSelectionServiceImpl();

    @Before
    public void setUp() {
        final List<TargetCarrierModel> carrierModels = new ArrayList<>();
        final TargetPostCodeAwareCarrierModel carrierStrack = new TargetPostCodeAwareCarrierModel();
        carrierStrack.setCode("STRACK");
        carrierStrack.setMaxWeight(Double.valueOf(3.00d));

        final PostCodeGroupModel postCodeGroup = new PostCodeGroupModel();
        final List<PostCodeModel> postCodes = new ArrayList<>();
        final PostCodeModel postCodeModel = new PostCodeModel();
        postCodeModel.setPostCode(TEST_POST_CODE);
        postCodes.add(postCodeModel);
        postCodeGroup.setPostCodes(postCodes);
        postCodeGroups.add(postCodeGroup);
        carrierStrack.setPostCodeGroups(postCodeGroups);
        final TargetCarrierModel carrierAP = new TargetCarrierModel();
        carrierAP.setCode("AP");
        final TargetCarrierModel carrierToll = new TargetCarrierModel();
        carrierToll.setCode("TOLL");

        // Adding in the order of priorities set in the system.
        carrierModels.add(carrierStrack);
        carrierModels.add(carrierAP);
        carrierModels.add(carrierToll);
        Mockito.when(consignment.getDeliveryMode()).thenReturn(deliveryMode);
        Mockito.when(consignment.getShippingAddress()).thenReturn(shippingAddress);
        Mockito.when(shippingAddress.getPostalcode()).thenReturn(TEST_POST_CODE);
        Mockito.when(targetCarrierDao.getApplicableCarriers(deliveryMode, false, false)).thenReturn(carrierModels);
    }

    @Test
    public void testGetPreferredCarrierWithNoConsignmentWeight() {
        Mockito.when(consignment.getTotalWeight()).thenReturn(null);
        final TargetCarrierModel carrier = targetCarrierSelectionServiceImpl.getPreferredCarrier(consignment, false,
                false);
        Assert.assertNotNull(carrier);
        Assert.assertEquals("AP", carrier.getCode());
    }

    @Test
    public void testGetPreferredCarrierWithWeight2kgAndSupportedPostCode() {
        Mockito.when(consignment.getTotalWeight()).thenReturn(Double.valueOf(2.00d));
        Mockito.when(Boolean.valueOf(targetPostCodeService.doesPostCodeBelongsToGroups(TEST_POST_CODE, postCodeGroups)))
                .thenReturn(Boolean.TRUE);
        final TargetCarrierModel carrier = targetCarrierSelectionServiceImpl.getPreferredCarrier(consignment, false,
                false);
        Assert.assertNotNull(carrier);
        Assert.assertEquals("STRACK", carrier.getCode());
    }

    @Test
    public void testGetPreferredCarrierWithWeight3kgAndSupportedPostCode() {
        Mockito.when(consignment.getTotalWeight()).thenReturn(Double.valueOf(3.00d));
        final TargetCarrierModel carrier = targetCarrierSelectionServiceImpl.getPreferredCarrier(consignment, false,
                false);
        Assert.assertNotNull(carrier);
        Assert.assertEquals("AP", carrier.getCode());
    }

    @Test
    public void testGetPreferredCarrierWithWeight4kgAndSupportedPostCode() {
        Mockito.when(consignment.getTotalWeight()).thenReturn(Double.valueOf(3.00d));
        final TargetCarrierModel carrier = targetCarrierSelectionServiceImpl.getPreferredCarrier(consignment, false,
                false);
        Assert.assertNotNull(carrier);
        Assert.assertEquals("AP", carrier.getCode());
    }

    @Test
    public void testGetPreferredCarrierSupportedWeightButUnsupportedPostCode() {
        Mockito.when(consignment.getTotalWeight()).thenReturn(Double.valueOf(2.00d));

        final TargetPostCodeAwareCarrierModel carrierStrack = new TargetPostCodeAwareCarrierModel();
        carrierStrack.setCode("STRACK");
        carrierStrack.setMaxWeight(Double.valueOf(3.00d));

        final PostCodeGroupModel postCodeGroup = new PostCodeGroupModel();
        final List<PostCodeModel> postCodes = new ArrayList<>();
        final PostCodeModel postCodeModel = new PostCodeModel();
        postCodeModel.setPostCode(TEST_POST_CODE);
        postCodes.add(postCodeModel);
        postCodeGroup.setPostCodes(postCodes);
        postCodeGroups.add(postCodeGroup);
        carrierStrack.setPostCodeGroups(postCodeGroups);
        final TargetPostCodeAwareCarrierModel carrierAP = new TargetPostCodeAwareCarrierModel();
        carrierAP.setCode("AP");
        final List<TargetCarrierModel> carrierModels = new ArrayList<>();
        carrierModels.add(carrierStrack);
        carrierModels.add(carrierAP);

        Mockito.when(shippingAddress.getPostalcode()).thenReturn("1234");
        Mockito.when(targetCarrierDao.getApplicableCarriers(deliveryMode, false, false)).thenReturn(carrierModels);

        final TargetCarrierModel carrier = targetCarrierSelectionServiceImpl.getPreferredCarrier(consignment, false,
                false);
        Assert.assertNotNull(carrier);
        Assert.assertEquals("AP", carrier.getCode());
    }

    @Test
    public void testGetPreferredCarrierSupportedWeightButPostCodeGroup() {
        Mockito.when(consignment.getTotalWeight()).thenReturn(Double.valueOf(2.00d));

        final TargetPostCodeAwareCarrierModel carrierStrack = new TargetPostCodeAwareCarrierModel();
        carrierStrack.setCode("STRACK");
        carrierStrack.setMaxWeight(Double.valueOf(3.00d));
        carrierStrack.setPostCodeGroups(null);
        final TargetPostCodeAwareCarrierModel carrierAP = new TargetPostCodeAwareCarrierModel();
        carrierAP.setCode("AP");
        final List<TargetCarrierModel> carrierModels = new ArrayList<>();
        carrierModels.add(carrierStrack);
        carrierModels.add(carrierAP);

        Mockito.when(shippingAddress.getPostalcode()).thenReturn("1234");
        Mockito.when(targetCarrierDao.getApplicableCarriers(deliveryMode, false, false)).thenReturn(carrierModels);

        final TargetCarrierModel carrier = targetCarrierSelectionServiceImpl.getPreferredCarrier(consignment, false,
                false);
        Assert.assertNotNull(carrier);
        Assert.assertEquals("AP", carrier.getCode());
    }
}

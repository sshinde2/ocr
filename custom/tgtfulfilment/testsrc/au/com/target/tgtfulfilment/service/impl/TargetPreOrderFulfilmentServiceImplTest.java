package au.com.target.tgtfulfilment.service.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.ListUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.order.FluentErrorException;
import au.com.target.tgtcore.order.FluentOrderService;
import au.com.target.tgtcore.order.TargetPreOrderService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.data.OrderResponse;
import au.com.target.tgtfluent.data.StockDatum;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.service.FluentStockLookupService;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPreOrderFulfilmentServiceImplTest {

    private static final String DEFAULT_ONLINE_WAREHOUSE_LOCATION = "FASTLINE";

    @Mock
    private TargetPreOrderService targetPreOrderService;

    @Mock
    private FluentOrderService fluentOrderService;

    @Mock
    private FluentStockLookupService fluentStockLookupService;

    @Mock
    private TargetWarehouseService targetWarehouseService;

    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;

    @InjectMocks
    private final TargetPreOrderFulfilmentServiceImpl targetPreOrderFulfilmentServiceImpl = new TargetPreOrderFulfilmentServiceImpl();

    private final int daysToAdvance = 1;

    @Before
    public void setup() {
        targetPreOrderFulfilmentServiceImpl.setDaysToAdvance(daysToAdvance);
    }

    @Test
    public void processPreOrderFulfilmentWithNullOrder() throws FulfilmentException {
        final ArgumentCaptor<Date> date = ArgumentCaptor.forClass(Date.class);
        given(targetPreOrderService.getParkedPreOrdersForReleaseDate(date.capture(),
                eq(daysToAdvance))).willReturn(null);
        targetPreOrderFulfilmentServiceImpl.processPreOrderFulfilment();
        verifyZeroInteractions(fluentOrderService);
    }

    @Test
    public void processPreOrderFulfilmentWithEmptyOrder() throws FulfilmentException {
        final ArgumentCaptor<Date> date = ArgumentCaptor.forClass(Date.class);
        given(targetPreOrderService.getParkedPreOrdersForReleaseDate(date.capture(),
                eq(daysToAdvance))).willReturn(ListUtils.EMPTY_LIST);
        targetPreOrderFulfilmentServiceImpl.processPreOrderFulfilment();
        verifyZeroInteractions(fluentOrderService);
    }

    @Test
    public void processPreOrderFulfilmentWithFluentErrorException()
            throws FulfilmentException {
        final String fluentOrderId = "01234";
        final ArgumentCaptor<Date> date = ArgumentCaptor.forClass(Date.class);
        final OrderModel order = mock(OrderModel.class);
        given(targetPreOrderService.getParkedPreOrdersForReleaseDate(date.capture(),
                eq(daysToAdvance)))
                        .willReturn(Arrays.asList(order));
        given(order.getFluentId()).willReturn(fluentOrderId);
        given(fluentOrderService.getOrder(fluentOrderId)).willThrow(new FluentErrorException());
        targetPreOrderFulfilmentServiceImpl.processPreOrderFulfilment();
        verifyZeroInteractions(fluentStockLookupService);
    }

    @Test
    public void processPreOrderFulfilmentWithOrder()
            throws FulfilmentException {
        final String fluentOrderId = "01234";
        final ArgumentCaptor<Date> date = ArgumentCaptor.forClass(Date.class);
        final OrderModel order = mock(OrderModel.class);
        final OrderResponse orderResponse = mock(OrderResponse.class);
        given(targetPreOrderService.getParkedPreOrdersForReleaseDate(date.capture(),
                eq(daysToAdvance))).willReturn(Arrays.asList(order));
        given(order.getFluentId()).willReturn(fluentOrderId);
        given(fluentOrderService.getOrder(fluentOrderId)).willReturn(orderResponse);
        targetPreOrderFulfilmentServiceImpl.processPreOrderFulfilment();
        verify(fluentOrderService).getOrder(fluentOrderId);
    }

    @Test
    public void processPreOrderFulfilmentWithOrderWithEmptyEntries()
            throws FulfilmentException {
        final String fluentOrderId = "01234";
        final ArgumentCaptor<Date> date = ArgumentCaptor.forClass(Date.class);
        final OrderModel order = mock(OrderModel.class);
        final OrderResponse orderResponse = mock(OrderResponse.class);
        given(orderResponse.getStatus()).willReturn("PARKED");
        given(targetPreOrderService.getParkedPreOrdersForReleaseDate(date.capture(),
                eq(daysToAdvance))).willReturn(Arrays.asList(order));
        given(order.getFluentId()).willReturn(fluentOrderId);
        given(fluentOrderService.getOrder(fluentOrderId)).willReturn(orderResponse);
        targetPreOrderFulfilmentServiceImpl.processPreOrderFulfilment();
        verify(fluentOrderService).getOrder(fluentOrderId);
        verifyZeroInteractions(fluentStockLookupService);
    }

    @Test
    public void processPreOrderFulfilmentWithOrderWithStatusParked()
            throws FluentClientException {
        final String fluentOrderId = "01234";
        final String skuId = "sku123";
        final ArgumentCaptor<Date> date = ArgumentCaptor.forClass(Date.class);
        createDefaultWarehouse();
        final OrderModel order = createOrderModel(fluentOrderId, skuId);
        final OrderResponse orderResponse = mock(OrderResponse.class);
        given(orderResponse.getStatus()).willReturn("PARKED");
        given(targetPreOrderService.getParkedPreOrdersForReleaseDate(date.capture(),
                eq(daysToAdvance))).willReturn(Arrays.asList(order));
        given(fluentOrderService.getOrder(fluentOrderId)).willReturn(orderResponse);
        targetPreOrderFulfilmentServiceImpl.processPreOrderFulfilment();
        verify(fluentOrderService).getOrder(fluentOrderId);
        verify(fluentStockLookupService).lookupStock(Arrays.asList(skuId), null,
                Arrays.asList(DEFAULT_ONLINE_WAREHOUSE_LOCATION));
    }

    @Test
    public void processPreOrderFulfilmentWithOrderFluentClientExcetpion()
            throws FluentClientException {
        final String fluentOrderId = "01234";
        final String skuId = "sku123";
        final ArgumentCaptor<Date> date = ArgumentCaptor.forClass(Date.class);
        createDefaultWarehouse();
        final OrderModel order = createOrderModel(fluentOrderId, skuId);
        final OrderResponse orderResponse = mock(OrderResponse.class);
        given(orderResponse.getStatus()).willReturn("PARKED");
        given(targetPreOrderService.getParkedPreOrdersForReleaseDate(date.capture(),
                eq(daysToAdvance))).willReturn(Arrays.asList(order));
        given(fluentOrderService.getOrder(fluentOrderId)).willReturn(orderResponse);
        willThrow(new FluentClientException(new Error())).given(fluentStockLookupService).lookupStock(
                Arrays.asList(skuId), null,
                Arrays.asList(DEFAULT_ONLINE_WAREHOUSE_LOCATION));
        targetPreOrderFulfilmentServiceImpl.processPreOrderFulfilment();
        verify(fluentOrderService).getOrder(fluentOrderId);
        verify(fluentOrderService, never()).releaseOrder(order);
        verifyZeroInteractions(targetBusinessProcessService);
    }

    @Test
    public void processPreOrderFulfilmentWithOrderWithNoDefaultWarehouse()
            throws FluentClientException {
        final String fluentOrderId = "01234";
        final String skuId = "sku123";
        final ArgumentCaptor<Date> date = ArgumentCaptor.forClass(Date.class);
        given(targetWarehouseService.getDefaultOnlineWarehouse()).willReturn(null);

        final OrderModel order = createOrderModel(fluentOrderId, skuId);
        final OrderResponse orderResponse = mock(OrderResponse.class);
        given(orderResponse.getStatus()).willReturn("PARKED");
        given(targetPreOrderService.getParkedPreOrdersForReleaseDate(date.capture(),
                eq(daysToAdvance))).willReturn(Arrays.asList(order));
        given(fluentOrderService.getOrder(fluentOrderId)).willReturn(orderResponse);
        targetPreOrderFulfilmentServiceImpl.processPreOrderFulfilment();
        verify(fluentOrderService).getOrder(fluentOrderId);
        verify(fluentStockLookupService).lookupStock(Arrays.asList(skuId), null, Arrays.asList(""));
    }

    @Test
    public void processPreOrderFulfilmentWithOrderWithStatusPending()
            throws FulfilmentException {
        final String fluentOrderId = "01234";
        final String skuId = "sku123";
        final ArgumentCaptor<Date> date = ArgumentCaptor.forClass(Date.class);

        createDefaultWarehouse();
        final OrderModel order = createOrderModel(fluentOrderId, skuId);
        final OrderResponse orderResponse = mock(OrderResponse.class);
        given(orderResponse.getStatus()).willReturn("PENDING");
        given(targetPreOrderService.getParkedPreOrdersForReleaseDate(date.capture(),
                eq(daysToAdvance))).willReturn(Arrays.asList(order));
        given(fluentOrderService.getOrder(fluentOrderId)).willReturn(orderResponse);
        targetPreOrderFulfilmentServiceImpl.processPreOrderFulfilment();
        verify(fluentOrderService).getOrder(fluentOrderId);
        verify(targetBusinessProcessService).startFluentReleasePreOrderProcess(order);
    }

    @Test
    public void processPreOrderFulfilment()
            throws FluentClientException {
        final String fluentOrderId = "01234";
        final String skuId = "sku123";
        final ArgumentCaptor<Date> date = ArgumentCaptor.forClass(Date.class);

        createDefaultWarehouse();
        final OrderModel order = createOrderModel(fluentOrderId, skuId);
        final OrderResponse orderResponse = mock(OrderResponse.class);
        final StockDatum stockDatum = mock(StockDatum.class);
        final Map<String, Integer> storeSoh = new HashMap<>();
        storeSoh.put(DEFAULT_ONLINE_WAREHOUSE_LOCATION, Integer.valueOf(100));
        given(stockDatum.getStoreSohQty()).willReturn(storeSoh);
        given(orderResponse.getStatus()).willReturn("PARKED");
        given(targetPreOrderService.getParkedPreOrdersForReleaseDate(date.capture(),
                eq(daysToAdvance)))
                        .willReturn(Arrays.asList(order));
        given(fluentOrderService.getOrder(fluentOrderId)).willReturn(orderResponse);
        given(fluentStockLookupService.lookupStock(Arrays.asList(skuId), null,
                Arrays.asList(DEFAULT_ONLINE_WAREHOUSE_LOCATION))).willReturn(Arrays.asList(stockDatum));
        willReturn(Boolean.TRUE).given(fluentOrderService).releaseOrder(order);
        targetPreOrderFulfilmentServiceImpl.processPreOrderFulfilment();
        verify(fluentOrderService).getOrder(fluentOrderId);
        verify(fluentStockLookupService).lookupStock(Arrays.asList(skuId), null,
                Arrays.asList(DEFAULT_ONLINE_WAREHOUSE_LOCATION));
        verify(targetBusinessProcessService).startFluentReleasePreOrderProcess(order);
    }

    @Test
    public void processPreOrderFulfilmentInsufficentStock()
            throws FluentClientException {
        final String fluentOrderId = "01234";
        final String skuId = "sku123";
        final ArgumentCaptor<Date> date = ArgumentCaptor.forClass(Date.class);

        createDefaultWarehouse();
        final OrderModel order = createOrderModel(fluentOrderId, skuId);
        final OrderResponse orderResponse = mock(OrderResponse.class);
        final StockDatum stockDatum = mock(StockDatum.class);
        final Map<String, Integer> storeSoh = new HashMap<>();
        storeSoh.put(DEFAULT_ONLINE_WAREHOUSE_LOCATION, Integer.valueOf(1));
        given(stockDatum.getStoreSohQty()).willReturn(storeSoh);
        given(orderResponse.getStatus()).willReturn("PARKED");
        given(targetPreOrderService.getParkedPreOrdersForReleaseDate(date.capture(),
                eq(daysToAdvance)))
                        .willReturn(Arrays.asList(order));
        given(fluentOrderService.getOrder(fluentOrderId)).willReturn(orderResponse);
        given(fluentStockLookupService.lookupStock(Arrays.asList(skuId), null,
                Arrays.asList(DEFAULT_ONLINE_WAREHOUSE_LOCATION))).willReturn(Arrays.asList(stockDatum));
        willReturn(Boolean.TRUE).given(fluentOrderService).releaseOrder(order);
        targetPreOrderFulfilmentServiceImpl.processPreOrderFulfilment();
        verify(fluentOrderService).getOrder(fluentOrderId);
        verify(fluentStockLookupService).lookupStock(Arrays.asList(skuId), null,
                Arrays.asList(DEFAULT_ONLINE_WAREHOUSE_LOCATION));
        verify(fluentOrderService, never()).releaseOrder(order);
        verifyZeroInteractions(targetBusinessProcessService);
    }

    @Test
    public void processPreOrderFulfilmentNoStock()
            throws FulfilmentException, FluentClientException {
        createDefaultWarehouse();

        final List<OrderModel> orders = createOrderModelList();

        final StockDatum stockDatum = mock(StockDatum.class);
        final Map<String, Integer> storeSoh = new HashMap<>();
        storeSoh.put(DEFAULT_ONLINE_WAREHOUSE_LOCATION, Integer.valueOf(0));
        given(stockDatum.getStoreSohQty()).willReturn(storeSoh);

        final List<AbstractOrderEntryModel> entries1 = orders.get(0).getEntries();
        final List<AbstractOrderEntryModel> entries2 = orders.get(1).getEntries();
        final List<AbstractOrderEntryModel> entries3 = orders.get(2).getEntries();

        final AbstractOrderEntryModel entry1 = entries1.get(0);
        final AbstractOrderEntryModel entry2 = entries2.get(0);
        final AbstractOrderEntryModel entry3 = entries3.get(0);

        given(fluentStockLookupService.lookupStock(Arrays.asList(entry1.getProduct().getCode()), null,
                Arrays.asList(DEFAULT_ONLINE_WAREHOUSE_LOCATION))).willReturn(Arrays.asList(stockDatum));
        given(fluentStockLookupService.lookupStock(Arrays.asList(entry2.getProduct().getCode()), null,
                Arrays.asList(DEFAULT_ONLINE_WAREHOUSE_LOCATION))).willReturn(Arrays.asList(stockDatum));
        given(fluentStockLookupService.lookupStock(Arrays.asList(entry3.getProduct().getCode()), null,
                Arrays.asList(DEFAULT_ONLINE_WAREHOUSE_LOCATION))).willReturn(Arrays.asList(stockDatum));

        targetPreOrderFulfilmentServiceImpl.processPreOrderFulfilment();

        verify(fluentOrderService).getOrder(orders.get(0).getFluentId());
        verify(fluentOrderService).getOrder(orders.get(1).getFluentId());
        verify(fluentOrderService).getOrder(orders.get(2).getFluentId());

        verify(fluentStockLookupService).lookupStock(Arrays.asList(entry1.getProduct().getCode()), null,
                Arrays.asList(DEFAULT_ONLINE_WAREHOUSE_LOCATION));
        verify(fluentStockLookupService).lookupStock(Arrays.asList(entry2.getProduct().getCode()), null,
                Arrays.asList(DEFAULT_ONLINE_WAREHOUSE_LOCATION));
        verify(fluentStockLookupService).lookupStock(Arrays.asList(entry3.getProduct().getCode()), null,
                Arrays.asList(DEFAULT_ONLINE_WAREHOUSE_LOCATION));

        verifyZeroInteractions(targetBusinessProcessService);

    }

    @Test
    public void processPreOrderFulfilmentNoAvailableStoreSoh()
            throws FluentClientException {
        final String fluentOrderId = "01234";
        final String skuId = "sku123";
        final ArgumentCaptor<Date> date = ArgumentCaptor.forClass(Date.class);

        createDefaultWarehouse();
        final OrderModel order = createOrderModel(fluentOrderId, skuId);
        final OrderResponse orderResponse = mock(OrderResponse.class);
        final StockDatum stockDatum = mock(StockDatum.class);
        final Map<String, Integer> storeSoh = new HashMap<>();
        storeSoh.put("5001", Integer.valueOf(1));
        given(stockDatum.getStoreSohQty()).willReturn(storeSoh);
        given(orderResponse.getStatus()).willReturn("PARKED");
        given(targetPreOrderService.getParkedPreOrdersForReleaseDate(date.capture(),
                eq(daysToAdvance)))
                        .willReturn(Arrays.asList(order));
        given(fluentOrderService.getOrder(fluentOrderId)).willReturn(orderResponse);
        given(fluentStockLookupService.lookupStock(Arrays.asList(skuId), null,
                Arrays.asList(DEFAULT_ONLINE_WAREHOUSE_LOCATION))).willReturn(Arrays.asList(stockDatum));
        willReturn(Boolean.TRUE).given(fluentOrderService).releaseOrder(order);
        targetPreOrderFulfilmentServiceImpl.processPreOrderFulfilment();
        verify(fluentOrderService).getOrder(fluentOrderId);
        verify(fluentStockLookupService).lookupStock(Arrays.asList(skuId), null,
                Arrays.asList(DEFAULT_ONLINE_WAREHOUSE_LOCATION));
        verify(targetBusinessProcessService, never()).startFluentReleasePreOrderProcess(order);
    }

    @Test
    public void processPreOrderFulfilmentWithNullOrderResponse()
            throws FluentClientException {
        final String fluentOrderId = "01234";
        final String skuId = "sku123";
        final ArgumentCaptor<Date> date = ArgumentCaptor.forClass(Date.class);
        createDefaultWarehouse();
        final OrderModel order = createOrderModel(fluentOrderId, skuId);
        given(targetPreOrderService.getParkedPreOrdersForReleaseDate(date.capture(),
                eq(daysToAdvance))).willReturn(Arrays.asList(order));
        given(fluentOrderService.getOrder(fluentOrderId)).willReturn(null);
        targetPreOrderFulfilmentServiceImpl.processPreOrderFulfilment();
        verify(fluentOrderService).getOrder(fluentOrderId);
        verifyZeroInteractions(fluentStockLookupService);
        verify(fluentOrderService, never()).releaseOrder(order);
        verifyZeroInteractions(targetBusinessProcessService);
    }

    private void createDefaultWarehouse() {
        final WarehouseModel warehouse = mock(WarehouseModel.class);
        given(targetWarehouseService.getDefaultOnlineWarehouse()).willReturn(warehouse);
        given(warehouse.getFluentLocationRef()).willReturn(DEFAULT_ONLINE_WAREHOUSE_LOCATION);
    }

    private OrderModel createOrderModel(final String fluentOrderId, final String skuId) {
        final OrderModel order = mock(OrderModel.class);
        final AbstractOrderEntryModel entry = mock(AbstractOrderEntryModel.class);
        final ProductModel product = mock(ProductModel.class);
        given(order.getFluentId()).willReturn(fluentOrderId);
        given(order.getEntries()).willReturn(Arrays.asList(entry));
        given(entry.getProduct()).willReturn(product);
        given(entry.getQuantity()).willReturn(Long.valueOf(2));
        given(product.getCode()).willReturn(skuId);
        return order;
    }

    private List<OrderModel> createOrderModelList() {
        final ArgumentCaptor<Date> date = ArgumentCaptor.forClass(Date.class);
        final OrderModel order1 = mock(OrderModel.class);
        final OrderModel order2 = mock(OrderModel.class);
        final OrderModel order3 = mock(OrderModel.class);

        final AbstractOrderEntryModel entry1 = mock(AbstractOrderEntryModel.class);
        final AbstractOrderEntryModel entry2 = mock(AbstractOrderEntryModel.class);
        final AbstractOrderEntryModel entry3 = mock(AbstractOrderEntryModel.class);

        final ProductModel product1 = mock(ProductModel.class);
        final ProductModel product2 = mock(ProductModel.class);
        final ProductModel product3 = mock(ProductModel.class);

        given(product1.getCode()).willReturn("sku123");
        given(entry1.getProduct()).willReturn(product1);
        given(entry1.getQuantity()).willReturn(Long.valueOf(2));
        given(order1.getEntries()).willReturn(Arrays.asList(entry1));
        given(order1.getFluentId()).willReturn("01233");

        given(product2.getCode()).willReturn("sku123");
        given(entry2.getProduct()).willReturn(product2);
        given(entry2.getQuantity()).willReturn(Long.valueOf(2));
        given(order2.getEntries()).willReturn(Arrays.asList(entry2));
        given(order2.getFluentId()).willReturn("01234");

        given(product3.getCode()).willReturn("sku124");
        given(entry3.getProduct()).willReturn(product3);
        given(entry3.getQuantity()).willReturn(Long.valueOf(2));
        given(order3.getEntries()).willReturn(Arrays.asList(entry3));
        given(order3.getFluentId()).willReturn("01235");

        final OrderResponse orderResponse = mock(OrderResponse.class);
        final List<OrderModel> orderList = new ArrayList<>(Arrays.asList(order1, order2, order3));
        given(orderResponse.getStatus()).willReturn("PARKED");

        given(fluentOrderService.getOrder(order1.getFluentId())).willReturn(orderResponse);
        given(fluentOrderService.getOrder(order2.getFluentId())).willReturn(orderResponse);
        given(fluentOrderService.getOrder(order3.getFluentId())).willReturn(orderResponse);

        given(targetPreOrderService.getParkedPreOrdersForReleaseDate(date.capture(),
                eq(daysToAdvance)))
                        .willReturn(orderList);
        return orderList;
    }

}

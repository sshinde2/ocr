/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.dao.TargetPointOfServiceDao;
import au.com.target.tgtfulfilment.dao.TargetConsignmentDao;
import au.com.target.tgtfulfilment.dto.TargetConsignmentPageResult;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.service.TargetStoreFulfilmentCapabilitiesService;


/**
 * @author smudumba
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetStoreConsignmentServiceTest {

    class MyTargetStoreConsignmentServiceImpl extends TargetStoreConsignmentServiceImpl {

        @Override
        protected Date getCurrentDate() {
            // return 11am of 1-Jan
            final Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.MONTH, 0);
            calendar.set(Calendar.DAY_OF_YEAR, 1);
            calendar.set(Calendar.HOUR_OF_DAY, 11);
            return calendar.getTime();
        }
    }

    @Mock
    private TargetConsignmentDao mockTargetConsignmentDao;

    @Mock
    private TargetConsignmentModel mockConsignment;

    @Mock
    private WarehouseModel mockWarehouse;

    @Mock
    private ConsignmentStatus consignmentStatus;

    @Mock
    private TargetConsignmentPageResult mockTargetConsignmentPageResult;

    @Mock
    private TargetPointOfServiceModel store;

    @Mock
    private TargetPointOfServiceDao targetPointOfServiceDao;

    @Mock
    private StoreFulfilmentCapabilitiesModel capabilitiesModel;

    @Mock
    private TargetStoreFulfilmentCapabilitiesService storeFulfilmentCapabilitiesService;

    @Mock
    private AbstractOrderModel mockOrder;

    @Mock
    private AddressModel mockStoreAddress;

    private final int storeId = 100;

    private final String mockStoreState = "VIC";

    private final String mockOrderCode = "98745642";

    @InjectMocks
    private final TargetStoreConsignmentServiceImpl targetStoreConsignmentService = new MyTargetStoreConsignmentServiceImpl();

    private List<TargetConsignmentModel> consignments;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        // Default to order with one consignment
        consignments = new ArrayList<>();
        when(mockTargetConsignmentDao.getConsignmentsForStore(any(Integer.class), anyInt(),
                anyInt(), anyInt(), any(String.class), anyList(), any(String.class), any(String.class)))
                        .thenReturn(
                                mockTargetConsignmentPageResult);
        when(mockTargetConsignmentPageResult.getConsignments()).thenReturn(
                consignments);
        when(mockTargetConsignmentDao.getConsignmentsForStore(any(Integer.class))).thenReturn(
                consignments);
        when(mockTargetConsignmentDao.getConsignmentsByCodes(anyList())).thenReturn(
                consignments);

        // Default consignment has mockWarehouse assigned to the store
        when(mockConsignment.getWarehouse()).thenReturn(mockWarehouse);
        given(mockConsignment.getOrder()).willReturn(mockOrder);
        given(mockOrder.getCode()).willReturn(mockOrderCode);

        final List<PointOfServiceModel> pointsOfServices = new ArrayList<>();
        pointsOfServices.add(store);
        when(mockWarehouse.getPointsOfService()).thenReturn(pointsOfServices);

        when(targetPointOfServiceDao.getAllFulfilmentEnabledPOS()).thenReturn(
                Collections.singletonList(store));
        when(capabilitiesModel.getEnabled()).thenReturn(Boolean.TRUE);
        when(store.getFulfilmentCapability()).thenReturn(capabilitiesModel);
        when(store.getStoreNumber()).thenReturn(Integer.valueOf(storeId));
        given(mockStoreAddress.getDistrict()).willReturn(mockStoreState);
        given(store.getAddress()).willReturn(mockStoreAddress);
        when(capabilitiesModel.getMobileNumber()).thenReturn("41234567898");
        when(
                Boolean.valueOf(storeFulfilmentCapabilitiesService.isTargetStoreBlackoutExist(store)))
                        .thenReturn(Boolean.FALSE);
    }

    @Test
    public void testGetPosAndOpenOrdersForStoresWhenBlackoutNotExists() {
        final Date currDate = targetStoreConsignmentService.getCurrentDate();
        final Date notificationStartDate = DateUtils.addDays(currDate, -2);
        final Date notificationEndDate = DateUtils.addDays(currDate, +2);
        when(capabilitiesModel.getNotificationStartTime()).thenReturn(
                DateUtils.addHours(notificationStartDate, -2));
        when(capabilitiesModel.getNotificationEndTime()).thenReturn(DateUtils.addHours(notificationEndDate, 2));
        when(capabilitiesModel.getNotificationsEnabled()).thenReturn(Boolean.TRUE);

        when(mockTargetConsignmentDao.getOpenConsignmentsForStore(store)).thenReturn(
                Collections.singletonList(mockConsignment));

        final Map<TargetPointOfServiceModel, Integer> smsNotifications = targetStoreConsignmentService
                .getPosAndOpenOrdersForStores();
        assertThat(smsNotifications).isNotNull();
        assertThat(smsNotifications.containsKey(store)).isTrue();
        assertThat(smsNotifications.containsValue(Integer.valueOf(1))).isTrue();
    }

    @Test
    public void testGetPosAndOpenOrdersForStoresWhenBlackoutExists() {
        final Date currDate = new Date();
        final Date notificationStartDate = DateUtils.addDays(currDate, -2);
        final Date notificationEndDate = DateUtils.addDays(currDate, +2);
        when(
                Boolean.valueOf(storeFulfilmentCapabilitiesService.isTargetStoreBlackoutExist(store)))
                        .thenReturn(Boolean.TRUE);
        when(capabilitiesModel.getNotificationStartTime()).thenReturn(notificationStartDate);
        when(capabilitiesModel.getNotificationEndTime()).thenReturn(notificationEndDate);
        when(mockTargetConsignmentDao.getOpenConsignmentsForStore(store)).thenReturn(
                Collections.singletonList(mockConsignment));
        final Map<TargetPointOfServiceModel, Integer> smsNotifications = targetStoreConsignmentService
                .getPosAndOpenOrdersForStores();
        assertThat(smsNotifications).isEmpty();
    }

    @Test
    public void testGetPosAndOpenOrdersForStoresWhenMobileNumberNotExists() {
        final Date currDate = new Date();
        final Date notificationStartDate = DateUtils.addDays(currDate, -2);
        final Date notificationEndDate = DateUtils.addDays(currDate, +2);

        when(capabilitiesModel.getMobileNumber()).thenReturn(null);
        when(capabilitiesModel.getNotificationStartTime()).thenReturn(notificationStartDate);
        when(capabilitiesModel.getNotificationEndTime()).thenReturn(notificationEndDate);
        when(mockTargetConsignmentDao.getOpenConsignmentsForStore(store)).thenReturn(
                Collections.singletonList(mockConsignment));

        final Map<TargetPointOfServiceModel, Integer> smsNotifications = targetStoreConsignmentService
                .getPosAndOpenOrdersForStores();
        assertThat(smsNotifications).isEmpty();
    }

    @Test
    public void testGetPosAndOpenOrdersForStoresWhenNotificationNotEnabled() {
        final Date currDate = new Date();
        final Date notificationStartDate = DateUtils.addDays(currDate, -2);
        final Date notificationEndDate = DateUtils.addDays(currDate, -1);

        when(capabilitiesModel.getNotificationStartTime()).thenReturn(notificationStartDate);
        when(capabilitiesModel.getNotificationEndTime()).thenReturn(notificationEndDate);

        when(mockTargetConsignmentDao.getOpenConsignmentsForStore(store)).thenReturn(
                Collections.singletonList(mockConsignment));

        final Map<TargetPointOfServiceModel, Integer> smsNotifications = targetStoreConsignmentService
                .getPosAndOpenOrdersForStores();
        assertThat(smsNotifications).isEmpty();
    }

    @Test
    public void testGetPosAndOpenOrdersForStoresWhenNotificationEnabledIsNull() {
        final Date currDate = targetStoreConsignmentService.getCurrentDate();
        final Date notificationStartDate = DateUtils.addDays(currDate, -2);
        final Date notificationEndDate = DateUtils.addDays(currDate, +2);
        when(capabilitiesModel.getNotificationStartTime()).thenReturn(
                DateUtils.addHours(notificationStartDate, -2));
        when(capabilitiesModel.getNotificationEndTime()).thenReturn(DateUtils.addHours(notificationEndDate, 2));
        when(capabilitiesModel.getNotificationsEnabled()).thenReturn(null);

        when(mockTargetConsignmentDao.getOpenConsignmentsForStore(store)).thenReturn(
                Collections.singletonList(mockConsignment));

        final Map<TargetPointOfServiceModel, Integer> smsNotifications = targetStoreConsignmentService
                .getPosAndOpenOrdersForStores();
        assertThat(smsNotifications).isEmpty();
    }

    @Test
    public void testGetPosAndOpenOrdersForStoresWhenNotificationEnabledFalse() {
        final Date currDate = targetStoreConsignmentService.getCurrentDate();
        final Date notificationStartDate = DateUtils.addDays(currDate, -2);
        final Date notificationEndDate = DateUtils.addDays(currDate, +2);
        when(capabilitiesModel.getNotificationStartTime()).thenReturn(
                DateUtils.addHours(notificationStartDate, -2));
        when(capabilitiesModel.getNotificationEndTime()).thenReturn(DateUtils.addHours(notificationEndDate, 2));
        when(capabilitiesModel.getNotificationsEnabled()).thenReturn(Boolean.FALSE);

        when(mockTargetConsignmentDao.getOpenConsignmentsForStore(store)).thenReturn(
                Collections.singletonList(mockConsignment));

        final Map<TargetPointOfServiceModel, Integer> smsNotifications = targetStoreConsignmentService
                .getPosAndOpenOrdersForStores();
        assertThat(smsNotifications).isEmpty();
    }

    /**
     * fromDate - 10days past | toDate - 2days past | time in valid period
     */
    @Test
    public void testIsStoreInNotificationEnabledPeriodValidTimePastDates() {
        final Date currDate = targetStoreConsignmentService.getCurrentDate();
        final Date fromDate = DateUtils.addDays(currDate, -10);
        final Date toDate = DateUtils.addDays(currDate, -2);

        assertThat(targetStoreConsignmentService.isStoreInNotificationEnabledPeriod(
                DateUtils.addHours(fromDate, -5), DateUtils.addHours(toDate, 5))).isTrue();
    }

    /**
     * fromDate - 10days past | toDate - 2days future | time in valid period
     */
    @Test
    public void testIsStoreInNotificationEnabledPeriodValidTimeCurrentDates() {
        final Date currDate = targetStoreConsignmentService.getCurrentDate();
        final Date fromDate = DateUtils.addDays(currDate, -10);
        final Date toDate = DateUtils.addDays(currDate, 2);

        assertThat(targetStoreConsignmentService.isStoreInNotificationEnabledPeriod(
                DateUtils.addHours(fromDate, -5), DateUtils.addHours(toDate, 5))).isTrue();
    }

    /**
     * fromDate - 2days future | toDate - 10days future | time in valid period
     */
    @Test
    public void testIsStoreInNotificationEnabledPeriodValidTimeFutureDates() {
        final Date currDate = targetStoreConsignmentService.getCurrentDate();
        final Date fromDate = DateUtils.addDays(currDate, 2);
        final Date toDate = DateUtils.addDays(currDate, 10);

        assertThat(targetStoreConsignmentService.isStoreInNotificationEnabledPeriod(
                DateUtils.addHours(fromDate, -5), DateUtils.addHours(toDate, 5))).isTrue();
    }

    /**
     * fromDate - 10days past | toDate - 2days past | time in invalid period
     */
    @Test
    public void testIsStoreInNotificationEnabledPeriodInvalidTimePastDates() {
        final Date currDate = targetStoreConsignmentService.getCurrentDate();
        final Date fromDate = DateUtils.addDays(currDate, -10);
        final Date toDate = DateUtils.addDays(currDate, -2);

        assertThat(targetStoreConsignmentService.isStoreInNotificationEnabledPeriod(
                DateUtils.addHours(fromDate, -5), DateUtils.addHours(toDate, -1))).isFalse();
    }

    /**
     * fromDate - 10days past | toDate - 2days future | time in invalid period
     */
    @Test
    public void testIsStoreInNotificationEnabledPeriodInvalidTimeCurrentDates() {
        final Date currDate = targetStoreConsignmentService.getCurrentDate();
        final Date fromDate = DateUtils.addDays(currDate, -10);
        final Date toDate = DateUtils.addDays(currDate, 2);

        assertThat(targetStoreConsignmentService.isStoreInNotificationEnabledPeriod(
                DateUtils.addHours(fromDate, -5), DateUtils.addHours(toDate, -1))).isFalse();
    }

    /**
     * fromDate - 2days future | toDate - 10days future | time in invalid period
     */
    @Test
    public void testIsStoreInNotificationEnabledPeriodInvalidTimeFutureDates() {
        final Date currDate = targetStoreConsignmentService.getCurrentDate();
        final Date fromDate = DateUtils.addDays(currDate, 2);
        final Date toDate = DateUtils.addDays(currDate, 10);

        assertThat(targetStoreConsignmentService.isStoreInNotificationEnabledPeriod(
                DateUtils.addHours(fromDate, 1), DateUtils.addHours(toDate, 5))).isFalse();
    }

    /**
     * Daylight saving test fromDate - 1-Jan 9am | toDate - 5-Jan 6pm | time in valid summer period
     */
    @Test
    public void testIsStoreInNotificationEnabledDaylightSameZone() {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 9);

        final Date fromDate = calendar.getTime();

        calendar.set(Calendar.DAY_OF_YEAR, 5);
        calendar.set(Calendar.HOUR_OF_DAY, 18);
        final Date toDate = calendar.getTime();

        assertThat(targetStoreConsignmentService.isStoreInNotificationEnabledPeriod(
                DateUtils.addHours(fromDate, 1), DateUtils.addHours(toDate, 5))).isTrue();
    }

    /**
     * Daylight saving test fromDate - 1-Jun 9am | toDate - 5-Jun 6pm | time in valid summer period
     */
    @Test
    public void testIsStoreInNotificationEnabledDaylightDifferentZone() {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, 5);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 9);

        final Date fromDate = calendar.getTime();

        calendar.set(Calendar.DAY_OF_YEAR, 5);
        calendar.set(Calendar.HOUR_OF_DAY, 18);
        final Date toDate = calendar.getTime();

        assertThat(targetStoreConsignmentService.isStoreInNotificationEnabledPeriod(
                DateUtils.addHours(fromDate, 1), DateUtils.addHours(toDate, 5))).isTrue();
    }

    @Test
    public void testGetConsignmentsForStoreWithConsignments() {

        final List<TargetConsignmentModel> result = targetStoreConsignmentService.getConsignmentsForStore(Integer
                .valueOf(1234));
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(consignments);
    }

    @Test
    public void testGetConsignmentsForStoreWithConsignmentsWithPagination() {

        final TargetConsignmentPageResult pageResult = targetStoreConsignmentService.getConsignmentsForStore(Integer
                .valueOf(1234), 0, 2, 60, null, null, null, null);
        final List<TargetConsignmentModel> result = pageResult.getConsignments();
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(consignments);
    }

    @Test
    public void testGetConsignmentsForStoreWithNoConsignments() {

        final List<TargetConsignmentModel> result = targetStoreConsignmentService.getConsignmentsForStore(Integer
                .valueOf(1234));
        assertThat(result).isNotNull();
        assertThat(result).isEmpty();
    }

    @Test
    public void testGetConsignmentsForStoreWithNoConsignmentsWithPagination() {

        final TargetConsignmentPageResult pageResult = targetStoreConsignmentService.getConsignmentsForStore(Integer
                .valueOf(1234), 0, 2, 60, null, null, null, null);
        final List<TargetConsignmentModel> result = pageResult.getConsignments();
        assertThat(result).isNotNull();
        assertThat(result).isEmpty();
    }

    @Test
    public void testGetConsignmentsForStoreWithNullStoreNumber() {
        final List<TargetConsignmentModel> result = targetStoreConsignmentService.getConsignmentsForStore(null);
        assertThat(result).isNull();
        assertThat(result).isNull();
    }

    @Test
    public void testGetConsignmentsForStoreWithNullStoreNumberWithPagination() {
        final TargetConsignmentPageResult pageResult = targetStoreConsignmentService
                .getConsignmentsForStore(null, 0, 0, 60, null, null, null, null);

        assertThat(pageResult).isNull();
    }

    @Test
    public void testGetConsignmentsForCodesWithConsignments() {

        consignments.add(mockConsignment);
        final TargetConsignmentPageResult pageResult = targetStoreConsignmentService.getConsignmentsForStore(Integer
                .valueOf(1234), 0, 0, 60, null, null, null, null);
        final List<TargetConsignmentModel> result = pageResult.getConsignments();
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(consignments);
        assertThat(result).hasSize(1);
    }

    @Test
    public void testGetConsignmentsByConsigmentCodes() {
        final List<String> consignmentCodes = new ArrayList<>();
        consignmentCodes.add("a12345");

        consignments.add(mockConsignment);
        when(mockTargetConsignmentDao.getConsignmentsByCodes(anyList())).thenReturn(
                consignments);
        final List<TargetConsignmentModel> result = targetStoreConsignmentService
                .getConsignmentsByCodes(consignmentCodes);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(consignments);
    }

    @Test
    public void testGetConsignmentsByConsigmentCodesWithNullList() {

        final List<TargetConsignmentModel> result = targetStoreConsignmentService
                .getConsignmentsByCodes(null);
        assertThat(result).isNull();
        verifyZeroInteractions(mockTargetConsignmentDao);
    }

    @Test
    public void testGetConsignmentsByConsigmentCodesWithEmptyList() {

        final List<TargetConsignmentModel> result = targetStoreConsignmentService
                .getConsignmentsByCodes(ListUtils.EMPTY_LIST);
        assertThat(result).isNull();
        verifyZeroInteractions(mockTargetConsignmentDao);
    }

    @Test
    public void testIsConsignmentAssignedToStoreNullWarehouse() {
        when(mockConsignment.getWarehouse()).thenReturn(null);
        final boolean isStoreConsignment = targetStoreConsignmentService
                .isConsignmentAssignedToAnyStore(mockConsignment);
        assertThat(isStoreConsignment).isFalse();
    }

    @Test
    public void testIsConsignmentAssignedToStoreEmptyPointOfService() {

        when(mockWarehouse.getPointsOfService()).thenReturn(CollectionUtils.EMPTY_COLLECTION);
        final boolean isStoreConsignment = targetStoreConsignmentService
                .isConsignmentAssignedToAnyStore(mockConsignment);
        assertThat(isStoreConsignment).isFalse();
    }

    @Test
    public void testIsConsignmentAssignedToStoreNullPointOfService() {

        when(mockWarehouse.getPointsOfService()).thenReturn(null);
        final boolean isStoreConsignment = targetStoreConsignmentService
                .isConsignmentAssignedToAnyStore(mockConsignment);
        assertThat(isStoreConsignment).isFalse();
    }

    @Test
    public void testIsConsignmentAssignedToStoreInvalidType() {
        when(store.getType()).thenReturn(PointOfServiceTypeEnum.WAREHOUSE);
        when(mockWarehouse.getPointsOfService()).thenReturn(null);
        final boolean isStoreConsignment = targetStoreConsignmentService
                .isConsignmentAssignedToAnyStore(mockConsignment);
        assertThat(isStoreConsignment).isFalse();
    }

    @Test
    public void testIsConsignmentAssignedToStore() {

        final boolean isStoreConsignment = targetStoreConsignmentService
                .isConsignmentAssignedToAnyStore(mockConsignment);
        assertThat(isStoreConsignment).isTrue();
    }

    @Test
    public void testIsConsignmentAssignedToNTL() {
        when(mockConsignment.getWarehouse()).thenReturn(mockWarehouse);
        when(store.getType()).thenReturn(PointOfServiceTypeEnum.WAREHOUSE);
        final List<PointOfServiceModel> pointsOfServices = new ArrayList<>();
        pointsOfServices.add(store);
        final boolean isNTLeConsignment = targetStoreConsignmentService.isConsignmentAssignedToAnyNTL(mockConsignment);
        assertThat(isNTLeConsignment).isTrue();
    }

    @Test
    public void testIsConsignmentAssignedToNTLNullWarehouse() {
        when(mockConsignment.getWarehouse()).thenReturn(null);
        final boolean isNTLeConsignment = targetStoreConsignmentService
                .isConsignmentAssignedToAnyNTL(mockConsignment);
        assertThat(isNTLeConsignment).isFalse();
    }

    @Test
    public void testIsConsignmentAssignedToNTLNullPointOfService() {
        when(mockWarehouse.getPointsOfService()).thenReturn(null);
        final boolean isNTLeConsignment = targetStoreConsignmentService
                .isConsignmentAssignedToAnyNTL(mockConsignment);
        assertThat(isNTLeConsignment).isFalse();
    }

    @Test
    public void testIsConsignmentAssignedToNTLInvalidType() {
        when(mockConsignment.getWarehouse()).thenReturn(mockWarehouse);
        when(store.getType()).thenReturn(PointOfServiceTypeEnum.TARGET);
        final List<PointOfServiceModel> pointsOfServices = new ArrayList<>();
        pointsOfServices.add(store);
        final boolean isNTLeConsignment = targetStoreConsignmentService.isConsignmentAssignedToAnyNTL(mockConsignment);
        assertThat(isNTLeConsignment).isFalse();
    }

    @Test
    public void testGetConsignmentsForCodesNullCode() {

        final List<TargetConsignmentModel> result = targetStoreConsignmentService
                .getConsignmentsByCodes(null);
        assertThat(result).isNull();
    }

    @Test
    public void testGetConsignmentsForCodesNoneFound() {

        final List<TargetConsignmentModel> result = targetStoreConsignmentService
                .getConsignmentsByCodes(Collections
                        .singletonList("1234"));
        assertThat(result).isNotNull();
        assertThat(result).isEmpty();
    }

    @Test
    public void testGetInstoreConsignmentsByStatus() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        when(mockTargetConsignmentDao.getConsignmentsUnmanifestedInStore(consignmentStatus, 2126))
                .thenReturn(
                        consignments);
        final List<TargetConsignmentModel> result = targetStoreConsignmentService.getConsignmentsNotManifestedForStore(
                2126);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(consignments);
    }

    @Test
    public void testGetInstoreConsignmentsByStatusEmptyList() {
        when(mockTargetConsignmentDao.getConsignmentsUnmanifestedInStore(consignmentStatus, 2126))
                .thenReturn(
                        Collections.EMPTY_LIST);
        final List<TargetConsignmentModel> result = targetStoreConsignmentService.getConsignmentsNotManifestedForStore(
                2126);
        assertThat(result).isEmpty();
    }

    /**
     * Test get consignments by status for all store, When no consignment found it will return empty List
     */
    @Test
    public void testGetConsignmentsByStatusForAllStoreNoConsignmentFound() {

        final List<TargetConsignmentModel> consignmentlist = Collections.emptyList();
        when(mockTargetConsignmentDao.getConsignmentsByStatusForAllStore(any(ConsignmentStatus.class)))
                .thenReturn(consignmentlist);

        assertThat(Collections.emptyList()).isEqualTo(
                targetStoreConsignmentService.getConsignmentsForAllStore(any(ConsignmentStatus.class)));
    }

    /**
     * Test get consignments by status for all store. will return List of TargetConsignemntModel
     */
    @Test
    public void testGetConsignmentsByStatusForAllStore() {

        final List<TargetConsignmentModel> consignmentlist = Arrays.asList(mock(TargetConsignmentModel.class));
        when(mockTargetConsignmentDao.getConsignmentsByStatusForAllStore(any(ConsignmentStatus.class)))
                .thenReturn(consignmentlist);

        assertThat(consignmentlist).isEqualTo(
                targetStoreConsignmentService.getConsignmentsForAllStore(any(ConsignmentStatus.class)));

    }

    /**
     * Test get consignments by status for stores in given states, When no consignment found it will return empty List
     */
    @Test
    public void testGetConsignmentsByStatusForForStoreInStateNoConsignmentFound() {

        final List<TargetConsignmentModel> consignmentlist = Collections.emptyList();
        when(mockTargetConsignmentDao
                .getConsignmentsByStatusForStoreInState(any(List.class), any(List.class)))
                        .thenReturn(consignmentlist);

        assertThat(Collections.emptyList()).isEqualTo(
                targetStoreConsignmentService.getConsignmentsForStoreInState(any(List.class),
                        any(List.class)));
    }

    /**
     * Test get consignments by status for stores in given states. will return List of TargetConsignemntModel
     */
    @Test
    public void testGetConsignmentsByStatusForStoreInState() {

        final List<TargetConsignmentModel> consignmentlist = Arrays.asList(mock(TargetConsignmentModel.class));
        when(mockTargetConsignmentDao
                .getConsignmentsByStatusForStoreInState(any(List.class), any(List.class)))
                        .thenReturn(consignmentlist);

        assertThat(consignmentlist).isEqualTo(
                targetStoreConsignmentService.getConsignmentsForStoreInState(any(List.class),
                        any(List.class)));

    }

    /**
     * Test get consignments by status for stores not in given states, When no consignment found it will return empty
     * List
     */
    @Test
    public void testGetConsignmentsByStatusForForStoreNotInStateNoConsignmentFound() {

        final List<TargetConsignmentModel> consignmentlist = Collections.emptyList();
        when(mockTargetConsignmentDao
                .getConsignmentsByStatusForStoreNotInState(any(List.class),
                        any(List.class)))
                                .thenReturn(consignmentlist);

        assertThat(Collections.emptyList()).isEqualTo(
                targetStoreConsignmentService.getConsignmentsForStoreNotInState(any(List.class),
                        any(List.class)));
    }

    /**
     * Test get consignments by status for stores not in given states. will return List of TargetConsignemntModel
     */
    @Test
    public void testGetConsignmentsByStatusForStoreNotInState() {

        final List<TargetConsignmentModel> consignmentlist = Arrays.asList(mock(TargetConsignmentModel.class));
        when(mockTargetConsignmentDao
                .getConsignmentsByStatusForStoreNotInState(any(List.class),
                        any(List.class)))
                                .thenReturn(consignmentlist);

        assertThat(consignmentlist).isEqualTo(
                targetStoreConsignmentService.getConsignmentsForStoreNotInState(any(List.class),
                        any(List.class)));

    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetConsignmentsByDayForStoreNullStore() {
        targetStoreConsignmentService.getConsignmentsByDayForStore(null, 0);
    }

    @Test
    public void testGetConsignmentsByDayForStoreNoConsignments() {
        when(mockTargetConsignmentDao.getConsignmentsNotProcessedForStore(store)).thenReturn(
                ListUtils.EMPTY_LIST);
        when(mockTargetConsignmentDao.getConsignmentsCompletedByDayForStore(store, 0)).thenReturn(
                ListUtils.EMPTY_LIST);
        when(mockTargetConsignmentDao.getConsignmentsRejectedByDayForStore(store, 0)).thenReturn(
                ListUtils.EMPTY_LIST);

        assertThat(targetStoreConsignmentService.getConsignmentsByDayForStore(store, 0).size()).isEqualTo(0);
    }

    @Test
    public void testGetConsignmentsByDayForStoreToday() {
        final List<TargetConsignmentModel> notProcessedConsignments = new ArrayList<>();
        notProcessedConsignments.add(new TargetConsignmentModel());
        notProcessedConsignments.add(new TargetConsignmentModel());

        final List<TargetConsignmentModel> completedConsignments = new ArrayList<>();
        completedConsignments.add(new TargetConsignmentModel());
        completedConsignments.add(new TargetConsignmentModel());

        final List<TargetConsignmentModel> rejectedConsignments = new ArrayList<>();
        rejectedConsignments.add(new TargetConsignmentModel());

        when(mockTargetConsignmentDao.getConsignmentsNotProcessedForStore(store)).thenReturn(
                notProcessedConsignments);
        when(mockTargetConsignmentDao.getConsignmentsCompletedByDayForStore(store, 0)).thenReturn(
                completedConsignments);
        when(mockTargetConsignmentDao.getConsignmentsRejectedByDayForStore(store, 0)).thenReturn(
                rejectedConsignments);

        assertThat(targetStoreConsignmentService.getConsignmentsByDayForStore(store, 0).size()).isEqualTo(5);
    }

    @Test
    public void testGetConsignmentsByDayForStoreYesterday() {
        final List<TargetConsignmentModel> notProcessedConsignments = new ArrayList<>();
        notProcessedConsignments.add(new TargetConsignmentModel());
        notProcessedConsignments.add(new TargetConsignmentModel());

        final List<TargetConsignmentModel> completedConsignments = new ArrayList<>();
        completedConsignments.add(new TargetConsignmentModel());
        completedConsignments.add(new TargetConsignmentModel());

        final List<TargetConsignmentModel> rejectedConsignments = new ArrayList<>();
        rejectedConsignments.add(new TargetConsignmentModel());

        when(mockTargetConsignmentDao.getConsignmentsNotProcessedForStore(store)).thenReturn(
                notProcessedConsignments);
        when(mockTargetConsignmentDao.getConsignmentsCompletedByDayForStore(store, 1)).thenReturn(
                completedConsignments);
        when(mockTargetConsignmentDao.getConsignmentsRejectedByDayForStore(store, 1)).thenReturn(
                rejectedConsignments);

        assertThat(targetStoreConsignmentService.getConsignmentsByDayForStore(store, 1).size()).isEqualTo(3);
    }

    @Test
    public void testGetAssignedStoreForConsignment() {

        // Default consignment is assigned to store
        assertThat(targetStoreConsignmentService.getAssignedStoreForConsignment(mockConsignment)).isEqualTo(store);
    }

    @Test
    public void testGetAssignedStoreForConsignmentNullConsignment() {

        assertThat(targetStoreConsignmentService.getAssignedStoreForConsignment(null)).isNull();
    }

    @Test
    public void testGetAssignedStoreForConsignmentNullWarehouse() {

        when(mockConsignment.getWarehouse()).thenReturn(null);
        assertThat(targetStoreConsignmentService.getAssignedStoreForConsignment(mockConsignment)).isNull();
    }

    @Test
    public void testGetAssignedStoreForConsignmentNoStores() {

        when(mockWarehouse.getPointsOfService()).thenReturn(Collections.EMPTY_SET);
        assertThat(targetStoreConsignmentService.getAssignedStoreForConsignment(null)).isNull();
    }

    @Test
    public void testGetConsignmentByCode() {

        consignments.add(mockConsignment);
        final TargetConsignmentModel result = targetStoreConsignmentService
                .getConsignmentByCode("1234");
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(mockConsignment);
    }

    @Test
    public void testGetConsignmentByCodeNullCode() {

        consignments.add(mockConsignment);
        final TargetConsignmentModel result = targetStoreConsignmentService
                .getConsignmentByCode(null);
        assertThat(result).isNull();
    }

    @Test
    public void testGetConsignmentByCodeNoneFound() {

        final TargetConsignmentModel result = targetStoreConsignmentService
                .getConsignmentByCode("1234");
        assertThat(result).isNull();
    }

    @Test
    public void tesGetConsignmentCountByStatusForZeroDaysValue() {
        final TargetPointOfServiceModel mockTpos = mock(TargetPointOfServiceModel.class);
        final HashMap<ConsignmentStatus, Integer> notProcessedConsignments = new HashMap<>();
        notProcessedConsignments.put(ConsignmentStatus.SENT_TO_WAREHOUSE, Integer.valueOf(4));
        notProcessedConsignments.put(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, Integer.valueOf(3));

        final HashMap<ConsignmentStatus, Integer> completedConsignments = new HashMap<>();
        completedConsignments.put(ConsignmentStatus.SHIPPED, Integer.valueOf(4));


        final HashMap<ConsignmentStatus, Integer> rejectedConsignments = new HashMap<>();
        rejectedConsignments.put(ConsignmentStatus.CANCELLED, Integer.valueOf(2));

        when(mockTargetConsignmentDao.getCountOfConsignmentsNotProcessedForStoreByStatus(mockTpos))
                .thenReturn(notProcessedConsignments);
        when(mockTargetConsignmentDao
                .getCountOfConsignmentsCompletedByDayForStoreByStatus(mockTpos, 0))
                        .thenReturn(completedConsignments);
        when(mockTargetConsignmentDao
                .getCountOfConsignmentsRejectedByDayForStoreByStatus(mockTpos, 0)).thenReturn(rejectedConsignments);

        final Map<ConsignmentStatus, Integer> consignmentStatusMap = targetStoreConsignmentService
                .getConsignmentCountByStatus(mockTpos, 0);
        assertThat(consignmentStatusMap).isNotEmpty();
        assertThat(consignmentStatusMap.get(ConsignmentStatus.SENT_TO_WAREHOUSE)).isEqualTo(4);
        assertThat(consignmentStatusMap.get(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE)).isEqualTo(3);
        assertThat(consignmentStatusMap.get(ConsignmentStatus.SHIPPED)).isEqualTo(4);
        assertThat(consignmentStatusMap.get(ConsignmentStatus.CANCELLED)).isEqualTo(2);


    }

    @Test
    public void tesGetConsignmentCountByStatusForNonZeroDaysValue() {
        final TargetPointOfServiceModel mockTpos = mock(TargetPointOfServiceModel.class);
        final HashMap<ConsignmentStatus, Integer> notProcessedConsignments = new HashMap<>();
        notProcessedConsignments.put(ConsignmentStatus.SENT_TO_WAREHOUSE, Integer.valueOf(4));
        notProcessedConsignments.put(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, Integer.valueOf(3));

        final HashMap<ConsignmentStatus, Integer> completedConsignments = new HashMap<>();
        completedConsignments.put(ConsignmentStatus.SHIPPED, Integer.valueOf(4));


        final HashMap<ConsignmentStatus, Integer> rejectedConsignments = new HashMap<>();
        rejectedConsignments.put(ConsignmentStatus.CANCELLED, Integer.valueOf(2));

        when(mockTargetConsignmentDao.getCountOfConsignmentsNotProcessedForStoreByStatus(mockTpos))
                .thenReturn(notProcessedConsignments);
        when(mockTargetConsignmentDao
                .getCountOfConsignmentsCompletedByDayForStoreByStatus(mockTpos, 1))
                        .thenReturn(completedConsignments);
        when(mockTargetConsignmentDao
                .getCountOfConsignmentsRejectedByDayForStoreByStatus(mockTpos, 1)).thenReturn(rejectedConsignments);

        final Map<ConsignmentStatus, Integer> consignmentStatusMap = targetStoreConsignmentService
                .getConsignmentCountByStatus(mockTpos, 1);
        assertThat(consignmentStatusMap).isNotEmpty();
        assertThat(consignmentStatusMap.containsKey(ConsignmentStatus.SENT_TO_WAREHOUSE)).isFalse();
        assertThat(consignmentStatusMap.containsKey(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE)).isFalse();
        assertThat(consignmentStatusMap.get(ConsignmentStatus.SHIPPED)).isEqualTo(4);
        assertThat(consignmentStatusMap.get(ConsignmentStatus.CANCELLED)).isEqualTo(2);


    }

    @Test
    public void testGetDeliverToStateFromConsignment() {
        final String district = "VIC";
        final AddressModel address = mock(AddressModel.class);
        given(address.getDistrict()).willReturn(district);
        when(mockConsignment.getShippingAddress()).thenReturn(address);
        final String state = targetStoreConsignmentService.getDeliverToStateFromConsignment(mockConsignment);
        assertThat(state).isEqualTo(district);
    }

    @Test
    public void testGetDeliverToStateFromConsignmentWithShippingAddressNull() {
        final String state = targetStoreConsignmentService.getDeliverToStateFromConsignment(mockConsignment);
        assertThat(state).isEqualTo(StringUtils.EMPTY);
    }

    @Test
    public void testGetDeliverToStoreNumberFromConsignment() {
        final Integer cncStoreNumber = Integer.valueOf(5001);
        given(mockOrder.getCncStoreNumber()).willReturn(cncStoreNumber);
        final String storeNumber = targetStoreConsignmentService
                .getDeliverToStoreNumberFromConsignment(mockConsignment);
        assertThat(storeNumber).isEqualTo(cncStoreNumber.toString());
    }

    @Test
    public void testGetDeliverToStoreNumberFromConsignmentWithEmptyStoreNumber() {
        given(mockOrder.getCncStoreNumber()).willReturn(null);
        final String storeNumber = targetStoreConsignmentService
                .getDeliverToStoreNumberFromConsignment(mockConsignment);
        assertThat(storeNumber).isEqualTo(StringUtils.EMPTY);
    }

    @Test
    public void testGetDeliverFromStateFromConsignment() {
        final String storeState = targetStoreConsignmentService
                .getDeliverFromStateFromConsignment(mockConsignment);
        assertThat(storeState).isEqualTo(mockStoreState);
    }

    @Test
    public void testGetDeliverFromStateFromConsignmentWithNullAddress() {
        given(store.getAddress()).willReturn(null);
        final String storeState = targetStoreConsignmentService
                .getDeliverFromStateFromConsignment(mockConsignment);
        assertThat(storeState).isEqualTo(StringUtils.EMPTY);
    }

    @Test
    public void testGetOrderCodeFromConsignmentWithNullOrder() {
        final String result = targetStoreConsignmentService.getOrderCodeFromConsignment(mockConsignment);
        assertThat(result).isEqualTo(mockOrderCode);
    }
}

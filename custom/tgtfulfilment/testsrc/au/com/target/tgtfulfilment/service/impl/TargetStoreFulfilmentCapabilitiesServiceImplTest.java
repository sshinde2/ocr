/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.strategy.impl.OrderEntryGroup;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.category.TargetCategoryService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfulfilment.dao.TargetConsignmentDao;
import au.com.target.tgtfulfilment.helper.OEGParameterHelper;
import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.model.ProductExclusionsModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;

import com.google.common.collect.Sets;


/**
 * Test TargetStoreFulfilmentCapabilitiesServiceImpl service Impl for store Blackout window
 * 
 * @author ajit
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetStoreFulfilmentCapabilitiesServiceImplTest {

    @InjectMocks
    private final TargetStoreFulfilmentCapabilitiesServiceImpl storeCapabilitiesService = new TargetStoreFulfilmentCapabilitiesServiceImpl();

    @Mock
    private TargetPointOfServiceModel tpos;

    @Mock
    private StoreFulfilmentCapabilitiesModel storeCapabilitiesModel;

    @Mock
    private GlobalStoreFulfilmentCapabilitiesModel globalStoreCapabilitiesModel;

    @Mock
    private TargetConsignmentDao targetConsignmentDao;

    @Mock
    private TargetZoneDeliveryModeModel deliveryMode;

    @Mock
    private TargetZoneDeliveryModeModel capDeliveryMode;

    @Mock
    private OEGParameterHelper oegParameterHelper;

    @Mock
    private TargetCategoryService categoryService;

    @Mock
    private CategoryModel category1;

    @Mock
    private CategoryModel category2;

    @Mock
    private TargetGlobalStoreFulfilmentService targetGlobalStoreFulfilmentService;

    @Mock
    private AbstractOrderEntryModel orderEntry1;

    @Mock
    private AbstractOrderEntryModel orderEntry2;

    @Mock
    private Set<ProductExclusionsModel> productExclusionList;

    @Mock
    private ProductModel exclProduct;

    @Mock
    private ProductModel exclProduct1;

    @Mock
    private ProductModel nonExcelProd;

    @Mock
    private ProductModel orderEntryProduct;

    @Mock
    private ProductModel orderEntryProduct1;

    @Mock
    private VariantProductModel variantProduct;

    @Mock
    private VariantProductModel variantProduct1;

    @Mock
    private ProductExclusionsModel productExclusion1;

    @Mock
    private ProductExclusionsModel productExclusion2;


    private final OrderEntryGroup oeg = new OrderEntryGroup();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        // Default capabilities
        Mockito.when(storeCapabilitiesModel.getEnabled()).thenReturn(Boolean.TRUE);
        Mockito.when(storeCapabilitiesModel.getCanBeUsed()).thenReturn(Boolean.TRUE);
        Mockito.when(storeCapabilitiesModel.getDeliveryModes()).thenReturn(Collections.singleton(capDeliveryMode));

        Mockito.when(deliveryMode.getCode()).thenReturn("clickandcollect");
        Mockito.when(capDeliveryMode.getCode()).thenReturn("clickandcollect");

        Mockito.when(oegParameterHelper.getOrderCode((OrderEntryGroup)Mockito.any())).thenReturn("10001000");
        // By default capabilities model is present
        Mockito.when(tpos.getFulfilmentCapability()).thenReturn(storeCapabilitiesModel);
    }

    @Test
    public void testIsStoreEnabledNullCapabilities() {

        Mockito.when(tpos.getFulfilmentCapability()).thenReturn(null);
        assertThat(storeCapabilitiesService.isStoreEnabledForFulfilment(tpos)).isFalse();
    }

    @Test
    public void testIsStoreEnabledWithEnabledNull() {

        Mockito.when(storeCapabilitiesModel.getEnabled()).thenReturn(null);
        assertThat(storeCapabilitiesService.isStoreEnabledForFulfilment(tpos)).isFalse();
    }

    @Test
    public void testIsStoreEnabledWithEnabledTrue() {

        Mockito.when(storeCapabilitiesModel.getEnabled()).thenReturn(Boolean.TRUE);
        assertThat(storeCapabilitiesService.isStoreEnabledForFulfilment(tpos)).isTrue();
    }

    @Test
    public void testIsStoreEnabledWithEnabledFalse() {

        Mockito.when(storeCapabilitiesModel.getEnabled()).thenReturn(Boolean.FALSE);
        assertThat(storeCapabilitiesService.isStoreEnabledForFulfilment(tpos)).isFalse();
    }

    @Test
    public void testStoreBlackoutPeriodExistNullCapabilities() {

        Mockito.when(tpos.getFulfilmentCapability()).thenReturn(null);
        assertThat(storeCapabilitiesService.isTargetStoreBlackoutExist(tpos)).isFalse();
    }

    @Test
    public void testStoreBlackoutPeriodExist() {

        Mockito.when(storeCapabilitiesModel.getCanBeUsed()).thenReturn(Boolean.FALSE);
        assertThat(storeCapabilitiesService.isTargetStoreBlackoutExist(tpos)).isTrue();
    }

    @Test
    public void testStoreBlackoutPeriodDoesNotExist() {

        Mockito.when(storeCapabilitiesModel.getCanBeUsed()).thenReturn(Boolean.TRUE);
        assertThat(storeCapabilitiesService.isTargetStoreBlackoutExist(tpos)).isFalse();
    }

    @Test
    public void testDeliveryModesNullCapabilities() {

        Mockito.when(tpos.getFulfilmentCapability()).thenReturn(null);
        assertThat(storeCapabilitiesService.isDeliveryModeAssociatedToStore(tpos, deliveryMode)).isFalse();
    }

    @Test
    public void testDeliveryModesCapabilitiesHasNoDeliveryModes() {

        Mockito.when(storeCapabilitiesModel.getDeliveryModes()).thenReturn(Collections.EMPTY_SET);
        assertThat(storeCapabilitiesService.isDeliveryModeAssociatedToStore(tpos, deliveryMode)).isFalse();
    }

    @Test
    public void testDeliveryModesCapabilitiesHasMatchingDeliveryModes() {

        assertThat(storeCapabilitiesService.isDeliveryModeAssociatedToStore(tpos, deliveryMode)).isTrue();
    }

    @Test
    public void testDeliveryModesCapabilitiesHasNonMatchingDeliveryModes() {

        Mockito.when(deliveryMode.getCode()).thenReturn("homedelivery");
        assertThat(storeCapabilitiesService.isDeliveryModeAssociatedToStore(tpos, deliveryMode)).isFalse();
    }

    @Test
    public void testForStoreFulfilmentCapablitiesIsNull() {
        Mockito.when(tpos.getFulfilmentCapability()).thenReturn(null);
        assertThat(storeCapabilitiesService.isMaxNumberOfOrdersExceeded(tpos)).isFalse();
    }

    @Test
    public void testForNoMaxConsignmentSetForStoreUnderTest() {

        Mockito.when(storeCapabilitiesModel.getMaxConsignmentsForDeliveryPerDay()).thenReturn(null);
        assertThat(storeCapabilitiesService.isMaxNumberOfOrdersExceeded(tpos)).isFalse();
    }

    @Test
    public void testForTposBeingNull() {

        Mockito.when(tpos.getStoreNumber()).thenReturn(null);
        assertThat(storeCapabilitiesService.isMaxNumberOfOrdersExceeded(tpos)).isFalse();
    }

    @Test
    public void testMaxConsignmentsPerDayZero() {
        Mockito.when(storeCapabilitiesModel.getMaxConsignmentsForDeliveryPerDay()).thenReturn(Integer.valueOf(0));
        assertThat(storeCapabilitiesService.isMaxNumberOfOrdersExceeded(tpos)).isFalse();
    }

    @Test
    public void testForMaxPeriodStartTimeNull() {

        Mockito.when(storeCapabilitiesModel.getMaxConsignmentsForDeliveryPerDay()).thenReturn(Integer.valueOf(2));
        Mockito.when(targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites()).thenReturn(
                globalStoreCapabilitiesModel);
        Mockito.when(globalStoreCapabilitiesModel.getMaxOrderPeriodStartTime()).thenReturn("00:00");
        assertThat(storeCapabilitiesService.isMaxNumberOfOrdersExceeded(tpos)).isFalse();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testForMaxOrderGreaterThanAllowed() {
        Mockito.when(tpos.getFulfilmentCapability()).thenReturn(storeCapabilitiesModel);
        Mockito.when(storeCapabilitiesModel.getMaxConsignmentsForDeliveryPerDay()).thenReturn(Integer.valueOf(5));
        Mockito.when(targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites()).thenReturn(
                globalStoreCapabilitiesModel);
        Mockito.when(globalStoreCapabilitiesModel.getMaxOrderPeriodStartTime()).thenReturn("00:00");
        BDDMockito.when(
                targetConsignmentDao.getConsignmentCountForStoreByMaxCutoffPeriod(Mockito.anyInt(),
                        Mockito.any(Date.class)))
                .thenReturn(Integer.valueOf(7));
        assertThat(storeCapabilitiesService.isMaxNumberOfOrdersExceeded(tpos)).isTrue();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testForMaxOrderLessThanAllowed() {

        Mockito.when(storeCapabilitiesModel.getMaxConsignmentsForDeliveryPerDay()).thenReturn(Integer.valueOf(5));
        Mockito.when(targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites()).thenReturn(
                globalStoreCapabilitiesModel);
        Mockito.when(globalStoreCapabilitiesModel.getMaxOrderPeriodStartTime()).thenReturn("00:00");
        BDDMockito.when(
                targetConsignmentDao.getConsignmentCountForStoreByMaxCutoffPeriod(Mockito.any(Integer.class),
                        Mockito.any(Date.class)))
                .thenReturn(2);
        assertThat(storeCapabilitiesService.isMaxNumberOfOrdersExceeded(tpos)).isFalse();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testForMaxOrderGreaterThanAllowedEdgeCase() {

        Mockito.when(storeCapabilitiesModel.getMaxConsignmentsForDeliveryPerDay()).thenReturn(Integer.valueOf(5));
        Mockito.when(targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites()).thenReturn(
                globalStoreCapabilitiesModel);
        Mockito.when(globalStoreCapabilitiesModel.getMaxOrderPeriodStartTime()).thenReturn("00:00");
        BDDMockito.when(
                targetConsignmentDao.getConsignmentCountForStoreByMaxCutoffPeriod(Mockito.anyInt(),
                        Mockito.any(Date.class)))
                .thenReturn(Integer.valueOf(6));
        assertThat(storeCapabilitiesService.isMaxNumberOfOrdersExceeded(tpos)).isTrue();
    }

    @Test
    public void testForMaxOrderGreaterThanAllowedNullPeriodStartTime() {

        Mockito.when(storeCapabilitiesModel.getMaxConsignmentsForDeliveryPerDay()).thenReturn(Integer.valueOf(5));
        Mockito.when(targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites()).thenReturn(
                globalStoreCapabilitiesModel);
        Mockito.when(globalStoreCapabilitiesModel.getMaxOrderPeriodStartTime()).thenReturn(null);
        BDDMockito.when(
                Integer.valueOf(targetConsignmentDao.getConsignmentCountForStoreByMaxCutoffPeriod(
                        Mockito.any(Integer.class),
                        Mockito.any(Date.class))))
                .thenReturn(Integer.valueOf(6));
        assertThat(storeCapabilitiesService.isMaxNumberOfOrdersExceeded(tpos)).isFalse();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testForMaxOrderEqualToAllowed() {

        Mockito.when(storeCapabilitiesModel.getMaxConsignmentsForDeliveryPerDay()).thenReturn(Integer.valueOf(5));
        Mockito.when(targetGlobalStoreFulfilmentService.getGlobalStoreFulfilmentCapabilites()).thenReturn(
                globalStoreCapabilitiesModel);
        Mockito.when(globalStoreCapabilitiesModel.getMaxOrderPeriodStartTime()).thenReturn("00:00");
        BDDMockito.when(
                targetConsignmentDao.getConsignmentCountForStoreByMaxCutoffPeriod(Mockito.anyInt(),
                        Mockito.any(Date.class)))
                .thenReturn(Integer.valueOf(5));
        assertThat(storeCapabilitiesService.isMaxNumberOfOrdersExceeded(tpos)).isTrue();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOrderContainingExcludedCategoriesNullOeg() {
        storeCapabilitiesService.isOrderContainingProductsFromExcludedCategories(null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOrderContainingExcludedCategoriesNullTpos() {
        final OrderEntryGroup oeg1 = Mockito.mock(OrderEntryGroup.class);
        storeCapabilitiesService.isOrderContainingProductsFromExcludedCategories(oeg1, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOrderContainingExcludedCategoriesNullProduct() {
        final OrderEntryModel entryModel1 = Mockito.mock(OrderEntryModel.class);
        final OrderEntryModel entryModel2 = Mockito.mock(OrderEntryModel.class);

        final OrderEntryGroup oeg1 = new OrderEntryGroup();
        oeg1.add(entryModel1);
        oeg1.add(entryModel2);

        storeCapabilitiesService.isOrderContainingProductsFromExcludedCategories(oeg1, createExcludedCategories());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOrderContainingExcludedCategoriesNullBaseProduct() {
        final OrderEntryModel entryModel1 = Mockito.mock(OrderEntryModel.class);
        final OrderEntryModel entryModel2 = Mockito.mock(OrderEntryModel.class);
        final AbstractTargetVariantProductModel product1 = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(product1.getBaseProduct()).willReturn(null);
        BDDMockito.given(entryModel1.getProduct()).willReturn(product1);
        final AbstractTargetVariantProductModel product2 = Mockito.mock(AbstractTargetVariantProductModel.class);
        BDDMockito.given(product2.getBaseProduct()).willReturn(null);
        BDDMockito.given(entryModel2.getProduct()).willReturn(product1);
        final OrderEntryGroup oeg1 = new OrderEntryGroup();
        oeg1.add(entryModel1);
        oeg1.add(entryModel2);

        storeCapabilitiesService.isOrderContainingProductsFromExcludedCategories(oeg1, createExcludedCategories());
    }

    @Test
    public void testOrderContainingExcludedCategoriesTrue() {
        final OrderEntryModel entryModel1 = Mockito.mock(OrderEntryModel.class);
        final OrderEntryModel entryModel2 = Mockito.mock(OrderEntryModel.class);
        final AbstractTargetVariantProductModel product1 = Mockito.mock(AbstractTargetVariantProductModel.class);
        final TargetProductModel baseProduct1 = Mockito.mock(TargetProductModel.class);
        BDDMockito.given(product1.getBaseProduct()).willReturn(baseProduct1);
        BDDMockito.given(entryModel1.getProduct()).willReturn(product1);
        final AbstractTargetVariantProductModel product2 = Mockito.mock(AbstractTargetVariantProductModel.class);
        final TargetProductModel baseProduct2 = Mockito.mock(TargetProductModel.class);
        BDDMockito.given(product2.getBaseProduct()).willReturn(baseProduct2);
        BDDMockito.given(entryModel2.getProduct()).willReturn(product1);
        final OrderEntryGroup oeg1 = new OrderEntryGroup();
        oeg1.add(entryModel1);
        oeg1.add(entryModel2);

        Mockito.when(categoryService.getAllSuperCategoriesForProduct(baseProduct1))
                .thenReturn(Collections.singleton(category1));

        Assert.assertTrue(storeCapabilitiesService.isOrderContainingProductsFromExcludedCategories(oeg1,
                createExcludedCategories()));
    }

    @Test
    public void testOrderContainingExcludedCategoriesFalse() {
        final OrderEntryModel entryModel1 = Mockito.mock(OrderEntryModel.class);
        final OrderEntryModel entryModel2 = Mockito.mock(OrderEntryModel.class);
        final AbstractTargetVariantProductModel product1 = Mockito.mock(AbstractTargetVariantProductModel.class);
        final TargetProductModel baseProduct1 = Mockito.mock(TargetProductModel.class);
        BDDMockito.given(product1.getBaseProduct()).willReturn(baseProduct1);
        BDDMockito.given(entryModel1.getProduct()).willReturn(product1);
        final AbstractTargetVariantProductModel product2 = Mockito.mock(AbstractTargetVariantProductModel.class);
        final TargetProductModel baseProduct2 = Mockito.mock(TargetProductModel.class);
        BDDMockito.given(product2.getBaseProduct()).willReturn(baseProduct2);
        BDDMockito.given(entryModel2.getProduct()).willReturn(product1);
        final OrderEntryGroup oeg1 = new OrderEntryGroup();
        oeg1.add(entryModel1);
        oeg1.add(entryModel2);

        Mockito.when(categoryService.getAllSuperCategoriesForProduct(baseProduct1))
                .thenReturn(Collections.singleton(category2));

        Assert.assertFalse(storeCapabilitiesService.isOrderContainingProductsFromExcludedCategories(oeg1,
                createExcludedCategories()));
    }

    private List<CategoryModel> createExcludedCategories() {
        final List<CategoryModel> excludeCategories = new ArrayList<>();
        excludeCategories.add(category1);
        return excludeCategories;
    }

    @Test
    public void testContainsProductForProductInList() {
        BDDMockito.given(exclProduct.getCode()).willReturn("P_20001");
        BDDMockito.given(productExclusion1.getProduct()).willReturn(exclProduct);
        assertThat(storeCapabilitiesService.containsProduct(getProductExclusionList(), exclProduct)).isTrue();
    }

    @Test
    public void testContainsProductForProductNotInList() {
        BDDMockito.given(exclProduct.getCode()).willReturn("P_20001");
        BDDMockito.given(nonExcelProd.getCode()).willReturn("P_20002");
        BDDMockito.given(productExclusion1.getProduct()).willReturn(exclProduct);
        assertThat(storeCapabilitiesService.containsProduct(getProductExclusionList(), nonExcelProd)).isFalse();
    }

    @Test
    public void testProductOrderedPresentInExcludedList() {
        oeg.add(orderEntry1);
        BDDMockito.given(orderEntry1.getProduct()).willReturn(variantProduct);
        BDDMockito.given(variantProduct.getBaseProduct()).willReturn(orderEntryProduct);
        BDDMockito.given(productExclusion1.getProduct()).willReturn(exclProduct);
        BDDMockito.given(exclProduct.getCode()).willReturn("P_10001");
        BDDMockito.given(orderEntryProduct.getCode()).willReturn("P_10001");
        final boolean result = storeCapabilitiesService.isProductInExclusionList(oeg, getProductExclusionList());
        Assert.assertTrue(result);
    }


    @Test
    public void testProductOrderedNotPresentInExcludedList() {
        oeg.add(orderEntry1);
        BDDMockito.given(orderEntry1.getProduct()).willReturn(variantProduct);
        BDDMockito.given(variantProduct.getBaseProduct()).willReturn(orderEntryProduct);
        BDDMockito.given(productExclusion1.getProduct()).willReturn(exclProduct);
        BDDMockito.given(exclProduct.getCode()).willReturn("P_10001");
        BDDMockito.given(orderEntryProduct.getCode()).willReturn("P_10002");
        final boolean result = storeCapabilitiesService.isProductInExclusionList(oeg, getProductExclusionList());
        Assert.assertFalse(result);
    }


    @Test
    public void testNullExclusionList() {
        oeg.add(orderEntry1);
        BDDMockito.given(orderEntry1.getProduct()).willReturn(variantProduct);
        BDDMockito.given(variantProduct.getBaseProduct()).willReturn(orderEntryProduct);
        BDDMockito.given(orderEntryProduct.getCode()).willReturn("P_10001");
        final boolean result = storeCapabilitiesService.isProductInExclusionList(oeg, null);

        Assert.assertFalse(result);
    }

    @Test
    public void testOneOfTheProductOrderedPresentInExcludedList() {
        oeg.add(orderEntry1);
        BDDMockito.given(orderEntry1.getProduct()).willReturn(variantProduct);
        BDDMockito.given(variantProduct.getBaseProduct()).willReturn(orderEntryProduct);
        BDDMockito.given(productExclusion1.getProduct()).willReturn(exclProduct);
        BDDMockito.given(exclProduct.getCode()).willReturn("P_10001");
        BDDMockito.given(productExclusion2.getProduct()).willReturn(exclProduct1);
        BDDMockito.given(exclProduct1.getCode()).willReturn("P_10002");
        BDDMockito.given(orderEntryProduct.getCode()).willReturn("P_10002");
        final boolean result = storeCapabilitiesService.isProductInExclusionList(oeg, getProductExclusionList());
        Assert.assertTrue(result);
    }

    @Test
    public void testNoneOfTheProductOrderedPresentInExcludedList() {
        oeg.add(orderEntry1);
        BDDMockito.given(orderEntry1.getProduct()).willReturn(variantProduct);
        BDDMockito.given(variantProduct.getBaseProduct()).willReturn(orderEntryProduct);
        BDDMockito.given(productExclusion1.getProduct()).willReturn(exclProduct);
        BDDMockito.given(exclProduct.getCode()).willReturn("P_10001");
        BDDMockito.given(productExclusion2.getProduct()).willReturn(exclProduct1);
        BDDMockito.given(exclProduct1.getCode()).willReturn("P_10002");
        BDDMockito.given(orderEntryProduct.getCode()).willReturn("P_20001");
        final boolean result = storeCapabilitiesService.isProductInExclusionList(oeg, getProductExclusionList());
        Assert.assertFalse(result);
    }


    private Set<ProductExclusionsModel> getProductExclusionList() {
        return Sets.newHashSet(productExclusion1, productExclusion2);
    }
}

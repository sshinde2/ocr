/**
 * 
 */
package au.com.target.tgtfulfilment.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Collections;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.dao.TargetPointOfServiceDao;
import au.com.target.tgtfulfilment.service.TargetStoreLocatorService;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetStoreLocatorServiceImplTest {

    @Mock
    private TargetPointOfServiceDao targetPointOfServiceDao;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @InjectMocks
    private final TargetStoreLocatorService targetStoreLocatorService = new TargetStoreLocatorServiceImpl();


    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Test
    public void testGetAllFulfilmentStoresInStateNullState() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("A value needs to be provided for the state");
        targetStoreLocatorService.getAllFulfilmentStoresInState(null);
        verifyZeroInteractions(targetPointOfServiceDao);
    }

    @Test
    public void testGetAllFulfilmentStoresInStateEmptyState() {
        when(targetPointOfServiceDao.getAllFulfilmentEnabledPOSInState(Mockito.anyString())).thenReturn(
                Collections.EMPTY_LIST);
        final List<TargetPointOfServiceModel> pos = targetStoreLocatorService.getAllFulfilmentStoresInState("");
        assertThat(pos).isNotNull().isEmpty();
        verify(targetPointOfServiceDao).getAllFulfilmentEnabledPOSInState(Mockito.anyString());
        verifyNoMoreInteractions(targetPointOfServiceDao);
    }

    @Test
    public void testGetAllFulfilmentStoresInState() {
        when(targetPointOfServiceDao.getAllFulfilmentEnabledPOSInState(Mockito.anyString())).thenReturn(
                Collections.singletonList(new TargetPointOfServiceModel()));
        final List<TargetPointOfServiceModel> pos = targetStoreLocatorService.getAllFulfilmentStoresInState("VIC");
        assertThat(pos).isNotNull().isNotEmpty().hasSize(1);
        verify(targetPointOfServiceDao).getAllFulfilmentEnabledPOSInState(Mockito.anyString());
        verifyNoMoreInteractions(targetPointOfServiceDao);
    }

    @Test
    public void testGetAllFulfilmentStores() {
        when(targetPointOfServiceDao.getAllFulfilmentEnabledPOS()).thenReturn(
                Collections.singletonList(new TargetPointOfServiceModel()));
        final List<TargetPointOfServiceModel> pos = targetStoreLocatorService.getAllFulfilmentStores();
        assertThat(pos).isNotNull().isNotEmpty().hasSize(1);
        verify(targetPointOfServiceDao).getAllFulfilmentEnabledPOS();
        verifyNoMoreInteractions(targetPointOfServiceDao);
    }

    @Test
    public void testGetAllFulfilmentStoresWithFulfilmentPPDFeatureOn() {
        when(
                Boolean.valueOf(targetFeatureSwitchService
                        .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FULFILMENT_PPD))).thenReturn(Boolean.TRUE);
        when(targetPointOfServiceDao.getAllFulfilmentPPDEnabledPOS()).thenReturn(
                Collections.singletonList(new TargetPointOfServiceModel()));
        final List<TargetPointOfServiceModel> pos = targetStoreLocatorService.getAllFulfilmentStores();
        assertThat(pos).isNotNull().isNotEmpty().hasSize(1);
        verify(targetPointOfServiceDao).getAllFulfilmentPPDEnabledPOS();
        verifyNoMoreInteractions(targetPointOfServiceDao);
    }
}

/**
 * 
 */
package au.com.target.tgtfulfilment.stock.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.stock.model.StockLevelHistoryEntryModel;
import de.hybris.platform.stock.strategy.StockLevelProductStrategy;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtfulfilment.stock.dao.TargetFulfilmentStockLevelDao;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFulfilmentStockServiceImplTest {

    protected class MyTargetFulfilmentStockServiceImpl extends TargetFulfilmentStockServiceImpl {

        @Override
        public void clearCacheForItem(final StockLevelModel stockLevel) {
            // do nothing
        }
    }

    @InjectMocks
    @Spy
    private final MyTargetFulfilmentStockServiceImpl targetFulfilmentStockService = new MyTargetFulfilmentStockServiceImpl();

    @Mock
    private TargetFulfilmentStockLevelDao targetFulfilmentStockLevelDao;

    @Mock
    private StockLevelModel stockLevelModel;

    @Mock
    private WarehouseModel warehouse;

    @Mock
    private TargetColourVariantProductModel colourVariant;

    @Mock
    private StockLevelProductStrategy stockLevelProductStrategy;

    @Mock
    private ModelService modelService;

    @Mock
    private StockLevelHistoryEntryModel stockLevelHistoryEntryModel;

    @Before
    public void setup() {
        Mockito.when(targetFulfilmentStockLevelDao.findStockLevel((String)Mockito.any(), (WarehouseModel)Mockito.any()))
                .thenReturn(stockLevelModel);
        Mockito.when(targetFulfilmentStockService.getStockLevel(colourVariant, warehouse)).thenReturn(
                stockLevelModel);
        Mockito.when(modelService.create(StockLevelHistoryEntryModel.class)).thenReturn(stockLevelHistoryEntryModel);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testReconcileStockLevelReservedQuantityUnder() {
        Mockito.when(targetFulfilmentStockLevelDao.calculateReservedStockAdjustment(colourVariant, warehouse))
                .thenReturn(15l);

        targetFulfilmentStockService.reconcileStockLevelReservedQuantity(colourVariant, warehouse);

        Mockito.verify(targetFulfilmentStockLevelDao).reserve(Mockito.eq(stockLevelModel), Mockito.eq(15));
        Mockito.verify(targetFulfilmentStockLevelDao, Mockito.times(0)).release(Mockito.eq(stockLevelModel),
                Mockito.anyInt());
        Mockito.verify(stockLevelModel, Mockito.times(0)).setReserved(Mockito.anyInt());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testReconcileStockLevelReservedQuantityOver() {
        Mockito.when(targetFulfilmentStockLevelDao.calculateReservedStockAdjustment(colourVariant, warehouse))
                .thenReturn(-5l);

        targetFulfilmentStockService.reconcileStockLevelReservedQuantity(colourVariant, warehouse);

        Mockito.when(modelService.create(StockLevelHistoryEntryModel.class)).thenReturn(stockLevelHistoryEntryModel);

        Mockito.verify(targetFulfilmentStockLevelDao).release(stockLevelModel, 5);
        Mockito.verify(targetFulfilmentStockLevelDao, Mockito.times(0)).reserve(Mockito.eq(stockLevelModel),
                Mockito.anyInt());
        Mockito.verify(stockLevelModel, Mockito.times(0)).setReserved(Mockito.anyInt());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testReconcileStockLevelReservedQuantityCorrect() {
        Mockito.when(targetFulfilmentStockLevelDao.calculateReservedStockAdjustment(colourVariant, warehouse))
                .thenReturn(0l);

        targetFulfilmentStockService.reconcileStockLevelReservedQuantity(colourVariant, warehouse);

        Mockito.verify(targetFulfilmentStockLevelDao, Mockito.times(0)).reserve(Mockito.eq(stockLevelModel),
                Mockito.anyInt());
        Mockito.verify(targetFulfilmentStockLevelDao, Mockito.times(0)).release(Mockito.eq(stockLevelModel),
                Mockito.anyInt());
        Mockito.verify(stockLevelModel, Mockito.times(0)).setReserved(Mockito.anyInt());
    }
}

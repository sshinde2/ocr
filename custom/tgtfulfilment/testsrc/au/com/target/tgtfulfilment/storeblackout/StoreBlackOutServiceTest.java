/**
 * 
 */
package au.com.target.tgtfulfilment.storeblackout;



import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.Calendar;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import au.com.target.tgtfulfilment.storeblackout.impl.StoreBlackOutServiceImpl;


/**
 * @author Pradeep
 *
 */
@UnitTest
public class StoreBlackOutServiceTest {


    private final StoreBlackOutService storeBlockoutService = new StoreBlackOutServiceImpl();

    //In the below method comment A - After currentDate B - Before CurrentDate

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private Calendar startDateCal;
    private Calendar endDateCal;

    @Before
    public void setUp() {
        startDateCal = Calendar.getInstance();
        endDateCal = Calendar.getInstance();
    }

    //CHECKSTYLE:ON
    /**
     * B A
     */
    @Test
    public void testWhenStartDateisBeforeNowAndEndDateAfterNow() {
        startDateCal.add(Calendar.DATE, -10);
        endDateCal.add(Calendar.DATE, 20);
        Assert.assertTrue(storeBlockoutService.doesBlackOutPeriodApply(startDateCal.getTime(),
                endDateCal.getTime()));
    }

    /**
     * A A
     */
    @Test
    public void testWhenStartDateAfterNowEndDateAfterNow() {
        startDateCal.add(Calendar.DATE, 10);
        endDateCal.add(Calendar.DATE, 20);
        Assert.assertFalse(storeBlockoutService.doesBlackOutPeriodApply(startDateCal.getTime(),
                endDateCal.getTime()));
    }

    /**
     * A B
     */
    @Test
    public void testWhenStartDateAfterNowEndDateBeforeNow() {
        startDateCal.add(Calendar.DATE, 10);
        endDateCal.add(Calendar.DATE, -20);
        Assert.assertFalse(storeBlockoutService.doesBlackOutPeriodApply(startDateCal.getTime(),
                endDateCal.getTime()));
    }

    /**
     * null A
     */
    @Test
    public void testWhenStartDateNullAndStartDateNull() {
        endDateCal.add(Calendar.DATE, 20);
        Assert.assertTrue(storeBlockoutService.doesBlackOutPeriodApply(null, endDateCal.getTime()));
    }

    /**
     * null B
     */
    @Test
    public void testWhenStartDateNullAndEndDateBeforeNow() {
        endDateCal.add(Calendar.DATE, -10);
        Assert.assertFalse(storeBlockoutService.doesBlackOutPeriodApply(null, endDateCal.getTime()));
    }

    /**
     * B B
     */
    @Test
    public void testWhenStartDateBeforeAndEndDateBeforeNow() {
        startDateCal.add(Calendar.DATE, -20);
        endDateCal.add(Calendar.DATE, -10);
        Assert.assertFalse(storeBlockoutService.doesBlackOutPeriodApply(startDateCal.getTime(),
                endDateCal.getTime()));
    }


    /**
     * B null
     */
    @Test
    public void testWhenStartDateBeforeNowAndEndDateNull() {
        startDateCal.add(Calendar.DATE, -10);
        Assert.assertTrue(storeBlockoutService.doesBlackOutPeriodApply(startDateCal.getTime(), null));
    }


    /**
     * A null
     */
    @Test
    public void testWhenStartDateAfterNowAndEndDateNull() {
        startDateCal.add(Calendar.DATE, 10);
        Assert.assertFalse(storeBlockoutService.doesBlackOutPeriodApply(startDateCal.getTime(), null));
    }

    /**
     * null null
     */
    @Test
    public void testWhenStartDateNullAndEndDateNull() {
        Assert.assertFalse(storeBlockoutService.doesBlackOutPeriodApply(null, null));
    }

    // Test Boundary value Condition

    @Test
    public void testWhenStartDateTodayAndEndDateToday() {
        startDateCal.add(Calendar.SECOND, -10);
        endDateCal.add(Calendar.SECOND, 10);

        Assert.assertTrue(storeBlockoutService.doesBlackOutPeriodApply(startDateCal.getTime(),
                endDateCal.getTime()));
    }


    /**
     * Method to test validation when start date is before end date
     */
    @Test
    public void testOnValidateWhenStartDateIsBeforeEndDate() {
        startDateCal.add(Calendar.DATE, 1);
        endDateCal.add(Calendar.DATE, 5);
        boolean isValidationSuccessful = false;
        try {
            storeBlockoutService.validateDates(startDateCal.getTime(), endDateCal.getTime());
            isValidationSuccessful = true;
        }
        catch (final InterceptorException ie) {
            isValidationSuccessful = false;
        }
        Assert.assertTrue("Start date is before end date", isValidationSuccessful);
    }

    /**
     * Method to test validation when start date is after end date
     * 
     * @throws InterceptorException
     */
    @Test
    public void testOnValidateWhenStartDateIsAfterEndDate() throws InterceptorException {
        exception.expect(InterceptorException.class);
        exception.expectMessage("Invalid blackout start date, it should be either before or equal to end date.");
        startDateCal.add(Calendar.DATE, 2);
        endDateCal.add(Calendar.DATE, 1);
        storeBlockoutService.validateDates(startDateCal.getTime(), endDateCal.getTime());
    }

    /**
     * Method to test validation when start date equals end date and they are of a future date. (Since end date should
     * not be a past date)
     */
    @Test
    public void testOnValidateWhenStartDateEqualsEndDate() {
        startDateCal.add(Calendar.DATE, 1);
        boolean isValidationSuccessful = false;
        try {
            storeBlockoutService.validateDates(startDateCal.getTime(), startDateCal.getTime());
            isValidationSuccessful = true;
        }
        catch (final InterceptorException ie) {
            isValidationSuccessful = false;
        }
        Assert.assertTrue("Start date equals end date", isValidationSuccessful);
    }

    /**
     * Method to test validation when end date is missing
     */
    @Test
    public void testOnValidateWhenEndDateIsMissing() {
        startDateCal.add(Calendar.DATE, 1);
        boolean isValidationSuccessful = false;
        try {
            storeBlockoutService.validateDates(startDateCal.getTime(), null);
            isValidationSuccessful = true;
        }
        catch (final InterceptorException ie) {
            isValidationSuccessful = false;
        }
        Assert.assertTrue("End date is missing", isValidationSuccessful);
    }

    /**
     * Method to test validation when start date is missing
     */
    @Test
    public void testOnValidateWhenStartDateIsMissing() {
        endDateCal.add(Calendar.DATE, 1);
        boolean isValidationSuccessful = false;
        try {
            storeBlockoutService.validateDates(null, endDateCal.getTime());
            isValidationSuccessful = true;
        }
        catch (final InterceptorException ie) {
            isValidationSuccessful = false;
        }
        Assert.assertTrue("Start date is missing", isValidationSuccessful);
    }

    /**
     * Method to test validation when both the dates are missing
     */
    @Test
    public void testOnValidateWhenNoDateIsSet() {
        boolean isValidationSuccessful = false;
        try {
            storeBlockoutService.validateDates(null, null);
            isValidationSuccessful = true;
        }
        catch (final InterceptorException ie) {
            isValidationSuccessful = false;
        }
        Assert.assertTrue("No date is set", isValidationSuccessful);
    }

    /**
     * Method to test validation when end date is of the past
     * 
     * @throws InterceptorException
     */
    @Test
    public void testOnValidateWhenEndDateIsInPast() throws InterceptorException {
        exception.expect(InterceptorException.class);
        exception.expectMessage("Invalid blackout end date, it should be greater than present date.");
        startDateCal.add(Calendar.DATE, 1);
        endDateCal.add(Calendar.DATE, -3);
        storeBlockoutService.validateDates(startDateCal.getTime(), endDateCal.getTime());
    }

    /**
     * Method to test validation when end date is of the past
     * 
     * @throws InterceptorException
     */
    @Test
    public void testOnValidateWhenEndDateIsInPastAndStartDateIsMissing() throws InterceptorException {
        exception.expect(InterceptorException.class);
        exception.expectMessage("Invalid blackout end date, it should be greater than present date.");
        endDateCal.add(Calendar.DATE, -2);
        storeBlockoutService.validateDates(null, endDateCal.getTime());
    }

    /**
     * Method to test validation when start date and end date is of the past
     * 
     * @throws InterceptorException
     */
    @Test
    public void testOnValidateWhenStartAndEndDateIsInPast() throws InterceptorException {
        exception.expect(InterceptorException.class);
        exception.expectMessage("Invalid blackout start date, it should be greater than present date.");
        startDateCal.add(Calendar.DATE, -5);
        endDateCal.add(Calendar.DATE, 1);
        storeBlockoutService.validateDates(startDateCal.getTime(), startDateCal.getTime());
    }

    /**
     * Method to test validation when start date and end date is of the past
     * 
     * @throws InterceptorException
     */
    @Test
    public void testOnValidateWhenStartDateIsInPast() throws InterceptorException {
        exception.expect(InterceptorException.class);
        exception.expectMessage("Invalid blackout start date, it should be greater than present date.");
        startDateCal.add(Calendar.DATE, -5);
        endDateCal.add(Calendar.DATE, 10);
        storeBlockoutService.validateDates(startDateCal.getTime(), startDateCal.getTime());
    }

    /**
     * Method to test validation when start date and end date is of the past
     * 
     * @throws InterceptorException
     */
    @Test
    public void testOnValidateWhenStartDateIsInPastAndEndDateIsMissing() throws InterceptorException {
        exception.expect(InterceptorException.class);
        exception.expectMessage("Invalid blackout start date, it should be greater than present date.");
        startDateCal.add(Calendar.DATE, -5);
        storeBlockoutService.validateDates(startDateCal.getTime(), null);
    }

}

/**
 * 
 */
package au.com.target.tgtfulfilment.util;

import de.hybris.bootstrap.annotations.UnitTest;

import junit.framework.Assert;

import org.junit.Test;


/**
 * Unit tests for EscapeSpecialCharactersUtil
 * 
 * @author jjayawa1
 *
 */
@UnitTest
public class EscapeSpecialCharactersUtilTest {

    @Test
    public void testStringWithAmpersandAndHtmlEncoding() {
        final String converted = EscapeSpecialCharactersUtil.escapeSpecialHtmlCharacters("Jhonson & Jhonson");
        Assert.assertEquals("Jhonson &amp; Jhonson", converted);
    }

    @Test
    public void testStringWithApostraphyAndHtmlEncoding() {
        final String converted = EscapeSpecialCharactersUtil.escapeSpecialHtmlCharacters("o'reilly");
        Assert.assertEquals("o'reilly", converted);
    }

    @Test
    public void testStringWithAmpersandAndXMLEncoding() {
        final String converted = EscapeSpecialCharactersUtil.escapeSpecialXMLCharacters("Jhonson & Jhonson");
        Assert.assertEquals("Jhonson &amp; Jhonson", converted);
    }

    @Test
    public void testStringWithApostraphyAndXMLEncoding() {
        final String converted = EscapeSpecialCharactersUtil.escapeSpecialXMLCharacters("o'reilly");
        Assert.assertEquals("o&apos;reilly", converted);
    }
}

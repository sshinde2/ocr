/**
 * 
 */
package au.com.target.tgtfulfilment.warehouseservices.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.warehouse.dao.TargetWarehouseDao;


/**
 * @author ajit
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFulfilmentWarehouseServiceImplTest {

    private static final String TEST_WAREHOUSE = "testWareHouse";

    @InjectMocks
    private final TargetFulfilmentWarehouseServiceImpl targetFulfilmentWarehouseService = new TargetFulfilmentWarehouseServiceImpl();

    @Mock
    private WarehouseModel warehouse;

    @Mock
    private TargetWarehouseDao targetWarehouseDao;


    /**
     * Test get digital giftcard warehouse.
     */
    @Test
    public void testGetGiftcardWarehouse() {
        final TargetFulfilmentWarehouseServiceImpl targetFulfilmentWarehouseServiceSpy = Mockito
                .spy(targetFulfilmentWarehouseService);
        targetFulfilmentWarehouseServiceSpy.setGiftcardWarehouseCode(TEST_WAREHOUSE);
        Mockito.doReturn(warehouse).when(targetFulfilmentWarehouseServiceSpy).getWarehouseForCode(TEST_WAREHOUSE);
        assertThat(targetFulfilmentWarehouseServiceSpy.getGiftcardWarehouse()).isNotNull();
        assertThat(warehouse).isEqualTo(targetFulfilmentWarehouseServiceSpy.getGiftcardWarehouse());
    }

    /**
     * Test get digital giftcard warehouse as empty.
     */
    @Test
    public void testGetDigitalGiftcardEmptyWarehouse() {
        final TargetFulfilmentWarehouseServiceImpl targetFulfilmentWarehouseServiceSpy = Mockito
                .spy(targetFulfilmentWarehouseService);
        targetFulfilmentWarehouseServiceSpy.setGiftcardWarehouseCode(TEST_WAREHOUSE);
        Mockito.doReturn(null).when(targetFulfilmentWarehouseServiceSpy).getWarehouseForCode(TEST_WAREHOUSE);
        assertThat(targetFulfilmentWarehouseServiceSpy.getGiftcardWarehouse()).isNull();
    }

    /**
     * Test get digital giftcard null code passed.
     */
    @Test
    public void testGetDigitalGiftcardNullCodePassed() {
        final TargetFulfilmentWarehouseServiceImpl targetFulfilmentWarehouseServiceSpy = Mockito
                .spy(targetFulfilmentWarehouseService);
        targetFulfilmentWarehouseServiceSpy.setGiftcardWarehouseCode(null);
        Mockito.doReturn(null).when(targetFulfilmentWarehouseServiceSpy).getWarehouseForCode(null);
        assertThat(targetFulfilmentWarehouseServiceSpy.getGiftcardWarehouse()).isNull();
    }


}

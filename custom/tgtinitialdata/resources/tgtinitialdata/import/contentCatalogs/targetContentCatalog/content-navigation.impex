$contentCatalog=targetContentCatalog
$contentCV=catalogVersion(CatalogVersion.catalog(Catalog.id[default=$contentCatalog]),CatalogVersion.version[default=Staged])[default=$contentCatalog:Staged]

# *************************************************************
# CMS LINK COMPONENTS 
# *************************************************************

INSERT_UPDATE CMSLinkComponent;$contentCV[unique=true];uid[unique=true];name;url;&componentRef;target(code)[default='sameWindow'];styleAttributes[default=''];external[default=false];linkName[lang=en]
;;HomeLink;Home Link;;HomeLink;;;;Home
;;StoreFinderLink;Stores Link;/store-finder;StoreFinderLink;;;;Stores
;;CompanyLink;Company Link;/Company;CompanyLink;;;;Company
;;CatalogueLink;Catalogue Link;http://target.dynamiccatalogue.com.au/portal/;CatalogueLink;;;true;Catalogues
;;eNewsLink;eNews Link;/eNewsSubscription?accredit=newsletter;eNewsLink;;;;eNews
;;HelpLink;Help Link;/help;HelpLink;;;;Help
;;WhatsOnLink;What's On Link;http://www.target.com.au/html/whatson/whatson.htm;WhatsOnLink;;;true;What's On
;;DesignersForTargetLink;Designers For Target Link;http://www.designersfortarget.com.au/;DesignersForTargetLink;;;true;Designers For Target
;;ServicesLink;Services Link;http://www.target.com.au/html/services/services.htm;ServicesLink;;;true;Services
;;CreditLoyaltyCardsLink;Credit + Loyalty Cards Link;http://www.target.com.au/html/cards/cards.htm;CreditLoyaltyCardsLink;;;true;Credit + Loyalty Cards
;;PersonalShoppingLink;Personal Shopping Link;http://www.target.com.au/html/personalshopping/howitworks.htm;PersonalShoppingLink;;;true;Personal Shopping
;;PhotobooksLink;Photobooks Link;http://www.albumprinter.com.au/target-photobooks/;PhotobooksLink;;;true;Photobooks
;;ConductVacancySearchLink;Conduct a Vacancy Search Link;http://careers.target.com.au/caw/en/listing/;ConductVacancySearchLink;;;true;Conduct a Vacancy Search
;;ExistingCandidateLoginLink;Existing Candidate Login Link;https://secure.pageuppeople.com/apply/555/aw/applicationForm/;ExistingCandidateLoginLink;;;true;Existing Candidate Login
;;CompanyLink;Company Link;http://www.target.com.au/;CompanyLink;;;true;Company
;;AboutUsLink;About Us Link;http://www.target.com.au/html/aboutus/aboutus.htm;AboutUsLink;;;true;About Us

# *************************************************************
# CMS MODAL LINK COMPONENTS 
# *************************************************************

INSERT_UPDATE CMSLinkComponent;$contentCV[unique=true];uid[unique=true];name;&componentRef;contentPage(uid, $contentCV);linkName[lang=en];target(code)[default='sameWindow'];styleAttributes[default=''];external[default=false]
;;ModalFaqsLink;Modal FAQS Link;ModalFaqsLink;ModalFaqsPage;FAQs;modalContent;;
;;ModalPaymentDeliveryLink;Modal Payment Delivery Link;ModalPaymentDeliveryLink;ModalPaymentDeliveryPage;Payments + Delivery;modalContent;;
;;ModalRefundsReturnsLink;Modal Refunds Returns Link;ModalRefundsReturnsLink;ModalRefundsReturnsPage;Refunds + Returns;modalContent;;

# *************************************************************
# NAVIGATION ENTRIES (LINKS)
# *************************************************************

INSERT_UPDATE CMSNavigationEntry;$contentCV[unique=true];uid[unique=true];name;item(CMSLinkComponent.uid,CMSLinkComponent.$contentCV);&entryRef
;;HomeNavEntry;Home Nav Entry;HomeLink;;HomeNavEntry
;;eNewsNavEntry;eNews Nav Entry;eNewsLink;;eNewsNavEntry
;;eNewsSignUpNavEntry;eNews Nav Entry;eNewsLink;;eNewsSignUpNavEntry
;;WhatsOnNavEntry;What's On Nav Entry;WhatsOnLink;;WhatsOnNavEntry
;;DesignersForTargetNavEntry;Designers For Target Nav Entry;DesignersForTargetLink;;DesignersForTargetNavEntry
;;CatalogueNavEntry;Catalogue Nav Entry;CatalogueLink;;CatalogueNavEntry
;;TargetCatalogueNavEntry;Catalogue Nav Entry;CatalogueLink;;TargetCatalogueNavEntry
;;TargetCountryCatalogueNavEntry;Catalogue Nav Entry;CatalogueLink;;TargetCountryCatalogueNavEntry
;;ServicesNavEntry;Services Nav Entry;ServicesLink;;ServicesNavEntry
;;CreditLoyaltyCardsNavEntry;Credit + Loyalty Cards Nav Entry;CreditLoyaltyCardsLink;;CreditLoyaltyCardsNavEntry
;;PersonalShoppingNavEntry;Personal Shopping Nav Entry;PersonalShoppingLink;;PersonalShoppingNavEntry
;;PhotobooksNavEntry;Photobooks Nav Entry;PhotobooksLink;;PhotobooksNavEntry
;;ConductVacancySearchNavEntry;Conduct a Vacancy Search Nav Entry;ConductVacancySearchLink;;ConductVacancySearchNavEntry
;;ExistingCandidateLoginNavEntry;Existing Candidate Login Nav Entry;ExistingCandidateLoginLink;;ExistingCandidateLoginNavEntry

# Articles Nav Entries
;;ArticlesPageNavEntry;Articles Page Nav Entry;WhatsOnLink;;ArticlesPageNavEntry
;;CompetitionsPageNavEntry;Competitions Page Nav Entry;WhatsOnLink;;CompetitionsPageNavEntry

# Company Nav Entries
;;CompanyNavEntry;Company Nav Entry;CompanyLink;;CompanyNavEntry
;;AboutUsNavEntry;About Us Nav Entry;AboutUsLink;;AboutUsNavEntry

# *************************************************************
# NAVIGATION ENTRIES (PAGES)
# *************************************************************

INSERT_UPDATE CMSNavigationEntry;$contentCV[unique=true];uid[unique=true];name;item(ContentPage.uid,ContentPage.$contentCV);&entryRef

# Help Nav Entries
;;HelpNavEntry;Help Nav Entry;HelpPage;HelpNavEntry
;;ContactUsNavEntry;Contact Us Nav Entry;ContactUsPage;ContactUsNavEntry
;;FAQsNavEntry;FAQs Nav Entry;FAQsPage;FAQsNavEntry
;;DeliveryFAQsNavEntry;Delivery FAQs Nav Entry;DeliveryFAQsPage;DeliveryFAQsNavEntry
;;StandardLayByFAQsNavEntry;Standard Lay By FAQs Nav Entry;StandardLayByFAQsPage;StandardLayByFAQsNavEntry
;;ToySaleLayByFAQsNavEntry;Toy Sale Lay By FAQs Nav Entry;ToySaleLayByFAQsPage;ToySaleLayByFAQsNavEntry
;;ReturnsFAQsNavEntry;Returns FAQs Nav Entry;ReturnsFAQsPage;ReturnsFAQsNavEntry
;;CustomerServiceFAQsNavEntry;Customer Service FAQs Nav Entry;CustomerServiceFAQsPage;CustomerServiceFAQsNavEntry
;;PaymentDeliveryNavEntry;Payment Delivery Nav Entry;PaymentDeliveryPage;PaymentDeliveryNavEntry
;;OrderTrackingNavEntry;Order Tracking Nav Entry;OrderTrackingPage;OrderTrackingNavEntry
;;RefundsReturnsNavEntry;Refunds + Returns Nav Entry;RefundsReturnsPage;RefundsReturnsNavEntry
;;SizeChartsNavEntry;Size Charts Nav Entry;SizeChartsPage;SizeChartsNavEntry

# Company Nav Entries
;;CareersNavEntry;Careers Nav Entry;CareersPage;CareersNavEntry
;;OurTeamNavEntry;Our Team Nav Entry;OurTeamPage;OurTeamNavEntry
;;OurBenefitsNavEntry;Our Benefits Nav Entry;OurBenefitsPage;OurBenefitsNavEntry
;;OurCareerPathsNavEntry;Our Career Paths Nav Entry;OurCareerPathsPage;OurCareerPathsNavEntry
;;CareersMoreInfoNavEntry;Careers More Info Nav Entry;CareersMoreInfoPage;CareersMoreInfoNavEntry

# Other Nav Entries
#;;SitemapPageNavEntry;Sitemap Page Nav Entry;SitemapPage;SitemapPageNavEntry

# *************************************************************
# NAVIGATION NODES
# *************************************************************

# Base Navigation Nodes
INSERT_UPDATE CMSNavigationNode;$contentCV[unique=true];uid[unique=true];name;parent(uid, $contentCV);entries(CMSNavigationEntry.uid,CMSNavigationEntry.$contentCV);&nodeRef;title[lang=en]
;;SiteRootNode;Site Root Node;;;SiteRootNode;
;;SiteHomeNavNode;Target Site Content;SiteRootNode;HomeNavEntry;SiteHomeNavNode;

# eNews Navigation Nodes
;;eNewsNavNode;eNews;SiteHomeNavNode;eNewsNavEntry;eNewsNavNode;eNews
;;eNewsSignUpNavNode;eNews Sign Up;eNewsNavNode;eNewsSignUpNavEntry;eNewsSignUpNavNode;eNews Sign Up

# What's On Navigation Nodes
;;WhatsOnNavNode;What's On;SiteHomeNavNode;WhatsOnNavEntry;WhatsOnNavNode;What's On
;;ArticlesNavNode;Articles;WhatsOnNavNode;ArticlesPageNavEntry;ArticlesNavNode;Articles
;;DesignersForTargetNavNode;Designers For Target;WhatsOnNavNode;DesignersForTargetNavEntry;DesignersForTargetNavNode;Designers for Target
;;CompetitionsNavNode;Competitions;WhatsOnNavNode;CompetitionsPageNavEntry;CompetitionsNavNode;Competitions


# Catalogue Navigation Nodes
;;CatalogueNavNode;Catalogue;SiteHomeNavNode;CatalogueNavEntry;CatalogueNavNode;Catalogue
;;TargetCatalogueNavNode;Target Catalogue;CatalogueNavNode;TargetCatalogueNavEntry;TargetCatalogueNavNode;Target Catalogue
;;TargetCountryCatalogueNavNode;Target Country Catalogue;CatalogueNavNode;TargetCountryCatalogueNavEntry;TargetCountryCatalogueNavNode;Target Country Catalogue

# Services Navigation Nodes
;;ServicesNavNode;Services;SiteHomeNavNode;ServicesNavEntry;ServicesNavNode;Services
;;CreditLoyaltyCardsNavNode;Credit + Loyalty Cards;ServicesNavNode;CreditLoyaltyCardsNavEntry;CreditLoyaltyCardsNavNode;Credit + Loyalty Cards
;;PersonalShoppingNavNode;Personal Shopping;ServicesNavNode;PersonalShoppingNavEntry;PersonalShoppingNavNode;Personal Shopping
;;PhotobooksNavNode;Photobooks;ServicesNavNode;PhotobooksNavEntry;PhotobooksNavNode;Photobooks

# Company Navigation Nodes
;;CompanyNavNode;Company;SiteHomeNavNode;CompanyNavEntry;CompanyNavNode;Company
;;AboutUsNavNode;About Us;CompanyNavNode;AboutUsNavEntry;AboutUsNavNode;About Us
;;CareersNavNode;Careers;CompanyNavNode;CareersNavEntry;CareersNavNode;Careers
;;OurTeamNavNode;Our Team;CareersNavNode;OurTeamNavEntry;OurTeamNavNode;Our Team
;;OurBenefitsNavNode;Our Benefits;CareersNavNode;OurBenefitsNavEntry;OurBenefitsNavNode;Our Benefits
;;OurCareerPathsNavNode;Our Career Paths;CareersNavNode;OurCareerPathsNavEntry;OurCareerPathsNavNode;Our Career Paths
;;CareersMoreInfoNavNode;Careers More Info;CareersNavNode;CareersMoreInfoNavEntry;CareersMoreInfoNavNode;Find Out More

# Help Navigation Nodes
;;HelpNavNode;Help;SiteHomeNavNode;HelpNavEntry;HelpNavNode;Help
;;ContactUsNavNode;Contact Us;HelpNavNode;ContactUsNavEntry;ContactUsNavNode;Contact Us
;;FAQsNavNode;Shop Online FAQs;HelpNavNode;FAQsNavEntry;FAQsNavNode;Shop Online FAQs
;;DeliveryFAQsNavNode;Delivery FAQs;FAQsNavNode;DeliveryFAQsNavEntry;DeliveryFAQsNavNode;Delivery FAQs
;;StandardLayByFAQsNavNode;Standard Lay-By FAQs;FAQsNavNode;StandardLayByFAQsNavEntry;StandardLayByFAQsNavNode;Standard Lay-By FAQs
;;ToySaleLayByFAQsNavNode;Toy Sale Lay-By FAQs;FAQsNavNode;ToySaleLayByFAQsNavEntry;ToySaleLayByFAQsNavNode;Toysale Lay-By FAQs
;;ReturnsFAQsNavNode;Returns FAQs;FAQsNavNode;ReturnsFAQsNavEntry;ReturnsFAQsNavNode;Returns FAQs
;;CustomerServiceFAQsNavNode;Customer Service FAQs;FAQsNavNode;CustomerServiceFAQsNavEntry;CustomerServiceFAQsNavNode;Customer Service FAQs
;;PaymentDeliveryNavNode;Payment + Delivery;HelpNavNode;PaymentDeliveryNavEntry;PaymentDeliveryNavNode;Payment + Delivery
;;OrderTrackingNavNode;Order Tracking;HelpNavNode;OrderTrackingNavEntry;OrderTrackingNavNode;Order Tracking
;;RefundsReturnsNavNode;Refunds + Returns;HelpNavNode;RefundsReturnsNavEntry;RefundsReturnsNavNode;Refunds + Returns
;;SizeChartsNavNode;Size Charts;HelpNavNode;SizeChartsNavEntry;SizeChartsNavNode;Size Charts

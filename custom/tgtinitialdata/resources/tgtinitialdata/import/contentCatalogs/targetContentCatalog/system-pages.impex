# Import the CMS content for the Target site
#
$cvName=Staged
$contentCatalog=targetContentCatalog
$productCatalog=targetProductCatalog
$contentCV=catalogVersion(CatalogVersion.catalog(Catalog.id[default=$contentCatalog]),CatalogVersion.version[default=$cvName])[default=$contentCatalog:$cvName]
$productCV=catalogVersion(CatalogVersion.catalog(Catalog.id[default=$productCatalog]),CatalogVersion.version[default=$cvName])[default=$productCatalog:$cvName]
$jarResourceCms=jar:/tgtwebcore/import/cockpits/cmscockpit
$fileLoader=de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator

$contentPageResource=jar:au.com.target.tgtinitialdata.constants.TgtInitialDataConstants&/tgtinitialdata/import/contentCatalogs/targetContentCatalog/systemContent
$contentSupportPageResource=jar:au.com.target.tgtinitialdata.constants.TgtInitialDataConstants&/tgtinitialdata/import/contentCatalogs/targetContentCatalog/supportContent

# *************************************************************
# Add Template Level Items
# *************************************************************

INSERT_UPDATE ContentSlot;$contentCV[unique=true];uid[unique=true];cmsComponents(uid,$contentCV);
;;MyAccountMenuSlot;;
;;UtilityMenuSlot;StoreFinderLink,CatalogueLink;
;;SiteLogoSlot;;
;;MiniCartSlot;MiniCart;

# *************************************************************
# eNews Page
# *************************************************************

INSERT_UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=en][translator=de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
;;eNewsMainParagraph;$contentPageResource/enews.html

INSERT_UPDATE ContentSlot;$contentCV[unique=true];uid[unique=true];name;active;cmsComponents(uid,$contentCV);
;;MainSlot-enews-subscription;Main Slot for the eNews Subscription Page;true;eNewsMainParagraph;
;;AlternativeContentSlot-enews-subscription;Alternative Content Slot;true;;

INSERT_UPDATE ContentSlotForPage;$contentCV[unique=true];uid[unique=true];position[unique=true];page(uid,$contentCV)[unique=true];contentSlot(uid,$contentCV)[unique=true];
;;MainContent-enews-subscription;Main;enews-subscription;MainSlot-enews-subscription;
;;AlternativeContent-enews-subscription;Alternative;enews-subscription;AlternativeContentSlot-enews-subscription;

# *************************************************************
# Mum's Hub Personalise Page
# *************************************************************

INSERT_UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];name;content[lang=en][translator=de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
;;mumsHubPeronaliseMainParagraph;Mums Hub Personalise Content;$contentSupportPageResource/mums-hub-personalise.html

INSERT_UPDATE ContentSlot;$contentCV[unique=true];uid[unique=true];name;active;cmsComponents(uid,$contentCV);
;;MainSlot-mums-hub-personalise;Main Slot for the Mums Hub Personalise Page;true;mumsHubPeronaliseMainParagraph;

INSERT_UPDATE ContentSlotForPage;$contentCV[unique=true];uid[unique=true];position[unique=true];page(uid,$contentCV)[unique=true];contentSlot(uid,$contentCV)[unique=true];
;;MainContent-mums-hub-personalise;Main;mums-hub-personalise;MainSlot-mums-hub-personalise;


# *************************************************************
# My Account Landing Page
# *************************************************************

INSERT_UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=en][translator=de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
;;myAccountHomeSupplementParagraph;$contentPageResource/myAccountHomeHelp.html

INSERT_UPDATE NavigationComponent;$contentCV[unique=true];uid[unique=true];navigationNode(uid,$contentCV);
;;myAccountHomeHelpComponent;HelpNavNode;

INSERT_UPDATE ContentSlot;$contentCV[unique=true];uid[unique=true];cmsComponents(uid,$contentCV);
;;SupplementSlot-my-account-home;myAccountHomeSupplementParagraph, myAccountHomeHelpComponent;


# *************************************************************
# Product Grid Page
# *************************************************************

INSERT_UPDATE ContentSlotForPage;$contentCV[unique=true];uid[unique=true];position[unique=true];page(uid,$contentCV)[unique=true];contentSlot(uid,$contentCV)[unique=true];
;;ListerChampion-ProductGridPage;ListerChampion;productGrid;ListerChampionSlot;


# *************************************************************
# Default Brand Page
# *************************************************************

INSERT_UPDATE ContentSlot;$contentCV[unique=true];uid[unique=true];name;active;
;;ListerChampionSlot-DefaultBrandLandingPage;Champion Slot for the Default Brand Landing Page;true;

INSERT_UPDATE ContentSlotForPage;$contentCV[unique=true];uid[unique=true];position[unique=true];page(uid,$contentCV)[unique=true];contentSlot(uid,$contentCV)[unique=true];
;;ListerChampion-DefaultBrandLandingPage;ListerChampion;defaultBrandLandingPage;ListerChampionSlot-DefaultBrandLandingPage


# *************************************************************
# Search Page
# *************************************************************

INSERT_UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=en][translator=de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator];
;;searchEmptyContentParagraph;$contentPageResource/searchTips.html

INSERT_UPDATE ContentSlot;$contentCV[unique=true];uid[unique=true];name;active;cmsComponents(uid,$contentCV);
;;MainSlot-searchEmpty;Main Slot for the search Empty Page;true;searchEmptyContentParagraph;

# *************************************************************
# Product Details Page
# *************************************************************

INSERT_UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=en];
;;deliveryHomeDelivery;"Home delivery is {{auspost-standard}} including GST per order. Excluding items labeled &quot;Big and Bulky&quot; which incur a {{auspost-bulky}} including GST fee. If three or more Big and Bulky items are purchased, the total postage will be a flat fee of {{auspost-doublebulky}} including GST.";
;;deliveryCNC;"Our Click + Collect service is FREE, just nominate your preferred store to collect your order from during checkout. The store will contact you via email or SMS when your order is ready for collection, this is generally within 4-10 business days for buy now orders.";
;;deliveryInStorePurchase;"";

INSERT_UPDATE ContentSlot;$contentCV[unique=true];uid[unique=true];cmsComponents(uid,$contentCV);
;;LinksSlot;ModalFaqsLink,ModalPaymentDeliveryLink,ModalRefundsReturnsLink;
;;RelatedProductsSlot;RelatedProducts,CrossSellProductRecommendations;
;;DeliverySlot;deliveryInStorePurchase;

# *************************************************************
# Product Details Page (non default)
# *************************************************************

INSERT_UPDATE ProductPage;$contentCV[unique=true];uid[unique=true];name;masterTemplate(uid,$contentCV);defaultPage;showSocialLinks;approvalStatus(code)[default='approved']
;;productDetailsLite;Product Details Lite;ProductDetailsPageTemplate;false;false

# Restriction W138532
INSERT_UPDATE CMSCategoryRestriction;$contentCV[unique=true];uid[unique=true];name;recursive
;;toysRestriction;Toy Restriction;true

INSERT_UPDATE RestrictionsForPages;qualifier;source($contentCV,uid)[unique=true];target($contentCV,uid)[unique=true]
;RestrictionsForPages;productDetailsLite;toysRestriction

INSERT_UPDATE CategoriesForRestriction;qualifier;source($contentCV,uid)[unique=true];target($productCV,code)[unique=true]
;CategoriesForRestriction;toysRestriction;W138532

# *************************************************************
# Cart Page
# *************************************************************

INSERT_UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];name;content[lang=en][translator=de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator];
;;deliveryTermsComponent;Delivery Terms Paragraph;$contentPageResource/DeliveryTermsComponent.html
;;cNCErrorMessageComponent;Click and collect error message;$contentPageResource/CNCErrorMessageComponent.html

INSERT_UPDATE ContentSlot;$contentCV[unique=true];uid[unique=true];name;active;cmsComponents(uid,$contentCV);
;;MessagesSlot;Cart Page Message;true;deliveryTermsComponent,cNCErrorMessageComponent,deliveryCNC,deliveryHomeDelivery;


# *************************************************************
# Login / Checkout Landing Page
# *************************************************************

$checkoutLandingID=checkout-landing
$loginCheckoutID=checkout-landing-LoginContent
$registerCheckoutID=login-RegisterContent
$guestCheckoutID=checkout-landing-GuestContent
$guestCheckoutBulletsID=checkout-landing-GuestBullets
$thankYouID=thank-you
$loginID=login

INSERT_UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];name;content[lang=en][translator=de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]

#Checkout
;;$loginCheckoutID;Login Checkout Landing Content;$contentPageResource/checkout-pages/login_checkout.html
;;$registerCheckoutID;Register Checkout Landing Content;$contentPageResource/checkout-pages/register_checkout.html
;;$guestCheckoutID;Guest Checkout Landing Content;$contentPageResource/checkout-pages/guest_checkout.html

;;$checkoutLandingID-GuestBullets;Guest Checkout Landing Bullets Content;$contentPageResource/checkout-pages/guest_checkout_bullets.html
;;$checkoutLandingID-InlinePrivacy;Inline Privacy Content;$contentPageResource/checkout-pages/inline_privacy.html

#Login
;;$loginID-InlinePrivacy;Inline Privacy Content;$contentPageResource/checkout-pages/inline_privacy.html
;;$loginID-LoginContent;Login Landing;$contentPageResource/checkout-pages/login_landing.html

#Thank you
;;$thankYouID-RegisterContent;Register Landing Content;$contentPageResource/checkout-pages/thank_you_content.html

INSERT_UPDATE ContentSlot;$contentCV[unique=true];uid[unique=true];name;active;cmsComponents(uid,$contentCV)

#Checkout
;;GuestLandingSlot-$checkoutLandingID;Guest Checkout Landing Content Slot;true;$guestCheckoutID
;;GuestBulletsSlot-$checkoutLandingID;Guest Checkout Landing Bullets Content Slot;true;$checkoutLandingID-GuestBullets
;;LoginLandingSlot-$checkoutLandingID;Login Checkout Landing Content Slot;true;$loginCheckoutID
;;RegisterLandingSlot-$checkoutLandingID;Register Checkout Landing Content Slot;true;$registerCheckoutID
;;InlinePrivacySlot-$checkoutLandingID;Inline Privacy Content Slot;true;$checkoutLandingID-InlinePrivacy

#Login
;;LoginLandingSlot-$loginID;Login Landing Content Slot;true;$loginID-LoginContent
;;RegisterLandingSlot-$loginID;Register Landing Content Slot;true;login-RegisterContent
;;InlinePrivacySlot-$loginID;Inline Privacy Content Slot;true;$checkoutLandingID-InlinePrivacy

#Thank you
;;RegisterLandingSlot-$thankYouID;Register Landing Content Slot;true;$thankYouID-RegisterContent
;;InlinePrivacySlot-$thankYouID;Inline Privacy Content Slot;true;$checkoutLandingID-InlinePrivacy

INSERT_UPDATE ContentSlotForPage;$contentCV[unique=true];uid[unique=true];position[unique=true];page(uid,$contentCV)[unique=true];contentSlot(uid,$contentCV)[unique=true];;;

#Checkout
;;GuestLandingContent-$checkoutLandingID;GuestLanding;$checkoutLandingID;GuestLandingSlot-$checkoutLandingID;;;
;;GuestBulletsContent-$checkoutLandingID;GuestBullets;$checkoutLandingID;GuestBulletsSlot-$checkoutLandingID;;;
;;LoginLandingContent-$checkoutLandingID;LoginLanding;$checkoutLandingID;LoginLandingSlot-$checkoutLandingID;;;
;;RegisterLandingContent-$checkoutLandingID;RegisterLanding;$checkoutLandingID;RegisterLandingSlot-$checkoutLandingID;;;
;;InlinePrivacyContent-$checkoutLandingID;InlinePrivacy;$checkoutLandingID;InlinePrivacySlot-$checkoutLandingID;;;

#Login
;;LoginLandingContent-$loginID;LoginLanding;$loginID;LoginLandingSlot-$loginID;;;
;;RegisterLandingContent-$loginID;RegisterLanding;$loginID;RegisterLandingSlot-$loginID;;;
;;InlinePrivacyContent-$loginID;InlinePrivacy;$loginID;InlinePrivacySlot-$loginID;;;

#Thank you
;;RegisterLandingContent-$thankYouID;RegisterLanding;$thankYouID;RegisterLandingSlot-$thankYouID;;;
;;InlinePrivacyContent-$thankYouID;InlinePrivacy;$thankYouID;InlinePrivacySlot-$thankYouID;;;
/**
 * 
 */
package au.com.target.tgtinitialdata.processor;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.fest.util.Collections;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.TargetProductSearchService;
import au.com.target.tgtupdate.processor.TgtUpdateDepartmentDeliveryModeProcessor;


/**
 * This Class gets the products in a batch of 500 and calls the department- delivery mode api to update the department
 * based on judgment.
 * 
 * @author pthoma20
 * 
 */
public class InitializeProductsDepartmentDeliveryMode {

    private static final int BATCH_SIZE = 500;

    private static final Logger LOG = Logger.getLogger(InitializeProductsDepartmentDeliveryMode.class);

    private TargetProductSearchService targetProductSearchService;

    private TgtUpdateDepartmentDeliveryModeProcessor tgtUpdateDepartmentDeliveryModeProcessor;

    /**
     * This method will Iterate over the products and will get the target products
     */
    public void performProcessJob() {
        final int batchCursorPosition = 0;
        int batchCount = 1;
        List<TargetProductModel> targetProductModelList = null;
        //Batch Logic which will Loop and get the products in predefined batch and update them them.
        do {
            //Get all Target Products from Staged Catalog
            targetProductModelList = targetProductSearchService
                    .getAllTargetProductsWithNoDepartment(TgtCoreConstants.Catalog.OFFLINE_VERSION,
                            batchCursorPosition, BATCH_SIZE);

            if (Collections.isEmpty(targetProductModelList)) {
                if (batchCount == 1) {
                    LOG.error("No Products where fetched ");
                }
                break;
            }
            LOG.info("Executing Batch number (" + (batchCount++) + ") of size - " + targetProductModelList.size());
            tgtUpdateDepartmentDeliveryModeProcessor.processDepartmentDeliveryModeUpdate(targetProductModelList);

        }
        while (CollectionUtils.isNotEmpty(targetProductModelList));

    }

    /**
     * @param targetProductSearchService
     *            the targetProductSearchService to set
     */
    @Required
    public void setTargetProductSearchService(final TargetProductSearchService targetProductSearchService) {
        this.targetProductSearchService = targetProductSearchService;
    }

    /**
     * @param tgtUpdateDepartmentDeliveryModeProcessor
     *            the tgtUpdateDepartmentDeliveryModeProcessor to set
     */
    @Required
    public void setTgtUpdateDepartmentDeliveryModeProcessor(
            final TgtUpdateDepartmentDeliveryModeProcessor tgtUpdateDepartmentDeliveryModeProcessor) {
        this.tgtUpdateDepartmentDeliveryModeProcessor = tgtUpdateDepartmentDeliveryModeProcessor;
    }

}

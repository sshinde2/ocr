/**
 * 
 */
package au.com.target.tgtjms.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtsale.ist.service.TargetInterStoreTransferService;


/**
 * @author Nandini
 *
 */
public class TgtJmsInterStoreTransferAction extends AbstractProceduralAction<OrderProcessModel> {

    private OrderProcessParameterHelper orderProcessParameterHelper;
    private TargetInterStoreTransferService targetInterStoreTransferService;

    @Override
    public void executeAction(final OrderProcessModel orderProcess) throws RetryLaterException, Exception {
        Assert.notNull(orderProcess, "order process can not be null");

        final ConsignmentModel consignment = orderProcessParameterHelper.getConsignment(orderProcess);
        Assert.notNull(consignment, "consignment can not be null");

        final String consignmentCode = consignment.getCode();
        targetInterStoreTransferService.publishInterStoreTransferInfo(consignmentCode);
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }

    /**
     * @param targetInterStoreTransferService
     *            the targetInterStoreTransferService to set
     */
    @Required
    public void setTargetInterStoreTransferService(final TargetInterStoreTransferService targetInterStoreTransferService) {
        this.targetInterStoreTransferService = targetInterStoreTransferService;
    }

}

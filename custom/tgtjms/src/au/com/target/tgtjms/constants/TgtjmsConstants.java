/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtjms.constants;

/**
 * Global class for all Tgtjms constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class TgtjmsConstants extends GeneratedTgtjmsConstants {
    public static final String EXTENSIONNAME = "tgtjms";
    public static final String CNC_STORE_PREFIX = "TARGET";

    public static final String FEATURE_VALUES_IN_CONSIGNMENT_EXTRACT = "fastline.include.item.order.values";
    public static final String FEATURE_FASTLINE_ORDEREXTRACT_CONSIGNMENTID = "fastline.orderextract.consignmentid";

    private TgtjmsConstants() {
        //empty to avoid instantiating this constant class
    }

    // implement here constants used by this extension
}

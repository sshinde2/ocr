/**
 * 
 */
package au.com.target.tgtjms.jms.converter;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import java.io.StringWriter;
import java.text.MessageFormat;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.jms.support.converter.MessageConversionException;

import au.com.target.tgtcore.customer.impl.TargetCustomerEmailResolutionService;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtjms.jms.message.CncOrderExtractMessage;
import au.com.target.tgtjms.jms.model.CncOrderExtract;


/**
 * @author fkratoch
 * 
 */
public final class CncOrderExtractMessageConverter
{

    private static final Logger LOG = Logger.getLogger(CncOrderExtractMessage.class);
    private static final String PURCHASE_OPTION_BUYNOW = "buynow";
    private static JAXBContext cncOrderExtractJaxbContext = null;
    private static TargetFeatureSwitchService targetFeatureSwitchService = (TargetFeatureSwitchService)Registry
            .getApplicationContext().getBean("targetFeatureSwitchService");

    private CncOrderExtractMessageConverter()
    {
    }

    static
    {
        try
        {
            cncOrderExtractJaxbContext = JAXBContext.newInstance(CncOrderExtract.class);
        }
        catch (final JAXBException e)
        {
            LOG.error("Unable to create Jaxb context", e);
        }
    }

    public static String getMessagePayload(final CncOrderExtractMessage orderExtractMessage,
            final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService)
    {
        final OrderModel order = orderExtractMessage.getOrder();
        final TargetConsignmentModel consignment = orderExtractMessage.getConsignment();
        CncOrderExtract cncExtract;

        if (order != null) {
            LOG.info(MessageFormat.format("Convert order {0} to CncOrderExtractMessage.", order.getCode()));
            cncExtract = populateCncOrderExtractForCompletion(order, targetCustomerEmailResolutionService);
        }
        else if (consignment != null) {
            cncExtract = populateCncOrderExtractByConsignment(consignment, targetCustomerEmailResolutionService);
        }
        else {
            throw new IllegalArgumentException("OrderModel and consignment are null");
        }

        try
        {
            return marshalCncOrderExtract(cncExtract);
        }
        catch (final JAXBException e)
        {
            LOG.error("Exception while marshaling CncOrderExtract");
            throw new MessageConversionException("Exception while marshaling CncOrderExtract", e);
        }
    }

    private static CncOrderExtract populateCncOrderExtractForCompletion(final OrderModel orderModel,
            final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService)
    {
        checkIsDeliveryToStore(orderModel.getDeliveryMode(), orderModel.getCode());
        final CncOrderExtract orderExtract = new CncOrderExtract();
        populateFromOrder(orderModel, orderExtract, targetCustomerEmailResolutionService);
        orderExtract.setFulfilmentId(StringUtils.EMPTY);
        orderExtract.setParcelCount(Integer.valueOf(0));
        orderExtract.setOrderCompleted("Y");
        return orderExtract;
    }

    private static CncOrderExtract populateCncOrderExtractByConsignment(final TargetConsignmentModel consignment,
            final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService
            )
    {
        final OrderModel orderModel = (OrderModel)consignment.getOrder();
        if (orderModel == null) {
            throw new IllegalArgumentException("OrderModel is null");
        }
        checkIsDeliveryToStore(consignment.getDeliveryMode(), orderModel.getCode());

        final CncOrderExtract orderExtract = new CncOrderExtract();
        populateFromOrder(orderModel, orderExtract, targetCustomerEmailResolutionService);
        orderExtract.setParcelCount(consignment.getParcelCount());
        orderExtract.setFulfilmentId(consignment.getCode());
        if (targetFeatureSwitchService.isFeatureEnabled("cncOrderExtractOnConsignment")) {
            orderExtract.setOrderCompleted("N");
        }
        else {
            orderExtract.setOrderCompleted("Y");
        }
        return orderExtract;
    }

    private static void populateFromOrder(final OrderModel orderModel, final CncOrderExtract orderExtract,
            final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService) {
        orderExtract.setStoreNum(orderModel.getCncStoreNumber());
        orderExtract.setId(orderModel.getCode());

        final String purchaseOption = orderModel.getPurchaseOption().getCode();
        orderExtract.setType(purchaseOption.equalsIgnoreCase(PURCHASE_OPTION_BUYNOW) ? "P" : "L");

        if (orderModel.getDeliveryAddress().getTitle() != null) {
            orderExtract.setTitle(orderModel.getDeliveryAddress().getTitle().getCode());
        }
        orderExtract.setFirstName(orderModel.getDeliveryAddress().getFirstname());
        orderExtract.setSurname(orderModel.getDeliveryAddress().getLastname());
        final TargetCustomerModel customer = (TargetCustomerModel)orderModel.getUser();
        orderExtract.setEmail(targetCustomerEmailResolutionService.getEmailForCustomer(customer));
        orderExtract.setMobile(orderModel.getDeliveryAddress().getPhone1());
        orderExtract.setPalletNum(StringUtils.EMPTY);
        orderExtract.setPaymentStatus("C");
    }

    private static void checkIsDeliveryToStore(final DeliveryModeModel deliveryMode, final String orderCode) {
        if (!(deliveryMode instanceof TargetZoneDeliveryModeModel)) {
            LOG.info(MessageFormat.format(
                    "Order with num: {0} does not have a valid delivery model, exiting publish process.",
                    orderCode));
            throw new IllegalArgumentException("Delivery mode model is not a valid type.");
        }

        if (!((TargetZoneDeliveryModeModel)deliveryMode).getIsDeliveryToStore().booleanValue())
        {
            LOG.info(MessageFormat.format("Order with num: {0} is not CNC order, exiting publish process.",
                    orderCode));
            throw new IllegalArgumentException("Order is not CNC type order.");
        }
    }

    private static String marshalCncOrderExtract(final CncOrderExtract cncOrder) throws JAXBException
    {
        final StringWriter writer = new StringWriter();

        final Marshaller jaxbMarshaller = cncOrderExtractJaxbContext.createMarshaller();
        jaxbMarshaller.marshal(cncOrder, writer);
        return writer.toString();
    }
}
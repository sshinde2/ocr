package au.com.target.tgtjms.jms.converter;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.jms.support.converter.MessageConversionException;

import au.com.target.tgtcore.currency.conversion.service.CurrencyConversionFactorService;
import au.com.target.tgtcore.customer.impl.TargetCustomerEmailResolutionService;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtfulfilment.ConsignmentSentToWarehouseReason;
import au.com.target.tgtfulfilment.model.ConsignmentExtractRecordModel;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;
import au.com.target.tgtfulfilment.salesapplication.TargetFulfilmentSalesApplicationConfigService;
import au.com.target.tgtjms.constants.TgtjmsConstants;
import au.com.target.tgtjms.jms.message.FastlineWarehouseConsignmentExtractMessage;
import au.com.target.tgtjms.jms.model.ConsignmentExtract;
import au.com.target.tgtjms.jms.model.OrderExtractList;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;


/**
 * @author mmaki
 * 
 */
public final class ConsignmentExtractMessageConverter {

    private static final Logger LOG = Logger.getLogger(ConsignmentExtractMessageConverter.class);
    private static final String LOG_PREFIX_WARN_FASTLINE_EXTRACT = "WARN_FASTLINE_EXTRACT: ";

    private static final String DEFAULT_CUSTOMER_CODE = "ANON";
    private static final String CANCELLED_DESCRIPTION = "Cancelled Order";
    private static final String CANCELLED_EAN = "1111111111111";
    private static final String CANCELLED_ITEM_NUMBER = "11111111";
    private static final Long CANCELLED_QUANTITY = Long.valueOf(1);
    private static final String PURCHASE_OPTION_LAYBY = "layby";
    private static final String PURCHASE_OPTION_BUYNOW = "buynow";
    private static final String ORDER_TYPE_BUYNOW = "B1";
    private static final String ORDER_TYPE_LAYBY = "L3";
    private static final String COUNTRY_CODE1 = "\\+61";
    private static final String COUNTRY_CODE2 = "61";
    private static final String CONTAINS_CODE1 = "+61";
    private static final String CONTAINS_CODE2 = "61";
    private static final String REPLACE_PHONE_NO_PREFIX_CONSTANT = "0";
    private static final int SHIP_TO_PHONE_LENGTH = 10;
    private static JAXBContext orderExtractJaxbContext = null;

    static {
        try {
            orderExtractJaxbContext = JAXBContext.newInstance(OrderExtractList.class);
        }
        catch (final JAXBException e) {
            LOG.error("Unable to create Jaxb context", e);
        }
    }

    private ConsignmentExtractMessageConverter() {
        //do not instantiate
    }


    public static String getMessagePayload(final FastlineWarehouseConsignmentExtractMessage consignmentExtractMessage,
            final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService,
            final ConsignmentSentToWarehouseReason reason,
            final TargetFulfilmentSalesApplicationConfigService salesApplicationConfigService,
            final CurrencyConversionFactorService currencyConversionFactorService,
            final TargetFeatureSwitchService targetFeatureSwitchService) {
        final ConsignmentModel consignment = consignmentExtractMessage.getConsignment();

        if (consignment == null) {
            throw new IllegalArgumentException("ConsignmentModel is null");
        }

        final OrderModel order = (OrderModel)consignment.getOrder();
        if (order == null) {
            throw new IllegalArgumentException("order is null");
        }

        LOG.info("Converting to ConsignmentExtractMessage, consignmentCode=" + consignment.getCode());

        final OrderExtractList oeList;

        if (ConsignmentStatus.CANCELLED.equals(consignment.getStatus())
                || OrderStatus.REJECTED.equals(order.getStatus())) {
            oeList = getCancelledOrderExtract(consignment, targetCustomerEmailResolutionService, reason,
                    targetFeatureSwitchService);
        }
        else {
            oeList = getConsignmentExtractList(consignment, targetCustomerEmailResolutionService, reason,
                    salesApplicationConfigService, currencyConversionFactorService,
                    targetFeatureSwitchService);
        }

        final String payload;
        try {
            payload = marshalOrderExtractList(oeList);
        }
        catch (final JAXBException e) {
            LOG.error("Unable to marshal OrderExtractList");
            throw new MessageConversionException("Unable to marshal OrderExtractList", e);
        }

        return payload;
    }

    private static OrderExtractList getConsignmentExtractList(final ConsignmentModel consignment,
            final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService,
            final ConsignmentSentToWarehouseReason reason,
            final TargetFulfilmentSalesApplicationConfigService salesApplicationConfigService,
            final CurrencyConversionFactorService currencyConversionFactorService,
            final TargetFeatureSwitchService targetFeatureSwitchService) {
        final OrderExtractList oeList = new OrderExtractList();

        int cnt = 1;
        for (final ConsignmentEntryModel entry : consignment.getConsignmentEntries()) {
            final Long quantity = entry.getQuantity();
            if (quantity != null && quantity.longValue() > 0L) {
                oeList.addOrderExtract(getConsignmentExtract(consignment, entry, cnt,
                        targetCustomerEmailResolutionService,
                        reason, salesApplicationConfigService, currencyConversionFactorService,
                        targetFeatureSwitchService));
                cnt++;
            }
        }

        return oeList;
    }

    private static OrderExtractList getCancelledOrderExtract(final ConsignmentModel consignment,
            final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService,
            final ConsignmentSentToWarehouseReason reason,
            final TargetFeatureSwitchService targetFeatureSwitchService) {
        final OrderExtractList oeList = new OrderExtractList();
        final ConsignmentExtract orderExtract = new ConsignmentExtract();

        fillOrderInformation(orderExtract, consignment, targetCustomerEmailResolutionService, reason, null,
                targetFeatureSwitchService);

        orderExtract.setOrderLineNum(Integer.valueOf(1));
        orderExtract.setItemEAN(CANCELLED_EAN);
        orderExtract.setItemNumber(CANCELLED_ITEM_NUMBER);
        orderExtract.setItemDescription1(CANCELLED_DESCRIPTION);
        orderExtract.setItemDescription2("");
        orderExtract.setQuantityOrdered(CANCELLED_QUANTITY);

        oeList.addOrderExtract(orderExtract);
        return oeList;
    }

    private static ConsignmentExtract getConsignmentExtract(final ConsignmentModel consignment,
            final ConsignmentEntryModel entry,
            final int lineNumber, final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService,
            final ConsignmentSentToWarehouseReason reason,
            final TargetFulfilmentSalesApplicationConfigService salesApplicationConfigService,
            final CurrencyConversionFactorService currencyConversionFactorService,
            final TargetFeatureSwitchService targetFeatureSwitchService) {

        final ConsignmentExtract consignmentExtract = new ConsignmentExtract();

        fillOrderInformation(consignmentExtract, consignment, targetCustomerEmailResolutionService, reason,
                salesApplicationConfigService, targetFeatureSwitchService);

        fillItemInformation(consignmentExtract, entry, lineNumber, currencyConversionFactorService,
                targetFeatureSwitchService);

        return consignmentExtract;
    }

    private static String marshalOrderExtractList(final OrderExtractList orderExtractList) throws JAXBException {
        final StringWriter writer = new StringWriter();

        final Marshaller jaxbMarshaller = orderExtractJaxbContext.createMarshaller();
        jaxbMarshaller.marshal(orderExtractList, writer);
        return writer.toString();
    }

    private static void fillOrderInformation(final ConsignmentExtract consignmentExtract,
            final ConsignmentModel consignment,
            final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService,
            final ConsignmentSentToWarehouseReason reason,
            final TargetFulfilmentSalesApplicationConfigService salesApplicationConfigService,
            final TargetFeatureSwitchService targetFeatureSwitchService) {

        final OrderModel order = (OrderModel)consignment.getOrder();
        //Get the Target Customer associated with this order.
        final TargetCustomerModel customer = (TargetCustomerModel)order.getUser();
        //Get the delivery address.
        final AddressModel address = order.getDeliveryAddress();

        fillConsignmentId(consignment, targetFeatureSwitchService, consignmentExtract, reason);

        consignmentExtract.setOrderNumber(order.getCode());
        consignmentExtract.setDeliveryDate(null);
        final String custCode = (customer.getCustomerID() == null) ? DEFAULT_CUSTOMER_CODE : customer.getCustomerID()
                .toUpperCase();
        consignmentExtract.setCustomerCode(custCode);
        consignmentExtract.setCustomerName(getCustomerName(address));
        consignmentExtract.setShipToCode(custCode);
        consignmentExtract.setShipToLocName(null);
        consignmentExtract.setShipToLocAddr1(ConverterUtil.formatAddressLine(address.getStreetname(),
                order.getCncStoreNumber()));
        consignmentExtract.setShipToLocAddr2(address.getStreetnumber() != null ? address.getStreetnumber()
                .toUpperCase()
                : null);
        consignmentExtract.setShipToCity(address.getTown() != null ? address.getTown().toUpperCase() : null);
        consignmentExtract.setShipToState(address.getDistrict() != null ? address.getDistrict().toUpperCase() : null);
        consignmentExtract.setShipToPostCode(address.getPostalcode() != null ? address.getPostalcode().toUpperCase()
                : null);
        final String phoneNumber = getPhoneNumberToUse(order.getPaymentAddress(), address);

        consignmentExtract.setShipToEmail(targetCustomerEmailResolutionService.getEmailForCustomer(customer));
        consignmentExtract.setShipToCountry(address.getCountry() != null ? address.getCountry().getName().toUpperCase()
                : null);

        consignmentExtract.setOrderInstructions1(null);
        consignmentExtract.setOrderInstructions2(null);
        consignmentExtract.setOrderPurpose(reason.getCode());
        consignmentExtract.setOrderType(getOrderType(order));

        // for cancel order extract service will not be provided hence checking not null first
        if (null != salesApplicationConfigService
                && salesApplicationConfigService.isDenyShortPicks(order.getSalesApplication())) {
            consignmentExtract.setAllowShortPick("N");
        }

        if (consignment instanceof TargetConsignmentModel) {
            final TargetConsignmentModel targetConsignment = (TargetConsignmentModel)consignment;
            consignmentExtract.setCarrier(targetConsignment.getTargetCarrier().getWarehouseCode());
            consignmentExtract.setCarrierService(targetConsignment.getTargetCarrier().getServiceCode());

            final TargetCarrierModel targetCarrier = targetConsignment.getTargetCarrier();
            if (targetCarrier != null) {
                consignmentExtract.setOrderInstructions1(targetCarrier.getOrderInstructions1());
                consignmentExtract.setOrderInstructions2(targetCarrier.getOrderInstructions2());
            }

            //Process the phone number
            if ((null != targetCarrier) && (targetCarrier.getTransformPhoneNumber().booleanValue())
                    && (StringUtils.isNotEmpty(phoneNumber))) {
                consignmentExtract.setShipToPhone(processShipToPhone(phoneNumber));
            }
            else {
                consignmentExtract.setShipToPhone(phoneNumber);
            }


        }

    }

    /**
     * Method to fill ConsignmentId depending on the feature if newly sent otherwise check consignment extract record
     * and fill consignmentId with what was previously sent.
     * 
     * @param consignment
     * @param targetFeatureSwitchService
     * @param consignmentExtract
     * @param reason
     */
    private static void fillConsignmentId(final ConsignmentModel consignment,
            final TargetFeatureSwitchService targetFeatureSwitchService, final ConsignmentExtract consignmentExtract,
            final ConsignmentSentToWarehouseReason reason) {
        final ConsignmentExtractRecordModel consgExtractRecord = targetFeatureSwitchService
                .getConsExtractRecordByConsignment((TargetConsignmentModel)consignment);

        if (consgExtractRecord == null) {
            if (reason.equals(ConsignmentSentToWarehouseReason.NEW)) {
                fillConsignmentIdUsingFeatureAndSaveConsgExtRec(consignment, targetFeatureSwitchService,
                        consignmentExtract);
            }
            else {
                consignmentExtract.setConsignmentId(consignment.getOrder().getCode());
            }
        }
        else {
            fillConsignmentIdUsingConsgExtRecord(consignment, consignmentExtract,
                    consgExtractRecord.getConsignmentAsId());
        }

    }


    /**
     * Method to fill consignment ID using the feature 'fastline.orderextract.consignmentid' value and save
     * ConsignmentExtractRecord
     * 
     * @param consignment
     * @param targetFeatureSwitchService
     * @param consignmentExtract
     */
    private static void fillConsignmentIdUsingFeatureAndSaveConsgExtRec(final ConsignmentModel consignment,
            final TargetFeatureSwitchService targetFeatureSwitchService, final ConsignmentExtract consignmentExtract) {
        if (targetFeatureSwitchService
                .isFeatureEnabled(TgtjmsConstants.FEATURE_FASTLINE_ORDEREXTRACT_CONSIGNMENTID)) {
            consignmentExtract.setConsignmentId(consignment.getCode());
            targetFeatureSwitchService.saveConsignmentExtractRecord((TargetConsignmentModel)consignment, true);
        }
        else {
            consignmentExtract.setConsignmentId(consignment.getOrder().getCode());
            targetFeatureSwitchService.saveConsignmentExtractRecord((TargetConsignmentModel)consignment, false);
        }
    }


    /**
     * Method to fill consignment ID using 'ConsignmentAsId' in consignmentExtractRecord
     * 
     * @param consignment
     * @param consignmentExtract
     * @param consignmentAsId
     */
    private static void fillConsignmentIdUsingConsgExtRecord(final ConsignmentModel consignment,
            final ConsignmentExtract consignmentExtract, final Boolean consignmentAsId) {
        if (BooleanUtils.isTrue(consignmentAsId)) {
            consignmentExtract.setConsignmentId(consignment.getCode());
        }
        else {
            consignmentExtract.setConsignmentId(consignment.getOrder().getCode());
        }
    }

    /**
     * picks the phone number from the payment address and if it does not exist then picks from delivery address
     * 
     * @param billAddress
     *            - payment address
     * @param deliveryAddress
     *            - delivery address
     * @return String representing the phone number
     */
    private static String getPhoneNumberToUse(final AddressModel billAddress, final AddressModel deliveryAddress) {
        String phoneNumber = null;
        if (billAddress != null) {
            phoneNumber = billAddress.getPhone1() != null ? billAddress.getPhone1() : billAddress.getCellphone();
        }

        // Take contact number from shipping address if it is not present in Billing Address
        if (StringUtils.isEmpty(phoneNumber)) {
            phoneNumber = deliveryAddress.getPhone1() != null ? deliveryAddress.getPhone1() : deliveryAddress
                    .getCellphone();
        }
        return phoneNumber;
    }

    private static void fillItemInformation(final ConsignmentExtract consignmentExtract,
            final ConsignmentEntryModel entry, final int lineNumber,
            final CurrencyConversionFactorService currencyConversionFactorService,
            final TargetFeatureSwitchService targetFeatureSwitchService) {
        final AbstractOrderEntryModel orderEntry = entry.getOrderEntry();
        final ProductModel product = orderEntry.getProduct();
        consignmentExtract.setOrderLineNum(Integer.valueOf(lineNumber));
        consignmentExtract.setItemEAN(product.getEan());
        consignmentExtract.setItemNumber(product.getCode());
        consignmentExtract.setItemDescription1(product.getName() != null ? product.getName().toUpperCase() : null);
        consignmentExtract.setItemDescription2(null);
        consignmentExtract.setQuantityOrdered(entry.getQuantity());

        if (targetFeatureSwitchService.isFeatureEnabled(TgtjmsConstants.FEATURE_VALUES_IN_CONSIGNMENT_EXTRACT)) {
            final OrderModel order = (OrderModel)orderEntry.getOrder();

            // populating itemAmount and orderGoodsAmount
            if (currencyConversionFactorService.canConvertToBaseCurrency(order)) {
                consignmentExtract.setItemAmount(Double.valueOf(currencyConversionFactorService
                        .convertPriceToBaseCurrency(orderEntry.getTotalPrice().doubleValue(), order)));
                consignmentExtract.setOrderGoodsAmount(Double.valueOf(currencyConversionFactorService
                        .convertPriceToBaseCurrency(order.getSubtotal().doubleValue(), order)));
                if (order.getDeliveryCost() != null) {
                    consignmentExtract.setDeliveryFee(Double.valueOf(currencyConversionFactorService
                            .convertPriceToBaseCurrency(order.getDeliveryCost().doubleValue(), order)));
                }
            }
            else {
                LOG.warn("FOREIGN_CURRENCY order cannot be converted to base currency. order=" + order.getCode());
            }

            // populating item weights
            if (product instanceof AbstractTargetVariantProductModel) {
                final TargetProductDimensionsModel dimensions = ((AbstractTargetVariantProductModel)product)
                        .getProductPackageDimensions();

                if (null != dimensions && null != dimensions.getWeight()) {
                    if (Double.valueOf(0).equals(dimensions.getWeight())) {
                        LOG.warn(LOG_PREFIX_WARN_FASTLINE_EXTRACT + "Zero weight found for the product="
                                + product.getCode()
                                + ", order=" + order.getCode() + ", salesChannel=" + order.getSalesApplication());
                    }
                    // set the weight if it's not null
                    consignmentExtract.setItemWeight(dimensions.getWeight());
                }
                else {
                    LOG.warn(LOG_PREFIX_WARN_FASTLINE_EXTRACT + "Missing weight value for the product="
                            + product.getCode()
                            + ", order=" + order.getCode() + ", salesChannel=" + order.getSalesApplication());
                }
            }
        }
    }

    private static String getCustomerName(final AddressModel address) {
        final StringBuilder str = new StringBuilder();
        if (address.getFirstname() != null) {
            str.append(address.getFirstname());
            str.append(' ');
        }
        if (address.getLastname() != null) {
            str.append(address.getLastname());
        }
        return str.toString().toUpperCase();
    }

    private static String getOrderType(final OrderModel order) {
        final String purchaseOption = order.getPurchaseOption().getCode();
        if (!PURCHASE_OPTION_LAYBY.equalsIgnoreCase(purchaseOption)
                && !PURCHASE_OPTION_BUYNOW.equalsIgnoreCase(purchaseOption)) {
            final String errorMsg = "Purchase option " + purchaseOption
                    + " not allowed for fastline order extract. Order code: "
                    + order.getCode();
            LOG.error(errorMsg);
            throw new IllegalArgumentException(errorMsg);
        }

        final String orderType;
        final PurchaseOptionConfigModel activePurchaseOptionConfig = order.getPurchaseOptionConfig();
        if (activePurchaseOptionConfig != null && activePurchaseOptionConfig.getWarehouseCode() != null) {
            orderType = activePurchaseOptionConfig.getWarehouseCode();
        }
        else {
            if (PURCHASE_OPTION_LAYBY.equalsIgnoreCase(purchaseOption)) {
                orderType = ORDER_TYPE_LAYBY;
            }
            else {
                orderType = ORDER_TYPE_BUYNOW;
            }
        }

        return orderType;
    }

    /**
     * Method to process ShipToPhone number
     * 
     * @param phoneNumber
     * @return String
     */
    private static String processShipToPhone(final String phoneNumber) {

        String result = null != phoneNumber ? phoneNumber : StringUtils.EMPTY;

        if (StringUtils.isNotEmpty(phoneNumber)) {
            //Trim all spaces if any
            final String sanitzedString = org.springframework.util.StringUtils.trimAllWhitespace(phoneNumber);
            if (StringUtils.isNotEmpty(sanitzedString)) {
                //BR if +61 exits from the start of the string replace +61 with 0
                if (StringUtils.startsWith(sanitzedString, CONTAINS_CODE1)) {
                    result = sanitzedString.replaceFirst(COUNTRY_CODE1, REPLACE_PHONE_NO_PREFIX_CONSTANT);
                }
                //BR if 61 exits from the start of the string replace 61 with 0
                else if (StringUtils.startsWith(sanitzedString, CONTAINS_CODE2)) {
                    result = sanitzedString.replaceFirst(COUNTRY_CODE2, REPLACE_PHONE_NO_PREFIX_CONSTANT);
                }
            }
            //Ensure length is 10 characters,  and if more truncate the execess left most characters
            if (StringUtils.isNotBlank(result)) {
                if (result.length() > SHIP_TO_PHONE_LENGTH) {
                    //Reverse the string 
                    final StringBuilder sb = new StringBuilder(result).reverse();
                    //Read first 10 character, and reverse it back..this will get the first 10 characters from right hand side of the string 
                    return new StringBuilder(sb.substring(0, SHIP_TO_PHONE_LENGTH)).reverse().toString();
                }
            }

        }

        return result;

    }
}

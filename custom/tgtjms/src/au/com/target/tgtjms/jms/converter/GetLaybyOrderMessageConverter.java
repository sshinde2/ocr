/**
 * 
 */
package au.com.target.tgtjms.jms.converter;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtjms.jms.message.CncOrderExtractMessage;
import au.com.target.tgtjms.jms.model.LaybyOrderResponse;
import au.com.target.tgtsale.integration.dto.TargetLaybyOrder;
import au.com.target.tgtsale.integration.exception.UnsuccessfulESBOperationException;


/**
 * @author rsamuel3
 * 
 */
public final class GetLaybyOrderMessageConverter {
    private static final Logger LOG = Logger.getLogger(CncOrderExtractMessage.class);
    private static JAXBContext laybyOrderJaxbContext = null;

    static {
        try {
            laybyOrderJaxbContext = JAXBContext.newInstance(LaybyOrderResponse.class);
        }
        catch (final JAXBException e) {
            LOG.error("Unable to create Jaxb context", e);
        }
    }

    /**
     * not to be instantiated
     */
    private GetLaybyOrderMessageConverter() {
        //not to be instantiated
    }

    /**
     * unmarshals the response to get the POJO
     * 
     * @param xml
     *            received from esb
     * @return TargetLaybyOrder - ESB DTO
     * @throws UnsuccessfulESBOperationException
     *             when the xml is not parseble or when the call was unsuccessful
     */
    public static TargetLaybyOrder unmarshalResponse(final String xml) throws UnsuccessfulESBOperationException {

        final Unmarshaller jaxbMarshaller;
        LaybyOrderResponse response = null;
        try {
            jaxbMarshaller = laybyOrderJaxbContext.createUnmarshaller();
            response = (LaybyOrderResponse)jaxbMarshaller.unmarshal(new StringReader(xml));
        }
        catch (final JAXBException e) {
            throw new UnsuccessfulESBOperationException(
                    "ERR-GETLAYBYORDER-UNMARSHAL : exception thrown when trying to make sense of the response from esb",
                    xml, e);
        }
        if (response != null && response.isSuccess()) {
            return response.getTargetLaybyOrder();
        }
        throw new UnsuccessfulESBOperationException(response != null ? response.getMessage() : StringUtils.EMPTY,
                response != null ? response.getErrorCode() : StringUtils.EMPTY, StringUtils.EMPTY);
    }
}

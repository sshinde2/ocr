/**
 * 
 */
package au.com.target.tgtjms.jms.converter;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.jms.support.converter.MessageConversionException;

import au.com.target.tgtjms.jms.message.LaybyAddressMessage;
import au.com.target.tgtjms.jms.model.LaybyAddressExtract;
import au.com.target.tgtjms.util.JmsMessageHelper;


/**
 * @author fkratoch
 * 
 */
public final class LaybyAddressMessageConverter {

    private static final Logger LOG = Logger.getLogger(LaybyAddressMessageConverter.class);


    private static JAXBContext addressJaxbContext = null;

    private LaybyAddressMessageConverter()
    {
    }

    static
    {
        try
        {
            addressJaxbContext = JAXBContext.newInstance(LaybyAddressExtract.class);
        }
        catch (final JAXBException e)
        {
            LOG.error("Unable to create Jaxb context:" + e.getMessage(), e);
        }
    }

    public static String getMessagePayload(final LaybyAddressMessage laybyAddressMessage)
    {
        final OrderModel order = laybyAddressMessage.getOrder();

        if (order == null)
        {
            throw new IllegalArgumentException("OrderModel is null");
        }

        LOG.info("Converting order '{0}' to LaybyExtractMessage." + order.getCode());

        final LaybyAddressExtract orderExtract = populateLaybyAddressExtract(order);

        try
        {
            return marshalLaybyAddressExtract(orderExtract);
        }
        catch (final JAXBException e)
        {
            LOG.error("Exception while marshaling LaybyExtractMessage");
            throw new MessageConversionException("Exception while marshaling LaybyExtractMessage", e);
        }
    }

    private static LaybyAddressExtract populateLaybyAddressExtract(final OrderModel order) {
        final String orderNum = order.getCode();

        final LaybyAddressExtract extract = new LaybyAddressExtract();

        final AddressModel delAdd = order.getDeliveryAddress();
        extract.setAddrLine1(delAdd.getLine1() != null ? delAdd.getLine1() : StringUtils.EMPTY);
        extract.setAddrLine2(delAdd.getLine2() != null ? delAdd.getLine2() : StringUtils.EMPTY);
        extract.setCity(delAdd.getTown() != null ? delAdd.getTown() : StringUtils.EMPTY);
        extract.setPostCode(delAdd.getPostalcode() != null ? delAdd.getPostalcode() : StringUtils.EMPTY);
        extract.setPhone(delAdd.getPhone1() != null ? delAdd.getPhone1() : StringUtils.EMPTY);
        extract.setMobile(delAdd.getCellphone() != null ? delAdd.getCellphone() : StringUtils.EMPTY);

        if (delAdd.getCountry() != null) {
            extract.setCountry(delAdd.getCountry().getName() != null ? delAdd.getCountry().getName()
                    : StringUtils.EMPTY);
        }
        else {
            extract.setCountry(StringUtils.EMPTY);
        }

        extract.setStoreNum(JmsMessageHelper.getStoreNumber());
        extract.setLaybyNum(JmsMessageHelper.getLaybyNumForPos(orderNum));

        return extract;
    }



    private static String marshalLaybyAddressExtract(final LaybyAddressExtract address) throws JAXBException
    {
        final StringWriter writer = new StringWriter();

        final Marshaller jaxbMarshaller = addressJaxbContext.createMarshaller();
        jaxbMarshaller.marshal(address, writer);
        return writer.toString();
    }
}

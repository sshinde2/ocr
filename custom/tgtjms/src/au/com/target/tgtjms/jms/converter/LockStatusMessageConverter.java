/**
 * 
 */
package au.com.target.tgtjms.jms.converter;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import au.com.target.tgtjms.jms.model.LockStatus;


/**
 * Marshalls TLOG to xml using JAXB
 * 
 * @author rsamuel3
 * 
 */
public class LockStatusMessageConverter {

    private static JAXBContext context = null;

    /**
     * instantiates JAXBContext
     * 
     * @throws JAXBException
     *             thrwn if teh context could not be created
     * 
     */
    public LockStatusMessageConverter() throws JAXBException {
        context = JAXBContext.newInstance(LockStatus.class);
    }

    /**
     * returns a xml representation of Tlog
     * 
     * @param lockStatus
     *            object to be marshalled
     * @return xml String
     * @throws JAXBException
     *             error when something goes wrong with marshalling
     */
    public String getMessagePayload(final LockStatus lockStatus) throws JAXBException {
        return marshalLockStatusExtract(lockStatus);
    }

    /**
     * marshall the object
     * 
     * @param lockStatus
     *            of type Transaction
     * @return String representing the object
     * @throws JAXBException
     *             throws the exception when marshalling goes wrong
     */
    private static String marshalLockStatusExtract(final LockStatus lockStatus) throws JAXBException
    {
        final StringWriter writer = new StringWriter();

        final Marshaller jaxbMarshaller = context.createMarshaller();
        jaxbMarshaller.marshal(lockStatus, writer);
        return writer.toString();
    }

}

/**
 * 
 */
package au.com.target.tgtjms.jms.converter;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.log4j.Logger;
import org.springframework.jms.support.converter.MessageConversionException;

import au.com.target.tgtjms.jms.message.NullPriceProductsMessage;
import au.com.target.tgtjms.jms.model.ProductCodeList;


/**
 * @author mmaki
 * 
 */
public final class NullPriceProductsMessageConverter {

    private static final Logger LOG = Logger.getLogger(NullPriceProductsMessageConverter.class);

    private static JAXBContext jaxbContext = null;

    static {
        try {
            jaxbContext = JAXBContext.newInstance(ProductCodeList.class);
        }
        catch (final JAXBException e) {
            LOG.error("Unable to create Jaxb context", e);
        }
    }

    private NullPriceProductsMessageConverter() {
    }


    public static String getMessagePayload(final NullPriceProductsMessage nullPriceProductsMessage) {
        final ProductCodeList productCodes = nullPriceProductsMessage.getProductCodes();

        if (productCodes == null || productCodes.getProductCodes() == null
                || productCodes.getProductCodes().size() == 0) {
            throw new IllegalArgumentException("List of product codes is null");
        }

        LOG.info("Converting " + productCodes.getProductCodes().size() + " product codes to a JMS message");

        final String payload;
        try {
            payload = marshalProductList(productCodes);
        }
        catch (final JAXBException e) {
            LOG.error("Unable to marshal ProductCodeList");
            throw new MessageConversionException("Unable to marshal ProductCodeList", e);
        }
        return payload;
    }

    private static String marshalProductList(final ProductCodeList productCodes) throws JAXBException {
        final StringWriter writer = new StringWriter();

        final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.marshal(productCodes, writer);
        return writer.toString();
    }
}
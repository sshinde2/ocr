/**
 * 
 */
package au.com.target.tgtjms.jms.converter;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.io.StringWriter;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.jms.support.converter.MessageConversionException;

import au.com.target.tgtcore.customer.impl.TargetCustomerEmailResolutionService;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtjms.jms.message.ShipAdviceMessage;
import au.com.target.tgtjms.jms.model.ShipAdvice;


/**
 * @author mmaki
 * 
 */
public final class ShipAdviceMessageConverter {

    private static final Logger LOG = Logger.getLogger(ShipAdviceMessageConverter.class);

    private static final String DEFAULT_CUSTOMER_CODE = "ANON";

    private static JAXBContext shipAdviceJaxbContext = null;

    static {
        try {
            shipAdviceJaxbContext = JAXBContext.newInstance(ShipAdvice.class);
        }
        catch (final JAXBException e) {
            LOG.error("Unable to create Jaxb context", e);
        }
    }

    private ShipAdviceMessageConverter() {
    }


    public static String getMessagePayload(final ShipAdviceMessage shipAdviceMessage,
            final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService) {
        final OrderModel order = shipAdviceMessage.getOrder();

        if (order == null) {
            throw new IllegalArgumentException("OrderModel is null");
        }

        final ShipAdvice shipAdvice = getShipAdvice(order, shipAdviceMessage.getDeliveryDate(),
                targetCustomerEmailResolutionService);

        final String payload;
        try {
            payload = marshalShipAdvice(shipAdvice);
        }
        catch (final JAXBException e) {
            LOG.error("Unable to marshal ShipAdvice");
            throw new MessageConversionException("Unable to marshal ShipAdvice", e);
        }
        return payload;
    }

    private static ShipAdvice getShipAdvice(final OrderModel order, final Date deliveryDate,
            final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService) {
        final ShipAdvice shipAdvice = new ShipAdvice();
        //Get the Target Customer associated with this order.
        final TargetCustomerModel customer = (TargetCustomerModel)order.getUser();
        //Get the delivery address.
        final AddressModel address = order.getDeliveryAddress();
        final AddressModel paymentAddress = order.getPaymentAddress();

        shipAdvice.setOrderNumber(order.getCode());
        shipAdvice.setDeliveryDate(deliveryDate);
        final String custCode = (customer.getCustomerID() == null) ? DEFAULT_CUSTOMER_CODE : customer.getCustomerID()
                .toUpperCase();
        shipAdvice.setCustomerCode(custCode);
        final String customerName = address.getFirstname() + " " + address.getLastname();
        shipAdvice.setShipToLocName(customerName);
        shipAdvice
                .setShipToLocAddr1(ConverterUtil.formatAddressLine(address.getStreetname(), order.getCncStoreNumber()));
        shipAdvice.setShipToLocAddr2(address.getStreetnumber() != null
                ? address.getStreetnumber().toUpperCase()
                : null);
        shipAdvice.setShipToCity(address.getTown() != null ? address.getTown().toUpperCase() : null);
        shipAdvice.setShipToState(address.getDistrict() != null ? address.getDistrict().toUpperCase() : null);
        shipAdvice.setShipToPostCode(address.getPostalcode() != null ? address.getPostalcode().toUpperCase() : null);
        shipAdvice.setShipToCountry(address.getCountry() != null ? address.getCountry().getName().toUpperCase() : null);

        String phoneNumber = paymentAddress.getPhone1() != null ? paymentAddress.getPhone1() : paymentAddress
                .getCellphone();
        // Take contact number from shipping address if it is not present in Billing Address
        if (StringUtils.isEmpty(phoneNumber)) {
            phoneNumber = address.getPhone1() != null ? address.getPhone1() : address
                    .getCellphone();
        }

        shipAdvice.setShipToPhone(phoneNumber != null ? phoneNumber
                : StringUtils.EMPTY);

        final String emailAddress = targetCustomerEmailResolutionService.getEmailForCustomer(customer);
        shipAdvice.setShipToEmail(emailAddress != null ? emailAddress
                : StringUtils.EMPTY);

        return shipAdvice;
    }

    private static String marshalShipAdvice(final ShipAdvice shipAdvice) throws JAXBException {
        final StringWriter writer = new StringWriter();

        final Marshaller jaxbMarshaller = shipAdviceJaxbContext.createMarshaller();
        jaxbMarshaller.marshal(shipAdvice, writer);
        return writer.toString();
    }
}
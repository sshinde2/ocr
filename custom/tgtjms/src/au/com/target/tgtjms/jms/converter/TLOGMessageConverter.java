/**
 * 
 */
package au.com.target.tgtjms.jms.converter;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.springframework.jms.support.converter.MessageConversionException;

import au.com.target.tgtsale.tlog.data.TLog;


/**
 * Marshalls TLOG to xml using JAXB
 * 
 * @author rsamuel3
 * 
 */
public class TLOGMessageConverter {

    private static JAXBContext context = null;

    /**
     * instantiates JAXBContext
     * 
     * @throws JAXBException
     *             thrwn if teh context could not be created
     * 
     */
    public TLOGMessageConverter() throws JAXBException {
        context = JAXBContext.newInstance(TLog.class);
    }

    /**
     * returns a xml representation of Tlog
     * 
     * @param transaction
     *            object to be marshalled
     * @return xml String
     * @throws MessageConversionException
     *             error when something goes wrong with marshalling
     */
    public String getMessagePayload(final TLog transaction) throws MessageConversionException {
        try {
            return marshalTlogExtract(transaction);
        }
        catch (final JAXBException e) {
            throw new MessageConversionException("Unable to marshal TLog", e);
        }
    }

    /**
     * marshall the object
     * 
     * @param transaction
     *            of type Transaction
     * @return String representing the object
     * @throws JAXBException
     *             throws the exception when marshalling goes wrong
     */
    private static String marshalTlogExtract(final TLog transaction) throws JAXBException {
        final StringWriter writer = new StringWriter();
        final Marshaller jaxbMarshaller = context.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        jaxbMarshaller.marshal(transaction, writer);
        return writer.toString();
    }

}

/**
 * 
 */
package au.com.target.tgtjms.jms.converter;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;

import au.com.target.tgtjms.jms.message.AbstractMessage;


/**
 * @author mmaki
 * 
 */
public class TargetMessageConverter implements MessageConverter
{

    private static final Logger LOG = Logger.getLogger(TargetMessageConverter.class);

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.jms.support.converter.MessageConverter#toMessage(java.lang.Object, javax.jms.Session)
     */
    @Override
    public Message toMessage(final Object object, final Session session) throws JMSException,
            MessageConversionException
    {

        if (object == null)
        {
            throw new IllegalArgumentException("Passed object is null");
        }
        if (!(object instanceof AbstractMessage))
        {
            throw new IllegalArgumentException("Passed object is of wrong type (" + object.getClass() + ")");
        }
        if (LOG.isDebugEnabled())
        {
            LOG.debug("toMessage:  " + object);
        }
        final AbstractMessage input = (AbstractMessage)object;
        final TextMessage message = session.createTextMessage();
        message.setText(input.convertToMessagePayload());
        return message;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.jms.support.converter.MessageConverter#fromMessage(javax.jms.Message)
     */
    @Override
    public Object fromMessage(final Message paramMessage) throws JMSException, MessageConversionException
    {
        return null;
    }
}
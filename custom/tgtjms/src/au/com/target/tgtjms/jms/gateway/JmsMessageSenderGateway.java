/**
 * 
 */
package au.com.target.tgtjms.jms.gateway;

import java.util.Map;

import au.com.target.tgtjms.jms.message.AbstractMessage;




/**
 * @author fkratoch
 * 
 */
public interface JmsMessageSenderGateway
{

    void send(String queue, String message);

    void send(final String queue, final AbstractMessage message);

    void sendMessageWithHeaders(final String queue, final AbstractMessage message,
            final Map<String, String> headers);
}

/**
 *
 */
package au.com.target.tgtjms.jms.gateway.impl;

import java.util.Map;
import java.util.UUID;

import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.log4j.Logger;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.core.MessagePostProcessor;
import org.springframework.jms.core.SessionCallback;
import org.springframework.jms.core.support.JmsGatewaySupport;
import org.springframework.jms.support.JmsUtils;

import au.com.target.tgtjms.jms.gateway.JmsMessageDispatcher;
import au.com.target.tgtjms.jms.message.AbstractMessage;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * @author fkratoch
 */
public class TargetJmsMessageDispatcher extends JmsGatewaySupport implements JmsMessageDispatcher {

    private static final Logger LOG = Logger.getLogger(TargetJmsMessageDispatcher.class);

    private int timeout;

    @Override
    public void send(final String queue, final String message) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Sending jms message to the ESB platform to the queue:'" + queue);
        }

        getJmsTemplate().send(queue, new TextMessageCreator(message));
    }

    @Override
    public void send(final String queue, final AbstractMessage message) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Sending jms message to the ESB platform to the queue:'" + queue + "' of type:'"
                    + message.getClass().getName());
        }
        getJmsTemplate().convertAndSend(queue, message);
    }

    @Override
    public void sendMessageWithHeaders(final String queue, final AbstractMessage message,
            final Map<String, String> headers) {
        LOG.info("Sending jms message to the ESB platform to the queue:'" + queue + "' of type:'"
                + message.getClass().getName());
        getJmsTemplate().convertAndSend(queue, message, new MessagePostProcessor() {

            @Override
            public Message postProcessMessage(final Message message) throws JMSException {
                for (final String key : headers.keySet()) {
                    message.setStringProperty(key, headers.get(key));
                }
                return message;
            }
        });

    }

    @Override
    public Message sendAndReceive(final String requestQueue, final String message, final String responseQueue) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Starting to send message: " + message);
        }
        final long startTime = System.currentTimeMillis();
        final Message response = getJmsTemplate().execute(
                new TargetJmsProducerConsumer(requestQueue, message, responseQueue), true);

        if (LOG.isDebugEnabled()) {
            if (response != null) {
                LOG.debug("Response received in " + (System.currentTimeMillis() - startTime) + " milliseconds");

            }
            else {
                LOG.debug("Timeout in " + (System.currentTimeMillis() - startTime) + " milliseconds");
            }
        }
        return response;
    }

    /**
     * Returns the timeout value used by message consumer upon receive operation.
     * 
     * @return the message receive timeout
     */
    public int getTimeout() {
        return timeout;
    }

    /**
     * Sets the timeout value used by message consumer upon receive operation.
     * 
     * @param timeout
     *            the timeout value to set
     */
    public void setTimeout(final int timeout) {
        this.timeout = timeout;
    }

    /**
     * String to text message converter.
     * 
     * @see TextMessage
     */
    class TextMessageCreator implements MessageCreator {

        private final String message;

        /**
         * Creates a new text message converted.
         * 
         * @param message
         *            the message body
         */
        private TextMessageCreator(final String message) {
            this.message = message;
        }

        @Override
        public Message createMessage(final Session session) throws JMSException {
            final TextMessage textMessage = session.createTextMessage();
            textMessage.setText(message);
            return textMessage;
        }

    }


    /**
     * Session callback implementation that sends and immediately attempts to consume a message.
     */
    protected class TargetJmsProducerConsumer implements SessionCallback<Message> {

        private final String requestQueueName;
        private final String responseQueueName;
        private final String messageBody;

        /**
         * Creates new jms producer/consumer based on {@code requestQueue}, {@code messageBody} and
         * {@code responseQueueName}.
         * 
         * @param requestQueueName
         *            the queue to be used to sending data
         * @param messageBody
         *            the text message body
         * @param responseQueueName
         *            the queue to be used for retrieving data
         */
        public TargetJmsProducerConsumer(final String requestQueueName, final String messageBody,
                final String responseQueueName) {
            this.requestQueueName = requestQueueName;
            this.messageBody = messageBody;
            this.responseQueueName = responseQueueName;
        }

        /**
         * Creates non-persistent text message with unique correlation ID, sends it to the outgoing queue and returns
         * response from incoming queue, if anything was received during {@link #getTimeout()} period. Sent message has
         * TTL corresponding to the response wait timeout, which means that if message consumer on the other side (ESB)
         * will not be available at the moment, it will not receive an expired message.
         * 
         * @param session
         *            the JMS session
         * @return the message received
         * @throws JMSException
         *             if thrown by JMS API methods
         */
        @Override
        public Message doInJms(final Session session) throws JMSException {
            MessageConsumer consumer = null;
            MessageProducer producer = null;
            QueueBrowser browser = null;
            MessageConsumer remover = null;
            try {
                final String correlationId = UUID.randomUUID().toString();
                final Queue requestQueue = new ActiveMQQueue(requestQueueName);
                final Queue responseQueue = responseQueueName == null
                        ? session.createTemporaryQueue()
                        : new ActiveMQQueue(responseQueueName);
                final String messageSelector = "JMSCorrelationID = '" + correlationId + "'";

                // Create the consumer first
                consumer = session.createConsumer(responseQueue, messageSelector);
                final TextMessage textMessage = session.createTextMessage(messageBody);
                textMessage.setJMSCorrelationID(correlationId);
                textMessage.setJMSReplyTo(responseQueue);
                // Send the request second
                producer = session.createProducer(requestQueue);
                LOG.info("Sending message " + messageBody + " to " + requestQueueName
                        + " with correlationID " + correlationId);
                producer.send(requestQueue, textMessage,
                        DeliveryMode.NON_PERSISTENT, Message.DEFAULT_PRIORITY, 0);

                // Block on receiving the response with a timeout
                final Message message = consumer.receive(getTimeout());
                if (message == null) {
                    browser = session.createBrowser(requestQueue, messageSelector);
                    // browse outgoing queue to find "request" message
                    if (browser.getEnumeration().hasMoreElements()) {
                        remover = session.createConsumer(requestQueue, messageSelector);
                        final Message request = remover.receive(100); // remove "request" message
                        if (request == null) {
                            LOG.error(SplunkLogFormatter.formatMessage(
                                    "Message was consumed by ESB right before Hybris attempted to remove it",
                                    TgtutilityConstants.ErrorCode.ERR_GENERIC, messageBody));
                        }
                    }
                    else {
                        LOG.error(SplunkLogFormatter.formatMessage(
                                "Message was consumed by ESB but we didn't receive response after certain timeout",
                                TgtutilityConstants.ErrorCode.ERR_GENERIC, messageBody));
                    }
                }
                return message;
            }
            finally {
                JmsUtils.closeMessageConsumer(consumer);
                JmsUtils.closeMessageProducer(producer);
                JmsUtils.closeQueueBrowser(browser);
                JmsUtils.closeMessageConsumer(remover);
            }
        }

    }
}

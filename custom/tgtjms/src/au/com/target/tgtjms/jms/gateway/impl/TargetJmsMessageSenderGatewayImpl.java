/**
 * 
 */
package au.com.target.tgtjms.jms.gateway.impl;

import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.core.MessagePostProcessor;
import org.springframework.jms.core.support.JmsGatewaySupport;

import au.com.target.tgtjms.jms.gateway.JmsMessageSenderGateway;
import au.com.target.tgtjms.jms.message.AbstractMessage;


/**
 * @author fkratoch
 * 
 */
public class TargetJmsMessageSenderGatewayImpl extends JmsGatewaySupport implements JmsMessageSenderGateway
{

    private static final Logger LOG = Logger.getLogger(TargetJmsMessageSenderGatewayImpl.class);

    @Override
    public void send(final String queue, final String message)
    {

        LOG.info("Sending jms message to the ESB platform to the queue:'" + queue);

        getJmsTemplate().send(queue, new TextMessageCreator(message));
    }

    /*
     * (non-Javadoc)
     * 
     * @see au.com.target.tgtjms.jms.gateway.JmsMessageSenderGateway#send(java.lang.String,
     * au.com.target.tgtjms.jms.message.AbstractMessage)
     */
    @Override
    public void send(final String queue, final AbstractMessage message)
    {
        LOG.info("Sending jms message to the ESB platform to the queue:'" + queue + "' of type:'"
                + message.getClass().getName());
        getJmsTemplate().convertAndSend(queue, message);

    }

    /*
     * (non-Javadoc)
     * 
     * @see au.com.target.tgtjms.jms.gateway.JmsMessageSenderGateway#send(java.lang.String,
     * au.com.target.tgtjms.jms.message.AbstractMessage)
     */
    @Override
    public void sendMessageWithHeaders(final String queue, final AbstractMessage message,
            final Map<String, String> headers)
    {
        LOG.info("Sending jms message to the ESB platform to the queue:'" + queue + "' of type:'"
                + message.getClass().getName());
        getJmsTemplate().convertAndSend(queue, message, new MessagePostProcessor() {

            @Override
            public Message postProcessMessage(final Message message) throws JMSException {
                for (final String key : headers.keySet()) {
                    message.setStringProperty(key, headers.get(key));
                }
                return message;
            }
        });

    }

    private static class TextMessageCreator implements MessageCreator {
        private final String message;

        private TextMessageCreator(final String message) {
            this.message = message;
        }

        @Override
        public Message createMessage(final Session session) throws JMSException
        {
            final TextMessage textMessage = session.createTextMessage();
            textMessage.setText(message);
            return textMessage;
        }

    }
}

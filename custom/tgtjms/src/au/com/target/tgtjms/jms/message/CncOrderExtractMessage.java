/**
 * 
 */
package au.com.target.tgtjms.jms.message;

import de.hybris.platform.core.model.order.OrderModel;

import au.com.target.tgtcore.customer.impl.TargetCustomerEmailResolutionService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtjms.jms.converter.CncOrderExtractMessageConverter;


/**
 * @author fkratoch
 * 
 */
public class CncOrderExtractMessage extends AbstractMessage
{

    private OrderModel order;

    private TargetConsignmentModel consignment;

    private final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService;

    public CncOrderExtractMessage(final OrderModel order, final TargetConsignmentModel consignment,
            final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService)
    {
        this.order = order;
        this.consignment = consignment;
        this.targetCustomerEmailResolutionService = targetCustomerEmailResolutionService;
    }

    /**
     * @return the order
     */
    public OrderModel getOrder()
    {
        return order;
    }

    /**
     * @param order
     *            the order to set
     */
    public void setOrder(final OrderModel order)
    {
        this.order = order;
    }

    /**
     * @return the consignment
     */
    public TargetConsignmentModel getConsignment() {
        return consignment;
    }

    /**
     * @param consignment
     *            the consignment to set
     */
    public void setConsignment(final TargetConsignmentModel consignment) {
        this.consignment = consignment;
    }

    /*
     * (non-Javadoc)
     * 
     * @see au.com.target.tgtutility.jms.message.AbstractMessage#convertToMessagePayload()
     */
    @Override
    public String convertToMessagePayload()
    {
        return CncOrderExtractMessageConverter.getMessagePayload(this, targetCustomerEmailResolutionService);
    }
}

/**
 * 
 */
package au.com.target.tgtjms.jms.message;

import de.hybris.platform.core.Registry;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import au.com.target.tgtcore.currency.conversion.service.CurrencyConversionFactorService;
import au.com.target.tgtcore.customer.impl.TargetCustomerEmailResolutionService;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtfulfilment.ConsignmentSentToWarehouseReason;
import au.com.target.tgtfulfilment.salesapplication.TargetFulfilmentSalesApplicationConfigService;
import au.com.target.tgtjms.jms.converter.ConsignmentExtractMessageConverter;



/**
 * @author mmaki
 * 
 */
public class FastlineWarehouseConsignmentExtractMessage extends AbstractConsignmentExtractMessage {

    public FastlineWarehouseConsignmentExtractMessage(final ConsignmentModel consignment,
            final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService,
            final ConsignmentSentToWarehouseReason reason) {
        super(consignment, targetCustomerEmailResolutionService, reason);
    }

    @Override
    public String convertToMessagePayload() {

        return ConsignmentExtractMessageConverter.getMessagePayload(this, getTargetCustomerEmailResolutionService(),
                getReason(), getTargetFulfilmentSalesApplicationConfigService(), getCurrencyConversionFactorService(),
                getTargetFeatureSwitchService());
    }

    /**
     * Gets the target fulfulment sales application config service.
     *
     * @return the target fulfulment sales application config service
     */
    protected TargetFulfilmentSalesApplicationConfigService getTargetFulfilmentSalesApplicationConfigService() {
        return (TargetFulfilmentSalesApplicationConfigService)Registry.getApplicationContext()
                .getBean("targetFulfilmentSalesApplicationConfigService");
    }

    /**
     * Gets the currency conversion factor service.
     *
     * @return the currency conversion factor service
     */
    protected CurrencyConversionFactorService getCurrencyConversionFactorService() {
        return (CurrencyConversionFactorService)Registry.getApplicationContext()
                .getBean("currencyConversionFactorService");
    }

    /**
     * Gets the target feature switch service.
     *
     * @return the target feature switch service
     */
    protected TargetFeatureSwitchService getTargetFeatureSwitchService() {
        return (TargetFeatureSwitchService)Registry.getApplicationContext()
                .getBean("targetFeatureSwitchService");
    }
}

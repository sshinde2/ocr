/**
 * 
 */
package au.com.target.tgtjms.jms.message;

import de.hybris.platform.core.model.order.OrderModel;

import au.com.target.tgtjms.jms.converter.LaybyAddressMessageConverter;


/**
 * @author fkratoch
 * 
 */
public class LaybyAddressMessage extends AbstractMessage {

    private OrderModel order;

    public LaybyAddressMessage(final OrderModel order)
    {
        this.order = order;
    }

    /**
     * @return the order
     */
    public OrderModel getOrder()
    {
        return order;
    }

    /**
     * @param order
     *            the order to set
     */
    public void setOrder(final OrderModel order)
    {
        this.order = order;
    }

    /*
     * (non-Javadoc)
     * 
     * @see au.com.target.tgtutility.jms.message.AbstractMessage#convertToMessagePayload()
     */
    @Override
    public String convertToMessagePayload()
    {
        return LaybyAddressMessageConverter.getMessagePayload(this);
    }

}

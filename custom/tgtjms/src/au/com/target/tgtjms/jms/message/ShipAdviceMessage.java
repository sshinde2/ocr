/**
 * 
 */
package au.com.target.tgtjms.jms.message;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.Date;

import au.com.target.tgtcore.customer.impl.TargetCustomerEmailResolutionService;
import au.com.target.tgtjms.jms.converter.ShipAdviceMessageConverter;



/**
 * @author mmaki
 * 
 */
public class ShipAdviceMessage extends AbstractMessage
{

    private OrderModel order;
    private Date deliveryDate;
    private final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService;

    public ShipAdviceMessage(final OrderModel order, final Date deliveryDate,
            final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService)
    {
        this.order = order;
        this.deliveryDate = deliveryDate;
        this.targetCustomerEmailResolutionService = targetCustomerEmailResolutionService;
    }

    /**
     * @return the order
     */
    public OrderModel getOrder()
    {
        return order;
    }

    /**
     * @param order
     *            the order to set
     */
    public void setOrder(final OrderModel order)
    {
        this.order = order;
    }

    /**
     * @return the deliveryDate
     */
    public Date getDeliveryDate()
    {
        return deliveryDate;
    }

    /**
     * @param deliveryDate
     *            the deliveryDate to set
     */
    public void setDeliveryDate(final Date deliveryDate)
    {
        this.deliveryDate = deliveryDate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see au.com.target.tgtutility.jms.message.AbstractMessage#convertToMessagePayload()
     */
    @Override
    public String convertToMessagePayload()
    {
        return ShipAdviceMessageConverter.getMessagePayload(this, targetCustomerEmailResolutionService);
    }

}

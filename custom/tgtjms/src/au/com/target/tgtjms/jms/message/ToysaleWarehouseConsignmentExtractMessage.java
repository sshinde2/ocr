/**
 * 
 */
package au.com.target.tgtjms.jms.message;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import au.com.target.tgtcore.customer.impl.TargetCustomerEmailResolutionService;
import au.com.target.tgtfulfilment.ConsignmentSentToWarehouseReason;
import au.com.target.tgtjms.jms.converter.LongtermLaybyConsignmentExtractMessageConverter;


/**
 * @author fkratoch
 * 
 */
public class ToysaleWarehouseConsignmentExtractMessage extends AbstractConsignmentExtractMessage
{

    public ToysaleWarehouseConsignmentExtractMessage(final ConsignmentModel consignment,
            final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService,
            final ConsignmentSentToWarehouseReason reason)
    {
        super(consignment, targetCustomerEmailResolutionService, reason);
    }

    @Override
    public String convertToMessagePayload()
    {
        return LongtermLaybyConsignmentExtractMessageConverter.getMessagePayload(this, getReason());
    }

}

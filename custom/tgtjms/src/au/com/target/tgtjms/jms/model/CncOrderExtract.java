/**
 * 
 */
package au.com.target.tgtjms.jms.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import au.com.target.tgtjms.util.CNCMobileNumberFormatter;


/**
 * @author fkratoch
 * 
 */
@XmlRootElement(name = "order")
@XmlAccessorType(XmlAccessType.FIELD)
public class CncOrderExtract implements Serializable
{

    @XmlAttribute
    private Integer storeNum;

    @XmlAttribute
    private String id;

    @XmlAttribute
    private String type;

    @XmlElement
    private String fulfilmentId;

    @XmlElement
    private String title;

    @XmlElement
    private String firstName;

    @XmlElement
    private String surname;

    @XmlElement
    private String email;

    @XmlElement
    @XmlJavaTypeAdapter(CNCMobileNumberFormatter.class)
    private String mobile;

    @XmlElement
    private Integer parcelCount;

    @XmlElement
    private String orderCompleted;

    @XmlElement
    private String palletNum;

    @XmlElement
    private String paymentStatus;


    /**
     * @return the storeNumber
     */
    public Integer getStoreNum()
    {
        return storeNum;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNum(final Integer storeNumber)
    {
        this.storeNum = storeNumber;
    }

    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final String id)
    {
        this.id = id;
    }

    /**
     * @return the type
     */
    public String getType()
    {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final String type)
    {
        this.type = type;
    }

    /**
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(final String title)
    {
        this.title = title;
    }

    /**
     * @return the firstName
     */
    public String getFirstName()
    {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName)
    {
        this.firstName = firstName;
    }

    /**
     * @return the surname
     */
    public String getSurname()
    {
        return surname;
    }

    /**
     * @param surname
     *            the surname to set
     */
    public void setSurname(final String surname)
    {
        this.surname = surname;
    }

    /**
     * @return the email
     */
    public String getEmail()
    {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(final String email)
    {
        this.email = email;
    }

    /**
     * @return the mobile
     */
    public String getMobile()
    {
        return mobile;
    }

    /**
     * @param mobile
     *            the mobile to set
     */
    public void setMobile(final String mobile)
    {
        this.mobile = mobile;
    }

    /**
     * @return the parcelCount
     */
    public Integer getParcelCount()
    {
        return parcelCount;
    }

    /**
     * @param parcelCount
     *            the parcelCount to set
     */
    public void setParcelCount(final Integer parcelCount)
    {
        this.parcelCount = parcelCount;
    }

    /**
     * @return the palletNumber
     */
    public String getPalletNum()
    {
        return palletNum;
    }

    /**
     * @param palletNum
     *            the palletNumber to set
     */
    public void setPalletNum(final String palletNum)
    {
        this.palletNum = palletNum;
    }

    /**
     * @return the paymentStatus
     */
    public String getPaymentStatus()
    {
        return paymentStatus;
    }

    /**
     * @param paymentStatus
     *            the paymentStatus to set
     */
    public void setPaymentStatus(final String paymentStatus)
    {
        this.paymentStatus = paymentStatus;
    }

    /**
     * @return the fulfilmentId
     */
    public String getFulfilmentId() {
        return fulfilmentId;
    }

    /**
     * @param fulfilmentId
     *            the fulfilmentId to set
     */
    public void setFulfilmentId(final String fulfilmentId) {
        this.fulfilmentId = fulfilmentId;
    }

    /**
     * @return the orderCompleted
     */
    public String getOrderCompleted() {
        return orderCompleted;
    }

    /**
     * @param orderCompleted
     *            the orderCompleted to set
     */
    public void setOrderCompleted(final String orderCompleted) {
        this.orderCompleted = orderCompleted;
    }

}

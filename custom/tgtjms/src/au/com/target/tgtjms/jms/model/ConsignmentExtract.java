/**
 * 
 */
package au.com.target.tgtjms.jms.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "integration-orderExtract")
@XmlAccessorType(XmlAccessType.FIELD)
public class ConsignmentExtract implements Serializable {

    private static final long serialVersionUID = 1L;

    @XmlElement
    private String orderNumber;

    @XmlElement
    private Date deliveryDate;

    @XmlElement
    private String customerCode;

    @XmlElement
    private String customerName;

    @XmlElement
    private String shipToCode;

    @XmlElement
    private String shipToLocName;

    @XmlElement
    private String shipToLocAddr1;

    @XmlElement
    private String shipToLocAddr2;

    @XmlElement
    private String shipToCity;

    @XmlElement
    private String shipToState;

    @XmlElement
    private String shipToCountry;

    @XmlElement
    private String shipToPhone;

    @XmlElement
    private String shipToEmail;

    @XmlElement
    private String shipToPostCode;

    @XmlElement
    private Integer orderLineNum;

    @XmlElement
    private String itemEAN;

    @XmlElement
    private String itemNumber;

    @XmlElement
    private String itemDescription1;

    @XmlElement
    private String itemDescription2;

    @XmlElement
    private Long quantityOrdered;

    @XmlElement
    private String orderInstructions1;

    @XmlElement
    private String orderInstructions2;

    @XmlElement
    private String carrier;

    @XmlElement
    private String carrierService;

    @XmlElement
    private String orderPurpose;

    @XmlElement
    private String orderType;

    @XmlElement
    private String allowShortPick = "Y";

    @XmlElement
    private Double itemAmount;

    @XmlElement
    private Double orderGoodsAmount;

    @XmlElement
    private Double itemWeight;

    @XmlElement
    private String consignmentId;

    @XmlElement
    private Double deliveryFee;


    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(final String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(final Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(final String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(final String customerName) {
        this.customerName = customerName;
    }

    public String getShipToCode() {
        return shipToCode;
    }

    public void setShipToCode(final String shipToCode) {
        this.shipToCode = shipToCode;
    }

    public String getShipToLocName() {
        return shipToLocName;
    }

    public void setShipToLocName(final String shipToLocName) {
        this.shipToLocName = shipToLocName;
    }

    public String getShipToLocAddr1() {
        return shipToLocAddr1;
    }

    public void setShipToLocAddr1(final String shipToLocAddr1) {
        this.shipToLocAddr1 = shipToLocAddr1;
    }

    public String getShipToLocAddr2() {
        return shipToLocAddr2;
    }

    public void setShipToLocAddr2(final String shipToLocAddr2) {
        this.shipToLocAddr2 = shipToLocAddr2;
    }

    public String getShipToCity() {
        return shipToCity;
    }

    public void setShipToCity(final String shipToCity) {
        this.shipToCity = shipToCity;
    }

    public String getShipToState() {
        return shipToState;
    }

    public void setShipToState(final String shipToState) {
        this.shipToState = shipToState;
    }

    public String getShipToCountry() {
        return shipToCountry;
    }

    public void setShipToCountry(final String shipToCountry) {
        this.shipToCountry = shipToCountry;
    }

    public String getShipToPostCode() {
        return shipToPostCode;
    }

    public void setShipToPostCode(final String shipToPostCode) {
        this.shipToPostCode = shipToPostCode;
    }

    public Integer getOrderLineNum() {
        return orderLineNum;
    }

    public void setOrderLineNum(final Integer orderLineNum) {
        this.orderLineNum = orderLineNum;
    }

    public String getItemEAN() {
        return itemEAN;
    }

    /**
     * @return the shipToPhone
     */
    public String getShipToPhone() {
        return shipToPhone;
    }

    /**
     * @param shipToPhone
     *            the shipToPhone to set
     */
    public void setShipToPhone(final String shipToPhone) {
        this.shipToPhone = shipToPhone;
    }

    /**
     * @return the shipToEmail
     */
    public String getShipToEmail() {
        return shipToEmail;
    }

    /**
     * @param shipToEmail
     *            the shipToEmail to set
     */
    public void setShipToEmail(final String shipToEmail) {
        this.shipToEmail = shipToEmail;
    }

    /**
     * @return the carrierService
     */
    public String getCarrierService() {
        return carrierService;
    }

    /**
     * @param carrierService
     *            the carrierService to set
     */
    public void setCarrierService(final String carrierService) {
        this.carrierService = carrierService;
    }

    public void setItemEAN(final String itemEAN) {
        this.itemEAN = itemEAN;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(final String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getItemDescription1() {
        return itemDescription1;
    }

    public void setItemDescription1(final String itemDescription1) {
        this.itemDescription1 = itemDescription1;
    }

    public String getItemDescription2() {
        return itemDescription2;
    }

    public void setItemDescription2(final String itemDescription2) {
        this.itemDescription2 = itemDescription2;
    }

    public Long getQuantityOrdered() {
        return quantityOrdered;
    }

    public void setQuantityOrdered(final Long quantityOrdered) {
        this.quantityOrdered = quantityOrdered;
    }

    public String getOrderInstructions1() {
        return orderInstructions1;
    }

    public void setOrderInstructions1(final String orderInstructions1) {
        this.orderInstructions1 = orderInstructions1;
    }

    public String getOrderInstructions2() {
        return orderInstructions2;
    }

    public void setOrderInstructions2(final String orderInstructions2) {
        this.orderInstructions2 = orderInstructions2;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(final String carrier) {
        this.carrier = carrier;
    }

    public String getOrderPurpose() {
        return orderPurpose;
    }

    public void setOrderPurpose(final String orderPurpose) {
        this.orderPurpose = orderPurpose;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(final String orderType) {
        this.orderType = orderType;
    }

    public String getAllowShortPick() {
        return allowShortPick;
    }

    public void setAllowShortPick(final String allowShortPick) {
        this.allowShortPick = allowShortPick;
    }


    /**
     * @return the itemAmount
     */
    public Double getItemAmount() {
        return itemAmount;
    }

    /**
     * @param itemAmount
     *            the itemAmount to set
     */
    public void setItemAmount(final Double itemAmount) {
        this.itemAmount = itemAmount;
    }

    /**
     * @return the orderGoodsAmount
     */
    public Double getOrderGoodsAmount() {
        return orderGoodsAmount;
    }

    /**
     * @param orderGoodsAmount
     *            the orderGoodsAmount to set
     */
    public void setOrderGoodsAmount(final Double orderGoodsAmount) {
        this.orderGoodsAmount = orderGoodsAmount;
    }

    /**
     * @return the item weight
     */
    public Double getItemWeight() {
        return itemWeight;
    }

    /**
     * @param itemWeight
     *            the new item weight
     */
    public void setItemWeight(final Double itemWeight) {
        this.itemWeight = itemWeight;
    }

    /**
     * @return the consigmnmentId
     */
    public String getConsignmentId() {
        return consignmentId;
    }

    /**
     * @param consignmentId
     *            the consigmnmentId to set
     */
    public void setConsignmentId(final String consignmentId) {
        this.consignmentId = consignmentId;
    }

    /**
     * @return the deliveryFee
     */
    public Double getDeliveryFee() {
        return deliveryFee;
    }

    /**
     * @param deliveryFee
     *            the deliveryFee to set
     */
    public void setDeliveryFee(final Double deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

}
/**
 * 
 */
package au.com.target.tgtjms.jms.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author fkratoch
 * 
 */
@XmlRootElement(name = "order")
@XmlAccessorType(XmlAccessType.FIELD)
public class LaybyAddressExtract implements Serializable {

    @XmlElement
    private String laybyNum;

    @XmlElement
    private String storeNum;

    @XmlElement
    private String addrLine1;

    @XmlElement
    private String addrLine2;

    @XmlElement
    private String city;

    @XmlElement
    private String country;

    @XmlElement
    private String postCode;

    @XmlElement
    private String phone;

    @XmlElement
    private String mobile;

    /**
     * @return the laybyNum
     */
    public String getLaybyNum() {
        return laybyNum;
    }

    /**
     * @param laybyNum
     *            the laybyNum to set
     */
    public void setLaybyNum(final String laybyNum) {
        this.laybyNum = laybyNum;
    }

    /**
     * @return the storeNum
     */
    public String getStoreNum() {
        return storeNum;
    }

    /**
     * @param storeNum
     *            the storeNum to set
     */
    public void setStoreNum(final String storeNum) {
        this.storeNum = storeNum;
    }

    /**
     * @return the addrLine1
     */
    public String getAddrLine1() {
        return addrLine1;
    }

    /**
     * @param addrLine1
     *            the addrLine1 to set
     */
    public void setAddrLine1(final String addrLine1) {
        this.addrLine1 = addrLine1;
    }

    /**
     * @return the addrLine2
     */
    public String getAddrLine2() {
        return addrLine2;
    }

    /**
     * @param addrLine2
     *            the addrLine2 to set
     */
    public void setAddrLine2(final String addrLine2) {
        this.addrLine2 = addrLine2;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city
     *            the city to set
     */
    public void setCity(final String city) {
        this.city = city;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country
     *            the country to set
     */
    public void setCountry(final String country) {
        this.country = country;
    }

    /**
     * @return the postCode
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * @param postCode
     *            the postCode to set
     */
    public void setPostCode(final String postCode) {
        this.postCode = postCode;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     *            the phone to set
     */
    public void setPhone(final String phone) {
        this.phone = phone;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile
     *            the mobile to set
     */
    public void setMobile(final String mobile) {
        this.mobile = mobile;
    }

}

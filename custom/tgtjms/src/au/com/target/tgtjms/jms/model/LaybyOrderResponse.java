/**
 * 
 */
package au.com.target.tgtjms.jms.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import au.com.target.tgtsale.integration.dto.TargetLaybyOrder;


/**
 * @author rsamuel3
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LaybyOrderResponse {

    @XmlElement
    private TargetLaybyOrder targetLaybyOrder;

    @XmlElement
    private boolean success;

    @XmlElement
    private String message;

    @XmlElement
    private String errorCode;

    /**
     * 
     */
    public LaybyOrderResponse() {
        // TODO Auto-generated constructor stub
    }

    public TargetLaybyOrder getTargetLaybyOrder() {
        return targetLaybyOrder;
    }

    public void setTargetLaybyOrder(final TargetLaybyOrder targetLaybyOrder) {
        this.targetLaybyOrder = targetLaybyOrder;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(final boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * @param errorCode
     *            the errorCode to set
     */
    public void setErrorCode(final String errorCode) {
        this.errorCode = errorCode;
    }

}

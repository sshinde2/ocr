/**
 * 
 */
package au.com.target.tgtjms.jms.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * @author rsamuel3
 * 
 */
@XmlType(name = "lockMode")
@XmlEnum
public enum LockEnum {
    @XmlEnumValue("D")
    DIRECT,
    @XmlEnumValue("I")
    INDIRECT,
    @XmlEnumValue("C")
    CLEAR;
}

/**
 * 
 */
package au.com.target.tgtjms.jms.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author fkratoch
 * 
 */
@XmlRootElement(name = "integration-longtermOrderExtract")
@XmlAccessorType(XmlAccessType.FIELD)
public class LongtermLaybyConsignmentExtract implements Serializable {

    @XmlElement
    private String orderNumber;

    @XmlElement
    private Date deliveryDate;

    @XmlElement
    private String customerCode;

    @XmlElement
    private String customerName;

    @XmlElement
    private String shipToCode;

    @XmlElement
    private String shipToLocName;

    @XmlElement
    private String shipToLocAddr1;

    @XmlElement
    private String shipToLocAddr2;

    @XmlElement
    private String shipToCity;

    @XmlElement
    private String shipToState;

    @XmlElement
    private String shipToCountry;

    @XmlElement
    private String shipToPostCode;

    @XmlElement
    private String orderInstructions1;

    @XmlElement
    private String orderInstructions2;

    @XmlElement
    private String carrier;

    @XmlElement
    private String orderPurpose;

    @XmlElement
    private String orderType;

    @XmlElementWrapper(name = "orderItems")
    @XmlElement(name = "orderItem")
    private List<OrderItemExtract> orderItems;

    public LongtermLaybyConsignmentExtract() {
        super();
        orderItems = new ArrayList<>();
    }

    /**
     * @return the orderItems
     */
    public List<OrderItemExtract> getOrderItems() {
        return orderItems;
    }

    /**
     * @param orderItems
     *            the orderItems to set
     */
    public void setOrderItems(final List<OrderItemExtract> orderItems) {
        this.orderItems = orderItems;
    }

    public String getOrderNumber()
    {
        return orderNumber;
    }

    public void setOrderNumber(final String orderNumber)
    {
        this.orderNumber = orderNumber;
    }

    public Date getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setDeliveryDate(final Date deliveryDate)
    {
        this.deliveryDate = deliveryDate;
    }

    public String getCustomerCode()
    {
        return customerCode;
    }

    public void setCustomerCode(final String customerCode)
    {
        this.customerCode = customerCode;
    }

    public String getCustomerName()
    {
        return customerName;
    }

    public void setCustomerName(final String customerName)
    {
        this.customerName = customerName;
    }

    public String getShipToCode()
    {
        return shipToCode;
    }

    public void setShipToCode(final String shipToCode)
    {
        this.shipToCode = shipToCode;
    }

    public String getShipToLocName()
    {
        return shipToLocName;
    }

    public void setShipToLocName(final String shipToLocName)
    {
        this.shipToLocName = shipToLocName;
    }

    public String getShipToLocAddr1()
    {
        return shipToLocAddr1;
    }

    public void setShipToLocAddr1(final String shipToLocAddr1)
    {
        this.shipToLocAddr1 = shipToLocAddr1;
    }

    public String getShipToLocAddr2()
    {
        return shipToLocAddr2;
    }

    public void setShipToLocAddr2(final String shipToLocAddr2)
    {
        this.shipToLocAddr2 = shipToLocAddr2;
    }

    public String getShipToCity()
    {
        return shipToCity;
    }

    public void setShipToCity(final String shipToCity)
    {
        this.shipToCity = shipToCity;
    }

    public String getShipToState()
    {
        return shipToState;
    }

    public void setShipToState(final String shipToState)
    {
        this.shipToState = shipToState;
    }

    public String getShipToCountry()
    {
        return shipToCountry;
    }

    public void setShipToCountry(final String shipToCountry)
    {
        this.shipToCountry = shipToCountry;
    }

    public String getShipToPostCode()
    {
        return shipToPostCode;
    }

    public void setShipToPostCode(final String shipToPostCode)
    {
        this.shipToPostCode = shipToPostCode;
    }

    public String getOrderInstructions1()
    {
        return orderInstructions1;
    }

    public void setOrderInstructions1(final String orderInstructions1)
    {
        this.orderInstructions1 = orderInstructions1;
    }

    public String getOrderInstructions2()
    {
        return orderInstructions2;
    }

    public void setOrderInstructions2(final String orderInstructions2)
    {
        this.orderInstructions2 = orderInstructions2;
    }

    public String getCarrier()
    {
        return carrier;
    }

    public void setCarrier(final String carrier)
    {
        this.carrier = carrier;
    }

    public String getOrderPurpose()
    {
        return orderPurpose;
    }

    public void setOrderPurpose(final String orderPurpose)
    {
        this.orderPurpose = orderPurpose;
    }

    public String getOrderType()
    {
        return orderType;
    }

    public void setOrderType(final String orderType)
    {
        this.orderType = orderType;
    }

}

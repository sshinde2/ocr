/**
 * 
 */
package au.com.target.tgtjms.jms.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author fkratoch
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "orderItem")
public class OrderItemExtract implements Serializable {

    @XmlElement
    private Integer orderLineNum;

    @XmlElement
    private String itemEAN;

    @XmlElement
    private String itemCode;

    @XmlElement
    private String itemDescription;

    @XmlElement
    private Long quantityOrdered;

    /**
     * @return the orderLineNum
     */
    public Integer getOrderLineNum() {
        return orderLineNum;
    }

    /**
     * @param orderLineNum
     *            the orderLineNum to set
     */
    public void setOrderLineNum(final Integer orderLineNum) {
        this.orderLineNum = orderLineNum;
    }

    /**
     * @return the itemEAN
     */
    public String getItemEAN() {
        return itemEAN;
    }

    /**
     * @param itemEAN
     *            the itemEAN to set
     */
    public void setItemEAN(final String itemEAN) {
        this.itemEAN = itemEAN;
    }

    /**
     * @return the itemNumber
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode
     *            the itemCode to set
     */
    public void setItemCode(final String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the itemDescription
     */
    public String getItemDescription() {
        return itemDescription;
    }

    /**
     * @param itemDescription
     *            the itemDescription to set
     */
    public void setItemDescription(final String itemDescription) {
        this.itemDescription = itemDescription;
    }

    /**
     * @return the quantityOrdered
     */
    public Long getQuantityOrdered() {
        return quantityOrdered;
    }

    /**
     * @param quantityOrdered
     *            the quantityOrdered to set
     */
    public void setQuantityOrdered(final Long quantityOrdered) {
        this.quantityOrdered = quantityOrdered;
    }

}

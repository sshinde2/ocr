/**
 * 
 */
package au.com.target.tgtjms.jms.service.impl;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.lang.reflect.Constructor;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.customer.impl.TargetCustomerEmailResolutionService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.ConsignmentSentToWarehouseReason;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseResponse;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseResponse;
import au.com.target.tgtfulfilment.service.SendToWarehouseProtocol;
import au.com.target.tgtjms.jms.gateway.JmsMessageDispatcher;
import au.com.target.tgtjms.jms.message.AbstractMessage;


/**
 * @author mmaki
 * 
 */
public class JmsSendToWarehouseProtocol implements SendToWarehouseProtocol {
    private static final String JMS_MESSAGE_PACKAGE = "au.com.target.tgtjms.jms.message.";

    private static final Logger LOG = Logger.getLogger(JmsSendToWarehouseProtocol.class);

    private TargetCustomerEmailResolutionService targetCustomerEmailResolutionService;

    private JmsMessageDispatcher messageDispatcher;

    private String consignmentExtractQueueId;


    @Override
    public SendToWarehouseResponse sendConsignmentExtract(final ConsignmentModel consignment,
            final ConsignmentSentToWarehouseReason reason) {
        Assert.notNull(consignment, "consignment should not be null");
        final WarehouseModel warehouse = consignment.getWarehouse();
        final String warehouseCode = warehouse != null ? warehouse.getCode() : "unknown";

        final AbstractMessage message = getInstanceOfExtractMessage(consignment, warehouseCode, reason);

        LOG.info("Sending consignment extract to " + warehouseCode + ". Contents: "
                + (message != null ? message.convertToMessagePayload() : message));
        if (message != null) {
            messageDispatcher.send(warehouseCode + consignmentExtractQueueId, message);
        }
        return SendToWarehouseResponse.getSuccessRespose();
    }

    /**
     * Returns an instance of order extract message based on given {@code warehouseCode}.
     * 
     * @param consignment
     *            the consignment to retrieve order information from
     * @param warehouseCode
     *            the warehouse code used to select correct message type
     * @return an instance of {@link AbstractMessage}, which is either Fastline warehouse or ToySale warehouse order
     *         extract
     */
    public AbstractMessage getInstanceOfExtractMessage(final ConsignmentModel consignment,
            final String warehouseCode, final ConsignmentSentToWarehouseReason reason) {
        final Class<?> clazz;
        try {
            final String className = warehouseCode + "ConsignmentExtractMessage";
            clazz = Class.forName(JMS_MESSAGE_PACKAGE + className);
            final Constructor<?> constructor = clazz.getConstructor(ConsignmentModel.class,
                    TargetCustomerEmailResolutionService.class, ConsignmentSentToWarehouseReason.class);
            return (AbstractMessage)constructor.newInstance(consignment,
                    this.targetCustomerEmailResolutionService, reason);

        }
        catch (final Exception e) {
            LOG.error("ERR-ORDEREXTRACT-MESSAGE - error occured when creating an instance of the Message", e);
        }
        return null;
    }

    /**
     * @param targetCustomerEmailResolutionService
     *            the targetCustomerEmailResolutionService to set
     */
    @Required
    public void setTargetCustomerEmailResolutionService(
            final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService) {
        this.targetCustomerEmailResolutionService = targetCustomerEmailResolutionService;
    }

    /**
     * @param messageDispatcher
     *            the messageDispatcher to set
     */
    @Required
    public void setMessageDispatcher(final JmsMessageDispatcher messageDispatcher) {
        this.messageDispatcher = messageDispatcher;
    }

    /**
     * @param consignmentExtractQueueId
     *            the consignmentExtractQueueId to set
     */
    @Required
    public void setConsignmentExtractQueueId(final String consignmentExtractQueueId) {
        this.consignmentExtractQueueId = consignmentExtractQueueId;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfulfilment.service.SendToWarehouseProtocol#sendUnshippedConsignments(java.util.List)
     */
    @Override
    public PollWarehouseResponse sendUnshippedConsignments(final List<TargetConsignmentModel> consignments) {
        //Not to be implemnted
        return null;
    }

}

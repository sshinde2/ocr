/**
 * 
 */
package au.com.target.tgtjms.jms.service.impl;

import javax.jms.Message;

import org.apache.activemq.command.ActiveMQBytesMessage;
import org.apache.activemq.util.ByteSequence;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.jms.JmsException;

import au.com.target.tgtjms.jms.converter.GetLaybyOrderMessageConverter;
import au.com.target.tgtjms.jms.gateway.JmsMessageDispatcher;
import au.com.target.tgtjms.util.JmsMessageHelper;
import au.com.target.tgtsale.integration.dto.TargetLaybyOrder;
import au.com.target.tgtsale.integration.exception.UnsuccessfulESBOperationException;
import au.com.target.tgtsale.pos.service.TargetGetPosLaybyOrderService;


/**
 * Default implementation for {@link TargetGetPosLaybyOrderService}.
 * 
 * @author rsamuel3
 */
public class TargetGetPosLaybyOrderServiceImpl implements TargetGetPosLaybyOrderService {

    private static final Logger LOG = Logger.getLogger(TargetGetPosLaybyOrderServiceImpl.class);

    private JmsMessageDispatcher messageDispatcher;

    private String incomingQueue;
    private String outgoingQueue;


    @Override
    public TargetLaybyOrder retrieveLaybyOrderFromPos(final String laybynum) throws UnsuccessfulESBOperationException {
        final String posLaybyNumber = JmsMessageHelper.getLaybyNumForPos(laybynum);
        try {
            final Message message = getMessageDispatcher().sendAndReceive(
                    getOutgoingQueue(), posLaybyNumber, getIncomingQueue());
            if (message == null) {
                throw new UnsuccessfulESBOperationException("ERR-GETLAYBYORDER-BADMESSASGE : No response received");
            }
            else if (message instanceof ActiveMQBytesMessage) {
                final ActiveMQBytesMessage responseMQ = (ActiveMQBytesMessage)message;
                final ByteSequence bytes = responseMQ.getContent();
                final String response = new String(bytes.getData());
                LOG.info("Response received: " + response);
                if (StringUtils.isNotBlank(response))
                {
                    return GetLaybyOrderMessageConverter.unmarshalResponse(response);
                }
            }
            throw new UnsuccessfulESBOperationException("ERR-GETLAYBYORDER-BADMESSASGE : Unexpected message format :"
                    + message);
        }
        catch (final JmsException e) {
            final String message = "ERR-GETLAYBYORDER-BADMESSASGE : "
                    + "could not retrieve the message from queue for lay-by number :" + laybynum;
            LOG.error(message, e);
            throw new UnsuccessfulESBOperationException(message, e);
        }
    }

    /**
     * Returns the message dispatcher.
     * 
     * @return the message dispatcher.
     */
    public JmsMessageDispatcher getMessageDispatcher() {
        return messageDispatcher;
    }

    /**
     * Sets the JMS message dispatcher.
     * 
     * @param messageDispatcher
     *            the JMS message dispatcher to set
     */
    @Required
    public void setMessageDispatcher(final JmsMessageDispatcher messageDispatcher) {
        this.messageDispatcher = messageDispatcher;
    }

    /**
     * Returns the incoming queue name.
     * 
     * @return the incoming queue name
     */
    public String getIncomingQueue() {
        return incomingQueue;
    }

    /**
     * Sets the incoming queue name.
     * 
     * @param incomingQueue
     *            the incoming queue name to set
     */
    @Required
    public void setIncomingQueue(final String incomingQueue) {
        this.incomingQueue = incomingQueue;
    }

    /**
     * Returns the outgoing queue name.
     * 
     * @return the outgoing queue name
     */
    public String getOutgoingQueue() {
        return outgoingQueue;
    }

    /**
     * Sets the outgoing queue name.
     * 
     * @param outgoingQueue
     *            the outgoing queue name to set
     */
    @Required
    public void setOutgoingQueue(final String outgoingQueue) {
        this.outgoingQueue = outgoingQueue;
    }
}

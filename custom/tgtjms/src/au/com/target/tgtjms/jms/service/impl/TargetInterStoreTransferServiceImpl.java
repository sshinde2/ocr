/**
 * 
 */
package au.com.target.tgtjms.jms.service.impl;

import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.jms.JmsException;
import org.springframework.util.Assert;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.dao.TargetConsignmentDao;
import au.com.target.tgtjms.jms.converter.ISTMessageConverter;
import au.com.target.tgtjms.jms.gateway.JmsMessageDispatcher;
import au.com.target.tgtsale.ist.dto.ItemDto;
import au.com.target.tgtsale.ist.dto.StockAdjustmentDto;
import au.com.target.tgtsale.ist.exception.PublishISTFailedException;
import au.com.target.tgtsale.ist.service.TargetInterStoreTransferService;


/**
 * @author Nandini
 *
 */
public class TargetInterStoreTransferServiceImpl extends AbstractBusinessService implements
        TargetInterStoreTransferService {

    private static final Logger LOG = Logger.getLogger(TargetInterStoreTransferServiceImpl.class);
    private JmsMessageDispatcher messageDispatcher;
    private String queueName;
    private ISTMessageConverter istMessageConverter;
    private TargetConsignmentDao targetConsignmentDao;

    @Override
    public void publishInterStoreTransferInfo(final String consignmentCode) {

        Assert.notNull(consignmentCode, "consignmentCode cannot be null");

        ConsignmentModel consignmentModel = null;
        try {
            consignmentModel = targetConsignmentDao.getConsignmentBycode(consignmentCode);
        }
        catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
            LOG.error("Error when trying to get the consignmentModel", e);
        }

        Assert.isInstanceOf(TargetConsignmentModel.class, consignmentModel,
                "consignmentModel must be TargetConsignmentModel");

        final Set<ConsignmentEntryModel> conEntries = consignmentModel.getConsignmentEntries();

        if (CollectionUtils.isNotEmpty(conEntries)) {
            // only process inter-store transfer if there are such entries
            final StockAdjustmentDto stockAdjustmentInfo = new StockAdjustmentDto();

            if (null != consignmentModel.getWarehouse()) {
                final Collection<PointOfServiceModel> warehousePOSs = consignmentModel.getWarehouse()
                        .getPointsOfService();

                if (warehousePOSs.size() == 1) {
                    // order is fulfilled by only one warehouse/store
                    for (final PointOfServiceModel pointOfService : warehousePOSs) {
                        Assert.isTrue(pointOfService instanceof TargetPointOfServiceModel,
                                "pointOfService must be TargetPointOfServiceModel");

                        final TargetPointOfServiceModel targetPointOfService = (TargetPointOfServiceModel)pointOfService;
                        stockAdjustmentInfo.setFromStore(targetPointOfService.getStoreNumber());

                        final List<ItemDto> items = new ArrayList<>();
                        for (final ConsignmentEntryModel conEnt : consignmentModel.getConsignmentEntries()) {

                            if (null != conEnt.getOrderEntry() && null != conEnt.getOrderEntry().getProduct()
                                    && null != conEnt.getShippedQuantity()
                                    && conEnt.getShippedQuantity().intValue() > 0) {
                                final ItemDto item = new ItemDto();
                                item.setCode(conEnt.getOrderEntry().getProduct().getCode());
                                item.setQty(conEnt.getShippedQuantity());
                                items.add(item);
                            }
                        }
                        stockAdjustmentInfo.setItems(items);
                    }

                    try {
                        final String istMessage = istMessageConverter.getMessagePayload(stockAdjustmentInfo);
                        messageDispatcher.send(queueName, istMessage);
                        LOG.debug("IST-MESSAGE\n****************************************\n"
                                + istMessage + "\n****************************************");
                    }
                    catch (final JmsException e) {
                        final String logMsg = "Failed to send message for Inter store transfer: ";
                        LOG.error(logMsg, e);
                        throw new PublishISTFailedException(logMsg, e);
                    }
                }
                else {
                    LOG.warn(
                            "There are no or more than one warehouses available hence not processing inter-store transfer for consignment: "
                                    + consignmentCode);
                }
            }
            else {
                LOG.warn("Warehouse or store is null hence not processing inter-store transfer for consignment: "
                        + consignmentCode);
            }
        }
        else {
            LOG.warn(
                    "There are no consignment entries available hence not processing inter-store transfer for consignment: "
                            + consignmentCode);
        }
    }

    /**
     * @param messageDispatcher
     *            the messageDispatcher to set
     */
    @Required
    public void setMessageDispatcher(final JmsMessageDispatcher messageDispatcher) {
        this.messageDispatcher = messageDispatcher;
    }

    /**
     * 
     * @param queueName
     *            the queueName to set
     */
    @Required
    public void setQueueName(final String queueName) {
        this.queueName = queueName;
    }

    /**
     * @param istMessageConverter
     *            the istMessageConverter to set
     */
    @Required
    public void setIstMessageConverter(final ISTMessageConverter istMessageConverter) {
        this.istMessageConverter = istMessageConverter;
    }

    /**
     * @param targetConsignmentDao
     *            the targetConsignmentDao to set
     */
    @Required
    public void setTargetConsignmentDao(final TargetConsignmentDao targetConsignmentDao) {
        this.targetConsignmentDao = targetConsignmentDao;
    }

}

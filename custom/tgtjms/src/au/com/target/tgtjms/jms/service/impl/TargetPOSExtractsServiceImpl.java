/**
 * 
 */
package au.com.target.tgtjms.jms.service.impl;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.customer.impl.TargetCustomerEmailResolutionService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtjms.jms.gateway.JmsMessageDispatcher;
import au.com.target.tgtjms.jms.message.CncOrderExtractMessage;
import au.com.target.tgtjms.jms.message.LaybyAddressMessage;
import au.com.target.tgtsale.pos.service.TargetPOSExtractsService;



/**
 * @author rsamuel3
 * 
 */
public class TargetPOSExtractsServiceImpl implements TargetPOSExtractsService {

    private static final Logger LOG = Logger.getLogger(TargetPOSExtractsServiceImpl.class);

    private JmsMessageDispatcher messageDispatcher;

    private String cncOrderExtractQueueId;

    private String laybyOrderAddressExtractQueueId;

    private TargetCustomerEmailResolutionService targetCustomerEmailResolutionService;

    private String posStoreNum;

    @Override
    public void sendCncOrderExtractForCompletion(final OrderModel order) {
        final CncOrderExtractMessage message = new CncOrderExtractMessage(order, null,
                targetCustomerEmailResolutionService);
        LOG.info("Sending CNC order extract to POS. Contents: " + message.convertToMessagePayload());

        final Map<String, String> headers = new HashMap<String, String>();
        headers.put("posStoreNumber", this.posStoreNum);
        messageDispatcher.sendMessageWithHeaders(cncOrderExtractQueueId, message, headers);
    }

    @Override
    public void sendCncOrderExtractForConsignment(final TargetConsignmentModel consignment) {
        final CncOrderExtractMessage message = new CncOrderExtractMessage(null, consignment,
                targetCustomerEmailResolutionService);
        LOG.info("Sending CNC order extract to POS. Contents: " + message.convertToMessagePayload());
        final Map<String, String> headers = new HashMap<String, String>();
        headers.put("posStoreNumber", this.posStoreNum);
        messageDispatcher.sendMessageWithHeaders(cncOrderExtractQueueId, message, headers);
    }

    @Override
    public void sendLaybyOrderAddressExtract(final OrderModel order) {
        final LaybyAddressMessage message = new LaybyAddressMessage(order);

        LOG.info("Sending layby order address extract to POS. Contents: " + message.convertToMessagePayload());

        messageDispatcher.send(laybyOrderAddressExtractQueueId, message);
    }

    /**
     * @param messageDispatcher
     *            the messageDispatcher to set
     */
    @Required
    public void setMessageDispatcher(final JmsMessageDispatcher messageDispatcher) {
        this.messageDispatcher = messageDispatcher;
    }

    /**
     * @param targetCustomerEmailResolutionService
     *            the targetCustomerEmailResolutionService to set
     */
    @Required
    public void setTargetCustomerEmailResolutionService(
            final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService) {
        this.targetCustomerEmailResolutionService = targetCustomerEmailResolutionService;
    }


    /**
     * @param posStoreNum
     *            the posStoreNum to set
     */
    @Required
    public void setPosStoreNum(final String posStoreNum) {
        this.posStoreNum = posStoreNum;
    }

    /**
     * @param cncOrderExtractQueueId
     *            the cncOrderExtractQueueId to set
     */
    @Required
    public void setCncOrderExtractQueueId(final String cncOrderExtractQueueId) {
        this.cncOrderExtractQueueId = cncOrderExtractQueueId;
    }

    /**
     * @param laybyOrderAddressExtractQueueId
     *            the laybyOrderAddressExtractQueueId to set
     */
    @Required
    public void setLaybyOrderAddressExtractQueueId(final String laybyOrderAddressExtractQueueId) {
        this.laybyOrderAddressExtractQueueId = laybyOrderAddressExtractQueueId;
    }

}

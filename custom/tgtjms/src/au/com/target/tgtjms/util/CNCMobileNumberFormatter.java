package au.com.target.tgtjms.util;



import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.lang.StringUtils;


public class CNCMobileNumberFormatter extends XmlAdapter<String, String> {

    private static final String ZERO = "0";
    private static final String ISDCODE = "+61";

    @Override
    public String unmarshal(final String v) throws Exception {
        if (StringUtils.startsWith(v, ISDCODE)) {
            return StringUtils.replace(v, ISDCODE, ZERO);
        }
        return v;
    }

    @Override
    public String marshal(final String v) throws Exception {
        if (StringUtils.startsWith(v, ISDCODE)) {
            return StringUtils.replace(v, ISDCODE, ZERO);
        }
        return v;
    }

}

/**
 * 
 */
package au.com.target.tgtjms.util;

import de.hybris.platform.util.Config;

import org.apache.commons.lang.StringUtils;


/**
 * @author rsamuel3
 * 
 */
public final class JmsMessageHelper {

    private static String storeNumber = null;

    /**
     * Static utility classes should not be instantiated.
     */
    private JmsMessageHelper() {
        //not to be instantiated
    }

    /**
     * Layby number in pos is <Store Number> + orderNumber
     * 
     * @param orderNum
     * @return String representing the layby number as seen by pos
     */
    public static String getLaybyNumForPos(final String orderNum) {
        return getStoreNumber().concat(orderNum);
    }

    /**
     * strips the storenumber from the pos layby number
     * 
     * @param posOrderNum
     * @return String with the laybyorder number in hybris
     */
    public static String getLaybyNumForHybris(final String posOrderNum) {
        return StringUtils.substringAfter(posOrderNum, getStoreNumber());
    }

    /**
     * Returns the predefined store number.
     * 
     * @return the store number
     */
    public static String getStoreNumber() {
        if (storeNumber == null) {
            storeNumber = Config.getString("tgtcore.store.number", "5972");
        }
        return storeNumber;
    }
}

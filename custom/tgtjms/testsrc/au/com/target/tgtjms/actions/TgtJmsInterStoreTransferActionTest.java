/**
 * 
 */
package au.com.target.tgtjms.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtsale.ist.service.TargetInterStoreTransferService;


/**
 * @author Nandini
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TgtJmsInterStoreTransferActionTest {

    private static final String CONSIGNMENT_CODE = "testConsignment";

    @InjectMocks
    private final TgtJmsInterStoreTransferAction interStoreTransferAction = new TgtJmsInterStoreTransferAction();

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private TargetConsignmentModel consignment;

    @Mock
    private TargetInterStoreTransferService targetInterStoreTransferService;

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteActionWithNullOrderProcess() throws RetryLaterException, Exception {
        interStoreTransferAction.executeAction(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteActionWithNullConsignmentModel() throws RetryLaterException, Exception {
        BDDMockito.given(orderProcessParameterHelper.getConsignment(process)).willReturn(null);
        interStoreTransferAction.executeAction(process);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteActionWithNullConsignmentCode() throws RetryLaterException, Exception {
        BDDMockito.given(orderProcessParameterHelper.getConsignment(process)).willReturn(null);
        BDDMockito.given(consignment.getCode()).willReturn(CONSIGNMENT_CODE);
        interStoreTransferAction.executeAction(process);
    }

    @Test
    public void testExecuteActionWithInterStoreTransferInfo() throws RetryLaterException, Exception {
        BDDMockito.given(orderProcessParameterHelper.getConsignment(process)).willReturn(consignment);
        BDDMockito.given(consignment.getCode()).willReturn(CONSIGNMENT_CODE);
        interStoreTransferAction.executeAction(process);
        Mockito.verify(targetInterStoreTransferService).publishInterStoreTransferInfo(CONSIGNMENT_CODE);
    }

}

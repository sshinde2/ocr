/**
 * 
 */
package au.com.target.tgtjms.email;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.junit.Test;

import au.com.target.tgtmail.converter.EmailMessageConverter;
import au.com.target.tgtmail.model.Email;


/**
 * @author Olivier Lamy
 */
@UnitTest
public class TargetJmsEmailServiceTest {

    private final Logger logger = Logger.getLogger(getClass());

    @Test
    public void testEmailToXml() throws Exception {
        final TargetJmsEmailService targetJmsEmailService = new TargetJmsEmailService();

        final EmailMessageModel emailMessageModel = new EmailMessageModel();

        final EmailAddressModel emailModel = new EmailAddressModel();
        emailModel.setDisplayName("foo");
        emailModel.setEmailAddress("olivier.lamy@target.com.au");
        emailMessageModel.setFromAddress(emailModel);

        emailMessageModel.setToAddresses(Arrays.asList(emailModel));

        emailMessageModel.setSubject("Yeahh");

        emailMessageModel.setBody("<b>bold</b>");

        final Email email = targetJmsEmailService.buildEmailFromHybrisModel(emailMessageModel);

        final EmailMessageConverter converter = new EmailMessageConverter();

        final String xml = converter.convert(email);

        logger.info("xml:" + xml);

    }

}

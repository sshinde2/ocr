/**
 * 
 */
package au.com.target.tgtjms.jms.converter;

import java.io.IOException;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;

import javax.annotation.Resource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLTestCase;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.xml.sax.SAXException;

import au.com.target.tgtsale.tlog.data.Flybuys;
import au.com.target.tgtsale.tlog.data.TLog;
import au.com.target.tgtsale.tlog.data.Transaction;

import com.google.common.collect.ImmutableList;


/**
 * @author vmuthura
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:tgtsale-spring-test.xml")
public class DataMarshallingTest extends XMLTestCase {

    @Resource
    private Jaxb2Marshaller jaxb2Marshaller;

    @Resource
    private JmsTemplate posJmsTemplate;

    private void testXmlFragment(final String fileName) throws MalformedURLException, IOException, SAXException
    {
        final StreamSource source = new StreamSource(new ClassPathResource(fileName).getFile());
        final String xml = IOUtils.toString(new URL(source.getSystemId()));

        final TLog tlog = (TLog)jaxb2Marshaller.unmarshal(source);

        final StringWriter output = new StringWriter();
        final StreamResult result = new StreamResult(output);
        jaxb2Marshaller.marshal(tlog, result);

        final Diff xmlDiff = new Diff(xml, output.toString());

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);

    }

    @BeforeClass
    public static void init()
    {
        XMLUnit.setIgnoreWhitespace(true);
    }

    @Test
    public void testSaleCash() throws IOException, SAXException
    {
        testXmlFragment("test/tlog-sale-cash.xml");
    }

    @Test
    public void testSaleCashNoFlybuys() throws IOException, SAXException
    {
        testXmlFragment("test/tlog-sale-noflybuy.xml");
    }

    @Test
    public void testSaleCashNoFlybuysMarshall() throws IOException, SAXException
    {
        final String xml = "<tlog>"
                + "<trans/>"
                + "</tlog>";
        final Transaction transaction = new Transaction();
        transaction.setFlybuys(new Flybuys());
        final TLog tlog = new TLog(null, ImmutableList.of(transaction));

        final StringWriter output = new StringWriter();
        final StreamResult result = new StreamResult(output);
        jaxb2Marshaller.marshal(tlog, result);
        final Diff xmlDiff = new Diff(xml, output.toString());
        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testSaleCashWithFlybuysMarshall() throws IOException, SAXException
    {
        final String xml = "<tlog>"
                + "<trans>"
                + "<flybuys number=\"122343456456456\"/>"
                + "</trans>"
                + "</tlog>";
        final Transaction transaction = new Transaction();
        transaction.setFlybuys(new Flybuys("122343456456456"));
        final TLog tlog = new TLog(null, ImmutableList.of(transaction));

        final StringWriter output = new StringWriter();
        final StreamResult result = new StreamResult(output);
        jaxb2Marshaller.marshal(tlog, result);
        Assert.assertNotNull(output);
        final Diff xmlDiff = new Diff(xml, output.toString());
        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

    @Test
    public void testSaleCreditCard() throws IOException, SAXException
    {
        testXmlFragment("test/tlog-sale-cc.xml");
    }

    @Test
    public void testSalePaypal() throws IOException, SAXException
    {
        testXmlFragment("test/tlog-sale-paypal.xml");
    }

    @Test
    public void testRefundCreditCard() throws IOException, SAXException
    {
        testXmlFragment("test/tlog-refund-cc.xml");
    }

    @Test
    public void testRefundPaypal() throws IOException, SAXException
    {
        testXmlFragment("test/tlog-refund-paypal.xml");
    }

    @Test
    public void testLaybyCreateCreditCard() throws IOException, SAXException
    {
        testXmlFragment("test/tlog-laybyCreate-cc.xml");
    }

    @Test
    public void testLaybyCreatePaypal() throws IOException, SAXException
    {
        testXmlFragment("test/tlog-laybyCreate-paypal.xml");
    }

    @Test
    public void testLaybyPaymentCreditCard() throws IOException, SAXException
    {
        testXmlFragment("test/tlog-laybyPayment-cc.xml");
    }

    @Test
    public void testLaybyPaymentPaypal() throws IOException, SAXException
    {
        testXmlFragment("test/tlog-laybyPayment-paypal.xml");
    }

    @Test
    public void testConsolidatedTlog() throws IOException, SAXException
    {
        testXmlFragment("test/tlog-consolidated.xml");
    }

    @Ignore
    @Test
    public void testPublishJmsMessage() throws IOException
    {
        final StreamSource source = new StreamSource(new ClassPathResource("test/tlog-consolidated.xml").getFile());

        final TLog tlog = (TLog)jaxb2Marshaller.unmarshal(source);
        posJmsTemplate.convertAndSend("tlog", tlog);

        final TLog received = (TLog)posJmsTemplate.receiveAndConvert("tlog");
        assertEquals("Transactions ", tlog.getTransactions().size(), received.getTransactions().size());
    }

}

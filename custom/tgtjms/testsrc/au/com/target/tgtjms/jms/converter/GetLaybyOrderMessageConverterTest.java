/**
 * 
 */
package au.com.target.tgtjms.jms.converter;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import au.com.target.tgtsale.integration.dto.TargetLaybyOrder;
import au.com.target.tgtsale.integration.exception.UnsuccessfulESBOperationException;


/**
 * @author rsamuel3
 * 
 */
@UnitTest
public class GetLaybyOrderMessageConverterTest {
    //CHECKSTYLE:OFF this one needs to be public
    @Rule
    public final ExpectedException expectedEx = ExpectedException.none();

    //CHECKSTYLE:ON

    @Test
    public void testUnmarshalResponse() throws UnsuccessfulESBOperationException, FileNotFoundException, IOException {
        final String laybynum = "00008001";
        final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<laybyOrderResponse>"
                + "    <targetLaybyOrder>"
                + "      <orderCode>" + laybynum + "</orderCode>"
                + "      <totalPrice>55.0</totalPrice>"
                + "      <subtotalPrice>36.0</subtotalPrice>"
                + "      <shippingPrice>9.0</shippingPrice>"
                + "      <outstandingAmt>49.0</outstandingAmt>"
                + "      <totalDiscounts>0.0</totalDiscounts>"
                + "      <layByFee>10.0</layByFee>"
                + "      <totalTax>5.0</totalTax>"
                + "      <entries>"
                + "         <entryNumber>0</entryNumber>"
                + "         <quantity>1</quantity>"
                + "         <basePrice>12.0</basePrice>"
                + "         <totalPrice>12.0</totalPrice>"
                + "         <gstRate>0</gstRate>"
                + "      </entries>"
                + "      <modifiedDate>20130625152052</modifiedDate>"
                + "      <lastActionedDate>Z</lastActionedDate>"
                + "      <laybyModified>false</laybyModified>"
                + "      <cancelled>false</cancelled>"
                + "     </targetLaybyOrder>"
                + "     <success>true</success>"
                + "</laybyOrderResponse>";
        final TargetLaybyOrder order = GetLaybyOrderMessageConverter.unmarshalResponse(xml);
        Assert.assertNotNull(order);
        Assert.assertEquals(laybynum, order.getOrderCode());
    }

    @Test
    public void testUnmarshalResponseWithUESBOException() throws UnsuccessfulESBOperationException,
            FileNotFoundException, IOException {
        final String exceptionMessage = "ERR-GETLAYBYORDER-NOTSUCCESS : The call to get the layby order was not successful";
        final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<laybyOrderResponse>"
                + "<success>false</success>"
                + "<message>" + exceptionMessage + "</message>"
                + "</laybyOrderResponse>";
        expectedEx.expect(UnsuccessfulESBOperationException.class);

        expectedEx.expectMessage(exceptionMessage);
        GetLaybyOrderMessageConverter.unmarshalResponse(xml);
    }

    @Test
    public void testUnmarshalResponseWithJAXBException() throws UnsuccessfulESBOperationException {
        final String exceptionMessage = "ERR-GETLAYBYORDER-UNMARSHAL : exception thrown when trying to make sense of the response from esb";
        final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<test/>";
        expectedEx.expect(UnsuccessfulESBOperationException.class);

        expectedEx.expectMessage(exceptionMessage);
        GetLaybyOrderMessageConverter.unmarshalResponse(xml);
    }
}

/**
 * 
 */
package au.com.target.tgtjms.jms.converter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.mockito.Mockito;

import au.com.target.tgtcore.customer.impl.TargetCustomerEmailResolutionService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfulfilment.ConsignmentSentToWarehouseReason;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;
import au.com.target.tgtjms.jms.message.ToysaleWarehouseConsignmentExtractMessage;
import au.com.target.tgtlayby.model.PurchaseOptionModel;


/**
 * @author fkratoch
 * 
 */
@UnitTest
public class LongtermLaybyConsignmentExtractMessageConverterTest {

    @Test
    public void expectCorrectExtractFormatTest() {
        final TargetCustomerEmailResolutionService cers = Mockito.mock(TargetCustomerEmailResolutionService.class);

        final OrderModel order = Mockito.mock(OrderModel.class);
        final TargetCarrierModel carrier = Mockito.mock(TargetCarrierModel.class);
        final TargetConsignmentModel consignment = Mockito.mock(TargetConsignmentModel.class);
        when(consignment.getTargetCarrier()).thenReturn(carrier);
        when(carrier.getWarehouseCode()).thenReturn("APC");
        Mockito.when(consignment.getOrder()).thenReturn(order);
        Mockito.when(order.getCode()).thenReturn("1234567890");

        final Set<ConsignmentEntryModel> entries = new HashSet<>();
        final ConsignmentEntryModel consignmentEntry1 = mock(ConsignmentEntryModel.class);
        final OrderEntryModel entry1 = Mockito.mock(OrderEntryModel.class);
        when(consignmentEntry1.getOrderEntry()).thenReturn(entry1);
        Mockito.when(consignmentEntry1.getQuantity()).thenReturn(Long.valueOf(2));
        entries.add(consignmentEntry1);
        final ConsignmentEntryModel consignmentEntry2 = mock(ConsignmentEntryModel.class);
        final OrderEntryModel entry2 = Mockito.mock(OrderEntryModel.class);
        when(consignmentEntry2.getOrderEntry()).thenReturn(entry2);
        Mockito.when(consignmentEntry2.getQuantity()).thenReturn(Long.valueOf(2));
        entries.add(consignmentEntry2);

        Mockito.when(consignment.getConsignmentEntries()).thenReturn(entries);

        final PurchaseOptionModel purchaseOptModel = Mockito.mock(PurchaseOptionModel.class);
        Mockito.when(purchaseOptModel.getCode()).thenReturn("longtermlayby");
        Mockito.when(order.getPurchaseOption()).thenReturn(purchaseOptModel);

        final ProductTypeModel prodTypeModel = Mockito.mock(ProductTypeModel.class);
        Mockito.when(prodTypeModel.getBulky()).thenReturn(Boolean.TRUE);

        final AbstractTargetVariantProductModel product1 = Mockito.mock(AbstractTargetVariantProductModel.class);
        Mockito.when(product1.getCode()).thenReturn("P4000_red_M");
        Mockito.when(product1.getEan()).thenReturn("89475847");
        Mockito.when(product1.getName()).thenReturn("Red T-shirt");
        Mockito.when(product1.getProductType()).thenReturn(prodTypeModel);
        Mockito.when(entry1.getProduct()).thenReturn(product1);

        final AbstractTargetVariantProductModel product2 = Mockito.mock(AbstractTargetVariantProductModel.class);
        Mockito.when(product2.getCode()).thenReturn("P4000_red_M");
        Mockito.when(product2.getEan()).thenReturn("89475847");
        Mockito.when(product2.getName()).thenReturn("Red T-shirt");
        Mockito.when(product2.getProductType()).thenReturn(prodTypeModel);
        Mockito.when(entry2.getProduct()).thenReturn(product2);

        final CustomerModel customer = Mockito.mock(CustomerModel.class);
        Mockito.when(customer.getCustomerID()).thenReturn("Cust984723");
        Mockito.when(order.getUser()).thenReturn(customer);
        final ToysaleWarehouseConsignmentExtractMessage object = new ToysaleWarehouseConsignmentExtractMessage(
                consignment,
                cers,
                ConsignmentSentToWarehouseReason.UPDATED);

        final AddressModel address = Mockito.mock(AddressModel.class);
        Mockito.when(address.getStreetname()).thenReturn("Victoria Street");
        Mockito.when(address.getStreetnumber()).thenReturn("23");
        Mockito.when(address.getFirstname()).thenReturn("John");
        Mockito.when(address.getLastname()).thenReturn("Doe");
        Mockito.when(address.getPostalcode()).thenReturn("3000");
        Mockito.when(address.getDistrict()).thenReturn("VIC");
        Mockito.when(address.getTown()).thenReturn("Melbourne");
        final CountryModel country = Mockito.mock(CountryModel.class);
        Mockito.when(country.getName()).thenReturn("Australia");
        Mockito.when(address.getCountry()).thenReturn(country);
        Mockito.when(order.getDeliveryAddress()).thenReturn(address);

        final String msg = LongtermLaybyConsignmentExtractMessageConverter.getMessagePayload(object,
                ConsignmentSentToWarehouseReason.UPDATED);

        assertEquals(
                "Can create XML extract",
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><integration-longtermOrderExtract><orderNumber>1234567890</orderNumber><customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName><shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1><shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity><shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry><shipToPostCode>3000</shipToPostCode><carrier>APC</carrier><orderPurpose>U</orderPurpose><orderType>O</orderType><orderItems><orderItem><orderLineNum>1</orderLineNum><itemEAN>89475847</itemEAN><itemCode>P4000_red_M</itemCode><itemDescription>RED T-SHIRT</itemDescription><quantityOrdered>2</quantityOrdered></orderItem><orderItem><orderLineNum>2</orderLineNum><itemEAN>89475847</itemEAN><itemCode>P4000_red_M</itemCode><itemDescription>RED T-SHIRT</itemDescription><quantityOrdered>2</quantityOrdered></orderItem></orderItems></integration-longtermOrderExtract>",
                msg);
    }

    @Test(expected = IllegalArgumentException.class)
    public void expectIllegalArgumentExceptionTest() {
        final TargetCustomerEmailResolutionService cers = Mockito.mock(TargetCustomerEmailResolutionService.class);

        final OrderModel order = Mockito.mock(OrderModel.class);
        final TargetConsignmentModel consignment = Mockito.mock(TargetConsignmentModel.class);
        Mockito.when(consignment.getOrder()).thenReturn(order);
        Mockito.when(order.getCode()).thenReturn("1234567890");

        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        final OrderEntryModel entry1 = Mockito.mock(OrderEntryModel.class);
        Mockito.when(entry1.getQuantity()).thenReturn(Long.valueOf(2));
        entries.add(entry1);
        final OrderEntryModel entry2 = Mockito.mock(OrderEntryModel.class);
        Mockito.when(entry2.getQuantity()).thenReturn(Long.valueOf(2));
        entries.add(entry2);

        Mockito.when(order.getEntries()).thenReturn(entries);

        final PurchaseOptionModel purchaseOptModel = Mockito.mock(PurchaseOptionModel.class);
        Mockito.when(purchaseOptModel.getCode()).thenReturn("buynow");
        Mockito.when(order.getPurchaseOption()).thenReturn(purchaseOptModel);

        final ProductTypeModel prodTypeModel = Mockito.mock(ProductTypeModel.class);
        Mockito.when(prodTypeModel.getBulky()).thenReturn(Boolean.TRUE);

        final TargetProductModel product1 = Mockito.mock(TargetProductModel.class);
        Mockito.when(product1.getCode()).thenReturn("P4000_red_M");
        Mockito.when(product1.getEan()).thenReturn("89475847");
        Mockito.when(product1.getName()).thenReturn("Red T-shirt");
        Mockito.when(product1.getProductType()).thenReturn(prodTypeModel);
        Mockito.when(entry1.getProduct()).thenReturn(product1);

        final TargetProductModel product2 = Mockito.mock(TargetProductModel.class);
        Mockito.when(product2.getCode()).thenReturn("P4000_red_M");
        Mockito.when(product2.getEan()).thenReturn("89475847");
        Mockito.when(product2.getName()).thenReturn("Red T-shirt");
        Mockito.when(product2.getProductType()).thenReturn(prodTypeModel);
        Mockito.when(entry2.getProduct()).thenReturn(product2);

        final CustomerModel customer = Mockito.mock(CustomerModel.class);
        Mockito.when(customer.getCustomerID()).thenReturn("Cust984723");
        Mockito.when(order.getUser()).thenReturn(customer);
        final ToysaleWarehouseConsignmentExtractMessage object = new ToysaleWarehouseConsignmentExtractMessage(
                consignment,
                cers,
                ConsignmentSentToWarehouseReason.UPDATED);

        final AddressModel address = Mockito.mock(AddressModel.class);
        Mockito.when(address.getStreetname()).thenReturn("Victoria Street");
        Mockito.when(address.getStreetnumber()).thenReturn("23");
        Mockito.when(address.getFirstname()).thenReturn("John");
        Mockito.when(address.getLastname()).thenReturn("Doe");
        Mockito.when(address.getPostalcode()).thenReturn("3000");
        Mockito.when(address.getDistrict()).thenReturn("VIC");
        Mockito.when(address.getTown()).thenReturn("Melbourne");
        final CountryModel country = Mockito.mock(CountryModel.class);
        Mockito.when(country.getName()).thenReturn("Australia");
        Mockito.when(address.getCountry()).thenReturn(country);
        Mockito.when(order.getDeliveryAddress()).thenReturn(address);

        final String msg = LongtermLaybyConsignmentExtractMessageConverter.getMessagePayload(object,
                ConsignmentSentToWarehouseReason.UPDATED);

        assertEquals(
                "Can create XML extract",
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><integration-longtermOrderExtract><orderNumber>1234567890</orderNumber><customerCode>CUST984723</customerCode><customerName>JOHN DOE</customerName><shipToCode>CUST984723</shipToCode><shipToLocAddr1>VICTORIA STREET</shipToLocAddr1><shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity><shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry><shipToPostCode>3000</shipToPostCode><carrier>APC</carrier><orderPurpose>U</orderPurpose><orderType>O</orderType><orderItems><orderItem><orderLineNum>1</orderLineNum><itemEAN>89475847</itemEAN><itemCode>P4000_red_M</itemCode><itemDescription>RED T-SHIRT</itemDescription><quantityOrdered>2</quantityOrdered></orderItem><orderItem><orderLineNum>2</orderLineNum><itemEAN>89475847</itemEAN><itemCode>P4000_red_M</itemCode><itemDescription>RED T-SHIRT</itemDescription><quantityOrdered>2</quantityOrdered></orderItem></orderItems></integration-longtermOrderExtract>",
                msg);
    }

}

/**
 * 
 */
package au.com.target.tgtjms.jms.converter;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import au.com.target.tgtjms.jms.message.NullPriceProductsMessage;


/**
 * @author mmaki
 * 
 */
@UnitTest
public class NullPriceProductsMessageConverterTest {

    @Test(expected = IllegalArgumentException.class)
    public void testGetMessagePayloadEmptyList() {
        final List<String> codes = new ArrayList<>();
        final NullPriceProductsMessage message = new NullPriceProductsMessage(codes);

        NullPriceProductsMessageConverter.getMessagePayload(message);
    }

    @Test
    public void testGetMessagePayload() {
        final List<String> codes = new ArrayList<>();
        codes.add("12345");
        codes.add("23456");
        codes.add("34567");
        final NullPriceProductsMessage message = new NullPriceProductsMessage(codes);

        final String msg = NullPriceProductsMessageConverter.getMessagePayload(message);

        assertEquals(
                "Can create XML extract",
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><integration-productCodeList><products><code>12345</code><code>23456</code><code>34567</code></products></integration-productCodeList>",
                msg);
    }

}
/**
 * 
 */
package au.com.target.tgtjms.jms.converter;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.io.IOException;

import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLTestCase;
import org.joda.time.DateTime;
import org.junit.Test;
import org.mockito.Mockito;
import org.xml.sax.SAXException;

import au.com.target.tgtcore.customer.impl.TargetCustomerEmailResolutionService;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtjms.jms.message.ShipAdviceMessage;


/**
 * @author mmaki
 * 
 */
@UnitTest
public class ShipAdviceMessageConverterTest extends XMLTestCase {

    @Test
    public void testGetMessagePayload() throws SAXException, IOException {

        final OrderModel order = Mockito.mock(OrderModel.class);
        Mockito.when(order.getCode()).thenReturn("1234567890");
        final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService = Mockito
                .mock(TargetCustomerEmailResolutionService.class);
        final TargetCustomerModel customer = Mockito.mock(TargetCustomerModel.class);
        Mockito.when(customer.getCustomerID()).thenReturn("Cust984723");
        Mockito.when(order.getUser()).thenReturn(customer);
        Mockito.when(targetCustomerEmailResolutionService.getEmailForCustomer(customer)).thenReturn("test@test.com");
        final DateTime date = new DateTime(2013, 2, 17, 14, 20);
        final ShipAdviceMessage object = new ShipAdviceMessage(order, date.toDate(),
                targetCustomerEmailResolutionService);

        final AddressModel address = Mockito.mock(AddressModel.class);
        Mockito.when(address.getStreetname()).thenReturn("Victoria Street");
        Mockito.when(address.getStreetnumber()).thenReturn("23");
        Mockito.when(address.getFirstname()).thenReturn("John");
        Mockito.when(address.getLastname()).thenReturn("Doe");
        Mockito.when(address.getPostalcode()).thenReturn("3000");
        Mockito.when(address.getDistrict()).thenReturn("VIC");
        Mockito.when(address.getTown()).thenReturn("Melbourne");
        final CountryModel country = Mockito.mock(CountryModel.class);
        Mockito.when(country.getName()).thenReturn("Australia");
        Mockito.when(address.getCountry()).thenReturn(country);
        Mockito.when(address.getPhone1()).thenReturn("0412734344");
        Mockito.when(order.getDeliveryAddress()).thenReturn(address);
        Mockito.when(order.getPaymentAddress()).thenReturn(address);
        Mockito.when(order.getCncStoreNumber()).thenReturn(Integer.valueOf(1234));

        final String msg = ShipAdviceMessageConverter.getMessagePayload(object, targetCustomerEmailResolutionService);

        final String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                "<integration-shipAdvice><orderNumber>1234567890</orderNumber>" +
                "<deliveryDate>2013-02-17T14:20:00+11:00</deliveryDate>" +
                "<customerCode>CUST984723</customerCode><shipToLocName>John Doe</shipToLocName>" +
                "<shipToLocAddr1>TARGET-VICTORIA STREET</shipToLocAddr1>" +
                "<shipToLocAddr2>23</shipToLocAddr2><shipToCity>MELBOURNE</shipToCity>" +
                "<shipToState>VIC</shipToState><shipToCountry>AUSTRALIA</shipToCountry>" +
                "<shipToPostCode>3000</shipToPostCode><shipToPhone>0412734344</shipToPhone>" +
                "<shipToEmail>test@test.com</shipToEmail></integration-shipAdvice>";

        final Diff xmlDiff = new Diff(expected, msg);

        assertXMLEqual("Is equal", xmlDiff, true);
        assertXMLIdentical("Is identical ", xmlDiff, true);
    }

}
/**
 * 
 */
package au.com.target.tgtjms.jms.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import de.hybris.bootstrap.annotations.UnitTest;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.command.ActiveMQTextMessage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.jms.support.converter.MessageConversionException;

import au.com.target.tgtcore.customer.impl.TargetCustomerEmailResolutionService;
import au.com.target.tgtfulfilment.ConsignmentSentToWarehouseReason;
import au.com.target.tgtjms.jms.message.FastlineWarehouseConsignmentExtractMessage;


/**
 * @author mmaki
 * 
 */
@UnitTest
public class TargetMessageConverterTest {
    private static final String MSG_PAYLOAD = "<orderExtract>test</orderextract>";

    private final TargetMessageConverter converter = new TargetMessageConverter();
    private Session session;


    @Before
    public void setUp() {
        session = Mockito.mock(Session.class);
    }

    @Test
    public void testToMessageOrderExtractMessageNull() throws JMSException {
        try {
            converter.toMessage(null, session);
            fail("Should throw IllegalArgumentException");
        }
        catch (final IllegalArgumentException e) {
            assertEquals("Passed object is null", e.getMessage());
        }

    }

    @Test
    public void testToMessageOrderExtractMessageWrongType() throws JMSException {
        try {
            converter.toMessage("Test", session);
            fail("Should throw IllegalArgumentException");
        }
        catch (final IllegalArgumentException e) {
            assertEquals("Passed object is of wrong type (class java.lang.String)", e.getMessage());
        }

    }

    @Test
    public void testToMessageOrderModelNull() throws MessageConversionException, JMSException {
        final TargetCustomerEmailResolutionService targetCustomerEmailResolutionService = Mockito
                .mock(TargetCustomerEmailResolutionService.class);
        final FastlineWarehouseConsignmentExtractMessage object = new FastlineWarehouseConsignmentExtractMessage(null,
                targetCustomerEmailResolutionService, ConsignmentSentToWarehouseReason.NEW);
        try {
            converter.toMessage(object, session);
            fail("Should throw IllegalArgumentException");
        }
        catch (final IllegalArgumentException e) {
            assertEquals("ConsignmentModel is null", e.getMessage());
        }
    }

    @Test
    public void testToMessage() throws MessageConversionException, JMSException {
        final TextMessage message = new ActiveMQTextMessage();
        Mockito.when(session.createTextMessage()).thenReturn(message);
        final FastlineWarehouseConsignmentExtractMessage object = Mockito
                .mock(FastlineWarehouseConsignmentExtractMessage.class);
        Mockito.when(object.convertToMessagePayload()).thenReturn(MSG_PAYLOAD);

        converter.toMessage(object, session);

        Mockito.verify(object).convertToMessagePayload();
        assertEquals("Message text is not correct", MSG_PAYLOAD, message.getText());
    }

}
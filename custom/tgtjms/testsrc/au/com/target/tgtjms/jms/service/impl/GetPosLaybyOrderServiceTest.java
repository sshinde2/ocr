/**
 * 
 */
package au.com.target.tgtjms.jms.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import javax.jms.Message;
import java.io.IOException;

import org.apache.activemq.command.ActiveMQBytesMessage;
import org.apache.activemq.util.ByteSequence;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import au.com.target.tgtjms.jms.gateway.impl.TargetJmsMessageDispatcher;
import au.com.target.tgtjms.util.JmsMessageHelper;
import au.com.target.tgtsale.integration.dto.TargetLaybyEntries;
import au.com.target.tgtsale.integration.dto.TargetLaybyOrder;
import au.com.target.tgtsale.integration.exception.UnsuccessfulESBOperationException;
import com.google.common.io.Resources;


/**
 * Test suite for {@link TargetGetPosLaybyOrderServiceImpl}.
 *
 * @author rsamuel3
 */
@UnitTest
@RunWith(PowerMockRunner.class)
@PrepareForTest(JmsMessageHelper.class)
public class GetPosLaybyOrderServiceTest {

    private static final String OUTGOING_QUEUE = "out";
    private static final String INCOMING_QUEUE = "in";

    @Mock
    private TargetJmsMessageDispatcher messageDispatcher;

    @InjectMocks
    private TargetGetPosLaybyOrderServiceImpl targetGetOrderService  = new TargetGetPosLaybyOrderServiceImpl();

    private Message message;

    /**
     * Initializes test cases before run.
     */
    @Before
    public void setUp() {
        PowerMockito.mockStatic(JmsMessageHelper.class);
        targetGetOrderService.setOutgoingQueue(OUTGOING_QUEUE);
        targetGetOrderService.setIncomingQueue(INCOMING_QUEUE);
    }


    @Test
    public void testGetPosLaybyOrderService() throws UnsuccessfulESBOperationException, IOException {
        final String layByNumber = "00008001";
        message = createActiveMQMessage("test/laybyOrderResponse.xml");
        when(JmsMessageHelper.getLaybyNumForPos(layByNumber)).thenReturn(layByNumber);
        when(messageDispatcher.sendAndReceive(OUTGOING_QUEUE, layByNumber, INCOMING_QUEUE))
                .thenReturn(message);

        final TargetLaybyOrder order = targetGetOrderService.retrieveLaybyOrderFromPos(layByNumber);
        assertNotNull(order);
        assertEquals(layByNumber, order.getOrderCode());
        final TargetLaybyEntries entries = order.getEntries();
        assertNotNull(entries);
        assertEquals(1, entries.getEntry().size());
    }

    @Test
    public void testGetPosLaybyOrderServiceSuccessFalse() throws UnsuccessfulESBOperationException, IOException {
        final String layByNumber = "00008001";
        message = createActiveMQMessage("test/laybyOrderResponseNoSuccess.xml");
        when(JmsMessageHelper.getLaybyNumForPos(layByNumber)).thenReturn(layByNumber);
        when(messageDispatcher.sendAndReceive(OUTGOING_QUEUE, layByNumber, INCOMING_QUEUE))
                .thenReturn(message);
        try {
            targetGetOrderService.retrieveLaybyOrderFromPos(layByNumber);
        }
        catch (final UnsuccessfulESBOperationException e) {
            assertEquals("ERR-GETLAYBYORDER-NOTSUCCESS : The call to get the layby order was not successful",
                    e.getMessage());
        }
    }

    @Test
    public void testGetPosLaybyOrderServiceNullResponse() throws UnsuccessfulESBOperationException {
        final String layByNumber = "00008001";
        when(JmsMessageHelper.getLaybyNumForPos(layByNumber)).thenReturn(layByNumber);
        when(messageDispatcher.sendAndReceive(OUTGOING_QUEUE, layByNumber, INCOMING_QUEUE))
                .thenReturn(message);
        try {
            targetGetOrderService.retrieveLaybyOrderFromPos(layByNumber);
        }
        catch (final UnsuccessfulESBOperationException e) {
            assertEquals("ERR-GETLAYBYORDER-BADMESSASGE : No response received", e.getMessage());
        }
    }

    /**
     * Creates a binary message from the file.
     *
     * @param resourcePath the path to resource file, which represents the source of message body
     * @throws IOException if file reading problem occurs
     * @return the AMQ binary message
     */
    private Message createActiveMQMessage(final String resourcePath) throws IOException {
        final byte[] xml = Resources.toByteArray(Resources.getResource(resourcePath));
        final ActiveMQBytesMessage mqMessage = new ActiveMQBytesMessage();
        mqMessage.setContent(new ByteSequence(xml));
        return mqMessage;
    }
}

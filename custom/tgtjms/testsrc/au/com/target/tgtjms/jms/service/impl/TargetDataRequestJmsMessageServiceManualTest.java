/**
 *
 */
package au.com.target.tgtjms.jms.service.impl;

import de.hybris.bootstrap.annotations.ManualTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;

import au.com.target.tgtsale.pos.service.TargetDataRequestJmsService;


/**
 * @author mmaki
 *
 */
@ManualTest
public class TargetDataRequestJmsMessageServiceManualTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetDataRequestJmsService dataRequestJmsService;

    @Test(expected = IllegalArgumentException.class)
    public void testRequestNullPriceProductsNullList() {
        dataRequestJmsService.requestNullPriceProducts(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRequestNullPriceProductsEmptyList() {
        final List<String> codes = new ArrayList<>();
        dataRequestJmsService.requestNullPriceProducts(codes);
    }

    @Test
    public void testRequestNullPriceProducts() {
        final List<String> codes = new ArrayList<>();
        codes.add("12345");
        codes.add("23456");
        dataRequestJmsService.requestNullPriceProducts(codes);
    }
}
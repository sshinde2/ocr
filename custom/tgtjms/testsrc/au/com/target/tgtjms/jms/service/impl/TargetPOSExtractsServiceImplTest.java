/**
 * 
 */
package au.com.target.tgtjms.jms.service.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.platform.core.model.order.OrderModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import au.com.target.tgtcore.customer.impl.TargetCustomerEmailResolutionService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtjms.jms.gateway.JmsMessageDispatcher;
import au.com.target.tgtjms.jms.message.CncOrderExtractMessage;


/**
 * @author bhuang3
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(TargetPOSExtractsServiceImpl.class)
public class TargetPOSExtractsServiceImplTest {

    @Mock
    private JmsMessageDispatcher messageDispatcher;

    @Mock
    private TargetCustomerEmailResolutionService targetCustomerEmailResolutionService;

    @Mock
    private OrderModel order;

    @Mock
    private TargetConsignmentModel consignment;

    @InjectMocks
    private final TargetPOSExtractsServiceImpl service = new TargetPOSExtractsServiceImpl();

    @Test
    public void testSendCncOrderExtractForCompletion() throws Exception {
        final CncOrderExtractMessage mockMessage = mock(CncOrderExtractMessage.class);
        when(mockMessage.convertToMessagePayload()).thenReturn("test");
        PowerMockito.whenNew(CncOrderExtractMessage.class)
                .withArguments(order, null, targetCustomerEmailResolutionService).thenReturn(mockMessage);
        service.sendCncOrderExtractForCompletion(order);
        verify(messageDispatcher).sendMessageWithHeaders(Mockito.anyString(),
                Mockito.any(CncOrderExtractMessage.class), Mockito.anyMap());
    }

    @Test
    public void testSendCncOrderExtractForConsignment() throws Exception {
        final CncOrderExtractMessage mockMessage = mock(CncOrderExtractMessage.class);
        when(mockMessage.convertToMessagePayload()).thenReturn("test");
        PowerMockito.whenNew(CncOrderExtractMessage.class)
                .withArguments(null, consignment, targetCustomerEmailResolutionService).thenReturn(mockMessage);
        service.sendCncOrderExtractForConsignment(consignment);
        verify(messageDispatcher).sendMessageWithHeaders(Mockito.anyString(),
                Mockito.any(CncOrderExtractMessage.class), Mockito.anyMap());
    }
}

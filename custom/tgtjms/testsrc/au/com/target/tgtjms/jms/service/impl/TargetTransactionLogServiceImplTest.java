/**
 * 
 */
package au.com.target.tgtjms.jms.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jms.UncategorizedJmsException;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.currency.conversion.service.CurrencyConversionFactorService;
import au.com.target.tgtcore.jalo.TMDiscountProductPromotion;
import au.com.target.tgtcore.model.TMDiscountProductPromotionModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.order.TargetDiscountService;
import au.com.target.tgtcore.order.data.AppliedFlybuysDiscountData;
import au.com.target.tgtcore.order.data.AppliedVoucherData;
import au.com.target.tgtjms.jms.converter.TLOGMessageConverter;
import au.com.target.tgtjms.jms.gateway.JmsMessageDispatcher;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtsale.tlog.converter.TlogConverter;
import au.com.target.tgtsale.tlog.converter.impl.TlogConverterImpl;
import au.com.target.tgtsale.tlog.converter.impl.TlogTMDConverterImpl;
import au.com.target.tgtsale.tlog.data.DealResults;
import au.com.target.tgtsale.tlog.data.DiscountInfo;
import au.com.target.tgtsale.tlog.data.DiscountTypeEnum;
import au.com.target.tgtsale.tlog.data.ItemInfo;
import au.com.target.tgtsale.tlog.data.OrderInfo;
import au.com.target.tgtsale.tlog.data.ShippingInfo;
import au.com.target.tgtsale.tlog.data.TLog;
import au.com.target.tgtsale.tlog.data.TenderInfo;
import au.com.target.tgtsale.tlog.data.TenderTypeEnum;
import au.com.target.tgtsale.tlog.data.Transaction;
import au.com.target.tgtsale.tlog.exception.PublishTlogFailedException;


/**
 * Unit test for {@link TargetTransactionLogServiceImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetTransactionLogServiceImplTest {


    private static final String DISPLAY_NAME = "displayName";

    private static final String AUD = "AUD";

    private static final String ORDER_ID = "orderid";

    private static final String FLYBUYS_CODE = "1234567890";
    private static final String PRD1_ID = "P1000_red_S";
    private static final String PRD2_ID = "P1000_red_L";
    private static String tmdCardNum = "2771100000000";
    private static String CARD_NUM = "424242******4242";
    private static String RECEIPT_NUMBER = "123456";

    @Mock
    private OrderModel orderModel;

    @Mock
    private TargetDiscountService targetDiscountService;

    @Mock
    private TlogConverter mockTlogConverter;

    @Mock
    private PromotionsService promotionsService;


    @Mock
    private ModelService modelService;

    @Mock
    private PurchaseOptionConfigModel purchaseOptionConfig;

    @Mock
    CurrencyConversionFactorService currencyConversionFactorService;

    @Mock
    private TargetCustomerModel userModel;

    @Mock
    private JmsMessageDispatcher messageDispatcher;

    @Mock
    private OrderModel mockOrder;

    @Mock
    private CurrencyModel currency;

    @Mock
    private TLOGMessageConverter tlogMessageConverter;

    private Date dueDate;

    @Spy
    @InjectMocks
    private final TargetTransactionLogServiceImpl tlogService = new TargetTransactionLogServiceImpl();

    private final Transaction transaction = new Transaction();

    private AppliedVoucherData voucherData1;

    private AppliedVoucherData voucherData2;

    private AppliedVoucherData appliedFlybuysDiscountData;

    private final Date orderDate = new Date(System.currentTimeMillis());

    private final double deliveryCost = 9.00;

    private final BigDecimal paymentAmt = BigDecimal.TEN;

    private final Map<String, String> deals = new HashMap<String, String>();

    private final OrderInfo orderInfo = new OrderInfo();

    private final ShippingInfo shippingInfo = new ShippingInfo();

    private final DiscountInfo discountInfo = new DiscountInfo();

    private final double discountValue = 5.0;


    @Before
    public void setup() {

        deals.put("valuebundle", "1");
        deals.put("spendget", "2");
        orderInfo.setId(ORDER_ID);
        orderInfo.setUserName(DISPLAY_NAME);
        shippingInfo.setAmount(BigDecimal.valueOf(deliveryCost));
        discountInfo.setType(DiscountTypeEnum.TEAM_MEMBER);
        discountInfo.setAmount(BigDecimal.valueOf(discountValue));
        discountInfo.setCode(tmdCardNum);
        given(userModel.getDisplayName()).willReturn(DISPLAY_NAME);
        given(currency.getIsocode()).willReturn(AUD);
        given(Double.valueOf(currencyConversionFactorService.convertPriceToBaseCurrency(deliveryCost, mockOrder)))
                .willReturn(
                        Double.valueOf(deliveryCost));
        given(mockTlogConverter.getOrderInfoFromOrderModel(mockOrder, DISPLAY_NAME)).willReturn(orderInfo);
        given(
                mockTlogConverter.getShippingInfo(BigDecimal.valueOf(deliveryCost).setScale(2, RoundingMode.HALF_UP),
                        null)).willReturn(shippingInfo);
        final Calendar calendar = Calendar.getInstance();
        calendar.set(2099, 1, 1);
        dueDate = calendar.getTime();
        tlogService.setDeals(deals);

        tlogService.setPromotionsService(promotionsService);
        tlogService.setModelService(modelService);

        tlogService.setPromotionsService(promotionsService);
        tlogService.setModelService(modelService);

        // Set converters
        final TlogConverterImpl tlogConverter = new TlogConverterImpl();
        tlogConverter.setTlogTMDConverter(new TlogTMDConverterImpl());
        tlogService.setTlogConverter(tlogConverter);
        tlogService.setCurrencyConversionFactorService(currencyConversionFactorService);
        tlogService.setTlogMessageConverter(tlogMessageConverter);
        tlogService.setMessageDispatcher(messageDispatcher);

        // Default no TMD or vouchers
        given(Double.valueOf(targetDiscountService.getTotalTMDiscount(orderModel))).willReturn(Double.valueOf(0.0));
        given(targetDiscountService.getAppliedVouchersAndFlybuysData(orderModel)).willReturn(
                Collections.EMPTY_LIST);

        // Test vouchers
        // Dollar off
        voucherData1 = new AppliedVoucherData();
        voucherData1.setAbsolute(Boolean.TRUE);
        voucherData1.setAppliedValue(Double.valueOf(5.5));
        voucherData1.setVoucherCode("AB1");

        // Percentage off
        voucherData2 = new AppliedVoucherData();
        voucherData2.setAbsolute(Boolean.FALSE);
        voucherData2.setAppliedValue(Double.valueOf(3.5));
        voucherData2.setVoucherCode("AB2");
        voucherData2.setPercent(Double.valueOf(15));

        // Test flybuys
        appliedFlybuysDiscountData = new AppliedFlybuysDiscountData();
        appliedFlybuysDiscountData.setVoucherCode("testVoucherCode");
        ((AppliedFlybuysDiscountData)appliedFlybuysDiscountData).setPoints(Integer.valueOf(1000));
        ((AppliedFlybuysDiscountData)appliedFlybuysDiscountData).setRedeemCode("testRedeemCode");
        ((AppliedFlybuysDiscountData)appliedFlybuysDiscountData).setConfirmationCode("testConfirmationCode");

    }

    @Test
    public void testAddTransactionDiscountsGivenNoDiscounts() {

        tlogService.addTransactionDiscounts(orderModel, null, transaction, Boolean.FALSE);
        assertThat(CollectionUtils.isEmpty(transaction.getDiscounts())).isTrue();
    }

    @Test
    public void testAddTransactionDiscountsGivenDollarVoucher() {

        given(targetDiscountService.getAppliedVouchersAndFlybuysData(orderModel)).willReturn(
                Collections.singletonList(voucherData1));

        tlogService.addTransactionDiscounts(orderModel, null, transaction, Boolean.FALSE);

        // Expect the attributes in DiscountInfo to match doc: https://target.jira.com/wiki/display/OCR/Transaction+Log
        assertThat(transaction.getDiscounts().size()).isEqualTo(1);
        final DiscountInfo info1 = transaction.getDiscounts().get(0);
        assertThat(info1.getCode()).isEqualTo("AB1");
        assertThat(info1.getIncremental()).isEqualTo("N");
        assertThat(info1.getAmount()).isEqualTo(BigDecimal.valueOf(5.5));
        assertThat(info1.getStyle().getStyle()).isEqualTo("dollar off");
        assertThat(info1.getType().getType()).isEqualTo("promo");

    }

    @Test
    public void testAddTransactionDiscountsGivenPercentVoucher() {

        given(targetDiscountService.getAppliedVouchersAndFlybuysData(orderModel)).willReturn(
                Collections.singletonList(voucherData2));

        tlogService.addTransactionDiscounts(orderModel, null, transaction, Boolean.FALSE);

        // Expect the attributes in DiscountInfo to match doc: https://target.jira.com/wiki/display/OCR/Transaction+Log
        assertThat(transaction.getDiscounts().size()).isEqualTo(1);
        final DiscountInfo info1 = transaction.getDiscounts().get(0);
        assertThat(info1.getCode()).isEqualTo("AB2");
        assertThat(info1.getIncremental()).isEqualTo("N");
        assertThat(info1.getAmount()).isEqualTo(BigDecimal.valueOf(3.5));
        assertThat(info1.getStyle().getStyle()).isEqualTo("percent off");
        assertThat(info1.getType().getType()).isEqualTo("promo");
        assertThat(info1.getPercentage()).isEqualTo(Double.valueOf(15));


    }

    @Test
    public void testAddTransactionDiscountsGivenFlybuys() {

        given(targetDiscountService.getAppliedVouchersAndFlybuysData(orderModel)).willReturn(
                Collections.singletonList(appliedFlybuysDiscountData));
        tlogService.addTransactionDiscounts(orderModel, null, transaction, Boolean.FALSE);
        assertThat(transaction.getDiscounts().size()).isEqualTo(1);
        final DiscountInfo info = transaction.getDiscounts().get(0);
        verifyFlyBuysDiscountData(info);

    }

    @SuppressWarnings("boxing")
    @Test
    public void testAddTransactionDiscountsWithTmd() {

        final TMDiscountProductPromotion promotion = mock(TMDiscountProductPromotion.class);
        final TMDiscountProductPromotionModel promotionModel = mock(TMDiscountProductPromotionModel.class);
        given(orderModel.getTmdCardNumber()).willReturn(tmdCardNum);
        final SessionContext sessioncontext = mock(SessionContext.class);
        final AbstractOrder abstractOrder = mock(AbstractOrder.class);
        final PromotionResult promotionResult = createPromotionResult(promotion, promotionModel);
        final PromotionOrderResults promotionOrderResults = new PromotionOrderResults(sessioncontext, abstractOrder,
                Collections.singletonList(promotionResult), 2.1);
        tlogService.addTransactionDiscounts(orderModel, promotionOrderResults, transaction, Boolean.FALSE);

        assertThat(transaction.getDiscounts().size()).isEqualTo(1);
        final DiscountInfo info1 = transaction.getDiscounts().get(0);
        assertThat(info1.getCode()).isEqualTo(tmdCardNum);
        assertThat(info1.getAmount()).isEqualTo(BigDecimal.valueOf(discountValue));
        assertThat(info1.getType().getType()).isEqualTo("staff");
        assertThat(info1.getPercentage()).isEqualTo(Double.valueOf(discountValue));

    }


    private PromotionResult createPromotionResult(final TMDiscountProductPromotion promotion,
            final TMDiscountProductPromotionModel promotionModel) {
        final PromotionResult promotionResult = mock(PromotionResult.class);

        final JaloSession jaloSession = mock(JaloSession.class);
        given(promotionResult.getSession()).willReturn(jaloSession);
        given(Boolean.valueOf(promotionResult.getFired())).willReturn(Boolean.TRUE);
        given(Boolean.valueOf(promotionResult.isApplied())).willReturn(Boolean.TRUE);
        given(promotionResult.getPromotion()).willReturn(promotion);
        given(Double.valueOf(promotionResult.getTotalDiscount())).willReturn(Double.valueOf(discountValue));
        given(promotionResult.getPK()).willReturn(PK.BIG_PK);
        given(promotionResult.getPromotion()).willReturn(promotion);
        given(modelService.get(promotion)).willReturn(promotionModel);
        given(promotionModel.getPercentageDiscount()).willReturn(Double.valueOf(discountValue));
        return promotionResult;
    }

    @Test
    public void testAddTenderForTradeMe() {
        final Transaction trans = new Transaction();
        final ShippingInfo shippingInfo = new ShippingInfo();
        final BigDecimal shippingAmount = BigDecimal.valueOf(10.21d);
        shippingInfo.setAmount(shippingAmount);
        trans.setShippingInfo(shippingInfo);
        final List<ItemInfo> items = new LinkedList<>();
        final ItemInfo item1 = new ItemInfo();
        final BigDecimal item1Price = BigDecimal.valueOf(12.23d);
        item1.setQuantity(Long.valueOf(3l));
        item1.setPrice(item1Price);
        items.add(item1);
        final ItemInfo item2 = new ItemInfo();
        final BigDecimal item2Price = BigDecimal.valueOf(11.14d);
        item2.setQuantity(Long.valueOf(1l));
        item2.setPrice(item2Price);
        items.add(item2);
        trans.setItems(items);
        given(orderModel.getSalesApplication()).willReturn(SalesApplication.TRADEME);
        tlogService.addTender(trans, orderModel);
        assertThat(trans.getTender().size()).isEqualTo(1);
        assertThat(trans.getTender().get(0).getType()).isEqualTo(TenderTypeEnum.CASH);
        assertThat(trans.getTender().get(0).getApproved()).isEqualTo("Y");
        assertThat(trans
                .getTender().get(0).getAmount()).isEqualTo(
                        item1Price.multiply(BigDecimal.valueOf(3l)).add(item2Price).add(shippingAmount));
    }

    @Test
    public void testAddTenderWithoutTradeMe() {
        final TlogConverter tlogConverter = Mockito.mock(TlogConverter.class);
        tlogService.setTlogConverter(tlogConverter);
        final Transaction trans = new Transaction();
        given(orderModel.getSalesApplication()).willReturn(SalesApplication.WEB);
        final IpgPaymentInfoModel ipgPaymentInfoModel = Mockito.mock(IpgPaymentInfoModel.class);
        given(orderModel.getPaymentInfo()).willReturn(ipgPaymentInfoModel);
        final List<PaymentTransactionModel> transactions = new ArrayList<>();
        final PaymentTransactionModel paymentTransactionModel = Mockito.mock(PaymentTransactionModel.class);
        given(paymentTransactionModel.getIsCaptureSuccessful()).willReturn(Boolean.TRUE);
        final List<PaymentTransactionEntryModel> transactionEntries = new ArrayList<>();
        final PaymentTransactionEntryModel paymentTransactionEntryModel = Mockito
                .mock(PaymentTransactionEntryModel.class);
        given(paymentTransactionEntryModel.getType()).willReturn(PaymentTransactionType.CAPTURE);
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        given(paymentTransactionEntryModel.getAmount()).willReturn(BigDecimal.valueOf(12.22d));
        final TenderInfo tenderInfo = new TenderInfo();
        given(tlogConverter.getTenderInfoFromPaymentEntry(paymentTransactionEntryModel, false)).willReturn(tenderInfo);
        transactionEntries.add(paymentTransactionEntryModel);
        given(paymentTransactionModel.getEntries()).willReturn(transactionEntries);
        transactions.add(paymentTransactionModel);
        given(orderModel.getPaymentTransactions()).willReturn(transactions);
        final BigDecimal paymentAmount = tlogService.addTender(trans, orderModel);
        assertThat(trans.getTender().size()).isEqualTo(1);
        assertThat(trans.getTender().get(0)).isEqualTo(tenderInfo);
        assertThat(paymentAmount).isEqualTo(BigDecimal.valueOf(12.22d));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPublishTlogForOrderNull() {
        tlogService.publishTlogForOrderSale(null);
    }

    @Test
    public void testPublishTlogForPreOrderWithoutTMD() {
        final ArgumentCaptor<TLog> tlogCaptor = ArgumentCaptor
                .forClass(TLog.class);
        final ArgumentCaptor<Transaction> transactionCaptor = ArgumentCaptor
                .forClass(Transaction.class);
        final DealResults dealResults = createDealResults();
        setupOrder(Boolean.TRUE);
        tlogService.setTlogConverter(mockTlogConverter);
        given(mockTlogConverter.getDetailsFromOrderEntriesNPromotions(mockOrder, null, deals, 0.0))
                .willReturn(dealResults);
        given(mockOrder.getNormalSaleStartDateTime()).willReturn(dueDate);
        willReturn(paymentAmt).given(tlogService)
                .addTender(transactionCaptor.capture(), eq(mockOrder));
        tlogService.publishTlogForOrderSale(mockOrder);
        verify(mockTlogConverter).addTransactionType(eq(mockOrder.getPurchaseOptionConfig()),
                transactionCaptor.capture(), eq(Boolean.TRUE));
        verify(mockTlogConverter).addPaymentAndCustomerInfoForPreOrder(eq(mockOrder), transactionCaptor.capture(),
                eq(paymentAmt));
        verify(tlogMessageConverter).getMessagePayload(tlogCaptor.capture());
        verify(mockTlogConverter, times(0)).addLaybyInformationDetails(orderModel, transaction, paymentAmt);
        verify(targetDiscountService, times(0))
                .getAppliedVouchersAndFlybuysData(mockOrder);
        final TLog tlog = tlogCaptor.getValue();
        final Transaction transaction = tlog.getTransactions().get(0);
        assertThat(transaction).isNotNull();
        assertThat(transaction.getCurrency()).isEqualTo(AUD);
        assertThat(transaction.getSalesChannel()).isEqualTo(SalesApplication.WEB.getCode());
        assertThat(transaction.getFlybuys().getNumber()).isEqualTo(FLYBUYS_CODE);
        assertThat(transaction.getOrder()).isNotNull();
        assertThat(DateUtils.truncate(transaction.getDueDate(), Calendar.MINUTE)).isEqualTo(
                DateUtils.truncate(dueDate, Calendar.MINUTE));
        assertThat(transaction.getOrder().getId()).isEqualTo(ORDER_ID);
        assertThat(transaction.getOrder().getUserName()).isEqualTo(DISPLAY_NAME);
        assertThat(transaction.getShippingInfo().getAmount()).isEqualTo(BigDecimal.valueOf(deliveryCost));
        assertThat(transaction.getShippingInfo().getOriginalAmount()).isNull();
        verifyItems(transaction.getItems());
    }

    @Test
    public void testPublishTlogForPreOrderWithTMD() {
        final ArgumentCaptor<TLog> tlogCaptor = ArgumentCaptor
                .forClass(TLog.class);
        final ArgumentCaptor<Transaction> transactionCaptor = ArgumentCaptor
                .forClass(Transaction.class);
        final ArgumentCaptor<BigDecimal> tmdDiscountCaptor = ArgumentCaptor
                .forClass(BigDecimal.class);
        final TMDiscountProductPromotion promotion = mock(TMDiscountProductPromotion.class);
        final TMDiscountProductPromotionModel promotionModel = mock(TMDiscountProductPromotionModel.class);
        final Map<PromotionResult, Double> promotionResultsAndDiscount = populatePromotionResult(promotion,
                promotionModel);
        final DealResults dealResults = createDealResults();
        setupOrder(Boolean.TRUE);
        tlogService.setTlogConverter(mockTlogConverter);
        given(
                mockTlogConverter.getDiscountInfoFromPromotion(eq(promotionModel), eq(tmdCardNum),
                        tmdDiscountCaptor.capture())).willReturn(discountInfo);
        given(mockTlogConverter.getAllTMDFromPromotionResults(null)).willReturn(promotionResultsAndDiscount);
        given(mockTlogConverter.getDetailsFromOrderEntriesNPromotions(mockOrder, null, deals, 0.0))
                .willReturn(dealResults);
        given(mockOrder.getTmdCardNumber()).willReturn(tmdCardNum);
        given(mockOrder.getNormalSaleStartDateTime()).willReturn(dueDate);
        willReturn(paymentAmt).given(tlogService)
                .addTender(transactionCaptor.capture(), eq(mockOrder));
        tlogService.publishTlogForOrderSale(mockOrder);
        verify(mockTlogConverter).addTransactionType(eq(mockOrder.getPurchaseOptionConfig()),
                transactionCaptor.capture(), eq(Boolean.TRUE));
        verify(mockTlogConverter).addPaymentAndCustomerInfoForPreOrder(eq(mockOrder), transactionCaptor.capture(),
                eq(paymentAmt));
        verify(tlogMessageConverter).getMessagePayload(tlogCaptor.capture());
        verify(mockTlogConverter, times(0)).addLaybyInformationDetails(orderModel, transaction, paymentAmt);
        verify(targetDiscountService, times(0))
                .getAppliedVouchersAndFlybuysData(mockOrder);
        final TLog tlog = tlogCaptor.getValue();
        final Transaction transaction = tlog.getTransactions().get(0);
        assertThat(transaction).isNotNull();
        assertThat(transaction.getCurrency()).isEqualTo(AUD);
        assertThat(DateUtils.truncate(transaction.getDueDate(), Calendar.MINUTE)).isEqualTo(
                DateUtils.truncate(dueDate, Calendar.MINUTE));
        assertThat(transaction.getSalesChannel()).isEqualTo(SalesApplication.WEB.getCode());
        assertThat(transaction.getFlybuys().getNumber()).isEqualTo(FLYBUYS_CODE);
        assertThat(transaction.getOrder()).isNotNull();
        assertThat(transaction.getOrder().getId()).isEqualTo(ORDER_ID);
        assertThat(transaction.getOrder().getUserName()).isEqualTo(DISPLAY_NAME);
        assertThat(transaction.getShippingInfo().getAmount()).isEqualTo(BigDecimal.valueOf(deliveryCost));
        assertThat(transaction.getShippingInfo().getOriginalAmount()).isNull();
        assertThat(transaction.getDiscounts().size()).isEqualTo(1);
        assertThat(transaction.getDiscounts().get(0).getAmount()).isEqualTo(discountInfo.getAmount());
        assertThat(transaction.getDiscounts().get(0).getCode()).isEqualTo(tmdCardNum);
        assertThat(transaction.getDiscounts().get(0).getType()).isEqualTo(DiscountTypeEnum.TEAM_MEMBER);
        verifyItems(transaction.getItems());
    }

    @Test
    public void testPublishTlogForOrderWithoutDiscounts() {
        final ArgumentCaptor<TLog> tlogCaptor = ArgumentCaptor
                .forClass(TLog.class);
        final ArgumentCaptor<Transaction> transactionCaptor = ArgumentCaptor
                .forClass(Transaction.class);
        final DealResults dealResults = createDealResults();
        setupOrder(Boolean.FALSE);
        tlogService.setTlogConverter(mockTlogConverter);
        given(mockTlogConverter.getDetailsFromOrderEntriesNPromotions(mockOrder, null, deals, 0.0))
                .willReturn(dealResults);
        willReturn(paymentAmt).given(tlogService)
                .addTender(transactionCaptor.capture(), eq(mockOrder));
        tlogService.publishTlogForOrderSale(mockOrder);
        verify(mockTlogConverter).addTransactionType(eq(mockOrder.getPurchaseOptionConfig()),
                transactionCaptor.capture(), eq(Boolean.FALSE));
        verify(mockTlogConverter, times(0)).addPaymentAndCustomerInfoForPreOrder(eq(mockOrder),
                transactionCaptor.capture(),
                eq(paymentAmt));
        verify(tlogMessageConverter).getMessagePayload(tlogCaptor.capture());
        verify(mockTlogConverter, times(0)).addLaybyInformationDetails(orderModel, transaction, paymentAmt);
        verify(targetDiscountService)
                .getAppliedVouchersAndFlybuysData(mockOrder);
        final TLog tlog = tlogCaptor.getValue();
        final Transaction transaction = tlog.getTransactions().get(0);
        assertThat(transaction).isNotNull();
        assertThat(transaction.getDueDate()).isNull();
        assertThat(transaction.getCurrency()).isEqualTo(AUD);
        assertThat(transaction.getSalesChannel()).isEqualTo(SalesApplication.WEB.getCode());
        assertThat(transaction.getFlybuys().getNumber()).isEqualTo(FLYBUYS_CODE);
        assertThat(transaction.getOrder()).isNotNull();
        assertThat(transaction.getOrder().getId()).isEqualTo(ORDER_ID);
        assertThat(transaction.getOrder().getUserName()).isEqualTo(DISPLAY_NAME);
        assertThat(transaction.getShippingInfo().getAmount()).isEqualTo(BigDecimal.valueOf(deliveryCost));
        assertThat(transaction.getShippingInfo().getOriginalAmount()).isNull();
        verifyItems(transaction.getItems());
    }

    @Test
    public void testPublishTlogForOrderWithDiscounts() {
        final ArgumentCaptor<TLog> tlogCaptor = ArgumentCaptor
                .forClass(TLog.class);
        final ArgumentCaptor<Transaction> transactionCaptor = ArgumentCaptor
                .forClass(Transaction.class);
        final ArgumentCaptor<BigDecimal> tmdDiscountCaptor = ArgumentCaptor
                .forClass(BigDecimal.class);
        final TMDiscountProductPromotion promotion = mock(TMDiscountProductPromotion.class);
        final TMDiscountProductPromotionModel promotionModel = mock(TMDiscountProductPromotionModel.class);
        final Map<PromotionResult, Double> promotionResultsAndDiscount = populatePromotionResult(promotion,
                promotionModel);
        given(
                mockTlogConverter.getDiscountInfoFromPromotion(eq(promotionModel), eq(tmdCardNum),
                        tmdDiscountCaptor.capture())).willReturn(discountInfo);
        given(mockTlogConverter.getAllTMDFromPromotionResults(null)).willReturn(promotionResultsAndDiscount);
        final DealResults dealResults = createDealResults();
        setupOrder(Boolean.FALSE);
        tlogService.setTlogConverter(mockTlogConverter);
        given(mockTlogConverter.getDetailsFromOrderEntriesNPromotions(mockOrder, null, deals, 0.0))
                .willReturn(dealResults);
        given(targetDiscountService
                .getAppliedVouchersAndFlybuysData(mockOrder)).willReturn(
                        Collections.singletonList(appliedFlybuysDiscountData));
        given(mockOrder.getTmdCardNumber()).willReturn(tmdCardNum);
        willReturn(paymentAmt).given(tlogService)
                .addTender(transactionCaptor.capture(), eq(mockOrder));
        tlogService.publishTlogForOrderSale(mockOrder);
        verify(mockTlogConverter).addTransactionType(eq(mockOrder.getPurchaseOptionConfig()),
                transactionCaptor.capture(), eq(Boolean.FALSE));
        verify(mockTlogConverter, times(0)).addPaymentAndCustomerInfoForPreOrder(eq(mockOrder),
                transactionCaptor.capture(),
                eq(paymentAmt));
        verify(tlogMessageConverter).getMessagePayload(tlogCaptor.capture());
        verify(mockTlogConverter, times(0)).addLaybyInformationDetails(orderModel, transaction, paymentAmt);
        verify(targetDiscountService)
                .getAppliedVouchersAndFlybuysData(mockOrder);
        final TLog tlog = tlogCaptor.getValue();
        final Transaction transaction = tlog.getTransactions().get(0);
        assertThat(transaction).isNotNull();
        assertThat(transaction.getCurrency()).isEqualTo(AUD);
        assertThat(transaction.getDueDate()).isNull();
        assertThat(transaction.getSalesChannel()).isEqualTo(SalesApplication.WEB.getCode());
        assertThat(transaction.getFlybuys().getNumber()).isEqualTo(FLYBUYS_CODE);
        assertThat(transaction.getOrder()).isNotNull();
        assertThat(transaction.getOrder().getId()).isEqualTo(ORDER_ID);
        assertThat(transaction.getOrder().getUserName()).isEqualTo(DISPLAY_NAME);
        assertThat(transaction.getShippingInfo().getAmount()).isEqualTo(BigDecimal.valueOf(deliveryCost));
        assertThat(transaction.getShippingInfo().getOriginalAmount()).isNull();
        verifyItems(transaction.getItems());
        assertThat(transaction.getDiscounts()).isNotEmpty();
        assertThat(transaction.getDiscounts().get(0).getAmount()).isEqualTo(discountInfo.getAmount());
        assertThat(transaction.getDiscounts().get(0).getCode()).isEqualTo(tmdCardNum);
        assertThat(transaction.getDiscounts().get(0).getType()).isEqualTo(DiscountTypeEnum.TEAM_MEMBER);
        verifyFlyBuysDiscountData(transaction.getDiscounts().get(1));
    }


    @Test
    public void testPublishTlogForCancelPreOrder() {
        final ArgumentCaptor<TLog> tlogCaptor = ArgumentCaptor
                .forClass(TLog.class);
        given(mockOrder.getCode()).willReturn(ORDER_ID);
        final Transaction expectedTransaction = createTransaction();
        final List<PaymentTransactionEntryModel> refundEntries = createRefundPaymentEntry();
        given(mockTlogConverter.getTransactionForPreOrderCancel(mockOrder, refundEntries)).willReturn(
                expectedTransaction);
        tlogService.setTlogConverter(mockTlogConverter);
        tlogService.publishTlogForPreOrderCancel(mockOrder, refundEntries);
        verify(tlogMessageConverter).getMessagePayload(tlogCaptor.capture());
        final TLog tlog = tlogCaptor.getValue();
        final Transaction actualTransaction = tlog.getTransactions().get(0);
        assertThat(actualTransaction).as("Transaction").isNotNull();
        assertThat(actualTransaction.getType()).as("Transaction Type").isEqualTo(expectedTransaction.getType());
        assertThat(actualTransaction.getOrder()).as("Order Info").isNotNull();
        assertThat(actualTransaction.getOrder().getId()).as("Order Number").isEqualTo(
                expectedTransaction.getOrder().getId());
        assertThat(actualTransaction.getTender()).as("Tender Info").isNotNull();
        assertThat(actualTransaction.getTender().size()).isEqualTo(1);
        assertThat(actualTransaction.getTender().get(0).getApproved()).as("Tender Approved").isEqualTo(
                expectedTransaction.getTender().get(0).getApproved());
        assertThat(actualTransaction.getTender().get(0).getType()).as("Tender Type").isEqualTo(
                expectedTransaction.getTender().get(0).getType());
        assertThat(actualTransaction.getTender().get(0).getCreditCardRRN()).as("Tender Receipt number").isEqualTo(
                expectedTransaction.getTender().get(0).getCreditCardRRN());
        assertThat(actualTransaction.getTender().get(0).getCardNumber()).as("Tender Card Number").isEqualTo(
                expectedTransaction.getTender().get(0).getCardNumber());
    }



    @Test(expected = PublishTlogFailedException.class)
    public void testPublishTlogForCancelPreOrderException() {
        final ArgumentCaptor<TLog> tlogCaptor = ArgumentCaptor
                .forClass(TLog.class);
        given(mockOrder.getCode()).willReturn(ORDER_ID);
        final Transaction expectedTransaction = createTransaction();
        final List<PaymentTransactionEntryModel> refundEntries = createRefundPaymentEntry();
        given(mockTlogConverter.getTransactionForPreOrderCancel(mockOrder, refundEntries)).willReturn(
                expectedTransaction);
        given(tlogMessageConverter.getMessagePayload(tlogCaptor.capture())).willThrow(
                new UncategorizedJmsException("test"));
        tlogService.setTlogConverter(mockTlogConverter);
        tlogService.publishTlogForPreOrderCancel(mockOrder, refundEntries);
        verify(tlogMessageConverter).getMessagePayload(tlogCaptor.capture());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPublishTlogForPreOrderFulfilmentWithNullOrder() {
        final OrderProcessModel process = mock(OrderProcessModel.class);
        tlogService.publishTlogForPreOrderFulfilment(process);
    }

    @Test
    public void testPublishTlogForPreOrderFulfilmentWithOrder() {
        final OrderProcessModel process = mock(OrderProcessModel.class);
        given(process.getOrder()).willReturn(mockOrder);
        given(mockTlogConverter.getTransactionForPreOrderFulfilment(process, mockOrder)).willReturn(transaction);

        tlogService.setTlogConverter(mockTlogConverter);
        tlogService.publishTlogForPreOrderFulfilment(process);
        verify(mockTlogConverter).getTransactionForPreOrderFulfilment(process, mockOrder);
    }

    @Test
    public void testPublishTlogForPreOrderFulfilment() {
        final ArgumentCaptor<TLog> tlogCaptor = ArgumentCaptor
                .forClass(TLog.class);
        final OrderProcessModel process = mock(OrderProcessModel.class);
        given(process.getOrder()).willReturn(mockOrder);
        final Transaction transaction1 = mock(Transaction.class);
        given(mockTlogConverter.getTransactionForPreOrderFulfilment(process, mockOrder)).willReturn(transaction1);
        given(tlogMessageConverter.getMessagePayload(tlogCaptor.capture())).willReturn("message");

        tlogService.setTlogConverter(mockTlogConverter);
        tlogService.setMessageDispatcher(messageDispatcher);
        tlogService.setTlogMessageConverter(tlogMessageConverter);
        tlogService.setQueueName("someQueueName");
        tlogService.publishTlogForPreOrderFulfilment(process);
        verify(mockTlogConverter).getTransactionForPreOrderFulfilment(process, mockOrder);
        verify(messageDispatcher).send("someQueueName", "message");
    }

    @Test(expected = PublishTlogFailedException.class)
    public void testPublishTlogForPreOrderFulfilmentWithException() {
        final ArgumentCaptor<TLog> tlogCaptor = ArgumentCaptor
                .forClass(TLog.class);
        final OrderProcessModel process = mock(OrderProcessModel.class);
        given(process.getOrder()).willReturn(mockOrder);
        final Transaction transaction1 = mock(Transaction.class);
        given(mockTlogConverter.getTransactionForPreOrderFulfilment(process, mockOrder)).willReturn(transaction1);
        given(tlogMessageConverter.getMessagePayload(tlogCaptor.capture()))
                .willThrow(new UncategorizedJmsException("test"));

        tlogService.setTlogConverter(mockTlogConverter);
        tlogService.setMessageDispatcher(messageDispatcher);
        tlogService.setTlogMessageConverter(tlogMessageConverter);
        tlogService.setQueueName("someQueueName");
        tlogService.publishTlogForPreOrderFulfilment(process);
        verify(mockTlogConverter).getTransactionForPreOrderFulfilment(process, mockOrder);
        verify(messageDispatcher, never()).send("someQueueName", "message");
    }

    private void verifyFlyBuysDiscountData(final DiscountInfo discount) {
        assertThat(discount.getCode()).isEqualTo("testVoucherCode");
        assertThat(discount.getPoints()).isEqualTo(Integer.valueOf(1000));
        assertThat(discount.getRedeemCode()).isEqualTo("testRedeemCode");
        assertThat(discount.getConfirmationCode()).isEqualTo("testConfirmationCode");
        assertThat(discount.getType()).isEqualTo(DiscountTypeEnum.FLYBUYS);
    }

    private void verifyItems(final List<ItemInfo> items) {
        assertThat(items.get(0).getId()).isEqualTo(PRD1_ID);
        assertThat(items.get(0).getFilePrice()).isEqualTo(BigDecimal.TEN);
        assertThat(items.get(0).getQuantity()).isEqualTo(new Long(2));
        assertThat(items.get(1).getId()).isEqualTo(PRD2_ID);
        assertThat(items.get(1).getFilePrice()).isEqualTo(BigDecimal.TEN);
        assertThat(items.get(1).getQuantity()).isEqualTo(new Long(3));
    }

    private void setupOrder(final Boolean isPreOrder) {
        given(mockOrder.getContainsPreOrderItems()).willReturn(isPreOrder);
        given(mockOrder.getFlyBuysCode()).willReturn(FLYBUYS_CODE);
        given(mockOrder.getCode()).willReturn(ORDER_ID);
        given(mockOrder.getDate()).willReturn(orderDate);
        given(mockOrder.getSalesApplication()).willReturn(SalesApplication.WEB);
        given(mockOrder.getCurrency()).willReturn(currency);
        given(mockOrder.getPurchaseOptionConfig()).willReturn(purchaseOptionConfig);
        given(mockOrder.getUser()).willReturn(userModel);
        given(mockOrder.getDeliveryCost()).willReturn(Double.valueOf(deliveryCost));
        given(promotionsService.getPromotionResults(mockOrder)).willReturn(null);
    }

    private Map<PromotionResult, Double> populatePromotionResult(final TMDiscountProductPromotion promotion,
            final TMDiscountProductPromotionModel promotionModel) {
        final Double discountAmt = new Double(15.00);
        final Map<PromotionResult, Double> promotionResultsAndDiscount = new HashMap<PromotionResult, Double>();
        final PromotionResult result = createPromotionResult(promotion, promotionModel);
        promotionResultsAndDiscount.put(result, discountAmt);
        return promotionResultsAndDiscount;

    }

    private DealResults createDealResults() {
        final DealResults dealResults = new DealResults();
        final List<ItemInfo> items = new ArrayList<ItemInfo>();
        final ItemInfo item1 = new ItemInfo();
        item1.setFilePrice(BigDecimal.TEN);
        item1.setQuantity(new Long(2));
        item1.setId(PRD1_ID);
        final ItemInfo item2 = new ItemInfo();
        item2.setFilePrice(BigDecimal.TEN);
        item2.setQuantity(new Long(3));
        item2.setId(PRD2_ID);
        items.add(item1);
        items.add(item2);
        dealResults.setItems(items);
        return dealResults;
    }


    private Transaction createTransaction() {
        final Transaction transaction = new Transaction();
        final OrderInfo order = new OrderInfo();
        order.setId(ORDER_ID);
        final TenderInfo tender = new TenderInfo();
        tender.setApproved("Y");
        tender.setType(TenderTypeEnum.EFT);
        tender.setCardNumber(CARD_NUM);
        tender.setCreditCardRRN(RECEIPT_NUMBER);
        transaction.setOrder(order);
        transaction.setTender(Collections.singletonList(tender));
        return transaction;

    }

    private List<PaymentTransactionEntryModel> createRefundPaymentEntry() {

        final PaymentTransactionModel ptm = Mockito.mock(PaymentTransactionModel.class);
        final PaymentTransactionEntryModel refund = Mockito.mock(PaymentTransactionEntryModel.class);
        final IpgCreditCardPaymentInfoModel ccard = Mockito.mock(IpgCreditCardPaymentInfoModel.class);
        final List<PaymentTransactionEntryModel> transactionEntries = new ArrayList<PaymentTransactionEntryModel>();
        given(refund.getType()).willReturn(PaymentTransactionType.REFUND_FOLLOW_ON);
        given(refund.getIpgPaymentInfo()).willReturn(ccard);
        given(refund.getTime()).willReturn(new Date());
        given(refund.getCreationtime()).willReturn(new Date());
        given(refund.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        given(refund.getReceiptNo()).willReturn(RECEIPT_NUMBER);
        given(refund.getAmount()).willReturn(BigDecimal.valueOf(21.00d));
        given(refund.getPaymentTransaction()).willReturn(ptm);
        given(ccard.getNumber()).willReturn(CARD_NUM);
        given(ptm.getEntries()).willReturn(ImmutableList.of(refund));
        given(ptm.getPaymentProvider()).willReturn("ipg");
        given(ptm.getInfo()).willReturn(ccard);
        transactionEntries.add(refund);
        return transactionEntries;

    }


}

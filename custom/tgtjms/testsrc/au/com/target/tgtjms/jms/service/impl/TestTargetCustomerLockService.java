/**
 * 
 */
package au.com.target.tgtjms.jms.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.jms.JMSException;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtsale.pos.service.TargetCustomerLockService;


/**
 * This test should not be included in the hybris test suite because it requires ActiveMQ connection.
 * 
 * @author mmaki
 * 
 */
@Ignore
public class TestTargetCustomerLockService extends ServicelayerTransactionalTest
{

    @Resource
    private TargetCustomerLockService targetCustomerLockService;

    private OrderModel order;

    @Before
    public void setUp() {
        order = new OrderModel();
        order.setCode("597200011002");
        final TargetCustomerModel user = new TargetCustomerModel();
        user.setCustomerID("00000000");
        order.setUser(user);
    }

    @Test
    public void testLockCustomerLock()
    {
        final boolean result = targetCustomerLockService.posLockDirect(order);
        Assert.assertTrue(result);
    }

    @Test
    public void testLockCustomerUnlock()
    {

        final boolean result = targetCustomerLockService.posUnLockDirect(order);
        Assert.assertTrue(result);
    }

    @Test
    public void testLockCustomerFail()
    {
        final boolean result = targetCustomerLockService.convertLockIndirect(order);
        Assert.assertFalse(result);
    }

    @Test
    public void testLockCustomerMultithreaded() throws JMSException, InterruptedException
    {

        final ExecutorService service = Executors.newFixedThreadPool(20);
        for (int i = 0; i < 20; i++)
        {
            service.execute(new TargetRunner());
        }
        service.shutdown();
        service.awaitTermination(30, TimeUnit.SECONDS);
    }

    private final class TargetRunner implements Runnable
    {

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Runnable#run()
         */
        @Override
        public void run()
        {
            targetCustomerLockService.posUnLockDirect(order);
        }



    }
}
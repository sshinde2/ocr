/**
 * 
 */
package au.com.target.tgtlayby.actions;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.ordercancel.model.OrderEntryCancelRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;


/**
 * Action for releasing stock for a cancel process
 */
public class CancelReleaseOrderStockAction extends ReleaseOrderStockAction {

    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process);

        final OrderModel order = process.getOrder();

        Assert.notNull(order, "Order Model can't be empty!");

        final OrderCancelRecordEntryModel orderCancelRequest = orderProcessParameterHelper
                .getOrderCancelRequest(process);

        Assert.notNull(orderCancelRequest,
                "No orderCancelRequest parameter found for this process context, process: " + process.getCode());

        final Collection<OrderEntryModificationRecordEntryModel> entries =
                orderCancelRequest.getOrderEntriesModificationEntries();
        releaseStockForLaybyCancel(order, entries);
    }


    protected void releaseStockForLaybyCancel(final OrderModel order,
            final Collection<OrderEntryModificationRecordEntryModel> entries) {

        Assert.notNull(order);

        if (CollectionUtils.isEmpty(entries)) {
            return;
        }

        for (final OrderEntryModificationRecordEntryModel entry : entries) {

            // convert entry to a OrderEntryCancelRecordEntryModel
            Assert.isInstanceOf(OrderEntryCancelRecordEntryModel.class, entry,
                    "Order cancel request does not contain cancel entries, order=" + order.getCode());

            final OrderEntryCancelRecordEntryModel cancelEntry = (OrderEntryCancelRecordEntryModel)entry;

            final AbstractOrderEntryModel orderEntry = cancelEntry.getOrderEntry();
            Assert.notNull(orderEntry, "Order Entry Model can't be empty!");
            final int amount = cancelEntry.getCancelRequestQuantity().intValue();
            final ProductModel product = orderEntry.getProduct();

            targetStockService.release(product, amount, amount
                    + " stock has been released for " + product.getName() + " from order number is " + order.getCode());
        }
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }


}

/**
 * 
 */
package au.com.target.tgtlayby.actions;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.paymentstandard.model.StandardPaymentModeModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.actions.RetryAction;
import au.com.target.tgtbusproc.constants.TgtbusprocConstants;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import au.com.target.tgtpayment.exceptions.InvalidTransactionRequestException;
import au.com.target.tgtpayment.model.ZippayPaymentInfoModel;
import au.com.target.tgtpayment.service.TargetPaymentService;


public class CheckOrderStatusAction extends RetryAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(CheckOrderStatusAction.class);

    private TargetPaymentService targetPaymentService;

    private TargetSalesApplicationConfigService salesApplicationConfigService;

    public enum Transition {
        OK, REJECTED, INSUFFICIENT;

        public static Set<String> getStringValues() {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values()) {
                res.add(t.toString());
            }
            return res;
        }
    }

    @Override
    protected Set<String> getTransitionsInternal() {
        return Transition.getStringValues();
    }

    @Override
    protected String executeInternal(final OrderProcessModel process) throws RetryLaterException, Exception {

        final OrderModel order = process.getOrder();
        Assert.notNull(order, "Order cannot be null");

        final SalesApplication salesApp = order.getSalesApplication();

        //changes for partner orders
        if (salesApplicationConfigService.isSkipFraudCheck(salesApp)) {
            LOG.info("Skipping order status checks as skipFraudCheck is marked as true for the salesApplication="
                    + salesApp.toString() + ", order=" + order.getCode());
            return Transition.OK.toString();
        }

        // Get the current payment entry out of the process
        final PaymentTransactionEntryModel paymentTransactionEntry = getOrderProcessParameterHelper()
                .getPaymentTransactionEntry(process);


        // If there isn't one then this is a place order process where nothing went wrong so just continue.
        if (paymentTransactionEntry == null) {
            return Transition.OK.toString();
        }

        Assert.notNull(paymentTransactionEntry.getTransactionStatus(),
                "Payment transaction entry status cannot be null");


        // If payment transaction entry has REVIEW status, we'll try to retrieve from the gateway 
        // and try again if it can't be found.
        // REVIEW status could occur either if the payment gateway returned REVIEW the first time,
        // OR if Hybris went down during the initial capture (in which case the business process is
        // restarted by the PaymentsInProgress resumer).

        if (paymentTransactionEntry.getTransactionStatus().equals(TransactionStatus.REVIEW.toString())) {
            if (order.getPaymentInfo() instanceof ZippayPaymentInfoModel) {
                retryCapture(order, paymentTransactionEntry);
            } else {
                try {
                    targetPaymentService.retrieveTransactionEntry(paymentTransactionEntry);
                } catch (final InvalidTransactionRequestException e) {
                    retryCapture(order, paymentTransactionEntry);
                }
            }
        }

        return doActionAndGetTransitionBasedOnTransactionStatus(paymentTransactionEntry, order, process);
    }

    private void retryCapture(final OrderModel order, final PaymentTransactionEntryModel paymentTransactionEntry) {
        final PaymentInfoModel paymentInfo = order.getPaymentInfo();
        Assert.notNull(paymentInfo, "PaymentInfo cannot be null");

        final CurrencyModel currency = order.getCurrency();
        Assert.notNull(currency, "Currency cannot be null");
        targetPaymentService.retryCapture(paymentTransactionEntry);
    }

    /**
     * based on pte transaction status return a transition
     * 
     * @param paymentTransactionEntry
     * @param order
     * @return String
     * @throws RetryLaterException
     */
    protected String doActionAndGetTransitionBasedOnTransactionStatus(
            final PaymentTransactionEntryModel paymentTransactionEntry,
            final OrderModel order, final OrderProcessModel process) throws RetryLaterException {

        if (TransactionStatus.REVIEW.toString().equals(paymentTransactionEntry.getTransactionStatus())) {
            throw new RetryLaterException(TgtbusprocConstants.RetryReason.RETRY_PAYMENT_FAILED);
        }

        if (TransactionStatus.REJECTED.toString().equals(paymentTransactionEntry.getTransactionStatus())
                || TransactionStatus.ERROR.toString().equals(paymentTransactionEntry.getTransactionStatus())) {

            getOrderProcessParameterHelper().setPaymentFailedReason(process, Transition.REJECTED.toString());
            return Transition.REJECTED.toString();
        }

        if (BooleanUtils.isTrue(order.getContainsPreOrderItems())) {
            if (checkInsufficientAmount(paymentTransactionEntry, order.getPreOrderDepositAmount())) {
                return Transition.INSUFFICIENT.toString();
            }
        }
        else {
            if (checkInsufficientAmount(paymentTransactionEntry, order.getTotalPrice())) {
                return Transition.INSUFFICIENT.toString();
            }
        }

        return Transition.OK.toString();
    }

    /**
     * This will check if the capturedAmount and Initial Deposit/TotalPrice has difference more than 0.05 cents
     * 
     * @param paymentTransactionEntry
     * @param amountPrice
     */
    private boolean checkInsufficientAmount(final PaymentTransactionEntryModel paymentTransactionEntry,
            final Double amountPrice) {

        return amountPrice.doubleValue() - paymentTransactionEntry.getAmount().doubleValue() > .005;

    }

    @Override
    protected int getAllowedRetries(final OrderProcessModel process) {

        final Integer allowedRetries = getPaymentMode(process).getAllowedPaymentRetries();
        Assert.notNull(allowedRetries, "allowedRetries cannot be null");
        return allowedRetries.intValue();
    }

    @Override
    protected int getRetryInterval(final OrderProcessModel process) {

        final Integer retryInterval = getPaymentMode(process).getPaymentRetryIntervalMillis();
        Assert.notNull(retryInterval, "retryInterval cannot be null");
        return retryInterval.intValue();
    }

    private StandardPaymentModeModel getPaymentMode(final OrderProcessModel process) {

        final OrderModel order = process.getOrder();
        Assert.notNull(order, "Order cannot be null");

        final StandardPaymentModeModel standardPaymentModeModel = (StandardPaymentModeModel)order.getPaymentMode();

        Assert.notNull(standardPaymentModeModel, "Payment mode cannot be null");

        return standardPaymentModeModel;
    }

    /**
     * @param targetPaymentService
     *            the targetPaymentService to set
     */
    @Required
    public void setTargetPaymentService(final TargetPaymentService targetPaymentService) {
        this.targetPaymentService = targetPaymentService;
    }

    /**
     * @param salesApplicationConfigService
     *            the salesApplicationConfigService to set
     */
    @Required
    public void setSalesApplicationConfigService(
            final TargetSalesApplicationConfigService salesApplicationConfigService) {
        this.salesApplicationConfigService = salesApplicationConfigService;
    }

}

/**
 * 
 */
package au.com.target.tgtlayby.actions;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;

import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.stock.TargetStockService;


public abstract class ReleaseOrderStockAction extends AbstractProceduralAction<OrderProcessModel> {

    protected TargetStockService targetStockService;


    protected void releaseAllStockForOrder(final OrderModel order) {
        Assert.notNull(order, "Order can't be empty!");

        final Set<ConsignmentModel> conList = order.getConsignments();
        if (CollectionUtils.isEmpty(conList)) {
            final List<AbstractOrderEntryModel> entries = order.getEntries();
            if (CollectionUtils.isEmpty(entries)) {
                return;
            }

            for (final AbstractOrderEntryModel entry : entries) {
                final ProductModel product = entry.getProduct();
                final int amount = (int)entry.getQuantity().longValue();

                targetStockService.release(product, amount, amount
                        + " stock has been released for "
                        + product.getName() + " from order number is " + order.getCode());
            }
        }
        else {
            for (final ConsignmentModel con : conList) {
                for (final ConsignmentEntryModel conEntry : con.getConsignmentEntries()) {
                    final AbstractOrderEntryModel entry = conEntry.getOrderEntry();
                    final ProductModel product = entry.getProduct();
                    final int amount = (int)entry.getQuantity().longValue();

                    targetStockService.release(product, amount, amount
                            + " stock has been released for "
                            + product.getName() + " from order number is " + order.getCode());
                }
            }
        }
    }

    /**
     * @param targetStockService
     *            the targetStockService to set
     */
    @Required
    public void setTargetStockService(final TargetStockService targetStockService) {
        this.targetStockService = targetStockService;
    }

}

/**
 * 
 */
package au.com.target.tgtlayby.attributes.dynamic;

import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.util.Assert;

import au.com.target.tgtlayby.model.PaymentDueConfigModel;


public class PaymentDueConfigHmcLabelProvider implements DynamicAttributeHandler<String, PaymentDueConfigModel> {

    @Override
    public String get(final PaymentDueConfigModel model) {
        Assert.notNull(model, "model cannot be null");

        final Integer dueDays = model.getDueDays();
        final Double duePercentage = model.getDuePercentage();
        final Date dueDate = model.getDueDate();

        if (dueDays == null && dueDate == null) {
            return "Initial payment of " + duePercentage + "% is due";
        }

        if (dueDays != null) {
            return duePercentage + "% is due in " + dueDays + " days";
        }

        else {
            return duePercentage + "% is due on " + formatDate(dueDate);
        }
    }

    /**
     * format a given date
     * 
     * @param date
     * @return String
     */

    protected String formatDate(final Date date) {
        if (date == null) {
            return "n/a";
        }
        final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(date);
    }

    @Override
    public void set(final PaymentDueConfigModel model, final String value) {
        throw new UnsupportedOperationException();
    }

}

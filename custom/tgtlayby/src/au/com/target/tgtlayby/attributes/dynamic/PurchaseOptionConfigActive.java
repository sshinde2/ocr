package au.com.target.tgtlayby.attributes.dynamic;

import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;

import java.util.Date;

import javax.validation.constraints.NotNull;

import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;


/**
 * Dynamic attribute handler for the PurchaseOptionConfig.active attribute.
 */
public class PurchaseOptionConfigActive extends
        AbstractDynamicAttributeHandler<Boolean, PurchaseOptionConfigModel> {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler#get(de.hybris.platform.servicelayer.model.AbstractItemModel)
     */
    @Override
    @NotNull
    public Boolean get(final PurchaseOptionConfigModel model) {
        final Date validFrom = model.getValidFrom();
        final Date validTo = model.getValidTo();
        final Date now = new Date();

        return Boolean.valueOf((validFrom == null || !now.before(validFrom))
                && (validTo == null || !now.after(validTo)));
    }

}

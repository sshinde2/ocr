package au.com.target.tgtlayby.cart;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartRestoration;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;

import java.math.BigDecimal;
import java.util.List;

import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;
import au.com.target.tgtlayby.cart.data.DeliveryModeInformation;



/**
 * Target Specific CommerceCart service that exposes methods to deal with cart operations.
 */
public interface TargetCommerceCartService extends CommerceCartService {


    /**
     * Adds to the (existing) {@link CartModel} the (existing) {@link ProductModel} in the given {@link UnitModel} and
     * with the given <code>quantity</code>. If in the cart already an entry with the given product and given purchase
     * option exists the given <code>quantity</code> is added to the the quantity of this cart entry. After this the
     * cart is calculated.
     * 
     * @param cartModel
     * @param productModel
     * @param quantity
     * @param unit
     * @return {@link CommerceCartModification}
     * @throws CommerceCartModificationException
     */
    CommerceCartModification addToCart(CartModel cartModel, ProductModel productModel, long quantity, UnitModel unit)
            throws CommerceCartModificationException;

    /**
     * 
     * @param cart
     * @param product
     * @param newQty
     * @return CommerceCartModification
     * @throws CommerceCartModificationException
     */
    CommerceCartModification updateEntry(CartModel cart, ProductModel product, final long newQty)
            throws CommerceCartModificationException;


    /**
     * @param cart
     * @param product
     * @param giftcardRecipientEntry
     * @param id
     *            PK of entry
     * @return CommerceCartModification
     * @throws CommerceCartModificationException
     */
    CommerceCartModification removeGiftCardRecipient(CartModel cart, ProductModel product,
            final int giftcardRecipientEntry, String id)
            throws CommerceCartModificationException;

    /**
     * perform SOH for master cart
     * 
     * @param cartModel
     * @return List<CommerceCartModification>
     * @throws CommerceCartModificationException
     */
    List<CommerceCartModification> performSOH(final CartModel cartModel) throws CommerceCartModificationException;

    /**
     * perform SOH for selected entries
     * 
     * @param cartModel
     * @param entries
     * @return List<CommerceCartModification>
     * @throws CommerceCartModificationException
     */
    public List<CommerceCartModification> performSOHOnEntries(final CartModel cartModel,
            final List<AbstractOrderEntryModel> entries) throws CommerceCartModificationException;

    /**
     * Return a list of objects holding information about each available delivery mode option
     * 
     * @param cartModel
     * @return List<DeliveryModeInformation>
     */
    List<DeliveryModeInformation> getDeliveryModeOptionsInformation(final CartModel cartModel);

    /**
     * 
     * Return a DeliveryModeInformation corresponding to the cart for the given deliveryMode.
     * 
     * @param cartModel
     * @param deliveryMode
     * @return DeliveryModeInformation null if there is no supported mode for the type
     */
    DeliveryModeInformation getDeliveryModeOptionInformation(final CartModel cartModel, DeliveryModeModel deliveryMode);

    /**
     * check to see that the cart layby amount is greater then miniumum purchase amount for layby
     * 
     * @param cartModel
     * @return true if layby purchase amount is greater then minimum purchase amount
     */
    boolean validateCartPurchaseAmount(final CartModel cartModel);

    /**
     * Determine if a delivery mode is applicable for the cart<br/>
     * ie if the cart has any products that have the delivery mode available.
     * 
     * @param deliveryMode
     * @param cartModel
     * @return true if the mode is applicable
     */
    boolean isDeliveryModeApplicableForCart(DeliveryModeModel deliveryMode, CartModel cartModel);

    /**
     * Remove missing products, and return true if some were removed.
     * 
     * @param cartModel
     */
    boolean removeMissingProducts(CartModel cartModel);

    /**
     * Restores the provided cart, with an option to cleanup the saved cart.
     * 
     * @param cartParameter
     *            The cart parameter
     * @param cleanupSavedCart
     *            If true, the saved cart is cleaned up
     * @return the restoration result
     * @throws CommerceCartRestorationException
     */
    CommerceCartRestoration restoreCart(final CommerceCartParameter cartParameter, final boolean cleanupSavedCart)
            throws CommerceCartRestorationException;

    /**
     * @param cartModel
     * @param product
     * @param giftcardRecipientEntry
     * @param id
     *            PK of entry
     * @param recipient
     * 
     * @return CommerceCartModification
     * @throws CommerceCartModificationException
     */
    CommerceCartModification updateGiftcardRecipientDetails(final CartModel cartModel, final ProductModel product,
            final int giftcardRecipientEntry, final String id, final GiftRecipientDTO recipient)
            throws CommerceCartModificationException;

    /**
     * inspect the AfterpayConfigModel to check if cart total is within thresholds of after pay. i.e. total > minimum
     * payment OR total < maximum payment
     * 
     * @return - boolean
     */
    boolean isCartWithinAfterpayThresholds(BigDecimal total);

    /**
     * check if the cart is excluded for afterpay
     * 
     * @param cartModel
     * @return boolean
     */
    boolean isExcludedForAfterpay(CartModel cartModel);

    /**
     * checks if preOrder product being added has all preOrder attributes correct.
     * 
     * @param product
     * @throws CommerceCartModificationException
     */
    void preOrderChecks(ProductModel product) throws CommerceCartModificationException;

    /**
     * checks for mixed baskets while adding preOrder product to cart
     * 
     * @param product
     * @param cartModel
     * @return - true, if for preOrder product user tries a mixed basket
     * @throws CommerceCartModificationException
     *             - exception, if any of mixed basket check fails for preOrder
     */
    boolean isPreOrderMixedBasket(CartModel cartModel, ProductModel product)
            throws CommerceCartModificationException;

    /**
     * check if the cart is excluded for zip
     * 
     * @param cartModel
     * @return boolean
     */
    boolean isExcludedForZip(CartModel cartModel);
}

package au.com.target.tgtlayby.cart.dao;

//CHECKSTYLE:OFF
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Collections;
import java.util.List;


//CHECKSTYLE:ON


/**
 * Data Access Object oriented on Cart and Cart entries
 * 
 */
public interface TargetCartDao {

    /**
     * @param cartModel
     * @return List of matching cart entries, or {@link Collections#EMPTY_LIST} in case if no entries were found.
     */
    List<CartEntryModel> findEntriesForProduct(CartModel cartModel, ProductModel productModel);


    /**
     * Return the most recent cart for the given customer
     * 
     * @param customer
     * @return the master cart or null if there is none found
     */
    CartModel getMostRecentCartForUser(CustomerModel customer);

}

package au.com.target.tgtlayby.cart.impl;

//CHECKSTYLE:OFF
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.order.AbstractOrderEntryService;
import de.hybris.platform.order.impl.DefaultAbstractOrderEntryService;


//CHECKSTYLE:ON

/**
 * Marker interface that defines {@link AbstractOrderEntryService} for {@link CartEntryModel} type.
 */
public class TargetCartEntryService extends DefaultAbstractOrderEntryService<CartEntryModel> {
    //
}

package au.com.target.tgtlayby.cart.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.order.CommerceCartRestoration;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.CommerceDeliveryModeStrategy;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeValueModel;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.calculation.OrderRequiresCalculationStrategy;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.PriceValue;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.exception.TargetNoPostCodeException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.giftcards.GiftCardService;
import au.com.target.tgtcore.giftcards.converter.GiftRecipientFormConverter;
import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.GiftRecipientModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.order.TargetCommerceCartModificationStatus;
import au.com.target.tgtcore.order.TargetPreOrderService;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.service.FluentStockLookupService;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtlayby.cart.data.DeliveryModeInformation;
import au.com.target.tgtlayby.constants.TgtlaybyConstants;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtpayment.model.AfterpayConfigModel;
import au.com.target.tgtpayment.service.AfterpayConfigService;
import au.com.target.tgtutility.util.TargetProductUtils;


/**
 * Implementation of {@link TargetCommerceCartService}
 */
public class TargetCommerceCartServiceImpl extends DefaultCommerceCartService implements TargetCommerceCartService {

    private static final String ENTRY_NOT_FOUND = "Entry not found for the product={0} in the cart";

    private static final String INVALID_GIFTCARD_PRODUCT = "Cannot remove recipient since the product={0} is not a giftcard product";

    private static final String INVALID_GIFTCARD_RECIPIENTENTRY = "Cannot remove recipient since the recipient entry={0} for the product={1} is not valid";

    private static final String INVALID_GIFTCARD_RECIPIENTPK = "Cannot remove recipient since the recipient entry={0} with PK={1} for the product={2} is not valid";

    private static final Logger LOG = Logger.getLogger(TargetCommerceCartServiceImpl.class);

    private TargetLaybyCartService targetLaybyCartService;

    private TargetDeliveryService targetDeliveryService;

    private SearchRestrictionService searchRestrictionService;

    private TargetVoucherService targetVoucherService;

    private ProductService productService;

    private TargetStockService stockService;

    private CartService cartService;

    private CalculationService calculationService;

    private GiftCardService giftCardService;

    private OrderRequiresCalculationStrategy orderRequiresCalculationStrategy;

    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    private CommerceDeliveryModeStrategy commerceDeliveryModeStrategy;

    private GiftRecipientFormConverter giftRecipientFormConverter;

    private TargetFeatureSwitchService targetFeatureSwitchService;

    private AfterpayConfigService afterpayConfigService;

    private TargetSharedConfigService targetSharedConfigService;

    private FluentStockLookupService fluentStockLookupService;

    private TargetPreOrderService targetPreOrderService;

    private TargetSalesApplicationService targetSalesApplicationService;


    @Override
    public CommerceCartModification addToCart(final CartModel cartModel, final ProductModel productModel,
            final long quantityToAdd, final UnitModel unit) throws CommerceCartModificationException {

        essentialCheck(cartModel, productModel, quantityToAdd);
        preOrderChecks(productModel);
        final CommerceCartModification modification = new CommerceCartModification();
        UnitModel orderableUnit = unit;
        if (orderableUnit == null) {
            orderableUnit = findOrderableUnitFromProduct(productModel);
        }

        if (isProductPreview(productModel)) {
            LOG.info("addToCart: Preview product not added to cart, productCode=" + productModel.getCode());
            modification.setStatusCode(TargetCommerceCartModificationStatus.PREVIEW);
            return modification;
        }

        if (isPreOrderMixedBasket(cartModel, productModel)) {
            LOG.info("addToCart: Mixed basket not allowed with pre order products = " + productModel.getCode());
            modification.setStatusCode(TargetCommerceCartModificationStatus.NO_MIXED_BASKET_WITH_PRE_ORDERS);
            return modification;
        }
        final long existingQtyOfEntry = existingQuantityOfEntryInCart(productModel, cartModel);
        long availableStockValue;
        try {
            availableStockValue = getAvailableStockLevelForCurrentProduct(productModel);
        }
        catch (final FluentClientException e) {
            throw new CommerceCartModificationException(null, e);
        }
        final long adjustedTotalQuantity = adjustQuantityFromStockAvailability(availableStockValue,
                adjustQuantityFromMaxOrderQuantity(productModel, existingQtyOfEntry + quantityToAdd));

        if (modificationStatusAfterAddToCartValidations(cartModel, productModel, modification,
                existingQtyOfEntry, adjustedTotalQuantity, availableStockValue)
                .getStatusCode() != null) {
            return modification;
        }

        final long actualQuantityToAdd = adjustedTotalQuantity - existingQtyOfEntry;
        addEntryToCart(cartModel, productModel, modification, orderableUnit, actualQuantityToAdd);
        final boolean isPreOrder = BooleanUtils.isTrue(cartModel.getContainsPreOrderItems());
        if (actualQuantityToAdd == quantityToAdd) {
            modification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
        }
        else {
            modification.setStatusCode(getCartModificationStatus(isPreOrder, CommerceCartModificationStatus.LOW_STOCK));

        }

        return modification;

    }

    /**
     * @param cartModel
     * @param productModel
     * @param modification
     * @param orderableUnit
     * @param actualQuantityToAdd
     */
    private void addEntryToCart(final CartModel cartModel, final ProductModel productModel,
            final CommerceCartModification modification, final UnitModel orderableUnit,
            final long actualQuantityToAdd) {

        final CartEntryModel cartEntryModel = targetLaybyCartService.addNewEntry(cartModel, productModel,
                actualQuantityToAdd, orderableUnit);
        getModelService().save(cartEntryModel);
        getModelService().refresh(cartModel);
        final CommerceCartParameter cartParameter = new CommerceCartParameter();
        cartParameter.setCart(cartModel);
        calculateCart(cartParameter);
        getModelService().save(cartEntryModel);
        modification.setQuantityAdded(actualQuantityToAdd);
        if (cartEntryModel != null) {
            modification.setQuantity(actualQuantityToAdd);
            modification.setEntry(cartEntryModel);
        }
    }

    /**
     * @param cartModel
     * @param productModel
     * @param modification
     * @param existingQtyOfEntry
     * @param adjustedTotalQuantity
     * @return cart modification with appropriate validation status
     * 
     */
    private CommerceCartModification modificationStatusAfterAddToCartValidations(final CartModel cartModel,
            final ProductModel productModel,
            final CommerceCartModification modification, final long existingQtyOfEntry,
            final long adjustedTotalQuantity, final long availableStockValue) {
        if (isLessThanMinOrderQty(productModel.getMinOrderQuantity(),
                adjustedTotalQuantity)) {
            modification.setQuantity(productModel.getMinOrderQuantity().intValue());
            modification
                    .setStatusCode(TargetCommerceCartModificationStatus.NO_MODFICATION_OCCURED_INSUFFICIENT_QUANTITY);
        }
        else if (existingQtyOfEntry == 0L && isNumberOfCartEntriesMoreThanOrEqualToThreshold(cartModel)) {
            modification.setStatusCode(
                    TargetCommerceCartModificationStatus.NO_MODIFICATION_OCCURED_CART_ENTRIES_THRESHOLD_REACHED);
            LOG.warn("addToCart: Number of cart entries reached threshold for cart with code= " + cartModel.getCode()
                    + " guid= "
                    + cartModel.getGuid());
            modification.setQuantityAdded(0);
        }
        else if ((adjustedTotalQuantity - existingQtyOfEntry) <= 0) {
            modification.setStatusCode(getCartModificationStatus(
                    TargetProductUtils.isProductOpenForPreOrder(productModel),
                    getCommerceCartModificationStatus(availableStockValue, adjustedTotalQuantity)));
            modification.setQuantityAdded(0);
        }
        return modification;
    }

    /**
     * This method will get the modification status for no stock and threshold reached
     * 
     * @param availableStockValue
     * @param adjustedTotalQuantity
     * @return TargetCommerceCartModificationStatus
     */
    private String getCommerceCartModificationStatus(final long availableStockValue, final long adjustedTotalQuantity) {
        if ((availableStockValue - adjustedTotalQuantity) > 0) {
            return TargetCommerceCartModificationStatus.ITEMS_THRESHOLD_REACHED;
        }
        return TargetCommerceCartModificationStatus.NO_STOCK;
    }

    /**
     * 
     * @param cartModel
     * @return true if cart already contains entries equal to threshold or more than threshold
     */
    private boolean isNumberOfCartEntriesMoreThanOrEqualToThreshold(final CartModel cartModel) {
        return (cartModel.getEntries() != null) && (cartModel.getEntries().size() >= targetSharedConfigService.getInt(
                TgtCoreConstants.Config.CART_ENTRY_THRESHOLD,
                Integer.MAX_VALUE));
    }

    /**
     * 
     * @param isPreOrder
     * @param modificationStatus
     * @return status
     */
    private String getCartModificationStatus(final boolean isPreOrder, final String modificationStatus) {
        final String preOrderPrefix = isPreOrder ? TgtlaybyConstants.PREORDER + TgtCoreConstants.Seperators.DOT : "";
        return StringUtils.isNotEmpty(preOrderPrefix) ? preOrderPrefix + modificationStatus : modificationStatus;
    }

    /**
     * @param minOrderQuantity
     * @param quantityToAdd
     * @return true if quantity is less than minOrderQty
     */
    private boolean isLessThanMinOrderQty(final Integer minOrderQuantity, final long quantityToAdd) {
        if (minOrderQuantity != null) {
            return quantityToAdd > 0L && minOrderQuantity.longValue() > quantityToAdd;
        }
        return false;
    }

    /**
     * @param productModel
     * @return {@link UnitModel}
     * @throws CommerceCartModificationException
     */
    @SuppressWarnings("deprecation")
    private UnitModel findOrderableUnitFromProduct(final ProductModel productModel)
            throws CommerceCartModificationException {
        UnitModel orderableUnit = null;
        try {
            orderableUnit = productService.getOrderableUnit(productModel);
        }
        catch (final ModelNotFoundException e) {
            throw new CommerceCartModificationException(e.getMessage(), e);
        }
        return orderableUnit;
    }

    @Override
    public void preOrderChecks(final ProductModel productModel) throws CommerceCartModificationException {

        if (TargetProductUtils.isProductEmbargoed(productModel)) {

            final boolean isSalesChannelKiosk = SalesApplication.KIOSK.toString()
                    .equalsIgnoreCase(targetSalesApplicationService.getCurrentSalesApplication().getCode());

            if (isSalesChannelKiosk) {
                throw new CommerceCartModificationException(
                        TgtCoreConstants.CART.ERR_PREORDER_CART);
            }

            final TargetColourVariantProductModel colourVariantProduct = TargetProductUtils
                    .getColourVariantProduct((AbstractTargetVariantProductModel)productModel);

            // if embargoed product added to basket, then must have preOrder start and end dates
            if (colourVariantProduct.getPreOrderStartDateTime() == null
                    || colourVariantProduct.getPreOrderEndDateTime() == null
                    || !TargetProductUtils.isProductOpenForPreOrder(colourVariantProduct)) {
                throw new CommerceCartModificationException(TgtCoreConstants.CART.ERR_PREORDER_CART);
            }
        }
    }

    @Override
    public boolean isPreOrderMixedBasket(final CartModel cartModel, final ProductModel product)
            throws CommerceCartModificationException {

        final List<AbstractOrderEntryModel> cartEntries = cartModel.getEntries();
        // if cart empty allow to add preOrder
        if (CollectionUtils.isNotEmpty(cartEntries)) {
            // cart not empty, check for preOrder
            final boolean isPreOrderProduct = TargetProductUtils.isProductOpenForPreOrder(product);
            if (isPreOrderProduct) {
                // pre order product can't be added with other products
                final List<CartEntryModel> entriesForProduct = cartService.getEntriesForProduct(cartModel, product);
                if (CollectionUtils.isEmpty(entriesForProduct)) {
                    return true;
                }
            }
            else {
                // normal product added
                final boolean cartHavingPreOrderProduct = targetPreOrderService.doesCartHavePreOrderProduct(cartModel);
                if (cartHavingPreOrderProduct) {
                    return true;
                }
            }
        }
        return false;

    }

    /**
     * @param cartModel
     * @param productModel
     * @param quantityToAdd
     * @throws CommerceCartModificationException
     */
    private void essentialCheck(final CartModel cartModel, final ProductModel productModel,
            final long quantityToAdd)
            throws CommerceCartModificationException {
        ServicesUtil.validateParameterNotNull(cartModel, "Cart model cannot be null");
        ServicesUtil.validateParameterNotNull(productModel, "Product model cannot be null");

        if (productModel.getVariantType() != null && CollectionUtils.isNotEmpty(productModel.getVariants())) {
            throw new CommerceCartModificationException("Choose a variant instead of the base product");
        }

        if (quantityToAdd < 1) {
            throw new CommerceCartModificationException("Quantity must not be less than one");
        }
    }

    /**
     * @param cartModel
     * @param productModel
     * @throws CommerceCartModificationException
     */
    private void essentialCheck(final CartModel cartModel, final ProductModel productModel)
            throws CommerceCartModificationException {
        // do not do quantity check here
        essentialCheck(cartModel, productModel, 1);
    }

    @Override
    public CommerceCartModification updateEntry(final CartModel cartModel, final ProductModel product,
            final long newQuantity) throws CommerceCartModificationException {

        essentialCheck(cartModel, product);
        final CommerceCartModification modification = new CommerceCartModification();
        final CartEntryModel entryToUpdate = targetLaybyCartService.getEntryForProduct(
                cartModel, product);

        if (entryToUpdate == null) {
            throw new CommerceCartModificationException(MessageFormat.format(ENTRY_NOT_FOUND, product.getCode()));
        }

        if (newQuantity == 0) {
            removeEntryAndPopulateModification(entryToUpdate, cartModel, product, modification);
            modification.setStatusCode(TargetCommerceCartModificationStatus.SUCCESS);
            return modification;
        }

        long adjustedTotalQuantity;
        try {
            adjustedTotalQuantity = adjustQuantityFromStockAvailability(
                    getAvailableStockLevelForCurrentProduct(product),
                    adjustQuantityFromMaxOrderQuantity(product,
                            newQuantity));
        }
        catch (final FluentClientException e) {
            modification.setStatusCode(TargetCommerceCartModificationStatus.FLUENT_LOOKUP_FAILED);
            return modification;
        }

        if (modificationStatusAfterValidation(cartModel, entryToUpdate, modification,
                adjustedTotalQuantity).getStatusCode() != null) {
            return modification;
        }
        final long quantityChange = adjustedTotalQuantity - entryToUpdate.getQuantity().longValue();
        calculateAndPrepareModificationRecord(cartModel, product, modification, entryToUpdate,
                quantityChange, Long.valueOf(adjustedTotalQuantity));
        if (newQuantity == adjustedTotalQuantity) {
            modification.setStatusCode(TargetCommerceCartModificationStatus.SUCCESS);
        }
        else {
            modification.setStatusCode(TargetCommerceCartModificationStatus.LOW_STOCK);
        }
        return modification;
    }

    /**
     * 
     * @param cartModel
     * @param entry
     * @param availableStock
     * @return CartModifications
     * @throws CommerceCartModificationException
     */
    private CommerceCartModification performSOHOnEntry(final CartModel cartModel, final AbstractOrderEntryModel entry,
            final long availableStock) throws CommerceCartModificationException {

        final CommerceCartModification modification = new CommerceCartModification();
        final ProductModel product = entry.getProduct();
        essentialCheck(cartModel, product);

        final long existingQtyOfEntry = entry.getQuantity().longValue();
        final long adjustedTotalQuantity = adjustQuantityFromStockAvailability(availableStock,
                adjustQuantityFromMaxOrderQuantity(product,
                        existingQtyOfEntry));

        if (modificationStatusAfterValidation(cartModel, entry, modification, adjustedTotalQuantity)
                .getStatusCode() != null) {
            return modification;
        }

        final long quantityChange = adjustedTotalQuantity - existingQtyOfEntry;
        calculateAndPrepareModificationRecord(cartModel, product, modification, entry,
                quantityChange, Long.valueOf(adjustedTotalQuantity));
        modification.setStatusCode(TargetCommerceCartModificationStatus.LOW_STOCK);
        return modification;

    }

    /**
     * 
     * @param cartModel
     * @param entry
     * @param modification
     * @param adjustedTotalQuantity
     * @return cart modification with appropriate validation status
     */
    private CommerceCartModification modificationStatusAfterValidation(final CartModel cartModel,
            final AbstractOrderEntryModel entry, final CommerceCartModification modification,
            final long adjustedTotalQuantity) {

        final long existingQtyOfEntry = entry.getQuantity().longValue();
        final ProductModel product = entry.getProduct();

        if (isProductPreview(product)) {
            LOG.info("Preview product removed from cart, productCode=" + product.getCode());
            removeEntryAndPopulateModification(entry, cartModel, product, modification);
            modification.setStatusCode(TargetCommerceCartModificationStatus.PREVIEW);
        }
        else if (isLessThanMinOrderQty(product.getMinOrderQuantity(), adjustedTotalQuantity)) {
            if (isLessThanMinOrderQty(product.getMinOrderQuantity(), existingQtyOfEntry)) {
                removeEntryAndPopulateModification(entry, cartModel, product, modification);
                modification.setStatusCode(TargetCommerceCartModificationStatus.NO_STOCK);
            }
            else {
                modification.setQuantity(product.getMinOrderQuantity().intValue());
                modification.setStatusCode(
                        TargetCommerceCartModificationStatus.NO_MODFICATION_OCCURED_INSUFFICIENT_QUANTITY);
            }
        }
        else if (adjustedTotalQuantity == existingQtyOfEntry) {
            modification.setEntry(entry);
            modification.setProduct(product);
            modification.setQuantity(existingQtyOfEntry);
            modification.setStatusCode(TargetCommerceCartModificationStatus.NO_MODIFICATION_OCCURRED);
        }
        else if (adjustedTotalQuantity <= 0) {
            removeEntryAndPopulateModification(entry, cartModel, product, modification);
            modification.setStatusCode(TargetCommerceCartModificationStatus.NO_STOCK);
        }
        return modification;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtlayby.cart.TargetCommerceCartService#removeGiftCardRecipient(de.hybris.platform.core.model.order.CartModel, de.hybris.platform.core.model.product.ProductModel, int)
     */
    @Override
    public CommerceCartModification removeGiftCardRecipient(final CartModel cart, final ProductModel product,
            final int giftcardRecipientEntry, final String id) throws CommerceCartModificationException {
        final CommerceCartModification modification = new CommerceCartModification();

        essentialCheck(cart, product);

        final CartEntryModel entryToUpdate = targetLaybyCartService.getEntryForProduct(cart, product);


        essentialCheckForGiftcards(entryToUpdate, product, giftcardRecipientEntry, id);

        final ArrayList<GiftRecipientModel> giftRecipients = new ArrayList<>(
                entryToUpdate.getGiftRecipients());

        giftRecipients.remove(giftcardRecipientEntry);

        if (CollectionUtils.isEmpty(giftRecipients)) {
            removeEntryAndPopulateModification(entryToUpdate, cart, product, modification);
        }
        else {
            // Adjust the entry quantity to the new value
            final long quantity = entryToUpdate.getQuantity().longValue();
            final Long newQuantity = Long.valueOf(quantity - 1);
            entryToUpdate.setGiftRecipients(giftRecipients);
            calculateAndPrepareModificationRecord(cart, product, modification, entryToUpdate, -1, newQuantity);
        }

        modification.setStatusCode(TargetCommerceCartModificationStatus.SUCCESS);
        return modification;
    }

    /**
     * @param cart
     * @param product
     * @param modification
     * @param entryToUpdate
     * @param newQuantity
     */
    protected void calculateAndPrepareModificationRecord(final CartModel cart, final ProductModel product,
            final CommerceCartModification modification, final AbstractOrderEntryModel entryToUpdate,
            final long actualAllowedQuantityChange, final Long newQuantity) {
        entryToUpdate.setQuantity(newQuantity);
        getModelService().save(entryToUpdate);
        getModelService().refresh(cart);
        final CommerceCartParameter cartParameter = new CommerceCartParameter();
        cartParameter.setCart(cart);
        calculateCart(cartParameter);
        getModelService().refresh(entryToUpdate);


        // Return the modification data
        modification.setQuantityAdded(actualAllowedQuantityChange);
        modification.setEntry(entryToUpdate);
        modification.setQuantity(newQuantity.longValue());
        modification.setProduct(product);
    }

    /***
     * {@inheritDoc}
     */
    @Override
    public CommerceCartModification updateGiftcardRecipientDetails(final CartModel cart, final ProductModel product,
            final int giftcardRecipientEntry, final String id, final GiftRecipientDTO recipient)
            throws CommerceCartModificationException {
        final CommerceCartModification modification = new CommerceCartModification();

        essentialCheck(cart, product);

        final CartEntryModel entryToUpdate = targetLaybyCartService.getEntryForProduct(cart, product);
        essentialCheckForGiftcards(entryToUpdate, product, giftcardRecipientEntry, id);

        // Get the recipient to update based on index
        // the essential check guarantees we will get one.
        final List<GiftRecipientModel> giftRecipients = entryToUpdate.getGiftRecipients();
        final GiftRecipientModel recipientModel = giftRecipients.get(giftcardRecipientEntry);

        giftRecipientFormConverter.update(recipientModel, recipient);

        calculateAndPrepareModificationRecord(cart, product, modification, entryToUpdate, 0,
                entryToUpdate.getQuantity());

        modification.setStatusCode(TargetCommerceCartModificationStatus.SUCCESS);
        return modification;
    }

    /**
     * @param entryToUpdate
     * @param product
     * @param giftcardRecipientEntry
     * @param id
     * @throws CommerceCartModificationException
     */
    private void essentialCheckForGiftcards(final CartEntryModel entryToUpdate, final ProductModel product,
            final int giftcardRecipientEntry, final String id) throws CommerceCartModificationException {
        final String productCode = product.getCode();
        if (!giftCardService.isProductAGiftCard(product)) {
            throw new CommerceCartModificationException(MessageFormat.format(INVALID_GIFTCARD_PRODUCT,
                    productCode));
        }

        if (entryToUpdate == null) {
            throw new CommerceCartModificationException(MessageFormat.format(ENTRY_NOT_FOUND, productCode));
        }

        final List<GiftRecipientModel> giftRecipients = entryToUpdate.getGiftRecipients();
        if (CollectionUtils.isEmpty(giftRecipients)
                || giftRecipients.size() <= giftcardRecipientEntry) {
            throw new CommerceCartModificationException(MessageFormat.format(INVALID_GIFTCARD_RECIPIENTENTRY,
                    Integer.valueOf(giftcardRecipientEntry), productCode));
        }

        final GiftRecipientModel recipient = giftRecipients.get(giftcardRecipientEntry);
        if (!StringUtils.equals(recipient.getPk().toString(), id)) {
            throw new CommerceCartModificationException(MessageFormat.format(INVALID_GIFTCARD_RECIPIENTPK,
                    Integer.valueOf(giftcardRecipientEntry), id, productCode));
        }
    }

    /**
     * @param entryToUpdate
     * @param cartModel
     * @param product
     * @param modification
     */
    private void removeEntryAndPopulateModification(final AbstractOrderEntryModel entryToUpdate,
            final CartModel cartModel,
            final ProductModel product, final CommerceCartModification modification) {
        getModelService().remove(entryToUpdate);
        getModelService().refresh(cartModel);

        final CommerceCartParameter cartParameter = new CommerceCartParameter();
        cartParameter.setCart(cartModel);
        calculateCart(cartParameter);

        modification.setEntry(entryToUpdate);
        modification.setQuantity(0);
        modification.setProduct(product);

        modification.setQuantityAdded(-entryToUpdate.getQuantity().longValue());
    }

    @Override
    public boolean removeMissingProducts(final CartModel cartModel) {

        ServicesUtil.validateParameterNotNull(cartModel, "Cart model cannot be null");

        final List<CartEntryModel> entriesToRemove = new ArrayList<>();

        final List<AbstractOrderEntryModel> entries = cartModel.getEntries();
        for (final AbstractOrderEntryModel entry : entries) {
            final String prodCode = entry.getProduct().getCode();

            // Check if the product and it's base are still available
            try {
                final ProductModel product = productService.getProductForCode(prodCode);
                if (product instanceof AbstractTargetVariantProductModel) {

                    final ProductModel baseProduct = getBaseTargetProduct((AbstractTargetVariantProductModel)product);
                    productService.getProductForCode(baseProduct.getCode());
                }
            }
            catch (final UnknownIdentifierException e) {
                LOG.info("Removing missing product from customer cart " + prodCode);
                entriesToRemove.add((CartEntryModel)entry);
            }
        }

        // Remove any unavailable entries and recalculate
        if (CollectionUtils.isNotEmpty(entriesToRemove)) {
            getModelService().removeAll(entriesToRemove);
            getModelService().refresh(cartModel);
            final CommerceCartParameter cartParameter = new CommerceCartParameter();
            cartParameter.setCart(cartModel);
            calculateCart(cartParameter);
            return true;
        }

        return false;
    }

    private TargetProductModel getBaseTargetProduct(final AbstractTargetVariantProductModel targetVariantProduct) {
        final ProductModel product = targetVariantProduct.getBaseProduct();

        if (product != null) {
            if (product instanceof TargetProductModel) {
                return (TargetProductModel)product;
            }
            else if (product instanceof AbstractTargetVariantProductModel) {
                return getBaseTargetProduct((AbstractTargetVariantProductModel)product);
            }
        }

        return null;
    }

    @Override
    public List<CommerceCartModification> performSOH(final CartModel cartModel)
            throws CommerceCartModificationException {
        Assert.notNull(cartModel);

        return performSOHOnEntries(cartModel, cartModel.getEntries());
    }

    @Override
    public List<CommerceCartModification> performSOHOnEntries(final CartModel cartModel,
            final List<AbstractOrderEntryModel> entries) throws CommerceCartModificationException {
        if (CollectionUtils.isEmpty(entries)) {
            return Collections.emptyList();
        }
        final List<CommerceCartModification> cartModifications = new ArrayList<>();
        try {
            final Map<String, Integer> stockMap = getStockMapForEntries(entries);
            searchRestrictionService.disableSearchRestrictions();
            for (final AbstractOrderEntryModel entry : entries) {
                final ProductModel productModel = entry.getProduct();
                if (productModel != null) {
                    final long availableStock = stockMap.get(productModel.getCode()).longValue();
                    cartModifications
                            .add(performSOHOnEntry(cartModel, entry, availableStock));
                }
            }
        }
        catch (final FluentClientException e) {
            throw new CommerceCartModificationException(null, e);
        }
        catch (final Exception e) {
            LOG.warn("Cannot check cart models");
        }
        finally {
            searchRestrictionService.enableSearchRestrictions();
        }
        modifyDMIfUpdatedCartIsDigitalOnly(cartModel);
        return cartModifications;
    }

    private void modifyDMIfUpdatedCartIsDigitalOnly(final CartModel cart) {
        if (isCheckOnCartDeliveryModeRequired(cart)) {
            final List<AbstractOrderEntryModel> orderEntries = cart.getEntries();
            if (CollectionUtils.isNotEmpty(orderEntries)) {
                TargetZoneDeliveryModeModel deliveryMode = null;
                for (final AbstractOrderEntryModel entry : orderEntries) {
                    if (null != entry) {
                        deliveryMode = targetDeliveryModeHelper.getDigitalDeliveryMode(entry.getProduct());
                        if (null == deliveryMode) {
                            return;
                        }
                    }
                }
                // force cnc store number and delivery address to null when the delivery mode is digital
                cart.setCncStoreNumber(null);
                cart.setDeliveryAddress(null);
                getModelService().save(cart);

                final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
                checkoutParameter.setCart(cart);
                checkoutParameter.setDeliveryMode(deliveryMode);
                commerceDeliveryModeStrategy.setDeliveryMode(checkoutParameter);
            }
        }
    }

    /**
     * 
     * @param entries
     * @return stock map containing sku code and corresponding stock value
     * @throws FluentClientException
     */
    private Map<String, Integer> getStockMapForEntries(final List<AbstractOrderEntryModel> entries)
            throws FluentClientException {
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)) {
            return getStockMapUsingFluent(entries);
        }
        return getStockMap(entries);
    }

    /**
     * 
     * @param entries
     * @return Map with sku code and stock value from fluent
     * @throws FluentClientException
     */
    private Map<String, Integer> getStockMapUsingFluent(final List<AbstractOrderEntryModel> entries)
            throws FluentClientException {
        final Map<String, Integer> stockMap = new HashMap<>();
        final List<String> skuCodes = new ArrayList<>();
        for (final AbstractOrderEntryModel entry : entries) {
            skuCodes.add(entry.getProduct().getCode());
        }
        stockMap.putAll(fluentStockLookupService.lookupAts(skuCodes.toArray(new String[] {})));
        return stockMap;
    }

    /**
     * 
     * @param entries
     * @return Map with sku code and stock value
     */
    private Map<String, Integer> getStockMap(final List<AbstractOrderEntryModel> entries) {
        final Map<String, Integer> stockMap = new HashMap<>();
        for (final AbstractOrderEntryModel entry : entries) {
            stockMap.put(entry.getProduct().getCode(), getStockQty(entry.getProduct()));
        }
        return stockMap;
    }

    /**
     * 
     * @param productModel
     * @return Stock level quantity
     */
    private Integer getStockQty(final ProductModel productModel) {
        if (TargetProductUtils.isProductOpenForPreOrder(productModel)) {
            return Integer.valueOf(targetPreOrderService.getAvailablePreOrderQuantityForCurrentProduct(productModel));
        }
        return Integer.valueOf(stockService
                .getStockLevelAmountFromOnlineWarehouses(productModel));
    }


    private boolean isCheckOnCartDeliveryModeRequired(final CartModel cart) {
        final DeliveryModeModel currentDeliveryMode = cart.getDeliveryMode();
        if (null == currentDeliveryMode) {
            return true;
        }
        else if (currentDeliveryMode instanceof TargetZoneDeliveryModeModel) {
            final TargetZoneDeliveryModeModel tgtCurrentDeliveryMode = (TargetZoneDeliveryModeModel)currentDeliveryMode;
            if (BooleanUtils.isFalse(tgtCurrentDeliveryMode.getIsDigital())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 
     * @param productModel
     * @param quantityToAdd
     * @return adjusted quantity from max quantity order
     */
    private long adjustQuantityFromMaxOrderQuantity(final ProductModel productModel,
            final long quantityToAdd) {
        final Integer maxOrderQty = productModel.getMaxOrderQuantity();
        long maxAllowedQuantity = quantityToAdd;
        if (maxOrderQty != null) {
            maxAllowedQuantity = maxAllowedQuantity > maxOrderQty.longValue() ? maxOrderQty.longValue()
                    : maxAllowedQuantity;
        }

        if (TargetProductUtils.isProductOpenForPreOrder(productModel)) {
            final int globalMaxPreOrderQuantity = targetSharedConfigService
                    .getInt(TgtCoreConstants.Config.MAX_PREORDER_THRESHOLD, 2);
            return maxAllowedQuantity > globalMaxPreOrderQuantity ? globalMaxPreOrderQuantity : maxAllowedQuantity;
        }
        return maxAllowedQuantity;
    }

    /**
     * 
     * @param stockLevel
     * @param quantityToAdd
     * @return adjusted quantity from stock availability
     */
    private long adjustQuantityFromStockAvailability(final long stockLevel,
            final long quantityToAdd) {
        return quantityToAdd > stockLevel ? stockLevel : quantityToAdd;
    }

    /**
     * @param productModel
     * @return available stock level
     * @throws FluentClientException
     */
    private long getAvailableStockLevelForCurrentProduct(final ProductModel productModel) throws FluentClientException {
        Assert.notNull(productModel);
        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)) {
            return availableToSellStockValueFromFluent(productModel.getCode());
        }
        if (TargetProductUtils.isProductOpenForPreOrder(productModel)) {
            return targetPreOrderService.getAvailablePreOrderQuantityForCurrentProduct(productModel);
        }
        return stockService.getStockLevelAmountFromOnlineWarehouses(productModel);
    }

    /**
     * @param skuRef
     * @return stock value
     * @throws FluentClientException
     */
    private long availableToSellStockValueFromFluent(final String skuRef) throws FluentClientException {
        final Map<String, Integer> stockMap = fluentStockLookupService.lookupAts(skuRef);
        final Integer stock = stockMap.get(skuRef);
        return stock != null ? stock.longValue() : 0L;
    }

    @Override
    public List<DeliveryModeInformation> getDeliveryModeOptionsInformation(final CartModel cartModel) {
        Assert.notNull(cartModel);

        // Default to empty list 
        List<DeliveryModeInformation> listInformations = Collections.emptyList();

        // If there are no cart entries then return no options
        final List<AbstractOrderEntryModel> entries = cartModel.getEntries();
        if (CollectionUtils.isEmpty(entries)) {
            return listInformations;
        }

        // If there are no delivery modes supported for the order then return no options
        final List<DeliveryModeModel> deliveryModes = targetDeliveryService
                .getSupportedDeliveryModeListForOrder(cartModel);
        if (CollectionUtils.isEmpty(deliveryModes)) {
            return listInformations;
        }

        // Remember the original selected mode in the cart if there is one -
        // we need to temporarily reset it for the call to get delivery fee
        final DeliveryModeModel originalDeliveryMode = cartModel.getDeliveryMode();

        try {
            listInformations = createDeliveryModeInformations(deliveryModes, cartModel);
        }
        finally {
            // Whatever happens, restore the original delivery mode
            if (originalDeliveryMode == null) {
                cartModel.setDeliveryMode(null);
                cartModel.setZoneDeliveryModeValue(null);
            }
            else {
                targetLaybyCartService.setDeliveryMode(cartModel, originalDeliveryMode);
            }
        }

        return listInformations;
    }

    @Override
    public boolean validateCartPurchaseAmount(final CartModel cartModel) {
        Assert.notNull(cartModel);

        final PurchaseOptionConfigModel purchaseOptionConfig = cartModel.getPurchaseOptionConfig();
        if (purchaseOptionConfig == null) {
            LOG.error("There is no active Purchase Option Config found for cart " + cartModel.getCode());
            return true;
        }
        final Double minimumPurchaseAmount = purchaseOptionConfig.getMinimumPurchaseAmount();
        if (minimumPurchaseAmount != null) {
            if (cartModel.getSubtotal().doubleValue() < minimumPurchaseAmount
                    .doubleValue()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public DeliveryModeInformation getDeliveryModeOptionInformation(final CartModel cartModel,
            final DeliveryModeModel deliveryMode) {
        Assert.notNull(cartModel);
        Assert.notNull(deliveryMode);

        final List<DeliveryModeInformation> listModeInformations = getDeliveryModeOptionsInformation(cartModel);
        if (CollectionUtils.isEmpty(listModeInformations)) {
            return null;
        }

        // Pick out the information for the given mode
        DeliveryModeInformation retInfo = null;
        for (final DeliveryModeInformation info : listModeInformations) {
            if (info.getDeliveryMode() != null
                    && info.getDeliveryMode() instanceof TargetZoneDeliveryModeModel
                    && ((TargetZoneDeliveryModeModel)info.getDeliveryMode()).getCode().equals(deliveryMode.getCode())) {
                retInfo = info;
                break;
            }
        }

        return retInfo;
    }

    @Override
    public boolean isDeliveryModeApplicableForCart(final DeliveryModeModel deliveryMode,
            final CartModel cartModel) {
        Assert.notNull(deliveryMode);
        Assert.notNull(cartModel);

        boolean hasProductsAvailableForMode = false;

        if (cartModel.getEntries() != null) {
            for (final AbstractOrderEntryModel entry : cartModel.getEntries()) {
                final ProductModel product = entry.getProduct();
                final Set<DeliveryModeModel> productDeliveryModes = targetDeliveryService
                        .getVariantsDeliveryModesFromBaseProduct(product);
                if (productDeliveryModes != null && productDeliveryModes.contains(deliveryMode)) {
                    hasProductsAvailableForMode = true;
                    break;
                }
            }
        }

        return hasProductsAvailableForMode;
    }

    private List<DeliveryModeInformation> createDeliveryModeInformations(
            final Collection<DeliveryModeModel> deliveryModes,
            final CartModel cartModel) {

        Assert.notNull(cartModel);
        Assert.notEmpty(deliveryModes);

        // For each delivery mode supported, create an information object and add to the result list
        // (unless there are no products that support the mode)

        final List<DeliveryModeInformation> listInformations = new ArrayList<>();
        for (final DeliveryModeModel deliveryMode : deliveryModes) {
            // Check each product to see if it is available for this delivery mode.
            // Construct a list of excluded cart entries for each mode.
            // If none are available then flag and don't add an info for this mode!
            final boolean hasProductsAvailableForMode = isDeliveryModeApplicableForCart(deliveryMode, cartModel);

            if (hasProductsAvailableForMode) {
                final DeliveryModeInformation info = createDeliveryModeInformation(deliveryMode, cartModel);
                listInformations.add(info);
            }
        }

        return listInformations;
    }

    private DeliveryModeInformation createDeliveryModeInformation(final DeliveryModeModel deliveryMode,
            final CartModel cartModel) {
        Assert.notNull(cartModel);
        Assert.notNull(deliveryMode);

        AbstractTargetZoneDeliveryModeValueModel deliveryValue = null;
        PriceValue deliveryFee = null;
        try {
            // Get the delivery fee for this order for the given mode -
            // need to temporarily set the mode in the cart for the findDeliveryCostStrategy method
            targetLaybyCartService.setDeliveryMode(cartModel, deliveryMode);

            deliveryValue = cartModel.getZoneDeliveryModeValue();
            if (deliveryValue != null) {
                final double fee = targetDeliveryService.getDeliveryCostForDeliveryType(deliveryValue,
                        cartModel).getDeliveryCost();
                deliveryFee = new PriceValue(cartModel.getCurrency().getIsocode(), fee, cartModel.getNet()
                        .booleanValue());
            }
        }
        catch (final TargetNoPostCodeException ex) {
            deliveryValue = null;
            deliveryFee = null;
        }
        // Create an information object for the mode and fee and exclusions
        return new DeliveryModeInformation(deliveryMode, deliveryValue, deliveryFee);
    }


    @Override
    public CommerceCartRestoration restoreCart(final CommerceCartParameter cartParameter)
            throws CommerceCartRestorationException {
        return restoreCart(cartParameter, true);
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgtlayby.cart.TargetCommerceCartService#restoreCart(de.hybris.platform.commerceservices.service.data.CommerceCartParameter, boolean)
     */
    @Override
    public CommerceCartRestoration restoreCart(final CommerceCartParameter cartParameter,
            final boolean cleanupSavedCart)
            throws CommerceCartRestorationException {
        Assert.notNull(cartParameter);
        final CartModel savedCart = cartParameter.getCart();
        Assert.notNull(savedCart);
        try {
            if (cleanupSavedCart) {
                cleanupSavedCart(savedCart);
            }

            // always recalculate to evaluate the deals again - this should remove the expired deals
            savedCart.setCalculated(Boolean.FALSE);
            recalculateCart(cartParameter);

            final CommerceCartRestoration restoration = new CommerceCartRestoration();
            final List<CommerceCartModification> modifications = new ArrayList<>();
            modifications.addAll(performSOH(savedCart));
            restoration.setModifications(modifications);

            cartService.setSessionCart(savedCart);
            return restoration;
        }
        catch (final CommerceCartModificationException e) {
            throw new CommerceCartRestorationException(e.getMessage(), e);
        }
    }

    /**
     * Cleanup saved cart. Removes TMD card number, flybuys/voucher, delivery data and payment data and info from the
     * cart if exists.
     * 
     * @param savedCart
     *            the saved cart
     */
    private void cleanupSavedCart(final CartModel savedCart) {

        // removing TMD card number
        if (StringUtils.isNotBlank(savedCart.getTmdCardNumber())) {
            savedCart.setTmdCardNumber(null);
        }

        // removing vouchers/flybuys
        final Collection<String> appliedVouchers = targetVoucherService.getAppliedVoucherCodes(savedCart);
        if (CollectionUtils.isNotEmpty(appliedVouchers)) {
            for (final String voucher : appliedVouchers) {
                try {
                    targetVoucherService.releaseVoucher(voucher, savedCart);
                }
                catch (final JaloPriceFactoryException e) {
                    LOG.error(
                            "Got an exception during releasing applied voucher from persisted cart while restoring it.",
                            e);
                }
            }
        }
        if (StringUtils.isNotBlank(savedCart.getFlyBuysCode())) {
            savedCart.setFlyBuysCode(null);
        }

        // removing delivery related data
        if (null != savedCart.getDeliveryMode()) {
            savedCart.setDeliveryMode(null);
        }
        if (null != savedCart.getZoneDeliveryModeValue()) {
            savedCart.setZoneDeliveryModeValue(null);
        }
        if (null != savedCart.getDeliveryAddress()) {
            savedCart.setDeliveryAddress(null);
        }
        if (null != savedCart.getCncStoreNumber()) {
            savedCart.setCncStoreNumber(null);
        }

        // removing payment related data
        if (null != savedCart.getPaymentMode()) {
            savedCart.setPaymentMode(null);
        }
        if (null != savedCart.getPaymentInfo()) {
            savedCart.setPaymentInfo(null);
        }
        if (null != savedCart.getPaymentAddress()) {
            savedCart.setPaymentAddress(null);
        }

        getModelService().save(savedCart);
    }

    @Override
    public boolean calculateCart(final CommerceCartParameter cartParameter) {
        if (super.calculateCart(cartParameter)) {
            final CartModel cartModel = cartParameter.getCart();
            recalcDeliveryAfterPromos(cartModel);
            try {
                calculationService.calculateTotals(cartModel, true);
            }
            catch (final CalculationException calculationException) {
                throw new IllegalStateException("Cart model " + cartModel.getCode()
                        + " totals were not calculated due to: " + calculationException.getMessage());
            }

            return true;
        }
        return false;
    }

    @Override
    public void recalculateCart(final CommerceCartParameter cartParameter) {
        if (orderRequiresCalculationStrategy.requiresCalculation(cartParameter.getCart())) {
            super.recalculateCart(cartParameter);
            final CartModel cartModel = cartParameter.getCart();
            recalcDeliveryAfterPromos(cartModel);
            try {
                calculationService.calculateTotals(cartModel, true);
            }
            catch (final CalculationException calculationException) {
                throw new IllegalStateException("Cart model " + cartModel.getCode()
                        + " totals were not calculated due to: " + calculationException.getMessage(),
                        calculationException);
            }
        }
    }

    private void recalcDeliveryAfterPromos(final CartModel cartModel) {
        // delivery costs were calculated prior to deals being applied, need to recheck that now.
        final DeliveryModeModel deliveryMode = cartModel.getDeliveryMode();
        if (deliveryMode instanceof TargetZoneDeliveryModeModel) {
            try {
                final ZoneDeliveryModeValueModel newValue = targetDeliveryService
                        .getZoneDeliveryModeValueForAbstractOrderAndMode(
                                (TargetZoneDeliveryModeModel)deliveryMode, cartModel);
                cartModel.setZoneDeliveryModeValue((AbstractTargetZoneDeliveryModeValueModel)newValue);
            }
            catch (final TargetNoPostCodeException e) {
                cartModel.setZoneDeliveryModeValue(null);
            }
        }
    }

    @Override
    public boolean isCartWithinAfterpayThresholds(final BigDecimal total) {
        final AfterpayConfigModel afterpayConfig = afterpayConfigService.getAfterpayConfig();
        ServicesUtil.validateParameterNotNull(afterpayConfig, "Afterpay config cannot be null");
        return total.compareTo(afterpayConfig.getMinimumAmount()) >= 0
                && total.compareTo(afterpayConfig.getMaximumAmount()) <= 0;
    }

    @Override
    public boolean isExcludedForAfterpay(final CartModel cartModel) {
        if (null == cartModel) {
            return false;
        }
        if (BooleanUtils.isTrue(cartModel.getContainsPreOrderItems())) {
            return true;
        }
        final List<AbstractOrderEntryModel> orderEntries = cartModel.getEntries();
        if (CollectionUtils.isEmpty(orderEntries)) {
            return false;
        }

        for (final AbstractOrderEntryModel entryModel : orderEntries) {
            final ProductModel product = entryModel.getProduct();
            if (null != product && product instanceof AbstractTargetVariantProductModel) {
                final AbstractTargetVariantProductModel variantProduct = (AbstractTargetVariantProductModel)product;
                if (checkExcludeForAfterpay(variantProduct)) {
                    return true;
                }
            }
        }
        return false;
    }


    private boolean checkExcludeForAfterpay(final AbstractTargetVariantProductModel variantProduct) {
        boolean isExcluded = false;
        final TargetColourVariantProductModel colourVariant = getTargetColourVariantProductModel(variantProduct);
        if (colourVariant != null) {
            isExcluded = colourVariant.isExcludeForAfterpay();
        }
        return isExcluded;
    }


    @Override
    public boolean isExcludedForZip(final CartModel cartModel) {
        if (null == cartModel) {
            return false;
        }
        if (BooleanUtils.isTrue(cartModel.getContainsPreOrderItems())) {
            return true;
        }
        final List<AbstractOrderEntryModel> orderEntries = cartModel.getEntries();
        if (CollectionUtils.isEmpty(orderEntries)) {
            return false;
        }
        return checkOrderEntriesForZipExclusion(orderEntries);
    }

    private boolean checkOrderEntriesForZipExclusion(final List<AbstractOrderEntryModel> orderEntries) {
        for (final AbstractOrderEntryModel entryModel : orderEntries) {
            final ProductModel product = entryModel.getProduct();
            if (null != product && product instanceof AbstractTargetVariantProductModel) {
                final AbstractTargetVariantProductModel variantProduct = (AbstractTargetVariantProductModel)product;
                if (checkExcludeForZip(variantProduct)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean checkExcludeForZip(final AbstractTargetVariantProductModel variantProduct) {
        boolean isExcluded = false;
        final TargetColourVariantProductModel colourVariant = getTargetColourVariantProductModel(variantProduct);
        if (colourVariant != null) {
            isExcluded = colourVariant.isExcludeForZipPayment();
        }
        return isExcluded;
    }

    /**
     * Get the colour variants product
     *
     * @param targetVariantModel
     * @return TargetColourVariantProductModel
     */
    private TargetColourVariantProductModel getTargetColourVariantProductModel(
            final AbstractTargetVariantProductModel targetVariantModel) {
        if (targetVariantModel instanceof TargetColourVariantProductModel) {
            return (TargetColourVariantProductModel)targetVariantModel;
        }
        final ProductModel product = targetVariantModel.getBaseProduct();
        if (product != null && product instanceof AbstractTargetVariantProductModel) {
            return getTargetColourVariantProductModel((AbstractTargetVariantProductModel)product);
        }
        return null;
    }

    /**
     * Check cart level.
     * 
     * @param productModel
     *            the product model
     * @param cartModel
     *            the cart model
     * @return the long
     */
    private long existingQuantityOfEntryInCart(final ProductModel productModel, final CartModel cartModel) {
        long cartLevel = 0;
        for (final CartEntryModel entryModel : cartService.getEntriesForProduct(cartModel, productModel)) {
            cartLevel += entryModel.getQuantity().longValue();
        }
        return cartLevel;
    }

    private boolean isProductPreview(final ProductModel product) {
        if (!targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.PRODUCT_PREVIEW_MODE)) {
            return false;
        }
        return BooleanUtils.isTrue(((AbstractTargetVariantProductModel)product).getPreview());

    }

    /**
     * @param targetDeliveryService
     *            the targetDeliveryService to set
     */
    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        this.targetDeliveryService = targetDeliveryService;
    }

    /**
     * @param targetLaybyCartService
     *            the targetLaybyCartService to set
     */
    @Required
    public void setTargetLaybyCartService(final TargetLaybyCartService targetLaybyCartService) {
        this.targetLaybyCartService = targetLaybyCartService;
    }

    /**
     * @param searchRestrictionService
     *            the searchRestrictionService to set
     */
    @Required
    public void setSearchRestrictionService(final SearchRestrictionService searchRestrictionService) {
        this.searchRestrictionService = searchRestrictionService;
    }

    /**
     * @param targetVoucherService
     *            the targetVoucherService to set
     */
    @Required
    public void setTargetVoucherService(final TargetVoucherService targetVoucherService) {
        this.targetVoucherService = targetVoucherService;
    }

    /**
     * @param productService
     *            the productService to set
     */
    @Required
    public void setProductService(final ProductService productService) {
        this.productService = productService;
    }

    /**
     * @param stockService
     *            the stockService to set
     */
    @Required
    public void setStockService(final TargetStockService stockService) {
        this.stockService = stockService;
    }

    /**
     * @param cartService
     *            the cartService to set
     */
    @Required
    public void setCartService(final CartService cartService) {
        this.cartService = cartService;
    }

    /**
     * @param calculationService
     *            the calculationService to set
     */
    @Required
    public void setCalculationService(final CalculationService calculationService) {
        this.calculationService = calculationService;
    }

    /**
     * @param orderRequiresCalculationStrategy
     *            the orderRequiresCalculationStrategy to set
     */
    @Required
    public void setOrderRequiresCalculationStrategy(
            final OrderRequiresCalculationStrategy orderRequiresCalculationStrategy) {
        this.orderRequiresCalculationStrategy = orderRequiresCalculationStrategy;
    }

    /**
     * @param targetDeliveryModeHelper
     *            the targetDeliveryModeHelper to set
     */
    @Required
    public void setTargetDeliveryModeHelper(final TargetDeliveryModeHelper targetDeliveryModeHelper) {
        this.targetDeliveryModeHelper = targetDeliveryModeHelper;
    }

    /**
     * @param commerceDeliveryModeStrategy
     *            the new commerce delivery mode strategy
     */
    @Required
    public void setCommerceDeliveryModeStrategy(
            final CommerceDeliveryModeStrategy commerceDeliveryModeStrategy) {
        this.commerceDeliveryModeStrategy = commerceDeliveryModeStrategy;
    }

    /**
     * @param giftCardService
     *            the giftCardService to set
     */
    @Required
    public void setGiftCardService(final GiftCardService giftCardService) {
        this.giftCardService = giftCardService;
    }

    /**
     * @param giftRecipientFormConverter
     *            the giftRecipientFormConverter to set
     */
    @Required
    public void setGiftRecipientFormConverter(final GiftRecipientFormConverter giftRecipientFormConverter) {
        this.giftRecipientFormConverter = giftRecipientFormConverter;
    }

    /**
     * @param targetFeatureSwitchService
     *            the targetFeatureSwitchService to set
     */
    @Required
    public void setTargetFeatureSwitchService(final TargetFeatureSwitchService targetFeatureSwitchService) {
        this.targetFeatureSwitchService = targetFeatureSwitchService;
    }

    /**
     * @param afterpayConfigService
     *            the afterpayConfigService to set
     */
    @Required
    public void setAfterpayConfigService(final AfterpayConfigService afterpayConfigService) {
        this.afterpayConfigService = afterpayConfigService;
    }

    /**
     * @param targetSharedConfigService
     *            the targetSharedConfigService to set
     */
    public void setTargetSharedConfigService(final TargetSharedConfigService targetSharedConfigService) {
        this.targetSharedConfigService = targetSharedConfigService;
    }

    /**
     * @param fluentStockLookupService
     *            the fluentStockLookupService to set
     */
    public void setFluentStockLookupService(final FluentStockLookupService fluentStockLookupService) {
        this.fluentStockLookupService = fluentStockLookupService;
    }

    /**
     * @param targetPreOrderService
     *            the targetPreOrderService to set
     */
    @Required
    public void setTargetPreOrderService(final TargetPreOrderService targetPreOrderService) {
        this.targetPreOrderService = targetPreOrderService;
    }

    /**
     * @param targetSalesApplicationService
     *            the targetSalesApplicationService to set
     */
    @Required
    public void setTargetSalesApplicationService(final TargetSalesApplicationService targetSalesApplicationService) {
        this.targetSalesApplicationService = targetSalesApplicationService;
    }

}

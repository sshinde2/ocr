package au.com.target.tgtlayby.cart.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.order.impl.DefaultCartService;
import de.hybris.platform.servicelayer.session.Session;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.cart.impl.TargetCartServiceImpl;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtlayby.cart.dao.TargetCartDao;


/**
 * Implementation of {@link TargetLaybyCartService}
 * 
 */
public class TargetLaybyCartServiceImpl extends TargetCartServiceImpl implements TargetLaybyCartService {

    private TargetCartDao targetCartDao;

    private void essentialCheck(final CartModel cart, final ProductModel product) {
        validateParameterNotNullStandardMessage("product", product);
        validateParameterNotNullStandardMessage("cart", cart);
    }

    @Override
    public CartEntryModel getEntryForProduct(final CartModel cartModel, final ProductModel productModel) {
        essentialCheck(cartModel, productModel);
        final List<CartEntryModel> entries = targetCartDao.findEntriesForProduct(cartModel, productModel);

        CartEntryModel returnEntry = null;
        if (CollectionUtils.isNotEmpty(entries)) {
            // To avoid lazy loading
            entries.get(0).getProduct();
            returnEntry = entries.get(0);
        }

        return returnEntry;

    }

    @Override
    public CartModel getMostRecentCartForCustomer(final CustomerModel customer) {
        validateParameterNotNullStandardMessage("customer", customer);
        return targetCartDao.getMostRecentCartForUser(customer);
    }

    @Override
    public CartModel getCurrentSessionCartBySession(final HttpSession session) {
        final JaloSession jaloSession = (JaloSession)session.getAttribute(TgtCoreConstants.JALO_SESSION);
        if (jaloSession == null) {
            return null;
        }
        final Session hybrisSession = (Session)jaloSession.getAttribute(TgtCoreConstants.SLSESSION);
        if (hybrisSession == null) {
            return null;
        }
        return (CartModel)hybrisSession.getAttribute(DefaultCartService.SESSION_CART_PARAMETER_NAME);
    }

    /**
     * @param targetCartDao
     *            the targetCartDao to set
     */
    @Required
    public void setTargetCartDao(final TargetCartDao targetCartDao) {
        this.targetCartDao = targetCartDao;
    }

}

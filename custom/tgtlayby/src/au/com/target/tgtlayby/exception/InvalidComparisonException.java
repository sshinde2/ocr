/**
 * 
 */
package au.com.target.tgtlayby.exception;


public class InvalidComparisonException extends LaybyException {

    public InvalidComparisonException(final String message)
    {
        super(message);
    }

    public InvalidComparisonException(final Throwable cause)
    {
        super(cause);
    }

    public InvalidComparisonException(final String message, final Throwable cause)
    {
        super(message, cause);
    }
}

/**
 * 
 */
package au.com.target.tgtlayby.exception;


public class LaybyException extends Exception {

    private final String errorCode;

    public LaybyException(final String message) {
        super(message);
        this.errorCode = null;
    }

    public LaybyException(final String message, final String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public LaybyException(final Throwable cause) {
        super(cause);
        this.errorCode = null;
    }

    public LaybyException(final String message, final Throwable cause) {
        super(message, cause);
        this.errorCode = null;
    }

    public LaybyException(final String message, final String errorCode, final Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

}

package au.com.target.tgtlayby.interceptors;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import de.hybris.platform.servicelayer.session.SessionService;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtlayby.constants.TgtlaybyConstants;
import au.com.target.tgtlayby.model.PurchaseOptionModel;


/**
 * Remove interceptor to cleanup the checkout cart session attribute. Does nothing in case of master cart
 */
public class CheckoutCartRemoveInterceptor implements RemoveInterceptor {

    private SessionService sessionService;

    @Override
    public void onRemove(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof CartModel)) {
            return;
        }

        final CartModel cartModel = (CartModel)model;
        final PurchaseOptionModel purchaseOption = cartModel.getPurchaseOption();

        if (purchaseOption == null) {
            // Purchaseoption not set in the cart. So this cannot be a checkout cart
            return;
        }

        final String sessionAttribute = TgtlaybyConstants.SESSION_CHECKOUT_CART + purchaseOption.getCode();
        sessionService.removeAttribute(sessionAttribute);
    }

    /**
     * @param sessionService
     *            the sessionService to set
     */
    @Required
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

}

/**
 * 
 */
package au.com.target.tgtlayby.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtlayby.model.PaymentDueConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.purchase.PurchaseOptionConfigService;


public class PurchaseOptionConfigValidator implements ValidateInterceptor {

    private static final Date INFINITE_FUTURE = new Date(Long.MAX_VALUE);
    private static final Date INFINITE_PAST = new Date(Long.MIN_VALUE);

    private PurchaseOptionConfigService purchaseOptionConfigService;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {

        if (!(model instanceof PurchaseOptionConfigModel)) {
            throw new IllegalArgumentException("Purchase Option Config Model is expected, " + model);
        }

        final PurchaseOptionConfigModel currentPurchaseOptionConfig = (PurchaseOptionConfigModel)model;

        final double maxPercentage = currentPurchaseOptionConfig.getMaxPercentage().doubleValue();

        if (maxPercentage < 0
                || maxPercentage > 100) {
            throw new InterceptorException("Max percentage " + maxPercentage
                    + " must be between 0 and 100");
        }

        final PaymentDueConfigModel initialPaymentDue = currentPurchaseOptionConfig.getMinimumPaymentDue();
        if (initialPaymentDue != null) {
            if (maxPercentage < initialPaymentDue.getDuePercentage().doubleValue()) {
                throw new InterceptorException("Max percentage " + maxPercentage
                        + " must be greater than " + initialPaymentDue.getDuePercentage().doubleValue());
            }
        }

        Date currentFrom = currentPurchaseOptionConfig.getValidFrom();
        Date currentTo = currentPurchaseOptionConfig.getValidTo();

        if (currentFrom == null) {
            currentFrom = INFINITE_PAST;
        }

        if (currentTo == null) {
            currentTo = INFINITE_FUTURE;
        }

        if (currentTo.before(currentFrom)) {
            throw new InterceptorException("You can't save a PurchaseOptionConfig validTo: " + currentTo
                    + " before validFrom:" + currentFrom);
        }

        final PurchaseOptionModel purchaseOption = currentPurchaseOptionConfig.getPurchaseOption();

        final List<PurchaseOptionConfigModel> purchaseOptionConfigs = purchaseOptionConfigService
                .getAllPurchaseOptionConfigs(purchaseOption);

        if (CollectionUtils.isEmpty(purchaseOptionConfigs)) {
            return;
        }

        for (final PurchaseOptionConfigModel purchaseOptionConfig : purchaseOptionConfigs) {

            // Only in the modification scenario, this code will prevent self conflicts
            if (currentPurchaseOptionConfig.getCode().equals(purchaseOptionConfig.getCode())) {
                continue;
            }

            Date from = purchaseOptionConfig.getValidFrom();
            Date to = purchaseOptionConfig.getValidTo();

            if (from == null) {
                from = INFINITE_PAST;
            }

            if (to == null) {
                to = INFINITE_FUTURE;
            }

            // Normal Scenario
            if (currentFrom.before(to) && currentTo.after(from)) {
                throw new InterceptorException(
                        "You can't save another PurchaseOptionConfig " + currentPurchaseOptionConfig.getCode()
                                + " overlapping with the current one " + purchaseOptionConfig.getCode());
            }
        }

    }

    /**
     * @param purchaseOptionConfigService
     *            the purchaseOptionConfigService to set
     */
    @Required
    public void setPurchaseOptionConfigService(final PurchaseOptionConfigService purchaseOptionConfigService) {
        this.purchaseOptionConfigService = purchaseOptionConfigService;
    }



}

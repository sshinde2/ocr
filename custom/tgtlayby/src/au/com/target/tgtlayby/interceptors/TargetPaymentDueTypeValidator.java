/**
 * 
 */
package au.com.target.tgtlayby.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.Date;

import au.com.target.tgtlayby.model.PaymentDueConfigModel;


public class TargetPaymentDueTypeValidator implements ValidateInterceptor {
    /**
     * validates that both due date and due days are not set to for in one payment config
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof PaymentDueConfigModel) {
            final PaymentDueConfigModel dueConfig = (PaymentDueConfigModel)model;
            final Date dueDate = dueConfig.getDueDate();
            final double dueAmount = dueConfig.getDuePercentage().doubleValue();
            final Integer dueDaysObj = dueConfig.getDueDays();

            final int dueDays = dueDaysObj == null ? 0 : dueDaysObj.intValue();
            if (dueDate != null && dueDays != 0) {
                throw new InterceptorException("Both Due Date and Due Days cannot be set in one payment config");
            }
            if (dueDays < 0) {
                throw new InterceptorException("Due days cannot be less than 0");
            }
            if (dueAmount < 0) {
                throw new InterceptorException("Due percentage cannot be less than 0");
            }
        }

    }
}

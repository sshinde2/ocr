/**
 * 
 */
package au.com.target.tgtlayby.jalo;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.order.Cart;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.order.impl.TargetFindFeeTaxValueStrategy;
import au.com.target.tgtlayby.order.impl.TargetFindLaybyFeeStrategy;


// This class needs to extend the Jalo Cart class for the purpose of updating the cart cost when layby fee 
// and a promotion is used as promotions operate on the Jalo layer. 
@SuppressWarnings("deprecation")
public class TargetCart extends Cart {

    private static final Logger LOG = Logger.getLogger(TargetCart.class);

    private TargetFindFeeTaxValueStrategy targetFindFeeTaxValueStrategy;
    private TargetFindLaybyFeeStrategy targetFindLaybyFeeStrategy;
    private ModelService modelService;

    /* 
     * Override the default to set the layby fee in the order
     */
    @Override
    protected void resetAdditionalCosts(final Collection<TaxValue> relativeTaxValues) throws JaloPriceFactoryException
    {
        super.resetAdditionalCosts(relativeTaxValues);

        final AbstractOrderModel o = modelService.toModelLayer(this);
        final Double laybyFee = targetFindLaybyFeeStrategy.getLaybyFee(o);
        try {
            setAttribute("layByFee", laybyFee);
        }
        catch (final JaloInvalidParameterException e) {
            throw new IllegalArgumentException("Bad jalo param", e);
        }
        catch (final JaloSecurityException e) {
            throw new SecurityException("Invalid security access", e);
        }
        catch (final JaloBusinessException e) {
            LOG.error("Attribute layByFee cannot be set", e);
        }
    }

    /* 
     * Add fee tax values to the map for use in calculateTotals
     */
    protected void addFeeTaxValues(final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap)
            throws JaloPriceFactoryException
    {
        if (getDeliveryCost() != null)
        {
            addFeeTaxValue(getDeliveryCost().doubleValue(), taxValueMap);
        }
        try {
            if (getAttribute("layByFee") != null)
            {
                final Double d = (Double)getAttribute("layByFee");
                addFeeTaxValue(d.doubleValue(), taxValueMap);
            }
        }
        catch (final JaloInvalidParameterException e) {
            throw new IllegalArgumentException("Bad jalo param", e);
        }
        catch (final JaloSecurityException e) {
            throw new SecurityException("Invalid security access", e);
        }

        final List<DiscountValue> globalDiscountValues = getGlobalDiscountValues();
        if (CollectionUtils.isNotEmpty(globalDiscountValues)) {
            for (final DiscountValue discount : globalDiscountValues) {
                addFeeTaxValue(-discount.getAppliedValue(), taxValueMap); // Negate the value because the applied value is positive, but it's actually a discount
            }
        }
    }

    protected void addFeeTaxValue(final double fee, final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap)
    {
        // Get the right tax to use for fees
        final TaxValue defaultTaxValue = targetFindFeeTaxValueStrategy.getTaxValueForFee();
        defaultTaxValue.unapply();

        // We need to create a key out of a set containing the TaxValue
        final Set<TaxValue> relativeTaxGroupKey = Collections.singleton(defaultTaxValue);

        addRelativeEntryTaxValue(fee, defaultTaxValue, relativeTaxGroupKey, taxValueMap);
    }

    /* 
     * Override the default to add layby fee to total and set taxAdjustmentFactor=1
     */
    @Override
    protected void calculateTotals(final boolean recalculate,
            final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap) throws JaloPriceFactoryException
    {
        if (recalculate || BooleanUtils.isFalse(isCalculated()))
        {
            addFeeTaxValues(taxValueMap);

            final Currency curr = getCurrency();
            final int digits = curr.getDigits().intValue();

            // subtotal
            final double subtotal = getSubtotal().doubleValue();

            // discounts            
            final double totalDiscounts = curr.round(calculateDiscountValues(recalculate));

            setTotalDiscounts(Double.valueOf(totalDiscounts));

            // set total            
            double total = curr.round(subtotal + getPaymentCosts() + getDeliveryCosts() - totalDiscounts);

            // Add layby fee
            try {
                if (getAttribute("layByFee") != null)
                {
                    final Double d = (Double)getAttribute("layByFee");
                    total += d.doubleValue();
                }
            }
            catch (final JaloInvalidParameterException e) {
                throw new IllegalArgumentException("Bad jalo param", e);
            }
            catch (final JaloSecurityException e) {
                throw new SecurityException("Invalid security access", e);
            }

            setTotal(total);

            // taxes             
            final double totalTaxes = curr.round(calculateTotalTaxValues(recalculate, digits, 1, taxValueMap));

            setTotalTax(totalTaxes);

            setCalculated(true);
        }
    }

    @Override
    protected Map<TaxValue, Map<Set<TaxValue>, Double>> calculateSubtotal(final boolean recalculate) {
        final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap = super.calculateSubtotal(recalculate);
        return taxValueMap.isEmpty() ? new LinkedHashMap() : taxValueMap;
    }

    /**
     * @param targetFindFeeTaxValueStrategy
     *            the targetFindFeeTaxValueStrategy to set
     */
    @Required
    public void setTargetFindFeeTaxValueStrategy(final TargetFindFeeTaxValueStrategy targetFindFeeTaxValueStrategy) {
        this.targetFindFeeTaxValueStrategy = targetFindFeeTaxValueStrategy;
    }

    /**
     * @param targetFindLaybyFeeStrategy
     *            the targetFindLaybyFeeStrategy to set
     */
    @Required
    public void setTargetFindLaybyFeeStrategy(final TargetFindLaybyFeeStrategy targetFindLaybyFeeStrategy) {
        this.targetFindLaybyFeeStrategy = targetFindLaybyFeeStrategy;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }
}

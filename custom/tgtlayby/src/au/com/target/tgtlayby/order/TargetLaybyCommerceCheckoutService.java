/**
 * 
 */
package au.com.target.tgtlayby.order;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.order.TargetCommerceCheckoutService;
import au.com.target.tgtlayby.model.PurchaseOptionModel;


/**
 * 
 * TargetLaybyCommerceCheckoutService
 * 
 * See {link @CommerceCheckoutService} for more details
 * 
 */
public interface TargetLaybyCommerceCheckoutService extends TargetCommerceCheckoutService {
    /**
     * 
     * @param purchaseOption
     * @return Boolean
     */
    boolean hasCheckoutCart(PurchaseOptionModel purchaseOption);


    /**
     * perform SOH check on cart
     * 
     * @param cart
     * @return List<CommerceCartModification>
     * @throws CommerceCartModificationException
     */
    List<CommerceCartModification> performSOHOnCart(final CartModel cart)
            throws CommerceCartModificationException;

    /**
     * Return all checkout carts
     * 
     * @param masterCart
     * @return all checkout carts
     */
    Map<String, CartModel> getCheckoutCarts(CartModel masterCart);

    /**
     * Update the customer in all checkout carts
     * 
     * @param masterCart
     * @param customer
     */
    void changeCurrentCustomerOnAllCheckoutCarts(CartModel masterCart, UserModel customer);

    /**
     * Set the amount of the current payment to the cart
     * 
     * @param cartModel
     *            The cart to store the value against
     * @param amountCurrentPayment
     *            The amount of the current payment
     * @return true if the value was set successfully
     */
    boolean setAmountCurrentPayment(CartModel cartModel, Double amountCurrentPayment);

}

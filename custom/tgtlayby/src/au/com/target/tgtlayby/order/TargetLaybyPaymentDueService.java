/**
 * 
 */
package au.com.target.tgtlayby.order;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.Set;

import au.com.target.tgtlayby.data.PaymentDueData;
import au.com.target.tgtlayby.model.PaymentDueConfigModel;


public interface TargetLaybyPaymentDueService {

    /**
     * given a order/cart get all the payment dues
     * 
     * @param abstractOrder
     * @return List<PaymentDueData>
     */
    Set<PaymentDueData> getAllPaymentDuesForAbstractOrder(AbstractOrderModel abstractOrder);

    /**
     * given a order/cart and payment due config construct a payment due data object
     * 
     * @param abstractOrder
     * @param paymentDueConfig
     * @return PaymentDueData
     */
    PaymentDueData getPaymentDueDataForAbstractOrderAndPaymentDueConfig(AbstractOrderModel abstractOrder,
            PaymentDueConfigModel paymentDueConfig);

    /**
     * get the initial payment due for a order/cart
     * 
     * @param abstractOrder
     * @return PaymentDueData
     */
    PaymentDueData getInitialPaymentDue(AbstractOrderModel abstractOrder);

    /**
     * get the intermediate payment due for a order/cart
     * 
     * @param abstractOrder
     * @return PaymentDueData
     */
    Set<PaymentDueData> getIntermediatePaymentDues(AbstractOrderModel abstractOrder);

    /**
     * get the final payment due for a order/cart
     * 
     * @param abstractOrder
     * @return PaymentDueData
     */
    PaymentDueData getFinalPaymentDue(AbstractOrderModel abstractOrder);

    /**
     * get the next payment that is due for a order
     * 
     * @param order
     * @return PaymentDueData
     */
    PaymentDueData getNextPaymentDue(OrderModel order);
}

/**
 * 
 */
package au.com.target.tgtlayby.order.dao;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;

import au.com.target.tgtcore.order.dao.TargetOrderDao;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;


public interface TargetLaybyOrderDao extends TargetOrderDao {


    /**
     * Get all orders by Customer and PurchaseOptionConfigs sorted by order creation time
     * 
     * @param customer
     * @param purchaseOptionConfigs
     * @return List<OrderModel>
     */
    List<OrderModel> getAllOrdersByCustomerAndPurchaseOptionConfigs(CustomerModel customer,
            List<PurchaseOptionConfigModel> purchaseOptionConfigs);

    /**
     * Get all orders by Customer and PurchaseOptions sorted by order creation time
     * 
     * @param customer
     * @param purchaseOptionCodes
     * @return List<OrderModel>
     */
    List<OrderModel> getAllOrdersByCustomerAndPurchaseOption(CustomerModel customer,
            List<String> purchaseOptionCodes);

}

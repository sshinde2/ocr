/**
 * 
 */
package au.com.target.tgtlayby.order.dao.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.order.dao.impl.TargetOrderDaoImpl;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.order.dao.TargetLaybyOrderDao;


public class TargetLaybyOrderDaoImpl extends TargetOrderDaoImpl implements TargetLaybyOrderDao {

    private static final String QUERY_FIND_ORDERS_BY_CUSTOMER_PURCHASEOPTIONCONFIGS =
            "SELECT " + OrderModel.PK + " FROM { " + OrderModel._TYPECODE + " AS c} WHERE {c:"
                    + OrderModel.PURCHASEOPTIONCONFIG + " } in (?purchaseOptionConfig) " +
                    "AND {c:" + OrderModel.USER + " }=?user order by {" + OrderModel.DATE + "} desc";

    private static final String QUERY_FIND_ORDERS_BY_CUSTOMER_PURCHASEOPTIONS =
            "SELECT {o." + OrderModel.PK + "} FROM { " + OrderModel._TYPECODE + " AS o join "
                    + PurchaseOptionConfigModel._TYPECODE + " as poc on {o." + OrderModel.PURCHASEOPTIONCONFIG
                    + "} = { poc." + PurchaseOptionConfigModel.PK + "} join "
                    + PurchaseOptionModel._TYPECODE
                    + " as po on {poc." + PurchaseOptionConfigModel.PURCHASEOPTION + "} = {po."
                    + PurchaseOptionModel.PK + "}} where {po.code} in (?purchaseOptionCode) order by {o."
                    + OrderModel.DATE + "} desc";


    @Override
    public List<OrderModel> getAllOrdersByCustomerAndPurchaseOptionConfigs(final CustomerModel customer,
            final List<PurchaseOptionConfigModel> purchaseOptionConfigs) {

        final Map<String, Object> params = new HashMap<String, Object>();

        params.put("user", customer);
        params.put("purchaseOptionConfig", purchaseOptionConfigs);

        final SearchResult<OrderModel> orders = getFlexibleSearchService().search(
                QUERY_FIND_ORDERS_BY_CUSTOMER_PURCHASEOPTIONCONFIGS, params);

        return orders == null ? Collections.EMPTY_LIST : orders.getResult();
    }

    @Override
    public List<OrderModel> getAllOrdersByCustomerAndPurchaseOption(final CustomerModel customer,
            final List<String> purchaseOptionCodes) {

        final Map<String, Object> params = new HashMap<String, Object>();

        params.put("user", customer);
        params.put("purchaseOptionCode", purchaseOptionCodes);

        final SearchResult<OrderModel> orders = getFlexibleSearchService().search(
                QUERY_FIND_ORDERS_BY_CUSTOMER_PURCHASEOPTIONS, params);

        return orders == null ? Collections.EMPTY_LIST : orders.getResult();
    }

}

/**
 * 
 */
package au.com.target.tgtlayby.order.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;


/**
 * Find the layby fee for the given order
 * 
 */
public class TargetFindLaybyFeeStrategy {
    private static final Logger LOG = Logger.getLogger(TargetFindLaybyFeeStrategy.class);

    /**
     * Look up the layby fee for the given order from the associated purchase option config
     * 
     * @param order
     * @return Double fee
     */
    public Double getLaybyFee(final AbstractOrderModel order) {
        Assert.notNull(order);

        final PurchaseOptionConfigModel purchaseOptionConfigModel = order.getPurchaseOptionConfig();

        // Find the fee for the selected option
        Double fee = null;

        if (purchaseOptionConfigModel != null)
        {
            fee = purchaseOptionConfigModel.getServiceFee();
        }
        else
        {
            LOG.error("Could not find purchase option config for given order "
                    + order.getCode());
        }

        return fee;
    }
}

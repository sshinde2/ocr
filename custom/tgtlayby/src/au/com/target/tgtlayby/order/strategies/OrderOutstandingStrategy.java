/**
 * 
 */
package au.com.target.tgtlayby.order.strategies;

import de.hybris.platform.core.model.order.OrderModel;


/**
 * @author dcwillia
 * 
 */
public interface OrderOutstandingStrategy {
    /**
     * Determines the outstanding amount to be paid on the (Layby)Order.
     * 
     * @param orderModel
     *            order to find outstanding amount
     * @returns amount outstanding, zero for non-layby(eg.Buynow) cancelled/completed layby orders
     */
    double calculateAmt(OrderModel orderModel);
}

/**
 * 
 */
package au.com.target.tgtlayby.order.strategies.impl;

import de.hybris.platform.core.model.order.OrderModel;

import org.springframework.util.Assert;

import au.com.target.tgtcore.order.strategies.ReplaceOrderDenialStrategy;


/**
 * A Denial Strategy using purchase option config's AllowPaymentDues() value to decide whether an order replacement is
 * possible
 * 
 */
public class AllowPaymentDueReplaceOrderDenialStrategy implements ReplaceOrderDenialStrategy {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDenied(final OrderModel order) {
        Assert.notNull(order);
        Assert.notNull(order.getPurchaseOptionConfig());
        return (order.getPurchaseOptionConfig().getAllowPaymentDues().booleanValue());
    }

}
package au.com.target.tgtlayby.order.strategies.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtlayby.order.strategies.OrderOutstandingStrategy;


/**
 * Determines the outstanding amount to be paid on the (Layby)Order. Uses {@link FindOrderTotalPaymentMadeStrategy} to
 * determine the amount paid so far.
 * 
 * Returns nothing for non-layby(eg.Buynow) orders and zero for cancelled/completed layby orders.
 */
public class OrderOutstandingStrategyImpl implements OrderOutstandingStrategy {

    private FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    private CommonI18NService commonI18NService;

    @Override
    public double calculateAmt(final OrderModel orderModel) {
        Assert.notNull(orderModel);

        // non-layby orders
        if (orderModel.getPurchaseOptionConfig() == null
                || BooleanUtils.isFalse(orderModel.getPurchaseOptionConfig().getAllowPaymentDues())) {
            return 0;
        }

        if (OrderStatus.CANCELLED.equals(orderModel.getStatus())
                || OrderStatus.COMPLETED.equals(orderModel.getStatus())) {
            return 0;
        }

        final double amountPaidToDate = findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(orderModel).doubleValue();
        final double orderTotal = orderModel.getTotalPrice().doubleValue();
        final int digitsToRound = orderModel.getCurrency().getDigits().intValue();
        final double difference = orderTotal - amountPaidToDate;

        if (difference < 0) {
            return 0;
        }

        return commonI18NService.roundCurrency(difference, digitsToRound);
    }

    /**
     * @param findOrderTotalPaymentMadeStrategy
     *            the findOrderTotalPaymentMadeStrategy to set
     */
    @Required
    public void setFindOrderTotalPaymentMadeStrategy(
            final FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy) {
        this.findOrderTotalPaymentMadeStrategy = findOrderTotalPaymentMadeStrategy;
    }

    /**
     * @param commonI18NService
     *            the commonI18NService to set
     */
    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }


}

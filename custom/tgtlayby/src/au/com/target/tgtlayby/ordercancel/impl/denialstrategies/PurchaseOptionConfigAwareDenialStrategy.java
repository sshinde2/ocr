package au.com.target.tgtlayby.ordercancel.impl.denialstrategies;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.ordercancel.OrderCancelDenialReason;
import de.hybris.platform.ordercancel.OrderCancelDenialStrategy;
import de.hybris.platform.ordercancel.impl.denialstrategies.AbstractCancelDenialStrategy;
import de.hybris.platform.ordercancel.model.OrderCancelConfigModel;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.util.Assert;

import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;


/**
 * An Implementation of {@link OrderCancelDenialStrategy} which determines if cancel is possible on the order by
 * inspecting the associated {@link PurchaseOptionConfigModel}
 * 
 */
public class PurchaseOptionConfigAwareDenialStrategy extends AbstractCancelDenialStrategy implements
        OrderCancelDenialStrategy {

    @Override
    public OrderCancelDenialReason getCancelDenialReason(final OrderCancelConfigModel configuration,
            final OrderModel order, final PrincipalModel requestor, final boolean partialCancel,
            final boolean partialEntryCancel) {

        Assert.notNull(order, "OrderModel cannot be null");
        final PurchaseOptionConfigModel purchaseOptionConfig = order.getPurchaseOptionConfig();
        Assert.notNull(order.getPurchaseOptionConfig(), "No PurchaseOptionConfig found on the order");

        if (BooleanUtils.isTrue(purchaseOptionConfig.getAllowOrderCancellation())) {
            return null;
        }

        return getReason();
    }
}

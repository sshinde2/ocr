/**
 * 
 */
package au.com.target.tgtlayby.payment;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtpayment.dto.AmountType;
import au.com.target.tgtpayment.dto.LineItem;
import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.service.TargetPaymentAbstractOrderConverter;


/**
 * Converter is used for getting populating the Order for paypal set express checkout this order is used to populate the
 * order summary at paypals checkout page
 * 
 */
public class TargetLaybyHostedSessionTokenAbstractOrderConverterImpl implements TargetPaymentAbstractOrderConverter {

    private String adminFeeDisplayName;
    private String amountPaidTodayDisplayName;
    private TargetPaymentAbstractOrderConverter targetPaymentAbstractOrderConverter;

    @Override
    public Order convertAbstractOrderModelToOrder(final AbstractOrderModel orderModel, final BigDecimal amount,
            final PaymentCaptureType paymentCaptureType) {
        Assert.notNull(orderModel, "order cannot be null");
        Assert.notNull(amount, "amount cannot be null");

        final Currency currency = Currency.getInstance(orderModel.getCurrency().getIsocode());

        //if the capture is a adhoc payment then just create a Line Item with the amount to be captured,
        //set the subtotal and total to match this capture amount and set shipping cost to zero
        if (PaymentCaptureType.LAYBYPAYMENT.equals(paymentCaptureType)) {
            return createLaybyPaymentLineItemAndOrder(amount, currency);
        }

        // If not a layby payment then call parent impl
        final Order order = targetPaymentAbstractOrderConverter.convertAbstractOrderModelToOrder(orderModel,
                amount, paymentCaptureType);

        final PurchaseOptionConfigModel poc = orderModel.getPurchaseOptionConfig();
        Assert.notNull(poc, "purchase option config cannot be null");

        // if its a layby order then create amount due today and layby fee line items otherwise return buynow order
        if (BooleanUtils.isTrue(poc.getAllowPaymentDues())) {

            final List<LineItem> lineItems = order.getLineItems();

            //create line item for layby fee
            final LineItem laybyFee = createLineItem(1, adminFeeDisplayName,
                    BigDecimal.valueOf(orderModel.getLayByFee().doubleValue()), currency);
            lineItems.add(laybyFee);

            //create line item for amount due today
            // We set the amount due today to zero as in paypal we dont use the 
            // amount, we just display the amount pay today in the line item description
            final LineItem amountDueToday = createLineItem(1,
                    amountPaidTodayDisplayName + " - $" + amount.setScale(2, RoundingMode.HALF_UP), BigDecimal.ZERO,
                    currency);
            lineItems.add(amountDueToday);

            //update the subtotal and set the new items and subtotal back in the order
            BigDecimal subtotal = order.getItemSubTotalAmount().getAmount();
            subtotal = subtotal.add(laybyFee.getPrice().getAmount());
            final AmountType itemSubTotalAmount = new AmountType(subtotal,
                    currency);
            order.setItemSubTotalAmount(itemSubTotalAmount);
            order.setLineItems(lineItems);
            return order;
        }
        return order;
    }

    private LineItem createLineItem(final long quantity, final String name, final BigDecimal amount,
            final Currency currency) {
        final LineItem lineItem = new LineItem();
        lineItem.setQuantity(quantity);
        lineItem.setName(name);
        final AmountType amountType = new AmountType(amount, currency);
        lineItem.setPrice(amountType);
        return lineItem;
    }

    private Order createLaybyPaymentLineItemAndOrder(final BigDecimal amount, final Currency currency) {
        final Order order = new Order();
        //create line item for amount due today
        final LineItem laybyPayment = createLineItem(1,
                amountPaidTodayDisplayName + " - $" + amount.setScale(2, RoundingMode.HALF_UP), amount, currency);
        order.setItemSubTotalAmount(laybyPayment.getPrice());
        order.setOrderTotalAmount(laybyPayment.getPrice());
        order.setShippingTotalAmount(new AmountType(BigDecimal.ZERO, currency));
        final List<LineItem> lineItems = new ArrayList<>();
        lineItems.add(laybyPayment);
        order.setLineItems(lineItems);
        return order;
    }

    /**
     * @param adminFeeDisplayName
     *            the adminFeeDisplayName to set
     */
    @Required
    public void setAdminFeeDisplayName(final String adminFeeDisplayName) {
        this.adminFeeDisplayName = adminFeeDisplayName;
    }

    /**
     * @param amountPaidTodayDisplayName
     *            the amountPaidTodayDisplayName to set
     */
    @Required
    public void setAmountPaidTodayDisplayName(final String amountPaidTodayDisplayName) {
        this.amountPaidTodayDisplayName = amountPaidTodayDisplayName;
    }

    /**
     * @param targetPaymentAbstractOrderConverter
     *            the targetPaymentAbstractOrderConverter to set
     */
    @Required
    public void setTargetPaymentAbstractOrderConverter(
            final TargetPaymentAbstractOrderConverter targetPaymentAbstractOrderConverter) {
        this.targetPaymentAbstractOrderConverter = targetPaymentAbstractOrderConverter;
    }

}

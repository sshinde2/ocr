/**
 * 
 */
package au.com.target.tgtlayby.payment;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtpayment.dto.AmountType;
import au.com.target.tgtpayment.dto.LineItem;
import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.service.TargetPaymentAbstractOrderConverter;


/**
 * Converter for populating the order that is used by paypal in do express checkout order contains details for capture
 * 
 */
public class TargetLaybyPaymentCaptureAbstractOrderConverterImpl implements TargetPaymentAbstractOrderConverter {

    private String amountPaidTodayDisplayName;
    private TargetPaymentAbstractOrderConverter targetPaymentAbstractOrderConverter;

    @Override
    public Order convertAbstractOrderModelToOrder(final AbstractOrderModel orderModel, final BigDecimal amount,
            final PaymentCaptureType paymentCaptureType) {
        Assert.notNull(orderModel, "order cannot be null");
        Assert.notNull(amount, "amount cannot be null");

        final Currency currency = Currency.getInstance(orderModel.getCurrency().getIsocode());
        final PurchaseOptionConfigModel poc = orderModel.getPurchaseOptionConfig();
        Assert.notNull(poc);

        // if layby payment or layby order then we just populate the amount paid today        
        if (PaymentCaptureType.LAYBYPAYMENT.equals(paymentCaptureType)
                || BooleanUtils.isTrue(poc.getAllowPaymentDues())) {
            return createLaybyLineItemAndOrder(amount, currency);
        }

        // otherwise call the parent and return the full order
        return targetPaymentAbstractOrderConverter.convertAbstractOrderModelToOrder(orderModel,
                amount, paymentCaptureType);
    }

    private Order createLaybyLineItemAndOrder(final BigDecimal amount, final Currency currency) {
        final Order order = new Order();
        //create line item for amount capture
        final LineItem amountCaptureItem = new LineItem();
        amountCaptureItem.setQuantity(1);
        amountCaptureItem.setName(amountPaidTodayDisplayName + " - $" + amount.setScale(2, RoundingMode.HALF_UP));

        // set the item total, order total to the amount due today so totals match. Set shipping to zero so we dont include shipping 
        // in the capture amount
        final AmountType amountDueToday = new AmountType(amount.setScale(2, RoundingMode.HALF_UP), currency);
        amountCaptureItem.setPrice(amountDueToday);
        order.setItemSubTotalAmount(amountDueToday);
        order.setOrderTotalAmount(amountDueToday);
        order.setShippingTotalAmount(new AmountType(BigDecimal.ZERO, currency));
        final List<LineItem> lineItems = new ArrayList<>();
        lineItems.add(amountCaptureItem);
        order.setLineItems(lineItems);
        return order;
    }

    /**
     * @param amountPaidTodayDisplayName
     *            the amountPaidTodayDisplayName to set
     */
    @Required
    public void setAmountPaidTodayDisplayName(final String amountPaidTodayDisplayName) {
        this.amountPaidTodayDisplayName = amountPaidTodayDisplayName;
    }

    /**
     * @param targetPaymentAbstractOrderConverter
     *            the targetPaymentAbstractOrderConverter to set
     */
    @Required
    public void setTargetPaymentAbstractOrderConverter(
            final TargetPaymentAbstractOrderConverter targetPaymentAbstractOrderConverter) {
        this.targetPaymentAbstractOrderConverter = targetPaymentAbstractOrderConverter;
    }
}

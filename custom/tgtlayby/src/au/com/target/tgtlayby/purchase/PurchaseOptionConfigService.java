/**
 * 
 */
package au.com.target.tgtlayby.purchase;

import java.util.Date;
import java.util.List;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;


/**
 * 
 * PurchaseOptionConfigService
 * 
 */
public interface PurchaseOptionConfigService {

    /**
     * Get Purchase Option Config by code
     * 
     * @param code
     * @return PurchaseOptionConfigModel
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    PurchaseOptionConfigModel getPurchaseOptionConfigByCode(String code) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException;

    /**
     * Get the current Purchase Option Config
     * 
     * @return PurchaseOptionConfigModel
     */
    PurchaseOptionConfigModel getCurrentPurchaseOptionConfig();

    /**
     * Get Purchase Option Config by purchaseOptionModel
     * 
     * @param purchaseOptionModel
     * @return PurchaseOptionConfigModel
     * @throws TargetUnknownIdentifierException
     */
    PurchaseOptionConfigModel getActivePurchaseOptionConfigByPurchaseOption(PurchaseOptionModel purchaseOptionModel)
            throws TargetUnknownIdentifierException;

    /**
     * get the active purchase option config at the time a order was placed
     * 
     * @param purchaseOptionModel
     * @param orderDate
     * @return PurchaseOptionConfigModel
     * @throws TargetUnknownIdentifierException
     */
    PurchaseOptionConfigModel getPurchaseOptionConfigForOrderPlacedDate(PurchaseOptionModel purchaseOptionModel,
            Date orderDate) throws TargetUnknownIdentifierException;

    /**
     * Get all the Purchase Option Config Models
     * 
     * @param purchaseOption
     * @return {@link List}
     */
    List<PurchaseOptionConfigModel> getAllPurchaseOptionConfigs(PurchaseOptionModel purchaseOption);
}

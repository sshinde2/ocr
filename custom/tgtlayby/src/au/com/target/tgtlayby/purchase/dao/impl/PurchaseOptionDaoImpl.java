/**
 * 
 */
package au.com.target.tgtlayby.purchase.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.util.TargetServicesUtil;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.purchase.dao.PurchaseOptionDao;


public class PurchaseOptionDaoImpl extends DefaultGenericDao<PurchaseOptionModel> implements PurchaseOptionDao {

    public PurchaseOptionDaoImpl() {
        super(PurchaseOptionModel._TYPECODE);
    }

    @Override
    public PurchaseOptionModel getPurchaseOptionForCode(final String code) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        ServicesUtil.validateParameterNotNullStandardMessage("code", code);
        final Map<String, String> params = new HashMap<String, String>();
        params.put(PurchaseOptionModel.CODE, code);
        final List<PurchaseOptionModel> purchaseOptions = find(params);

        TargetServicesUtil.validateIfSingleResult(purchaseOptions, PurchaseOptionModel.class, PurchaseOptionModel.CODE,
                code);
        return purchaseOptions.get(0);

    }

    @Override
    public PurchaseOptionModel getPurchaseOptionForName(final String name) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        ServicesUtil.validateParameterNotNullStandardMessage("name", name);
        final Map<String, String> params = new HashMap<String, String>();
        params.put(PurchaseOptionModel.NAME, name);
        final List<PurchaseOptionModel> purchaseOptions = find(params);

        TargetServicesUtil.validateIfSingleResult(purchaseOptions, PurchaseOptionModel.class, PurchaseOptionModel.NAME,
                name);
        return purchaseOptions.get(0);
    }


    @Override
    public List<PurchaseOptionModel> getAllPurchaseOptions() {
        return find();

    }

}

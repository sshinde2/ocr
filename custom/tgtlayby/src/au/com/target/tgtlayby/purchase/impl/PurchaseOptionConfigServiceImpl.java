/**
 * 
 */
package au.com.target.tgtlayby.purchase.impl;

import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.purchase.PurchaseOptionConfigService;
import au.com.target.tgtlayby.purchase.PurchaseOptionService;
import au.com.target.tgtlayby.purchase.dao.PurchaseOptionConfigDao;


public class PurchaseOptionConfigServiceImpl extends AbstractBusinessService implements PurchaseOptionConfigService {
    private static final Logger LOG = Logger.getLogger(PurchaseOptionConfigServiceImpl.class);
    private PurchaseOptionConfigDao purchaseOptionConfigDao;
    private PurchaseOptionService purchaseOptionService;


    /* (non-Javadoc)
     * @see au.com.target.tgtlayby.purchase.PurchaseOptionConfigService#getPurchaseOptionConfigByCode(java.lang.String)
     */
    @Override
    public PurchaseOptionConfigModel getPurchaseOptionConfigByCode(final String code)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        ServicesUtil.validateParameterNotNullStandardMessage("Option Config Code", code);
        return purchaseOptionConfigDao.getPurchaseOptionConfigByCode(code);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtlayby.purchase.PurchaseOptionConfigService#getCurrentPurchaseOptionConfig()
     */
    @Override
    public PurchaseOptionConfigModel getCurrentPurchaseOptionConfig() {
        final PurchaseOptionModel purchOption = purchaseOptionService.getCurrentPurchaseOption();
        try {
            if (null != purchOption) {
                return getActivePurchaseOptionConfigByPurchaseOption(purchOption);
            }
            return null;
        }
        catch (final TargetUnknownIdentifierException e) {
            LOG.error("There is no purchase option with " + purchOption.getName(), e);
            return null;
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtlayby.purchase.PurchaseOptionConfigService#getActivePurchaseOptionConfigByPurchaseOption(au.com.target.tgtlayby.model.PurchaseOptionModel)
     */
    @Override
    public PurchaseOptionConfigModel getActivePurchaseOptionConfigByPurchaseOption(
            final PurchaseOptionModel purchaseOption)
            throws TargetUnknownIdentifierException {
        Assert.notNull(purchaseOption, "PurchaseOption can't be empty!");
        return purchaseOptionConfigDao.getActivePurchaseOptionConfigByPurchaseOption(purchaseOption);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtlayby.purchase.PurchaseOptionConfigService#getPurchaseOptionConfigForOrderPlacedDate(au.com.target.tgtlayby.model.PurchaseOptionModel, java.util.Date)
     */
    @Override
    public PurchaseOptionConfigModel getPurchaseOptionConfigForOrderPlacedDate(
            final PurchaseOptionModel purchaseOption,
            final Date orderDate) throws TargetUnknownIdentifierException {
        Assert.notNull(purchaseOption, "PurchaseOption can't be empty!");
        Assert.notNull(orderDate, "Order date can't be empty!");
        return purchaseOptionConfigDao.getPurchaseOptionConfigForOrderPlacedDate(purchaseOption, orderDate);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtlayby.purchase.PurchaseOptionConfigService#getAllPurchaseOptionConfigs(au.com.target.tgtlayby.model.PurchaseOptionModel)
     */
    @Override
    public List<PurchaseOptionConfigModel> getAllPurchaseOptionConfigs(final PurchaseOptionModel purchaseOption) {
        Assert.notNull(purchaseOption, "PurchaseOption can't be empty!");
        return purchaseOptionConfigDao.getAllPurchaseOptionConfigs(purchaseOption);
    }

    /**
     * @return the purchaseOptionConfigDao
     */
    public PurchaseOptionConfigDao getPurchaseOptionConfigDao() {
        return purchaseOptionConfigDao;
    }

    /**
     * @param purchaseOptionConfigDao
     *            the purchaseOptionConfigDao to set
     */
    @Required
    public void setPurchaseOptionConfigDao(final PurchaseOptionConfigDao purchaseOptionConfigDao) {
        this.purchaseOptionConfigDao = purchaseOptionConfigDao;
    }

    /**
     * @param purchaseOptionService
     *            the purchaseOptionService to set
     */
    @Required
    public void setPurchaseOptionService(final PurchaseOptionService purchaseOptionService) {
        this.purchaseOptionService = purchaseOptionService;
    }
}

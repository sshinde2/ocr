/**
 * 
 */
package au.com.target.tgtlayby.purchase.impl;

import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.purchase.PurchaseOptionService;
import au.com.target.tgtlayby.purchase.dao.PurchaseOptionDao;


public class PurchaseOptionServiceImpl extends AbstractBusinessService implements PurchaseOptionService {
    private static final Logger LOG = Logger.getLogger(PurchaseOptionServiceImpl.class);
    private PurchaseOptionDao purchaseOptionDao;
    private String currentPurchaseOptionCode;

    @Override
    public PurchaseOptionModel getPurchaseOptionForCode(final String code) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        return purchaseOptionDao.getPurchaseOptionForCode(code);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtlayby.purchase.PurchaseOptionService#getPurchaseOptionForName(java.lang.String)
     */
    @Override
    public PurchaseOptionModel getPurchaseOptionForName(final String name) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        return purchaseOptionDao.getPurchaseOptionForName(name);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtlayby.purchase.PurchaseOptionService#getCurrentPurchaseOption()
     */
    @Override
    //TODO unittest
    public PurchaseOptionModel getCurrentPurchaseOption() {
        try {
            return getPurchaseOptionForCode(currentPurchaseOptionCode);
        }
        catch (final TargetUnknownIdentifierException e) {
            LOG.error("There is no purchase option with " + currentPurchaseOptionCode, e);
            return null;
        }
        catch (final TargetAmbiguousIdentifierException e) {
            LOG.error("more than one purchase option with " + currentPurchaseOptionCode, e);
            return null;
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtlayby.purchase.PurchaseOptionService#getAllPurchaseOptions()
     */
    @Override
    public List<PurchaseOptionModel> getAllPurchaseOptions() {
        return purchaseOptionDao.getAllPurchaseOptions();
    }

    /**
     * @return the purchaseOptionDao
     */
    public PurchaseOptionDao getPurchaseOptionDao() {
        return purchaseOptionDao;
    }

    /**
     * @param purchaseOptionDao
     *            the purchaseOptionDao to set
     */
    @Required
    public void setPurchaseOptionDao(final PurchaseOptionDao purchaseOptionDao) {
        this.purchaseOptionDao = purchaseOptionDao;
    }

    /**
     * @param currentPurchaseOptionCode
     *            the buyNow to set
     */
    @Required
    public void setCurrentPurchaseOptionCode(final String currentPurchaseOptionCode) {
        this.currentPurchaseOptionCode = currentPurchaseOptionCode;
    }
}

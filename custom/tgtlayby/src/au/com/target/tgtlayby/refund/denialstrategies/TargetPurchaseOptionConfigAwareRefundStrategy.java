/**
 * 
 */
package au.com.target.tgtlayby.refund.denialstrategies;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.strategy.ReturnableCheck;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import org.springframework.util.Assert;

import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;


public class TargetPurchaseOptionConfigAwareRefundStrategy extends AbstractBusinessService implements
        ReturnableCheck {

    /* (non-Javadoc)
     * @see de.hybris.platform.returns.strategy.ReturnableCheck#perform(de.hybris.platform.core.model.order.OrderModel, de.hybris.platform.core.model.order.AbstractOrderEntryModel, long)
     */
    @Override
    public boolean perform(final OrderModel order, final AbstractOrderEntryModel entry, final long returnQuantity) {

        Assert.notNull(order, "OrderModel cannot be null");
        final PurchaseOptionConfigModel purchaseOptionConfig = order.getPurchaseOptionConfig();
        Assert.notNull(purchaseOptionConfig, "No PurchaseOptionConfig found on the order");

        return purchaseOptionConfig.getAllowOrderRefund().booleanValue();
    }
}

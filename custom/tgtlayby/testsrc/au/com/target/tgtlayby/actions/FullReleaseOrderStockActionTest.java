/**
 * 
 */
package au.com.target.tgtlayby.actions;

import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;


/**
 * @author mgazal
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class FullReleaseOrderStockActionTest {

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @InjectMocks
    @Spy
    private final FullReleaseOrderStockAction fullReleaseOrderStockAction = new FullReleaseOrderStockAction();

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Before
    public void setup() {
        willReturn(order).given(process).getOrder();
    }

    @Test
    public void testExecuteAction() throws RetryLaterException, Exception {
        fullReleaseOrderStockAction.executeAction(process);

        verify(fullReleaseOrderStockAction).releaseAllStockForOrder(order);
    }

    @Test
    public void testExecuteActionWithFluent() throws RetryLaterException, Exception {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        fullReleaseOrderStockAction.executeAction(process);

        verify(fullReleaseOrderStockAction, never()).releaseAllStockForOrder(any(OrderModel.class));
    }
}

/**
 * 
 */
package au.com.target.tgtlayby.attributes.dynamic;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderPurchaseOptionTest {

    private final OrderPurchaseOption attrHandler = new OrderPurchaseOption();

    @Mock
    private AbstractOrderModel abstractOrderModel;

    @Mock
    private PurchaseOptionModel purchaseOption;

    @Mock
    private PurchaseOptionConfigModel purchaseOptionConfig;


    @Test(expected = UnsupportedOperationException.class)
    public void testSet() {
        attrHandler.set(abstractOrderModel, purchaseOption);
    }

    @Test
    public void testGet() {

        BDDMockito.given(abstractOrderModel.getPurchaseOptionConfig()).willReturn(purchaseOptionConfig);
        BDDMockito.given(purchaseOptionConfig.getPurchaseOption()).willReturn(purchaseOption);

        Assert.assertEquals(purchaseOption, attrHandler.get(abstractOrderModel));
    }

    @Test
    public void testGetForNullConfig() {

        BDDMockito.given(abstractOrderModel.getPurchaseOptionConfig()).willReturn(null);

        Assert.assertNull(attrHandler.get(abstractOrderModel));
    }


}

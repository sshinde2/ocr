package au.com.target.tgtlayby.cart.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.strategies.ModifiableChecker;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.calculation.FindDeliveryCostStrategy;
import de.hybris.platform.order.strategies.calculation.OrderRequiresCalculationStrategy;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantTypeModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.delivery.dto.DeliveryCostDto;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.giftcards.GiftCardService;
import au.com.target.tgtcore.giftcards.converter.GiftRecipientFormConverter;
import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.GiftRecipientModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.model.TargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.order.TargetCommerceCartModificationStatus;
import au.com.target.tgtcore.order.TargetPreOrderService;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.voucher.service.TargetVoucherService;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.service.FluentStockLookupService;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtlayby.cart.data.DeliveryModeInformation;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.purchase.PurchaseOptionService;
import au.com.target.tgtpayment.model.AfterpayConfigModel;
import au.com.target.tgtpayment.service.AfterpayConfigService;


/**
 * Unit Test for {@link TargetCommerceCartServiceImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCommerceCartServiceImplTest {

    /**
     * 
     */
    private static final String PK_STR = "12345678";
    private static final String PK_STR2 = "12345679";
    private static final Integer STOCK_QUANTITY = Integer.valueOf(99);
    private static final Integer MAX_ORDER_QUANTITY = Integer.valueOf(5);
    private static final Integer MIN_ORDER_QUANTITY = Integer.valueOf(6);
    private static final String PREORDER_PREFIX = "preOrder.";


    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @Mock
    private TargetDeliveryService targetDeliveryService;

    @Mock
    private GiftCardService giftCardService;

    @Mock
    private ProductService productService;

    @Mock
    private TargetLaybyCartService targetLaybyCartService;

    @Mock
    private TimeService timeService;

    @Mock
    private ModelService modelService;

    @Mock
    private CalculationService calculationService;

    @Mock
    private PromotionsService promotionsService;

    @Mock
    private FindDeliveryCostStrategy findDeliveryCostStrategy;

    @Mock
    private ModifiableChecker<AbstractOrderEntryModel> entryOrderChecker;

    @Mock
    private PurchaseOptionService purchaseOptionService;

    @InjectMocks
    @Spy
    private final TargetCommerceCartServiceImpl targetCommerceCartService = new TargetCommerceCartServiceImpl();

    @Mock
    private SearchRestrictionService searchRestrictionService;

    @Mock
    private TargetVoucherService targetVoucherService;

    @Mock
    private GiftRecipientFormConverter giftRecipientFormConverter;

    @Mock
    private ProductModel product;

    @Mock
    private CartModel cart;

    @Mock
    private GiftRecipientModel giftRecipientModel1;

    @Mock
    private GiftRecipientModel giftRecipientModel2;

    @Mock
    private PurchaseOptionModel purchaseOption;

    @Mock
    private CartEntryModel cartEntry;

    @Mock
    private UnitModel unitModel;

    @Mock
    private WarehouseService warehouseService;

    @Mock
    private PurchaseOptionConfigModel purchaseOptionConfigModel;

    @Mock
    private WarehouseModel warehouseModel;

    @Mock
    private StockLevelModel stockLevelModel;

    @Mock
    private TargetStockService targetStockService;

    @Mock
    private SessionService sessionService;

    // Delivery mode mocks
    @Mock
    private TargetZoneDeliveryModeModel deliveryModeModel1;

    @Mock
    private TargetZoneDeliveryModeModel deliveryModeModel2;

    @Mock
    private TargetZoneDeliveryModeModel deliveryModeModel3;

    @Mock
    private TargetZoneDeliveryModeValueModel targetZoneDeliveryModeValueModel;

    // Cart entry mocks for delivery options
    @Mock
    private CartEntryModel cartEntryForDelivery1;

    @Mock
    private CartEntryModel cartEntryForDelivery2;

    @Mock
    private VariantProductModel product1;

    @Mock
    private VariantProductModel product2;

    @Mock
    private VariantProductModel productInvalid;

    @Mock
    private VariantProductModel productMinOrderEqualMaxOrder;

    @Mock
    private CurrencyModel currency;

    // Mock fee
    @Mock
    private PriceValue feePriceValue;

    // Mocks for testChangeCurrentUserOnAllCarts
    @Mock
    private TargetCustomerModel customer;

    @Mock
    private CartModel cartPO1;

    @Mock
    private CommerceCartCalculationStrategy commerceCartCalculationStrategy;

    @Mock
    private OrderRequiresCalculationStrategy orderRequiresCalculationStrategy;

    @Mock
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Mock
    private TargetFeatureSwitchService mockTargetFeatureSwitchService;

    @Mock
    private AfterpayConfigService afterpayConfigService;

    @Mock
    private TargetSharedConfigService targetSharedConfigService;

    @Mock
    private FluentStockLookupService fluentStockLookupService;

    @Mock
    private TargetPreOrderService targetPreOrderService;

    @Mock
    private TargetProductService targetProductService;

    @Mock
    private TargetSalesApplicationService targetSalesApplicationService;

    @Before
    public void setup() throws TargetUnknownIdentifierException {

        willReturn(Integer.valueOf(2)).given(targetSharedConfigService)
                .getInt(TgtCoreConstants.Config.MAX_PREORDER_THRESHOLD, 2);
        willReturn("P1000").given(productMinOrderEqualMaxOrder).getCode();
        willReturn(Integer.valueOf(99999)).given(targetStockService).getStockLevelAmountFromOnlineWarehouses(product);
        willReturn(Integer.valueOf(99999)).given(targetStockService).getStockLevelAmountFromOnlineWarehouses(product2);
        given(targetStockService.getStockLevel(product, warehouseModel)).willReturn(stockLevelModel);
        given(targetStockService.getStockLevel(product2, warehouseModel)).willReturn(stockLevelModel);
        given(targetStockService.getStockLevel(productMinOrderEqualMaxOrder, warehouseModel))
                .willReturn(
                        stockLevelModel);

        given(purchaseOptionService.getCurrentPurchaseOption()).willReturn(purchaseOption);

        given(product.getMaxOrderQuantity()).willReturn(MAX_ORDER_QUANTITY);
        given(product1.getMaxOrderQuantity()).willReturn(null);
        given(product2.getMaxOrderQuantity()).willReturn(null);
        given(productInvalid.getMaxOrderQuantity()).willReturn(MAX_ORDER_QUANTITY);
        given(productInvalid.getMaxOrderQuantity()).willReturn(MIN_ORDER_QUANTITY);
        given(productMinOrderEqualMaxOrder.getMaxOrderQuantity()).willReturn(MAX_ORDER_QUANTITY);
        given(productMinOrderEqualMaxOrder.getMaxOrderQuantity()).willReturn(MAX_ORDER_QUANTITY);

        willReturn(STOCK_QUANTITY).given(stockLevelModel).getAvailable();

        given(deliveryModeModel1.getCode()).willReturn("deliveryModeModel1");
        given(deliveryModeModel2.getCode()).willReturn("deliveryModeModel2");
        given(deliveryModeModel3.getCode()).willReturn("deliveryModeModel3");

        given(cart.getCurrency()).willReturn(currency);
        given(cart.getCurrency().getIsocode()).willReturn("AUD");
        willReturn(Boolean.TRUE).given(cart).getNet();

        given(targetDeliveryService.getZoneDeliveryModeValueForAbstractOrderAndMode(deliveryModeModel1, cart))
                .willReturn(targetZoneDeliveryModeValueModel);
        final DeliveryCostDto deliveryCostDto = new DeliveryCostDto();
        deliveryCostDto.setDeliveryCost(20.0);
        willReturn(deliveryCostDto).given(targetDeliveryService)
                .getDeliveryCostForDeliveryType(targetZoneDeliveryModeValueModel, cart);

        willReturn(Boolean.FALSE).given(mockTargetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.PRODUCT_PREVIEW_MODE);

        setUpAfterpayConfigModel();

        given(Integer.valueOf(targetSharedConfigService.getInt(TgtCoreConstants.Config.CART_ENTRY_THRESHOLD,
                Integer.MAX_VALUE))).willReturn(Integer.valueOf(300));

        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
    }

    @Test
    public void testAddToCartWithNullProduct() throws CommerceCartModificationException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Product model cannot be null");
        targetCommerceCartService.addToCart(cart, null, 1, unitModel);
    }

    @Test
    public void testAddToCartWithNullCart() throws CommerceCartModificationException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Cart model cannot be null");
        targetCommerceCartService.addToCart(null, product, 1, unitModel);
    }

    @Test
    public void testAddToCartWithInvalidQuantity() throws CommerceCartModificationException {
        expectedException.expect(CommerceCartModificationException.class);
        expectedException.expectMessage("Quantity must not be less than one");
        targetCommerceCartService.addToCart(cart, product, 0, unitModel);
    }

    @Test
    public void testAddToCartWithQuantityBiggerThanStock() throws CommerceCartModificationException {
        final long quantityToAdd = 150;
        willReturn(Integer.valueOf(99)).given(targetStockService).getStockLevelAmountFromOnlineWarehouses(product);
        willReturn(Integer.valueOf(99)).given(targetStockService).getStockLevelAmountFromOnlineWarehouses(product2);
        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart, product2,
                quantityToAdd, unitModel);
        assertThat(cartModification.getQuantityAdded()).isEqualTo(STOCK_QUANTITY.longValue());
    }


    @Test
    public void testAddToCartWithQuantityEqualStock() throws CommerceCartModificationException {
        final long quantityToAdd = STOCK_QUANTITY.longValue();
        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart, product2,
                quantityToAdd, unitModel);
        assertThat(cartModification.getQuantityAdded()).isEqualTo(quantityToAdd);
    }

    @Test
    public void testAddToCartWithMinOrderQtyBiggerThanMaxOrderQty() throws CommerceCartModificationException {
        final long quantityToAdd = STOCK_QUANTITY.longValue();
        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart, productInvalid,
                quantityToAdd, unitModel);
        assertThat(cartModification.getStatusCode()).isEqualTo(TargetCommerceCartModificationStatus.NO_STOCK);
    }

    @Test
    public void testAddToCartWithEntriesEqualToThreshold() throws CommerceCartModificationException {
        given(Integer.valueOf(targetSharedConfigService.getInt(TgtCoreConstants.Config.CART_ENTRY_THRESHOLD,
                Integer.MAX_VALUE))).willReturn(Integer.valueOf(2));

        final CartEntryModel entry1 = mock(CartEntryModel.class);
        final CartEntryModel entry2 = mock(CartEntryModel.class);
        given(cart.getEntries()).willReturn(Arrays.asList((AbstractOrderEntryModel)entry1, entry2));
        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart, product, 1,
                unitModel);
        assertThat(cartModification.getStatusCode())
                .isEqualTo(TargetCommerceCartModificationStatus.NO_MODIFICATION_OCCURED_CART_ENTRIES_THRESHOLD_REACHED);
    }

    @Test
    public void testAddToCartWithEntriesMoreThanThreshold() throws CommerceCartModificationException {
        given(Integer.valueOf(targetSharedConfigService.getInt(TgtCoreConstants.Config.CART_ENTRY_THRESHOLD,
                Integer.MAX_VALUE))).willReturn(Integer.valueOf(1));

        final CartEntryModel entry1 = mock(CartEntryModel.class);
        final CartEntryModel entry2 = mock(CartEntryModel.class);
        given(cart.getEntries()).willReturn(Arrays.asList((AbstractOrderEntryModel)entry1, entry2));
        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart, product, 1,
                unitModel);
        assertThat(cartModification.getStatusCode())
                .isEqualTo(TargetCommerceCartModificationStatus.NO_MODIFICATION_OCCURED_CART_ENTRIES_THRESHOLD_REACHED);
    }

    @Test
    public void testAddToCartWithEntriesLessThanThreshold() throws CommerceCartModificationException {
        given(Integer.valueOf(targetSharedConfigService.getInt(TgtCoreConstants.Config.CART_ENTRY_THRESHOLD,
                Integer.MAX_VALUE))).willReturn(Integer.valueOf(2));

        final CartEntryModel entry1 = mock(CartEntryModel.class);
        given(cart.getEntries()).willReturn(Arrays.asList((AbstractOrderEntryModel)entry1));
        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart, product, 1,
                unitModel);
        assertThat(cartModification.getStatusCode())
                .isEqualTo(TargetCommerceCartModificationStatus.SUCCESS);
    }

    @Test
    public void testAddToCartWithEntriesLessThanThresholdAndQuantityOfEntryMoreThanThresholdValue()
            throws CommerceCartModificationException {
        given(Integer.valueOf(targetSharedConfigService.getInt(TgtCoreConstants.Config.CART_ENTRY_THRESHOLD,
                Integer.MAX_VALUE))).willReturn(Integer.valueOf(2));

        final CartEntryModel entry1 = mock(CartEntryModel.class);
        given(cart.getEntries()).willReturn(Arrays.asList((AbstractOrderEntryModel)entry1));

        final long quantityToAdd = STOCK_QUANTITY.longValue();

        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart, product2,
                quantityToAdd, unitModel);
        assertThat(cartModification.getStatusCode())
                .isEqualTo(TargetCommerceCartModificationStatus.SUCCESS);
        assertThat(cartModification.getQuantityAdded()).isEqualTo(quantityToAdd);
    }

    @Test
    public void testAddToCartWithMinOrderQtyEqalMaxOrderQty() throws CommerceCartModificationException {
        willReturn(Integer.valueOf(5)).given(targetStockService)
                .getStockLevelAmountFromOnlineWarehouses(productMinOrderEqualMaxOrder);
        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart,
                productMinOrderEqualMaxOrder, 5, unitModel);

        assertThat(cartModification.getStatusCode()).isEqualTo(TargetCommerceCartModificationStatus.SUCCESS);
    }

    @Test
    public void testAddToCartWithQuantityBiggerMaxOrderQuantity() throws CommerceCartModificationException {
        final long quantityToAdd = 150;
        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart, product,
                quantityToAdd, unitModel);
        assertThat(cartModification.getQuantityAdded()).isEqualTo(MAX_ORDER_QUANTITY.longValue());
    }

    @Test
    public void testAddToCartWithFluentStockService() throws FluentClientException, CommerceCartModificationException {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);

        final Map<String, Integer> stockMap = new HashMap<>();
        stockMap.put("P1000", Integer.valueOf(10));
        willReturn(stockMap).given(fluentStockLookupService).lookupAts("P1000");

        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart,
                productMinOrderEqualMaxOrder, 5, unitModel);
        assertThat(cartModification.getStatusCode()).isEqualTo(TargetCommerceCartModificationStatus.SUCCESS);
    }


    @Test
    public void testAddToCartWithFluentStockServiceNoStock()
            throws FluentClientException, CommerceCartModificationException {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);

        final Map<String, Integer> stockMap = new HashMap<>();
        stockMap.put("P1000", Integer.valueOf(0));
        willReturn(stockMap).given(fluentStockLookupService).lookupAts("P1000");

        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart,
                productMinOrderEqualMaxOrder, 5, unitModel);
        assertThat(cartModification.getStatusCode()).isEqualTo(TargetCommerceCartModificationStatus.NO_STOCK);
    }

    @Test(expected = CommerceCartModificationException.class)
    public void testAddToCartWithFluentStockServiceException()
            throws FluentClientException, CommerceCartModificationException {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);

        willThrow(new FluentClientException(new Error())).given(fluentStockLookupService).lookupAts("P1000");

        targetCommerceCartService.addToCart(cart, productMinOrderEqualMaxOrder, 5, unitModel);
    }

    @Test
    public void testAddToCartWithFluentStockServiceEmptyStockMap()
            throws FluentClientException, CommerceCartModificationException {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        willReturn(null).given(fluentStockLookupService).lookupStock(
                Arrays.asList("P1000"),
                Arrays.asList(TgtCoreConstants.DELIVERY_TYPE.CC, TgtCoreConstants.DELIVERY_TYPE.HD,
                        TgtCoreConstants.DELIVERY_TYPE.ED),
                null);

        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart,
                productMinOrderEqualMaxOrder, 5, unitModel);
        assertThat(cartModification.getStatusCode()).isEqualTo(TargetCommerceCartModificationStatus.NO_STOCK);
    }

    @Test
    public void testAddToCartWithFluentStockServiceThrowingException()
            throws FluentClientException, CommerceCartModificationException {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        given(fluentStockLookupService.lookupStock(product.getCode(), null))
                .willThrow(new FluentClientException(new Error()));
        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart,
                productMinOrderEqualMaxOrder, 5, unitModel);

        assertThat(cartModification.getStatusCode()).isEqualTo(TargetCommerceCartModificationStatus.NO_STOCK);
    }

    @Test
    public void testAddToCartWithQuantityEqualMaxOrderQuantity() throws CommerceCartModificationException {
        final long quantityToAdd = 5;
        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart, product,
                quantityToAdd, unitModel);
        assertThat(cartModification.getQuantityAdded()).isEqualTo(quantityToAdd);
    }

    @Test
    public void testAddToCartWithBaseProduct() throws CommerceCartModificationException {
        expectedException.expect(CommerceCartModificationException.class);
        expectedException.expectMessage("Choose a variant instead of the base product");

        final VariantProductModel variantProduct = mock(VariantProductModel.class);

        final VariantTypeModel variantType = mock(VariantTypeModel.class);
        given(product.getVariantType()).willReturn(variantType);
        given(product.getVariants()).willReturn(Collections.singletonList(variantProduct));

        targetCommerceCartService.addToCart(cart, product, 1, unitModel);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testValidAddToCartWithNullUnit() throws CommerceCartModificationException {


        given(cartEntry.getQuantity()).willReturn(Long.valueOf(1));
        given(targetLaybyCartService.addNewEntry(cart, product, 1, null)).willReturn(cartEntry);

        final CommerceCartModification result = targetCommerceCartService.addToCart(cart, product, 1, null);

        assertThat(result).isNotNull();

        verify(productService).getOrderableUnit(product);
        assertThat(result.getStatusCode()).isEqualTo(CommerceCartModificationStatus.SUCCESS);
        assertThat(result.getQuantity()).isEqualTo(1);
        assertThat(result.getQuantityAdded()).isEqualTo(1);
        assertThat(result.getEntry()).isEqualTo(cartEntry);
    }

    @Test
    public void testAddToCartWithPreviewProduct() throws CommerceCartModificationException {
        final AbstractTargetVariantProductModel mockVariant = mock(AbstractTargetVariantProductModel.class);
        willReturn(Boolean.TRUE).given(mockVariant).getPreview();

        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.PRODUCT_PREVIEW_MODE);

        given(cartEntry.getQuantity()).willReturn(Long.valueOf(1));
        given(targetLaybyCartService.addNewEntry(cart, mockVariant, 1, unitModel)).willReturn(cartEntry);

        final CommerceCartModification result = targetCommerceCartService.addToCart(cart, mockVariant, 1, unitModel);

        assertThat(result).isNotNull();

        verifyZeroInteractions(productService);

        assertThat(result.getStatusCode()).isEqualTo(TargetCommerceCartModificationStatus.PREVIEW);
        assertThat(result.getQuantity()).isEqualTo(0);
        assertThat(result.getQuantityAdded()).isEqualTo(0);
        assertThat(result.getEntry()).isNull();
    }

    @Test
    public void testValidAddToCartWithValidUnit() throws CommerceCartModificationException {

        given(cartEntry.getQuantity()).willReturn(Long.valueOf(1));
        given(targetLaybyCartService.addNewEntry(cart, product, 1, unitModel)).willReturn(cartEntry);

        final CommerceCartModification result = targetCommerceCartService.addToCart(cart, product, 1, unitModel);

        assertThat(result).isNotNull();

        verifyZeroInteractions(productService);

        assertThat(result.getStatusCode()).isEqualTo(CommerceCartModificationStatus.SUCCESS);
        assertThat(result.getQuantity()).isEqualTo(1);
        assertThat(result.getQuantityAdded()).isEqualTo(1);
        assertThat(result.getEntry()).isEqualTo(cartEntry);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateEntryEssentialCheckProductNull() throws CommerceCartModificationException {
        targetCommerceCartService.updateEntry(cart, null, 1);
    }

    @Test
    public void updateEntryEssentialCheckPurchaseZeroQuantity() throws CommerceCartModificationException {
        given(cartEntry.getQuantity()).willReturn(Long.valueOf(10));
        given(targetLaybyCartService.getEntryForProduct(cart, product)).willReturn(
                cartEntry);
        willReturn(Boolean.TRUE).given(entryOrderChecker).canModify(cartEntry);
        final CommerceCartModification modification = targetCommerceCartService.updateEntry(cart, product, 0);

        assertThat(modification.getQuantity()).isEqualTo(0);
        assertThat(modification.getQuantityAdded()).isEqualTo(-10);
        assertThat(modification.getStatusCode()).isEqualTo(CommerceCartModificationStatus.SUCCESS);
    }

    @Test
    public void updateEntryEssentialCheckPurchaseSameQuantity() throws CommerceCartModificationException {
        given(cartEntry.getQuantity()).willReturn(Long.valueOf(3));
        given(cartEntry.getProduct()).willReturn(product);
        given(targetLaybyCartService.getEntryForProduct(cart, product))
                .willReturn(cartEntry);
        willReturn(Boolean.TRUE).given(entryOrderChecker).canModify(cartEntry);
        final CommerceCartModification modification = targetCommerceCartService.updateEntry(cart, product, 3);

        assertThat(modification.getQuantity()).isEqualTo(3);
        assertThat(modification.getQuantityAdded()).isEqualTo(0);
        assertThat(modification.getStatusCode())
                .isEqualTo(TargetCommerceCartModificationStatus.NO_MODIFICATION_OCCURRED);
    }

    @Test
    public void updateEntryEssentialCheckPurchaseNormal() throws CommerceCartModificationException {
        given(cartEntry.getQuantity()).willReturn(Long.valueOf(10));
        given(cartEntry.getProduct()).willReturn(product2);
        given(targetLaybyCartService.getEntryForProduct(cart, product2))
                .willReturn(cartEntry);
        willReturn(Boolean.TRUE).given(entryOrderChecker).canModify(cartEntry);
        final CommerceCartModification modification = targetCommerceCartService.updateEntry(cart, product2, 5);

        assertThat(modification.getQuantity()).isEqualTo(5);
        assertThat(modification.getQuantityAdded()).isEqualTo(-5);
        assertThat(modification.getStatusCode()).isEqualTo(CommerceCartModificationStatus.SUCCESS);
    }

    @Test
    public void updateEntryEssentialCheckPurchaseLowStock() throws CommerceCartModificationException {
        given(cartEntry.getQuantity()).willReturn(Long.valueOf(10));
        given(cartEntry.getProduct()).willReturn(product2);
        given(targetLaybyCartService.getEntryForProduct(cart, product2))
                .willReturn(cartEntry);
        given(targetLaybyCartService.getEntriesForProduct(cart, product2))
                .willReturn(Collections.singletonList(cartEntry));
        willReturn(Boolean.TRUE).given(entryOrderChecker).canModify(cartEntry);
        willReturn(Integer.valueOf(3)).given(targetStockService).getStockLevelAmountFromOnlineWarehouses(product2);
        final CommerceCartModification modification = targetCommerceCartService.updateEntry(cart, product2, 5);

        assertThat(modification.getQuantity()).isEqualTo(3);
        assertThat(modification.getQuantityAdded()).isEqualTo(-7);
        assertThat(modification.getStatusCode()).isEqualTo(CommerceCartModificationStatus.LOW_STOCK);
    }

    @Test
    public void updateEntryEssentialPurchaseInsufficientStock() throws CommerceCartModificationException {
        given(cartEntry.getQuantity()).willReturn(Long.valueOf(3));
        given(cartEntry.getProduct()).willReturn(product2);
        given(targetLaybyCartService.getEntryForProduct(cart, product2))
                .willReturn(cartEntry);
        given(targetLaybyCartService.getEntriesForProduct(cart, product2))
                .willReturn(Collections.singletonList(cartEntry));
        willReturn(Boolean.TRUE).given(entryOrderChecker).canModify(cartEntry);
        willReturn(Integer.valueOf(3)).given(targetStockService).getStockLevelAmountFromOnlineWarehouses(product2);
        final CommerceCartModification modification = targetCommerceCartService.updateEntry(cart, product2, 5);

        assertThat(modification.getQuantity()).isEqualTo(3);
        assertThat(modification.getQuantityAdded()).isEqualTo(0);
        assertThat(modification.getStatusCode())
                .isEqualTo(TargetCommerceCartModificationStatus.NO_MODIFICATION_OCCURRED);
    }

    @Test
    public void updateEntryEssentialPurchaseBelowMinimumQty() throws CommerceCartModificationException {
        given(cartEntry.getQuantity()).willReturn(Long.valueOf(3));
        given(cartEntry.getProduct()).willReturn(product2);
        given(product2.getMinOrderQuantity()).willReturn(Integer.valueOf(2));
        given(targetLaybyCartService.getEntryForProduct(cart, product2))
                .willReturn(cartEntry);
        willReturn(Boolean.TRUE).given(entryOrderChecker).canModify(cartEntry);
        willReturn(Integer.valueOf(3)).given(targetStockService).getStockLevelAmount(product2, warehouseModel);
        final CommerceCartModification modification = targetCommerceCartService.updateEntry(cart, product2, 1);

        assertThat(modification.getQuantity()).isEqualTo(2);
        assertThat(modification.getQuantityAdded()).isEqualTo(0);
        assertThat(modification.getStatusCode())
                .isEqualTo(TargetCommerceCartModificationStatus.NO_MODFICATION_OCCURED_INSUFFICIENT_QUANTITY);
    }

    @Test
    public void testUpdateEntryWhenNewQuanityAndExistingQtyInCartIsLessThanMinAllowedQuantity()
            throws CommerceCartModificationException {
        given(cartEntry.getQuantity()).willReturn(Long.valueOf(1));
        given(cartEntry.getProduct()).willReturn(product2);
        given(product2.getMinOrderQuantity()).willReturn(Integer.valueOf(4));
        given(targetLaybyCartService.getEntryForProduct(cart, product2))
                .willReturn(cartEntry);
        willReturn(Boolean.TRUE).given(entryOrderChecker).canModify(cartEntry);
        willReturn(Integer.valueOf(10)).given(targetStockService).getStockLevelAmount(product2, warehouseModel);
        final CommerceCartModification modification = targetCommerceCartService.updateEntry(cart, product2, 2);

        assertThat(modification.getQuantity()).isEqualTo(0);
        assertThat(modification.getQuantityAdded()).isEqualTo(-1);
        assertThat(modification.getStatusCode())
                .isEqualTo(TargetCommerceCartModificationStatus.NO_STOCK);
    }

    @Test
    public void testUpdateEntryWhenNewQuanityIsZero()
            throws CommerceCartModificationException {
        given(cartEntry.getQuantity()).willReturn(Long.valueOf(1));
        given(product2.getMinOrderQuantity()).willReturn(Integer.valueOf(4));
        given(targetLaybyCartService.getEntryForProduct(cart, product2))
                .willReturn(cartEntry);
        willReturn(Boolean.TRUE).given(entryOrderChecker).canModify(cartEntry);
        final CommerceCartModification modification = targetCommerceCartService.updateEntry(cart, product2, 0);

        assertThat(modification.getQuantity()).isEqualTo(0);
        assertThat(modification.getQuantityAdded()).isEqualTo(-1);
        assertThat(modification.getStatusCode())
                .isEqualTo(TargetCommerceCartModificationStatus.SUCCESS);
        verifyZeroInteractions(targetStockService);
        verifyZeroInteractions(fluentStockLookupService);
    }

    @Test
    public void updateEntryEssentialPurchaseNoStock() throws CommerceCartModificationException {
        given(cartEntry.getQuantity()).willReturn(Long.valueOf(3));
        given(cartEntry.getProduct()).willReturn(product2);
        given(targetLaybyCartService.getEntryForProduct(cart, product2))
                .willReturn(cartEntry);
        given(targetLaybyCartService.getEntriesForProduct(cart, product2))
                .willReturn(Collections.singletonList(cartEntry));
        willReturn(Boolean.TRUE).given(entryOrderChecker).canModify(cartEntry);
        willReturn(Integer.valueOf(0)).given(targetStockService).getStockLevelAmountFromOnlineWarehouses(product2);
        final CommerceCartModification modification = targetCommerceCartService.updateEntry(cart, product2, 4);

        assertThat(modification.getQuantity()).isEqualTo(0);
        assertThat(modification.getQuantityAdded()).isEqualTo(-3);
        assertThat(modification.getStatusCode())
                .isEqualTo(TargetCommerceCartModificationStatus.NO_STOCK);
    }

    @Test
    public void updateEntryEssentialPurchasePreviewProduct() throws CommerceCartModificationException {
        given(cartEntry.getQuantity()).willReturn(Long.valueOf(3));

        final TargetColourVariantProductModel mockVariant = mock(TargetColourVariantProductModel.class);
        given(cartEntry.getProduct()).willReturn(mockVariant);
        given(mockVariant.getMinOrderQuantity()).willReturn(Integer.valueOf(2));
        willReturn(Boolean.TRUE).given(mockVariant).getPreview();

        given(targetLaybyCartService.getEntryForProduct(cart, mockVariant))
                .willReturn(cartEntry);
        willReturn(Boolean.TRUE).given(entryOrderChecker).canModify(cartEntry);
        willReturn(Integer.valueOf(3)).given(targetStockService).getStockLevelAmount(mockVariant, warehouseModel);

        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.PRODUCT_PREVIEW_MODE);

        final CommerceCartModification modification = targetCommerceCartService.updateEntry(cart, mockVariant, 1);

        assertThat(modification.getQuantity()).isEqualTo(0);
        assertThat(modification.getQuantityAdded()).isEqualTo(-3);
        assertThat(modification.getStatusCode())
                .isEqualTo(TargetCommerceCartModificationStatus.PREVIEW);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPerformSOHEmptyParam() throws CommerceCartModificationException {
        targetCommerceCartService.performSOH(null);
    }

    @Test
    public void testPerformSOHEmptyEntries() throws CommerceCartModificationException {
        given(cart.getEntries()).willReturn(Collections.EMPTY_LIST);
        final List<CommerceCartModification> resultList = targetCommerceCartService.performSOH(cart);

        assertThat(resultList).hasSize(0);
    }

    @Test
    public void testPerformSOH() throws CommerceCartModificationException {
        willReturn(Integer.valueOf(99)).given(stockLevelModel).getAvailable();
        given(cartEntry.getQuantity()).willReturn(Long.valueOf(10));
        given(cartEntry.getProduct()).willReturn(product2);
        given(product2.getApprovalStatus()).willReturn(ArticleApprovalStatus.APPROVED);
        given(targetLaybyCartService.getEntryForProduct(cart, product2))
                .willReturn(cartEntry);
        willReturn(Boolean.TRUE).given(entryOrderChecker).canModify(cartEntry);

        given(cart.getEntries()).willReturn(Collections.singletonList((AbstractOrderEntryModel)cartEntry));
        final List<CommerceCartModification> resultList = targetCommerceCartService.performSOH(cart);

        assertThat(resultList).hasSize(1);

        final CommerceCartModification modification = resultList.get(0);
        assertThat(modification.getQuantity()).isEqualTo(10);
        assertThat(modification.getQuantityAdded()).isEqualTo(0);
        assertThat(modification.getStatusCode())
                .isEqualTo(TargetCommerceCartModificationStatus.NO_MODIFICATION_OCCURRED);
    }

    @Test
    public void testPerformSOHUnapprovedProducts() throws CommerceCartModificationException {
        willReturn(Integer.valueOf(99)).given(stockLevelModel).getAvailable();
        given(cartEntry.getQuantity()).willReturn(Long.valueOf(10));
        given(cartEntry.getProduct()).willReturn(product2);
        given(product2.getApprovalStatus()).willReturn(ArticleApprovalStatus.UNAPPROVED);
        given(targetLaybyCartService.getEntryForProduct(cart, product2))
                .willReturn(cartEntry);
        willReturn(Boolean.TRUE).given(entryOrderChecker).canModify(cartEntry);

        given(cart.getEntries()).willReturn(Collections.singletonList((AbstractOrderEntryModel)cartEntry));
        final List<CommerceCartModification> resultList = targetCommerceCartService.performSOH(cart);

        assertThat(resultList).hasSize(1);

        final CommerceCartModification modification = resultList.get(0);
        assertThat(modification.getQuantity()).isEqualTo(10);
        assertThat(modification.getQuantityAdded()).isEqualTo(0);
        assertThat(modification.getStatusCode())
                .isEqualTo(TargetCommerceCartModificationStatus.NO_MODIFICATION_OCCURRED);
    }

    @Test
    public void testPerformSOHPreviewProducts() throws CommerceCartModificationException {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.PRODUCT_PREVIEW_MODE);

        final AbstractTargetVariantProductModel mockVariant = mock(AbstractTargetVariantProductModel.class);
        willReturn(Boolean.TRUE).given(mockVariant).getPreview();

        willReturn(Integer.valueOf(99)).given(stockLevelModel).getAvailable();
        given(cartEntry.getQuantity()).willReturn(Long.valueOf(10));
        given(cartEntry.getProduct()).willReturn(mockVariant);
        given(targetLaybyCartService.getEntryForProduct(cart, mockVariant))
                .willReturn(cartEntry);
        willReturn(Boolean.TRUE).given(entryOrderChecker).canModify(cartEntry);

        given(cart.getEntries()).willReturn(Collections.singletonList((AbstractOrderEntryModel)cartEntry));
        final List<CommerceCartModification> resultList = targetCommerceCartService.performSOH(cart);

        assertThat(resultList).hasSize(1);

        final CommerceCartModification modification = resultList.get(0);
        assertThat(modification.getQuantity()).isEqualTo(0);
        assertThat(modification.getQuantityAdded()).isEqualTo(-10);
        assertThat(modification.getStatusCode())
                .isEqualTo(TargetCommerceCartModificationStatus.PREVIEW);
    }

    @Test
    public void testPerformSOHWithStockFromFluent()
            throws CommerceCartModificationException, FluentClientException {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        final CartEntryModel entry1 = mock(CartEntryModel.class);
        final CartEntryModel entry2 = mock(CartEntryModel.class);
        given(cart.getEntries()).willReturn(Arrays.asList((AbstractOrderEntryModel)entry1, entry2));
        given(entry1.getProduct()).willReturn(product1);
        given(entry1.getQuantity()).willReturn(Long.valueOf(1L));
        given(entry2.getProduct()).willReturn(product2);
        given(entry2.getQuantity()).willReturn(Long.valueOf(1L));

        given(product1.getCode()).willReturn("P1");
        given(product2.getCode()).willReturn("P2");

        final Map<String, Integer> stockMap = new HashMap<>();
        stockMap.put("P1", Integer.valueOf(10));
        stockMap.put("P2", Integer.valueOf(10));
        given(fluentStockLookupService.lookupAts("P1", "P2")).willReturn(stockMap);

        final List<CommerceCartModification> resultList = targetCommerceCartService.performSOH(cart);
        assertThat(resultList).hasSize(2);
        assertThat(resultList).onProperty("quantity").containsExactly(Long.valueOf(1L), Long.valueOf(1L));
        assertThat(resultList).onProperty("statusCode").containsExactly("noModificationOccurred",
                "noModificationOccurred");
    }

    @Test
    public void testPerformSOHWithStockFromFluentWithPreOrder()
            throws CommerceCartModificationException, FluentClientException {
        final TargetSizeVariantProductModel sizeProduct = mock(TargetSizeVariantProductModel.class);
        final TargetColourVariantProductModel colourVariant = mock(TargetColourVariantProductModel.class);
        given(sizeProduct.getBaseProduct()).willReturn(colourVariant);
        given(sizeProduct.getMaxOrderQuantity()).willReturn(Integer.valueOf(10));
        final Calendar start = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), Calendar.MONTH - 1, 15, 0, 0, 0);

        final Calendar end = Calendar.getInstance();
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 2, 22, 0, 0, 0);
        given(colourVariant.getPreOrderStartDateTime()).willReturn(start.getTime());
        given(colourVariant.getPreOrderEndDateTime()).willReturn(end.getTime());
        willReturn(Integer.valueOf(100)).given(targetPreOrderService)
                .getAvailablePreOrderQuantityForCurrentProduct(sizeProduct);

        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        final CartEntryModel entry1 = mock(CartEntryModel.class);
        final CartEntryModel entry2 = mock(CartEntryModel.class);
        given(cart.getEntries()).willReturn(Arrays.asList((AbstractOrderEntryModel)entry1, entry2));
        given(entry1.getProduct()).willReturn(product1);
        given(entry1.getQuantity()).willReturn(Long.valueOf(1L));
        given(entry2.getProduct()).willReturn(sizeProduct);
        given(entry2.getQuantity()).willReturn(Long.valueOf(1L));

        given(product1.getCode()).willReturn("P1");
        given(sizeProduct.getCode()).willReturn("PreOrder1");

        final Map<String, Integer> stockMap = new HashMap<>();
        stockMap.put("P1", Integer.valueOf(10));
        stockMap.put("PreOrder1", Integer.valueOf(10));
        given(fluentStockLookupService.lookupAts("P1", "PreOrder1")).willReturn(stockMap);

        final List<CommerceCartModification> resultList = targetCommerceCartService.performSOH(cart);
        assertThat(resultList).hasSize(2);
        assertThat(resultList).onProperty("quantity").containsExactly(Long.valueOf(1L), Long.valueOf(1L));
        assertThat(resultList).onProperty("statusCode").containsExactly("noModificationOccurred",
                "noModificationOccurred");
    }

    @Test
    public void testPerformSOHWithStockFromFluentWithAdjustment()
            throws CommerceCartModificationException, FluentClientException {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        final CartEntryModel entry1 = mock(CartEntryModel.class);
        final CartEntryModel entry2 = mock(CartEntryModel.class);
        given(cart.getEntries()).willReturn(Arrays.asList((AbstractOrderEntryModel)entry1, entry2));
        given(entry1.getProduct()).willReturn(product1);
        given(entry1.getQuantity()).willReturn(Long.valueOf(11L));
        given(entry2.getProduct()).willReturn(product2);
        given(entry2.getQuantity()).willReturn(Long.valueOf(1L));

        given(product1.getCode()).willReturn("P1");
        given(product2.getCode()).willReturn("P2");

        final Map<String, Integer> stockMap = new HashMap<>();
        stockMap.put("P1", Integer.valueOf(10));
        stockMap.put("P2", Integer.valueOf(10));
        given(fluentStockLookupService.lookupAts("P1", "P2")).willReturn(stockMap);

        final List<CommerceCartModification> resultList = targetCommerceCartService.performSOH(cart);
        assertThat(resultList).hasSize(2);
        assertThat(resultList).onProperty("quantity").containsExactly(Long.valueOf(10L), Long.valueOf(1L));
        assertThat(resultList).onProperty("statusCode").containsExactly("lowStock", "noModificationOccurred");
    }

    @Test
    public void testPerformSOHWithStockFromFluentWithNoStock()
            throws CommerceCartModificationException, FluentClientException {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        final CartEntryModel entry1 = mock(CartEntryModel.class);
        final CartEntryModel entry2 = mock(CartEntryModel.class);
        given(cart.getEntries()).willReturn(Arrays.asList((AbstractOrderEntryModel)entry1, entry2));
        given(entry1.getProduct()).willReturn(product1);
        given(entry1.getQuantity()).willReturn(Long.valueOf(1L));
        given(entry2.getProduct()).willReturn(product2);
        given(entry2.getQuantity()).willReturn(Long.valueOf(1L));

        given(product1.getCode()).willReturn("P1");
        given(product2.getCode()).willReturn("P2");

        final Map<String, Integer> stockMap = new HashMap<>();
        stockMap.put("P1", Integer.valueOf(10));
        stockMap.put("P2", Integer.valueOf(0));
        given(fluentStockLookupService.lookupAts("P1", "P2")).willReturn(stockMap);

        final List<CommerceCartModification> resultList = targetCommerceCartService.performSOH(cart);
        assertThat(resultList).hasSize(2);
        assertThat(resultList).onProperty("quantity").containsExactly(Long.valueOf(1L), Long.valueOf(0L));
        assertThat(resultList).onProperty("statusCode").containsExactly("noModificationOccurred", "noStock");
    }

    @Test(expected = CommerceCartModificationException.class)
    public void testPerformSOHWithStockFromFluentException()
            throws CommerceCartModificationException, FluentClientException {
        willReturn(Boolean.TRUE).given(mockTargetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);
        final CartEntryModel entry1 = mock(CartEntryModel.class);
        final CartEntryModel entry2 = mock(CartEntryModel.class);
        given(cart.getEntries()).willReturn(Arrays.asList((AbstractOrderEntryModel)entry1, entry2));
        given(entry1.getProduct()).willReturn(product1);
        given(entry1.getQuantity()).willReturn(Long.valueOf(1L));
        given(entry2.getProduct()).willReturn(product2);
        given(entry2.getQuantity()).willReturn(Long.valueOf(1L));

        given(product1.getCode()).willReturn("P1");
        given(product2.getCode()).willReturn("P2");

        given(fluentStockLookupService.lookupAts("P1", "P2")).willThrow(new FluentClientException(new Error()));

        targetCommerceCartService.performSOH(cart);
    }

    @Test
    public void testPerformSOHWithPreOrderProduct() throws CommerceCartModificationException {
        final TargetSizeVariantProductModel sizeProduct = mock(TargetSizeVariantProductModel.class);
        final CartEntryModel entry1 = mock(CartEntryModel.class);
        given(cart.getEntries()).willReturn(Arrays.asList((AbstractOrderEntryModel)entry1));
        given(entry1.getQuantity()).willReturn(Long.valueOf(10));
        given(entry1.getProduct()).willReturn(sizeProduct);
        given(sizeProduct.getCode()).willReturn("preOrderProduct1");
        given(sizeProduct.getMaxOrderQuantity()).willReturn(Integer.valueOf(10));
        final TargetColourVariantProductModel colourVariant = mock(TargetColourVariantProductModel.class);
        given(sizeProduct.getBaseProduct()).willReturn(colourVariant);

        final Calendar start = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), Calendar.MONTH - 1, 15, 0, 0, 0);

        final Calendar end = Calendar.getInstance();
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 2, 22, 0, 0, 0);
        given(colourVariant.getPreOrderStartDateTime()).willReturn(start.getTime());
        given(colourVariant.getPreOrderEndDateTime()).willReturn(end.getTime());
        willReturn(Integer.valueOf(100)).given(targetPreOrderService)
                .getAvailablePreOrderQuantityForCurrentProduct(sizeProduct);

        final List<CommerceCartModification> resultList = targetCommerceCartService.performSOH(cart);
        verify(targetPreOrderService).getAvailablePreOrderQuantityForCurrentProduct(sizeProduct);
        verify(targetStockService, never()).getStockLevelAmountFromOnlineWarehouses(sizeProduct);

        assertThat(resultList).isNotEmpty();
        assertThat(resultList.get(0).getQuantity()).isEqualTo(2);
        assertThat(resultList.get(0).getQuantityAdded()).isEqualTo(-8);
    }

    @Test
    public void testDeliveryModeApplicableForCart() {

        setupCartEntriesForDeliveryModeTests();
        // Set up products to have both delivery modes
        final Set<DeliveryModeModel> prodDeliveryModes = new HashSet<>();
        prodDeliveryModes.add(deliveryModeModel1);
        given(targetDeliveryService.getVariantsDeliveryModesFromBaseProduct(product1)).willReturn(
                prodDeliveryModes);
        assertThat(targetCommerceCartService.isDeliveryModeApplicableForCart(deliveryModeModel1, cart)).isTrue();
    }

    @Test
    public void testDeliveryModeNotApplicableForCart() {

        setupCartEntriesForDeliveryModeTests();

        // Set up products to have both delivery modes
        final Set<DeliveryModeModel> prodDeliveryModes = new HashSet<>();
        prodDeliveryModes.add(deliveryModeModel1);
        given(targetDeliveryService.getVariantsDeliveryModesFromBaseProduct(product1)).willReturn(
                prodDeliveryModes);
        assertThat(targetCommerceCartService.isDeliveryModeApplicableForCart(deliveryModeModel2, cart))
                .isFalse();
    }

    /**
     * If there are no entries then do not return any delivery information.
     */
    @Test
    public void testGetDeliveryModeOptionsInformationEmptyEntries() {

        given(cart.getEntries()).willReturn(Collections.EMPTY_LIST);
        final List<DeliveryModeInformation> listDeliveryModes = targetCommerceCartService
                .getDeliveryModeOptionsInformation(cart);
        assertThat(listDeliveryModes).isNotNull();
        assertThat(listDeliveryModes).hasSize(0);
    }


    /**
     * If there are no supported delivery modes then do not return any delivery information.
     */
    @Test
    public void testGetDeliveryModeOptionsInformationNoSupportedModes() {

        setupCartEntriesForDeliveryModeTests();

        given(targetDeliveryService.getSupportedDeliveryModeListForOrder(cart)).willReturn(
                Collections.EMPTY_LIST);

        final List<DeliveryModeInformation> listDeliveryModes = targetCommerceCartService
                .getDeliveryModeOptionsInformation(cart);
        assertThat(listDeliveryModes).isNotNull();
        assertThat(listDeliveryModes).hasSize(0);
    }


    @Test
    public void testGetDeliveryModeOptionsInformation() {

        setupCartEntriesForDeliveryModeTests();
        setupDeliveryServiceToReturnTwoSupportedModes();
        setupMockCartDeliveryFee();

        // Set up products to have both delivery modes
        final Set<DeliveryModeModel> prodDeliveryModes = new HashSet<>();
        prodDeliveryModes.add(deliveryModeModel1);
        prodDeliveryModes.add(deliveryModeModel2);
        given(targetDeliveryService.getVariantsDeliveryModesFromBaseProduct(product1)).willReturn(
                prodDeliveryModes);
        given(targetDeliveryService.getVariantsDeliveryModesFromBaseProduct(product2)).willReturn(
                prodDeliveryModes);

        final List<DeliveryModeInformation> listDeliveryModeInfos = targetCommerceCartService
                .getDeliveryModeOptionsInformation(cart);
        assertThat(listDeliveryModeInfos).isNotNull();
        assertThat(listDeliveryModeInfos).hasSize(2);

        // There should be a result DeliveryModeInformation for each of the modes we set up
        final List<DeliveryModeModel> resultModes = new ArrayList<>();
        for (final DeliveryModeInformation info : listDeliveryModeInfos) {
            resultModes.add(info.getDeliveryMode());
        }
        assertThat(resultModes.contains(deliveryModeModel1)).isTrue();
        assertThat(resultModes.contains(deliveryModeModel2)).isTrue();

    }

    @Test
    public void testGetDeliveryModeOptionsInformationWithModeExcluded() {

        setupCartEntriesForDeliveryModeTests();
        setupDeliveryServiceToReturnTwoSupportedModes();
        setupMockCartDeliveryFee();

        // Set up products to have only one delivery mode
        final Set<DeliveryModeModel> prodDeliveryModes = new HashSet<>();
        prodDeliveryModes.add(deliveryModeModel1);

        given(targetDeliveryService.getVariantsDeliveryModesFromBaseProduct(product1)).willReturn(
                prodDeliveryModes);
        given(targetDeliveryService.getVariantsDeliveryModesFromBaseProduct(product2)).willReturn(
                prodDeliveryModes);

        final List<DeliveryModeInformation> listDeliveryModeInfos = targetCommerceCartService
                .getDeliveryModeOptionsInformation(cart);
        assertThat(listDeliveryModeInfos).isNotNull();
        assertThat(listDeliveryModeInfos).hasSize(1);

        // There should be a result DeliveryModeInformation for only deliveryModeModel1
        final List<DeliveryModeModel> resultModes = new ArrayList<>();
        for (final DeliveryModeInformation info : listDeliveryModeInfos) {
            resultModes.add(info.getDeliveryMode());
        }
        assertThat(resultModes.contains(deliveryModeModel1)).isTrue();
        assertThat(resultModes.contains(deliveryModeModel2)).isFalse();
    }

    @Test
    public void testGetDeliveryModeOptionInformationForModeNoMatch() {

        setupCartEntriesForDeliveryModeTests();
        setupDeliveryServiceToReturnTwoSupportedModes();
        setupMockCartDeliveryFee();

        // Set up products to have both delivery modes
        final Set<DeliveryModeModel> prodDeliveryModes = new HashSet<>();
        prodDeliveryModes.add(deliveryModeModel1);
        prodDeliveryModes.add(deliveryModeModel2);

        given(targetDeliveryService.getVariantsDeliveryModesFromBaseProduct(product1)).willReturn(
                prodDeliveryModes);
        given(targetDeliveryService.getVariantsDeliveryModesFromBaseProduct(product2)).willReturn(
                prodDeliveryModes);

        // No types matching modes
        final DeliveryModeInformation listDeliveryModeInfo = targetCommerceCartService
                .getDeliveryModeOptionInformation(cart,
                        deliveryModeModel3);
        assertThat(listDeliveryModeInfo).isNull();
    }

    @Test
    public void testGetDeliveryModeOptionInformationForModeWithMatch() {

        setupCartEntriesForDeliveryModeTests();
        setupDeliveryServiceToReturnTwoSupportedModes();
        setupMockCartDeliveryFee();

        // Set up products to have both delivery modes
        final Set<DeliveryModeModel> prodDeliveryModes = new HashSet<>();
        prodDeliveryModes.add(deliveryModeModel1);
        prodDeliveryModes.add(deliveryModeModel2);

        given(targetDeliveryService.getVariantsDeliveryModesFromBaseProduct(product1)).willReturn(
                prodDeliveryModes);
        given(targetDeliveryService.getVariantsDeliveryModesFromBaseProduct(product2)).willReturn(
                prodDeliveryModes);


        // Has a matching mode        
        final DeliveryModeInformation listDeliveryModeInfo = targetCommerceCartService
                .getDeliveryModeOptionInformation(cart, deliveryModeModel1);
        assertThat(listDeliveryModeInfo).isNotNull();
        assertThat(listDeliveryModeInfo.getDeliveryMode()).isEqualTo(deliveryModeModel1);
    }

    @Test
    public void testCartPurchaseAmountLessThanMinPurchaseAmount() throws TargetUnknownIdentifierException {
        given(cart.getPurchaseOptionConfig()).willReturn(purchaseOptionConfigModel);
        given(purchaseOptionConfigModel.getMinimumPurchaseAmount()).willReturn(Double.valueOf(20.00));
        given(cart.getTotalPrice()).willReturn(Double.valueOf(10.00));
        final boolean result = targetCommerceCartService.validateCartPurchaseAmount(cart);
        assertThat(result).isFalse();
    }

    @Test
    public void testCartPurchaseAmountGreaterThanMinPurchaseAmount() throws TargetUnknownIdentifierException {
        given(cart.getPurchaseOption()).willReturn(purchaseOption);
        given(purchaseOptionConfigModel.getMinimumPurchaseAmount()).willReturn(Double.valueOf(20.00));
        given(cart.getTotalPrice()).willReturn(Double.valueOf(50.00));
        final boolean result = targetCommerceCartService.validateCartPurchaseAmount(cart);
        assertThat(result).isTrue();
    }

    @Test
    public void testValidateCartPurchaseAmountNoPurchaseOption() throws TargetUnknownIdentifierException {
        given(cart.getPurchaseOption()).willReturn(null);
        final boolean result = targetCommerceCartService.validateCartPurchaseAmount(cart);
        assertThat(result).isTrue();
    }

    @Test
    public void testRestoreCartWithNullSavedCart() throws CommerceCartRestorationException {
        expectedException.expect(IllegalArgumentException.class);
        final CartModel cartModel = null;
        final CommerceCartParameter cartParameter = new CommerceCartParameter();
        cartParameter.setCart(cartModel);
        targetCommerceCartService.restoreCart(cartParameter);
    }

    @Test
    public void testRestoreCartWithoutAnyCheckoutData() throws CommerceCartRestorationException,
            CommerceCartModificationException, CalculationException {

        final CartModel savedCart = mock(CartModel.class);
        given(savedCart.getTmdCardNumber()).willReturn(null);
        given(savedCart.getFlyBuysCode()).willReturn(null);
        given(savedCart.getDeliveryMode()).willReturn(null);
        given(savedCart.getZoneDeliveryModeValue()).willReturn(
                null);
        given(savedCart.getDeliveryAddress()).willReturn(null);
        given(savedCart.getCncStoreNumber()).willReturn(null);
        given(savedCart.getPaymentMode()).willReturn(null);
        given(savedCart.getPaymentInfo()).willReturn(null);
        willReturn(Boolean.TRUE).given(orderRequiresCalculationStrategy).requiresCalculation(savedCart);


        final CommerceCartParameter cartParameter = new CommerceCartParameter();
        cartParameter.setCart(savedCart);
        targetCommerceCartService.restoreCart(cartParameter);

        verify(modelService).save(savedCart);
        verify(calculationService).calculateTotals(savedCart, true);
        verify(targetCommerceCartService).performSOH(savedCart);
        verify(targetLaybyCartService).setSessionCart(savedCart);
    }


    @Test
    public void testRecalculateCartWithRequiresCalculationFalse() throws CommerceCartRestorationException,
            CommerceCartModificationException, CalculationException {

        final CartModel savedCart = mock(CartModel.class);
        willReturn(Boolean.FALSE).given(orderRequiresCalculationStrategy).requiresCalculation(savedCart);

        final CommerceCartParameter cartParameter = new CommerceCartParameter();
        cartParameter.setCart(savedCart);
        targetCommerceCartService.recalculateCart(cartParameter);
        verify(calculationService, never()).calculateTotals(savedCart, true);

    }

    @Test
    public void testRestoreCartWithAllCheckoutData() throws CommerceCartRestorationException,
            CommerceCartModificationException, CalculationException {

        final CartModel savedCart = mock(CartModel.class);
        final AddressModel mockAddress = mock(AddressModel.class);
        final PaymentModeModel mockPayMode = mock(PaymentModeModel.class);
        final PaymentInfoModel mockPayInfo = mock(PaymentInfoModel.class);

        final Collection<String> vouchers = new ArrayList<>();
        vouchers.add("test-voucher");

        given(savedCart.getTmdCardNumber()).willReturn("test-tmd");
        given(savedCart.getFlyBuysCode()).willReturn("test-flybuys");
        given(targetVoucherService.getAppliedVoucherCodes(savedCart)).willReturn(vouchers);
        given(savedCart.getDeliveryMode()).willReturn(deliveryModeModel1);
        given(savedCart.getZoneDeliveryModeValue()).willReturn(
                targetZoneDeliveryModeValueModel);
        given(savedCart.getDeliveryAddress()).willReturn(mockAddress);
        given(savedCart.getCncStoreNumber()).willReturn(Integer.valueOf(0));
        given(savedCart.getPaymentMode()).willReturn(mockPayMode);
        given(savedCart.getPaymentInfo()).willReturn(mockPayInfo);
        willReturn(Boolean.TRUE).given(orderRequiresCalculationStrategy).requiresCalculation(savedCart);

        final CommerceCartParameter cartParameter = new CommerceCartParameter();
        cartParameter.setCart(savedCart);
        targetCommerceCartService.restoreCart(cartParameter);

        verify(savedCart).setTmdCardNumber(null);
        verify(savedCart).setFlyBuysCode(null);
        verify(savedCart).setDeliveryMode(null);
        verify(savedCart, times(2)).setZoneDeliveryModeValue(null); // one call during restore and another one during recalculate
        verify(savedCart).setDeliveryAddress(null);
        verify(savedCart).setCncStoreNumber(null);
        verify(savedCart).setPaymentMode(null);
        verify(savedCart).setPaymentInfo(null);
        verify(modelService).save(savedCart);
        verify(calculationService).calculateTotals(savedCart, true);
        verify(targetCommerceCartService).performSOH(savedCart);
        verify(targetLaybyCartService).setSessionCart(savedCart);
    }

    @Test
    public void testRestoreCartWithAllCheckoutDataNoCleanup() throws CommerceCartRestorationException,
            CommerceCartModificationException, CalculationException {

        final CartModel savedCart = mock(CartModel.class);
        final AddressModel mockAddress = mock(AddressModel.class);
        final PaymentModeModel mockPayMode = mock(PaymentModeModel.class);
        final PaymentInfoModel mockPayInfo = mock(PaymentInfoModel.class);

        final Collection<String> vouchers = new ArrayList<>();
        vouchers.add("test-voucher");

        given(savedCart.getTmdCardNumber()).willReturn("test-tmd");
        given(savedCart.getFlyBuysCode()).willReturn("test-flybuys");
        given(targetVoucherService.getAppliedVoucherCodes(savedCart)).willReturn(vouchers);
        given(savedCart.getDeliveryMode()).willReturn(deliveryModeModel1);
        given(savedCart.getZoneDeliveryModeValue()).willReturn(
                targetZoneDeliveryModeValueModel);
        given(savedCart.getDeliveryAddress()).willReturn(mockAddress);
        given(savedCart.getCncStoreNumber()).willReturn(Integer.valueOf(0));
        given(savedCart.getPaymentMode()).willReturn(mockPayMode);
        given(savedCart.getPaymentInfo()).willReturn(mockPayInfo);
        willReturn(Boolean.TRUE).given(orderRequiresCalculationStrategy).requiresCalculation(savedCart);

        final CommerceCartParameter cartParameter = new CommerceCartParameter();
        cartParameter.setCart(savedCart);
        targetCommerceCartService.restoreCart(cartParameter, false);

        verify(savedCart, never()).setTmdCardNumber(null);
        verify(savedCart, never()).setFlyBuysCode(null);
        verify(savedCart, never()).setDeliveryMode(null);
        verify(savedCart).setZoneDeliveryModeValue(null);
        verify(savedCart, never()).setDeliveryAddress(null);
        verify(savedCart, never()).setCncStoreNumber(null);
        verify(savedCart, never()).setPaymentMode(null);
        verify(savedCart, never()).setPaymentInfo(null);
        verify(modelService, never()).save(savedCart);
        verify(calculationService).calculateTotals(savedCart, true);
        verify(targetCommerceCartService).performSOH(savedCart);
        verify(targetLaybyCartService).setSessionCart(savedCart);
    }

    @Test
    public void testRestoreCartWithAllCheckoutDataCleanup() throws CommerceCartRestorationException,
            CommerceCartModificationException, CalculationException {

        final CartModel savedCart = mock(CartModel.class);
        final AddressModel mockAddress = mock(AddressModel.class);
        final PaymentModeModel mockPayMode = mock(PaymentModeModel.class);
        final PaymentInfoModel mockPayInfo = mock(PaymentInfoModel.class);

        final Collection<String> vouchers = new ArrayList<>();
        vouchers.add("test-voucher");

        given(savedCart.getTmdCardNumber()).willReturn("test-tmd");
        given(savedCart.getFlyBuysCode()).willReturn("test-flybuys");
        given(targetVoucherService.getAppliedVoucherCodes(savedCart)).willReturn(vouchers);
        given(savedCart.getDeliveryMode()).willReturn(deliveryModeModel1);
        given(savedCart.getZoneDeliveryModeValue()).willReturn(
                targetZoneDeliveryModeValueModel);
        given(savedCart.getDeliveryAddress()).willReturn(mockAddress);
        given(savedCart.getCncStoreNumber()).willReturn(Integer.valueOf(0));
        given(savedCart.getPaymentMode()).willReturn(mockPayMode);
        given(savedCart.getPaymentInfo()).willReturn(mockPayInfo);
        given(savedCart.getPaymentAddress()).willReturn(mockAddress);
        willReturn(Boolean.TRUE).given(orderRequiresCalculationStrategy).requiresCalculation(savedCart);

        final CommerceCartParameter cartParameter = new CommerceCartParameter();
        cartParameter.setCart(savedCart);
        targetCommerceCartService.restoreCart(cartParameter, true);

        verify(savedCart).setTmdCardNumber(null);
        verify(savedCart).setFlyBuysCode(null);
        verify(savedCart).setDeliveryMode(null);
        verify(savedCart, times(2)).setZoneDeliveryModeValue(null);
        verify(savedCart).setDeliveryAddress(null);
        verify(savedCart).setCncStoreNumber(null);
        verify(savedCart).setPaymentMode(null);
        verify(savedCart).setPaymentInfo(null);
        verify(savedCart).setPaymentAddress(null);
        verify(modelService).save(savedCart);
        verify(calculationService).calculateTotals(savedCart, true);
        verify(targetCommerceCartService).performSOH(savedCart);
        verify(targetLaybyCartService).setSessionCart(savedCart);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemoveGiftCardEntryEssentialCheckProductNull() throws CommerceCartModificationException {
        targetCommerceCartService.removeGiftCardRecipient(cart, null, 1, PK_STR);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemoveGiftCardEntryEssentialCheckCartNull() throws CommerceCartModificationException {
        targetCommerceCartService.removeGiftCardRecipient(null, product, 1, PK_STR);
    }

    @Test(expected = CommerceCartModificationException.class)
    public void testRemoveGiftCardEntryEssentialCheckProductNotDigital() throws CommerceCartModificationException {
        given(product.getVariants()).willReturn(Collections.singletonList(product1));
        given(targetLaybyCartService.getEntryForProduct(cart, product)).willReturn(cartEntry);
        willReturn(Boolean.FALSE).given(giftCardService).isProductAGiftCard(product);
        targetCommerceCartService.removeGiftCardRecipient(cart, product, 0, PK_STR);
    }

    @Test(expected = CommerceCartModificationException.class)
    public void testRemoveGiftCardEntryEssentialCheckRecipientEntryEmpty() throws CommerceCartModificationException {
        given(product.getVariants()).willReturn(Collections.singletonList(product1));
        given(targetLaybyCartService.getEntryForProduct(cart, product)).willReturn(cartEntry);
        willReturn(Boolean.TRUE).given(giftCardService).isProductAGiftCard(product);
        final List<GiftRecipientModel> recipients = new ArrayList<>();
        given(cartEntry.getGiftRecipients()).willReturn(recipients);
        targetCommerceCartService.removeGiftCardRecipient(cart, product, 0, PK_STR);
    }

    @Test(expected = CommerceCartModificationException.class)
    public void testRemoveGiftCardEntryEssentialCheckRecipientEntryMismatchedPKID()
            throws CommerceCartModificationException {
        given(product.getVariants()).willReturn(Collections.singletonList(product1));
        given(targetLaybyCartService.getEntryForProduct(cart, product)).willReturn(cartEntry);
        willReturn(Boolean.TRUE).given(giftCardService).isProductAGiftCard(product);
        final List<GiftRecipientModel> recipients = new ArrayList<>();
        given(cartEntry.getGiftRecipients()).willReturn(recipients);
        targetCommerceCartService.removeGiftCardRecipient(cart, product, 0, PK_STR);
    }

    @Test
    public void testRemoveGiftCardRemoveEntry() throws CommerceCartModificationException {
        given(product.getVariants()).willReturn(Collections.singletonList(product1));
        given(targetLaybyCartService.getEntryForProduct(cart, product)).willReturn(cartEntry);
        willReturn(Boolean.TRUE).given(giftCardService).isProductAGiftCard(product);
        given(giftRecipientModel1.getPk()).willReturn(PK.parse(PK_STR));
        final List<GiftRecipientModel> recipients = new ArrayList<>();
        recipients.add(giftRecipientModel1);
        given(cartEntry.getGiftRecipients()).willReturn(recipients);
        final CommerceCartModification cartModification = targetCommerceCartService.removeGiftCardRecipient(cart,
                product, 0, PK_STR);
        assertThat(cartModification).isNotNull();
        assertThat(cartModification.getStatusCode()).isNotNull().isNotEmpty()
                .isEqualTo(TargetCommerceCartModificationStatus.SUCCESS);
        verify(modelService).remove(cartEntry);
    }

    @Test
    public void testRemoveGiftCardRemoveARecipient() throws CommerceCartModificationException {
        given(product.getVariants()).willReturn(Collections.singletonList(product1));
        given(targetLaybyCartService.getEntryForProduct(cart, product)).willReturn(cartEntry);
        willReturn(Boolean.TRUE).given(giftCardService).isProductAGiftCard(product);
        final List<GiftRecipientModel> recipients = new ArrayList<>();
        recipients.add(giftRecipientModel1);
        recipients.add(giftRecipientModel2);
        given(cartEntry.getGiftRecipients()).willReturn(recipients);
        given(giftRecipientModel1.getPk()).willReturn(PK.parse(PK_STR));
        given(giftRecipientModel2.getPk()).willReturn(PK.parse(PK_STR2));
        final CommerceCartModification cartModification = targetCommerceCartService.removeGiftCardRecipient(cart,
                product, 0, PK_STR);
        assertThat(cartModification).isNotNull();
        assertThat(cartModification).isNotNull();
        assertThat(cartModification.getStatusCode()).isNotNull().isNotEmpty()
                .isEqualTo(TargetCommerceCartModificationStatus.SUCCESS);
        assertThat(cartModification.getQuantityAdded()).isNotNull().isEqualTo(-1);
        verify(modelService, never()).remove(cartEntry);
    }

    @Test(expected = CommerceCartModificationException.class)
    public void testUpdateRecipientDetailsEssentialCheckProductNotDigital() throws CommerceCartModificationException {
        given(product.getVariants()).willReturn(Collections.singletonList(product1));
        given(targetLaybyCartService.getEntryForProduct(cart, product)).willReturn(cartEntry);
        willReturn(Boolean.FALSE).given(giftCardService).isProductAGiftCard(product);
        final GiftRecipientDTO recipientDto = mock(GiftRecipientDTO.class);
        final GiftRecipientModel recipientModel = mock(GiftRecipientModel.class);
        given(giftRecipientFormConverter.convert(recipientDto)).willReturn(recipientModel);
        targetCommerceCartService.updateGiftcardRecipientDetails(cart, product, 0, PK_STR, recipientDto);
    }

    @Test(expected = CommerceCartModificationException.class)
    public void testUpdateRecipientDetailsEssentialCheckRecipientEntryEmpty() throws CommerceCartModificationException {
        given(product.getVariants()).willReturn(Collections.singletonList(product1));
        given(targetLaybyCartService.getEntryForProduct(cart, product)).willReturn(cartEntry);
        willReturn(Boolean.TRUE).given(giftCardService).isProductAGiftCard(product);
        final List<GiftRecipientModel> recipients = new ArrayList<>();
        given(cartEntry.getGiftRecipients()).willReturn(recipients);
        final GiftRecipientDTO recipientDto = mock(GiftRecipientDTO.class);
        final GiftRecipientModel recipientModel = mock(GiftRecipientModel.class);
        given(giftRecipientFormConverter.convert(recipientDto)).willReturn(recipientModel);
        targetCommerceCartService.updateGiftcardRecipientDetails(cart, product, 0, PK_STR, recipientDto);
    }

    @Test
    public void testUpdateGiftCardRecipientDetails() throws CommerceCartModificationException {
        given(product.getVariants()).willReturn(Collections.singletonList(product1));
        given(targetLaybyCartService.getEntryForProduct(cart, product)).willReturn(cartEntry);
        willReturn(Boolean.TRUE).given(giftCardService).isProductAGiftCard(product);
        given(giftRecipientModel1.getPk()).willReturn(PK.parse(PK_STR));
        final List<GiftRecipientModel> recipients = new ArrayList<>();
        recipients.add(giftRecipientModel1);
        given(cartEntry.getGiftRecipients()).willReturn(recipients);
        final GiftRecipientDTO recipientDto = mock(GiftRecipientDTO.class);
        final CommerceCartModification cartModification = targetCommerceCartService.updateGiftcardRecipientDetails(
                cart, product, 0, PK_STR, recipientDto);
        assertThat(cartModification).isNotNull();
        assertThat(cartModification.getStatusCode()).isNotNull().isNotEmpty()
                .isEqualTo(TargetCommerceCartModificationStatus.SUCCESS);
        verify(giftRecipientFormConverter).update(giftRecipientModel1, recipientDto);
    }

    @Test
    public void testIsCartWithinAfterpayThresholdsWhenCartIs50Dollars() {
        assertThat(targetCommerceCartService.isCartWithinAfterpayThresholds(new BigDecimal("50"))).isTrue();
    }

    @Test
    public void testIsCartWithinAfterpayThresholdsWhenCartIs1000Dollars() {
        assertThat(targetCommerceCartService.isCartWithinAfterpayThresholds(new BigDecimal("1000"))).isTrue();
    }

    @Test
    public void testIsCartWithinAfterpayThresholdsWhenCartIsOverMaxLimit() {
        assertThat(targetCommerceCartService.isCartWithinAfterpayThresholds(new BigDecimal("1200"))).isFalse();
    }

    @Test
    public void testIsCartWithinAfterpayThresholdsWhenCartIsOverUnderMinLimit() {
        assertThat(targetCommerceCartService.isCartWithinAfterpayThresholds(new BigDecimal("49"))).isFalse();
    }

    @Test
    public void testIsCartWithinAfterpayThresholdsForDecimals() {
        // when cart total is 49.99
        assertThat(targetCommerceCartService.isCartWithinAfterpayThresholds(new BigDecimal("49.99"))).isFalse();
        // when cart total is 999.99
        assertThat(targetCommerceCartService.isCartWithinAfterpayThresholds(new BigDecimal("999.99"))).isTrue();
    }

    @Test
    public void testIsExcludedForAfterpayWithNullCart() {
        final boolean result = targetCommerceCartService.isExcludedForAfterpay(null);
        assertThat(result).isFalse();
    }

    @Test
    public void testIsExcludedForAfterpayWithEmptyCart() {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        given(cart.getEntries()).willReturn(entries);
        final boolean result = targetCommerceCartService.isExcludedForAfterpay(cart);
        assertThat(result).isFalse();
    }

    @Test
    public void testIsExcludedForAfterpayWithNoExcludedVarientProduct() {
        final AbstractOrderEntryModel entryModel1 = mock(AbstractOrderEntryModel.class);
        final TargetSizeVariantProductModel entryProduct1 = mock(TargetSizeVariantProductModel.class);
        final TargetColourVariantProductModel coulourProduct1 = mock(TargetColourVariantProductModel.class);
        given(Boolean.valueOf(coulourProduct1.isExcludeForAfterpay())).willReturn(Boolean.FALSE);
        given(entryProduct1.getBaseProduct()).willReturn(coulourProduct1);
        given(entryModel1.getProduct()).willReturn(entryProduct1);

        final AbstractOrderEntryModel entryModel2 = mock(AbstractOrderEntryModel.class);
        final TargetColourVariantProductModel entryProduct2 = mock(TargetColourVariantProductModel.class);
        given(Boolean.valueOf(entryProduct2.isExcludeForAfterpay())).willReturn(Boolean.FALSE);
        given(entryModel2.getProduct()).willReturn(entryProduct2);

        final AbstractOrderEntryModel entryModel3 = mock(AbstractOrderEntryModel.class);
        final TargetColourVariantProductModel entryProduct3 = mock(TargetColourVariantProductModel.class);
        given(Boolean.valueOf(entryProduct3.isExcludeForAfterpay())).willReturn(Boolean.FALSE);
        given(entryModel3.getProduct()).willReturn(entryProduct3);

        final List<AbstractOrderEntryModel> entries = Arrays.asList(entryModel1, entryModel2, entryModel3);
        given(cart.getEntries()).willReturn(entries);
        final boolean result = targetCommerceCartService.isExcludedForAfterpay(cart);
        assertThat(result).isFalse();
    }

    @Test
    public void testIsExcludedForAfterpayWithExcludedVarientProduct() {
        final AbstractOrderEntryModel entryModel1 = mock(AbstractOrderEntryModel.class);
        final TargetSizeVariantProductModel entryProduct1 = mock(TargetSizeVariantProductModel.class);
        final TargetColourVariantProductModel coulourProduct1 = mock(TargetColourVariantProductModel.class);
        given(Boolean.valueOf(coulourProduct1.isExcludeForAfterpay())).willReturn(Boolean.TRUE);
        given(entryProduct1.getBaseProduct()).willReturn(coulourProduct1);
        given(entryModel1.getProduct()).willReturn(entryProduct1);

        final AbstractOrderEntryModel entryModel2 = mock(AbstractOrderEntryModel.class);
        final TargetColourVariantProductModel entryProduct2 = mock(TargetColourVariantProductModel.class);
        given(Boolean.valueOf(entryProduct2.isExcludeForAfterpay())).willReturn(Boolean.FALSE);
        given(entryModel2.getProduct()).willReturn(entryProduct2);

        final AbstractOrderEntryModel entryModel3 = mock(AbstractOrderEntryModel.class);
        final TargetColourVariantProductModel entryProduct3 = mock(TargetColourVariantProductModel.class);
        given(Boolean.valueOf(entryProduct3.isExcludeForAfterpay())).willReturn(Boolean.FALSE);
        given(entryModel3.getProduct()).willReturn(entryProduct3);

        final List<AbstractOrderEntryModel> entries = Arrays.asList(entryModel1, entryModel2, entryModel3);
        given(cart.getEntries()).willReturn(entries);
        final boolean result = targetCommerceCartService.isExcludedForAfterpay(cart);
        assertThat(result).isTrue();
    }



    /**
     * Empty cart and tryring to add a product which has no stock for nomal prd The status will be NO_STOCK
     * 
     * @throws CommerceCartModificationException
     */
    @Test
    public void testUpdateEntryWhenNewQuanityIsZeroForNormalPrd()
            throws CommerceCartModificationException {
        final TargetSizeVariantProductModel sizeProduct = mock(TargetSizeVariantProductModel.class);
        final TargetColourVariantProductModel colourVariant = mock(TargetColourVariantProductModel.class);
        given(sizeProduct.getBaseProduct()).willReturn(colourVariant);
        given(colourVariant.getPreOrderStartDateTime()).willReturn(null);
        given(colourVariant.getPreOrderEndDateTime()).willReturn(null);
        willReturn(Integer.valueOf(0)).given(targetPreOrderService)
                .getAvailablePreOrderQuantityForCurrentProduct(sizeProduct);
        given(sizeProduct.getMaxOrderQuantity()).willReturn(Integer.valueOf(10));
        given(cart.getEntries()).willReturn(Collections.EMPTY_LIST);
        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart, sizeProduct, 4,
                unitModel);
        assertThat(cartModification.getStatusCode())
                .isEqualTo(TargetCommerceCartModificationStatus.NO_STOCK);
    }

    /**
     * Empty cart and tryring to add a product which has no stock for preoder product The status will be
     * preOrder.noStock
     * 
     * @throws CommerceCartModificationException
     */
    @Test
    public void testUpdateEntryWhenNewQuanityIsZeroForPreOrder()
            throws CommerceCartModificationException {
        final TargetSizeVariantProductModel sizeProduct = mock(TargetSizeVariantProductModel.class);
        final TargetColourVariantProductModel colourVariant = mock(TargetColourVariantProductModel.class);
        given(sizeProduct.getBaseProduct()).willReturn(colourVariant);
        final Calendar start = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), Calendar.MONTH - 1, 15, 0, 0, 0);
        final Calendar end = Calendar.getInstance();
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 2, 22, 0, 0, 0);
        given(colourVariant.getPreOrderStartDateTime()).willReturn(start.getTime());
        given(colourVariant.getPreOrderEndDateTime()).willReturn(end.getTime());
        willReturn(Integer.valueOf(0)).given(targetPreOrderService)
                .getAvailablePreOrderQuantityForCurrentProduct(sizeProduct);
        given(sizeProduct.getMaxOrderQuantity()).willReturn(Integer.valueOf(10));
        given(cart.getEntries()).willReturn(Collections.EMPTY_LIST);
        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart, sizeProduct, 4,
                unitModel);
        assertThat(cartModification.getStatusCode())
                .isEqualTo(PREORDER_PREFIX + TargetCommerceCartModificationStatus.NO_STOCK);
    }

    @Test
    public void testAddToCartPreOrderWithQuantityMoreThanGlobalThreshold() throws CommerceCartModificationException {
        final TargetSizeVariantProductModel sizeProduct = mock(TargetSizeVariantProductModel.class);
        final TargetColourVariantProductModel colourVariant = mock(TargetColourVariantProductModel.class);
        given(sizeProduct.getBaseProduct()).willReturn(colourVariant);
        given(cart.getContainsPreOrderItems()).willReturn(Boolean.TRUE);
        final Calendar start = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), Calendar.MONTH - 1, 15, 0, 0, 0);

        final Calendar end = Calendar.getInstance();
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 2, 22, 0, 0, 0);

        given(colourVariant.getPreOrderStartDateTime()).willReturn(start.getTime());
        given(colourVariant.getPreOrderEndDateTime()).willReturn(end.getTime());
        willReturn(Integer.valueOf(150)).given(targetPreOrderService)
                .getAvailablePreOrderQuantityForCurrentProduct(sizeProduct);
        given(sizeProduct.getMaxOrderQuantity()).willReturn(Integer.valueOf(10));

        given(cart.getEntries()).willReturn(Collections.EMPTY_LIST);
        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart, sizeProduct, 3,
                unitModel);
        assertThat(cartModification.getStatusCode())
                .isEqualTo(PREORDER_PREFIX + TargetCommerceCartModificationStatus.LOW_STOCK);
    }


    @Test
    public void testAddToCartPreOrderWithQuantityMoreThanMaxPreOrderAvailableQuantity()
            throws CommerceCartModificationException {
        final TargetSizeVariantProductModel sizeProduct = mock(TargetSizeVariantProductModel.class);
        final TargetColourVariantProductModel colourVariant = mock(TargetColourVariantProductModel.class);
        given(sizeProduct.getBaseProduct()).willReturn(colourVariant);
        given(cart.getContainsPreOrderItems()).willReturn(Boolean.TRUE);

        final Calendar start = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), Calendar.MONTH - 1, 15, 0, 0, 0);

        final Calendar end = Calendar.getInstance();
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 2, 22, 0, 0, 0);

        given(colourVariant.getPreOrderStartDateTime()).willReturn(start.getTime());
        given(colourVariant.getPreOrderEndDateTime()).willReturn(end.getTime());
        willReturn(Integer.valueOf(2)).given(targetPreOrderService)
                .getAvailablePreOrderQuantityForCurrentProduct(sizeProduct);
        given(sizeProduct.getMaxOrderQuantity()).willReturn(Integer.valueOf(10));

        given(cart.getEntries()).willReturn(Collections.EMPTY_LIST);
        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart, sizeProduct, 3,
                unitModel);
        assertThat(cartModification.getStatusCode())
                .isEqualTo(PREORDER_PREFIX + TargetCommerceCartModificationStatus.LOW_STOCK);
    }

    @Test
    public void testAddToCartPreOrderWithQuantityLessThanGlobalThreshold() throws CommerceCartModificationException {
        final TargetSizeVariantProductModel sizeProduct = mock(TargetSizeVariantProductModel.class);
        final TargetColourVariantProductModel colourVariant = mock(TargetColourVariantProductModel.class);
        given(sizeProduct.getBaseProduct()).willReturn(colourVariant);
        given(cart.getContainsPreOrderItems()).willReturn(Boolean.TRUE);

        final Calendar start = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), Calendar.MONTH - 1, 15, 0, 0, 0);

        final Calendar end = Calendar.getInstance();
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 2, 22, 0, 0, 0);

        given(colourVariant.getPreOrderStartDateTime()).willReturn(start.getTime());
        given(colourVariant.getPreOrderEndDateTime()).willReturn(end.getTime());
        willReturn(Integer.valueOf(150)).given(targetPreOrderService)
                .getAvailablePreOrderQuantityForCurrentProduct(sizeProduct);
        given(sizeProduct.getMaxOrderQuantity()).willReturn(Integer.valueOf(10));

        given(cart.getEntries()).willReturn(Collections.EMPTY_LIST);
        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart, sizeProduct, 1,
                unitModel);
        assertThat(cartModification.getStatusCode())
                .isEqualTo(TargetCommerceCartModificationStatus.SUCCESS);
    }

    @Test
    public void testAddToCartAssertStockCheckOnce() throws CommerceCartModificationException {
        final long quantityToAdd = STOCK_QUANTITY.longValue();
        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart, product2,
                quantityToAdd, unitModel);
        assertThat(cartModification.getQuantityAdded()).isEqualTo(quantityToAdd);
        verify(targetStockService).getStockLevelAmountFromOnlineWarehouses(product2);
    }

    @Test(expected = CommerceCartModificationException.class)
    public void testPreOrderChecksWhenEmbargoProductButNoStartDate() throws CommerceCartModificationException {

        final TargetColourVariantProductModel productModel = mock(TargetColourVariantProductModel.class);
        final Calendar embargoDate = Calendar.getInstance();
        // future date
        embargoDate.set(embargoDate.get(Calendar.YEAR), embargoDate.get(Calendar.MONTH) + 2, 22, 0, 0, 0);
        given(productModel.getNormalSaleStartDateTime()).willReturn(embargoDate.getTime());
        given(productModel.getPreOrderStartDateTime()).willReturn(null);

        try {
            targetCommerceCartService.preOrderChecks(productModel);
        }
        catch (final CommerceCartModificationException e) {
            assertThat(e.getMessage()).isEqualTo(TgtCoreConstants.CART.ERR_PREORDER_CART);
            verify(productModel).getPreOrderStartDateTime();
            verify(targetSalesApplicationService).getCurrentSalesApplication();
            throw e;
        }
    }

    @Test(expected = CommerceCartModificationException.class)
    public void testPreOrderChecksWhenEmbargoProductButNoEndDate() throws CommerceCartModificationException {

        final TargetColourVariantProductModel productModel = mock(TargetColourVariantProductModel.class);
        final Calendar embargoDate = Calendar.getInstance();
        embargoDate.set(embargoDate.get(Calendar.YEAR), embargoDate.get(Calendar.MONTH) + 2, 22, 0, 0, 0);

        given(productModel.getNormalSaleStartDateTime()).willReturn(embargoDate.getTime());
        given(productModel.getPreOrderStartDateTime()).willReturn(new Date());
        given(productModel.getPreOrderEndDateTime()).willReturn(null);

        try {
            targetCommerceCartService.preOrderChecks(productModel);
        }
        catch (final CommerceCartModificationException e) {
            assertThat(e.getMessage()).isEqualTo(TgtCoreConstants.CART.ERR_PREORDER_CART);
            verify(productModel).getPreOrderStartDateTime();
            verify(productModel).getPreOrderEndDateTime();
            verify(targetSalesApplicationService).getCurrentSalesApplication();
            throw e;
        }


    }

    @Test(expected = CommerceCartModificationException.class)
    public void testPreOrderChecksWhenEmbargoProductButStartDateAfterTodaysDate()
            throws CommerceCartModificationException {

        final TargetColourVariantProductModel productModel = mock(TargetColourVariantProductModel.class);
        final Calendar start = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), start.get(Calendar.MONTH) + 2, 22, 0, 0, 0);
        final Calendar embargoDate = Calendar.getInstance();
        embargoDate.set(embargoDate.get(Calendar.YEAR), embargoDate.get(Calendar.MONTH) + 2, 22, 0, 0, 0);

        given(productModel.getPreOrderStartDateTime()).willReturn(start.getTime());
        given(productModel.getPreOrderEndDateTime()).willReturn(new Date());
        given(productModel.getNormalSaleStartDateTime()).willReturn(embargoDate.getTime());

        try {
            targetCommerceCartService.preOrderChecks(productModel);
        }
        catch (final CommerceCartModificationException e) {
            assertThat(e.getMessage()).isEqualTo(TgtCoreConstants.CART.ERR_PREORDER_CART);
            verify(productModel, times(2)).getPreOrderStartDateTime();
            verify(productModel, times(2)).getPreOrderEndDateTime();
            verify(targetSalesApplicationService).getCurrentSalesApplication();
            throw e;
        }

    }

    @Test(expected = CommerceCartModificationException.class)
    public void testPreOrderChecksWhenEmbargoProductButEndDateHasPassed()
            throws CommerceCartModificationException {

        final TargetColourVariantProductModel productModel = mock(TargetColourVariantProductModel.class);
        final Calendar end = Calendar.getInstance();
        final Calendar embargoDate = Calendar.getInstance();
        embargoDate.set(embargoDate.get(Calendar.YEAR), embargoDate.get(Calendar.MONTH) + 2, 22, 0, 0, 0);
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) - 2, 22, 0, 0, 0);

        given(productModel.getPreOrderStartDateTime()).willReturn(new Date());
        given(productModel.getPreOrderEndDateTime()).willReturn(end.getTime());
        given(productModel.getNormalSaleStartDateTime()).willReturn(embargoDate.getTime());

        try {
            targetCommerceCartService.preOrderChecks(productModel);
        }
        catch (final CommerceCartModificationException e) {
            assertThat(e.getMessage()).isEqualTo(TgtCoreConstants.CART.ERR_PREORDER_CART);
            verify(productModel, times(2)).getPreOrderStartDateTime();
            verify(productModel, times(2)).getPreOrderEndDateTime();
            verify(targetSalesApplicationService).getCurrentSalesApplication();

            throw e;
        }


    }

    @Test(expected = CommerceCartModificationException.class)
    public void testPreOrderChecksWhenValidPreOrderButSalesApplicationKiosk()
            throws CommerceCartModificationException {

        final TargetColourVariantProductModel productModel = mock(TargetColourVariantProductModel.class);
        final Calendar end = Calendar.getInstance();
        final Calendar embargoDate = Calendar.getInstance();
        embargoDate.set(embargoDate.get(Calendar.YEAR), embargoDate.get(Calendar.MONTH) + 2, 22, 0, 0, 0);
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 2, 22, 0, 0, 0);

        given(productModel.getPreOrderStartDateTime()).willReturn(new Date());
        given(productModel.getPreOrderEndDateTime()).willReturn(end.getTime());
        given(productModel.getNormalSaleStartDateTime()).willReturn(embargoDate.getTime());
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.KIOSK);

        try {
            targetCommerceCartService.preOrderChecks(productModel);
        }
        catch (final CommerceCartModificationException e) {
            assertThat(e.getMessage()).isEqualTo(TgtCoreConstants.CART.ERR_PREORDER_CART);
            verify(targetSalesApplicationService).getCurrentSalesApplication();
            throw e;
        }

    }

    @Test
    public void testPreOrderChecksProductNotEmbargoed()
            throws CommerceCartModificationException {

        final TargetColourVariantProductModel productModel = mock(TargetColourVariantProductModel.class);

        targetCommerceCartService.preOrderChecks(productModel);

        verifyZeroInteractions(targetSalesApplicationService);
    }

    @Test
    public void testPreOrderChecksProductEmbargoedAndValidPreOrderProductSalesChannelWeb()
            throws CommerceCartModificationException {

        final TargetColourVariantProductModel productModel = createPreOrderProduct();
        final Calendar embargoDate = Calendar.getInstance();
        embargoDate.set(embargoDate.get(Calendar.YEAR), embargoDate.get(Calendar.MONTH) + 2, 22, 0, 0, 0);
        given(productModel.getNormalSaleStartDateTime()).willReturn(embargoDate.getTime());
        given(targetSalesApplicationService.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);

        targetCommerceCartService.preOrderChecks(productModel);

        verify(targetSalesApplicationService).getCurrentSalesApplication();
        verify(productModel, times(2)).getPreOrderStartDateTime();
        verify(productModel, times(2)).getPreOrderEndDateTime();
    }

    /**
     * @return {@link TargetColourVariantProductModel}
     */
    private TargetColourVariantProductModel createPreOrderProduct() {
        final TargetColourVariantProductModel productModel = mock(TargetColourVariantProductModel.class);

        final Calendar end = Calendar.getInstance();
        end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH) + 2, 22, 0, 0, 0);

        final Calendar start = Calendar.getInstance();
        start.set(start.get(Calendar.YEAR), start.get(Calendar.MONTH) - 2, 22, 0, 0, 0);

        final Calendar embargo = Calendar.getInstance();
        embargo.set(embargo.get(Calendar.YEAR), embargo.get(Calendar.MONTH) + 2, 25, 0, 0, 0);

        given(productModel.getPreOrderStartDateTime()).willReturn(start.getTime());
        given(productModel.getPreOrderEndDateTime()).willReturn(end.getTime());
        given(productModel.getNormalSaleStartDateTime()).willReturn(embargo.getTime());
        return productModel;
    }

    @Test
    public void testPreOrderMixedBasketNonEmptyBasketAndPreOrderProductAdded()
            throws CommerceCartModificationException {


        final AbstractOrderEntryModel entryModel1 = mock(AbstractOrderEntryModel.class);
        final TargetSizeVariantProductModel entryProduct1 = mock(TargetSizeVariantProductModel.class);
        final TargetColourVariantProductModel coulourProduct1 = mock(TargetColourVariantProductModel.class);
        given(entryProduct1.getBaseProduct()).willReturn(coulourProduct1);
        given(entryModel1.getProduct()).willReturn(entryProduct1);

        final AbstractOrderEntryModel entryModel2 = mock(AbstractOrderEntryModel.class);
        final TargetColourVariantProductModel entryProduct2 = mock(TargetColourVariantProductModel.class);
        given(entryModel2.getProduct()).willReturn(entryProduct2);

        final List<AbstractOrderEntryModel> entries = Arrays.asList(entryModel1, entryModel2);

        final TargetColourVariantProductModel productModel = createPreOrderProduct();
        given(cart.getEntries()).willReturn(entries);
        given(targetLaybyCartService.getEntriesForProduct(cart, productModel))
                .willReturn(Collections.EMPTY_LIST);

        assertThat(targetCommerceCartService.isPreOrderMixedBasket(cart, productModel)).isTrue();

        verify(targetLaybyCartService).getEntriesForProduct(cart, productModel);
        verifyZeroInteractions(targetPreOrderService);
    }

    @Test
    public void testPreOrderMixedBasketNonEmptyWithPreOrderAndDifferentPreOrderProductAdded()
            throws CommerceCartModificationException {


        final AbstractOrderEntryModel entryModel1 = mock(AbstractOrderEntryModel.class);
        final TargetSizeVariantProductModel entryProduct1 = mock(TargetSizeVariantProductModel.class);
        final TargetColourVariantProductModel coulourProduct1 = mock(TargetColourVariantProductModel.class);
        given(entryProduct1.getBaseProduct()).willReturn(coulourProduct1);
        given(entryModel1.getProduct()).willReturn(entryProduct1);

        final AbstractOrderEntryModel entryModel2 = mock(AbstractOrderEntryModel.class);
        final TargetColourVariantProductModel entryProduct2 = createPreOrderProduct();
        given(entryModel2.getProduct()).willReturn(entryProduct2);

        final List<AbstractOrderEntryModel> entries = Arrays.asList(entryModel1, entryModel2);

        final TargetColourVariantProductModel productModel = createPreOrderProduct();
        given(cart.getEntries()).willReturn(entries);
        given(targetLaybyCartService.getEntriesForProduct(cart, productModel))
                .willReturn(Collections.EMPTY_LIST);

        assertThat(targetCommerceCartService.isPreOrderMixedBasket(cart, productModel)).isTrue();

        verify(targetLaybyCartService).getEntriesForProduct(cart, productModel);
        verify(targetPreOrderService, never()).doesCartHavePreOrderProduct(cart);
    }

    @Test
    public void testPreOrderMixedBasketNonEmptyWithPreOrderAndSamePreOrderProductAdded()
            throws CommerceCartModificationException {

        final AbstractOrderEntryModel entryModel1 = mock(AbstractOrderEntryModel.class);
        final TargetSizeVariantProductModel entryProduct1 = mock(TargetSizeVariantProductModel.class);
        final TargetColourVariantProductModel coulourProduct1 = mock(TargetColourVariantProductModel.class);
        given(entryProduct1.getBaseProduct()).willReturn(coulourProduct1);
        given(entryModel1.getProduct()).willReturn(entryProduct1);

        final AbstractOrderEntryModel entryModel2 = mock(AbstractOrderEntryModel.class);
        final TargetColourVariantProductModel entryProduct2 = createPreOrderProduct();
        given(entryModel2.getProduct()).willReturn(entryProduct2);

        final List<AbstractOrderEntryModel> entries = Arrays.asList(entryModel1, entryModel2);

        given(cart.getEntries()).willReturn(entries);
        given(targetLaybyCartService.getEntriesForProduct(cart, entryProduct2))
                .willReturn(Collections.singletonList(cartEntry));

        assertThat(targetCommerceCartService.isPreOrderMixedBasket(cart, entryProduct2)).isFalse();

        verify(targetLaybyCartService).getEntriesForProduct(cart, entryProduct2);
        verify(targetPreOrderService, never()).doesCartHavePreOrderProduct(cart);
    }

    @Test
    public void testPreOrderMixedBasketWithEmptyBasket() throws CommerceCartModificationException {

        final TargetColourVariantProductModel productModel = createPreOrderProduct();
        given(cart.getEntries()).willReturn(Collections.EMPTY_LIST);
        given(targetLaybyCartService.getEntriesForProduct(cart, productModel))
                .willReturn(Collections.singletonList(cartEntry));

        assertThat(targetCommerceCartService.isPreOrderMixedBasket(cart, productModel)).isFalse();

        verifyZeroInteractions(targetLaybyCartService);
        verify(targetPreOrderService, never()).doesCartHavePreOrderProduct(cart);
    }

    @Test
    public void testPreOrderMixedBasketNonEmptyWithNoPreOrderAndNormalProductAdded()
            throws CommerceCartModificationException {


        final AbstractOrderEntryModel entryModel1 = mock(AbstractOrderEntryModel.class);
        final TargetSizeVariantProductModel entryProduct1 = mock(TargetSizeVariantProductModel.class);
        final TargetColourVariantProductModel coulourProduct1 = mock(TargetColourVariantProductModel.class);
        given(entryProduct1.getBaseProduct()).willReturn(coulourProduct1);
        given(entryModel1.getProduct()).willReturn(entryProduct1);

        final AbstractOrderEntryModel entryModel2 = mock(AbstractOrderEntryModel.class);
        final TargetColourVariantProductModel entryProduct2 = mock(TargetColourVariantProductModel.class);
        given(entryModel2.getProduct()).willReturn(entryProduct2);

        final List<AbstractOrderEntryModel> entries = Arrays.asList(entryModel1, entryModel2);

        final TargetColourVariantProductModel productModel = mock(TargetColourVariantProductModel.class);
        given(cart.getEntries()).willReturn(entries);
        willReturn(Boolean.FALSE).given(targetPreOrderService).doesCartHavePreOrderProduct(cart);

        assertThat(targetCommerceCartService.isPreOrderMixedBasket(cart, productModel)).isFalse();

        verifyZeroInteractions(targetLaybyCartService);
        verify(targetPreOrderService).doesCartHavePreOrderProduct(cart);
    }

    @Test
    public void testPreOrderMixedBasketNonEmptyWithPreOrderAndNormalProductAdded()
            throws CommerceCartModificationException {


        final AbstractOrderEntryModel entryModel1 = mock(AbstractOrderEntryModel.class);
        final TargetSizeVariantProductModel entryProduct1 = mock(TargetSizeVariantProductModel.class);
        final TargetColourVariantProductModel coulourProduct1 = mock(TargetColourVariantProductModel.class);
        given(entryProduct1.getBaseProduct()).willReturn(coulourProduct1);
        given(entryModel1.getProduct()).willReturn(entryProduct1);

        final AbstractOrderEntryModel entryModel2 = mock(AbstractOrderEntryModel.class);
        final TargetColourVariantProductModel entryProduct2 = createPreOrderProduct();
        given(entryModel2.getProduct()).willReturn(entryProduct2);

        final List<AbstractOrderEntryModel> entries = Arrays.asList(entryModel1, entryModel2);

        final TargetColourVariantProductModel productModel = mock(TargetColourVariantProductModel.class);
        given(cart.getEntries()).willReturn(entries);
        willReturn(Boolean.TRUE).given(targetPreOrderService).doesCartHavePreOrderProduct(cart);

        assertThat(targetCommerceCartService.isPreOrderMixedBasket(cart, productModel)).isTrue();
        verifyZeroInteractions(targetLaybyCartService);
        verify(targetPreOrderService).doesCartHavePreOrderProduct(cart);
    }

    /**
     * Method to verify if pre-order item is present.
     */
    @Test
    public void testIsExcludedForAfterpayVerifyPreOrderItemsPresent() {
        willReturn(Boolean.TRUE).given(cart).getContainsPreOrderItems();
        final boolean result = targetCommerceCartService.isExcludedForAfterpay(cart);

        assertThat(result).isTrue();
    }

    /**
     * Method to verify if pre-order item is not present and no order entries.
     */
    @Test
    public void testIsExcludedForAfterpayVerifyPreOrderItemNotPresentWithEmptyBasket() {
        willReturn(Boolean.FALSE).given(cart).getContainsPreOrderItems();

        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        given(cart.getEntries()).willReturn(entries);

        final boolean result = targetCommerceCartService.isExcludedForAfterpay(cart);

        assertThat(result).isFalse();
    }


    private void setupCartEntriesForDeliveryModeTests() {

        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        entries.add(cartEntryForDelivery1);
        entries.add(cartEntryForDelivery2);
        given(cart.getEntries()).willReturn(entries);

        given(cartEntryForDelivery1.getProduct()).willReturn(product1);
        given(cartEntryForDelivery2.getProduct()).willReturn(product2);
    }

    private void setupDeliveryServiceToReturnTwoSupportedModes() {
        // Set up deliveryService for 2 delivery modes available
        final List<DeliveryModeModel> deliveryModeList = new ArrayList<>();
        deliveryModeList.add(deliveryModeModel1);
        deliveryModeList.add(deliveryModeModel2);

        given(targetDeliveryService.getSupportedDeliveryModeListForOrder(cart)).willReturn(deliveryModeList);

    }

    private void setupMockCartDeliveryFee() {
        given(findDeliveryCostStrategy.getDeliveryCost(cart)).willReturn(feePriceValue);
    }

    private void setUpAfterpayConfigModel() {
        final AfterpayConfigModel afterPayModel = new AfterpayConfigModel();
        afterPayModel.setMaximumAmount(new BigDecimal("1000"));
        afterPayModel.setMinimumAmount(new BigDecimal("50"));
        given(afterpayConfigService.getAfterpayConfig()).willReturn(afterPayModel);
    }

    @Test
    public void testAddToCartPreOrderWithQuantityMoreThanItemsThreshold()
            throws CommerceCartModificationException {

        final TargetSizeVariantProductModel sizeProduct = mock(TargetSizeVariantProductModel.class);
        final TargetColourVariantProductModel colourVariant = createPreOrderProduct();
        given(sizeProduct.getBaseProduct()).willReturn(colourVariant);
        willReturn(Boolean.TRUE).given(cart).getContainsPreOrderItems();

        final CartEntryModel entryModel = mock(CartEntryModel.class);
        final TargetSizeVariantProductModel entryProduct = mock(TargetSizeVariantProductModel.class);
        given(entryProduct.getBaseProduct()).willReturn(sizeProduct);
        given(entryModel.getProduct()).willReturn(entryProduct);
        given(entryModel.getQuantity()).willReturn(Long.valueOf(2L));

        final CartEntryModel entryModel2 = mock(CartEntryModel.class);
        final TargetSizeVariantProductModel entryProduct2 = mock(TargetSizeVariantProductModel.class);
        given(entryProduct2.getBaseProduct()).willReturn(sizeProduct);
        given(entryModel2.getProduct()).willReturn(entryProduct2);
        given(entryModel2.getQuantity()).willReturn(Long.valueOf(2L));

        final List<CartEntryModel> entries = Arrays.asList(entryModel, entryModel2);

        willReturn(Integer.valueOf(150)).given(targetPreOrderService)
                .getAvailablePreOrderQuantityForCurrentProduct(sizeProduct);

        willReturn(entries).given(cart).getEntries();
        given(targetLaybyCartService.getEntriesForProduct(cart, sizeProduct))
                .willReturn(Collections.singletonList(entryModel));

        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart, sizeProduct,
                12,
                unitModel);

        assertThat(cartModification.getStatusCode())
                .isEqualTo(PREORDER_PREFIX + TargetCommerceCartModificationStatus.ITEMS_THRESHOLD_REACHED);
    }

    @Test
    public void testAddToCartNormalOrderWithQuantityMoreThanItemsThreshold()
            throws CommerceCartModificationException, FluentClientException {

        final TargetSizeVariantProductModel sizeProduct = mock(TargetSizeVariantProductModel.class);
        final TargetColourVariantProductModel colourVariant = mock(TargetColourVariantProductModel.class);
        given(sizeProduct.getBaseProduct()).willReturn(colourVariant);
        willReturn(Integer.valueOf(99999)).given(targetStockService)
                .getStockLevelAmountFromOnlineWarehouses(sizeProduct);

        final CartEntryModel entryModel = mock(CartEntryModel.class);
        final TargetSizeVariantProductModel entryProduct = mock(TargetSizeVariantProductModel.class);
        given(entryProduct.getBaseProduct()).willReturn(sizeProduct);
        given(entryModel.getProduct()).willReturn(entryProduct);
        given(entryModel.getQuantity()).willReturn(Long.valueOf(2L));

        final CartEntryModel entryModel2 = mock(CartEntryModel.class);
        final TargetSizeVariantProductModel entryProduct2 = mock(TargetSizeVariantProductModel.class);
        given(entryProduct2.getBaseProduct()).willReturn(sizeProduct);
        given(entryModel2.getProduct()).willReturn(entryProduct2);
        given(entryModel2.getQuantity()).willReturn(Long.valueOf(2L));

        final List<CartEntryModel> entries = Arrays.asList(entryModel, entryModel2);

        willReturn(entries).given(cart).getEntries();
        given(targetLaybyCartService.getEntriesForProduct(cart, sizeProduct))
                .willReturn(Collections.singletonList(entryModel));

        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart, sizeProduct,
                12,
                unitModel);

        assertThat(cartModification.getStatusCode())
                .isEqualTo(TargetCommerceCartModificationStatus.ITEMS_THRESHOLD_REACHED);
    }

    @Test
    public void testAddToCartPreOrderWithQuantityMoreThanAvailableStock()
            throws CommerceCartModificationException {

        final TargetSizeVariantProductModel sizeProduct = mock(TargetSizeVariantProductModel.class);
        final TargetColourVariantProductModel colourVariant = createPreOrderProduct();
        given(sizeProduct.getBaseProduct()).willReturn(colourVariant);
        willReturn(Boolean.TRUE).given(cart).getContainsPreOrderItems();

        final CartEntryModel entryModel = mock(CartEntryModel.class);
        final TargetSizeVariantProductModel entryProduct = mock(TargetSizeVariantProductModel.class);
        given(entryProduct.getBaseProduct()).willReturn(sizeProduct);
        given(entryModel.getProduct()).willReturn(entryProduct);

        final CartEntryModel entryModel2 = mock(CartEntryModel.class);
        final TargetSizeVariantProductModel entryProduct2 = mock(TargetSizeVariantProductModel.class);
        given(entryProduct2.getBaseProduct()).willReturn(sizeProduct);
        given(entryModel2.getProduct()).willReturn(entryProduct2);

        final List<CartEntryModel> entries = Arrays.asList(entryModel, entryModel2);

        willReturn(Integer.valueOf(0)).given(targetPreOrderService)
                .getAvailablePreOrderQuantityForCurrentProduct(sizeProduct);

        willReturn(entries).given(cart).getEntries();
        given(targetLaybyCartService.getEntriesForProduct(cart, sizeProduct))
                .willReturn(Collections.singletonList(entryModel));

        final CommerceCartModification cartModification = targetCommerceCartService.addToCart(cart, sizeProduct,
                12,
                unitModel);

        assertThat(cartModification.getStatusCode())
                .isEqualTo(PREORDER_PREFIX + TargetCommerceCartModificationStatus.NO_STOCK);
    }

    @Test
    public void testIsExcludedForZipWithEmptyCart() {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        given(cart.getEntries()).willReturn(entries);
        final boolean result = targetCommerceCartService.isExcludedForZip(cart);
        assertThat(result).isFalse();
    }

    @Test
    public void testIsExcludedForzipWithNullCart() {
        final boolean result = targetCommerceCartService.isExcludedForZip(null);
        assertThat(result).isFalse();
    }

    @Test
    public void testIsExcludedForZipWithExcludedVarientProduct() {
        final AbstractOrderEntryModel entryModel1 = mock(AbstractOrderEntryModel.class);
        final TargetSizeVariantProductModel entryProduct1 = mock(TargetSizeVariantProductModel.class);
        final TargetColourVariantProductModel coulourProduct1 = mock(TargetColourVariantProductModel.class);
        given(Boolean.valueOf(coulourProduct1.isExcludeForZipPayment())).willReturn(Boolean.TRUE);
        given(entryProduct1.getBaseProduct()).willReturn(coulourProduct1);
        given(entryModel1.getProduct()).willReturn(entryProduct1);

        final AbstractOrderEntryModel entryModel2 = mock(AbstractOrderEntryModel.class);
        final TargetColourVariantProductModel entryProduct2 = mock(TargetColourVariantProductModel.class);
        given(Boolean.valueOf(entryProduct2.isExcludeForZipPayment())).willReturn(Boolean.FALSE);
        given(entryModel2.getProduct()).willReturn(entryProduct2);

        final AbstractOrderEntryModel entryModel3 = mock(AbstractOrderEntryModel.class);
        final TargetColourVariantProductModel entryProduct3 = mock(TargetColourVariantProductModel.class);
        given(Boolean.valueOf(entryProduct3.isExcludeForZipPayment())).willReturn(Boolean.FALSE);
        given(entryModel3.getProduct()).willReturn(entryProduct3);

        final List<AbstractOrderEntryModel> entries = Arrays.asList(entryModel1, entryModel2, entryModel3);
        given(cart.getEntries()).willReturn(entries);
        final boolean result = targetCommerceCartService.isExcludedForZip(cart);
        assertThat(result).isTrue();
    }

    @Test
    public void testIsExcludedForZipWithNoExcludedVarientProduct() {
        final AbstractOrderEntryModel entryModel1 = mock(AbstractOrderEntryModel.class);
        final TargetSizeVariantProductModel entryProduct1 = mock(TargetSizeVariantProductModel.class);
        final TargetColourVariantProductModel coulourProduct1 = mock(TargetColourVariantProductModel.class);
        given(Boolean.valueOf(coulourProduct1.isExcludeForZipPayment())).willReturn(Boolean.FALSE);
        given(entryProduct1.getBaseProduct()).willReturn(coulourProduct1);
        given(entryModel1.getProduct()).willReturn(entryProduct1);

        final AbstractOrderEntryModel entryModel2 = mock(AbstractOrderEntryModel.class);
        final TargetColourVariantProductModel entryProduct2 = mock(TargetColourVariantProductModel.class);
        given(Boolean.valueOf(entryProduct2.isExcludeForZipPayment())).willReturn(Boolean.FALSE);
        given(entryModel2.getProduct()).willReturn(entryProduct2);

        final AbstractOrderEntryModel entryModel3 = mock(AbstractOrderEntryModel.class);
        final TargetColourVariantProductModel entryProduct3 = mock(TargetColourVariantProductModel.class);
        given(Boolean.valueOf(entryProduct3.isExcludeForZipPayment())).willReturn(Boolean.FALSE);
        given(entryModel3.getProduct()).willReturn(entryProduct3);

        final List<AbstractOrderEntryModel> entries = Arrays.asList(entryModel1, entryModel2, entryModel3);
        given(cart.getEntries()).willReturn(entries);
        final boolean result = targetCommerceCartService.isExcludedForZip(cart);
        assertThat(result).isFalse();
    }

    @Test
    public void testIsExcludedForZipVerifyPreOrderItemNotPresentWithEmptyBasket() {
        willReturn(Boolean.FALSE).given(cart).getContainsPreOrderItems();
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        given(cart.getEntries()).willReturn(entries);
        final boolean result = targetCommerceCartService.isExcludedForZip(cart);
        assertThat(result).isFalse();
    }

    @Test
    public void testIsExcludedForZipVerifyPreOrderItemsPresent() {
        willReturn(Boolean.TRUE).given(cart).getContainsPreOrderItems();
        final boolean result = targetCommerceCartService.isExcludedForZip(cart);
        assertThat(result).isTrue();
    }
}

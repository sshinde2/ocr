package au.com.target.tgtlayby.cart.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.order.impl.DefaultCartService;
import de.hybris.platform.servicelayer.session.Session;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtlayby.cart.dao.TargetCartDao;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.purchase.PurchaseOptionService;


/**
 * Unit Test for {@link TargetLaybyCartServiceImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetLaybyCartServiceImplTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @Mock
    private TargetCartEntryService targetCartEntryService;

    @Mock
    private PurchaseOptionService purchaseOptionService;

    @Mock
    private TargetCartDao targetCartDao;

    @InjectMocks
    private final TargetLaybyCartServiceImpl targetLaybyCartService = new TargetLaybyCartServiceImpl();

    @Mock
    private ProductModel product;

    @Mock
    private CartModel cart;

    @Mock
    private PurchaseOptionModel purchaseOption;

    @Mock
    private CartEntryModel cartEntry;

    @Mock
    private UnitModel unitModel;

    @Mock
    private TargetCustomerModel customer;

    @Before
    public void setup() {
        BDDMockito.given(purchaseOptionService.getCurrentPurchaseOption()).willReturn(purchaseOption);
    }

    @Test
    public void testGetEntriesForProductNoResult() {
        BDDMockito.given(targetCartDao.findEntriesForProduct(cart, product)).willReturn(Collections.EMPTY_LIST);
        final CartEntryModel entry = targetLaybyCartService.getEntryForProduct(cart, product);
        Assert.assertNull(entry);
    }

    @Test
    public void testGetEntriesForProduct() {
        final List<CartEntryModel> entryList = new ArrayList<>();
        final CartEntryModel entryModel = new CartEntryModel();
        entryModel.setInfo("test");
        entryList.add(entryModel);

        BDDMockito.given(targetCartDao.findEntriesForProduct(cart, product)).willReturn(entryList);
        final CartEntryModel entry = targetLaybyCartService.getEntryForProduct(cart, product);
        Assert.assertEquals(entry.getInfo(), "test");
    }

    @Test
    public void testGetMostRecentCartForCustomer() {
        BDDMockito.given(targetCartDao.getMostRecentCartForUser(customer)).willReturn(cart);
        final CartModel customerCart = targetLaybyCartService.getMostRecentCartForCustomer(customer);
        Assert.assertNotNull(customerCart);
        Assert.assertEquals(cart, customerCart);
    }

    @Test
    public void testGetMostRecentCartForCustomerWithNoCart() {
        BDDMockito.given(targetCartDao.getMostRecentCartForUser(customer)).willReturn(null);
        final CartModel customerCart = targetLaybyCartService.getMostRecentCartForCustomer(customer);
        Assert.assertNull(customerCart);
    }

    @Test
    public void testGetMostRecentCartForCustomerNullArg() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Parameter customer can not be null");
        targetLaybyCartService.getMostRecentCartForCustomer(null);
    }

    @Test
    public void testGetCurrentSessionCartBySessionWithNullJalo() {
        final HttpSession session = mock(HttpSession.class);
        given(session.getAttribute(TgtCoreConstants.JALO_SESSION)).willReturn(null);
        final CartModel result = targetLaybyCartService.getCurrentSessionCartBySession(session);
        assertThat(result).isNull();
    }

    @Test
    public void testGetCurrentSessionCartBySessionWithHybrisSessionNull() {
        final HttpSession session = mock(HttpSession.class);
        final JaloSession jaloSession = mock(JaloSession.class);
        given(session.getAttribute(TgtCoreConstants.JALO_SESSION)).willReturn(jaloSession);
        given(jaloSession.getAttribute(TgtCoreConstants.SLSESSION)).willReturn(null);
        final CartModel result = targetLaybyCartService.getCurrentSessionCartBySession(session);
        assertThat(result).isNull();
    }

    @Test
    public void testGetCurrentSessionCartBySessionWithCartNull() {
        final HttpSession session = mock(HttpSession.class);
        final JaloSession jaloSession = mock(JaloSession.class);
        final Session hybrisSession = mock(Session.class);
        final CartModel userModel = null;
        given(session.getAttribute(TgtCoreConstants.JALO_SESSION)).willReturn(jaloSession);
        given(jaloSession.getAttribute(TgtCoreConstants.SLSESSION)).willReturn(hybrisSession);
        given(hybrisSession.getAttribute(DefaultCartService.SESSION_CART_PARAMETER_NAME)).willReturn(userModel);
        final CartModel result = targetLaybyCartService.getCurrentSessionCartBySession(session);
        assertThat(result).isNull();
    }

    @Test
    public void testGetCurrentSessionCartBySessionWithCartReturned() {
        final HttpSession session = mock(HttpSession.class);
        final JaloSession jaloSession = mock(JaloSession.class);
        final Session hybrisSession = mock(Session.class);
        final CartModel userModel = mock(CartModel.class);
        given(session.getAttribute(TgtCoreConstants.JALO_SESSION)).willReturn(jaloSession);
        given(jaloSession.getAttribute(TgtCoreConstants.SLSESSION)).willReturn(hybrisSession);
        given(hybrisSession.getAttribute(DefaultCartService.SESSION_CART_PARAMETER_NAME)).willReturn(userModel);
        final CartModel result = targetLaybyCartService.getCurrentSessionCartBySession(session);
        assertThat(result).isEqualTo(userModel);
    }

}

/**
 * 
 */
package au.com.target.tgtlayby.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtlayby.model.PaymentDueConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.purchase.PurchaseOptionConfigService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PurchaseOptionConfigValidatorTest {

    @InjectMocks
    private final PurchaseOptionConfigValidator purchaseOptionConfigValidator = new PurchaseOptionConfigValidator();

    @Mock
    private PurchaseOptionConfigService purchaseOptionConfigService;

    @Mock
    private InterceptorContext interceptorContext;

    @Mock
    private PurchaseOptionConfigModel currentPurchaseOption;

    @Mock
    private PurchaseOptionConfigModel existingPurchaseOption;

    @Mock
    private PurchaseOptionModel purchaseOption;

    @Mock
    private PaymentDueConfigModel initPaymentDue;

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    //CHECKSTYLE:ON

    private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @Before
    public void setup() {
        BDDMockito.given(currentPurchaseOption.getMinimumPaymentDue()).willReturn(initPaymentDue);
        BDDMockito.given(initPaymentDue.getDuePercentage()).willReturn(Double.valueOf(0.0));
    }

    /**
     * This is testing for inner overlapping
     * 
     * @throws InterceptorException
     * @throws ParseException
     * @throws TargetUnknownIdentifierException
     */
    @Test(expected = InterceptorException.class)
    public void testOnValidateOverLapping1() throws InterceptorException, ParseException,
            TargetUnknownIdentifierException {
        final Date from1 = sdf.parse("01/06/2013 12:00:00");
        final Date to1 = sdf.parse("01/07/2013 12:00:00");

        final Date from2 = sdf.parse("10/06/2013 12:00:00");
        final Date to2 = sdf.parse("20/06/2013 12:00:00");

        BDDMockito.given(existingPurchaseOption.getValidFrom()).willReturn(from1);
        BDDMockito.given(existingPurchaseOption.getValidTo()).willReturn(to1);

        BDDMockito.given(currentPurchaseOption.getCode()).willReturn("test-code");
        BDDMockito.given(currentPurchaseOption.getValidFrom()).willReturn(from2);
        BDDMockito.given(currentPurchaseOption.getValidTo()).willReturn(to2);
        BDDMockito.given(currentPurchaseOption.getPurchaseOption()).willReturn(purchaseOption);

        final List<PurchaseOptionConfigModel> puchaseOptions = new ArrayList<>();
        puchaseOptions.add(existingPurchaseOption);

        BDDMockito.given(purchaseOptionConfigService.getAllPurchaseOptionConfigs(purchaseOption))
                .willReturn(puchaseOptions);

        purchaseOptionConfigValidator.onValidate(currentPurchaseOption, interceptorContext);
    }


    /**
     * This is testing for right overlapping
     * 
     * @throws InterceptorException
     * @throws ParseException
     * @throws TargetUnknownIdentifierException
     */
    @Test(expected = InterceptorException.class)
    public void testOnValidateOverLapping2() throws InterceptorException, ParseException,
            TargetUnknownIdentifierException {
        final Date from1 = sdf.parse("01/06/2013 12:00:00");
        final Date to1 = sdf.parse("01/07/2013 12:00:00");

        final Date from2 = sdf.parse("30/06/2013 12:00:00");
        final Date to2 = sdf.parse("20/08/2013 12:00:00");

        BDDMockito.given(existingPurchaseOption.getValidFrom()).willReturn(from1);
        BDDMockito.given(existingPurchaseOption.getValidTo()).willReturn(to1);

        BDDMockito.given(currentPurchaseOption.getCode()).willReturn("test-code");
        BDDMockito.given(currentPurchaseOption.getValidFrom()).willReturn(from2);
        BDDMockito.given(currentPurchaseOption.getValidTo()).willReturn(to2);
        BDDMockito.given(currentPurchaseOption.getPurchaseOption()).willReturn(purchaseOption);

        final List<PurchaseOptionConfigModel> puchaseOptions = new ArrayList<>();
        puchaseOptions.add(existingPurchaseOption);

        BDDMockito.given(purchaseOptionConfigService.getAllPurchaseOptionConfigs(purchaseOption))
                .willReturn(puchaseOptions);


        purchaseOptionConfigValidator.onValidate(currentPurchaseOption, interceptorContext);
    }


    /**
     * This is testing for left overlapping
     * 
     * @throws InterceptorException
     * @throws ParseException
     * @throws TargetUnknownIdentifierException
     */
    @Test(expected = InterceptorException.class)
    public void testOnValidateOverLapping3() throws InterceptorException, ParseException,
            TargetUnknownIdentifierException {
        final Date from1 = sdf.parse("01/06/2013 12:00:00");
        final Date to1 = sdf.parse("01/07/2013 12:00:00");

        final Date from2 = sdf.parse("30/04/2013 12:00:00");
        final Date to2 = sdf.parse("10/06/2013 12:00:00");

        BDDMockito.given(existingPurchaseOption.getValidFrom()).willReturn(from1);
        BDDMockito.given(existingPurchaseOption.getValidTo()).willReturn(to1);

        BDDMockito.given(currentPurchaseOption.getCode()).willReturn("test-code");
        BDDMockito.given(currentPurchaseOption.getValidFrom()).willReturn(from2);
        BDDMockito.given(currentPurchaseOption.getValidTo()).willReturn(to2);
        BDDMockito.given(currentPurchaseOption.getPurchaseOption()).willReturn(purchaseOption);

        final List<PurchaseOptionConfigModel> puchaseOptions = new ArrayList<>();
        puchaseOptions.add(existingPurchaseOption);

        BDDMockito.given(purchaseOptionConfigService.getAllPurchaseOptionConfigs(purchaseOption))
                .willReturn(puchaseOptions);


        purchaseOptionConfigValidator.onValidate(currentPurchaseOption, interceptorContext);
    }

    /**
     * This is testing for accuracy to seconds
     * 
     * @throws InterceptorException
     * @throws ParseException
     * @throws TargetUnknownIdentifierException
     */
    @Test(expected = InterceptorException.class)
    public void testOnValidateOverLapping4() throws InterceptorException, ParseException,
            TargetUnknownIdentifierException {
        final Date from1 = sdf.parse("01/06/2013 12:00:00");
        final Date to1 = sdf.parse("01/07/2013 12:00:00");

        final Date from2 = sdf.parse("1/07/2013 11:59:59");
        final Date to2 = sdf.parse("1/08/2013 12:00:00");

        BDDMockito.given(existingPurchaseOption.getValidFrom()).willReturn(from1);
        BDDMockito.given(existingPurchaseOption.getValidTo()).willReturn(to1);

        BDDMockito.given(currentPurchaseOption.getCode()).willReturn("test-code");
        BDDMockito.given(currentPurchaseOption.getValidFrom()).willReturn(from2);
        BDDMockito.given(currentPurchaseOption.getValidTo()).willReturn(to2);
        BDDMockito.given(currentPurchaseOption.getPurchaseOption()).willReturn(purchaseOption);

        final List<PurchaseOptionConfigModel> puchaseOptions = new ArrayList<>();
        puchaseOptions.add(existingPurchaseOption);

        BDDMockito.given(purchaseOptionConfigService.getAllPurchaseOptionConfigs(purchaseOption))
                .willReturn(puchaseOptions);


        purchaseOptionConfigValidator.onValidate(currentPurchaseOption, interceptorContext);
    }

    /**
     * This is testing for preventing self conflicts
     * 
     * @throws InterceptorException
     * @throws ParseException
     * @throws TargetUnknownIdentifierException
     */
    @Test
    public void testOnValidateOverLappingByItSelf() throws InterceptorException, ParseException,
            TargetUnknownIdentifierException {
        final Date from1 = sdf.parse("01/06/2013 12:00:00");
        final Date to1 = sdf.parse("01/07/2013 12:00:00");

        final Date from2 = sdf.parse("1/07/2013 12:00:00");
        final Date to2 = sdf.parse("1/08/2013 12:00:00");

        BDDMockito.given(existingPurchaseOption.getValidFrom()).willReturn(from1);
        BDDMockito.given(existingPurchaseOption.getValidTo()).willReturn(to1);

        BDDMockito.given(currentPurchaseOption.getCode()).willReturn("test-code");
        BDDMockito.given(currentPurchaseOption.getValidFrom()).willReturn(from2);
        BDDMockito.given(currentPurchaseOption.getValidTo()).willReturn(to2);
        BDDMockito.given(currentPurchaseOption.getPurchaseOption()).willReturn(purchaseOption);

        final List<PurchaseOptionConfigModel> puchaseOptions = new ArrayList<>();
        puchaseOptions.add(existingPurchaseOption);
        puchaseOptions.add(currentPurchaseOption);

        BDDMockito.given(purchaseOptionConfigService.getAllPurchaseOptionConfigs(purchaseOption))
                .willReturn(puchaseOptions);


        purchaseOptionConfigValidator.onValidate(currentPurchaseOption, interceptorContext);
    }


    /**
     * This is testing for multiple existing Purchase Option Config Conflicts
     * 
     * @throws InterceptorException
     * @throws ParseException
     * @throws TargetUnknownIdentifierException
     */
    @Test(expected = InterceptorException.class)
    public void testOnValidateOverLapping5() throws InterceptorException, ParseException,
            TargetUnknownIdentifierException {
        final Date from1 = sdf.parse("01/06/2013 12:00:00");
        final Date to1 = sdf.parse("01/07/2013 12:00:00");

        final Date from2 = sdf.parse("1/08/2013 12:00:00");
        final Date to2 = sdf.parse("1/09/2013 12:00:00");

        final Date from3 = sdf.parse("30/06/2013 12:00:00");
        final Date to3 = sdf.parse("30/80/2013 12:00:00");

        final PurchaseOptionConfigModel anotherPurchaseOption = Mockito.mock(PurchaseOptionConfigModel.class);

        BDDMockito.given(existingPurchaseOption.getValidFrom()).willReturn(from1);
        BDDMockito.given(existingPurchaseOption.getValidTo()).willReturn(to1);

        BDDMockito.given(anotherPurchaseOption.getValidFrom()).willReturn(from2);
        BDDMockito.given(anotherPurchaseOption.getValidTo()).willReturn(to2);

        BDDMockito.given(currentPurchaseOption.getCode()).willReturn("test-code");
        BDDMockito.given(currentPurchaseOption.getValidFrom()).willReturn(from3);
        BDDMockito.given(currentPurchaseOption.getValidTo()).willReturn(to3);
        BDDMockito.given(currentPurchaseOption.getPurchaseOption()).willReturn(purchaseOption);

        final List<PurchaseOptionConfigModel> puchaseOptions = new ArrayList<>();
        puchaseOptions.add(existingPurchaseOption);
        puchaseOptions.add(anotherPurchaseOption);

        BDDMockito.given(purchaseOptionConfigService.getAllPurchaseOptionConfigs(purchaseOption))
                .willReturn(puchaseOptions);


        purchaseOptionConfigValidator.onValidate(currentPurchaseOption, interceptorContext);
    }

    @Test
    public void testOnValidateNormal() throws InterceptorException, ParseException,
            TargetUnknownIdentifierException {
        final List<PurchaseOptionConfigModel> puchaseOptions = new ArrayList<>();

        BDDMockito.given(currentPurchaseOption.getPurchaseOption()).willReturn(purchaseOption);
        BDDMockito.given(purchaseOptionConfigService.getAllPurchaseOptionConfigs(purchaseOption))
                .willReturn(puchaseOptions);
        purchaseOptionConfigValidator.onValidate(currentPurchaseOption, interceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testMaxPercantageInvalidValue() throws InterceptorException {
        BDDMockito.given(currentPurchaseOption.getMaxPercentage()).willReturn(Double.valueOf(120.0));
        purchaseOptionConfigValidator.onValidate(currentPurchaseOption, interceptorContext);
    }

    @Test
    public void testMaxPercantageValidValue() throws InterceptorException {
        BDDMockito.given(currentPurchaseOption.getMaxPercentage()).willReturn(Double.valueOf(55.0));
        final List<PurchaseOptionConfigModel> puchaseOptions = new ArrayList<>();

        BDDMockito.given(currentPurchaseOption.getPurchaseOption()).willReturn(purchaseOption);
        BDDMockito.given(purchaseOptionConfigService.getAllPurchaseOptionConfigs(purchaseOption))
                .willReturn(puchaseOptions);
        purchaseOptionConfigValidator.onValidate(currentPurchaseOption, interceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testMaxPercantageLessThanInitDuePercantage() throws InterceptorException {
        BDDMockito.given(initPaymentDue.getDuePercentage()).willReturn(Double.valueOf(10.0));
        BDDMockito.given(currentPurchaseOption.getMaxPercentage()).willReturn(Double.valueOf(5.0));
        purchaseOptionConfigValidator.onValidate(currentPurchaseOption, interceptorContext);
    }

    @Test
    public void testMaxPercantageGreaterThanInitDuePercantage() throws InterceptorException {
        BDDMockito.given(initPaymentDue.getDuePercentage()).willReturn(Double.valueOf(10.0));
        BDDMockito.given(currentPurchaseOption.getMaxPercentage()).willReturn(Double.valueOf(20.0));
        purchaseOptionConfigValidator.onValidate(currentPurchaseOption, interceptorContext);
    }
}

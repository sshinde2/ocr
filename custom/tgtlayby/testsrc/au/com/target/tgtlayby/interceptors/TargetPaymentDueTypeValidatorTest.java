/**
 * 
 */
package au.com.target.tgtlayby.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.Date;

import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;

import au.com.target.tgtlayby.model.PaymentDueConfigModel;


@UnitTest
public class TargetPaymentDueTypeValidatorTest {

    private final TargetPaymentDueTypeValidator paymentDueTypeValidator = new TargetPaymentDueTypeValidator();

    @Mock
    private InterceptorContext interceptorContext;

    @Test(expected = InterceptorException.class)
    public void testPaymentDueValuesTrue() throws InterceptorException {
        final PaymentDueConfigModel paymentDueConfigModel = Mockito.mock(PaymentDueConfigModel.class);
        BDDMockito.given(paymentDueConfigModel.getDueDate()).willReturn(new Date());
        BDDMockito.given(paymentDueConfigModel.getDueDays()).willReturn(Integer.valueOf(10));
        paymentDueTypeValidator.onValidate(paymentDueConfigModel, interceptorContext);
    }

    @Test(expected = InterceptorException.class)
    public void testForNegativeDueAmounts() throws InterceptorException {
        final PaymentDueConfigModel paymentDueConfigModel = Mockito.mock(PaymentDueConfigModel.class);
        BDDMockito.given(paymentDueConfigModel.getDuePercentage()).willReturn(Double.valueOf(-10.0));
        BDDMockito.given(paymentDueConfigModel.getDueDays()).willReturn(Integer.valueOf(-10));
        paymentDueTypeValidator.onValidate(paymentDueConfigModel, interceptorContext);
    }
}

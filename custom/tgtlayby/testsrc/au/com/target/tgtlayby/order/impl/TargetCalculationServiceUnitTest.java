/**
 * 
 */
package au.com.target.tgtlayby.order.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.calculation.FindPaymentCostStrategy;
import de.hybris.platform.order.strategies.calculation.OrderRequiresCalculationStrategy;
import de.hybris.platform.order.strategies.calculation.impl.FindPricingWithCurrentPriceFactoryStrategy;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.delivery.dto.DeliveryCostDto;
import au.com.target.tgtcore.order.impl.TargetFindDeliveryCostStrategyImpl;
import au.com.target.tgtcore.order.impl.TargetFindFeeTaxValueStrategy;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCalculationServiceUnitTest {

    @Mock
    private TargetFindFeeTaxValueStrategy targetFindFeeTaxValueStrategy;

    @Mock
    private TargetFindLaybyFeeStrategy targetFindLaybyFeeStrategy;

    @Mock
    private CommonI18NService commonI18NService;

    @Mock
    private AbstractOrderModel order;

    @Mock
    private FindPaymentCostStrategy findPaymentCostStrategy;

    @Mock
    private TargetFindDeliveryCostStrategyImpl findDeliveryCostStrategy;

    @Mock
    private OrderRequiresCalculationStrategy orderRequiresCalculationStrategy;

    @Mock
    private ModelService modelService;

    @Mock
    private FindPricingWithCurrentPriceFactoryStrategy findPriceStrategy;


    @Mock
    private CurrencyModel curr;

    @InjectMocks
    private final TargetCalculationService targetCalculationService = new TargetCalculationService();

    private Map<Double, Double> mapRoundingCheck;

    private TaxValue defaultTaxValue;

    @Mock
    private TargetSalesApplicationConfigService salesApplicationConfigService;

    @Before
    public void setup() {
        // Tax should be rounded to nearest cent, with 0.5 cents rounding up
        mapRoundingCheck = new HashMap<Double, Double>();
        mapRoundingCheck.put(new Double(2.5), new Double(0.23));
        mapRoundingCheck.put(new Double(2.804), new Double(0.25)); // 0.2549 should round down
        mapRoundingCheck.put(new Double(2.805), new Double(0.26)); // 0.255 should round up
        mapRoundingCheck.put(new Double(2.9), new Double(0.26));

        // Set up a tax value for GST at 10% 
        defaultTaxValue = new TaxValue("aus-gst-10", 10, false, "AUD");
        given(targetFindFeeTaxValueStrategy.getTaxValueForFee()).willReturn(defaultTaxValue);


    }

    @Test
    public void testResetAdditionalCostsWithOrder() {

        given(targetFindLaybyFeeStrategy.getLaybyFee(order)).willReturn(new Double(2.5));

        targetCalculationService.resetAdditionalCosts(order, null);

        verify(targetFindLaybyFeeStrategy).getLaybyFee(order);
        verify(order).setLayByFee(new Double(2.5));
        verify(findDeliveryCostStrategy, times(0)).resetDeliveryValue(any(CartModel.class));
    }


    @Test
    public void testResetAdditionalCostsWithCart() {
        final CartModel cart = mock(CartModel.class);

        given(targetFindLaybyFeeStrategy.getLaybyFee(cart)).willReturn(new Double(2.5));

        targetCalculationService.resetAdditionalCosts(cart, null);

        verify(targetFindLaybyFeeStrategy).getLaybyFee(cart);
        verify(cart).setLayByFee(new Double(2.5));
        verify(findDeliveryCostStrategy).resetDeliveryValue(any(CartModel.class));
    }


    @SuppressWarnings("boxing")
    @Test
    public void testCalculateTotals() throws CalculationException {

        given(order.getCurrency()).willReturn(curr);
        given(order.getCurrency().getDigits()).willReturn(new Integer(10));
        given(curr.getDigits()).willReturn(2);

        given(order.getSubtotal()).willReturn(new Double(10.0));
        given(order.getLayByFee()).willReturn(new Double(2.5));
        given(order.getDeliveryCost()).willReturn(new Double(9));
        given(commonI18NService.roundCurrency(21.5, 2)).willReturn(21.5);

        final double deliveryCost = 9d;
        final DeliveryCostDto deliveryCostDto = new DeliveryCostDto();
        deliveryCostDto.setDeliveryCost(deliveryCost);
        given(findDeliveryCostStrategy.getDeliveryCostDto(order)).willReturn(deliveryCostDto);

        final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap = new HashMap<TaxValue, Map<Set<TaxValue>, Double>>();
        targetCalculationService.calculateTotals(order, true, taxValueMap);
        verify(order).setDeliveryCost(Double.valueOf(deliveryCost));
        verify(findDeliveryCostStrategy).resetShipsterAttibutes(order, deliveryCostDto);
        verify(order).setTotalPrice(new Double(21.5));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testTaxRounding() throws CalculationException {

        given(order.getCurrency()).willReturn(curr);
        given(curr.getDigits()).willReturn(2);

        // Add a tax record
        for (final Double gross : mapRoundingCheck.keySet()) {
            final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap = new HashMap<TaxValue, Map<Set<TaxValue>, Double>>();
            targetCalculationService.addFeeTaxValue(gross, taxValueMap);

            final double tax = targetCalculationService.calculateTotalTaxValues(order, true, 2, taxValueMap);
            Assert.assertEquals(checkTax(gross), tax);
        }
    }


    @SuppressWarnings("boxing")
    private double checkTax(final double gross) {
        return mapRoundingCheck.get(new Double(gross));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testFindPriceWithOutPreserveValue() throws CalculationException {

        targetCalculationService.setFindPriceStrategy(findPriceStrategy);
        final AbstractOrderEntryModel entry = mock(AbstractOrderEntryModel.class);
        given(orderRequiresCalculationStrategy.requiresCalculation(order)).willReturn(Boolean.TRUE);
        given(entry.getOrder()).willReturn(order);
        given(entry.getOrder().getCurrency()).willReturn(curr);
        given(entry.getOrder().getCurrency().getIsocode()).willReturn("AUD");
        given(entry.getOrder().getPreserveItemValue().booleanValue()).willReturn(false);
        targetCalculationService.findBasePrice(entry);
        verify(findPriceStrategy).findBasePrice(entry);

    }

    @SuppressWarnings("boxing")
    @Test
    public void testFindPriceWithPreserveValue() throws CalculationException {

        targetCalculationService.setFindPriceStrategy(findPriceStrategy);
        final AbstractOrderEntryModel entry = mock(AbstractOrderEntryModel.class);
        given(orderRequiresCalculationStrategy.requiresCalculation(order)).willReturn(Boolean.TRUE);
        given(entry.getOrder()).willReturn(order);
        given(entry.getOrder().getCurrency()).willReturn(curr);
        given(entry.getOrder().getCurrency().getIsocode()).willReturn("AUD");
        given(entry.getOrder().getPreserveItemValue().booleanValue()).willReturn(true);
        targetCalculationService.findBasePrice(entry);
        verify(findPriceStrategy, Mockito.never()).findBasePrice(entry);

    }

    @SuppressWarnings("boxing")
    @Test
    public void testFindPriceWithNullPreserveValue() throws CalculationException {

        targetCalculationService.setFindPriceStrategy(findPriceStrategy);
        final AbstractOrderEntryModel entry = mock(AbstractOrderEntryModel.class);
        given(orderRequiresCalculationStrategy.requiresCalculation(order)).willReturn(Boolean.TRUE);
        given(entry.getOrder()).willReturn(order);
        given(entry.getOrder().getCurrency()).willReturn(curr);
        given(entry.getOrder().getCurrency().getIsocode()).willReturn("AUD");
        given(entry.getOrder().getPreserveItemValue()).willReturn(null);
        targetCalculationService.findBasePrice(entry);
        verify(findPriceStrategy).findBasePrice(entry);

    }

    @Test
    public void testTotalTaxForTradeMeOrders() throws CalculationException {
        final OrderModel orderModel = mockOrderData(SalesApplication.TRADEME);

        given(Boolean.valueOf(salesApplicationConfigService.isSuppressTaxCalculation(SalesApplication.TRADEME)))
                .willReturn(Boolean.valueOf(true));
        given(findDeliveryCostStrategy.getDeliveryCostDto(orderModel)).willReturn(new DeliveryCostDto());
        final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap = new HashMap<TaxValue, Map<Set<TaxValue>, Double>>();
        targetCalculationService.calculateTotals(orderModel, true, taxValueMap);
        verify(orderModel).setTotalTax(Double.valueOf(0.00));

    }

    @Test
    public void testTotalTaxForEbayOrders() throws CalculationException {
        final OrderModel orderModel = mockOrderData(SalesApplication.EBAY);

        given(Boolean.valueOf(salesApplicationConfigService.isSuppressTaxCalculation(SalesApplication.EBAY)))
                .willReturn(Boolean.valueOf(false));
        given(findDeliveryCostStrategy.getDeliveryCostDto(orderModel)).willReturn(new DeliveryCostDto());
        final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap = new HashMap<TaxValue, Map<Set<TaxValue>, Double>>();
        targetCalculationService.calculateTotals(orderModel, true, taxValueMap);
        verify(orderModel).setTotalTax(Double.valueOf(1.05));
    }

    /**
     * Method to mock order data
     * 
     * @return OrderModel
     */
    private OrderModel mockOrderData(final SalesApplication salesApplication) {
        final OrderModel orderModel = Mockito.mock(OrderModel.class);
        given(orderModel.getSalesApplication()).willReturn(salesApplication);
        given(orderModel.getCurrency()).willReturn(curr);
        given(orderModel.getCurrency().getDigits()).willReturn(new Integer(10));
        given(curr.getDigits()).willReturn(Integer.valueOf(2));

        given(orderModel.getSubtotal()).willReturn(new Double(10.0));
        given(orderModel.getLayByFee()).willReturn(new Double(2.5));
        given(orderModel.getDeliveryCost()).willReturn(new Double(9));
        given(Double.valueOf(commonI18NService.roundCurrency(21.5, 2))).willReturn(Double.valueOf(21.5));
        given(Double.valueOf(commonI18NService.roundCurrency(1.05, 2))).willReturn(Double.valueOf(1.05));

        final PriceValue deliCost = Mockito.mock(PriceValue.class);
        given(findDeliveryCostStrategy.getDeliveryCost(orderModel)).willReturn(deliCost);
        given(Double.valueOf(deliCost.getValue())).willReturn(new Double(9));
        return orderModel;
    }

}

/**
 * 
 */
package au.com.target.tgtlayby.order.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.order.FindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtcore.order.TargetDiscountService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtlayby.constants.TgtlaybyConstants;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.purchase.PurchaseOptionService;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.service.PaymentsInProgressService;
import au.com.target.tgtpayment.service.TargetPaymentService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetLaybyCommerceCheckoutServiceImplTest {

    @Mock
    private SessionService sessionService;

    @Mock
    private TargetLaybyCartService targetLaybyCartService;

    @Mock
    private CartModel masterCart;

    @Mock
    private CartModel checkoutCart;

    @Mock
    private UnitModel unit;

    @Mock
    private CalculationService calculationService;

    @Mock
    private ModelService modelService;

    @Mock
    private FindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy;

    @Mock
    private TargetStockService targetStockService;

    @Mock
    private TargetCommerceCartService targetCommerceCartService;

    @Mock
    private PaymentsInProgressService paymentsInProgressService;

    @Mock
    private TargetPaymentService targetPaymentService;

    @Mock
    private PurchaseOptionService purchaseOptionService;

    @Mock
    private TargetDiscountService targetDiscountService;

    @InjectMocks
    private final TargetLaybyCommerceCheckoutServiceImpl targetLaybyCheckoutService = new TargetLaybyCommerceCheckoutServiceImpl();

    @Mock
    private CartModel cartModel;

    @Mock
    private PurchaseOptionModel purchaseOptionModel;

    @Mock
    private ProductModel productModel;

    @Mock
    private CartEntryModel targetEntryModel;

    @Mock
    private PurchaseOptionModel purchaseOption;

    @Mock
    private PurchaseOptionConfigModel purchaseOptionConfig;

    @Mock
    private CartModel cartPO1;

    @Mock
    private TargetCustomerModel customer;

    @Mock
    private PurchaseOptionModel purchaseOption1;

    @Mock
    private DeliveryModeModel deliveryModeModel;

    @Mock
    private PaymentInfoModel paymentInfoModel;

    @Mock
    private CurrencyModel currencyModel;

    @Mock
    private PaymentTransactionEntryModel paymentTransactionEntryModel;

    @Mock
    private CommerceCheckoutService commerceCheckoutService;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Before
    public void setup() {
        given(cartModel.getPurchaseOption()).willReturn(purchaseOptionModel);
        given(targetEntryModel.getProduct()).willReturn(productModel);
        given(productModel.getName()).willReturn("test");
        given(targetEntryModel.getQuantity()).willReturn(Long.valueOf(10L));

        given(masterCart.getEntries()).willReturn(
                Collections.singletonList((AbstractOrderEntryModel)targetEntryModel));
        given(targetLaybyCartService.getSessionCart()).willReturn(masterCart);
        given(targetEntryModel.getProduct()).willReturn(productModel);

        given(targetEntryModel.getUnit()).willReturn(unit);

        given(purchaseOption.getCode()).willReturn("buynow");
        given(purchaseOptionConfig.getPurchaseOption()).willReturn(purchaseOption);
        given(checkoutCart.getPaymentInfo()).willReturn(paymentInfoModel);
        given(checkoutCart.getCurrency()).willReturn(currencyModel);
        given(checkoutCart.getTotalPrice()).willReturn(Double.valueOf(10));
        given(sessionService.getAttribute(TgtlaybyConstants.SESSION_CHECKOUT_CART + "buynow")).willReturn(
                checkoutCart);

        given(
                targetPaymentService.capture(Mockito.any(CartModel.class), Mockito.any(PaymentInfoModel.class),
                        Mockito.any(BigDecimal.class), Mockito.any(CurrencyModel.class),
                        Mockito.any(PaymentCaptureType.class)))
                .willReturn(paymentTransactionEntryModel);
    }

    @Test
    public void testPerformSOHOnCart() throws CommerceCartModificationException {
        final List<CommerceCartModification> cartModifications = new ArrayList<>();
        final CommerceCartModification modification = Mockito.mock(CommerceCartModification.class);
        cartModifications.add(modification);
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        entries.add(targetEntryModel);
        final List<AbstractOrderEntryModel> list = new ArrayList<>();
        list.addAll(entries);
        given(masterCart.getEntries()).willReturn(entries);
        given(targetCommerceCartService.performSOHOnEntries(masterCart, list)).willReturn(
                cartModifications);
        final List<CommerceCartModification> result = targetLaybyCheckoutService.performSOHOnCart(masterCart);
        assertThat(result.size()).isEqualTo(1);
    }

    @Test
    public void testChangeCurrentCustomerOnAllCheckoutCarts() {

        given(masterCart.getPurchaseOption()).willReturn(purchaseOption);

        targetLaybyCheckoutService.changeCurrentCustomerOnAllCheckoutCarts(masterCart, customer);
        Mockito.verify(masterCart).setUser(customer);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testValidateDeliveryModeInvalid() {
        given(cartModel.getDeliveryMode()).willReturn(deliveryModeModel);
        given(targetCommerceCartService.isDeliveryModeApplicableForCart(deliveryModeModel, cartModel))
                .willReturn(Boolean.FALSE);

        final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
        checkoutParameter.setCart(cartModel);
        targetLaybyCheckoutService.validateDeliveryMode(checkoutParameter);

        verify(cartModel, times(1)).setDeliveryMode(null);
        verify(modelService, times(1)).save(cartModel);
        verify(targetCommerceCartService).calculateCart(Mockito.any(CommerceCartParameter.class));

    }

    @SuppressWarnings("boxing")
    @Test
    public void testValidateDeliveryModeValid() {
        given(cartModel.getDeliveryMode()).willReturn(deliveryModeModel);
        given(targetCommerceCartService.isDeliveryModeApplicableForCart(deliveryModeModel, cartModel))
                .willReturn(Boolean.TRUE);

        final CommerceCheckoutParameter checkoutParameter = new CommerceCheckoutParameter();
        checkoutParameter.setCart(cartModel);
        targetLaybyCheckoutService.validateDeliveryMode(checkoutParameter);

        verify(cartModel, times(0)).setDeliveryMode(null);
        verify(modelService, times(0)).save(cartModel);
    }

    @Test
    public void testBeforePlaceOrderForFullAmountCapture() throws InsufficientStockLevelException {

        final AbstractOrderEntryModel entry = Mockito.mock(AbstractOrderEntryModel.class);
        given(entry.getProduct()).willReturn(productModel);
        given(entry.getQuantity()).willReturn(Long.valueOf(10L));
        given(checkoutCart.getEntries()).willReturn(Collections.singletonList(entry));
        given(checkoutCart.getPurchaseOption()).willReturn(purchaseOption);
        given(checkoutCart.getPurchaseOptionConfig()).willReturn(purchaseOptionConfig);
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(checkoutCart)).willReturn(
                Double.valueOf(10.0));

        final boolean successful = targetLaybyCheckoutService.beforePlaceOrder(checkoutCart, null);
        assertThat(successful).isTrue();

        Mockito.verify(targetStockService).reserve(productModel, 10,
                "ReserveStock : cart=null, quantity=10 for product=test");
        Mockito.verify(targetPaymentService).capture(checkoutCart, paymentInfoModel, BigDecimal.valueOf(10.0),
                currencyModel, PaymentCaptureType.PLACEORDER);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testBeforePlaceOrderForZeroAmountCapture() throws InsufficientStockLevelException {

        final AbstractOrderEntryModel entry = Mockito.mock(AbstractOrderEntryModel.class);
        given(entry.getProduct()).willReturn(productModel);
        given(entry.getQuantity()).willReturn(Long.valueOf(10L));
        given(checkoutCart.getEntries()).willReturn(Collections.singletonList(entry));

        given(checkoutCart.getPurchaseOption()).willReturn(purchaseOption);
        given(checkoutCart.getPurchaseOptionConfig()).willReturn(purchaseOptionConfig);
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(checkoutCart)).willReturn(
                Double.valueOf(0d));
        given(paymentsInProgressService.removeInProgressPayment(checkoutCart)).willReturn(Boolean.TRUE);

        final boolean successful = targetLaybyCheckoutService.beforePlaceOrder(checkoutCart, null);
        assertThat(successful).isFalse();

        Mockito.verify(targetStockService)
                .reserve(productModel, 10, "ReserveStock : cart=null, quantity=10 for product=test");
        Mockito.verify(targetPaymentService).capture(checkoutCart, paymentInfoModel, BigDecimal.valueOf(10.0),
                currencyModel, PaymentCaptureType.PLACEORDER);
        Mockito.verify(targetStockService)
                .release(productModel, 10, "ReleaseStock : cart=null, quantity=10 for product=test");
        Mockito.verify(paymentsInProgressService).removeInProgressPayment(checkoutCart);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testBeforePlaceOrderForPartialAmountCapture() throws InsufficientStockLevelException {

        final AbstractOrderEntryModel entry = Mockito.mock(AbstractOrderEntryModel.class);
        given(entry.getProduct()).willReturn(productModel);
        given(entry.getQuantity()).willReturn(Long.valueOf(10L));
        given(checkoutCart.getEntries()).willReturn(Collections.singletonList(entry));

        given(checkoutCart.getPurchaseOption()).willReturn(purchaseOption);
        given(checkoutCart.getPurchaseOptionConfig()).willReturn(purchaseOptionConfig);
        given(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(checkoutCart)).willReturn(
                Double.valueOf(8d));
        given(paymentsInProgressService.removeInProgressPayment(checkoutCart)).willReturn(Boolean.TRUE);
        final boolean successful = targetLaybyCheckoutService.beforePlaceOrder(checkoutCart, null);
        assertThat(successful).isFalse();

        Mockito.verify(targetStockService)
                .reserve(productModel, 10, "ReserveStock : cart=null, quantity=10 for product=test");
        Mockito.verify(targetPaymentService).capture(checkoutCart, paymentInfoModel, BigDecimal.valueOf(10.0),
                currencyModel, PaymentCaptureType.PLACEORDER);
        Mockito.verify(targetStockService)
                .release(productModel, 10, "ReleaseStock : cart=null, quantity=10 for product=test");
        Mockito.verify(paymentsInProgressService).removeInProgressPayment(checkoutCart);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testSetAmountCurrentPaymentSuccess() {
        final Double amountCurrentPayment = Double.valueOf(7.5d);
        final CartModel mockCart = Mockito.mock(CartModel.class);

        given(targetCommerceCartService.calculateCart(Mockito.any(CommerceCartParameter.class))).willReturn(
                Boolean.TRUE);

        final boolean result = targetLaybyCheckoutService.setAmountCurrentPayment(mockCart, amountCurrentPayment);

        assertThat(result).isTrue();

        final InOrder verifyOrder = Mockito.inOrder(mockCart, modelService, targetCommerceCartService);
        verifyOrder.verify(mockCart).setAmountCurrentPayment(amountCurrentPayment);
        verifyOrder.verify(modelService).save(mockCart);
        verifyOrder.verify(targetCommerceCartService).calculateCart(Mockito.any(CommerceCartParameter.class));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testSetAmountCurrentPaymentFail() {
        final Double amountCurrentPayment = Double.valueOf(7.5d);
        final CartModel mockCart = Mockito.mock(CartModel.class);
        given(targetCommerceCartService.calculateCart(Mockito.any(CommerceCartParameter.class))).willReturn(
                Boolean.FALSE);
        final boolean result = targetLaybyCheckoutService.setAmountCurrentPayment(mockCart, amountCurrentPayment);
        assertThat(result).isFalse();

        final InOrder verifyOrder = Mockito.inOrder(mockCart, modelService, targetCommerceCartService);
        verifyOrder.verify(mockCart).setAmountCurrentPayment(amountCurrentPayment);
        verifyOrder.verify(modelService).save(mockCart);
        verifyOrder.verify(targetCommerceCartService).calculateCart(Mockito.any(CommerceCartParameter.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetAmountCurrentPaymentWithNullCart() {
        final Double amountCurrentPayment = Double.valueOf(7.5d);

        targetLaybyCheckoutService.setAmountCurrentPayment(null, amountCurrentPayment);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testSetAmountCurrentWithNullAmount() {
        final CartModel mockCart = Mockito.mock(CartModel.class);
        given(targetCommerceCartService.calculateCart(Mockito.any(CommerceCartParameter.class))).willReturn(
                Boolean.TRUE);
        final boolean result = targetLaybyCheckoutService.setAmountCurrentPayment(mockCart, null);

        assertThat(result).isTrue();
        final InOrder verifyOrder = Mockito.inOrder(mockCart, modelService, targetCommerceCartService);
        verifyOrder.verify(mockCart).setAmountCurrentPayment(null);
        verifyOrder.verify(modelService).save(mockCart);
        verifyOrder.verify(targetCommerceCartService).calculateCart(Mockito.any(CommerceCartParameter.class));
    }

    @Test
    public void setTmdForLaybyCart() {
        final String tmdCardNumber = "tmd";
        willReturn(Boolean.TRUE).given(targetDiscountService).isValidTMDCard(tmdCardNumber);
        willReturn(Boolean.TRUE).given(targetCommerceCartService).calculateCart(any(CommerceCartParameter.class));
        willReturn(Boolean.TRUE).given(targetDiscountService).isTMDiscountAppliedToOrder(any(CartModel.class));
        targetLaybyCheckoutService.setTMDCardNumber(checkoutCart, tmdCardNumber);
        verify(checkoutCart).setTmdCardNumber(tmdCardNumber);
        verify(masterCart).setTmdCardNumber(tmdCardNumber);
    }

    @Test
    public void setTmdForNormalCart() {
        final String tmdCardNumber = "tmd";
        willReturn(Boolean.TRUE).given(targetDiscountService).isValidTMDCard(tmdCardNumber);
        willReturn(Boolean.TRUE).given(targetCommerceCartService).calculateCart(any(CommerceCartParameter.class));
        willReturn(checkoutCart).given(targetLaybyCartService).getSessionCart();
        targetLaybyCheckoutService.setTMDCardNumber(checkoutCart, tmdCardNumber);
        verify(checkoutCart, times(1)).setTmdCardNumber(tmdCardNumber);
    }
}

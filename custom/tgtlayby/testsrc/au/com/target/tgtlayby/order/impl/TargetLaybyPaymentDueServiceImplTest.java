/**
 * 
 */
package au.com.target.tgtlayby.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import au.com.target.tgtcore.order.impl.TargetFindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtlayby.data.PaymentDueData;
import au.com.target.tgtlayby.model.PaymentDueConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("boxing")
public class TargetLaybyPaymentDueServiceImplTest {

    @Mock
    private CommonI18NService commonI18NService;

    @Mock
    private AbstractOrderModel abstractOrder;

    @Mock
    private PurchaseOptionConfigModel poc;

    @Mock
    private PaymentDueConfigModel paymentDue1;

    @Mock
    private PaymentDueConfigModel paymentDue2;

    @Mock
    private PaymentDueConfigModel paymentDue3;

    @Mock
    private CurrencyModel currency;

    @Mock
    private PaymentDueData dueData1;

    @Mock
    private PaymentDueData dueData2;

    @Mock
    private PaymentDueData dueData3;

    @Mock
    private TargetFindOrderTotalPaymentMadeStrategy targetFindOrderTotalPaymentMadeStrategy;

    @InjectMocks
    @Spy
    private final TargetLaybyPaymentDueServiceImpl service = new TargetLaybyPaymentDueServiceImpl();

    private Set<PaymentDueConfigModel> paymentDues;

    private SimpleDateFormat sdf;

    @Before
    public void setup() throws ParseException {
        sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        paymentDues = new HashSet<>();
        paymentDues.add(paymentDue1);
        paymentDues.add(paymentDue2);
        paymentDues.add(paymentDue3);
        BDDMockito.given(abstractOrder.getPurchaseOptionConfig()).willReturn(poc);
        BDDMockito.given(poc.getAllowPaymentDues()).willReturn(true);
        BDDMockito.given(poc.getPaymentDueConfigs()).willReturn(paymentDues);
        BDDMockito.given(abstractOrder.getCurrency()).willReturn(currency);
        BDDMockito.given(currency.getDigits()).willReturn(2);

        BDDMockito.given(commonI18NService.roundCurrency(Mockito.anyDouble(), Mockito.anyInt())).willAnswer(
                new Answer<Object>() {

                    @Override
                    public Object answer(final InvocationOnMock invocation) {
                        final Object[] array = invocation.getArguments();
                        return array[0];
                    }
                });
    }

    @Test
    public void testCreatePaymentDueDataFromPaymentDueConfigDueDateVersion() throws ParseException {
        BDDMockito.given(paymentDue1.getDuePercentage()).willReturn(50.0);
        BDDMockito.given(paymentDue1.getDueDays()).willReturn(null);
        final Date date = sdf.parse("22/07/2013 00:00:00");
        BDDMockito.given(paymentDue1.getDueDate()).willReturn(date);
        BDDMockito.given(abstractOrder.getTotalPrice()).willReturn(100.0);
        final PaymentDueData result = service.createPaymentDueDataFromPaymentDueConfig(paymentDue1, abstractOrder);
        Assert.assertEquals(50.0, result.getPercentage());
        Assert.assertEquals(50.0, result.getAmount());
        Assert.assertEquals(date, result.getDueDate());
    }

    @Test
    public void testCreatePaymentDueDataFromPaymentDueConfigDueDaysVersion() throws ParseException {
        BDDMockito.given(paymentDue1.getDuePercentage()).willReturn(20.0);
        BDDMockito.given(paymentDue1.getDueDays()).willReturn(null);
        final Date date = sdf.parse("22/07/2013 00:00:00");
        final Date finalDate = sdf.parse("19/08/2013 00:00:00");
        BDDMockito.given(paymentDue1.getDueDays()).willReturn(28);
        BDDMockito.given(abstractOrder.getDate()).willReturn(date);
        BDDMockito.given(abstractOrder.getTotalPrice()).willReturn(120.0);
        final PaymentDueData result = service.createPaymentDueDataFromPaymentDueConfig(paymentDue1, abstractOrder);
        Assert.assertEquals(20.0, result.getPercentage());
        Assert.assertEquals(24.0, result.getAmount());
        Assert.assertEquals(finalDate, result.getDueDate());
    }

    @Test
    public void testPaymentDueDataDateRoundedDownForCart() throws ParseException {
        final Date date = sdf.parse("12/04/2013 11:59:00");
        final Date finalDate = sdf.parse("07/06/2013 00:00:00");
        BDDMockito.given(abstractOrder.getDate()).willReturn(date);
        BDDMockito.given(paymentDue1.getDueDays()).willReturn(Integer.valueOf(56));
        final PaymentDueData result = service.createPaymentDueDataFromPaymentDueConfig(paymentDue1, abstractOrder);

        Assert.assertEquals(finalDate, result.getDueDate());
    }

    @Test
    public void testPaymentDueDataDateRoundedUpForCart() throws ParseException {
        final Date date = sdf.parse("12/04/2013 12:01:00");
        final Date finalDate = sdf.parse("08/06/2013 00:00:00");
        BDDMockito.given(abstractOrder.getDate()).willReturn(date);
        BDDMockito.given(paymentDue1.getDueDays()).willReturn(Integer.valueOf(56));
        final PaymentDueData result = service.createPaymentDueDataFromPaymentDueConfig(paymentDue1, abstractOrder);

        Assert.assertEquals(finalDate, result.getDueDate());
    }

    @Test
    public void testBeforeDaylightSavingsDates() throws ParseException {
        final Date date = sdf.parse("07/04/2013 00:01:00");
        final Date finalDate = sdf.parse("05/05/2013 00:00:00");
        BDDMockito.given(abstractOrder.getDate()).willReturn(date);
        BDDMockito.given(paymentDue1.getDueDays()).willReturn(Integer.valueOf(28));
        final PaymentDueData result = service.createPaymentDueDataFromPaymentDueConfig(paymentDue1, abstractOrder);

        Assert.assertEquals(finalDate, result.getDueDate());
    }

    @Test
    public void testAfterDaylightSavingsDates() throws ParseException {
        final Date date = sdf.parse("06/10/2013 12:01:00");
        final Date finalDate = sdf.parse("04/11/2013 00:00:00");
        BDDMockito.given(abstractOrder.getDate()).willReturn(date);
        BDDMockito.given(paymentDue1.getDueDays()).willReturn(Integer.valueOf(28));
        final PaymentDueData result = service.createPaymentDueDataFromPaymentDueConfig(paymentDue1, abstractOrder);

        Assert.assertEquals(finalDate, result.getDueDate());
    }

    @Test
    public void testGetAllPaymentDuesForAbstractOrder() {
        mockPaymentDueDatas();
        final Set<PaymentDueData> result = service.getAllPaymentDuesForAbstractOrder(abstractOrder);
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void testGetPaymentDueDataForAbstractOrderAndPaymentDueConfig() {
        mockPaymentDueDatas();
        final PaymentDueData result = service.getPaymentDueDataForAbstractOrderAndPaymentDueConfig(abstractOrder,
                paymentDue1);
        Assert.assertEquals(dueData1, result);
    }

    @Test
    public void testGetInitialPaymentDue() {
        mockPaymentDueDatas();
        BDDMockito.given(poc.getMinimumPaymentDue()).willReturn(paymentDue1);
        final PaymentDueData result = service.getInitialPaymentDue(abstractOrder);
        Assert.assertEquals(dueData1, result);
    }

    @Test
    public void testGetIntermediatePaymentDues() {
        mockPaymentDueDatas();
        BDDMockito.given(poc.getMinimumPaymentDue()).willReturn(paymentDue1);
        BDDMockito.given(poc.getFinalPaymentDue()).willReturn(paymentDue3);
        final Set<PaymentDueData> result = service.getIntermediatePaymentDues(abstractOrder);
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void testGetFinalPaymentDue() {
        mockPaymentDueDatas();
        BDDMockito.given(poc.getFinalPaymentDue()).willReturn(paymentDue3);
        final PaymentDueData result = service.getFinalPaymentDue(abstractOrder);
        Assert.assertEquals(dueData3, result);
    }

    @Test
    public void testGetNextPaymentDue() throws ParseException {
        BDDMockito.given(paymentDue1.getDuePercentage()).willReturn(10.0);
        BDDMockito.given(paymentDue2.getDuePercentage()).willReturn(30.0);
        BDDMockito.given(paymentDue3.getDuePercentage()).willReturn(50.0);
        BDDMockito.given(poc.getFinalPaymentDue()).willReturn(paymentDue3);
        final OrderModel order = Mockito.mock(OrderModel.class);
        BDDMockito.given(order.getPurchaseOptionConfig()).willReturn(poc);
        BDDMockito.given(order.getCurrency()).willReturn(currency);
        BDDMockito.given(order.getTotalPrice()).willReturn(100.0);
        BDDMockito.given(paymentDue2.getDueDays()).willReturn(null);
        final Date date = sdf.parse("22/07/2013 00:00:00");
        BDDMockito.given(paymentDue2.getDueDate()).willReturn(date);
        BDDMockito.given(targetFindOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(order)).willReturn(20.0);
        final PaymentDueData result = service.getNextPaymentDue(order);
        Assert.assertEquals(30.0, result.getAmount());
        Assert.assertEquals(date, result.getDueDate());
    }

    private void mockPaymentDueDatas() {
        BDDMockito.doReturn(dueData1).when(service)
                .createPaymentDueDataFromPaymentDueConfig(paymentDue1, abstractOrder);
        BDDMockito.doReturn(dueData2).when(service)
                .createPaymentDueDataFromPaymentDueConfig(paymentDue2, abstractOrder);
        BDDMockito.doReturn(dueData3).when(service)
                .createPaymentDueDataFromPaymentDueConfig(paymentDue3, abstractOrder);
    }
}

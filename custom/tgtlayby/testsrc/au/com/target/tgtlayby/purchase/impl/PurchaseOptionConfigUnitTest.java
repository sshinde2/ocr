/**
 * 
 */
package au.com.target.tgtlayby.purchase.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Date;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.purchase.dao.PurchaseOptionConfigDao;


@UnitTest
public class PurchaseOptionConfigUnitTest {
    private PurchaseOptionConfigServiceImpl purchaseOptionConfigServiceImpl;

    @Mock
    private PurchaseOptionConfigDao purchaseOptionConfigDao;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        purchaseOptionConfigServiceImpl = new PurchaseOptionConfigServiceImpl();
        purchaseOptionConfigServiceImpl.setPurchaseOptionConfigDao(purchaseOptionConfigDao);
    }

    @Test
    public void testGetPurchaseOptionConfigByCode() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final PurchaseOptionConfigModel config = new PurchaseOptionConfigModel();
        config.setCode("code");
        config.setServiceFee(Double.valueOf(10.0));
        BDDMockito.given(purchaseOptionConfigDao.getPurchaseOptionConfigByCode("code")).willReturn(config);

        final PurchaseOptionConfigModel result = purchaseOptionConfigServiceImpl.getPurchaseOptionConfigByCode("code");

        Assert.assertEquals("code", result.getCode());
        Assert.assertEquals(Double.valueOf(10.0), result.getServiceFee());

    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetPurchaseOptionConfigByCodeNullParam() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        purchaseOptionConfigServiceImpl.getPurchaseOptionConfigByCode(null);
    }

    @Test
    public void testGetActivePurchaseOptionConfigByCode() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final PurchaseOptionConfigModel config = new PurchaseOptionConfigModel();
        config.setCode("code");
        config.setServiceFee(Double.valueOf(10.0));
        final PurchaseOptionModel option = Mockito.mock(PurchaseOptionModel.class);
        BDDMockito.given(purchaseOptionConfigDao.getActivePurchaseOptionConfigByPurchaseOption(option)).willReturn(
                config);

        final PurchaseOptionConfigModel result = purchaseOptionConfigServiceImpl
                .getActivePurchaseOptionConfigByPurchaseOption(option);

        Assert.assertEquals("code", result.getCode());
        Assert.assertEquals(Double.valueOf(10.0), result.getServiceFee());

    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetActivePurchaseOptionConfigByCodeNullParam() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        purchaseOptionConfigServiceImpl.getActivePurchaseOptionConfigByPurchaseOption(null);

    }

    @Test
    public void testGetActivePurchaseOptionForOrderPlacedDate() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final PurchaseOptionConfigModel config = new PurchaseOptionConfigModel();
        config.setCode("code");
        config.setServiceFee(Double.valueOf(10.0));
        final Date date = new Date();
        final PurchaseOptionModel option = Mockito.mock(PurchaseOptionModel.class);
        BDDMockito.given(purchaseOptionConfigDao.getPurchaseOptionConfigForOrderPlacedDate(option, date))
                .willReturn(
                        config);

        final PurchaseOptionConfigModel result = purchaseOptionConfigServiceImpl
                .getPurchaseOptionConfigForOrderPlacedDate(option, date);

        Assert.assertEquals("code", result.getCode());
        Assert.assertEquals(Double.valueOf(10.0), result.getServiceFee());

    }

    @Test(expected = IllegalArgumentException.class)
    public void getPurchaseOptionConfigForOrderPlacedDateNullParam() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        purchaseOptionConfigServiceImpl.getPurchaseOptionConfigForOrderPlacedDate(null, null);

    }
}

/**
 * 
 */
package au.com.target.tgtlayby.refund.denialstrategies;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPurchaseOptionConfigAwareRefundStrategyTest {
    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @InjectMocks
    private final TargetPurchaseOptionConfigAwareRefundStrategy strategy = new TargetPurchaseOptionConfigAwareRefundStrategy();

    @Mock
    private AbstractOrderEntryModel entry;

    @Mock
    private OrderModel order;

    @Mock
    private PurchaseOptionConfigModel purchaseOptionConfigModel;

    @Test
    public void testGetRefundDenialReasonForNullOrder() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("OrderModel cannot be null");
        strategy.perform(null, entry, 2L);
    }

    @Test
    public void testGetRefundDenialReasonForOrderWithNOPOConfig() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("No PurchaseOptionConfig found on the order");
        strategy.perform(order, entry, 2L);
    }

    @Test
    public void testGetRefundDenialReasonForOrderWithPOConfig() {
        BDDMockito.given(order.getPurchaseOptionConfig()).willReturn(purchaseOptionConfigModel);
        BDDMockito.given(purchaseOptionConfigModel.getAllowOrderRefund()).willReturn(Boolean.TRUE);

        final boolean result1 = strategy.perform(order, entry, 2L);
        Assert.assertTrue(result1);

        BDDMockito.given(purchaseOptionConfigModel.getAllowOrderRefund()).willReturn(Boolean.FALSE);
        final boolean result2 = strategy.perform(order, entry, 2L);
        Assert.assertFalse(result2);
    }
}

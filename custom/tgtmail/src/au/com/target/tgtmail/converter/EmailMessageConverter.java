/**
 * 
 */
package au.com.target.tgtmail.converter;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import au.com.target.tgtmail.model.Email;



/**
 * @author Olivier Lamy
 */
public class EmailMessageConverter {


    private final JAXBContext jaxbContext;


    public EmailMessageConverter() throws JAXBException {
        jaxbContext = JAXBContext.newInstance(Email.class);
    }

    /**
     * 
     * @param email
     * @return String as an xml view
     * @throws JAXBException
     */
    public String convert(final Email email) throws JAXBException {
        final StringWriter stringWriter = new StringWriter();
        jaxbContext.createMarshaller().marshal(email, stringWriter);
        return stringWriter.toString();
    }

    public Email convert(final String xml) throws JAXBException {
        return (Email)jaxbContext.createUnmarshaller().unmarshal(new StringReader(xml));
    }

}

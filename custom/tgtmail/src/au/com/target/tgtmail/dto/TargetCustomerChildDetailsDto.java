/**
 * 
 */
package au.com.target.tgtmail.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @author mjanarth
 *
 */
public class TargetCustomerChildDetailsDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String firstName;

    private Date birthday;

    private String gender;

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the birthday
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * @param birthday
     *            the birthday to set
     */
    public void setBirthday(final Date birthday) {
        this.birthday = birthday;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender
     *            the gender to set
     */
    public void setGender(final String gender) {
        this.gender = gender;
    }

}

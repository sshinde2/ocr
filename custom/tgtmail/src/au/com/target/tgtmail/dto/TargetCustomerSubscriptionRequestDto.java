package au.com.target.tgtmail.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import au.com.target.tgtmail.enums.TargetCustomerSubscriptionSource;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionType;



public class TargetCustomerSubscriptionRequestDto implements Serializable
{

    private static final long serialVersionUID = 1L;

    private String customerEmail;

    private String firstName;

    private String lastName;

    private String title;

    private String gender;

    private Date birthday;

    private Date dueDate;

    private String babyGender;

    private List<TargetCustomerChildDetailsDto> childrenDetails;

    private TargetCustomerSubscriptionType subscriptionType = TargetCustomerSubscriptionType.NEWSLETTER;

    private TargetCustomerSubscriptionSource customerSubscriptionSource;

    private boolean updateCustomerPersonalDetails;

    private boolean registerForEmail = true;


    /**
     * @return the customerEmail
     */
    public String getCustomerEmail() {
        return customerEmail;
    }

    /**
     * @param customerEmail
     *            the customerEmail to set
     */
    public void setCustomerEmail(final String customerEmail) {
        this.customerEmail = customerEmail;
    }


    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the subscriptionType
     */
    public TargetCustomerSubscriptionType getSubscriptionType() {
        return subscriptionType;
    }

    /**
     * @param subscriptionType
     *            the subscriptionType to set
     */
    public void setSubscriptionType(final TargetCustomerSubscriptionType subscriptionType) {
        this.subscriptionType = subscriptionType;
    }


    /**
     * @return the customerSubscriptionSource
     */
    public TargetCustomerSubscriptionSource getCustomerSubscriptionSource() {
        return customerSubscriptionSource;
    }

    /**
     * @param customerSubscriptionSource
     *            the customerSubscriptionSource to set
     */
    public void setCustomerSubscriptionSource(final TargetCustomerSubscriptionSource customerSubscriptionSource) {
        this.customerSubscriptionSource = customerSubscriptionSource;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender
     *            the gender to set
     */
    public void setGender(final String gender) {
        this.gender = gender;
    }

    /**
     * @return the birthday
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * @param birthday
     *            the birthday to set
     */
    public void setBirthday(final Date birthday) {
        this.birthday = birthday;
    }

    /**
     * @return the dueDate
     */
    public Date getDueDate() {
        return dueDate;
    }

    /**
     * @param dueDate
     *            the dueDate to set
     */
    public void setDueDate(final Date dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * @return the babyGender
     */
    public String getBabyGender() {
        return babyGender;
    }

    /**
     * @param babyGender
     *            the babyGender to set
     */
    public void setBabyGender(final String babyGender) {
        this.babyGender = babyGender;
    }


    /**
     * @return the childrenDetails
     */
    public List<TargetCustomerChildDetailsDto> getChildrenDetails() {
        return childrenDetails;
    }

    /**
     * @param childrenDetails
     *            the childrenDetails to set
     */
    public void setChildrenDetails(final List<TargetCustomerChildDetailsDto> childrenDetails) {
        this.childrenDetails = childrenDetails;
    }

    /**
     * @return the updateCustomerPersonalDetails
     */
    public boolean isUpdateCustomerPersonalDetails() {
        return updateCustomerPersonalDetails;
    }

    /**
     * @param updateCustomerPersonalDetails
     *            the updateCustomerPersonalDetails to set
     */
    public void setUpdateCustomerPersonalDetails(final boolean updateCustomerPersonalDetails) {
        this.updateCustomerPersonalDetails = updateCustomerPersonalDetails;
    }


    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).
                append("customerEmail", customerEmail).
                append("firstName", firstName).
                append("lastName", lastName).
                append("title", title).
                append("source", customerSubscriptionSource).
                append("subscriptionType", subscriptionType).
                append("gender", gender).
                append("birthday", birthday).
                append("duedate", dueDate).
                append("babyGender", babyGender).
                toString();
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the registerForEmail
     */
    public boolean isRegisterForEmail() {
        return registerForEmail;
    }

    /**
     * @param registerForEmail
     *            the registerForEmail to set
     */
    public void setRegisterForEmail(final boolean registerForEmail) {
        this.registerForEmail = registerForEmail;
    }


}

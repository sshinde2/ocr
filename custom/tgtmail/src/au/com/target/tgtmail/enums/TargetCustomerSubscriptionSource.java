/**
 * 
 */
package au.com.target.tgtmail.enums;

/**
 * @author mjanarth
 *
 */
public enum TargetCustomerSubscriptionSource {

    WIFI("wifi"),
    KIOSK("kiosk"),
    WEBACCOUNT("webAccount"),
    WEBCHECKOUT("webCheckout"),
    NEWSLETTER("newsLetter"),
    MUMSHUB("mumshub");



    private final String source;

    /**
     * 
     * @param source
     */
    private TargetCustomerSubscriptionSource(final String source) {
        this.source = source;
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }




}

/**
 * 
 */
package au.com.target.tgtmail.enums;

/**
 * @author mjanarth
 * 
 */
public enum TargetCustomerSubscriptionType {
    NEWSLETTER("Newsletter"),
    MUMS_HUB("MumsHub");

    private final String value;

    /**
     * 
     * @param response
     */
    private TargetCustomerSubscriptionType(final String response)
    {

        this.value = response;
    }

    /**
     * @return the response
     */
    public String getValue() {
        return value;
    }

}

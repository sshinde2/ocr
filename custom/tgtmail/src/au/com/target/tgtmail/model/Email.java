package au.com.target.tgtmail.model;

import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailAttachmentModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaService;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;


/**
 * Model representation for an email.
 * 
 * @author Olivier Lamy
 */
@XmlRootElement(name = "email")
public class Email
        implements Serializable
{

    private static final Logger LOG = Logger.getLogger(Email.class);

    private EmailAddressList toAddresses;

    private EmailAddressList ccAddresses;

    private EmailAddressList bccAddresses;

    private EmailAddress fromAddress;

    private EmailAddress replyToAddress;

    private String subject;

    private String body;

    private String charset = "UTF-8";

    private boolean htmlEmail = true;

    private List<EmailAttachment> emailAttachments;




    public Email()
    {
        // no op
    }


    public EmailAddressList getToAddresses()
    {
        if (toAddresses == null)
        {
            toAddresses = new EmailAddressList();
        }
        return toAddresses;
    }

    public void setToAddresses(final EmailAddressList toAddresses)
    {
        this.toAddresses = toAddresses;
    }

    public EmailAddressList getCcAddresses()
    {
        if (ccAddresses == null)
        {
            ccAddresses = new EmailAddressList();
        }
        return ccAddresses;
    }

    public void setCcAddresses(final EmailAddressList ccAddresses)
    {
        this.ccAddresses = ccAddresses;
    }

    public EmailAddressList getBccAddresses()
    {
        if (bccAddresses == null)
        {
            bccAddresses = new EmailAddressList();
        }
        return bccAddresses;
    }

    public void setBccAddresses(final EmailAddressList bccAddresses)
    {
        this.bccAddresses = bccAddresses;
    }

    public EmailAddress getFromAddress()
    {
        return fromAddress;
    }

    public void setFromAddress(final EmailAddress fromAddress)
    {
        this.fromAddress = fromAddress;
    }

    public EmailAddress getReplyToAddress()
    {
        return replyToAddress;
    }

    public void setReplyToAddress(final EmailAddress replyToAddress)
    {
        this.replyToAddress = replyToAddress;
    }

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(final String subject)
    {
        this.subject = subject;
    }

    public String getBody()
    {
        return body;
    }

    public void setBody(final String body)
    {
        this.body = body;
    }

    public boolean isHtmlEmail()
    {
        return htmlEmail;
    }

    public void setHtmlEmail(final boolean htmlEmail)
    {
        this.htmlEmail = htmlEmail;
    }

    public List<EmailAttachment> getEmailAttachments()
    {
        if (emailAttachments == null)
        {
            emailAttachments = new ArrayList<>();
        }
        return emailAttachments;
    }

    public void setEmailAttachments(final List<EmailAttachment> emailAttachments)
    {
        this.emailAttachments = emailAttachments;
    }

    public String getCharset()
    {
        return charset;
    }

    public void setCharset(final String charset)
    {
        this.charset = charset;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("Email{");
        sb.append("toAddresses=").append(toAddresses);
        sb.append(", ccAddresses=").append(ccAddresses);
        sb.append(", bccAddresses=").append(bccAddresses);
        sb.append(", fromAddress=").append(fromAddress);
        sb.append(", replyToAddress=").append(replyToAddress);
        sb.append(", subject='").append(subject).append('\'');
        sb.append(", body='").append(body).append('\'');
        sb.append(", charset='").append(charset).append('\'');
        sb.append(", htmlEmail=").append(htmlEmail);
        sb.append(", emailAttachments=").append(emailAttachments);
        sb.append('}');
        return sb.toString();
    }


    /**
     * Builder to use in order to build {@link Email} instances if your {@link EmailMessageModel} contains any
     * attachments you must provide a {@link MediaService} implementation in order to retrieve attachments bytes.
     * 
     * @author Olivier Lamy
     */
    public static class Builder {

        private EmailMessageModel emailMessageModel;

        private MediaService mediaService;

        public Builder() {
            // no op
        }

        /**
         * 
         * @param model
         *            bean to map
         * @return Builder
         */
        public Builder withEmailMessageModel(final EmailMessageModel model) {
            this.emailMessageModel = model;
            return this;
        }


        /**
         * 
         * @param themediaService
         *            the {@link MediaService} instance to use to get attachments
         * 
         * @return Builder
         */
        public Builder withMediaService(final MediaService themediaService) {
            this.mediaService = themediaService;
            return this;
        }

        public Email build() throws IOException {
            if (this.emailMessageModel == null) {
                throw new IllegalArgumentException("EmailMessageModel instance cannot be null");
            }

            final Email email = new Email();

            if (this.emailMessageModel.getFromAddress() == null) {
                throw new IllegalArgumentException("FromAddress cannot be null");
            }

            email.setFromAddress(
                    new EmailAddress.Builder().withEmailAddressModel(this.emailMessageModel.getFromAddress()).build());

            if (this.emailMessageModel.getToAddresses() == null || this.emailMessageModel.getToAddresses().isEmpty()) {
                throw new IllegalArgumentException("ToAddressess cannot be null");
            }

            for (final EmailAddressModel emailAddressModel : this.emailMessageModel.getToAddresses()) {

                email.getToAddresses().add(new EmailAddress.Builder()
                        .withEmailAddressModel(emailAddressModel)
                        .build());
            }


            if (this.emailMessageModel.getCcAddresses() != null && !this.emailMessageModel.getCcAddresses().isEmpty()) {
                for (final EmailAddressModel emailAddressModel : this.emailMessageModel.getCcAddresses()) {

                    email.getCcAddresses().add(
                            new EmailAddress.Builder().withEmailAddressModel(emailAddressModel).build());
                }
            }

            if (this.emailMessageModel.getBccAddresses() != null && !this.emailMessageModel.getBccAddresses().isEmpty()) {

                for (final EmailAddressModel emailAddressModel : this.emailMessageModel.getBccAddresses()) {

                    email.getBccAddresses().add(new EmailAddress.Builder()
                            .withEmailAddressModel(emailAddressModel)
                            .build());
                }
            }
            //when message is > 2000 characters body gets stored as a media file.
            if (null != emailMessageModel.getBodyMedia()) {

                try {
                    final MediaModel mediaModel = emailMessageModel.getBodyMedia();
                    final String content = convertMediaFileToString(mediaModel, email.charset);
                    email.setBody(content);
                }
                catch (final IOException ex) {
                    throw ex;
                }

            }
            else {
                LOG.debug("Media does not exist therefore using the body of the message");
                email.setBody(this.emailMessageModel.getBody());
            }

            email.setSubject(this.emailMessageModel.getSubject());


            final boolean hasAttachments = this.emailMessageModel.getAttachments() != null &&
                    !this.emailMessageModel.getAttachments().isEmpty();

            if (hasAttachments && this.mediaService == null) {
                throw new IllegalArgumentException(
                        "If you have attachments you must configure the media service implementation to use");
            }
            if (hasAttachments) {
                for (final EmailAttachmentModel attachment : this.emailMessageModel.getAttachments())
                {
                    final byte[] attachmentBytes = this.mediaService.getDataFromMedia(attachment);
                    final String type = attachment.getMime();
                    email.getEmailAttachments().add(
                            new EmailAttachment(type, attachmentBytes, attachment.getRealFileName(), attachment
                                    .getAltText()));
                }
            }

            return email;
        }

        public String convertMediaFileToString(final MediaModel mediaModel, final String charset) throws IOException {

            final byte[] contentBytes = this.mediaService.getDataFromMedia(mediaModel);
            final String content = new String(contentBytes, charset);
            return content;

        }
    }



}

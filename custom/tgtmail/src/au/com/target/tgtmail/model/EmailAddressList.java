/**
 * 
 */
package au.com.target.tgtmail.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;


/**
 * @author Olivier Lamy
 */
public class EmailAddressList implements Serializable {

    private List<EmailAddress> emailAddresses = new ArrayList<EmailAddress>(1);

    public EmailAddressList()
    {
        // no op
    }


    public EmailAddressList(final List<EmailAddress> emailAddresses)
    {
        this.emailAddresses = emailAddresses;
    }

    @XmlElement(name = "emailAddress")
    public List<EmailAddress> getEmailAddresses()
    {
        return emailAddresses;
    }

    public void setEmailAddresses(final List<EmailAddress> emailAddresses)
    {
        this.emailAddresses = emailAddresses;
    }

    public void add(final EmailAddress emailAddress)
    {
        if (this.emailAddresses == null)
        {
            this.emailAddresses = new ArrayList<>();
        }
        this.emailAddresses.add(emailAddress);
    }
}

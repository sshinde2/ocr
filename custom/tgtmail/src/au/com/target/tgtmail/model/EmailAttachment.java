package au.com.target.tgtmail.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.codec.binary.BinaryCodec;


/**
 * Model representation for an email attachment.
 * 
 * @author Olivier Lamy
 */
@XmlRootElement(name = "emailAttachment")
@XmlAccessorType(XmlAccessType.FIELD)
public class EmailAttachment
        implements Serializable
{

    private String type;

    private byte[] bytes;

    private String fileName;

    private String altText;

    /**
     * to prevent encoding issue when transferring bytes are stored as an array of ASCII 0 and 1 characters.
     */
    private transient BinaryCodec binaryCodec = new BinaryCodec();

    public EmailAttachment()
    {
        // no op
    }

    public EmailAttachment(final String type, final byte[] bytes, final String fileName, final String altText)
    {
        this.type = type;
        this.bytes = binaryCodec.encode(bytes);
        this.fileName = fileName;
        this.altText = altText;
    }


    public String getType()
    {
        return type;
    }

    public void setType(final String type)
    {
        this.type = type;
    }

    public byte[] getBytes()
    {
        return binaryCodec.decode(bytes);
    }

    public void setBytes(final byte[] bytes)
    {
        this.bytes = binaryCodec.encode(bytes);
    }

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(final String fileName)
    {
        this.fileName = fileName;
    }

    public String getAltText()
    {
        return altText;
    }

    public void setAltText(final String altText)
    {
        this.altText = altText;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("EmailAttachment{");
        sb.append("altText='").append(altText).append('\'');
        sb.append(", fileName='").append(fileName).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

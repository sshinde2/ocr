/**
 * 
 */
package au.com.target.tgtmail.service.impl;

import java.lang.reflect.InvocationTargetException;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.constants.TgtbusprocConstants;
import au.com.target.tgtcore.customer.TargetCustomerAccountService;
import au.com.target.tgtmail.client.TargetCustomerSubscriptionClient;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionResponseDto;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionResponseType;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionType;
import au.com.target.tgtmail.service.TargetCustomerSubscriptionService;


/**
 * @author rmcalave
 * 
 */
public class TargetCustomerSubscriptionServiceImpl implements TargetCustomerSubscriptionService {

    private static final Logger LOG = Logger.getLogger(TargetCustomerSubscriptionServiceImpl.class);

    private TargetCustomerSubscriptionClient targetCustomerSubscriptionClient;

    @Resource
    private TargetCustomerAccountService targetCustomerAccountService;

    @Override
    public boolean processCustomerSubscription(final TargetCustomerSubscriptionRequestDto updateCustomerSubscriptionDto) {
        boolean success = false;
        try {
            if (updateCustomerSubscriptionDto != null
                    && updateCustomerSubscriptionDto.getSubscriptionType().equals(
                            TargetCustomerSubscriptionType.NEWSLETTER)) {
                getTargetBusinessProcessService().startCustomerSubscriptionProcess(updateCustomerSubscriptionDto,
                        TgtbusprocConstants.BusinessProcess.CUSTOMER_NEWSLETTER_SUBSCRIPTION_PROCESS);
                success = true;
            }
            else if (updateCustomerSubscriptionDto != null
                    && updateCustomerSubscriptionDto.getSubscriptionType().equals(
                            TargetCustomerSubscriptionType.MUMS_HUB)) {
                getTargetBusinessProcessService().startCustomerSubscriptionProcess(updateCustomerSubscriptionDto,
                        TgtbusprocConstants.BusinessProcess.CUSTOMER_SUBSCRIPTION_PROCESS);
                success = true;
            }

        }
        catch (final IllegalAccessException e) {
            LOG.error(e.getMessage(), e);
        }
        catch (final InvocationTargetException e) {
            LOG.error(e.getMessage(), e);
        }
        catch (final NoSuchMethodException e) {
            LOG.error(e.getMessage(), e);
        }
        return success;
    }

    @Override
    public TargetCustomerSubscriptionResponseDto createCustomerSubscription(
            final TargetCustomerSubscriptionRequestDto targetCustomerSubscriptionRequestDto) {
        final TargetCustomerSubscriptionResponseDto response = targetCustomerSubscriptionClient
                .sendCustomerSubscription(targetCustomerSubscriptionRequestDto);
        final TargetCustomerSubscriptionResponseType responseType = response.getResponseType();
        if (TargetCustomerSubscriptionResponseType.SUCCESS == responseType
                || TargetCustomerSubscriptionResponseType.ALREADY_EXISTS == responseType) {
            updateSubscrptionKey(targetCustomerSubscriptionRequestDto.getCustomerEmail(), response);
        }
        return response;
    }


    @Override
    public boolean processCustomerPersonalDetails(
            final TargetCustomerSubscriptionRequestDto updateCustomerSubscriptionDto) {
        boolean success = false;
        try {
            updateCustomerSubscriptionDto.setUpdateCustomerPersonalDetails(true);
            getTargetBusinessProcessService().startCustomerSubscriptionProcess(
                    updateCustomerSubscriptionDto,
                    TgtbusprocConstants.BusinessProcess.CUSTOMER_PERSONALDETAILS_SUBSCRIPTION_PROCESS);
            success = true;
        }
        catch (final IllegalAccessException e) {
            LOG.error(e.getMessage(), e);
        }
        catch (final InvocationTargetException e) {
            LOG.error(e.getMessage(), e);
        }
        catch (final NoSuchMethodException e) {
            LOG.error(e.getMessage(), e);
        }
        return success;

    }

    /**
     * The method is based on the response message is the subscriber key if it is numeric.
     * 
     * @param email
     * @param response
     */
    private void updateSubscrptionKey(final String email, final TargetCustomerSubscriptionResponseDto response) {
        final String subscribeKey = response.getResponseMessage();
        if (StringUtils.isNumeric(subscribeKey)) {
            LOG.info("Try to map customer subscribe key:" + subscribeKey + " to email:" + email);
            final boolean update = targetCustomerAccountService.updateTargetCustomerWithSubscrptionKey(email,
                    subscribeKey);
            LOG.info("Map customer subscribe key:" + subscribeKey + " to email:" + email + " Result:" + update);
        }
    }

    /**
     * @param targetCustomerSubscriptionClient
     *            the targetCustomerSubscriptionClient to set
     */
    @Required
    public void setTargetCustomerSubscriptionClient(
            final TargetCustomerSubscriptionClient targetCustomerSubscriptionClient) {
        this.targetCustomerSubscriptionClient = targetCustomerSubscriptionClient;
    }

    /**
     * 
     * @return TargetBusinessProcessService
     */
    public TargetBusinessProcessService getTargetBusinessProcessService() {
        throw new UnsupportedOperationException(
                "Please define in the spring configuration a <lookup-method> for getTargetBusinessProcessService().");
    }

    public TargetCustomerAccountService getTargetCustomerAccountService() {
        return targetCustomerAccountService;
    }

    /**
     * @param targetCustomerAccountService
     *            the targetCustomerAccountService to set
     */
    public void setTargetCustomerAccountService(final TargetCustomerAccountService targetCustomerAccountService) {
        this.targetCustomerAccountService = targetCustomerAccountService;
    }



}

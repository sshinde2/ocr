/**
 * 
 */
package au.com.target.tgtmail.model;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaService;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Arrays;

import javax.xml.bind.JAXBContext;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtmail.model.Email.Builder;




/**
 * @author Olivier Lamy
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EmailModelTest {

    private static final Logger LOGGER = Logger.getLogger(EmailModelTest.class);
    private static final String RESULTBODY = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><email><bccAddresses><emailAddress><displayName>Bcc</displayName><address>bcc@olamy.fr</address></emailAddress><emailAddress><displayName>Bcc2</displayName><address>bcc2@olamy.fr</address></emailAddress></bccAddresses><body>&lt;b&gt;the body&lt;/b&gt;</body><ccAddresses><emailAddress><displayName>Cc</displayName><address>cc@olamy.fr</address></emailAddress></ccAddresses><charset>UTF-8</charset><fromAddress><displayName>Yup</displayName><address>yup@yumi.fr</address></fromAddress><htmlEmail>true</htmlEmail><toAddresses><emailAddress><displayName>Pale Ale</displayName><address>pale-ale@olamy.fr</address></emailAddress></toAddresses></email>";
    private static final String RESULTWITHMEDIABODY = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><email><bccAddresses><emailAddress><displayName>Bcc</displayName><address>bcc@olamy.fr</address></emailAddress><emailAddress><displayName>Bcc2</displayName><address>bcc2@olamy.fr</address></emailAddress></bccAddresses><body>Hello</body><ccAddresses><emailAddress><displayName>Cc</displayName><address>cc@olamy.fr</address></emailAddress></ccAddresses><charset>UTF-8</charset><fromAddress><displayName>Yup</displayName><address>yup@yumi.fr</address></fromAddress><htmlEmail>true</htmlEmail><toAddresses><emailAddress><displayName>Pale Ale</displayName><address>pale-ale@olamy.fr</address></emailAddress></toAddresses></email>";
    private static final String RESULTWITHMEDIANULLBODY = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><email><bccAddresses><emailAddress><displayName>Bcc</displayName><address>bcc@olamy.fr</address></emailAddress><emailAddress><displayName>Bcc2</displayName><address>bcc2@olamy.fr</address></emailAddress></bccAddresses><ccAddresses><emailAddress><displayName>Cc</displayName><address>cc@olamy.fr</address></emailAddress></ccAddresses><charset>UTF-8</charset><fromAddress><displayName>Yup</displayName><address>yup@yumi.fr</address></fromAddress><htmlEmail>true</htmlEmail><toAddresses><emailAddress><displayName>Pale Ale</displayName><address>pale-ale@olamy.fr</address></emailAddress></toAddresses></email>";
    private static final String CHARSET = "Utf-8";

    private JAXBContext jaxbContext;



    @Mock
    private MediaService mediaService;

    @Mock
    private EmailMessageModel emailMessageModel;

    @Mock
    private MediaModel mediaModel;


    @Before
    public void setup() throws Exception
    {

        jaxbContext = JAXBContext.newInstance(Email.class);


    }

    @Test
    public void emailToXml() throws Exception
    {
        final StringWriter sw = new StringWriter();
        final Email email = buildEmail();
        email.setFromAddress(new EmailAddress("Yup", "yup@yumi.fr"));
        email.setToAddresses(
                new EmailAddressList(Arrays.asList(new EmailAddress("Pale Ale", "pale-ale@olamy.fr"))));
        email.setCcAddresses(new EmailAddressList(Arrays.asList(new EmailAddress("Cc", "cc@olamy.fr"))));
        email.setBccAddresses(new EmailAddressList(
                Arrays.asList(new EmailAddress("Bcc", "bcc@olamy.fr"), new EmailAddress("Bcc2", "bcc2@olamy.fr"))));
        jaxbContext.createMarshaller().marshal(email, sw);
        final String xml = sw.toString();
        Assert.assertEquals(RESULTBODY, xml);
        LOGGER.info("xml: " + xml);
        jaxbContext.createUnmarshaller().unmarshal(new StringReader(xml));
    }

    @Test
    public void emailToXmlWithMedia() throws Exception
    {
        final StringWriter sw = new StringWriter();
        final Email email = buildEmailWithMedia();
        email.setFromAddress(new EmailAddress("Yup", "yup@yumi.fr"));
        email.setToAddresses(
                new EmailAddressList(Arrays.asList(new EmailAddress("Pale Ale", "pale-ale@olamy.fr"))));
        email.setCcAddresses(new EmailAddressList(Arrays.asList(new EmailAddress("Cc", "cc@olamy.fr"))));
        email.setBccAddresses(new EmailAddressList(
                Arrays.asList(new EmailAddress("Bcc", "bcc@olamy.fr"), new EmailAddress("Bcc2", "bcc2@olamy.fr"))));
        jaxbContext.createMarshaller().marshal(email, sw);
        final String xml = sw.toString();
        LOGGER.info("xml: " + xml);
        Assert.assertEquals(RESULTWITHMEDIABODY, xml);
        jaxbContext.createUnmarshaller().unmarshal(new StringReader(xml));
    }

    @Test
    public void emailToXmlWithMediaNull() throws Exception
    {
        final StringWriter sw = new StringWriter();
        final Email email = buildEmailWithMediaNull();
        email.setFromAddress(new EmailAddress("Yup", "yup@yumi.fr"));
        email.setToAddresses(
                new EmailAddressList(Arrays.asList(new EmailAddress("Pale Ale", "pale-ale@olamy.fr"))));
        email.setCcAddresses(new EmailAddressList(Arrays.asList(new EmailAddress("Cc", "cc@olamy.fr"))));
        email.setBccAddresses(new EmailAddressList(
                Arrays.asList(new EmailAddress("Bcc", "bcc@olamy.fr"), new EmailAddress("Bcc2", "bcc2@olamy.fr"))));
        jaxbContext.createMarshaller().marshal(email, sw);
        final String xml = sw.toString();
        LOGGER.info("xml: " + xml);
        Assert.assertEquals(RESULTWITHMEDIANULLBODY, xml);
        jaxbContext.createUnmarshaller().unmarshal(new StringReader(xml));
    }


    private static Email buildEmail()
    {
        final Email email = new Email();
        email.setBody("<b>the body</b>");
        return email;
    }

    private static Email buildEmailWithMedia() throws Exception
    {
        final MediaModel mockMediaModel = Mockito.mock(MediaModel.class);
        final Builder mockBuilder = Mockito.mock(Email.Builder.class);
        final String responseString = "Hello";
        final EmailMessageModel emailMessageModel = Mockito.mock(EmailMessageModel.class);
        BDDMockito.given(emailMessageModel.getBodyMedia()).willReturn(mockMediaModel);
        BDDMockito.given(mockBuilder.convertMediaFileToString(mockMediaModel, CHARSET)).willReturn(
                responseString);
        final String body = mockBuilder.convertMediaFileToString(mockMediaModel, CHARSET);
        final Email email = new Email();
        email.setBody(body);
        return email;
    }

    private static Email buildEmailWithMediaNull() throws Exception
    {
        final MediaModel mockMediaModel = Mockito.mock(MediaModel.class);
        final Builder mockBuilder = Mockito.mock(Email.Builder.class);
        final EmailMessageModel emailMessageModel = Mockito.mock(EmailMessageModel.class);
        BDDMockito.given(emailMessageModel.getBodyMedia()).willReturn(mockMediaModel);
        BDDMockito.given(mockBuilder.convertMediaFileToString(mockMediaModel, CHARSET)).willReturn(
                null);
        final String body = mockBuilder.convertMediaFileToString(mockMediaModel, CHARSET);
        final Email email = new Email();
        email.setBody(body);
        return email;
    }



}

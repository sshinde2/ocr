package au.com.target.tgtmail.service;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.constants.TgtbusprocConstants;
import au.com.target.tgtcore.customer.TargetCustomerAccountService;
import au.com.target.tgtmail.client.TargetCustomerSubscriptionClient;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionResponseDto;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionResponseType;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionSource;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionType;
import au.com.target.tgtmail.service.impl.TargetCustomerSubscriptionServiceImpl;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCustomerSubscriptionServiceImplTest {
    @Mock
    private TargetBusinessProcessService mockTargetBusinessProcessService;

    @Mock
    private TargetCustomerSubscriptionClient targetCustomerSubscriptionClient;

    @Mock
    private TargetCustomerAccountService targetCustomerAccountService;

    @Spy
    @InjectMocks
    private final TargetCustomerSubscriptionServiceImpl targetCustomerSubscriptionServiceImpl = new TargetCustomerSubscriptionServiceImpl();

    @Test
    public void testProcessNewsletterSubscription() throws Exception {
        BDDMockito.doReturn(mockTargetBusinessProcessService).when(targetCustomerSubscriptionServiceImpl)
                .getTargetBusinessProcessService();
        final TargetCustomerSubscriptionRequestDto updateCustomerSubscriptionDto = createRequest();

        updateCustomerSubscriptionDto.setSubscriptionType(TargetCustomerSubscriptionType.NEWSLETTER);
        final ArgumentCaptor<TargetCustomerSubscriptionRequestDto> requestDtoCaptor = ArgumentCaptor
                .forClass(TargetCustomerSubscriptionRequestDto.class);

        final boolean result = targetCustomerSubscriptionServiceImpl
                .processCustomerSubscription(updateCustomerSubscriptionDto);

        assertThat(result).isTrue();

        verify(mockTargetBusinessProcessService).startCustomerSubscriptionProcess(requestDtoCaptor.capture(),
                Mockito.eq(TgtbusprocConstants.BusinessProcess.CUSTOMER_NEWSLETTER_SUBSCRIPTION_PROCESS));

        final TargetCustomerSubscriptionRequestDto capturedDto = requestDtoCaptor.getValue();
        assertThat(capturedDto.getCustomerEmail()).isEqualTo(updateCustomerSubscriptionDto.getCustomerEmail());
        assertThat(capturedDto.getTitle()).isEqualTo(updateCustomerSubscriptionDto.getTitle());
        assertThat(capturedDto.getFirstName()).isEqualTo(updateCustomerSubscriptionDto.getFirstName());
    }

    @Test
    public void testProcessMumshubSubscription() throws Exception {
        BDDMockito.doReturn(mockTargetBusinessProcessService).when(targetCustomerSubscriptionServiceImpl)
                .getTargetBusinessProcessService();
        final TargetCustomerSubscriptionRequestDto updateCustomerSubscriptionDto = createRequest();

        updateCustomerSubscriptionDto.setSubscriptionType(TargetCustomerSubscriptionType.MUMS_HUB);
        final ArgumentCaptor<TargetCustomerSubscriptionRequestDto> requestDtoCaptor = ArgumentCaptor
                .forClass(TargetCustomerSubscriptionRequestDto.class);

        final boolean result = targetCustomerSubscriptionServiceImpl
                .processCustomerSubscription(updateCustomerSubscriptionDto);

        assertThat(result).isTrue();

        verify(mockTargetBusinessProcessService).startCustomerSubscriptionProcess(requestDtoCaptor.capture(),
                Mockito.eq(TgtbusprocConstants.BusinessProcess.CUSTOMER_SUBSCRIPTION_PROCESS));

        final TargetCustomerSubscriptionRequestDto capturedDto = requestDtoCaptor.getValue();
        assertThat(capturedDto.getCustomerEmail()).isEqualTo(updateCustomerSubscriptionDto.getCustomerEmail());
        assertThat(capturedDto.getTitle()).isEqualTo(updateCustomerSubscriptionDto.getTitle());
        assertThat(capturedDto.getFirstName()).isEqualTo(updateCustomerSubscriptionDto.getFirstName());
    }

    @Test
    public void testCreateENewsSubscription() {
        final TargetCustomerSubscriptionRequestDto updateCustomerSubscriptionDto = createRequest();
        final TargetCustomerSubscriptionResponseDto response = mock(TargetCustomerSubscriptionResponseDto.class);
        when(targetCustomerSubscriptionClient.sendCustomerSubscription(any(TargetCustomerSubscriptionRequestDto.class)))
                .thenReturn(response);
        when(response.getResponseType()).thenReturn(TargetCustomerSubscriptionResponseType.SUCCESS);

        final TargetCustomerSubscriptionResponseDto result = targetCustomerSubscriptionServiceImpl
                .createCustomerSubscription(updateCustomerSubscriptionDto);

        verify(targetCustomerSubscriptionClient).sendCustomerSubscription(
                any(TargetCustomerSubscriptionRequestDto.class));
        assertNotNull(result);
        assertEquals("success", result.getResponseType().getResponse());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testShouldUpdateCustomerWithNewSubscribeKey() {
        final TargetCustomerSubscriptionRequestDto updateCustomerSubscriptionDto = createRequest();
        final TargetCustomerSubscriptionResponseDto response = mock(TargetCustomerSubscriptionResponseDto.class);
        when(targetCustomerSubscriptionClient.sendCustomerSubscription(any(TargetCustomerSubscriptionRequestDto.class)))
                .thenReturn(response);
        when(response.getResponseMessage()).thenReturn("12345");
        when(response.getResponseType()).thenReturn(TargetCustomerSubscriptionResponseType.SUCCESS);
        when(targetCustomerAccountService.updateTargetCustomerWithSubscrptionKey("foo@bar.com", "12345")).thenReturn(
                true);

        final TargetCustomerSubscriptionResponseDto result = targetCustomerSubscriptionServiceImpl
                .createCustomerSubscription(updateCustomerSubscriptionDto);
        assertEquals("12345", result.getResponseMessage());
        verify(targetCustomerAccountService).updateTargetCustomerWithSubscrptionKey("foo@bar.com", "12345");
    }

    @SuppressWarnings("boxing")
    @Test
    public void testShouldUpdateCustomerWithAlreadyExistsSubscribeKey() {
        final TargetCustomerSubscriptionRequestDto updateCustomerSubscriptionDto = createRequest();
        final TargetCustomerSubscriptionResponseDto response = mock(TargetCustomerSubscriptionResponseDto.class);
        when(targetCustomerSubscriptionClient.sendCustomerSubscription(any(TargetCustomerSubscriptionRequestDto.class)))
                .thenReturn(response);
        when(response.getResponseMessage()).thenReturn("12345");
        when(response.getResponseType()).thenReturn(TargetCustomerSubscriptionResponseType.ALREADY_EXISTS);
        when(targetCustomerAccountService.updateTargetCustomerWithSubscrptionKey("foo@bar.com", "12345")).thenReturn(
                true);

        final TargetCustomerSubscriptionResponseDto result = targetCustomerSubscriptionServiceImpl
                .createCustomerSubscription(updateCustomerSubscriptionDto);
        assertEquals("12345", result.getResponseMessage());
        verify(targetCustomerAccountService).updateTargetCustomerWithSubscrptionKey("foo@bar.com", "12345");
    }

    @SuppressWarnings("boxing")
    @Test
    public void testShouldUpdateCustomerWithNewSubscribeKeyButFailedCode() {
        final TargetCustomerSubscriptionRequestDto updateCustomerSubscriptionDto = createRequest();
        final TargetCustomerSubscriptionResponseDto response = mock(TargetCustomerSubscriptionResponseDto.class);
        when(targetCustomerSubscriptionClient.sendCustomerSubscription(any(TargetCustomerSubscriptionRequestDto.class)))
                .thenReturn(response);
        when(response.getResponseMessage()).thenReturn("12345");
        when(response.getResponseType()).thenReturn(TargetCustomerSubscriptionResponseType.OTHERS);
        when(targetCustomerAccountService.updateTargetCustomerWithSubscrptionKey("foo@bar.com", "12345")).thenReturn(
                true);

        final TargetCustomerSubscriptionResponseDto result = targetCustomerSubscriptionServiceImpl
                .createCustomerSubscription(updateCustomerSubscriptionDto);
        assertEquals("12345", result.getResponseMessage());
        verifyZeroInteractions(targetCustomerAccountService);
    }

    @Test
    public void testShouldNotUpdateCustomerIfNoNewSubscribeKey() {
        final TargetCustomerSubscriptionRequestDto updateCustomerSubscriptionDto = createRequest();
        final TargetCustomerSubscriptionResponseDto response = mock(TargetCustomerSubscriptionResponseDto.class);
        when(targetCustomerSubscriptionClient.sendCustomerSubscription(any(TargetCustomerSubscriptionRequestDto.class)))
                .thenReturn(response);
        when(response.getResponseMessage()).thenReturn("Message");

        final TargetCustomerSubscriptionResponseDto result = targetCustomerSubscriptionServiceImpl
                .createCustomerSubscription(updateCustomerSubscriptionDto);
        assertEquals("Message", result.getResponseMessage());
        verifyZeroInteractions(targetCustomerAccountService);
    }

    @Test
    public void testShouldNotUpdateCustomerIfNullSubscribeKey() {
        final TargetCustomerSubscriptionRequestDto updateCustomerSubscriptionDto = createRequest();
        final TargetCustomerSubscriptionResponseDto response = mock(TargetCustomerSubscriptionResponseDto.class);
        when(targetCustomerSubscriptionClient.sendCustomerSubscription(any(TargetCustomerSubscriptionRequestDto.class)))
                .thenReturn(response);
        when(response.getResponseMessage()).thenReturn(null);

        final TargetCustomerSubscriptionResponseDto result = targetCustomerSubscriptionServiceImpl
                .createCustomerSubscription(updateCustomerSubscriptionDto);
        assertNull(result.getResponseMessage());
        verifyZeroInteractions(targetCustomerAccountService);
    }

    @Test(expected = NullPointerException.class)
    public void testShouldThrowExceptionWhenUpdateCustomerWithNullRequest() {
        targetCustomerSubscriptionServiceImpl.createCustomerSubscription(null);
    }

    @Test(expected = NullPointerException.class)
    public void testShouldThrowExceptionWhenUpdateCustomerWithNullEmail() {
        targetCustomerSubscriptionServiceImpl.createCustomerSubscription(new TargetCustomerSubscriptionRequestDto());
    }

    @Test
    public void testProcessCustomerPersonalDetailsSubscription() throws Exception {
        BDDMockito.doReturn(mockTargetBusinessProcessService).when(targetCustomerSubscriptionServiceImpl)
                .getTargetBusinessProcessService();
        final TargetCustomerSubscriptionRequestDto updateCustomerSubscriptionDto = createCustomerPersonalDetailsRequest();
        final boolean result = targetCustomerSubscriptionServiceImpl
                .processCustomerPersonalDetails(updateCustomerSubscriptionDto);
        assertThat(result).isTrue();
        verify(mockTargetBusinessProcessService).startCustomerSubscriptionProcess(
                updateCustomerSubscriptionDto,
                TgtbusprocConstants.BusinessProcess.CUSTOMER_PERSONALDETAILS_SUBSCRIPTION_PROCESS);
    }

    private TargetCustomerSubscriptionRequestDto createRequest() {
        final TargetCustomerSubscriptionRequestDto updateCustomerSubscriptionDto = new TargetCustomerSubscriptionRequestDto();
        updateCustomerSubscriptionDto.setCustomerEmail("foo@bar.com");
        updateCustomerSubscriptionDto.setTitle("Mr");
        updateCustomerSubscriptionDto.setFirstName("James");
        return updateCustomerSubscriptionDto;
    }

    private TargetCustomerSubscriptionRequestDto createCustomerPersonalDetailsRequest() {
        final TargetCustomerSubscriptionRequestDto updateCustomerSubscriptionDto = new TargetCustomerSubscriptionRequestDto();
        updateCustomerSubscriptionDto.setCustomerEmail("foo@bar.com");
        updateCustomerSubscriptionDto.setTitle("Ms");
        updateCustomerSubscriptionDto.setFirstName("Firstname");
        updateCustomerSubscriptionDto.setGender("female");
        updateCustomerSubscriptionDto.setBabyGender("female");
        updateCustomerSubscriptionDto.setSubscriptionType(TargetCustomerSubscriptionType.MUMS_HUB);
        updateCustomerSubscriptionDto.setCustomerSubscriptionSource(TargetCustomerSubscriptionSource.MUMSHUB);
        return updateCustomerSubscriptionDto;
    }
}

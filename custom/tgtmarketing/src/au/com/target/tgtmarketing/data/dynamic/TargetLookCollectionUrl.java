/**
 * 
 */
package au.com.target.tgtmarketing.data.dynamic;

import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtmarketing.model.TargetLookCollectionModel;


/**
 * @author mgazal
 *
 */
public class TargetLookCollectionUrl extends AbstractUrlResolver<TargetLookCollectionModel>
        implements DynamicAttributeHandler<String, TargetLookCollectionModel> {

    private String lookCollectionUrlPrefix;

    @Override
    public String get(final TargetLookCollectionModel lookCollection) {
        return getLookCollectionUrlPrefix()
                + urlSafe(lookCollection.getShopTheLook().getName()).toLowerCase() + "/"
                + lookCollection.getShopTheLook().getId() + "#" + lookCollection.getId();
    }

    @Override
    public void set(final TargetLookCollectionModel lookCollection, final String url) {
        throw new UnsupportedOperationException();
    }

    /**
     * @return the lookCollectionUrlPrefix
     */
    public String getLookCollectionUrlPrefix() {
        return lookCollectionUrlPrefix;
    }

    /**
     * @param lookCollectionUrlPrefix
     *            the lookCollectionUrlPrefix to set
     */
    @Required
    public void setLookCollectionUrlPrefix(final String lookCollectionUrlPrefix) {
        this.lookCollectionUrlPrefix = lookCollectionUrlPrefix;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver#resolveInternal(java.lang.Object)
     */
    @Override
    protected String resolveInternal(final TargetLookCollectionModel collection) {
        return get(collection);
    }
}

/**
 * 
 */
package au.com.target.tgtmarketing.data.dynamic;

import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtmarketing.model.TargetLookModel;


/**
 * @author mgazal
 *
 */
public class TargetLookUrl extends AbstractUrlResolver<TargetLookModel>
        implements DynamicAttributeHandler<String, TargetLookModel> {

    private String lookUrlPrefix;

    @Override
    public String get(final TargetLookModel look) {
        return getLookUrlPrefix() + urlSafe(look.getName()).toLowerCase() + "/" + look.getId();
    }

    @Override
    public void set(final TargetLookModel look, final String url) {
        throw new UnsupportedOperationException();
    }

    /**
     * @return the lookUrlPrefix
     */
    public String getLookUrlPrefix() {
        return lookUrlPrefix;
    }

    /**
     * @param lookUrlPrefix
     *            the lookUrlPrefix to set
     */
    @Required
    public void setLookUrlPrefix(final String lookUrlPrefix) {
        this.lookUrlPrefix = lookUrlPrefix;
    }

    /* (non-Javadoc)
     * @see de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver#resolveInternal(java.lang.Object)
     */
    @Override
    protected String resolveInternal(final TargetLookModel look) {
        return get(look);
    }
}

package au.com.target.tgtmarketing.deals.dao;

import de.hybris.platform.promotions.model.AbstractDealModel;

import java.util.List;



/**
 * DAO to get deal information related to mobile
 */
public interface TargetMobileDealsDao {

    /**
     * @return list of active deals for Mobile
     */
    List<AbstractDealModel> getAllActiveMobileDeals();

}

package au.com.target.tgtmarketing.deals.dao.impl;

import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtmarketing.deals.dao.TargetMobileDealsDao;


/**
 * Default implementation of {@link TargetMobileDealsDao}
 * 
 */
public class TargetMobileDealsDaoImpl extends DefaultGenericDao<AbstractDealModel> implements TargetMobileDealsDao {

    private static final String FIND_ALL_ACTIVE_MOBILE_DEALS = "SELECT {" + AbstractDealModel.PK + "} FROM {"
            + AbstractDealModel._TYPECODE
            + "} WHERE ({" + AbstractDealModel.ENDDATE + "} IS NULL OR {" + AbstractDealModel.ENDDATE + "} >= ?now ) "
            + " AND ({" + AbstractDealModel.STARTDATE + "} IS NULL OR {" + AbstractDealModel.STARTDATE + "} <= ?now ) "
            + " AND ( {" + AbstractDealModel.AVAILABLEFORMOBILE + "} = ?active ) "
            + " AND ({" + AbstractDealModel.IMMUTABLEKEYHASH + "} IS NULL)";



    public TargetMobileDealsDaoImpl() {
        super(AbstractDealModel._TYPECODE);
    }


    @Override
    public List<AbstractDealModel> getAllActiveMobileDeals() {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("now", new Date());
        params.put("active", Boolean.TRUE);
        final SearchResult<AbstractDealModel> searchResult = getFlexibleSearchService().search(
                FIND_ALL_ACTIVE_MOBILE_DEALS, params);
        return searchResult.getResult();
    }
}

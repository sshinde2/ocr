package au.com.target.tgtmarketing.deals.impl;

import de.hybris.platform.promotions.model.AbstractDealModel;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtmarketing.deals.TargetMobileDealService;
import au.com.target.tgtmarketing.deals.dao.TargetMobileDealsDao;



/**
 * 
 * Default implementation of {@link TargetMobileDealService}
 * 
 */
public class TargetMobileDealServiceImpl implements TargetMobileDealService {

    private TargetMobileDealsDao targetMobileDealDao;


    @Override
    public List<AbstractDealModel> getAllActiveMobileDeals() {
        final List<AbstractDealModel> activeDeals = targetMobileDealDao.getAllActiveMobileDeals();
        return activeDeals;
    }

    /**
     * @param targetMobileDealDao
     *            the targetDealDao to set
     */
    @Required
    public void setTargetMobileDealDao(final TargetMobileDealsDao targetMobileDealDao) {
        this.targetMobileDealDao = targetMobileDealDao;
    }

}

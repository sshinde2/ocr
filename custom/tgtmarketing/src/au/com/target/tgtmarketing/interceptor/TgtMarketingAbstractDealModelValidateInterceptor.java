/**
 * 
 */
package au.com.target.tgtmarketing.interceptor;

import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author pthoma20
 * 
 */
public class TgtMarketingAbstractDealModelValidateInterceptor implements ValidateInterceptor {


    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof AbstractDealModel)) {
            return;
        }

        final AbstractDealModel abstractDeal = (AbstractDealModel)model;

        if (Boolean.TRUE.equals(abstractDeal.getAvailableForMobile())) {

            if (CollectionUtils.isEmpty(abstractDeal.getTargetMobileOfferHeadings())) {
                throw new InterceptorException(ErrorMessages.ERROR_DEAL_AVAILABLE_MOBILE
                        + ErrorMessages.ASSOCIATED_MOBILE_OFFER_HEADING_REQUIRED);
            }

            if (TgtMarketingInterceptorUtil.mobileOfferHeadingContainDuplicates(abstractDeal
                    .getTargetMobileOfferHeadings())) {
                throw new InterceptorException(ErrorMessages.ERROR_DEAL_AVAILABLE_MOBILE
                        + ErrorMessages.DUPLICATE_MOBILE_OFFER_HEADING);
            }


        }
    }

    protected interface ErrorMessages {
        public static final String ERROR_DEAL_AVAILABLE_MOBILE = "Deal can be Available For Mobile only when -";
        public static final String BOLD_TITLE_REQUIRED = "Bold Title Present.";
        public static final String ASSOCIATED_MOBILE_OFFER_HEADING_REQUIRED = "At least one Mobile Offer Heading Present.";
        public static final String DUPLICATE_MOBILE_OFFER_HEADING = "No Duplicate Mobile Offer Heading Present.";
    }

}

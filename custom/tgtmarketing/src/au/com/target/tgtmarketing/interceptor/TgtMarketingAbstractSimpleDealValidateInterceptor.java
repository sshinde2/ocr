/**
 * 
 */
package au.com.target.tgtmarketing.interceptor;

import de.hybris.platform.promotions.model.AbstractSimpleDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.lang.StringUtils;


/**
 * @author pthoma20
 * 
 */
public class TgtMarketingAbstractSimpleDealValidateInterceptor implements ValidateInterceptor {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (!(model instanceof AbstractSimpleDealModel)) {
            return;
        }

        final AbstractSimpleDealModel abstractSimpleDeal = (AbstractSimpleDealModel)model;

        if (Boolean.TRUE.equals(abstractSimpleDeal.getAvailableForMobile())) {
            if (StringUtils.isBlank(abstractSimpleDeal.getPageTitle())) {
                throw new InterceptorException(ErrorMessages.ERROR_DEAL_AVAILABLE_MOBILE
                        + ErrorMessages.PAGE_TITLE_QUALIFIER_REQUIRED);
            }
        }
    }

    protected interface ErrorMessages {
        public static final String ERROR_DEAL_AVAILABLE_MOBILE = "Error enabling Available For Mobile -";
        public static final String PAGE_TITLE_QUALIFIER_REQUIRED = "Please add a Page Title.";
    }
}

/**
 * 
 */
package au.com.target.tgtmarketing.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import de.hybris.platform.voucher.model.DateRestrictionModel;
import de.hybris.platform.voucher.model.PromotionVoucherModel;
import de.hybris.platform.voucher.model.RestrictionModel;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.util.StringUtils;


/**
 * @author pthoma20
 * 
 */
public class TgtMarketingPromotionVoucherModelValidateInterceptor
        implements ValidateInterceptor<PromotionVoucherModel> {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final PromotionVoucherModel promotionVoucher, final InterceptorContext ctx)
            throws InterceptorException {

        if (BooleanUtils.isTrue(promotionVoucher.getMobileLoyaltyVoucher())
                && BooleanUtils.isFalse(promotionVoucher.getAvailableForMobile())) {
            throw new InterceptorException(ErrorMessages.ERROR_MOBILE_LOYALTY_VOUCHER
                    + ErrorMessages.AVAILABLE_FOR_MOBILE_REQUIRED);
        }

        if (Boolean.TRUE.equals(promotionVoucher.getAvailableForMobile())) {

            if (StringUtils.isEmpty(promotionVoucher.getName())) {
                throw new InterceptorException(ErrorMessages.ERROR_AVAILABLE_MOBILE
                        + ErrorMessages.NAME_REQUIRED);
            }

            if (!checkWhetherDateRestrictionPresent(promotionVoucher)) {
                throw new InterceptorException(ErrorMessages.ERROR_AVAILABLE_MOBILE
                        + ErrorMessages.TIME_RESTRICTION_REQUIRED);
            }

            if (CollectionUtils.isEmpty(promotionVoucher.getTargetMobileOfferHeadings())) {
                throw new InterceptorException(ErrorMessages.ERROR_AVAILABLE_MOBILE
                        + ErrorMessages.ASSOCIATED_MOBILE_OFFER_HEADING_REQUIRED);
            }

            if (TgtMarketingInterceptorUtil.mobileOfferHeadingContainDuplicates(promotionVoucher
                    .getTargetMobileOfferHeadings())) {
                throw new InterceptorException(ErrorMessages.ERROR_AVAILABLE_MOBILE
                        + ErrorMessages.DUPLICATE_MOBILE_OFFER_HEADING);
            }

        }
    }

    private boolean checkWhetherDateRestrictionPresent(final PromotionVoucherModel promotionVoucher) {
        final Set<RestrictionModel> restrictions = promotionVoucher.getRestrictions();
        for (final RestrictionModel restrictionModel : restrictions) {
            if (restrictionModel instanceof DateRestrictionModel) {
                return true;
            }
        }
        return false;
    }

    protected interface ErrorMessages {
        public static final String ERROR_MOBILE_LOYALTY_VOUCHER = "Error enabling Mobile Loyalty  Voucher-";
        public static final String AVAILABLE_FOR_MOBILE_REQUIRED = "Please enable available for mobile";
        public static final String ERROR_AVAILABLE_MOBILE = "Error enabling Available For Mobile -";
        public static final String TIME_RESTRICTION_REQUIRED = "Please add a target temporal date restriction present for making this voucher available for mobile.";
        public static final String NAME_REQUIRED = "Please add a name/description for making this voucher available for mobile.";
        public static final String ASSOCIATED_MOBILE_OFFER_HEADING_REQUIRED = "Please select at least one Mobile Offer Heading.";
        public static final String DUPLICATE_MOBILE_OFFER_HEADING = "Please remove duplicate Mobile Offer Heading.";
        public static final String BOLD_TITLE_REQUIRED = "Please add a Bold Title.";
    }

}

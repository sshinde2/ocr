/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtmarketing.jalo;

import de.hybris.platform.core.Registry;

import org.apache.log4j.Logger;

import au.com.target.tgtmarketing.constants.TgtmarketingConstants;



/**
 * This is the extension manager of the Tgtmarketing extension.
 */

public class TgtmarketingManager extends GeneratedTgtmarketingManager
{
    /** Edit the local|project.properties to change logging behavior (properties 'log4j.*'). */
    private static final Logger LOG = Logger.getLogger(TgtmarketingManager.class.getName());

    /*
     * Some important tips for development:
     * 
     * Do NEVER use the default constructor of manager's or items. => If you want to do something whenever the manger is
     * created use the init() or destroy() methods described below
     * 
     * Do NEVER use STATIC fields in your manager or items! => If you want to cache anything in a "static" way, use an
     * instance variable in your manager, the manager is created only once in the lifetime of a "deployment" or tenant.
     */

    public TgtmarketingManager() // NOPMD 
    {
        if (LOG.isDebugEnabled())
        {
            LOG.debug("constructor of TgtmarketingManager called.");
        }
    }

    /**
     * Get the valid instance of this manager.
     * 
     * @return the current instance of this manager
     */
    public static TgtmarketingManager getInstance()
    {
        return (TgtmarketingManager)Registry.getCurrentTenant().getJaloConnection().getExtensionManager().getExtension(
                TgtmarketingConstants.EXTENSIONNAME);
    }

    /**
     * Never call the constructor of any manager directly, call getInstance() You can place your business logic here -
     * like registering a jalo session listener. Each manager is created once for each tenant.
     */


}

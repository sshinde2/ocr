package au.com.target.tgtmarketing.offers;


import java.util.List;

import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;


/**
 * 
 * Service API to manage {@link TargetMobileOfferHeadingModel}
 * 
 */
public interface TargetMobileOfferHeadingService {

    /**
     * @return list of all the Mobile Offer Headings
     */
    List<TargetMobileOfferHeadingModel> getAllTargetMobileOfferHeadings();

}

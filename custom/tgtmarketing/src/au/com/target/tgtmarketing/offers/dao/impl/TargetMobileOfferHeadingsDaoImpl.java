package au.com.target.tgtmarketing.offers.dao.impl;

import de.hybris.platform.core.GenericQuery;
import de.hybris.platform.genericsearch.GenericSearchService;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;
import au.com.target.tgtmarketing.offers.dao.TargetMobileOfferHeadingsDao;


/**
 * Default implementation of {@link TargetMobileOfferHeadingsDao}
 * 
 */
public class TargetMobileOfferHeadingsDaoImpl implements
        TargetMobileOfferHeadingsDao {

    private GenericSearchService searchService;

    @Override
    public List<TargetMobileOfferHeadingModel> getAllTargetMobileOfferHeadings() {

        final GenericQuery query = new GenericQuery(TargetMobileOfferHeadingModel._TYPECODE);
        return searchService.<TargetMobileOfferHeadingModel> search(query).getResult();
    }

    /**
     * @param searchService
     *            the searchService to set
     */
    @Required
    public void setSearchService(final GenericSearchService searchService) {
        this.searchService = searchService;
    }
}

package au.com.target.tgtmarketing.offers.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;
import au.com.target.tgtmarketing.offers.TargetMobileOfferHeadingService;
import au.com.target.tgtmarketing.offers.dao.TargetMobileOfferHeadingsDao;


/**
 * 
 * Default implementation of {@link TargetMobileOfferHeadingService}
 * 
 */
public class TargetMobileOfferHeadingServiceImpl implements TargetMobileOfferHeadingService {

    private TargetMobileOfferHeadingsDao targetMobileOfferHeadingsDao;


    /**
     * @param targetMobileOfferHeadingsDao
     *            the targetMobileOfferHeadingsDao to set
     */
    @Required
    public void setTargetMobileOfferHeadingsDao(final TargetMobileOfferHeadingsDao targetMobileOfferHeadingsDao) {
        this.targetMobileOfferHeadingsDao = targetMobileOfferHeadingsDao;
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtmarketing.deals.TargetMobileOfferHeadingService#getAllTargetMobileOfferHeadings()
     */
    @Override
    public List<TargetMobileOfferHeadingModel> getAllTargetMobileOfferHeadings() {
        return targetMobileOfferHeadingsDao.getAllTargetMobileOfferHeadings();
    }

}

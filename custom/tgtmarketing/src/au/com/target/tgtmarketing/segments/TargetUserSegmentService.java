/**
 * 
 */
package au.com.target.tgtmarketing.segments;

import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.usergroups.model.UserSegmentModel;

import java.util.List;
import java.util.Map;


/**
 * @author pthoma20
 *
 */
public interface TargetUserSegmentService {

    /**
     * This method returns all the user segments for a given user.
     * 
     * @param userModel
     * @return list of User Group Models
     */
    List<PrincipalGroupModel> getAllUserSegmentsForUser(UserModel userModel);

    /**
     * This method returns all the user segments map
     * 
     * @return list of User Group Models (uid,UserSegment)
     */
    Map<String, UserSegmentModel> getAllUserSegmentsMap();

}
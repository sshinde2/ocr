/**
 * 
 */
package au.com.target.tgtmarketing.segments;

import de.hybris.platform.cms2.model.restrictions.AbstractRestrictionModel;
import de.hybris.platform.cms2.model.restrictions.CMSUserGroupRestrictionModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.usergroups.model.UserSegmentModel;

import java.util.Collection;
import java.util.List;

import org.springframework.util.CollectionUtils;

import au.com.target.tgtwebcore.model.cms2.pages.TargetFragmentPageModel;


/**
 * @author pthoma20
 *
 */
public class UserSegmentRestrictionForFragmentPageEvaluator {


    /**
     * This method evaluates the restriction applied on the page and returns true if the page is applicable for the user
     * segment passed in.
     * 
     * @param targetFragmentPageModel
     * @param userSegmentModel
     * @return whether the page is applicable for the user segment.
     */
    public boolean evaluate(final TargetFragmentPageModel targetFragmentPageModel,
            final UserSegmentModel userSegmentModel) {
        final List<AbstractRestrictionModel> abstractRestrictionModels = targetFragmentPageModel.getRestrictions();
        if (CollectionUtils.isEmpty(abstractRestrictionModels)) {
            return false;
        }

        //evaluate user group restrictions applied for the page.
        for (final AbstractRestrictionModel abstractRestrictionModel : abstractRestrictionModels) {
            if (!(abstractRestrictionModel instanceof CMSUserGroupRestrictionModel)) {
                continue;
            }
            //check the groups in the restriction if it satisfies the requested segment.
            final CMSUserGroupRestrictionModel cmsUserGroupRestrictionModel = (CMSUserGroupRestrictionModel)abstractRestrictionModel;
            final Collection<UserGroupModel> userGroupModelsInRestriction = cmsUserGroupRestrictionModel
                    .getUserGroups();
            if (CollectionUtils.isEmpty(userGroupModelsInRestriction)) {
                continue;
            }
            for (final UserGroupModel restrictedUserSegmentGroup : userGroupModelsInRestriction) {
                if (restrictedUserSegmentGroup instanceof UserSegmentModel
                        && restrictedUserSegmentGroup.getUid().equals(userSegmentModel.getUid())) {
                    return true;
                }
            }
        }
        return false;
    }
}
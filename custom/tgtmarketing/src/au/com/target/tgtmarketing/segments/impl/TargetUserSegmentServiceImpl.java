/**
 * 
 */
package au.com.target.tgtmarketing.segments.impl;

import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.usergroups.model.UserSegmentModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.CollectionUtils;

import au.com.target.tgtmarketing.segments.TargetUserSegmentService;


/**
 * @author pthoma20
 *
 */
public class TargetUserSegmentServiceImpl implements TargetUserSegmentService {

    private DefaultGenericDao<UserSegmentModel> targetUserSegmentDao;

    /* (non-Javadoc)
     * @see au.com.target.tgtmarketing.segments.TargetUserSegmentService#getAllUserSegmentsForUser(de.hybris.platform.core.model.user.UserModel)
     */
    @Override
    public List<PrincipalGroupModel> getAllUserSegmentsForUser(final UserModel userModel) {

        return getAllUserSegmentsFromGroups(userModel.getGroups());
    }

    private List<PrincipalGroupModel> getAllUserSegmentsFromGroups(final Collection<PrincipalGroupModel> groups) {
        final List<PrincipalGroupModel> userSegments = new ArrayList<>();
        if (groups == null) {
            return userSegments;
        }
        for (final PrincipalGroupModel principalGroup : groups) {
            if (principalGroup instanceof UserSegmentModel) {
                userSegments.add(principalGroup);
                userSegments.addAll(getAllUserSegmentsFromGroups(principalGroup.getGroups()));
            }
        }
        return userSegments;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtmarketing.segments.TargetUserSegmentService#getAllUserSegmentsMap()
     */
    @Override
    public Map<String, UserSegmentModel> getAllUserSegmentsMap() {
        final List<UserSegmentModel> userSegmentModels = targetUserSegmentDao.find();
        if (CollectionUtils.isEmpty(userSegmentModels)) {
            return null;
        }
        final Map<String, UserSegmentModel> userSegmentMap = new HashMap<String, UserSegmentModel>();
        for (final UserSegmentModel userSegmentModel : userSegmentModels) {
            userSegmentMap.put(userSegmentModel.getUid().toLowerCase(), userSegmentModel);
        }
        return userSegmentMap;
    }

    /**
     * @param targetUserSegmentDao
     *            the targetUserSegmentDao to set
     */
    @Required
    public void setTargetUserSegmentDao(final DefaultGenericDao<UserSegmentModel> targetUserSegmentDao) {
        this.targetUserSegmentDao = targetUserSegmentDao;
    }


}
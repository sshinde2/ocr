/**
 * 
 */
package au.com.target.tgtmarketing.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;

import java.util.ArrayList;
import java.util.List;

import au.com.target.tgtmarketing.constants.TgtmarketingConstants;


/**
 * @author paul
 *
 */

/**
 * This class provides hooks into the system's initialisation and update processes.
 * 
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = TgtmarketingConstants.EXTENSIONNAME)
public class TgtMarketingSystemSetup extends AbstractSystemSetup {


    /**
     * This method will be called by system creator during initialisation and system update. Be sure that this method
     * can be called repeatedly.
     * 
     * @param context
     *            the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
    public void createEssentialData(final SystemSetupContext context) {
        importImpexFile(context, "/tgtmarketing/import/user-groups/deals-user-group.impex");
        importImpexFile(context, "/tgtmarketing/import/user-groups/vouchers-user-group.impex");
        importImpexFile(context, "/tgtmarketing/import/user-groups/social-media-user-group.impex");
    }

    /**
     * Generates the Dropdown and Multi-select boxes for the project data import
     */
    @Override
    @SystemSetupParameterMethod
    public List<SystemSetupParameter> getInitializationOptions() {
        final List<SystemSetupParameter> params = new ArrayList<>();
        // add stuff here if you need params
        return params;
    }

    /**
     * This method will be called during the system initialization.
     * 
     * @param context
     *            the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.PROJECT, process = Process.ALL)
    public void createProjectData(final SystemSetupContext context) {
        importImpexFile(context, "/tgtmarketing/import/offers/targetMob-offr-heading.impex");
    }
}

/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetLookProductModel;


/**
 * Service to manage {@link TargetLookProductModel}
 * 
 * @author mgazal
 *
 */
public interface LookProductService {

    /**
     * Get the look product for a given code and look.
     * 
     * @param productCode
     * @param look
     * @return {@link TargetLookProductModel}
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    TargetLookProductModel getLookProductForCodeAndLook(String productCode, TargetLookModel look)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;
}

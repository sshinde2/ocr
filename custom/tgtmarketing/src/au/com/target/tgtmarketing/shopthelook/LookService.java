/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.Collection;
import java.util.List;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtmarketing.model.TargetLookModel;


/**
 * Service to manage {@link TargetLookModel}
 * 
 * @author mgazal
 *
 */
public interface LookService {

    /**
     * Get the look for a given code.
     * 
     * @param code
     * @return {@link TargetLookModel}
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    TargetLookModel getLookForCode(String code)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;

    /**
     * Get the list of visible looks given a collectionCode.
     * 
     * @param collectionCode
     * @return list of {@link TargetLookModel}
     */
    List<TargetLookModel> getVisibleLooksForCollection(String collectionCode);

    /**
     * Get the list of visible looks for given productCodes.
     * 
     * @param productCodes
     * @return list of {@link TargetLookModel}
     */
    List<TargetLookModel> getVisibleLooksForProductCodes(Collection<String> productCodes);

    /**
     * Get a limited list of visible looks for given productCodes.
     * 
     * @param productCodes
     * @param limit
     * @return list of {@link TargetLookModel}
     */
    List<TargetLookModel> getVisibleLooksForProductCodes(Collection<String> productCodes, int limit);

    /**
     * Get the list of visible looks for a given product.
     * 
     * @param productModel
     * @return list of {@link TargetLookModel}
     */
    List<TargetLookModel> getVisibleLooksForProduct(ProductModel productModel);

    /**
     * Get a limited list of visible looks for given product.
     * 
     * @param productModel
     * @param limit
     * @return list of {@link TargetLookModel}
     */
    List<TargetLookModel> getVisibleLooksForProduct(ProductModel productModel, int limit);

    /**
     * Get all the visible looks with Instagram Url.
     * 
     * @return list of {@link TargetLookModel}
     */
    List<TargetLookModel> getAllVisibleLooksWithInstagramUrl();

    /**
     * Get limited number of visible looks with Instagram Url.
     * 
     * @param limit
     * @return list of {@link TargetLookModel}
     */
    List<TargetLookModel> getAllVisibleLooksWithInstagramUrl(int limit);
}

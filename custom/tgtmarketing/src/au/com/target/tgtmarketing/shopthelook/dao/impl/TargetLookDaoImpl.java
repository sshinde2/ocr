/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.util.TargetServicesUtil;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetLookProductModel;
import au.com.target.tgtmarketing.shopthelook.dao.LookDao;


/**
 * 
 * @author mgazal
 *
 */
public class TargetLookDaoImpl extends DefaultGenericDao<TargetLookModel> implements LookDao {

    private static final String IS_VISIBLE_CLAUSE = new StringBuilder().append("{l:").append(TargetLookModel.ENABLED)
            .append("} = 1 AND ({l:").append(TargetLookModel.STARTDATE).append("} IS NULL OR {l:")
            .append(TargetLookModel.STARTDATE).append("} <= ?now) AND ({l:").append(TargetLookModel.ENDDATE)
            .append("} IS NULL OR {l:").append(TargetLookModel.ENDDATE).append("} >= ?now)").toString();

    public TargetLookDaoImpl() {
        super(TargetLookModel._TYPECODE);
    }

    @Override
    public TargetLookModel getLookForCode(final String code)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        ServicesUtil.validateParameterNotNullStandardMessage("code", code);

        final Map<String, String> params = new HashMap<>();
        params.put(TargetLookModel.ID, code);
        final List<TargetLookModel> lookModels = find(params);

        TargetServicesUtil.validateIfSingleResult(lookModels, TargetLookModel.class, TargetLookModel.ID, code);

        return lookModels.get(0);
    }

    @Override
    public List<TargetLookModel> getVisibleLooksForCollection(final String collectionCode) {
        ServicesUtil.validateParameterNotNullStandardMessage("collectionCode", collectionCode);

        final StringBuilder query = new StringBuilder()
                .append("SELECT {l:").append(TargetLookModel.PK)
                .append("} FROM {").append(TargetLookModel._TYPECODE).append(" AS l JOIN ")
                .append(TargetLookCollectionModel._TYPECODE).append(" AS lc ON {l:").append(TargetLookModel.COLLECTION)
                .append("} = {lc:").append(TargetLookCollectionModel.PK).append("}} WHERE {lc:")
                .append(TargetLookModel.ID).append("} = (?collectionCode) AND ")
                .append(IS_VISIBLE_CLAUSE).append(" ORDER BY {l:").append(TargetLookModel.STARTDATE).append("} DESC");
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.addQueryParameter("collectionCode", collectionCode);
        searchQuery.addQueryParameter("now", getCurrentDate());
        final SearchResult result = getFlexibleSearchService().search(searchQuery);
        return result.getResult();
    }

    @Override
    public List<TargetLookModel> getVisibleLooksForProductCodes(final Collection<String> productCodes) {
        return getVisibleLooksForProductCodes(productCodes, -1);
    }

    @Override
    public List<TargetLookModel> getVisibleLooksForProductCodes(final Collection<String> productCodes,
            final int limit) {

        ServicesUtil.validateParameterNotNullStandardMessage("productCodes", productCodes);

        final StringBuilder query = new StringBuilder()
                .append("SELECT {l:").append(TargetLookModel.PK).append("} FROM {")
                .append(TargetLookModel._TYPECODE).append(" AS l JOIN ").append(TargetLookProductModel._TYPECODE)
                .append(" AS lp ON {l:").append(TargetLookModel.PK).append("} = {lp:")
                .append(TargetLookProductModel.LOOK).append("}} WHERE {lp:").append(TargetLookProductModel.PRODUCTCODE)
                .append("} IN (?productCodes) AND ")
                .append(IS_VISIBLE_CLAUSE).append(" GROUP BY {l:").append(TargetLookModel.PK).append("}, {l:")
                .append(TargetLookModel.STARTDATE).append("} ORDER BY {l:").append(TargetLookModel.STARTDATE)
                .append("} DESC");
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.setCount(limit);
        searchQuery.addQueryParameter("productCodes", productCodes);
        searchQuery.addQueryParameter("now", getCurrentDate());
        final SearchResult result = getFlexibleSearchService().search(searchQuery);
        return result.getResult();
    }

    @Override
    public List<TargetLookModel> getAllVisibleLooksWithInstagramUrl() {
        return getAllVisibleLooksWithInstagramUrl(-1);
    }

    @Override
    public List<TargetLookModel> getAllVisibleLooksWithInstagramUrl(final int limit) {
        final StringBuilder query = new StringBuilder()
                .append("SELECT {l:").append(TargetLookModel.PK)
                .append("} FROM {").append(TargetLookModel._TYPECODE).append(" AS l } WHERE ")
                .append(IS_VISIBLE_CLAUSE).append(" AND {l:").append(TargetLookModel.INSTAGRAMURL)
                .append("} IS NOT NULL AND {l:").append(TargetLookModel.INSTAGRAMURL).append("} <> '' ORDER BY {l:")
                .append(TargetLookModel.STARTDATE).append("} DESC");
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.setCount(limit);
        searchQuery.addQueryParameter("now", getCurrentDate());
        final SearchResult result = getFlexibleSearchService().search(searchQuery);
        return result.getResult();
    }

    private Date getCurrentDate() {
        return DateUtils.round(new Date(), Calendar.MINUTE);
    }
}

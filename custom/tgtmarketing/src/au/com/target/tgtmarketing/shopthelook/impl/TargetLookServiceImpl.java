/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.shopthelook.LookService;
import au.com.target.tgtmarketing.shopthelook.dao.LookDao;


/**
 * @author mgazal
 *
 */
public class TargetLookServiceImpl extends AbstractBusinessService implements LookService {

    private LookDao lookDao;

    @Override
    public TargetLookModel getLookForCode(final String code)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        ServicesUtil.validateParameterNotNullStandardMessage("code", code);
        return lookDao.getLookForCode(code);
    }

    @Override
    public List<TargetLookModel> getVisibleLooksForCollection(final String collectionCode) {
        ServicesUtil.validateParameterNotNullStandardMessage("collectionCode", collectionCode);
        return lookDao.getVisibleLooksForCollection(collectionCode);
    }

    @Override
    public List<TargetLookModel> getVisibleLooksForProductCodes(final Collection<String> productCodes) {
        return getVisibleLooksForProductCodes(productCodes, -1);
    }

    @Override
    public List<TargetLookModel> getVisibleLooksForProductCodes(final Collection<String> productCodes,
            final int limit) {
        ServicesUtil.validateParameterNotNullStandardMessage("productCodes", productCodes);
        return lookDao.getVisibleLooksForProductCodes(productCodes, limit);
    }

    @Override
    public List<TargetLookModel> getVisibleLooksForProduct(final ProductModel productModel) {
        return getVisibleLooksForProduct(productModel, -1);
    }

    @Override
    public List<TargetLookModel> getVisibleLooksForProduct(final ProductModel productModel, final int limit) {
        ServicesUtil.validateParameterNotNullStandardMessage("productModel", productModel);
        if (CollectionUtils.isNotEmpty(productModel.getVariants())) {
            final Collection<String> productCodes = new HashSet<>();
            for (final VariantProductModel variantProduct : productModel.getVariants()) {
                if (variantProduct instanceof TargetColourVariantProductModel) {
                    productCodes.add(variantProduct.getCode());
                }
            }
            return getVisibleLooksForProductCodes(productCodes, limit);
        }
        return Collections.emptyList();
    }

    @Override
    public List<TargetLookModel> getAllVisibleLooksWithInstagramUrl() {
        return getAllVisibleLooksWithInstagramUrl(-1);
    }

    @Override
    public List<TargetLookModel> getAllVisibleLooksWithInstagramUrl(final int limit) {
        return lookDao.getAllVisibleLooksWithInstagramUrl(limit);
    }

    /**
     * @param lookDao
     *            the lookDao to set
     */
    @Required
    public void setLookDao(final LookDao lookDao) {
        this.lookDao = lookDao;
    }
}

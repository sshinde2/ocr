/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook.impl;

import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;
import au.com.target.tgtmarketing.shopthelook.ShopTheLookService;
import au.com.target.tgtmarketing.shopthelook.dao.ShopTheLookDao;


/**
 * @author mgazal
 *
 */
public class TargetShopTheLookServiceImpl extends AbstractBusinessService implements ShopTheLookService {
    private static final Logger LOG = Logger.getLogger(TargetShopTheLookServiceImpl.class);

    private ShopTheLookDao shopTheLookDao;
    private SearchRestrictionService searchRestrictionService;



    @Override
    public TargetShopTheLookModel getShopTheLookForCode(final String code)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        ServicesUtil.validateParameterNotNullStandardMessage("code", code);
        return shopTheLookDao.getShopTheLookForCode(code);
    }

    /**
     * @param shopTheLookDao
     *            the shopTheLookDao to set
     */
    @Required
    public void setShopTheLookDao(final ShopTheLookDao shopTheLookDao) {
        this.shopTheLookDao = shopTheLookDao;
    }

    @Override
    public List<TargetLookModel> getListOfLooksForShopTheLookUsingCode(final String id)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, IllegalArgumentException {
        ServicesUtil.validateParameterNotNullStandardMessage("id", id);
        return shopTheLookDao.getListOfLooksForShopTheLookUsingCode(id);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtmarketing.shopthelook.ShopTheLookService#getShopTheLookByCagegoryCode(java.lang.String)
     */
    @Override
    public TargetShopTheLookModel getShopTheLookByCategoryCode(final String code) {

        return getSessionService()
                .executeInLocalView(new SessionExecutionBody() {
                    @Override
                    public Object execute() {
                        TargetShopTheLookModel shopTheLookModel = null;
                        try {
                            searchRestrictionService.disableSearchRestrictions();
                            shopTheLookModel = shopTheLookDao.getShopTheLookByCategoryCode(code);
                        }
                        catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
                            LOG.warn(e.getMessage());
                        }
                        finally {
                            searchRestrictionService.enableSearchRestrictions();
                        }
                        return shopTheLookModel;
                    }
                });


    }

    /**
     * @param searchRestrictionService
     *            the searchRestrictionService to set
     */
    @Required
    public void setSearchRestrictionService(final SearchRestrictionService searchRestrictionService) {
        this.searchRestrictionService = searchRestrictionService;
    }
}

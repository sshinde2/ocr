/**
 * 
 */
package au.com.target.tgtmarketing.social;

import java.util.List;

import au.com.target.tgtmarketing.model.SocialMediaProductsModel;


/**
 * @author rmcalave
 *
 */
public interface TargetSocialMediaProductsService {

    /**
     * Retrieve a list of <code>SocialMediaProductsModel</code>s, ordered by descending creation date, limited by
     * <code>count</code>.
     * 
     * @param count
     *            The maximum number of records to retrieve
     * @return A list of <code>SocialMediaProductsModel</code>
     */
    List<SocialMediaProductsModel> getSocialMediaProductsByCreationTimeDescending(int count);

}
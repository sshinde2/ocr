/**
 * 
 */
package au.com.target.tgtmarketing.social.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.internal.dao.SortParameters;
import de.hybris.platform.servicelayer.internal.dao.SortParameters.SortOrder;

import java.util.List;

import org.apache.commons.lang.Validate;

import au.com.target.tgtmarketing.model.SocialMediaProductsModel;
import au.com.target.tgtmarketing.social.dao.TargetSocialMediaProductsDao;


/**
 * @author rmcalave
 *
 */
public class TargetSocialMediaProductsDaoImpl extends DefaultGenericDao<SocialMediaProductsModel>
        implements TargetSocialMediaProductsDao {

    public TargetSocialMediaProductsDaoImpl() {
        super(SocialMediaProductsModel._TYPECODE);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.marketing.dao.TargetSocialMediaProductsDao#getSocialMediaProductsByCreationTimeDescending(int)
     */
    @Override
    public List<SocialMediaProductsModel> findSocialMediaProductsByCreationTimeDescending(final int count) {
        Validate.isTrue(count > 0, "count must be greater than zero");

        final SortParameters sortParams = new SortParameters();
        sortParams.addSortParameter(SocialMediaProductsModel.CREATIONTIME, SortOrder.DESCENDING);

        return find(null, sortParams, count);
    }

}

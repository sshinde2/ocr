package au.com.target.tgtmarketing.voucher;

import de.hybris.platform.voucher.model.PromotionVoucherModel;

import java.util.List;


/**
 * 
 * Service API to manage {@link PromotionVoucherModel}
 * 
 */
public interface TargetMobileVoucherService {

    /**
     * This method will return a list of all promotional vouchers which are applicable for mobile based on the current
     * time frame. This method evaluates all the Target Date Restrictions associated to a mobile Promotion Voucher.
     * 
     * @return list of active vouchers
     */
    List<PromotionVoucherModel> getAllActiveMobileVouchers();

}

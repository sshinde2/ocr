package au.com.target.tgtmarketing.voucher.dao;

import de.hybris.platform.voucher.model.PromotionVoucherModel;

import java.util.List;



/**
 * DAO to get mobile voucher information related to mobile
 */
public interface TargetMobileVoucherDao {

    /**
     * This method will retrieve all the promotional vouchers which are marked as available for mobile. This method does
     * not evaluate the restrictions.
     * 
     * @return list of active vouchers for Mobile
     */
    List<PromotionVoucherModel> getAllActiveMobileVouchers();

}

package au.com.target.tgtmarketing.voucher.impl;

import de.hybris.platform.voucher.model.DateRestrictionModel;
import de.hybris.platform.voucher.model.PromotionVoucherModel;
import de.hybris.platform.voucher.model.RestrictionModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtmarketing.voucher.TargetMobileVoucherService;
import au.com.target.tgtmarketing.voucher.dao.TargetMobileVoucherDao;



/**
 * 
 * Default implementation of {@link TargetMobileVoucherService}
 * 
 */
public class TargetMobileVoucherServiceImpl implements TargetMobileVoucherService {

    private TargetMobileVoucherDao targetMobileVoucherDao;

    @Override
    public List<PromotionVoucherModel> getAllActiveMobileVouchers() {
        final List<PromotionVoucherModel> activeVouchers = targetMobileVoucherDao.getAllActiveMobileVouchers();
        final List<PromotionVoucherModel> validPromotionVoucherList = new ArrayList<>();
        for (final PromotionVoucherModel promotionVoucherModel : activeVouchers) {
            if (isVoucherValidForDateRestriction(promotionVoucherModel.getRestrictions())) {
                validPromotionVoucherList.add(promotionVoucherModel);
            }
        }
        return validPromotionVoucherList;
    }

    private boolean isVoucherValidForDateRestriction(final Set<RestrictionModel> restrictionModelList) {
        if (CollectionUtils.isEmpty(restrictionModelList)) {
            return false;
        }
        boolean isDateRestrictionValid = false;
        for (final RestrictionModel restrictionModel : restrictionModelList) {
            if (!(restrictionModel instanceof DateRestrictionModel)) {
                continue;
            }
            final DateRestrictionModel dateRestrictionModel = (DateRestrictionModel)restrictionModel;
            final Date now = new Date();
            if (null != dateRestrictionModel.getStartDate()
                    && null != dateRestrictionModel.getEndDate()
                    && now.after(dateRestrictionModel.getStartDate())
                    && now.before(dateRestrictionModel.getEndDate())) {
                isDateRestrictionValid = true;
            }
            else {
                return false;
            }
        }
        return isDateRestrictionValid;
    }

    /**
     * @param targetMobileVoucherDao
     *            the targetMobileVoucherDao to set
     */
    @Required
    public void setTargetMobileVoucherDao(final TargetMobileVoucherDao targetMobileVoucherDao) {
        this.targetMobileVoucherDao = targetMobileVoucherDao;
    }

}

/**
 * 
 */
package au.com.target.tgtmarketing.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.Arrays;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;


/**
 * @author pthoma20
 *
 */
@UnitTest
public class TgtMarketingInterceptorUtilTest {

    @Test
    public void testForNullMobileOfferHeadingList() {
        Assertions.assertThat(TgtMarketingInterceptorUtil.mobileOfferHeadingContainDuplicates(null)).isEqualTo(false);
    }

    @Test
    public void testForEmptyMobileOfferHeadingList() {
        Assertions.assertThat(
                TgtMarketingInterceptorUtil
                        .mobileOfferHeadingContainDuplicates(new ArrayList<TargetMobileOfferHeadingModel>()))
                .isEqualTo(false);
    }

    @Test
    public void testForDuplicateMobileOfferHeadingList() {
        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModelMock1 = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        BDDMockito.given(targetMobileOfferHeadingModelMock1.getCode()).willReturn("baby");

        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModelMock2 = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        BDDMockito.given(targetMobileOfferHeadingModelMock2.getCode()).willReturn("baby");
        Assertions.assertThat(
                TgtMarketingInterceptorUtil
                        .mobileOfferHeadingContainDuplicates(Arrays.asList(targetMobileOfferHeadingModelMock1,
                                targetMobileOfferHeadingModelMock2)))
                .isEqualTo(true);
    }

    @Test
    public void testForNonDuplicateMobileOfferHeadingList() {
        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModelMock1 = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        BDDMockito.given(targetMobileOfferHeadingModelMock1.getCode()).willReturn("baby");

        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModelMock2 = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        BDDMockito.given(targetMobileOfferHeadingModelMock2.getCode()).willReturn("womens");
        Assertions.assertThat(
                TgtMarketingInterceptorUtil
                        .mobileOfferHeadingContainDuplicates(Arrays.asList(targetMobileOfferHeadingModelMock1,
                                targetMobileOfferHeadingModelMock2)))
                .isEqualTo(false);
    }
}

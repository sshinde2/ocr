/**
 * 
 */
package au.com.target.tgtmarketing.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.voucher.model.DateRestrictionModel;
import de.hybris.platform.voucher.model.PromotionVoucherModel;
import de.hybris.platform.voucher.model.RestrictionModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mockito;

import au.com.target.tgtcore.model.TargetDateRestrictionModel;
import au.com.target.tgtmarketing.interceptor.TgtMarketingPromotionVoucherModelValidateInterceptor.ErrorMessages;
import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;


/**
 * @author paul
 *
 */
@UnitTest
public class TgtMarketingPromotionVoucherModelValidateInterceptorTest {

    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";

    @InjectMocks
    private final TgtMarketingPromotionVoucherModelValidateInterceptor tgtMarketingPromotionVoucherModelValidateInterceptor = new TgtMarketingPromotionVoucherModelValidateInterceptor();


    @Test
    public void testDescriptionMissingWhenEnablingForMobile() throws InterceptorException {
        final PromotionVoucherModel mockModel = Mockito.mock(PromotionVoucherModel.class);
        BDDMockito.given(mockModel.getAvailableForMobile()).willReturn(Boolean.TRUE);
        try {
            tgtMarketingPromotionVoucherModelValidateInterceptor.onValidate(mockModel, null);
            Assert.fail("Expected InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage())
                    .isEqualTo(
                            EXCEPTION_MESSAGE_PREFIX + ErrorMessages.ERROR_AVAILABLE_MOBILE
                                    + ErrorMessages.NAME_REQUIRED);
        }
    }

    @Test
    public void testEnablingForMobileWhenNoTimeRestrictionSetupForTheVoucher() throws InterceptorException {
        final PromotionVoucherModel mockModel = Mockito.mock(PromotionVoucherModel.class);
        BDDMockito.given(mockModel.getAvailableForMobile()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockModel.getName()).willReturn("Name");

        try {
            tgtMarketingPromotionVoucherModelValidateInterceptor.onValidate(mockModel, null);
            Assert.fail("Expected InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage())
                    .isEqualTo(
                            EXCEPTION_MESSAGE_PREFIX + ErrorMessages.ERROR_AVAILABLE_MOBILE
                                    + ErrorMessages.TIME_RESTRICTION_REQUIRED);
        }
    }

    @Test
    public void testEnablingForMobileWhenNoMobOfferHeadingsSetupForTheVoucher() throws InterceptorException {
        final PromotionVoucherModel mockModel = Mockito.mock(PromotionVoucherModel.class);
        BDDMockito.given(mockModel.getAvailableForMobile()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockModel.getName()).willReturn("name");
        final TargetDateRestrictionModel dateRestrictionMock = Mockito.mock(TargetDateRestrictionModel.class);
        final Set<RestrictionModel> timeRestriction = new HashSet<>();
        timeRestriction.add(dateRestrictionMock);
        BDDMockito.given(mockModel.getRestrictions()).willReturn(timeRestriction);

        try {
            tgtMarketingPromotionVoucherModelValidateInterceptor.onValidate(mockModel, null);
            Assert.fail("Expected InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage())
                    .isEqualTo(
                            EXCEPTION_MESSAGE_PREFIX + ErrorMessages.ERROR_AVAILABLE_MOBILE
                                    + ErrorMessages.ASSOCIATED_MOBILE_OFFER_HEADING_REQUIRED);
        }
    }

    @Test
    public void testEnablingLoyaltyMobileVoucherWithoutEnablingItForMobile() throws InterceptorException {
        final PromotionVoucherModel mockModel = Mockito.mock(PromotionVoucherModel.class);
        BDDMockito.given(mockModel.getAvailableForMobile()).willReturn(Boolean.FALSE);
        BDDMockito.given(mockModel.getMobileLoyaltyVoucher()).willReturn(Boolean.TRUE);

        try {
            tgtMarketingPromotionVoucherModelValidateInterceptor.onValidate(mockModel, null);
            Assert.fail("Expected InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage())
                    .isEqualTo(
                            EXCEPTION_MESSAGE_PREFIX + ErrorMessages.ERROR_MOBILE_LOYALTY_VOUCHER
                                    + ErrorMessages.AVAILABLE_FOR_MOBILE_REQUIRED);
        }
    }


    @Test
    public void testOnValidateMobileAttributeWithDuplicateMobileOfferHeading() throws InterceptorException {
        final PromotionVoucherModel mockModel = Mockito.mock(PromotionVoucherModel.class);
        BDDMockito.given(mockModel.getAvailableForMobile()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockModel.getName()).willReturn("name");
        final TargetDateRestrictionModel dateRestrictionMock = Mockito.mock(TargetDateRestrictionModel.class);
        final Set<RestrictionModel> timeRestriction = new HashSet<>();
        timeRestriction.add(dateRestrictionMock);
        BDDMockito.given(mockModel.getRestrictions()).willReturn(timeRestriction);

        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel1 = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        BDDMockito.given(targetMobileOfferHeadingModel1.getCode()).willReturn("entertainment");

        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel2 = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        BDDMockito.given(targetMobileOfferHeadingModel2.getCode()).willReturn("entertainment");

        BDDMockito.given(mockModel.getTargetMobileOfferHeadings()).willReturn(
                new ArrayList<TargetMobileOfferHeadingModel>(Arrays.asList(targetMobileOfferHeadingModel1,
                        targetMobileOfferHeadingModel2)));
        try {
            tgtMarketingPromotionVoucherModelValidateInterceptor.onValidate(mockModel, null);
            Assert.fail("Expected InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.ERROR_AVAILABLE_MOBILE
                            + ErrorMessages.DUPLICATE_MOBILE_OFFER_HEADING);
        }
    }


    @Test
    public void testSuccessfulWithAvailableOnMobileWithDateRestriction() throws InterceptorException {
        final PromotionVoucherModel mockModel = Mockito.mock(PromotionVoucherModel.class);
        BDDMockito.given(mockModel.getAvailableForMobile()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockModel.getName()).willReturn("Name");
        final DateRestrictionModel dateRestrictionMock = Mockito.mock(DateRestrictionModel.class);
        final Set<RestrictionModel> timeRestriction = new HashSet<>();
        timeRestriction.add(dateRestrictionMock);
        BDDMockito.given(mockModel.getRestrictions()).willReturn(timeRestriction);

        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel1 = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        BDDMockito.given(targetMobileOfferHeadingModel1.getCode()).willReturn("entertainment");

        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel2 = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        BDDMockito.given(targetMobileOfferHeadingModel2.getCode()).willReturn("baby");

        BDDMockito.given(mockModel.getTargetMobileOfferHeadings()).willReturn(
                new ArrayList<TargetMobileOfferHeadingModel>(Arrays.asList(targetMobileOfferHeadingModel1,
                        targetMobileOfferHeadingModel2)));
        tgtMarketingPromotionVoucherModelValidateInterceptor.onValidate(mockModel, null);
    }

    @Test
    public void testSuccessfulWithAvailableOnMobile() throws InterceptorException {
        final PromotionVoucherModel mockModel = Mockito.mock(PromotionVoucherModel.class);
        BDDMockito.given(mockModel.getAvailableForMobile()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockModel.getMobileLoyaltyVoucher()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockModel.getName()).willReturn("Name");
        final TargetDateRestrictionModel dateRestrictionMock = Mockito.mock(TargetDateRestrictionModel.class);
        final Set<RestrictionModel> timeRestriction = new HashSet<>();
        timeRestriction.add(dateRestrictionMock);
        BDDMockito.given(mockModel.getRestrictions()).willReturn(timeRestriction);

        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel1 = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        BDDMockito.given(targetMobileOfferHeadingModel1.getCode()).willReturn("entertainment");

        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel2 = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        BDDMockito.given(targetMobileOfferHeadingModel2.getCode()).willReturn("baby");

        BDDMockito.given(mockModel.getTargetMobileOfferHeadings()).willReturn(
                new ArrayList<TargetMobileOfferHeadingModel>(Arrays.asList(targetMobileOfferHeadingModel1,
                        targetMobileOfferHeadingModel2)));
        tgtMarketingPromotionVoucherModelValidateInterceptor.onValidate(mockModel, null);
    }


}

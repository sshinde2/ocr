/**
 * 
 */
package au.com.target.tgtmarketing.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.promotions.model.DealBreakPointModel;
import de.hybris.platform.promotions.model.QuantityBreakDealModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.model.TargetDealCategoryModel;
import au.com.target.tgtmarketing.interceptor.TgtMarketingQuantityBreakDealModelValidateInterceptor.ErrorMessages;
import junit.framework.Assert;




/**
 * @author pthoma20
 *
 */
@UnitTest
public class TgtMarketingQuantityBreakDealModelValidateInterceptorTest {

    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";

    private final TgtMarketingQuantityBreakDealModelValidateInterceptor quantityBreakDealModelValidateInterceptor = new TgtMarketingQuantityBreakDealModelValidateInterceptor();


    @Test
    public void testOnValidateWithAvailableForMobileSet() throws InterceptorException {

        final QuantityBreakDealModel mockQuantityBreakDeal = createQuantityBreakDeal();
        BDDMockito.given(mockQuantityBreakDeal.getAvailableForMobile()).willReturn(Boolean.TRUE);
        try {
            quantityBreakDealModelValidateInterceptor.onValidate(mockQuantityBreakDeal, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions
                    .assertThat(ex.getMessage())
                    .isEqualTo(
                            EXCEPTION_MESSAGE_PREFIX
                                    + ErrorMessages.ERROR_QTY_BREAK_DEAL_AVAILABLE_MOBILE);
        }
    }

    @Test
    public void testOnValidateSuccessfulWhenAvailableForMobileNotSet() throws InterceptorException {

        final QuantityBreakDealModel mockQuantityBreakDeal = createQuantityBreakDeal();
        BDDMockito.given(mockQuantityBreakDeal.getAvailableForMobile()).willReturn(Boolean.FALSE);
        quantityBreakDealModelValidateInterceptor.onValidate(mockQuantityBreakDeal, null);
    }

    private QuantityBreakDealModel createQuantityBreakDeal() {
        final Collection<CategoryModel> categories = setUpCategoriesMockData();
        final List<DealBreakPointModel> dealBreakPoints = setupDealBreakPointsData();
        final QuantityBreakDealModel mockQuantityBreakDeal = Mockito.mock(QuantityBreakDealModel.class);
        BDDMockito.given(mockQuantityBreakDeal.getCategories()).willReturn(categories);
        BDDMockito.given(mockQuantityBreakDeal.getEnabled()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockQuantityBreakDeal.getBreakPoints()).willReturn(dealBreakPoints);
        return mockQuantityBreakDeal;
    }

    private Collection<CategoryModel> setUpCategoriesMockData() {
        final CatalogVersionModel mockStagedCatalogVersion = Mockito.mock(CatalogVersionModel.class);
        BDDMockito.given(mockStagedCatalogVersion.getActive()).willReturn(Boolean.FALSE);

        final CatalogVersionModel mockOnlineCatalogVersion = Mockito.mock(CatalogVersionModel.class);
        BDDMockito.given(mockStagedCatalogVersion.getActive()).willReturn(Boolean.TRUE);

        final TargetDealCategoryModel mockStagedCategory = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(mockStagedCategory.getCatalogVersion()).willReturn(mockStagedCatalogVersion);

        final TargetDealCategoryModel mockOnlineCategory = Mockito.mock(TargetDealCategoryModel.class);
        BDDMockito.given(mockOnlineCategory.getCatalogVersion()).willReturn(mockOnlineCatalogVersion);

        final Collection<CategoryModel> categories = new ArrayList<>();
        categories.add(mockStagedCategory);
        categories.add(mockOnlineCategory);
        return categories;

    }

    private List<DealBreakPointModel> setupDealBreakPointsData() {
        final DealBreakPointModel mockDealBreakPoint1 = Mockito.mock(DealBreakPointModel.class);
        final DealBreakPointModel mockDealBreakPoint2 = Mockito.mock(DealBreakPointModel.class);

        final List<DealBreakPointModel> dealBreakPoints = new ArrayList<>();
        dealBreakPoints.add(mockDealBreakPoint1);
        dealBreakPoints.add(mockDealBreakPoint2);
        return dealBreakPoints;
    }

}

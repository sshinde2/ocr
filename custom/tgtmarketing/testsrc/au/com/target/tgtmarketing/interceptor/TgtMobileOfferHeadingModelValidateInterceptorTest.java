/**
 * 
 */
package au.com.target.tgtmarketing.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtmarketing.interceptor.TgtMobileOfferHeadingModelValidateInterceptor.ErrorMessages;
import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;
import junit.framework.Assert;



/**
 * @author paul
 *
 */
@UnitTest
public class TgtMobileOfferHeadingModelValidateInterceptorTest {

    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";

    private final TgtMobileOfferHeadingModelValidateInterceptor tgtMobileOfferHeadingModelValidateInterceptor = new TgtMobileOfferHeadingModelValidateInterceptor();

    @Test
    public void testOnValidateWithNullModel() throws InterceptorException {
        tgtMobileOfferHeadingModelValidateInterceptor.onValidate(null, null);
    }

    @Test
    public void testOnValidateWithWrongModelType() throws InterceptorException {
        final ProductPromotionModel mockProductPromotionModel = Mockito.mock(ProductPromotionModel.class);

        tgtMobileOfferHeadingModelValidateInterceptor.onValidate(mockProductPromotionModel, null);

        Mockito.verifyZeroInteractions(mockProductPromotionModel);
    }

    @Test
    public void testOnValidateEmptyColour() throws InterceptorException {
        final TargetMobileOfferHeadingModel mockTargetMobileOfferHeadingModel = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        try {
            tgtMobileOfferHeadingModelValidateInterceptor.onValidate(mockTargetMobileOfferHeadingModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.TARGET_MOBILE_OFFER_HEADING_ERROR
                            + ErrorMessages.COLOUR_REQUIRED);
        }
    }

    @Test
    public void testOnValidateInvalidColour() throws InterceptorException {
        final TargetMobileOfferHeadingModel mockTargetMobileOfferHeadingModel = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        BDDMockito.given(mockTargetMobileOfferHeadingModel.getColour()).willReturn("abcd");
        try {
            tgtMobileOfferHeadingModelValidateInterceptor.onValidate(mockTargetMobileOfferHeadingModel, null);
            Assert.fail("Expected an InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.TARGET_MOBILE_OFFER_HEADING_ERROR
                            + ErrorMessages.INVALID_COLOUR_FORMAT);
        }
    }

    @Test
    public void testOnValidateSuccessful() throws InterceptorException {
        final TargetMobileOfferHeadingModel mockTargetMobileOfferHeadingModel = Mockito
                .mock(TargetMobileOfferHeadingModel.class);
        BDDMockito.given(mockTargetMobileOfferHeadingModel.getColour()).willReturn("#4EB9C7");
        tgtMobileOfferHeadingModelValidateInterceptor.onValidate(mockTargetMobileOfferHeadingModel, null);
    }
}

package au.com.target.tgtmarketing.offers;

import de.hybris.platform.core.Registry;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;



/**
 * 
 */
public final class TargetMobileOfferHeadingsTestHelper {

    private static ModelService modelService = (ModelService)Registry.getApplicationContext().getBean("modelService");

    private TargetMobileOfferHeadingsTestHelper() {
        // never instantiate
    }

    public static TargetMobileOfferHeadingModel setupTargetMobileOfferHeadingsData(final String code,
            final String name, final String colour) {
        final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel = modelService
                .create(TargetMobileOfferHeadingModel.class);
        targetMobileOfferHeadingModel.setCode(code);
        targetMobileOfferHeadingModel.setName(name);
        targetMobileOfferHeadingModel.setColour(colour);
        modelService.save(targetMobileOfferHeadingModel);
        return targetMobileOfferHeadingModel;
    }

    public static void removeTargetMobileOfferHeadingsData(
            final List<TargetMobileOfferHeadingModel> targetMobileOfferHeadingModelList) {
        if (CollectionUtils.isEmpty(targetMobileOfferHeadingModelList)) {
            return;
        }
        for (final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel : targetMobileOfferHeadingModelList) {
            modelService.remove(targetMobileOfferHeadingModel);
        }
    }
}

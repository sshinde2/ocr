/**
 * 
 */
package au.com.target.tgtmarketing.segments;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.restrictions.AbstractRestrictionModel;
import de.hybris.platform.cms2.model.restrictions.CMSUserGroupRestrictionModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.usergroups.model.UserSegmentModel;

import java.util.Arrays;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtwebcore.model.cms2.pages.TargetFragmentPageModel;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UserSegmentRestrictionForFragmentPageEvaluatorTest {

    private final UserSegmentRestrictionForFragmentPageEvaluator evaluator = new UserSegmentRestrictionForFragmentPageEvaluator();

    @Test
    public void testEvaluateWhenNoPageRestrictionsPresent() {

        final TargetFragmentPageModel targetFragmentPageModel = Mockito.mock(TargetFragmentPageModel.class);
        final UserSegmentModel userSegmentModel = Mockito.mock(UserSegmentModel.class);
        Assertions.assertThat(
                evaluator.evaluate(targetFragmentPageModel, userSegmentModel))
                .isFalse();
    }

    @Test
    public void testEvaluateWhenUserGroupRestrictionPresentButDoesNotMatchSegment() {

        final TargetFragmentPageModel targetFragmentPageModel = Mockito.mock(TargetFragmentPageModel.class);
        final UserSegmentModel userSegmentModel = Mockito.mock(UserSegmentModel.class);
        final UserSegmentModel userSegment = Mockito.mock(UserSegmentModel.class);
        BDDMockito.given(userSegment.getUid())
                .willReturn("womens");
        final AbstractRestrictionModel cmsUserGroupRestrictionModel = Mockito
                .mock(CMSUserGroupRestrictionModel.class);
        final UserGroupModel userSegmentInRestriction = Mockito.mock(UserSegmentModel.class);
        BDDMockito.given(userSegmentInRestriction.getUid())
                .willReturn("mens");
        final List<UserGroupModel> restrictionGroups = Arrays.asList(userSegmentInRestriction);
        BDDMockito.given(((CMSUserGroupRestrictionModel)cmsUserGroupRestrictionModel).getUserGroups())
                .willReturn(restrictionGroups);
        final List<AbstractRestrictionModel> abstractRestrictionModel = Arrays.asList(cmsUserGroupRestrictionModel);
        BDDMockito.given(targetFragmentPageModel.getRestrictions()).willReturn(abstractRestrictionModel);
        Assertions.assertThat(
                evaluator.evaluate(targetFragmentPageModel, userSegmentModel))
                .isFalse();
    }

    @Test
    public void testEvaluateWhenUserGroupRestrictionPresentAndMatchSegment() {

        final TargetFragmentPageModel targetFragmentPageModel = Mockito.mock(TargetFragmentPageModel.class);
        final UserSegmentModel userSegment = Mockito.mock(UserSegmentModel.class);
        BDDMockito.given(userSegment.getUid())
                .willReturn("womens");
        final AbstractRestrictionModel cmsUserGroupRestrictionModel = Mockito
                .mock(CMSUserGroupRestrictionModel.class);
        final UserGroupModel userSegmentInRestriction = Mockito.mock(UserSegmentModel.class);
        BDDMockito.given(userSegmentInRestriction.getUid())
                .willReturn("womens");
        final List<UserGroupModel> restrictionGroups = Arrays.asList(userSegmentInRestriction);
        BDDMockito.given(((CMSUserGroupRestrictionModel)cmsUserGroupRestrictionModel).getUserGroups())
                .willReturn(restrictionGroups);
        final List<AbstractRestrictionModel> abstractRestrictionModel = Arrays.asList(cmsUserGroupRestrictionModel);
        BDDMockito.given(targetFragmentPageModel.getRestrictions()).willReturn(abstractRestrictionModel);
        Assertions.assertThat(
                evaluator.evaluate(targetFragmentPageModel, userSegment))
                .isTrue();
    }

    @Test
    public void testEvaluateWhenUserGroupRestrictionWhichMatchAlongWithOtherInvalidRestrictions() {

        final TargetFragmentPageModel targetFragmentPageModel = Mockito.mock(TargetFragmentPageModel.class);
        final UserSegmentModel userSegment = Mockito.mock(UserSegmentModel.class);
        BDDMockito.given(userSegment.getUid())
                .willReturn("womens");

        final AbstractRestrictionModel cmsUserGroupRestrictionModelWithUserSegment = Mockito
                .mock(CMSUserGroupRestrictionModel.class);
        final UserGroupModel userSegmentInRestriction = Mockito.mock(UserSegmentModel.class);
        BDDMockito.given(userSegmentInRestriction.getUid())
                .willReturn("womens");
        final List<UserGroupModel> restrictionGroups = Arrays.asList(userSegmentInRestriction);
        BDDMockito.given(((CMSUserGroupRestrictionModel)cmsUserGroupRestrictionModelWithUserSegment).getUserGroups())
                .willReturn(restrictionGroups);

        final AbstractRestrictionModel userGroupRestrictionWithUserGroups = Mockito
                .mock(CMSUserGroupRestrictionModel.class);
        final UserGroupModel invalidGroup = Mockito.mock(UserGroupModel.class);
        BDDMockito.given(invalidGroup.getUid())
                .willReturn("123");
        final List<UserGroupModel> invalidRestrictionGroups = Arrays.asList(invalidGroup);
        BDDMockito.given(((CMSUserGroupRestrictionModel)userGroupRestrictionWithUserGroups).getUserGroups())
                .willReturn(invalidRestrictionGroups);

        final List<AbstractRestrictionModel> abstractRestrictionModel = Arrays
                .asList(cmsUserGroupRestrictionModelWithUserSegment, userGroupRestrictionWithUserGroups);
        BDDMockito.given(targetFragmentPageModel.getRestrictions()).willReturn(abstractRestrictionModel);
        Assertions.assertThat(
                evaluator.evaluate(targetFragmentPageModel, userSegment))
                .isTrue();
    }

    @Test
    public void testEvaluateWhenUserGroupRestrictionWithMultipleUserSegmentsInARestriction() {

        final TargetFragmentPageModel targetFragmentPageModel = Mockito.mock(TargetFragmentPageModel.class);
        final UserSegmentModel userSegment = Mockito.mock(UserSegmentModel.class);
        BDDMockito.given(userSegment.getUid())
                .willReturn("womens");

        final AbstractRestrictionModel cmsUserGroupRestrictionModelWithUserSegment = Mockito
                .mock(CMSUserGroupRestrictionModel.class);
        final UserGroupModel userSegmentInRestriction1 = Mockito.mock(UserSegmentModel.class);
        BDDMockito.given(userSegmentInRestriction1.getUid())
                .willReturn("womens");
        final UserGroupModel userSegmentInRestriction2 = Mockito.mock(UserSegmentModel.class);
        BDDMockito.given(userSegmentInRestriction2.getUid())
                .willReturn("mens");
        final List<UserGroupModel> restrictionGroups = Arrays.asList(userSegmentInRestriction1,
                userSegmentInRestriction2);
        BDDMockito.given(((CMSUserGroupRestrictionModel)cmsUserGroupRestrictionModelWithUserSegment).getUserGroups())
                .willReturn(restrictionGroups);


        final List<AbstractRestrictionModel> abstractRestrictionModel = Arrays
                .asList(cmsUserGroupRestrictionModelWithUserSegment);
        BDDMockito.given(targetFragmentPageModel.getRestrictions()).willReturn(abstractRestrictionModel);
        Assertions.assertThat(
                evaluator.evaluate(targetFragmentPageModel, userSegment))
                .isTrue();
    }


}

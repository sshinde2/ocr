/**
 * 
 */
package au.com.target.tgtmarketing.voucher.impl;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.voucher.model.DateRestrictionModel;
import de.hybris.platform.voucher.model.PromotionVoucherModel;
import de.hybris.platform.voucher.model.RestrictionModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.fest.assertions.Assertions;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetProductRestrictionModel;
import au.com.target.tgtmarketing.voucher.dao.TargetMobileVoucherDao;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetMobileVoucherServiceImplTest {

    @Mock
    private TargetMobileVoucherDao targetMobileVoucherDao;

    @InjectMocks
    private final TargetMobileVoucherServiceImpl targetMobileVoucherServiceImpl = new TargetMobileVoucherServiceImpl();

    @Test
    public void testGetAllMobileActiveVouchersWhenNullReturnedFromDao() {
        BDDMockito.given(targetMobileVoucherDao.getAllActiveMobileVouchers()).willReturn(
                new ArrayList<PromotionVoucherModel>());
        Assertions.assertThat(targetMobileVoucherServiceImpl.getAllActiveMobileVouchers()).isEmpty();
    }

    @Test
    public void testGetAllMobileActiveVouchersWhenNoVouchersReturned() {
        BDDMockito.given(targetMobileVoucherDao.getAllActiveMobileVouchers()).willReturn(
                new ArrayList<PromotionVoucherModel>());
        Assertions.assertThat(targetMobileVoucherServiceImpl.getAllActiveMobileVouchers()).isEmpty();
    }

    @Test
    public void testGetAllMobileActiveVouchersWithNoRestrictions() {
        final PromotionVoucherModel promotionVoucherModel = Mockito.mock(PromotionVoucherModel.class);
        BDDMockito.given(promotionVoucherModel.getRestrictions()).willReturn(null);
        BDDMockito.given(targetMobileVoucherDao.getAllActiveMobileVouchers()).willReturn(
                new ArrayList<PromotionVoucherModel>(Arrays.asList(promotionVoucherModel)));
        Assertions.assertThat(targetMobileVoucherServiceImpl.getAllActiveMobileVouchers()).isEmpty();
    }

    @Test
    public void testGetAllMobileActiveVouchersWithInvalidTimeRestriction() {
        final DateRestrictionModel dateRestrictionModelInvalid = createMockDateRestriction(-20, -10);
        final PromotionVoucherModel promotionVoucherModel = Mockito.mock(PromotionVoucherModel.class);
        final Set<RestrictionModel> restrictionModelList = new HashSet<>();
        restrictionModelList.add(dateRestrictionModelInvalid);
        BDDMockito.given(promotionVoucherModel.getRestrictions()).willReturn(restrictionModelList);
        BDDMockito.given(targetMobileVoucherDao.getAllActiveMobileVouchers()).willReturn(
                new ArrayList<PromotionVoucherModel>(Arrays.asList(promotionVoucherModel)));
        Assertions.assertThat(targetMobileVoucherServiceImpl.getAllActiveMobileVouchers()).isEmpty();
    }

    @Test
    public void testGetAllMobileActiveVouchersWithAValidAndInvalidTimeRestriction() {
        final DateRestrictionModel dateRestrictionModelInvalid = createMockDateRestriction(-20, -10);
        final DateRestrictionModel dateRestrictionModelValid = createMockDateRestriction(-10, 20);
        final PromotionVoucherModel promotionVoucherModel = Mockito.mock(PromotionVoucherModel.class);
        final Set<RestrictionModel> restrictionModelList = new HashSet<>();
        restrictionModelList.add(dateRestrictionModelInvalid);
        restrictionModelList.add(dateRestrictionModelValid);
        BDDMockito.given(promotionVoucherModel.getRestrictions()).willReturn(restrictionModelList);
        BDDMockito.given(targetMobileVoucherDao.getAllActiveMobileVouchers()).willReturn(
                new ArrayList<PromotionVoucherModel>(Arrays.asList(promotionVoucherModel)));
        Assertions.assertThat(targetMobileVoucherServiceImpl.getAllActiveMobileVouchers()).isEmpty();
    }

    @Test
    public void testGetAllMobActiveVouchersWithOneValidAndOneInvalidVoucher() {
        final DateRestrictionModel dateRestrictionModelInvalid = createMockDateRestriction(-20, -10);
        final PromotionVoucherModel promotionVoucherModel1 = Mockito.mock(PromotionVoucherModel.class);
        final Set<RestrictionModel> restrictionModelList = new HashSet<>();
        restrictionModelList.add(dateRestrictionModelInvalid);
        BDDMockito.given(promotionVoucherModel1.getRestrictions()).willReturn(restrictionModelList);
        final DateRestrictionModel dateRestrictionModelValid = createMockDateRestriction(-10, 20);
        final PromotionVoucherModel promotionVoucherModel2 = Mockito.mock(PromotionVoucherModel.class);
        final Set<RestrictionModel> restrictionModelList2 = new HashSet<>();
        restrictionModelList2.add(dateRestrictionModelValid);
        BDDMockito.given(promotionVoucherModel2.getRestrictions()).willReturn(restrictionModelList2);
        BDDMockito.given(targetMobileVoucherDao.getAllActiveMobileVouchers()).willReturn(
                new ArrayList<PromotionVoucherModel>(Arrays.asList(promotionVoucherModel1, promotionVoucherModel2)));
        final List<PromotionVoucherModel> promotionVoucherModelList = targetMobileVoucherServiceImpl
                .getAllActiveMobileVouchers();
        Assertions.assertThat(promotionVoucherModelList).isNotEmpty();
        Assertions.assertThat(promotionVoucherModelList.get(0)).isEqualTo(
                promotionVoucherModel2);
    }

    @Test
    public void testGetAllMobActiveVouchersWithTwoValidTimeRestrictionAndProductRestrictionForOneVoucher() {
        final DateRestrictionModel dateRestrictionModelValid1 = createMockDateRestriction(-20, 10);
        final DateRestrictionModel dateRestrictionModelValid2 = createMockDateRestriction(-10, 20);
        final TargetProductRestrictionModel productRestriction = Mockito.mock(TargetProductRestrictionModel.class);
        final PromotionVoucherModel promotionVoucherModel = Mockito.mock(PromotionVoucherModel.class);
        final Set<RestrictionModel> restrictionModelList = new HashSet<>();
        restrictionModelList.add(dateRestrictionModelValid1);
        restrictionModelList.add(dateRestrictionModelValid2);
        restrictionModelList.add(productRestriction);
        BDDMockito.given(promotionVoucherModel.getRestrictions()).willReturn(restrictionModelList);
        BDDMockito.given(targetMobileVoucherDao.getAllActiveMobileVouchers()).willReturn(
                new ArrayList<PromotionVoucherModel>(Arrays.asList(promotionVoucherModel)));
        final List<PromotionVoucherModel> promotionVoucherModelList = targetMobileVoucherServiceImpl
                .getAllActiveMobileVouchers();
        Assertions.assertThat(promotionVoucherModelList).isNotEmpty();
        Assertions.assertThat(promotionVoucherModelList.get(0)).isEqualTo(
                promotionVoucherModel);
    }


    private DateRestrictionModel createMockDateRestriction(final int startDays, final int endDays) {
        final DateRestrictionModel dateRestrictionModel = Mockito.mock(DateRestrictionModel.class);
        final DateTime currentDate = new DateTime();
        BDDMockito.given(dateRestrictionModel.getStartDate())
                .willReturn(currentDate.plusDays(startDays).toDate());
        BDDMockito.given(dateRestrictionModel.getEndDate()).willReturn(currentDate.plusDays(endDays).toDate());
        return dateRestrictionModel;
    }

}

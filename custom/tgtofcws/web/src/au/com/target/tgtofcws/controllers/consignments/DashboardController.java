/**
 * 
 */
package au.com.target.tgtofcws.controllers.consignments;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtofcws.controllers.BaseController;
import au.com.target.tgtwsfacades.instore.TargetInStoreIntegrationFacade;


/**
 * Controller class for OFC Dashboard activities.
 * 
 * @author Rahul
 *
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/store")
public class DashboardController extends BaseController {

    @Resource
    private TargetInStoreIntegrationFacade targetInStoreIntegrationFacade;

    @RequestMapping(value = "dashboard/today", method = RequestMethod.GET)
    @ResponseBody
    public Response getStatisticsForToday(final HttpServletRequest request) {
        return targetInStoreIntegrationFacade.getConsignmentsByStatusForToday(getStoreNumber(request));
    }

    @RequestMapping(value = "dashboard/yesterday", method = RequestMethod.GET)
    @ResponseBody
    public Response getStatisticsForYesterday(final HttpServletRequest request) {
        return targetInStoreIntegrationFacade.getConsignmentsByStatusForYesterday(getStoreNumber(request));
    }

}

/**
 * 
 */
package au.com.target.tgtofcws.controllers.consignments;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtofcws.controllers.BaseController;
import au.com.target.tgtwsfacades.instore.TargetInStoreIntegrationFacade;


/**
 * Controller class for getting unmanifested consignments
 * 
 * @author Nandini
 *
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/store/manifests")
public class ManifestsController extends BaseController {

    @Resource
    private TargetInStoreIntegrationFacade targetInStoreIntegrationFacade;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Response getManifestHistory(final HttpServletRequest request) {
        return targetInStoreIntegrationFacade.getManifestHistoryForStore(getStoreNumber(request).intValue());
    }

    @RequestMapping(value = "{manifestCode}", method = RequestMethod.GET)
    @ResponseBody
    public Response getManifestByCode(@PathVariable final String manifestCode, final HttpServletRequest request) {
        return targetInStoreIntegrationFacade.getManifestByCode(manifestCode, getStoreNumber(request));
    }

    @RequestMapping(value = "unmanifested", method = RequestMethod.GET)
    @ResponseBody
    public Response getUnmanifestedConsignments(final HttpServletRequest request) {
        return targetInStoreIntegrationFacade.getConsignmentsNotManifestedForStore(getStoreNumber(request).intValue());
    }

    @RequestMapping(value = "unmanifested/transmit", method = RequestMethod.POST)
    @ResponseBody
    public Response transmitManifest(final HttpServletRequest request) {
        return targetInStoreIntegrationFacade.transmitManifestForStore(getStoreNumber(request).intValue());
    }

    @RequestMapping(value = "{manifestCode}/summary", method = RequestMethod.GET)
    @ResponseBody
    public Response getHardCopyManifest(@PathVariable final String manifestCode, final HttpServletRequest request) {
        return targetInStoreIntegrationFacade.getHardCopyManifestData(manifestCode, getStoreNumber(request));
    }
}

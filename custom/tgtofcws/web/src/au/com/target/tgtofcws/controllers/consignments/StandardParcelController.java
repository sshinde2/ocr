/**
 * 
 */
package au.com.target.tgtofcws.controllers.consignments;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtofcws.controllers.BaseController;
import au.com.target.tgtwsfacades.instore.TargetInStoreIntegrationFacade;


/**
 * @author smishra1
 *
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}")
public class StandardParcelController extends BaseController {
    @Resource
    private TargetInStoreIntegrationFacade targetInStoreIntegrationFacade;

    /**
     * Get All Standard parcel details
     *
     * @param request
     *            request
     * @return Response with the list of all standard parcel details
     */
    @RequestMapping(value = "standard-parcels", method = RequestMethod.GET)
    @ResponseBody
    public Response getEnabledStandardParcelDetails(final HttpServletRequest request) {
        return targetInStoreIntegrationFacade.getStandardParcelDetails();
    }

}

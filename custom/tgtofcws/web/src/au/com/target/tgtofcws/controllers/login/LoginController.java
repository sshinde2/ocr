/**
 * 
 */
package au.com.target.tgtofcws.controllers.login;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtwsfacades.instore.TargetInStoreIntegrationFacade;
import au.com.target.tgtwsfacades.instore.dto.OfcErrorConstants;
import au.com.target.tgtwsfacades.instore.dto.user.UserResponseData;



/**
 * @author rsamuel3
 *
 */
@Controller
@RequestMapping(value = "/login")
public class LoginController {


    @Resource
    private TargetInStoreIntegrationFacade targetInStoreIntegrationFacade;


    @RequestMapping(value = "success", method = RequestMethod.GET)
    @ResponseBody
    public Response loginSuccess() {
        final Response response = new Response(true);

        final UserResponseData data = targetInStoreIntegrationFacade.getLoggedInUser();
        response.setData(data);
        return response;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Response login() {
        final Response response = targetInStoreIntegrationFacade.createErrorResponse(OfcErrorConstants.REQUIRES_LOGIN,
                OfcErrorConstants.ERR_LOGIN);
        return response;
    }

    @RequestMapping(value = "failed", method = RequestMethod.GET)
    @ResponseBody
    public Response loginFailed() {
        final Response response = targetInStoreIntegrationFacade.createErrorResponse(
                OfcErrorConstants.AUTHENTICATION_FAILED, OfcErrorConstants.ERR_LOGIN);
        return response;
    }


}

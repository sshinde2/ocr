/**
 * 
 */
package au.com.target.tgtofcws.controllers.login;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.response.data.Response;



/**
 * @author rsamuel3
 *
 */
@Controller
@RequestMapping(value = "/logout")
public class LogoutController {

    @RequestMapping(value = "success", method = RequestMethod.GET)
    @ResponseBody
    public Response logoutSuccess() {
        final Response response = new Response(true);
        return response;
    }
}

/**
 * 
 */
package au.com.target.tgtofcws.security;

import de.hybris.platform.servicelayer.exceptions.ClassMismatchException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;

import au.com.target.tgtcore.model.StoreEmployeeModel;
import au.com.target.tgtfacades.login.TargetAuthenticationFacade;



/**
 * @author rsamuel3
 *
 */
public class TgtOfcwsPreAuthenticationChecks implements UserDetailsChecker {

    /**
     * 
     */
    private static final String USER_DOESNOT_EXIST = "User doesn't exist";

    private static final String STOREFULFILMENT_EMPOYEE_GROUP = "storefulfilmentemployeegroup";

    private static final String BAD_CREDENTIALS = "Login attempt as " + STOREFULFILMENT_EMPOYEE_GROUP + " is rejected";

    private static final String WRONG_TYPE = "Login attempt failed because user is not a Store Employee";

    private static final Logger LOG = Logger.getLogger(TgtOfcwsPreAuthenticationChecks.class);


    /**
     * 
     */

    private static final String ROLE_STOREFULFILMENT_EMPOYEE_GROUP = "ROLE_"
            + StringUtils.upperCase(STOREFULFILMENT_EMPOYEE_GROUP);

    private TargetAuthenticationFacade targetAuthenticationFacade;

    private UserService userService;

    private GrantedAuthority storeFulfilmentAuthority = new SimpleGrantedAuthority(ROLE_STOREFULFILMENT_EMPOYEE_GROUP);

    /* (non-Javadoc)
     * @see org.springframework.security.core.userdetails.UserDetailsChecker#check(org.springframework.security.core.userdetails.UserDetails)
     */
    @Override
    public void check(final UserDetails details) {
        // Check if user is anonymous

        // Check if the user is in role storefulfilmentgroup
        if (getStoreFulfilmentAuthority() != null && !details.getAuthorities().contains(getStoreFulfilmentAuthority()))
        {
            LOG.error(BAD_CREDENTIALS);
            throw new BadCredentialsException(BAD_CREDENTIALS);
        }

        try {
            userService.getUserForUID(details.getUsername(), StoreEmployeeModel.class);
        }
        catch (final UnknownIdentifierException ex) {
            LOG.error(USER_DOESNOT_EXIST, ex);
            throw new BadCredentialsException(USER_DOESNOT_EXIST, ex);
        }
        catch (final ClassMismatchException cme) {
            LOG.error(WRONG_TYPE, cme);
            throw new BadCredentialsException(USER_DOESNOT_EXIST, cme);
        }

    }

    /**
     * @return the targetAuthenticationFacade
     */
    protected TargetAuthenticationFacade getTargetAuthenticationFacade() {
        return targetAuthenticationFacade;
    }

    /**
     * @param targetAuthenticationFacade
     *            the targetAuthenticationFacade to set
     */
    @Required
    public void setTargetAuthenticationFacade(final TargetAuthenticationFacade targetAuthenticationFacade) {
        this.targetAuthenticationFacade = targetAuthenticationFacade;
    }

    /**
     * @return the userService
     */
    protected UserService getUserService() {
        return userService;
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @param storefulfilmentGroup
     *            the adminGroup to set
     */
    public void setStoreFulfilmentGroup(final String storefulfilmentGroup)
    {
        if (StringUtils.isBlank(storefulfilmentGroup))
        {
            storeFulfilmentAuthority = null;
        }
        else
        {
            storeFulfilmentAuthority = new SimpleGrantedAuthority(storefulfilmentGroup);
        }
    }

    protected GrantedAuthority getStoreFulfilmentAuthority()
    {
        return storeFulfilmentAuthority;
    }
}

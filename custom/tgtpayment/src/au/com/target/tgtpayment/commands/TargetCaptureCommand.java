package au.com.target.tgtpayment.commands;

import de.hybris.platform.payment.commands.Command;

import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;


/**
 * Command for handling fund captures. Does not require any previous authorized transaction.
 */
public interface TargetCaptureCommand extends Command<TargetCaptureRequest, TargetCaptureResult>
{

    //

}

package au.com.target.tgtpayment.commands;

import de.hybris.platform.payment.commands.Command;

import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;


/**
 * Command for creating a subscription.
 */
public interface TargetCreateSubscriptionCommand extends
        Command<TargetCreateSubscriptionRequest, TargetCreateSubscriptionResult>
{

    //

}

/**
 * 
 */
package au.com.target.tgtpayment.commands;

import de.hybris.platform.payment.commands.Command;

import au.com.target.tgtpayment.commands.request.TargetExcessiveRefundRequest;
import au.com.target.tgtpayment.commands.result.TargetExcessiveRefundResult;


/**
 * @author jspence8
 * 
 */
public interface TargetExcessiveRefundCommand extends
        Command<TargetExcessiveRefundRequest, TargetExcessiveRefundResult> {

    //
}

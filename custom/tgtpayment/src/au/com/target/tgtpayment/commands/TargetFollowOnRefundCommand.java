package au.com.target.tgtpayment.commands;

import de.hybris.platform.payment.commands.Command;

import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;


/**
 * Command for handling follow-on refunds. Follow-on refund means to return back money to customer account associated
 * with order or previous transaction. It is contrary to stand-alone refund {@link TargetStandaloneRefundCommand}
 * 
 */
public interface TargetFollowOnRefundCommand extends Command<TargetFollowOnRefundRequest, TargetFollowOnRefundResult>
{

    //

}

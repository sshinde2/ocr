/**
 * 
 */
package au.com.target.tgtpayment.commands;

import de.hybris.platform.payment.commands.Command;

import au.com.target.tgtpayment.commands.request.TargetGetPaymentConfigurationRequest;
import au.com.target.tgtpayment.commands.result.TargetGetPaymentConfigurationResult;


/**
 * @author bhuang3
 *
 */
public interface TargetGetPaymentConfigurationCommand extends
        Command<TargetGetPaymentConfigurationRequest, TargetGetPaymentConfigurationResult> {
    //
}

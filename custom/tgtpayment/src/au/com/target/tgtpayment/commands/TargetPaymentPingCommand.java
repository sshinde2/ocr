/**
 * 
 */
package au.com.target.tgtpayment.commands;

import de.hybris.platform.payment.commands.Command;

import au.com.target.tgtpayment.commands.request.TargetPaymentPingRequest;
import au.com.target.tgtpayment.commands.result.TargetPaymentPingResult;


/**
 * @author gsing236
 *
 */
public interface TargetPaymentPingCommand extends
        Command<TargetPaymentPingRequest, TargetPaymentPingResult> {
    //
}

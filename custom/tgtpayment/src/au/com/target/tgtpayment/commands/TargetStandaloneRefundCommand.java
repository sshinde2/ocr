package au.com.target.tgtpayment.commands;

import de.hybris.platform.payment.commands.Command;

import au.com.target.tgtpayment.commands.request.AbstractTargetStandaloneRefundRequest;
import au.com.target.tgtpayment.commands.result.TargetStandaloneRefundResult;


/**
 * Command for handling stand-alone refunds. Stand-alone refund means to return back money to customer account not
 * associated with any order or previous transactions. Just passes money from one account to another contrary to
 * {@link TargetFollowOnRefundCommand}
 * 
 */
public interface TargetStandaloneRefundCommand<REQUEST extends AbstractTargetStandaloneRefundRequest>
        extends
        Command<REQUEST, TargetStandaloneRefundResult>
{

    /* (non-Javadoc)
     * @see de.hybris.platform.payment.commands.Command#perform(java.lang.Object)
     */
    @Override
    public TargetStandaloneRefundResult perform(REQUEST request);

}

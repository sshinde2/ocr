package au.com.target.tgtpayment.commands;

import de.hybris.platform.payment.commands.Command;

import au.com.target.tgtpayment.commands.request.TargetTokenizeRequest;
import au.com.target.tgtpayment.commands.result.TargetTokenizeResult;


/**
 * Command for permanently storing the hosted session details in the payment gateway. Refer to
 * {@link TargetCreateSubscriptionCommand} to initiate a hosted payment session
 */
public interface TargetTokenizeCommand extends Command<TargetTokenizeRequest, TargetTokenizeResult> {

    //

}

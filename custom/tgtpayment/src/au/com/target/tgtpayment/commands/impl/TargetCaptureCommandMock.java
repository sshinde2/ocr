/**
 * 
 */
package au.com.target.tgtpayment.commands.impl;

import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.util.Date;

import au.com.target.tgtpayment.commands.TargetCaptureCommand;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpayment.constants.TgtpaymentConstants;


public class TargetCaptureCommandMock implements TargetCaptureCommand {

    /* (non-Javadoc)
     * @see de.hybris.platform.payment.commands.Command#perform(java.lang.Object)
     */
    @Override
    public TargetCaptureResult perform(final TargetCaptureRequest request) {

        final TargetCaptureResult result = new TargetCaptureResult();

        result.setCurrency(request.getCurrency());
        result.setTotalAmount(request.getTotalAmount());
        result.setRequestTime(new Date());
        result.setRequestToken(TgtpaymentConstants.MOCKTOKEN);
        result.setRequestId(TgtpaymentConstants.MOCKID);
        result.setTransactionStatus(TransactionStatus.ACCEPTED);
        result.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL);
        result.setMerchantTransactionCode(request.getTransactionId());

        return result;
    }

}

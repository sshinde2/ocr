/**
 * 
 */
package au.com.target.tgtpayment.commands.impl;

import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.util.Date;

import au.com.target.tgtpayment.commands.TargetFollowOnRefundCommand;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpayment.constants.TgtpaymentConstants;


public class TargetFollowOnRefundCommandMock implements TargetFollowOnRefundCommand {

    /* (non-Javadoc)
     * @see de.hybris.platform.payment.commands.Command#perform(java.lang.Object)
     */
    @Override
    public TargetFollowOnRefundResult perform(final TargetFollowOnRefundRequest request) {

        final TargetFollowOnRefundResult result = new TargetFollowOnRefundResult();

        result.setCurrency(request.getCurrency());
        result.setTotalAmount(request.getTotalAmount());
        result.setRequestTime(new Date());
        result.setRequestId(TgtpaymentConstants.MOCKID);
        result.setRequestToken(TgtpaymentConstants.MOCKTOKEN);
        result.setTransactionStatus(TransactionStatus.ACCEPTED);
        result.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL);
        result.setMerchantTransactionCode(request.getTransactionId());

        return result;

    }

}

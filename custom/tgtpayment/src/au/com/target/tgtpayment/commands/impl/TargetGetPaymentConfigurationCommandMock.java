/**
 * 
 */
package au.com.target.tgtpayment.commands.impl;

import java.math.BigDecimal;

import au.com.target.tgtpayment.commands.TargetGetPaymentConfigurationCommand;
import au.com.target.tgtpayment.commands.request.TargetGetPaymentConfigurationRequest;
import au.com.target.tgtpayment.commands.result.TargetGetPaymentConfigurationResult;


/**
 * @author bhuang3
 *
 */
public class TargetGetPaymentConfigurationCommandMock implements TargetGetPaymentConfigurationCommand {

    @Override
    public TargetGetPaymentConfigurationResult perform(final TargetGetPaymentConfigurationRequest paramRequest) {
        final TargetGetPaymentConfigurationResult result = new TargetGetPaymentConfigurationResult();
        result.setDescription("mock description");
        result.setType("mock type");
        result.setMaximumAmount(BigDecimal.ZERO);
        result.setMinimumAmount(BigDecimal.ZERO);
        return result;
    }
}

/**
 * 
 */
package au.com.target.tgtpayment.commands.impl;

import au.com.target.tgtpayment.commands.TargetPaymentPingCommand;
import au.com.target.tgtpayment.commands.request.TargetPaymentPingRequest;
import au.com.target.tgtpayment.commands.result.TargetPaymentPingResult;


/**
 * @author gsing236
 *
 */
public class TargetPaymentPingCommandMock implements TargetPaymentPingCommand {

    @Override
    public TargetPaymentPingResult perform(final TargetPaymentPingRequest request) {
        final TargetPaymentPingResult result = new TargetPaymentPingResult();
        result.setSuccess(true);
        return result;
    }

}

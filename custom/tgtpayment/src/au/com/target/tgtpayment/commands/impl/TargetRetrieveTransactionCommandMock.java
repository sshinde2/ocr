/**
 * 
 */
package au.com.target.tgtpayment.commands.impl;

import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.math.BigDecimal;

import au.com.target.tgtpayment.commands.TargetRetrieveTransactionCommand;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;
import au.com.target.tgtpayment.constants.TgtpaymentConstants;


public class TargetRetrieveTransactionCommandMock implements TargetRetrieveTransactionCommand {

    /* (non-Javadoc)
     * @see de.hybris.platform.payment.commands.Command#perform(java.lang.Object)
     */
    @Override
    public TargetRetrieveTransactionResult perform(final TargetRetrieveTransactionRequest request) {

        final TargetRetrieveTransactionResult result = new TargetRetrieveTransactionResult();

        result.setTotalAmount(new BigDecimal("10.99"));
        result.setTransactionStatus(TransactionStatus.ACCEPTED);
        result.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL);
        result.setMerchantTransactionCode(request.getTransactionId());
        result.setRequestId(TgtpaymentConstants.MOCKID);
        result.setRequestToken(TgtpaymentConstants.MOCKTOKEN);

        return result;
    }
}

/**
 * 
 */
package au.com.target.tgtpayment.commands.impl;

import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.util.Date;

import au.com.target.tgtpayment.commands.TargetStandaloneRefundCommand;
import au.com.target.tgtpayment.commands.request.TargetCreditCardStandaloneRefundRequest;
import au.com.target.tgtpayment.commands.result.TargetStandaloneRefundResult;
import au.com.target.tgtpayment.constants.TgtpaymentConstants;


/**
 * @author schen13
 * 
 */
public class TargetStandaloneRefundCommandMock
        implements
        TargetStandaloneRefundCommand<TargetCreditCardStandaloneRefundRequest> {

    /* (non-Javadoc)
     * @see de.hybris.platform.payment.commands.Command#perform(java.lang.Object)
     */
    @Override
    public TargetStandaloneRefundResult perform(final TargetCreditCardStandaloneRefundRequest request) {

        final TargetStandaloneRefundResult result = new TargetStandaloneRefundResult();

        result.setCurrency(request.getCurrency());
        result.setTotalAmount(request.getTotalAmount());
        result.setRequestTime(new Date());
        result.setRequestId(TgtpaymentConstants.MOCKID);
        result.setRequestToken(TgtpaymentConstants.MOCKTOKEN);
        result.setTransactionStatus(TransactionStatus.ACCEPTED);
        result.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL);

        return result;
    }
}

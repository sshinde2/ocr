package au.com.target.tgtpayment.commands.request;

/**
 * @author schen13
 * 
 */
public class AbstractRequest {

    private String transactionId;

    /**
     * @return the transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * @param transactionId
     *            the transactionId to set
     */
    public void setTransactionId(final String transactionId) {
        this.transactionId = transactionId;
    }

}

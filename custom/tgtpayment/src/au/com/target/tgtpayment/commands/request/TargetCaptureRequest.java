package au.com.target.tgtpayment.commands.request;

//CHECKSTYLE:OFF
import java.math.BigDecimal;
import java.util.Currency;

import au.com.target.tgtpayment.commands.TargetCaptureCommand;
import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpayment.dto.Order;


//CHECKSTYLE:ON


/**
 * 
 * Request for {@link TargetCaptureCommand}
 * 
 */
public class TargetCaptureRequest extends AbstractRequest
{
    //

    private String token;

    private String orderId;

    private BigDecimal totalAmount;

    private Currency currency;

    private String payerId;

    private Order order;

    private String sessionId;

    private TargetCardResult cardResult;

    private String authorityType;

    private String authorityValue;

    private boolean capture;


    /**
     * @return the card result
     */
    public TargetCardResult getCardResult() {
        return cardResult;
    }

    /**
     * @param cardResult
     *            the ipgCardResult to set
     */
    public void setCardResult(final TargetCardResult cardResult) {
        this.cardResult = cardResult;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token
     *            the token to set
     */
    public void setToken(final String token) {
        this.token = token;
    }

    /**
     * @return the amount
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * @param totalAmount
     *            the amount to set
     */
    public void setTotalAmount(final BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * @return the currency
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * @param currency
     *            the currency to set
     */
    public void setCurrency(final Currency currency) {
        this.currency = currency;
    }

    /**
     * @return the payerId
     */
    public String getPayerId() {
        return payerId;
    }

    /**
     * @param payerId
     *            the payerId to set
     */
    public void setPayerId(final String payerId) {
        this.payerId = payerId;
    }

    /**
     * @return the orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * @param orderId
     *            the orderId to set
     */
    public void setOrderId(final String orderId) {
        this.orderId = orderId;
    }

    /**
     * @return the order
     */
    public Order getOrder() {
        return order;
    }

    /**
     * @param order
     *            the order to set
     */
    public void setOrder(final Order order) {
        this.order = order;
    }

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId
     *            the sessionId to set
     */
    public void setSessionId(final String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @return the authorityType
     */
    public String getAuthorityType() {
        return authorityType;
    }

    /**
     * @param authorityType
     *            the authorityType to set
     */
    public void setAuthorityType(final String authorityType) {
        this.authorityType = authorityType;
    }

    /**
     * @return the authorityValue
     */
    public String getAuthorityValue() {
        return authorityValue;
    }

    /**
     * @param authorityValue
     *            the authorityValue to set
     */
    public void setAuthorityValue(final String authorityValue) {
        this.authorityValue = authorityValue;
    }

    /**
     * @return the capture
     */
    public boolean isCapture() {
        return capture;
    }

    /**
     * @param capture
     *            the capture to set
     */
    public void setCapture(final boolean capture) {
        this.capture = capture;
    }
}
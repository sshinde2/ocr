package au.com.target.tgtpayment.commands.request;

//CHECKSTYLE:OFF

import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;


//CHECKSTYLE:ON

/**
 * 
 * Request for {@link TargetCreateSubscriptionCommand}
 * 
 */
public class TargetCreateChargeRequest extends TargetCreateSubscriptionRequest
{

    private String authorityType;
    private String authorityValue;
    private boolean capture;

    public String getAuthorityType() {
        return authorityType;
    }

    public void setAuthorityType(final String authorityType) {
        this.authorityType = authorityType;
    }

    public String getAuthorityValue() {
        return authorityValue;
    }

    public void setAuthorityValue(final String authorityValue) {
        this.authorityValue = authorityValue;
    }

    public boolean isCapture() {
        return capture;
    }

    public void setCapture(final boolean capture) {
        this.capture = capture;
    }

}
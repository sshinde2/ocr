/**
 * 
 */
package au.com.target.tgtpayment.commands.request;

import de.hybris.platform.payment.dto.CardInfo;


/**
 * @author schen13
 * 
 */
public class TargetCreditCardStandaloneRefundRequest extends AbstractTargetStandaloneRefundRequest {
    private CardInfo cardInfo;

    /**
     * @return the cardInfo
     */
    public CardInfo getCardInfo() {
        return cardInfo;
    }

    /**
     * @param cardInfo
     *            the cardInfo to set
     */
    public void setCardInfo(final CardInfo cardInfo) {
        this.cardInfo = cardInfo;
    }



}

package au.com.target.tgtpayment.commands.request;

import java.math.BigDecimal;
import java.util.Currency;


/**
 * @author jspence8
 * 
 */
public class TargetExcessiveRefundRequest extends AbstractRequest {

    private BigDecimal totalAmount;
    private Currency currency;
    private String originalTransactionId;

    /**
     * @return the totalAmount
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * @param totalAmount
     *            the totalAmount to set
     */
    public void setTotalAmount(final BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * @return the currency
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * @param currency
     *            the currency to set
     */
    public void setCurrency(final Currency currency) {
        this.currency = currency;
    }

    /**
     * @return the originalTransactionId
     */
    public String getOriginalTransactionId() {
        return originalTransactionId;
    }

    /**
     * @param originalTransactionId
     *            the originalTransactionId to set
     */
    public void setOriginalTransactionId(final String originalTransactionId) {
        this.originalTransactionId = originalTransactionId;
    }
}

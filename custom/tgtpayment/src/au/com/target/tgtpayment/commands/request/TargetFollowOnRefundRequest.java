package au.com.target.tgtpayment.commands.request;

//CHECKSTYLE:OFF
import java.math.BigDecimal;
import java.util.Currency;

import au.com.target.tgtpayment.commands.TargetFollowOnRefundCommand;


//CHECKSTYLE:ON

/**
 * 
 * Request for {@link TargetFollowOnRefundCommand}
 * 
 */
public class TargetFollowOnRefundRequest extends AbstractRequest
{

    private String token;
    private String orderId;
    private Currency currency;
    private BigDecimal totalAmount;
    private String captureTransactionId;
    private boolean partial;
    private boolean giftcard;

    //used by IPG for refund
    private String receiptNumber;

    /**
     * @return the currency
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * @param currency
     *            the currency to set
     */
    public void setCurrency(final Currency currency) {
        this.currency = currency;
    }

    /**
     * @return the amount
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * @param totalAmount
     *            the amount to set
     */
    public void setTotalAmount(final BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token
     *            the token to set
     */
    public void setToken(final String token) {
        this.token = token;
    }

    /**
     * @return the orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * @param orderId
     *            the orderId to set
     */
    public void setOrderId(final String orderId) {
        this.orderId = orderId;
    }

    /**
     * @return the captureTransactionId
     */
    public String getCaptureTransactionId() {
        return captureTransactionId;
    }

    /**
     * @param captureTransactionId
     *            the captureTransactionId to set
     */
    public void setCaptureTransactionId(final String captureTransactionId) {
        this.captureTransactionId = captureTransactionId;
    }

    /**
     * @return the partial
     */
    public boolean isPartial() {
        return partial;
    }

    /**
     * @param partial
     *            the partial to set
     */
    public void setPartial(final boolean partial) {
        this.partial = partial;
    }

    /**
     * @return the receiptNumber
     */
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * @param receiptNumber
     *            the receiptNumber to set
     */
    public void setReceiptNumber(final String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    /**
     * @return the giftcard
     */
    public boolean isGiftcard() {
        return giftcard;
    }

    /**
     * @param giftcard
     *            the giftcard to set
     */
    public void setGiftcard(final boolean giftcard) {
        this.giftcard = giftcard;
    }



}

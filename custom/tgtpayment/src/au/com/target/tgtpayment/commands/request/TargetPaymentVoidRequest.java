package au.com.target.tgtpayment.commands.request;

//CHECKSTYLE:OFF
import au.com.target.tgtpayment.commands.TargetPaymentVoidCommand;


//CHECKSTYLE:ON

/**
 * Request for {@link TargetPaymentVoidCommand}
 *
 */
public class TargetPaymentVoidRequest {
    /**
     * Receipt number of the transaction to be voided
     */
    private String receiptNumber;

    /**
     * @return the receiptNumber
     */
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * @param receiptNumber
     *            the receiptNumber to set
     */
    public void setReceiptNumber(final String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }
}

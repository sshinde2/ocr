package au.com.target.tgtpayment.commands.request;

import java.math.BigDecimal;
import java.util.Date;

//CHECKSTYLE:OFF
import au.com.target.tgtpayment.commands.TargetRetrieveTransactionCommand;


//CHECKSTYLE:ON

/**
 * 
 * Request for {@link TargetRetrieveTransactionCommand}
 * 
 */
public class TargetRetrieveTransactionRequest extends AbstractRequest
{
    public enum TrnType {
        ALL, FAILED, APPROVED;
    }

    private String orderId;

    //SST Used by IPG for retrieving transaction results
    private String sessionToken;

    //Used by IPG for retrieving transaction results
    private String sessionId;

    private boolean queryGiftCardOnly;

    private Date orderDate;

    private String receiptNumber;

    private TrnType trnType;

    private BigDecimal entryAmount;


    /**
     * @return the entryAmount
     */
    public BigDecimal getEntryAmount() {
        return entryAmount;
    }

    /**
     * @param entryAmount
     *            the entryAmount to set
     */
    public void setEntryAmount(final BigDecimal entryAmount) {
        this.entryAmount = entryAmount;
    }

    /**
     * @return the orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * @param orderId
     *            the orderId to set
     */
    public void setOrderId(final String orderId) {
        this.orderId = orderId;
    }

    /**
     * @return the sessionToken
     */
    public String getSessionToken() {
        return sessionToken;
    }

    /**
     * @param sessionToken
     *            the sessionToken to set
     */
    public void setSessionToken(final String sessionToken) {
        this.sessionToken = sessionToken;
    }

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId
     *            the sessionId to set
     */
    public void setSessionId(final String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @return the orderDate
     */
    public Date getOrderDate() {
        return orderDate;
    }

    /**
     * @param orderDate
     *            the orderDate to set
     */
    public void setOrderDate(final Date orderDate) {
        this.orderDate = orderDate;
    }

    /**
     * @return the queryGiftCardOnly
     */
    public boolean isQueryGiftCardOnly() {
        return queryGiftCardOnly;
    }

    /**
     * @param queryGiftCardOnly
     *            the queryGiftCardOnly to set
     */
    public void setQueryGiftCardOnly(final boolean queryGiftCardOnly) {
        this.queryGiftCardOnly = queryGiftCardOnly;
    }

    /**
     * @return the receiptNumber
     */
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * @param receiptNumber
     *            the receiptNumber to set
     */
    public void setReceiptNumber(final String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    /**
     * @return the trnType
     */
    public TrnType getTrnType() {
        return trnType;
    }

    /**
     * @param trnType
     *            the trnType to set
     */
    public void setTrnType(final TrnType trnType) {
        this.trnType = trnType;
    }

}
/**
 * 
 */
package au.com.target.tgtpayment.commands.result;

import de.hybris.platform.payment.commands.result.AbstractResult;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;


/**
 * Common base for capture and refund results
 * 
 */
public abstract class TargetAbstractPaymentResult extends AbstractResult {

    private Currency currency;
    private BigDecimal totalAmount;
    private Date requestTime;
    private String paymentProvider;
    private String declineCode;
    private String declineMessage;

    /**
     * @return the paymentProvider
     */
    public String getPaymentProvider()
    {
        return paymentProvider;
    }

    /**
     * @param paymentProvider
     *            the paymentProvider to set
     */
    public void setPaymentProvider(final String paymentProvider)
    {
        this.paymentProvider = paymentProvider;
    }

    /**
     * @return the currency
     */
    public Currency getCurrency()
    {
        return currency;
    }

    /**
     * @param currency
     *            the currency to set
     */
    public void setCurrency(final Currency currency)
    {
        this.currency = currency;
    }

    /**
     * @return the totalAmount
     */
    public BigDecimal getTotalAmount()
    {
        return totalAmount;
    }

    /**
     * @param totalAmount
     *            the totalAmount to set
     */
    public void setTotalAmount(final BigDecimal totalAmount)
    {
        this.totalAmount = totalAmount;
    }

    /**
     * @return the requestTime
     */
    public Date getRequestTime()
    {
        return requestTime;
    }

    /**
     * @param requestTime
     *            the requestTime to set
     */
    public void setRequestTime(final Date requestTime)
    {
        this.requestTime = requestTime;
    }

    /**
     * @return the declineCode
     */
    public String getDeclineCode() {
        return declineCode;
    }

    /**
     * @param declineCode
     *            the declineCode to set
     */
    public void setDeclineCode(final String declineCode) {
        this.declineCode = declineCode;
    }

    /**
     * @return the declineMessage
     */
    public String getDeclineMessage() {
        return declineMessage;
    }

    /**
     * @param declineMessage
     *            the declineMessage to set
     */
    public void setDeclineMessage(final String declineMessage) {
        this.declineMessage = declineMessage;
    }


}

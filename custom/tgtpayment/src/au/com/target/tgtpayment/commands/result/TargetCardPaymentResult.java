package au.com.target.tgtpayment.commands.result;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;


/**
 * Payment details for card
 */
public class TargetCardPaymentResult {
    public static final String TRANSACTION_TYPE_IMMEDIATE = "1";
    public static final String RESPONSE_CODE_SUCCESS = "0";
    public static final String SIMPLE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private BigDecimal amount;
    private String transactionType;
    private String receiptNumber;
    private String responseCode;
    private String responseText;
    private String transactionDateTime;
    private String settlementDate;
    private String declinedCode;
    private String declinedMessage;

    /**
     * Get transaction date as a {@link Date}
     * 
     * @return date
     */
    public Date getTransactionDate() {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(SIMPLE_DATE_FORMAT);
        Date transactionDate;
        try {
            transactionDate = simpleDateFormat.parse(getTransactionDateTime());
        }
        catch (final ParseException e) {
            // Set to current time. At least, it will help in checking the transaction details in HMC
            transactionDate = new Date();
        }
        return transactionDate;
    }

    /**
     * Returns whether payment is an immediate payment
     * 
     * @return is immediate payment
     */
    public boolean isImmediatePayment() {
        if (StringUtils.isNotEmpty(getTransactionType())) {
            return getTransactionType().equals(TRANSACTION_TYPE_IMMEDIATE);
        }
        return false;
    }

    /**
     * Returns whether payment is successful
     * 
     * @return is payment successful
     */
    public boolean isPaymentSuccessful() {
        if (StringUtils.isNotEmpty(getResponseCode())) {
            return getResponseCode().equals(RESPONSE_CODE_SUCCESS);
        }
        return false;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the transactionType
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * @param transactionType
     *            the transactionType to set
     */
    public void setTransactionType(final String transactionType) {
        this.transactionType = transactionType;
    }

    /**
     * @return the receiptNumber
     */
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * @param receiptNumber
     *            the receiptNumber to set
     */
    public void setReceiptNumber(final String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode
     *            the responseCode to set
     */
    public void setResponseCode(final String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseText
     */
    public String getResponseText() {
        return responseText;
    }

    /**
     * @param responseText
     *            the responseText to set
     */
    public void setResponseText(final String responseText) {
        this.responseText = responseText;
    }

    /**
     * @return the transactionDateTime
     */
    public String getTransactionDateTime() {
        return transactionDateTime;
    }

    /**
     * @param transactionDateTime
     *            the transactionDateTime to set
     */
    public void setTransactionDateTime(final String transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    /**
     * @return the settlementDate
     */
    public String getSettlementDate() {
        return settlementDate;
    }

    /**
     * @param settlementDate
     *            the settlementDate to set
     */
    public void setSettlementDate(final String settlementDate) {
        this.settlementDate = settlementDate;
    }

    /**
     * @return the declinedCode
     */
    public String getDeclinedCode() {
        return declinedCode;
    }

    /**
     * @param declinedCode
     *            the declinedCode to set
     */
    public void setDeclinedCode(final String declinedCode) {
        this.declinedCode = declinedCode;
    }

    /**
     * @return the declinedMessage
     */
    public String getDeclinedMessage() {
        return declinedMessage;
    }

    /**
     * @param declinedMessage
     *            the declinedMessage to set
     */
    public void setDeclinedMessage(final String declinedMessage) {
        this.declinedMessage = declinedMessage;
    }
}
package au.com.target.tgtpayment.commands.result;

/**
 * Card details including transaction details relevant to card
 */
public class TargetCardResult {
    private String cardType;
    private String cardNumber;
    private String cardExpiry;
    private String bin;
    private String promoId;
    private String token;
    private String tokenExpiry;
    private boolean isDefaultCard;
    private boolean cardOnFile;
    private boolean firstCredentialStorage;
    private TargetCardPaymentResult targetCardPaymentResult;


    /**
     * @return the cardOnFile
     */
    public boolean isCardOnFile() {
        return cardOnFile;
    }

    /**
     * @param cardOnFile
     *            the cardOnFile to set
     */
    public void setCardOnFile(final boolean cardOnFile) {
		this.cardOnFile = cardOnFile;
    }

    /**
     * @return the firstCredentialStorage
     */
    public boolean isFirstCredentialStorage() {
        return firstCredentialStorage;
    }

    /**
     * @param firstCredentialStorage
     *            the firstCredentialStorage to set
     */
    public void setFirstCredentialStorage(final boolean firstCredentialStorage) {
		this.firstCredentialStorage = firstCredentialStorage;
    }

    /**
     * @return the isDefaultCard
     */
    public boolean isDefaultCard() {
        return isDefaultCard;
    }

    /**
     * @param defaultCard
     *            the isDefaultCard to set
     */
    public void setDefaultCard(final boolean defaultCard) {
        this.isDefaultCard = defaultCard;
    }

    /**
     * @return the cardType
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * @param cardType
     *            the cardType to set
     */
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }

    /**
     * @return the cardNumber
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * @param cardNumber
     *            the cardNumber to set
     */
    public void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * @return the cardExpiry
     */
    public String getCardExpiry() {
        return cardExpiry;
    }

    /**
     * @param cardExpiry
     *            the cardExpiry to set
     */
    public void setCardExpiry(final String cardExpiry) {
        this.cardExpiry = cardExpiry;
    }

    /**
     * @return the bin
     */
    public String getBin() {
        return bin;
    }

    /**
     * @param bin
     *            the bin to set
     */
    public void setBin(final String bin) {
        this.bin = bin;
    }

    /**
     * @return the promoId
     */
    public String getPromoId() {
        return promoId;
    }

    /**
     * @param promoId
     *            the promoId to set
     */
    public void setPromoId(final String promoId) {
        this.promoId = promoId;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token
     *            the token to set
     */
    public void setToken(final String token) {
        this.token = token;
    }

    /**
     * @return the tokenExpiry
     */
    public String getTokenExpiry() {
        return tokenExpiry;
    }

    /**
     * @param tokenExpiry
     *            the tokenExpiry to set
     */
    public void setTokenExpiry(final String tokenExpiry) {
        this.tokenExpiry = tokenExpiry;
    }

    /**
     * @return the card payment result
     */
    public TargetCardPaymentResult getTargetCardPaymentResult() {
        return targetCardPaymentResult;
    }

    /**
     * @param targetCardPaymentResult
     *            the card payment result to set
     */
    public void setTargetCardPaymentResult(final TargetCardPaymentResult targetCardPaymentResult) {
        this.targetCardPaymentResult = targetCardPaymentResult;
    }
}
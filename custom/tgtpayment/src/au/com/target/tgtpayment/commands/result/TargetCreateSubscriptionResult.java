package au.com.target.tgtpayment.commands.result;

//CHECKSTYLE:OFF
import de.hybris.platform.payment.commands.result.AbstractResult;

import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;



//CHECKSTYLE:ON

/**
 * 
 * Result for {@link TargetCreateSubscriptionCommand}
 * 
 */
public class TargetCreateSubscriptionResult extends AbstractResult {

    private String subscriptionID;

    private String zipPaymentRedirectUrl;

    /**
     * @return the subscriptionID
     */
    public String getSubscriptionID() {
        return subscriptionID;
    }

    /**
     * @param subscriptionID
     *            the subscriptionID to set
     */
    public void setSubscriptionID(final String subscriptionID) {
        this.subscriptionID = subscriptionID;
    }

    /**
     * @return the zipPaymentRedirectUrl
     */
    public String getZipPaymentRedirectUrl() {
        return zipPaymentRedirectUrl;
    }

    /**
     * @param zipPaymentRedirectUrl
     *            the zipPaymentRedirectUrl to set
     */
    public void setZipPaymentRedirectUrl(final String zipPaymentRedirectUrl) {
        this.zipPaymentRedirectUrl = zipPaymentRedirectUrl;
    }



}

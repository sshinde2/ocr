package au.com.target.tgtpayment.commands.result;

//CHECKSTYLE:OFF
import au.com.target.tgtpayment.commands.TargetFollowOnRefundCommand;


//CHECKSTYLE:ON

/**
 * 
 * Result for {@link TargetFollowOnRefundCommand}
 * 
 */
public class TargetFollowOnRefundResult extends TargetAbstractPaymentResult
{
    //Refund response from IPG
    private TargetCardPaymentResult refundResult;

    /**
     * @return the refundResult
     */
    public TargetCardPaymentResult getRefundResult() {
        return refundResult;
    }

    /**
     * @param refundResult
     *            the refundResult to set
     */
    public void setRefundResult(final TargetCardPaymentResult refundResult) {
        this.refundResult = refundResult;
    }


}

/**
 * 
 */
package au.com.target.tgtpayment.commands.result;

//CHECKSTYLE:OFF
import au.com.target.tgtpayment.commands.TargetPaymentVoidCommand;


//CHECKSTYLE:ON

/**
 * Result for {@link TargetPaymentVoidCommand}
 *
 */
public class TargetPaymentVoidResult {
    /**
     * Payment void result from IPG
     */
    private TargetCardPaymentResult voidResult;

    /**
     * @return the voidResult
     */
    public TargetCardPaymentResult getVoidResult() {
        return voidResult;
    }

    /**
     * @param voidResult
     *            the voidResult to set
     */
    public void setVoidResult(final TargetCardPaymentResult voidResult) {
        this.voidResult = voidResult;
    }
}
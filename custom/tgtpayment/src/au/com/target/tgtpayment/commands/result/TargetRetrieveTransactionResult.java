package au.com.target.tgtpayment.commands.result;

import java.util.List;

//CHECKSTYLE:OFF
import au.com.target.tgtpayment.commands.TargetRetrieveTransactionCommand;
import au.com.target.tgtpayment.dto.PaymentTransactionEntryData;


//CHECKSTYLE:ON

/**
 * 
 * Result for {@link TargetRetrieveTransactionCommand}
 * 
 */
public class TargetRetrieveTransactionResult extends TargetAbstractPaymentResult {
    private String payerId;
    private Double capturedAmount;
    private List<PaymentTransactionEntryData> payments;
    private int matchingCount;

    /**
     * @return the payerId
     */
    public String getPayerId() {
        return payerId;
    }

    /**
     * @param payerId
     *            the payerId to set
     */
    public void setPayerId(final String payerId) {
        this.payerId = payerId;
    }

    /**
     * @return the capturedAmount
     */
    public Double getCapturedAmount() {
        return capturedAmount;
    }

    /**
     * @param capturedAmount
     *            the capturedAmount to set
     */
    public void setCapturedAmount(final Double capturedAmount) {
        this.capturedAmount = capturedAmount;
    }

    /**
     * @return the payments
     */
    public List<PaymentTransactionEntryData> getPayments() {
        return payments;
    }

    /**
     * @param payments
     *            the payments to set
     */
    public void setPayments(final List<PaymentTransactionEntryData> payments) {
        this.payments = payments;
    }

    /**
     * @return the matchingCount
     */
    public int getMatchingCount() {
        return matchingCount;
    }

    /**
     * @param matchingCount
     *            the matchingCount to set
     */
    public void setMatchingCount(final int matchingCount) {
        this.matchingCount = matchingCount;
    }

}

package au.com.target.tgtpayment.commands.result;

import de.hybris.platform.payment.commands.result.AbstractResult;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;

import au.com.target.tgtpayment.commands.TargetStandaloneRefundCommand;
//CHECKSTYLE:OFF


//CHECKSTYLE:ON

/**
 * 
 * Result for {@link TargetStandaloneRefundCommand}
 * 
 */
public class TargetStandaloneRefundResult extends AbstractResult
{

    private Currency currency;
    private BigDecimal totalAmount;
    private Date requestTime;
    private String paymentProvider;

    /**
     * @return the paymentProvider
     */
    public String getPaymentProvider()
    {
        return paymentProvider;
    }

    /**
     * @param paymentProvider
     *            the paymentProvider to set
     */
    public void setPaymentProvider(final String paymentProvider)
    {
        this.paymentProvider = paymentProvider;
    }

    /**
     * @return the currency
     */
    public Currency getCurrency()
    {
        return currency;
    }

    /**
     * @param currency
     *            the currency to set
     */
    public void setCurrency(final Currency currency)
    {
        this.currency = currency;
    }

    /**
     * @return the totalAmount
     */
    public BigDecimal getTotalAmount()
    {
        return totalAmount;
    }

    /**
     * @param totalAmount
     *            the totalAmount to set
     */
    public void setTotalAmount(final BigDecimal totalAmount)
    {
        this.totalAmount = totalAmount;
    }

    /**
     * @return the requestTime
     */
    public Date getRequestTime()
    {
        return requestTime;
    }

    /**
     * @param requestTime
     *            the requestTime to set
     */
    public void setRequestTime(final Date requestTime)
    {
        this.requestTime = requestTime;
    }

}

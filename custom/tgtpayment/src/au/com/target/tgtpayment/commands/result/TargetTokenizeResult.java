package au.com.target.tgtpayment.commands.result;

//CHECKSTYLE:OFF
import de.hybris.platform.payment.commands.result.AbstractResult;

import au.com.target.tgtpayment.commands.TargetTokenizeCommand;


//CHECKSTYLE:ON

/**
 * 
 * Result for {@link TargetTokenizeCommand}
 * 
 */
public class TargetTokenizeResult extends AbstractResult {

    private String savedToken;

    /**
     * @return the savedToken
     */
    public String getSavedToken() {
        return savedToken;
    }

    /**
     * @param savedToken
     *            the savedToken to set
     */
    public void setSavedToken(final String savedToken) {
        this.savedToken = savedToken;
    }

}

/**
 * 
 */
package au.com.target.tgtpayment.dao;

import au.com.target.tgtpayment.jalo.AfterpayConfig;
import au.com.target.tgtpayment.model.AfterpayConfigModel;


/**
 * @author mgazal
 *
 */
public interface AfterpayConfigDao {

    /**
     * Fetch AfterpayConfig
     * 
     * @return {@link AfterpayConfig}
     */
    AfterpayConfigModel getAfterpayConfig();
}

/**
 * 
 */
package au.com.target.tgtpayment.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtpayment.dao.AfterpayConfigDao;
import au.com.target.tgtpayment.model.AfterpayConfigModel;


/**
 * @author mgazal
 *
 */
public class AfterpayConfigDaoImpl extends DefaultGenericDao<AfterpayConfigModel> implements AfterpayConfigDao {

    public AfterpayConfigDaoImpl() {
        super(AfterpayConfigModel._TYPECODE);
    }

    @Override
    public AfterpayConfigModel getAfterpayConfig() {
        final List<AfterpayConfigModel> afterpayConfigs = find();
        return CollectionUtils.isNotEmpty(afterpayConfigs) ? afterpayConfigs.get(0) : null;
    }

}

/**
 * 
 */
package au.com.target.tgtpayment.dto;

/**
 * Credit card information to be sent with session creation request (used for IPG)
 */
public class CreditCard {
    private String token;
    private String tokenExpiry;
    private String cardType;
    private String maskedCard;
    private String cardExpiry;
    private Boolean defaultCard;
    private String bin;
    private String promoId;
    private Boolean cardOnFile;
    private Boolean firstCredentialStorage;

	/**
	 * @return the cardOnFile
	 */
    public Boolean getCardOnFile() {
        return cardOnFile;
    }

	/**
	 * @param cardOnFile
	 *            the cardOnFile to set
	 */
    public void setCardOnFile(final Boolean cardOnFile) {
		this.cardOnFile = cardOnFile;
    }

	/**
	 * @return the firstCredentialStorage
	 */
    public Boolean getFirstCredentialStorage() {
        return firstCredentialStorage;
    }

	/**
	 * @param firstCredentialStorage
	 *            the firstCredentialStorage to set
	 */
    public void setFirstCredentialStorage(final Boolean firstCredentialStorage) {
		this.firstCredentialStorage = firstCredentialStorage;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token
     *            the token to set
     */
    public void setToken(final String token) {
        this.token = token;
    }

    /**
     * @return the tokenExpiry
     */
    public String getTokenExpiry() {
        return tokenExpiry;
    }

    /**
     * @param tokenExpiry
     *            the tokenExpiry to set
     */
    public void setTokenExpiry(final String tokenExpiry) {
        this.tokenExpiry = tokenExpiry;
    }

    /**
     * @return the cardType
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * @param cardType
     *            the cardType to set
     */
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }

    /**
     * @return the maskedCard
     */
    public String getMaskedCard() {
        return maskedCard;
    }

    /**
     * @param maskedCard
     *            the maskedCard to set
     */
    public void setMaskedCard(final String maskedCard) {
        this.maskedCard = maskedCard;
    }

    /**
     * @return the cardExpiry
     */
    public String getCardExpiry() {
        return cardExpiry;
    }

    /**
     * @param cardExpiry
     *            the cardExpiry to set
     */
    public void setCardExpiry(final String cardExpiry) {
        this.cardExpiry = cardExpiry;
    }

    /**
     * @return the defaultCard
     */
    public Boolean getDefaultCard() {
        return defaultCard;
    }

    /**
     * @param defaultCard
     *            the defaultCard to set
     */
    public void setDefaultCard(final Boolean defaultCard) {
        this.defaultCard = defaultCard;
    }

    /**
     * @return the bin
     */
    public String getBin() {
        return bin;
    }

    /**
     * @param bin
     *            the bin to set
     */
    public void setBin(final String bin) {
        this.bin = bin;
    }

    /**
     * @return the promoId
     */
    public String getPromoId() {
        return promoId;
    }

    /**
     * @param promoId
     *            the promoId to set
     */
    public void setPromoId(final String promoId) {
        this.promoId = promoId;
    }
}
/**
 * 
 */
package au.com.target.tgtpayment.dto;

/**
 * @author mgazal
 *
 */
public class Customer {

    private String givenNames;

    private String surname;

    private String email;

    /**
     * @return the givenNames
     */
    public String getGivenNames() {
        return givenNames;
    }

    /**
     * @param givenNames
     *            the givenNames to set
     */
    public void setGivenNames(final String givenNames) {
        this.givenNames = givenNames;
    }

    /**
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * @param surname
     *            the surname to set
     */
    public void setSurname(final String surname) {
        this.surname = surname;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }
}

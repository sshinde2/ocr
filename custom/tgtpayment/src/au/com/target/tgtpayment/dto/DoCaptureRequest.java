/**
 * 
 */
package au.com.target.tgtpayment.dto;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import java.math.BigDecimal;

import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.methods.TargetPaymentMethod;


public class DoCaptureRequest {

    private AbstractOrderModel orderModel;
    private PaymentInfoModel paymentInfo;
    private PaymentTransactionEntryModel paymentTransactionEntry;
    private BigDecimal amount;
    private CurrencyModel currency;
    private TargetPaymentMethod paymentMethod;
    private PaymentCaptureType paymentCaptureType;

    /**
     * @return the orderModel
     */
    public AbstractOrderModel getOrderModel() {
        return orderModel;
    }

    /**
     * @param orderModel
     *            the orderModel to set
     */
    public void setOrderModel(final AbstractOrderModel orderModel) {
        this.orderModel = orderModel;
    }

    /**
     * @return the paymentInfo
     */
    public PaymentInfoModel getPaymentInfo() {
        return paymentInfo;
    }

    /**
     * @param paymentInfo
     *            the paymentInfo to set
     */
    public void setPaymentInfo(final PaymentInfoModel paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    /**
     * @return the paymentTransactionEntry
     */
    public PaymentTransactionEntryModel getPaymentTransactionEntry() {
        return paymentTransactionEntry;
    }

    /**
     * @param paymentTransactionEntry
     *            the paymentTransactionEntry to set
     */
    public void setPaymentTransactionEntry(final PaymentTransactionEntryModel paymentTransactionEntry) {
        this.paymentTransactionEntry = paymentTransactionEntry;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the currency
     */
    public CurrencyModel getCurrency() {
        return currency;
    }

    /**
     * @param currency
     *            the currency to set
     */
    public void setCurrency(final CurrencyModel currency) {
        this.currency = currency;
    }

    /**
     * @return the paymentMethod
     */
    public TargetPaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * @param paymentMethod
     *            the paymentMethod to set
     */
    public void setPaymentMethod(final TargetPaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /**
     * @return the paymentCaptureType
     */
    public PaymentCaptureType getPaymentCaptureType() {
        return paymentCaptureType;
    }

    /**
     * @param paymentCaptureType
     *            the paymentCaptureType to set
     */
    public void setPaymentCaptureType(final PaymentCaptureType paymentCaptureType) {
        this.paymentCaptureType = paymentCaptureType;
    }
}

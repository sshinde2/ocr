/**
 * 
 */
package au.com.target.tgtpayment.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @author htan3
 *
 */
public class GiftCardReversalData implements Serializable {

    private static final long serialVersionUID = -1L;

    private String customerEmail;

    private String receiptNumber;

    private String amount;

    private String maskedCardNumber;

    private String provider;

    private String mobile;

    private String firstName;

    private String lastName;

    private String title;

    private String cartId;

    private Date paymentDate;

    /**
     * @return the customerEmail
     */
    public String getCustomerEmail() {
        return customerEmail;
    }

    /**
     * @param customerEmail
     *            the customerEmail to set
     */
    public void setCustomerEmail(final String customerEmail) {
        this.customerEmail = customerEmail;
    }

    /**
     * @return the receiptNumber
     */
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * @param receiptNumber
     *            the receiptNumber to set
     */
    public void setReceiptNumber(final String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final String amount) {
        this.amount = amount;
    }

    /**
     * @return the maskedCardNumber
     */
    public String getMaskedCardNumber() {
        return maskedCardNumber;
    }

    /**
     * @param maskedCardNumber
     *            the maskedCardNumber to set
     */
    public void setMaskedCardNumber(final String maskedCardNumber) {
        this.maskedCardNumber = maskedCardNumber;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider
     *            the provider to set
     */
    public void setProvider(final String provider) {
        this.provider = provider;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile
     *            the mobile to set
     */
    public void setMobile(final String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * @return the cartId
     */
    public String getCartId() {
        return cartId;
    }

    /**
     * @param cartId
     *            the cartId to set
     */
    public void setCartId(final String cartId) {
        this.cartId = cartId;
    }

    /**
     * @return the paymentDate
     */
    public Date getPaymentDate() {
        return paymentDate;
    }

    /**
     * @param paymentDate
     *            the paymentDate to set
     */
    public void setPaymentDate(final Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("receipt:");
        builder.append(receiptNumber);
        builder.append("date:");
        builder.append(paymentDate);
        return builder.toString();
    }

}

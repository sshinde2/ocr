/**
 * 
 */
package au.com.target.tgtpayment.dto;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.enums.PaymentCaptureType;


public class HostedSessionTokenRequest {

    private AbstractOrderModel orderModel;
    private BigDecimal amount;
    private PaymentModeModel paymentMode;
    private String cancelUrl;
    private String returnUrl;
    private PaymentCaptureType paymentCaptureType;
    private CurrencyModel currency;
    private String sessionId;
    private List<CreditCard> savedCreditCards;
    private IpgPaymentTemplateType ipgPaymentTemplateType;

    /**
     * @return the orderModel
     */
    public AbstractOrderModel getOrderModel() {
        return orderModel;
    }

    /**
     * @param orderModel
     *            the orderModel to set
     */
    public void setOrderModel(final AbstractOrderModel orderModel) {
        this.orderModel = orderModel;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the paymentMode
     */
    public PaymentModeModel getPaymentMode() {
        return paymentMode;
    }

    /**
     * @param paymentMode
     *            the paymentMode to set
     */
    public void setPaymentMode(final PaymentModeModel paymentMode) {
        this.paymentMode = paymentMode;
    }

    /**
     * @return the cancelUrl
     */
    public String getCancelUrl() {
        return cancelUrl;
    }

    /**
     * @param cancelUrl
     *            the cancelUrl to set
     */
    public void setCancelUrl(final String cancelUrl) {
        this.cancelUrl = cancelUrl;
    }

    /**
     * @return the returnUrl
     */
    public String getReturnUrl() {
        return returnUrl;
    }

    /**
     * @param returnUrl
     *            the returnUrl to set
     */
    public void setReturnUrl(final String returnUrl) {
        this.returnUrl = returnUrl;
    }

    /**
     * @return the paymentCaptureType
     */
    public PaymentCaptureType getPaymentCaptureType() {
        return paymentCaptureType;
    }

    /**
     * @param paymentCaptureType
     *            the paymentCaptureType to set
     */
    public void setPaymentCaptureType(final PaymentCaptureType paymentCaptureType) {
        this.paymentCaptureType = paymentCaptureType;
    }

    /**
     * @return the currency
     */
    public CurrencyModel getCurrency() {
        return currency;
    }

    /**
     * @param currency
     *            the currency to set
     */
    public void setCurrency(final CurrencyModel currency) {
        this.currency = currency;
    }

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId
     *            the sessionId to set
     */
    public void setSessionId(final String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @return the savedCreditCards
     */
    public List<CreditCard> getSavedCreditCards() {
        return savedCreditCards;
    }

    /**
     * Add a credit card to saved credit card list
     * 
     * @param creditCard
     *            the savedCreditCards to set
     */
    public void addSavedCreditCard(final CreditCard creditCard) {
        if (savedCreditCards == null) {
            savedCreditCards = new ArrayList<>();
        }
        savedCreditCards.add(creditCard);
    }

    /**
     * @return the ipgPaymentTemplateType
     */
    public IpgPaymentTemplateType getIpgPaymentTemplateType() {
        return ipgPaymentTemplateType;
    }

    /**
     * @param ipgPaymentTemplateType
     *            the ipgPaymentTemplateType to set
     */
    public void setIpgPaymentTemplateType(final IpgPaymentTemplateType ipgPaymentTemplateType) {
        this.ipgPaymentTemplateType = ipgPaymentTemplateType;
    }


}
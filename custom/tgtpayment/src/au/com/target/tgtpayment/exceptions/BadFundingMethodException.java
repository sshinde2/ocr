package au.com.target.tgtpayment.exceptions;


/**
 * Insufficient funds in the account. Request a different card or other form of payment.
 */
public class BadFundingMethodException extends PaymentException {

    /**
     * @param message
     */
    public BadFundingMethodException(final String message) {
        super(message);
    }

}

/**
 * 
 */
package au.com.target.tgtpayment.interceptors;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import java.math.BigDecimal;


public class PaymentTransactionPrepareInterceptor implements PrepareInterceptor {

    @Override
    public void onPrepare(final Object model, final InterceptorContext ctx) throws InterceptorException {
        if (model instanceof PaymentTransactionModel) {
            final PaymentTransactionModel pt = (PaymentTransactionModel)model;
            if (pt.getPlannedAmount() != null) {
                final CurrencyModel currency = pt.getCurrency();
                if (currency != null) {
                    pt.setPlannedAmount(pt.getPlannedAmount().setScale(currency.getDigits().intValue(),
                            BigDecimal.ROUND_HALF_UP));
                }
            }
        }
    }
}

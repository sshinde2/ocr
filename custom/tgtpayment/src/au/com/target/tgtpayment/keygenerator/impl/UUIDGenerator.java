package au.com.target.tgtpayment.keygenerator.impl;

import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import java.util.UUID;


/**
 * Generates a key using {@link UUID}
 * 
 */
public class UUIDGenerator implements KeyGenerator {

    @Override
    public Object generate() {
        return UUID.randomUUID().toString();
    }

    @Override
    public Object generateFor(final Object object) {
        throw new UnsupportedOperationException("Not supported, please call generate().");
    }

    @Override
    public void reset() {
        throw new UnsupportedOperationException("A reset of UUID is not supported.");
    }

}

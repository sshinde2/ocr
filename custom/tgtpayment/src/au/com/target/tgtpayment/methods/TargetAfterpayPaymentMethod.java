/**
 * 
 */
package au.com.target.tgtpayment.methods;

/**
 * Afterpay payment method
 */
public interface TargetAfterpayPaymentMethod extends TargetPaymentMethod {
    //
}

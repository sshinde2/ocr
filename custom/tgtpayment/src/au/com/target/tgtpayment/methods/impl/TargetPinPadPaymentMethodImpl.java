package au.com.target.tgtpayment.methods.impl;

import de.hybris.platform.payment.commands.factory.CommandFactory;
import de.hybris.platform.payment.commands.request.SubscriptionDataRequest;

import au.com.target.tgtpayment.commands.request.AbstractTargetStandaloneRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.request.TargetExcessiveRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetGetPaymentConfigurationRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentPingRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentVoidRequest;
import au.com.target.tgtpayment.commands.request.TargetQueryTransactionDetailsRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.request.TargetTokenizeRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.commands.result.TargetExcessiveRefundResult;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpayment.commands.result.TargetGetPaymentConfigurationResult;
import au.com.target.tgtpayment.commands.result.TargetGetSubscriptionResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentPingResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentVoidResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;
import au.com.target.tgtpayment.commands.result.TargetStandaloneRefundResult;
import au.com.target.tgtpayment.commands.result.TargetTokenizeResult;
import au.com.target.tgtpayment.methods.TargetPinPadPaymentMethod;


public class TargetPinPadPaymentMethodImpl implements TargetPinPadPaymentMethod {

    private CommandFactory commandFactory;

    @Override
    public TargetCaptureResult capture(final TargetCaptureRequest captureRequest) {
        /* Just need to return null as there will not be any updates which will be happening from here.
         * The payment transaction is getting created while getting the payment transaction entry 
         * just before this method.
        */
        return null;
    }

    @Override
    public TargetRetrieveTransactionResult retrieveTransaction(
            final TargetRetrieveTransactionRequest retrieveTransactionRequest) {
        /* Just need to return null and the entry will be refreshed in 
         * the service layer to fetch the details from the database.
        */
        return null;
    }


    @Override
    public TargetCreateSubscriptionResult createSubscription(
            final TargetCreateSubscriptionRequest createSubscriptionRequest) {
        throw new UnsupportedOperationException("We don't support createSubscription for pinpad.");
    }

    @Override
    public TargetTokenizeResult tokenize(final TargetTokenizeRequest tokenizeRequest) {
        throw new UnsupportedOperationException("We don't support tokenize for pinpad.");
    }

    @Override
    public TargetFollowOnRefundResult followOnRefund(final TargetFollowOnRefundRequest followOnRefundRequest) {
        throw new UnsupportedOperationException("We don't support followOnRefund for pinpad.");
    }

    @Override
    public TargetStandaloneRefundResult standaloneRefund(
            final AbstractTargetStandaloneRefundRequest standaloneRefundRequest) {
        throw new UnsupportedOperationException("We don't support standaloneRefund for pinpad.");
    }

    @Override
    public TargetExcessiveRefundResult excessiveRefund(
            final TargetExcessiveRefundRequest targetExcessiveRefundRequest) {
        throw new UnsupportedOperationException("We don't support excessive refund for pinpad.");
    }

    @Override
    public TargetQueryTransactionDetailsResult queryTransactionDetails(
            final TargetQueryTransactionDetailsRequest targetQueryTransactionDetailsRequest) {
        throw new UnsupportedOperationException("We don't support querying transaction details for pinpad.");
    }

    @Override
    public String getPaymentProvider() {
        if (commandFactory == null) {
            return null;
        }
        return commandFactory.getPaymentProvider();
    }

    @Override
    public void setCommandFactory(final CommandFactory commandFactory) {
        this.commandFactory = commandFactory;

    }

    @Override
    public TargetPaymentVoidResult voidPayment(final TargetPaymentVoidRequest voidRequest) {
        throw new UnsupportedOperationException("Pinpad does not support payment void");
    }

    @Override
    public TargetGetPaymentConfigurationResult getConfiguration(final TargetGetPaymentConfigurationRequest request) {
        throw new UnsupportedOperationException("Pinpad does not support get configuration");
    }

    @Override
    public TargetPaymentPingResult ping(final TargetPaymentPingRequest request) {
        throw new UnsupportedOperationException("Pinpad does not support ping");
    }

    @Override
    public TargetGetSubscriptionResult getSubscription(final SubscriptionDataRequest request) {
        throw new UnsupportedOperationException("Pinpad does not support get subscription");
    }
}
/**
 * 
 */
package au.com.target.tgtpayment.resumer;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;


/**
 * Interface for resuming a process when there has been an interrupted payment
 * 
 */
public interface PaymentProcessResumer {

    /**
     * Resume the process for the given cart or order
     * 
     * @param abstractOrderModel
     */
    void resumeProcess(AbstractOrderModel abstractOrderModel, PaymentTransactionEntryModel paymentTransactionEntryModel);
}

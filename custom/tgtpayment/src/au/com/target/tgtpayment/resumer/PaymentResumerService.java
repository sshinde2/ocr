/**
 * 
 */
package au.com.target.tgtpayment.resumer;


/**
 * Interface supporting resumption of payment processes
 * 
 */
public interface PaymentResumerService {

    /**
     * Resume processes that were interrupted during payment, eg during place order or adhoc payments.
     */
    void resumeProcessesForInterruptedPayments();
}

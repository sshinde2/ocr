/**
 * 
 */
package au.com.target.tgtpayment.resumer.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtpayment.dao.PaymentsInProgressDao;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.model.PaymentsInProgressModel;
import au.com.target.tgtpayment.resumer.PaymentProcessResumer;
import au.com.target.tgtpayment.resumer.PaymentProcessResumerFactory;
import au.com.target.tgtpayment.resumer.PaymentResumerService;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


public class PaymentResumerServiceImpl implements PaymentResumerService {

    private static final Logger LOG = Logger.getLogger(PaymentResumerServiceImpl.class);

    private PaymentProcessResumerFactory paymentProcessResumerFactory;

    private ModelService modelService;

    private PaymentsInProgressDao paymentsInProgressDao;

    // This time Threshold is calculated as minutes, 
    // and also set the default values in 30 minutes, you can always override this value through Config
    private int serverCrashedRetryTimeThreshold = 30;

    @Override
    public void resumeProcessesForInterruptedPayments() {

        final List<PaymentsInProgressModel> paymentsInProgressModels = paymentsInProgressDao
                .fetchPaymentsInProgressAfterServerCrash(DateTime
                        .now()
                        .minusMinutes(getCrashedRetryTimeThreshold()).toDate());

        if (CollectionUtils.isEmpty(paymentsInProgressModels)) {
            return;
        }

        for (final PaymentsInProgressModel paymentsInProgress : paymentsInProgressModels) {

            if (paymentsInProgress == null) {
                continue;
            }

            AbstractOrderModel abstractOrder = null;

            try {
                abstractOrder = paymentsInProgress.getAbstractOrder();

                // if the cart/order is deleted by hmc or other reasons
                // the DAO method, will return a list contains null object
                // so we need to filter out this scenario first
                if (abstractOrder == null) {
                    LOG.warn(SplunkLogFormatter.formatOrderMessage(
                            "paymentsInProgress missing associated cart or order",
                            TgtutilityConstants.ErrorCode.WARN_TGTCORE, null));
                    modelService.remove(paymentsInProgress);
                    continue;
                }

                // Check the payment status - if it is missing or REJECTED then we should not resume the process
                final PaymentTransactionEntryModel entry = paymentsInProgress.getPaymentTransactionEntry();
                if (entry == null) {
                    LOG.warn(SplunkLogFormatter.formatOrderMessage(
                            "paymentsInProgress missing associated payment entry",
                            TgtutilityConstants.ErrorCode.WARN_TGTCORE, abstractOrder.getCode()));
                    modelService.remove(paymentsInProgress);
                    continue;
                }
                if (entry.getTransactionStatus() == null) {
                    LOG.warn(SplunkLogFormatter.formatOrderMessage(
                            "paymentsInProgress has payment entry with null status",
                            TgtutilityConstants.ErrorCode.WARN_TGTCORE, abstractOrder.getCode()));
                    modelService.remove(paymentsInProgress);
                    continue;
                }
                if (entry.getTransactionStatus().equals(TransactionStatus.REJECTED.toString())) {
                    LOG.warn(SplunkLogFormatter.formatOrderMessage(
                            "paymentsInProgress has payment entry with REJECTED status",
                            TgtutilityConstants.ErrorCode.WARN_TGTCORE, abstractOrder.getCode()));
                    modelService.remove(paymentsInProgress);
                    continue;
                }


                // The abstract order could be a cart that was interrupted in placeOrder,
                // or an order with interrupted layby payment.
                final PaymentCaptureType type = paymentsInProgress.getPaymentCaptureType();

                // Type should not be null since it is mandatory
                if (type == null) {
                    LOG.warn(SplunkLogFormatter.formatOrderMessage("paymentsInProgress missing a type",
                            TgtutilityConstants.ErrorCode.WARN_TGTCORE, abstractOrder.getCode()));
                    continue;
                }

                // Delegate to the appropriate process resumer - if there is not one returned this is a programming error
                // and it needs to be implemented in the factory.
                final PaymentProcessResumer resumer = paymentProcessResumerFactory.getPaymentProcessResumer(type);
                Assert.notNull(resumer, "unrecognised PaymentCaptureType: " + type);
                resumer.resumeProcess(abstractOrder, entry);
            }
            catch (final Throwable t) {
                LOG.warn(SplunkLogFormatter.formatOrderMessage("paymentsInProgress processing threw unexpected error",
                        TgtutilityConstants.ErrorCode.WARN_TGTCORE,
                        abstractOrder == null ? null : abstractOrder.getCode()),
                        t);
            }
        }
    }



    /**
     * Here it supports both negative settings and positive settings
     * 
     * @return int
     */
    private int getCrashedRetryTimeThreshold() {
        if (this.serverCrashedRetryTimeThreshold < 0) {
            return serverCrashedRetryTimeThreshold * -1;
        }

        return serverCrashedRetryTimeThreshold;
    }


    /**
     * @param serverCrashedRetryTimeThreshold
     *            the serverCrashedRetryTimeThreshold to set
     */
    public void setServerCrashedRetryTimeThreshold(final int serverCrashedRetryTimeThreshold) {
        this.serverCrashedRetryTimeThreshold = serverCrashedRetryTimeThreshold;
    }


    /**
     * @param paymentProcessResumerFactory
     *            the paymentProcessResumerFactory to set
     */
    @Required
    public void setPaymentProcessResumerFactory(final PaymentProcessResumerFactory paymentProcessResumerFactory) {
        this.paymentProcessResumerFactory = paymentProcessResumerFactory;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }



    /**
     * @param paymentsInProgressDao
     *            the paymentsInProgressDao to set
     */
    public void setPaymentsInProgressDao(final PaymentsInProgressDao paymentsInProgressDao) {
        this.paymentsInProgressDao = paymentsInProgressDao;
    }



}

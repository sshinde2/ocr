/**
 * 
 */
package au.com.target.tgtpayment.service;

import au.com.target.tgtpayment.model.AfterpayConfigModel;


/**
 * @author mgazal
 *
 */
public interface AfterpayConfigService {

    /**
     * Fetch AfterpayConfig
     * 
     * @return {@link AfterpayConfigModel}
     */
    AfterpayConfigModel getAfterpayConfig();

    /**
     * create or update Afterpay configuration
     */
    void createOrUpdateAfterpayConfig();

    /**
     * ping after pay service to check availability.
     * 
     * @return - boolean
     */
    boolean ping();
}

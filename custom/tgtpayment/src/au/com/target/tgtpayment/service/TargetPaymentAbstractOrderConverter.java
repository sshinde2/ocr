/**
 * 
 */
package au.com.target.tgtpayment.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.math.BigDecimal;

import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpayment.enums.PaymentCaptureType;


public interface TargetPaymentAbstractOrderConverter {

    /**
     * convert a AbstractOrderModel to Order pojo
     * 
     * @param orderModel
     * @param amount
     * @return Order
     */
    Order convertAbstractOrderModelToOrder(final AbstractOrderModel orderModel, final BigDecimal amount,
            final PaymentCaptureType paymentCaptureType);
}

/**
 * 
 */
package au.com.target.tgtpayment.service.impl;

import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtpayment.commands.request.TargetGetPaymentConfigurationRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentPingRequest;
import au.com.target.tgtpayment.commands.result.TargetGetPaymentConfigurationResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentPingResult;
import au.com.target.tgtpayment.constants.TgtpaymentConstants;
import au.com.target.tgtpayment.dao.AfterpayConfigDao;
import au.com.target.tgtpayment.methods.TargetPaymentMethod;
import au.com.target.tgtpayment.model.AfterpayConfigModel;
import au.com.target.tgtpayment.service.AfterpayConfigService;
import au.com.target.tgtpayment.strategy.PaymentMethodStrategy;


/**
 * @author mgazal
 *
 */
public class AfterpayConfigServiceImpl extends AbstractBusinessService implements AfterpayConfigService {

    private static final Logger LOG = Logger.getLogger(AfterpayConfigServiceImpl.class);

    private AfterpayConfigDao afterpayConfigDao;

    private PaymentMethodStrategy paymentMethodStrategy;

    @Override
    public AfterpayConfigModel getAfterpayConfig() {
        return afterpayConfigDao.getAfterpayConfig();
    }

    @Override
    public void createOrUpdateAfterpayConfig() {
        final TargetPaymentMethod paymentMethod = paymentMethodStrategy
                .getPaymentMethod(TgtpaymentConstants.AFTERPAY_PAYMENT_TYPE);
        Assert.notNull(paymentMethod, "paymentMethod cannot be null!");
        try {
            final TargetGetPaymentConfigurationResult configuration = paymentMethod
                    .getConfiguration(new TargetGetPaymentConfigurationRequest());
            if (configuration != null) {
                AfterpayConfigModel model = this.getAfterpayConfig();
                if (model == null) {
                    model = getModelService().create(AfterpayConfigModel.class);
                }
                model.setPaymentType(configuration.getType());
                model.setDescription(configuration.getDescription());
                model.setMaximumAmount(configuration.getMaximumAmount());
                model.setMinimumAmount(configuration.getMinimumAmount());
                getModelService().save(model);
                getModelService().refresh(model);
            }
        }
        catch (final AdapterException ae) {
            LOG.error("Afterpay create or update configuration error", ae);
            throw ae;
        }
        catch (final ModelSavingException mse) {
            LOG.error("Afterpay create or update configuration error", mse);
            throw mse;
        }
    }

    /**
     * @param afterpayConfigDao
     *            the afterpayConfigDao to set
     */
    @Required
    public void setAfterpayConfigDao(final AfterpayConfigDao afterpayConfigDao) {
        this.afterpayConfigDao = afterpayConfigDao;
    }


    /**
     * @param paymentMethodStrategy
     *            the paymentMethodStrategy to set
     */
    @Required
    public void setPaymentMethodStrategy(final PaymentMethodStrategy paymentMethodStrategy) {
        this.paymentMethodStrategy = paymentMethodStrategy;
    }

    @Override
    public boolean ping() {
        final TargetPaymentMethod paymentMethod = paymentMethodStrategy
                .getPaymentMethod(TgtpaymentConstants.AFTERPAY_PAYMENT_TYPE);
        Assert.notNull(paymentMethod, "paymentMethod cannot be null!");

        final TargetPaymentPingResult result = paymentMethod
                .ping(new TargetPaymentPingRequest());
        if (result != null) {
            return result.isSuccess();
        }
        else {
            return false;
        }
    }

}

package au.com.target.tgtpayment.service.impl;

//CHECKSTYLE:OFF
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.model.PaymentsInProgressModel;
import au.com.target.tgtpayment.service.PaymentsInProgressService;


//CHECKSTYLE:ON
/**
 * Default implementation of {@link PaymentsInProgressService}. Uses {@link ModelService} and
 * {@link FlexibleSearchService} for internal operations
 * 
 */
public class PaymentsInProgressServiceImpl extends AbstractBusinessService implements PaymentsInProgressService {

    private FlexibleSearchService flexibleSearchService;

    @Override
    public PaymentsInProgressModel createInProgressPayment(final AbstractOrderModel abstractOrderModel,
            final PaymentTransactionEntryModel paymentTransactionEntryModel,
            final PaymentCaptureType captureType) {
        Assert.notNull(abstractOrderModel, "abstractOrderModel cannot be null");

        final PaymentsInProgressModel paymentsInProgress = getModelService().create(PaymentsInProgressModel.class);
        paymentsInProgress.setAbstractOrder(abstractOrderModel);
        paymentsInProgress.setPaymentInitiatedTime(new Date());
        paymentsInProgress.setPaymentCaptureType(captureType);
        paymentsInProgress.setPaymentTransactionEntry(paymentTransactionEntryModel);
        getModelService().save(paymentsInProgress);
        return paymentsInProgress;
    }

    @Override
    public boolean hasInProgressPayment(final AbstractOrderModel abstractOrderModel) {
        Assert.notNull(abstractOrderModel, "abstractOrderModel cannot be null");

        final List<PaymentsInProgressModel> paymentsInProgressModel = findPaymentsInProgressForAbstractOrder(abstractOrderModel);
        return CollectionUtils.isNotEmpty(paymentsInProgressModel);
    }

    @Override
    public boolean removeInProgressPayment(final AbstractOrderModel abstractOrderModel) {
        Assert.notNull(abstractOrderModel, "abstractOrderModel cannot be null");

        final List<PaymentsInProgressModel> paymentsInProgressModels = findPaymentsInProgressForAbstractOrder(abstractOrderModel);
        if (CollectionUtils.isEmpty(paymentsInProgressModels)) {
            return false;
        }

        for (final PaymentsInProgressModel paymentsInProgressModel : paymentsInProgressModels) {
            getModelService().remove(paymentsInProgressModel);
        }

        return true;
    }

    private List<PaymentsInProgressModel> findPaymentsInProgressForAbstractOrder(
            final AbstractOrderModel abstractOrderModel) {
        final PaymentsInProgressModel example = new PaymentsInProgressModel();
        example.setAbstractOrder(abstractOrderModel);
        try {
            return flexibleSearchService.getModelsByExample(example);
        }
        catch (final ModelNotFoundException mnfe) {
            return null;
        }
    }

    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

}

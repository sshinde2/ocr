/**
 * 
 */
package au.com.target.tgtpayment.service.impl;

import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.jalo.ProductPromotion;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.voucher.VoucherService;
import de.hybris.platform.voucher.model.VoucherModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Currency;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtpayment.dto.Address;
import au.com.target.tgtpayment.dto.AmountType;
import au.com.target.tgtpayment.dto.Customer;
import au.com.target.tgtpayment.dto.LineItem;
import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.service.TargetPaymentAbstractOrderConverter;
import au.com.target.tgtpayment.strategy.FindVoucherValueStrategy;
import au.com.target.tgtpayment.strategy.TargetVoucherConverterStategy;
import au.com.target.tgtpayment.util.TargetPaymentOrderHelper;


public class TargetPaymentAbstractOrderConverterImpl implements TargetPaymentAbstractOrderConverter {

    static final String DEAL_LINE_NAME = "Deal";

    private PromotionsService promotionsService;
    private VoucherService voucherService;
    private FindVoucherValueStrategy findVoucherValueStrategy;
    private TargetVoucherConverterStategy targetVoucherConverterStategy;
    private TargetPaymentOrderHelper targetPaymentOrderHelper;

    @Override
    public Order convertAbstractOrderModelToOrder(final AbstractOrderModel orderModel, final BigDecimal amount,
            final PaymentCaptureType paymentCaptureType) {
        Assert.notNull(orderModel, "order cannot be null");


        final List<AbstractOrderEntryModel> entries = orderModel.getEntries();
        if (CollectionUtils.isEmpty(entries)) {
            return null;
        }

        final List<LineItem> lineItems = new ArrayList<>();
        final Currency currency = Currency.getInstance(orderModel.getCurrency().getIsocode());

        //init the line item subtotal
        BigDecimal lineItemSubTotal = BigDecimal.ZERO;
        int i = 1;
        for (final AbstractOrderEntryModel entry : entries) {
            final BigDecimal basePrice = BigDecimal.valueOf(entry.getBasePrice().doubleValue());
            final long quantity = entry.getQuantity().longValue();
            final AmountType amountType = new AmountType(basePrice, currency);

            final LineItem item = new LineItem();
            if (entry.getProduct() != null) {
                final ProductModel productModel = entry.getProduct();
                item.setName(productModel.getName());
                item.setProductCode(productModel.getCode());
                // Added image uri for zip pay
                final String thumbnailUrl = targetPaymentOrderHelper.getProductThumbnailUrl(productModel);
                if (StringUtils.isNotEmpty(thumbnailUrl)) {
                    item.setImageUri(thumbnailUrl);
                }
            }

            //set the qty, price and line item number
            item.setQuantity(quantity);
            item.setPrice(amountType);
            item.setNumber(i);
            i++;
            lineItems.add(item);
            lineItemSubTotal = lineItemSubTotal.add(basePrice.multiply(BigDecimal.valueOf(quantity)));
        }

        final Order order = new Order();
        final AmountType shippingTotalAmount = new AmountType(
                BigDecimal.valueOf(orderModel.getDeliveryCost().doubleValue()),
                currency);
        if (null != orderModel.getPaymentAddress()) {
            order.setBillingAddress(createAddress(orderModel.getPaymentAddress()));
        }
        else {
            order.setBillingAddress(createAddress(orderModel.getDeliveryAddress()));
        }
        order.setShippingAddress(createAddress(orderModel.getDeliveryAddress()));
        order.setShippingTotalAmount(shippingTotalAmount);
        order.setContainPreOrderItems(orderModel.getContainsPreOrderItems());

        final AmountType orderTotalAmount = new AmountType(BigDecimal.valueOf(orderModel.getTotalPrice().doubleValue()),
                currency);
        order.setOrderTotalAmount(orderTotalAmount);

        //get the discounts for the order and add item to line item list and add the discount value to subtotal
        final List<LineItem> discounts = getDiscountsAndCreateLineItems(orderModel, currency);
        if (CollectionUtils.isNotEmpty(discounts)) {
            for (final LineItem item : discounts) {
                lineItems.add(item);
                lineItemSubTotal = lineItemSubTotal.add(item.getPrice().getAmount());
            }
        }

        order.setLineItems(lineItems);
        final AmountType itemSubTotalAmount = new AmountType(lineItemSubTotal,
                currency);
        order.setItemSubTotalAmount(itemSubTotalAmount);
        order.setOrderId(orderModel.getCode());
        final UserModel user = orderModel.getUser();
        if (user != null && (user instanceof TargetCustomerModel)) {
            final TargetCustomerModel customerModel = (TargetCustomerModel)user;
            order.setUserId(customerModel.getCustomerID());
            final Customer customer = new Customer();
            if (CustomerType.GUEST.equals(customerModel.getType())) {
                final AddressModel address = orderModel.getDeliveryAddress() != null ? orderModel.getDeliveryAddress()
                        : orderModel.getPaymentAddress();
                if (address != null) {
                    customer.setGivenNames(address.getFirstname());
                    customer.setSurname(address.getLastname());
                }
            }
            else {
                customer.setGivenNames(customerModel.getFirstname());
                customer.setSurname(customerModel.getLastname());
            }
            customer.setEmail(customerModel.getContactEmail());
            order.setCustomer(customer);
        }

        // Added for Zippay - Populating pickup for click n collect
        if (orderModel.getDeliveryMode() != null) {
            final DeliveryModeModel deliveryModeModel = orderModel.getDeliveryMode();
            if (deliveryModeModel instanceof TargetZoneDeliveryModeModel) {
                final Boolean isDeliveryToStore = ((TargetZoneDeliveryModeModel)deliveryModeModel)
                        .getIsDeliveryToStore();
                order.setPickup(isDeliveryToStore);
            }
        }

        return order;
    }

    /**
     * get order and product promos applied to the cart/order and create line items
     * 
     * @param orderModel
     * @param currency
     * @return List<LineItem>
     */
    protected List<LineItem> getDiscountsAndCreateLineItems(final AbstractOrderModel orderModel,
            final Currency currency) {
        // get the order promos and create line items for each one
        final PromotionOrderResults promoResult = promotionsService.getPromotionResults(orderModel);
        final List<PromotionResult> orderPromoResults = promoResult.getAllResults();
        final List<LineItem> discountLineItems = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(orderPromoResults)) {
            for (final PromotionResult result : orderPromoResults) {
                if (result.isApplied() && !(result.getPromotion() instanceof ProductPromotion)) {
                    discountLineItems.add(createLineItemForDiscount(result, currency));
                }
            }
        }

        // get the product promos and create line item for each one, do not create duplicate discount
        // line items if one already exists
        final List<PromotionResult> productPromoResults = getTargetProductPromotions(promoResult);
        if (CollectionUtils.isNotEmpty(productPromoResults)) {
            for (final PromotionResult result : productPromoResults) {
                if (CollectionUtils.isEmpty(discountLineItems)) {
                    discountLineItems.add(createLineItemForDiscount(result, currency));
                }
                else {
                    final LineItem existingItem = findExisitingDiscountItem(discountLineItems, result.getPromotion()
                            .getCode());
                    if (null == existingItem) {
                        discountLineItems.add(createLineItemForDiscount(result, currency));
                    }
                    else {
                        final BigDecimal discountedPrice = BigDecimal.valueOf(result.getTotalDiscount());
                        existingItem.setPrice(new AmountType(
                                existingItem.getPrice().getAmount().subtract(discountedPrice), currency));
                    }
                }
            }
        }

        addVoucherLineItems(discountLineItems, orderModel, currency);

        return discountLineItems;
    }

    /**
     * Gets the target product promotions.
     * 
     * @param promoResult
     *            the promo result
     * @return the target product promotions
     */
    private List<PromotionResult> getTargetProductPromotions(final PromotionOrderResults promoResult) {
        final List<PromotionResult> productPromoResults = new ArrayList<>();
        for (final PromotionResult result : promoResult.getAllResults()) {
            if (result.isApplied()
                    && (result.getPromotion() instanceof ProductPromotion)) {
                productPromoResults.add(result);
            }
        }
        return productPromoResults;
    }

    /**
     * check to see if a discount line item has already been created
     * 
     * @param discountLineItems
     * @param discountCode
     * @return LineItem
     */
    protected LineItem findExisitingDiscountItem(final List<LineItem> discountLineItems, final String discountCode) {
        for (final LineItem item : discountLineItems) {
            if (item.getProductCode().equals(discountCode)) {
                return item;
            }
        }
        return null;
    }

    /**
     * create Address from AddressModel
     * 
     * @param addressModel
     * @return Address
     */
    protected Address createAddress(final AddressModel addressModel) {
        final Address address = new Address();

        if (null != addressModel) {
            address.setStreet1(addressModel.getLine1());
            address.setStreet2(addressModel.getLine2());
            address.setState(addressModel.getDistrict());
            address.setCity(addressModel.getTown());
            address.setPostalCode(addressModel.getPostalcode());
            address.setName(addressModel.getFirstname() + " " + addressModel.getLastname());
            address.setCountryName(addressModel.getCountry().getName());
            address.setCountryCode(addressModel.getCountry().getIsocode());
        }
        return address;
    }

    private LineItem createLineItemForDiscount(final PromotionResult result, final Currency currency) {
        final double discount = result.getTotalDiscount();
        final LineItem lineItem = new LineItem();
        lineItem.setProductCode(result.getPromotion().getCode());
        lineItem.setDescription(getDealDescription(result));
        lineItem.setName(getDealName(result));
        lineItem.setQuantity(1);
        lineItem.setDiscount(true);
        lineItem.setPrice(new AmountType(BigDecimal.valueOf(-discount), currency));
        return lineItem;
    }


    private String getDealName(final PromotionResult result) {

        // TMD has a description in the promotion title field.
        // Other deals don't, so we'll hardcode 'Deal' in there if it is empty.
        String name = result.getPromotion().getTitle();
        if (StringUtils.isEmpty(name)) {
            name = DEAL_LINE_NAME;
        }
        return name;
    }

    private String getDealDescription(final PromotionResult result) {
        return result.getPromotion().getDescription();
    }


    protected void addVoucherLineItems(final List<LineItem> lineItems, final AbstractOrderModel orderModel,
            final Currency currency) {

        final Collection<String> discountCodes;

        if (orderModel instanceof OrderModel) {
            discountCodes = voucherService.getAppliedVoucherCodes((OrderModel)orderModel);
        }
        else if (orderModel instanceof CartModel) {
            discountCodes = voucherService.getAppliedVoucherCodes((CartModel)orderModel);
        }
        else {
            return;
        }

        if (CollectionUtils.isNotEmpty(discountCodes)) {
            for (final String discountCode : discountCodes) {

                final VoucherModel voucher = voucherService.getVoucher(discountCode);
                if (voucher != null) {
                    final Double discountValue = findVoucherValueStrategy.getDiscountValueAppliedValue(voucher,
                            orderModel);
                    if (discountValue != null) {
                        lineItems.add(createLineItemForVoucher(discountCode, discountValue, voucher, currency));
                    }
                }
            }
        }
    }

    private LineItem createLineItemForVoucher(final String voucherCode,
            final Double discountValue,
            final VoucherModel voucherModel,
            final Currency currency) {

        final LineItem lineItem = new LineItem();
        lineItem.setProductCode(voucherCode);
        lineItem.setName(targetVoucherConverterStategy.getVoucherName(voucherModel, voucherCode));
        lineItem.setDescription(targetVoucherConverterStategy.getVoucherDescription(voucherModel, voucherCode));
        lineItem.setQuantity(1);
        lineItem.setDiscount(true);
        lineItem.setPrice(new AmountType(BigDecimal.valueOf(-discountValue.doubleValue()), currency));
        return lineItem;
    }


    /**
     * @param promotionsService
     *            the promotionsService to set
     */
    @Required
    public void setPromotionsService(final PromotionsService promotionsService) {
        this.promotionsService = promotionsService;
    }

    /**
     * @param voucherService
     *            the voucherService to set
     */
    @Required
    public void setVoucherService(final VoucherService voucherService) {
        this.voucherService = voucherService;
    }

    /**
     * @param findVoucherValueStrategy
     *            the findVoucherValueStrategy to set
     */
    @Required
    public void setFindVoucherValueStrategy(final FindVoucherValueStrategy findVoucherValueStrategy) {
        this.findVoucherValueStrategy = findVoucherValueStrategy;
    }

    /**
     * @param targetVoucherConverterStategy
     *            the targetVoucherConverterStategy to set
     */
    @Required
    public void setTargetVoucherConverterStategy(final TargetVoucherConverterStategy targetVoucherConverterStategy) {
        this.targetVoucherConverterStategy = targetVoucherConverterStategy;
    }

    /**
     * @param targetPaymentOrderHelper
     *            the targetPaymentOrderHelper to set
     */
    @Required
    public void setTargetPaymentOrderHelper(final TargetPaymentOrderHelper targetPaymentOrderHelper) {
        this.targetPaymentOrderHelper = targetPaymentOrderHelper;
    }
}

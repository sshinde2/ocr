/**
 * 
 */
package au.com.target.tgtpayment.strategy;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.voucher.model.VoucherModel;


/**
 * Strategy to find the value of a voucher discount on an order.
 * 
 */
public interface FindVoucherValueStrategy {

    /**
     * Get the DiscountValue item for the given voucher and order
     * 
     * @param voucher
     * @param order
     * @return discountValue
     */
    DiscountValue getDiscountValue(VoucherModel voucher, AbstractOrderModel order);

    /**
     * Get the discount value applied value for the given voucher and order
     * 
     * @param voucher
     * @param order
     * @return double value
     */
    Double getDiscountValueAppliedValue(VoucherModel voucher, AbstractOrderModel order);

}

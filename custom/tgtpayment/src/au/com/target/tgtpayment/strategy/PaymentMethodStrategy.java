package au.com.target.tgtpayment.strategy;

import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;

import au.com.target.tgtpayment.methods.TargetPaymentMethod;


/**
 * PaymentStrategy to find the write Payment method service
 * 
 */
public interface PaymentMethodStrategy {

    /**
     * @param paymentInfoModel
     * @param paymentProvider
     * @return {@link TargetPaymentMethod}
     */
    TargetPaymentMethod getPaymentMethod(PaymentInfoModel paymentInfoModel, String paymentProvider);

    /**
     * @param paymentInfoModel
     * @return {@link TargetPaymentMethod}
     */
    TargetPaymentMethod getPaymentMethod(PaymentInfoModel paymentInfoModel);

    /**
     * Choose the right TargetPaymentMethod implementation using PaymentMode
     * 
     * @param paymentModeModel
     * @return {@link TargetPaymentMethod}
     */
    TargetPaymentMethod getPaymentMethod(PaymentModeModel paymentModeModel);

    /**
     * Choose the right TargetPaymentMethod implementation using keyword
     * 
     * @param provider
     * @return {@link TargetPaymentMethod}
     */
    TargetPaymentMethod getPaymentMethod(String provider);
}

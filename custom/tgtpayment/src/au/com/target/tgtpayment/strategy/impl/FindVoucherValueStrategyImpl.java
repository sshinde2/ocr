/**
 * 
 */
package au.com.target.tgtpayment.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.voucher.model.VoucherModel;

import java.util.Collection;

import au.com.target.tgtpayment.strategy.FindVoucherValueStrategy;



/**
 * Strategy to find the value of a discount on an order, simply using VoucherModelService.
 * 
 * Note an alternative method using jalo was being used in TargetCheckoutFacadeImpl.getFirstVoucherDiscount - they could
 * use this strategy instead.
 * 
 */
public class FindVoucherValueStrategyImpl implements FindVoucherValueStrategy {


    @Override
    public DiscountValue getDiscountValue(final VoucherModel voucher, final AbstractOrderModel order)
    {
        final Collection<DiscountValue> globalDiscountValues = order.getGlobalDiscountValues();
        for (final DiscountValue discountValue : globalDiscountValues) {
            if (voucher.getCode().equals(discountValue.getCode())) {
                return discountValue;
            }
        }

        return null;
    }

    @Override
    public Double getDiscountValueAppliedValue(final VoucherModel voucher, final AbstractOrderModel order) {
        final DiscountValue discountValue = getDiscountValue(voucher, order);
        if (discountValue == null) {
            return null;
        }

        return Double.valueOf(discountValue.getAppliedValue());
    }


}

/**
 * 
 */
package au.com.target.tgtpayment.commands.result;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCardPaymentResultTest {
    private static final String TRANSACTION_TYPE_IMMEDIATE = "1";
    private static final String RESPONSE_CODE_SUCCESS = "0";

    private final TargetCardPaymentResult ipgTransactionResult = new TargetCardPaymentResult();

    @SuppressWarnings("deprecation")
    @Test
    public void testGetTransactionDateWithValidDate() {
        final String dateTime = "2015-10-10 10:10:10";
        final TargetCardPaymentResult spy = spy(ipgTransactionResult);
        doReturn(dateTime).when(spy).getTransactionDateTime();

        final Date date = spy.getTransactionDate();
        assertNotNull(date);
        assertEquals(2015, 1900 + date.getYear());
        assertEquals(9, date.getMonth());
        assertEquals(10, date.getDate());
        assertEquals(10, date.getHours());
        assertEquals(10, date.getMinutes());
        assertEquals(10, date.getSeconds());
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testGetTransactionDateWithInvalidDate() {
        final Date today = new Date();
        final String dateTime = "2015-10-10";
        final TargetCardPaymentResult spy = spy(ipgTransactionResult);
        doReturn(dateTime).when(spy).getTransactionDateTime();

        final Date date = spy.getTransactionDate();
        assertNotNull(date);
        assertEquals(today.getYear(), date.getYear());
        assertEquals(today.getMonth(), date.getMonth());
        assertEquals(today.getDate(), date.getDate());
        assertEquals(today.getHours(), date.getHours());
        assertEquals(today.getMinutes(), date.getMinutes());
    }

    @Test
    public void testImmediatePayment() {
        final TargetCardPaymentResult spy = spy(ipgTransactionResult);
        doReturn(TRANSACTION_TYPE_IMMEDIATE).when(spy).getTransactionType();

        final boolean isImmediatePayment = spy.isImmediatePayment();
        assertTrue(isImmediatePayment);
    }

    @Test
    public void testNotImmediatePayment() {
        final TargetCardPaymentResult spy = spy(ipgTransactionResult);
        doReturn("").when(spy).getTransactionType();

        final boolean isImmediatePayment = spy.isImmediatePayment();
        assertFalse(isImmediatePayment);
    }

    @Test
    public void testSuccessfulPayment() {
        final TargetCardPaymentResult spy = spy(ipgTransactionResult);
        doReturn(RESPONSE_CODE_SUCCESS).when(spy).getResponseCode();

        final boolean isPaymentSuccessful = spy.isPaymentSuccessful();
        assertTrue(isPaymentSuccessful);
    }

    @Test
    public void testUnsuccessfulPayment() {
        final TargetCardPaymentResult spy = spy(ipgTransactionResult);
        doReturn("").when(spy).getResponseCode();

        final boolean isPaymentSuccessful = spy.isPaymentSuccessful();
        assertFalse(isPaymentSuccessful);
    }
}
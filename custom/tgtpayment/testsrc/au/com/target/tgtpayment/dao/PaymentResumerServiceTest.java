/**
 * 
 */
package au.com.target.tgtpayment.dao;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Arrays;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.model.PaymentsInProgressModel;
import au.com.target.tgtpayment.resumer.PaymentProcessResumer;
import au.com.target.tgtpayment.resumer.PaymentProcessResumerFactory;
import au.com.target.tgtpayment.resumer.impl.PaymentResumerServiceImpl;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PaymentResumerServiceTest {

    @Mock
    private ModelService modelService;

    @Mock
    private PaymentsInProgressDao paymentsInProgressDao;

    @Mock
    private PaymentProcessResumerFactory paymentProcessResumerFactory;

    @Mock
    private PaymentProcessResumer paymentProcessResumer;

    @Mock
    private PaymentsInProgressModel paymentsInProgress;

    @Mock
    private CartModel cart;

    @Mock
    private PaymentTransactionEntryModel paymentTransactionEntryModel;


    @InjectMocks
    @Spy
    private final PaymentResumerServiceImpl paymentResumerService = new PaymentResumerServiceImpl();

    @Before
    public void setup() {

        // Set up PaymentsInProgressModel with place order type and cart and ptem
        BDDMockito.given(paymentsInProgress.getAbstractOrder()).willReturn(cart);
        BDDMockito.given(paymentsInProgress.getPaymentCaptureType()).willReturn(PaymentCaptureType.PLACEORDER);
        BDDMockito.given(paymentsInProgress.getPaymentTransactionEntry()).willReturn(paymentTransactionEntryModel);
        BDDMockito.given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(
                TransactionStatus.ACCEPTED.toString());

        // Factory will return PaymentProcessResumer
        BDDMockito.given(paymentProcessResumerFactory.getPaymentProcessResumer(PaymentCaptureType.PLACEORDER))
                .willReturn(paymentProcessResumer);

        // There is a single PaymentsInProgressModel saved
        BDDMockito.given(paymentsInProgressDao.fetchPaymentsInProgressAfterServerCrash(Mockito.any(Date.class)))
                .willReturn(Arrays.asList(paymentsInProgress));

    }


    @Test
    public void testResumeProcessesForInterruptedPaymentsNullCart() throws InvalidCartException {

        BDDMockito.given(paymentsInProgress.getAbstractOrder()).willReturn(null);

        paymentResumerService.resumeProcessesForInterruptedPayments();

        Mockito.verifyZeroInteractions(paymentProcessResumer);
    }

    @Test
    public void testResumeProcessesForInterruptedPaymentsNullType() throws InvalidCartException {

        BDDMockito.given(paymentsInProgress.getPaymentCaptureType()).willReturn(null);

        paymentResumerService.resumeProcessesForInterruptedPayments();

        Mockito.verifyZeroInteractions(paymentProcessResumer);
    }

    @Test
    public void testResumeProcessesForInterruptedPaymentsNullEntry() throws InvalidCartException {

        BDDMockito.given(paymentsInProgress.getPaymentTransactionEntry()).willReturn(null);

        paymentResumerService.resumeProcessesForInterruptedPayments();

        Mockito.verifyZeroInteractions(paymentProcessResumer);
    }

    @Test
    public void testResumeProcessesForInterruptedPaymentsRejectedEntry() throws InvalidCartException {

        BDDMockito.given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(
                TransactionStatus.REJECTED.toString());

        paymentResumerService.resumeProcessesForInterruptedPayments();

        Mockito.verifyZeroInteractions(paymentProcessResumer);
    }

    @Test
    public void testResumeProcessesForInterruptedPayments() throws InvalidCartException {

        paymentResumerService.resumeProcessesForInterruptedPayments();

        Mockito.verify(paymentProcessResumer).resumeProcess(cart, paymentTransactionEntryModel);
    }



}

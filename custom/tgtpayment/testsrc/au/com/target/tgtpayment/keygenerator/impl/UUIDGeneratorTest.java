package au.com.target.tgtpayment.keygenerator.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import junit.framework.Assert;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


/**
 * Unit test for {@link UUIDGenerator}
 * 
 */
@UnitTest
public class UUIDGeneratorTest {

    private final UUIDGenerator uuidGenerator = new UUIDGenerator();

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Test
    public void testGenerate() {
        final Object result = uuidGenerator.generate();
        Assert.assertNotNull(result);
    }

    @Test
    public void testGenerateFor() {
        expectedException.expect(UnsupportedOperationException.class);
        uuidGenerator.generateFor(null);
    }

    @Test
    public void testReset() {
        expectedException.expect(UnsupportedOperationException.class);
        uuidGenerator.reset();
    }

}

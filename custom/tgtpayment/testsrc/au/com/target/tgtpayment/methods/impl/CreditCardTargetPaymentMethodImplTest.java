/**
 * 
 */
package au.com.target.tgtpayment.methods.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.factory.CommandFactory;
import de.hybris.platform.payment.commands.factory.CommandNotSupportedException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtpayment.commands.TargetCaptureCommand;
import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.commands.TargetFollowOnRefundCommand;
import au.com.target.tgtpayment.commands.TargetRetrieveTransactionCommand;
import au.com.target.tgtpayment.commands.TargetStandaloneRefundCommand;
import au.com.target.tgtpayment.commands.request.AbstractTargetStandaloneRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;


@UnitTest
public class CreditCardTargetPaymentMethodImplTest {
    private TargetCreditCardPaymentMethodImpl tnsTargetPaymentMethod;

    @Mock
    private CommandFactory commandFactory;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        tnsTargetPaymentMethod = new TargetCreditCardPaymentMethodImpl();
        tnsTargetPaymentMethod.setCommandFactory(commandFactory);
    }

    @After
    public void tearDown() {
        //
    }

    @Test(expected = AdapterException.class)
    public void testCapture() throws CommandNotSupportedException {
        final TargetCaptureRequest captureRequest = Mockito.mock(TargetCaptureRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetCaptureCommand.class)).willThrow(
                new CommandNotSupportedException());
        tnsTargetPaymentMethod.capture(captureRequest);
    }

    @Test(expected = AdapterException.class)
    public void testCreateSubscription() throws CommandNotSupportedException {
        final TargetCreateSubscriptionRequest request = Mockito.mock(TargetCreateSubscriptionRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetCreateSubscriptionCommand.class)).willThrow(
                new CommandNotSupportedException());
        tnsTargetPaymentMethod.createSubscription(request);
    }

    @Test(expected = AdapterException.class)
    public void testFollowOnRefund() throws CommandNotSupportedException {
        final TargetFollowOnRefundRequest request = Mockito.mock(TargetFollowOnRefundRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetFollowOnRefundCommand.class)).willThrow(
                new CommandNotSupportedException());
        tnsTargetPaymentMethod.followOnRefund(request);
    }

    @Test(expected = AdapterException.class)
    public void testRetrieveTransaction() throws CommandNotSupportedException {
        final TargetRetrieveTransactionRequest request = Mockito.mock(TargetRetrieveTransactionRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetRetrieveTransactionCommand.class)).willThrow(
                new CommandNotSupportedException());
        tnsTargetPaymentMethod.retrieveTransaction(request);
    }

    @Test(expected = AdapterException.class)
    public void testStandaloneRefund() throws CommandNotSupportedException {
        final AbstractTargetStandaloneRefundRequest request = Mockito.mock(AbstractTargetStandaloneRefundRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetStandaloneRefundCommand.class)).willThrow(
                new CommandNotSupportedException());
        tnsTargetPaymentMethod.standaloneRefund(request);
    }
}

/**
 * 
 */
package au.com.target.tgtpayment.methods.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.GetSubscriptionDataCommand;
import de.hybris.platform.payment.commands.factory.CommandFactory;
import de.hybris.platform.payment.commands.factory.CommandNotSupportedException;
import de.hybris.platform.payment.commands.request.SubscriptionDataRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.commands.TargetFollowOnRefundCommand;
import au.com.target.tgtpayment.commands.TargetGetPaymentConfigurationCommand;
import au.com.target.tgtpayment.commands.TargetPaymentPingCommand;
import au.com.target.tgtpayment.commands.TargetRetrieveTransactionCommand;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetGetPaymentConfigurationRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentPingRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetAfterpayPaymentMethodImplTest {

    @InjectMocks
    private final TargetAfterpayPaymentMethodImpl paymentMethodImpl = new TargetAfterpayPaymentMethodImpl();

    @Mock
    private TargetGetPaymentConfigurationRequest request;

    @Mock
    private TargetPaymentPingRequest pingRequest;

    @Mock
    private TargetCreateSubscriptionRequest createSubscriptionRequest;

    @Mock
    private SubscriptionDataRequest subscriptionRequest;

    @Mock
    private TargetRetrieveTransactionRequest retrieveTransactionRequest;

    @Mock
    private TargetFollowOnRefundRequest followOnRefundRequest;

    @Mock
    private CommandFactory commandFactory;

    @Test(expected = AdapterException.class)
    public void testGetPaymentConfigurationCommandWithCommandNotSupported() throws CommandNotSupportedException {
        given(commandFactory.createCommand(TargetGetPaymentConfigurationCommand.class)).willThrow(
                new CommandNotSupportedException());
        paymentMethodImpl.getConfiguration(request);
    }

    @Test
    public void testGetPaymentConfigurationCommand() throws CommandNotSupportedException {
        final TargetGetPaymentConfigurationCommand command = mock(TargetGetPaymentConfigurationCommand.class);
        given(commandFactory.createCommand(TargetGetPaymentConfigurationCommand.class)).willReturn(command);
        paymentMethodImpl.getConfiguration(request);
        verify(command).perform(request);
    }

    @Test
    public void testPing() throws CommandNotSupportedException {
        final TargetPaymentPingCommand command = mock(TargetPaymentPingCommand.class);
        given(commandFactory.createCommand(TargetPaymentPingCommand.class)).willReturn(command);
        paymentMethodImpl.ping(pingRequest);
        verify(command).perform(pingRequest);
    }

    @Test(expected = AdapterException.class)
    public void testPingWithCommandNotSupported() throws CommandNotSupportedException {
        given(commandFactory.createCommand(TargetPaymentPingCommand.class)).willThrow(
                new CommandNotSupportedException());
        paymentMethodImpl.ping(pingRequest);
    }

    @Test(expected = AdapterException.class)
    public void testCreateSubscriptionCommandWithCommandNotSupported() throws CommandNotSupportedException {
        given(commandFactory.createCommand(TargetCreateSubscriptionCommand.class)).willThrow(
                new CommandNotSupportedException());
        paymentMethodImpl.createSubscription(createSubscriptionRequest);
    }

    @Test
    public void testCreateSubscriptionCommand() throws CommandNotSupportedException {
        final TargetCreateSubscriptionCommand command = mock(TargetCreateSubscriptionCommand.class);
        given(commandFactory.createCommand(TargetCreateSubscriptionCommand.class)).willReturn(command);
        paymentMethodImpl.createSubscription(createSubscriptionRequest);
        verify(command).perform(createSubscriptionRequest);
    }

    @Test
    public void testGetSubscription() throws CommandNotSupportedException {
        final GetSubscriptionDataCommand command = mock(GetSubscriptionDataCommand.class);
        given(commandFactory.createCommand(GetSubscriptionDataCommand.class)).willReturn(command);
        paymentMethodImpl.getSubscription(subscriptionRequest);
        verify(command).perform(subscriptionRequest);
    }

    @Test(expected = AdapterException.class)
    public void testGetSubscriptionWithException() throws CommandNotSupportedException {
        given(commandFactory.createCommand(GetSubscriptionDataCommand.class)).willThrow(
                new CommandNotSupportedException());
        paymentMethodImpl.getSubscription(subscriptionRequest);
    }

    @Test
    public void testRetrieveTransaction() throws CommandNotSupportedException {
        final TargetRetrieveTransactionCommand command = mock(TargetRetrieveTransactionCommand.class);
        given(commandFactory.createCommand(TargetRetrieveTransactionCommand.class)).willReturn(command);
        paymentMethodImpl.retrieveTransaction(retrieveTransactionRequest);
        verify(command).perform(retrieveTransactionRequest);
    }

    @Test(expected = AdapterException.class)
    public void testRetrieveTransactionWithException() throws CommandNotSupportedException {
        given(commandFactory.createCommand(TargetRetrieveTransactionCommand.class))
                .willThrow(new CommandNotSupportedException());
        paymentMethodImpl.retrieveTransaction(retrieveTransactionRequest);
    }

    @Test
    public void testFollowOnRefund() throws CommandNotSupportedException {
        final TargetFollowOnRefundCommand command = mock(TargetFollowOnRefundCommand.class);
        given(commandFactory.createCommand(TargetFollowOnRefundCommand.class)).willReturn(command);
        paymentMethodImpl.followOnRefund(followOnRefundRequest);
        verify(command).perform(followOnRefundRequest);
    }

    @Test(expected = AdapterException.class)
    public void testFollowOnRefundWithException() throws CommandNotSupportedException {
        given(commandFactory.createCommand(TargetFollowOnRefundCommand.class)).willThrow(
                new CommandNotSupportedException());
        paymentMethodImpl.followOnRefund(followOnRefundRequest);
    }
}

package au.com.target.tgtpayment.methods.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.factory.CommandFactory;
import de.hybris.platform.payment.commands.factory.CommandNotSupportedException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtpayment.commands.TargetCaptureCommand;
import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.commands.TargetExcessiveRefundCommand;
import au.com.target.tgtpayment.commands.TargetFollowOnRefundCommand;
import au.com.target.tgtpayment.commands.TargetPaymentVoidCommand;
import au.com.target.tgtpayment.commands.TargetRetrieveTransactionCommand;
import au.com.target.tgtpayment.commands.TargetStandaloneRefundCommand;
import au.com.target.tgtpayment.commands.TargetTokenizeCommand;
import au.com.target.tgtpayment.commands.request.AbstractTargetStandaloneRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.request.TargetExcessiveRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentVoidRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.request.TargetTokenizeRequest;


/**
 * Tests for {@link TargetIpgPaymentMethodImpl}
 */
@UnitTest
public class TargetIpgPaymentMethodImplTest {
    private TargetIpgPaymentMethodImpl targetIpgPaymentMethod;

    @Mock
    private CommandFactory commandFactory;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        targetIpgPaymentMethod = new TargetIpgPaymentMethodImpl();
        targetIpgPaymentMethod.setCommandFactory(commandFactory);
    }

    @Test(expected = AdapterException.class)
    public void testCreateSubscription() throws CommandNotSupportedException {
        final TargetCreateSubscriptionRequest request = Mockito.mock(TargetCreateSubscriptionRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetCreateSubscriptionCommand.class)).willThrow(
                new CommandNotSupportedException());
        targetIpgPaymentMethod.createSubscription(request);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testCapture() throws CommandNotSupportedException {
        final TargetCaptureRequest request = Mockito.mock(TargetCaptureRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetCaptureCommand.class)).willThrow(
                new UnsupportedOperationException());
        targetIpgPaymentMethod.capture(request);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testTokenize() throws CommandNotSupportedException {
        final TargetTokenizeRequest request = Mockito.mock(TargetTokenizeRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetTokenizeCommand.class)).willThrow(
                new UnsupportedOperationException());
        targetIpgPaymentMethod.tokenize(request);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testStandaloneRefund() throws CommandNotSupportedException {
        final AbstractTargetStandaloneRefundRequest request = Mockito.mock(AbstractTargetStandaloneRefundRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetStandaloneRefundCommand.class)).willThrow(
                new UnsupportedOperationException());
        targetIpgPaymentMethod.standaloneRefund(request);
    }

    @Test(expected = AdapterException.class)
    public void testFollowOnRefund() throws CommandNotSupportedException {
        final TargetFollowOnRefundRequest request = Mockito.mock(TargetFollowOnRefundRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetFollowOnRefundCommand.class)).willThrow(
                new CommandNotSupportedException());
        targetIpgPaymentMethod.followOnRefund(request);
    }

    @Test
    public void testFollowOnRefundSuccess() throws CommandNotSupportedException {
        final TargetFollowOnRefundRequest request = Mockito.mock(TargetFollowOnRefundRequest.class);
        final TargetFollowOnRefundCommand command = Mockito.mock(TargetFollowOnRefundCommand.class);
        BDDMockito.given(commandFactory.createCommand(TargetFollowOnRefundCommand.class)).willReturn(
                command);
        targetIpgPaymentMethod.followOnRefund(request);
        BDDMockito.verify(command, Mockito.times(1)).perform(request);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testRetrieveTransaction() throws CommandNotSupportedException {
        final TargetRetrieveTransactionRequest request = Mockito.mock(TargetRetrieveTransactionRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetRetrieveTransactionCommand.class)).willThrow(
                new UnsupportedOperationException());
        targetIpgPaymentMethod.retrieveTransaction(request);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testExcessiveRefund() throws CommandNotSupportedException {
        final TargetExcessiveRefundRequest request = Mockito.mock(TargetExcessiveRefundRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetExcessiveRefundCommand.class)).willThrow(
                new UnsupportedOperationException());
        targetIpgPaymentMethod.excessiveRefund(request);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testVoidPayment() throws CommandNotSupportedException {
        final TargetPaymentVoidRequest request = Mockito.mock(TargetPaymentVoidRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetPaymentVoidCommand.class)).willThrow(
                new UnsupportedOperationException());
        targetIpgPaymentMethod.voidPayment(request);
    }
}
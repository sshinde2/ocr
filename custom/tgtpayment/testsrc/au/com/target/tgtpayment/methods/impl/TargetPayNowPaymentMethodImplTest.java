package au.com.target.tgtpayment.methods.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.GetSubscriptionDataCommand;
import de.hybris.platform.payment.commands.factory.CommandFactory;
import de.hybris.platform.payment.commands.factory.CommandNotSupportedException;

import org.apache.commons.lang.NotImplementedException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.commands.TargetFollowOnRefundCommand;
import au.com.target.tgtpayment.commands.TargetRetrieveTransactionCommand;
import au.com.target.tgtpayment.commands.TargetStandaloneRefundCommand;
import au.com.target.tgtpayment.commands.request.AbstractTargetStandaloneRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.request.TargetExcessiveRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentVoidRequest;
import au.com.target.tgtpayment.commands.request.TargetQueryTransactionDetailsRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.request.TargetTokenizeRequest;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPayNowPaymentMethodImplTest {

    @InjectMocks
    private final TargetPayNowPaymentMethodImpl targetPayNowPaymentMethod = new TargetPayNowPaymentMethodImpl();

    @Mock
    private CommandFactory commandFactory;

    @Test(expected = UnsupportedOperationException.class)
    public void testCaptureNotSupported() throws CommandNotSupportedException {
        final TargetCaptureRequest request = Mockito.mock(TargetCaptureRequest.class);
        BDDMockito.given(commandFactory.createCommand(GetSubscriptionDataCommand.class)).willThrow(
                new UnsupportedOperationException());
        targetPayNowPaymentMethod.capture(request);
    }

    @Test(expected = NotImplementedException.class)
    public void testCaptureNotImplemented() throws CommandNotSupportedException {
        final TargetCaptureRequest request = Mockito.mock(TargetCaptureRequest.class);
        BDDMockito.given(commandFactory.createCommand(GetSubscriptionDataCommand.class)).willThrow(
                new NotImplementedException());
        targetPayNowPaymentMethod.capture(request);
    }

    @Test(expected = AdapterException.class)
    public void testCreateSubscriptionNotSupported() throws CommandNotSupportedException {
        final TargetCreateSubscriptionRequest request = Mockito.mock(TargetCreateSubscriptionRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetCreateSubscriptionCommand.class)).willThrow(
                new CommandNotSupportedException());
        targetPayNowPaymentMethod.createSubscription(request);
    }

    @Test(expected = NotImplementedException.class)
    public void testCreateSubscriptionNotImplemented() throws CommandNotSupportedException {
        final TargetCreateSubscriptionRequest request = Mockito.mock(TargetCreateSubscriptionRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetCreateSubscriptionCommand.class)).willThrow(
                new NotImplementedException());
        targetPayNowPaymentMethod.createSubscription(request);
    }

    @Test(expected = AdapterException.class)
    public void testFollowOnRefundNotSupported() throws CommandNotSupportedException {
        final TargetFollowOnRefundRequest request = Mockito.mock(TargetFollowOnRefundRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetFollowOnRefundCommand.class)).willThrow(
                new CommandNotSupportedException());
        targetPayNowPaymentMethod.followOnRefund(request);
    }

    @Test(expected = NotImplementedException.class)
    public void testFollowOnRefundNotImplemented() throws CommandNotSupportedException {
        final TargetFollowOnRefundRequest request = Mockito.mock(TargetFollowOnRefundRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetFollowOnRefundCommand.class)).willThrow(
                new NotImplementedException());
        targetPayNowPaymentMethod.followOnRefund(request);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testRetrieveTransactionNotSupported() throws CommandNotSupportedException {
        final TargetRetrieveTransactionRequest request = Mockito.mock(TargetRetrieveTransactionRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetRetrieveTransactionCommand.class)).willThrow(
                new UnsupportedOperationException());
        targetPayNowPaymentMethod.retrieveTransaction(request);
    }

    @Test(expected = NotImplementedException.class)
    public void testRetrieveTransactionNotImplemented() throws CommandNotSupportedException {
        final TargetRetrieveTransactionRequest request = Mockito.mock(TargetRetrieveTransactionRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetRetrieveTransactionCommand.class)).willThrow(
                new NotImplementedException());
        targetPayNowPaymentMethod.retrieveTransaction(request);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testStandaloneRefundNotSupported() throws CommandNotSupportedException {
        final AbstractTargetStandaloneRefundRequest request = Mockito.mock(AbstractTargetStandaloneRefundRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetStandaloneRefundCommand.class)).willThrow(
                new UnsupportedOperationException());
        targetPayNowPaymentMethod.standaloneRefund(request);
    }

    @Test(expected = NotImplementedException.class)
    public void testStandaloneRefundNotImplemented() throws CommandNotSupportedException {
        final AbstractTargetStandaloneRefundRequest request = Mockito.mock(AbstractTargetStandaloneRefundRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetStandaloneRefundCommand.class)).willThrow(
                new NotImplementedException());
        targetPayNowPaymentMethod.standaloneRefund(request);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testTokenizeNotSupported() throws CommandNotSupportedException {
        final TargetTokenizeRequest request = Mockito.mock(TargetTokenizeRequest.class);
        targetPayNowPaymentMethod.tokenize(request);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testVoidPaymentNotSupported() throws CommandNotSupportedException {
        final TargetPaymentVoidRequest request = Mockito.mock(TargetPaymentVoidRequest.class);
        targetPayNowPaymentMethod.voidPayment(request);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testExcessiveRefundNotSupported() throws CommandNotSupportedException {
        final TargetExcessiveRefundRequest request = Mockito.mock(TargetExcessiveRefundRequest.class);
        targetPayNowPaymentMethod.excessiveRefund(request);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testQueryTransactionDetailsNotSupported() throws Exception {
        final TargetQueryTransactionDetailsRequest request = Mockito.mock(TargetQueryTransactionDetailsRequest.class);
        targetPayNowPaymentMethod.queryTransactionDetails(request);
    }

}

/**
 * 
 */
package au.com.target.tgtpayment.methods.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.GetSubscriptionDataCommand;
import de.hybris.platform.payment.commands.factory.CommandFactory;
import de.hybris.platform.payment.commands.factory.CommandNotSupportedException;
import de.hybris.platform.payment.commands.request.SubscriptionDataRequest;
import de.hybris.platform.payment.commands.result.SubscriptionDataResult;
import de.hybris.platform.payment.dto.BillingInfo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.TargetCaptureCommand;
import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.commands.TargetFollowOnRefundCommand;
import au.com.target.tgtpayment.commands.TargetRetrieveTransactionCommand;
import au.com.target.tgtpayment.commands.TargetStandaloneRefundCommand;
import au.com.target.tgtpayment.commands.request.AbstractTargetStandaloneRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;


/**
 * Tests for {@link TargetPayPalPaymentMethodImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPayPalPaymentMethodImplTest {

    @InjectMocks
    private final TargetPayPalPaymentMethodImpl targetPaypalPaymentMethod = new TargetPayPalPaymentMethodImpl();

    @Mock
    private CommandFactory commandFactory;

    @Mock
    private GetSubscriptionDataCommand subCommand;

    @Mock
    private TargetCaptureCommand captureCommand;

    @Mock
    private TargetCaptureRequest captureRequest;

    @Mock
    private TargetCaptureResult result;

    @Before
    public void setup() {
        targetPaypalPaymentMethod.setCommandFactory(commandFactory);
    }

    @Test
    public void testCapture() throws CommandNotSupportedException {
        final TargetCaptureResult captureResult = new TargetCaptureResult();
        BDDMockito.given(captureRequest.getTransactionId()).willReturn("TRA_ID");
        BDDMockito.given(captureRequest.getToken()).willReturn("Token");
        BDDMockito.given(commandFactory.getPaymentProvider()).willReturn("Paypal");
        BDDMockito.given(commandFactory.createCommand(GetSubscriptionDataCommand.class)).willReturn(subCommand);
        final BillingInfo bInfo = Mockito.mock(BillingInfo.class);
        final SubscriptionDataResult subResult = Mockito.mock(SubscriptionDataResult.class);
        BDDMockito.given(subCommand.perform(Mockito.any(SubscriptionDataRequest.class))).willReturn(subResult);
        BDDMockito.given(commandFactory.createCommand(TargetCaptureCommand.class)).willReturn(captureCommand);
        BDDMockito.given(subResult.getBillingInfo()).willReturn(bInfo);
        BDDMockito.given(captureCommand.perform(captureRequest)).willReturn(captureResult);
        targetPaypalPaymentMethod.capture(captureRequest);
        Assert.assertNotNull(captureResult);
        Assert.assertEquals(bInfo, captureResult.getBillingInfo());
    }

    @Test(expected = AdapterException.class)
    public void testCreateSubscription() throws CommandNotSupportedException {
        final TargetCreateSubscriptionRequest request = Mockito.mock(TargetCreateSubscriptionRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetCreateSubscriptionCommand.class)).willThrow(
                new CommandNotSupportedException());
        targetPaypalPaymentMethod.createSubscription(request);
    }

    @Test(expected = AdapterException.class)
    public void testFollowOnRefund() throws CommandNotSupportedException {
        final TargetFollowOnRefundRequest request = Mockito.mock(TargetFollowOnRefundRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetFollowOnRefundCommand.class)).willThrow(
                new CommandNotSupportedException());
        targetPaypalPaymentMethod.followOnRefund(request);
    }

    @Test(expected = AdapterException.class)
    public void testRetrieveTransaction() throws CommandNotSupportedException {
        final TargetRetrieveTransactionRequest request = Mockito.mock(TargetRetrieveTransactionRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetRetrieveTransactionCommand.class)).willThrow(
                new CommandNotSupportedException());
        targetPaypalPaymentMethod.retrieveTransaction(request);
    }

    @Test(expected = AdapterException.class)
    public void testStandaloneRefund() throws CommandNotSupportedException {
        final AbstractTargetStandaloneRefundRequest request = Mockito.mock(AbstractTargetStandaloneRefundRequest.class);
        BDDMockito.given(commandFactory.createCommand(TargetStandaloneRefundCommand.class)).willThrow(
                new CommandNotSupportedException());
        targetPaypalPaymentMethod.standaloneRefund(request);
    }


}

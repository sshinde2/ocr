/**
 * 
 */
package au.com.target.tgtpayment.methods.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.GetSubscriptionDataCommand;
import de.hybris.platform.payment.commands.factory.CommandFactory;
import de.hybris.platform.payment.commands.factory.CommandNotSupportedException;
import de.hybris.platform.payment.commands.request.SubscriptionDataRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.TargetCaptureCommand;
import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.commands.TargetFollowOnRefundCommand;
import au.com.target.tgtpayment.commands.TargetPaymentPingCommand;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetPaymentPingRequest;


/**
 * @author salexa10
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetZippayPaymentMethodImplTest {

    @InjectMocks
    private final TargetZippayPaymentMethodImpl paymentMethodImpl = new TargetZippayPaymentMethodImpl();

    @Mock
    private TargetCreateSubscriptionRequest createSubscriptionRequest;

    @Mock
    private CommandFactory commandFactory;

    @Mock
    private TargetPaymentPingRequest pingRequest;

    @Mock
    private SubscriptionDataRequest subscriptionRequest;

    @Mock
    private TargetCaptureRequest targetCaptureRequest;

    @Mock
    private TargetFollowOnRefundRequest followOnRefundRequest;

    @Test(expected = AdapterException.class)
    public void testCreateSubscriptionCommandWithCommandNotSupported() throws CommandNotSupportedException {
        given(commandFactory.createCommand(TargetCreateSubscriptionCommand.class)).willThrow(
                new CommandNotSupportedException());
        paymentMethodImpl.createSubscription(createSubscriptionRequest);
    }

    @Test
    public void testCreateSubscriptionCommand() throws CommandNotSupportedException {
        final TargetCreateSubscriptionCommand command = mock(TargetCreateSubscriptionCommand.class);
        given(commandFactory.createCommand(TargetCreateSubscriptionCommand.class)).willReturn(command);
        paymentMethodImpl.createSubscription(createSubscriptionRequest);
        verify(command).perform(createSubscriptionRequest);
    }

    @Test
    public void testPing() throws CommandNotSupportedException {
        final TargetPaymentPingCommand command = mock(TargetPaymentPingCommand.class);
        given(commandFactory.createCommand(TargetPaymentPingCommand.class)).willReturn(command);
        paymentMethodImpl.ping(pingRequest);
        verify(command).perform(pingRequest);
    }

    @Test(expected = AdapterException.class)
    public void testPingWithCommandNotSupported() throws CommandNotSupportedException {
        given(commandFactory.createCommand(TargetPaymentPingCommand.class)).willThrow(
                new CommandNotSupportedException());
        paymentMethodImpl.ping(pingRequest);
    }

    @Test
    public void testGetSubscription() throws CommandNotSupportedException {
        final GetSubscriptionDataCommand command = mock(GetSubscriptionDataCommand.class);
        given(commandFactory.createCommand(GetSubscriptionDataCommand.class)).willReturn(command);
        paymentMethodImpl.getSubscription(subscriptionRequest);
        verify(command).perform(subscriptionRequest);
    }

    @Test(expected = AdapterException.class)
    public void testGetSubscriptionWithException() throws CommandNotSupportedException {
        given(commandFactory.createCommand(GetSubscriptionDataCommand.class)).willThrow(
                new CommandNotSupportedException());
        paymentMethodImpl.getSubscription(subscriptionRequest);
    }


    @Test(expected = AdapterException.class)
    public void testCaptureCommandWithCommandNotSupported() throws CommandNotSupportedException {
        given(commandFactory.createCommand(TargetCaptureCommand.class)).willThrow(
                new CommandNotSupportedException());
        paymentMethodImpl.capture(targetCaptureRequest);
    }

    @Test
    public void testCaptureCommand() throws CommandNotSupportedException {
        final TargetCaptureCommand command = mock(TargetCaptureCommand.class);
        given(commandFactory.createCommand(TargetCaptureCommand.class)).willReturn(command);
        paymentMethodImpl.capture(targetCaptureRequest);
        verify(command).perform(targetCaptureRequest);
    }

    @Test
    public void testFollowOnRefund() throws CommandNotSupportedException {
        final TargetFollowOnRefundCommand command = mock(TargetFollowOnRefundCommand.class);
        given(commandFactory.createCommand(TargetFollowOnRefundCommand.class)).willReturn(command);
        paymentMethodImpl.followOnRefund(followOnRefundRequest);
        verify(command).perform(followOnRefundRequest);
    }

    @Test(expected = AdapterException.class)
    public void testFollowOnRefundWithException() throws CommandNotSupportedException {
        given(commandFactory.createCommand(TargetFollowOnRefundCommand.class)).willThrow(
                new CommandNotSupportedException());
        paymentMethodImpl.followOnRefund(followOnRefundRequest);
    }
}

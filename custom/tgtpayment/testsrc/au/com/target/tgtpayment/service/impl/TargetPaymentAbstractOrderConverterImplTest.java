/**
 * 
 */
package au.com.target.tgtpayment.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.voucher.VoucherService;
import de.hybris.platform.voucher.model.PromotionVoucherModel;
import de.hybris.platform.voucher.model.VoucherModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Currency;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtpayment.dto.Address;
import au.com.target.tgtpayment.dto.AmountType;
import au.com.target.tgtpayment.dto.LineItem;
import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpayment.strategy.FindVoucherValueStrategy;
import au.com.target.tgtpayment.strategy.TargetVoucherConverterStategy;
import au.com.target.tgtpayment.util.TargetPaymentOrderHelper;
import au.com.target.tgtutility.util.TargetProductUtils;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPaymentAbstractOrderConverterImplTest {

    private static final Double VOUCHER_VALUE_1 = Double.valueOf(5.5);
    private static final String VOUCHER_CODE_1 = "ABC1";

    private static final Double VOUCHER_VALUE_2 = Double.valueOf(3.5);
    private static final String VOUCHER_CODE_2 = "ABC2";

    private static final String VOUCHER_NAME = "voucher_name";
    private static final String VOUCHER_DESC = "voucher_desc";

    @Mock
    private AbstractOrderModel order;

    @Mock
    private CartModel cartModel;

    @Mock
    private OrderModel orderModel;

    @Mock
    private CurrencyModel currencyModel;

    @Mock
    private AbstractOrderEntryModel abstractOrderEntry;

    @Mock
    private ProductModel product;

    @Mock
    private LineItem lineItem1;

    @Mock
    private AmountType at1;

    @Mock
    private Address address;

    @Mock
    private VoucherService voucherService;

    @Mock
    private FindVoucherValueStrategy findVoucherValueStrategy;

    @Mock
    private TargetVoucherConverterStategy targetVoucherConverterStategy;

    @Mock
    private TargetZoneDeliveryModeModel targetZoneDeliveryModeModel;

    @Mock
    private DeliveryModeModel deliveryModeModel;

    @Mock
    private MediaModel mediaModel;

    @Mock
    private AddressModel addressModel;

    @Mock
    private CountryModel countryModel;

    @InjectMocks
    @Spy
    private final TargetPaymentAbstractOrderConverterImpl converter = new TargetPaymentAbstractOrderConverterImpl();

    private VoucherModel voucher1;
    private VoucherModel voucher2;
    private Currency currency;

    @Mock
    private AbstractTargetVariantProductModel abstractTargetVariantProductModel;

    @Mock
    private TargetColourVariantProductModel targetColourVariantProductModel;

    @Mock
    private TargetPaymentOrderHelper targetPaymentOrderHelper;

    @Before
    public void setup() {
        final List<AbstractOrderEntryModel> entries = new ArrayList<>();
        entries.add(abstractOrderEntry);
        given(order.getEntries()).willReturn(entries);
        given(order.getCurrency()).willReturn(currencyModel);
        given(currencyModel.getIsocode()).willReturn("AUD");
        currency = Currency.getInstance("AUD");

        given(abstractOrderEntry.getBasePrice()).willReturn(Double.valueOf(10.0));
        given(abstractOrderEntry.getQuantity()).willReturn(Long.valueOf(10L));

        product = abstractTargetVariantProductModel;
        given(targetPaymentOrderHelper.getBaseSecureUrl()).willReturn(
                "https://www.target.com.au");
        given(targetPaymentOrderHelper.getProductThumbnailUrl(product)).willReturn(
                "https://www.target.com.au/medias/static_content/product/images/hero/31/93/A1203193.jpg");
        given(mediaModel.getURL())
                .willReturn("/medias/static_content/product/images/hero/31/93/A1203193.jpg");
        given(targetColourVariantProductModel.getThumbnail())
                .willReturn(mediaModel);
        given(TargetProductUtils.getColourVariantProduct(abstractTargetVariantProductModel))
                .willReturn(targetColourVariantProductModel);

        given(abstractOrderEntry.getProduct()).willReturn(product);
        given(product.getName()).willReturn("Product1");

        given(order.getDeliveryCost()).willReturn(Double.valueOf(5.0));
        given(order.getTotalPrice()).willReturn(Double.valueOf(150.00));

        final List<LineItem> discounts = new ArrayList<>();
        discounts.add(lineItem1);
        willReturn(discounts).given(converter).getDiscountsAndCreateLineItems(order, currency);
        given(at1.getAmount()).willReturn(BigDecimal.valueOf(-2.0));
        given(lineItem1.getPrice()).willReturn(at1);

        given(addressModel.getLine1()).willReturn("12 Bourke Street");
        given(addressModel.getLine2()).willReturn("");
        given(addressModel.getDistrict()).willReturn("Test");
        given(addressModel.getTown()).willReturn("Paramatta");
        given(addressModel.getPostalcode()).willReturn("4501");
        given(addressModel.getFirstname()).willReturn("John");
        given(addressModel.getLastname()).willReturn("Towns");
        given(addressModel.getCountry()).willReturn(countryModel);
        given(countryModel.getName()).willReturn("Australia");
        given(addressModel.getCountry().getIsocode()).willReturn("AU");

        // Set up 2 vouchers each with a specified value on the cartModel and the orderModel
        voucher1 = mock(PromotionVoucherModel.class);
        given(findVoucherValueStrategy.getDiscountValueAppliedValue(voucher1, cartModel)).willReturn(VOUCHER_VALUE_1);
        given(findVoucherValueStrategy.getDiscountValueAppliedValue(voucher1, orderModel)).willReturn(VOUCHER_VALUE_1);
        given(voucherService.getVoucher(VOUCHER_CODE_1)).willReturn(voucher1);

        voucher2 = mock(PromotionVoucherModel.class);
        given(findVoucherValueStrategy.getDiscountValueAppliedValue(voucher2, cartModel)).willReturn(VOUCHER_VALUE_2);
        given(findVoucherValueStrategy.getDiscountValueAppliedValue(voucher2, orderModel)).willReturn(VOUCHER_VALUE_2);
        given(voucherService.getVoucher(VOUCHER_CODE_2)).willReturn(voucher2);

        given(targetVoucherConverterStategy.getVoucherName(Mockito.any(VoucherModel.class), Mockito.anyString()))
                .willReturn(VOUCHER_NAME);
        given(targetVoucherConverterStategy.getVoucherDescription(Mockito.any(VoucherModel.class), Mockito.anyString()))
                .willReturn(VOUCHER_DESC);

        deliveryModeModel = targetZoneDeliveryModeModel;
        given(((TargetZoneDeliveryModeModel)deliveryModeModel).getIsDeliveryToStore())
                .willReturn(Boolean.valueOf(true));
        given(order.getDeliveryMode()).willReturn(deliveryModeModel);


    }

    @Test
    public void testConvertAbstractOrderModelToOrder() {

        final Order result = converter.convertAbstractOrderModelToOrder(order, null, null);
        assertThat(result).isNotNull();
        assertThat(result.getShippingTotalAmount().getAmount()).isEqualTo(BigDecimal.valueOf(5.0));
        assertThat(result.getItemSubTotalAmount().getAmount()).isEqualTo(BigDecimal.valueOf(98.0));
        assertThat(result.getOrderTotalAmount().getAmount()).isEqualTo(BigDecimal.valueOf(150.0));
        assertThat(result.getLineItems()).onProperty("imageUri").containsExactly(
                "https://www.target.com.au/medias/static_content/product/images/hero/31/93/A1203193.jpg", null);
        assertThat(result.isPickup()).isEqualTo(Boolean.valueOf(true));
    }

    @Test
    public void testAddVoucherLineItemsGivenCartWithNoVouchers() {

        final List<LineItem> lineItems = new ArrayList<>();
        given(voucherService.getAppliedVoucherCodes(cartModel)).willReturn(Collections.EMPTY_LIST);

        converter.addVoucherLineItems(lineItems, cartModel, currency);

        assertThat(lineItems).isEmpty();
    }

    @Test
    public void testAddVoucherLineItemsGivenOrderWithNoVouchers() {

        final List<LineItem> lineItems = new ArrayList<>();
        given(voucherService.getAppliedVoucherCodes(orderModel)).willReturn(Collections.EMPTY_LIST);

        converter.addVoucherLineItems(lineItems, orderModel, currency);

        assertThat(lineItems).isEmpty();
    }

    @Test
    public void testAddVoucherLineItemsGivenCartWithOneVoucher() {

        final List<LineItem> lineItems = new ArrayList<>();

        given(voucherService.getAppliedVoucherCodes(cartModel)).willReturn(Collections.singletonList(VOUCHER_CODE_1));

        converter.addVoucherLineItems(lineItems, cartModel, currency);
        assertThat(lineItems).hasSize(1);
        checkLineItemForVoucher1(lineItems.get(0));
    }

    @Test
    public void testAddVoucherLineItemsGivenOrderWithOneVoucher() {

        final List<LineItem> lineItems = new ArrayList<>();

        given(voucherService.getAppliedVoucherCodes(orderModel)).willReturn(Collections.singletonList(VOUCHER_CODE_1));

        converter.addVoucherLineItems(lineItems, orderModel, currency);
        assertThat(lineItems).hasSize(1);
        checkLineItemForVoucher1(lineItems.get(0));
    }

    @Test
    public void testAddVoucherLineItemsGivenCartWithTwoVouchers() {

        final List<LineItem> lineItems = new ArrayList<>();

        final Collection<String> codes = new ArrayList<>();
        codes.add(VOUCHER_CODE_1);
        codes.add(VOUCHER_CODE_2);

        given(voucherService.getAppliedVoucherCodes(cartModel)).willReturn(codes);

        converter.addVoucherLineItems(lineItems, cartModel, currency);
        assertThat(lineItems).hasSize(2);
        checkLineItemForVoucher1(lineItems.get(0));
        checkLineItemForVoucher2(lineItems.get(1));
    }

    private void checkLineItemForVoucher1(final LineItem line) {

        assertThat(line.getProductCode()).isEqualTo(VOUCHER_CODE_1);
        assertThat(line.getName()).isEqualTo(VOUCHER_NAME);
        assertThat(line.getDescription()).isEqualTo(VOUCHER_DESC);
        assertThat(line.isDiscount()).isEqualTo(true);
    }

    private void checkLineItemForVoucher2(final LineItem line) {

        assertThat(line.getProductCode()).isEqualTo(VOUCHER_CODE_2);
        assertThat(line.getName()).isEqualTo(VOUCHER_NAME);
        assertThat(line.getDescription()).isEqualTo(VOUCHER_DESC);
        assertThat(line.isDiscount()).isEqualTo(true);
    }

    @Test
    public void testConvertAbstractOrderModelToOrderCustomer() {
        final String customerId = "123456789";
        final TargetCustomerModel user = mock(TargetCustomerModel.class);
        given(order.getUser()).willReturn(user);
        given(user.getCustomerID()).willReturn(customerId);
        given(user.getFirstname()).willReturn("Peter");
        given(user.getLastname()).willReturn("Parker");
        given(user.getContactEmail()).willReturn("spidy@avengers.com");
        final Order convertedOrder = converter.convertAbstractOrderModelToOrder(order, null, null);
        assertThat(convertedOrder.getUserId()).isEqualTo(customerId);
        assertThat(convertedOrder.getCustomer()).isNotNull();
        assertThat(convertedOrder.getCustomer().getGivenNames()).isEqualTo("Peter");
        assertThat(convertedOrder.getCustomer().getSurname()).isEqualTo("Parker");
        assertThat(convertedOrder.getCustomer().getEmail()).isEqualTo("spidy@avengers.com");
    }

    @Test
    public void testConvertAbstractOrderModelToOrderForGuest() {
        final String customerId = "123456789";
        final TargetCustomerModel user = mock(TargetCustomerModel.class);

        given(order.getUser()).willReturn(user);
        final AddressModel deliveryAddress = mock(AddressModel.class);
        given(deliveryAddress.getFirstname()).willReturn("Tony");
        given(deliveryAddress.getLastname()).willReturn("Stark");
        given(deliveryAddress.getCountry()).willReturn(countryModel);
        given(deliveryAddress.getCountry().getIsocode()).willReturn("AU");

        given(order.getDeliveryAddress()).willReturn(deliveryAddress);
        given(user.getType()).willReturn(CustomerType.GUEST);
        given(user.getCustomerID()).willReturn(customerId);
        given(user.getFirstname()).willReturn("Peter");
        given(user.getLastname()).willReturn("Parker");
        given(user.getContactEmail()).willReturn("spidy@avengers.com");
        final Order convertedOrder = converter.convertAbstractOrderModelToOrder(order, null, null);
        assertThat(convertedOrder.getUserId()).isEqualTo(customerId);
        assertThat(convertedOrder.getCustomer()).isNotNull();
        assertThat(convertedOrder.getCustomer().getGivenNames()).isEqualTo("Tony");
        assertThat(convertedOrder.getCustomer().getSurname()).isEqualTo("Stark");
        assertThat(convertedOrder.getCustomer().getEmail()).isEqualTo("spidy@avengers.com");
    }

    @Test
    public void testConvertAbstractOrderModelToOrderForGuestCnC() {
        final String customerId = "123456789";
        final TargetCustomerModel user = mock(TargetCustomerModel.class);
        given(order.getUser()).willReturn(user);
        final AddressModel paymentAddress = mock(AddressModel.class);
        given(paymentAddress.getFirstname()).willReturn("Tony");
        given(paymentAddress.getLastname()).willReturn("Stark");
        given(paymentAddress.getCountry()).willReturn(countryModel);
        given(paymentAddress.getCountry().getIsocode()).willReturn("AU");
        given(paymentAddress.getCountry().getName()).willReturn("Australia");
        given(order.getPaymentAddress()).willReturn(paymentAddress);
        given(user.getType()).willReturn(CustomerType.GUEST);
        given(user.getCustomerID()).willReturn(customerId);
        given(user.getFirstname()).willReturn("Peter");
        given(user.getLastname()).willReturn("Parker");
        given(user.getContactEmail()).willReturn("spidy@avengers.com");
        final Order convertedOrder = converter.convertAbstractOrderModelToOrder(order, null, null);
        assertThat(convertedOrder.getUserId()).isEqualTo(customerId);
        assertThat(convertedOrder.getCustomer()).isNotNull();
        assertThat(convertedOrder.getCustomer().getGivenNames()).isEqualTo("Tony");
        assertThat(convertedOrder.getCustomer().getSurname()).isEqualTo("Stark");
        assertThat(convertedOrder.getCustomer().getEmail()).isEqualTo("spidy@avengers.com");
    }
}

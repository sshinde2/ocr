/**
 * 
 */
package au.com.target.tgtpayment.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.voucher.model.VoucherModel;

import java.util.Arrays;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Unit test for {@link FindVoucherValueStrategyImpl}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FindVoucherValueStrategyImplTest {

    @Mock
    private VoucherModel voucherModel;

    @Mock
    private AbstractOrderModel orderModel;

    private final FindVoucherValueStrategyImpl strategy = new FindVoucherValueStrategyImpl();


    @Before
    public void setUp() {
        BDDMockito.given(voucherModel.getCode()).willReturn("DOLLAR-OFF");
    }

    @Test
    public void testGetDiscountValueForNotAppliedVoucher() {

        final DiscountValue discountValue1 = Mockito.mock(DiscountValue.class);
        final DiscountValue discountValue2 = Mockito.mock(DiscountValue.class);

        BDDMockito.given(discountValue1.getCode()).willReturn("DOLLAR-OFF");
        BDDMockito.given(discountValue2.getCode()).willReturn("PERCENT-OFF");

        BDDMockito.given(orderModel.getGlobalDiscountValues())
                .willReturn(Arrays.asList(discountValue1, discountValue2));

        final VoucherModel notAppliedVoucher = Mockito.mock(VoucherModel.class);
        BDDMockito.given(notAppliedVoucher.getCode()).willReturn("DUMMY-CODE");

        final DiscountValue ret = strategy.getDiscountValue(notAppliedVoucher, orderModel);
        Assert.assertNull(ret);
    }

    @Test
    public void testGetDiscountValue() {

        final DiscountValue discountValue1 = Mockito.mock(DiscountValue.class);
        final DiscountValue discountValue2 = Mockito.mock(DiscountValue.class);

        BDDMockito.given(discountValue1.getCode()).willReturn("DOLLAR-OFF");
        BDDMockito.given(discountValue2.getCode()).willReturn("PERCENT-OFF");

        BDDMockito.given(orderModel.getGlobalDiscountValues())
                .willReturn(Arrays.asList(discountValue1, discountValue2));

        final DiscountValue ret = strategy.getDiscountValue(voucherModel, orderModel);
        Assert.assertEquals(discountValue1, ret);
    }

    @Test
    public void testGetDiscountValueAppliedValueGivenNull() {

        final Double ret = strategy.getDiscountValueAppliedValue(voucherModel, orderModel);
        Assert.assertNull(ret);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetDiscountValueAppliedValue() {
        final DiscountValue discountValue1 = Mockito.mock(DiscountValue.class);
        final DiscountValue discountValue2 = Mockito.mock(DiscountValue.class);

        BDDMockito.given(discountValue1.getCode()).willReturn("DOLLAR-OFF");
        BDDMockito.given(discountValue1.getAppliedValue()).willReturn(5.5);

        BDDMockito.given(discountValue2.getCode()).willReturn("PERCENT-OFF");
        BDDMockito.given(discountValue2.getAppliedValue()).willReturn(1.3);

        BDDMockito.given(orderModel.getGlobalDiscountValues())
                .willReturn(Arrays.asList(discountValue1, discountValue2));

        final Double ret = strategy.getDiscountValueAppliedValue(voucherModel, orderModel);
        Assert.assertEquals(Double.valueOf(5.5), ret);
    }


}

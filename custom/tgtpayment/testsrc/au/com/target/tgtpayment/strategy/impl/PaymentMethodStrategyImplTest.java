/**
 * 
 */
package au.com.target.tgtpayment.strategy.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.commands.factory.CommandFactoryRegistry;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.methods.TargetPaymentMethod;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PaymentMethodStrategyImplTest {
    @Mock
    private CommandFactoryRegistry commandFactoryRegistry;
    @Mock
    private Map<String, TargetPaymentMethod> paymentMode2MethodMapping;
    @Mock
    private Map<String, String> paymentProviderMapping;
    @Mock
    private Map<String, String> paymentMode2ProviderMapping;
    @Mock
    private TargetPaymentMethod paymentMethod;

    @InjectMocks
    private final PaymentMethodStrategyImpl paymentMethodStrategy = new PaymentMethodStrategyImpl();

    @Before
    public void setUp() {
        when(paymentMode2MethodMapping.get(anyString())).thenReturn(paymentMethod);
    }

    @Test
    public void testGetMethodByProviderCode() {
        final TargetPaymentMethod ipgPaymentMethod = paymentMethodStrategy.getPaymentMethod("ipg");
        assertEquals(ipgPaymentMethod, paymentMethod);
        verify(paymentMode2MethodMapping).get("ipg");
        verify(paymentMode2ProviderMapping).get("ipg");
    }
}

/**
 * 
 */
package au.com.target.tgtpayment.util;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.IpgGiftCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtpayment.dto.PaymentTransactionEntryData;
import au.com.target.tgtpayment.exceptions.PaymentException;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;


/**
 * @author vmuthura
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PaymentTransactionHelperTest {

    private PaymentTransactionModel captureTxn;
    private PaymentTransactionModel refundTxn;
    private PaymentTransactionModel pendingTxn;
    private PaymentTransactionModel rejectedTxn;
    private PaymentTransactionEntryModel pendingTxnEntry;
    private PaymentTransactionEntryModel captureTxnEntry;
    private PaymentTransactionEntryModel refundTxnEntry;
    private PaymentTransactionEntryModel rejectedTxnEntry;

    @Mock
    private AbstractOrderModel abstractOrderModel;


    private final List<PaymentTransactionModel> trans = new ArrayList<>();

    @Before
    public void setUp() {
        captureTxn = mock(PaymentTransactionModel.class);
        refundTxn = mock(PaymentTransactionModel.class);
        pendingTxn = mock(PaymentTransactionModel.class);
        rejectedTxn = mock(PaymentTransactionModel.class);
        captureTxnEntry = mock(PaymentTransactionEntryModel.class);
        refundTxnEntry = mock(PaymentTransactionEntryModel.class);
        pendingTxnEntry = mock(PaymentTransactionEntryModel.class);
        rejectedTxnEntry = mock(PaymentTransactionEntryModel.class);

        given(captureTxnEntry.getCode()).willReturn("a813d93b-d2d0-41d5-82bf-34eb88987f65-1");
        given(captureTxnEntry.getType()).willReturn(PaymentTransactionType.CAPTURE);
        given(captureTxnEntry.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.name());
        given(captureTxnEntry.getTime()).willReturn(new Date(System.currentTimeMillis()));

        given(refundTxnEntry.getCode()).willReturn("a813d93b-d2d0-41d5-82bf-34eb88987f65-2");
        given(refundTxnEntry.getType()).willReturn(PaymentTransactionType.REFUND_FOLLOW_ON);
        given(refundTxnEntry.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.name());
        given(refundTxnEntry.getTime()).willReturn(new Date(System.currentTimeMillis()));

        given(pendingTxnEntry.getCode()).willReturn("a813d93b-d2d0-41d5-82bf-34eb88987f65-1");
        given(pendingTxnEntry.getType()).willReturn(PaymentTransactionType.CAPTURE);
        given(pendingTxnEntry.getTransactionStatus()).willReturn(TransactionStatus.REVIEW.name());
        given(pendingTxnEntry.getTime()).willReturn(new Date(System.currentTimeMillis()));

        given(rejectedTxnEntry.getCode()).willReturn("a813d93b-d2d0-41d5-82bf-34eb88987f65-1");
        given(rejectedTxnEntry.getType()).willReturn(PaymentTransactionType.CAPTURE);
        given(rejectedTxnEntry.getTransactionStatus()).willReturn(TransactionStatus.REJECTED.name());
        given(rejectedTxnEntry.getTime()).willReturn(new Date(System.currentTimeMillis()));

        given(captureTxn.getEntries()).willReturn(ImmutableList.of(captureTxnEntry));
        given(refundTxn.getEntries()).willReturn(ImmutableList.of(refundTxnEntry));
        given(pendingTxn.getEntries()).willReturn(ImmutableList.of(pendingTxnEntry));
        given(rejectedTxn.getEntries()).willReturn(ImmutableList.of(rejectedTxnEntry));

        given(abstractOrderModel.getPaymentTransactions()).willReturn(trans);

        trans.clear();


    }

    @Test
    public void testGetLastPaymentForCapture() {
        final PaymentTransactionEntryModel lastEntry = PaymentTransactionHelper.getLastPayment(
                ImmutableList.of(captureTxn, refundTxn), ImmutableList.of(PaymentTransactionType.CAPTURE),
                TransactionStatus.ACCEPTED);

        assertEquals("Code ", "a813d93b-d2d0-41d5-82bf-34eb88987f65-1", lastEntry.getCode());
    }

    public void testGetLatestAcceptedTransactionEntry() {
        final PaymentTransactionEntryModel lastEntry = PaymentTransactionHelper
                .getLatestAcceptedTransactionEntry(ImmutableList.of(captureTxn, refundTxn));

        assertEquals("Code ", "a813d93b-d2d0-41d5-82bf-34eb88987f65-2", lastEntry.getCode());
    }

    @Test
    public void isLastTransactionPendingWithPaymentTransactionEmpty() {
        final OrderModel order = mock(OrderModel.class);
        final List<PaymentTransactionModel> paymentTransactionList = new ArrayList<>();
        given(order.getPaymentTransactions()).willReturn(paymentTransactionList);
        final boolean lastEntry = PaymentTransactionHelper.isLastTransactionPending(order);
        assertThat(lastEntry).as("TransactionStatus is not Pending").isFalse();
    }

    @Test
    public void isLastTransactionPendingWithPaymentTransactionWithoutPendingEntry() {
        final OrderModel order = mock(OrderModel.class);
        final List<PaymentTransactionModel> paymentTransactionList = new ArrayList<>();
        paymentTransactionList.add(captureTxn);
        given(order.getPaymentTransactions()).willReturn(paymentTransactionList);
        final boolean lastEntry = PaymentTransactionHelper.isLastTransactionPending(order);
        assertThat(lastEntry).as("TransactionStatus is not Pending").isFalse();
    }

    @Test
    public void isLastTransactionPendingWithPaymentTransactionWithPendingEntry() {
        final OrderModel order = mock(OrderModel.class);
        final List<PaymentTransactionModel> paymentTransactionList = new ArrayList<>();
        paymentTransactionList.add(pendingTxn);
        given(order.getPaymentTransactions()).willReturn(paymentTransactionList);
        final boolean lastEntry = PaymentTransactionHelper.isLastTransactionPending(order);
        assertThat(lastEntry).as("TransactionStatus is pending").isTrue();
    }

    @Test
    public void testFindCaptureTransactionGivenAcceptedTxn() {

        final List<PaymentTransactionModel> paymentTransactionList = new ArrayList<>();
        paymentTransactionList.add(captureTxn);

        final PaymentTransactionEntryModel ptem = PaymentTransactionHelper
                .findCaptureTransaction(paymentTransactionList);
        assertThat(ptem).isEqualTo(captureTxnEntry);
    }

    @Test
    public void testFindCaptureTransactionGivenPendingTxn() {

        final List<PaymentTransactionModel> paymentTransactionList = new ArrayList<>();
        paymentTransactionList.add(pendingTxn);

        final PaymentTransactionEntryModel ptem = PaymentTransactionHelper
                .findCaptureTransaction(paymentTransactionList);
        assertThat(ptem).isNull();
    }

    @Test
    public void testFindCaptureTransactionGivenRejectedTxn() {

        final List<PaymentTransactionModel> paymentTransactionList = new ArrayList<>();
        paymentTransactionList.add(rejectedTxn);

        final PaymentTransactionEntryModel ptem = PaymentTransactionHelper
                .findCaptureTransaction(paymentTransactionList);
        assertThat(ptem).isNull();
    }

    @Test
    public void testFindCaptureTransactionGivenRefundTxn() {

        final List<PaymentTransactionModel> paymentTransactionList = new ArrayList<>();
        paymentTransactionList.add(refundTxn);

        final PaymentTransactionEntryModel ptem = PaymentTransactionHelper
                .findCaptureTransaction(paymentTransactionList);
        assertThat(ptem).isNull();
    }

    @Test
    public void testFindCaptureTransactionInAcceptedOrReviewGivenAcceptedTxn() {

        final List<PaymentTransactionModel> paymentTransactionList = new ArrayList<>();
        paymentTransactionList.add(captureTxn);

        final PaymentTransactionEntryModel ptem = PaymentTransactionHelper
                .findCaptureTransactionInAcceptedOrReview(paymentTransactionList);
        assertThat(ptem).isEqualTo(captureTxnEntry);
    }

    @Test
    public void testFindCaptureTransactionInAcceptedOrReviewGivenPendingTxn() {

        final List<PaymentTransactionModel> paymentTransactionList = new ArrayList<>();
        paymentTransactionList.add(pendingTxn);

        final PaymentTransactionEntryModel ptem = PaymentTransactionHelper
                .findCaptureTransactionInAcceptedOrReview(paymentTransactionList);
        assertThat(ptem).isEqualTo(pendingTxnEntry);
    }

    @Test
    public void testFindCaptureTransactionInAcceptedOrReviewGivenRejectedTxn() {

        final List<PaymentTransactionModel> paymentTransactionList = new ArrayList<>();
        paymentTransactionList.add(rejectedTxn);

        final PaymentTransactionEntryModel ptem = PaymentTransactionHelper
                .findCaptureTransactionInAcceptedOrReview(paymentTransactionList);
        assertThat(ptem).isNull();
    }

    @Test
    public void testFindCaptureTransactionInAcceptedOrReviewGivenNoTxn() {

        final List<PaymentTransactionModel> paymentTransactionList = new ArrayList<>();

        final PaymentTransactionEntryModel ptem = PaymentTransactionHelper
                .findCaptureTransactionInAcceptedOrReview(paymentTransactionList);
        assertThat(ptem).isNull();
    }

    @Test
    public void testFindCaptureTransactionInAcceptedOrReviewGivenRefundTxn() {

        final List<PaymentTransactionModel> paymentTransactionList = new ArrayList<>();
        paymentTransactionList.add(refundTxn);

        final PaymentTransactionEntryModel ptem = PaymentTransactionHelper
                .findCaptureTransactionInAcceptedOrReview(paymentTransactionList);
        assertThat(ptem).isNull();
    }

    @Test
    public void testHasCaptureTransactionInAcceptedOrReviewGivenAcceptedTxn() {
        final List<PaymentTransactionModel> paymentTransactionList = new ArrayList<>();
        paymentTransactionList.add(captureTxn);

        assertThat(PaymentTransactionHelper.hasCaptureTransactionInAcceptedOrReview(paymentTransactionList)).isTrue();
    }

    @Test
    public void testHasCaptureTransactionInAcceptedOrReviewGivenPendingTxn() {
        final List<PaymentTransactionModel> paymentTransactionList = new ArrayList<>();
        paymentTransactionList.add(pendingTxn);

        assertThat(PaymentTransactionHelper.hasCaptureTransactionInAcceptedOrReview(paymentTransactionList)).isTrue();
    }

    @Test
    public void testHasCaptureTransactionInAcceptedOrReviewGivenRejectedTxn() {
        final List<PaymentTransactionModel> paymentTransactionList = new ArrayList<>();
        paymentTransactionList.add(rejectedTxn);

        assertThat(PaymentTransactionHelper.hasCaptureTransactionInAcceptedOrReview(paymentTransactionList)).isFalse();
    }

    @Test
    public void testExtractCardExpiryMonth() {
        final String expire = "03/17";
        final String expireMonth = PaymentTransactionHelper.extractCardExpiryMonth(expire);
        assertThat(expireMonth).isEqualTo("03");
    }

    @Test
    public void testExtractCardExpiryYear() {
        final String expire = "03/17";
        final String expireMonth = PaymentTransactionHelper.extractCardExpiryYear(expire);
        assertThat(expireMonth).isEqualTo("17");
    }

    @Test
    public void testFindCaptureTransactionWithIpgPaymentModel() {

        final PaymentTransactionModel transaction1 = mock(PaymentTransactionModel.class);
        given(transaction1.getIsCaptureSuccessful()).willReturn(Boolean.TRUE);
        final PaymentTransactionModel transaction2 = mock(PaymentTransactionModel.class);
        given(transaction2.getIsCaptureSuccessful()).willReturn(Boolean.FALSE);
        final PaymentTransactionModel transaction3 = mock(PaymentTransactionModel.class);
        given(transaction3.getIsCaptureSuccessful()).willReturn(Boolean.FALSE);
        final List<PaymentTransactionModel> paymentTransactionList = ImmutableList.of(transaction1, transaction2,
                transaction3);
        final OrderModel orderModel = mock(OrderModel.class);
        given(orderModel.getPaymentTransactions()).willReturn(paymentTransactionList);
        final IpgPaymentInfoModel ipgPaymentInfoModel = mock(IpgPaymentInfoModel.class);
        given(orderModel.getPaymentInfo()).willReturn(ipgPaymentInfoModel);
        final PaymentTransactionModel result = PaymentTransactionHelper.findCaptureTransaction(orderModel);
        assertThat(result).isEqualTo(transaction1);
    }

    @Test
    public void testFindCaptureTransactionWithIpgPaymentModelAndEmptyTransactions() {
        final List<PaymentTransactionModel> paymentTransactionList = new ArrayList<>();
        final OrderModel orderModel = mock(OrderModel.class);
        given(orderModel.getPaymentTransactions()).willReturn(paymentTransactionList);
        final IpgPaymentInfoModel ipgPaymentInfoModel = mock(IpgPaymentInfoModel.class);
        given(orderModel.getPaymentInfo()).willReturn(ipgPaymentInfoModel);
        final PaymentTransactionModel result = PaymentTransactionHelper.findCaptureTransaction(orderModel);
        assertThat(result).isNull();
    }

    @Test
    public void testFindCaptureTransactionWithIpgPaymentModelAndAllFailedTransaction() {

        final PaymentTransactionModel transaction1 = mock(PaymentTransactionModel.class);
        given(transaction1.getIsCaptureSuccessful()).willReturn(null);
        final PaymentTransactionModel transaction2 = mock(PaymentTransactionModel.class);
        given(transaction2.getIsCaptureSuccessful()).willReturn(Boolean.FALSE);
        final PaymentTransactionModel transaction3 = mock(PaymentTransactionModel.class);
        given(transaction3.getIsCaptureSuccessful()).willReturn(Boolean.FALSE);
        final List<PaymentTransactionModel> paymentTransactionList = ImmutableList.of(transaction1, transaction2,
                transaction3);
        final OrderModel orderModel = mock(OrderModel.class);
        given(orderModel.getPaymentTransactions()).willReturn(paymentTransactionList);
        final IpgPaymentInfoModel ipgPaymentInfoModel = mock(IpgPaymentInfoModel.class);
        given(orderModel.getPaymentInfo()).willReturn(ipgPaymentInfoModel);
        final PaymentTransactionModel result = PaymentTransactionHelper.findCaptureTransaction(orderModel);
        assertThat(result).isNull();
    }

    @Test
    public void testFindCaptureTransactionWithTnsPaymentModel() {
        final PaymentTransactionEntryModel paymentTransactionEntryModel = Mockito
                .mock(PaymentTransactionEntryModel.class);
        given(paymentTransactionEntryModel.getType()).willReturn(PaymentTransactionType.CAPTURE);
        given(paymentTransactionEntryModel.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        final List<PaymentTransactionEntryModel> entryList = ImmutableList.of(paymentTransactionEntryModel);
        final PaymentTransactionModel transaction = mock(PaymentTransactionModel.class);
        given(transaction.getIsCaptureSuccessful()).willReturn(null);
        given(transaction.getEntries()).willReturn(entryList);
        given(paymentTransactionEntryModel.getPaymentTransaction()).willReturn(transaction);
        final List<PaymentTransactionModel> paymentTransactionList = ImmutableList.of(transaction);
        final OrderModel orderModel = mock(OrderModel.class);
        given(orderModel.getPaymentTransactions()).willReturn(paymentTransactionList);
        final PaymentInfoModel tnsPaymentInfoModel = mock(PaymentInfoModel.class);
        given(orderModel.getPaymentInfo()).willReturn(tnsPaymentInfoModel);
        final PaymentTransactionModel result = PaymentTransactionHelper.findCaptureTransaction(orderModel);
        assertThat(result).isEqualTo(transaction);
    }


    @Test
    public void itWillReturnNullIfNoTran() {
        assertThat(PaymentTransactionHelper.findLastTransaction(abstractOrderModel)).isNull();
    }

    @Test
    public void itWillReturnNullIfEmptyTran() {
        trans.add(new PaymentTransactionModel());
        assertThat(PaymentTransactionHelper.findLastTransaction(abstractOrderModel)).isNull();
    }

    @Test
    public void itWillReturnLastOneIfNoTranHasDate() {
        given(captureTxn.getCreationtime()).willReturn(null);
        given(pendingTxn.getCreationtime()).willReturn(null);
        trans.add(captureTxn);
        trans.add(pendingTxn);
        assertThat(PaymentTransactionHelper.findLastTransaction(abstractOrderModel)).isEqualTo(pendingTxn);
    }

    @Test
    public void itWillReturnTheOneWithDateWhenGetLatestTran() {
        given(captureTxn.getCreationtime()).willReturn(new Date());
        given(pendingTxn.getCreationtime()).willReturn(null);
        trans.add(captureTxn);
        trans.add(pendingTxn);
        assertThat(PaymentTransactionHelper.findLastTransaction(abstractOrderModel)).isEqualTo(captureTxn);
    }

    @Test
    public void itWillReturnTheOneWithLatestDateWhenGetLatestTran() {
        given(captureTxn.getCreationtime()).willReturn(new Date());
        given(pendingTxn.getCreationtime()).willReturn(DateUtils.addHours(new Date(), 1));
        given(rejectedTxn.getCreationtime()).willReturn(DateUtils.addHours(new Date(), -1));
        trans.add(captureTxn);
        trans.add(pendingTxn);
        trans.add(rejectedTxn);
        assertThat(PaymentTransactionHelper.findLastTransaction(abstractOrderModel)).isEqualTo(pendingTxn);
    }

    @Test
    public void isCaptureTransactionReturnTrueForCaptureTxn() {
        assertThat(PaymentTransactionHelper.isCaptureTransaction(captureTxn)).isTrue();
    }

    @Test
    public void isCaptureTransactionReturnFalseForCaptureTxn() {
        assertThat(PaymentTransactionHelper.isCaptureTransaction(refundTxn)).isFalse();
    }

    @Test
    public void isCaptureTransactionReturnFalseForNull() {
        assertThat(PaymentTransactionHelper.isCaptureTransaction(null)).isFalse();
    }

    @Test
    public void isCaptureTransactionReturnFalseForEmptyTxn() {
        assertThat(PaymentTransactionHelper.isCaptureTransaction(new PaymentTransactionModel())).isFalse();
    }

    @Test
    public void testFindPreOrderBalanceAmountCaptureTransactionEntryWithNullOrder() {
        assertThat(PaymentTransactionHelper.findPreOrderBalanceAmountCaptureTransactionEntry(null)).isNull();
    }

    @Test
    public void testFindPreOrderBalanceAmountCaptureTransactionEntryWithNullPaymentModel() {
        final OrderModel orderModel = mock(OrderModel.class);
        final PaymentTransactionEntryModel entries = PaymentTransactionHelper
                .findPreOrderBalanceAmountCaptureTransactionEntry(orderModel);
        assertThat(entries).isNull();
    }

    @Test
    public void testFindPreOrderBalanceAmountCaptureTransactionEntryWithEmptyPaymentModel() {
        final OrderModel orderModel = mock(OrderModel.class);
        given(orderModel.getPaymentTransactions()).willReturn(ListUtils.EMPTY_LIST);
        final PaymentTransactionEntryModel entries = PaymentTransactionHelper
                .findPreOrderBalanceAmountCaptureTransactionEntry(orderModel);
        assertThat(entries).isNull();
    }

    @Test
    public void testFindPreOrderBalanceAmountCaptureTransactionEntryWithPaymentModel() {
        final OrderModel orderModel = mock(OrderModel.class);
        final PaymentTransactionModel transactionModel1 = mock(PaymentTransactionModel.class);
        final List<PaymentTransactionModel> paymentTransactions = new ArrayList<>();
        paymentTransactions.add(transactionModel1);
        given(orderModel.getPaymentTransactions()).willReturn(paymentTransactions);
        willReturn(Boolean.TRUE).given(transactionModel1).isIsPreOrderDepositCapture();
        willReturn(Boolean.TRUE).given(transactionModel1).getIsCaptureSuccessful();
        final PaymentTransactionEntryModel entries = PaymentTransactionHelper
                .findPreOrderBalanceAmountCaptureTransactionEntry(orderModel);
        assertThat(entries).isNull();
    }

    @Test
    public void testFindPreOrderBalanceAmountCaptureTransactionEntryBothInitalAndFinalPaymentModel() {
        final OrderModel orderModel = mock(OrderModel.class);
        final PaymentTransactionModel transactionModel1 = mock(PaymentTransactionModel.class);
        final PaymentTransactionModel transactionModel2 = mock(PaymentTransactionModel.class);
        final PaymentTransactionEntryModel entry1 = mock(PaymentTransactionEntryModel.class);

        final List<PaymentTransactionModel> paymentTransactions = new ArrayList<>();
        paymentTransactions.add(transactionModel1);
        paymentTransactions.add(transactionModel2);

        final List<PaymentTransactionEntryModel> entries = new ArrayList<>();
        entries.add(entry1);

        given(orderModel.getPaymentTransactions()).willReturn(paymentTransactions);
        willReturn(Boolean.TRUE).given(transactionModel1).isIsPreOrderDepositCapture();
        willReturn(Boolean.TRUE).given(transactionModel1).getIsCaptureSuccessful();
        willReturn(Boolean.FALSE).given(transactionModel2).isIsPreOrderDepositCapture();
        willReturn(Boolean.TRUE).given(transactionModel2).getIsCaptureSuccessful();
        given(transactionModel2.getEntries()).willReturn(entries);
        given(entry1.getTransactionStatus()).willReturn(TransactionStatus.ACCEPTED.toString());
        given(entry1.getType()).willReturn(PaymentTransactionType.CAPTURE);

        final PaymentTransactionEntryModel resultEntries = PaymentTransactionHelper
                .findPreOrderBalanceAmountCaptureTransactionEntry(orderModel);
        assertThat(resultEntries).isNotNull();
        assertThat(resultEntries).isEqualTo(entry1);
    }

    @Test
    public void testFindPreOrderBalanceAmountCaptureTransactionEntryWithStatusReject() {
        final OrderModel orderModel = mock(OrderModel.class);
        final PaymentTransactionModel transactionModel1 = mock(PaymentTransactionModel.class);
        final PaymentTransactionModel transactionModel2 = mock(PaymentTransactionModel.class);
        final PaymentTransactionEntryModel entry1 = mock(PaymentTransactionEntryModel.class);

        final List<PaymentTransactionModel> paymentTransactions = new ArrayList<>();
        paymentTransactions.add(transactionModel1);
        paymentTransactions.add(transactionModel2);

        final List<PaymentTransactionEntryModel> entries = new ArrayList<>();
        entries.add(entry1);

        given(orderModel.getPaymentTransactions()).willReturn(paymentTransactions);
        willReturn(Boolean.TRUE).given(transactionModel1).isIsPreOrderDepositCapture();
        willReturn(Boolean.TRUE).given(transactionModel1).getIsCaptureSuccessful();
        willReturn(Boolean.FALSE).given(transactionModel2).isIsPreOrderDepositCapture();
        willReturn(Boolean.TRUE).given(transactionModel2).getIsCaptureSuccessful();
        given(transactionModel2.getEntries()).willReturn(entries);
        given(entry1.getType()).willReturn(PaymentTransactionType.CAPTURE);
        given(entry1.getTransactionStatus()).willReturn(TransactionStatus.REJECTED.toString());

        final PaymentTransactionEntryModel resultEntries = PaymentTransactionHelper
                .findPreOrderBalanceAmountCaptureTransactionEntry(orderModel);
        assertThat(resultEntries).isNull();
    }

    @Test
    public void testFindPreOrderBalanceAmountCaptureTransactionEntryFailedFinalCapture() {
        final OrderModel orderModel = mock(OrderModel.class);
        final PaymentTransactionModel transactionModel1 = mock(PaymentTransactionModel.class);
        final PaymentTransactionModel transactionModel2 = mock(PaymentTransactionModel.class);
        final PaymentTransactionEntryModel entry1 = mock(PaymentTransactionEntryModel.class);

        final List<PaymentTransactionModel> paymentTransactions = new ArrayList<>();
        paymentTransactions.add(transactionModel1);
        paymentTransactions.add(transactionModel2);

        final List<PaymentTransactionEntryModel> entries = new ArrayList<>();
        entries.add(entry1);

        given(orderModel.getPaymentTransactions()).willReturn(paymentTransactions);
        willReturn(Boolean.TRUE).given(transactionModel1).isIsPreOrderDepositCapture();
        willReturn(Boolean.TRUE).given(transactionModel1).getIsCaptureSuccessful();
        willReturn(Boolean.FALSE).given(transactionModel2).isIsPreOrderDepositCapture();
        willReturn(Boolean.FALSE).given(transactionModel2).getIsCaptureSuccessful();
        given(transactionModel2.getEntries()).willReturn(entries);

        final PaymentTransactionEntryModel resultEntries = PaymentTransactionHelper
                .findPreOrderBalanceAmountCaptureTransactionEntry(orderModel);
        assertThat(resultEntries).isNull();
    }


    private PaymentTransactionEntryModel setUpPaymentTransactionEntry(final PaymentTransactionModel transaction,
            final BigDecimal amount,
            final PaymentTransactionType transactionType, final TransactionStatus transactionStatus,
            final PaymentInfoModel paymentInfo) {
        final PaymentTransactionEntryModel paymentTransactionEntry = mock(PaymentTransactionEntryModel.class);
        given(paymentTransactionEntry.getPaymentTransaction()).willReturn(transaction);
        given(paymentTransactionEntry.getIpgPaymentInfo()).willReturn(paymentInfo);
        given(paymentTransactionEntry.getType()).willReturn(transactionType);
        if (transactionStatus != null) {
            given(paymentTransactionEntry.getTransactionStatus()).willReturn(transactionStatus.toString());
        }
        given(paymentTransactionEntry.getAmount()).willReturn(amount);

        return paymentTransactionEntry;
    }

    private PaymentTransactionEntryData setUpPaymentTransactionData(final PaymentTransactionModel transaction,
            final BigDecimal amount,
            final PaymentTransactionType transactionType, final TransactionStatus transactionStatus,
            final PaymentInfoModel paymentInfo, final BigDecimal refundableAmount) {
        return new PaymentTransactionEntryData(setUpPaymentTransactionEntry(transaction, amount, transactionType,
                transactionStatus, paymentInfo), refundableAmount);
    }

    private PaymentInfoModel createPaymentInfoModel(final long num, final boolean isGiftcard) {

        final PaymentInfoModel paymentInfor;
        if (isGiftcard) {
            paymentInfor = mock(IpgGiftCardPaymentInfoModel.class);
        }
        else {
            paymentInfor = mock(PaymentInfoModel.class);
        }

        given(paymentInfor.getPk()).willReturn(PK.createFixedCounterPK(1, num));
        return paymentInfor;
    }

    @Test
    public void testFindCaptureTransactionEntriesWithRefundableAmountForIpg() {

        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);

        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);
        final BigDecimal amount2 = BigDecimal.valueOf(56.78d).setScale(2);
        final BigDecimal amount3 = BigDecimal.valueOf(23.99d).setScale(2);
        final BigDecimal amount4 = BigDecimal.valueOf(63.00d).setScale(2);
        final BigDecimal amount5 = BigDecimal.valueOf(14.21d).setScale(2);
        final BigDecimal amount6 = BigDecimal.valueOf(3.48d).setScale(2);
        final BigDecimal amount7 = BigDecimal.valueOf(63.00d).setScale(2);
        final BigDecimal amount8 = BigDecimal.valueOf(5.21d).setScale(2);
        final BigDecimal amount9 = BigDecimal.valueOf(1.01d).setScale(2);

        final PaymentInfoModel paymentInfor1 = createPaymentInfoModel(1, false);
        final PaymentInfoModel paymentInfor2 = createPaymentInfoModel(2, false);
        final PaymentInfoModel paymentInfor3 = createPaymentInfoModel(3, false);
        final PaymentInfoModel paymentInfor4 = createPaymentInfoModel(4, false);
        final PaymentInfoModel paymentInfor5 = createPaymentInfoModel(5, true);

        final PaymentTransactionEntryModel paymentTransactionCapture1 = setUpPaymentTransactionEntry(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.REJECTED, paymentInfor1);

        final PaymentTransactionEntryModel paymentTransactionCapture2 = setUpPaymentTransactionEntry(
                paymentTransaction, amount2,
                PaymentTransactionType.CAPTURE, TransactionStatus.REVIEW, paymentInfor2);

        final PaymentTransactionEntryModel paymentTransactionCapture3 = setUpPaymentTransactionEntry(
                paymentTransaction, amount3,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor3);

        final PaymentTransactionEntryModel paymentTransactionCapture4 = setUpPaymentTransactionEntry(
                paymentTransaction, amount4,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor4);

        final PaymentTransactionEntryModel paymentTransactionCapture5 = setUpPaymentTransactionEntry(
                paymentTransaction, amount5,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor5);

        final PaymentTransactionEntryModel paymentTransactionRefund1 = setUpPaymentTransactionEntry(paymentTransaction,
                amount6,
                PaymentTransactionType.REFUND_STANDALONE, TransactionStatus.ACCEPTED, paymentInfor3);

        final PaymentTransactionEntryModel paymentTransactionRefund2 = setUpPaymentTransactionEntry(paymentTransaction,
                amount7,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.ACCEPTED, paymentInfor4);

        final PaymentTransactionEntryModel paymentTransactionRefund3 = setUpPaymentTransactionEntry(paymentTransaction,
                amount8,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.ACCEPTED, paymentInfor5);

        final PaymentTransactionEntryModel paymentTransactionRefund4 = setUpPaymentTransactionEntry(paymentTransaction,
                amount9,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.REVIEW, paymentInfor5);


        given(paymentTransaction.getEntries()).willReturn(
                Arrays.asList(paymentTransactionCapture1, paymentTransactionCapture2,
                        paymentTransactionCapture3,
                        paymentTransactionCapture4, paymentTransactionCapture5, paymentTransactionRefund1,
                        paymentTransactionRefund2, paymentTransactionRefund3, paymentTransactionRefund4));
        final IpgPaymentInfoModel ipgPaymentInfoModel = mock(IpgPaymentInfoModel.class);
        given(paymentTransaction.getInfo()).willReturn(ipgPaymentInfoModel);

        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .findCaptureTransactionEntriesWithRefundableAmount(paymentTransaction);
        assertThat(result).hasSize(2);
        assertThat(result.get(0).getPaymentTransactionEntryModel()).isEqualTo(paymentTransactionCapture3);
        assertThat(result.get(0).getAmount()).isEqualTo(amount3.subtract(amount6));
        assertThat(result.get(1).getPaymentTransactionEntryModel()).isEqualTo(paymentTransactionCapture5);
        assertThat(result.get(1).getAmount()).isEqualTo(amount5.subtract(amount8));

    }

    @Test
    public void testFindCaptureTransactionEntriesWithRefundableAmountForIpgWithEmptyTransactionEntries() {

        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);

        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .findCaptureTransactionEntriesWithRefundableAmount(paymentTransaction);
        assertThat(result).isEmpty();
    }

    @Test
    public void testFindCaptureTransactionEntriesWithRefundableAmountForTns() {

        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);

        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);
        final BigDecimal amount2 = BigDecimal.valueOf(56.78d).setScale(2);
        final BigDecimal amount3 = BigDecimal.valueOf(150.99d).setScale(2);

        final BigDecimal amount6 = BigDecimal.valueOf(3.48d).setScale(2);
        final BigDecimal amount7 = BigDecimal.valueOf(63.00d).setScale(2);
        final BigDecimal amount8 = BigDecimal.valueOf(5.21d).setScale(2);
        final BigDecimal amount9 = BigDecimal.valueOf(1.01d).setScale(2);

        final PaymentTransactionEntryModel paymentTransactionCapture1 = setUpPaymentTransactionEntry(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.REJECTED, null);

        final PaymentTransactionEntryModel paymentTransactionCapture2 = setUpPaymentTransactionEntry(
                paymentTransaction, amount2,
                PaymentTransactionType.CAPTURE, TransactionStatus.REVIEW, null);

        final PaymentTransactionEntryModel paymentTransactionCapture3 = setUpPaymentTransactionEntry(
                paymentTransaction, amount3,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, null);


        final PaymentTransactionEntryModel paymentTransactionRefund1 = setUpPaymentTransactionEntry(paymentTransaction,
                amount6,
                PaymentTransactionType.REFUND_STANDALONE, TransactionStatus.ACCEPTED, null);

        final PaymentTransactionEntryModel paymentTransactionRefund2 = setUpPaymentTransactionEntry(paymentTransaction,
                amount7,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.ACCEPTED, null);

        final PaymentTransactionEntryModel paymentTransactionRefund3 = setUpPaymentTransactionEntry(paymentTransaction,
                amount8,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.REVIEW, null);

        final PaymentTransactionEntryModel paymentTransactionRefund4 = setUpPaymentTransactionEntry(paymentTransaction,
                amount9,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.REVIEW, null);

        given(paymentTransaction.getEntries()).willReturn(
                Arrays.asList(paymentTransactionCapture1, paymentTransactionCapture2,
                        paymentTransactionCapture3, paymentTransactionRefund1,
                        paymentTransactionRefund2, paymentTransactionRefund3, paymentTransactionRefund4));
        final PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);
        given(paymentTransaction.getInfo()).willReturn(paymentInfoModel);

        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .findCaptureTransactionEntriesWithRefundableAmountForTns(paymentTransaction);
        assertThat(result).hasSize(1);
        assertThat(result.get(0).getPaymentTransactionEntryModel()).isEqualTo(paymentTransactionCapture3);
        assertThat(result.get(0).getAmount()).isEqualTo(amount3.subtract(amount6).subtract(amount7));
    }

    @Test
    public void testFindCaptureTransactionEntriesWithRefundableAmountForTnsWithMatchCaptureAndRefund() {
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);

        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);
        final BigDecimal amount2 = BigDecimal.valueOf(56.78d).setScale(2);
        final BigDecimal amount3 = BigDecimal.valueOf(150.99d).setScale(2);

        final BigDecimal amount6 = BigDecimal.valueOf(50.99d).setScale(2);
        final BigDecimal amount7 = BigDecimal.valueOf(100.00d).setScale(2);
        final BigDecimal amount8 = BigDecimal.valueOf(5.21d).setScale(2);
        final BigDecimal amount9 = BigDecimal.valueOf(1.01d).setScale(2);

        final PaymentTransactionEntryModel paymentTransactionCapture1 = setUpPaymentTransactionEntry(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.REJECTED, null);

        final PaymentTransactionEntryModel paymentTransactionCapture2 = setUpPaymentTransactionEntry(
                paymentTransaction, amount2,
                PaymentTransactionType.CAPTURE, TransactionStatus.REVIEW, null);

        final PaymentTransactionEntryModel paymentTransactionCapture3 = setUpPaymentTransactionEntry(
                paymentTransaction, amount3,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, null);


        final PaymentTransactionEntryModel paymentTransactionRefund1 = setUpPaymentTransactionEntry(paymentTransaction,
                amount6,
                PaymentTransactionType.REFUND_STANDALONE, TransactionStatus.ACCEPTED, null);

        final PaymentTransactionEntryModel paymentTransactionRefund2 = setUpPaymentTransactionEntry(paymentTransaction,
                amount7,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.ACCEPTED, null);

        final PaymentTransactionEntryModel paymentTransactionRefund3 = setUpPaymentTransactionEntry(paymentTransaction,
                amount8,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.REVIEW, null);

        final PaymentTransactionEntryModel paymentTransactionRefund4 = setUpPaymentTransactionEntry(paymentTransaction,
                amount9,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.REVIEW, null);



        given(paymentTransaction.getEntries()).willReturn(
                Arrays.asList(paymentTransactionCapture1, paymentTransactionCapture2,
                        paymentTransactionCapture3, paymentTransactionRefund1,
                        paymentTransactionRefund2, paymentTransactionRefund3, paymentTransactionRefund4));
        final PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);
        given(paymentTransaction.getInfo()).willReturn(paymentInfoModel);

        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .findCaptureTransactionEntriesWithRefundableAmountForTns(paymentTransaction);
        assertThat(result).isEmpty();
    }

    @Test
    public void testFindCaptureTransactionEntriesWithRefundableAmountForTnsWithEmptyTransactionEntries() {

        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);

        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .findCaptureTransactionEntriesWithRefundableAmountForTns(paymentTransaction);
        assertThat(result).isEmpty();
    }

    @Test
    public void testSortCaptureTransactionEntriesByPaymentInfoWithEmptyList() {
        final List<PaymentTransactionEntryData> transactionEntryDataList = new ArrayList<>();
        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .sortCaptureTransactionEntriesByPaymentInfo(transactionEntryDataList);
        assertThat(result).isNull();
    }

    @Test
    public void testSortCaptureTransactionEntriesByPaymentInfoWithOneGiftcard() {

        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);

        //three credit card, one gift card
        final PaymentInfoModel paymentInfor1 = createPaymentInfoModel(1, true);
        final PaymentInfoModel paymentInfor2 = createPaymentInfoModel(2, false);
        final PaymentInfoModel paymentInfor3 = createPaymentInfoModel(3, false);
        final PaymentInfoModel paymentInfor4 = createPaymentInfoModel(4, false);

        final PaymentTransactionEntryData paymentTransactionCapture1 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor1, amount1);

        final PaymentTransactionEntryData paymentTransactionCapture2 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor2, amount1);
        final PaymentTransactionEntryData paymentTransactionCapture3 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor3, amount1);
        final PaymentTransactionEntryData paymentTransactionCapture4 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor4, amount1);

        final List<PaymentTransactionEntryData> transactionEntryDataList = new ArrayList<>();
        transactionEntryDataList.addAll(Arrays.asList(paymentTransactionCapture1, paymentTransactionCapture2,
                paymentTransactionCapture3, paymentTransactionCapture4));

        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .sortCaptureTransactionEntriesByPaymentInfo(transactionEntryDataList);

        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(0))).isFalse();
        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(1))).isFalse();
        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(2))).isFalse();
        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(3))).isTrue();
    }

    @Test
    public void testSortCaptureTransactionEntriesByPaymentInfoWithTwoGiftcard() {

        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);

        final PaymentInfoModel paymentInfor1 = createPaymentInfoModel(1, true);
        final PaymentInfoModel paymentInfor2 = createPaymentInfoModel(2, false);
        final PaymentInfoModel paymentInfor3 = createPaymentInfoModel(3, true);
        final PaymentInfoModel paymentInfor4 = createPaymentInfoModel(4, false);

        final PaymentTransactionEntryData paymentTransactionCapture1 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor1, amount1);

        final PaymentTransactionEntryData paymentTransactionCapture2 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor2, amount1);
        final PaymentTransactionEntryData paymentTransactionCapture3 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor3, amount1);
        final PaymentTransactionEntryData paymentTransactionCapture4 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor4, amount1);

        final List<PaymentTransactionEntryData> transactionEntryDataList = new ArrayList<>();
        transactionEntryDataList.addAll(Arrays.asList(paymentTransactionCapture1, paymentTransactionCapture2,
                paymentTransactionCapture3, paymentTransactionCapture4));

        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .sortCaptureTransactionEntriesByPaymentInfo(transactionEntryDataList);

        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(0))).isFalse();
        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(1))).isFalse();
        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(2))).isTrue();
        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(3))).isTrue();
    }

    @Test
    public void testSortCaptureTransactionEntriesByPaymentInfoWithThreeGiftcard() {

        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);

        final PaymentInfoModel paymentInfor1 = createPaymentInfoModel(1, true);
        final PaymentInfoModel paymentInfor2 = createPaymentInfoModel(2, false);
        final PaymentInfoModel paymentInfor3 = createPaymentInfoModel(3, true);
        final PaymentInfoModel paymentInfor4 = createPaymentInfoModel(4, true);

        final PaymentTransactionEntryData paymentTransactionCapture1 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor1, amount1);

        final PaymentTransactionEntryData paymentTransactionCapture2 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor2, amount1);
        final PaymentTransactionEntryData paymentTransactionCapture3 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor3, amount1);
        final PaymentTransactionEntryData paymentTransactionCapture4 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor4, amount1);

        final List<PaymentTransactionEntryData> transactionEntryDataList = new ArrayList<>();
        transactionEntryDataList.addAll(Arrays.asList(paymentTransactionCapture1, paymentTransactionCapture2,
                paymentTransactionCapture3, paymentTransactionCapture4));

        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .sortCaptureTransactionEntriesByPaymentInfo(transactionEntryDataList);

        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(0))).isFalse();
        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(1))).isTrue();
        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(2))).isTrue();
        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(3))).isTrue();
    }

    @Test
    public void testSortCaptureTransactionEntriesByPaymentInfoWithFourGiftcard() {

        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);

        final PaymentInfoModel paymentInfor1 = createPaymentInfoModel(1, true);
        final PaymentInfoModel paymentInfor2 = createPaymentInfoModel(2, true);
        final PaymentInfoModel paymentInfor3 = createPaymentInfoModel(3, true);
        final PaymentInfoModel paymentInfor4 = createPaymentInfoModel(4, true);

        final PaymentTransactionEntryData paymentTransactionCapture1 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor1, amount1);

        final PaymentTransactionEntryData paymentTransactionCapture2 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor2, amount1);
        final PaymentTransactionEntryData paymentTransactionCapture3 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor3, amount1);
        final PaymentTransactionEntryData paymentTransactionCapture4 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor4, amount1);

        final List<PaymentTransactionEntryData> transactionEntryDataList = new ArrayList<>();
        transactionEntryDataList.addAll(Arrays.asList(paymentTransactionCapture1, paymentTransactionCapture2,
                paymentTransactionCapture3, paymentTransactionCapture4));

        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .sortCaptureTransactionEntriesByPaymentInfo(transactionEntryDataList);

        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(0))).isTrue();
        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(1))).isTrue();
        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(2))).isTrue();
        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(3))).isTrue();
    }

    @Test
    public void testSortCaptureTransactionEntriesByPaymentInfoWithFourCreditcard() {

        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);

        final PaymentInfoModel paymentInfor1 = createPaymentInfoModel(1, false);
        final PaymentInfoModel paymentInfor2 = createPaymentInfoModel(2, false);
        final PaymentInfoModel paymentInfor3 = createPaymentInfoModel(3, false);
        final PaymentInfoModel paymentInfor4 = createPaymentInfoModel(4, false);

        final PaymentTransactionEntryData paymentTransactionCapture1 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor1, amount1);

        final PaymentTransactionEntryData paymentTransactionCapture2 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor2, amount1);
        final PaymentTransactionEntryData paymentTransactionCapture3 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor3, amount1);
        final PaymentTransactionEntryData paymentTransactionCapture4 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor4, amount1);

        final List<PaymentTransactionEntryData> transactionEntryDataList = new ArrayList<>();
        transactionEntryDataList.addAll(Arrays.asList(paymentTransactionCapture1, paymentTransactionCapture2,
                paymentTransactionCapture3, paymentTransactionCapture4));

        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .sortCaptureTransactionEntriesByPaymentInfo(transactionEntryDataList);

        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(0))).isFalse();
        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(1))).isFalse();
        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(2))).isFalse();
        assertThat(checkIfPaymentInfoInTransactionEntryDataIsGiftcard(result.get(3))).isFalse();
    }

    private boolean checkIfPaymentInfoInTransactionEntryDataIsGiftcard(final PaymentTransactionEntryData data) {
        return data.getPaymentTransactionEntryModel().getIpgPaymentInfo() instanceof IpgGiftCardPaymentInfoModel;
    }

    @Test
    public void testCreatePaymentTransactionEntryDataForRefundWithNullRefundablePaymentEntries() {
        final List<PaymentTransactionEntryData> refundablePaymentTransactionEntries = new ArrayList<>();
        final BigDecimal amount = BigDecimal.TEN;
        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .createPaymentTransactionEntryDataForRefund(refundablePaymentTransactionEntries, amount);
        assertThat(result).isEmpty();
    }

    @Test
    public void testCreatePaymentTransactionEntryDataForRefundWithNullTotalAmount() {
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);

        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);
        final BigDecimal amount2 = BigDecimal.valueOf(56.78d).setScale(2);
        final BigDecimal amount3 = BigDecimal.valueOf(23.99d).setScale(2);
        final BigDecimal amount4 = BigDecimal.valueOf(63.00d).setScale(2);

        final PaymentInfoModel paymentInfor1 = createPaymentInfoModel(1, false);
        final PaymentInfoModel paymentInfor2 = createPaymentInfoModel(2, false);
        final PaymentInfoModel paymentInfor3 = createPaymentInfoModel(3, false);
        final PaymentInfoModel paymentInfor4 = createPaymentInfoModel(4, false);

        final PaymentTransactionEntryData paymentTransactionCapture1 = setUpPaymentTransactionData(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor1, amount1);

        final PaymentTransactionEntryData paymentTransactionCapture2 = setUpPaymentTransactionData(
                paymentTransaction, amount2,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor2, amount2);

        final PaymentTransactionEntryData paymentTransactionCapture3 = setUpPaymentTransactionData(
                paymentTransaction, amount3,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor3, amount3);

        final PaymentTransactionEntryData paymentTransactionCapture4 = setUpPaymentTransactionData(
                paymentTransaction, amount4,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor4, amount4);


        final List<PaymentTransactionEntryData> refundablePaymentTransactionEntries = new ArrayList<PaymentTransactionEntryData>(
                Arrays.asList(paymentTransactionCapture1, paymentTransactionCapture2, paymentTransactionCapture3,
                        paymentTransactionCapture4));
        final BigDecimal amount = null;
        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .createPaymentTransactionEntryDataForRefund(refundablePaymentTransactionEntries, amount);
        assertThat(result).isEmpty();
    }

    @Test
    public void testCreatePaymentTransactionEntryDataForRefundWithFullRefundTheFirstEntry() {
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);

        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);

        final PaymentInfoModel paymentInfor1 = createPaymentInfoModel(1, false);

        final PaymentTransactionEntryModel paymentTransactionCapture1 = setUpPaymentTransactionEntry(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor1);
        final PaymentTransactionEntryData paymentTransactionCaptureData1 = new PaymentTransactionEntryData(
                paymentTransactionCapture1, amount1);

        final List<PaymentTransactionEntryData> refundablePaymentTransactionEntries = new ArrayList<PaymentTransactionEntryData>(
                Arrays.asList(paymentTransactionCaptureData1));
        final BigDecimal amount = amount1;
        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .createPaymentTransactionEntryDataForRefund(refundablePaymentTransactionEntries, amount);
        assertThat(result).hasSize(1);
        assertThat(result.get(0).getPaymentTransactionEntryModel()).isEqualTo(paymentTransactionCapture1);
        assertThat(result.get(0).getAmount()).isEqualTo(amount1);
    }

    @Test
    public void testCreatePaymentTransactionEntryDataForRefundWithPartialRefundTheFirstEntry() {
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);

        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);
        final BigDecimal amount2 = BigDecimal.valueOf(56.78d).setScale(2);
        final BigDecimal amount3 = BigDecimal.valueOf(23.99d).setScale(2);
        final BigDecimal amount4 = BigDecimal.valueOf(63.00d).setScale(2);

        final PaymentInfoModel paymentInfor1 = createPaymentInfoModel(1, false);
        final PaymentInfoModel paymentInfor2 = createPaymentInfoModel(2, false);
        final PaymentInfoModel paymentInfor3 = createPaymentInfoModel(3, false);
        final PaymentInfoModel paymentInfor4 = createPaymentInfoModel(4, false);

        final PaymentTransactionEntryModel paymentTransactionCapture1 = setUpPaymentTransactionEntry(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor1);
        final PaymentTransactionEntryData paymentTransactionCaptureData1 = new PaymentTransactionEntryData(
                paymentTransactionCapture1, amount1);

        final PaymentTransactionEntryModel paymentTransactionCapture2 = setUpPaymentTransactionEntry(
                paymentTransaction, amount2,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor2);
        final PaymentTransactionEntryData paymentTransactionCaptureData2 = new PaymentTransactionEntryData(
                paymentTransactionCapture2, amount2);

        final PaymentTransactionEntryModel paymentTransactionCapture3 = setUpPaymentTransactionEntry(
                paymentTransaction, amount3,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor3);
        final PaymentTransactionEntryData paymentTransactionCaptureData3 = new PaymentTransactionEntryData(
                paymentTransactionCapture3, amount3);

        final PaymentTransactionEntryModel paymentTransactionCapture4 = setUpPaymentTransactionEntry(
                paymentTransaction, amount4,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor4);
        final PaymentTransactionEntryData paymentTransactionCaptureData4 = new PaymentTransactionEntryData(
                paymentTransactionCapture4, amount4);


        final List<PaymentTransactionEntryData> refundablePaymentTransactionEntries = new ArrayList<PaymentTransactionEntryData>(
                Arrays.asList(paymentTransactionCaptureData1, paymentTransactionCaptureData2,
                        paymentTransactionCaptureData3,
                        paymentTransactionCaptureData4));
        final BigDecimal totalAmount = BigDecimal.valueOf(5.99d).setScale(2);
        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .createPaymentTransactionEntryDataForRefund(refundablePaymentTransactionEntries, totalAmount);
        assertThat(result).hasSize(1);
        assertThat(result.get(0).getPaymentTransactionEntryModel()).isEqualTo(paymentTransactionCapture1);
        assertThat(result.get(0).getAmount()).isEqualTo(totalAmount);
    }

    @Test
    public void testCreatePaymentTransactionEntryDataForRefundWithPartialRefundTheThreeEntries() {
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);

        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);
        final BigDecimal amount2 = BigDecimal.valueOf(56.78d).setScale(2);
        final BigDecimal amount3 = BigDecimal.valueOf(23.99d).setScale(2);
        final BigDecimal amount4 = BigDecimal.valueOf(63.00d).setScale(2);

        final PaymentInfoModel paymentInfor1 = createPaymentInfoModel(1, false);
        final PaymentInfoModel paymentInfor2 = createPaymentInfoModel(2, false);
        final PaymentInfoModel paymentInfor3 = createPaymentInfoModel(3, false);
        final PaymentInfoModel paymentInfor4 = createPaymentInfoModel(4, false);

        final PaymentTransactionEntryModel paymentTransactionCapture1 = setUpPaymentTransactionEntry(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor1);
        final PaymentTransactionEntryData paymentTransactionCaptureData1 = new PaymentTransactionEntryData(
                paymentTransactionCapture1, amount1);

        final PaymentTransactionEntryModel paymentTransactionCapture2 = setUpPaymentTransactionEntry(
                paymentTransaction, amount2,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor2);
        final PaymentTransactionEntryData paymentTransactionCaptureData2 = new PaymentTransactionEntryData(
                paymentTransactionCapture2, amount2);

        final PaymentTransactionEntryModel paymentTransactionCapture3 = setUpPaymentTransactionEntry(
                paymentTransaction, amount3,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor3);
        final PaymentTransactionEntryData paymentTransactionCaptureData3 = new PaymentTransactionEntryData(
                paymentTransactionCapture3, amount3);

        final PaymentTransactionEntryModel paymentTransactionCapture4 = setUpPaymentTransactionEntry(
                paymentTransaction, amount4,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor4);
        final PaymentTransactionEntryData paymentTransactionCaptureData4 = new PaymentTransactionEntryData(
                paymentTransactionCapture4, amount4);

        final List<PaymentTransactionEntryData> refundablePaymentTransactionEntries = new ArrayList<PaymentTransactionEntryData>(
                Arrays.asList(paymentTransactionCaptureData1, paymentTransactionCaptureData2,
                        paymentTransactionCaptureData3,
                        paymentTransactionCaptureData4));
        final BigDecimal totalAmount = BigDecimal.valueOf(79.00d).setScale(2);
        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .createPaymentTransactionEntryDataForRefund(refundablePaymentTransactionEntries, totalAmount);
        assertThat(result).hasSize(3);
        assertThat(result.get(0).getPaymentTransactionEntryModel()).isEqualTo(paymentTransactionCapture1);
        assertThat(result.get(0).getAmount()).isEqualTo(amount1);
        assertThat(result.get(1).getPaymentTransactionEntryModel()).isEqualTo(paymentTransactionCapture2);
        assertThat(result.get(1).getAmount()).isEqualTo(amount2);
        assertThat(result.get(2).getPaymentTransactionEntryModel()).isEqualTo(paymentTransactionCapture3);
        assertThat(result.get(2).getAmount()).isEqualTo(totalAmount.subtract(amount1).subtract(amount2));
    }

    @Test
    public void testCreatePaymentTransactionEntryDataForRefundWithFullRefundTheAllEntries() {
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);

        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);
        final BigDecimal amount2 = BigDecimal.valueOf(56.78d).setScale(2);
        final BigDecimal amount3 = BigDecimal.valueOf(23.99d).setScale(2);
        final BigDecimal amount4 = BigDecimal.valueOf(63.00d).setScale(2);

        final PaymentInfoModel paymentInfor1 = createPaymentInfoModel(1, false);
        final PaymentInfoModel paymentInfor2 = createPaymentInfoModel(2, false);
        final PaymentInfoModel paymentInfor3 = createPaymentInfoModel(3, false);
        final PaymentInfoModel paymentInfor4 = createPaymentInfoModel(4, false);

        final PaymentTransactionEntryModel paymentTransactionCapture1 = setUpPaymentTransactionEntry(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor1);
        final PaymentTransactionEntryData paymentTransactionCaptureData1 = new PaymentTransactionEntryData(
                paymentTransactionCapture1, amount1);

        final PaymentTransactionEntryModel paymentTransactionCapture2 = setUpPaymentTransactionEntry(
                paymentTransaction, amount2,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor2);
        final PaymentTransactionEntryData paymentTransactionCaptureData2 = new PaymentTransactionEntryData(
                paymentTransactionCapture2, amount2);

        final PaymentTransactionEntryModel paymentTransactionCapture3 = setUpPaymentTransactionEntry(
                paymentTransaction, amount3,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor3);
        final PaymentTransactionEntryData paymentTransactionCaptureData3 = new PaymentTransactionEntryData(
                paymentTransactionCapture3, amount3);

        final PaymentTransactionEntryModel paymentTransactionCapture4 = setUpPaymentTransactionEntry(
                paymentTransaction, amount4,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor4);
        final PaymentTransactionEntryData paymentTransactionCaptureData4 = new PaymentTransactionEntryData(
                paymentTransactionCapture4, amount4);

        final List<PaymentTransactionEntryData> refundablePaymentTransactionEntries = new ArrayList<PaymentTransactionEntryData>(
                Arrays.asList(paymentTransactionCaptureData1, paymentTransactionCaptureData2,
                        paymentTransactionCaptureData3,
                        paymentTransactionCaptureData4));
        final BigDecimal totalAmount = amount1.add(amount2).add(amount3).add(amount4);
        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .createPaymentTransactionEntryDataForRefund(refundablePaymentTransactionEntries, totalAmount);
        assertThat(result).hasSize(4);
        assertThat(result.get(0).getPaymentTransactionEntryModel()).isEqualTo(paymentTransactionCapture1);
        assertThat(result.get(0).getAmount()).isEqualTo(amount1);
        assertThat(result.get(1).getPaymentTransactionEntryModel()).isEqualTo(paymentTransactionCapture2);
        assertThat(result.get(1).getAmount()).isEqualTo(amount2);
        assertThat(result.get(2).getPaymentTransactionEntryModel()).isEqualTo(paymentTransactionCapture3);
        assertThat(result.get(2).getAmount()).isEqualTo(amount3);
        assertThat(result.get(3).getPaymentTransactionEntryModel()).isEqualTo(paymentTransactionCapture4);
        assertThat(result.get(3).getAmount()).isEqualTo(amount4);
    }

    @Test
    public void testCreatePaymentTransactionEntryDataForRefundWithTotalAmountExceedTheRefundableAmount() {
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);

        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);
        final BigDecimal amount2 = BigDecimal.valueOf(56.78d).setScale(2);
        final BigDecimal amount3 = BigDecimal.valueOf(23.99d).setScale(2);
        final BigDecimal amount4 = BigDecimal.valueOf(63.00d).setScale(2);

        final PaymentInfoModel paymentInfor1 = createPaymentInfoModel(1, false);
        final PaymentInfoModel paymentInfor2 = createPaymentInfoModel(2, false);
        final PaymentInfoModel paymentInfor3 = createPaymentInfoModel(3, false);
        final PaymentInfoModel paymentInfor4 = createPaymentInfoModel(4, false);

        final PaymentTransactionEntryModel paymentTransactionCapture1 = setUpPaymentTransactionEntry(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor1);
        final PaymentTransactionEntryData paymentTransactionCaptureData1 = new PaymentTransactionEntryData(
                paymentTransactionCapture1, amount1);

        final PaymentTransactionEntryModel paymentTransactionCapture2 = setUpPaymentTransactionEntry(
                paymentTransaction, amount2,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor2);
        final PaymentTransactionEntryData paymentTransactionCaptureData2 = new PaymentTransactionEntryData(
                paymentTransactionCapture2, amount2);

        final PaymentTransactionEntryModel paymentTransactionCapture3 = setUpPaymentTransactionEntry(
                paymentTransaction, amount3,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor3);
        final PaymentTransactionEntryData paymentTransactionCaptureData3 = new PaymentTransactionEntryData(
                paymentTransactionCapture3, amount3);

        final PaymentTransactionEntryModel paymentTransactionCapture4 = setUpPaymentTransactionEntry(
                paymentTransaction, amount4,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor4);
        final PaymentTransactionEntryData paymentTransactionCaptureData4 = new PaymentTransactionEntryData(
                paymentTransactionCapture4, amount4);

        final List<PaymentTransactionEntryData> refundablePaymentTransactionEntries = new ArrayList<PaymentTransactionEntryData>(
                Arrays.asList(paymentTransactionCaptureData1, paymentTransactionCaptureData2,
                        paymentTransactionCaptureData3,
                        paymentTransactionCaptureData4));
        //exceed 1 cent
        final BigDecimal totalAmount = BigDecimal.valueOf(156.12).setScale(2);
        try {
            PaymentTransactionHelper.createPaymentTransactionEntryDataForRefund(refundablePaymentTransactionEntries,
                    totalAmount);
            assertThat(false).as("should throw the PaymentException").isTrue();
        }
        catch (final PaymentException e) {
            assertThat(true).as("expect to throw the PaymentException").isTrue();
        }
    }

    @Test
    public void testCreatePaymentTransactionEntryDataWithEmptyAmount() {
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        final BigDecimal totalAmount = null;
        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .createPaymentTransactionEntryData(paymentTransaction, totalAmount);
        assertThat(result).isNull();
    }

    @Test
    public void testCreatePaymentTransactionEntryDataWithZeroAmount() {
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        final BigDecimal totalAmount = BigDecimal.ZERO;
        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .createPaymentTransactionEntryData(paymentTransaction, totalAmount);
        assertThat(result).isNull();
    }

    @Test
    public void testCreatePaymentTransactionEntryDataWithEmptyTransaction() {
        final PaymentTransactionModel paymentTransaction = null;
        final BigDecimal totalAmount = BigDecimal.valueOf(28.00d).setScale(2);
        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .createPaymentTransactionEntryData(paymentTransaction, totalAmount);
        assertThat(result).isNull();
    }

    //All other scenarios have been tested by the sub methods unit tests.
    @Test
    public void testCreatePaymentTransactionEntryData() {

        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);

        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);
        final BigDecimal amount2 = BigDecimal.valueOf(56.78d).setScale(2);
        final BigDecimal amount3 = BigDecimal.valueOf(23.99d).setScale(2);
        final BigDecimal amount4 = BigDecimal.valueOf(63.00d).setScale(2);
        final BigDecimal amount5 = BigDecimal.valueOf(14.21d).setScale(2);
        final BigDecimal amount6 = BigDecimal.valueOf(3.48d).setScale(2);
        final BigDecimal amount7 = BigDecimal.valueOf(63.00d).setScale(2);
        final BigDecimal amount8 = BigDecimal.valueOf(5.21d).setScale(2);
        final BigDecimal amount9 = BigDecimal.valueOf(1.01d).setScale(2);

        final PaymentInfoModel paymentInfor1 = createPaymentInfoModel(1, false);
        final PaymentInfoModel paymentInfor2 = createPaymentInfoModel(2, false);
        final PaymentInfoModel paymentInfor3 = createPaymentInfoModel(3, false);
        final PaymentInfoModel paymentInfor4 = createPaymentInfoModel(4, false);
        final PaymentInfoModel paymentInfor5 = createPaymentInfoModel(5, true);

        final PaymentTransactionEntryModel paymentTransactionCapture1 = setUpPaymentTransactionEntry(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.REJECTED, paymentInfor1);

        final PaymentTransactionEntryModel paymentTransactionCapture2 = setUpPaymentTransactionEntry(
                paymentTransaction, amount2,
                PaymentTransactionType.CAPTURE, TransactionStatus.REVIEW, paymentInfor2);

        final PaymentTransactionEntryModel paymentTransactionCapture3 = setUpPaymentTransactionEntry(
                paymentTransaction, amount3,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor3);

        final PaymentTransactionEntryModel paymentTransactionCapture4 = setUpPaymentTransactionEntry(
                paymentTransaction, amount4,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor4);

        final PaymentTransactionEntryModel paymentTransactionCapture5 = setUpPaymentTransactionEntry(
                paymentTransaction, amount5,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor5);

        final PaymentTransactionEntryModel paymentTransactionRefund1 = setUpPaymentTransactionEntry(paymentTransaction,
                amount6,
                PaymentTransactionType.REFUND_STANDALONE, TransactionStatus.ACCEPTED, paymentInfor3);

        final PaymentTransactionEntryModel paymentTransactionRefund2 = setUpPaymentTransactionEntry(paymentTransaction,
                amount7,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.ACCEPTED, paymentInfor4);

        final PaymentTransactionEntryModel paymentTransactionRefund3 = setUpPaymentTransactionEntry(paymentTransaction,
                amount8,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.REVIEW, paymentInfor5);

        final PaymentTransactionEntryModel paymentTransactionRefund4 = setUpPaymentTransactionEntry(paymentTransaction,
                amount9,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.REVIEW, paymentInfor5);


        given(paymentTransaction.getEntries()).willReturn(
                Arrays.asList(paymentTransactionCapture5, paymentTransactionCapture1, paymentTransactionCapture2,
                        paymentTransactionCapture3,
                        paymentTransactionCapture4, paymentTransactionRefund1,
                        paymentTransactionRefund2, paymentTransactionRefund3, paymentTransactionRefund4));
        final IpgPaymentInfoModel ipgPaymentInfoModel = mock(IpgPaymentInfoModel.class);
        given(paymentTransaction.getInfo()).willReturn(ipgPaymentInfoModel);

        final BigDecimal totalAmount = BigDecimal.valueOf(28.00d).setScale(2);
        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .createPaymentTransactionEntryData(paymentTransaction, totalAmount);
        assertThat(result).hasSize(1);
        //first credit
        assertThat(result.get(0).getPaymentTransactionEntryModel()).isEqualTo(paymentTransactionCapture3);
        assertThat(result.get(0).getAmount()).isEqualTo(amount3.subtract(amount6));
    }

    @Test
    public void testCreatePaymentTransactionEntryDataWithFullGiftcardReversal() {

        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);

        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);
        final BigDecimal amount2 = BigDecimal.valueOf(56.78d).setScale(2);
        final BigDecimal amount3 = BigDecimal.valueOf(23.99d).setScale(2);
        final BigDecimal amount4 = BigDecimal.valueOf(63.00d).setScale(2);
        final BigDecimal amount5 = BigDecimal.valueOf(7.49d).setScale(2);
        final BigDecimal amount6 = BigDecimal.valueOf(3.48d).setScale(2);
        final BigDecimal amount7 = BigDecimal.valueOf(63.00d).setScale(2);
        final BigDecimal amount8 = BigDecimal.valueOf(5.21d).setScale(2);
        final BigDecimal amount9 = BigDecimal.valueOf(1.01d).setScale(2);

        final PaymentInfoModel paymentInfor1 = createPaymentInfoModel(1, false);
        final PaymentInfoModel paymentInfor2 = createPaymentInfoModel(2, false);
        final PaymentInfoModel paymentInfor3 = createPaymentInfoModel(3, false);
        final PaymentInfoModel paymentInfor4 = createPaymentInfoModel(4, false);
        final PaymentInfoModel paymentInfor5 = createPaymentInfoModel(5, true);

        final PaymentTransactionEntryModel paymentTransactionCapture1 = setUpPaymentTransactionEntry(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.REJECTED, paymentInfor1);

        final PaymentTransactionEntryModel paymentTransactionCapture2 = setUpPaymentTransactionEntry(
                paymentTransaction, amount2,
                PaymentTransactionType.CAPTURE, TransactionStatus.REVIEW, paymentInfor2);

        final PaymentTransactionEntryModel paymentTransactionCapture3 = setUpPaymentTransactionEntry(
                paymentTransaction, amount3,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor3);

        final PaymentTransactionEntryModel paymentTransactionCapture4 = setUpPaymentTransactionEntry(
                paymentTransaction, amount4,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor4);

        final PaymentTransactionEntryModel paymentTransactionCapture5 = setUpPaymentTransactionEntry(
                paymentTransaction, amount5,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, paymentInfor5);

        final PaymentTransactionEntryModel paymentTransactionRefund1 = setUpPaymentTransactionEntry(paymentTransaction,
                amount6,
                PaymentTransactionType.REFUND_STANDALONE, TransactionStatus.ACCEPTED, paymentInfor3);

        final PaymentTransactionEntryModel paymentTransactionRefund2 = setUpPaymentTransactionEntry(paymentTransaction,
                amount7,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.ACCEPTED, paymentInfor4);

        final PaymentTransactionEntryModel paymentTransactionRefund3 = setUpPaymentTransactionEntry(paymentTransaction,
                amount8,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.REVIEW, paymentInfor5);

        final PaymentTransactionEntryModel paymentTransactionRefund4 = setUpPaymentTransactionEntry(paymentTransaction,
                amount9,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.REVIEW, paymentInfor5);


        given(paymentTransaction.getEntries()).willReturn(
                Arrays.asList(paymentTransactionCapture5, paymentTransactionCapture1, paymentTransactionCapture2,
                        paymentTransactionCapture3,
                        paymentTransactionCapture4, paymentTransactionRefund1,
                        paymentTransactionRefund2, paymentTransactionRefund3, paymentTransactionRefund4));
        final IpgPaymentInfoModel ipgPaymentInfoModel = mock(IpgPaymentInfoModel.class);
        given(paymentTransaction.getInfo()).willReturn(ipgPaymentInfoModel);

        final BigDecimal totalAmount = BigDecimal.valueOf(28.00d).setScale(2);
        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .createPaymentTransactionEntryData(paymentTransaction, totalAmount);
        assertThat(result).hasSize(2);
        //first credit
        assertThat(result.get(0).getPaymentTransactionEntryModel()).isEqualTo(paymentTransactionCapture3);
        assertThat(result.get(0).getAmount()).isEqualTo(amount3.subtract(amount6));
        //gift card
        assertThat(result.get(1).getPaymentTransactionEntryModel()).isEqualTo(paymentTransactionCapture5);
        assertThat(result.get(1).getAmount()).isEqualTo(amount5);
    }

    @Test
    public void testCreatePaymentTransactionEntryDataForTns() {
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        final PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);
        given(paymentTransaction.getInfo()).willReturn(paymentInfoModel);

        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);
        final PaymentTransactionEntryModel paymentTransactionCapture1 = setUpPaymentTransactionEntry(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.REJECTED, null);
        final BigDecimal amount2 = BigDecimal.valueOf(20.34d).setScale(2);
        final PaymentTransactionEntryModel paymentTransactionCapture2 = setUpPaymentTransactionEntry(
                paymentTransaction, amount2,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, null);
        final BigDecimal amount7 = BigDecimal.valueOf(10.99d).setScale(2);
        final PaymentTransactionEntryModel paymentTransactionCapture3 = setUpPaymentTransactionEntry(
                paymentTransaction, amount7,
                PaymentTransactionType.CAPTURE, TransactionStatus.REVIEW, null);
        final BigDecimal amount3 = BigDecimal.valueOf(2.34d).setScale(2);
        final PaymentTransactionEntryModel paymentTransactionRefund3 = setUpPaymentTransactionEntry(
                paymentTransaction, amount3,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.ACCEPTED, null);
        final BigDecimal amount4 = BigDecimal.valueOf(1.34d).setScale(2);
        final PaymentTransactionEntryModel paymentTransactionRefund4 = setUpPaymentTransactionEntry(
                paymentTransaction, amount4,
                PaymentTransactionType.REFUND_STANDALONE, TransactionStatus.ACCEPTED, null);
        final BigDecimal amount5 = BigDecimal.valueOf(1.14d).setScale(2);
        final PaymentTransactionEntryModel paymentTransactionRefund5 = setUpPaymentTransactionEntry(
                paymentTransaction, amount5,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.REJECTED, null);
        final BigDecimal amount6 = BigDecimal.valueOf(2.34d).setScale(2);
        final PaymentTransactionEntryModel paymentTransactionRefund6 = setUpPaymentTransactionEntry(
                paymentTransaction, amount6,
                PaymentTransactionType.REFUND_STANDALONE, TransactionStatus.REJECTED, null);
        given(paymentTransaction.getEntries()).willReturn(
                Arrays.asList(paymentTransactionCapture1, paymentTransactionCapture2, paymentTransactionCapture3,
                        paymentTransactionRefund3, paymentTransactionRefund4, paymentTransactionRefund5,
                        paymentTransactionRefund6));
        final BigDecimal totalAmount = BigDecimal.valueOf(15.12d).setScale(2);
        final List<PaymentTransactionEntryData> result = PaymentTransactionHelper
                .createPaymentTransactionEntryData(paymentTransaction, totalAmount);
        assertThat(result).hasSize(1);
        assertThat(result.get(0).getPaymentTransactionEntryModel()).isEqualTo(paymentTransactionCapture2);
        assertThat(result.get(0).getAmount()).isEqualTo(totalAmount);
    }

    @Test
    public void testCreatePaymentTransactionEntryDataForTnsOverTotalCaptured() {
        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        final PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);
        given(paymentTransaction.getInfo()).willReturn(paymentInfoModel);

        final BigDecimal amount1 = BigDecimal.valueOf(12.34d).setScale(2);
        final PaymentTransactionEntryModel paymentTransactionCapture1 = setUpPaymentTransactionEntry(
                paymentTransaction, amount1,
                PaymentTransactionType.CAPTURE, TransactionStatus.REJECTED, null);
        final BigDecimal amount2 = BigDecimal.valueOf(20.34d).setScale(2);
        final PaymentTransactionEntryModel paymentTransactionCapture2 = setUpPaymentTransactionEntry(
                paymentTransaction, amount2,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, null);
        final BigDecimal amount7 = BigDecimal.valueOf(10.99d).setScale(2);
        final PaymentTransactionEntryModel paymentTransactionCapture3 = setUpPaymentTransactionEntry(
                paymentTransaction, amount7,
                PaymentTransactionType.CAPTURE, TransactionStatus.REVIEW, null);
        final BigDecimal amount3 = BigDecimal.valueOf(2.34d).setScale(2);
        final PaymentTransactionEntryModel paymentTransactionRefund3 = setUpPaymentTransactionEntry(
                paymentTransaction, amount3,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.ACCEPTED, null);
        final BigDecimal amount4 = BigDecimal.valueOf(1.34d).setScale(2);
        final PaymentTransactionEntryModel paymentTransactionRefund4 = setUpPaymentTransactionEntry(
                paymentTransaction, amount4,
                PaymentTransactionType.REFUND_STANDALONE, TransactionStatus.ACCEPTED, null);
        final BigDecimal amount5 = BigDecimal.valueOf(1.14d).setScale(2);
        final PaymentTransactionEntryModel paymentTransactionRefund5 = setUpPaymentTransactionEntry(
                paymentTransaction, amount5,
                PaymentTransactionType.REFUND_FOLLOW_ON, TransactionStatus.REJECTED, null);
        final BigDecimal amount6 = BigDecimal.valueOf(2.34d).setScale(2);
        final PaymentTransactionEntryModel paymentTransactionRefund6 = setUpPaymentTransactionEntry(
                paymentTransaction, amount6,
                PaymentTransactionType.REFUND_STANDALONE, TransactionStatus.REJECTED, null);
        given(paymentTransaction.getEntries()).willReturn(
                Arrays.asList(paymentTransactionCapture1, paymentTransactionCapture2, paymentTransactionCapture3,
                        paymentTransactionRefund3, paymentTransactionRefund4, paymentTransactionRefund5,
                        paymentTransactionRefund6));
        final BigDecimal totalAmount = BigDecimal.valueOf(16.67d).setScale(2);
        try {
            PaymentTransactionHelper.createPaymentTransactionEntryData(paymentTransaction, totalAmount);
            assertThat(true).as("expected payment exception").isFalse();
        }
        catch (final PaymentException e) {
            assertThat(true).as("expected payment exception").isTrue();
        }


    }

    @Test
    public void testFindTransactionEntryWithState() {

        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        final PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);
        given(paymentTransaction.getInfo()).willReturn(paymentInfoModel);

        final BigDecimal amount2 = BigDecimal.valueOf(20.0d).setScale(2);
        final PaymentTransactionEntryModel paymentTransactionCapture2 = setUpPaymentTransactionEntry(
                paymentTransaction, amount2,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, null);

        final BigDecimal amount3 = BigDecimal.valueOf(220.0d).setScale(2);
        final PaymentTransactionEntryModel paymentTransactionCapture3 = setUpPaymentTransactionEntry(
                paymentTransaction, amount3,
                PaymentTransactionType.DEFERRED, TransactionStatus.REVIEW, null);

        given(paymentTransaction.getEntries()).willReturn(
                Arrays.asList(paymentTransactionCapture2, paymentTransactionCapture3));

        final List<PaymentTransactionModel> paymentTransactionList = new ArrayList<>();
        paymentTransactionList.add(paymentTransaction);
        final PaymentTransactionEntryModel txnEnryModel = PaymentTransactionHelper
                .findTransactionEntryWithState(paymentTransactionList, PaymentTransactionType.DEFERRED);

        assertThat(txnEnryModel).isNotNull();
        assertThat(txnEnryModel.getType()).isEqualTo(PaymentTransactionType.DEFERRED);
        assertThat(txnEnryModel.getAmount()).isEqualTo(paymentTransactionCapture3.getAmount());
    }

    @Test
    public void testFindTransactionEntryWithStateWhenNoDeferredEntry() {

        final PaymentTransactionModel paymentTransaction = mock(PaymentTransactionModel.class);
        final PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);
        given(paymentTransaction.getInfo()).willReturn(paymentInfoModel);

        final BigDecimal amount2 = BigDecimal.valueOf(20.0d).setScale(2);
        final PaymentTransactionEntryModel paymentTransactionCapture2 = setUpPaymentTransactionEntry(
                paymentTransaction, amount2,
                PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED, null);

        final BigDecimal amount3 = BigDecimal.valueOf(220.0d).setScale(2);
        final PaymentTransactionEntryModel paymentTransactionCapture3 = setUpPaymentTransactionEntry(
                paymentTransaction, amount3,
                PaymentTransactionType.CAPTURE, TransactionStatus.REVIEW, null);

        given(paymentTransaction.getEntries()).willReturn(
                Arrays.asList(paymentTransactionCapture2, paymentTransactionCapture3));

        final List<PaymentTransactionModel> paymentTransactionList = new ArrayList<>();
        paymentTransactionList.add(paymentTransaction);
        final PaymentTransactionEntryModel txnEnryModel = PaymentTransactionHelper
                .findTransactionEntryWithState(paymentTransactionList, PaymentTransactionType.DEFERRED);

        assertThat(txnEnryModel).isNull();
    }

    @Test
    public void testFindTransactionEntryWithStateWhenNoPaymentTransactionModel() {

        final PaymentTransactionEntryModel txnEnryModel = PaymentTransactionHelper
                .findTransactionEntryWithState(new ArrayList<PaymentTransactionModel>(),
                        PaymentTransactionType.DEFERRED);

        assertThat(txnEnryModel).isNull();
    }

}
/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.client.impl;

import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.net.URI;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtpaymentprovider.afterpay.client.TargetAfterpayClient;
import au.com.target.tgtpaymentprovider.afterpay.client.exception.TargetAfterpayClientException;
import au.com.target.tgtpaymentprovider.afterpay.data.request.CapturePaymentRequest;
import au.com.target.tgtpaymentprovider.afterpay.data.request.CreateOrderRequest;
import au.com.target.tgtpaymentprovider.afterpay.data.request.CreateRefundRequest;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CapturePaymentResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CreateOrderResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CreateRefundResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetConfigurationResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetOrderResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetPaymentResponse;
import au.com.target.tgtutility.logging.JsonLogger;


/**
 * @author bhuang3
 *
 */
public class TargetAfterpayClientImpl implements TargetAfterpayClient {

    private static final Logger LOG = Logger.getLogger(TargetAfterpayClientImpl.class);

    private static final String LOG_FORMAT = "PAYMENT-Afterpay: action={0}  order_cart_id={1} userPk={2}: {3}";

    private static final String ERROR = "error";

    private static final String GET_CONFIGURATION = "getConfiguration";

    private static final String EMPTY_RESPONSE = "empty json response";

    private static final String START_REST_CALL = "start";

    private static final String CHECK_AVAILABILITY = "ping";

    private static final String CREATE_ORDER = "createOrder";

    private static final String CAPTURE_PAYMENT = "capturePayment";

    private static final String GET_ORDER = "getOrder";
    private static final String GET_PAYMENT = "getPayment";

    private static final String CREATE_REFUND = "createRefund";

    private RestTemplate afterpayRestTemplate;

    private UserService userService;

    private String baseUrl;

    private String configurationUrl;

    private String pingUrl;

    private String orderUrl;

    private String capturePaymentUrl;

    private String getPaymentUrl;

    private String createRefundUrl;

    @Override
    public GetConfigurationResponse getConfiguration() throws TargetAfterpayClientException {
        GetConfigurationResponse[] getConfigurations = null;
        try {
            log(GET_CONFIGURATION, null, START_REST_CALL, null);
            getConfigurations = afterpayRestTemplate.getForObject(
                    URI.create(baseUrl + configurationUrl),
                    GetConfigurationResponse[].class);
        }
        catch (final HttpStatusCodeException e) {
            log(Level.ERROR, GET_CONFIGURATION, null, ERROR, e);
            JsonLogger.logJson(LOG, e.getResponseBodyAsString());
            throw new TargetAfterpayClientException(e);
        }
        catch (final Exception e) {
            log(Level.ERROR, GET_CONFIGURATION, null, ERROR, e);
            throw new TargetAfterpayClientException(e);
        }
        if (ArrayUtils.isNotEmpty(getConfigurations)) {
            JsonLogger.logJson(LOG, getConfigurations);
            return getConfigurations[0];
        }
        log(Level.ERROR, GET_CONFIGURATION, null, EMPTY_RESPONSE, null);
        return null;
    }

    @Override
    public boolean isAfterpayConnectionAvailable() {
        log(CHECK_AVAILABILITY, null, START_REST_CALL, null);
        try {
            final ResponseEntity<String> pingResponse = afterpayRestTemplate.exchange(baseUrl + pingUrl,
                    HttpMethod.GET,
                    null,
                    String.class);
            return pingResponse.getStatusCode().value() == 200 ? true : false;
        }
        catch (final Exception e) {
            // don't throw exception, just return false
            log(Level.ERROR, CHECK_AVAILABILITY, null, ERROR, e);
            return false;
        }

    }

    @Override
    public CreateOrderResponse createOrder(final CreateOrderRequest request) throws TargetAfterpayClientException {
        log(CREATE_ORDER, request != null ? request.getMerchantReference() : null, START_REST_CALL, null);
        try {
            final CreateOrderResponse createOrderResponse = afterpayRestTemplate.postForObject(baseUrl + orderUrl,
                    new HttpEntity<CreateOrderRequest>(request), CreateOrderResponse.class);
            JsonLogger.logJson(LOG, createOrderResponse);
            return createOrderResponse;
        }
        catch (final HttpStatusCodeException e) {
            log(Level.ERROR, CREATE_ORDER, request != null ? request.getMerchantReference() : null, ERROR, e);
            JsonLogger.logJson(LOG, e.getResponseBodyAsString());
            throw new TargetAfterpayClientException(e);
        }
        catch (final Exception e) {
            log(Level.ERROR, CREATE_ORDER, request != null ? request.getMerchantReference() : null, ERROR, e);
            throw new TargetAfterpayClientException(e);
        }
    }


    @Override
    public CapturePaymentResponse capturePayment(final CapturePaymentRequest request)
            throws TargetAfterpayClientException {
        log(CAPTURE_PAYMENT, request != null ? request.getMerchantReference() : null, START_REST_CALL, null);
        try {
            final ResponseEntity<CapturePaymentResponse> capturePaymentResponseEntity = afterpayRestTemplate
                    .postForEntity(
                            baseUrl + capturePaymentUrl,
                            new HttpEntity<CapturePaymentRequest>(request), CapturePaymentResponse.class);
            final CapturePaymentResponse capturePaymentResponse = capturePaymentResponseEntity.getBody();
            capturePaymentResponse.setHttpStatus(capturePaymentResponseEntity.getStatusCode());
            JsonLogger.logJson(LOG, capturePaymentResponse);
            return capturePaymentResponse;
        }
        catch (final HttpStatusCodeException httpException) {
            log(Level.ERROR, CAPTURE_PAYMENT, request != null ? request.getMerchantReference() : null, ERROR,
                    httpException);

            final CapturePaymentResponse capturePaymentResponse = new CapturePaymentResponse();
            capturePaymentResponse.setHttpStatus(httpException.getStatusCode());
            return capturePaymentResponse;
        }
        catch (final Exception e) {
            log(Level.ERROR, CAPTURE_PAYMENT, request != null ? request.getMerchantReference() : null, ERROR, e);
            throw new TargetAfterpayClientException(e);
        }
    }

    @Override
    public GetOrderResponse getOrder(final String afterpayToken) throws TargetAfterpayClientException {
        log(GET_ORDER, null, START_REST_CALL, null);

        try {
            final GetOrderResponse response = afterpayRestTemplate.getForObject(
                    URI.create(baseUrl + orderUrl + "/" + afterpayToken), GetOrderResponse.class);
            JsonLogger.logJson(LOG, response);
            return response;
        }
        catch (final HttpStatusCodeException e) {
            log(Level.ERROR, GET_ORDER, null, ERROR, e);
            JsonLogger.logJson(LOG, e.getResponseBodyAsString());
            throw new TargetAfterpayClientException(e);
        }
        catch (final Exception e) {
            log(Level.ERROR, GET_ORDER, null, ERROR, e);
            throw new TargetAfterpayClientException(e);
        }
    }

    @Override
    public GetPaymentResponse getPayment(final String afterpayToken) throws TargetAfterpayClientException {
        log(GET_PAYMENT, null, START_REST_CALL, null);

        try {
            final GetPaymentResponse response = afterpayRestTemplate.getForObject(
                    URI.create(baseUrl + getPaymentUrl + afterpayToken), GetPaymentResponse.class);
            JsonLogger.logJson(LOG, response);
            return response;
        }
        catch (final HttpStatusCodeException e) {
            // when payment detail not found afterpay is throwing not found exception and will be caught here
            log(Level.ERROR, GET_PAYMENT, null, ERROR, e);
            JsonLogger.logJson(LOG, e.getResponseBodyAsString());
            throw new TargetAfterpayClientException(e);
        }
        catch (final Exception e) {
            log(Level.ERROR, GET_PAYMENT, null, ERROR, e);
            throw new TargetAfterpayClientException(e);
        }
    }

    @Override
    public CreateRefundResponse createRefund(final String paymentId, final CreateRefundRequest request)
            throws TargetAfterpayClientException {
        log(CREATE_REFUND, null, START_REST_CALL, null);
        try {
            final Map<String, String> uriVariables = new HashMap<>();
            uriVariables.put("paymentId", paymentId);
            final CreateRefundResponse response = afterpayRestTemplate.postForObject(baseUrl + createRefundUrl,
                    new HttpEntity<CreateRefundRequest>(request), CreateRefundResponse.class, uriVariables);
            JsonLogger.logJson(LOG, response);
            return response;
        }
        catch (final HttpStatusCodeException e) {
            log(Level.ERROR, CREATE_REFUND, request != null ? request.getMerchantReference() : null, ERROR, e);
            JsonLogger.logJson(LOG, e.getResponseBodyAsString());
            throw new TargetAfterpayClientException(e);
        }
        catch (final Exception e) {
            log(Level.ERROR, CREATE_REFUND, request != null ? request.getMerchantReference() : null, ERROR, e);
            throw new TargetAfterpayClientException(e);
        }
    }

    /**
     * Log an info message using the afterpay log format
     * 
     * @param action
     * @param cartId
     * @param message
     * @param t
     */
    protected void log(final String action, final String cartId, final String message, final Throwable t) {
        log(Level.INFO, action, cartId, message, t);
    }

    /**
     * Log a message using the afterpay log format
     * 
     * @param level
     * @param action
     * @param cartId
     * @param message
     * @param t
     */
    protected void log(final Level level, final String action, final String cartId, final String message,
            final Throwable t) {
        PK userPk = null;
        final UserModel currentUser = userService.getCurrentUser();
        if (null != currentUser) {
            userPk = currentUser.getPk();
        }
        LOG.log(level, MessageFormat.format(LOG_FORMAT, action, cartId, userPk, message), t);
    }

    /**
     * @return the afterpayRestTemplate
     */
    protected RestTemplate getAfterpayRestTemplate() {
        return afterpayRestTemplate;
    }

    /**
     * @param afterpayRestTemplate
     *            the afterpayRestTemplate to set
     */
    @Required
    public void setAfterpayRestTemplate(final RestTemplate afterpayRestTemplate) {
        this.afterpayRestTemplate = afterpayRestTemplate;
    }

    /**
     * @param userService
     *            the userService to set
     */
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @param baseUrl
     *            the baseUrl to set
     */
    @Required
    public void setBaseUrl(final String baseUrl) {
        this.baseUrl = baseUrl;
    }

    /**
     * @param configurationUrl
     *            the configurationUrl to set
     */
    @Required
    public void setConfigurationUrl(final String configurationUrl) {
        this.configurationUrl = configurationUrl;
    }

    /**
     * @param pingUrl
     *            the pingUrl to set
     */
    @Required
    public void setPingUrl(final String pingUrl) {
        this.pingUrl = pingUrl;
    }

    /**
     * @param orderUrl
     *            the orderUrl to set
     */
    @Required
    public void setOrderUrl(final String orderUrl) {
        this.orderUrl = orderUrl;
    }


    /**
     * @param getPaymentUrl
     *            the getPaymentUrl to set
     */
    @Required
    public void setGetPaymentUrl(final String getPaymentUrl) {
        this.getPaymentUrl = getPaymentUrl;
    }


    /**
     * @param capturePaymentUrl
     *            the capturePaymentUrl to set
     */
    @Required
    public void setCapturePaymentUrl(final String capturePaymentUrl) {
        this.capturePaymentUrl = capturePaymentUrl;
    }

    /**
     * @param createRefundUrl
     *            the createRefundUrl to set
     */
    @Required
    public void setCreateRefundUrl(final String createRefundUrl) {
        this.createRefundUrl = createRefundUrl;
    }

}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.commands.impl;

import de.hybris.platform.payment.AdapterException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.dto.LineItem;
import au.com.target.tgtpaymentprovider.afterpay.client.exception.TargetAfterpayClientException;
import au.com.target.tgtpaymentprovider.afterpay.data.request.CreateOrderRequest;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CreateOrderResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.types.Consumer;
import au.com.target.tgtpaymentprovider.afterpay.data.types.Item;
import au.com.target.tgtpaymentprovider.afterpay.data.types.Merchant;


/**
 * @author mgazal
 *
 */
public class AfterpayCreateSubscriptionCommandImpl extends TargetAbstractAfterpayCommand
        implements TargetCreateSubscriptionCommand {


    @Override
    public TargetCreateSubscriptionResult perform(final TargetCreateSubscriptionRequest paramRequest) {
        if (paramRequest == null) {
            throw new IllegalArgumentException("TargetCreateSubscriptionRequest is null");
        }
        try {
            final CreateOrderResponse createOrderResponse = getTargetAfterpayClient()
                    .createOrder(translateRequest(paramRequest));
            return translateResponse(createOrderResponse);
        }
        catch (final TargetAfterpayClientException e) {
            throw new AdapterException(e);
        }
    }



    /**
     * @param paramRequest
     * @return {@link CreateOrderRequest}
     */
    private CreateOrderRequest translateRequest(final TargetCreateSubscriptionRequest paramRequest) {
        final CreateOrderRequest createOrderRequest = new CreateOrderRequest();
        if (paramRequest.getOrder() != null) {
            createOrderRequest.setTotalAmount(
                    getTargetAfterpayHelper().convertAmountToMoney(paramRequest.getOrder().getOrderTotalAmount()));
            if (paramRequest.getOrder().getCustomer() != null) {
                final Consumer consumer = new Consumer();
                consumer.setGivenNames(paramRequest.getOrder().getCustomer().getGivenNames());
                consumer.setSurname(paramRequest.getOrder().getCustomer().getSurname());
                consumer.setEmail(paramRequest.getOrder().getCustomer().getEmail());
                createOrderRequest.setConsumer(consumer);
            }
            createOrderRequest.setItems(convertItems(paramRequest.getOrder().getLineItems()));
        }
        final Merchant merchant = new Merchant();
        merchant.setRedirectConfirmUrl(paramRequest.getReturnUrl());
        merchant.setRedirectCancelUrl(paramRequest.getCancelUrl());
        createOrderRequest.setMerchant(merchant);

        return createOrderRequest;
    }

    /**
     * @param lineItems
     * @return List of {@link Item}
     */
    private List<Item> convertItems(final List<LineItem> lineItems) {
        List<Item> items = null;
        Item item;
        if (CollectionUtils.isNotEmpty(lineItems)) {
            items = new ArrayList<>(lineItems.size());
            for (final LineItem lineItem : lineItems) {
                if (lineItem.getPrice() == null || lineItem.getPrice().getAmount() == null
                        || BigDecimal.ZERO.compareTo(lineItem.getPrice().getAmount()) > 0) {
                    continue;
                }
                item = new Item();
                item.setName(lineItem.getName());
                item.setPrice(getTargetAfterpayHelper().convertAmountToMoney(lineItem.getPrice()));
                item.setQuantity(lineItem.getQuantity());
                item.setSku(lineItem.getProductCode());
                items.add(item);
            }
        }
        return items;
    }

    /**
     * @param createOrderResponse
     * @return {@link TargetCreateSubscriptionResult}
     */
    private TargetCreateSubscriptionResult translateResponse(final CreateOrderResponse createOrderResponse) {
        final TargetCreateSubscriptionResult createSubscriptionResult = new TargetCreateSubscriptionResult();
        createSubscriptionResult.setRequestToken(createOrderResponse.getToken());
        return createSubscriptionResult;
    }

}

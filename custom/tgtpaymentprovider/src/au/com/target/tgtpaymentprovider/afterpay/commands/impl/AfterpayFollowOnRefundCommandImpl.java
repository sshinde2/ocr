/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.commands.impl;

import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.TransactionStatus;

import java.math.BigDecimal;

import org.springframework.http.HttpStatus;

import au.com.target.tgtpayment.commands.TargetFollowOnRefundCommand;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpaymentprovider.afterpay.client.exception.TargetAfterpayClientException;
import au.com.target.tgtpaymentprovider.afterpay.data.request.CreateRefundRequest;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CreateRefundResponse;


/**
 * @author mgazal
 *
 */
public class AfterpayFollowOnRefundCommandImpl extends TargetAbstractAfterpayCommand
        implements TargetFollowOnRefundCommand {

    @Override
    public TargetFollowOnRefundResult perform(final TargetFollowOnRefundRequest paramRequest) {
        if (paramRequest == null) {
            throw new IllegalArgumentException("TargetFollowOnRefundRequest is null");
        }
        try {
            final CreateRefundResponse createRefundResponse = getTargetAfterpayClient().createRefund(
                    paramRequest.getCaptureTransactionId(), translateRequest(paramRequest));
            return translateResponse(createRefundResponse);
        }
        catch (final TargetAfterpayClientException e) {
            throw new AdapterException(e);
        }
    }

    /**
     * @param paramRequest
     * @return {@link CreateRefundRequest}
     */
    private CreateRefundRequest translateRequest(final TargetFollowOnRefundRequest paramRequest) {
        final CreateRefundRequest refundRequest = new CreateRefundRequest();
        refundRequest.setRequestId(paramRequest.getOrderId());
        refundRequest.setAmount(getTargetAfterpayHelper()
                .convertBigDecimalAndCurrencyToMoney(paramRequest.getTotalAmount(), paramRequest.getCurrency()));
        return refundRequest;
    }

    /**
     * @param createRefundResponse
     * @return {@link TargetFollowOnRefundResult}
     */
    private TargetFollowOnRefundResult translateResponse(final CreateRefundResponse createRefundResponse) {
        final TargetFollowOnRefundResult refundResult = new TargetFollowOnRefundResult();
        refundResult.setRequestId(createRefundResponse.getRequestId());
        refundResult.setReconciliationId(createRefundResponse.getRefundId());
        if (createRefundResponse.getAmount() != null) {
            refundResult.setTotalAmount(new BigDecimal(createRefundResponse.getAmount().getAmount()));
        }
        final TransactionStatus transactionStatus;
        if (createRefundResponse.getErrorStatus() == null) {
            transactionStatus = TransactionStatus.ACCEPTED;
        }
        else if (HttpStatus.PRECONDITION_FAILED.equals(createRefundResponse.getErrorStatus())
                || HttpStatus.UNPROCESSABLE_ENTITY.equals(createRefundResponse.getErrorStatus())) {
            transactionStatus = TransactionStatus.REJECTED;
        }
        else {
            transactionStatus = TransactionStatus.ERROR;
        }

        refundResult.setTransactionStatus(transactionStatus);
        return refundResult;
    }
}

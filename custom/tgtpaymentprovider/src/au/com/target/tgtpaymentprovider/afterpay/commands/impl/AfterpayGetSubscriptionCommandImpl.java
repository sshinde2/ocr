/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.commands.impl;

import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.GetSubscriptionDataCommand;
import de.hybris.platform.payment.commands.request.SubscriptionDataRequest;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtpayment.commands.result.TargetGetSubscriptionResult;
import au.com.target.tgtpayment.dto.LineItem;
import au.com.target.tgtpaymentprovider.afterpay.client.exception.TargetAfterpayClientException;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetOrderResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.types.Item;


/**
 * @author gsing236
 *
 */
public class AfterpayGetSubscriptionCommandImpl extends TargetAbstractAfterpayCommand
        implements GetSubscriptionDataCommand {

    @Override
    public TargetGetSubscriptionResult perform(final SubscriptionDataRequest request) {
        if (request == null) {
            throw new IllegalArgumentException("SubscriptionDataRequest is null");
        }
        // afterpay token
        final String afterpayToken = request.getSubscriptionID();
        try {
            final GetOrderResponse response = getTargetAfterpayClient().getOrder(afterpayToken);

            return translateResponse(response);
        }
        catch (final TargetAfterpayClientException e) {
            throw new AdapterException(e);
        }
    }

    private TargetGetSubscriptionResult translateResponse(final GetOrderResponse response) {
        final TargetGetSubscriptionResult result = new TargetGetSubscriptionResult();

        result.setSubscriptionID(response.getToken());
        result.setAmount(getTargetAfterpayHelper().convertMoneyToBigDecimal(response.getTotalAmount()));
        result.setLineItems(convertItemsToLineItems(response.getItems()));
        result.setAfterpayToken(response.getToken());
        return result;
    }

    private List<LineItem> convertItemsToLineItems(final List<Item> items) {
        List<LineItem> lineItems = null;

        if (CollectionUtils.isNotEmpty(items)) {
            lineItems = new ArrayList<>();
            for (final Item item : items) {
                final LineItem lineItem = new LineItem();
                lineItem.setName(item.getName());
                lineItem.setProductCode(item.getSku());
                lineItem.setQuantity(item.getQuantity());
                lineItem.setPrice(getTargetAfterpayHelper().convertMoneyToAmount(item.getPrice()));
                lineItems.add(lineItem);
            }
        }
        return lineItems;
    }
}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.commands.impl;

import de.hybris.platform.payment.AdapterException;

import au.com.target.tgtpayment.commands.TargetRetrieveTransactionCommand;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;
import au.com.target.tgtpaymentprovider.afterpay.client.exception.TargetAfterpayClientException;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetPaymentResponse;


/**
 * @author gsing236
 *
 */
public class AfterpayRetrieveTransactionCommandImpl extends TargetAbstractAfterpayCommand
        implements TargetRetrieveTransactionCommand {

    @Override
    public TargetRetrieveTransactionResult perform(final TargetRetrieveTransactionRequest paramRequest) {

        if (paramRequest == null) {
            throw new IllegalArgumentException("TargetRetrieveTransactionRequest is null");
        }
        // afterpay token
        final String afterpayToken = paramRequest.getSessionToken();
        try {
            final GetPaymentResponse getPaymentResponse = getTargetAfterpayClient().getPayment(afterpayToken);
            return translateResult(getPaymentResponse);
        }
        catch (final TargetAfterpayClientException e) {
            throw new AdapterException(e);
        }
    }

    private TargetRetrieveTransactionResult translateResult(final GetPaymentResponse getPaymentResponse) {
        TargetRetrieveTransactionResult result = null;
        if (getPaymentResponse != null) {
            result = new TargetRetrieveTransactionResult();
            mapPaymentResponse(getPaymentResponse, result);
        }
        return result;
    }
}

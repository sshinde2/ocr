/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.commands.impl;

import de.hybris.platform.payment.AdapterException;

import au.com.target.tgtpayment.commands.TargetGetPaymentConfigurationCommand;
import au.com.target.tgtpayment.commands.request.TargetGetPaymentConfigurationRequest;
import au.com.target.tgtpayment.commands.result.TargetGetPaymentConfigurationResult;
import au.com.target.tgtpaymentprovider.afterpay.client.exception.TargetAfterpayClientException;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetConfigurationResponse;


/**
 * @author bhuang3
 *
 */
public class TargetAfterpayGetConfigurationCommandImpl extends TargetAbstractAfterpayCommand implements
        TargetGetPaymentConfigurationCommand {


    @Override
    public TargetGetPaymentConfigurationResult perform(
            final TargetGetPaymentConfigurationRequest paramRequest) {
        TargetGetPaymentConfigurationResult result = null;
        try {
            final GetConfigurationResponse configuration = getTargetAfterpayClient().getConfiguration();
            if (configuration != null) {
                result = new TargetGetPaymentConfigurationResult();
                result.setDescription(configuration.getDescription());
                result.setType(configuration.getType());
                result.setMaximumAmount(
                        getTargetAfterpayHelper().convertMoneyToBigDecimal(configuration.getMaximumAmount()));
                result.setMinimumAmount(
                        getTargetAfterpayHelper().convertMoneyToBigDecimal(configuration.getMinimumAmount()));
            }
        }
        catch (final TargetAfterpayClientException e) {
            throw new AdapterException(e);

        }
        return result;
    }


}

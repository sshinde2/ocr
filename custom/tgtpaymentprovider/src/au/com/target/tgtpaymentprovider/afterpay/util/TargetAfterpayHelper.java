/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.util;

import de.hybris.platform.payment.AdapterException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.format.ISODateTimeFormat;

import au.com.target.tgtpayment.dto.AmountType;
import au.com.target.tgtpaymentprovider.afterpay.data.types.Money;


/**
 * @author bhuang3
 *
 */
public class TargetAfterpayHelper {

    private static final Logger LOG = Logger.getLogger(TargetAfterpayHelper.class);


    public TargetAfterpayHelper() {
        //
    }

    /**
     * convert the money to BigDecimal
     * 
     * @param money
     * @return BigDecimal amount
     */
    public BigDecimal convertMoneyToBigDecimal(final Money money) {
        BigDecimal amount = null;
        if (money != null && StringUtils.isNotEmpty(money.getAmount())) {
            try {
                amount = new BigDecimal(money.getAmount()).setScale(2, BigDecimal.ROUND_HALF_UP);
            }
            catch (final NumberFormatException e) {
                LOG.error("Afterpay error: cannot parse Money to BigDecimal");
                throw new AdapterException(e);
            }
        }
        return amount;
    }

    /**
     * Convert the amountType to money
     * 
     * @param amount
     * @return {@link Money}
     */
    public Money convertAmountToMoney(final AmountType amount) {
        final Money money;
        if (amount != null && amount.getAmount() != null && amount.getCurrency() != null) {
            money = new Money();
            money.setAmount(amount.getAmount().setScale(2, RoundingMode.HALF_UP).toString());
            money.setCurrency(amount.getCurrency().getCurrencyCode());
            return money;
        }
        else {
            LOG.error("Afterpay error: amount is null or empty");
            throw new AdapterException(new IllegalArgumentException("amount is null or empty"));
        }
    }


    public Date convertStringToDate(final String created) {
        Date date = null;
        if (created != null && StringUtils.isNotEmpty(created)) {
            try {
                date = ISODateTimeFormat.dateTimeParser().parseDateTime(created).toDate();

            }
            catch (final Exception e) {
                LOG.error("Afterpay error: created date parse error");
                throw new AdapterException(e);
            }
        }
        return date;
    }


    /**
     * convert the money to Currency
     * 
     * @param money
     * @return BigDecimal amount
     */
    public Currency getCurrencyInstanceFromMoney(final Money money) {
        Currency currency = null;
        if (money != null && money.getCurrency() != null) {

            currency = Currency.getInstance(money.getCurrency());
        }
        return currency;
    }

    public AmountType convertMoneyToAmount(final Money money) {
        final AmountType amount;
        if (money != null && money.getAmount() != null && money.getCurrency() != null) {
            amount = new AmountType();
            amount.setAmount(convertMoneyToBigDecimal(money));
            final Currency currency = Currency.getInstance(money.getCurrency());
            amount.setCurrency(currency);
            return amount;
        }
        else {
            LOG.error("Afterpay error: money is null or empty");
            throw new AdapterException(new IllegalArgumentException("money is null or empty"));
        }
    }

    /**
     * convert amount in BigDecimal and Currency to Money
     * 
     * @param amount
     * @param currency
     * @return {@link Money}
     */
    public Money convertBigDecimalAndCurrencyToMoney(final BigDecimal amount, final Currency currency) {
        return convertAmountToMoney(new AmountType(amount, currency));
    }
}

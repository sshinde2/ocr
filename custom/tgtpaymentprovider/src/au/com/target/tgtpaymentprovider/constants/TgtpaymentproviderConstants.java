/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtpaymentprovider.constants;

/**
 * Global class for all Tgtpaymentprovider constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class TgtpaymentproviderConstants extends GeneratedTgtpaymentproviderConstants {
    public static final String EXTENSIONNAME = "tgtpaymentprovider";

    public static final boolean PAYPAL_TRUST_ALL = true;
    public static final int PAYPAL_TIMEOUT = 30000;
    public static final String PAYPAL_DOMAIN = "paypal.com";

    //Error strings
    public static final String ERR_TNS_TIMEOUT = "ERR-TNS-TIMEOUT";
    public static final String ERR_TNS_VALIDATION = "ERR-TNS-VALIDATION";
    public static final String ERR_TNS_REQUEST_REJECTED = "ERR-TNS-REQUEST-REJECTED";
    public static final String ERR_TNS_SERVER_BUSY = "ERR-TNS-SERVER-BUSY";
    public static final String ERR_TNS_SERVER_FAILED = "ERR-TNS-SERVER-FAILED";

    public static final String ERR_PAYPAL_VALIDATION = "ERR-PAYPAL-VALIDATION";
    public static final String ERR_PAYPAL_APIUNSUPPORTED = "ERR-PAYPAL-APIUNSUPPORTED";
    public static final String ERR_PAYPAL_SERVER_FAILED = "ERR-PAYPAL-SERVER-FAILED";
    public static final String ERR_PAYPAL_INVALIDREQUEST = "ERR-PAYPAL-INVALIDREQUEST";

    public static final String CURRENCY_AU = "AUD";
    public static final String SKU = "SKU";
    public static final String ZIPPAY_REDIRECT_URL = "/payment-details/zippay-return/spc";
    public static final String ZIPPAY_DISCOUNT = "discount";
    public static final String ZIPPAY_SHIPPING = "shipping";
    public static final String ZIPPAY_REFUND = "zip refund";

    /**
     * 
     */
    private TgtpaymentproviderConstants() {
        //empty to avoid instantiating this constant class
    }

    //Constants for IPG payment gateway
    public interface IPGRequestAttributes {
        String HEADER_CONTENT_TYPE = "Content-Type";
        String HEADER_CONTENT_TYPE_FORM_URLENCODED = "application/x-www-form-urlencoded";
        String USERNAME = "UserName";
        String PASSWD = "Password";
        String ACCOUNTNUMBER = "AccountNumber";
        String SESSIONID = "SessionID";
        String SESSIONKEY = "SessionKey";
        String DIRECTLINK = "DL";
        String URL = "UserURL";
        String AMOUNT = "Amount";
        String CUSTREF = "CustRef";
        String CUSTOMER_NUMBER = "CustNumber";
        String POS_CONDITION = "POSCond";
        String SAVED_CARDS = "SavedCards";
        String STORE_ID = "StoreID";
        String MERCHANT_NUMBER = "MerchantNumber";
        String QUERY = "Query";
        String STRING_TRUE = "True";

        //IPG response attributes
        String SESSIONTOKEN = "SST";
        String SESSIONSTORED = "SessionStored";
        String SESSION_STORED_ERROR = "SessionStoredError";
        String TRANSACTION_RESULT = "TrnResult";
        String CARD_RESULTS = "CardResults";
        String QUERY_RESULT = "QueryResult";
        String QUERY_ERROR = "QueryError";
        String SESSION_ID = "SessionId";

    }

    //Constants for Zip payment
	public interface ZipPayment {
    	String HEADER_IDEMPOTENCY_KEY = "Idempotency-Key";
	}
    // implement here constants used by this extension
}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.client;

import au.com.target.tgtpaymentprovider.ipg.data.request.SingleVoidRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSinglePaymentRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSingleRefundRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.TokenizeRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.TransactionQueryRequest;
import au.com.target.tgtpaymentprovider.ipg.data.response.SessionResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.SingleVoidResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.SubmitSinglePaymentResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.SubmitSingleRefundResponse;
import au.com.target.tgtpaymentprovider.ipg.data.response.TransactionsResultResponse;
import au.com.target.tgtpaymentprovider.ipg.data.soap.QueryResponse;
import au.com.target.tgtpaymentprovider.ipg.data.soap.QueryTransaction;
import au.com.target.tgtpaymentprovider.ipg.exception.IPGClientException;


/**
 * @author mjanarth
 *
 */
public interface IPGClient {

    /**
     * Post the request to IPG with the information and returns the sessionToken
     * 
     * @return SessionResponse
     */
    public SessionResponse requestIPGSessionToken(TokenizeRequest request) throws IPGClientException;

    /**
     * Post the request to IPG to get the transaction Results
     * 
     * @param request
     * @return TransactionsResultResponse
     * @throws IPGClientException
     */
    public TransactionsResultResponse getTransactionResults(TransactionQueryRequest request) throws IPGClientException;

    /**
     * Submits the request to IPG to do the payment capture
     * 
     * @param request
     * @return TransactionsResultResponse
     * @throws IPGClientException
     */
    public SubmitSinglePaymentResponse submitSinglePayment(SubmitSinglePaymentRequest request)
            throws IPGClientException;

    /**
     * Submits the request to IPG to do the refund
     * 
     * @param request
     * @return SubmitSingleRefundResponse
     * @throws IPGClientException
     */
    public SubmitSingleRefundResponse submitSingleRefund(
            SubmitSingleRefundRequest request)
            throws IPGClientException;

    /**
     * Submits the request to void gift card payment
     * 
     * @param request
     * @return single void response
     * @throws IPGClientException
     */
    public SingleVoidResponse submitSingleVoid(SingleVoidRequest request) throws IPGClientException;

    /**
     * Query transactions based on criteria
     * 
     * @param queryRequest
     * @return queryResponse
     * @throws IPGClientException
     */
    public QueryResponse queryTransaction(QueryTransaction queryRequest) throws IPGClientException;

}
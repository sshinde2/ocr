/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.commands.impl;

import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtpaymentprovider.ipg.service.IpgService;


/**
 * Abstract class to be extended by all IPG related commands
 */
public class AbstractIpgCommand {

    private IpgService ipgService;
    private TargetOrderService targetOrderService;

    /**
     * @return the ipgService
     */
    public IpgService getIpgService() {
        return ipgService;
    }

    /**
     * @param ipgService
     *            the ipgService to set
     */
    public void setIpgService(final IpgService ipgService) {
        this.ipgService = ipgService;
    }

    /**
     * @return the targetOrderService
     */
    public TargetOrderService getTargetOrderService() {
        return targetOrderService;
    }

    /**
     * @param targetOrderService
     *            the targetOrderService to set
     */
    public void setTargetOrderService(final TargetOrderService targetOrderService) {
        this.targetOrderService = targetOrderService;
    }
}
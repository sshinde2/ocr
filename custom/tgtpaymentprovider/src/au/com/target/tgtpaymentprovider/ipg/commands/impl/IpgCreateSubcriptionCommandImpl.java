/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.commands.impl;

import de.hybris.platform.payment.AdapterException;

import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpaymentprovider.ipg.data.response.SessionResponse;
import au.com.target.tgtpaymentprovider.ipg.exception.IpgServiceException;


/**
 * Create subscription command for IPG
 */
public class IpgCreateSubcriptionCommandImpl extends AbstractIpgCommand implements TargetCreateSubscriptionCommand {
    @Override
    public TargetCreateSubscriptionResult perform(final TargetCreateSubscriptionRequest paramRequest) {
        final TargetCreateSubscriptionResult result;
        try {
            final SessionResponse response = getIpgService().createSession(paramRequest);

            result = mapResponse(response);
        }
        catch (final IpgServiceException ex) {
            throw new AdapterException(ex);
        }
        return result;
    }

    private TargetCreateSubscriptionResult mapResponse(final SessionResponse response) {
        final TargetCreateSubscriptionResult result = new TargetCreateSubscriptionResult();
        if (response != null) {
            result.setRequestToken(response.getSecureSessionToken());
        }
        return result;
    }
}
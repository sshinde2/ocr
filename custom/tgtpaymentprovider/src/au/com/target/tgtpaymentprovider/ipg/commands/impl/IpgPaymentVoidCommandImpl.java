package au.com.target.tgtpaymentprovider.ipg.commands.impl;

import de.hybris.platform.payment.AdapterException;

import au.com.target.tgtpayment.commands.TargetPaymentVoidCommand;
import au.com.target.tgtpayment.commands.request.TargetPaymentVoidRequest;
import au.com.target.tgtpayment.commands.result.TargetCardPaymentResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentVoidResult;
import au.com.target.tgtpaymentprovider.ipg.exception.IpgServiceException;


/**
 * Void (reverse) payment on IPG system. Used for gift card reversal.
 *
 */
public class IpgPaymentVoidCommandImpl extends AbstractIpgCommand implements TargetPaymentVoidCommand {

    @Override
    public TargetPaymentVoidResult perform(final TargetPaymentVoidRequest paramRequest) {
        TargetPaymentVoidResult voidResult = null;
        try {
            final TargetCardPaymentResult result = getIpgService().submitSingleVoid(paramRequest);
            voidResult = mapResponse(result);
        }
        catch (final IpgServiceException e) {
            throw new AdapterException(e);
        }
        return voidResult;
    }

    /**
     * Map payment void result
     * 
     * @param result
     * @return target payment void result
     */
    protected TargetPaymentVoidResult mapResponse(final TargetCardPaymentResult result) {
        TargetPaymentVoidResult paymentVoidResult = null;
        if (result != null) {
            paymentVoidResult = new TargetPaymentVoidResult();
            paymentVoidResult.setVoidResult(result);
        }
        return paymentVoidResult;
    }
}
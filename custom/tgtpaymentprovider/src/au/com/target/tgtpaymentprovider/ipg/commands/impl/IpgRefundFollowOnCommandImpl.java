/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.commands.impl;

import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtpayment.commands.TargetFollowOnRefundCommand;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.result.TargetCardPaymentResult;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpaymentprovider.ipg.exception.IpgServiceException;
import au.com.target.tgtpaymentprovider.ipg.util.IpgToHybrisResponseMap;


/**
 * @author mjanarth
 *
 */
public class IpgRefundFollowOnCommandImpl extends AbstractIpgCommand implements TargetFollowOnRefundCommand {

    /* (non-Javadoc)
     * @see de.hybris.platform.payment.commands.Command#perform(java.lang.Object)
     */
    @Override
    public TargetFollowOnRefundResult perform(final TargetFollowOnRefundRequest paramRequest) {
        TargetFollowOnRefundResult refundFollowonResult = null;
        try {
            if (null != paramRequest) {
                refundFollowonResult = new TargetFollowOnRefundResult();
                final TargetCardPaymentResult refundResult = getIpgService().submitSingleRefund(paramRequest);
                refundFollowonResult.setRefundResult(refundResult);
                final String declineCode = refundResult.getDeclinedCode();
                final TransactionStatusDetails transactionStatusDetails = IpgToHybrisResponseMap
                        .getTransactionStatusDetails(declineCode);
                //Handling only failed scenario
                if (StringUtils.isNotEmpty(declineCode)) {
                    if (null != transactionStatusDetails) {
                        refundFollowonResult.setTransactionStatusDetails(transactionStatusDetails);
                    }
                    else {
                        refundFollowonResult.setTransactionStatusDetails(TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
                    }
                    refundFollowonResult.setDeclineCode(declineCode);
                    refundFollowonResult.setDeclineMessage(refundResult.getDeclinedMessage());
                    //for failure scenario capture the receipt number from request
                    refundFollowonResult.setReconciliationId(paramRequest.getReceiptNumber());
                }
                //if the decline code is empty or null,then the transaction status is successful
                else {
                    refundFollowonResult.setTransactionStatusDetails(transactionStatusDetails);
                    //for success set the receipt number from response
                    refundFollowonResult.setReconciliationId(refundResult.getReceiptNumber());
                }
                refundFollowonResult.setTransactionStatus(IpgToHybrisResponseMap.getTransactionStatus(refundResult
                        .getResponseCode()
                        ));
            }
        }
        catch (final IpgServiceException e) {
            throw new AdapterException(e);
        }
        return refundFollowonResult;
    }
}
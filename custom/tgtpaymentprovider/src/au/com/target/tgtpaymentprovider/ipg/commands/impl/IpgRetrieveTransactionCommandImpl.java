package au.com.target.tgtpaymentprovider.ipg.commands.impl;

import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtpayment.commands.TargetRetrieveTransactionCommand;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest.TrnType;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;
import au.com.target.tgtpayment.dto.PaymentTransactionEntryData;
import au.com.target.tgtpaymentprovider.ipg.data.soap.QueryResponse;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Response;
import au.com.target.tgtpaymentprovider.ipg.util.IpgPaymentStatus;
import au.com.target.tgtpaymentprovider.ipg.util.IpgPaymentType;
import au.com.target.tgtpaymentprovider.ipg.util.IpgToHybrisResponseMap;


/**
 * Retrieve transaction details from IPG
 */
public class IpgRetrieveTransactionCommandImpl extends AbstractIpgCommand implements TargetRetrieveTransactionCommand {

    @Override
    public TargetRetrieveTransactionResult perform(
            final TargetRetrieveTransactionRequest targetRetrieveTransactionRequest) {

        if (targetRetrieveTransactionRequest == null) {
            throw new IllegalArgumentException("TargetRetrieveTransactionRequest is null");
        }
        TargetRetrieveTransactionResult result = null;
        final QueryResponse response = getIpgService().queryTransaction(targetRetrieveTransactionRequest);
        result = mapQueryResponse(response,
                targetRetrieveTransactionRequest.isQueryGiftCardOnly() ? IpgPaymentType.GIFTCARD_PURCHASE
                        : IpgPaymentType.CREDITCARD_PURCHASE,
                getAcceptedTrnStatus(targetRetrieveTransactionRequest.getTrnType()));
        return result;
    }

    /**
     * @param trnType
     * @return list of status
     */
    private List<String> getAcceptedTrnStatus(final TrnType trnType) {
        if (trnType == null) {
            return null;
        }
        switch (trnType) {
            case FAILED:
                return ImmutableList.of(IpgPaymentStatus.DECLINED.getCode());
            case APPROVED:
                return ImmutableList.of(IpgPaymentStatus.APPROVED.getCode());
            default:
                return null;
        }

    }

    /**
     * @param response
     * @param acceptedTrnStatues
     * @return result
     */
    private TargetRetrieveTransactionResult mapQueryResponse(final QueryResponse response,
            final IpgPaymentType paymentType,
            final List<String> acceptedTrnStatues) {
        final TargetRetrieveTransactionResult result = new TargetRetrieveTransactionResult();
        if (response != null && CollectionUtils.isNotEmpty(response.getResponse())) {
            result.setMatchingCount(response.getMatchingCount());
            final List<PaymentTransactionEntryData> payments = new ArrayList<>();
            PaymentTransactionEntryData data = null;
            Response matchingResponseEntry = null;
            for (final Response responseEntry : response.getResponse()) {
                if (paymentType.getCode().equals(responseEntry.getTrnTypeId()) &&
                        (acceptedTrnStatues == null || acceptedTrnStatues.contains(responseEntry.getTrnStatusId()))) {
                    data = new PaymentTransactionEntryData();
                    matchingResponseEntry = responseEntry;
                    data.setAmount(new BigDecimal(responseEntry.getAmount()).divide(BigDecimal.valueOf(100d)).setScale(
                            2,
                            BigDecimal.ROUND_HALF_DOWN));
                    data.setReceiptNumber(responseEntry.getReceipt());
                    payments.add(data);
                }
            }
            if (payments.size() == 1) {
                result.setCapturedAmount(
                        Double.valueOf(data != null && data.getAmount() != null ? data.getAmount().doubleValue() : 0));
                result.setReconciliationId(matchingResponseEntry.getReceipt());
                final String declineCode = matchingResponseEntry.getDeclinedCode();
                if (!StringUtils.isEmpty(declineCode)) {
                    result.setDeclineCode(declineCode);
                    result.setDeclineMessage(matchingResponseEntry.getDeclinedMessage());
                }
                final TransactionStatusDetails transactionStatusDetails = IpgToHybrisResponseMap
                        .getTransactionStatusDetails(declineCode);
                result.setTransactionStatusDetails(transactionStatusDetails);
                if (transactionStatusDetails == null) {
                    result.setTransactionStatusDetails(TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
                }
                if (IpgPaymentStatus.APPROVED.getCode().equals(matchingResponseEntry.getTrnStatusId())) {
                    result.setTransactionStatus(TransactionStatus.ACCEPTED);
                }
                else {
                    result.setTransactionStatus(TransactionStatus.REJECTED);
                }
            }
            result.setPayments(payments);
        }
        return result;
    }
}
/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author mjanarth
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class CardResultData {

    @XmlElement(name = "Details")
    private TransactionDetails transactionDetails;
    @XmlElement(name = "Card")
    private CardDetails cardDetails;
    @XmlElement(name = "Fraud")
    private FraudDetails fraudDetails;

    /**
     * @return the transactionDetails
     */
    public TransactionDetails getTransactionDetails() {
        return transactionDetails;
    }

    /**
     * @param transactionDetails
     *            the transactionDetails to set
     */
    public void setTransactionDetails(final TransactionDetails transactionDetails) {
        this.transactionDetails = transactionDetails;
    }

    /**
     * @return the fraudDetails
     */
    public FraudDetails getFraudDetails() {
        return fraudDetails;
    }

    /**
     * @param fraudDetails
     *            the fraudDetails to set
     */
    public void setFraudDetails(final FraudDetails fraudDetails) {
        this.fraudDetails = fraudDetails;
    }

    /**
     * @return the cardDetails
     */
    public CardDetails getCardDetails() {
        return cardDetails;
    }

    /**
     * @param cardDetails
     *            the cardDetails to set
     */
    public void setCardDetails(final CardDetails cardDetails) {
        this.cardDetails = cardDetails;
    }

}

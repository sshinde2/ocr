/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author mjanarth
 *
 */
@XmlRootElement(name = "CardResults")
@XmlAccessorType(XmlAccessType.FIELD)
public class CardResultsData {
    @XmlElement(name = "Result")
    private List<CardResultData> cardResults;

    /**
     * @return the cardResults
     */
    public List<CardResultData> getCardResults() {
        return cardResults;
    }

    /**
     * @param cardResults
     *            the cardResults to set
     */
    public void setCardResults(final List<CardResultData> cardResults) {
        this.cardResults = cardResults;
    }

}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;



/**
 * @author mjanarth
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class FraudDetails {

    @XmlElement(name = "ReD")
    private ReDShieldFraudData redShieldFraudData;
    @XmlElement(name = "ThreeDS")
    private ThreeDsAuthenticationData threeDsAuthenticationData;

    /**
     * @return the redShieldFraudData
     */
    public ReDShieldFraudData getRedShieldFraudData() {
        return redShieldFraudData;
    }

    /**
     * @param redShieldFraudData
     *            the redShieldFraudData to set
     */
    public void setRedShieldFraudData(final ReDShieldFraudData redShieldFraudData) {
        this.redShieldFraudData = redShieldFraudData;
    }

    /**
     * @return the threeDsAuthenticationData
     */
    public ThreeDsAuthenticationData getThreeDsAuthenticationData() {
        return threeDsAuthenticationData;
    }

    /**
     * @param threeDsAuthenticationData
     *            the threeDsAuthenticationData to set
     */
    public void setThreeDsAuthenticationData(final ThreeDsAuthenticationData threeDsAuthenticationData) {
        this.threeDsAuthenticationData = threeDsAuthenticationData;
    }

}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data.request;

import au.com.target.tgtpaymentprovider.ipg.data.soap.Transaction;


/**
 * @author htan3
 *
 */
public class SubmitSinglePaymentRequest {

    private Transaction transaction;

    /**
     * @return the transaction
     */
    public Transaction getTransaction() {
        return transaction;
    }

    /**
     * @param transaction
     *            the transaction to set
     */
    public void setTransaction(final Transaction transaction) {
        this.transaction = transaction;
    }
}

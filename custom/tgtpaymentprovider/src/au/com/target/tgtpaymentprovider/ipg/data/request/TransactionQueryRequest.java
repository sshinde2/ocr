/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data.request;

/**
 * @author mjanarth
 *
 */
public class TransactionQueryRequest {
    private String sessionToken;
    private String sessionId;

    /**
     * @return the sessionToken
     */
    public String getSessionToken() {
        return sessionToken;
    }

    /**
     * @param sessionToken
     *            the sessionToken to set
     */
    public void setSessionToken(final String sessionToken) {
        this.sessionToken = sessionToken;
    }

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId
     *            the sessionId to set
     */
    public void setSessionId(final String sessionId) {
        this.sessionId = sessionId;
    }

}

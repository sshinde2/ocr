/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data.response;

import au.com.target.tgtpaymentprovider.ipg.data.soap.Response;


/**
 * @author mjanarth
 *
 */
public class SubmitSingleRefundResponse {

    private Response response;

    /**
     * @return the response
     */
    public Response getResponse() {
        return response;
    }

    /**
     * @param response
     *            the response to set
     */
    public void setResponse(final Response response) {
        this.response = response;
    }

}

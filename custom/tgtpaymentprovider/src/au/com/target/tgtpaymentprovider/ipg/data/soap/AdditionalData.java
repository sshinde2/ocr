/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data.soap;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author htan3
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class AdditionalData {
    @XmlElement(name = "Core")
    private List<String> coreList;

    public void addCore(final String core) {
        if (coreList == null) {
            coreList = new ArrayList<>();
        }
        coreList.add(core);
    }

    /**
     * @return the coreList
     */
    public List<String> getCoreList() {
        return coreList;
    }

    /**
     * @param coreList
     *            the coreList to set
     */
    public void setCoreList(final List<String> coreList) {
        this.coreList = coreList;
    }

}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author htan3
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class AdditionalInfo {
    @XmlElement(name = "LiabilityReversal")
    private String liabilityReversal;
    @XmlElement(name = "ReDFraudCheck")
    private ReDFraudCheck reDFraudCheck;

    /**
     * @return the liabilityReversal
     */
    public String getLiabilityReversal() {
        return liabilityReversal;
    }

    /**
     * @param liabilityReversal
     *            the liabilityReversal to set
     */
    public void setLiabilityReversal(final String liabilityReversal) {
        this.liabilityReversal = liabilityReversal;
    }

    /**
     * @return the reDFraudCheck
     */
    public ReDFraudCheck getReDFraudCheck() {
        return reDFraudCheck;
    }

    /**
     * @param reDFraudCheck
     *            the reDFraudCheck to set
     */
    public void setReDFraudCheck(final ReDFraudCheck reDFraudCheck) {
        this.reDFraudCheck = reDFraudCheck;
    }
}

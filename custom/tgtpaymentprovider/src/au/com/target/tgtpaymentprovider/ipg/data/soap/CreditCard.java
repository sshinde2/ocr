/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


@XmlAccessorType(XmlAccessType.FIELD)
public class CreditCard {
    @XmlElement(name = "CardNumber")
    private String cardNumber;

    @XmlElement(name = "CardType")
    private String cardType;

    @XmlElement(name = "MaskedCardNumber")
    private String maskedCardNumber;

    @XmlElement(name = "ThreeDS")
    private ThreeDS threeDS;

    @XmlElement(name = "IsCardOnFile")
    private String cardOnFile;

    @XmlElement(name = "IsFirstCredentialStorage")
    private String firstCredentialStorage;

    /**
     * @return the cardOnFile
     */
    public String getCardOnFile() {
        return cardOnFile;
    }

    /**
     * @param cardOnFile
     *            the cardOnFile to set
     */
    public void setCardOnFile(final String cardOnFile) {
        this.cardOnFile = cardOnFile;
    }

    /**
     * @return the firstCredentialStorage
     */
    public String getFirstCredentialStorage() {
        return firstCredentialStorage;
    }

    /**
     * @param firstCredentialStorage
     *            the firstCredentialStorage to set
     */
    public void setFirstCredentialStorage(final String firstCredentialStorage) {
        this.firstCredentialStorage = firstCredentialStorage;
    }
    /**
     * @return the cardNumber
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * @param cardNumber
     *            the cardNumber to set
     */
    public void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * @return the cardType
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * @param cardType
     *            the cardType to set
     */
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }

    /**
     * @return the maskedCardNumber
     */
    public String getMaskedCardNumber() {
        return maskedCardNumber;
    }

    /**
     * @param maskedCardNumber
     *            the maskedCardNumber to set
     */
    public void setMaskedCardNumber(final String maskedCardNumber) {
        this.maskedCardNumber = maskedCardNumber;
    }

    /**
     * @return the threeDS
     */
    public ThreeDS getThreeDS() {
        return threeDS;
    }

    /**
     * @param threeDS
     *            the threeDS to set
     */
    public void setThreeDS(final ThreeDS threeDS) {
        this.threeDS = threeDS;
    }


}

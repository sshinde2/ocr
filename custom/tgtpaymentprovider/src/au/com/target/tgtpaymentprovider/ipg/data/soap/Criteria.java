/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author htan3
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Criteria {

    @XmlElement(name = "AccountNumber")
    private String accountNumber;

    @XmlElement(name = "TrnStartTimestamp")
    private String trnStartTimestamp;

    @XmlElement(name = "TrnEndTimestamp")
    private String trnEndTimestamp;

    @XmlElement(name = "CustRef")
    private String custRef;

    @XmlElement(name = "CustNumber")
    private String custNumber;

    @XmlElement(name = "Receipt")
    private String receipt;

    @XmlElement(name = "Amount")
    private String amount;

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * @param accountNumber
     *            the accountNumber to set
     */
    public void setAccountNumber(final String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * @return the trnStartTimestamp
     */
    public String getTrnStartTimestamp() {
        return trnStartTimestamp;
    }

    /**
     * @param trnStartTimestamp
     *            the trnStartTimestamp to set
     */
    public void setTrnStartTimestamp(final String trnStartTimestamp) {
        this.trnStartTimestamp = trnStartTimestamp;
    }

    /**
     * @return the trnEndTimestamp
     */
    public String getTrnEndTimestamp() {
        return trnEndTimestamp;
    }

    /**
     * @param trnEndTimestamp
     *            the trnEndTimestamp to set
     */
    public void setTrnEndTimestamp(final String trnEndTimestamp) {
        this.trnEndTimestamp = trnEndTimestamp;
    }

    /**
     * @return the custRef
     */
    public String getCustRef() {
        return custRef;
    }

    /**
     * @param custRef
     *            the custRef to set
     */
    public void setCustRef(final String custRef) {
        this.custRef = custRef;
    }

    /**
     * @return the custNumber
     */
    public String getCustNumber() {
        return custNumber;
    }

    /**
     * @param custNumber
     *            the custNumber to set
     */
    public void setCustNumber(final String custNumber) {
        this.custNumber = custNumber;
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final String amount) {
        this.amount = amount;
    }

    /**
     * @return the receipt
     */
    public String getReceipt() {
        return receipt;
    }

    /**
     * @param receipt
     *            the receipt to set
     */
    public void setReceipt(final String receipt) {
        this.receipt = receipt;
    }

}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data.soap;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author htan3
 *
 */
@XmlRootElement(name = "QueryResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class QueryResponse {

    @XmlAttribute(name = "MatchingCount")
    private int matchingCount;

    @XmlAttribute(name = "MatchingReturned")
    private int matchingReturned;

    @XmlAttribute(name = "Error")
    private int error;

    @XmlElement(name = "Response")
    private List<Response> response;

    /**
     * @return the matchingCount
     */
    public int getMatchingCount() {
        return matchingCount;
    }

    /**
     * @param matchingCount
     *            the matchingCount to set
     */
    public void setMatchingCount(final int matchingCount) {
        this.matchingCount = matchingCount;
    }

    /**
     * @return the matchingReturned
     */
    public int getMatchingReturned() {
        return matchingReturned;
    }

    /**
     * @param matchingReturned
     *            the matchingReturned to set
     */
    public void setMatchingReturned(final int matchingReturned) {
        this.matchingReturned = matchingReturned;
    }

    /**
     * @return the error
     */
    public int getError() {
        return error;
    }

    /**
     * @param error
     *            the error to set
     */
    public void setError(final int error) {
        this.error = error;
    }

    /**
     * @return the response
     */
    public List<Response> getResponse() {
        return response;
    }

    /**
     * @param response
     *            the response to set
     */
    public void setResponse(final List<Response> response) {
        this.response = response;
    }

}

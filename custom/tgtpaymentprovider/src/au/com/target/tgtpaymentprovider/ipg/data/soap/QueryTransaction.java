/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author htan3
 *
 */
@XmlRootElement(name = "QueryTransaction")
@XmlAccessorType(XmlAccessType.FIELD)
public class QueryTransaction {

    @XmlElement(name = "Criteria")
    private Criteria criteria;

    @XmlElement(name = "AdditionalData")
    private AdditionalData addtionalData;

    @XmlElement(name = "Security")
    private Security security;

    /**
     * @return the criteria
     */
    public Criteria getCriteria() {
        return criteria;
    }

    /**
     * @param criteria
     *            the criteria to set
     */
    public void setCriteria(final Criteria criteria) {
        this.criteria = criteria;
    }

    /**
     * @return the addtionalData
     */
    public AdditionalData getAddtionalData() {
        return addtionalData;
    }

    /**
     * @param addtionalData
     *            the addtionalData to set
     */
    public void setAddtionalData(final AdditionalData addtionalData) {
        this.addtionalData = addtionalData;
    }

    /**
     * @return the security
     */
    public Security getSecurity() {
        return security;
    }

    /**
     * @param security
     *            the security to set
     */
    public void setSecurity(final Security security) {
        this.security = security;
    }

}

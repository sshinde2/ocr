/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author htan3
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ReDFraudCheck {
    @XmlElement(name = "ReDOrderNumber")
    private String reDOrderNumber;
    @XmlElement(name = "REQ_ID")
    private String reqId;

    /**
     * @return the reDOrderNumber
     */
    public String getReDOrderNumber() {
        return reDOrderNumber;
    }

    /**
     * @param reDOrderNumber
     *            the reDOrderNumber to set
     */
    public void setReDOrderNumber(final String reDOrderNumber) {
        this.reDOrderNumber = reDOrderNumber;
    }

    /**
     * @return the reqId
     */
    public String getReqId() {
        return reqId;
    }

    /**
     * @param reqId
     *            the reqId to set
     */
    public void setReqId(final String reqId) {
        this.reqId = reqId;
    }

}

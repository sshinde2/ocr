/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class Response {

    @XmlElement(name = "ResponseCode")
    private String responseCode;
    @XmlElement(name = "Timestamp")
    private String timestamp;
    @XmlElement(name = "Receipt")
    private String receipt;
    @XmlElement(name = "SettlementDate ")
    private String settlementDate;
    @XmlElement(name = "DeclinedCode")
    private String declinedCode;
    @XmlElement(name = "DeclinedMessage")
    private String declinedMessage;
    @XmlElement(name = "Amount")
    private String amount;
    @XmlElement(name = "CustNumber")
    private String custNumber;
    @XmlElement(name = "CustRef")
    private String custRef;
    @XmlElement(name = "TrnTypeID")
    private String trnTypeId;
    @XmlElement(name = "TrnStatusID")
    private String trnStatusId;



    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode
     *            the responseCode to set
     */
    public void setResponseCode(final String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp
     *            the timestamp to set
     */
    public void setTimestamp(final String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the receipt
     */
    public String getReceipt() {
        return receipt;
    }

    /**
     * @param receipt
     *            the receipt to set
     */
    public void setReceipt(final String receipt) {
        this.receipt = receipt;
    }

    /**
     * @return the declinedCode
     */
    public String getDeclinedCode() {
        return declinedCode;
    }

    /**
     * @param declinedCode
     *            the declinedCode to set
     */
    public void setDeclinedCode(final String declinedCode) {
        this.declinedCode = declinedCode;
    }

    /**
     * @return the declinedMessage
     */
    public String getDeclinedMessage() {
        return declinedMessage;
    }

    /**
     * @param declinedMessage
     *            the declinedMessage to set
     */
    public void setDeclinedMessage(final String declinedMessage) {
        this.declinedMessage = declinedMessage;
    }

    /**
     * @return the settlementDate
     */
    public String getSettlementDate() {
        return settlementDate;
    }

    /**
     * @param settlementDate
     *            the settlementDate to set
     */
    public void setSettlementDate(final String settlementDate) {
        this.settlementDate = settlementDate;
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final String amount) {
        this.amount = amount;
    }

    /**
     * @return the custNumber
     */
    public String getCustNumber() {
        return custNumber;
    }

    /**
     * @param custNumber
     *            the custNumber to set
     */
    public void setCustNumber(final String custNumber) {
        this.custNumber = custNumber;
    }

    /**
     * @return the custRef
     */
    public String getCustRef() {
        return custRef;
    }

    /**
     * @param custRef
     *            the custRef to set
     */
    public void setCustRef(final String custRef) {
        this.custRef = custRef;
    }

    /**
     * @return the trnTypeId
     */
    public String getTrnTypeId() {
        return trnTypeId;
    }

    /**
     * @param trnTypeId
     *            the trnTypeId to set
     */
    public void setTrnTypeId(final String trnTypeId) {
        this.trnTypeId = trnTypeId;
    }

    /**
     * @return the trnStatusId
     */
    public String getTrnStatusId() {
        return trnStatusId;
    }

    /**
     * @param trnStatusId
     *            the trnStatusId to set
     */
    public void setTrnStatusId(final String trnStatusId) {
        this.trnStatusId = trnStatusId;
    }

}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.data.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author htan3
 *
 */
@XmlRootElement(name = "Transaction")
@XmlAccessorType(XmlAccessType.FIELD)
public class Transaction {

    @XmlElement(name = "AccountNumber")
    private String accountNumber;

    @XmlElement(name = "MerchantNumber")
    private String merchantNumber;

    @XmlElement(name = "CustRef")
    private String custRef;

    @XmlElement(name = "CustNumber")
    private String custNumber;

    @XmlElement(name = "Amount")
    private String amount;

    @XmlElement(name = "TrnType")
    private String trnType;

    @XmlElement(name = "CreditCard")
    private CreditCard creditCard;

    @XmlElement(name = "Security")
    private Security security;

    @XmlElement(name = "UserDefined")
    private UserDefined userDefined;

    @XmlElement(name = "AdditionalInfoXML")
    private AdditionalInfo addtionalInfoXml;

    @XmlElement(name = "TrnSource")
    private String trnSource;

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * @param accountNumber
     *            the accountNumber to set
     */
    public void setAccountNumber(final String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * @return the merchantNumber
     */
    public String getMerchantNumber() {
        return merchantNumber;
    }

    /**
     * @param merchantNumber
     *            the merchantNumber to set
     */
    public void setMerchantNumber(final String merchantNumber) {
        this.merchantNumber = merchantNumber;
    }

    /**
     * @return the custRef
     */
    public String getCustRef() {
        return custRef;
    }

    /**
     * @param custRef
     *            the custRef to set
     */
    public void setCustRef(final String custRef) {
        this.custRef = custRef;
    }

    /**
     * @return the custNumber
     */
    public String getCustNumber() {
        return custNumber;
    }

    /**
     * @param custNumber
     *            the custNumber to set
     */
    public void setCustNumber(final String custNumber) {
        this.custNumber = custNumber;
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final String amount) {
        this.amount = amount;
    }

    /**
     * @return the trnType
     */
    public String getTrnType() {
        return trnType;
    }

    /**
     * @param trnType
     *            the trnType to set
     */
    public void setTrnType(final String trnType) {
        this.trnType = trnType;
    }

    /**
     * @return the creditCard
     */
    public CreditCard getCreditCard() {
        return creditCard;
    }

    /**
     * @param creditCard
     *            the creditCard to set
     */
    public void setCreditCard(final CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    /**
     * @return the security
     */
    public Security getSecurity() {
        return security;
    }

    /**
     * @param security
     *            the security to set
     */
    public void setSecurity(final Security security) {
        this.security = security;
    }

    /**
     * @return the userDefined
     */
    public UserDefined getUserDefined() {
        return userDefined;
    }

    /**
     * @param userDefined
     *            the userDefined to set
     */
    public void setUserDefined(final UserDefined userDefined) {
        this.userDefined = userDefined;
    }

    /**
     * @return the addtionalInfoXml
     */
    public AdditionalInfo getAddtionalInfoXml() {
        return addtionalInfoXml;
    }

    /**
     * @param addtionalInfoXml
     *            the addtionalInfoXml to set
     */
    public void setAddtionalInfoXml(final AdditionalInfo addtionalInfoXml) {
        this.addtionalInfoXml = addtionalInfoXml;
    }

    /**
     * @return the trnSource
     */
    public String getTrnSource() {
        return trnSource;
    }

    /**
     * @param trnSource
     *            the trnSource to set
     */
    public void setTrnSource(final String trnSource) {
        this.trnSource = trnSource;
    }

}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.exception;

/**
 * @author mjanarth
 *
 */
public class IPGClientException extends Exception {

    /**
     * @param message
     *            - Descriptive message
     */
    public IPGClientException(final String message) {
        super(message);
    }

    /**
     * @param cause
     *            - Root cause
     */
    public IPGClientException(final Exception cause) {
        super(cause);
    }

    /**
     * @param message
     *            - Descriptive message
     * @param cause
     *            - Root cause
     */
    public IPGClientException(final String message, final Exception cause) {
        super(message, cause);
    }

}

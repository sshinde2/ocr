/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.exception;

/**
 * Any IPG service exception would be wrapped to {@link IpgServiceException}
 */
public class IpgServiceException extends Exception {

    /**
     * No argument constructor
     */
    public IpgServiceException() {
        super();
    }

    /**
     * @param message
     *            - descriptive message
     */
    public IpgServiceException(final String message) {
        super(message);
    }

    /**
     * @param message
     *            - descriptive message
     * @param cause
     *            - root cause
     */
    public IpgServiceException(final String message, final Exception cause) {
        super(message, cause);
    }

    /**
     * @param cause
     *            - root cause
     */
    public IpgServiceException(final Exception cause) {
        super(cause);
    }
}
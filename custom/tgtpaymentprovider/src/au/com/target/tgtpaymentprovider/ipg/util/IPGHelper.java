/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.util;

import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtpaymentprovider.ipg.data.CardResultsData;
import au.com.target.tgtpaymentprovider.ipg.data.SavedCardsData;
import au.com.target.tgtpaymentprovider.ipg.data.soap.QueryResponse;
import au.com.target.tgtpaymentprovider.ipg.data.soap.QueryTransaction;
import au.com.target.tgtpaymentprovider.ipg.data.soap.RefundInfo;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Response;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Transaction;
import au.com.target.tgtpaymentprovider.ipg.data.soap.VoidInfo;



/**
 * @author mjanarth
 *
 */
public class IPGHelper {

    private static final Logger LOG = Logger.getLogger(IPGHelper.class);

    private JAXBContext context;

    public IPGHelper() {
        try {
            context = JAXBContext
                    .newInstance(SavedCardsData.class, Transaction.class, CardResultsData.class, Response.class,
                            RefundInfo.class, VoidInfo.class, QueryTransaction.class, QueryResponse.class);
        }
        catch (final JAXBException e) {
            LOG.error("failed to initialize JAXBContext", e);
        }
    }

    /**
     * parse amount into cents
     * 
     * @param amount
     * @return amountInCents
     */
    public static int getAmountInCents(final BigDecimal amount) {
        final BigDecimal amountInCents = amount.multiply(new BigDecimal("100")).setScale(0, RoundingMode.HALF_EVEN);
        return amountInCents.intValue();
    }


    /**
     * Converts the SavedCardsRequest Object to xml format
     * 
     * @param request
     * @return String
     * @throws JAXBException
     */
    public String convertSavedCardRequestToXML(final SavedCardsData request) throws JAXBException {
        return toXml(request);
    }

    /**
     * Converts object to xml format
     * 
     * @param request
     * @return String
     * @throws JAXBException
     */
    public String marshalTransaction(final Transaction request) throws JAXBException {
        return toXml(request);
    }

    /**
     * Converts object to xml format
     * 
     * @param request
     * @return String
     * @throws JAXBException
     */
    public String marshalRefundInfo(final RefundInfo request) throws JAXBException {
        return toXml(request);
    }

    /**
     * Converts submit single void request object to xml
     * 
     * @param request
     * @return String
     * @throws JAXBException
     */
    public String marshalVoidInfo(final VoidInfo request) throws JAXBException {
        return toXml(request);
    }

    /**
     * Unmarshall String xml
     * 
     * @param responseXml
     * @return Response
     * @throws JAXBException
     */
    public Response unmarshalResponse(final String responseXml) throws JAXBException {
        return (Response)fromXml(responseXml);
    }

    /**
     * Converts the xml to CardResults object
     * 
     * @param cardResultsXml
     * @return CardResultsData
     * @throws JAXBException
     */
    public CardResultsData unmarshalCardResults(final String cardResultsXml) throws JAXBException {
        return (CardResultsData)fromXml(cardResultsXml);
    }

    /**
     * Converts the xml to SavedCards object
     * 
     * @param savedCardsXML
     * @return SavedCardsData
     * @throws JAXBException
     */
    public SavedCardsData unmarshalSavedCards(final String savedCardsXML) throws JAXBException {
        return (SavedCardsData)fromXml(savedCardsXML);
    }

    /**
     * Encode the String base64
     * 
     * @param request
     * @return String
     */
    public String encodeBase64(final String request) {
        if (StringUtils.isNotEmpty(request)) {
            final byte[] encodedByteArray = Base64.encodeBase64(request.getBytes());
            return new String(encodedByteArray);
        }
        return null;
    }

    /**
     * Decode the String base64
     * 
     * @param request
     * @return String
     */
    public String decode(final String request) {
        if (StringUtils.isNotEmpty(request)) {
            final byte[] encodedByteArray = Base64.decodeBase64(request);
            return new String(encodedByteArray);
        }
        return null;
    }

    public Object fromXml(final String responseXml) throws JAXBException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Unmarshal ipg response:" + responseXml);
        }
        Object response = null;
        if (StringUtils.isNotEmpty(responseXml)) {
            final StringReader payloadReader = new StringReader(responseXml);
            response = context.createUnmarshaller().unmarshal(payloadReader);
        }
        return response;
    }

    public String toXml(final Object request) throws JAXBException {
        if (null != request) {
            final StringWriter payloadWriter = new StringWriter();
            final Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            marshaller.marshal(request, payloadWriter);
            return payloadWriter.toString();
        }
        return null;
    }
}

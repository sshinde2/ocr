/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.util;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import au.com.target.tgtpaymentprovider.constants.TgtpaymentproviderConstants;
import au.com.target.tgtpaymentprovider.ipg.data.SavedCardsData;
import au.com.target.tgtpaymentprovider.ipg.data.request.TokenizeRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.TransactionQueryRequest;


/**
 * @author mjanarth
 *
 */
public class IPGRequestParametersBuilder {

    private IPGHelper ipgHelper;

    /**
     * @return the ipgHelper
     */
    public IPGHelper getIpgHelper() {
        return ipgHelper;
    }

    /**
     * @param ipgHelper
     *            the ipgHelper to set
     */
    public void setIpgHelper(final IPGHelper ipgHelper) {
        this.ipgHelper = ipgHelper;
    }

    /**
     * Builds the request attributes for session request
     * 
     * @param request
     * @return List
     * @throws JAXBException
     */
    public List<NameValuePair> buildRequestParametersForSessionToken(final TokenizeRequest request)
            throws JAXBException {

        if (null != request) {
            final List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair(TgtpaymentproviderConstants.IPGRequestAttributes.SESSIONID, request
                    .getJsessionID()));
            params.add(new BasicNameValuePair(TgtpaymentproviderConstants.IPGRequestAttributes.SESSIONKEY, request
                    .getSessionKey()));
            params.add(new BasicNameValuePair(TgtpaymentproviderConstants.IPGRequestAttributes.DIRECTLINK, request
                    .getiFrameConfig()));
            params.add(new BasicNameValuePair(TgtpaymentproviderConstants.IPGRequestAttributes.MERCHANT_NUMBER, request
                    .getMerchantNumber()));
            params.add(new BasicNameValuePair(TgtpaymentproviderConstants.IPGRequestAttributes.URL, ipgHelper
                    .encodeBase64(request
                            .getUserUrl())));
            params.add(new BasicNameValuePair(TgtpaymentproviderConstants.IPGRequestAttributes.CUSTREF, request
                    .getCartId()));
            params.add(new BasicNameValuePair(TgtpaymentproviderConstants.IPGRequestAttributes.CUSTOMER_NUMBER, request
                    .getCustomerNumber()));
            params.add(new BasicNameValuePair(TgtpaymentproviderConstants.IPGRequestAttributes.POS_CONDITION, request
                    .getPosCond()));
            params.add(new BasicNameValuePair(TgtpaymentproviderConstants.IPGRequestAttributes.STORE_ID, request
                    .getStoreNumber()));
            final String savedCards = processSavedCards(request.getSavedCards());
            if (null != savedCards) {
                params.add(new BasicNameValuePair(TgtpaymentproviderConstants.IPGRequestAttributes.SAVED_CARDS,
                        savedCards));
            }
            params.add(new BasicNameValuePair(TgtpaymentproviderConstants.IPGRequestAttributes.AMOUNT, request
                    .getTenderAmount()));
            return params;
        }
        return null;
    }

    /**
     * Builds the request parameters for transaction results
     * 
     * @param request
     * @return List
     */
    public List<NameValuePair> buildRequestParamatersForGetTransactionResult(final TransactionQueryRequest request) {
        if (null != request) {
            final List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair(TgtpaymentproviderConstants.IPGRequestAttributes.SESSIONID, request
                    .getSessionId()));
            params.add(new BasicNameValuePair(TgtpaymentproviderConstants.IPGRequestAttributes.SESSIONTOKEN, request
                    .getSessionToken()));
            params.add(new BasicNameValuePair(TgtpaymentproviderConstants.IPGRequestAttributes.QUERY,
                    TgtpaymentproviderConstants.IPGRequestAttributes.STRING_TRUE));
            return params;
        }
        return null;
    }

    private String processSavedCards(final SavedCardsData request) throws JAXBException {
        final String xmlSavedCards = ipgHelper.convertSavedCardRequestToXML(request);
        return ipgHelper.encodeBase64(xmlSavedCards);

    }
}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.util;

/**
 * @author htan3
 *
 */
public enum IpgPaymentStatus {

    DECLINED("2"),
    ERROR("3"),
    APPROVED("1");

    private String code;

    private IpgPaymentStatus(final String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}

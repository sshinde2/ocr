/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.util;

/**
 * @author htan3
 *
 */
public enum IpgPaymentType {

    GIFTCARD_PURCHASE("47"),
    CREDITCARD_PURCHASE("50");

    private String code;

    private IpgPaymentType(final String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}

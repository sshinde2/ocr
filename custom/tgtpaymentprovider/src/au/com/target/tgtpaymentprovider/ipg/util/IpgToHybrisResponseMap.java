package au.com.target.tgtpaymentprovider.ipg.util;

import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.util.HashMap;
import java.util.Map;


/**
 * IPG response status mapping
 */
public final class IpgToHybrisResponseMap {

    private static final HashMap<String, TransactionStatus> IPG_STATUS_MAP = new HashMap<String, TransactionStatus>();
    private static final HashMap<String, TransactionStatusDetails> IPG_STATUS_DETAILS_MAP = new HashMap<String, TransactionStatusDetails>();

    private static Map<IpgTransactionResultEnum, TransactionStatus> resultMap = new HashMap<>();
    private static Map<IpgTransactionResultEnum, TransactionStatusDetails> responseMap = new HashMap<>();


    private IpgToHybrisResponseMap() {
        // no instantiation from external class
    }

    static {
        // response map
        responseMap.put(IpgTransactionResultEnum.SUCCESSFUL, TransactionStatusDetails.SUCCESFULL);
        responseMap.put(IpgTransactionResultEnum.DECLINED, TransactionStatusDetails.BANK_DECLINE);
        responseMap.put(IpgTransactionResultEnum.SESSION_TIMED_OUT, TransactionStatusDetails.GENERAL_SYSTEM_ERROR);

        // resultMap
        resultMap.put(IpgTransactionResultEnum.SUCCESSFUL, TransactionStatus.ACCEPTED);
        resultMap.put(IpgTransactionResultEnum.SESSION_TIMED_OUT, TransactionStatus.ERROR);
        resultMap.put(IpgTransactionResultEnum.DECLINED, TransactionStatus.REJECTED);


        IPG_STATUS_MAP.put("0", TransactionStatus.ACCEPTED);
        IPG_STATUS_MAP.put("1", TransactionStatus.ERROR);
        IPG_STATUS_DETAILS_MAP.put(null, TransactionStatusDetails.SUCCESFULL);
        IPG_STATUS_DETAILS_MAP.put("", TransactionStatusDetails.SUCCESFULL);
        IPG_STATUS_DETAILS_MAP.put("13", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("14", TransactionStatusDetails.INCORRECT_CARD_NUMBER_OR_TYPE);
        IPG_STATUS_DETAILS_MAP.put("23", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("24", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("33", TransactionStatusDetails.INVALID_CARD_EXPIRATION_DATE);

        IPG_STATUS_DETAILS_MAP.put("34", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("35", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("36", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("37", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("38", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("39", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("40", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("41", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("42", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("43", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("44", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("51", TransactionStatusDetails.INSUFFICIENT_FUNDS);
        IPG_STATUS_DETAILS_MAP.put("52", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("53", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("54", TransactionStatusDetails.INVALID_CARD_EXPIRATION_DATE);
        IPG_STATUS_DETAILS_MAP.put("55", TransactionStatusDetails.INVALID_CVN);
        IPG_STATUS_DETAILS_MAP.put("56", TransactionStatusDetails.INCORRECT_CARD_NUMBER_OR_TYPE);
        IPG_STATUS_DETAILS_MAP.put("57", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("59", TransactionStatusDetails.STOLEN_OR_LOST_CARD);
        IPG_STATUS_DETAILS_MAP.put("61", TransactionStatusDetails.CREDIT_LIMIT_REACHED);
        IPG_STATUS_DETAILS_MAP.put("62", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("63", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("64", TransactionStatusDetails.AMOUNTS_MUST_MATCH);
        IPG_STATUS_DETAILS_MAP.put("65", TransactionStatusDetails.CARD_USED_TOO_RECENTLY);
        IPG_STATUS_DETAILS_MAP.put("66", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("67", TransactionStatusDetails.BANK_DECLINE);
        //68 stands for timeout, however ipg treats this as declined, detail reason will be saved into declined details
        IPG_STATUS_DETAILS_MAP.put("68", TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        IPG_STATUS_DETAILS_MAP.put("75", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("90", TransactionStatusDetails.TRANSACTION_ALREADY_SETTLED_OR_REVERSED);
        IPG_STATUS_DETAILS_MAP.put("91", TransactionStatusDetails.COMMUNICATION_PROBLEM);
        IPG_STATUS_DETAILS_MAP.put("92", TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        IPG_STATUS_DETAILS_MAP.put("93", TransactionStatusDetails.DUPLICATE_TRANSACTION);
        IPG_STATUS_DETAILS_MAP.put("94", TransactionStatusDetails.BANK_DECLINE);
        IPG_STATUS_DETAILS_MAP.put("95", TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        IPG_STATUS_DETAILS_MAP.put("96", TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        IPG_STATUS_DETAILS_MAP.put("97", TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        IPG_STATUS_DETAILS_MAP.put("98", TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        IPG_STATUS_DETAILS_MAP.put("99", TransactionStatusDetails.UNKNOWN_CODE);
        IPG_STATUS_DETAILS_MAP.put("100", TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        IPG_STATUS_DETAILS_MAP.put("180", TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        IPG_STATUS_DETAILS_MAP.put("182", TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        IPG_STATUS_DETAILS_MAP.put("183", TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        IPG_STATUS_DETAILS_MAP.put("184", TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        IPG_STATUS_DETAILS_MAP.put("190", TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        IPG_STATUS_DETAILS_MAP.put("191", TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        IPG_STATUS_DETAILS_MAP.put("192", TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        IPG_STATUS_DETAILS_MAP.put("200", TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        IPG_STATUS_DETAILS_MAP.put("201", TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
    }

    /**
     * Return {@link TransactionStatusDetails} for given IPG transaction restul status
     * 
     * @param status
     * @return transaction status details
     */
    public static TransactionStatusDetails getTransactionStatusDetails(final IpgTransactionResultEnum status) {
        return responseMap.get(status);
    }

    /**
     * Return {@link TransactionStatus} for given IPG transaction result status
     * 
     * @param status
     * @return transaction status
     */
    public static TransactionStatus getTransactionStatus(final IpgTransactionResultEnum status) {
        return resultMap.get(status);
    }

    /**
     * Return {@link TransactionStatusDetails} for given decline code
     * 
     * @param declineCode
     * @return transaction status details
     */
    public static TransactionStatusDetails getTransactionStatusDetails(final String declineCode) {
        return IPG_STATUS_DETAILS_MAP.get(declineCode);
    }

    /**
     * Return {@link TransactionStatus} for given IPG transaction result status
     * 
     * @param responseCode
     * @return transaction status
     */
    public static TransactionStatus getTransactionStatus(final String responseCode) {
        if (IPG_STATUS_MAP.containsKey(responseCode)) {
            return IPG_STATUS_MAP.get(responseCode);
        }
        return TransactionStatus.ERROR;
    }
}
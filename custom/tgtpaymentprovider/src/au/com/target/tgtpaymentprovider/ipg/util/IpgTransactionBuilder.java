/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.util;

import org.apache.commons.lang.BooleanUtils;

import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpaymentprovider.ipg.data.soap.CreditCard;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Security;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Transaction;
import au.com.target.tgtpaymentprovider.ipg.data.soap.UserDefined;


/**
 * Help Class to build Transaction for ipg soap client.
 * 
 * @author htan3
 *
 */
public class IpgTransactionBuilder {

    private static final String TRN_TYPE_DEFERRED = "50";

    private final Transaction transaction = new Transaction();

    private final UserDefined userDefined = new UserDefined();

    private final CreditCard creditCard = new CreditCard();

    private IpgTransactionBuilder() {
        //do nothing, please use IpgTransactionBuilder.create()
    }

    public static IpgTransactionBuilder create() {
        final IpgTransactionBuilder builder = new IpgTransactionBuilder();
        return builder;
    }

    /**
     * return the transaction with credit card and other info.
     * 
     * @return Transaction
     */
    public Transaction build() {
        transaction.setCreditCard(creditCard);
        transaction.setUserDefined(userDefined);
        transaction.setTrnType(TRN_TYPE_DEFERRED);
        return transaction;
    }

    /**
     * applySecurity.
     * 
     * @param security
     */
    public void applySecurity(final Security security) {
        transaction.setSecurity(security);
    }

    /**
     * addAccountNumber.
     * 
     * @param accountNumber
     * @param merchantNumber
     */
    public void addAccountNumber(final String accountNumber, final String merchantNumber) {
        transaction.setAccountNumber(accountNumber);
        transaction.setMerchantNumber(merchantNumber);
    }

    /**
     * addAmountInCents.
     * 
     * @param cents
     */
    public void addAmountInCents(final Integer cents) {
        if (cents != null) {
            transaction.setAmount(cents.toString());
        }
    }

    /**
     * addCustomer.
     * 
     * @param custRef
     * @param custNumber
     */
    public void addCustomer(final String custRef, final String custNumber) {
        transaction.setCustNumber(custNumber);
        transaction.setCustRef(custRef);
    }

    /**
     * populate credit card and user defined info from IpgCardResult.
     * 
     * @param ipgCard
     */
    public void addIpgCardInfo(final TargetCardResult ipgCard, final boolean addCredentialOnFileDetails) {
        creditCard.setCardNumber(ipgCard.getToken());
        creditCard.setCardType(ipgCard.getCardType());
        creditCard.setMaskedCardNumber(ipgCard.getCardNumber());
        if (addCredentialOnFileDetails) {
            if (BooleanUtils.isTrue(ipgCard.isCardOnFile())) {
                creditCard.setCardOnFile("TRUE");
            }
            if (BooleanUtils.isTrue(ipgCard.isFirstCredentialStorage())) {
                creditCard.setFirstCredentialStorage("TRUE");
            }
        }
        userDefined.setToken(ipgCard.getToken());
        userDefined.setBin(ipgCard.getBin());
        userDefined.setCcExpiry(ipgCard.getCardExpiry());
        userDefined.setTokenExpiry(ipgCard.getTokenExpiry());
    }

    /**
     * set ip address for end user.
     * 
     * @param ip
     */
    public void addClientIP(final String ip) {
        transaction.setTrnSource(ip);
    }

    /**
     * Sets the store id
     * 
     * @param storeId
     */
    public void setStoreId(final String storeId) {
        userDefined.setStoreId(storeId);
    }

    /**
     * Sets the POSCond parameter
     * 
     * @param posCond
     */
    public void setPosCond(final String posCond) {
        userDefined.setPosCond(posCond);
    }
}
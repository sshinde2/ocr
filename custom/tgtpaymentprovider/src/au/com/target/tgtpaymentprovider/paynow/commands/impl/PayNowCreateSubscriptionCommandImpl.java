/**
 * 
 */
package au.com.target.tgtpaymentprovider.paynow.commands.impl;


import org.apache.commons.lang.NotImplementedException;

import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;


/**
 * @author ragarwa3
 * 
 */
public class PayNowCreateSubscriptionCommandImpl extends AbstractPayNowCommand implements
        TargetCreateSubscriptionCommand {

    static final String COMMAND_CREATE_SUBSCRIPTION = "createSubscription";

    @Override
    public TargetCreateSubscriptionResult perform(final TargetCreateSubscriptionRequest arg0) {
        throw new NotImplementedException(getCommandNotImplementedMessage(COMMAND_CREATE_SUBSCRIPTION));
    }

}

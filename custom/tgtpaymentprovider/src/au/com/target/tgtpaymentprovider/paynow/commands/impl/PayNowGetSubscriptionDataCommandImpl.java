/**
 * 
 */
package au.com.target.tgtpaymentprovider.paynow.commands.impl;

import de.hybris.platform.payment.commands.GetSubscriptionDataCommand;
import de.hybris.platform.payment.commands.request.SubscriptionDataRequest;
import de.hybris.platform.payment.commands.result.SubscriptionDataResult;

import org.apache.commons.lang.NotImplementedException;


/**
 * @author ragarwa3
 * 
 */
public class PayNowGetSubscriptionDataCommandImpl extends AbstractPayNowCommand implements GetSubscriptionDataCommand {

    static final String COMMAND_GET_SUBSCRIPTION = "getSubscription";

    @Override
    public SubscriptionDataResult perform(final SubscriptionDataRequest arg0) {
        throw new NotImplementedException(getCommandNotImplementedMessage(COMMAND_GET_SUBSCRIPTION));
    }

}

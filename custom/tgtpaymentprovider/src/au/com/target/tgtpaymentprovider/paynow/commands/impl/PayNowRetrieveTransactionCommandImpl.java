/**
 * 
 */
package au.com.target.tgtpaymentprovider.paynow.commands.impl;


import org.apache.commons.lang.NotImplementedException;

import au.com.target.tgtpayment.commands.TargetRetrieveTransactionCommand;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;


/**
 * @author ragarwa3
 * 
 */
public class PayNowRetrieveTransactionCommandImpl extends AbstractPayNowCommand implements
        TargetRetrieveTransactionCommand {

    static final String COMMAND_RETRIEVE_TRANSACTION = "retrieveTransaction";

    @Override
    public TargetRetrieveTransactionResult perform(final TargetRetrieveTransactionRequest arg0) {
        throw new NotImplementedException(getCommandNotImplementedMessage(COMMAND_RETRIEVE_TRANSACTION));
    }

}

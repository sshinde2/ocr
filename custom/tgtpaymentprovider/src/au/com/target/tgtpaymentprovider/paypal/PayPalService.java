/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal;

import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentReq;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentResponseType;
import urn.ebay.api.PayPalAPI.DoReferenceTransactionResponseType;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsReq;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsResponseType;
import urn.ebay.api.PayPalAPI.GetTransactionDetailsReq;
import urn.ebay.api.PayPalAPI.GetTransactionDetailsResponseType;
import urn.ebay.api.PayPalAPI.RefundTransactionReq;
import urn.ebay.api.PayPalAPI.RefundTransactionResponseType;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutReq;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutResponseType;
import au.com.target.tgtpaymentprovider.paypal.exception.InvalidRequestException;
import au.com.target.tgtpaymentprovider.paypal.exception.PayPalServiceException;
import au.com.target.tgtpaymentprovider.paypal.exception.ValidationFailedException;


/**
 * @author vmuthura
 * 
 */
public interface PayPalService
{
    /**
     * Retrieves a PayPal transaction token, given transaction details, which can be used to capture.
     * 
     * @param request
     *            -
     * @return token
     * @throws InvalidRequestException
     *             - if the request is missing parameters
     * @throws ValidationFailedException
     *             - if any of the data is invalid
     * @throws PayPalServiceException
     *             - if there is an internal error/other specific paypal errorcodes
     */
    SetExpressCheckoutResponseType setExpressCheckout(SetExpressCheckoutReq request)
            throws InvalidRequestException, ValidationFailedException, PayPalServiceException;

    /**
     * Given a transaction token, retrieves information like amount, shipping address etc.
     * 
     * @param request
     *            -
     * @return Transaction details
     * @throws InvalidRequestException
     *             - if the request is missing parameters
     * @throws ValidationFailedException
     *             - if any of the data is invalid
     * @throws PayPalServiceException
     *             - if there is an internal error/other specific paypal errorcodes
     */
    GetExpressCheckoutDetailsResponseType getCheckoutDetails(GetExpressCheckoutDetailsReq request)
            throws InvalidRequestException, ValidationFailedException,
            PayPalServiceException;

    /**
     * Finalizes the transaction and captures the payment.
     * 
     * @param request
     *            -
     * @return - express checkout payment response info
     * @throws InvalidRequestException
     *             - if the request is missing parameters
     * @throws ValidationFailedException
     *             - if any of the data is invalid
     * @throws PayPalServiceException
     *             - if there is an internal error/other specific paypal errorcodes
     */
    DoExpressCheckoutPaymentResponseType doExpressCheckout(DoExpressCheckoutPaymentReq request)
            throws InvalidRequestException, ValidationFailedException, PayPalServiceException;

    /**
     * Refunds the amount captured by the provided token/payerid
     * 
     * @param request
     *            -
     * @return -
     * @throws InvalidRequestException
     *             - if the request is missing parameters
     * @throws ValidationFailedException
     *             - if any of the data is invalid
     * @throws PayPalServiceException
     *             - if there is an internal error/other specific paypal errorcodes
     */
    RefundTransactionResponseType refund(RefundTransactionReq request) throws InvalidRequestException,
            ValidationFailedException,
            PayPalServiceException;

    /**
     * Performs a reference transaction to validate the account
     * 
     * @param request
     *            -
     * @return -
     * @throws InvalidRequestException
     *             - if the request is missing parameters
     * @throws ValidationFailedException
     *             - if any of the data is invalid
     * @throws PayPalServiceException
     *             - if there is an internal error/other specific paypal errorcodes
     */
    DoReferenceTransactionResponseType performReferenceTransaction(Object request) throws InvalidRequestException,
            ValidationFailedException,
            PayPalServiceException;

    /**
     * Search for previous transactions.
     * 
     * @param request
     *            -
     * @return -
     * @throws InvalidRequestException
     *             - if the request is missing parameters
     * @throws ValidationFailedException
     *             - if any of the data is invalid
     * @throws PayPalServiceException
     *             - if there is an internal error/other specific paypal errorcodes
     */
    GetTransactionDetailsResponseType retrieve(GetTransactionDetailsReq request)
            throws InvalidRequestException, ValidationFailedException, PayPalServiceException;

}
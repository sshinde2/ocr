/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.commands.impl;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtpaymentprovider.paypal.PayPalService;


/**
 * @author vmuthura
 * 
 */
public class AbstractPayPalCommand
{
    private PayPalService payPalService;

    /**
     * @return the payPalService
     */
    public PayPalService getPayPalService()
    {
        return payPalService;
    }

    /**
     * @param payPalService
     *            the payPalService to set
     */
    @Required
    public void setPayPalService(final PayPalService payPalService)
    {
        this.payPalService = payPalService;
    }
}

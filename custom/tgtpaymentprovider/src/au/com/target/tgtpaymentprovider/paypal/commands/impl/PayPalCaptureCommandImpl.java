/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.commands.impl;

import de.hybris.platform.payment.AdapterException;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import au.com.target.tgtpayment.commands.TargetCaptureCommand;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpayment.dto.Address;
import au.com.target.tgtpayment.dto.LineItem;
import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpaymentprovider.paypal.exception.PayPalServiceException;
import au.com.target.tgtpaymentprovider.paypal.util.PayPal2HybrisResponseMap;
import au.com.target.tgtpaymentprovider.paypal.util.Util;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentReq;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentRequestType;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentResponseType;
import urn.ebay.apis.eBLBaseComponents.AddressType;
import urn.ebay.apis.eBLBaseComponents.CountryCodeType;
import urn.ebay.apis.eBLBaseComponents.DoExpressCheckoutPaymentRequestDetailsType;
import urn.ebay.apis.eBLBaseComponents.PaymentActionCodeType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsItemType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsType;


/**
 * @author vmuthura
 * 
 */
public class PayPalCaptureCommandImpl extends AbstractPayPalCommand implements TargetCaptureCommand {

    /* (non-Javadoc)
     * @see de.hybris.platform.payment.commands.Command#perform(java.lang.Object)
     */
    @Override
    public TargetCaptureResult perform(final TargetCaptureRequest captureRequest) {
        final DoExpressCheckoutPaymentReq expressCheckoutPaymentRequestType = translateRequest(captureRequest);

        DoExpressCheckoutPaymentResponseType checkoutPaymentResponseType = null;

        try {
            checkoutPaymentResponseType = getPayPalService()
                    .doExpressCheckout(expressCheckoutPaymentRequestType);
        }
        catch (final PayPalServiceException ex) {
            throw new AdapterException(ex.getMessage(), ex);
        }

        final TargetCaptureResult captureResult = new TargetCaptureResult();

        // Success
        if (checkoutPaymentResponseType != null) {
            if (checkoutPaymentResponseType.getAck() != null
                    && Util.isCaptureSuccessful(checkoutPaymentResponseType.getAck())) {
                captureResult.setMerchantTransactionCode(checkoutPaymentResponseType
                        .getDoExpressCheckoutPaymentResponseDetails().getPaymentInfo().get(0).getTransactionID());
                captureResult.setReconciliationId(checkoutPaymentResponseType.getCorrelationID());
                captureResult.setRequestToken(checkoutPaymentResponseType.getDoExpressCheckoutPaymentResponseDetails()
                        .getToken());
                captureResult.setRequestId(checkoutPaymentResponseType
                        .getDoExpressCheckoutPaymentResponseDetails().getPaymentInfo().get(0).getTransactionID());

                captureResult
                        .setTotalAmount(new BigDecimal(checkoutPaymentResponseType
                                .getDoExpressCheckoutPaymentResponseDetails().getPaymentInfo().get(0).getGrossAmount()
                                .getValue()));
            }
            captureResult.setTransactionStatus(
                    PayPal2HybrisResponseMap.getHybrisTransactionStatus(checkoutPaymentResponseType.getAck()));

            captureResult.setTransactionStatusDetails(
                    PayPal2HybrisResponseMap.getHybrisTransactionStatusDetails(checkoutPaymentResponseType
                            .getDoExpressCheckoutPaymentResponseDetails().getPaymentInfo().get(0).getPaymentStatus()));
        }

        return captureResult;
    }

    /**
     * @param captureRequest
     *            -
     * @return -
     */
    protected DoExpressCheckoutPaymentReq translateRequest(final TargetCaptureRequest captureRequest) {

        /* Formatter to eliminate $ sign in order amounts*/
        final DecimalFormat formatter = new DecimalFormat("###.##");
        final DoExpressCheckoutPaymentRequestType doECPaymentReqType = new DoExpressCheckoutPaymentRequestType();

        final DoExpressCheckoutPaymentRequestDetailsType doECpaymentReqDetailsType = new DoExpressCheckoutPaymentRequestDetailsType();
        doECpaymentReqDetailsType.setToken(captureRequest.getToken());
        doECpaymentReqDetailsType.setPayerID(captureRequest.getPayerId());

        doECPaymentReqType.setDoExpressCheckoutPaymentRequestDetails(doECpaymentReqDetailsType);

        if (captureRequest.getOrder() != null) {
            final Order order = captureRequest.getOrder();

            final List<PaymentDetailsItemType> paymentItemDetail = new LinkedList<>();
            for (final LineItem item : order.getLineItems()) {
                final PaymentDetailsItemType paymentDetailsItemType = new PaymentDetailsItemType();
                paymentDetailsItemType.setNumber(Integer.toString(item.getNumber()));
                paymentDetailsItemType.setName(item.getName());

                final Long quantity = Long.valueOf(item.getQuantity());
                paymentDetailsItemType.setQuantity(Integer.valueOf(quantity.intValue()));

                paymentDetailsItemType.setAmount(Util.toBasicAmountType(item.getPrice(), formatter));
                paymentItemDetail.add(paymentDetailsItemType);
            }

            final PaymentDetailsType paymentDetailsType = new PaymentDetailsType();
            paymentDetailsType.setOrderDescription(order.getOrderDescription());

            paymentDetailsType.setShippingTotal(Util.toBasicAmountType(order.getShippingTotalAmount(), formatter));
            paymentDetailsType.setOrderTotal(Util.toBasicAmountType(order.getOrderTotalAmount(), formatter));
            paymentDetailsType.setItemTotal(Util.toBasicAmountType(order.getItemSubTotalAmount(), formatter));

            final Address shippingAddress = order.getShippingAddress();
            if (shippingAddress != null) {
                final AddressType shipToAddress = new AddressType();
                shipToAddress.setStreet1(shippingAddress.getStreet1());
                shipToAddress.setStreet2(shippingAddress.getStreet2());
                shipToAddress.setCityName(shippingAddress.getCity());
                shipToAddress.setStateOrProvince(shippingAddress.getState());
                shipToAddress.setCountryName(shippingAddress.getCountryName());
                if (null != shippingAddress.getCountryCode()) {
                    shipToAddress.setCountry(CountryCodeType.valueOf(shippingAddress.getCountryCode()));
                }
                shipToAddress.setPostalCode(shippingAddress.getPostalCode());

                paymentDetailsType.setShipToAddress(shipToAddress);
            }

            paymentDetailsType.setPaymentAction(PaymentActionCodeType.SALE);
            paymentDetailsType.setInvoiceID(captureRequest.getTransactionId());
            doECpaymentReqDetailsType.setPaymentDetails(Collections.singletonList(paymentDetailsType));


        }

        final DoExpressCheckoutPaymentReq req = new DoExpressCheckoutPaymentReq();
        req.setDoExpressCheckoutPaymentRequest(doECPaymentReqType);

        return req;
    }
}

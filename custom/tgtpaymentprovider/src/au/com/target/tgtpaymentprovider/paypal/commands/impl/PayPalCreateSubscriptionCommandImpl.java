/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.commands.impl;


import de.hybris.platform.payment.AdapterException;

import java.text.NumberFormat;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.springframework.beans.factory.annotation.Required;

import urn.ebay.api.PayPalAPI.SetExpressCheckoutReq;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutRequestType;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutResponseType;
import urn.ebay.apis.CoreComponentTypes.BasicAmountType;
import urn.ebay.apis.eBLBaseComponents.AddressType;
import urn.ebay.apis.eBLBaseComponents.BillingAgreementDetailsType;
import urn.ebay.apis.eBLBaseComponents.BillingCodeType;
import urn.ebay.apis.eBLBaseComponents.CountryCodeType;
import urn.ebay.apis.eBLBaseComponents.CurrencyCodeType;
import urn.ebay.apis.eBLBaseComponents.PaymentActionCodeType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsItemType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsType;
import urn.ebay.apis.eBLBaseComponents.SetExpressCheckoutRequestDetailsType;
import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.dto.Address;
import au.com.target.tgtpayment.dto.AmountType;
import au.com.target.tgtpayment.dto.LineItem;
import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpaymentprovider.paypal.exception.PayPalServiceException;
import au.com.target.tgtpaymentprovider.paypal.util.PayPal2HybrisResponseMap;


/**
 * @author vmuthura
 * 
 */
public class PayPalCreateSubscriptionCommandImpl extends AbstractPayPalCommand implements
        TargetCreateSubscriptionCommand
{
    private static final int PAYPAL_ITEM_DESCRIPTION_MAX_LENGTH = 127;

    private String reqBillingAddress;
    private String allowNote;
    private String addressOverride;
    private String pageStyle;
    private String imageUrl;

    /**
     * @param reqBillingAddress
     *            the reqBillingAddress to set
     */
    @Required
    public void setReqBillingAddress(final String reqBillingAddress)
    {
        this.reqBillingAddress = reqBillingAddress;
    }

    /**
     * @param allowNote
     *            the allowNote to set
     */
    @Required
    public void setAllowNote(final String allowNote)
    {
        this.allowNote = allowNote;
    }

    /**
     * @param addressOverride
     *            the addressOverride to set
     */
    @Required
    public void setAddressOverride(final String addressOverride)
    {
        this.addressOverride = addressOverride;
    }

    /**
     * @param pageStyle
     *            the pageStyle to set
     */
    public void setPageStyle(final String pageStyle)
    {
        this.pageStyle = pageStyle;
    }

    /**
     * @param imageUrl
     *            the imageUrl to set
     */
    public void setImageUrl(final String imageUrl)
    {
        this.imageUrl = imageUrl;
    }

    @Override
    public TargetCreateSubscriptionResult perform(final TargetCreateSubscriptionRequest targetCreateSubscriptionRequest)
    {
        if (targetCreateSubscriptionRequest == null)
        {
            throw new IllegalArgumentException("TargetCreateSubscriptionRequest is null");
        }

        try
        {
            final SetExpressCheckoutResponseType response = getPayPalService().setExpressCheckout(
                    translateRequest(targetCreateSubscriptionRequest));

            return translateResponse(response);
        }
        catch (final PayPalServiceException e)
        {
            throw new AdapterException(e);
        }

    }

    /**
     * @param targetCreateSubscriptionRequest
     *            - {@link TargetCreateSubscriptionRequest}
     * @return - PayPal request {@link SetExpressCheckoutRequestType}
     */
    private SetExpressCheckoutReq translateRequest(
            final TargetCreateSubscriptionRequest targetCreateSubscriptionRequest)
    {
        final NumberFormat formatter = NumberFormat.getCurrencyInstance();
        final SetExpressCheckoutRequestType setECRequest = new SetExpressCheckoutRequestType();
        final SetExpressCheckoutRequestDetailsType setECRequestDetails = new SetExpressCheckoutRequestDetailsType();
        setECRequestDetails.setReturnURL(targetCreateSubscriptionRequest.getReturnUrl());
        setECRequestDetails.setCancelURL(targetCreateSubscriptionRequest.getCancelUrl());
        setECRequestDetails.setReqBillingAddress(reqBillingAddress);
        setECRequestDetails.setAllowNote(allowNote);
        setECRequestDetails.setAddressOverride(addressOverride);
        setECRequestDetails.setPageStyle(pageStyle);
        setECRequestDetails.setCppLogoImage(imageUrl);

        if (targetCreateSubscriptionRequest.getOrder() != null)
        {
            final Order order = targetCreateSubscriptionRequest.getOrder();
            final List<PaymentDetailsItemType> paymentItemDetail = new LinkedList<>();
            for (final LineItem item : order.getLineItems())
            {
                final PaymentDetailsItemType paymentDetailsItemType = new PaymentDetailsItemType();
                if (item.getNumber() != 0) {
                    paymentDetailsItemType.setNumber(Integer.toString(item.getNumber()));
                }
                paymentDetailsItemType.setName(item.getName());

                final String description = item.getDescription();
                if (StringUtils.isNotBlank(description)) {
                    final String cleanDescription = Jsoup.clean(description, Whitelist.none());
                    final String shortenedDescription = StringUtils.abbreviate(cleanDescription,
                            PAYPAL_ITEM_DESCRIPTION_MAX_LENGTH);
                    paymentDetailsItemType.setDescription(shortenedDescription);
                }

                final Long quantity = Long.valueOf(item.getQuantity());
                paymentDetailsItemType.setQuantity(Integer.valueOf(quantity.intValue()));

                paymentDetailsItemType.setAmount(toBasicAmountType(item.getPrice(), formatter));
                paymentItemDetail.add(paymentDetailsItemType);
            }

            final PaymentDetailsType paymentDetailsType = new PaymentDetailsType();
            paymentDetailsType.setOrderDescription(order.getOrderDescription());
            paymentDetailsType.setPaymentDetailsItem(paymentItemDetail);

            paymentDetailsType.setShippingTotal(toBasicAmountType(order.getShippingTotalAmount(), formatter));
            paymentDetailsType.setOrderTotal(toBasicAmountType(order.getOrderTotalAmount(), formatter));
            paymentDetailsType.setItemTotal(toBasicAmountType(order.getItemSubTotalAmount(), formatter));

            final Address shippingAddress = order.getShippingAddress();
            if (shippingAddress != null)
            {
                final AddressType shipToAddress = new AddressType();
                shipToAddress.setStreet1(shippingAddress.getStreet1());
                shipToAddress.setStreet2(shippingAddress.getStreet2());
                shipToAddress.setCityName(shippingAddress.getCity());
                shipToAddress.setStateOrProvince(shippingAddress.getState());
                shipToAddress.setCountryName(shippingAddress.getCountryName());
                if (null != shippingAddress.getCountryCode()) {
                    shipToAddress.setCountry(CountryCodeType.valueOf(shippingAddress.getCountryCode()));
                }
                shipToAddress.setPostalCode(shippingAddress.getPostalCode());

                paymentDetailsType.setShipToAddress(shipToAddress);
            }
            paymentDetailsType.setPaymentAction(PaymentActionCodeType.SALE);

            setECRequestDetails.setPaymentDetails(Collections.singletonList(paymentDetailsType));
            setECRequestDetails.setMaxAmount(toBasicAmountType(order.getOrderTotalAmount(), formatter));
        }

        if (targetCreateSubscriptionRequest.isBillingAgreementRequired())
        {
            final BillingAgreementDetailsType billingAgreementDetailsType = new BillingAgreementDetailsType();
            billingAgreementDetailsType
                    .setBillingType(BillingCodeType.MERCHANTINITIATEDBILLINGSINGLEAGREEMENT);
            setECRequestDetails
                    .setBillingAgreementDetails(Collections.singletonList(billingAgreementDetailsType));
        }

        setECRequest.setSetExpressCheckoutRequestDetails(setECRequestDetails);

        final SetExpressCheckoutReq req = new SetExpressCheckoutReq();
        req.setSetExpressCheckoutRequest(setECRequest);
        return req;
    }

    /**
     * @param amount
     *            - {@link AmountType}
     * @param formatter
     *            -{@link NumberFormat}
     * @return {@link BasicAmountType}
     */
    private BasicAmountType toBasicAmountType(final AmountType amount, final NumberFormat formatter)
    {
        if (amount == null)
        {
            return null;
        }

        if (formatter == null)
        {
            throw new IllegalArgumentException("Please provide a formatter");
        }

        final BasicAmountType basicAmountType = new BasicAmountType();
        basicAmountType.setValue(formatter.format(amount.getAmount()));
        basicAmountType.setCurrencyID(CurrencyCodeType.valueOf(amount.getCurrency().getCurrencyCode()));

        return basicAmountType;
    }

    /**
     * @param response
     *            -
     * @return -
     */
    private TargetCreateSubscriptionResult translateResponse(final SetExpressCheckoutResponseType response)
    {
        final TargetCreateSubscriptionResult result = new TargetCreateSubscriptionResult();
        result.setRequestToken(response.getToken());

        result.setReconciliationId(response.getCorrelationID());
        result.setTransactionStatus(PayPal2HybrisResponseMap.getHybrisTransactionStatus(response.getAck()));

        return result;
    }

}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.commands.impl;

import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.GetSubscriptionDataCommand;
import de.hybris.platform.payment.commands.request.SubscriptionDataRequest;
import de.hybris.platform.payment.commands.result.SubscriptionDataResult;
import de.hybris.platform.payment.dto.BillingInfo;

import org.apache.commons.lang.StringUtils;

import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsReq;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsRequestType;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsResponseType;
import urn.ebay.apis.eBLBaseComponents.AddressType;
import au.com.target.tgtpaymentprovider.paypal.exception.PayPalServiceException;



/**
 * @author vmuthura
 * 
 */
public class PayPalGetSubscriptionDataCommandImpl extends AbstractPayPalCommand implements GetSubscriptionDataCommand
{

    @Override
    public SubscriptionDataResult perform(final SubscriptionDataRequest subscriptionDataRequest)
    {
        if (subscriptionDataRequest == null)
        {
            throw new IllegalArgumentException("subscriptionDataRequest is null");
        }

        try
        {
            final GetExpressCheckoutDetailsResponseType response = getPayPalService().getCheckoutDetails(
                    translateRequest(subscriptionDataRequest));

            return translateResponse(response);
        }
        catch (final PayPalServiceException e)
        {
            throw new AdapterException(e);
        }
    }

    private SubscriptionDataResult translateResponse(final GetExpressCheckoutDetailsResponseType response) {
        final SubscriptionDataResult subscriptionDataResult = new SubscriptionDataResult();
        final AddressType billingAddress = response.getGetExpressCheckoutDetailsResponseDetails().getBillingAddress();
        if (billingAddress != null) {
            final BillingInfo billingInfo = new BillingInfo();

            //Paypal just provides a name... We need to split that, though it is not ideal.
            final String[] names = StringUtils.split(billingAddress.getName(), ' ');

            billingInfo.setFirstName(names != null && names.length > 0 ? names[0] : null);
            billingInfo.setLastName(names != null && names.length > 1 ? names[1] : null);
            billingInfo.setEmail(response.getGetExpressCheckoutDetailsResponseDetails().getPayerInfo().getPayer());
            billingInfo.setStreet1(billingAddress.getStreet1());
            billingInfo.setStreet2(billingAddress.getStreet2());
            billingInfo.setState(billingAddress.getStateOrProvince());
            billingInfo.setCity(billingAddress.getCityName());
            billingInfo.setCountry(billingAddress.getCountry().toString());
            billingInfo.setPostalCode(billingAddress.getPostalCode());
            subscriptionDataResult.setBillingInfo(billingInfo);
        }

        return subscriptionDataResult;
    }

    private GetExpressCheckoutDetailsReq translateRequest(final SubscriptionDataRequest subscriptionDataRequest) {
        final GetExpressCheckoutDetailsRequestType requestType = new GetExpressCheckoutDetailsRequestType();
        final String token = subscriptionDataRequest.getSubscriptionID();
        requestType.setToken(token);

        final GetExpressCheckoutDetailsReq req = new GetExpressCheckoutDetailsReq();
        req.setGetExpressCheckoutDetailsRequest(requestType);
        return req;
    }
}

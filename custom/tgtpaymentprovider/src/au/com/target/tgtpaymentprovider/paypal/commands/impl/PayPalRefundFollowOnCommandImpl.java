/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.commands.impl;

import de.hybris.platform.payment.AdapterException;

import java.math.BigDecimal;

import au.com.target.tgtpayment.commands.TargetFollowOnRefundCommand;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpaymentprovider.paypal.exception.PayPalServiceException;
import au.com.target.tgtpaymentprovider.paypal.util.PayPal2HybrisResponseMap;
import au.com.target.tgtpaymentprovider.paypal.util.Util;
import urn.ebay.api.PayPalAPI.RefundTransactionReq;
import urn.ebay.api.PayPalAPI.RefundTransactionRequestType;
import urn.ebay.api.PayPalAPI.RefundTransactionResponseType;
import urn.ebay.apis.CoreComponentTypes.BasicAmountType;
import urn.ebay.apis.eBLBaseComponents.CurrencyCodeType;
import urn.ebay.apis.eBLBaseComponents.RefundType;


/**
 * @author bjames4
 * 
 */
public class PayPalRefundFollowOnCommandImpl extends AbstractPayPalCommand implements TargetFollowOnRefundCommand {

    @Override
    public TargetFollowOnRefundResult perform(final TargetFollowOnRefundRequest request) {
        final RefundTransactionReq refundRequest = translateRequest(request);
        RefundTransactionResponseType response = null;
        try {
            response = getPayPalService().refund(refundRequest);
        }
        catch (final PayPalServiceException ex) {
            throw new AdapterException(ex.getMessage(), ex);
        }

        final TargetFollowOnRefundResult refundResult = new TargetFollowOnRefundResult();

        if (response != null) {
            if (response.getAck() != null
                    && Util.isCaptureSuccessful(response.getAck())) {
                refundResult.setMerchantTransactionCode(response.getRefundTransactionID());
                refundResult.setReconciliationId(response.getCorrelationID());
                refundResult.setRequestToken(response.getMsgSubID());
                if (response.getGrossRefundAmount() != null) {
                    refundResult.setTotalAmount(new BigDecimal(response.getGrossRefundAmount().getValue()));
                }
                refundResult.setRequestId(response.getRefundTransactionID());
            }
            refundResult.setTransactionStatus(PayPal2HybrisResponseMap
                    .getHybrisTransactionStatus(response.getAck()));
            refundResult.setTransactionStatusDetails(PayPal2HybrisResponseMap
                    .getHybrisTransactionStatusDetails(response.getRefundInfo().getRefundStatus()));
        }

        return refundResult;
    }

    /**
     * @param request
     *            -
     * @return -
     */
    private RefundTransactionReq translateRequest(final TargetFollowOnRefundRequest request) {

        final RefundTransactionRequestType refundTransactionReqType = new RefundTransactionRequestType();
        final BasicAmountType amtType = new BasicAmountType();
        final CurrencyCodeType currencyCode = CurrencyCodeType.valueOf(request.getCurrency().toString());

        amtType.setValue(request.getTotalAmount().toString());
        amtType.setCurrencyID(currencyCode);
        refundTransactionReqType.setAmount(amtType);
        refundTransactionReqType.setTransactionID(request.getCaptureTransactionId());

        refundTransactionReqType.setRefundType(request.isPartial() ? RefundType.PARTIAL : RefundType.FULL);

        final RefundTransactionReq req = new RefundTransactionReq();
        req.setRefundTransactionRequest(refundTransactionReqType);
        return req;
    }
}

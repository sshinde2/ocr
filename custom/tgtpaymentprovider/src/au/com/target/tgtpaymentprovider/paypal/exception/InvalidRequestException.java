/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.exception;

/**
 * @author vmuthura
 * 
 */
public class InvalidRequestException extends PayPalServiceException
{
    /**
     * 
     */
    public InvalidRequestException()
    {
        super();
    }

    /**
     * @param message
     *            - message explaining the cause
     */
    public InvalidRequestException(final String message)
    {
        super(message);
    }

    /**
     * @param message
     *            - message explaining the cause
     * @param cause
     *            - the root cause
     */
    public InvalidRequestException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    /**
     * @param cause
     *            - the root cause
     */
    public InvalidRequestException(final Throwable cause)
    {
        super(cause);
    }
}

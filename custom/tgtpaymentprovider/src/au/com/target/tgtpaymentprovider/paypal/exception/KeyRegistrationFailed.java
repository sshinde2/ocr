/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.exception;

/**
 * @author vmuthura
 * 
 */
public class KeyRegistrationFailed extends Exception
{
    /**
     * 
     */
    public KeyRegistrationFailed()
    {
        super();
    }

    /**
     * @param message
     *            - message explaining the cause
     */
    public KeyRegistrationFailed(final String message)
    {
        super(message);
    }

    /**
     * @param throwable
     *            - the root cause
     */
    public KeyRegistrationFailed(final Throwable throwable)
    {
        super(throwable);
    }

    /**
     * @param message
     *            - message explaining the cause
     * @param throwable
     *            - the root cause
     */
    public KeyRegistrationFailed(final String message, final Throwable throwable)
    {
        super(message, throwable);
    }

}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.exception;

/**
 * @author vmuthura
 * 
 */
public class ValidationFailedException extends PayPalServiceException
{
    /**
     * 
     */
    public ValidationFailedException()
    {
        super();
    }

    /**
     * @param message
     *            - message explaining the cause
     */
    public ValidationFailedException(final String message)
    {
        super(message);
    }

    /**
     * @param message
     *            - message explaining the cause
     * @param cause
     *            - the root cause
     */
    public ValidationFailedException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    /**
     * @param cause
     *            - the root cause
     */
    public ValidationFailedException(final Throwable cause)
    {
        super(cause);
    }
}

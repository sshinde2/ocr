package au.com.target.tgtpaymentprovider.paypal.util;



import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.tgtpaymentprovider.paypal.exception.KeyRegistrationFailed;


public final class Keys {
    private static final Map KEYMANAGERS = Collections.synchronizedMap(new HashMap());

    /**
     * 
     */
    private Keys() {
        // prevent construction
    }

    /**
     * @param hashcode
     *            - a unique key
     * @param certificateFile
     *            - a p12 certificate file
     * @param privateKeyPassword
     *            - password for the keystore
     * @throws KeyRegistrationFailed
     *             - if the key registration fails
     */
    public static void registerKey(final String hashcode, final String certificateFile, final String privateKeyPassword)
            throws KeyRegistrationFailed {
        if (StringUtils.isBlank(certificateFile) || StringUtils.isBlank(privateKeyPassword)) {
            throw new IllegalArgumentException("certificatefile/privatekeypassword is missing");
        }

        final KeyManagerFactory kmf;
        try {
            kmf = KeyManagerFactory.getInstance("SunX509");
            final KeyStore keystore = getPKCS12KeyStore(certificateFile, privateKeyPassword);
            kmf.init(keystore, privateKeyPassword.toCharArray());
            registerKeys(hashcode, kmf.getKeyManagers());
        }
        catch (final NoSuchAlgorithmException e) {
            throw new KeyRegistrationFailed(e);
        }
        catch (final KeyStoreException e) {
            throw new KeyRegistrationFailed(e);
        }
        catch (final NoSuchProviderException e) {
            throw new KeyRegistrationFailed(e);
        }
        catch (final CertificateException e) {
            throw new KeyRegistrationFailed(e);
        }
        catch (final IOException e) {
            throw new KeyRegistrationFailed(e);
        }
        catch (final UnrecoverableKeyException e) {
            throw new KeyRegistrationFailed(e);
        }
    }

    /**
     * Registers new key managers with the given identifier. If the key managers with the given ID already exists it
     * will be overridden.
     * 
     * @param keyId
     *            the identifier for this protocol
     * @param keymanagers
     *            the key managers to register
     */
    public static void registerKeys(final String keyId, final KeyManager[] keymanagers) {
        if (keyId == null) {
            throw new IllegalArgumentException("keyId is null");
        }
        if (keymanagers == null) {
            throw new IllegalArgumentException("Key managers is null");
        }
        KEYMANAGERS.put(keyId, keymanagers);
    }

    /**
     * Unregisters the key managers with the given ID.
     * 
     * @param keyId
     *            the ID of the key managers to remove
     */
    public static void unregisterKeys(final String keyId) {
        if (keyId == null) {
            throw new IllegalArgumentException("keyId is null");
        }
        KEYMANAGERS.remove(keyId);
    }

    /**
     * Gets the SSL Context with the given ID.
     * 
     * @param keyId
     *            the SSL context ID
     * @return KeyManager[] the key managers
     * @throws IllegalStateException
     *             if a protocol with the ID cannot be found
     */
    public static KeyManager[] getKeyManagers(final String keyId) throws IllegalStateException {
        if (keyId == null) {
            throw new IllegalArgumentException("keyId is null");
        }
        return (KeyManager[])KEYMANAGERS.get(keyId);
    }

    /**
     * Given a keystore file path and the password, loads it into a {@link KeyStore}. The keystore type should be
     * "PKCS12" and provider is "JSSE"
     * 
     * @param PKCS12Path
     *            - path to the cert files
     * @param password
     *            - keystore password
     * @return KeyStore - PKCS12 KeyStore
     * @throws KeyStoreException
     *             - if there is an exception opening the keystore
     * @throws NoSuchProviderException
     *             - if the provider, SUNJSSE, is not supported
     * @throws NoSuchAlgorithmException
     *             - if the PKCS12 algorithm is not supported
     * @throws CertificateException
     *             - if the certificate is unreadable
     * @throws IOException
     *             - if the certificate/keystore is unreadable
     */
    public static KeyStore getPKCS12KeyStore(final String PKCS12Path, final String password) throws KeyStoreException,
            NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException {
        // Make sure *not* to load the BouncyCastle PKCS12 KeyStore, since
        // it requires the US-only policy jars to be installed in jre/lib/security.
        // This dependency exists for EWP users, but not for users of the API only.
        final KeyStore keystore = KeyStore.getInstance("PKCS12", "SunJSSE");

        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(PKCS12Path);
            keystore.load(inputStream, password.toCharArray());
        }
        finally {
            IOUtils.closeQuietly(inputStream);
        }

        return keystore;
    }
}
package au.com.target.tgtpaymentprovider.paypal.util;



import java.io.IOException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.NumberFormat;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.log4j.Logger;

import urn.ebay.apis.CoreComponentTypes.BasicAmountType;
import urn.ebay.apis.eBLBaseComponents.AckCodeType;
import urn.ebay.apis.eBLBaseComponents.CurrencyCodeType;
import au.com.target.tgtpayment.dto.AmountType;
import au.com.target.tgtpaymentprovider.constants.TgtpaymentproviderConstants;


class RelaxedX509TrustManager implements X509TrustManager
{
    /**
     * @param chain
     *            cert chain
     * @return is trusted
     */
    public boolean checkClientTrusted(final java.security.cert.X509Certificate[] chain)
    {
        return true;
    }

    /**
     * @param chain
     *            cert chain
     * @return is trusted
     */
    public boolean isServerTrusted(final java.security.cert.X509Certificate[] chain)
    {
        return true;
    }

    @Override
    public X509Certificate[] getAcceptedIssuers()//NOPMD
    {
        return null;
    }

    @Override
    public void checkClientTrusted(final java.security.cert.X509Certificate[] chain, final String authType)
    {
        //
    }

    @Override
    public void checkServerTrusted(final java.security.cert.X509Certificate[] chain, final String authType)
    {
        //
    }
}

public class Util
{
    private static final Logger LOG = Logger.getLogger(Util.class);

    /**
     * 
     */
    private Util()
    {//
    }

    /**
     * This method returns the SSLContext object.
     * 
     * @param keymanagers
     *            KeyManager[] The key managers
     * @return SSLContext
     * @throws IOException
     *             if an IOException occurs
     */
    public static SSLContext getSSLContext(final KeyManager[] keymanagers) throws IOException
    {
        try
        {
            final SSLContext ctx = SSLContext.getInstance("SSL"); // TLS, SSLv3, SSL
            final SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            random.setSeed(System.currentTimeMillis());
            if (TgtpaymentproviderConstants.PAYPAL_TRUST_ALL)
            {
                final TrustManager[] trustManager =
                { new RelaxedX509TrustManager() };
                ctx.init(keymanagers, trustManager, random);
            }
            else
            {
                ctx.init(keymanagers, null, random);
            }
            return ctx;
        }
        catch (final Exception e)
        {
            LOG.error(e.getMessage());
            throw new IOException(e.getMessage());//NOPMD
        }
    }

    /**
     * @param ackCode
     *            -
     * @return - true on success
     */
    public static boolean isCaptureSuccessful(final AckCodeType ackCode) {
        boolean result = false;
        if (ackCode == AckCodeType.SUCCESS || ackCode == AckCodeType.SUCCESSWITHWARNING) {
            result = true;
        }
        return result;
    }

    public static BasicAmountType toBasicAmountType(final AmountType amount, final NumberFormat formatter)
    {
        if (amount == null)
        {
            return null;
        }

        if (formatter == null)
        {
            throw new IllegalArgumentException("Please provide a formatter");
        }

        final BasicAmountType basicAmountType = new BasicAmountType();
        basicAmountType.setValue(formatter.format(amount.getAmount()));
        basicAmountType.setCurrencyID(CurrencyCodeType.valueOf(amount.getCurrency().getCurrencyCode()));

        return basicAmountType;
    }
}

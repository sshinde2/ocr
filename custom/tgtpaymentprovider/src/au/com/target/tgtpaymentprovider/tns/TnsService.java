/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns;


import java.math.BigDecimal;
import java.util.Currency;

import au.com.target.tgtpaymentprovider.tns.data.response.CreateSessionResponse;
import au.com.target.tgtpaymentprovider.tns.data.response.PayResponse;
import au.com.target.tgtpaymentprovider.tns.data.response.RefundResponse;
import au.com.target.tgtpaymentprovider.tns.data.response.RetrieveTransactionResponse;
import au.com.target.tgtpaymentprovider.tns.data.response.TokenizeResponse;
import au.com.target.tgtpaymentprovider.tns.data.response.VpcRefundResponse;
import au.com.target.tgtpaymentprovider.tns.exception.TnsServiceException;


/**
 * @author vmuthura
 * 
 */
public interface TnsService {

    /**
     * A single transaction to authorize the payment and transfer funds from the payer's account to your account. For
     * card payments, Pay is a mode where the Authorize and Capture operations are completed at the same time. Pay is
     * the most common type of payment model used by merchants to accept card payments. The Pay model is used when the
     * merchant is allowed to bill the cardholder's account immediately, for example when providing services or goods on
     * the spot.
     * 
     * @param orderId
     *            - unique order id
     * @param transactionId
     *            - unique transaction id
     * @param token
     *            - unique token
     * @param amount
     *            -
     * @param currency
     *            -
     * @return PayTransactionResponse - response
     * @throws TnsServiceException
     *             - as required
     */
    PayResponse pay(final String orderId, final String transactionId, final String token,
            BigDecimal amount,
            Currency currency) throws TnsServiceException;

    /**
     * Request to create a Form Session. A Form Session is used by the Hosted Payment Form service to temporarily store
     * card details so that the merchant does not need to handle these details directly. A Form Session is created using
     * this command so that it is available to be updated directly by the cardholder with their card details. These card
     * details may then be used by the merchant (for payment or another type of operation) by referring to the session.
     * 
     * @return the response from TNS
     * @throws TnsServiceException
     *             -
     */
    CreateSessionResponse createSession() throws TnsServiceException;

    /**
     * Get details of a previously requested transaction from TNS.
     * 
     * @param orderNumber
     *            -
     * @param transactionId
     *            -
     * @return RetrieveTransactionResponse - the response from TNS
     * @throws TnsServiceException
     *             -
     */
    RetrieveTransactionResponse retrieve(final String orderNumber, final String transactionId)
            throws TnsServiceException;

    /**
     * Saves the card information for this session in TNS and provides a token. This token can be used for future
     * payment captures, for the same card.
     * 
     * @param session
     *            -
     * @return the response from TNS
     * @throws TnsServiceException
     *             -
     */
    TokenizeResponse tokenize(final String session) throws TnsServiceException;

    /**
     * @param orderId
     *            -
     * @param transactionId
     *            -
     * @param captureTransactionId
     *            -
     * @param token
     *            -
     * @param amount
     *            -
     * @param currency
     *            -
     * @return -
     * @throws TnsServiceException
     *             -
     */
    RefundResponse refund(final String orderId, final String transactionId, final String captureTransactionId,
            final String token,
            final BigDecimal amount, final Currency currency)
            throws TnsServiceException;


    /**
     * Return funds to a card, but it could be more than we originally charged the card.
     * 
     * @param transactionId
     *            - unique identifier for this transaction
     * @param originalTransactionId
     *            - transaction id from the original transaction
     * @param amount
     *            - how much we are giving the customer
     * @param currency
     *            - probably AUD
     * @return -
     * @throws TnsServiceException
     *             -
     */
    VpcRefundResponse excessRefund(final String transactionId, final String originalTransactionId,
            final BigDecimal amount, final Currency currency) throws TnsServiceException;

}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns;

import au.com.target.tgtpaymentprovider.tns.data.Request;
import au.com.target.tgtpaymentprovider.tns.exception.InvalidRequestException;
import au.com.target.tgtpaymentprovider.tns.exception.RequestRejectedException;
import au.com.target.tgtpaymentprovider.tns.exception.TnsServerException;



/**
 * @author vmuthura
 * 
 */
public interface TnsWebOperations {

    /**
     * @param url
     *            - Service endpoint URL
     * @param responseType
     *            - The type of response, to be marshalled into
     * @param uriVariables
     *            - The variables to be replaced in the uri - in that order
     * @return - instance of the passed in response type
     * @throws RequestRejectedException
     *             - if the service rejects the request
     * @throws InvalidRequestException
     *             - if the details required are incomplete or unavailable
     * @throws TnsServerException
     *             - if the service is not available or busy
     */
    //CHECKSTYLE:OFF
    <T> T get(String url, Class<T> responseType, Object... uriVariables) throws RequestRejectedException,
            InvalidRequestException,
            TnsServerException;

    //CHECKSTYLE:ON
    /**
     * @param url
     *            - Service endpoint URL
     * @param request
     *            - Request details
     * @param responseType
     *            - The type of response, to be marshalled into
     * @param uriVariables
     *            - The variables to be replaced in the uri - in that order
     * @return - instance of the passed in response type
     * @throws RequestRejectedException
     *             - if the service rejects the request
     * @throws InvalidRequestException
     *             - if the details required are incomplete or unavailable
     * @throws TnsServerException
     *             - if the service is not available or busy
     */
    //CHECKSTYLE:OFF
    <T> T post(String url, Request request, Class<T> responseType, Object... uriVariables)
            throws RequestRejectedException,
            InvalidRequestException,
            TnsServerException;

    //CHECKSTYLE:ON
    /**
     * @param url
     *            - Service endpoint URL
     * @param request
     *            - Request details
     * @param responseType
     *            - The type of response, to be marshalled into
     * @param uriVariables
     *            - The variables to be replaced in the uri - in that order
     * @return - instance of the passed in response type
     * @throws RequestRejectedException
     *             - if the service rejects the request
     * @throws InvalidRequestException
     *             - if the details required are incomplete or unavailable
     * @throws TnsServerException
     *             - if the service is not available or busy
     */
    //CHECKSTYLE:OFF
    <T> T put(String url, Request request, Class<T> responseType, Object... uriVariables)
            throws RequestRejectedException,
            InvalidRequestException,
            TnsServerException;

    //CHECKSTYLE:ON
    /**
     * @param url
     *            - Service endpoint URL
     * @param request
     *            - Request details
     * @param responseType
     *            - The type of response, to be marshalled into
     * @param uriVariables
     *            - The variables to be replaced in the uri - in that order
     * @return - instance of the passed in response type
     * @throws RequestRejectedException
     *             - if the service rejects the request
     * @throws InvalidRequestException
     *             - if the details required are incomplete or unavailable
     * @throws TnsServerException
     *             - if the service is not available or busy
     */
    //CHECKSTYLE:OFF
    <T> T delete(String url, Request request, Class<T> responseType, Object... uriVariables)
            throws RequestRejectedException,
            InvalidRequestException,
            TnsServerException;
    //CHECKSTYLE:ON
}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.commands.impl;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtpaymentprovider.tns.TnsService;


/**
 * @author vmuthura
 * 
 */
public class AbstractTnsCommand {

    private TnsService tnsService;

    /**
     * @return the tnsService
     */
    public TnsService getTnsService() {
        return tnsService;
    }

    /**
     * @param tnsService
     *            the tnsService to set
     */
    @Required
    public void setTnsService(final TnsService tnsService) {
        this.tnsService = tnsService;
    }

}

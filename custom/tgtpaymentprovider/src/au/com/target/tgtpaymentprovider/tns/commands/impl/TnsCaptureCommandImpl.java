/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.commands.impl;

import de.hybris.platform.payment.AdapterException;

import java.math.BigDecimal;
import java.util.Currency;

import au.com.target.tgtpayment.commands.TargetCaptureCommand;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpaymentprovider.tns.data.ResultEnum;
import au.com.target.tgtpaymentprovider.tns.data.response.GenericTransactionResponse;
import au.com.target.tgtpaymentprovider.tns.exception.TnsServiceException;
import au.com.target.tgtpaymentprovider.tns.util.Tns2HybrisResponseMap;


/**
 * @author bjames4
 * 
 */
public class TnsCaptureCommandImpl extends AbstractTnsCommand implements TargetCaptureCommand {

    @Override
    public TargetCaptureResult perform(final TargetCaptureRequest captureRequest) {

        if (captureRequest == null)
        {
            throw new IllegalArgumentException("TargetCaptureRequest is null");
        }

        final String orderId = captureRequest.getOrderId();
        final String transactionId = captureRequest.getTransactionId();
        final String token = captureRequest.getToken();
        final BigDecimal amount = captureRequest.getTotalAmount();
        final Currency currency = captureRequest.getCurrency();
        GenericTransactionResponse response = null;

        try {
            response = getTnsService().pay(orderId, transactionId, token, amount, currency);
        }
        catch (final TnsServiceException ex) {
            throw new AdapterException(ex.getMessage(), ex);
        }

        final TargetCaptureResult captureResult = new TargetCaptureResult();
        if (response != null)
        {
            // Success
            if (response.getResult() != null && response.getResult().equals(ResultEnum.SUCCESS))
            {
                captureResult.setMerchantTransactionCode(transactionId);
                captureResult.setReconciliationId(response.getTransaction().getReceipt());
                captureResult.setRequestToken(token);
                captureResult.setRequestId(response.getTransaction().getId());
                captureResult.setTotalAmount(new BigDecimal(response.getTransaction().getAmount()));
                captureResult.setRequestTime(response.getTimeOfRecord());
            }

            captureResult.setTransactionStatus(Tns2HybrisResponseMap.getHybrisTransactionStatus(response.getResult()));
            captureResult.setTransactionStatusDetails(Tns2HybrisResponseMap.getHybrisTransactionStatusDetails(response
                    .getResponse().getGatewayCode()));
        }

        return captureResult;
    }
}

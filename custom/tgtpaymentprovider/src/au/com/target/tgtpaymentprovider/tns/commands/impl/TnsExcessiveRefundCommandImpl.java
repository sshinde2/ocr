/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.commands.impl;

import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.TransactionStatus;

import java.math.BigDecimal;
import java.util.Currency;

import au.com.target.tgtpayment.commands.TargetExcessiveRefundCommand;
import au.com.target.tgtpayment.commands.request.TargetExcessiveRefundRequest;
import au.com.target.tgtpayment.commands.result.TargetExcessiveRefundResult;
import au.com.target.tgtpaymentprovider.tns.data.VpcTransactionResponseEnum;
import au.com.target.tgtpaymentprovider.tns.data.response.VpcRefundResponse;
import au.com.target.tgtpaymentprovider.tns.exception.TnsServiceException;
import au.com.target.tgtpaymentprovider.tns.util.Vpc2HybrisResponseMap;


/**
 * @author bjames4
 * 
 */
public class TnsExcessiveRefundCommandImpl extends AbstractTnsCommand implements TargetExcessiveRefundCommand {

    @Override
    public TargetExcessiveRefundResult perform(final TargetExcessiveRefundRequest refundRequest) {

        final BigDecimal toCents = BigDecimal.valueOf(100);

        if (refundRequest == null)
        {
            throw new IllegalArgumentException("TargetExcessiveRefundRequest is null");
        }

        VpcRefundResponse response = null;
        final String transactionId = refundRequest.getTransactionId();
        final BigDecimal amount = refundRequest.getTotalAmount().multiply(toCents);//Convert the amount to cents for vpc
        final Currency currency = refundRequest.getCurrency();
        final String originaltransactionId = refundRequest.getOriginalTransactionId();

        try {
            response = getTnsService().excessRefund(transactionId, originaltransactionId, amount, currency);
        }
        catch (final TnsServiceException ex) {
            throw new AdapterException(ex.getMessage(), ex);
        }

        final TargetExcessiveRefundResult refundResult = new TargetExcessiveRefundResult();

        // Success
        if (response.getTransactionResponseCode().equals(VpcTransactionResponseEnum.SUCCESS)) {
            refundResult.setMerchantTransactionCode(transactionId);
            refundResult.setReconciliationId(response.getReceiptNumber());
            refundResult.setRequestToken(response.getVpcTransactionNumber());
            refundResult.setRequestId(response.getMerchTxnRef());
            refundResult.setTotalAmount(new BigDecimal(response.getAmount()).divide(toCents));//Convert the amount back from cents
            refundResult.setTransactionStatus(TransactionStatus.ACCEPTED);
        }

        refundResult.setTransactionStatusDetails(Vpc2HybrisResponseMap.getHybrisTransactionStatusDetails(response
                .getTransactionResponseCode()));

        return refundResult;
    }
}

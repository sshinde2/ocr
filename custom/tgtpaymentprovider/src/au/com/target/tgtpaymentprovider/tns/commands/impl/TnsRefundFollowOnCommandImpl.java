/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.commands.impl;

import de.hybris.platform.payment.AdapterException;

import java.math.BigDecimal;
import java.util.Currency;

import au.com.target.tgtpayment.commands.TargetFollowOnRefundCommand;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpaymentprovider.tns.data.ResultEnum;
import au.com.target.tgtpaymentprovider.tns.data.response.GenericTransactionResponse;
import au.com.target.tgtpaymentprovider.tns.exception.TnsServiceException;
import au.com.target.tgtpaymentprovider.tns.util.Tns2HybrisResponseMap;


/**
 * @author bjames4
 * 
 */
public class TnsRefundFollowOnCommandImpl extends AbstractTnsCommand implements TargetFollowOnRefundCommand {

    @Override
    public TargetFollowOnRefundResult perform(final TargetFollowOnRefundRequest refundRequest) {

        if (refundRequest == null) {
            throw new IllegalArgumentException("TargetFollowOnRefundRequest is null");
        }

        final String orderId = refundRequest.getOrderId();
        final String token = refundRequest.getToken();
        final String transactionId = refundRequest.getTransactionId();
        final String capturetransactionId = refundRequest.getCaptureTransactionId();
        final BigDecimal amount = refundRequest.getTotalAmount();
        final Currency currency = refundRequest.getCurrency();
        GenericTransactionResponse response = null;

        try {
            response = getTnsService().refund(orderId, transactionId, capturetransactionId, token, amount, currency);
        }
        catch (final TnsServiceException ex) {
            throw new AdapterException(ex.getMessage(), ex);
        }

        final TargetFollowOnRefundResult refundResult = new TargetFollowOnRefundResult();

        // Success
        if (response != null) {
            if (response.getTransaction() != null
                    && response.getResult().equals(ResultEnum.SUCCESS)) {
                refundResult.setMerchantTransactionCode(transactionId);
                refundResult.setReconciliationId(response.getTransaction().getReceipt());
                refundResult.setRequestToken(response.getSession());
                refundResult.setRequestId(response.getTransaction().getId());
                refundResult.setTotalAmount(new BigDecimal(response.getTransaction().getAmount()));
                refundResult.setRequestTime(response.getTimeOfRecord());
            }
            refundResult.setTransactionStatus(Tns2HybrisResponseMap.getHybrisTransactionStatus(response.getResult()));
            refundResult.setTransactionStatusDetails(Tns2HybrisResponseMap.getHybrisTransactionStatusDetails(response
                    .getResponse().getGatewayCode()));
        }
        return refundResult;
    }
}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.commands.impl;

import de.hybris.platform.payment.AdapterException;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtpayment.commands.TargetRetrieveTransactionCommand;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;
import au.com.target.tgtpaymentprovider.tns.data.ResultEnum;
import au.com.target.tgtpaymentprovider.tns.data.response.RetrieveTransactionResponse;
import au.com.target.tgtpaymentprovider.tns.exception.TnsServiceException;
import au.com.target.tgtpaymentprovider.tns.util.Tns2HybrisResponseMap;


/**
 * @author vmuthura
 * 
 */
public class TnsRetrieveTransactionCommandImpl extends AbstractTnsCommand implements TargetRetrieveTransactionCommand
{

    @Override
    public TargetRetrieveTransactionResult perform(
            final TargetRetrieveTransactionRequest targetRetrieveTransactionRequest)
    {
        if (targetRetrieveTransactionRequest == null)
        {
            throw new IllegalArgumentException("TargetRetrieveTransactionRequest is null");
        }

        if (StringUtils.isBlank(targetRetrieveTransactionRequest.getOrderId()))
        {
            throw new IllegalArgumentException("Please provide orderId");
        }

        if (StringUtils.isBlank(targetRetrieveTransactionRequest.getTransactionId()))
        {
            throw new IllegalArgumentException("Please provide transactionId");
        }

        TargetRetrieveTransactionResult result = null;

        try
        {
            final RetrieveTransactionResponse response = getTnsService().retrieve(
                    targetRetrieveTransactionRequest.getOrderId(),
                    targetRetrieveTransactionRequest.getTransactionId());

            result = new TargetRetrieveTransactionResult();
            result.setTransactionStatus(Tns2HybrisResponseMap.getHybrisTransactionStatus(response.getResult()));
            result.setTransactionStatusDetails(Tns2HybrisResponseMap.getHybrisTransactionStatusDetails(response
                    .getResponse().getGatewayCode()));

            if (response.getResult() != null
                    && response.getResult().equals(ResultEnum.SUCCESS) && response.getTransaction() != null) {
                result.setMerchantTransactionCode(targetRetrieveTransactionRequest.getTransactionId());
                result.setReconciliationId(response.getTransaction().getReceipt());
                result.setRequestId(response.getTransaction().getId());
                result.setRequestToken(response.getSession());
                result.setTotalAmount(new BigDecimal(response.getTransaction().getAmount()));
            }
        }
        catch (final TnsServiceException e)
        {
            throw new AdapterException(e);
        }

        return result;
    }
}

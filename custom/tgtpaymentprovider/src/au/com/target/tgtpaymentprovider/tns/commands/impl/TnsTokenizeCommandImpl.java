/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.commands.impl;

import de.hybris.platform.payment.AdapterException;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import au.com.target.tgtpayment.commands.TargetTokenizeCommand;
import au.com.target.tgtpayment.commands.request.TargetTokenizeRequest;
import au.com.target.tgtpayment.commands.result.TargetTokenizeResult;
import au.com.target.tgtpaymentprovider.tns.data.ResultEnum;
import au.com.target.tgtpaymentprovider.tns.data.response.TokenizeResponse;
import au.com.target.tgtpaymentprovider.tns.exception.TnsServiceException;
import au.com.target.tgtpaymentprovider.tns.util.Tns2HybrisResponseMap;


/**
 * @author vmuthura
 * 
 */
public class TnsTokenizeCommandImpl extends AbstractTnsCommand implements TargetTokenizeCommand {

    @Override
    public TargetTokenizeResult perform(final TargetTokenizeRequest tokenizeRequest)
    {
        Assert.notNull(tokenizeRequest, "TokenizeRequest is null");
        if (StringUtils.isBlank(tokenizeRequest.getSessionToken()))
        {
            throw new IllegalArgumentException("Session token is null or empty");
        }

        TargetTokenizeResult targetTokenizeResult = null;
        try
        {
            final TokenizeResponse response = getTnsService().tokenize(tokenizeRequest.getSessionToken());

            targetTokenizeResult = new TargetTokenizeResult();
            targetTokenizeResult.setTransactionStatus(Tns2HybrisResponseMap.getHybrisTransactionStatus(response
                    .getResult()));
            targetTokenizeResult.setTransactionStatusDetails(Tns2HybrisResponseMap
                    .getHybrisTransactionStatusDetails(response.getResponse().getGatewayCode()));

            if (response.getResult() != null
                    && response.getResult().equals(ResultEnum.SUCCESS))
            {
                targetTokenizeResult.setSavedToken(response.getToken());
            }
        }
        catch (final TnsServiceException e)
        {
            throw new AdapterException(e);
        }

        return targetTokenizeResult;
    }

}

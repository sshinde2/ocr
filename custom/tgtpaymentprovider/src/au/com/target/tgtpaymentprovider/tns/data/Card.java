/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data;

import javax.validation.constraints.Size;


/**
 * @author bjames4
 * 
 */
public class Card {

    @Size(min = 9, max = 19, message = "Card number number must be between 9 and 19 digits")
    private String number;

    @Size(min = 3, max = 4, message = "Enter a valid security code")
    private String securityCode;

    private CardExpiry expiry;

    private String scheme;

    /**
     * @return the cardNumber
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number
     *            the cardNumber to set
     */
    public void setNumber(final String number) {
        this.number = number;
    }

    /**
     * @return the securityCode
     */
    public String getSecurityCode() {
        return securityCode;
    }

    /**
     * @param securityCode
     *            the securityCode to set
     */
    public void setSecurityCode(final String securityCode) {
        this.securityCode = securityCode;
    }

    /**
     * @return the expiry
     */
    public CardExpiry getExpiry() {
        return expiry;
    }

    /**
     * @param expiry
     *            the expiry to set
     */
    public void setExpiry(final CardExpiry expiry) {
        this.expiry = expiry;
    }

    /**
     * @return the scheme
     */
    public String getScheme() {
        return scheme;
    }

    /**
     * @param scheme
     *            the scheme to set
     */
    public void setScheme(final String scheme) {
        this.scheme = scheme;
    }
}

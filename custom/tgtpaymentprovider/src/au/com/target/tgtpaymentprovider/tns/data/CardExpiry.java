/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data;

import javax.validation.constraints.Size;


/**
 * @author bjames4
 * 
 */
public class CardExpiry {

    @Size(min = 1, max = 2, message = "Enter a two digit month code")
    private String month;

    @Size(min = 1, max = 2, message = "Enter a two digit year code post 2000")
    private String year;

    /**
     * @return the month
     */
    public String getMonth() {
        return month;
    }

    /**
     * @param month
     *            the month to set
     */
    public void setMonth(final String month) {
        this.month = month;
    }

    /**
     * @return the year
     */
    public String getYear() {
        return year;
    }

    /**
     * @param year
     *            the year to set
     */
    public void setYear(final String year) {
        this.year = year;
    }
}

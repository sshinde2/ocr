/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data;

import javax.validation.constraints.Size;


/**
 * @author bjames4
 * 
 */
public class Order { //NOPMD
    private double amount;

    @Size(min = 3, max = 3, message = "currency - three character currency code")
    private String currency;

    @Size(min = 1, max = 127, message = "description - between 1 and 127 character description")
    private String description;

    private double itemAmount;
    private double shippingAndHandlingAmount;
    private double taxAmount;

    @Size(min = 1, max = 40, message = "id - between 1 and 40 characters")
    private String id;

    @Size(min = 1, max = 200, message = "reference - between 1 and 200 characters")
    private String reference;

    @Size(min = 1, max = 15, message = "productSKU - between 1 and 15 characters")
    private String productSKU;

    private double totalAuthorizedAmount;
    private double totalCapturedAmount;
    private double totalRefundedAmount;

    @Size(min = 10, max = 10, message = "customerOrderDate - yyyy-mm-dd")
    private String customerOrderDate;

    @Size(min = 0, max = 25, message = "customerReference - between 0 and 25 ASCII characters")
    private String customerReference;

    @Size(min = 1, max = 100, message = "requestorName - between 1 and 100 characters")
    private String requestorName;

    @Size(min = 1, max = 20, message = "requestorName - between 1 and 20 characters")
    private String taxRegistrationId;

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final double amount) {
        this.amount = amount;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency
     *            the currency to set
     */
    public void setCurrency(final String currency) {
        this.currency = currency;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return the itemAmount
     */
    public double getItemAmount() {
        return itemAmount;
    }

    /**
     * @param itemAmount
     *            the itemAmount to set
     */
    public void setItemAmount(final double itemAmount) {
        this.itemAmount = itemAmount;
    }

    /**
     * @return the shippingAndHandlingAmount
     */
    public double getShippingAndHandlingAmount() {
        return shippingAndHandlingAmount;
    }

    /**
     * @param shippingAndHandlingAmount
     *            the shippingAndHandlingAmount to set
     */
    public void setShippingAndHandlingAmount(final double shippingAndHandlingAmount) {
        this.shippingAndHandlingAmount = shippingAndHandlingAmount;
    }

    /**
     * @return the taxAmount
     */
    public double getTaxAmount() {
        return taxAmount;
    }

    /**
     * @param taxAmount
     *            the taxAmount to set
     */
    public void setTaxAmount(final double taxAmount) {
        this.taxAmount = taxAmount;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference
     *            the reference to set
     */
    public void setReference(final String reference) {
        this.reference = reference;
    }

    /**
     * @return the productSKU
     */
    public String getProductSKU() {
        return productSKU;
    }

    /**
     * @param productSKU
     *            the productSKU to set
     */
    public void setProductSKU(final String productSKU) {
        this.productSKU = productSKU;
    }

    /**
     * @return the totalAuthorizedAmount
     */
    public double getTotalAuthorizedAmount() {
        return totalAuthorizedAmount;
    }

    /**
     * @param totalAuthorizedAmount
     *            the totalAuthorizedAmount to set
     */
    public void setTotalAuthorizedAmount(final double totalAuthorizedAmount) {
        this.totalAuthorizedAmount = totalAuthorizedAmount;
    }

    /**
     * @return the totalCapturedAmount
     */
    public double getTotalCapturedAmount() {
        return totalCapturedAmount;
    }

    /**
     * @param totalCapturedAmount
     *            the totalCapturedAmount to set
     */
    public void setTotalCapturedAmount(final double totalCapturedAmount) {
        this.totalCapturedAmount = totalCapturedAmount;
    }

    /**
     * @return the totalRefundAmount
     */
    public double getTotalRefundedAmount() {
        return totalRefundedAmount;
    }

    /**
     * @param totalRefundAmount
     *            the totalRefundAmount to set
     */
    public void setTotalRefundedAmount(final double totalRefundAmount) {
        this.totalRefundedAmount = totalRefundAmount;
    }

    /**
     * @return the customerOrderDate
     */
    public String getCustomerOrderDate() {
        return customerOrderDate;
    }

    /**
     * @param customerOrderDate
     *            the customerOrderDate to set
     */
    public void setCustomerOrderDate(final String customerOrderDate) {
        this.customerOrderDate = customerOrderDate;
    }

    /**
     * @return the customerReference
     */
    public String getCustomerReference() {
        return customerReference;
    }

    /**
     * @param customerReference
     *            the customerReference to set
     */
    public void setCustomerReference(final String customerReference) {
        this.customerReference = customerReference;
    }

    /**
     * @return the requestorName
     */
    public String getRequestorName() {
        return requestorName;
    }

    /**
     * @param requestorName
     *            the requestorName to set
     */
    public void setRequestorName(final String requestorName) {
        this.requestorName = requestorName;
    }

    /**
     * @return the taxRegistrationId
     */
    public String getTaxRegistrationId() {
        return taxRegistrationId;
    }

    /**
     * @param taxRegistrationId
     *            the taxRegistrationId to set
     */
    public void setTaxRegistrationId(final String taxRegistrationId) {
        this.taxRegistrationId = taxRegistrationId;
    }

}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data;

/**
 * @author bjames4
 * 
 */
public class ResponseDetails {

    private String acquirerMessage;
    private String acquirerCode;
    private ResultEnum result;
    private String debugInformation;
    private GatewayResponseEnum gatewayCode;

    /**
     * @return the acquirerMessage
     */
    public String getAcquirerMessage() {
        return acquirerMessage;
    }

    /**
     * @param acquirerMessage
     *            the acquirerMessage to set
     */
    public void setAcquirerMessage(final String acquirerMessage) {
        this.acquirerMessage = acquirerMessage;
    }

    /**
     * @return the acquirerCode
     */
    public String getAcquirerCode() {
        return acquirerCode;
    }

    /**
     * @param acquirerCode
     *            the acquirerCode to set
     */
    public void setAcquirerCode(final String acquirerCode) {
        this.acquirerCode = acquirerCode;
    }

    /**
     * @return the gatewayCode
     */
    public GatewayResponseEnum getGatewayCode() {
        return gatewayCode;
    }

    /**
     * @param gatewayCode
     *            the gatewayCode to set
     */
    public void setGatewayCode(final GatewayResponseEnum gatewayCode) {
        this.gatewayCode = gatewayCode;
    }

    /**
     * @return the result
     */
    public ResultEnum getResult() {
        return result;
    }

    /**
     * @param result
     *            the result to set
     */
    public void setResult(final ResultEnum result) {
        this.result = result;
    }

    /**
     * @return the debugInformation
     */
    public String getDebugInformation() {
        return debugInformation;
    }

    /**
     * @param debugInformation
     *            the debugInformation to set
     */
    public void setDebugInformation(final String debugInformation) {
        this.debugInformation = debugInformation;
    }
}

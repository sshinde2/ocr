/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data;

/**
 * @author bjames4
 * 
 */
public class ShippingDetails {

    private AddressDetails address;
    private String firstName;
    private String lastName;
    private ShippingMethod method;
    private String phone;

    /**
     * @return the address
     */
    public AddressDetails getAddress() {
        return address;
    }

    /**
     * @param address
     *            the address to set
     */
    public void setAddress(final AddressDetails address) {
        this.address = address;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the method
     */
    public ShippingMethod getMethod() {
        return method;
    }

    /**
     * @param method
     *            the method to set
     */
    public void setMethod(final ShippingMethod method) {
        this.method = method;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     *            the phone to set
     */
    public void setPhone(final String phone) {
        this.phone = phone;
    }

}

enum ShippingMethod {
    SAME_DAY, //Same day.
    OVERNIGHT, //Overnight (next day).
    PRIORITY, //Priority (2-3 days).
    GROUND, //Ground (4 or more days).
    ELECTRONIC //Electronic delivery.    
}
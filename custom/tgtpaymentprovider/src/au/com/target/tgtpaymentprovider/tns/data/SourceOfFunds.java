/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data;

import javax.validation.constraints.Size;


/**
 * @author bjames4
 * 
 */
public class SourceOfFunds {

    private SourceOfFundDetails provided;

    @Size(min = 31, max = 25, message = "session - ASCII between 31 and 35 characters")
    private String session;

    @Size(min = 1, max = 40, message = "token - Alphanumeric min 1, max 40 characters")
    private String token;

    private PaymentType type;
    private Card card;

    /**
     * @return the sourceOfFundsDetails
     */
    public SourceOfFundDetails getProvided() {
        return provided;
    }

    /**
     * @param provided
     *            the sourceOfFundsDetails to set
     */
    public void setProvided(final SourceOfFundDetails provided) {
        this.provided = provided;
    }

    /**
     * @return the type
     */
    public PaymentType getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final PaymentType type) {
        this.type = type;
    }

    /**
     * @return the session
     */
    public String getSession() {
        return session;
    }

    /**
     * @param session
     *            the session to set
     */
    public void setSession(final String session) {
        this.session = session;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token
     *            the token to set
     */
    public void setToken(final String token) {
        this.token = token;
    }

    /**
     * @return the card
     */
    public Card getCard() {
        return card;
    }

    /**
     * @param card
     *            the card to set
     */
    public void setCard(final Card card) {
        this.card = card;
    }
}

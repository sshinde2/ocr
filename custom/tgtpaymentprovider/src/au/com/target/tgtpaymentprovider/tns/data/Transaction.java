/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data;

import javax.validation.constraints.Size;


/**
 * @author bjames4
 * 
 */
public class Transaction {

    private String authorizationCode;
    private TransactionFrequency frequency;
    private String receipt;
    private Acquirer acquirer;
    private String amount;
    private Long batch;
    private String source;
    private String terminal;
    private String targetTransactionId;

    @Size(min = 3, max = 3, message = "currency - 3 character currency code")
    private String currency;

    @Size(min = 1, max = 40, message = "id - between 1 and 40 characters")
    private String id;

    @Size(min = 1, max = 40, message = "reference - between 1 and 40 characters")
    private String reference;

    private TransactionType type;

    /**
     * @return the acquirer
     */
    public Acquirer getAcquirer() {
        return acquirer;
    }

    /**
     * @param acquirer
     *            the acquirer to set
     */
    public void setAcquirer(final Acquirer acquirer) {
        this.acquirer = acquirer;
    }

    /**
     * @return amount the amount to set
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount
     *            amount
     */
    public void setAmount(final String amount) {
        this.amount = amount;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency
     *            the currency to set
     */
    public void setCurrency(final String currency) {
        this.currency = currency;
    }

    /**
     * @return the frequency
     */
    public TransactionFrequency getFrequency() {
        return frequency;
    }

    /**
     * @param frequency
     *            the frequency to set
     */
    public void setFrequency(final TransactionFrequency frequency) {
        this.frequency = frequency;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference
     *            the reference to set
     */
    public void setReference(final String reference) {
        this.reference = reference;
    }

    /**
     * @return the type
     */
    public TransactionType getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final TransactionType type) {
        this.type = type;
    }

    /**
     * @return the authorizationCode
     */
    public String getAuthorizationCode() {
        return authorizationCode;
    }

    /**
     * @param authorizationCode
     *            the authorizationCode to set
     */
    public void setAuthorizationCode(final String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    /**
     * @return the receipt
     */
    public String getReceipt() {
        return receipt;
    }

    /**
     * @param receipt
     *            the receipt to set
     */
    public void setReceipt(final String receipt) {
        this.receipt = receipt;
    }

    /**
     * @return the batch
     */
    public Long getBatch() {
        return batch;
    }

    /**
     * @param batch
     *            the batch to set
     */
    public void setBatch(final Long batch) {
        this.batch = batch;
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source
     *            the source to set
     */
    public void setSource(final String source) {
        this.source = source;
    }

    /**
     * @return the terminal
     */
    public String getTerminal() {
        return terminal;
    }

    /**
     * @param terminal
     *            the terminal to set
     */
    public void setTerminal(final String terminal) {
        this.terminal = terminal;
    }

    /**
     * @return the targetTransactionId
     */
    public String getTargetTransactionId() {
        return targetTransactionId;
    }

    /**
     * @param targetTransactionId
     *            the targetTransactionId to set
     */
    public void setTargetTransactionId(final String targetTransactionId) {
        this.targetTransactionId = targetTransactionId;
    }

}

enum TransactionFrequency {
    SINGLE, INSTALLMENT, RECURRING;
}

enum TransactionType {

    AUTHORIZATION, //Authorization
    AUTHORIZATION_UPDATE, //Authorization Update
    BALANCE_ENQUIRY, //Balance Enquiry
    CAPTURE, //Capture
    CREDIT_PAYMENT, //Credit Payment
    ORDER_AGREEMENT, //Order Agreement
    PRE_AUTHORIZATION, //Pre-Authorization
    PAYMENT, //Payment (Purchase)
    REFUND, //Refund
    VOID_AUTHORIZATION, //Void Authorization
    VOID_CAPTURE, //Void Capture
    VOID_CREDIT_PAYMENT, //Void Credit Payment
    VOID_PAYMENT, //Void Payment
    VOID_REFUND, //Void Refund
    VERIFICATION, //Verification
    OTHER //Other transaction types    

}

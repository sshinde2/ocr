/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data;

/**
 * @author vmuthura
 * 
 */
public enum ValidationTypeEnum
{
    INVALID, //The request contained a field with a value that did not pass validation.
    MISSING, //The request was missing a mandatory field.
    UNSUPPORTED //The request contained a field that is unsupported.    
}
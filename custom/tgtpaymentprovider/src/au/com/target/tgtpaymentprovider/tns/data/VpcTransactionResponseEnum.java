/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;


/**
 * @author vmuthura
 * 
 */
public enum VpcTransactionResponseEnum
{
    UNKNOWN("?", "Response Unknown"),
    SUCCESS("0", "Transaction Successful"),
    TXN_NOT_PROCESSED("1", "Transaction could not be processed"),
    TXN_DECLINED_CONTACT_BANK("2", "Transaction Declined - Contact Issuing Bank"),
    TXN_DECLINED_NO_REPLY("3", "Transaction Declined - No Reply from Bank"),
    TXN_DECLINED_CARD_EXPIRED("4", "Transaction Declined - Expired Card"),
    TXN_DECLINED_INSUFFICIENT_FUNDS("5", "Transaction Declined - Insufficient funds"),
    TXN_DECLINED_BANK_ERROR("6", "Transaction Declined - Bank system error"),
    INVALID_DATA("7", "Payment Server Processing Error - Invalid input data"),
    TXN_DECLINED_TXN_NOT_SUPPORTED("8", "Transaction Declined - Transaction Type Not Supported"),
    BANK_DECLINED("9", "Bank Declined Transaction (Do not contact Bank)"),
    TXN_ABORTED("A", "Transaction Aborted"),
    TXN_BLOCKED("B", "Transaction Blocked"),
    TXN_CANCELLED("C", "Transaction Cancelled"),
    TXN_DEFFERED("D", "Deferred Transaction"),
    TXN_DECLINED_CONTACT_CARD_ISSUER("E", "Transaction Declined - Refer to card issuer"),
    SECURE_3DS_FAILED("F", "3D Secure Authentication Failed"),
    CARD_SECURITY_CODE_FAILED("I", "Card Security Code Failed"),
    TXN_LOCKED("L", "Shopping Transaction Locked"),
    TXN_SUBMITTED("M", "Transaction Submitted"),
    NOT_ENROLLED_3DS("N", "Cardholder is not enrolled in 3D Secure "),
    TXN_PENDING("P", "Transaction is Pending"),
    RETRY_LIMIT_EXCEEDED("R", "Retry Limits Exceeded, Transaction Not Processed"),
    TXN_DECLINED_DUPLICATE_BATCH("S", "Transaction Declined - Duplicate Batch"),
    ADDR_VERIFICATION_FAILED("T", "Address Verification Failed"),
    CSC_FAILED("U", "Card Security Code Failed"),
    ADDR_VERIFICATION_CSC_FAILED("V", "Address Verification and Card Security Code Failed"),
    TXN_DECLINED_PAYMENT_PLAN("W", "Transaction Declined - Payment Plan not supported"),
    APPROVED_PENDING_SETTLEMENT("X", "Approved pending settlement");

    private final String code;
    private final String description;

    /**
     * @param code
     *            - vpc_TxnResponseCode from VPC
     * @param description
     *            - The detailed message for the vpc_TxnResponseCode code
     */
    private VpcTransactionResponseEnum(final String code, final String description)
    {
        this.code = code;
        this.description = description;
    }

    /**
     * @return - The detailed message for the vpc_TxnResponseCode code
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @return - vpc_TxnResponseCode from VPC
     */
    @JsonValue
    public String getCode()
    {
        return code;
    }

    @Override
    public String toString()
    {
        return code;
    }

    /**
     * @param code
     *            vpc_TxnResponseCode from VPC
     * @return the {@link VpcTransactionResponseEnum} for the code
     */
    @JsonCreator
    public static VpcTransactionResponseEnum fromCode(final String code)
    {
        for (final VpcTransactionResponseEnum type : VpcTransactionResponseEnum.values()) {
            if (type.getCode().equals(code)) {
                return type;
            }
        }

        return null;
    }
}

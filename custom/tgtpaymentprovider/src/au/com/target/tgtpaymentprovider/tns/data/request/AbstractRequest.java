/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data.request;

import javax.validation.constraints.NotNull;

import au.com.target.tgtpaymentprovider.tns.data.Request;


/**
 * @author vmuthura
 * 
 */
public class AbstractRequest implements Request
{
    @NotNull(message = "Please specify operation")
    private String apiOperation;


    /**
     * @return the apiOperation
     */
    public String getApiOperation()
    {
        return apiOperation;
    }

    /**
     * @param apiOperation
     *            the apiOperation to set
     */
    public void setApiOperation(final String apiOperation)
    {
        this.apiOperation = apiOperation;
    }
}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data.request;

import au.com.target.tgtpaymentprovider.tns.data.SourceOfFunds;
import au.com.target.tgtpaymentprovider.tns.data.Transaction;



/**
 * @author bjames4
 * 
 */
public abstract class AbstractTransactionRequest extends AbstractRequest
{

    private SourceOfFunds sourceOfFunds;
    private Transaction transaction;


    /**
     * @return the sourceOfFunds
     */
    public SourceOfFunds getSourceOfFunds()
    {
        return sourceOfFunds;
    }

    /**
     * @param sourceOfFunds
     *            the sourceOfFunds to set
     */
    public void setSourceOfFunds(final SourceOfFunds sourceOfFunds)
    {
        this.sourceOfFunds = sourceOfFunds;
    }

    /**
     * @return the transaction
     */
    public Transaction getTransaction()
    {
        return transaction;
    }

    /**
     * @param transaction
     *            the transaction to set
     */
    public void setTransaction(final Transaction transaction)
    {
        this.transaction = transaction;
    }
}

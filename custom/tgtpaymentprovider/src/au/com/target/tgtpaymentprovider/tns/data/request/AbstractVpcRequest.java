/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data.request;

import org.codehaus.jackson.annotate.JsonProperty;

import au.com.target.tgtpaymentprovider.tns.data.Request;
import au.com.target.tgtpaymentprovider.tns.data.VpcTransactionResponseEnum;



/**
 * @author vmuthura
 * 
 */
public class AbstractVpcRequest implements Request
{

    @JsonProperty(value = "vpc_AccessCode")
    private String accessCode;

    @JsonProperty(value = "vpc_User")
    private String userName;

    @JsonProperty(value = "vpc_Password")
    private String password;

    @JsonProperty(value = "vpc_Version")
    private String version;

    @JsonProperty(value = "vpc_Merchant")
    private String merchant;

    @JsonProperty(value = "vpc_Command")
    private String command;

    @JsonProperty(value = "vpc_ReturnAuthResponseData")
    private String returnAuthResponseData;

    @JsonProperty(value = "vpc_TxnResponseCode")
    private VpcTransactionResponseEnum transactionResponseCode;

    @JsonProperty(value = "vpc_Message")
    private String message;


    /**
     * @return the accessCode
     */
    public String getAccessCode() {
        return accessCode;
    }

    /**
     * @param accessCode
     *            the accessCode to set
     */
    public void setAccessCode(final String accessCode) {
        this.accessCode = accessCode;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName
     *            the userName to set
     */
    public void setUserName(final String userName) {
        this.userName = userName;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version
     *            the version to set
     */
    public void setVersion(final String version) {
        this.version = version;
    }

    /**
     * @return the merchant
     */
    public String getMerchant() {
        return merchant;
    }

    /**
     * @param merchant
     *            the merchant to set
     */
    public void setMerchant(final String merchant) {
        this.merchant = merchant;
    }

    /**
     * @return the command
     */
    public String getCommand() {
        return command;
    }

    /**
     * @param command
     *            the command to set
     */
    public void setCommand(final String command) {
        this.command = command;
    }

    /**
     * @return the returnAuthResponseData
     */
    public String getReturnAuthResponseData() {
        return returnAuthResponseData;
    }

    /**
     * @param returnAuthResponseData
     *            the returnAuthResponseData to set
     */
    public void setReturnAuthResponseData(final String returnAuthResponseData) {
        this.returnAuthResponseData = returnAuthResponseData;
    }


    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(final String message) {
        this.message = message;
    }


    /**
     * @return the transactionResponseCode
     */
    public VpcTransactionResponseEnum getTransactionResponseCode() {
        return transactionResponseCode;
    }

    /**
     * @param transactionResponseCode
     *            the transactionResponseCode to set
     */
    public void setTransactionResponseCode(final VpcTransactionResponseEnum transactionResponseCode) {
        this.transactionResponseCode = transactionResponseCode;
    }

}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data.request;

import au.com.target.tgtpaymentprovider.tns.data.SourceOfFunds;

/**
 * @author bjames4
 * 
 */
public class SaveCardRequest extends AbstractRequest
{
    private SourceOfFunds sourceOfFunds;

    /**
     * @return the sourceOfFunds
     */
    public SourceOfFunds getSourceOfFunds()
    {
        return sourceOfFunds;
    }

    /**
     * @param sourceOfFunds
     *            the sourceOfFunds to set
     */
    public void setSourceOfFunds(final SourceOfFunds sourceOfFunds)
    {
        this.sourceOfFunds = sourceOfFunds;
    }
}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data.response;

import au.com.target.tgtpaymentprovider.tns.data.ErrorDetails;
import au.com.target.tgtpaymentprovider.tns.data.Response;
import au.com.target.tgtpaymentprovider.tns.data.ResultEnum;

/**
 * @author vmuthura
 * 
 */
public class AbstractResponse implements Response
{
    private String merchant;
    private ResultEnum result;
    private ErrorDetails error;

    /**
     * @return the merchant
     */
    public String getMerchant()
    {
        return merchant;
    }

    /**
     * @param merchant
     *            the merchant to set
     */
    public void setMerchant(final String merchant)
    {
        this.merchant = merchant;
    }

    /**
     * @return the result
     */
    public ResultEnum getResult()
    {
        return result;
    }

    /**
     * @param result
     *            the result to set
     */
    public void setResult(final ResultEnum result)
    {
        this.result = result;
    }

    /**
     * @return the error
     */
    public ErrorDetails getError()
    {
        return error;
    }

    /**
     * @param error
     *            the error to set
     */
    public void setError(final ErrorDetails error)
    {
        this.error = error;
    }
}

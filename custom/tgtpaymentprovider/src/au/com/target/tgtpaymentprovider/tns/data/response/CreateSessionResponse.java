/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data.response;

import javax.validation.constraints.Size;



/**
 * @author bjames4
 * 
 */
public class CreateSessionResponse extends AbstractResponse
{

    @Size(min = 31, max = 35, message = "session - between 31 and 35 ASCII characters")
    private String session;

    /**
     * @return the session
     */
    public String getSession()
    {
        return session;
    }

    /**
     * @param session
     *            the session to set
     */
    public void setSession(final String session)
    {
        this.session = session;
    }

}

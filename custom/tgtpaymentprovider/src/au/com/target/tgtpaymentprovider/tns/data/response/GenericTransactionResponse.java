/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data.response;

import java.util.Date;

import javax.validation.constraints.Size;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import au.com.target.tgtpaymentprovider.tns.data.Order;
import au.com.target.tgtpaymentprovider.tns.data.ResponseDetails;
import au.com.target.tgtpaymentprovider.tns.data.ShippingDetails;
import au.com.target.tgtpaymentprovider.tns.data.SourceOfFunds;
import au.com.target.tgtpaymentprovider.tns.data.Transaction;
import au.com.target.tgtpaymentprovider.tns.util.DateTypeAdapter;


/**
 * @author bjames4
 * 
 */
public class GenericTransactionResponse extends AbstractResponse {

    @Size(min = 31, max = 35, message = "session - between 31 and 35 ASCII chars")
    private String session;

    private Order order;
    private ResponseDetails response;
    private ShippingDetails shipping;
    private SourceOfFunds sourceOfFunds;
    private Transaction transaction;

    @XmlJavaTypeAdapter(DateTypeAdapter.class)
    private Date timeOfRecord;

    /**
     * @return the session
     */
    public String getSession()
    {
        return session;
    }

    /**
     * @param session
     *            the session to set
     */
    public void setSession(final String session)
    {
        this.session = session;
    }

    /**
     * @return the order
     */
    public Order getOrder() {
        return order;
    }

    /**
     * @param order
     *            the order to set
     */
    public void setOrder(final Order order) {
        this.order = order;
    }

    /**
     * @return the response
     */
    public ResponseDetails getResponse() {
        return response;
    }

    /**
     * @param response
     *            the response to set
     */
    public void setResponse(final ResponseDetails response) {
        this.response = response;
    }

    /**
     * @return the shipping
     */
    public ShippingDetails getShipping() {
        return shipping;
    }

    /**
     * @param shipping
     *            the shipping to set
     */
    public void setShipping(final ShippingDetails shipping) {
        this.shipping = shipping;
    }

    /**
     * @return the sourceOfFunds
     */
    public SourceOfFunds getSourceOfFunds() {
        return sourceOfFunds;
    }

    /**
     * @param sourceOfFunds
     *            the sourceOfFunds to set
     */
    public void setSourceOfFunds(final SourceOfFunds sourceOfFunds) {
        this.sourceOfFunds = sourceOfFunds;
    }

    /**
     * @return the transaction
     */
    public Transaction getTransaction() {
        return transaction;
    }

    /**
     * @param transaction
     *            the transaction to set
     */
    public void setTransaction(final Transaction transaction) {
        this.transaction = transaction;
    }

    /**
     * @return the timeOfRecord
     */
    public Date getTimeOfRecord() {
        return timeOfRecord;
    }

    /**
     * @param timeOfRecord
     *            the timeOfRecord to set
     */
    public void setTimeOfRecord(final Date timeOfRecord) {
        this.timeOfRecord = timeOfRecord;
    }

}

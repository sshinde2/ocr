/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data.response;

import au.com.target.tgtpaymentprovider.tns.data.SourceOfFunds;

/**
 * @author bjames4
 * 
 */
public class RetrieveCardResponse extends AbstractResponse {
    private SourceOfFunds sourceOfFunds;

    /**
     * @return the sourceOfFunds
     */
    public SourceOfFunds getSourceOfFunds() {
        return sourceOfFunds;
    }

    /**
     * @param sourceOfFunds
     *            the sourceOfFunds to set
     */
    public void setSourceOfFunds(final SourceOfFunds sourceOfFunds) {
        this.sourceOfFunds = sourceOfFunds;
    }
}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data.response;

import javax.validation.constraints.Size;

import au.com.target.tgtpaymentprovider.tns.data.SourceOfFunds;


/**
 * @author bjames4
 * 
 */
public class SaveCardResponse extends AbstractResponse {
    @Size(min = 1, max = 40, message = "token - between 1 and 40 alpha numeric characters")
    private String token;
    private SourceOfFunds sourceOfFunds;

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token
     *            the token to set
     */
    public void setToken(final String token) {
        this.token = token;
    }

    /**
     * @return the sourceOfFunds
     */
    public SourceOfFunds getSourceOfFunds() {
        return sourceOfFunds;
    }

    /**
     * @param sourceOfFunds
     *            the sourceOfFunds to set
     */
    public void setSourceOfFunds(final SourceOfFunds sourceOfFunds) {
        this.sourceOfFunds = sourceOfFunds;
    }
}

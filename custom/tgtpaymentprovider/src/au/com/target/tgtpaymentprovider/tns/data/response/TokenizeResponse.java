/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data.response;

/**
 * @author vmuthura
 * 
 *         Response wrapper for Tokenize response, for TNS.
 */
public class TokenizeResponse extends GenericTransactionResponse
{
    private String token;

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token
     *            the token to set
     */
    public void setToken(final String token) {
        this.token = token;
    }

}

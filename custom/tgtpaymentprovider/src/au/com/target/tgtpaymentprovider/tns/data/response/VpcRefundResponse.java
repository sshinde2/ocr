/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data.response;

import org.codehaus.jackson.annotate.JsonProperty;

import au.com.target.tgtpaymentprovider.tns.data.Response;
import au.com.target.tgtpaymentprovider.tns.data.request.VpcRefundRequest;


/**
 * @author vmuthura
 * 
 */
public class VpcRefundResponse extends VpcRefundRequest implements Response {

    @JsonProperty(value = "vpc_BatchNo")
    private String batchNumber;

    @JsonProperty(value = "vpc_AuthorizeId")
    private String authrorizeId;

    @JsonProperty(value = "vpc_Debug")
    private String debug;

    @JsonProperty(value = "vpc_terminalID")
    private String terminalId;

    @JsonProperty(value = "vpc_AcqResponseCode")
    private String acquirerResponseCode;

    @JsonProperty(value = "vpc_AuthorisedAmount")
    private String authorisedAmount;

    @JsonProperty(value = "vpc_TransactionNo")
    private String vpcTransactionNumber;

    @JsonProperty(value = "vpc_ReceiptNo")
    private String receiptNumber;

    @JsonProperty(value = "vpc_ShopTransactionNo")
    private String shopTransactionNo;

    @JsonProperty(value = "vpc_Card")
    private String card;

    @JsonProperty(value = "vpc_RefundedAmount")
    private String refundedAmount;

    @JsonProperty(value = "vpc_Locale")
    private String locale;

    @JsonProperty(value = "vpc_CapturedAmount")
    private String capturedAmount;


    /**
     * @return the batchNumber
     */
    public String getBatchNumber() {
        return batchNumber;
    }

    /**
     * @param batchNumber
     *            the batchNumber to set
     */
    public void setBatchNumber(final String batchNumber) {
        this.batchNumber = batchNumber;
    }

    /**
     * @return the authrorizeId
     */
    public String getAuthrorizeId() {
        return authrorizeId;
    }

    /**
     * @param authrorizeId
     *            the authrorizeId to set
     */
    public void setAuthrorizeId(final String authrorizeId) {
        this.authrorizeId = authrorizeId;
    }

    /**
     * @return the debug
     */
    public String getDebug() {
        return debug;
    }

    /**
     * @param debug
     *            the debug to set
     */
    public void setDebug(final String debug) {
        this.debug = debug;
    }

    /**
     * @return the terminalId
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * @param terminalId
     *            the terminalId to set
     */
    public void setTerminalId(final String terminalId) {
        this.terminalId = terminalId;
    }

    /**
     * @return the acquirerResponseCode
     */
    public String getAcquirerResponseCode() {
        return acquirerResponseCode;
    }

    /**
     * @param acquirerResponseCode
     *            the acquirerResponseCode to set
     */
    public void setAcquirerResponseCode(final String acquirerResponseCode) {
        this.acquirerResponseCode = acquirerResponseCode;
    }

    /**
     * @return the authorisedAmount
     */
    public String getAuthorisedAmount() {
        return authorisedAmount;
    }

    /**
     * @param authorisedAmount
     *            the authorisedAmount to set
     */
    public void setAuthorisedAmount(final String authorisedAmount) {
        this.authorisedAmount = authorisedAmount;
    }

    /**
     * @return the vpcTransactionNumber
     */
    public String getVpcTransactionNumber() {
        return vpcTransactionNumber;
    }

    /**
     * @param vpcTransactionNumber
     *            the vpcTransactionNumber to set
     */
    public void setVpcTransactionNumber(final String vpcTransactionNumber) {
        this.vpcTransactionNumber = vpcTransactionNumber;
    }

    /**
     * @return the receiptNumber
     */
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * @param receiptNumber
     *            the receiptNumber to set
     */
    public void setReceiptNumber(final String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    /**
     * @return the shopTransactionNo
     */
    public String getShopTransactionNo() {
        return shopTransactionNo;
    }

    /**
     * @param shopTransactionNo
     *            the shopTransactionNo to set
     */
    public void setShopTransactionNo(final String shopTransactionNo) {
        this.shopTransactionNo = shopTransactionNo;
    }

    /**
     * @return the card
     */
    public String getCard() {
        return card;
    }

    /**
     * @param card
     *            the card to set
     */
    public void setCard(final String card) {
        this.card = card;
    }

    /**
     * @return the refundedAmount
     */
    public String getRefundedAmount() {
        return refundedAmount;
    }

    /**
     * @param refundedAmount
     *            the refundedAmount to set
     */
    public void setRefundedAmount(final String refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    /**
     * @return the locale
     */
    public String getLocale() {
        return locale;
    }

    /**
     * @param locale
     *            the locale to set
     */
    public void setLocale(final String locale) {
        this.locale = locale;
    }

    /**
     * @return the capturedAmount
     */
    public String getCapturedAmount() {
        return capturedAmount;
    }

    /**
     * @param capturedAmount
     *            the capturedAmount to set
     */
    public void setCapturedAmount(final String capturedAmount) {
        this.capturedAmount = capturedAmount;
    }

}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.exception;

/**
 * @author vmuthura
 * 
 */
public class InvalidRequestException extends TnsServiceException
{
    /**
     * @param message
     *            - Descriptive message
     */
    public InvalidRequestException(final String message)
    {
        super(message);
    }

    /**
     * @param cause
     *            - Root cause
     */
    public InvalidRequestException(final Exception cause)
    {
        super(cause);
    }

    /**
     * @param message
     *            - Descriptive message
     * @param cause
     *            - Root cause
     */
    public InvalidRequestException(final String message, final Exception cause)
    {
        super(message, cause);
    }
}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.exception;

/**
 * @author vmuthura
 * 
 */
public class RequestRejectedException extends TnsServiceException
{
    /**
     * @param message
     *            - Descriptive message
     */
    public RequestRejectedException(final String message)
    {
        super(message);
    }

    /**
     * @param cause
     *            - Root cause
     */
    public RequestRejectedException(final Exception cause)
    {
        super(cause);
    }

    /**
     * @param message
     *            - Descriptive message
     * @param cause
     *            - Root cause
     */
    public RequestRejectedException(final String message, final Exception cause)
    {
        super(message, cause);
    }
}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.exception;

/**
 * @author vmuthura
 * 
 */
public class TnsServerException extends TnsServiceException
{
    /**
     * @param message
     *            - Descriptive message
     */
    public TnsServerException(final String message)
    {
        super(message);
    }

    /**
     * @param cause
     *            - Root cause
     */
    public TnsServerException(final Exception cause)
    {
        super(cause);
    }

    /**
     * @param message
     *            - Descriptive message
     * @param cause
     *            - Root cause
     */
    public TnsServerException(final String message, final Exception cause)
    {
        super(message, cause);
    }
}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.exception;

/**
 * @author vmuthura
 * 
 */
public class TnsServiceException extends Exception
{

    /**
     * @param message
     *            - Descriptive message
     */
    public TnsServiceException(final String message)
    {
        super(message);
    }

    /**
     * @param cause
     *            - Root cause
     */
    public TnsServiceException(final Exception cause)
    {
        super(cause);
    }

    /**
     * @param message
     *            - Descriptive message
     * @param cause
     *            - Root cause
     */
    public TnsServiceException(final String message, final Exception cause)
    {
        super(message, cause);
    }
}

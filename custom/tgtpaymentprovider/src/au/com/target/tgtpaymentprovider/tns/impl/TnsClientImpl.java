/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtpaymentprovider.constants.TgtpaymentproviderConstants;
import au.com.target.tgtpaymentprovider.tns.TnsWebOperations;
import au.com.target.tgtpaymentprovider.tns.data.ErrorDetails;
import au.com.target.tgtpaymentprovider.tns.data.Request;
import au.com.target.tgtpaymentprovider.tns.data.ResultEnum;
import au.com.target.tgtpaymentprovider.tns.data.response.AbstractResponse;
import au.com.target.tgtpaymentprovider.tns.exception.InvalidRequestException;
import au.com.target.tgtpaymentprovider.tns.exception.RequestRejectedException;
import au.com.target.tgtpaymentprovider.tns.exception.TnsServerException;


/**
 * @author vmuthura
 * 
 */
public class TnsClientImpl implements TnsWebOperations {

    private static final Logger LOG = Logger.getLogger(TnsClientImpl.class);

    private RestTemplate tnsRestTemplate;

    /**
     * @param tnsRestTemplate
     *            the tnsRestTemplate to set
     */
    @Required
    public void setTnsRestTemplate(final RestTemplate tnsRestTemplate) {
        this.tnsRestTemplate = tnsRestTemplate;
    }

    @Override
    public <T> T get(final String url, final Class<T> responseType, final Object... uriVariable)
            throws RequestRejectedException, InvalidRequestException, TnsServerException {
        final AbstractResponse response;
        try {
            response = (AbstractResponse)tnsRestTemplate.getForObject(url, responseType, uriVariable);
        }
        catch (final ResourceAccessException rae) {
            LOG.error(TgtpaymentproviderConstants.ERR_TNS_TIMEOUT, rae);
            throw new TnsServerException(rae);
        }
        catch (final RestClientException e) {
            LOG.error("GET request to TNS server failed: ", e);
            throw new TnsServerException(e);
        }
        handleError(response);

        return (T)response;
    }

    @Override
    public <T> T post(final String url, final Request request, final Class<T> responseType, final Object... uriVariable)
            throws RequestRejectedException,
            InvalidRequestException, TnsServerException {
        final AbstractResponse response;
        try {
            response = (AbstractResponse)tnsRestTemplate.postForObject(url, request, responseType, uriVariable);
        }
        catch (final ResourceAccessException rae) {
            LOG.error(TgtpaymentproviderConstants.ERR_TNS_TIMEOUT, rae);
            throw new TnsServerException(rae);
        }
        catch (final RestClientException e) {
            LOG.error("POST request to TNS server failed: ", e);
            throw new TnsServerException(e);
        }
        handleError(response);

        return (T)response;
    }

    @Override
    public <T> T put(final String url, final Request request, final Class<T> responseType, final Object... uriVariable)
            throws RequestRejectedException, InvalidRequestException, TnsServerException {
        final ResponseEntity<AbstractResponse> response;
        try {
            response = (ResponseEntity<AbstractResponse>)tnsRestTemplate.exchange(url, HttpMethod.PUT,
                    new HttpEntity(request), responseType, uriVariable);
        }
        catch (final ResourceAccessException rae) {
            LOG.error(TgtpaymentproviderConstants.ERR_TNS_TIMEOUT, rae);
            throw new TnsServerException(rae);
        }
        catch (final RestClientException e) {
            LOG.error("PUT request to TNS server failed: ", e);
            throw new TnsServerException(e);
        }
        handleError(response.getBody());

        return (T)response.getBody();
    }

    @Override
    public <T> T delete(final String url, final Request request, final Class<T> responseType,
            final Object... uriVariable)
                    throws RequestRejectedException, InvalidRequestException, TnsServerException {
        final ResponseEntity<AbstractResponse> response;
        try {
            response = (ResponseEntity<AbstractResponse>)tnsRestTemplate.exchange(url, HttpMethod.DELETE,
                    new HttpEntity(request), responseType, uriVariable);
        }
        catch (final ResourceAccessException rae) {
            LOG.error(TgtpaymentproviderConstants.ERR_TNS_TIMEOUT, rae);
            throw new TnsServerException(rae);
        }
        catch (final RestClientException e) {
            LOG.error("DELETE request to TNS server failed: ", e);
            throw new TnsServerException(e);
        }
        handleError(response.getBody());

        return (T)response.getBody();
    }

    /**
     * @param response
     *            - tns response
     * @throws RequestRejectedException
     *             - for illegal request
     * @throws InvalidRequestException
     *             - for bad request
     * @throws TnsServerException
     *             - for everything else
     */
    private void handleError(final AbstractResponse response) throws RequestRejectedException,
            InvalidRequestException, TnsServerException {
        if (response == null) {
            return;
        }
        if (ResultEnum.ERROR.equals(response.getResult())) {
            final ErrorDetails errorDetails = response.getError();
            switch (errorDetails.getCause()) {
                case REQUEST_REJECTED:
                {
                    LOG.error(TgtpaymentproviderConstants.ERR_TNS_REQUEST_REJECTED + " "
                            + errorDetails.getSupportCode());
                    throw new RequestRejectedException("TNS server rejected the request. Support code: "
                            + errorDetails.getSupportCode());
                }
                case INVALID_REQUEST:
                {
                    LOG.error(TgtpaymentproviderConstants.ERR_TNS_VALIDATION + " " + errorDetails.getExplanation());
                    throw new InvalidRequestException(errorDetails.getExplanation() + " : "
                            + ((errorDetails.getValidationType() == null) ? "" : errorDetails.getValidationType())
                            + " "
                            + ((errorDetails.getField() == null) ? "" : errorDetails.getField()));
                }
                case SERVER_FAILED:
                {
                    LOG.error(TgtpaymentproviderConstants.ERR_TNS_SERVER_FAILED + " " + errorDetails.getSupportCode());
                    throw new TnsServerException("TNS server failure. Support code: " + errorDetails.getSupportCode());
                }
                case SERVER_BUSY:
                {
                    LOG.error(TgtpaymentproviderConstants.ERR_TNS_SERVER_BUSY + " " + errorDetails.getExplanation());
                    throw new TnsServerException(errorDetails.getExplanation());
                }
                default:
                    throw new TnsServerException("TNS server: Unknown error occured");
            }
        }
    }

}

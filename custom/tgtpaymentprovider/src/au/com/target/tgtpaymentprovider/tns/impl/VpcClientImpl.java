/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtpaymentprovider.tns.TnsWebOperations;
import au.com.target.tgtpaymentprovider.tns.data.Request;
import au.com.target.tgtpaymentprovider.tns.data.request.AbstractVpcRequest;
import au.com.target.tgtpaymentprovider.tns.exception.InvalidRequestException;
import au.com.target.tgtpaymentprovider.tns.exception.RequestRejectedException;
import au.com.target.tgtpaymentprovider.tns.exception.TnsServerException;


/**
 * @author vmuthura
 * 
 */
public class VpcClientImpl implements TnsWebOperations {

    private static final Logger LOG = Logger.getLogger(VpcClientImpl.class);

    private RestTemplate vpcRestTemplate;

    private String accessCode;
    private String merchant;
    private String userName;
    private String password;
    private String version;

    /**
     * @param accessCode
     *            the accessCode to set
     */
    @Required
    public void setAccessCode(final String accessCode) {
        this.accessCode = accessCode;
    }

    /**
     * @param merchant
     *            the merchant to set
     */
    @Required
    public void setMerchant(final String merchant) {
        this.merchant = merchant;
    }

    /**
     * @param userName
     *            the userName to set
     */
    @Required
    public void setUserName(final String userName) {
        this.userName = userName;
    }

    /**
     * @param password
     *            the password to set
     */
    @Required
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * @param version
     *            the version to set
     */
    @Required
    public void setVersion(final String version) {
        this.version = version;
    }

    /**
     * @param vpcRestTemplate
     *            the vpcRestTemplate to set
     */
    @Required
    public void setVpcRestTemplate(final RestTemplate vpcRestTemplate) {
        this.vpcRestTemplate = vpcRestTemplate;
    }

    @Override
    public <T> T get(final String url, final Class<T> responseType, final Object... uriVariable)
            throws RequestRejectedException,
            InvalidRequestException, TnsServerException {
        throw new UnsupportedOperationException("GET operation is not supported by VPC");
    }

    @Override
    public <T> T post(final String url, final Request request, final Class<T> responseType, final Object... uriVariable)
            throws RequestRejectedException, InvalidRequestException, TnsServerException {
        final AbstractVpcRequest response;
        ((AbstractVpcRequest)request).setAccessCode(accessCode);
        ((AbstractVpcRequest)request).setMerchant(merchant);
        ((AbstractVpcRequest)request).setUserName(userName);
        ((AbstractVpcRequest)request).setPassword(password);
        ((AbstractVpcRequest)request).setVersion(version);

        try {
            response = (AbstractVpcRequest)vpcRestTemplate.postForObject(url, request, responseType, uriVariable);
        }
        catch (final RestClientException e) {
            LOG.error("POST request to VPC server failed: ", e);
            throw new TnsServerException(e);
        }
        handleError(response);

        return (T)response;
    }

    @Override
    public <T> T put(final String url, final Request request, final Class<T> responseType, final Object... uriVariable)
            throws RequestRejectedException, InvalidRequestException, TnsServerException {
        throw new UnsupportedOperationException("PUT operation is not supported by VPC");
    }

    @Override
    public <T> T delete(final String url, final Request request, final Class<T> responseType,
            final Object... uriVariable)
                    throws RequestRejectedException, InvalidRequestException, TnsServerException {
        throw new UnsupportedOperationException("DELETE operation is not supported by VPC");
    }

    /**
     * @param response
     *            - tns response
     * @throws RequestRejectedException
     *             - for illegal request
     * @throws InvalidRequestException
     *             - for bad request
     * @throws TnsServerException
     *             - for everything else
     */
    private void handleError(final AbstractVpcRequest response) throws RequestRejectedException,
            InvalidRequestException,
            TnsServerException {
        if (response == null) {
            return;
        }
        switch (response.getTransactionResponseCode()) {
            case RETRY_LIMIT_EXCEEDED:
                throw new RequestRejectedException(response.getMessage());
            case INVALID_DATA:
                throw new InvalidRequestException(response.getMessage());
            case UNKNOWN:
                throw new TnsServerException(response.getMessage());
            default:
                break;
        }
    }

}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.util;

import de.hybris.platform.core.PK;

import java.text.MessageFormat;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;


/**
 * @author salexa10
 *
 */
public class PaymentProviderLogUtil {

    /**
     * Static utility classes should not be instantiable.
     */
    private PaymentProviderLogUtil() {
        //No-op; won't be called
    }

    /**
     * Log a message using the payment provider log format
     * 
     * @param logger
     * @param level
     * @param message
     * @param throwable
     */
    public static void log(final Logger logger, final Level level, final String message, final Throwable throwable) {
        logger.log(level, message, throwable);
    }

    /**
     * Format the message
     * 
     * @param logFormat
     * @param action
     * @param status
     * @param id
     * @param userPk
     * @param message
     */
    public static String formatMessage(final String logFormat, final String action, final String status,
            final String id,
            final PK userPk,
            final String message) {
        return MessageFormat
                .format(logFormat, action, status, id, userPk != null ? userPk : StringUtils.EMPTY, message);
    }

}

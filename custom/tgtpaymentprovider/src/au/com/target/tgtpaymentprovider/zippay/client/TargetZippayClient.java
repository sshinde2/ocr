/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.client;

import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayClientException;
import au.com.target.tgtpaymentprovider.zippay.data.request.CreateChargeRequest;
import au.com.target.tgtpaymentprovider.zippay.data.request.CreateCheckoutRequest;
import au.com.target.tgtpaymentprovider.zippay.data.request.CreateRefundRequest;
import au.com.target.tgtpaymentprovider.zippay.data.response.CheckoutResponse;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateChargeResponse;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateCheckoutResponse;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateRefundResponse;


/**
 * @author salexa10
 *
 */
public interface TargetZippayClient {

    /**
     * OCR-20678 Create Checkout
     * 
     * @param createCheckoutRequest
     * @return {@link CreateCheckoutResponse}
     * @throws TargetZippayClientException
     * @see <a href="https://zip-online.api-docs.io/v1/api-specification/create-a-checkout">Create Checkout</a>
     */
    CreateCheckoutResponse createCheckout(CreateCheckoutRequest createCheckoutRequest)
            throws TargetZippayClientException;

    /**
     * OCR-20795 Ping
     * 
     * @return boolean
     */
    boolean isZippayConnectionAvailable();

    /**
     * OCR-20779 Retrieve Checkout
     * 
     * @param checkoutId
     * @return {@link CheckoutResponse}
     * @throws TargetZippayClientException
     * @see <a href="https://zip-online.api-docs.io/v1/api-specification/checkout-retrieve-a-checkout">Retrieve
     *      Checkout</a>
     */
    CheckoutResponse retrieveCheckout(final String checkoutId) throws TargetZippayClientException;

    /**
     * OCR-20781 Create Charge
     *
     * @param createChargeRequest
     * @return {@link CreateChargeResponse}
     * @throws TargetZippayClientException
     * @see <a href="https://zip-online.api-docs.io/v1/api-specification/charge-create-a-charge">Create Charge</a>
     */
    CreateChargeResponse createCharge(final CreateChargeRequest createChargeRequest) throws TargetZippayClientException;

    /**
     * OCR-20746 Create Refund
     * 
     * @param createRefundRequest
     * @return {@link CreateRefundResponse}
     * @throws TargetZippayClientException
     * @see <a href="https://zip-online.api-docs.io/v1/api-specification/refund-create-a-refund">Create Refund</a>
     */
    CreateRefundResponse createRefund(final CreateRefundRequest createRefundRequest)
            throws TargetZippayClientException;

}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.client.exception;

/**
 * @author salexa10
 *
 */
public class TargetZippayTransactionRejectedException extends RuntimeException {

    public TargetZippayTransactionRejectedException() {
        super();
    }

    public TargetZippayTransactionRejectedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public TargetZippayTransactionRejectedException(final String message) {
        super(message);
    }

    public TargetZippayTransactionRejectedException(final Throwable cause) {
        super(cause);
    }

}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.client.impl;

import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.net.URI;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtpaymentprovider.constants.TgtpaymentproviderConstants;
import au.com.target.tgtpaymentprovider.util.PaymentProviderLogUtil;
import au.com.target.tgtpaymentprovider.zippay.client.TargetZippayClient;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayClientException;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayTransactionError;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayTransactionRejectedException;
import au.com.target.tgtpaymentprovider.zippay.data.request.CreateChargeRequest;
import au.com.target.tgtpaymentprovider.zippay.data.request.CreateCheckoutRequest;
import au.com.target.tgtpaymentprovider.zippay.data.request.CreateRefundRequest;
import au.com.target.tgtpaymentprovider.zippay.data.response.CheckoutResponse;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateChargeResponse;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateCheckoutResponse;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateRefundResponse;
import au.com.target.tgtutility.logging.JsonLogger;


/**
 * @author salexa10
 *
 */
public class TargetZippayClientImpl implements TargetZippayClient {

    private static final Logger LOG = Logger.getLogger(TargetZippayClientImpl.class);

    private static final String LOG_FORMAT = "PAYMENT-Zip: action={0}, status={1}, id={2}, userPk={3}, {4}";

    private static final String START_REST_CALL = "start";

    private static final String END_REST_CALL = "end";

    private static final String PAYLOAD = "payload=";

    private static final String ERROR_IS = "error=";

    private static final String ERROR = "error";

    private static final String FAILED = "failed";

    private static final String FAILURE_REASON = "failureReason=";

    private static final String CHECK_AVAILABILITY = "zip ping ";

    private static final String ACTION_CREATE_CHECKOUT = "create checkout";

    private static final String CREATE_CHECKOUT_NULL_REQUEST = "zip create checkout request or order is null";

    private static final String ACTION_RETRIEVE_CHECKOUT = "retrieve checkout";

    private static final String RETRIEVE_CHECKOUT_ID_NULL = "zip retrieve checkout Id is null";

    private static final String ACTION_CREATE_CHARGE = "create charge";

    private static final String CREATE_CHARGE_NULL_REQUEST = "zip create charge request or checkout id is null";

    private static final String ACTION_CREATE_REFUND = "create refund";

    private static final String CREATE_REFUND_NULL_REQUEST = "zip create refund request is null";

    private RestTemplate zippayRestTemplate;

    private UserService userService;

    private String baseUrl;

    private String checkoutUrl;

    private String pingUrl;

    private String chargeUrl;

    private String refundUrl;

    @Override
    public CreateCheckoutResponse createCheckout(final CreateCheckoutRequest createCheckoutRequest)
            throws TargetZippayClientException {
        if (createCheckoutRequest == null || createCheckoutRequest.getOrder() == null
                || createCheckoutRequest.getOrder().getReference() == null) {
            PaymentProviderLogUtil.log(LOG, Level.ERROR, CREATE_CHECKOUT_NULL_REQUEST, null);
            throw new TargetZippayClientException(CREATE_CHECKOUT_NULL_REQUEST);
        }
        final PK userPk = getUserPK();
        try {
            PaymentProviderLogUtil.log(LOG, Level.INFO,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, ACTION_CREATE_CHECKOUT, START_REST_CALL,
                            createCheckoutRequest.getOrder().getReference(), userPk, START_REST_CALL),
                    null);
            JsonLogger.logJson(Level.DEBUG, LOG,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, ACTION_CREATE_CHECKOUT, START_REST_CALL,
                            createCheckoutRequest.getOrder().getReference(), userPk, PAYLOAD),
                    createCheckoutRequest);
            final CreateCheckoutResponse createCheckoutResponse = zippayRestTemplate.postForObject(
                    baseUrl + checkoutUrl,
                    new HttpEntity<>(createCheckoutRequest), CreateCheckoutResponse.class);
            PaymentProviderLogUtil.log(LOG, Level.INFO,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, ACTION_CREATE_CHECKOUT, END_REST_CALL,
                            createCheckoutRequest.getOrder().getReference(), userPk, END_REST_CALL),
                    null);
            JsonLogger.logJson(Level.INFO, LOG,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, ACTION_CREATE_CHECKOUT, END_REST_CALL,
                            createCheckoutRequest.getOrder().getReference(), userPk, PAYLOAD), createCheckoutResponse);
            return createCheckoutResponse;
        }
        catch (final HttpStatusCodeException e) {
            JsonLogger.logJson(Level.ERROR, LOG,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, ACTION_CREATE_CHECKOUT, FAILED,
                            createCheckoutRequest.getOrder().getReference(), userPk, FAILURE_REASON),
                    e.getResponseBodyAsString());
            throw new TargetZippayClientException(e);
        }
        catch (final Exception e) {
            PaymentProviderLogUtil.log(LOG, Level.ERROR,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, ACTION_CREATE_CHECKOUT, ERROR,
                            createCheckoutRequest.getOrder().getReference(), userPk, ERROR_IS),
                    e);
            throw new TargetZippayClientException(e);
        }
    }

    @Override
    public boolean isZippayConnectionAvailable() {
        PaymentProviderLogUtil.log(LOG, Level.INFO,
                PaymentProviderLogUtil.formatMessage(LOG_FORMAT, CHECK_AVAILABILITY, START_REST_CALL,
                        StringUtils.EMPTY, null, START_REST_CALL), null);
        try {
            zippayRestTemplate.exchange(baseUrl + pingUrl,
                    HttpMethod.GET,
                    null,
                    String.class);
            PaymentProviderLogUtil.log(LOG, Level.INFO,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, CHECK_AVAILABILITY, END_REST_CALL,
                            StringUtils.EMPTY, null, END_REST_CALL), null);
            return true;
        }
        catch (final Exception e) {
            // don't throw exception, just return false
            PaymentProviderLogUtil.log(LOG, Level.ERROR,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, CHECK_AVAILABILITY, ERROR,
                            StringUtils.EMPTY, null, ERROR), e);
            return false;
        }

    }

    @Override
    public CheckoutResponse retrieveCheckout(final String checkoutId) throws TargetZippayClientException {
        if (StringUtils.isEmpty(checkoutId)) {
            PaymentProviderLogUtil.log(LOG, Level.ERROR, RETRIEVE_CHECKOUT_ID_NULL, null);
            throw new TargetZippayClientException(RETRIEVE_CHECKOUT_ID_NULL);
        }
        final PK userPk = getUserPK();
        try {
            PaymentProviderLogUtil.log(LOG, Level.INFO,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, ACTION_RETRIEVE_CHECKOUT, START_REST_CALL,
                            checkoutId, userPk, START_REST_CALL),
                    null);
            final CheckoutResponse checkoutResponse = zippayRestTemplate.getForObject(
                    URI.create(baseUrl + checkoutUrl + "/" + checkoutId), CheckoutResponse.class);
            PaymentProviderLogUtil.log(LOG, Level.INFO,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, ACTION_RETRIEVE_CHECKOUT, END_REST_CALL,
                            checkoutId, userPk, END_REST_CALL),
                    null);
            JsonLogger.logJson(Level.DEBUG, LOG,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, ACTION_RETRIEVE_CHECKOUT, END_REST_CALL,
                            checkoutId, userPk, PAYLOAD),
                    checkoutResponse);
            return checkoutResponse;
        }
        catch (final HttpStatusCodeException e) {
            JsonLogger.logJson(Level.ERROR, LOG,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, ACTION_RETRIEVE_CHECKOUT, FAILED,
                            checkoutId, userPk, FAILURE_REASON),
                    e.getResponseBodyAsString());
            throw new TargetZippayClientException(e);
        }
        catch (final Exception e) {
            PaymentProviderLogUtil.log(LOG, Level.ERROR,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, ACTION_RETRIEVE_CHECKOUT, ERROR,
                            checkoutId, userPk, ERROR_IS),
                    e);
            throw new TargetZippayClientException(e);
        }
    }

    @Override
    public CreateChargeResponse createCharge(final CreateChargeRequest createChargeRequest)
            throws TargetZippayClientException {
        if (createChargeRequest == null || createChargeRequest.getAuthority() == null
                || createChargeRequest.getAuthority().getValue() == null) {
            PaymentProviderLogUtil.log(LOG, Level.ERROR, CREATE_CHARGE_NULL_REQUEST, null);
            throw new TargetZippayClientException(CREATE_CHARGE_NULL_REQUEST);
        }
        final PK userPk = getUserPK();
        try {
            JsonLogger.logJson(Level.INFO, LOG,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, ACTION_CREATE_CHARGE, START_REST_CALL,
                            createChargeRequest.getAuthority().getValue(), getUserPK(), PAYLOAD), createChargeRequest);
            final HttpHeaders headers = new HttpHeaders();
            headers.add(TgtpaymentproviderConstants.ZipPayment.HEADER_IDEMPOTENCY_KEY,
                    createChargeRequest.getAuthority().getValue());
            final HttpEntity<CreateChargeRequest> entity = new HttpEntity<>(createChargeRequest, headers);
            final CreateChargeResponse createChargeResponse = zippayRestTemplate.postForObject(baseUrl + chargeUrl,
                    entity, CreateChargeResponse.class);
            JsonLogger.logJson(Level.INFO, LOG,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, ACTION_CREATE_CHARGE, END_REST_CALL,
                            createChargeRequest.getAuthority().getValue(), userPk, PAYLOAD), createChargeResponse);
            return createChargeResponse;
        }
        catch (final HttpStatusCodeException e) {
            JsonLogger.logJson(Level.ERROR, LOG,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, ACTION_CREATE_CHARGE, FAILED,
                            createChargeRequest.getAuthority().getValue(), userPk, FAILURE_REASON),
                    e.getResponseBodyAsString());
            throw new TargetZippayTransactionError(e);
        }
        catch (final Exception e) {
            PaymentProviderLogUtil.log(LOG, Level.ERROR,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, ACTION_CREATE_CHARGE, ERROR,
                            createChargeRequest.getAuthority().getValue(), userPk, ERROR_IS),
                    e);
            throw new TargetZippayClientException(e);
        }
    }

    @Override
    public CreateRefundResponse createRefund(final CreateRefundRequest createRefundRequest)
            throws TargetZippayClientException {
        if (createRefundRequest == null) {
            PaymentProviderLogUtil.log(LOG, Level.ERROR, CREATE_REFUND_NULL_REQUEST, null);
            throw new TargetZippayClientException(CREATE_REFUND_NULL_REQUEST);
        }
        final PK userPk = getUserPK();
        try {
            JsonLogger.logJson(Level.INFO, LOG,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, ACTION_CREATE_REFUND, START_REST_CALL,
                            createRefundRequest.getChargeId(), userPk, PAYLOAD), createRefundRequest);
            final CreateRefundResponse createRefundResponse = zippayRestTemplate.postForObject(
                    baseUrl + refundUrl,
                    new HttpEntity<CreateRefundRequest>(createRefundRequest), CreateRefundResponse.class);
            JsonLogger.logJson(Level.INFO, LOG,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, ACTION_CREATE_REFUND, END_REST_CALL,
                            createRefundRequest.getChargeId(), userPk, PAYLOAD), createRefundResponse);
            return createRefundResponse;
        }
        catch (final HttpStatusCodeException httpStatusCodeException) {
            JsonLogger.logJson(Level.ERROR, LOG,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, ACTION_CREATE_REFUND, FAILED,
                            createRefundRequest.getChargeId(), userPk, FAILURE_REASON),
                    httpStatusCodeException.getResponseBodyAsString());
            // Check for HTTP 402 Payment Required - invalid_state, amount_invalid & account_inoperative
            if (HttpStatus.PAYMENT_REQUIRED.equals(httpStatusCodeException.getStatusCode())) {
                throw new TargetZippayTransactionRejectedException(httpStatusCodeException);
            }
            // Otherwise
            else {
                throw new TargetZippayTransactionError(httpStatusCodeException);
            }
        }
        catch (final Exception exception) {
            PaymentProviderLogUtil.log(LOG, Level.ERROR,
                    PaymentProviderLogUtil.formatMessage(LOG_FORMAT, ACTION_CREATE_REFUND, ERROR,
                            createRefundRequest.getChargeId(), userPk, ERROR_IS),
                    exception);
            throw new TargetZippayClientException();
        }
    }

    /**
     * Retrieve the user PK
     * 
     */
    protected PK getUserPK() {
        PK userPk = null;
        final UserModel currentUser = userService.getCurrentUser();
        if (null != currentUser) {
            userPk = currentUser.getPk();
        }
        return userPk;
    }

    /**
     * @return the zippayRestTemplate
     */
    protected RestTemplate getZippayRestTemplate() {
        return zippayRestTemplate;
    }

    /**
     * @param zippayRestTemplate
     *            the zippayRestTemplate to set
     */
    @Required
    public void setZippayRestTemplate(final RestTemplate zippayRestTemplate) {
        this.zippayRestTemplate = zippayRestTemplate;
    }

    /**
     * @param userService
     *            the userService to set
     */
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @param baseUrl
     *            the baseUrl to set
     */
    @Required
    public void setBaseUrl(final String baseUrl) {
        this.baseUrl = baseUrl;
    }

    /**
     * @param checkoutUrl
     *            the checkoutUrl to set
     */
    @Required
    public void setCheckoutUrl(final String checkoutUrl) {
        this.checkoutUrl = checkoutUrl;
    }

    /**
     * @param pingUrl
     *            the pingUrl to set
     */
    @Required
    public void setPingUrl(final String pingUrl) {
        this.pingUrl = pingUrl;
    }

    /**
     * 
     * @param chargeUrl
     *            the chargeUrl to set
     */
    @Required
    public void setChargeUrl(final String chargeUrl) {
        this.chargeUrl = chargeUrl;
    }

    /**
     * 
     * @param refundUrl
     *            the refundUrl to set
     */
    @Required
    public void setRefundUrl(final String refundUrl) {
        this.refundUrl = refundUrl;
    }

}

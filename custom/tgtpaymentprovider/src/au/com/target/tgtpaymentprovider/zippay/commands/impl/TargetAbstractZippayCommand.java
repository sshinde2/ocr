/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.commands.impl;

import au.com.target.tgtpaymentprovider.zippay.client.TargetZippayClient;


/**
 * @author salexa10
 *
 */
public class TargetAbstractZippayCommand {

    private TargetZippayClient targetZippayClient;

    /**
     * @return the targetZippayClient
     */
    protected TargetZippayClient getTargetZippayClient() {
        return targetZippayClient;
    }

    /**
     * @param targetZippayClient
     *            the targetZippayClient to set
     */
    public void setTargetZippayClient(final TargetZippayClient targetZippayClient) {
        this.targetZippayClient = targetZippayClient;
    }

}

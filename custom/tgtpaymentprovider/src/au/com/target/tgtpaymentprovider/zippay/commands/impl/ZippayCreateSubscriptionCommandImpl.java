/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.commands.impl;

import de.hybris.platform.payment.AdapterException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.dto.LineItem;
import au.com.target.tgtpaymentprovider.constants.TgtpaymentproviderConstants;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayClientException;
import au.com.target.tgtpaymentprovider.zippay.data.request.CreateCheckoutRequest;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateCheckoutResponse;
import au.com.target.tgtpaymentprovider.zippay.data.types.Address;
import au.com.target.tgtpaymentprovider.zippay.data.types.Config;
import au.com.target.tgtpaymentprovider.zippay.data.types.Item;
import au.com.target.tgtpaymentprovider.zippay.data.types.Order;
import au.com.target.tgtpaymentprovider.zippay.data.types.Shipping;
import au.com.target.tgtpaymentprovider.zippay.data.types.Shopper;


/**
 * @author salexa10
 *
 */
public class ZippayCreateSubscriptionCommandImpl extends TargetAbstractZippayCommand
        implements TargetCreateSubscriptionCommand {

    @Override
    public TargetCreateSubscriptionResult perform(final TargetCreateSubscriptionRequest paramRequest) {
        if (paramRequest == null) {
            throw new IllegalArgumentException("TargetCreateSubscriptionRequest is null");
        }
        try {
            final CreateCheckoutResponse createCheckoutResponse = getTargetZippayClient()
                    .createCheckout(translateRequest(paramRequest));
            return translateResponse(createCheckoutResponse);
        }
        catch (final TargetZippayClientException e) {
            throw new AdapterException(e);
        }
    }



    /**
     * @param paramRequest
     * @return {@link CreateCheckoutRequest}
     */
    private CreateCheckoutRequest translateRequest(final TargetCreateSubscriptionRequest paramRequest) {
        final CreateCheckoutRequest createCheckoutRequest = new CreateCheckoutRequest();
        if (paramRequest.getOrder() != null) {
            final Shopper shopper = new Shopper();
            final Address billingAddress = new Address();
            final Address shippingAddress = new Address();
            final Config config = new Config();
            final Order order = new Order();
            final Shipping shipping = new Shipping();

            if (paramRequest.getOrder().getCustomer() != null) {
                shopper.setFirstName(paramRequest.getOrder().getCustomer().getGivenNames());
                shopper.setLastName(paramRequest.getOrder().getCustomer().getSurname());
                shopper.setEmail(paramRequest.getOrder().getCustomer().getEmail());
                // Set the billing address
                if (paramRequest.getOrder().getBillingAddress() != null) {
                    populateAddress(paramRequest.getOrder().getBillingAddress(), billingAddress);
                    shopper.setBillingAddress(billingAddress);
                }
                createCheckoutRequest.setShopper(shopper);
            }
            order.setReference(paramRequest.getOrder().getOrderId());
            order.setCurrency(paramRequest.getOrder().getOrderTotalAmount().getCurrency().getCurrencyCode());
            order.setAmount(paramRequest.getOrder().getOrderTotalAmount().getAmount().doubleValue());
            shipping.setPickup(paramRequest.getOrder().isPickup());
            // Check if the pickup is false, then it's HD and shipping address is required. if true then it's CNC - so no shipping address
            if (!shipping.isPickup()) {
                populateAddress(paramRequest.getOrder().getShippingAddress(), shippingAddress);
                shipping.setAddress(shippingAddress);
            }
            // set shipping
            order.setShipping(shipping);
            final double shippingAmount = paramRequest.getOrder().getShippingTotalAmount() != null
                    ? paramRequest.getOrder().getShippingTotalAmount().getAmount().doubleValue() : 0;
            order.setItems(convertItems(paramRequest.getOrder().getLineItems(),
                    shippingAmount));
            createCheckoutRequest.setOrder(order);
            // config
            config.setRedirectUri(paramRequest.getReturnUrl());

            createCheckoutRequest.setConfig(config);
        }

        return createCheckoutRequest;
    }


    private void populateAddress(final au.com.target.tgtpayment.dto.Address fromAddress, final Address toAddresss) {
        if (fromAddress != null) {
            toAddresss.setLine1(fromAddress.getStreet1());
            toAddresss.setState(fromAddress.getState());
            toAddresss.setPostalCode(fromAddress.getPostalCode());
            toAddresss.setCountry(fromAddress.getCountryCode());
            toAddresss.setCity(fromAddress.getCity());
        }
    }

    /**
     * @param lineItems
     * @return List of {@link Item}
     */
    private List<Item> convertItems(final List<LineItem> lineItems, final double shippingAmount) {
        List<Item> items = null;
        Item item;
        Item itemForShipping;
        if (CollectionUtils.isNotEmpty(lineItems)) {
            items = new ArrayList<>(lineItems.size());
            for (final LineItem lineItem : lineItems) {
                if (lineItem.getPrice() == null || lineItem.getPrice().getAmount() == null) {
                    continue;
                }
                item = new Item();
                item.setName(lineItem.getName());
                item.setAmount(lineItem.getPrice().getAmount().doubleValue());

                // Check if the product type is discount - flybuys, TMD, Promo Voucher etc
                if (lineItem.isDiscount()) {
                    item.setType(TgtpaymentproviderConstants.ZIPPAY_DISCOUNT);
                }
                // otherwise the product is a SKU
                else {
                    item.setType(TgtpaymentproviderConstants.SKU);
                }
                item.setQuantity(lineItem.getQuantity());
                item.setImageUri(lineItem.getImageUri());
                item.setReference(lineItem.getProductCode());
                items.add(item);
            }
            // Add separate Line Item for Shipping
            if (Double.compare(shippingAmount, 0.0) > 0) {
                itemForShipping = new Item();
                itemForShipping.setName(TgtpaymentproviderConstants.ZIPPAY_SHIPPING);
                itemForShipping.setAmount(shippingAmount);
                itemForShipping.setQuantity(0);
                itemForShipping.setReference(StringUtils.EMPTY);
                itemForShipping.setType(TgtpaymentproviderConstants.ZIPPAY_SHIPPING);
                itemForShipping.setImageUri(null);
                items.add(itemForShipping);
            }
        }
        return items;

    }

    /**
     * @param createCheckoutResponse
     * @return {@link TargetCreateSubscriptionResult}
     */
    private TargetCreateSubscriptionResult translateResponse(final CreateCheckoutResponse createCheckoutResponse) {
        final TargetCreateSubscriptionResult createSubscriptionResult = new TargetCreateSubscriptionResult();
        createSubscriptionResult.setRequestToken(createCheckoutResponse.getId());
        createSubscriptionResult.setZipPaymentRedirectUrl(createCheckoutResponse.getUri());
        return createSubscriptionResult;
    }

}

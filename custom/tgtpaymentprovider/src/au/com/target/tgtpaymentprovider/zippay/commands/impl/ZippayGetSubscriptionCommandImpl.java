/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.commands.impl;

import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.GetSubscriptionDataCommand;
import de.hybris.platform.payment.commands.request.SubscriptionDataRequest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.tgtpayment.commands.result.TargetGetSubscriptionResult;
import au.com.target.tgtpayment.dto.AmountType;
import au.com.target.tgtpayment.dto.LineItem;
import au.com.target.tgtpaymentprovider.constants.TgtpaymentproviderConstants;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayClientException;
import au.com.target.tgtpaymentprovider.zippay.data.response.CheckoutResponse;
import au.com.target.tgtpaymentprovider.zippay.data.types.Item;


/**
 * @author salexa10
 *
 */
public class ZippayGetSubscriptionCommandImpl extends TargetAbstractZippayCommand
        implements GetSubscriptionDataCommand {

    @Override
    public TargetGetSubscriptionResult perform(final SubscriptionDataRequest request) {
        if (request == null) {
            throw new IllegalArgumentException("SubscriptionDataRequest is null");
        }
        // zippay checkoutId
        final String checkoutId = request.getSubscriptionID();
        try {
            final CheckoutResponse response = getTargetZippayClient().retrieveCheckout(checkoutId);
            return translateResponse(response);
        }
        catch (final TargetZippayClientException e) {
            throw new AdapterException(e);
        }
    }

    private List<LineItem> convertItemsToLineItems(final List<Item> items, final String currencyStr) {
        List<LineItem> lineItems = null;

        if (CollectionUtils.isNotEmpty(items)) {
            lineItems = new ArrayList<>();
            for (final Item item : items) {
                // We only need Product SKU, rest can be ignored
                if (StringUtils.isNotEmpty(item.getType())
                        && TgtpaymentproviderConstants.SKU.equalsIgnoreCase(item.getType())) {
                    final LineItem lineItem = new LineItem();
                    lineItem.setName(item.getName());
                    lineItem.setProductCode(item.getReference());
                    lineItem.setQuantity(item.getQuantity());
                    lineItem.setType(item.getType());
                    lineItem.setImageUri(item.getImageUri());
                    lineItem.setPrice(new AmountType(BigDecimal.valueOf(item.getAmount()),
                            getCurrency(currencyStr)));
                    lineItems.add(lineItem);
                }

            }
        }
        return lineItems;
    }

    private TargetGetSubscriptionResult translateResponse(final CheckoutResponse response) {
        final TargetGetSubscriptionResult result = new TargetGetSubscriptionResult();
        if (null != response) {
            result.setOrderState(response.getState());
            if (null != response.getOrder()) {
                result.setAmount(BigDecimal.valueOf(response.getOrder().getAmount()));
                result.setOrderReference(response.getOrder().getReference());
                result.setLineItems(
                        convertItemsToLineItems(response.getOrder().getItems(), response.getOrder().getCurrency()));
            }
        }
        return result;
    }


    private Currency getCurrency(final String currency) {
        final Currency currencyObj;
        if (StringUtils.isEmpty(currency)) {
            currencyObj = Currency.getInstance(TgtpaymentproviderConstants.CURRENCY_AU);
        }
        else {
            currencyObj = Currency.getInstance(currency);
        }
        return currencyObj;
    }
}

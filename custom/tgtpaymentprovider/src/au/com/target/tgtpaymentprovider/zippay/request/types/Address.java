/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.request.types;

import org.codehaus.jackson.annotate.JsonProperty;


/**
 * @author salexa10
 *
 */
public abstract class Address {

    @JsonProperty("postal_code")
    abstract String getPostalCode();

}

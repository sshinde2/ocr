/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.request.types;

import org.codehaus.jackson.annotate.JsonProperty;


/**
 * @author salexa10
 *
 */
public abstract class Config {

    @JsonProperty("redirect_uri")
    abstract String getRedirectUri();

}

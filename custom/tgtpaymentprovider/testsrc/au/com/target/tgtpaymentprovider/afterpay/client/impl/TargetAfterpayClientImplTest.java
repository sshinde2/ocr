/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.client.impl;


import static org.fest.assertions.Assertions.assertThat;
import static org.springframework.test.web.client.match.RequestMatchers.method;
import static org.springframework.test.web.client.match.RequestMatchers.requestTo;
import static org.springframework.test.web.client.response.ResponseCreators.withBadRequest;
import static org.springframework.test.web.client.response.ResponseCreators.withServerError;
import static org.springframework.test.web.client.response.ResponseCreators.withSuccess;

import de.hybris.platform.servicelayer.user.UserService;

import java.io.IOException;
import java.util.Properties;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.ResponseCreator;
import org.springframework.web.client.RestClientException;

import au.com.target.tgtpaymentprovider.afterpay.client.exception.TargetAfterpayClientException;
import au.com.target.tgtpaymentprovider.afterpay.data.request.CapturePaymentRequest;
import au.com.target.tgtpaymentprovider.afterpay.data.request.CreateOrderRequest;
import au.com.target.tgtpaymentprovider.afterpay.data.request.CreateRefundRequest;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CapturePaymentResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CreateOrderResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CreateRefundResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetConfigurationResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetOrderResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetPaymentResponse;
import au.com.target.tgtpaymentprovider.tns.data.request.PayRequestTest;


/**
 * @author bhuang3
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:tgtpaymentprovider-spring-test.xml")
public class TargetAfterpayClientImplTest {

    private final String configurationResponseJson = "[ {\"type\" : \"PAY_BY_INSTALLMENT\","
            + "\"description\" : \"Pay over time\",\"maximumAmount\" : {\"amount\" : \"1000.00\","
            + "\"currency\" : \"AUD\" },\"minimumAmount\" : {\"amount\" : \"80.00\",\"currency\" : \"AUD\" }} ]";

    private final String createOrderResponseJson = "{ \"token\": \"q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu\","
            + " \"expires\": \"2016-05-10T13:14:01Z\" }";

    private final String capturePaymentResponseJson = "{ \"id\": \"12345678\","
            + "\"token\": \"q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu\","
            + "\"status\": \"APPROVED\","
            + "\"created\": \"2015-07-14T10:08:14.123Z\","
            + "\"totalAmount\": {  \"amount\": \"10.00\",\"currency\": \"AUD\"},"
            + "\"merchantReference\": \"merchantOrder-1234\","
            + "\"events\": [{\"created\": \"2016-06-21T10:00:11Z\",\"id\": \"vHxUGE5FO9bBesLnv4eQ\",\"type\":\"CAPTURE\"}],"
            + "\"orderDetails\": {\"consumer\": {  \"phoneNumber\": \"0422042042\",\"givenNames\": \"Joe\",\"surname\": \"Consumer\",\"email\": \"test@afterpay.com.au\"},"
            + "\"billing\": {  \"name\": \"Joe Consumer\",\"line1\": \"Unit 1 16 Floor\",\"line2\": \"380 LaTrobe Street\",\"suburb\": \"Melbourne\",\"state\": \"VIC\",\"postcode\": \"3000\",\"countryCode\": \"AU\",\"phoneNumber\": \"0400892011\"},"
            + "\"shipping\": {  \"name\": \"Joe Consumer\",\"line1\": \"Unit 1 16 Floor\",\"line2\": \"380 LaTrobe Street\",\"suburb\": \"Melbourne\",\"state\": \"VIC\",\"postcode\": \"3000\",\"countryCode\": \"AU\",\"phoneNumber\": \"0400892011\"},"
            + "\"items\":[  {\"name\": \"widget\",\"sku\": \"123412234\",\"quantity\": 1,\"price\": {\"amount\": \"10.00\",\"currency\": \"AUD\"}}]"
            + "}}";

    private final String getPaymentResponseJson = "{\"id\":\"12345678\",\"token\":\"q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu\",\"status\":\"APPROVED\",\"created\":\"2015-07-14T10:08:14.123Z\","
            + "\"totalAmount\":{\"amount\":\"10.00\",\"currency\":\"AUD\"},\"merchantReference\":\"merchantOrder-1234\",\"events\":[{\"created\":\"2016-06-21T10:00:11Z\",\"id\":\"2136442160\","
            + "\"type\":\"AUTHORISE\"},{\"created\":\"2016-06-22T12:00:01Z\",\"id\":\"6558959651\",\"type\":\"CAPTURE\"}],\"refunds\":[{\"id\":\"67890123\",\"refundedAt\":\"2015-07-10T15:01:14.123Z\","
            + "\"merchantReference\":\"refundId-1234\",\"amount\":{\"amount\":\"10.00\",\"currency\":\"AUD\"}}],\"orderDetails\":{\"consumer\":{\"phoneNumber\":\"0422042042\",\"givenNames\":\"Joe\","
            + "\"surname\":\"Consumer\",\"email\":\"test@afterpay.com.au\"},\"billing\":{\"name\":\"Joe Consumer\",\"line1\":\"Unit 1 16 Floor\",\"line2\":\"380 LaTrobe Street\",\"suburb\":\"Melbourne\","
            + "\"state\":\"VIC\",\"postcode\":\"3000\",\"countryCode\":\"AU\",\"phoneNumber\":\"0400892011\"},\"shipping\":{\"name\":\"Joe Consumer\",\"line1\":\"Unit 1 16 Floor\",\"line2\":\"380 LaTrobe Street\","
            + "\"suburb\":\"Melbourne\",\"state\":\"VIC\",\"postcode\":\"3000\",\"countryCode\":\"AU\",\"phoneNumber\":\"0400892011\"},\"items\":[{\"name\":\"widget\",\"sku\":\"123412234\",\"quantity\":1,"
            + "\"price\":{\"amount\":\"10.00\",\"currency\":\"AUD\"}}]}}";

    private final String createRefundResponseJson = "{ \"refundId\": \"1234234\","
            + "\"refundedAt\": \"2015-07-10T15:01:14.123Z\","
            + "\"requestId\":\"61cdad2d-8e10-42ec-a97b-8712dd7a8ca9\","
            + "\"amount\" : { \"amount\" : \"50.00\", \"currency\" : \"AUD\"},"
            + "\"merchantReference\" : \"RF127261AD22\" }";
    private MockRestServiceServer mockServer;

    @Mock
    private UserService userService;

    @InjectMocks
    @Autowired
    private TargetAfterpayClientImpl targetAfterpayClientImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockServer = MockRestServiceServer.createServer(targetAfterpayClientImpl
                .getAfterpayRestTemplate());
    }

    @Test(expected = TargetAfterpayClientException.class)
    public void testConfigurationWithWrongFormatResponse() throws TargetAfterpayClientException {
        mockServer.expect(requestTo("https://api-sandbox.secure-afterpay.com.au/v1/configuration"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("json", MediaType.APPLICATION_JSON));
        targetAfterpayClientImpl.getConfiguration();
    }

    @Test(expected = TargetAfterpayClientException.class)
    public void testConfigurationWithConnectionError() throws TargetAfterpayClientException {
        mockServer.expect(requestTo("https://api-sandbox.secure-afterpay.com.au/v1/configuration"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withServerError());
        targetAfterpayClientImpl.getConfiguration();
    }

    @Test
    public void testConfiguration() throws TargetAfterpayClientException {
        mockServer.expect(requestTo("https://api-sandbox.secure-afterpay.com.au/v1/configuration"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(configurationResponseJson, MediaType.APPLICATION_JSON));
        final GetConfigurationResponse response = targetAfterpayClientImpl.getConfiguration();
        assertThat(response.getDescription()).isEqualTo("Pay over time");
        assertThat(response.getType()).isEqualTo("PAY_BY_INSTALLMENT");
        assertThat(response.getMaximumAmount().getAmount()).isEqualTo("1000.00");
        assertThat(response.getMaximumAmount().getCurrency()).isEqualTo("AUD");
        assertThat(response.getMinimumAmount().getAmount()).isEqualTo("80.00");
        assertThat(response.getMinimumAmount().getCurrency()).isEqualTo("AUD");
    }

    @Test
    public void testConfigurationWithEmptyResponse() throws TargetAfterpayClientException {
        mockServer.expect(requestTo("https://api-sandbox.secure-afterpay.com.au/v1/configuration"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("[]", MediaType.APPLICATION_JSON));
        final GetConfigurationResponse response = targetAfterpayClientImpl.getConfiguration();
        assertThat(response).isNull();
    }

    @Test
    public void testConfigurationWithResponseContainsNullAttributes() throws TargetAfterpayClientException {
        mockServer.expect(requestTo("https://api-sandbox.secure-afterpay.com.au/v1/configuration"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("[{}]", MediaType.APPLICATION_JSON));
        final GetConfigurationResponse response = targetAfterpayClientImpl.getConfiguration();
        assertThat(response).isNotNull();
        assertThat(response.getDescription()).isNull();
        assertThat(response.getMaximumAmount()).isNull();
        assertThat(response.getMinimumAmount()).isNull();
        assertThat(response.getType()).isNull();
    }

    @Test
    public void testIsAfterPayConnectionSuccess() throws TargetAfterpayClientException {
        mockServer.expect(requestTo("https://api-sandbox.secure-afterpay.com.au/ping"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("pong", MediaType.TEXT_PLAIN));
        final boolean result = targetAfterpayClientImpl.isAfterpayConnectionAvailable();
        assertThat(result).isEqualTo(true);
    }

    @Test
    public void testIsAfterPayConnectionFailure() throws TargetAfterpayClientException {
        mockServer.expect(requestTo("https://api-sandbox.secure-afterpay.com.au/ping"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withServerError());
        final boolean result = targetAfterpayClientImpl.isAfterpayConnectionAvailable();
        assertThat(result).isEqualTo(false);
    }

    @Test
    public void testIsAfterPayConnectionBadRequest() throws TargetAfterpayClientException {
        mockServer.expect(requestTo("https://api-sandbox.secure-afterpay.com.au/ping"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withBadRequest());
        final boolean result = targetAfterpayClientImpl.isAfterpayConnectionAvailable();
        assertThat(result).isEqualTo(false);
    }

    @Test
    public void testIsAfterPayConnectionBlankResponse() throws TargetAfterpayClientException {
        mockServer.expect(requestTo("https://api-sandbox.secure-afterpay.com.au/ping"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("", MediaType.TEXT_PLAIN));
        final boolean result = targetAfterpayClientImpl.isAfterpayConnectionAvailable();
        assertThat(result).isEqualTo(true);
    }

    @Test
    public void testCreateOrder() throws TargetAfterpayClientException {
        mockServer.expect(requestTo("https://api-sandbox.secure-afterpay.com.au/v1/orders"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(createOrderResponseJson, MediaType.APPLICATION_JSON));
        final CreateOrderResponse createOrerResponse = targetAfterpayClientImpl.createOrder(new CreateOrderRequest());
        assertThat(createOrerResponse.getToken()).isEqualTo("q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu");
        assertThat(createOrerResponse.getExpires()).isEqualTo("2016-05-10T13:14:01Z");
    }

    @Test(expected = TargetAfterpayClientException.class)
    public void testCreateOrderError() throws TargetAfterpayClientException {
        mockServer.expect(requestTo("https://api-sandbox.secure-afterpay.com.au/v1/orders"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withBadRequest());
        targetAfterpayClientImpl.createOrder(new CreateOrderRequest());
    }


    @Test
    public void testCapturePayment() throws TargetAfterpayClientException {
        mockServer.expect(requestTo("https://api-sandbox.secure-afterpay.com.au/v1/payments/capture"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(capturePaymentResponseJson, MediaType.APPLICATION_JSON));
        final CapturePaymentResponse capturePaymentResponse = targetAfterpayClientImpl
                .capturePayment(new CapturePaymentRequest());
        assertThat(capturePaymentResponse.getToken()).isEqualTo("q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu");
        assertThat(capturePaymentResponse.getHttpStatus().value()).isEqualTo(200);
    }

    @Test
    public void testCapturePaymentHttp400Error() throws TargetAfterpayClientException {
        mockServer.expect(requestTo("https://api-sandbox.secure-afterpay.com.au/v1/payments/capture"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withBadRequest());
        final CapturePaymentResponse capturePaymentResponse = targetAfterpayClientImpl
                .capturePayment(new CapturePaymentRequest());
        assertThat(capturePaymentResponse.getHttpStatus().value()).isEqualTo(400);
    }

    @Test
    public void testCapturePaymentHttp500Error() throws TargetAfterpayClientException {
        mockServer.expect(requestTo("https://api-sandbox.secure-afterpay.com.au/v1/payments/capture"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withServerError());
        final CapturePaymentResponse capturePaymentResponse = targetAfterpayClientImpl
                .capturePayment(new CapturePaymentRequest());
        assertThat(capturePaymentResponse.getHttpStatus().value()).isEqualTo(500);
    }

    @Test(expected = TargetAfterpayClientException.class)
    public void testCapturePaymentHttpTimeout() throws TargetAfterpayClientException {
        mockServer.expect(requestTo("https://api-sandbox.secure-afterpay.com.au/v1/payments/capture"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(new TimeoutResponseCreator());
        final CapturePaymentResponse capturePaymentResponse = targetAfterpayClientImpl
                .capturePayment(new CapturePaymentRequest());
        assertThat(capturePaymentResponse.getHttpStatus().value()).isEqualTo(500);
    }

    @Test
    public void testGetPaymentSuccess() throws TargetAfterpayClientException {
        mockServer.expect(requestTo(
                "https://api-sandbox.secure-afterpay.com.au/v1/payments/token:q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(getPaymentResponseJson, MediaType.APPLICATION_JSON));
        final GetPaymentResponse capturePaymentResponse = targetAfterpayClientImpl
                .getPayment("q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu");
        assertThat(capturePaymentResponse.getToken()).isEqualTo("q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu");
        assertThat(capturePaymentResponse.getStatus()).isEqualTo("APPROVED");
    }

    @Test(expected = TargetAfterpayClientException.class)
    public void testGetPaymentError() throws TargetAfterpayClientException {
        mockServer.expect(requestTo(
                "https://api-sandbox.secure-afterpay.com.au/v1/payments/token:q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withServerError());
        targetAfterpayClientImpl.getPayment("q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu");
    }


    private class TimeoutResponseCreator implements ResponseCreator {

        @Override
        public ClientHttpResponse createResponse(final ClientHttpRequest request) throws IOException {
            throw new RestClientException("Testing timeout exception");
        }

    }

    public void testGetOrderSuccess() throws Exception {

        final ObjectMapper mapper = new ObjectMapper();
        final Properties jsonStringProps = new Properties();
        jsonStringProps.load(PayRequestTest.class.getResourceAsStream("/ExpectedJSON.properties"));
        final String jsonString = jsonStringProps.getProperty("getOrderResponse");
        final GetOrderResponse expectedResponse = mapper.readValue(jsonString, GetOrderResponse.class);

        mockServer
                .expect(requestTo(
                        "https://api-sandbox.secure-afterpay.com.au/v1/orders/q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(jsonString, MediaType.APPLICATION_JSON));
        final GetOrderResponse getOrderResponse = targetAfterpayClientImpl
                .getOrder("q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu");
        assertThat(getOrderResponse.getToken()).isEqualTo(expectedResponse.getToken());
        assertThat(getOrderResponse.getExpires()).isEqualTo(expectedResponse.getExpires());
        assertThat(getOrderResponse.getMerchantReference()).isEqualTo(expectedResponse.getMerchantReference());
        assertThat(getOrderResponse.getConsumer().getGivenNames())
                .isEqualTo(expectedResponse.getConsumer().getGivenNames());

    }

    @Test(expected = TargetAfterpayClientException.class)
    public void testGetOrderError() throws TargetAfterpayClientException {
        mockServer
                .expect(requestTo(
                        "https://api-sandbox.secure-afterpay.com.au/v1/orders/q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withBadRequest());
        targetAfterpayClientImpl.getOrder("q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu");
    }

    @Test
    public void testCreateRefund() throws TargetAfterpayClientException {
        mockServer.expect(requestTo("https://api-sandbox.secure-afterpay.com.au/v1/payments/123456/refund"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(createRefundResponseJson, MediaType.APPLICATION_JSON));
        final CreateRefundResponse createRefundResponse = targetAfterpayClientImpl.createRefund("123456",
                new CreateRefundRequest());
        assertThat(createRefundResponse.getRefundId()).isEqualTo("1234234");
        assertThat(createRefundResponse.getRefundedAt()).isEqualTo("2015-07-10T15:01:14.123Z");
    }

    @Test(expected = TargetAfterpayClientException.class)
    public void testCreateRefundError() throws TargetAfterpayClientException {
        mockServer.expect(requestTo("https://api-sandbox.secure-afterpay.com.au/v1/payments/123456/refund"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withBadRequest());
        targetAfterpayClientImpl.createRefund("123456", new CreateRefundRequest());
    }

}

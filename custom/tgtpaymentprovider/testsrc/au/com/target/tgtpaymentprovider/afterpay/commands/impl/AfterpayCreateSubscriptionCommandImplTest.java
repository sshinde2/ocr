/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.commands.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.dto.AmountType;
import au.com.target.tgtpayment.dto.Customer;
import au.com.target.tgtpayment.dto.LineItem;
import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpaymentprovider.afterpay.client.TargetAfterpayClient;
import au.com.target.tgtpaymentprovider.afterpay.client.exception.TargetAfterpayClientException;
import au.com.target.tgtpaymentprovider.afterpay.data.request.CreateOrderRequest;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CreateOrderResponse;
import au.com.target.tgtpaymentprovider.afterpay.util.TargetAfterpayHelper;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AfterpayCreateSubscriptionCommandImplTest {

    private final TargetAfterpayHelper targetAfterpayHelper = new TargetAfterpayHelper();

    @Mock
    private TargetAfterpayClient targetAfterpayClient;

    @InjectMocks
    private final AfterpayCreateSubscriptionCommandImpl command = new AfterpayCreateSubscriptionCommandImpl();

    @Mock
    private TargetCreateSubscriptionRequest paramRequest;

    @Before
    public void setup() {
        command.setTargetAfterpayHelper(targetAfterpayHelper);
    }

    @Test
    public void testPerform() throws TargetAfterpayClientException {
        final Order order = mock(Order.class);
        final AmountType totalAmount = mock(AmountType.class);
        given(totalAmount.getAmount()).willReturn(BigDecimal.valueOf(10));
        given(totalAmount.getCurrency()).willReturn(Currency.getInstance("AUD"));
        given(order.getOrderTotalAmount()).willReturn(totalAmount);
        final Customer customer = mock(Customer.class);
        given(customer.getGivenNames()).willReturn("Barry");
        given(customer.getSurname()).willReturn("Allen");
        given(customer.getEmail()).willReturn("barry@starlabs.com");
        given(order.getCustomer()).willReturn(customer);
        final List<LineItem> lineItems = new ArrayList<>();
        final LineItem lineItem1 = mock(LineItem.class);
        given(lineItem1.getName()).willReturn("Product 1");
        given(lineItem1.getProductCode()).willReturn("p1");
        willReturn(Long.valueOf(1)).given(lineItem1).getQuantity();
        final AmountType price1 = mock(AmountType.class);
        given(price1.getAmount()).willReturn(BigDecimal.valueOf(4));
        given(price1.getCurrency()).willReturn(Currency.getInstance("AUD"));
        given(lineItem1.getPrice()).willReturn(price1);
        lineItems.add(lineItem1);
        final LineItem lineItem2 = mock(LineItem.class);
        given(lineItem2.getName()).willReturn("Product 2");
        given(lineItem2.getProductCode()).willReturn("p2");
        willReturn(Long.valueOf(2)).given(lineItem2).getQuantity();
        final AmountType price2 = mock(AmountType.class);
        given(price2.getAmount()).willReturn(BigDecimal.valueOf(3));
        given(price2.getCurrency()).willReturn(Currency.getInstance("AUD"));
        given(lineItem2.getPrice()).willReturn(price2);
        lineItems.add(lineItem2);
        given(order.getLineItems()).willReturn(lineItems);
        given(paramRequest.getOrder()).willReturn(order);
        given(paramRequest.getReturnUrl()).willReturn("returnUrl");
        given(paramRequest.getCancelUrl()).willReturn("cancelUrl");
        final CreateOrderResponse response = mock(CreateOrderResponse.class);
        given(response.getToken()).willReturn("token");
        given(targetAfterpayClient.createOrder(Mockito.any(CreateOrderRequest.class)))
                .willReturn(response);
        final TargetCreateSubscriptionResult subscriptionResult = command.perform(paramRequest);
        assertThat(subscriptionResult).isNotNull();
        assertThat(subscriptionResult.getRequestToken()).isEqualTo("token");
        final ArgumentCaptor<CreateOrderRequest> createOrderRequestCapture = ArgumentCaptor
                .forClass(CreateOrderRequest.class);
        verify(targetAfterpayClient).createOrder(createOrderRequestCapture.capture());
        final CreateOrderRequest createOrderRequest = createOrderRequestCapture.getValue();
        assertThat(createOrderRequest).isNotNull();
        assertThat(createOrderRequest.getTotalAmount().getAmount()).isEqualTo("10.00");
        assertThat(createOrderRequest.getTotalAmount().getCurrency()).isEqualTo("AUD");
        assertThat(createOrderRequest.getConsumer().getGivenNames()).isEqualTo("Barry");
        assertThat(createOrderRequest.getConsumer().getSurname()).isEqualTo("Allen");
        assertThat(createOrderRequest.getConsumer().getEmail()).isEqualTo("barry@starlabs.com");
        assertThat(createOrderRequest.getMerchant().getRedirectConfirmUrl()).isEqualTo("returnUrl");
        assertThat(createOrderRequest.getMerchant().getRedirectCancelUrl()).isEqualTo("cancelUrl");
        assertThat(createOrderRequest.getItems()).hasSize(2);
        assertThat(createOrderRequest.getItems()).onProperty("quantity").containsExactly(Long.valueOf(1),
                Long.valueOf(2));
        assertThat(createOrderRequest.getItems()).onProperty("price.amount").containsExactly("4.00", "3.00");
        assertThat(createOrderRequest.getItems()).onProperty("price.currency").containsExactly("AUD", "AUD");
        assertThat(createOrderRequest.getItems()).onProperty("name").containsExactly("Product 1", "Product 2");
        assertThat(createOrderRequest.getItems()).onProperty("sku").containsExactly("p1", "p2");
    }

    @Test
    public void testPerformWithDiscountItems() throws TargetAfterpayClientException {
        final Order order = mock(Order.class);
        final AmountType totalAmount = mock(AmountType.class);
        given(totalAmount.getAmount()).willReturn(BigDecimal.valueOf(10));
        given(totalAmount.getCurrency()).willReturn(Currency.getInstance("AUD"));
        given(order.getOrderTotalAmount()).willReturn(totalAmount);
        final Customer customer = mock(Customer.class);
        given(customer.getGivenNames()).willReturn("Barry");
        given(customer.getSurname()).willReturn("Allen");
        given(customer.getEmail()).willReturn("barry@starlabs.com");
        given(order.getCustomer()).willReturn(customer);
        final List<LineItem> lineItems = new ArrayList<>();
        final LineItem lineItem1 = mock(LineItem.class);
        given(lineItem1.getName()).willReturn("Product 1");
        given(lineItem1.getProductCode()).willReturn("p1");
        willReturn(Long.valueOf(1)).given(lineItem1).getQuantity();
        final AmountType price1 = mock(AmountType.class);
        given(price1.getAmount()).willReturn(BigDecimal.valueOf(10));
        given(price1.getCurrency()).willReturn(Currency.getInstance("AUD"));
        given(lineItem1.getPrice()).willReturn(price1);
        lineItems.add(lineItem1);
        final LineItem lineItem2 = mock(LineItem.class);
        given(lineItem2.getName()).willReturn("voucher");
        given(lineItem2.getProductCode()).willReturn("redemptionCode");
        willReturn(Long.valueOf(1)).given(lineItem2).getQuantity();
        final AmountType price2 = mock(AmountType.class);
        given(price2.getAmount()).willReturn(BigDecimal.valueOf(-3));
        given(price2.getCurrency()).willReturn(Currency.getInstance("AUD"));
        given(lineItem2.getPrice()).willReturn(price2);
        lineItems.add(lineItem2);
        given(order.getLineItems()).willReturn(lineItems);
        given(paramRequest.getOrder()).willReturn(order);
        given(paramRequest.getReturnUrl()).willReturn("returnUrl");
        given(paramRequest.getCancelUrl()).willReturn("cancelUrl");
        final CreateOrderResponse response = mock(CreateOrderResponse.class);
        given(response.getToken()).willReturn("token");
        given(targetAfterpayClient.createOrder(Mockito.any(CreateOrderRequest.class)))
                .willReturn(response);
        final TargetCreateSubscriptionResult subscriptionResult = command.perform(paramRequest);
        assertThat(subscriptionResult).isNotNull();
        assertThat(subscriptionResult.getRequestToken()).isEqualTo("token");
        final ArgumentCaptor<CreateOrderRequest> createOrderRequestCapture = ArgumentCaptor
                .forClass(CreateOrderRequest.class);
        verify(targetAfterpayClient).createOrder(createOrderRequestCapture.capture());
        final CreateOrderRequest createOrderRequest = createOrderRequestCapture.getValue();
        assertThat(createOrderRequest).isNotNull();
        assertThat(createOrderRequest.getTotalAmount().getAmount()).isEqualTo("10.00");
        assertThat(createOrderRequest.getTotalAmount().getCurrency()).isEqualTo("AUD");
        assertThat(createOrderRequest.getConsumer().getGivenNames()).isEqualTo("Barry");
        assertThat(createOrderRequest.getConsumer().getSurname()).isEqualTo("Allen");
        assertThat(createOrderRequest.getConsumer().getEmail()).isEqualTo("barry@starlabs.com");
        assertThat(createOrderRequest.getMerchant().getRedirectConfirmUrl()).isEqualTo("returnUrl");
        assertThat(createOrderRequest.getMerchant().getRedirectCancelUrl()).isEqualTo("cancelUrl");
        assertThat(createOrderRequest.getItems()).hasSize(1);
        assertThat(createOrderRequest.getItems()).onProperty("quantity").containsExactly(Long.valueOf(1));
        assertThat(createOrderRequest.getItems()).onProperty("price.amount").containsExactly("10.00");
        assertThat(createOrderRequest.getItems()).onProperty("price.currency").containsExactly("AUD");
        assertThat(createOrderRequest.getItems()).onProperty("name").containsExactly("Product 1");
        assertThat(createOrderRequest.getItems()).onProperty("sku").containsExactly("p1");
    }

    @Test(expected = AdapterException.class)
    public void testPerformWithClientExpection() throws TargetAfterpayClientException {
        given(targetAfterpayClient.createOrder(Mockito.any(CreateOrderRequest.class)))
                .willThrow(new TargetAfterpayClientException());
        command.perform(paramRequest);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPerformWithNullRequest() throws TargetAfterpayClientException {
        command.perform(null);
    }

}

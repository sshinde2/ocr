/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.commands.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.request.SubscriptionDataRequest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.result.TargetGetSubscriptionResult;
import au.com.target.tgtpaymentprovider.afterpay.client.TargetAfterpayClient;
import au.com.target.tgtpaymentprovider.afterpay.client.exception.TargetAfterpayClientException;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetOrderResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.types.Item;
import au.com.target.tgtpaymentprovider.afterpay.data.types.Money;
import au.com.target.tgtpaymentprovider.afterpay.util.TargetAfterpayHelper;


/**
 * @author gsing236
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AfterpayGetSubscriptionCommandImplTest {

    @InjectMocks
    private final AfterpayGetSubscriptionCommandImpl command = new AfterpayGetSubscriptionCommandImpl();

    private final TargetAfterpayHelper targetAfterpayHelper = new TargetAfterpayHelper();
    @Mock
    private TargetAfterpayClient targetAfterpayClient;

    @Before
    public void setUp() {
        command.setTargetAfterpayHelper(targetAfterpayHelper);
    }

    @Test
    public void testPerform() throws Exception {

        final SubscriptionDataRequest request = mock(SubscriptionDataRequest.class);
        final GetOrderResponse response = new GetOrderResponse();
        response.setToken("token");
        final Money totalAmount = new Money();
        totalAmount.setAmount("25.30");
        totalAmount.setCurrency("AUD");
        response.setTotalAmount(totalAmount);
        final ArrayList<Item> items = new ArrayList<>();
        final Item item = new Item();
        item.setName("name");
        item.setQuantity(1);
        item.setSku("12345");
        item.setPrice(totalAmount);
        items.add(item);
        response.setItems(items);
        given(targetAfterpayClient.getOrder(request.getSubscriptionID())).willReturn(response);
        final TargetGetSubscriptionResult result = command.perform(request);

        assertThat(result.getAfterpayToken()).isEqualTo("token");
        assertThat(result.getAmount()).isEqualTo(new BigDecimal("25.30"));
        assertThat(result.getLineItems().get(0).getName()).isEqualTo("name");
        assertThat(result.getLineItems().get(0).getQuantity()).isEqualTo(1);
        assertThat(result.getLineItems().get(0).getProductCode()).isEqualTo("12345");
        assertThat(result.getLineItems().get(0).getPrice().getAmount()).isEqualTo(new BigDecimal("25.30"));
        assertThat(result.getLineItems().get(0).getPrice().getCurrency()).isEqualTo(Currency.getInstance("AUD"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPerformWhenRequestNull() {
        command.perform(null);
    }

    @Test(expected = AdapterException.class)
    public void testPerformWhenAfterpayClientException() throws TargetAfterpayClientException {

        final SubscriptionDataRequest request = mock(SubscriptionDataRequest.class);

        given(targetAfterpayClient.getOrder(request.getSubscriptionID()))
                .willThrow(new TargetAfterpayClientException());
        command.perform(request);
    }
}

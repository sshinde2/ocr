/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.commands.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.TransactionStatus;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;
import au.com.target.tgtpaymentprovider.afterpay.client.TargetAfterpayClient;
import au.com.target.tgtpaymentprovider.afterpay.client.exception.TargetAfterpayClientException;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetPaymentResponse;
import au.com.target.tgtpaymentprovider.afterpay.util.TargetAfterpayHelper;


/**
 * @author gsing236
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AfterpayRetrieveTransactionCommandImplTest {

    @InjectMocks
    private final AfterpayRetrieveTransactionCommandImpl command = new AfterpayRetrieveTransactionCommandImpl();

    @Mock
    private TargetAfterpayClient targetAfterpayClient;
    @Mock
    private TargetAfterpayHelper targetAfterpayHelper;

    @Test(expected = IllegalArgumentException.class)
    public void testPerformWhenRequestIsNull() {
        command.perform(null);
    }

    @Test
    public void testPerform() throws Exception {
        final String afterpayToken = "afterpayToken";
        final TargetRetrieveTransactionRequest request = new TargetRetrieveTransactionRequest();
        request.setSessionToken(afterpayToken);

        final GetPaymentResponse response = new GetPaymentResponse();
        response.setStatus("APPROVED");
        response.setToken(afterpayToken);
        given(targetAfterpayClient.getPayment(afterpayToken)).willReturn(response);
        final TargetRetrieveTransactionResult result = command.perform(request);
        assertThat(result.getTransactionStatus()).isEqualTo(TransactionStatus.ACCEPTED);
        assertThat(result.getRequestToken()).isEqualTo(afterpayToken);
    }

    @Test(expected = AdapterException.class)
    public void testPerformWhenAfterpayServiceUnavailable() throws Exception {
        final String afterpayToken = "afterpayToken";
        final TargetRetrieveTransactionRequest request = new TargetRetrieveTransactionRequest();
        request.setSessionToken(afterpayToken);

        final GetPaymentResponse response = new GetPaymentResponse();
        response.setStatus("APPROVED");
        response.setToken(afterpayToken);
        given(targetAfterpayClient.getPayment(afterpayToken)).willThrow(new TargetAfterpayClientException());
        command.perform(request);
    }
}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.commands.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetGetPaymentConfigurationRequest;
import au.com.target.tgtpayment.commands.result.TargetGetPaymentConfigurationResult;
import au.com.target.tgtpaymentprovider.afterpay.client.TargetAfterpayClient;
import au.com.target.tgtpaymentprovider.afterpay.client.exception.TargetAfterpayClientException;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetConfigurationResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.types.Money;
import au.com.target.tgtpaymentprovider.afterpay.util.TargetAfterpayHelper;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetAfterpayGetConfigurationCommandImplTest {

    private final TargetGetPaymentConfigurationRequest paramRequest = new TargetGetPaymentConfigurationRequest();

    @InjectMocks
    private final TargetAfterpayGetConfigurationCommandImpl command = new TargetAfterpayGetConfigurationCommandImpl();

    private final TargetAfterpayHelper targetAfterpayHelper = new TargetAfterpayHelper();

    @Mock
    private TargetAfterpayClient targetAfterpayClient;

    @Before
    public void setup() {
        command.setTargetAfterpayHelper(targetAfterpayHelper);
    }

    @Test
    public void testPerformWithEmptyResponse() throws TargetAfterpayClientException {
        given(targetAfterpayClient.getConfiguration()).willReturn(null);
        final TargetGetPaymentConfigurationResult result = command.perform(paramRequest);
        assertThat(result).isNull();
    }

    @Test(expected = AdapterException.class)
    public void testPerformWithClientExpection() throws TargetAfterpayClientException {
        given(targetAfterpayClient.getConfiguration()).willThrow(new TargetAfterpayClientException());
        command.perform(paramRequest);
    }

    @Test
    public void testPerformWithNormalResponse() throws TargetAfterpayClientException {
        final String description = "description";
        final String type = "type";
        final GetConfigurationResponse configuration = new GetConfigurationResponse();
        final Money max = new Money();
        max.setCurrency("Aud");
        max.setAmount("100.22");
        final Money min = new Money();
        min.setCurrency("Aud");
        min.setAmount("12.00");
        configuration.setDescription(description);
        configuration.setType(type);
        configuration.setMaximumAmount(max);
        configuration.setMinimumAmount(min);
        given(targetAfterpayClient.getConfiguration()).willReturn(configuration);
        final TargetGetPaymentConfigurationResult result = command.perform(paramRequest);
        assertThat(result.getDescription()).isEqualTo(description);
        assertThat(result.getType()).isEqualTo(type);
        assertThat(result.getMaximumAmount()).isEqualTo(new BigDecimal(100.22d).setScale(2, BigDecimal.ROUND_HALF_UP));
        assertThat(result.getMinimumAmount()).isEqualTo(new BigDecimal(12.00d).setScale(2, BigDecimal.ROUND_HALF_UP));
    }

    @Test(expected = AdapterException.class)
    public void testPerformWithInvalidAmount() throws TargetAfterpayClientException {
        final String description = "description";
        final String type = "type";
        final GetConfigurationResponse configuration = new GetConfigurationResponse();
        final Money max = new Money();
        max.setCurrency("Aud");
        max.setAmount("invalid");
        final Money min = new Money();
        min.setCurrency("Aud");
        min.setAmount("invalid");
        configuration.setDescription(description);
        configuration.setType(type);
        configuration.setMaximumAmount(max);
        configuration.setMinimumAmount(min);
        given(targetAfterpayClient.getConfiguration()).willReturn(configuration);
        command.perform(paramRequest);
    }
}

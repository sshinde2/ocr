/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.commands.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetPaymentPingRequest;
import au.com.target.tgtpayment.commands.result.TargetPaymentPingResult;
import au.com.target.tgtpaymentprovider.afterpay.client.TargetAfterpayClient;


/**
 * @author gsing236
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetAfterpayPaymentPingCommandImplTest {

    private final TargetPaymentPingRequest request = new TargetPaymentPingRequest();

    @Mock
    private TargetAfterpayClient targetAfterpayClient;

    @InjectMocks
    private final TargetAfterpayPaymentPingCommandImpl command = new TargetAfterpayPaymentPingCommandImpl();

    @Test
    public void testPingSuccessTrue() {
        given(Boolean.valueOf(targetAfterpayClient.isAfterpayConnectionAvailable())).willReturn(Boolean.TRUE);
        final TargetPaymentPingResult result = command.perform(request);
        assertThat(result).isNotNull();
        assertThat(result.isSuccess()).isTrue();
    }

    @Test
    public void testPingSuccessFalse() {
        given(Boolean.valueOf(targetAfterpayClient.isAfterpayConnectionAvailable())).willReturn(Boolean.FALSE);
        final TargetPaymentPingResult result = command.perform(request);
        assertThat(result).isNotNull();
        assertThat(result.isSuccess()).isFalse();
    }
}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.afterpay.util;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;

import java.math.BigDecimal;
import java.util.Currency;

import org.junit.Test;

import au.com.target.tgtpayment.dto.AmountType;
import au.com.target.tgtpaymentprovider.afterpay.data.types.Money;


/**
 * @author bhuang3
 *
 */
@UnitTest
public class TargetAfterpayHelperTest {

    private final TargetAfterpayHelper helper = new TargetAfterpayHelper();

    @Test
    public void testConvertMoneyToBigDecimal() {
        final Money money = new Money();
        money.setAmount("16.23");
        money.setCurrency("aud");
        final BigDecimal expected = new BigDecimal(16.23d).setScale(2, BigDecimal.ROUND_HALF_UP);
        final BigDecimal result = helper.convertMoneyToBigDecimal(money);
        assertThat(result).isEqualTo(expected);
    }

    @Test(expected = AdapterException.class)
    public void testConvertMoneyToBigDecimalWithNaN() {
        final Money money = new Money();
        money.setAmount("Nan");
        money.setCurrency("aud");
        helper.convertMoneyToBigDecimal(money);
    }

    @Test
    public void testConvertMoneyToBigDecimalWithEmptyAcountString() {
        final Money money = new Money();
        money.setCurrency("aud");
        final BigDecimal result = helper.convertMoneyToBigDecimal(money);
        assertThat(result).isNull();
    }

    @Test
    public void testConvertAmountToMoney() {
        final AmountType amount = mock(AmountType.class);
        given(amount.getAmount()).willReturn(BigDecimal.valueOf(2));
        given(amount.getCurrency()).willReturn(Currency.getInstance("AUD"));
        final Money money = helper.convertAmountToMoney(amount);
        assertThat(money.getAmount()).isEqualTo("2.00");
        assertThat(money.getCurrency()).isEqualTo("AUD");
    }

    @Test
    public void testConvertAmountToMoneyRounding() {
        final AmountType amount = mock(AmountType.class);
        given(amount.getAmount()).willReturn(BigDecimal.valueOf(2.675));
        final Currency currency = Currency.getInstance("AUD");
        given(amount.getCurrency()).willReturn(currency);
        final Money money = helper.convertAmountToMoney(amount);
        assertThat(money.getAmount()).isEqualTo("2.68");
        assertThat(money.getCurrency()).isEqualTo("AUD");
    }

    @Test(expected = AdapterException.class)
    public void testConvertAmountToMoneyNull() {
        helper.convertAmountToMoney(null);
    }

    @Test(expected = AdapterException.class)
    public void testConvertAmountToMoneyNullAmount() {
        final AmountType amount = mock(AmountType.class);
        given(amount.getCurrency()).willReturn(Currency.getInstance("AUD"));
        helper.convertAmountToMoney(amount);
    }

    @Test(expected = AdapterException.class)
    public void testConvertAmountToMoneyNullCurrency() {
        final AmountType amount = mock(AmountType.class);
        given(amount.getAmount()).willReturn(BigDecimal.valueOf(2));
        helper.convertAmountToMoney(amount);
    }

    @Test
    public void testConvertMoneyToAmount() {
        final Money money = new Money();
        money.setAmount("16.23");
        money.setCurrency("AUD");
        final AmountType amount = helper.convertMoneyToAmount(money);
        assertThat(amount.getAmount()).isEqualTo(new BigDecimal("16.23"));
        assertThat(amount.getCurrency().getCurrencyCode()).isEqualTo("AUD");
    }

    @Test(expected = AdapterException.class)
    public void testConvertMoneyToAmountNullAmount() {
        final Money money = new Money();
        money.setAmount(null);
        money.setCurrency("AUD");
        helper.convertMoneyToAmount(money);
    }

    @Test(expected = AdapterException.class)
    public void testConvertMoneyToAmountNullCurrency() {
        final Money money = new Money();
        money.setAmount("16.23");
        money.setCurrency(null);
        helper.convertMoneyToAmount(money);
    }

    @Test(expected = AdapterException.class)
    public void testConvertMoneyToAmountNullMoney() {
        helper.convertMoneyToAmount(null);
    }

    @Test
    public void testConvertBigDecimalAndCurrencyToMoney() {
        final Money money = helper.convertBigDecimalAndCurrencyToMoney(BigDecimal.valueOf(10),
                Currency.getInstance("AUD"));
        assertThat(money.getAmount()).isEqualTo("10.00");
        assertThat(money.getCurrency()).isEqualTo("AUD");
    }
}

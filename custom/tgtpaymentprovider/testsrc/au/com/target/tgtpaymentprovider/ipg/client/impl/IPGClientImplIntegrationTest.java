/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.client.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.XmlMappingException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSinglePaymentRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSingleRefundRequest;
import au.com.target.tgtpaymentprovider.ipg.data.soap.AdditionalData;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Criteria;
import au.com.target.tgtpaymentprovider.ipg.data.soap.QueryResponse;
import au.com.target.tgtpaymentprovider.ipg.data.soap.QueryTransaction;
import au.com.target.tgtpaymentprovider.ipg.data.soap.RefundInfo;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Response;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Transaction;
import au.com.target.tgtpaymentprovider.ipg.exception.IPGClientException;


@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:tgtpaymentprovider-spring-test.xml")
@ActiveProfiles("dev")
public class IPGClientImplIntegrationTest {


    @Autowired
    private IPGClientImpl ipgClient;


    @Test
    public void testSumbitSinglePayment() throws XmlMappingException, IOException, IPGClientException {
        final SubmitSinglePaymentRequest request = new SubmitSinglePaymentRequest();
        final Transaction transaction = new Transaction();
        request.setTransaction(transaction);
        final Response response = ipgClient.submitSinglePayment(request).getResponse();
        assertEquals("1", response.getResponseCode());
        assertEquals("Invalid credit card expiry date", response.getDeclinedMessage());
        assertEquals("111", response.getDeclinedCode());
    }

    @Test
    public void testSumbitSingleRefund() throws XmlMappingException, IOException,
            IPGClientException {
        final SubmitSingleRefundRequest request = new SubmitSingleRefundRequest();
        final RefundInfo refundInfo = new RefundInfo();
        refundInfo.setReceiptNumber("12345");
        refundInfo.setAmount("1500");
        request.setRefundInfo(refundInfo);
        final Response response = ipgClient.submitSingleRefund(request).getResponse();
        assertEquals("1", response.getResponseCode());
        assertEquals("Purchase or Capture transaction was not found", response.getDeclinedMessage());
        assertEquals("159", response.getDeclinedCode());
    }

    @Test
    public void testQueryTransaction() throws IPGClientException {
        final QueryTransaction queryRequest = new QueryTransaction();
        final Criteria criteria = new Criteria();
        criteria.setCustRef("53546004");
        criteria.setTrnStartTimestamp("03-Aug-2015 09:32:43");
        criteria.setTrnEndTimestamp("03-Aug-2017 09:32:43");
        queryRequest.setCriteria(criteria);
        final QueryResponse response = ipgClient.queryTransaction(queryRequest);
        assertNotNull(response);
    }

    @Test
    public void testQueryTransactionWithReceipt() throws IPGClientException {
        final QueryTransaction queryRequest = new QueryTransaction();
        final Criteria criteria = new Criteria();
        criteria.setReceipt("90609267");
        criteria.setTrnStartTimestamp("03-Aug-2015 09:32:43");
        criteria.setTrnEndTimestamp("03-Aug-2017 09:32:43");
        queryRequest.setCriteria(criteria);
        final AdditionalData addtionalData = new AdditionalData();
        addtionalData.addCore("Amount");
        addtionalData.addCore("CustRef");
        addtionalData.addCore("CustNumber");
        addtionalData.addCore("TrnTypeID");
        addtionalData.addCore("TrnStatusID");
        queryRequest.setAddtionalData(addtionalData);
        final QueryResponse response = ipgClient.queryTransaction(queryRequest);
        assertNotNull(response);
    }

}

package au.com.target.tgtpaymentprovider.ipg.commands.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetPaymentVoidRequest;
import au.com.target.tgtpayment.commands.result.TargetCardPaymentResult;
import au.com.target.tgtpayment.commands.result.TargetPaymentVoidResult;
import au.com.target.tgtpaymentprovider.ipg.exception.IpgServiceException;
import au.com.target.tgtpaymentprovider.ipg.service.IpgService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class IpgPaymentVoidCommandImplTest {

    private static final String RESPONSE_CODE_SUCCESS = "0";
    private static final String RESPONSE_CODE_FAIL = "1";

    @InjectMocks
    private final IpgPaymentVoidCommandImpl voidCommand = new IpgPaymentVoidCommandImpl();

    @Mock
    private IpgService ipgService;

    @Mock
    private TargetPaymentVoidRequest request;

    @Before
    public void setup() {
        request = new TargetPaymentVoidRequest();
    }

    @Test(expected = AdapterException.class)
    public void testPerformWhenIpgServiceThrowsException() throws IpgServiceException {
        given(ipgService.submitSingleVoid(request)).willThrow(new IpgServiceException());
        voidCommand.perform(request);
    }

    @Test
    public void testMapResponeWithNoResultFromIpg() {
        final TargetCardPaymentResult ipgResult = null;
        final TargetPaymentVoidResult paymentVoidResult = voidCommand.mapResponse(ipgResult);
        assertNull(paymentVoidResult);
    }

    @Test
    public void testMapResponeWithSuccessResultFromIpg() {
        final String receiptNumber = "132456987";
        final String responseText = "response text";

        final TargetCardPaymentResult ipgResult = new TargetCardPaymentResult();
        ipgResult.setResponseCode(RESPONSE_CODE_SUCCESS);
        ipgResult.setReceiptNumber(receiptNumber);
        ipgResult.setResponseText(responseText);

        final TargetPaymentVoidResult paymentVoidResult = voidCommand.mapResponse(ipgResult);

        assertNotNull(paymentVoidResult);
        assertNotNull(paymentVoidResult.getVoidResult());
        assertEquals(receiptNumber, paymentVoidResult.getVoidResult().getReceiptNumber());
        assertEquals(RESPONSE_CODE_SUCCESS, paymentVoidResult.getVoidResult().getResponseCode());
        assertEquals(responseText, paymentVoidResult.getVoidResult().getResponseText());
        assertTrue(paymentVoidResult.getVoidResult().isPaymentSuccessful());
    }

    @Test
    public void testMapResponeWithFailureResultFromIpg() {
        final String responseText = "response text";
        final String declinedCode = "11";
        final String declinedMessage = "declined";

        final TargetCardPaymentResult ipgResult = new TargetCardPaymentResult();
        ipgResult.setResponseCode(RESPONSE_CODE_FAIL);
        ipgResult.setResponseText(responseText);
        ipgResult.setDeclinedCode(declinedCode);
        ipgResult.setDeclinedMessage(declinedMessage);

        final TargetPaymentVoidResult paymentVoidResult = voidCommand.mapResponse(ipgResult);

        assertNotNull(paymentVoidResult);
        assertNotNull(paymentVoidResult.getVoidResult());
        assertEquals(RESPONSE_CODE_FAIL, paymentVoidResult.getVoidResult().getResponseCode());
        assertEquals(responseText, paymentVoidResult.getVoidResult().getResponseText());
        assertEquals(declinedCode, paymentVoidResult.getVoidResult().getDeclinedCode());
        assertEquals(declinedMessage, paymentVoidResult.getVoidResult().getDeclinedMessage());
        assertFalse(paymentVoidResult.getVoidResult().isPaymentSuccessful());
    }

    @Test
    public void testPerformSuccess() throws IpgServiceException {
        final String receiptNumber = "132456987";
        final String responseText = "response text";

        final TargetCardPaymentResult ipgResult = new TargetCardPaymentResult();
        ipgResult.setResponseCode(RESPONSE_CODE_SUCCESS);
        ipgResult.setResponseText(responseText);
        ipgResult.setReceiptNumber(receiptNumber);

        given(ipgService.submitSingleVoid(request)).willReturn(ipgResult);

        final TargetPaymentVoidResult result = voidCommand.perform(request);

        assertNotNull(result);
        assertNotNull(result.getVoidResult());
        assertEquals(receiptNumber, result.getVoidResult().getReceiptNumber());
        assertEquals(RESPONSE_CODE_SUCCESS, result.getVoidResult().getResponseCode());
    }
}
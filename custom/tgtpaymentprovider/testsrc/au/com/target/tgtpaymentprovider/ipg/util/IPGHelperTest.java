/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.util;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import au.com.target.tgtpaymentprovider.ipg.data.CardDetails;
import au.com.target.tgtpaymentprovider.ipg.data.CardResultsData;
import au.com.target.tgtpaymentprovider.ipg.data.SavedCardsData;
import au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSinglePaymentRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.SubmitSingleRefundRequest;
import au.com.target.tgtpaymentprovider.ipg.data.soap.RefundInfo;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Response;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Security;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Transaction;
import au.com.target.tgtpaymentprovider.ipg.data.soap.VoidInfo;


/**
 * @author mjanarth
 *
 */
@UnitTest
public class IPGHelperTest {

    private final IPGHelper ipgHelper = new IPGHelper();

    @Test
    public void testConvertSavedCardRequestWithNullRequest() throws JAXBException {
        final SavedCardsData request = null;
        final String xmlString = ipgHelper.convertSavedCardRequestToXML(request);
        assertThat(xmlString).isNull();
    }

    @Test
    public void testConvertSavedCardRequestWithEmptyCards() throws JAXBException {
        final List<CardDetails> cards = new ArrayList<>();
        final SavedCardsData request = new SavedCardsData();
        request.setCardRequests(cards);
        final String xmlString = ipgHelper.convertSavedCardRequestToXML(request);
        assertThat(xmlString).isEqualTo("<SavedCards/>");
    }

    @Test
    public void testConvertSavedCardRequestWithSingleCardToXML() throws JAXBException {
        final List<CardDetails> cards = new ArrayList<>();
        final SavedCardsData request = new SavedCardsData();
        final CardDetails card = new CardDetails();
        card.setBin("1234");
        card.setTokenExpiry("12/06/2016");
        card.setCardType("VISA");
        card.setCardNumber("12345678998765");
        card.setIsDefaultCard("1");
        card.setCardExpiry("12/06/2016");
        card.setToken("abdc##789034");
        card.setCardOnFile("TRUE");
        card.setFirstCredentialStorage("TRUE");
        cards.add(card);
        request.setCardRequests(cards);
        final String xmlString = ipgHelper.convertSavedCardRequestToXML(request);
        final StringBuilder expectedPayload = new StringBuilder();
        expectedPayload.append("<SavedCards><Card>");
        expectedPayload.append("<Token>abdc##789034</Token>");
        expectedPayload.append("<TokenExpiry>12/06/2016</TokenExpiry>");
        expectedPayload.append("<CardType>VISA</CardType>");
        expectedPayload.append("<MaskedCard>12345678998765</MaskedCard>");
        expectedPayload.append("<CardExpiry>12/06/2016</CardExpiry>");
        expectedPayload.append("<DefaultCard>1</DefaultCard>");
        expectedPayload.append("<BIN>1234</BIN>");
        expectedPayload.append("<IsCardOnFile>TRUE</IsCardOnFile>");
        expectedPayload.append("<IsFirstCredentialStorage>TRUE</IsFirstCredentialStorage>");
        expectedPayload.append("</Card></SavedCards>");
        assertThat(xmlString).isEqualTo(expectedPayload.toString());
    }


    @Test
    public void testConvertSavedCardRequestWithTwoCardsToXML() throws JAXBException {
        final List<CardDetails> cards = new ArrayList<>();
        final SavedCardsData request = new SavedCardsData();
        final CardDetails cardVisa = new CardDetails();
        final CardDetails cardMaster = new CardDetails();
        cardVisa.setBin("1234");
        cardVisa.setCardExpiry("12/06/2016");
        cardVisa.setTokenExpiry("12/06/2016");
        cardVisa.setCardType("VISA");
        cardVisa.setCardNumber("12345678998765");
        cardVisa.setIsDefaultCard("1");
        cardVisa.setToken("abdc##789034");
        cardVisa.setCardOnFile("TRUE");
        cardVisa.setFirstCredentialStorage("TRUE");
        cardMaster.setBin("1234");
        cardMaster.setCardExpiry("12/06/2016");
        cardMaster.setTokenExpiry("12/06/2016");
        cardMaster.setCardType("Master");
        cardMaster.setCardNumber("12345678998765");
        cardMaster.setIsDefaultCard("0");
        cardMaster.setToken("abdc##789034");
        cardMaster.setCardOnFile("FALSE");
        cardMaster.setFirstCredentialStorage(null);
        cards.add(cardVisa);
        cards.add(cardMaster);
        request.setCardRequests(cards);
        final String xmlString = ipgHelper.convertSavedCardRequestToXML(request);
        final StringBuilder expectedPayload = new StringBuilder();
        expectedPayload.append("<SavedCards>");
        expectedPayload.append("<Card>");
        expectedPayload.append("<Token>abdc##789034</Token>");
        expectedPayload.append("<TokenExpiry>12/06/2016</TokenExpiry>");
        expectedPayload.append("<CardType>VISA</CardType>");
        expectedPayload.append("<MaskedCard>12345678998765</MaskedCard>");
        expectedPayload.append("<CardExpiry>12/06/2016</CardExpiry>");
        expectedPayload.append("<DefaultCard>1</DefaultCard>");
        expectedPayload.append("<BIN>1234</BIN>");
        expectedPayload.append("<IsCardOnFile>TRUE</IsCardOnFile>");
        expectedPayload.append("<IsFirstCredentialStorage>TRUE</IsFirstCredentialStorage>");
        expectedPayload.append("</Card>");
        expectedPayload.append("<Card>");
        expectedPayload.append("<Token>abdc##789034</Token>");
        expectedPayload.append("<TokenExpiry>12/06/2016</TokenExpiry>");
        expectedPayload.append("<CardType>Master</CardType>");
        expectedPayload.append("<MaskedCard>12345678998765</MaskedCard>");
        expectedPayload.append("<CardExpiry>12/06/2016</CardExpiry>");
        expectedPayload.append("<DefaultCard>0</DefaultCard>");
        expectedPayload.append("<BIN>1234</BIN>");
        expectedPayload.append("<IsCardOnFile>FALSE</IsCardOnFile>");
        expectedPayload.append("</Card>");
        expectedPayload.append("</SavedCards>");
        assertThat(xmlString).isEqualTo(expectedPayload.toString());
    }


    @Test
    public void testEncodeBase64WithEmptyString() {
        final String input = "";
        final String encodedStr = ipgHelper.encodeBase64(input);
        assertThat(encodedStr).isNull();
    }

    @Test
    public void testEncodeBase64WithNull() {
        final String input = null;
        final String encodedStr = ipgHelper.encodeBase64(input);
        assertThat(encodedStr).isNull();
    }

    @Test
    public void testEncodeBase64ValidStr() {
        final String input = "abcdefgh";
        final String encodedStr = ipgHelper.encodeBase64(input);
        assertThat(encodedStr).isNotNull();
    }

    @Test
    public void testUnmarshalCardResultsWithNullRequest() throws JAXBException {
        final String xmlRequest = null;
        final CardResultsData data = ipgHelper.unmarshalCardResults(xmlRequest);
        assertThat(data).isNull();
    }

    @Test
    public void testUnmarshalCardResultsWithEmptyRequest() throws JAXBException {
        final String xmlRequest = "";
        final CardResultsData data = ipgHelper.unmarshalCardResults(xmlRequest);
        assertThat(data).isNull();
    }

    @Test
    public void testUnmarshalCardResultsWithValidRequest() throws JAXBException {
        final String xmlRequest = "<CardResults><Result><Details><Amount>1000</Amount><TransType>1</TransType><Receipt>82626354</Receipt><RespCode>0</RespCode><RespText>Approved</RespText><TrnDateTime>2014-11-11 15:30:00</TrnDateTime><SettlementDate>2014-11-12</SettlementDate>"
                + "</Details><Card><CardType>VISA</CardType><MaskedCard>424242******4242</MaskedCard><CardExpiry>12/15</CardExpiry><BIN>42424</BIN> <PromoID>2000</PromoID><Token>36639373737373737726</Token>"
                + "<TokenExpiry>01/12/14</TokenExpiry><IsCardOnFile>TRUE</IsCardOnFile><IsFirstCredentialStorage>TRUE</IsFirstCredentialStorage></Card><Fraud><ThreeDS>"
                + "<ThreeDSSessToken></ThreeDSSessToken><ECI></ECI><VER></VER><PAR></PAR><SigVerif></SigVerif><CAVV></CAVV><XID></XID><CAR>98</CAR></ThreeDS><ReD><ReDOrderNumber></ReDOrderNumber>"
                + "<FRAUD_STAT_CD>aaaa</FRAUD_STAT_CD><REQ_ID></REQ_ID><ORD_ID></ORD_ID><STAT_CD>aaaa</STAT_CD><FRAUD_RSP_CD></FRAUD_RSP_CD></ReD></Fraud></Result></CardResults>";
        final CardResultsData data = ipgHelper.unmarshalCardResults(xmlRequest);
        assertThat(data).isNotNull();
        assertThat(data.getCardResults()).isNotNull();
        assertThat(data.getCardResults()).hasSize(1);
        assertThat(data.getCardResults().get(0).getFraudDetails()).isNotNull();
        assertThat(data.getCardResults().get(0).getTransactionDetails()).isNotNull();
    }

    @Test
    public void tesDeccodeBase64WithEmptyString() {
        final String input = "";
        final String decodedStr = ipgHelper.decode(input);
        assertThat(decodedStr).isNull();
    }

    @Test
    public void tesDeccodeBase64WithNullString() {
        final String input = null;
        final String decodedStr = ipgHelper.decode(input);
        assertThat(decodedStr).isNull();
    }

    @Test
    public void tesDeccodeBase64WithValidString() {
        final String input = "ENhcmRSZXN1bHRzPjxSZXN1bHQ%2bPERldGFpbHM";
        final String decodedStr = ipgHelper.decode(input);
        assertThat(decodedStr).isNotNull();
    }

    @Test
    public void testSubmitSinglePaymentRequestToXml() throws JAXBException {
        final SubmitSinglePaymentRequest request = new SubmitSinglePaymentRequest();
        final Transaction transaction = new Transaction();
        final Security security = new Security();
        security.setPassword("123");
        security.setUserName("test");
        transaction.setSecurity(security);
        request.setTransaction(transaction);
        transaction.setAccountNumber("123");
        final String xml = ipgHelper.marshalTransaction(request.getTransaction());
        assertThat(xml).isEqualTo(
                "<Transaction><AccountNumber>123</AccountNumber>" +
                        "<Security><UserName>test</UserName><Password>123</Password></Security></Transaction>");
    }

    @Test
    public void testSubmitSingleRefundRequestToXml() throws JAXBException {
        final SubmitSingleRefundRequest request = new SubmitSingleRefundRequest();
        final RefundInfo refundInfo = new RefundInfo();
        final Security security = new Security();
        refundInfo.setAmount("1200");
        refundInfo.setReceiptNumber("123456");
        security.setPassword("123");
        security.setUserName("test");
        refundInfo.setSecurity(security);
        request.setRefundInfo(refundInfo);
        final String xml = ipgHelper.marshalRefundInfo(request.getRefundInfo());
        assertThat(xml).isEqualTo(
                "<Refund><Receipt>123456</Receipt><Amount>1200</Amount><Security><UserName>test</UserName><Password>123</Password></Security></Refund>");
    }

    @Test
    public void testUnmarshalResponse() throws JAXBException {
        final String responseXml = "<Response><ResponseCode>0</ResponseCode><Timestamp>02-Jul-2014 11:07:08"
            + "</Timestamp><Receipt>10001197</Receipt><SettlementDate>02-Jul-2014</SettlementDate><DeclinedCode>"
            + "123</DeclinedCode><DeclinedMessage>456</DeclinedMessage></Response>";

        final Response response = ipgHelper.unmarshalResponse(responseXml);
        assertThat(response.getResponseCode()).isEqualTo("0");
        assertThat(response.getTimestamp()).isEqualTo("02-Jul-2014 11:07:08");
        assertThat(response.getReceipt()).isEqualTo("10001197");
    }

    @Test
    public void testUnmarshalSavedCardsWithEmptyRequest() throws JAXBException {
        final String xmlRequest = "";
        final SavedCardsData data = ipgHelper.unmarshalSavedCards(xmlRequest);
        assertThat(data).isNull();
    }

    @Test
    public void testUnmarshalSavedCardsWithNull() throws JAXBException {
        final String xmlRequest = null;
        final SavedCardsData data = ipgHelper.unmarshalSavedCards(xmlRequest);
        assertThat(data).isNull();
    }

    @Test
    public void testUnmarshalSavedCards() throws JAXBException {
        final String xmlRequest = "<SavedCards><Card>"
                + "<Token>99442123456704242</Token><TokenExpiry>08/12/20</TokenExpiry><CardExpiry>04/20</CardExpiry><CardType>VISA</CardType><MaskedCard>424242******4242</MaskedCard>"
                + "<DefaultCard>1</DefaultCard><IsCardOnFile>TRUE</IsCardOnFile><IsFirstCredentialStorage>TRUE</IsFirstCredentialStorage><BIN>42</BIN></Card></SavedCards>";
        final SavedCardsData data = ipgHelper.unmarshalSavedCards(xmlRequest);
        assertThat(data).isNotNull();
        assertThat(data.getCardRequests()).isNotNull();
        assertThat(data.getCardRequests()).hasSize(1);
        assertThat(data.getCardRequests().get(0).getToken()).isEqualTo("99442123456704242");
        assertThat(data.getCardRequests().get(0).getTokenExpiry()).isEqualTo("08/12/20");
        assertThat(data.getCardRequests().get(0).getCardExpiry()).isEqualTo("04/20");
        assertThat(data.getCardRequests().get(0).getCardType()).isEqualTo("VISA");
        assertThat(data.getCardRequests().get(0).getCardNumber()).isEqualTo("424242******4242");
        assertThat(data.getCardRequests().get(0).getCardOnFile()).isEqualTo("TRUE");
        assertThat(data.getCardRequests().get(0).getFirstCredentialStorage()).isEqualTo("TRUE");


    }

    @Test
    public void testGetAmountInCents() {
        assertThat(IPGHelper.getAmountInCents(new BigDecimal(10.009d))).isEqualTo(1001);
        assertThat(IPGHelper.getAmountInCents(new BigDecimal(9.9808d))).isEqualTo(998);
        assertThat(IPGHelper.getAmountInCents(new BigDecimal(33.899f))).isEqualTo(3390);
        assertThat(IPGHelper.getAmountInCents(new BigDecimal(33.89f))).isEqualTo(3389);
    }

    @Test
    public void testMarshalVoidInfo() throws JAXBException {
        final VoidInfo voidInfo = new VoidInfo();
        voidInfo.setReceiptNumber("90384670");

        final Security security = new Security();
        security.setPassword("123");
        security.setUserName("test");

        voidInfo.setSecurity(security);
        final String xml = ipgHelper.marshalVoidInfo(voidInfo);
        assertThat(xml).isNotNull();
        assertThat(xml).isEqualTo(
                "<Void><Receipt>90384670</Receipt><Security><UserName>test</UserName><Password>123</Password></Security></Void>");
    }

    @Test
    public void testUnmarshalSingleVoidResponse() throws JAXBException {
        final String xmlResponse = "<Response><ResponseCode>0</ResponseCode>" +
                "<Timestamp>02-Jul-2014 11:07:08</Timestamp><Receipt>10001197</Receipt>" +
                "<DeclinedCode></DeclinedCode><DeclinedMessage></DeclinedMessage></Response>";

        final Response response = ipgHelper.unmarshalResponse(xmlResponse);

        assertThat(response).isNotNull();
        assertThat(response.getResponseCode()).isEqualTo("0");
        assertThat(response.getReceipt()).isEqualTo("10001197");
        assertThat(response.getDeclinedCode()).isEmpty();
        assertThat(response.getDeclinedMessage()).isEmpty();
    }
}
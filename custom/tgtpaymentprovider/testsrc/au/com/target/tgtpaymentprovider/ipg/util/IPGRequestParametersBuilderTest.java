/**
 * 
 */
package au.com.target.tgtpaymentprovider.ipg.util;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.apache.http.NameValuePair;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtpaymentprovider.constants.TgtpaymentproviderConstants;
import au.com.target.tgtpaymentprovider.ipg.data.CardDetails;
import au.com.target.tgtpaymentprovider.ipg.data.SavedCardsData;
import au.com.target.tgtpaymentprovider.ipg.data.request.TokenizeRequest;
import au.com.target.tgtpaymentprovider.ipg.data.request.TransactionQueryRequest;


/**
 * @author mjanarth
 *
 */
@UnitTest
public class IPGRequestParametersBuilderTest {

    private final IPGRequestParametersBuilder builder = new IPGRequestParametersBuilder();
    private final IPGHelper ipgHelper = new IPGHelper();
    private final int sizeofRequestWithoutSavedCard = 10;
    private final int sizeofRequestWithSavedCard = 11;

    @Before
    public void setUp() {
        builder.setIpgHelper(ipgHelper);
    }

    @Test
    public void testBuildRequestParametersForSessionTokenWithNullReq() throws JAXBException {
        final TokenizeRequest request = null;
        final List<NameValuePair> params = builder.buildRequestParametersForSessionToken(request);
        Assert.assertNull(params);
    }

    @Test
    public void testBuildRequestParametersForSessionTokenwithValidRequest() throws JAXBException {
        final TokenizeRequest request = populateRequest();
        final List<NameValuePair> params = builder.buildRequestParametersForSessionToken(request);
        Assert.assertNotNull(params);
        Assert.assertEquals(params.size(), sizeofRequestWithoutSavedCard);
        Assert.assertEquals(params.get(0).getName(), TgtpaymentproviderConstants.IPGRequestAttributes.SESSIONID);
        Assert.assertEquals(params.get(0).getValue(), "CE537711586707F4CAEE54CB777CE050");
        Assert.assertEquals(params.get(1).getName(), TgtpaymentproviderConstants.IPGRequestAttributes.SESSIONKEY);
        Assert.assertEquals(params.get(1).getValue(), "12345688");
        Assert.assertEquals(params.get(2).getName(), TgtpaymentproviderConstants.IPGRequestAttributes.DIRECTLINK);
        Assert.assertEquals(params.get(2).getValue(), "MobileConfig");
        Assert.assertEquals(params.get(3).getName(), TgtpaymentproviderConstants.IPGRequestAttributes.MERCHANT_NUMBER);
        Assert.assertEquals(params.get(3).getValue(), "5500");
        Assert.assertEquals(params.get(4).getName(), TgtpaymentproviderConstants.IPGRequestAttributes.URL);
        Assert.assertEquals(params.get(4).getValue(), ipgHelper.encodeBase64("/userurl"));
        Assert.assertEquals(params.get(5).getName(), TgtpaymentproviderConstants.IPGRequestAttributes.CUSTREF);
        Assert.assertEquals(params.get(5).getValue(), "100190");
        Assert.assertEquals(params.get(6).getName(), TgtpaymentproviderConstants.IPGRequestAttributes.CUSTOMER_NUMBER);
        Assert.assertEquals(params.get(6).getValue(), "cust001");
        Assert.assertEquals(params.get(7).getName(), TgtpaymentproviderConstants.IPGRequestAttributes.POS_CONDITION);
        Assert.assertEquals(params.get(7).getValue(), "48");
        Assert.assertEquals(params.get(8).getName(), TgtpaymentproviderConstants.IPGRequestAttributes.STORE_ID);
        Assert.assertEquals(params.get(8).getValue(), "5500");
    }

    @Test
    public void testBuildRequestParametersForSessionTokenwithSavedCards() throws JAXBException {
        final TokenizeRequest request = populateRequest();
        final List<CardDetails> cards = new ArrayList<>();
        final SavedCardsData savedCardsRequest = new SavedCardsData();
        final CardDetails card = new CardDetails();
        card.setBin("1234");
        card.setTokenExpiry("12/06/2016");
        card.setCardType("VISA");
        card.setCardNumber("12345678998765");
        card.setIsDefaultCard("1");
        card.setCardExpiry("12/06/2016");
        card.setToken("abdc##789034");
        cards.add(card);
        savedCardsRequest.setCardRequests(cards);
        request.setSavedCards(savedCardsRequest);
        final List<NameValuePair> params = builder.buildRequestParametersForSessionToken(request);
        Assert.assertEquals(params.size(), sizeofRequestWithSavedCard);
        Assert.assertEquals(params.get(9).getName(), TgtpaymentproviderConstants.IPGRequestAttributes.SAVED_CARDS);
        Assert.assertNotNull(params.get(9).getValue());
    }


    @Test
    public void testBuildRequestParamForTransactionResultsWithNull() throws JAXBException {
        final TransactionQueryRequest request = null;
        final List<NameValuePair> params = builder.buildRequestParamatersForGetTransactionResult(request);
        Assert.assertNull(params);
    }

    @Test
    public void testBuildRequestParamForTransactionResults() throws JAXBException {
        final TransactionQueryRequest request = new TransactionQueryRequest();
        request.setSessionId("abcdefg-12-re45");
        request.setSessionToken("@@@@67890");
        final List<NameValuePair> params = builder.buildRequestParamatersForGetTransactionResult(request);
        Assert.assertNotNull(params);
        Assert.assertEquals(params.size(), 3);
        Assert.assertEquals(params.get(0).getName(), TgtpaymentproviderConstants.IPGRequestAttributes.SESSIONID);
        Assert.assertEquals(params.get(1).getName(), TgtpaymentproviderConstants.IPGRequestAttributes.SESSIONTOKEN);
        Assert.assertEquals(params.get(2).getName(), TgtpaymentproviderConstants.IPGRequestAttributes.QUERY);
        Assert.assertEquals(params.get(0).getValue(), request.getSessionId());
        Assert.assertEquals(params.get(1).getValue(), request.getSessionToken());
        Assert.assertEquals(params.get(2).getValue(), TgtpaymentproviderConstants.IPGRequestAttributes.STRING_TRUE);

    }

    private TokenizeRequest populateRequest() {
        final TokenizeRequest request = new TokenizeRequest();
        request.setCartId("100190");
        request.setCustomerNumber("cust001");
        request.setiFrameConfig("MobileConfig");
        request.setJsessionID("CE537711586707F4CAEE54CB777CE050");
        request.setMerchantNumber("5500");
        request.setPosCond("48");
        request.setSessionKey("12345688");
        request.setStoreNumber("5500");
        request.setTenderAmount("50.00");
        request.setUserUrl("/userurl");
        return request;

    }

}

/**
 *
 */
package au.com.target.tgtpaymentprovider.ipg.util;


import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpaymentprovider.ipg.data.soap.CreditCard;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Security;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Transaction;


/**
 * @author htan3
 *
 */
@UnitTest
public class IpgTransactionBuilderTest {

    private IpgTransactionBuilder builder = null;

    @Before
    public void setUp() {
        builder = IpgTransactionBuilder.create();
    }

    @Test
    public void testApplySecurity() {
        final Security security = mock(Security.class);
        builder.applySecurity(security);
        assertThat(builder.build().getSecurity()).isEqualTo(security);
    }

    @Test
    public void testAddAccountNumber() {
        builder.addAccountNumber("123", "456");
        final Transaction transaction = builder.build();
        assertThat(transaction.getAccountNumber()).isEqualTo("123");
        assertThat(transaction.getMerchantNumber()).isEqualTo("456");
    }

    @Test
    public void testAddIpgCard() {
        final TargetCardResult ipgCard = new TargetCardResult();
        ipgCard.setBin("bin");
        ipgCard.setCardExpiry("12/2018");
        ipgCard.setCardNumber("434234");
        ipgCard.setCardType("visa");
        ipgCard.setToken("wewqew");
        ipgCard.setTokenExpiry("12/2016");
        ipgCard.setCardOnFile(true);
        ipgCard.setFirstCredentialStorage(true);
        builder.addIpgCardInfo(ipgCard,true);
        final Transaction transaction = builder.build();
        final CreditCard creditCard = transaction.getCreditCard();
        assertThat(creditCard.getCardNumber()).isEqualTo("wewqew");
        assertThat(transaction.getUserDefined().getBin()).isEqualTo("bin");
        assertThat(transaction.getUserDefined().getCcExpiry()).isEqualTo("12/2018");
        assertThat(transaction.getUserDefined().getTokenExpiry()).isEqualTo("12/2016");
        assertThat(creditCard.getMaskedCardNumber()).isEqualTo("434234");
        assertThat(creditCard.getCardType()).isEqualTo("visa");
        assertThat(creditCard.getCardOnFile()).isEqualTo("TRUE");
        assertThat(creditCard.getFirstCredentialStorage()).isEqualTo("TRUE");

    }


    @Test
    public void testAddIpgCardWhenCredentialOnFileFeatureDisabled() {
        final TargetCardResult ipgCard = new TargetCardResult();
        ipgCard.setCardOnFile(true);
        ipgCard.setFirstCredentialStorage(true);
        builder.addIpgCardInfo(ipgCard,false);
        final Transaction transaction = builder.build();
        final CreditCard creditCard = transaction.getCreditCard();
        assertThat(creditCard.getCardOnFile()).isNull();
        assertThat(creditCard.getFirstCredentialStorage()).isNull();

    }

    @Test
    public void testAddIpgCardWhenCredentialOnFileValuesAreFalse() {
        final TargetCardResult ipgCard = new TargetCardResult();
        ipgCard.setCardOnFile(false);
        ipgCard.setFirstCredentialStorage(false);
        builder.addIpgCardInfo(ipgCard,true);
        final Transaction transaction = builder.build();
        final CreditCard creditCard = transaction.getCreditCard();
        assertThat(creditCard.getCardOnFile()).isNull();
        assertThat(creditCard.getFirstCredentialStorage()).isNull();

    }

    @Test
    public void test() {
        builder.addClientIP("1.2.3.4");
        final Transaction transaction = builder.build();
        assertThat(transaction.getTrnSource()).isEqualTo("1.2.3.4");
    }

    @Test
    public void testStoreId() {
        final String storeId = "5599";
        builder.setStoreId(storeId);
        final Transaction transaction = builder.build();
        assertThat(transaction.getUserDefined().getStoreId()).isEqualTo(storeId);
    }
}
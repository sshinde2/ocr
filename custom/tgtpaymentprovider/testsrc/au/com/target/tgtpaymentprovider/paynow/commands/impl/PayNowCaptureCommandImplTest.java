package au.com.target.tgtpaymentprovider.paynow.commands.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.apache.commons.lang.NotImplementedException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PayNowCaptureCommandImplTest {

    @InjectMocks
    private final PayNowCaptureCommandImpl command = new PayNowCaptureCommandImpl();

    @Mock
    private TargetCaptureRequest request;

    @Test
    public void testPerformNotImplemented() {
        try {
            command.perform(request);
            Assert.fail();
        }
        catch (final NotImplementedException e) {
            Assert.assertEquals(command.getCommandNotImplementedMessage(PayNowCaptureCommandImpl.COMMAND_CAPTURE),
                    e.getMessage());
        }
    }

}

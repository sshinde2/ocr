package au.com.target.tgtpaymentprovider.paynow.commands.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.apache.commons.lang.NotImplementedException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PayNowCreateSubscriptionCommandImplTest {

    @InjectMocks
    private final PayNowCreateSubscriptionCommandImpl command = new PayNowCreateSubscriptionCommandImpl();

    @Mock
    private TargetCreateSubscriptionRequest request;

    @Test
    public void testPerformNotImplemented() {
        try {
            command.perform(request);
            Assert.fail();
        }
        catch (final NotImplementedException e) {
            Assert.assertEquals(command
                    .getCommandNotImplementedMessage(PayNowCreateSubscriptionCommandImpl.COMMAND_CREATE_SUBSCRIPTION),
                    e.getMessage());
        }
    }

}

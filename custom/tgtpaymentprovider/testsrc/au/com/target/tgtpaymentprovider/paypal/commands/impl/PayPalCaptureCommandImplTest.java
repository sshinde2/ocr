/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.commands.impl;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.dto.TransactionStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpayment.dto.Address;
import au.com.target.tgtpayment.dto.AmountType;
import au.com.target.tgtpayment.dto.LineItem;
import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpaymentprovider.paypal.PayPalService;
import au.com.target.tgtpaymentprovider.paypal.exception.PayPalServiceException;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentReq;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentResponseType;
import urn.ebay.apis.CoreComponentTypes.BasicAmountType;
import urn.ebay.apis.eBLBaseComponents.AckCodeType;
import urn.ebay.apis.eBLBaseComponents.CurrencyCodeType;
import urn.ebay.apis.eBLBaseComponents.DoExpressCheckoutPaymentResponseDetailsType;
import urn.ebay.apis.eBLBaseComponents.PaymentInfoType;
import urn.ebay.apis.eBLBaseComponents.PaymentStatusCodeType;


/**
 * @author bjames4
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PayPalCaptureCommandImplTest {

    private String transactionId = "123456789";
    private String correlationId = "123";

    @Mock
    private PayPalService payPalService;

    @InjectMocks
    private final PayPalCaptureCommandImpl payPalCaptureCommandImpl = new PayPalCaptureCommandImpl();

    private final TargetCaptureRequest captureRequest = new TargetCaptureRequest();

    private Currency currency;

    private DoExpressCheckoutPaymentReq expressCheckoutPaymentReq;

    private Order order;

    /**
     * @throws PayPalServiceException
     *             -
     */
    @Before
    public void init() throws PayPalServiceException {
        transactionId = "123456789";
        correlationId = "123";
        order = new Order();
        final DoExpressCheckoutPaymentResponseType doExpressCheckoutPaymentResponseType = new DoExpressCheckoutPaymentResponseType();
        doExpressCheckoutPaymentResponseType.setAck(AckCodeType.SUCCESS);

        currency = Currency.getInstance("AUD");
        final DoExpressCheckoutPaymentResponseDetailsType paymentResponseDetails = new DoExpressCheckoutPaymentResponseDetailsType();

        final List<PaymentInfoType> paymentInfos = new ArrayList<>();
        final PaymentInfoType paymentInfo = new PaymentInfoType();
        paymentInfo.setTransactionID(transactionId);
        paymentInfo.setPaymentStatus(PaymentStatusCodeType.COMPLETED);

        paymentInfo.setGrossAmount(new BasicAmountType(CurrencyCodeType.AUD, "10.0"));
        paymentInfos.add(paymentInfo);

        paymentResponseDetails.setPaymentInfo(paymentInfos);

        doExpressCheckoutPaymentResponseType.setDoExpressCheckoutPaymentResponseDetails(paymentResponseDetails);
        doExpressCheckoutPaymentResponseType.setCorrelationID(correlationId);

        when(payPalService.doExpressCheckout((DoExpressCheckoutPaymentReq)Mockito.anyObject())).thenReturn(
                doExpressCheckoutPaymentResponseType);


        //Test the format change for $ in the order amounts

        final List<LineItem> lineItems = new ArrayList<>();
        final LineItem item = new LineItem();
        item.setName("Bugs bunny");
        item.setDescription("Test Item bugs bunny");
        item.setNumber(93698382);
        final AmountType amount = new AmountType();
        amount.setAmount(new BigDecimal(12.00d));
        amount.setCurrency(Currency.getInstance("AUD"));
        item.setPrice(amount);
        item.setQuantity(1);

        lineItems.add(item);
        order.setLineItems(lineItems);
        order.setOrderDescription("Easter bunny order");

        final Address shipAddress = new Address();
        shipAddress.setStreet1("12 Thompson Rd");
        shipAddress.setCity("North Geelong");
        shipAddress.setCountryName("Australia");
        shipAddress.setCountryCode("AU");
        shipAddress.setPostalCode("3219");
        order.setShippingAddress(shipAddress);

        final AmountType subTotal = new AmountType();
        subTotal.setAmount(new BigDecimal(150.760));
        subTotal.setCurrency(Currency.getInstance("AUD"));
        order.setItemSubTotalAmount(subTotal);

        final AmountType shipping = new AmountType();
        shipping.setAmount(new BigDecimal(123456789.403));
        shipping.setCurrency(Currency.getInstance("AUD"));
        order.setShippingTotalAmount(shipping);

        final AmountType total = new AmountType();
        total.setAmount(new BigDecimal(50.00));
        total.setCurrency(Currency.getInstance("AUD"));
        order.setOrderTotalAmount(total);


    }


    @Test
    public void testPerform() {
        captureRequest.setTotalAmount(BigDecimal.valueOf(10.00));
        captureRequest.setCurrency(currency);
        captureRequest.setPayerId("12345");
        captureRequest.setToken("123");
        final TargetCaptureResult res = payPalCaptureCommandImpl.perform(captureRequest);
        assertEquals("Transaction response ", TransactionStatus.ACCEPTED, res.getTransactionStatus());
        assertEquals("Transaction id ", transactionId, res.getMerchantTransactionCode());
        assertEquals("Transaction id ", correlationId, res.getReconciliationId());


    }

    @Test
    public void testTranslateRequest() {


        //Test the format change for $ in the order amounts

        captureRequest.setOrder(order);

        expressCheckoutPaymentReq = payPalCaptureCommandImpl.translateRequest(captureRequest);

        Assertions.assertThat(expressCheckoutPaymentReq.getDoExpressCheckoutPaymentRequest()
                .getDoExpressCheckoutPaymentRequestDetails().getPaymentDetails()
                .get(0).getItemTotal().getValue()).doesNotContain("$");
        Assertions.assertThat(expressCheckoutPaymentReq.getDoExpressCheckoutPaymentRequest()
                .getDoExpressCheckoutPaymentRequestDetails().getPaymentDetails()
                .get(0).getShippingTotal().getValue()).doesNotContain("$");
        Assertions.assertThat(expressCheckoutPaymentReq.getDoExpressCheckoutPaymentRequest()
                .getDoExpressCheckoutPaymentRequestDetails().getPaymentDetails()
                .get(0).getOrderTotal().getValue()).doesNotContain("$");


        Assertions.assertThat(expressCheckoutPaymentReq.getDoExpressCheckoutPaymentRequest()
                .getDoExpressCheckoutPaymentRequestDetails().getPaymentDetails()
                .get(0).getItemTotal().getValue()).isEqualTo(("150.76"));
        Assertions.assertThat(expressCheckoutPaymentReq.getDoExpressCheckoutPaymentRequest()
                .getDoExpressCheckoutPaymentRequestDetails().getPaymentDetails()
                .get(0).getShippingTotal().getValue()).isEqualTo("123456789.4");
        Assertions.assertThat(expressCheckoutPaymentReq.getDoExpressCheckoutPaymentRequest()
                .getDoExpressCheckoutPaymentRequestDetails().getPaymentDetails()
                .get(0).getOrderTotal().getValue()).isEqualTo(("50"));

    }
}

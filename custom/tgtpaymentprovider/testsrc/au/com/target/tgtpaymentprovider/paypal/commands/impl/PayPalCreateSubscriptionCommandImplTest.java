/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.commands.impl;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.TransactionStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.TargetCreateSubscriptionCommand;
import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.dto.Address;
import au.com.target.tgtpayment.dto.AmountType;
import au.com.target.tgtpayment.dto.LineItem;
import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpaymentprovider.paypal.PayPalService;
import au.com.target.tgtpaymentprovider.paypal.exception.InvalidRequestException;
import au.com.target.tgtpaymentprovider.paypal.exception.PayPalServiceException;
import au.com.target.tgtpaymentprovider.paypal.exception.ValidationFailedException;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutReq;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutResponseType;
import urn.ebay.apis.eBLBaseComponents.AckCodeType;


/**
 * @author vmuthura
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PayPalCreateSubscriptionCommandImplTest {

    @Mock
    private PayPalService payPalService;

    @InjectMocks
    private final TargetCreateSubscriptionCommand createSubscriptionCommand = new PayPalCreateSubscriptionCommandImpl();

    private TargetCreateSubscriptionRequest request;

    /**
     * 
     */
    @Before
    public void init() {
        request = new TargetCreateSubscriptionRequest();
        final Order order = new Order();
        final List<LineItem> lineItems = new ArrayList<>();
        final LineItem item = new LineItem();
        item.setName("Easter bunny");
        item.setDescription("Easter bunny by Ferrero Rocher");
        item.setNumber(93698382);
        final AmountType amount = new AmountType();
        amount.setAmount(new BigDecimal(12.00d));
        amount.setCurrency(Currency.getInstance("AUD"));
        item.setPrice(amount);
        item.setQuantity(1);

        lineItems.add(item);
        order.setLineItems(lineItems);
        order.setOrderDescription("Easter bunny order");

        final Address shipAddress = new Address();
        shipAddress.setStreet1("12 Thompson Rd");
        shipAddress.setCity("North Geelong");
        shipAddress.setCountryName("Australia");
        shipAddress.setCountryCode("AU");
        shipAddress.setPostalCode("3219");
        order.setShippingAddress(shipAddress);

        final AmountType subTotal = new AmountType();
        subTotal.setAmount(new BigDecimal(12.00d));
        subTotal.setCurrency(Currency.getInstance("AUD"));
        order.setItemSubTotalAmount(subTotal);

        final AmountType shipping = new AmountType();
        shipping.setAmount(new BigDecimal(9.00d));
        shipping.setCurrency(Currency.getInstance("AUD"));
        order.setShippingTotalAmount(shipping);

        final AmountType total = new AmountType();
        total.setAmount(new BigDecimal(21.00d));
        total.setCurrency(Currency.getInstance("AUD"));
        order.setOrderTotalAmount(total);

        request.setOrder(order);
        request.setBillingAgreementRequired(false);

    }

    /**
     * 
     */
    @Test(expected = IllegalArgumentException.class)
    public void testPerformWithNullRequest() {
        createSubscriptionCommand.perform(null);
    }

    /**
     * @throws InvalidRequestException
     *             -
     * @throws ValidationFailedException
     *             -
     * @throws PayPalServiceException
     *             -
     */
    @Test(expected = AdapterException.class)
    public void testForValidationFailed() throws InvalidRequestException, ValidationFailedException,
            PayPalServiceException {

        Mockito.when(payPalService.setExpressCheckout((SetExpressCheckoutReq)Mockito.anyObject())).thenThrow(
                new ValidationFailedException());
        createSubscriptionCommand.perform(new TargetCreateSubscriptionRequest());
    }

    /**
     * @throws InvalidRequestException
     *             -
     * @throws ValidationFailedException
     *             -
     * @throws PayPalServiceException
     *             -
     */
    @Test(expected = AdapterException.class)
    public void testForInvalidRequest() throws InvalidRequestException, ValidationFailedException,
            PayPalServiceException {

        Mockito.when(payPalService.setExpressCheckout((SetExpressCheckoutReq)Mockito.anyObject())).thenThrow(
                new InvalidRequestException());
        createSubscriptionCommand.perform(new TargetCreateSubscriptionRequest());
    }

    /**
     * @throws InvalidRequestException
     *             -
     * @throws ValidationFailedException
     *             -
     * @throws PayPalServiceException
     *             -
     */
    @Test(expected = AdapterException.class)
    public void testForPayPalServiceException() throws InvalidRequestException, ValidationFailedException,
            PayPalServiceException {

        Mockito.when(payPalService.setExpressCheckout((SetExpressCheckoutReq)Mockito.anyObject())).thenThrow(
                new PayPalServiceException());
        createSubscriptionCommand.perform(new TargetCreateSubscriptionRequest());
    }

    /**
     * @throws InvalidRequestException
     *             -
     * @throws ValidationFailedException
     *             -
     * @throws PayPalServiceException
     *             -
     */
    @Test
    public void testPerform() throws InvalidRequestException, ValidationFailedException, PayPalServiceException {
        final SetExpressCheckoutResponseType response = new SetExpressCheckoutResponseType();
        response.setAck(AckCodeType.SUCCESS);

        Mockito.when(payPalService.setExpressCheckout((SetExpressCheckoutReq)Mockito.anyObject())).thenReturn(
                response);
        final TargetCreateSubscriptionResult result = createSubscriptionCommand.perform(request);

        assertEquals("Expecting token", response.getToken(), result.getRequestToken());
        assertEquals("Expecting reconcilliationId", response.getCorrelationID(), result.getReconciliationId());
        assertEquals("Expecting transaction status detail", TransactionStatus.ACCEPTED,
                result.getTransactionStatus());

    }
}

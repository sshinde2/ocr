/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.commands.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.dto.TransactionStatus;

import java.math.BigDecimal;
import java.util.Currency;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpaymentprovider.paypal.PayPalService;
import au.com.target.tgtpaymentprovider.paypal.exception.PayPalServiceException;
import urn.ebay.api.PayPalAPI.RefundTransactionReq;
import urn.ebay.api.PayPalAPI.RefundTransactionResponseType;
import urn.ebay.apis.eBLBaseComponents.AckCodeType;
import urn.ebay.apis.eBLBaseComponents.PaymentStatusCodeType;
import urn.ebay.apis.eBLBaseComponents.RefundInfoType;


/**
 * @author bjames4
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PayPalRefundFollowOnCommandImplTest {

    private static final String TRANSACTION_ID = "123456789";
    private static final String CORRELATION_ID = "123";
    private static final String TOKEN = "token";
    private static final String REFUND_TRANSACTION_ID = "refundTransactionId";


    @Mock
    private PayPalService payPalService;

    @InjectMocks
    //CHECKSTYLE:OFF
    private final PayPalRefundFollowOnCommandImpl payPalRefundFollowOnCommandImpl = new PayPalRefundFollowOnCommandImpl();
    //CHECKSTYLE:ON
    private final TargetFollowOnRefundRequest refundRequest = new TargetFollowOnRefundRequest();

    /**
     * @throws PayPalServiceException
     *             -
     */
    @Before
    public void init() throws PayPalServiceException {
        final RefundTransactionResponseType response = new RefundTransactionResponseType();
        response.setAck(AckCodeType.SUCCESS);
        response.setCorrelationID(CORRELATION_ID);
        response.setMsgSubID(TOKEN);
        response.setRefundTransactionID(REFUND_TRANSACTION_ID);

        final RefundInfoType refundInfo = new RefundInfoType();
        refundInfo.setRefundStatus(PaymentStatusCodeType.REFUNDED);
        response.setRefundInfo(refundInfo);

        when(payPalService.refund(Mockito.any(RefundTransactionReq.class))).thenReturn(response);
    }

    @Test
    public void testPerform() {
        refundRequest.setTotalAmount(BigDecimal.valueOf(10.00));
        refundRequest.setCurrency(Currency.getInstance("AUD"));
        refundRequest.setTransactionId(TRANSACTION_ID);

        final TargetFollowOnRefundResult res = payPalRefundFollowOnCommandImpl.perform(refundRequest);
        assertEquals("TransactionStatus", TransactionStatus.ACCEPTED, res.getTransactionStatus());
        assertEquals("Reconciliation id", CORRELATION_ID, res.getReconciliationId());
        assertEquals("Token", TOKEN, res.getRequestToken());
        assertEquals("refundTransactionId", REFUND_TRANSACTION_ID, res.getRequestId());
    }

}

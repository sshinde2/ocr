/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.commands.impl;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.TargetRetrieveTransactionCommand;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;
import au.com.target.tgtpaymentprovider.paypal.PayPalService;
import au.com.target.tgtpaymentprovider.paypal.exception.InvalidRequestException;
import au.com.target.tgtpaymentprovider.paypal.exception.PayPalServiceException;
import au.com.target.tgtpaymentprovider.paypal.exception.ValidationFailedException;
import urn.ebay.api.PayPalAPI.GetTransactionDetailsReq;
import urn.ebay.api.PayPalAPI.GetTransactionDetailsResponseType;
import urn.ebay.apis.CoreComponentTypes.BasicAmountType;
import urn.ebay.apis.eBLBaseComponents.AckCodeType;
import urn.ebay.apis.eBLBaseComponents.PaymentInfoType;
import urn.ebay.apis.eBLBaseComponents.PaymentStatusCodeType;
import urn.ebay.apis.eBLBaseComponents.PaymentTransactionType;


/**
 * @author vmuthura
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PayPalRetrieveTransactionCommandImplTest {

    @Mock
    private PayPalService payPalService;

    @InjectMocks
    //CHECKSTYLE:OFF
    private final TargetRetrieveTransactionCommand retrieveTransactionCommand = new PayPalRetrieveTransactionCommandImpl();
    //CHECKSTYLE:ON

    private TargetRetrieveTransactionRequest request;

    /**
     * 
     */
    @Before
    public void init() {
        request = new TargetRetrieveTransactionRequest();
        request.setOrderId("123456");
        request.setTransactionId("m2n3b4v5c6x7");
    }

    /**
     * 
     */
    @Test(expected = IllegalArgumentException.class)
    public void testPerformWithNullRequest() {
        retrieveTransactionCommand.perform(null);
    }

    /**
     * @throws InvalidRequestException
     *             -
     * @throws ValidationFailedException
     *             -
     * @throws PayPalServiceException
     *             -
     */
    @Test(expected = AdapterException.class)
    public void testForValidationFailed() throws InvalidRequestException, ValidationFailedException,
            PayPalServiceException {

        Mockito.when(payPalService.retrieve((GetTransactionDetailsReq)Mockito.anyObject())).thenThrow(
                new ValidationFailedException());
        retrieveTransactionCommand.perform(new TargetRetrieveTransactionRequest());
    }

    /**
     * @throws InvalidRequestException
     *             -
     * @throws ValidationFailedException
     *             -
     * @throws PayPalServiceException
     *             -
     */
    @Test(expected = AdapterException.class)
    public void testForInvalidRequest() throws InvalidRequestException, ValidationFailedException,
            PayPalServiceException {

        Mockito.when(payPalService.retrieve((GetTransactionDetailsReq)Mockito.anyObject())).thenThrow(
                new InvalidRequestException());
        retrieveTransactionCommand.perform(new TargetRetrieveTransactionRequest());
    }

    /**
     * @throws InvalidRequestException
     *             -
     * @throws ValidationFailedException
     *             -
     * @throws PayPalServiceException
     *             -
     */
    @Test(expected = AdapterException.class)
    public void testForPayPalServiceException() throws InvalidRequestException, ValidationFailedException,
            PayPalServiceException {

        Mockito.when(payPalService.retrieve((GetTransactionDetailsReq)Mockito.anyObject())).thenThrow(
                new PayPalServiceException());
        retrieveTransactionCommand.perform(new TargetRetrieveTransactionRequest());
    }

    /**
     * @throws InvalidRequestException
     *             -
     * @throws ValidationFailedException
     *             -
     * @throws PayPalServiceException
     *             -
     */
    @Test
    public void testPerform() throws InvalidRequestException, ValidationFailedException, PayPalServiceException {
        final PaymentTransactionType payment = new PaymentTransactionType();
        final PaymentInfoType paymentInfo = new PaymentInfoType();
        final BasicAmountType basicAmount = new BasicAmountType();
        paymentInfo.setPaymentStatus(PaymentStatusCodeType.COMPLETED);
        basicAmount.setValue("1.00");
        paymentInfo.setGrossAmount(basicAmount);
        payment.setPaymentInfo(paymentInfo);
        final GetTransactionDetailsResponseType response = new GetTransactionDetailsResponseType();

        response.setAck(AckCodeType.SUCCESS);
        response.setPaymentTransactionDetails(payment);
        Mockito.when(payPalService.retrieve((GetTransactionDetailsReq)Mockito.anyObject())).thenReturn(
                response);
        final TargetRetrieveTransactionResult result = retrieveTransactionCommand.perform(request);

        assertEquals("Expecting reconcilliationId", response.getCorrelationID(), result.getReconciliationId());
        assertEquals("Expecting transaction status detail", TransactionStatus.ACCEPTED, result.getTransactionStatus());
        assertEquals("Expecting retrieved transaction status", TransactionStatusDetails.SUCCESFULL,
                result.getTransactionStatusDetails());

    }
}

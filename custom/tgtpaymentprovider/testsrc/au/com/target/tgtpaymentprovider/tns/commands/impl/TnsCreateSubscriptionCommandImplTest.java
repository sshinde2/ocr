/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.commands.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.dto.TransactionStatus;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpaymentprovider.tns.TnsService;
import au.com.target.tgtpaymentprovider.tns.TnsWebOperations;
import au.com.target.tgtpaymentprovider.tns.data.ResultEnum;
import au.com.target.tgtpaymentprovider.tns.data.response.CreateSessionResponse;
import au.com.target.tgtpaymentprovider.tns.exception.TnsServiceException;


/**
 * @author bjames4
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TnsCreateSubscriptionCommandImplTest {

    @Mock
    private TnsService tnsService;

    @Mock
    private TnsWebOperations tnsClient;

    @InjectMocks
    private final TnsCreateSubcriptionCommandImpl subscriptionCommandImpl = new TnsCreateSubcriptionCommandImpl();

    private final TargetCreateSubscriptionRequest subscriptionRequest = new TargetCreateSubscriptionRequest();

    /**
     * @throws TnsServiceException
     *             -
     */
    @Before
    public void init() throws TnsServiceException {
        final CreateSessionResponse response = new CreateSessionResponse();
        response.setResult(ResultEnum.SUCCESS);
        response.setSession("abc123");
        when(tnsService.createSession()).thenReturn(response);
    }

    /**
     * 
     */
    @Test
    public void testPerform() {

        final TargetCreateSubscriptionResult res = subscriptionCommandImpl.perform(subscriptionRequest);
        assertEquals("Expecting response", TransactionStatus.ACCEPTED, res.getTransactionStatus());
    }
}

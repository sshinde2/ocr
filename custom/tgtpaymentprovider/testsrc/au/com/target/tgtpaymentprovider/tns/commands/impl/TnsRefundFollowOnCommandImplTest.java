/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.commands.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.math.BigDecimal;
import java.util.Currency;

import javax.xml.bind.DatatypeConverter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpaymentprovider.paypal.exception.PayPalServiceException;
import au.com.target.tgtpaymentprovider.tns.TnsService;
import au.com.target.tgtpaymentprovider.tns.data.GatewayResponseEnum;
import au.com.target.tgtpaymentprovider.tns.data.ResponseDetails;
import au.com.target.tgtpaymentprovider.tns.data.ResultEnum;
import au.com.target.tgtpaymentprovider.tns.data.Transaction;
import au.com.target.tgtpaymentprovider.tns.data.response.RefundResponse;
import au.com.target.tgtpaymentprovider.tns.exception.TnsServiceException;


/**
 * @author bjames4
 * 
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(value = "classpath:tgtpaymentprovider-spring-test.xml")
public class TnsRefundFollowOnCommandImplTest {

    private String transactionId = "123456789";
    private String correlationId = "123";

    private final TargetFollowOnRefundRequest refundRequest = new TargetFollowOnRefundRequest();

    @Mock
    private TnsService tnsService;

    @InjectMocks
    private final TnsRefundFollowOnCommandImpl refundImpl = new TnsRefundFollowOnCommandImpl();

    /**
     * @throws PayPalServiceException
     *             -
     * @throws TnsServiceException
     *             -
     */
    @Before
    public void init() throws PayPalServiceException, TnsServiceException
    {
        transactionId = "123456";
        correlationId = "123";

        final RefundResponse response = new RefundResponse();

        final Transaction transaction = new Transaction();
        transaction.setId(transactionId);
        transaction.setReceipt(correlationId);
        transaction.setAmount("10.00");

        response.setTransaction(transaction);
        response.setResult(ResultEnum.SUCCESS);
        response.setSession("abc123");
        response.setTimeOfRecord(DatatypeConverter.parseDate("2013-07-03T01:30:45.123Z").getTime());

        final ResponseDetails responseDetails = new ResponseDetails();
        responseDetails.setGatewayCode(GatewayResponseEnum.APPROVED);
        response.setResponse(responseDetails);

        when(
                tnsService.refund(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
                        (BigDecimal)Mockito.anyObject(), (Currency)Mockito.anyObject())).thenReturn(response);
    }

    /**
     * 
     */
    @Test
    public void testPerform() {

        refundRequest.setTransactionId(transactionId);
        refundRequest.setTotalAmount(BigDecimal.valueOf(10.00));
        refundRequest.setCurrency(Currency.getInstance("AUD"));

        final TargetFollowOnRefundResult response = refundImpl.perform(refundRequest);
        assertEquals("Expecting response", TransactionStatus.ACCEPTED, response.getTransactionStatus());
        assertEquals("Expecting response", TransactionStatusDetails.SUCCESFULL,
                response.getTransactionStatusDetails());
        assertEquals("Expecting MerchantTransactionCode", "123456", response.getMerchantTransactionCode());
        assertEquals("Expecting ReconciliationId", "123", response.getReconciliationId());
        assertEquals("Expecting RequestId", "123456", response.getRequestId());
        assertEquals("Expecting RequestToken", "abc123", response.getRequestToken());
        assertEquals("Expecting TotalAmount", "10.00", response.getTotalAmount().toString());
    }

}

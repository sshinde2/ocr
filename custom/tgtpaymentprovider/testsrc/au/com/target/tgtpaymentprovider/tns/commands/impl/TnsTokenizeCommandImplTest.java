/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.commands.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetTokenizeRequest;
import au.com.target.tgtpayment.commands.result.TargetTokenizeResult;
import au.com.target.tgtpaymentprovider.tns.TnsService;
import au.com.target.tgtpaymentprovider.tns.TnsWebOperations;
import au.com.target.tgtpaymentprovider.tns.data.GatewayResponseEnum;
import au.com.target.tgtpaymentprovider.tns.data.ResponseDetails;
import au.com.target.tgtpaymentprovider.tns.data.ResultEnum;
import au.com.target.tgtpaymentprovider.tns.data.response.TokenizeResponse;
import au.com.target.tgtpaymentprovider.tns.exception.TnsServiceException;


/**
 * @author vmuthura
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TnsTokenizeCommandImplTest {
    @Mock
    private TnsService tnsService;

    @Mock
    private TnsWebOperations tnsClient;

    @InjectMocks
    private final TnsTokenizeCommandImpl tokenizeCommand = new TnsTokenizeCommandImpl();
    private final TargetTokenizeRequest tokenizeRequest = new TargetTokenizeRequest();


    /**
     * @throws TnsServiceException
     *             -
     */
    @Before
    public void init() throws TnsServiceException {
        final TokenizeResponse response = new TokenizeResponse();
        response.setToken("FEA56FG4BN348DSFHDF8");
        response.setResult(ResultEnum.SUCCESS);
        final ResponseDetails responseDetails = new ResponseDetails();
        responseDetails.setGatewayCode(GatewayResponseEnum.APPROVED);
        response.setResponse(responseDetails);

        when(tnsService.tokenize(Mockito.anyString())).thenReturn(response);
    }

    /**
     * 
     */
    @Test
    public void testPerform() {
        tokenizeRequest.setSessionToken("123456789");
        final TargetTokenizeResult response = tokenizeCommand.perform(tokenizeRequest);
        assertEquals("Transaction status", TransactionStatus.ACCEPTED, response.getTransactionStatus());
        assertEquals("Transaction status detail", TransactionStatusDetails.SUCCESFULL,
                response.getTransactionStatusDetails());
        assertEquals("Token", "FEA56FG4BN348DSFHDF8", response.getSavedToken());
    }

}

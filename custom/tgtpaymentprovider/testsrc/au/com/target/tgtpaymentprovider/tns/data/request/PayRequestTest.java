/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data.request;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.IOException;
import java.util.Properties;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

import au.com.target.tgtpaymentprovider.tns.data.Card;
import au.com.target.tgtpaymentprovider.tns.data.CardExpiry;
import au.com.target.tgtpaymentprovider.tns.data.PaymentType;
import au.com.target.tgtpaymentprovider.tns.data.SourceOfFundDetails;
import au.com.target.tgtpaymentprovider.tns.data.SourceOfFunds;


/**
 * @author bjames4
 * 
 */
@UnitTest
public class PayRequestTest {

    /**
     * 
     */
    @Test
    public void toJsonTest() {
        final PayRequest payRequest = new PayRequest();
        payRequest.setApiOperation("Pay");
        payRequest.setSourceOfFunds(new SourceOfFunds());
        payRequest.getSourceOfFunds().setProvided(new SourceOfFundDetails());
        payRequest.getSourceOfFunds().getProvided().setCard(new Card());
        payRequest.getSourceOfFunds().getProvided().getCard().setExpiry(new CardExpiry());
        payRequest.getSourceOfFunds().getProvided().getCard().getExpiry().setMonth("08");
        payRequest.getSourceOfFunds().getProvided().getCard().getExpiry().setYear("13");
        payRequest.getSourceOfFunds().getProvided().getCard().setSecurityCode("111");
        payRequest.getSourceOfFunds().setSession("DefaultSESSION");
        payRequest.getSourceOfFunds().setToken("DefaultTOKEN");
        payRequest.getSourceOfFunds().setType(PaymentType.CARD);

        final ObjectMapper objectMapper = new ObjectMapper();
        final Properties jsonStringProps = new Properties();

        try {
            jsonStringProps.load(PayRequestTest.class.getResourceAsStream("/ExpectedJSON.properties"));
            final String jsonString = jsonStringProps.getProperty("payRequest");
            assertNotNull("Expecting non null JSON string", objectMapper.writeValueAsString(payRequest));
            assertEquals("Unexpected JSON string", jsonString.replaceAll("\\s*", ""),
                    objectMapper.writeValueAsString(payRequest));
        }
        catch (final JsonGenerationException e) {
            fail("Unexpected exception:" + e.getMessage());
        }
        catch (final JsonMappingException e) {
            fail("Unexpected exception:" + e.getMessage());
        }
        catch (final IOException e) {
            fail("Unexpected exception:" + e.getMessage());
        }
    }
}

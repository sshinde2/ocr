/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.data.response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.IOException;
import java.util.Properties;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

import au.com.target.tgtpaymentprovider.tns.data.ResultEnum;


/**
 * @author bjames4
 * 
 */
@UnitTest
public class SaveCardResponseTest {

    /**
     * 
     */
    @Test
    public void toJsonTest() {

        final SaveCardResponse response = new SaveCardResponse();
        response.setResult(ResultEnum.SUCCESS);
        response.setToken("DFGDFGDF345345345sdfgdfg");

        final ObjectMapper objectMapper = new ObjectMapper();
        final Properties jsonStringProps = new Properties();

        try {
            jsonStringProps.load(SaveCardResponseTest.class.getResourceAsStream("/ExpectedJSON.properties"));
            final String jsonString = jsonStringProps.getProperty("saveCardResponse");
            assertNotNull("Expecting non null JSON string", objectMapper.writeValueAsString(response));
            assertEquals("Unexpected JSON string", jsonString.replaceAll("\\s*", ""),
                    objectMapper.writeValueAsString(response));
        }
        catch (final JsonGenerationException e) {
            fail("Unexpected exception:" + e.getMessage());
        }
        catch (final JsonMappingException e) {
            fail("Unexpected exception:" + e.getMessage());
        }
        catch (final IOException e) {
            fail("Unexpected exception:" + e.getMessage());
        }
    }
}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.tns.util;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestClientException;


/**
 * @author vmuthura
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TnsResponseErrorHandlerTest {

    @Mock
    private ClientHttpResponse httpResponse;

    private final TnsResponseErrorHandler handler = new TnsResponseErrorHandler();

    /**
     * @throws IOException
     *             -
     */
    @Test
    public void testHandleErrorFor4xx() throws IOException {
        Mockito.when(httpResponse.getStatusCode()).thenReturn(HttpStatus.BAD_REQUEST);
        handler.handleError(httpResponse);
    }

    /**
     * @throws IOException
     *             -
     */
    @Test
    public void testHandleErrorFor5xx() throws IOException {
        Mockito.when(httpResponse.getStatusCode()).thenReturn(HttpStatus.INTERNAL_SERVER_ERROR);
        handler.handleError(httpResponse);
    }

    /**
     * @throws IOException
     *             -
     */
    @Test(expected = RestClientException.class)
    public void testHandleErrorForOtherError() throws IOException {
        Mockito.when(httpResponse.getStatusCode()).thenReturn(HttpStatus.MOVED_PERMANENTLY);
        handler.handleError(httpResponse);
    }

}

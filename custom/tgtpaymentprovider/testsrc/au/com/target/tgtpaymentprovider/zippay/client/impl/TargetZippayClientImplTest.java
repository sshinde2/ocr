/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.client.impl;


import static org.fest.assertions.Assertions.assertThat;
import static org.springframework.test.web.client.match.RequestMatchers.method;
import static org.springframework.test.web.client.match.RequestMatchers.requestTo;
import static org.springframework.test.web.client.response.ResponseCreators.withBadRequest;
import static org.springframework.test.web.client.response.ResponseCreators.withNoContent;
import static org.springframework.test.web.client.response.ResponseCreators.withServerError;
import static org.springframework.test.web.client.response.ResponseCreators.withStatus;
import static org.springframework.test.web.client.response.ResponseCreators.withSuccess;

import de.hybris.platform.servicelayer.user.UserService;

import java.util.Properties;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import au.com.target.tgtpaymentprovider.tns.data.request.PayRequestTest;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayClientException;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayTransactionError;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayTransactionRejectedException;
import au.com.target.tgtpaymentprovider.zippay.data.request.Authority;
import au.com.target.tgtpaymentprovider.zippay.data.request.CreateChargeRequest;
import au.com.target.tgtpaymentprovider.zippay.data.request.CreateCheckoutRequest;
import au.com.target.tgtpaymentprovider.zippay.data.request.CreateRefundRequest;
import au.com.target.tgtpaymentprovider.zippay.data.response.CheckoutResponse;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateChargeResponse;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateCheckoutResponse;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateRefundResponse;
import au.com.target.tgtpaymentprovider.zippay.data.types.Order;


/**
 * @author salexa10
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:tgtpaymentprovider-spring-test.xml")
public class TargetZippayClientImplTest {

    private final String createCheckoutResponseJson = "{ \"id\": \"co_8EVtW6efOqscHHSrlxRYl0\","
            + " \"uri\": \"https://account.sandbox.zipmoney.com.au/?co=co_8EVtW6efOqscHHSrlxRYl0&m=59bff7d0-5bfa-4d03-ab91-a071ecc5c64e\" }";

    private final String createChargeResponseJson = "{ \"id\": \"ch_OdvddBkayYR3xAn6AWwjT5\","
            + " \"receipt_number\": \"1234555\", \"state\": \"captured\" }";

    private final String createRefundResponseJson = "{ \"id\": \"rf_H5J24ZnqpKMByItvF2Jh85\" }";

    private MockRestServiceServer mockServer;

    @Mock
    private UserService userService;

    @InjectMocks
    @Autowired
    private TargetZippayClientImpl targetZippayClientImpl;

    final CreateRefundRequest createRefundRequest = new CreateRefundRequest();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockServer = MockRestServiceServer.createServer(targetZippayClientImpl
                .getZippayRestTemplate());
        createRefundRequest.setChargeId("ch_OdvddBkayYR3xAn6AWwjT5");
        createRefundRequest.setAmount(20);
        createRefundRequest.setReason("zip refund");
    }

    @Test
    public void testCreateCheckout() throws TargetZippayClientException {
        mockServer.expect(requestTo("https://api.sandbox.zipmoney.com.au/merchant/v1/checkouts"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(createCheckoutResponseJson, MediaType.APPLICATION_JSON));
        final CreateCheckoutRequest createCheckoutRequest = new CreateCheckoutRequest();
        final Order order = new Order();
        order.setReference("172810");
        createCheckoutRequest.setOrder(order);
        final CreateCheckoutResponse createCheckoutResponse = targetZippayClientImpl
                .createCheckout(createCheckoutRequest);
        assertThat(createCheckoutResponse.getId()).isEqualTo("co_8EVtW6efOqscHHSrlxRYl0");
        assertThat(createCheckoutResponse.getUri()).isEqualTo(
                "https://account.sandbox.zipmoney.com.au/?co=co_8EVtW6efOqscHHSrlxRYl0&m=59bff7d0-5bfa-4d03-ab91-a071ecc5c64e");
    }

    @Test(expected = TargetZippayClientException.class)
    public void testCreateCheckoutError() throws TargetZippayClientException {
        mockServer.expect(requestTo("https://api.sandbox.zipmoney.com.au/merchant/v1/checkouts"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withBadRequest());
        targetZippayClientImpl.createCheckout(new CreateCheckoutRequest());
    }

    @Test
    public void testIsZipPayConnectionSuccess() throws TargetZippayClientException {
        mockServer.expect(requestTo("https://api.sandbox.zipmoney.com.au/merchant/v1/diagnostics/ping"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("pong", MediaType.TEXT_PLAIN));
        final boolean result = targetZippayClientImpl.isZippayConnectionAvailable();
        assertThat(result).isEqualTo(true);
    }

    @Test
    public void testIsZipPayConnectionFailure() throws TargetZippayClientException {
        mockServer.expect(requestTo("https://api.sandbox.zipmoney.com.au/merchant/v1/diagnostics/ping"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withServerError());
        final boolean result = targetZippayClientImpl.isZippayConnectionAvailable();
        assertThat(result).isEqualTo(false);
    }

    @Test
    public void testIsZipPayConnectionBadRequest() throws TargetZippayClientException {
        mockServer.expect(requestTo("https://api.sandbox.zipmoney.com.au/merchant/v1/diagnostics/ping"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withBadRequest());
        final boolean result = targetZippayClientImpl.isZippayConnectionAvailable();
        assertThat(result).isEqualTo(false);
    }

    @Test
    public void testIsZipPayConnectionBlankResponse() throws TargetZippayClientException {
        mockServer.expect(requestTo("https://api.sandbox.zipmoney.com.au/merchant/v1/diagnostics/ping"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("", MediaType.TEXT_PLAIN));
        final boolean result = targetZippayClientImpl.isZippayConnectionAvailable();
        assertThat(result).isEqualTo(true);
    }

    @Test
    public void testRetrieveCheckoutSuccess() throws Exception {

        final ObjectMapper mapper = new ObjectMapper();
        final Properties jsonStringProps = new Properties();
        jsonStringProps.load(PayRequestTest.class.getResourceAsStream("/ExpectedJSON.properties"));
        final String jsonString = jsonStringProps.getProperty("checkoutResponse");
        final CheckoutResponse expectedResponse = mapper.readValue(jsonString, CheckoutResponse.class);

        mockServer
                .expect(requestTo(
                        "https://api.sandbox.zipmoney.com.au/merchant/v1/checkouts/co_OqWoMy93XwR5s86KZfH1x2"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(jsonString, MediaType.APPLICATION_JSON));
        final CheckoutResponse checkoutResponse = targetZippayClientImpl
                .retrieveCheckout("co_OqWoMy93XwR5s86KZfH1x2");
        assertThat(checkoutResponse.getState()).isEqualTo(expectedResponse.getState());
        assertThat(checkoutResponse.getOrder().getAmount()).isEqualTo(expectedResponse.getOrder().getAmount());
        assertThat(checkoutResponse.getOrder().getReference()).isEqualTo(expectedResponse.getOrder().getReference());
        assertThat(checkoutResponse.getOrder().getItems()).onProperty("name").containsExactly(
                "Awesome shoes", "Great shoes");
        assertThat(checkoutResponse.getOrder().getItems()).onProperty("amount").containsExactly(
                new Double(200), new Double(100));
        assertThat(checkoutResponse.getOrder().getItems()).onProperty("reference").containsExactly(
                "P4025_blue_M", "P4026_black_M");
        assertThat(checkoutResponse.getOrder().getItems()).onProperty("quantity").containsExactly(
                Long.valueOf(1), Long.valueOf(1));
        assertThat(checkoutResponse.getOrder().getItems()).onProperty("type").containsExactly(
                "sku", "sku");


    }

    @Test(expected = TargetZippayClientException.class)
    public void testRetrieveCheckoutError() throws TargetZippayClientException {
        mockServer
                .expect(requestTo(
                        "https://api.sandbox.zipmoney.com.au/merchant/v1/checkouts/co_OqWoMy93XwR5s86KZfH1x1"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withBadRequest());
        targetZippayClientImpl.retrieveCheckout("co_OqWoMy93XwR5s86KZfH1x1");
    }

    @Test
    public void testCreateCharge() throws TargetZippayClientException {
        mockServer.expect(requestTo("https://api.sandbox.zipmoney.com.au/merchant/v1/charges"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(createChargeResponseJson, MediaType.APPLICATION_JSON));
        final CreateChargeRequest request = new CreateChargeRequest();
        request.setAmount(20);
        request.setCurrency("AUD");
        final Authority authority = new Authority();
        authority.setValue("co_8EVtW6efOqscHHSrlxRYl0");
        request.setAuthority(authority);
        final CreateChargeResponse createChargeResponse = targetZippayClientImpl.createCharge(request);
        assertThat(createChargeResponse.getId()).isEqualTo("ch_OdvddBkayYR3xAn6AWwjT5");
        assertThat(createChargeResponse.getReceiptNumber()).isEqualTo("1234555");
        assertThat(createChargeResponse.getState()).isEqualTo("captured");
    }

    @Test(expected = TargetZippayClientException.class)
    public void testCreateChargeError() throws TargetZippayClientException {
        mockServer.expect(requestTo("https://api.sandbox.zipmoney.com.au/merchant/v1/charges"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withBadRequest());
        targetZippayClientImpl.createCharge(new CreateChargeRequest());
    }

    @Test
    public void testCreateRefundSuccess()
            throws TargetZippayTransactionRejectedException, TargetZippayTransactionError, TargetZippayClientException {
        mockServer.expect(requestTo("https://api.sandbox.zipmoney.com.au/merchant/v1/refunds"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(createRefundResponseJson, MediaType.APPLICATION_JSON));
        final CreateRefundResponse createRefundResponse = targetZippayClientImpl
                .createRefund(createRefundRequest);
        assertThat(createRefundResponse.getId()).isEqualTo("rf_H5J24ZnqpKMByItvF2Jh85");
    }

    @Test
    public void testCreateRefundWithNoResponse()
            throws TargetZippayClientException, TargetZippayTransactionRejectedException, TargetZippayTransactionError {
        mockServer.expect(requestTo("https://api.sandbox.zipmoney.com.au/merchant/v1/refunds"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withNoContent());
        final CreateRefundResponse createRefundResponse = targetZippayClientImpl.createRefund(createRefundRequest);
        assertThat(createRefundResponse).isNull();
    }

    @Test(expected = TargetZippayTransactionRejectedException.class)
    public void testCreateRefundRejected()
            throws TargetZippayClientException, TargetZippayTransactionRejectedException, TargetZippayTransactionError {
        mockServer.expect(requestTo("https://api.sandbox.zipmoney.com.au/merchant/v1/refunds"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.PAYMENT_REQUIRED));
        targetZippayClientImpl.createRefund(createRefundRequest);
    }

    @Test(expected = TargetZippayTransactionError.class)
    public void testCreateRefundError()
            throws TargetZippayClientException, TargetZippayTransactionRejectedException, TargetZippayTransactionError {
        mockServer.expect(requestTo("https://api.sandbox.zipmoney.com.au/merchant/v1/refunds"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withBadRequest());
        targetZippayClientImpl.createRefund(createRefundRequest);
    }

}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.commands.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpaymentprovider.zippay.client.TargetZippayClient;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayClientException;
import au.com.target.tgtpaymentprovider.zippay.data.request.CreateChargeRequest;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateChargeResponse;


/**
 * @author ramsatish
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetZippayCreateChargeCommandImplTest {

    private final TargetCaptureRequest request = new TargetCaptureRequest();
    @InjectMocks
    private final TargetZippayCreateChargeCommandImpl command = new TargetZippayCreateChargeCommandImpl();
    @Mock
    private TargetZippayClient targetZippayClient;

    @Test
    public void testCreateChargeCommandSuccessResponse() throws TargetZippayClientException {
        final CreateChargeResponse createChargeResponse = new CreateChargeResponse();
        createChargeResponse.setId("121243434");
        createChargeResponse.setReceiptNumber("444324234");
        createChargeResponse.setState("captured");

        given(targetZippayClient.createCharge((CreateChargeRequest)any())).willReturn(createChargeResponse);
        final TargetCaptureResult result = command.perform(request);
        assertThat(result).isNotNull();
        assertThat(result.getTransactionStatus()).isEqualTo(TransactionStatus.ACCEPTED);
        assertThat(result.getTransactionStatusDetails()).isEqualTo(TransactionStatusDetails.SUCCESFULL);

    }

    @Test
    public void testCreateChargeCommandFailureResponse() throws TargetZippayClientException {
        final CreateChargeResponse createChargeResponse = new CreateChargeResponse();
        createChargeResponse.setId("121243434");
        createChargeResponse.setReceiptNumber("444324234");
        createChargeResponse.setState("declined");

        given(targetZippayClient.createCharge((CreateChargeRequest)any())).willReturn(createChargeResponse);
        final TargetCaptureResult result = command.perform(request);
        assertThat(result).isNotNull();
        assertThat(result.getTransactionStatus()).isEqualTo(TransactionStatus.ERROR);
        assertThat(result.getTransactionStatusDetails()).isEqualTo(TransactionStatusDetails.GENERAL_SYSTEM_ERROR);
        assertThat(result.getDeclineCode()).isEqualTo(createChargeResponse.getState());

    }

    @Test(expected = AdapterException.class)
    public void testCreateChargeCommandThrowsException() throws TargetZippayClientException {
        given(targetZippayClient.createCharge((CreateChargeRequest)any()))
                .willThrow(new TargetZippayClientException());
        command.perform(request);

    }
}

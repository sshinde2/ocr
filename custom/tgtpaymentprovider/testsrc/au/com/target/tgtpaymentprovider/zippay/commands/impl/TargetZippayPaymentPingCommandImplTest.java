/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.commands.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetPaymentPingRequest;
import au.com.target.tgtpayment.commands.result.TargetPaymentPingResult;
import au.com.target.tgtpaymentprovider.zippay.client.TargetZippayClient;


/**
 * @author salexa10
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetZippayPaymentPingCommandImplTest {

    private final TargetPaymentPingRequest request = new TargetPaymentPingRequest();

    @Mock
    private TargetZippayClient targetZippayClient;

    @InjectMocks
    private final TargetZippayPaymentPingCommandImpl command = new TargetZippayPaymentPingCommandImpl();

    @Test
    public void testPingSuccessTrue() {
        given(Boolean.valueOf(targetZippayClient.isZippayConnectionAvailable())).willReturn(Boolean.TRUE);
        final TargetPaymentPingResult result = command.perform(request);
        assertThat(result).isNotNull();
        assertThat(result.isSuccess()).isTrue();
    }

    @Test
    public void testPingSuccessFalse() {
        given(Boolean.valueOf(targetZippayClient.isZippayConnectionAvailable())).willReturn(Boolean.FALSE);
        final TargetPaymentPingResult result = command.perform(request);
        assertThat(result).isNotNull();
        assertThat(result.isSuccess()).isFalse();
    }
}

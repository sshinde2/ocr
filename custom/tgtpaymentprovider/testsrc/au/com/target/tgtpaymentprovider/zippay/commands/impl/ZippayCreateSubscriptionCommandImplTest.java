/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.commands.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.request.TargetCreateSubscriptionRequest;
import au.com.target.tgtpayment.commands.result.TargetCreateSubscriptionResult;
import au.com.target.tgtpayment.dto.Address;
import au.com.target.tgtpayment.dto.AmountType;
import au.com.target.tgtpayment.dto.Customer;
import au.com.target.tgtpayment.dto.LineItem;
import au.com.target.tgtpayment.dto.Order;
import au.com.target.tgtpaymentprovider.zippay.client.TargetZippayClient;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayClientException;
import au.com.target.tgtpaymentprovider.zippay.data.request.CreateCheckoutRequest;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateCheckoutResponse;


/**
 * @author salexa10
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ZippayCreateSubscriptionCommandImplTest {

    @Mock
    private TargetZippayClient targetZippayClient;

    @InjectMocks
    private final ZippayCreateSubscriptionCommandImpl command = new ZippayCreateSubscriptionCommandImpl();

    @Mock
    private TargetCreateSubscriptionRequest paramRequest;

    @Mock
    private Order order;

    @Mock
    private AmountType totalAmount;

    @Mock
    private AmountType shippingAmount;

    List<LineItem> lineItems = new ArrayList<>();

    @Before
    public void setup() {
        given(totalAmount.getAmount()).willReturn(BigDecimal.valueOf(10));
        given(totalAmount.getCurrency()).willReturn(Currency.getInstance("AUD"));
        given(order.getOrderTotalAmount()).willReturn(totalAmount);
        final Customer customer = mock(Customer.class);
        given(customer.getGivenNames()).willReturn("Barry");
        given(customer.getSurname()).willReturn("Allen");
        given(customer.getEmail()).willReturn("barry@starlabs.com");
        given(order.getCustomer()).willReturn(customer);
        final Address address = mock(Address.class);
        given(address.getStreet1()).willReturn("10 Test st");
        given(address.getState()).willReturn("NSW");
        given(address.getPostalCode()).willReturn("2000");
        given(address.getCountryCode()).willReturn("AU");
        given(address.getCity()).willReturn("Sydney");
        given(order.getShippingAddress()).willReturn(address);
        final Address billingAddress = mock(Address.class);
        given(billingAddress.getStreet1()).willReturn("10 Test st");
        given(billingAddress.getState()).willReturn("NSW");
        given(billingAddress.getPostalCode()).willReturn("2000");
        given(billingAddress.getCountryCode()).willReturn("AU");
        given(billingAddress.getCity()).willReturn("Sydney");
        given(order.getBillingAddress()).willReturn(billingAddress);
        final LineItem lineItem1 = mock(LineItem.class);
        given(lineItem1.getName()).willReturn("Product 1");
        given(lineItem1.getProductCode()).willReturn("p1");
        willReturn(Long.valueOf(1)).given(lineItem1).getQuantity();
        final AmountType price1 = mock(AmountType.class);
        given(price1.getAmount()).willReturn(BigDecimal.valueOf(4));
        given(price1.getCurrency()).willReturn(Currency.getInstance("AUD"));
        given(lineItem1.getPrice()).willReturn(price1);
        given(lineItem1.getType()).willReturn("SKU");
        given(lineItem1.getImageUri())
                .willReturn("https://www.target.com.au/medias/static_content/product/images/hero/31/93/A1203193.jpg");

        lineItems.add(lineItem1);
        final LineItem lineItem2 = mock(LineItem.class);
        given(lineItem2.getName()).willReturn("Product 2");
        given(lineItem2.getProductCode()).willReturn("p2");
        willReturn(Long.valueOf(2)).given(lineItem2).getQuantity();
        final AmountType price2 = mock(AmountType.class);
        given(price2.getAmount()).willReturn(BigDecimal.valueOf(3));
        given(price2.getCurrency()).willReturn(Currency.getInstance("AUD"));
        given(lineItem2.getPrice()).willReturn(price2);
        given(lineItem2.getType()).willReturn("SKU");
        given(lineItem2.getImageUri())
                .willReturn("https://www.target.com.au/medias/static_content/product/images/full/78/57/A1197857.jpg");
        lineItems.add(lineItem2);
        given(order.getLineItems()).willReturn(lineItems);
        given(paramRequest.getOrder()).willReturn(order);
        given(paramRequest.getReturnUrl()).willReturn("/payment-details/zippay-return/spc");
    }

    @Test
    public void testPerform() throws TargetZippayClientException {

        final CreateCheckoutResponse response = mock(CreateCheckoutResponse.class);
        given(response.getId()).willReturn("co_NNb4qU1DCqE1Dvu2kqcTJ0");
        given(response.getUri()).willReturn(
                "https://account.sandbox.zipmoney.com.au/?co=co_NNb4qU1DCqE1Dvu2kqcTJ0&m=59bff7d0-5bfa-4d03-ab91-a071ecc5c64e");
        given(targetZippayClient.createCheckout(Mockito.any(CreateCheckoutRequest.class)))
                .willReturn(response);
        final TargetCreateSubscriptionResult subscriptionResult = command.perform(paramRequest);
        assertThat(subscriptionResult).isNotNull();
        assertThat(subscriptionResult.getRequestToken()).isEqualTo("co_NNb4qU1DCqE1Dvu2kqcTJ0");
        assertThat(subscriptionResult.getZipPaymentRedirectUrl()).isEqualTo(
                "https://account.sandbox.zipmoney.com.au/?co=co_NNb4qU1DCqE1Dvu2kqcTJ0&m=59bff7d0-5bfa-4d03-ab91-a071ecc5c64e");
        final ArgumentCaptor<CreateCheckoutRequest> createCheckoutRequestCapture = ArgumentCaptor
                .forClass(CreateCheckoutRequest.class);
        verify(targetZippayClient).createCheckout(createCheckoutRequestCapture.capture());
        final CreateCheckoutRequest createCheckoutRequest = createCheckoutRequestCapture.getValue();
        assertThat(createCheckoutRequest).isNotNull();
        assertThat(createCheckoutRequest.getOrder().getAmount()).isEqualTo(10);
        assertThat(createCheckoutRequest.getOrder().getCurrency()).isEqualTo("AUD");
        assertThat(createCheckoutRequest.getShopper().getFirstName()).isEqualTo("Barry");
        assertThat(createCheckoutRequest.getShopper().getLastName()).isEqualTo("Allen");
        assertThat(createCheckoutRequest.getShopper().getEmail()).isEqualTo("barry@starlabs.com");
        assertThat(createCheckoutRequest.getConfig().getRedirectUri()).isEqualTo("/payment-details/zippay-return/spc");

        assertThat(createCheckoutRequest.getOrder().getItems()).hasSize(2);
        assertThat(createCheckoutRequest.getOrder().getItems()).onProperty("amount").containsExactly(
                4.0,
                3.0);
        assertThat(createCheckoutRequest.getOrder().getCurrency()).isEqualTo("AUD");
        assertThat(createCheckoutRequest.getOrder().getItems()).onProperty("name").containsExactly("Product 1",
                "Product 2");
        assertThat(createCheckoutRequest.getOrder().getItems()).onProperty("type").containsExactly("SKU", "SKU");
        assertThat(createCheckoutRequest.getOrder().getItems()).onProperty("imageUri").containsExactly(
                "https://www.target.com.au/medias/static_content/product/images/hero/31/93/A1203193.jpg",
                "https://www.target.com.au/medias/static_content/product/images/full/78/57/A1197857.jpg");
        assertThat(createCheckoutRequest.getOrder().getItems()).onProperty("quantity").containsExactly(Long.valueOf(1),
                Long.valueOf(2));
    }

    @Test
    public void testPerformWithDiscountItemsWithShipping() throws TargetZippayClientException {

        given(shippingAmount.getAmount()).willReturn(BigDecimal.valueOf(9));
        given(shippingAmount.getCurrency()).willReturn(Currency.getInstance("AUD"));
        given(order.getShippingTotalAmount()).willReturn(shippingAmount);

        final LineItem lineItem3 = mock(LineItem.class);
        given(lineItem3.getName()).willReturn("flybuys");
        given(lineItem3.getProductCode()).willReturn(StringUtils.EMPTY);
        willReturn(Long.valueOf(0)).given(lineItem3).getQuantity();
        final AmountType price3 = mock(AmountType.class);
        given(price3.getAmount()).willReturn(BigDecimal.valueOf(-2));
        given(price3.getCurrency()).willReturn(Currency.getInstance("AUD"));
        given(lineItem3.getPrice()).willReturn(price3);
        given(lineItem3.getType()).willReturn("discount");
        given(Boolean.valueOf(lineItem3.isDiscount())).willReturn(Boolean.valueOf(true));
        given(lineItem3.getImageUri())
                .willReturn(StringUtils.EMPTY);
        lineItems.add(lineItem3);

        final LineItem lineItem4 = mock(LineItem.class);
        given(lineItem4.getName()).willReturn("tmd");
        given(lineItem4.getProductCode()).willReturn(StringUtils.EMPTY);
        willReturn(Long.valueOf(0)).given(lineItem3).getQuantity();
        final AmountType price4 = mock(AmountType.class);
        given(price4.getAmount()).willReturn(BigDecimal.valueOf(-1));
        given(price4.getCurrency()).willReturn(Currency.getInstance("AUD"));
        given(lineItem4.getPrice()).willReturn(price4);
        given(lineItem4.getType()).willReturn("discount");
        given(Boolean.valueOf(lineItem4.isDiscount())).willReturn(Boolean.valueOf(true));
        given(lineItem4.getImageUri())
                .willReturn(StringUtils.EMPTY);
        lineItems.add(lineItem4);

        given(order.getLineItems()).willReturn(lineItems);
        given(paramRequest.getOrder()).willReturn(order);
        given(paramRequest.getReturnUrl()).willReturn("/payment-details/zippay-return/spc");
        final CreateCheckoutResponse response = mock(CreateCheckoutResponse.class);
        given(response.getId()).willReturn("co_NNb4qU1DCqE1Dvu2kqcTJ0");
        given(response.getUri()).willReturn(
                "https://account.sandbox.zipmoney.com.au/?co=co_NNb4qU1DCqE1Dvu2kqcTJ0&m=59bff7d0-5bfa-4d03-ab91-a071ecc5c64e");
        given(targetZippayClient.createCheckout(Mockito.any(CreateCheckoutRequest.class)))
                .willReturn(response);
        final TargetCreateSubscriptionResult subscriptionResult = command.perform(paramRequest);
        assertThat(subscriptionResult).isNotNull();
        assertThat(subscriptionResult.getRequestToken()).isEqualTo("co_NNb4qU1DCqE1Dvu2kqcTJ0");
        assertThat(subscriptionResult.getZipPaymentRedirectUrl()).isEqualTo(
                "https://account.sandbox.zipmoney.com.au/?co=co_NNb4qU1DCqE1Dvu2kqcTJ0&m=59bff7d0-5bfa-4d03-ab91-a071ecc5c64e");

        final ArgumentCaptor<CreateCheckoutRequest> createCheckoutRequestCapture = ArgumentCaptor
                .forClass(CreateCheckoutRequest.class);
        verify(targetZippayClient).createCheckout(createCheckoutRequestCapture.capture());
        final CreateCheckoutRequest createCheckoutRequest = createCheckoutRequestCapture.getValue();
        assertThat(createCheckoutRequest).isNotNull();
        assertThat(createCheckoutRequest.getOrder().getAmount()).isEqualTo(10);
        assertThat(createCheckoutRequest.getOrder().getCurrency()).isEqualTo("AUD");
        assertThat(createCheckoutRequest.getShopper().getFirstName()).isEqualTo("Barry");
        assertThat(createCheckoutRequest.getShopper().getLastName()).isEqualTo("Allen");
        assertThat(createCheckoutRequest.getShopper().getEmail()).isEqualTo("barry@starlabs.com");
        assertThat(createCheckoutRequest.getConfig().getRedirectUri()).isEqualTo("/payment-details/zippay-return/spc");

        assertThat(createCheckoutRequest.getOrder().getItems()).hasSize(5);
        assertThat(createCheckoutRequest.getOrder().getItems()).onProperty("amount").containsExactly(
                Double.valueOf(4.0),
                Double.valueOf(3.0), Double.valueOf(-2.0), Double.valueOf(-1.0), Double.valueOf(9.0));
        assertThat(createCheckoutRequest.getOrder().getCurrency()).isEqualTo("AUD");
        assertThat(createCheckoutRequest.getOrder().getItems()).onProperty("name").containsExactly("Product 1",
                "Product 2", "flybuys", "tmd", "shipping");
        assertThat(createCheckoutRequest.getOrder().getItems()).onProperty("type").containsExactly("SKU", "SKU",
                "discount", "discount", "shipping");
        assertThat(createCheckoutRequest.getOrder().getItems()).onProperty("imageUri").containsExactly(
                "https://www.target.com.au/medias/static_content/product/images/hero/31/93/A1203193.jpg",
                "https://www.target.com.au/medias/static_content/product/images/full/78/57/A1197857.jpg",
                "", "", null);
        assertThat(createCheckoutRequest.getOrder().getItems()).onProperty("quantity").containsExactly(Long.valueOf(1),
                Long.valueOf(2), Long.valueOf(0), Long.valueOf(0), Long.valueOf(0));
    }

    @Test(expected = AdapterException.class)
    public void testPerformWithClientExpection() throws TargetZippayClientException {
        given(targetZippayClient.createCheckout(Mockito.any(CreateCheckoutRequest.class)))
                .willThrow(new TargetZippayClientException());
        command.perform(paramRequest);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPerformWithNullRequest() throws TargetZippayClientException {
        command.perform(null);
    }

}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.zippay.commands.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.request.SubscriptionDataRequest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpayment.commands.result.TargetGetSubscriptionResult;
import au.com.target.tgtpaymentprovider.zippay.client.TargetZippayClient;
import au.com.target.tgtpaymentprovider.zippay.client.exception.TargetZippayClientException;
import au.com.target.tgtpaymentprovider.zippay.data.response.CheckoutResponse;
import au.com.target.tgtpaymentprovider.zippay.data.types.Item;
import au.com.target.tgtpaymentprovider.zippay.data.types.Order;


/**
 * @author salexa10
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ZippayGetSubscriptionCommandImplTest {

    @InjectMocks
    private final ZippayGetSubscriptionCommandImpl command = new ZippayGetSubscriptionCommandImpl();

    @Mock
    private TargetZippayClient targetZippayClient;

    @Mock
    private SubscriptionDataRequest subscriptionDataRequest;

    @Mock
    private CheckoutResponse checkoutResponse;

    @Mock
    private Order order;

    @Mock
    private Item item1;

    @Mock
    private Item item2;

    private final List<Item> items = new ArrayList<>();

    @Before
    public void setUp() {
        given(checkoutResponse.getState()).willReturn("approved");
        given(order.getReference()).willReturn("1234567");
        given(Double.valueOf(order.getAmount())).willReturn(Double.valueOf(100.00));
        given(order.getCurrency()).willReturn("AUD");

        given(item1.getName()).willReturn("Awesome shoes");
        given(Double.valueOf(item1.getAmount())).willReturn(Double.valueOf(70.00));
        given(item1.getReference()).willReturn("P4025_blue_M");
        given(Long.valueOf(item1.getQuantity())).willReturn(Long.valueOf(1));
        given(item1.getType()).willReturn("sku");
        given(item1.getImageUri())
                .willReturn("https://www.target.com.au/medias/static_content/product/images/hero/31/93/A1203193.jpg");
        items.add(item1);

        given(item2.getName()).willReturn("Awesome Toy");
        given(Double.valueOf(item2.getAmount())).willReturn(Double.valueOf(30.00));
        given(item2.getReference()).willReturn("P4025_blue_S");
        given(Long.valueOf(item2.getQuantity())).willReturn(Long.valueOf(2));
        given(item2.getType()).willReturn("sku");
        given(item2.getImageUri())
                .willReturn("https://www.target.com.au/medias/static_content/product/images/full/78/57/A1197857.jpg");
        items.add(item2);

        given(order.getItems()).willReturn(items);
        given(checkoutResponse.getOrder()).willReturn(order);
    }

    @Test
    public void testPerform() throws Exception {
        given(targetZippayClient.retrieveCheckout(subscriptionDataRequest.getSubscriptionID()))
                .willReturn(checkoutResponse);
        final TargetGetSubscriptionResult result = command.perform(subscriptionDataRequest);
        assertThat(result).isNotNull();
        assertThat(result.getOrderState()).isEqualTo("approved");
        assertThat(result.getOrderReference()).isEqualTo("1234567");
        assertThat(result.getAmount()).isEqualTo(BigDecimal.valueOf(100.00));
        assertThat(result.getLineItems().size()).isEqualTo(2);
        assertThat(result.getLineItems()).onProperty("name").containsExactly("Awesome shoes",
                "Awesome Toy");
        assertThat(result.getLineItems()).onProperty("price.amount").containsExactly(BigDecimal.valueOf(70.00),
                BigDecimal.valueOf(30.00));
        assertThat(result.getLineItems()).onProperty("productCode").containsExactly("P4025_blue_M",
                "P4025_blue_S");
        assertThat(result.getLineItems()).onProperty("quantity").containsExactly(Long.valueOf(1),
                Long.valueOf(2));
        assertThat(result.getLineItems()).onProperty("type").containsExactly("sku",
                "sku");
        assertThat(result.getLineItems()).onProperty("imageUri").containsExactly(
                "https://www.target.com.au/medias/static_content/product/images/hero/31/93/A1203193.jpg",
                "https://www.target.com.au/medias/static_content/product/images/full/78/57/A1197857.jpg");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPerformWhenRequestNull() {
        command.perform(null);
    }

    @Test(expected = AdapterException.class)
    public void testPerformWhenZippayClientException() throws TargetZippayClientException {

        final SubscriptionDataRequest request = mock(SubscriptionDataRequest.class);

        given(targetZippayClient.retrieveCheckout(request.getSubscriptionID()))
                .willThrow(new TargetZippayClientException());
        command.perform(request);
    }
}

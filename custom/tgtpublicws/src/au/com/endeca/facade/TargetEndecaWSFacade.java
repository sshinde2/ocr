/**
 * 
 */
package au.com.endeca.facade;

import java.util.List;

import au.com.target.tgtfacades.product.data.TargetProductListerData;


/**
 * @author pthoma20
 *
 */
public interface TargetEndecaWSFacade {

    /**
     * This method takes in a list of productCodes for instagram and catalogue and queries endeca to get the product
     * information.
     * 
     * @param productCodes
     * @return List<TargetProductListerData>
     */
    public List<TargetProductListerData> getProductsForInstagramAndCatalogue(final List<String> productCodes);

    /**
     * Get a list of the products contained in the supplied categories. Important note: dealCategoryDimIds should
     * contain IDs for a single dimension (eg. dealQualifierCategory OR dealRewardCategory, not both).
     * 
     * @param dealCategoryDimIds
     *            Endeca dimension IDs for deal categories
     * @return A list of products
     */
    List<TargetProductListerData> getProductsForDealCategories(final List<String> dealCategoryDimIds);
}

/**
 * 
 */
package au.com.target.tgtpublicws.cart.impl;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.CartModel;

import au.com.target.tgtfacades.order.impl.TargetCartFacadeImpl;


public class CommerceWebServicesCartFacade extends TargetCartFacadeImpl
{
    @Override
    public CartData getSessionCart()
    {
        final CartData cartData;
        final CartModel cart = getCartService().getSessionCart();
        cartData = getCartConverter().convert(cart);
        return cartData;
    }


    /**
     * Method to check whether a cart is present for the cartGuid passed against the base site for an anonymous user
     * 
     * @param cartGuid
     * @return whether a cart is found
     */
    public boolean isAnonymousUserCart(final String cartGuid) {
        final CartModel cart = getCommerceCartService().getCartForGuidAndSiteAndUser(cartGuid,
                getBaseSiteService().getCurrentBaseSite(), getUserService().getAnonymousUser());
        return cart != null;
    }

    /**
     * Method to check whether a cart is present for the cartGuid passed against the base site for current user
     * 
     * @param cartGuid
     * @return whether a cart is found
     */
    public boolean isCurrentUserCart(final String cartGuid) {
        final CartModel cart = getCommerceCartService().getCartForGuidAndSiteAndUser(cartGuid,
                getBaseSiteService().getCurrentBaseSite(), getUserService().getCurrentUser());
        return cart != null;
    }
}

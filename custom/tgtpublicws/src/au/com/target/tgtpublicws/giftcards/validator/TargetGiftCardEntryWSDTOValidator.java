/**
 * 
 */
package au.com.target.tgtpublicws.giftcards.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import au.com.target.tgtpublicws.delivery.dto.order.TargetGiftCardOrderEntryWsDTO;
import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * Validate TargetGiftCardOrderEntryDTO to GiftRecipientDTO
 * 
 * @author pthoma20
 *
 */
public class TargetGiftCardEntryWSDTOValidator implements Validator {

    /**
     * Validate method to validate the giftcard entry details
     * 
     * @param paramObject
     * @param errors
     */
    @Override
    public void validate(final Object paramObject, final Errors errors) {
        final TargetGiftCardOrderEntryWsDTO dto = (TargetGiftCardOrderEntryWsDTO)paramObject;

        if (StringUtils.isBlank(dto.getFirstName())) {
            errors.reject("giftCardEntry.firstName.blank");
        }

        if (StringUtils.isBlank(dto.getLastName())) {
            errors.reject("giftCardEntry.lastName.blank");
        }

        if (StringUtils.isBlank(dto.getRecipientEmailAddress())) {
            errors.reject("giftCardEntry.recipientEmailAddress.blank");
        }
        else if (!TargetValidationCommon.Email.PATTERN.matcher(dto.getRecipientEmailAddress()).matches()) {
            errors.reject("giftCardEntry.recipientEmailAddress.invalid", dto.getRecipientEmailAddress());
        }
    }

    /* (non-Javadoc)
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(final Class paramClass) {
        return TargetGiftCardOrderEntryWsDTO.class.equals(paramClass);
    }

}

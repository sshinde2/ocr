package au.com.target.tgtpublicws.navigation;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.navigation.NavigationMenuItem;
import au.com.target.tgtwebcore.enums.NavigationNodeStyleEnum;
import au.com.target.tgtwebcore.enums.NavigationNodeTypeEnum;


@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class MobileMenuBuilderTest {

    @InjectMocks
    protected final MobileMenuBuilder mobileMenuBuilder = new MobileMenuBuilder();

    @Mock
    private Converter<CategoryModel, CategoryData> mockCategoryConverter;

    @Mock
    private UrlResolver<CategoryData> mockCategoryDataUrlResolver;

    @Mock
    private CMSNavigationNodeModel cmsNavigationNodeModel;

    @SuppressWarnings("boxing")
    @Test
    public void testCreateMenuItemWithSingleNotVisibleChildNavNode() {
        final CMSNavigationNodeModel mockInvisibleNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockInvisibleNavigationNode.isVisible()).willReturn(false);
        final List<CMSNavigationNodeModel> navNodeList = new ArrayList<>();
        navNodeList.add(mockInvisibleNavigationNode);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.isVisible()).willReturn(Boolean.TRUE);
        given(mockNavigationNode.getChildren()).willReturn(navNodeList);

        final NavigationMenuItem menuItem = mobileMenuBuilder.createMenuItem(mockNavigationNode);

        assertThat(menuItem).isNull();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCreateMenuItemWithSingleChildWithNoEntriesNoChildren() {
        final CMSNavigationNodeModel mockChildNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockChildNavigationNode.isVisible()).willReturn(true);

        final List<CMSNavigationNodeModel> navNodeList = new ArrayList<>();
        navNodeList.add(mockChildNavigationNode);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.isVisible()).willReturn(Boolean.TRUE);
        given(mockNavigationNode.getChildren()).willReturn(navNodeList);

        final NavigationMenuItem menuItem = mobileMenuBuilder.createMenuItem(mockNavigationNode);

        assertThat(menuItem).isNull();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCreateMenuItemsForChildrenWithNoEntriesWithChildWithAllTypes() {
        final ContentPageModel mockContentPage = mock(ContentPageModel.class);
        final CMSNavigationEntryModel mockContentPageNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockContentPageNavigationEntry.getItem()).willReturn(mockContentPage);
        final CMSNavigationNodeModel mockContentPageNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockContentPageNavigationNode.isVisible()).willReturn(true);
        given(mockContentPageNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockContentPageNavigationEntry));

        final CategoryModel mockCategory = mock(CategoryModel.class);
        final CMSNavigationEntryModel mockCategoryNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockCategoryNavigationEntry.getItem()).willReturn(mockCategory);
        final CMSNavigationNodeModel mockCategoryNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockCategoryNavigationNode.isVisible()).willReturn(true);
        given(mockCategoryNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockCategoryNavigationEntry));
        final CategoryData mockCategoryData = mock(CategoryData.class);
        given(mockCategoryConverter.convert(mockCategory)).willReturn(mockCategoryData);

        final CMSLinkComponentModel mockCmsLinkComponent = mock(CMSLinkComponentModel.class);
        given(mockCmsLinkComponent.getVisible()).willReturn(Boolean.TRUE);
        final CMSNavigationEntryModel mockCmsLinkComponentNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockCmsLinkComponentNavigationEntry.getItem()).willReturn(mockCmsLinkComponent);
        final CMSNavigationNodeModel mockCmsLinkComponentNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockCmsLinkComponentNavigationNode.isVisible()).willReturn(true);
        given(mockCmsLinkComponentNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockCmsLinkComponentNavigationEntry));

        final ItemModel mockItem = mock(ItemModel.class);
        final CMSNavigationEntryModel mockItemNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockItemNavigationEntry.getItem()).willReturn(mockItem);
        final CMSNavigationNodeModel mockItemNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockItemNavigationNode.isVisible()).willReturn(true);
        given(mockItemNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockItemNavigationEntry));

        final List<CMSNavigationNodeModel> mockChildNavigationNodeList = new ArrayList<>();
        mockChildNavigationNodeList.add(mockContentPageNavigationNode);
        mockChildNavigationNodeList.add(mockCategoryNavigationNode);
        mockChildNavigationNodeList.add(mockCmsLinkComponentNavigationNode);
        mockChildNavigationNodeList.add(mockItemNavigationNode);

        final CMSNavigationNodeModel mockSectionNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockSectionNavigationNode.isVisible()).willReturn(Boolean.TRUE);
        given(mockSectionNavigationNode.getChildren()).willReturn(mockChildNavigationNodeList);
        given(mockSectionNavigationNode.getStyle()).willReturn(NavigationNodeStyleEnum.NEWLINK);
        given(mockSectionNavigationNode.getName()).willReturn("Section Name");
        given(mockSectionNavigationNode.getTitle()).willReturn("Section Title");
        given(mockSectionNavigationNode.getType()).willReturn(NavigationNodeTypeEnum.SECTION);

        final List<CMSNavigationNodeModel> mockSectionNavigationNodeList = new ArrayList<>();
        mockSectionNavigationNodeList.add(mockSectionNavigationNode);

        final CMSNavigationNodeModel mockColumnNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockColumnNavigationNode.isVisible()).willReturn(Boolean.TRUE);
        given(mockColumnNavigationNode.getChildren()).willReturn(mockSectionNavigationNodeList);
        given(mockColumnNavigationNode.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);

        final List<CMSNavigationNodeModel> mockColumnNavigationNodeList = new ArrayList<>();
        mockColumnNavigationNodeList.add(mockColumnNavigationNode);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.isVisible()).willReturn(true);
        given(mockNavigationNode.getChildren()).willReturn(mockColumnNavigationNodeList);

        final List<CMSNavigationNodeModel> navNodeList = new ArrayList<>();
        navNodeList.add(mockNavigationNode);

        final CMSNavigationNodeModel mockRootNavNode = mock(CMSNavigationNodeModel.class);
        given(mockRootNavNode.isVisible()).willReturn(Boolean.TRUE);
        given(mockRootNavNode.getChildren()).willReturn(navNodeList);

        final NavigationMenuItem menuItem = mobileMenuBuilder.createMenuItem(mockRootNavNode);

        assertThat(menuItem).isNotNull();

        final Set<NavigationMenuItem> menuItems = menuItem.getChildren();
        assertThat(menuItems).isNotNull();
        assertThat(menuItems).hasSize(1);

        final NavigationMenuItem secondMenuItem = menuItems.iterator().next();
        assertThat(secondMenuItem).isNotNull();
        assertThat(secondMenuItem.getStyle()).isEqualTo(NavigationNodeStyleEnum.NEWLINK.getCode());
        assertThat(secondMenuItem.getName()).isEqualTo("Section Title");
        assertThat(secondMenuItem.getChildren()).hasSize(3);
    }

    @Test
    public void testCreateMenuItemNull() {
        assertThat(mobileMenuBuilder.createMenuItem(null)).isNull();
    }

    @Test
    public void testCreateMenuItemEmpty() {
        given(cmsNavigationNodeModel.getChildren()).willReturn(null);
        given(cmsNavigationNodeModel.getEntries()).willReturn(null);
        assertThat(mobileMenuBuilder.createMenuItem(cmsNavigationNodeModel)).isNull();
    }

    @Test
    public void testCreateMenuItemEmptyEntries() {
        final List<CMSNavigationNodeModel> nodes = new ArrayList<>();
        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        nodes.add(mockNavigationNode);
        given(cmsNavigationNodeModel.getChildren()).willReturn(nodes);
        given(cmsNavigationNodeModel.getEntries()).willReturn(null);
        assertThat(mobileMenuBuilder.createMenuItem(cmsNavigationNodeModel)).isNull();
    }

    @Test
    public void testCreateMenuItemNullChildrenAndEmptyEntries() {
        final List<CMSNavigationEntryModel> entries = new ArrayList<>();
        final CMSNavigationEntryModel mockEntry = mock(CMSNavigationEntryModel.class);
        entries.add(mockEntry);
        given(cmsNavigationNodeModel.getChildren()).willReturn(null);
        given(cmsNavigationNodeModel.getEntries()).willReturn(entries);
        given(mockEntry.getItem()).willReturn(null);
        assertThat(mobileMenuBuilder.createMenuItem(cmsNavigationNodeModel)).isNull();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCreateMenuItemForChildrenWithNoEntriesWithChildWithAllTypes() {
        final ContentPageModel mockContentPage = mock(ContentPageModel.class);
        final CMSNavigationEntryModel mockContentPageNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockContentPageNavigationEntry.getItem()).willReturn(mockContentPage);
        final CMSNavigationNodeModel mockContentPageNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockContentPageNavigationNode.isVisible()).willReturn(true);
        given(mockContentPageNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockContentPageNavigationEntry));

        final CategoryModel mockCategory = mock(CategoryModel.class);
        final CMSNavigationEntryModel mockCategoryNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockCategoryNavigationEntry.getItem()).willReturn(mockCategory);
        final CMSNavigationNodeModel mockCategoryNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockCategoryNavigationNode.isVisible()).willReturn(true);
        given(mockCategoryNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockCategoryNavigationEntry));
        final CategoryData mockCategoryData = mock(CategoryData.class);
        given(mockCategoryConverter.convert(mockCategory)).willReturn(mockCategoryData);

        final CMSLinkComponentModel mockCmsLinkComponent = mock(CMSLinkComponentModel.class);
        given(mockCmsLinkComponent.getVisible()).willReturn(Boolean.TRUE);
        final CMSNavigationEntryModel mockCmsLinkComponentNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockCmsLinkComponentNavigationEntry.getItem()).willReturn(mockCmsLinkComponent);
        final CMSNavigationNodeModel mockCmsLinkComponentNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockCmsLinkComponentNavigationNode.isVisible()).willReturn(true);
        given(mockCmsLinkComponentNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockCmsLinkComponentNavigationEntry));

        final ItemModel mockItem = mock(ItemModel.class);
        final CMSNavigationEntryModel mockItemNavigationEntry = mock(CMSNavigationEntryModel.class);
        given(mockItemNavigationEntry.getItem()).willReturn(mockItem);
        final CMSNavigationNodeModel mockItemNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockItemNavigationNode.isVisible()).willReturn(true);
        given(mockItemNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockItemNavigationEntry));

        final List<CMSNavigationNodeModel> mockChildNavigationNodeList = new ArrayList<>();
        mockChildNavigationNodeList.add(mockContentPageNavigationNode);
        mockChildNavigationNodeList.add(mockCategoryNavigationNode);
        mockChildNavigationNodeList.add(mockCmsLinkComponentNavigationNode);
        mockChildNavigationNodeList.add(mockItemNavigationNode);

        final CMSNavigationNodeModel mockNavigationNode = mock(CMSNavigationNodeModel.class);
        given(mockNavigationNode.isVisible()).willReturn(true);
        given(mockNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockCmsLinkComponentNavigationEntry));
        given(mockNavigationNode.getChildren()).willReturn(null);

        final List<CMSNavigationNodeModel> navNodeList = new ArrayList<>();
        navNodeList.add(mockNavigationNode);
        given(cmsNavigationNodeModel.isVisible()).willReturn(Boolean.TRUE);
        given(cmsNavigationNodeModel.getChildren()).willReturn(navNodeList);
        given(cmsNavigationNodeModel.getEntries()).willReturn(
                Collections.singletonList(mockCmsLinkComponentNavigationEntry));
        final NavigationMenuItem menuItem = mobileMenuBuilder.createMenuItem(cmsNavigationNodeModel);

        assertThat(menuItem).isNotNull();
        assertThat(menuItem.getChildren()).hasSize(1);
    }

}

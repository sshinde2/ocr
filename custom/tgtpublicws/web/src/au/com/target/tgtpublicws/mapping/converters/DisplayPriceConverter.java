/**
 * 
 */
package au.com.target.tgtpublicws.mapping.converters;

import de.hybris.platform.commercefacades.product.data.PriceData;

import au.com.target.tgtfacades.util.PriceFormatUtils;
import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.metadata.Type;


/**
 * @author rmcalave
 *
 */
public class DisplayPriceConverter extends CustomConverter<PriceData, String> {

    /* (non-Javadoc)
     * @see ma.glasnost.orika.Converter#convert(java.lang.Object, ma.glasnost.orika.metadata.Type)
     */
    @Override
    public String convert(final PriceData source, final Type<? extends String> destinationType) {
        return PriceFormatUtils.formatPrice(source.getFormattedValue());
    }

}

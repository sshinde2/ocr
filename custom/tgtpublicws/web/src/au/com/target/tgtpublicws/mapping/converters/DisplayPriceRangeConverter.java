/**
 * 
 */
package au.com.target.tgtpublicws.mapping.converters;

import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;

import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.metadata.Type;
import au.com.target.tgtfacades.util.PriceFormatUtils;


/**
 * @author pthoma20
 *
 */
public class DisplayPriceRangeConverter extends CustomConverter<PriceRangeData, String> {

    /* (non-Javadoc)
     * @see ma.glasnost.orika.Converter#convert(java.lang.Object, ma.glasnost.orika.metadata.Type)
     */
    @Override
    public String convert(final PriceRangeData source, final Type<? extends String> destinationType) {
        return PriceFormatUtils.formatPrice(source);
    }
}

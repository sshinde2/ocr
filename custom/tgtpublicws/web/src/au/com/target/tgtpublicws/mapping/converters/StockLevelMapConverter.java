/**
 * 
 */
package au.com.target.tgtpublicws.mapping.converters;

import java.util.Map;

import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.metadata.Type;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author rmcalave
 *
 */
public class StockLevelMapConverter extends CustomConverter<Map<String, Boolean>, Boolean> {

    /* (non-Javadoc)
     * @see ma.glasnost.orika.Converter#convert(java.lang.Object, ma.glasnost.orika.metadata.Type)
     */
    @Override
    public Boolean convert(final Map<String, Boolean> source, final Type<? extends Boolean> destinationType) {
        if (CollectionUtils.isEmpty(source.values())) {
            return Boolean.FALSE;
        }

        return Boolean.valueOf(source.values().contains(Boolean.TRUE));
    }

}

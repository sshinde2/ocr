package au.com.target.tgtpublicws.v2.controller;


import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtpublicws.product.dto.TgtEndecaProductsWsDTO;
import au.com.target.tgtpublicws.v2.helper.EndecaHelper;


/**
 * 
 */

@Controller
@RequestMapping(value = "/{baseSiteId}/endeca")
public class EndecaProductsController extends BaseController {


    private static final Logger LOG = Logger.getLogger(EndecaProductsController.class);

    @Resource(name = "endecaHelper")
    private EndecaHelper endecaHelper;

    @Value("#{configurationService.configuration.getString('tgtstorefront.host.fe.fqdn')}")
    private String fqdn;

    /**
     * Method gets the products for Instagram and Catalogue products. This method accepts a set of product codes in a
     * json format which can be a base product code or a colour variant code and gives back the data.
     *
     * @param endecaProducts
     *            - json embeded in the body containing requestedCodes
     * @return endecaProducts
     */
    @RequestMapping(value = "/getInstagramAndCatalogueProducts", method = RequestMethod.POST, consumes = {
            MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public TgtEndecaProductsWsDTO getInstagramAndCatalogueProducts(
            @RequestBody final TgtEndecaProductsWsDTO endecaProducts) {

        LOG.info("EndecaController -  tgtpublicws - Getting Products from Endeca");
        endecaProducts.setProducts(endecaHelper.getProductsForInstagramAndCatalogue((endecaProducts.getProducts())));
        endecaProducts.setFqdn(fqdn);
        return endecaProducts;
    }

    @RequestMapping(value = "/getProductsForDeal/{dealId}", method = RequestMethod.GET)
    @Cacheable(value = "productSearchCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,#dealId)")
    @ResponseBody
    public TgtEndecaProductsWsDTO getProductsForDeal(@PathVariable final String dealId) {
        final TgtEndecaProductsWsDTO response = new TgtEndecaProductsWsDTO();
        response.setProducts(endecaHelper.getProductsForDeal(dealId));
        response.setFqdn(fqdn);

        return response;
    }
}

package au.com.target.tgtpublicws.v2.controller;


import de.hybris.platform.commercewebservicescommons.mapping.FieldSetLevelHelper;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.featureswitch.data.FeatureSwitchData;
import au.com.target.tgtpublicws.featureSwitch.dto.FeatureSwitchWsDTO;
import au.com.target.tgtpublicws.featureSwitch.dto.FeatureSwitchesWsDTO;


/**
 * This class is intended for enabling webservice operations on Feature Switches.
 */

@Controller
@RequestMapping(value = "/{baseSiteId}/features")
public class FeatureSwitchController extends BaseController {


    private static final Logger LOG = Logger.getLogger(FeatureSwitchController.class);

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;


    /**
     * Method gets the current status of all the feature switches in the system
     */
    @RequestMapping(method = RequestMethod.GET, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    @ResponseBody
    public FeatureSwitchesWsDTO getStatus() {

        LOG.info("Getting Feature Switch Status");
        final FeatureSwitchesWsDTO featureSwitchesWSDTO = new FeatureSwitchesWsDTO();
        final List<FeatureSwitchData> featureSwitchesData = targetFeatureSwitchFacade.getAllFeatures();
        final List<FeatureSwitchWsDTO> featureSwitches = new ArrayList<>();
        for (final FeatureSwitchData featureSwitchData : featureSwitchesData) {
            featureSwitches.add(dataMapper.map(featureSwitchData, FeatureSwitchWsDTO.class,
                    FieldSetLevelHelper.DEFAULT_LEVEL));

        }
        featureSwitchesWSDTO.setFeatureSwitches(featureSwitches);
        return featureSwitchesWSDTO;
    }

}

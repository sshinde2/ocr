/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 */
package au.com.target.tgtpublicws.v2.controller;

import javax.annotation.Resource;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.deals.TargetDealsFacade;
import au.com.target.tgtfacades.deals.data.TargetDealsData;
import au.com.target.tgtfacades.deals.data.TargetVouchersData;
import au.com.target.tgtfacades.voucher.TargetVoucherFacade;
import au.com.target.tgtpublicws.user.dto.deals.TargetDealsWsDTO;
import au.com.target.tgtpublicws.user.dto.voucher.TargetVouchersWsDTO;


/**
 * Main Controller for Offers This is for handling deals and vouchers interaction.
 *
 * 
 */
@Controller
@RequestMapping(value = "/{baseSiteId}/offers")
public class OffersController extends BaseController
{

    @Resource
    private TargetDealsFacade targetDealsFacade;

    @Resource
    private TargetVoucherFacade targetVoucherFacade;

    /**
     * Returns a list of all deals which are active for Mobile
     *
     * @return Deals for Mobile
     */
    @RequestMapping(value = "/mobile-deals", method = RequestMethod.GET)
    @Cacheable(value = "promotionCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'mobile-deals')")
    @ResponseBody
    public TargetDealsWsDTO getAllMobileDeals()
    {
        final TargetDealsData targetDealsData = targetDealsFacade
                .getAllMobileActiveDealsWithOfferHeadings();

        final TargetDealsWsDTO targetDealsWsDto = dataMapper.map(targetDealsData,
                TargetDealsWsDTO.class, DEFAULT_FIELD_SET);
        return targetDealsWsDto;
    }

    /**
     * Returns a list of all vouchers which are applicable to be shown in Mobile
     *
     * @return Vouchers for Mobile
     */
    @RequestMapping(value = "/mobile-vouchers", method = RequestMethod.GET)
    @Cacheable(value = "promotionCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'mobile-vouchers')")
    @ResponseBody
    public TargetVouchersWsDTO getAllMobileVouchers()
    {
        final TargetVouchersData targetVouchersData = targetVoucherFacade
                .getAllMobileActivePromotionVouchersWithOfferHeadings();
        final TargetVouchersWsDTO targetVouchersWsDto = dataMapper.map(targetVouchersData, TargetVouchersWsDTO.class,
                DEFAULT_FIELD_SET);
        return targetVouchersWsDto;
    }
}

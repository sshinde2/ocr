/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package au.com.target.tgtpublicws.v2.controller;

import de.hybris.platform.commercefacades.catalog.CatalogFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ReviewData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commercefacades.product.data.SuggestionData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.AutocompleteSuggestionData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.storefinder.StoreFinderStockFacade;
import de.hybris.platform.commercefacades.storefinder.data.StoreFinderStockSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commercewebservicescommons.cache.CacheControl;
import de.hybris.platform.commercewebservicescommons.cache.CacheControlDirective;
import de.hybris.platform.commercewebservicescommons.dto.product.ReviewListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.product.ReviewWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.product.StockWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.product.SuggestionListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.product.SuggestionWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.store.StoreFinderStockSearchPageWsDTO;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.RequestParameterException;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.StockSystemException;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.WebserviceValidationException;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.product.TargetProductListerFacade;
import au.com.target.tgtfacades.product.data.TargetPOSProductData;
import au.com.target.tgtfacades.product.data.TargetPOSProductData.ErrorType;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtpublicws.constants.YcommercewebservicesConstants;
import au.com.target.tgtpublicws.exceptions.ProductPriceCheckException;
import au.com.target.tgtpublicws.exceptions.UnknownResourceException;
import au.com.target.tgtpublicws.formatters.WsDateFormatter;
import au.com.target.tgtpublicws.product.dto.TargetPriceCheckWsDTO;
import au.com.target.tgtpublicws.product.dto.TargetProductOldWsDTO;
import au.com.target.tgtpublicws.product.dto.TargetProductPartialWsDTO;
import au.com.target.tgtpublicws.product.dto.TargetProductWsDTO;
import au.com.target.tgtpublicws.stock.CommerceStockFacade;
import au.com.target.tgtpublicws.v2.helper.ProductsHelper;
import au.com.target.tgtpublicws.validator.PointOfServiceValidator;


/**
 * Web Services Controller to expose the functionality of the
 * {@link de.hybris.platform.commercefacades.product.ProductFacade} and SearchFacade.
 *
 * @pathparam productCode Product identifier
 * @pathparam storeName Store identifier
 */
@Controller
@RequestMapping(value = "/{baseSiteId}/products")
public class ProductsController extends BaseController {
    private static final String BASIC_OPTION = "BASIC";
    private static final int CATALOG_ID_POS = 0;
    private static final int CATALOG_VERSION_POS = 1;
    private static final Logger LOG = Logger.getLogger(ProductsController.class);
    @Resource
    private StoreFinderStockFacade storeFinderStockFacade;
    @Resource(name = "targetProductFacade")
    private TargetProductFacade productFacade;
    @Resource(name = "targetProductListerFacade")
    private TargetProductListerFacade targetProductListerFacade;
    @Resource(name = "wsDateFormatter")
    private WsDateFormatter wsDateFormatter;
    @Resource(name = "productSearchFacade")
    private ProductSearchFacade<ProductData> productSearchFacade;
    @Resource(name = "solrSearchStateConverter")
    private Converter<SolrSearchQueryData, SearchStateData> solrSearchStateConverter;
    @Resource(name = "httpRequestReviewDataPopulator")
    private Populator<HttpServletRequest, ReviewData> httpRequestReviewDataPopulator;
    @Resource(name = "reviewValidator")
    private Validator reviewValidator;
    @Resource(name = "reviewDTOValidator")
    private Validator reviewDTOValidator;
    @Resource(name = "commerceStockFacade")
    private CommerceStockFacade commerceStockFacade;
    @Resource(name = "pointOfServiceValidator")
    private PointOfServiceValidator pointOfServiceValidator;
    @Resource(name = "catalogFacade")
    private CatalogFacade catalogFacade;
    @Value("#{configurationService.configuration.getString('tgtstorefront.host.fe.fqdn')}")
    private String fqdn;
    @Resource(name = "productsHelper")
    private ProductsHelper productsHelper;

    private static Set<ProductOption> extractOptions(final String options) {
        final String optionsStrings[] = options.split(YcommercewebservicesConstants.OPTIONS_SEPARATOR);

        final Set<ProductOption> opts = new HashSet<>();
        for (final String option : optionsStrings) {
            opts.add(ProductOption.valueOf(option));
        }
        return opts;
    }

    /**
     * Returns details of a single product specified by a product code.
     *
     * @queryparam fields Response configuration (list of fields, which should be returned in response)
     * @return Product details
     */
    @RequestMapping(value = "/{productCode}", method = RequestMethod.GET)
    @CacheControl(directive = CacheControlDirective.PRIVATE, maxAge = 120)
    @Cacheable(value = "productCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(true,true,#productCode,#fields)")
    @ResponseBody
    public TargetProductOldWsDTO getProductByCodeOld(@PathVariable final String productCode,
            @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getProductByCode: code=" + productCode);
        }

        final TargetProductListerData product = targetProductListerFacade
                .getBaseProductDataByCode(productCode, productOptions());

        final TargetProductOldWsDTO dto = dataMapper.map(product, TargetProductOldWsDTO.class, fields);

        if (dto == null) {
            throw new UnknownResourceException("No product found for code " + productCode);
        }

        productsHelper.populateAdditionalProductAttributes(product, dto);

        return dto;
    }


    /**
     * Returns details of a single product specified by a product code.
     *
     * @queryparam fields Response configuration (list of fields, which should be returned in response)
     * @return Product details
     */
    @RequestMapping(value = "/ver1/{productCode}", method = RequestMethod.GET)
    @CacheControl(directive = CacheControlDirective.PRIVATE, maxAge = 120)
    @Cacheable(value = "productCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(true,true,'ver1',#productCode,#fields)")
    @ResponseBody
    public TargetProductWsDTO getProductByCode(@PathVariable final String productCode,
            @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getProductByCode: code=" + productCode);
        }

        final TargetProductListerData product = targetProductListerFacade
                .getBaseProductDataByCode(productCode, productOptions());

        final TargetProductWsDTO dto = dataMapper.map(product, TargetProductWsDTO.class, fields);

        if (dto == null) {
            throw new UnknownResourceException("No product found for code " + productCode);
        }

        productsHelper.populateAdditionalProductAttributes(product, dto);

        return dto;
    }

    /**
     * All the product options available for mobile devices.
     * 
     * @return List<ProductOption>
     */
    private List<ProductOption> productOptions() {
        final List<ProductOption> options = new ArrayList<>();
        options.add(ProductOption.BASIC);
        options.add(ProductOption.VARIANT_FULL);
        options.add(ProductOption.BULKY_PRODUCT_DETAIL);
        options.add(ProductOption.DELIVERY_MODE_AVAILABILITY);
        options.add(ProductOption.DELIVERY_PROMOTIONAL_MESSAGE);
        return options;
    }

    /**
     * Returns partial product information for the supplied product code. {guid} is the cart GUID, used by the
     * {@link au.com.target.tgtpublicws.v2.filter.CartMatchingFilter} to load the cart so that the correct deal messages
     * can be calculated.
     * 
     * This is intended to retrieve product information that cannot be cached, because it is dynamic or user specific.
     * 
     * This is different to {@link #getProductByCode(String, String)} in that only information about the supplied
     * product code is retrieved, not information about all base product, sibling variants and child variants.
     * 
     * @queryparam fields Response configuration (list of fields, which should be returned in response)
     * @return Product promotion details
     */
    @RequestMapping(value = { "/{productCode}/partial/{guid}", "/{productCode}/partial" }, method = RequestMethod.GET)
    @ResponseBody
    public TargetProductPartialWsDTO getProductPartialDetailsByProductCode(@PathVariable final String productCode,
            @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getProductPartialDetailsByProductCode: code=" + productCode);
        }

        TargetProductData productData = null;

        try {
            productData = productFacade.getProductForCodeAndOptionsWithDealMessage(
                    productCode,
                    Arrays.asList(ProductOption.PROMOTIONS));
        }
        catch (final UnknownIdentifierException ex) {
            final String message = "No product found for code " + productCode;
            LOG.info(message);
            throw new UnknownResourceException(message);
        }
        catch (final AmbiguousIdentifierException ex) {
            final String message = "Too many products found for code " + productCode;
            LOG.warn(message);
            throw new UnknownResourceException(message);
        }

        final TargetProductPartialWsDTO dto = dataMapper.map(productData,
                TargetProductPartialWsDTO.class, fields);

        productsHelper.populateAdditionalProductPartialAttributes(productData, dto);

        return dto;
    }

    /**
     * Returns price and basic product information from the specified store POS, and enriches it with data from hybris,
     * if available.
     * 
     * @queryparam barcode The barcode of the product
     * @queryparam storeNumber The number of the store to retrieve the price from
     * @queryparam fields Response configuration (list of fields, which should be returned in response) (only supports
     *             DEFAULT)
     * @return Price and product data
     */
    @RequestMapping(value = { "/{barcode}/price/{storeNumber}" }, method = RequestMethod.GET)
    @ResponseBody
    public TargetPriceCheckWsDTO getProductPriceByProductCodeAndStore(@PathVariable final String barcode,
            @PathVariable final String storeNumber,
            @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getProductPriceByProductCodeAndStore: barcode=" + barcode + ", storeNumber=" + storeNumber);
        }

        final TargetPOSProductData posProduct = productFacade.getPOSProductDetails(storeNumber, barcode);

        if (ErrorType.NONE.equals(posProduct.getErrorType())) {
            final TargetPriceCheckWsDTO dto = dataMapper.map(posProduct, TargetPriceCheckWsDTO.class, fields);

            // If possible get product from hybris which may be available to enrich display with review and image data
            if (StringUtils.isNotEmpty(posProduct.getItemCode())) {
                try {
                    final TargetProductData productData = (TargetProductData)productFacade.getProductForCodeAndOptions(
                            posProduct.getItemCode(),
                            Arrays.asList(ProductOption.BASIC, ProductOption.GALLERY, ProductOption.REVIEW,
                                    ProductOption.STOCK, ProductOption.DESCRIPTION));

                    dataMapper.map(productData, dto, fields);
                    dto.setFqdn(fqdn);
                }
                catch (final UnknownIdentifierException e) {
                    LOG.info("Could not load hybris product model from POS data: itemcode=" + posProduct.getItemCode()
                            + ", barcode=" + barcode + ", storeNumber=" + storeNumber);
                }
            }

            dto.setStoreNumber(storeNumber);

            return dto;
        }

        if (ErrorType.POS.equals(posProduct.getErrorType())) {
            if ("Itemcode not found.".equalsIgnoreCase(posProduct.getFailureReason())) {
                final String message = "No product found for barcode=" + barcode + ", storeNumber=" + storeNumber;
                LOG.info(message);
                throw new UnknownResourceException(message);
            }
        }

        throw new ProductPriceCheckException(posProduct.getErrorType().toString(), posProduct.getFailureReason(),
                barcode, storeNumber);
    }

    /**
     * Method returns product's stock level for a particular store (POS).
     *
     * @queryparam fields Response configuration (list of fields, which should be returned in response)
     * @return Stock level information for product in store
     * @throws WebserviceValidationException
     *             If store doesn't exist
     * @throws StockSystemException
     *             When stock system is not enabled on this site
     */
    @RequestMapping(value = "/{productCode}/stock/{storeName}", method = RequestMethod.GET)
    @ResponseBody
    public StockWsDTO getStockData(@PathVariable final String baseSiteId, @PathVariable final String productCode,
            @PathVariable final String storeName, @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields)
                    throws WebserviceValidationException, StockSystemException {
        validate(storeName, "storeName", pointOfServiceValidator);
        if (!commerceStockFacade.isStockSystemEnabled(baseSiteId)) {
            throw new StockSystemException("Stock system is not enabled on this site",
                    StockSystemException.NOT_ENABLED, baseSiteId);
        }
        final StockData stockData = commerceStockFacade.getStockDataForProductAndPointOfService(productCode, storeName);
        final StockWsDTO dto = dataMapper.map(stockData, StockWsDTO.class, fields);
        return dto;
    }

    /**
     * Method returns product's stock levels sorted by distance from specific location passed by free-text parameter or
     * longitude and latitude parameters.<br>
     * There are two set of parameters available :
     * <ul>
     * <li>location (Required), currentPage (Optional), pageSize(Optional) or</li>>
     * <li>longitude (Required), latitude (Required), currentPage (Optional), pageSize(Optional).</li>
     * </ul>
     *
     * @queryparam location Free-text location
     * @queryparam latitude Longitude location parameter.
     * @queryparam longitude Latitude location parameter.
     * @queryparam currentPage The current result page requested.
     * @queryparam pageSize The number of results returned per page.
     * @queryparam fields Response configuration (list of fields, which should be returned in response)
     * @return product's stock levels
     */
    @RequestMapping(value = "/{productCode}/stock", method = RequestMethod.GET)
    @ResponseBody
    public StoreFinderStockSearchPageWsDTO searchProductStockByLocation(@PathVariable final String productCode,
            @RequestParam(required = false) final String location,
            @RequestParam(required = false) final Double latitude,
            @RequestParam(required = false) final Double longitude,
            @RequestParam(required = false, defaultValue = DEFAULT_CURRENT_PAGE) final int currentPage,
            @RequestParam(required = false, defaultValue = DEFAULT_PAGE_SIZE) final int pageSize,
            @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields, final HttpServletResponse response) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getProductStockByLocation: code=" + productCode + " | location=" + location + " | latitude="
                    + latitude
                    + " | longitude=" + longitude);
        }

        final StoreFinderStockSearchPageData result = doSearchProductStockByLocation(productCode, location, latitude,
                longitude,
                currentPage, pageSize);

        // X-Total-Count header
        setTotalCountHeader(response, result.getPagination().getTotalNumberOfResults());

        return dataMapper.map(result, StoreFinderStockSearchPageWsDTO.class, fields);
    }

    /**
     * Returns {@value BaseController#HEADER_TOTAL_COUNT} header with total number of product's stock levels. It doesn't
     * return HTTP body.<br/>
     * There are two set of parameters available :
     * <ul>
     * <li>location (Required) or</li>
     * <li>longitude (Required), latitude (Required).</li>
     * </ul>
     *
     * @queryparam location Free-text location
     * @queryparam latitude Longitude location parameter.
     * @queryparam longitude Latitude location parameter.
     */
    @RequestMapping(value = "/{productCode}/stock", method = RequestMethod.HEAD)
    public void countSearchProductStockByLocation(@PathVariable final String productCode,
            @RequestParam(required = false) final String location,
            @RequestParam(required = false) final Double latitude,
            @RequestParam(required = false) final Double longitude, final HttpServletResponse response) {
        final StoreFinderStockSearchPageData result = doSearchProductStockByLocation(productCode, location, latitude,
                longitude, 0,
                1);

        setTotalCountHeader(response, result.getPagination().getTotalNumberOfResults());
    }

    protected StoreFinderStockSearchPageData doSearchProductStockByLocation(final String productCode,
            final String location,
            final Double latitude, final Double longitude, final int currentPage, final int pageSize) {
        final Set<ProductOption> opts = extractOptions(BASIC_OPTION);
        final StoreFinderStockSearchPageData result;
        if (latitude != null && longitude != null) {
            result = storeFinderStockFacade.productSearch(createGeoPoint(latitude, longitude),
                    productFacade.getProductForCodeAndOptions(productCode, opts),
                    createPageableData(currentPage, pageSize, null));
        }
        else if (location != null) {
            result = storeFinderStockFacade.productSearch(location,
                    productFacade.getProductForCodeAndOptions(productCode, opts),
                    createPageableData(currentPage, pageSize, null));
        }
        else {
            throw new RequestParameterException("You need to provide location or longitute and latitute parameters",
                    RequestParameterException.MISSING, "location or longitute and latitute");
        }
        return result;
    }

    /**
     * Method returns the reviews for a product with the given product code.
     *
     * @return product's review list
     */
    @RequestMapping(value = "/{productCode}/reviews", method = RequestMethod.GET)
    @ResponseBody
    public ReviewListWsDTO getProductReviews(@PathVariable final String productCode,
            @RequestParam(required = false) final Integer maxCount,
            @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields) {
        final ReviewListWsDTO reviewListWsDTO = new ReviewListWsDTO();
        final List<ReviewData> reviewDataList = productFacade.getReviews(productCode, maxCount);
        reviewListWsDTO.setReviews(dataMapper.mapAsList(reviewDataList, ReviewWsDTO.class, fields));
        return reviewListWsDTO;
    }

    /**
     * Method creates a new customer review as an anonymous user.
     *
     * @formparam rating This parameter is required. Value needs to be between 1 and 5.
     * @formparam alias
     * @formparam headline This parameter is required.
     * @formparam comment This parameter is required.
     * @queryparam fields Response configuration (list of fields, which should be returned in response)
     * @return Created review
     * @throws WebserviceValidationException
     *             When given parameters are incorrect
     */
    @RequestMapping(value = "/{productCode}/reviews", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public ReviewWsDTO createReview(@PathVariable final String productCode,
            @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields, final HttpServletRequest request)
                    throws WebserviceValidationException {
        final ReviewData reviewData = new ReviewData();
        httpRequestReviewDataPopulator.populate(request, reviewData);
        validate(reviewData, "reviewData", reviewValidator);
        final ReviewData reviewDataRet = productFacade.postReview(productCode, reviewData);
        final ReviewWsDTO dto = dataMapper.map(reviewDataRet, ReviewWsDTO.class, fields);
        return dto;
    }

    /**
     * Method creates a new customer review as an anonymous user.
     *
     * @param review
     *            Object contains review details like : rating, alias, headline, comment
     * @queryparam fields Response configuration (list of fields, which should be returned in response)
     * @bodyparams headline,alias,rating,date,comment
     * @return Created review
     * @throws WebserviceValidationException
     *             When given parameters are incorrect
     */
    @RequestMapping(value = "/{productCode}/reviews", method = RequestMethod.POST, consumes = {
            MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public ReviewWsDTO createReview(@PathVariable final String productCode, @RequestBody final ReviewWsDTO review,
            @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields) throws WebserviceValidationException {
        validate(review, "review", reviewDTOValidator);
        final ReviewData reviewData = dataMapper.map(review, ReviewData.class, "alias,rating,headline,comment");
        final ReviewData reviewDataRet = productFacade.postReview(productCode, reviewData);
        final ReviewWsDTO dto = dataMapper.map(reviewDataRet, ReviewWsDTO.class, fields);
        return dto;
    }


    private PageableData createPageableData(final int currentPage, final int pageSize, final String sort) {
        final PageableData pageable = new PageableData();

        pageable.setCurrentPage(currentPage);
        pageable.setPageSize(pageSize);
        pageable.setSort(sort);
        return pageable;
    }

    private GeoPoint createGeoPoint(final Double latitude, final Double longitude) {
        final GeoPoint point = new GeoPoint();
        point.setLatitude(latitude.doubleValue());
        point.setLongitude(longitude.doubleValue());

        return point;
    }

    /**
     * Method returns list of all available suggestions related to the given term and limits the results to a given
     * value of the max parameter.
     *
     * @queryparam term Specified term
     * @queryparam max Specifies the limit of results.
     * @queryparam fields Response configuration (list of fields, which should be returned in response)
     * @return List of auto suggestions
     */
    @RequestMapping(value = "/suggestions", method = RequestMethod.GET)
    @ResponseBody
    public SuggestionListWsDTO getSuggestions(@RequestParam(required = true, defaultValue = " ") final String term,
            @RequestParam(required = true, defaultValue = "10") final int max,
            @RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields) {
        final List<SuggestionData> suggestions = new ArrayList<>();
        final SuggestionListWsDTO suggestionListWsDTO = new SuggestionListWsDTO();

        List<AutocompleteSuggestionData> autoSuggestions = productSearchFacade.getAutocompleteSuggestions(term);
        if (max < autoSuggestions.size()) {
            autoSuggestions = autoSuggestions.subList(0, max);
        }

        for (final AutocompleteSuggestionData autoSuggestion : autoSuggestions) {
            final SuggestionData suggestionData = new SuggestionData();
            suggestionData.setValue(autoSuggestion.getTerm());
            suggestions.add(suggestionData);
        }

        suggestionListWsDTO.setSuggestions(dataMapper.mapAsList(suggestions, SuggestionWsDTO.class, fields));
        return suggestionListWsDTO;
    }

    protected List<String> validateAndSplitCatalog(final String catalog) throws RequestParameterException {
        final List<String> catalogInfo = new ArrayList<>();
        if (StringUtils.isNotEmpty(catalog)) {
            catalogInfo.addAll(Lists.newArrayList(Splitter.on(':').trimResults().omitEmptyStrings().split(catalog)));
            if (catalogInfo.size() == 2) {
                catalogFacade.getProductCatalogVersionForTheCurrentSite(catalogInfo.get(CATALOG_ID_POS),
                        catalogInfo.get(CATALOG_VERSION_POS), Collections.EMPTY_SET);
            }
            else if (!catalogInfo.isEmpty()) {
                throw new RequestParameterException(
                        "Invalid format. You have to provide catalog as 'catalogId:catalogVersion'",
                        RequestParameterException.INVALID, "catalog");
            }
        }
        return catalogInfo;
    }

}

/**
 * 
 */
package au.com.target.tgtpublicws.v2.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.stock.TargetStockLookUpFacade;
import au.com.target.tgtpublicws.stock.dto.StockDataListWsDTO;
import au.com.target.tgtpublicws.stock.dto.StockDataWsDTO;
import au.com.target.tgtwebcore.fluent.StockRequestDto;


/**
 * @author bmcmuffin
 *
 */
@Controller
@RequestMapping(value = "/{baseSiteId}")
public class StockController extends BaseController {

    private static final Logger LOG = Logger.getLogger(BaseCommerceController.class);

    @Autowired
    private TargetStockLookUpFacade targetStockLookUpFacade;

    @RequestMapping(value = "/stock", method = RequestMethod.POST)
    @ResponseBody
    public StockDataListWsDTO getStockData(final StockRequestDto request) {

        final List<StockDataWsDTO> stockDataItems = new ArrayList<>();
        try {
            final Map<String, Boolean> stockMap = targetStockLookUpFacade.lookupStockOnline(request.getVariantCode());
            for (final Entry<String, Boolean> stockItem : stockMap.entrySet()) {
                final StockDataWsDTO stockData = new StockDataWsDTO();
                stockData.setAts(stockItem.getValue());
                stockData.setVariantCode(stockItem.getKey());
                stockDataItems.add(stockData);
            }
        }
        catch (final Exception e) {
            LOG.error("STOCK LOOKUP: No stock data", e);
        }

        final StockDataListWsDTO stockDataList = new StockDataListWsDTO();
        stockDataList.setStockData(stockDataItems);
        return stockDataList;
    }


}

package au.com.target.tgtpublicws.v2.controller;


import de.hybris.platform.commercewebservicescommons.cache.CacheControl;
import de.hybris.platform.commercewebservicescommons.cache.CacheControlDirective;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.RequestParameterException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServicesData;
import au.com.target.tgtpublicws.user.dto.store.TargetPointOfServicesWsDTO;


/**
 * 
 */

@Controller
@CacheControl(directive = CacheControlDirective.PUBLIC, maxAge = 1800)
public class StoresController extends BaseController
{


    private static final Logger LOG = Logger.getLogger(StoresController.class);

    @Resource
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    /**
     * Returns {@value BaseController#HEADER_TOTAL_COUNT} header with the number of all store locations that are near
     * the location specified in a query or by latitude and longitude.
     * 
     */
    @RequestMapping(value = "/{baseSiteId}/stores", method = RequestMethod.GET)
    @Cacheable(value = "storeCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'targetPointOfServices')")
    @ResponseBody
    public TargetPointOfServicesWsDTO getAllStores() throws RequestParameterException
    {
        if (LOG.isDebugEnabled())
        {
            LOG.debug("tgtpublicws.storescontroller.getAllStores invoked");
        }
        final Map<String, List<TargetPointOfServiceData>> targetPointOfServices = targetStoreLocatorFacade
                .getAllStateAndStores();

        final TargetPointOfServicesData targetPointOfServicesData = new TargetPointOfServicesData();

        targetPointOfServicesData.setTargetPointOfServices(new ArrayList<TargetPointOfServiceData>());
        if (null == targetPointOfServices || CollectionUtils.isEmpty(targetPointOfServices.values())) {
            return null;
        }
        final Collection<List<TargetPointOfServiceData>> targetPointOfServiceStores = targetPointOfServices.values();
        for (final List<TargetPointOfServiceData> targetPointOfServiceList : targetPointOfServiceStores) {
            targetPointOfServicesData.getTargetPointOfServices().addAll(targetPointOfServiceList);
        }
        final TargetPointOfServicesWsDTO targetPointOfServicesWsDTO = dataMapper.map(targetPointOfServicesData,
                TargetPointOfServicesWsDTO.class, DEFAULT_FIELD_SET);

        return targetPointOfServicesWsDTO;
    }

    /**
     * @param targetStoreLocatorFacade
     *            the targetStoreLocatorFacade to set
     */
    public void setTargetStoreLocatorFacade(final TargetStoreLocatorFacade targetStoreLocatorFacade) {
        this.targetStoreLocatorFacade = targetStoreLocatorFacade;
    }
}

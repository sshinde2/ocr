package au.com.target.tgtpublicws.v2.filter;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.filter.OncePerRequestFilter;


/**
 * Abstract matching filter that helps parsing urls.
 */
public abstract class AbstractUrlMatchingFilter extends OncePerRequestFilter
{
    protected boolean matchesUrl(final HttpServletRequest request, final List<String> regexpList) {
        for (final String regexp : regexpList) {
            if (matchesUrl(request, regexp)) {
                return true;
            }
        }

        return false;
    }

    protected boolean matchesUrl(final HttpServletRequest request, final String regexp)
    {
        final Matcher matcher = getMatcher(request, regexp);
        if (matcher.find())
        {
            return true;
        }
        return false;
    }

    protected String getValue(final HttpServletRequest request, final List<String> regexpList) {
        String value = null;

        for (final String regexp : regexpList) {
            value = getValue(request, regexp);

            if (value != null) {
                return value;
            }
        }

        return value;
    }

    protected String getValue(final HttpServletRequest request, final String regexp)
    {
        final Matcher matcher = getMatcher(request, regexp);
        if (matcher.find())
        {
            return matcher.group(1);
        }
        return null;
    }

    protected String getValue(final HttpServletRequest request, final String regexp, final String groupName)
    {
        final Matcher matcher = getMatcher(request, regexp);
        if (matcher.find())
        {
            return matcher.group(groupName);
        }
        return null;
    }

    private Matcher getMatcher(final HttpServletRequest request, final String regexp)
    {
        final Pattern pattern = Pattern.compile(regexp);
        final String path = request.getPathInfo() != null ? request.getPathInfo() : "";
        return pattern.matcher(path);
    }
}

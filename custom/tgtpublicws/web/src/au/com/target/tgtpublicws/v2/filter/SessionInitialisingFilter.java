/**
 * 
 */
package au.com.target.tgtpublicws.v2.filter;

import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;

import java.io.IOException;
import java.util.Collections;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.filter.OncePerRequestFilter;


/**
 * @author rmcalave
 *
 */
public class SessionInitialisingFilter extends OncePerRequestFilter {

    private StoreSessionFacade storeSessionFacade;

    /* (non-Javadoc)
     * @see org.springframework.web.filter.OncePerRequestFilter#doFilterInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, javax.servlet.FilterChain)
     */
    @Override
    protected void doFilterInternal(final HttpServletRequest request,
            final HttpServletResponse response, final FilterChain filterChain)
                    throws ServletException, IOException {
        storeSessionFacade.initializeSession(Collections.list(request.getLocales()));
        filterChain.doFilter(request, response);
    }

    @Required
    public void setStoreSessionFacade(final StoreSessionFacade storeSessionFacade) {
        this.storeSessionFacade = storeSessionFacade;
    }

}

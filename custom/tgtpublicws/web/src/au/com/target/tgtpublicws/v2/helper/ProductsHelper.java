
package au.com.target.tgtpublicws.v2.helper;

import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercewebservicescommons.dto.product.ImageWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.product.PromotionWsDTO;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtpublicws.product.dto.TargetColourVariantWsDTO;
import au.com.target.tgtpublicws.product.dto.TargetProductOldWsDTO;
import au.com.target.tgtpublicws.product.dto.TargetProductPartialWsDTO;
import au.com.target.tgtpublicws.product.dto.TargetProductWsDTO;
import au.com.target.tgtpublicws.product.dto.TargetVariantProductWsDTO;


@Component
public class ProductsHelper extends AbstractHelper {

    @Resource(name = "messageSource")
    private MessageSource messageSource;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Resource(name = "i18nService")
    private I18NService i18nService;

    @Value("#{configurationService.configuration.getString('tgtstorefront.host.fe.fqdn')}")
    private String fqdn;


    /**
     * This method will populate additional attributes and change to required format. This method accepts
     * TargetProductOldWsDTO which will be removed in a while after the change has been made to the newer version of the
     * end point. Please refrain from using this till that time.
     * 
     * @param source
     * @param target
     */
    //This method will be removed once the new product endpoint becomes used.
    public void populateAdditionalProductAttributes(final TargetProductListerData source,
            final TargetProductOldWsDTO target) {
        target.setFqdn(fqdn);

        for (final TargetColourVariantWsDTO colourVariant : target.getColourVariants()) {
            if (CollectionUtils.isEmpty(colourVariant.getImages())) {
                colourVariant.setImages(createPlaceholderGallery(colourVariant.getDisplayName()));
            }
        }
        setDeliveryFeeNotice(source, target);
    }


    /**
     * This method will populate additional attributes for the get Product end point.
     * 
     * @param source
     * @param target
     */
    public void populateAdditionalProductAttributes(final TargetProductListerData source,
            final TargetProductWsDTO target) {
        target.setFqdn(fqdn);

        for (final TargetVariantProductWsDTO variant : target.getVariants()) {
            if (TargetColourVariantProductModel._TYPECODE.equals(variant.getItemType())
                    && CollectionUtils.isEmpty(variant.getImages())) {
                variant.setImages(createPlaceholderGallery(variant.getDisplayName()));
            }
        }

        setDeliveryFeeNotice(source, target);
    }

    /**
     * THis method populates additional attributes in the case of a partial product request.
     * 
     * @param source
     * @param target
     */
    public void populateAdditionalProductPartialAttributes(final TargetProductData source,
            final TargetProductPartialWsDTO target) {
        if (CollectionUtils.isEmpty(target.getPromotions()) && StringUtils.isNotBlank(source.getDealDescription())) {
            final PromotionWsDTO promotionDto = new PromotionWsDTO();
            promotionDto.setDescription(source.getDealDescription());

            final List<PromotionWsDTO> promotions = new ArrayList<>();
            promotions.add(promotionDto);

            target.setPromotions(promotions);
        }
    }

    private List<ImageWsDTO> createPlaceholderGallery(final String displayName) {
        final List<ImageWsDTO> placeholderGallery = new ArrayList<>();
        Collections.addAll(placeholderGallery, createPlaceholderImage(displayName, "zoom"),
                createPlaceholderImage(displayName, "product"), createPlaceholderImage(displayName, "thumbnail"));

        return placeholderGallery;
    }

    private ImageWsDTO createPlaceholderImage(final String displayName, final String format) {
        final ImageWsDTO placeholderImage = new ImageWsDTO();
        placeholderImage.setAltText(displayName);
        placeholderImage.setFormat(format);
        placeholderImage.setGalleryIndex(Integer.valueOf(0));
        placeholderImage.setImageType(ImageDataType.GALLERY);
        placeholderImage.setUrl(configurationService.getConfiguration()
                .getString("tgtpublicws.missingProductImage." + format));

        return placeholderImage;
    }

    private void setDeliveryFeeNotice(final TargetProductListerData source,
            final TargetProductWsDTO target) {
        if (!source.isProductDeliveryToStoreOnly() && source.isBigAndBulky()) {
            if (source.getBulkyHomeDeliveryFee() != null && source.getBulkyCncFreeThreshold() == null) {
                if (!source.isAllowDeliverToStore()) {
                    final PriceData bulkyHomeDeliveryFee = source.getBulkyHomeDeliveryFee();
                    if (BigDecimal.ZERO.compareTo(bulkyHomeDeliveryFee.getValue()) < 0) {
                        final String largeItemMessage = messageSource.getMessage("product.largeItem", null,
                                i18nService.getCurrentLocale());
                        target.setDeliveryFeeNotice(largeItemMessage);
                    }
                }
            }
        }
        if (source.isGiftCard() && TgtCoreConstants.PHYSICAL_GIFTCARD.equals(source.getProductTypeCode())) {
            final Object[] giftCardFeeList = { source.getGiftCardDeliveryFee() };
            final String physicalGiftCardMessage = messageSource.getMessage("product.physicalGiftCard", giftCardFeeList,
                    i18nService.getCurrentLocale());
            target.setDeliveryFeeNotice(physicalGiftCardMessage);
        }
    }
}

/**
 * 
 */
package au.com.target.tgtpublicws.controller;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercewebservicescommons.mapping.DataMapper;
import de.hybris.platform.commercewebservicescommons.mapping.FieldSetLevelHelper;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.product.TargetProductListerFacade;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtpublicws.product.dto.TargetProductOldWsDTO;
import au.com.target.tgtpublicws.product.dto.TargetProductWsDTO;
import au.com.target.tgtpublicws.v2.controller.ProductsController;
import au.com.target.tgtpublicws.v2.helper.ProductsHelper;


/**
 * Mocking ProductsContorller for mobile devices.
 * 
 * @author bbaral1
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductsControllerTest {

    private static final String PRODUCT_CODE = "P1000";
    private static final String PRODUCT_CODE_1 = "P1001";

    @InjectMocks
    @Spy
    private final ProductsController productsController = new ProductsController();

    @Mock
    private TargetProductListerFacade targetProductListerFacade;

    @Mock
    private TargetProductListerData targetProductListerData;

    @Mock
    private DataMapper dataMapper;

    @Mock
    private final TargetProductModel targetProductModel = new TargetProductModel();

    @Mock
    private ProductsHelper productsHelper;

    @Test
    public void testProductByCode() {

        final TargetProductWsDTO targetProductWsDTO = new TargetProductWsDTO();
        final List<ProductOption> productOoptions = populateProductOptions();

        given(targetProductListerFacade.getBaseProductDataByCode(PRODUCT_CODE, productOoptions))
                .willReturn(targetProductListerData);
        given(dataMapper.map(targetProductListerData, TargetProductWsDTO.class, "DEFAULT"))
                .willReturn(targetProductWsDTO);

        final TargetProductWsDTO targetProductWsDTOs = productsController.getProductByCode(PRODUCT_CODE,
                FieldSetLevelHelper.DEFAULT_LEVEL);
        doNothing().when(productsHelper).populateAdditionalProductAttributes(targetProductListerData,
                targetProductWsDTOs);

        verify(targetProductListerFacade).getBaseProductDataByCode(PRODUCT_CODE, productOoptions);
        verify(dataMapper).map(targetProductListerData, TargetProductWsDTO.class, "DEFAULT");

        assertThat(targetProductWsDTOs).isEqualTo(targetProductWsDTO);
    }


    @Test
    public void testProductByCodeOld() {
        final TargetProductOldWsDTO targetProductOldWsDTO = new TargetProductOldWsDTO();

        final List<ProductOption> productOoptions = populateProductOptions();
        given(targetProductListerFacade.getBaseProductDataByCode(PRODUCT_CODE_1, productOoptions))
                .willReturn(targetProductListerData);

        doNothing().when(productsHelper).populateAdditionalProductAttributes(targetProductListerData,
                targetProductOldWsDTO);
        given(dataMapper.map(targetProductListerData, TargetProductOldWsDTO.class, "DEFAULT"))
                .willReturn(targetProductOldWsDTO);

        final TargetProductWsDTO targetProductWsDTOs = productsController.getProductByCodeOld(PRODUCT_CODE_1,
                FieldSetLevelHelper.DEFAULT_LEVEL);

        verify(targetProductListerFacade).getBaseProductDataByCode(PRODUCT_CODE_1, productOoptions);
        verify(dataMapper).map(targetProductListerData, TargetProductOldWsDTO.class, "DEFAULT");

        assertThat(targetProductWsDTOs).isEqualTo(targetProductOldWsDTO);
    }

    /**
     * This method populates product options to be used to get lister product data.
     * 
     * @return List<ProductOption>
     */
    private List<ProductOption> populateProductOptions() {
        final List<ProductOption> productOoptions = new ArrayList<ProductOption>();
        productOoptions.add(ProductOption.BASIC);
        productOoptions.add(ProductOption.VARIANT_FULL);
        productOoptions.add(ProductOption.BULKY_PRODUCT_DETAIL);
        productOoptions.add(ProductOption.DELIVERY_MODE_AVAILABILITY);
        productOoptions.add(ProductOption.DELIVERY_PROMOTIONAL_MESSAGE);
        return productOoptions;
    }


}

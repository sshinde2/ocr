package au.com.target.tgtpublicws.giftcards.converters;

import org.fest.assertions.Assertions;
import org.junit.Test;

import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;
import au.com.target.tgtpublicws.delivery.dto.order.TargetGiftCardOrderEntryWsDTO;


public class TargetGiftCardEntryWSDTOtoGiftRecipientDTOConverterTest {

    private static final String FIRST_NAME = "Hello";
    private static final String EMAIL = "hello@world.com";
    private static final String LAST_NAME = "World";
    private final TargetGiftCardEntryWSDTOtoGiftRecipientDTOConverter converter = new TargetGiftCardEntryWSDTOtoGiftRecipientDTOConverter();

    @Test
    public void testConvert() throws Exception {
        final TargetGiftCardOrderEntryWsDTO wsDto = new TargetGiftCardOrderEntryWsDTO();
        wsDto.setFirstName(FIRST_NAME);
        wsDto.setLastName(LAST_NAME);
        wsDto.setRecipientEmailAddress(EMAIL);
        final GiftRecipientDTO dto = converter.convert(wsDto);
        Assertions.assertThat(dto.getFirstName()).isEqualTo(FIRST_NAME);
        Assertions.assertThat(dto.getLastName()).isEqualTo(LAST_NAME);
        Assertions.assertThat(dto.getRecipientEmailAddress()).isEqualTo(EMAIL);
    }

}

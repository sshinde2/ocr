package au.com.target.tgtpublicws.mapping.converters;

import static org.fest.assertions.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;


public class StockLevelMapConverterTest {

    private final StockLevelMapConverter stockLevelMapConverter = new StockLevelMapConverter();

    @Test
    public void testConvertWithEmptyMap() {
        final Map<String, Boolean> stockLevelMap = new HashMap<String, Boolean>();

        final Boolean result = stockLevelMapConverter.convert(stockLevelMap, null);

        assertThat(result).isFalse();
    }

    @Test
    public void testConvertWithOutOfStock() {
        final Map<String, Boolean> stockLevelMap = new HashMap<String, Boolean>();
        stockLevelMap.put("buynow", Boolean.FALSE);

        final Boolean result = stockLevelMapConverter.convert(stockLevelMap, null);

        assertThat(result).isFalse();
    }

    @Test
    public void testConvertWithInStock() {
        final Map<String, Boolean> stockLevelMap = new HashMap<String, Boolean>();
        stockLevelMap.put("buynow", Boolean.TRUE);

        final Boolean result = stockLevelMapConverter.convert(stockLevelMap, null);

        assertThat(result).isTrue();
    }

    @Test
    public void testConvertWithBothInStockAndOutOfStock() {
        final Map<String, Boolean> stockLevelMap = new HashMap<String, Boolean>();
        stockLevelMap.put("buynow", Boolean.TRUE);
        stockLevelMap.put("layby", Boolean.FALSE);

        final Boolean result = stockLevelMapConverter.convert(stockLevelMap, null);

        assertThat(result).isTrue();
    }
}

package au.com.target.tgtpublicws.navigation;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtpublicws.navigation.dto.HomeScreenLayoutDTO;
import au.com.target.tgtpublicws.navigation.dto.HomeScreenTileDTO;
import au.com.target.tgtwebcore.enums.MobileHomeScreenImageLayoutEnum;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class HomeScreenLayoutBuilderTest {

    @Mock
    private Converter<CategoryModel, CategoryData> mockCategoryConverter;

    @Mock
    private UrlResolver<CategoryData> mockCategoryDataUrlResolver;

    @InjectMocks
    private final HomeScreenLayoutBuilder homeScreenLayoutBuilder = new HomeScreenLayoutBuilder();

    @Mock
    private CMSNavigationNodeModel mockRootNavNode;

    @Before
    public void setUp() {
        doReturn(Boolean.TRUE).when(mockRootNavNode).isVisible();
    }

    @Test
    public void testBuildHomeScreenLayoutWithNullNavNode() {
        final HomeScreenLayoutDTO result = homeScreenLayoutBuilder.buildHomeScreenLayout(null);

        assertThat(result).isNull();
    }

    @Test
    public void testBuildHomeScreenLayoutWithNotVisibleNavNode() {
        doReturn(Boolean.FALSE).when(mockRootNavNode).isVisible();

        final HomeScreenLayoutDTO result = homeScreenLayoutBuilder.buildHomeScreenLayout(mockRootNavNode);

        assertThat(result).isNull();
    }

    @Test
    public void testBuildHomeScreenLayoutWithNoChildren() {
        final HomeScreenLayoutDTO result = homeScreenLayoutBuilder.buildHomeScreenLayout(mockRootNavNode);

        assertThat(result).isNotNull();
        assertThat(result.getTiles()).isNotNull().isEmpty();
    }

    @Test
    public void testBuildHomeScreenLayoutWithOneNullChild() {
        final CMSNavigationNodeModel mockNavNode = mock(CMSNavigationNodeModel.class);
        doReturn(Boolean.FALSE).when(mockNavNode).isVisible();

        final List<CMSNavigationNodeModel> childNodes = new ArrayList<>();
        childNodes.add(null);
        given(mockRootNavNode.getChildren()).willReturn(childNodes);

        final HomeScreenLayoutDTO result = homeScreenLayoutBuilder.buildHomeScreenLayout(mockRootNavNode);

        assertThat(result).isNotNull();
        assertThat(result.getTiles()).isNotNull().isEmpty();
    }

    @Test
    public void testBuildHomeScreenLayoutWithOneNotVisibleChild() {
        final CMSNavigationNodeModel mockNavNode = mock(CMSNavigationNodeModel.class);
        doReturn(Boolean.FALSE).when(mockNavNode).isVisible();

        given(mockRootNavNode.getChildren()).willReturn(Collections.singletonList(mockNavNode));

        final HomeScreenLayoutDTO result = homeScreenLayoutBuilder.buildHomeScreenLayout(mockRootNavNode);

        assertThat(result).isNotNull();
        assertThat(result.getTiles()).isNotNull().isEmpty();
    }

    @Test
    public void testBuildHomeScreenLayoutWithOneChildNoEntries() {
        final CMSNavigationNodeModel mockNavNode = mock(CMSNavigationNodeModel.class);
        doReturn(Boolean.TRUE).when(mockNavNode).isVisible();

        given(mockRootNavNode.getChildren()).willReturn(Collections.singletonList(mockNavNode));

        final HomeScreenLayoutDTO result = homeScreenLayoutBuilder.buildHomeScreenLayout(mockRootNavNode);

        assertThat(result).isNotNull();
        assertThat(result.getTiles()).hasSize(1);

        final HomeScreenTileDTO tile = result.getTiles().get(0);
        assertThat(tile.getLayout()).isNull();
        assertThat(tile.getImageUrl()).isNull();
        assertThat(tile.getLinkUrl()).isNull();
    }

    @Test
    public void testBuildHomeScreenLayoutWithOneChildWithImageOnly() {
        final String imageUrl = "/images/image.png";

        final MediaModel mockMedia = createMockMediaModel(imageUrl, "image/png");

        final CMSNavigationEntryModel mockImageNavEntry = mock(CMSNavigationEntryModel.class);
        given(mockImageNavEntry.getItem()).willReturn(mockMedia);

        final List<CMSNavigationEntryModel> navigationEntries = new ArrayList<>();
        navigationEntries.add(mockImageNavEntry);

        final CMSNavigationNodeModel mockNavNode = mock(CMSNavigationNodeModel.class);
        doReturn(Boolean.TRUE).when(mockNavNode).isVisible();
        given(mockNavNode.getEntries()).willReturn(navigationEntries);

        given(mockRootNavNode.getChildren()).willReturn(Collections.singletonList(mockNavNode));

        final HomeScreenLayoutDTO result = homeScreenLayoutBuilder.buildHomeScreenLayout(mockRootNavNode);

        assertThat(result).isNotNull();
        assertThat(result.getTiles()).hasSize(1);

        final HomeScreenTileDTO tile = result.getTiles().get(0);
        assertThat(tile.getLayout()).isNull();
        assertThat(tile.getImageUrl()).isEqualTo(imageUrl);
        assertThat(tile.getLinkUrl()).isNull();
    }

    @Test
    public void testBuildHomeScreenLayoutWithImageAndContentPage() {
        final String imageUrl = "/images/image.png";
        final String pageUrl = "/pageurl";

        final MediaModel mockMedia = createMockMediaModel(imageUrl, "image/png");

        final CMSNavigationEntryModel mockImageNavEntry = mock(CMSNavigationEntryModel.class);
        given(mockImageNavEntry.getItem()).willReturn(mockMedia);

        final ContentPageModel mockContentPage = createMockContentPageModel(pageUrl);

        final CMSNavigationEntryModel mockContentPageNavEntry = mock(CMSNavigationEntryModel.class);
        given(mockContentPageNavEntry.getItem()).willReturn(mockContentPage);

        final List<CMSNavigationEntryModel> navigationEntries = new ArrayList<>();
        navigationEntries.add(mockImageNavEntry);
        navigationEntries.add(mockContentPageNavEntry);

        final CMSNavigationNodeModel mockNavNode = mock(CMSNavigationNodeModel.class);
        doReturn(Boolean.TRUE).when(mockNavNode).isVisible();
        given(mockNavNode.getLayout()).willReturn(MobileHomeScreenImageLayoutEnum.SQUARE);
        given(mockNavNode.getEntries()).willReturn(navigationEntries);

        given(mockRootNavNode.getChildren()).willReturn(Collections.singletonList(mockNavNode));

        final HomeScreenLayoutDTO result = homeScreenLayoutBuilder.buildHomeScreenLayout(mockRootNavNode);

        assertThat(result).isNotNull();
        assertThat(result.getTiles()).hasSize(1);

        final HomeScreenTileDTO tile = result.getTiles().get(0);
        assertThat(tile.getLayout()).isEqualTo(MobileHomeScreenImageLayoutEnum.SQUARE.getCode());
        assertThat(tile.getImageUrl()).isEqualTo(imageUrl);
        assertThat(tile.getLinkUrl()).isEqualTo(pageUrl);
    }

    @Test
    public void testBuildHomeScreenLayoutWithImageAndCategoryPage() {
        final String imageUrl = "/images/image.png";
        final String categoryUrl = "/c/women";

        final MediaModel mockMedia = createMockMediaModel(imageUrl, "image/png");

        final CMSNavigationEntryModel mockImageNavEntry = mock(CMSNavigationEntryModel.class);
        given(mockImageNavEntry.getItem()).willReturn(mockMedia);

        final CategoryModel mockCategory = createMockCategoryModel(categoryUrl);

        final CMSNavigationEntryModel mockCategoryNavEntry = mock(CMSNavigationEntryModel.class);
        given(mockCategoryNavEntry.getItem()).willReturn(mockCategory);

        final List<CMSNavigationEntryModel> navigationEntries = new ArrayList<>();
        navigationEntries.add(mockImageNavEntry);
        navigationEntries.add(mockCategoryNavEntry);

        final CMSNavigationNodeModel mockNavNode = mock(CMSNavigationNodeModel.class);
        doReturn(Boolean.TRUE).when(mockNavNode).isVisible();
        given(mockNavNode.getLayout()).willReturn(MobileHomeScreenImageLayoutEnum.BANNER16BY9);
        given(mockNavNode.getEntries()).willReturn(navigationEntries);

        given(mockRootNavNode.getChildren()).willReturn(Collections.singletonList(mockNavNode));

        final HomeScreenLayoutDTO result = homeScreenLayoutBuilder.buildHomeScreenLayout(mockRootNavNode);

        assertThat(result).isNotNull();
        assertThat(result.getTiles()).hasSize(1);

        final HomeScreenTileDTO tile = result.getTiles().get(0);
        assertThat(tile.getLayout()).isEqualTo(MobileHomeScreenImageLayoutEnum.BANNER16BY9.getCode());
        assertThat(tile.getImageUrl()).isEqualTo(imageUrl);
        assertThat(tile.getLinkUrl()).isEqualTo(categoryUrl);
    }

    @Test
    public void testBuildHomeScreenLayoutWithImageAndVisibleExternalLink() {
        final String imageUrl = "/images/image.png";
        final String linkUrl = "https://www.target.com.au";

        final MediaModel mockMedia = createMockMediaModel(imageUrl, "image/png");

        final CMSNavigationEntryModel mockImageNavEntry = mock(CMSNavigationEntryModel.class);
        given(mockImageNavEntry.getItem()).willReturn(mockMedia);

        final CMSLinkComponentModel mockCmsLinkComponent = createMockCMSLinkComponentModel(true, linkUrl);

        final CMSNavigationEntryModel mockExternalLinkNavEntry = mock(CMSNavigationEntryModel.class);
        given(mockExternalLinkNavEntry.getItem()).willReturn(mockCmsLinkComponent);

        final List<CMSNavigationEntryModel> navigationEntries = new ArrayList<>();
        navigationEntries.add(mockImageNavEntry);
        navigationEntries.add(mockExternalLinkNavEntry);

        final CMSNavigationNodeModel mockNavNode = mock(CMSNavigationNodeModel.class);
        doReturn(Boolean.TRUE).when(mockNavNode).isVisible();
        given(mockNavNode.getLayout()).willReturn(MobileHomeScreenImageLayoutEnum.SQUAREX2);
        given(mockNavNode.getEntries()).willReturn(navigationEntries);

        given(mockRootNavNode.getChildren()).willReturn(Collections.singletonList(mockNavNode));

        final HomeScreenLayoutDTO result = homeScreenLayoutBuilder.buildHomeScreenLayout(mockRootNavNode);

        assertThat(result).isNotNull();
        assertThat(result.getTiles()).hasSize(1);

        final HomeScreenTileDTO tile = result.getTiles().get(0);
        assertThat(tile.getLayout()).isEqualTo(MobileHomeScreenImageLayoutEnum.SQUAREX2.getCode());
        assertThat(tile.getImageUrl()).isEqualTo(imageUrl);
        assertThat(tile.getLinkUrl()).isEqualTo(linkUrl);
    }

    @Test
    public void testBuildHomeScreenLayoutWithImageAndNotVisibleExternalLink() {
        final String imageUrl = "/images/image.png";

        final MediaModel mockMedia = createMockMediaModel(imageUrl, "image/png");

        final CMSNavigationEntryModel mockImageNavEntry = mock(CMSNavigationEntryModel.class);
        given(mockImageNavEntry.getItem()).willReturn(mockMedia);

        final CMSLinkComponentModel mockCmsLinkComponent = createMockCMSLinkComponentModel(false, null);

        final CMSNavigationEntryModel mockExternalLinkNavEntry = mock(CMSNavigationEntryModel.class);
        given(mockExternalLinkNavEntry.getItem()).willReturn(mockCmsLinkComponent);

        final List<CMSNavigationEntryModel> navigationEntries = new ArrayList<>();
        navigationEntries.add(mockImageNavEntry);
        navigationEntries.add(mockExternalLinkNavEntry);

        final CMSNavigationNodeModel mockNavNode = mock(CMSNavigationNodeModel.class);
        doReturn(Boolean.TRUE).when(mockNavNode).isVisible();
        given(mockNavNode.getLayout()).willReturn(MobileHomeScreenImageLayoutEnum.BANNER4BY3);
        given(mockNavNode.getEntries()).willReturn(navigationEntries);

        given(mockRootNavNode.getChildren()).willReturn(Collections.singletonList(mockNavNode));

        final HomeScreenLayoutDTO result = homeScreenLayoutBuilder.buildHomeScreenLayout(mockRootNavNode);

        assertThat(result).isNotNull();
        assertThat(result.getTiles()).hasSize(1);

        final HomeScreenTileDTO tile = result.getTiles().get(0);
        assertThat(tile.getLayout()).isEqualTo(MobileHomeScreenImageLayoutEnum.BANNER4BY3.getCode());
        assertThat(tile.getImageUrl()).isEqualTo(imageUrl);
        assertThat(tile.getLinkUrl()).isNull();
    }

    @Test
    public void testBuildHomeScreenLayoutWithContentPageOnly() {
        final String pageUrl = "/pageurl";

        final ContentPageModel mockContentPage = createMockContentPageModel(pageUrl);

        final CMSNavigationEntryModel mockContentPageNavEntry = mock(CMSNavigationEntryModel.class);
        given(mockContentPageNavEntry.getItem()).willReturn(mockContentPage);

        final List<CMSNavigationEntryModel> navigationEntries = new ArrayList<>();
        navigationEntries.add(mockContentPageNavEntry);

        final CMSNavigationNodeModel mockNavNode = mock(CMSNavigationNodeModel.class);
        doReturn(Boolean.TRUE).when(mockNavNode).isVisible();
        given(mockNavNode.getLayout()).willReturn(MobileHomeScreenImageLayoutEnum.SQUAREX3);
        given(mockNavNode.getEntries()).willReturn(navigationEntries);

        given(mockRootNavNode.getChildren()).willReturn(Collections.singletonList(mockNavNode));

        final HomeScreenLayoutDTO result = homeScreenLayoutBuilder.buildHomeScreenLayout(mockRootNavNode);

        assertThat(result).isNotNull();
        assertThat(result.getTiles()).hasSize(1);

        final HomeScreenTileDTO tile = result.getTiles().get(0);
        assertThat(tile.getLayout()).isEqualTo(MobileHomeScreenImageLayoutEnum.SQUAREX3.getCode());
        assertThat(tile.getImageUrl()).isNull();
        assertThat(tile.getLinkUrl()).isEqualTo(pageUrl);
    }

    @Test
    public void testBuildHomeScreenLayoutWithWrongMediaTypeAndContentPage() {
        final String mediaUrl = "/terms.html";
        final String pageUrl = "/pageurl";

        final MediaModel mockMedia = createMockMediaModel(mediaUrl, "text/html");

        final CMSNavigationEntryModel mockMediaNavEntry = mock(CMSNavigationEntryModel.class);
        given(mockMediaNavEntry.getItem()).willReturn(mockMedia);

        final ContentPageModel mockContentPage = createMockContentPageModel(pageUrl);

        final CMSNavigationEntryModel mockContentPageNavEntry = mock(CMSNavigationEntryModel.class);
        given(mockContentPageNavEntry.getItem()).willReturn(mockContentPage);

        final List<CMSNavigationEntryModel> navigationEntries = new ArrayList<>();
        navigationEntries.add(mockMediaNavEntry);
        navigationEntries.add(mockContentPageNavEntry);

        final CMSNavigationNodeModel mockNavNode = mock(CMSNavigationNodeModel.class);
        doReturn(Boolean.TRUE).when(mockNavNode).isVisible();
        given(mockNavNode.getLayout()).willReturn(MobileHomeScreenImageLayoutEnum.SQUARE);
        given(mockNavNode.getEntries()).willReturn(navigationEntries);

        given(mockRootNavNode.getChildren()).willReturn(Collections.singletonList(mockNavNode));

        final HomeScreenLayoutDTO result = homeScreenLayoutBuilder.buildHomeScreenLayout(mockRootNavNode);

        assertThat(result).isNotNull();
        assertThat(result.getTiles()).hasSize(1);

        final HomeScreenTileDTO tile = result.getTiles().get(0);
        assertThat(tile.getLayout()).isEqualTo(MobileHomeScreenImageLayoutEnum.SQUARE.getCode());
        assertThat(tile.getImageUrl()).isNull();
        assertThat(tile.getLinkUrl()).isEqualTo(pageUrl);
    }

    @Test
    public void testBuildHomeScreenLayoutWithImageAndMultipleEntries() {
        final String imageUrl = "/images/image.png";
        final String linkUrl = "https://www.target.com.au";
        final String categoryUrl = "/c/women";
        final String pageUrl = "/termsandconditions";

        final MediaModel mockMedia = createMockMediaModel(imageUrl, "image/png");

        final CMSNavigationEntryModel mockImageNavEntry = mock(CMSNavigationEntryModel.class);
        given(mockImageNavEntry.getItem()).willReturn(mockMedia);

        final CMSLinkComponentModel mockCmsLinkComponent = createMockCMSLinkComponentModel(true, linkUrl);

        final CMSNavigationEntryModel mockExternalLinkNavEntry = mock(CMSNavigationEntryModel.class);
        given(mockExternalLinkNavEntry.getItem()).willReturn(mockCmsLinkComponent);

        final CategoryModel mockCategory = createMockCategoryModel(categoryUrl);

        final CMSNavigationEntryModel mockCategoryNavEntry = mock(CMSNavigationEntryModel.class);
        given(mockCategoryNavEntry.getItem()).willReturn(mockCategory);

        final ContentPageModel mockContentPage = createMockContentPageModel(pageUrl);

        final CMSNavigationEntryModel mockContentPageNavEntry = mock(CMSNavigationEntryModel.class);
        given(mockContentPageNavEntry.getItem()).willReturn(mockContentPage);

        final List<CMSNavigationEntryModel> navigationEntries = new ArrayList<>();
        navigationEntries.add(mockImageNavEntry);
        navigationEntries.add(mockExternalLinkNavEntry);
        navigationEntries.add(mockCategoryNavEntry);
        navigationEntries.add(mockContentPageNavEntry);

        final CMSNavigationNodeModel mockNavNode = mock(CMSNavigationNodeModel.class);
        doReturn(Boolean.TRUE).when(mockNavNode).isVisible();
        given(mockNavNode.getLayout()).willReturn(MobileHomeScreenImageLayoutEnum.SQUAREX2);
        given(mockNavNode.getEntries()).willReturn(navigationEntries);

        given(mockRootNavNode.getChildren()).willReturn(Collections.singletonList(mockNavNode));

        final HomeScreenLayoutDTO result = homeScreenLayoutBuilder.buildHomeScreenLayout(mockRootNavNode);

        assertThat(result).isNotNull();
        assertThat(result.getTiles()).hasSize(1);

        final HomeScreenTileDTO tile = result.getTiles().get(0);
        assertThat(tile.getLayout()).isEqualTo(MobileHomeScreenImageLayoutEnum.SQUAREX2.getCode());
        assertThat(tile.getImageUrl()).isEqualTo(imageUrl);
        assertThat(tile.getLinkUrl()).isEqualTo(linkUrl);
    }

    @Test
    public void testBuildHomeScreenLayoutWithNoImageAndMultipleEntries() {
        final String linkUrl = "https://www.target.com.au";
        final String categoryUrl = "/c/women";
        final String pageUrl = "/termsandconditions";

        final CMSLinkComponentModel mockCmsLinkComponent = createMockCMSLinkComponentModel(true, linkUrl);

        final CMSNavigationEntryModel mockExternalLinkNavEntry = mock(CMSNavigationEntryModel.class);
        given(mockExternalLinkNavEntry.getItem()).willReturn(mockCmsLinkComponent);

        final CategoryModel mockCategory = createMockCategoryModel(categoryUrl);

        final CMSNavigationEntryModel mockCategoryNavEntry = mock(CMSNavigationEntryModel.class);
        given(mockCategoryNavEntry.getItem()).willReturn(mockCategory);

        final ContentPageModel mockContentPage = createMockContentPageModel(pageUrl);
        final CMSNavigationEntryModel mockContentPageNavEntry = mock(CMSNavigationEntryModel.class);
        given(mockContentPageNavEntry.getItem()).willReturn(mockContentPage);

        final List<CMSNavigationEntryModel> navigationEntries = new ArrayList<>();
        navigationEntries.add(mockExternalLinkNavEntry);
        navigationEntries.add(mockCategoryNavEntry);
        navigationEntries.add(mockContentPageNavEntry);

        final CMSNavigationNodeModel mockNavNode = mock(CMSNavigationNodeModel.class);
        doReturn(Boolean.TRUE).when(mockNavNode).isVisible();
        given(mockNavNode.getLayout()).willReturn(MobileHomeScreenImageLayoutEnum.SQUAREX2);
        given(mockNavNode.getEntries()).willReturn(navigationEntries);

        given(mockRootNavNode.getChildren()).willReturn(Collections.singletonList(mockNavNode));

        final HomeScreenLayoutDTO result = homeScreenLayoutBuilder.buildHomeScreenLayout(mockRootNavNode);

        assertThat(result).isNotNull();
        assertThat(result.getTiles()).hasSize(1);

        final HomeScreenTileDTO tile = result.getTiles().get(0);
        assertThat(tile.getLayout()).isEqualTo(MobileHomeScreenImageLayoutEnum.SQUAREX2.getCode());
        assertThat(tile.getImageUrl()).isNull();
        assertThat(tile.getLinkUrl()).isEqualTo(linkUrl);
    }

    @Test
    public void testBuildHomeScreenLayoutWithMultipleTiles() {
        // Tile 1 - Image and category
        final String imageUrlTile1 = "/images/tile1.png";
        final String categoryUrlTile1 = "/c/tile1";

        final MediaModel mockMediaTile1 = createMockMediaModel(imageUrlTile1, "image/png");

        final CMSNavigationEntryModel mockImageNavEntryTile1 = mock(CMSNavigationEntryModel.class);
        given(mockImageNavEntryTile1.getItem()).willReturn(mockMediaTile1);

        final CategoryModel mockCategoryTile1 = createMockCategoryModel(categoryUrlTile1);

        final CMSNavigationEntryModel mockCategoryNavEntryTile1 = mock(CMSNavigationEntryModel.class);
        given(mockCategoryNavEntryTile1.getItem()).willReturn(mockCategoryTile1);

        final List<CMSNavigationEntryModel> navigationEntriesTile1 = new ArrayList<>();
        navigationEntriesTile1.add(mockImageNavEntryTile1);
        navigationEntriesTile1.add(mockCategoryNavEntryTile1);

        final CMSNavigationNodeModel mockNavNodeTile1 = mock(CMSNavigationNodeModel.class);
        doReturn(Boolean.TRUE).when(mockNavNodeTile1).isVisible();
        given(mockNavNodeTile1.getLayout()).willReturn(MobileHomeScreenImageLayoutEnum.BANNER16BY9);
        given(mockNavNodeTile1.getEntries()).willReturn(navigationEntriesTile1);

        // Tile 2 - Image and content page
        final String imageUrlTile2 = "/images/tile2.png";
        final String pageUrlTile2 = "/tile2";

        final MediaModel mockMediaTile2 = createMockMediaModel(imageUrlTile2, "image/png");

        final CMSNavigationEntryModel mockImageNavEntryTile2 = mock(CMSNavigationEntryModel.class);
        given(mockImageNavEntryTile2.getItem()).willReturn(mockMediaTile2);

        final ContentPageModel mockContentPageTile2 = createMockContentPageModel(pageUrlTile2);

        final CMSNavigationEntryModel mockContentPageNavEntryTile2 = mock(CMSNavigationEntryModel.class);
        given(mockContentPageNavEntryTile2.getItem()).willReturn(mockContentPageTile2);

        final List<CMSNavigationEntryModel> navigationEntriesTile2 = new ArrayList<>();
        navigationEntriesTile2.add(mockImageNavEntryTile2);
        navigationEntriesTile2.add(mockContentPageNavEntryTile2);

        final CMSNavigationNodeModel mockNavNodeTile2 = mock(CMSNavigationNodeModel.class);
        doReturn(Boolean.TRUE).when(mockNavNodeTile2).isVisible();
        given(mockNavNodeTile2.getLayout()).willReturn(MobileHomeScreenImageLayoutEnum.SQUAREX3);
        given(mockNavNodeTile2.getEntries()).willReturn(navigationEntriesTile2);

        // Tile 3 - Image only
        final String imageUrlTile3 = "/images/tile3.png";

        final MediaModel mockMediaTile3 = createMockMediaModel(imageUrlTile3, "image/png");

        final CMSNavigationEntryModel mockImageNavEntryTile3 = mock(CMSNavigationEntryModel.class);
        given(mockImageNavEntryTile3.getItem()).willReturn(mockMediaTile3);

        final List<CMSNavigationEntryModel> navigationEntriesTile3 = new ArrayList<>();
        navigationEntriesTile3.add(mockImageNavEntryTile3);

        final CMSNavigationNodeModel mockNavNodeTile3 = mock(CMSNavigationNodeModel.class);
        doReturn(Boolean.TRUE).when(mockNavNodeTile3).isVisible();
        given(mockNavNodeTile3.getEntries()).willReturn(navigationEntriesTile3);

        // Tile 4 - Content page only
        final String pageUrlTile4 = "/tile4";

        final ContentPageModel mockContentPageTile4 = createMockContentPageModel(pageUrlTile4);

        final CMSNavigationEntryModel mockContentPageNavEntryTile4 = mock(CMSNavigationEntryModel.class);
        given(mockContentPageNavEntryTile4.getItem()).willReturn(mockContentPageTile4);

        final List<CMSNavigationEntryModel> navigationEntriesTile4 = new ArrayList<>();
        navigationEntriesTile4.add(mockContentPageNavEntryTile4);

        final CMSNavigationNodeModel mockNavNodeTile4 = mock(CMSNavigationNodeModel.class);
        doReturn(Boolean.TRUE).when(mockNavNodeTile4).isVisible();
        given(mockNavNodeTile4.getLayout()).willReturn(MobileHomeScreenImageLayoutEnum.SQUARE);
        given(mockNavNodeTile4.getEntries()).willReturn(navigationEntriesTile4);

        final List<CMSNavigationNodeModel> childNavNodes = new ArrayList<>();
        childNavNodes.add(mockNavNodeTile1);
        childNavNodes.add(mockNavNodeTile2);
        childNavNodes.add(mockNavNodeTile3);
        childNavNodes.add(mockNavNodeTile4);
        given(mockRootNavNode.getChildren()).willReturn(childNavNodes);

        final HomeScreenLayoutDTO result = homeScreenLayoutBuilder.buildHomeScreenLayout(mockRootNavNode);

        assertThat(result).isNotNull();
        assertThat(result.getTiles()).hasSize(4);

        final HomeScreenTileDTO tile1 = result.getTiles().get(0);
        assertThat(tile1.getLayout()).isEqualTo(MobileHomeScreenImageLayoutEnum.BANNER16BY9.getCode());
        assertThat(tile1.getImageUrl()).isEqualTo(imageUrlTile1);
        assertThat(tile1.getLinkUrl()).isEqualTo(categoryUrlTile1);

        final HomeScreenTileDTO tile2 = result.getTiles().get(1);
        assertThat(tile2.getLayout()).isEqualTo(MobileHomeScreenImageLayoutEnum.SQUAREX3.getCode());
        assertThat(tile2.getImageUrl()).isEqualTo(imageUrlTile2);
        assertThat(tile2.getLinkUrl()).isEqualTo(pageUrlTile2);

        final HomeScreenTileDTO tile3 = result.getTiles().get(2);
        assertThat(tile3.getLayout()).isNull();
        assertThat(tile3.getImageUrl()).isEqualTo(imageUrlTile3);
        assertThat(tile3.getLinkUrl()).isNull();

        final HomeScreenTileDTO tile4 = result.getTiles().get(3);
        assertThat(tile4.getLayout()).isEqualTo(MobileHomeScreenImageLayoutEnum.SQUARE.getCode());
        assertThat(tile4.getImageUrl()).isNull();
        assertThat(tile4.getLinkUrl()).isEqualTo(pageUrlTile4);
    }

    private MediaModel createMockMediaModel(final String downloadUrl, final String mimeType) {
        final MediaModel mockMedia = mock(MediaModel.class);
        given(mockMedia.getDownloadURL()).willReturn(downloadUrl);
        given(mockMedia.getMime()).willReturn(mimeType);

        return mockMedia;
    }

    private CategoryModel createMockCategoryModel(final String categoryUrl) {
        final CategoryModel mockCategory = mock(CategoryModel.class);

        final CategoryData categoryData = new CategoryData();
        given(mockCategoryConverter.convert(mockCategory)).willReturn(categoryData);

        given(mockCategoryDataUrlResolver.resolve(categoryData)).willReturn(categoryUrl);

        return mockCategory;
    }

    private ContentPageModel createMockContentPageModel(final String label) {
        final ContentPageModel mockContentPage = mock(ContentPageModel.class);
        given(mockContentPage.getLabel()).willReturn(label);

        return mockContentPage;
    }

    private CMSLinkComponentModel createMockCMSLinkComponentModel(final boolean visible, final String url) {
        final CMSLinkComponentModel mockCmsLinkComponent = mock(CMSLinkComponentModel.class);
        given(mockCmsLinkComponent.getVisible()).willReturn(Boolean.valueOf(visible));
        given(mockCmsLinkComponent.getUrl()).willReturn(url);

        return mockCmsLinkComponent;
    }
}

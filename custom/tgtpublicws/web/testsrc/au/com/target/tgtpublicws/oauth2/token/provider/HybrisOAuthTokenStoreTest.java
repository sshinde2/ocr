/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package au.com.target.tgtpublicws.oauth2.token.provider;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercewebservicescommons.model.OAuthAccessTokenModel;
import de.hybris.platform.commercewebservicescommons.model.OAuthRefreshTokenModel;
import de.hybris.platform.commercewebservicescommons.oauth2.token.OAuthTokenService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.DefaultOAuth2RefreshToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.common.util.SerializationUtils;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.OAuth2Authentication;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class HybrisOAuthTokenStoreTest {
    protected static final String ACCESS_TOKEN_ID = "1";
    protected static final String ACCESS_TOKEN_VALUE = "accessTokenValue";
    protected static final String REFRESH_TOKEN_ID = "2";
    protected static final String REFRESH_TOKEN_VALUE = "refreshTokenValue";
    protected static final String AUTHENTICATION_ID = "userAuthId";
    protected static final String CLIENT_ID = "clientId";
    protected static final String USER_NAME = "userName";

    protected static final String NOT_EXISTING_TOKEN = "notExisitngToken";

    @Mock
    private OAuthTokenService oauthTokenService;

    @InjectMocks
    private final HybrisOAuthTokenStore tokenStore = new HybrisOAuthTokenStore();

    private DefaultOAuth2AccessToken oauthAccessToken;
    private OAuth2RefreshToken oauthRefreshToken;
    private OAuth2Authentication oauthAuthentication;
    private Authentication authentication;

    private OAuthAccessTokenModel accessTokenModel;
    private OAuthRefreshTokenModel refreshTokenModel;
    private List<OAuthAccessTokenModel> accessTokenModelList;

    @Before
    public void setUp() {
        oauthRefreshToken = new DefaultOAuth2RefreshToken(REFRESH_TOKEN_VALUE);
        oauthAccessToken = new DefaultOAuth2AccessToken(ACCESS_TOKEN_VALUE);
        oauthAccessToken.setRefreshToken(oauthRefreshToken);
        authentication = new TestingAuthenticationToken(USER_NAME, null);
        oauthAuthentication = new OAuth2Authentication(new AuthorizationRequest(CLIENT_ID, null).createOAuth2Request(),
                authentication);

        accessTokenModel = new OAuthAccessTokenModel();
        accessTokenModel.setTokenId(ACCESS_TOKEN_ID);
        accessTokenModel.setToken(SerializationUtils.serialize(oauthAccessToken));
        accessTokenModel.setAuthenticationId(AUTHENTICATION_ID);
        accessTokenModel.setAuthentication(SerializationUtils.serialize(oauthAuthentication));
        accessTokenModel.setClientId(CLIENT_ID);
        accessTokenModel.setUserName(USER_NAME);
        refreshTokenModel = new OAuthRefreshTokenModel();
        refreshTokenModel.setTokenId(REFRESH_TOKEN_ID);
        refreshTokenModel.setToken(SerializationUtils.serialize(oauthRefreshToken));
        refreshTokenModel.setAuthentication(SerializationUtils.serialize(oauthAuthentication));
        accessTokenModel.setRefreshToken(refreshTokenModel);

        accessTokenModelList = new ArrayList<>();
        accessTokenModelList.add(accessTokenModel);
    }

    @Test
    public void testReadAuthentication() {
        given(oauthTokenService.getAccessToken(anyString())).willReturn(accessTokenModel);
        final OAuth2Authentication result = tokenStore.readAuthentication(oauthAccessToken);
        assertThat(result).isEqualTo(oauthAuthentication);
    }

    @Test
    public void testReadAuthenticationForNotExistingToken() {
        given(oauthTokenService.getAccessToken(anyString())).willThrow(
                new UnknownIdentifierException("Unknown identifier"));
        assertThat(tokenStore.readAuthentication(NOT_EXISTING_TOKEN)).isNull();
    }

    @Test
    public void testReadAccessToken() {
        given(oauthTokenService.getAccessToken(anyString())).willReturn(accessTokenModel);
        final OAuth2AccessToken result = tokenStore.readAccessToken(ACCESS_TOKEN_VALUE);
        assertThat(result).isEqualTo(oauthAccessToken);
    }

    @Test
    public void testReadNotExistingAccessToken() {
        given(oauthTokenService.getAccessToken(anyString())).willThrow(
                new UnknownIdentifierException("Unknown identifier"));
        assertThat(tokenStore.readAccessToken(NOT_EXISTING_TOKEN)).isNull();
    }

    @Test
    public void testStoreAccessToken() {
        given(oauthTokenService.getRefreshToken(anyString())).willThrow(
                new UnknownIdentifierException("Unknown identifier"));

        tokenStore.storeAccessToken(oauthAccessToken, oauthAuthentication);

        verify(oauthTokenService, times(1)).saveRefreshToken(anyString(), any(), any());
        verify(oauthTokenService, times(1)).saveAccessToken(anyString(), any(byte[].class), anyString(),
                any(byte[].class),
                eq(USER_NAME), eq(CLIENT_ID), any(OAuthRefreshTokenModel.class));
    }

    @Test
    public void testRemoveAccessToken() {
        tokenStore.removeAccessToken(oauthAccessToken);
        verify(oauthTokenService, times(1)).removeAccessToken(anyString());
    }

    @Test
    public void testFindAccessTokensByUserName() {
        given(oauthTokenService.getAccessTokensForUser(USER_NAME)).willReturn(accessTokenModelList);
        final Collection<OAuth2AccessToken> result = tokenStore.findTokensByClientIdAndUserName(null, USER_NAME);
        assertThat(result).hasSize(1);
        assertThat(result.toArray()[0]).isEqualTo(oauthAccessToken);
    }

    @Test
    public void testFindAccessTokensByClientId() {
        given(oauthTokenService.getAccessTokensForClient(CLIENT_ID)).willReturn(accessTokenModelList);
        final Collection<OAuth2AccessToken> result = tokenStore.findTokensByClientId(CLIENT_ID);
        assertThat(result).hasSize(1);
        assertThat(result.toArray()[0]).isEqualTo(oauthAccessToken);
    }

    @Test
    public void testReadRefreshToken() {
        given(oauthTokenService.getRefreshToken(anyString())).willReturn(refreshTokenModel);
        final OAuth2RefreshToken result = tokenStore.readRefreshToken(REFRESH_TOKEN_VALUE);
        assertThat(result).isEqualTo(oauthRefreshToken);
    }

    @Test
    public void testReadNotExistingRefreshToken() {
        given(oauthTokenService.getRefreshToken(anyString())).willThrow(
                new UnknownIdentifierException("Unknown identifier"));
        assertThat(tokenStore.readRefreshToken(NOT_EXISTING_TOKEN)).isNull();
    }

    @Test
    public void testStoreRefreshToken() {
        tokenStore.storeRefreshToken(oauthRefreshToken, oauthAuthentication);
        verify(oauthTokenService, times(1)).saveRefreshToken(anyString(), any(), any());
    }

    @Test
    public void testReadAuthenticationForRefreshToken() {
        given(oauthTokenService.getRefreshToken(anyString())).willReturn(refreshTokenModel);
        final OAuth2Authentication result = tokenStore.readAuthenticationForRefreshToken(REFRESH_TOKEN_VALUE);
        assertThat(result).isEqualTo(oauthAuthentication);
    }

    @Test
    public void testReadAuthenticationForNotExistingRefreshToken() {
        given(oauthTokenService.getRefreshToken(anyString())).willThrow(
                new UnknownIdentifierException("Unknown identifier"));
        assertThat(tokenStore.readAuthenticationForRefreshToken(REFRESH_TOKEN_VALUE)).isNull();
    }

    @Test
    public void testRemoveRefreshToken() {
        tokenStore.removeRefreshToken(oauthRefreshToken);
        verify(oauthTokenService, times(1)).removeRefreshToken(anyString());
    }

    @Test
    public void testGetAccessTokenAmbigiousException() {
        given(oauthTokenService.getAccessTokenForAuthentication(anyString())).willThrow(
                new AmbiguousIdentifierException("Ambiguous identifier"));
        assertThat(tokenStore.getAccessToken(oauthAuthentication)).isNull();
        verify(oauthTokenService, times(1)).getAccessTokenForAuthentication(anyString());
    }

    @Test
    public void testGetAccessTokenNotFoundException() {
        given(oauthTokenService.getAccessTokenForAuthentication(anyString())).willThrow(
                new UnknownIdentifierException("Unknown identifier"));
        assertThat(tokenStore.getAccessToken(oauthAuthentication)).isNull();
        verify(oauthTokenService, times(1)).getAccessTokenForAuthentication(anyString());
    }

    @Test
    public void testGetAccessTokenInvalidAuthenticationError() {
        given(oauthTokenService.getAccessTokenForAuthentication(anyString())).willThrow(
                new ClassCastException("Could not extract access token"));
        assertThat(tokenStore.getAccessToken(oauthAuthentication)).isNull();
        verify(oauthTokenService, times(1)).getAccessTokenForAuthentication(anyString());
    }

    @Test
    public void testGetAccess() {
        given(oauthTokenService.getAccessTokenForAuthentication(anyString())).willReturn(accessTokenModel);
        final OAuth2AccessToken accessToken = tokenStore.getAccessToken(oauthAuthentication);

        verify(oauthTokenService, times(1)).getAccessTokenForAuthentication(anyString());
        assertThat(accessToken).isEqualTo(tokenStore.deserializeAccessToken((byte[])accessTokenModel.getToken()));
    }
}

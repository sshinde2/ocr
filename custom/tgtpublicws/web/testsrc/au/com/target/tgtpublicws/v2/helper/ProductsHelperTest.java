package au.com.target.tgtpublicws.v2.helper;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercewebservicescommons.dto.product.ImageWsDTO;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.configuration.BaseConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtpublicws.product.dto.TargetColourVariantWsDTO;
import au.com.target.tgtpublicws.product.dto.TargetProductOldWsDTO;
import au.com.target.tgtpublicws.product.dto.TargetProductPartialWsDTO;
import au.com.target.tgtpublicws.product.dto.TargetProductWsDTO;
import au.com.target.tgtpublicws.product.dto.TargetVariantProductWsDTO;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductsHelperTest {


    private static final String DISPLAY_NAME = "Display Name";
    private static final String LARGE_ITEM_MSG = "Large Item Msg";
    private static final String PHYSICAL_GIFT_CARD_MSG = "Physical GiftCard Msg";
    private static final String ZOOM_MISSING_IMAGE_URL = "/zoom/missing.jpg";
    private static final String THUMB_MISSING_IMAGE_URL = "/thumbnail/missing.jpg";
    private static final String PRODUCT_MISSING_IMAGE_URL = "/product/missing.jpg";
    private static final String DEAL_DESC = "DEAL_DESC";

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private MessageSource messageSource;

    @Mock
    private I18NService i18nService;


    @InjectMocks
    private final ProductsHelper productsHelper = new ProductsHelper();

    @Test
    public void testPopulateImageMissingAdditionalProductAttributesWithProductOldWSDTO() {

        final TargetProductListerData source = mock(TargetProductListerData.class);
        final TargetProductOldWsDTO target = new TargetProductOldWsDTO();
        final TargetColourVariantWsDTO colourVariant = new TargetColourVariantWsDTO();
        final List<TargetColourVariantWsDTO> listOfColourVariant = Arrays.asList(colourVariant);
        colourVariant.setDisplayName(DISPLAY_NAME);
        colourVariant.setItemType(TargetColourVariantProductModel._TYPECODE);
        target.setColourVariants(listOfColourVariant);
        productsHelper.populateAdditionalProductAttributes(source, target);
        assertionsWhenImageMissing(target.getColourVariants().get(0).getImages());
    }

    @Test
    public void testPopulateImageMissingAdditionalProductAttributesWithTargetProductWSDTO() {
        final TargetProductListerData source = mock(TargetProductListerData.class);
        final TargetProductWsDTO target = new TargetProductWsDTO();
        final TargetVariantProductWsDTO variant = new TargetVariantProductWsDTO();
        variant.setItemType(TargetColourVariantProductModel._TYPECODE);
        final List<TargetVariantProductWsDTO> listOfVariants = Arrays.asList(variant);
        variant.setDisplayName(DISPLAY_NAME);
        target.setVariants(listOfVariants);
        productsHelper.populateAdditionalProductAttributes(source, target);
        assertionsWhenImageMissing(target.getVariants().get(0).getImages());
    }

    @Test
    public void testPopulateAdditionalProductPartialAttributes() {
        final TargetProductData source = mock(TargetProductData.class);
        final TargetProductPartialWsDTO target = new TargetProductPartialWsDTO();
        given(source.getDealDescription()).willReturn(DEAL_DESC);
        productsHelper.populateAdditionalProductPartialAttributes(source, target);
        assertThat(target.getPromotions()).isNotEmpty();
        assertThat(target.getPromotions().get(0).getDescription()).isEqualTo(DEAL_DESC);
    }

    @Test
    public void testSetDeliveryFeeNoticeWhenLargeMsgToBeDisplayed() {
        final TargetProductListerData source = mock(TargetProductListerData.class);
        final TargetProductWsDTO target = new TargetProductWsDTO();
        final List<TargetVariantProductWsDTO> listOfVariants = new ArrayList<>();
        target.setVariants(listOfVariants);
        given(Boolean.valueOf(source.isProductDeliveryToStoreOnly())).willReturn(Boolean.FALSE);
        given(Boolean.valueOf(source.isBigAndBulky())).willReturn(Boolean.TRUE);
        final PriceData priceData = mock(PriceData.class);
        given(source.getBulkyHomeDeliveryFee()).willReturn(priceData);
        given(source.getBulkyCncFreeThreshold()).willReturn(null);
        given(Boolean.valueOf(source.isAllowDeliverToStore())).willReturn(Boolean.FALSE);
        given(priceData.getValue()).willReturn(BigDecimal.valueOf(20));
        productsHelper.populateAdditionalProductAttributes(source, target);
        assertThat(target.getDeliveryFeeNotice()).isEqualTo(LARGE_ITEM_MSG);
    }

    @Test
    public void testSetDeliveryFeeNoticeWhenPhysicalGiftCardProduct() {
        final TargetProductListerData source = mock(TargetProductListerData.class);
        final TargetProductWsDTO target = new TargetProductWsDTO();
        final List<TargetVariantProductWsDTO> listOfVariants = new ArrayList<>();
        target.setVariants(listOfVariants);
        given(Boolean.valueOf(source.isGiftCard())).willReturn(Boolean.TRUE);
        given(source.getProductTypeCode()).willReturn("giftCard");
        given(source.getGiftCardDeliveryFee()).willReturn("$2.00");
        productsHelper.populateAdditionalProductAttributes(source, target);
        assertThat(target.getDeliveryFeeNotice()).isEqualTo(PHYSICAL_GIFT_CARD_MSG);
    }

    private void assertionsWhenImageMissing(final List<ImageWsDTO> images) {
        final List<String> expectedImageFormats = Arrays.asList("zoom", "product", "thumbnail");
        assertThat(images).isNotNull();
        for (final ImageWsDTO image : images) {
            assertThat(image.getAltText()).isEqualTo(DISPLAY_NAME);
            assertThat(expectedImageFormats).contains(image.getFormat());
            if (image.getFormat().equals("zoom")) {
                assertThat(image.getUrl().equals(ZOOM_MISSING_IMAGE_URL));
            }
            if (image.getFormat().equals("product")) {
                assertThat(image.getUrl().equals(PRODUCT_MISSING_IMAGE_URL));
            }
            if (image.getFormat().equals("thumbnail")) {
                assertThat(image.getUrl().equals(THUMB_MISSING_IMAGE_URL));
            }
        }
    }

    @Before
    public void setupData() {
        final BaseConfiguration configuration = Mockito.mock(BaseConfiguration.class);
        given(configurationService.getConfiguration()).willReturn(configuration);

        given(configuration.getString("tgtpublicws.missingProductImage.zoom"))
                .willReturn(ZOOM_MISSING_IMAGE_URL);

        given(configuration.getString("tgtpublicws.missingProductImage.product"))
                .willReturn(PRODUCT_MISSING_IMAGE_URL);

        given(configuration.getString("tgtpublicws.missingProductImage.thumbnail"))
                .willReturn(THUMB_MISSING_IMAGE_URL);

        given(messageSource.getMessage("product.largeItem", null,
                null)).willReturn(LARGE_ITEM_MSG);

        final Object[] giftCardFee = { "$2.00" };
        given(messageSource.getMessage("product.physicalGiftCard", giftCardFee,
                null)).willReturn(PHYSICAL_GIFT_CARD_MSG);
    }

}

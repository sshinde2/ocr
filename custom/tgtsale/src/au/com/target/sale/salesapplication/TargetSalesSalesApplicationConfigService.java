/**
 * 
 */
package au.com.target.sale.salesapplication;

import de.hybris.platform.commerceservices.enums.SalesApplication;

import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;


/**
 * Extension of TargetSalesApplicationConfigService for tgtsales
 * 
 * @author jjayawa1
 *
 */
public interface TargetSalesSalesApplicationConfigService extends TargetSalesApplicationConfigService {
    /**
     * Checks if given sales application is configured to skip Tlogs.
     *
     * @param salesApp
     *            the sales app
     * @return true, if configured as skipTlogs otherwise false.
     */
    boolean isSkipTlogs(final SalesApplication salesApp);
}

/**
 * 
 */
package au.com.target.tgtsale.actions;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.task.RetryLaterException;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtlayby.actions.CheckOrderStatusAction;
import au.com.target.tgtsale.pos.service.TargetCustomerLockService;


/**
 * Extend the check order status action to clear the lock in POS if the pending transaction fails
 * 
 */
public class PosCheckOrderStatusAction extends CheckOrderStatusAction {
    private TargetCustomerLockService targetCustomerLockService;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String doActionAndGetTransitionBasedOnTransactionStatus(
            final PaymentTransactionEntryModel paymentTransactionEntry,
            final OrderModel order, final OrderProcessModel process) throws RetryLaterException {
        Assert.notNull(order, "Order cannot be null");
        final String retVal =
                super.doActionAndGetTransitionBasedOnTransactionStatus(paymentTransactionEntry, order, process);

        if (TransactionStatus.REJECTED.toString().equals(retVal)) {
            // if this is a layby payment (as apposed to a create) then we would have locked the record in POS an now need to unlock
            if (OrderStatus.INPROGRESS.equals(order.getStatus())
                    && order.getPurchaseOptionConfig().getAllowPaymentDues().booleanValue()) {
                targetCustomerLockService.posUnLockDirect(order);
            }
        }

        return retVal;
    }

    /**
     * @param targetCustomerLockService
     *            the targetCustomerLockService to set
     */
    @Required
    public void setTargetCustomerLockService(final TargetCustomerLockService targetCustomerLockService) {
        this.targetCustomerLockService = targetCustomerLockService;
    }
}

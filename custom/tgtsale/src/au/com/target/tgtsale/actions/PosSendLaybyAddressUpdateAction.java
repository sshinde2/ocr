/**
 * 
 */
package au.com.target.tgtsale.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtsale.pos.service.TargetPOSExtractsService;


/**
 * Actions that sends a lay-by order address extract.
 */
public class PosSendLaybyAddressUpdateAction extends AbstractProceduralAction<OrderProcessModel> {

    private TargetPOSExtractsService posExtractsService;

    @Override
    public void executeAction(final OrderProcessModel process) throws Exception {
        Assert.notNull(process, "process cannot be null");
        Assert.notNull(process.getOrder(), "order cannot be null");

        getPosExtractsService().sendLaybyOrderAddressExtract(process.getOrder());
    }

    /**
     * Returns the POS extracts service.
     *
     * @return the POST extracts service
     */
    public TargetPOSExtractsService getPosExtractsService() {
        return posExtractsService;
    }

    /**
     * Sets the POS extracts service.
     *
     * @param posExtractsService the POS extracts service to set
     */
    @Required
    public void setPosExtractsService(final TargetPOSExtractsService posExtractsService) {
        this.posExtractsService = posExtractsService;
    }
}

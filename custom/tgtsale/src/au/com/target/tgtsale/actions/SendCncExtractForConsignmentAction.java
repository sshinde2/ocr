/**
 * 
 */
package au.com.target.tgtsale.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtsale.pos.service.TargetPOSExtractsService;


/**
 * Action responsible for notifying POS about the shipments confirmation of CNC orders on consignment level
 */
public class SendCncExtractForConsignmentAction extends AbstractProceduralAction<OrderProcessModel> {

    private static final Logger LOG = Logger.getLogger(SendCncExtractForConsignmentAction.class);

    private TargetPOSExtractsService posExtractsService;

    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        final ConsignmentModel consignment = orderProcessParameterHelper.getConsignment(process);
        Assert.notNull(consignment);
        if (consignment instanceof TargetConsignmentModel) {
            final TargetConsignmentModel targetConsignment = (TargetConsignmentModel)consignment;
            posExtractsService.sendCncOrderExtractForConsignment(targetConsignment);
            LOG.info("Shipment confirmation of CNC Order for consignment: " + targetConsignment.getCode()
                    + " published to POS successfully");
        }
        else {
            LOG.error("consignmentModel is not a TargetConsignmentModel: " + consignment.getCode());
        }
    }

    /**
     * @param posExtractsService
     *            the posExtractsService to set
     */
    @Required
    public void setPosExtractsService(final TargetPOSExtractsService posExtractsService) {
        this.posExtractsService = posExtractsService;
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }


}

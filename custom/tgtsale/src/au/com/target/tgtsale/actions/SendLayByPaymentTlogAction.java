package au.com.target.tgtsale.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtsale.pos.service.TargetCustomerLockService;


/**
 * Action responsible for notifying POS about the adhoc layby payments.
 * 
 */
public class SendLayByPaymentTlogAction extends SendOrderTlogAction {
    private TargetCustomerLockService targetCustomerLockService;

    @Override
    protected void publishTlog(final OrderProcessModel process) {
        Assert.notNull(process, "Order process cannot be null");
        final OrderModel orderModel = process.getOrder();
        Assert.notNull(orderModel, "Ordermodel cannot be null");
        final PaymentTransactionEntryModel ptem =
                getOrderProcessParameterHelper().getPaymentTransactionEntry(process);
        Assert.notNull(ptem, "PaymentTransactionEntryModel may not be null");

        // don't care if the convert lock fails, we will just continue on anyway
        targetCustomerLockService.convertLockIndirect(orderModel);
        getTargetTransactionLogService().publishTlogForPayment(ptem);
    }

    /**
     * @param targetCustomerLockService
     *            the targetCustomerLockService to set
     */
    @Required
    public void setTargetCustomerLockService(final TargetCustomerLockService targetCustomerLockService) {
        this.targetCustomerLockService = targetCustomerLockService;
    }
}

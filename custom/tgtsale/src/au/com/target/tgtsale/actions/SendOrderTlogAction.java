/**
 * 
 */
package au.com.target.tgtsale.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtsale.tlog.TargetTransactionLogService;


/**
 * Base class for actions that publish to TLOG
 * 
 */
public abstract class SendOrderTlogAction extends AbstractProceduralAction<OrderProcessModel> {

    private TargetTransactionLogService targetTransactionLogService;
    private OrderProcessParameterHelper orderProcessParameterHelper;

    protected abstract void publishTlog(OrderProcessModel process);

    @Override
    public void executeAction(final OrderProcessModel process) throws RetryLaterException, Exception {
        Assert.notNull(process, "process may not be null");

        publishTlog(process);
    }

    /**
     * @return the targetTransactionLogService
     */
    public TargetTransactionLogService getTargetTransactionLogService() {
        return targetTransactionLogService;
    }

    /**
     * @param targetTransactionLogService
     *            the targetTransactionLogService to set
     */
    @Required
    public void setTargetTransactionLogService(final TargetTransactionLogService targetTransactionLogService) {
        this.targetTransactionLogService = targetTransactionLogService;
    }

    /**
     * @return the orderProcessParameterHelper
     */
    public OrderProcessParameterHelper getOrderProcessParameterHelper() {
        return orderProcessParameterHelper;
    }

    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }
}

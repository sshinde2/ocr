/**
 * 
 */
package au.com.target.tgtsale.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.sale.salesapplication.TargetSalesSalesApplicationConfigService;


/**
 * Send to TLOG for a Cancel
 */
public class SendOrderTlogCancelAction extends SendOrderTlogAction {

    private static final Logger LOG = Logger.getLogger(SendOrderTlogCancelAction.class);
    private TargetSalesSalesApplicationConfigService salesApplicationConfigService;

    @Override
    protected void publishTlog(final OrderProcessModel process) {

        final OrderModel order = process.getOrder();
        Assert.notNull(order);
        //Bypass the tlog if the order is from trademe
        if (salesApplicationConfigService.isSkipTlogs(order.getSalesApplication())) {
            LOG.info(MessageFormat.format(
                    "Order Cancel Tlog is not getting triggered for sales application={0} and order={1}.",
                    order.getSalesApplication(), order.getCode()));
            return;
        }

        final OrderCancelRecordEntryModel cancelRequestModel = getOrderProcessParameterHelper()
                .getOrderCancelRequest(process);
        Assert.notNull(cancelRequestModel, "cancelRequest may not be null");
        final List<PaymentTransactionEntryModel> refundPaymentTransactionEntries = getOrderProcessParameterHelper()
                .getRefundPaymentEntryList(process);
        if (BooleanUtils.isTrue(order.getContainsPreOrderItems())) {
            getTargetTransactionLogService().publishTlogForPreOrderCancel(order, refundPaymentTransactionEntries);
        }
        else {

            final BigDecimal refundAmountRemaining = getOrderProcessParameterHelper().getRefundAmountRemaining(process);
            getTargetTransactionLogService().publishTlogForOrderCancel(cancelRequestModel,
                    refundPaymentTransactionEntries,
                    refundAmountRemaining);
        }

    }

    /**
     * @param salesApplicationConfigService
     *            the salesApplicationConfigService to set
     */
    @Required
    public void setSalesApplicationConfigService(
            final TargetSalesSalesApplicationConfigService salesApplicationConfigService) {
        this.salesApplicationConfigService = salesApplicationConfigService;
    }

}

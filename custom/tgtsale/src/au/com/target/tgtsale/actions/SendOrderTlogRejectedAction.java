/**
 * 
 */
package au.com.target.tgtsale.actions;

import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import java.util.List;

import org.springframework.util.Assert;


/**
 * Send to TLOG for a Rejected order
 */
public class SendOrderTlogRejectedAction extends SendOrderTlogAction {

    @Override
    protected void publishTlog(final OrderProcessModel process) {
        final OrderCancelRecordEntryModel cancelRequestModel = getOrderProcessParameterHelper()
                .getOrderCancelRequest(process);
        final List<PaymentTransactionEntryModel> refundPaymentTransactionEntries = getOrderProcessParameterHelper()
                .getRefundPaymentEntryList(process);
        Assert.notNull(cancelRequestModel, "cancelRequest may not be null");

        /************
         * NOTE : Please note that the refunded amount needs to be set on the cancelRequestModel for the TLOG's to work
         * in case of this action
         **********/

        getTargetTransactionLogService().publishTlogForOrderRejected(cancelRequestModel,
                refundPaymentTransactionEntries);
    }

}

/**
 * 
 */
package au.com.target.tgtsale.actions;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.returns.model.OrderReturnRecordEntryModel;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.util.Assert;


/**
 * Send to TLOG for a Return
 */
public class SendOrderTlogReturnAction extends SendOrderTlogAction {

    @Override
    protected void publishTlog(final OrderProcessModel process) {

        final OrderReturnRecordEntryModel returnRequestModel = getOrderProcessParameterHelper()
                .getReturnRequest(process);
        Assert.notNull(returnRequestModel, "returnRequest may not be null");

        final List<PaymentTransactionEntryModel> refundPaymentTransactionEntries = getOrderProcessParameterHelper()
                .getRefundPaymentEntryList(process);
        final BigDecimal refundAmountRemaining = getOrderProcessParameterHelper().getRefundAmountRemaining(process);
        getTargetTransactionLogService().publishTlogForOrderReturn(returnRequestModel, refundPaymentTransactionEntries,
                refundAmountRemaining);
    }

}

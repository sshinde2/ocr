/**
 * 
 */
package au.com.target.tgtsale.actions;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import org.springframework.util.Assert;


/**
 * Send to TLOG for a Sale
 * 
 */
public class SendOrderTlogSaleAction extends SendOrderTlogAction {

    @Override
    protected void publishTlog(final OrderProcessModel process) {
        final OrderModel order = process.getOrder();
        Assert.notNull(order, "Order can't be empty!");
        getTargetTransactionLogService().publishTlogForOrderSale(order);
    }

}

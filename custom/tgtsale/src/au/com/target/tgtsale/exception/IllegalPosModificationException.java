/**
 * 
 */
package au.com.target.tgtsale.exception;

import au.com.target.tgtlayby.exception.LaybyException;

/**
 * POS has performed a modification that is not supported by our app
 */
public class IllegalPosModificationException extends LaybyException {
    /**
     * @param msg
     *            descriptive message
     */
    public IllegalPosModificationException(final String msg) {
        super(msg);
    }

    /**
     * @param msg
     *            descriptive message
     * @param cause
     *            for cascading exceptions
     */
    public IllegalPosModificationException(final String msg, final Throwable cause) {
        super(msg, cause);
    }
}

package au.com.target.tgtsale.integration.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "integration-pos-price")
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationPosProductsDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @XmlElementWrapper(name = "products")
    @XmlElement(name = "product")
    private List<IntegrationPosProductItemDto> products;

    public IntegrationPosProductsDto() {
        super();
        this.products = new ArrayList<>();
    }

    public List<IntegrationPosProductItemDto> getProducts() {
        return products;
    }

    public void setProducts(final List<IntegrationPosProductItemDto> products) {
        this.products = products;
    }
}

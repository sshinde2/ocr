/**
 * 
 */
package au.com.target.tgtsale.integration.dto;

import java.util.List;


/**
 * @author rsamuel3
 * 
 */
public class IntegrationProductsList {
    private List<IntegrationPosProductItemDto> product;

    /**
     * @return the product
     */
    public List<IntegrationPosProductItemDto> getProduct() {
        return product;
    }

    /**
     * @param product
     *            the product to set
     */
    public void setProduct(final List<IntegrationPosProductItemDto> product) {
        this.product = product;
    }


}

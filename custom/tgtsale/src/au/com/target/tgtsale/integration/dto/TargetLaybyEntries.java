/**
 * 
 */
package au.com.target.tgtsale.integration.dto;

import java.util.List;


/**
 * @author rsamuel3
 * 
 */
public class TargetLaybyEntries {
    private List<TargetLaybyOrderEntry> entry;

    /**
     * @return the entry
     */
    public List<TargetLaybyOrderEntry> getEntry() {
        return entry;
    }

    /**
     * @param entry
     *            the entry to set
     */
    public void setEntry(final List<TargetLaybyOrderEntry> entry) {
        this.entry = entry;
    }
}

/**
 * 
 */
package au.com.target.tgtsale.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author rsamuel3
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class TargetLaybyOrderEntry {

    @XmlElement
    private String code;
    /*
     * Hold the quantity of the product added
     */
    @XmlElement
    private long quantity;
    /*
     * This hold the ProductData , which contains the product details
     */
    // private ProductModel product;
    /*
     * Hold the baseprice of the product
     */
    @XmlElement
    private double basePrice;
    /*
     * Hold the totalPrice of the entry ( base price quantity )
     */
    @XmlElement
    private double totalPrice;
    /*
     * GST rate for product
     */
    @XmlElement
    private int gstRate;


    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the quantity
     */
    public long getQuantity() {
        return quantity;
    }

    /**
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(final long quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the basePrice
     */
    public double getBasePrice() {
        return basePrice;
    }

    /**
     * @param basePrice
     *            the basePrice to set
     */
    public void setBasePrice(final double basePrice) {
        this.basePrice = basePrice;
    }

    /**
     * @return the totalPrice
     */
    public double getTotalPrice() {
        return totalPrice;
    }

    /**
     * @param totalPrice
     *            the totalPrice to set
     */
    public void setTotalPrice(final double totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * @return the gstRate
     */
    public int getGstRate() {
        return gstRate;
    }

    /**
     * @param gstRate
     *            the gstRate to set
     */
    public void setGstRate(final int gstRate) {
        this.gstRate = gstRate;
    }

}

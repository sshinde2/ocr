/**
 * 
 */
package au.com.target.tgtsale.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import de.hybris.platform.voucher.model.PromotionVoucherModel;

import org.apache.commons.lang.BooleanUtils;

import au.com.target.tgtutility.util.BarcodeTools;


/**
 * @author pthoma20
 * 
 */
public class TgtSalePromotionVoucherModelValidateInterceptor implements ValidateInterceptor<PromotionVoucherModel> {

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object, de.hybris.platform.servicelayer.interceptor.InterceptorContext)
     */
    @Override
    public void onValidate(final PromotionVoucherModel promotionVoucher, final InterceptorContext ctx)
            throws InterceptorException {

        if (BooleanUtils.isFalse(promotionVoucher.getEnabledInstore()) && BooleanUtils.isFalse(promotionVoucher
                .getEnabledOnline())) {
            throw new InterceptorException(ErrorMessages.VOUCHER_NOT_ENABLED);
        }

        if (Boolean.TRUE.equals(promotionVoucher.getEnabledInstore())
                && (null == promotionVoucher.getBarcode()
                || !BarcodeTools.testEANCheckDigit(promotionVoucher.getBarcode().toString()))) {
            throw new InterceptorException(ErrorMessages.BARCODE_INVALID);
        }
    }

    protected interface ErrorMessages {
        String VOUCHER_NOT_ENABLED = "The voucher should at least be enabled for online or store";
        String BARCODE_INVALID = "For enabling a voucher in store, please ensure that the barcode has a value and is valid.";
    }
}

/**
 * 
 */
package au.com.target.tgtsale.ist.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author Nandini
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemDto {

    @XmlAttribute
    private String code;

    @XmlAttribute
    private Long qty;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the qty
     */
    public Long getQty() {
        return qty;
    }

    /**
     * @param qty
     *            the qty to set
     */
    public void setQty(final Long qty) {
        this.qty = qty;
    }

}

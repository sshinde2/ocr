/**
 * 
 */
package au.com.target.tgtsale.ist.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author Nandini
 *
 */
@XmlRootElement(name = "stockAdjustment")
@XmlAccessorType(XmlAccessType.FIELD)
public class StockAdjustmentDto {

    @XmlAttribute
    private Integer fromStore;

    @XmlElement
    private List<ItemDto> items;

    /**
     * @return the fromStore
     */
    public Integer getFromStore() {
        return fromStore;
    }

    /**
     * @param fromStore
     *            the fromStore to set
     */
    public void setFromStore(final Integer fromStore) {
        this.fromStore = fromStore;
    }

    /**
     * @return the items
     */
    public List<ItemDto> getItems() {
        return items;
    }

    /**
     * @param items
     *            the items to set
     */
    public void setItems(final List<ItemDto> items) {
        this.items = items;
    }

}

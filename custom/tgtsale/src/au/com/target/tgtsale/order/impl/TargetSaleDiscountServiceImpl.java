package au.com.target.tgtsale.order.impl;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TMDiscountProductPromotionModel;
import au.com.target.tgtcore.order.impl.TargetDiscountServiceImpl;
import au.com.target.tgtsale.model.TargetTMDPrefixModel;
import au.com.target.tgtsale.storeconfig.TargetStoreConfigDao;
import au.com.target.tgtsale.tmd.TargetTMDHotCardService;
import au.com.target.tgtutility.util.BarcodeTools;


public class TargetSaleDiscountServiceImpl extends TargetDiscountServiceImpl {
    private static final Logger LOG = Logger.getLogger(TargetSaleDiscountServiceImpl.class);

    private static final int CARD_LEN = 13;

    @Resource
    private TargetStoreConfigDao targetStoreConfigDao;
    @Resource
    private TargetTMDHotCardService targetTMDHotCardService;

    @Override
    public boolean isValidTMDCard(final String cardNumber) {
        Assert.notNull(cardNumber, "Card number cannot be null");
        try {
            if (cardNumber.length() != CARD_LEN || !StringUtils.isNumeric(cardNumber)) {
                return false;
            }
            if (targetTMDHotCardService.exists(Long.parseLong(cardNumber, 10))) {
                return false;
            }
            if (!BarcodeTools.testEANCheckDigit(cardNumber)) {
                return false;
            }
            return true;
        }
        catch (final Exception e) {
            LOG.error("Could not validate TMD card number", e);
            // prefer to give away discount rather than loose the sale, so accept this card
            return true;
        }
    }

    /**
     * Validate the type of card
     * 
     * @param card
     *            card number to find
     * @param tmDiscountProductPromotionModel
     *            team member discount promotion model
     * @return the true if card type match with card, else false
     */
    @Override
    public boolean isValidCardType(final String card,
            final TMDiscountProductPromotionModel tmDiscountProductPromotionModel) {
        if (tmDiscountProductPromotionModel == null) {
            return false;
        }
        final TargetTMDPrefixModel prefixModel = tmDiscountProductPromotionModel.getTgtTmdPrefix();

        if (null != prefixModel && null != prefixModel.getPrefixes()) {
            final String[] prefixList = prefixModel.getPrefixes().split(",");
            for (final String prefix : prefixList) {
                if (card.startsWith(prefix)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return the targetStoreConfigDao
     */
    public TargetStoreConfigDao getTargetStoreConfigDao() {
        return targetStoreConfigDao;
    }

    /**
     * @param targetStoreConfigDao
     *            the targetStoreConfigDao to set
     */
    public void setTargetStoreConfigDao(final TargetStoreConfigDao targetStoreConfigDao) {
        this.targetStoreConfigDao = targetStoreConfigDao;
    }

    /**
     * @return the targetTMDHotCardService
     */
    public TargetTMDHotCardService getTargetTMDHotCardService() {
        return targetTMDHotCardService;
    }

    /**
     * @param targetTMDHotCardService
     *            the targetTMDHotCardDao to set
     */
    public void setTargetTMDHotCardService(final TargetTMDHotCardService targetTMDHotCardService) {
        this.targetTMDHotCardService = targetTMDHotCardService;
    }
}

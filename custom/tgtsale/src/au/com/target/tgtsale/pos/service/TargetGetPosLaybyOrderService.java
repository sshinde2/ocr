/**
 * 
 */
package au.com.target.tgtsale.pos.service;

import au.com.target.tgtsale.integration.dto.TargetLaybyOrder;
import au.com.target.tgtsale.integration.exception.UnsuccessfulESBOperationException;



/**
 * @author rsamuel3
 * 
 */
public interface TargetGetPosLaybyOrderService {
    /**
     * retrieve the layby order from pos
     * 
     * @param laybynum
     * @return TargetLaybyOrder DTO of the order received from POS
     * @throws UnsuccessfulESBOperationException
     */
    TargetLaybyOrder retrieveLaybyOrderFromPos(String laybynum) throws UnsuccessfulESBOperationException;
}

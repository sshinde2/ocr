/**
 * 
 */
package au.com.target.tgtsale.pos.service;

import de.hybris.platform.core.model.order.OrderModel;

import au.com.target.tgtcore.model.TargetConsignmentModel;


/**
 * interface to extract CNC Orders to POS
 * 
 * @author rsamuel3
 * 
 */
public interface TargetPOSExtractsService {

    /**
     * Extracts the cnc orders to POS for order completion
     * 
     * @param order
     *            orders to be extracte
     */
    void sendCncOrderExtractForCompletion(OrderModel order);

    /**
     * Extracts the cnc orders to POS for consignment
     * 
     * @param consignment
     *            consignment to be extracte
     */
    void sendCncOrderExtractForConsignment(TargetConsignmentModel consignment);

    /**
     * send the layby order Address ex
     * 
     * @param order
     */
    void sendLaybyOrderAddressExtract(OrderModel order);
}

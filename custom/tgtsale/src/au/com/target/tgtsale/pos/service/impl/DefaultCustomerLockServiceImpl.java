/**
 * 
 */
package au.com.target.tgtsale.pos.service.impl;

import de.hybris.platform.core.model.order.OrderModel;

import au.com.target.tgtsale.pos.service.TargetCustomerLockService;


/**
 * @author rsamuel3
 * 
 */
public class DefaultCustomerLockServiceImpl implements TargetCustomerLockService {

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.pos.service.TargetCustomerLockService#lockLaybyCustomerDirect(de.hybris.platform.core.model.order.OrderModel)
     */
    @Override
    public boolean posLockDirect(final OrderModel order) {
        // YTODO Auto-generated method stub
        return false;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.pos.service.TargetCustomerLockService#unLockLaybyCustomerDirect(de.hybris.platform.core.model.order.OrderModel)
     */
    @Override
    public boolean posUnLockDirect(final OrderModel order) {
        // YTODO Auto-generated method stub
        return false;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.pos.service.TargetCustomerLockService#lockLaybyCustomerIndirect(de.hybris.platform.core.model.order.OrderModel)
     */
    @Override
    public boolean convertLockIndirect(final OrderModel order) {
        // YTODO Auto-generated method stub
        return false;
    }

}

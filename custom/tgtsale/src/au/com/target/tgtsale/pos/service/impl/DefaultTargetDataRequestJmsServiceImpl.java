/**
 * 
 */
package au.com.target.tgtsale.pos.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import au.com.target.tgtsale.pos.service.TargetDataRequestJmsService;


/**
 * Default implementation to avoid using jms
 * 
 * @author dcwillia
 */
public class DefaultTargetDataRequestJmsServiceImpl implements TargetDataRequestJmsService {
    private static final Logger LOG = Logger.getLogger(DefaultTargetDataRequestJmsServiceImpl.class);

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.pos.service.TargetDataRequestJmsService#requestProductPriceData()
     */
    @Override
    public void requestProductPriceData() {
        LOG.debug("called default requestProductPriceData");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.pos.service.TargetDataRequestJmsService#requestHotcardData()
     */
    @Override
    public void requestHotcardData() {
        LOG.debug("called default requestHotcardData");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.pos.service.TargetDataRequestJmsService#requestLaybyChangesData()
     */
    @Override
    public void requestLaybyChangesData() {
        LOG.debug("called default requestLaybyChangesData");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.pos.service.TargetDataRequestJmsService#triggerCatalogSyncFeedback()
     */
    @Override
    public void triggerCatalogSyncFeedback() {
        LOG.debug("called default triggerCatalogSyncFeedback");
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.pos.service.TargetDataRequestJmsService#requestNullPriceProducts(java.util.List)
     */
    @Override
    public void requestNullPriceProducts(final List<String> codes) {
        LOG.debug("called default requestNullPriceProducts");
    }

}

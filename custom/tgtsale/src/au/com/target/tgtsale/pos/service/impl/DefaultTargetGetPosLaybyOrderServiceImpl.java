/**
 * 
 */
package au.com.target.tgtsale.pos.service.impl;

import au.com.target.tgtsale.integration.dto.TargetLaybyOrder;
import au.com.target.tgtsale.integration.exception.UnsuccessfulESBOperationException;
import au.com.target.tgtsale.pos.service.TargetGetPosLaybyOrderService;


/**
 * Default implementation to avoid using jms
 * 
 * @author rsamuel3
 * 
 * 
 */
public class DefaultTargetGetPosLaybyOrderServiceImpl implements TargetGetPosLaybyOrderService {

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.pos.service.TargetGetPosLaybyOrderService#retrieveLaybyOrderFromPos(java.lang.String)
     */
    @Override
    public TargetLaybyOrder retrieveLaybyOrderFromPos(final String laybynum) throws UnsuccessfulESBOperationException {
        // YTODO Auto-generated method stub
        return new TargetLaybyOrder();
    }

}

/**
 * 
 */
package au.com.target.tgtsale.pos.service.impl;

import de.hybris.platform.core.model.order.OrderModel;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtsale.pos.service.TargetPOSExtractsService;


/**
 * @author rsamuel3
 * 
 */
public class DefaultTargetPOSExtractsServiceImpl implements TargetPOSExtractsService {

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.pos.service.TargetPOSExtractsService#sendLaybyOrderAddressExtract(de.hybris.platform.core.model.order.OrderModel)
     */
    @Override
    public void sendLaybyOrderAddressExtract(final OrderModel order) {
        //DUMMY IMPLEMENTATION ----actual implementation in tgtjms
    }

    @Override
    public void sendCncOrderExtractForCompletion(final OrderModel order) {
        //DUMMY IMPLEMENTATION ----actual implementation in tgtjms    
    }

    @Override
    public void sendCncOrderExtractForConsignment(final TargetConsignmentModel consignment) {
        //DUMMY IMPLEMENTATION ----actual implementation in tgtjms     
    }

}

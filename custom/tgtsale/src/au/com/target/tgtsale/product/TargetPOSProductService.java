/**
 * 
 */
package au.com.target.tgtsale.product;

import au.com.target.tgtsale.product.data.POSProduct;


/**
 * Accessing product details from POS
 * 
 */
public interface TargetPOSProductService {

    /**
     * Get the product info from given POS
     * 
     * @param storeNumber
     * @param ean
     * @return product details
     */
    POSProduct getPOSProductDetails(final Integer storeNumber, String ean);
}

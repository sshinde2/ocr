/**
 * 
 */
package au.com.target.tgtsale.product.client;

import au.com.target.tgtsale.product.dto.request.ProductRequestDto;
import au.com.target.tgtsale.product.dto.response.ProductResponseDto;
import au.com.target.tgtwebcore.exception.TargetIntegrationException;


/**
 * Client for retrieving product info from POS
 * 
 */
public interface TargetPOSProductClient {

    /**
     * Retrieves product details for product code and store specified in the request
     * 
     * @param request
     * @return product details from pos
     * @throws TargetIntegrationException
     *             when requests to external resources fail
     */
    ProductResponseDto getPOSProductDetails(ProductRequestDto request) throws TargetIntegrationException;
}

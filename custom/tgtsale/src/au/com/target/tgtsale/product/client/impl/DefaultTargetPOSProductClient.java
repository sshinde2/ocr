/**
 * 
 */
package au.com.target.tgtsale.product.client.impl;

import org.apache.log4j.Logger;

import au.com.target.tgtsale.product.client.TargetPOSProductClient;
import au.com.target.tgtsale.product.dto.request.ProductRequestDto;
import au.com.target.tgtsale.product.dto.response.ProductResponseDto;


/**
 * Default stub implementation of the pos product client
 * 
 */
public class DefaultTargetPOSProductClient implements TargetPOSProductClient {

    private static final Logger LOG = Logger.getLogger(DefaultTargetPOSProductClient.class);

    @Override
    public ProductResponseDto getPOSProductDetails(final ProductRequestDto request) {

        LOG.info("Called default stub implementation of getPOSProductDetails");

        // Mock some data that will work on staging or dev based on ean passed in
        final String ean = request.getEan();

        final ProductResponseDto dto = new ProductResponseDto();
        dto.setSuccess(true);
        dto.setDescription("Description from POS");
        dto.setPrice(Integer.valueOf(300));
        dto.setWasPrice(Integer.valueOf(500));

        if (ean.equals("9300675001410")) {
            // This should work on staging
            dto.setItemcode("53867049");
        }
        else
        {
            // Works on dev
            dto.setItemcode("P1000");
        }

        if (ean.equals("9300675001434")) {
            dto.setSuccess(false);
            dto.setErrorMessage("Error message from POS eg Itemcode not found");
        }

        return dto;
    }

}

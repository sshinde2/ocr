/**
 * 
 */
package au.com.target.tgtsale.product.data;

/**
 * Represents product info retrieved from POS
 * 
 */
public class POSProduct {

    private String itemCode;
    private String description;
    private Integer priceInCents;
    private Integer wasPriceInCents;
    private String error;
    private boolean systemError; // Error in service rather than pos


    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(final String itemCode) {
        this.itemCode = itemCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Integer getPriceInCents() {
        return priceInCents;
    }

    public void setPriceInCents(final Integer priceInCents) {
        this.priceInCents = priceInCents;
    }

    public Integer getWasPriceInCents() {
        return wasPriceInCents;
    }

    public void setWasPriceInCents(final Integer wasPriceInCents) {
        this.wasPriceInCents = wasPriceInCents;
    }

    public String getError() {
        return error;
    }

    public void setError(final String error) {
        this.error = error;
    }

    public boolean getSystemError() {
        return systemError;
    }

    public void setSystemError(final boolean systemError) {
        this.systemError = systemError;
    }


}

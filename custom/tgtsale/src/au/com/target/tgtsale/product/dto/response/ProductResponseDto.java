/**
 * 
 */
package au.com.target.tgtsale.product.dto.response;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;


/**
 * represent response when getting product info from POS
 * 
 */
public class ProductResponseDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @SerializedName("success")
    private boolean success;

    @SerializedName("errorSource")
    private String errorSource;

    @SerializedName("msg")
    private String errorMessage;

    @SerializedName("itemcode")
    private String itemcode;

    @SerializedName("desc")
    private String description;

    @SerializedName("price")
    private Integer price;

    @SerializedName("was")
    private Integer wasPrice;

    @SerializedName("timeout")
    private boolean timeout;

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(final boolean success) {
        this.success = success;
    }

    public String getErrorSource() {
        return errorSource;
    }

    public void setErrorSource(final String errorSource) {
        this.errorSource = errorSource;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(final String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(final String itemcode) {
        this.itemcode = itemcode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(final Integer price) {
        this.price = price;
    }

    public Integer getWasPrice() {
        return wasPrice;
    }

    public void setWasPrice(final Integer wasPrice) {
        this.wasPrice = wasPrice;
    }

    public boolean isTimeout() {
        return timeout;
    }

    public void setTimeout(final boolean timeout) {
        this.timeout = timeout;
    }
}

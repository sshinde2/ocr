/**
 * 
 */
package au.com.target.tgtsale.product.impl;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtsale.product.TargetPOSProductService;
import au.com.target.tgtsale.product.client.TargetPOSProductClient;
import au.com.target.tgtsale.product.data.POSProduct;
import au.com.target.tgtsale.product.dto.request.ProductRequestDto;
import au.com.target.tgtsale.product.dto.response.ProductResponseDto;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;
import au.com.target.tgtwebcore.exception.TargetIntegrationException;


/**
 * Implementation of service to get product data from POS
 * 
 */
public class TargetPOSProductServiceImpl implements TargetPOSProductService {

    private static final Logger LOG = Logger.getLogger(TargetPOSProductServiceImpl.class);

    private static final String TIMEOUT = "Timeout";

    private TargetPOSProductClient targetPOSProductClient;

    private TargetPointOfServiceService targetPointOfServiceService;

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.product.TargetPOSProductService#getPOSProductDetails(java.lang.Integer, java.lang.String)
     */
    @Override
    public POSProduct getPOSProductDetails(final Integer storeNumber, final String ean) {

        Assert.notNull(storeNumber, "Parameter storeNumber should not be null.");
        Assert.notNull(ean, "Parameter ean should not be null.");

        // Try to retrieve the store and get the state - if unsuccessful then return error response
        final String state = getState(storeNumber);
        if (state == null) {
            return createSystemErrorResponse("Store number is not in the system");
        }

        final ProductRequestDto request = createRequest(storeNumber, ean, state);

        // Call pos
        try {
            final ProductResponseDto response = targetPOSProductClient.getPOSProductDetails(request);
            Assert.notNull(response);

            return convertResponse(response);
        }
        catch (final TargetIntegrationException ex) {
            return createSystemErrorResponse(ex.getMessage());
        }
    }


    protected ProductRequestDto createRequest(final Integer storeNumber, final String ean, final String state) {

        // Create request dto
        final ProductRequestDto dto = new ProductRequestDto();
        dto.setEan(ean);
        dto.setStoreNumber(storeNumber.toString());
        dto.setStoreState(state);
        return dto;
    }


    /**
     * Get state for the store number
     * 
     * @param storeNumber
     * @return null if store not found or state could not be read
     */
    protected String getState(final Integer storeNumber) {

        final TargetPointOfServiceModel posModel;

        try {
            posModel = targetPointOfServiceService.getPOSByStoreNumber(storeNumber);
        }
        catch (final TargetUnknownIdentifierException e) {
            LOG.warn(SplunkLogFormatter.formatMessage("Store could not be found: " + storeNumber,
                    TgtutilityConstants.ErrorCode.ERR_TGTSALE));
            return null;
        }
        catch (final TargetAmbiguousIdentifierException e) {
            LOG.warn(SplunkLogFormatter.formatMessage("Store could not be found: " + storeNumber,
                    TgtutilityConstants.ErrorCode.ERR_TGTSALE));
            return null;
        }

        if (posModel == null || posModel.getAddress() == null || posModel.getAddress().getDistrict() == null) {
            LOG.warn(SplunkLogFormatter.formatMessage("Store state could not be found for store: " + storeNumber,
                    TgtutilityConstants.ErrorCode.ERR_TGTSALE));
            return null;
        }

        return posModel.getAddress().getDistrict();
    }


    /**
     * Conversion of product response to POSProduct (trivial)
     * 
     * @param response
     * @return POSProduct
     */
    protected POSProduct convertResponse(final ProductResponseDto response) {

        final POSProduct posProduct = new POSProduct();

        if (!response.getSuccess()) {
            if (response.isTimeout()) {
                posProduct.setError(TIMEOUT);
            }
            else {
                posProduct.setError(response.getErrorMessage());
            }
        }
        else {

            posProduct.setDescription(response.getDescription());
            posProduct.setItemCode(response.getItemcode());
            posProduct.setPriceInCents(response.getPrice());
            posProduct.setWasPriceInCents(response.getWasPrice());
        }

        return posProduct;
    }


    private POSProduct createSystemErrorResponse(final String errorMessage) {

        final POSProduct posProduct = new POSProduct();
        posProduct.setSystemError(true);
        posProduct.setError(errorMessage);
        return posProduct;
    }


    /**
     * @param targetPOSProductClient
     */
    @Required
    public void setTargetPOSProductClient(final TargetPOSProductClient targetPOSProductClient) {
        this.targetPOSProductClient = targetPOSProductClient;
    }

    /**
     * @param targetPointOfServiceService
     */
    @Required
    public void setTargetPointOfServiceService(final TargetPointOfServiceService targetPointOfServiceService) {
        this.targetPointOfServiceService = targetPointOfServiceService;
    }

}

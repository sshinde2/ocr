/**
 * 
 */
package au.com.target.tgtsale.salesapplication.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;

import au.com.target.sale.salesapplication.TargetSalesSalesApplicationConfigService;
import au.com.target.tgtcore.model.SalesApplicationConfigModel;
import au.com.target.tgtcore.salesapplication.impl.TargetSalesApplicationConfigServiceImpl;


/**
 * Implementation of the extended TargetSalesApplicationConfigService methods in tgtsale.
 * 
 * @author jjayawa1
 *
 */
public class TargetSalesSalesApplicationConfigServiceImpl extends TargetSalesApplicationConfigServiceImpl implements
        TargetSalesSalesApplicationConfigService {
    @Override
    public boolean isSkipTlogs(final SalesApplication salesApp) {
        final SalesApplicationConfigModel salesAppConfig = getConfigForSalesApplication(salesApp);

        if (null != salesAppConfig && salesAppConfig.isSkipTlogs()) {
            return true;
        }

        return false;
    }
}

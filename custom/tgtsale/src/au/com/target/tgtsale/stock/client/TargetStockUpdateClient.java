/**
 * 
 */
package au.com.target.tgtsale.stock.client;

import au.com.target.tgtsale.stock.dto.request.StockUpdateDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateResponseDto;
import au.com.target.tgtwebcore.exception.TargetIntegrationException;



/**
 * @author rmcalave
 * 
 */
public interface TargetStockUpdateClient {

    /**
     * Retrieves stock levels for the specified products in the specified stores.
     * 
     * @param stockUpdateDto
     *            The products and stores to retrieve stock levels for
     * @return The stock levels for the provided products and stores
     */
    StockUpdateResponseDto getStockLevelsFromPos(StockUpdateDto stockUpdateDto) throws TargetIntegrationException;

}
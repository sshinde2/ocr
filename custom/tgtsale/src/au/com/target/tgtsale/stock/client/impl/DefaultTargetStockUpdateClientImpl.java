/**
 * 
 */
package au.com.target.tgtsale.stock.client.impl;

import org.apache.log4j.Logger;

import au.com.target.tgtsale.stock.client.TargetStockUpdateClient;
import au.com.target.tgtsale.stock.dto.request.StockUpdateDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateResponseDto;


/**
 * @author rmcalave
 * 
 */
public class DefaultTargetStockUpdateClientImpl implements TargetStockUpdateClient {

    private static final Logger LOG = Logger.getLogger(DefaultTargetStockUpdateClientImpl.class);

    /* (non-Javadoc)
     * @see au.com.target.tgtsale.stock.client.TargetStockUpdateClient#getStockLevelsFromPos(au.com.target.tgtsale.stock.dto.request.StockUpdateDto)
     */
    @Override
    public StockUpdateResponseDto getStockLevelsFromPos(final StockUpdateDto stockUpdateDto) {
        LOG.info("Called the default noop implementation.");
        return new StockUpdateResponseDto();
    }

}

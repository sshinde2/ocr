/**
 * 
 */
package au.com.target.tgtsale.stock.dto.request;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author rmcalave
 * 
 */
@XmlRootElement(name = "integration-stockUpdateProduct")
@XmlAccessorType(XmlAccessType.FIELD)
public class StockUpdateProductDto implements Serializable {
    @XmlElement(name = "itemcode")
    private String itemcode;

    @XmlElement(name = "ean")
    private String ean;

    /**
     * @return the itemcode
     */
    public String getItemcode() {
        return itemcode;
    }

    /**
     * @param itemcode
     *            the itemcode to set
     */
    public void setItemcode(final String itemcode) {
        this.itemcode = itemcode;
    }

    /**
     * @return the ean
     */
    public String getEan() {
        return ean;
    }

    /**
     * @param ean
     *            the ean to set
     */
    public void setEan(final String ean) {
        this.ean = ean;
    }
}

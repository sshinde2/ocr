/**
 * 
 */
package au.com.target.tgtsale.stock.dto.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author rmcalave
 * 
 */
@XmlRootElement(name = "integration-stockUpdateResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class StockUpdateResponseDto {
    @XmlElementWrapper(name = "stores")
    @XmlElement(name = "store")
    private List<StockUpdateStoreResponseDto> stockUpdateStoreResponseDtos;

    /**
     * @return the stockUpdateStoreResponseDtos
     */
    public List<StockUpdateStoreResponseDto> getStockUpdateStoreResponseDtos() {
        return stockUpdateStoreResponseDtos;
    }

    /**
     * @param stockUpdateStoreResponseDtos
     *            the stockUpdateStoreResponseDtos to set
     */
    public void setStockUpdateStoreResponseDtos(final List<StockUpdateStoreResponseDto> stockUpdateStoreResponseDtos) {
        this.stockUpdateStoreResponseDtos = stockUpdateStoreResponseDtos;
    }
}

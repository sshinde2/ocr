package au.com.target.tgtsale.storeconfig;

import java.util.List;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtsale.model.TargetStoreConfigModel;


/**
 *
 */
public interface TargetStoreConfigDao {
    /**
     * Find a list of store configuration based on section and tag
     * 
     * @param section
     *            name of the section or group to read
     * @param tag
     *            tag within the section
     * @return List<TargetStoreConfigModel>
     */
    List<TargetStoreConfigModel> getListStoreConfigs(final String section, final String tag);

    /**
     * Find a store configuration based on section, tag and occurance.<br/>
     * Note we misspell the word occurance is on purpose. This is because we mirror the logic from the POS team and they
     * spell it incorrectly.
     * 
     * @param section
     *            name of the section or group to read
     * @param tag
     *            tag within the section
     * @param occurance
     *            which occurance of the section/tag combination to get
     * @return TargetStoreConfigModel
     * 
     * @throws TargetUnknownIdentifierException
     *             when there is no record with the combination of section/tag/occurance
     * @throws TargetAmbiguousIdentifierException
     *             when there is more than one record with the combination of section/tag/occurance
     * 
     */
    TargetStoreConfigModel getStoreConfig(final String section, final String tag, final int occurance)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;

}

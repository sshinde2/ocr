/**
 * 
 */
package au.com.target.tgtsale.tlog;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.returns.model.OrderReturnRecordEntryModel;

import java.math.BigDecimal;
import java.util.List;


/**
 * @author vmuthura
 * 
 */
public interface TargetTransactionLogService {
    /**
     * @param paymentTransactionEntryModel
     *            - The payment entry for which TLOG should be published
     */
    void publishTlogForPayment(PaymentTransactionEntryModel paymentTransactionEntryModel);

    /**
     * @param orderModel
     *            - The order for which sale TLOG should be published
     */
    void publishTlogForOrderSale(OrderModel orderModel);

    /**
     * publish TLOG when pre-order fulfilment happens.
     * 
     * @param process
     */
    void publishTlogForPreOrderFulfilment(OrderProcessModel process);

    /**
     * @param orderCancelRecordEntryModel
     *            - The cancel request for which return TLOG should be published
     */
    void publishTlogForOrderCancel(OrderCancelRecordEntryModel orderCancelRecordEntryModel,
            List<PaymentTransactionEntryModel> refundTransactionEntries, BigDecimal refundAmountRemaining);

    /**
     * 
     * @param orderModel
     * @param refundTransactionEntries
     */
    void publishTlogForPreOrderCancel(OrderModel orderModel,
            List<PaymentTransactionEntryModel> refundTransactionEntries);


    /**
     * @param orderReturnRecordEntryModel
     *            - The return request for which return TLOG should be published
     */
    void publishTlogForOrderReturn(OrderReturnRecordEntryModel orderReturnRecordEntryModel,
            List<PaymentTransactionEntryModel> refundTransactionEntries, BigDecimal refundAmountRemaining);

    /**
     * @param orderCancelRecordEntryModel
     *            - The order for which reject TLOG should be published
     */
    void publishTlogForOrderRejected(OrderCancelRecordEntryModel orderCancelRecordEntryModel,
            List<PaymentTransactionEntryModel> refundTransactionEntries);

    /**
     * @param orderCancelRecordEntryModel
     *            - The order for which zero pick TLOG should be published
     */
    void publishTlogForOrderZeroPick(OrderCancelRecordEntryModel orderCancelRecordEntryModel,
            List<PaymentTransactionEntryModel> refundTransactionEntries);

}

/**
 * 
 */
package au.com.target.tgtsale.tlog.converter;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.result.PromotionOrderResults;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtsale.tlog.data.CustomerInfo;
import au.com.target.tgtsale.tlog.data.DealResults;
import au.com.target.tgtsale.tlog.data.DiscountInfo;
import au.com.target.tgtsale.tlog.data.OrderInfo;
import au.com.target.tgtsale.tlog.data.ShippingInfo;
import au.com.target.tgtsale.tlog.data.TenderInfo;
import au.com.target.tgtsale.tlog.data.Transaction;


/**
 * @author mjanarth
 *
 */
public interface TlogConverter {


    /**
     * 
     * @param orderModel
     * @return OrderInfo
     */
    public OrderInfo getOrderInfoFromOrderModel(final OrderModel orderModel, final String userName);

    /**
     * 
     * @param orderModel
     * @param promotionResults
     * @param deals
     * @param totalTmdiscount
     * @return DealResults
     */
    public DealResults getDetailsFromOrderEntriesNPromotions(final OrderModel orderModel,
            final PromotionOrderResults promotionResults, final Map<String, String> deals,
            final double totalTmdiscount);

    /**
     * 
     * @param customerModel
     * @param billingAddress
     * @param deliveryAddress
     * @param pos
     * @return CustomerInfo
     */
    public CustomerInfo getCustomerInfoFromTargetCustomerModel(final TargetCustomerModel customerModel,
            final AddressModel billingAddress, final AddressModel deliveryAddress, final TargetPointOfServiceModel pos);


    /**
     * Converts delivery cost to {@link ShippingInfo}
     * 
     * @param cost
     *            - Delivery cost
     * @return - {@link ShippingInfo}
     */
    public ShippingInfo getShippingInfo(final BigDecimal cost, final BigDecimal originalCost);

    /**
     * 
     * @param orderModRecordEntryModel
     * @param userName
     * @return Transaction
     */
    public Transaction getTransactionForModificationRecordEntry(
            final OrderModificationRecordEntryModel orderModRecordEntryModel, final String userName,
            final List<PaymentTransactionEntryModel> refundPaymentTransactions, BigDecimal refundAmountRemaining);

    /**
     * 
     * @param tMDiscountModel
     * @param code
     * @param amount
     * @return DiscountInfo
     */
    public DiscountInfo getDiscountInfoFromPromotion(final ProductPromotionModel tMDiscountModel,
            final String code, final BigDecimal amount);

    /**
     * 
     * @param paymentTransactionEntryModel
     * @param refund
     * @return TenderInfo
     */
    public TenderInfo getTenderInfoFromPaymentEntry(
            final PaymentTransactionEntryModel paymentTransactionEntryModel, final boolean refund);

    /**
     * Gets the all tmd from promotion results.
     * 
     * @param promotionOrderResults
     *            the order model
     * @return the all tmd from promotion results
     */
    public Map<PromotionResult, Double> getAllTMDFromPromotionResults(
            final PromotionOrderResults promotionOrderResults);

    /**
     * Add the transaction type and laybyVersion,type for the transaction
     * 
     * @param purchaseOptionConfig
     * @param transaction
     * @param isPreOrder
     */
    void addTransactionType(PurchaseOptionConfigModel purchaseOptionConfig, Transaction transaction,
            Boolean isPreOrder);

    /**
     * Add the layby information for the transaction
     * 
     * @param orderModel
     * @param transaction
     * @param paymentAmount
     */
    void addLaybyInformationDetails(OrderModel orderModel, Transaction transaction, BigDecimal paymentAmount);

    /**
     * Add customer info and layby fee and payment amount for preorder transaction
     * 
     * @param orderModel
     * @param transaction
     * @param paymentAmount
     */
    void addPaymentAndCustomerInfoForPreOrder(OrderModel orderModel, Transaction transaction, BigDecimal paymentAmount);

    /**
     * Populates the transaction with tender for pre-order cancellation
     * 
     * @param orderModel
     * @param refundPaymentTransactions
     * @return Transaction
     */
    public Transaction getTransactionForPreOrderCancel(OrderModel orderModel,
            final List<PaymentTransactionEntryModel> refundPaymentTransactions);

    /**
     * Populates the transaction with tender for pre-order fulfilmnent
     * 
     * @param orderModel
     * @param process
     * @return transaction
     */
    Transaction getTransactionForPreOrderFulfilment(OrderProcessModel process, OrderModel orderModel);
}

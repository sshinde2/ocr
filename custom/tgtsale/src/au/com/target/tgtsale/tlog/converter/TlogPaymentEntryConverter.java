/**
 * 
 */
package au.com.target.tgtsale.tlog.converter;

import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import au.com.target.tgtsale.tlog.data.TenderInfo;


/**
 * @author mjanarth
 *
 */
public interface TlogPaymentEntryConverter {

    /**
     * Converts a {@link PaymentTransactionEntryModel} to {@link TenderInfo}
     * 
     * @param paymentTransactionEntryModel
     *            - A {@link PaymentTransactionEntryModel}
     * @param refund
     *            - If it is a refund transaction
     * @return - {@link TenderInfo}
     */
    public TenderInfo getTenderInfoFromPaymentEntry(
            final PaymentTransactionEntryModel paymentTransactionEntryModel, final boolean refund);
}

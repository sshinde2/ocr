/**
 * 
 */
package au.com.target.tgtsale.tlog.converter;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.model.TargetPromotionOrderEntryConsumedModel;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import au.com.target.tgtsale.tlog.data.DealResults;
import au.com.target.tgtsale.tlog.data.DiscountInfo;
import au.com.target.tgtsale.tlog.data.ItemInfo;


/**
 * @author mjanarth
 *
 */
public interface TlogSaleConverter {

    /**
     * Convert a {@link AbstractOrderEntryModel} to {@link ItemInfo}
     * 
     * @param orderEntryModel
     *            - a {@link AbstractOrderEntryModel}
     * @param promotionResult
     * @param promoResultModels
     *            - if it is a refund
     * @return - {@link ItemInfo}
     */
    public List<ItemInfo> getItemInfoFromOrderEntry(final AbstractOrderEntryModel orderEntryModel,
            final PromotionResult promotionResult, final Set<PromotionResultModel> promoResultModels,
            final DealResults dealResults, final Map<String, String> deals);

    /**
     * @param orderEntryModel
     * @param entryPromoResultsModel
     * @param promotionResult
     * @param deals
     * @param dealResults
     * @return collection of items both Abstract deals and TMD
     */
    public Collection<? extends ItemInfo> getItemsForConsumedEntriesAndTmd(
            final AbstractOrderEntryModel orderEntryModel,
            final List<TargetPromotionOrderEntryConsumedModel> entryPromoResultsModel,
            final PromotionResult promotionResult, final DealResults dealResults,
            final Map<String, String> deals);

    /**
     * 
     * @param tMDiscountModel
     * @param code
     * @param amount
     * @return DiscountInfo
     */
    public DiscountInfo getDiscountInfoFromPromotion(final ProductPromotionModel tMDiscountModel,
            final String code, final BigDecimal amount);

}

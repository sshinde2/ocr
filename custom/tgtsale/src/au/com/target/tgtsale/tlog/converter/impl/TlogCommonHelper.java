/**
 * 
 */
package au.com.target.tgtsale.tlog.converter.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.promotions.model.TargetPromotionOrderEntryConsumedModel;

import au.com.target.tgtcore.util.DealUtils;


/**
 * @author mjanarth
 *
 */
public final class TlogCommonHelper {

    private TlogCommonHelper() {
        //
    }


    public static int getMarkdownInCents(final AbstractOrderEntryModel orderEntry,
            final TargetPromotionOrderEntryConsumedModel entryConsumedModel) {

        final int markdown = (int)Math
                .round(DealUtils.getTotalDiscounts(orderEntry, entryConsumedModel) * 100);
        return markdown;
    }

}

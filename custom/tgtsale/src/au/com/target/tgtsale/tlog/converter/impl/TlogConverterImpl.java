/**
 * 
 */
package au.com.target.tgtsale.tlog.converter.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.result.PromotionOrderResults;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.jalo.TMDiscountProductPromotion;
import au.com.target.tgtcore.jalo.TMDiscountPromotion;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtlayby.data.PaymentDueData;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.order.TargetLaybyPaymentDueService;
import au.com.target.tgtpayment.util.PaymentTransactionHelper;
import au.com.target.tgtsale.constants.TgtsaleConstants;
import au.com.target.tgtsale.tlog.converter.TlogCancelConverter;
import au.com.target.tgtsale.tlog.converter.TlogConverter;
import au.com.target.tgtsale.tlog.converter.TlogCustomerConverter;
import au.com.target.tgtsale.tlog.converter.TlogPaymentEntryConverter;
import au.com.target.tgtsale.tlog.converter.TlogSaleConverter;
import au.com.target.tgtsale.tlog.converter.TlogTMDConverter;
import au.com.target.tgtsale.tlog.data.CustomerInfo;
import au.com.target.tgtsale.tlog.data.DealResults;
import au.com.target.tgtsale.tlog.data.DiscountInfo;
import au.com.target.tgtsale.tlog.data.Flybuys;
import au.com.target.tgtsale.tlog.data.ItemInfo;
import au.com.target.tgtsale.tlog.data.LayByFee;
import au.com.target.tgtsale.tlog.data.OrderInfo;
import au.com.target.tgtsale.tlog.data.Payment;
import au.com.target.tgtsale.tlog.data.ShippingInfo;
import au.com.target.tgtsale.tlog.data.TenderInfo;
import au.com.target.tgtsale.tlog.data.TenderTypeEnum;
import au.com.target.tgtsale.tlog.data.Transaction;
import au.com.target.tgtsale.tlog.data.TransactionTypeEnum;


/**
 * Helper class to convert hybris models to JAXB annotated POJO's to generate TLOG XML. The individual methods can
 * become a converter by themselves, at some point of time.
 * 
 * @author vmuthura
 * 
 */
public class TlogConverterImpl implements TlogConverter {

    private static final Logger LOG = Logger.getLogger(TlogConverterImpl.class);

    private TlogSaleConverter tlogSaleConverter;
    private TlogTMDConverter tlogTMDConverter;
    private TlogPaymentEntryConverter tlogPaymentEntryConverter;
    private TlogCustomerConverter tlogCustomerConverter;
    private TlogCancelConverter tlogCancelConverter;
    private TargetLaybyPaymentDueService targetLaybyPaymentDueService;
    private TargetPointOfServiceService targetPointOfServiceService;


    /**
     * 
     * @param orderModel
     * @return OrderInfo
     */
    @Override
    public OrderInfo getOrderInfoFromOrderModel(final OrderModel orderModel, final String userName) {
        if (orderModel == null) {
            return null;
        }

        final OrderInfo order = new OrderInfo();
        order.setId(orderModel.getCode());

        if (StringUtils.isNotBlank(userName)) {
            order.setUserName(userName);
        }

        return order;
    }



    /**
     * 
     * @param orderModel
     * @param promotionResults
     * @param deals
     * @param totalTmdiscount
     * @return DealResults
     */
    @Override
    public DealResults getDetailsFromOrderEntriesNPromotions(final OrderModel orderModel,
            final PromotionOrderResults promotionResults, final Map<String, String> deals,
            final double totalTmdiscount) {
        if (orderModel == null || orderModel.getEntries() == null) {
            return null;
        }

        final Set<PromotionResultModel> promoResultModels = orderModel.getAllPromotionResults();

        final List<AbstractOrderEntryModel> orderEntries = orderModel.getEntries();

        final DealResults dealResults = new DealResults();

        final List<ItemInfo> itemList = new LinkedList<>();
        for (final AbstractOrderEntryModel orderEntryModel : orderEntries) {
            PromotionResult promotionResult = null;
            if (null != promotionResults) {
                promotionResult = tlogTMDConverter.getTMDPromotionResultForCurrentEntry(orderEntryModel,
                        promotionResults.getAllResults());
            }

            final List<ItemInfo> item = tlogSaleConverter.getItemInfoFromOrderEntry(orderEntryModel,
                    promotionResult,
                    promoResultModels, dealResults, deals);
            if (item != null) {
                itemList.addAll(item);
            }
        }

        final double variationAmtTmd = tlogTMDConverter.variationInTmDiscount(itemList, totalTmdiscount);
        tlogTMDConverter.addVariationAmtToTMDDiscount(itemList, variationAmtTmd);
        dealResults.setItems(itemList);
        return dealResults;
    }


    /**
     * 
     * @param customerModel
     * @param billingAddress
     * @param deliveryAddress
     * @param pos
     * @return CustomerInfo
     */
    @Override
    public CustomerInfo getCustomerInfoFromTargetCustomerModel(final TargetCustomerModel customerModel,
            final AddressModel billingAddress, final AddressModel deliveryAddress,
            final TargetPointOfServiceModel pos) {
        return tlogCustomerConverter.getCustomerInfoFromTargetCustomerModel(customerModel, billingAddress,
                deliveryAddress, pos);
    }

    /**
     * Converts delivery cost to {@link ShippingInfo}
     * 
     * @param cost
     *            - Delivery cost
     * @return - {@link ShippingInfo}
     */
    @Override
    public ShippingInfo getShippingInfo(final BigDecimal cost, final BigDecimal originalCost) {
        if (cost == null) {
            return null;
        }

        final ShippingInfo shippingInfo = new ShippingInfo();
        shippingInfo.setAmount(cost);
        if (originalCost != null) {
            shippingInfo.setOriginalAmount(originalCost);
        }

        return shippingInfo;
    }


    /**
     * 
     * @param orderModRecordEntryModel
     * @param userName
     * @return Transaction
     */
    @Override
    public Transaction getTransactionForModificationRecordEntry(
            final OrderModificationRecordEntryModel orderModRecordEntryModel, final String userName,
            final List<PaymentTransactionEntryModel> refundPaymentTransactions,
            final BigDecimal refundAmountRemaining) {
        final OrderModel orderModel = orderModRecordEntryModel.getModificationRecord().getOrder();
        final Transaction transaction = new Transaction();

        BigDecimal shippingAmount = BigDecimal.ZERO;
        Double refundedAmount = orderModRecordEntryModel.getRefundedAmount();
        BigDecimal tenderAmount = BigDecimal.ZERO;

        if (orderModRecordEntryModel.getRefundedShippingAmount() != null) {
            shippingAmount = BigDecimal.valueOf(orderModRecordEntryModel.getRefundedShippingAmount()
                    .doubleValue());
            if (null != tlogCancelConverter.getFlybuysRedeemCode(orderModel)
                    && null != orderModRecordEntryModel.getRefundedFlybuysAmount()) {
                refundedAmount = Double.valueOf(refundedAmount.doubleValue()
                        + orderModRecordEntryModel.getRefundedFlybuysAmount().doubleValue());
            }
            if (refundedAmount == null || refundedAmount.doubleValue() < shippingAmount.doubleValue()) {
                shippingAmount = BigDecimal.ZERO;
            }
            transaction.setShippingInfo(getShippingInfo(
                    shippingAmount.negate(),
                    null));
        }
        else {
            transaction.setShippingInfo(getShippingInfo(BigDecimal.ZERO, null));
        }

        //tender list
        if (refundedAmount != null
                && refundedAmount.doubleValue() > 0) {
            final List<TenderInfo> tenderList = new ArrayList<>();
            tenderAmount = tlogCancelConverter.getTenderInfoForRefund(tenderList, refundPaymentTransactions);

            // If there is a refund amount remaining, a ticket will have been raised for it to be refunded manually.
            // In this case, we will send this amount in the TLOG as cash, so that the TLOG values are correct.
            // When the manual refund is processed, no TLOG will be sent.
            if (refundAmountRemaining != null && refundAmountRemaining.compareTo(BigDecimal.ZERO) > 0) {
                final TenderInfo refundAmountRemainingTender = new TenderInfo();
                refundAmountRemainingTender.setType(TenderTypeEnum.CASH);
                refundAmountRemainingTender.setAmount(refundAmountRemaining.negate());
                refundAmountRemainingTender.setProcessedTime(new Date());
                refundAmountRemainingTender.setApproved("Y");

                tenderList.add(refundAmountRemainingTender);
                tenderAmount = tenderAmount.subtract(refundAmountRemaining);
            }

            transaction.setTender(tenderList.isEmpty() ? null : tenderList);
            tenderAmount = tenderAmount.subtract(shippingAmount.negate());
        }

        transaction.setType(TransactionTypeEnum.REFUND);
        transaction.setTimestamp(orderModRecordEntryModel.getTimestamp());
        transaction.setOrder(getOrderInfoFromOrderModel(orderModel, userName));
        final List<ItemInfo> itemListForModifiedEntries = tlogCancelConverter.getItemListForModifiedEntries(
                orderModRecordEntryModel.getOrderEntriesModificationEntries(), tenderAmount,
                null != tlogCancelConverter.getFlybuysRedeemCode(orderModel));
        transaction.setItems(itemListForModifiedEntries);
        transaction.setFlybuys(new Flybuys(orderModel.getFlyBuysCode()));
        tlogCancelConverter.addFlybuysDiscountInfo(transaction, orderModRecordEntryModel,
                orderModel.getFlyBuysCode());

        return transaction;
    }


    /**
     * 
     * @param tMDiscountModel
     * @param code
     * @param amount
     * @return DiscountInfo
     */
    @Override
    public DiscountInfo getDiscountInfoFromPromotion(final ProductPromotionModel tMDiscountModel,
            final String code, final BigDecimal amount) {
        return tlogTMDConverter.getDiscountInfoFromPromotion(tMDiscountModel, code, amount);
    }

    /**
     * 
     * @param paymentTransactionEntryModel
     * @param refund
     * @return TenderInfo
     */
    @Override
    public TenderInfo getTenderInfoFromPaymentEntry(
            final PaymentTransactionEntryModel paymentTransactionEntryModel, final boolean refund) {
        final TenderInfo tenderInfo = tlogPaymentEntryConverter.getTenderInfoFromPaymentEntry(
                paymentTransactionEntryModel, refund);
        return tenderInfo;
    }

    /**
     * Gets the all tmd from promotion results.
     * 
     * @param promotionOrderResults
     *            the order model
     * @return the all tmd from promotion results
     */
    @Override
    public Map<PromotionResult, Double> getAllTMDFromPromotionResults(
            final PromotionOrderResults promotionOrderResults) {
        if (promotionOrderResults == null || CollectionUtils.isEmpty(promotionOrderResults.getAllResults())) {
            return null;
        }

        final Map<PromotionResult, Double> promotionResultsAndDiscount = new HashMap<PromotionResult, Double>();

        for (final PromotionResult promotionResult : promotionOrderResults.getAllResults()) {
            if (promotionResult.isApplied()
                    && (promotionResult.getPromotion() instanceof TMDiscountPromotion
                            || promotionResult.getPromotion() instanceof TMDiscountProductPromotion)) {
                final PromotionResult existingResultInMap = getExistingTMDFromMap(promotionResultsAndDiscount.keySet(),
                        promotionResult);
                if (null == existingResultInMap) {
                    promotionResultsAndDiscount
                            .put(promotionResult, Double.valueOf(promotionResult.getTotalDiscount()));
                }
                else {
                    final Double previousDiscount = promotionResultsAndDiscount.get(existingResultInMap);
                    promotionResultsAndDiscount
                            .put(existingResultInMap,
                                    Double.valueOf(previousDiscount == null ? 0 : previousDiscount.doubleValue()
                                            + promotionResult.getTotalDiscount()));
                }
            }
        }

        return promotionResultsAndDiscount;
    }

    /**
     * 
     * @param transaction
     */
    @Override
    public void addPaymentAndCustomerInfoForPreOrder(final OrderModel orderModel, final Transaction transaction,
            final BigDecimal paymentAmount) {

        final AddressModel paymentAddress = orderModel.getPaymentAddress() != null ? orderModel.getPaymentAddress()
                : orderModel.getDeliveryAddress();
        transaction.setCustomerInfo(tlogCustomerConverter.getCustomerInfoFromTargetCustomerModel(
                (TargetCustomerModel)orderModel.getUser(), paymentAddress,
                orderModel.getDeliveryAddress(), null));
        transaction.setPayment(new Payment(paymentAmount));
        //The layby fee will be always zero for pre order 
        transaction.setLaybyFee(new LayByFee(BigDecimal.ZERO));
    }


    /**
     * @param orderModel
     * @param transaction
     * @param paymentAmount
     */
    @Override
    public void addLaybyInformationDetails(final OrderModel orderModel,
            final Transaction transaction,
            final BigDecimal paymentAmount) {

        final PaymentDueData paymentDueData = targetLaybyPaymentDueService.getFinalPaymentDue(orderModel);
        if (paymentDueData != null && paymentDueData.getDueDate() != null) {
            transaction.setDueDate(paymentDueData.getDueDate());
        }
        TargetPointOfServiceModel pos = null;
        if (orderModel.getCncStoreNumber() != null) {
            try {
                pos = targetPointOfServiceService.getPOSByStoreNumber(orderModel.getCncStoreNumber());
            }
            catch (final TargetUnknownIdentifierException e) {
                LOG.warn("Unknown store number: " + orderModel.getCncStoreNumber());
            }
            catch (final TargetAmbiguousIdentifierException e) {
                LOG.warn("Multiple stores found for store number : " + orderModel.getCncStoreNumber());
            }
        }

        transaction.setCustomerInfo(tlogCustomerConverter.getCustomerInfoFromTargetCustomerModel(
                (TargetCustomerModel)orderModel.getUser(), orderModel.getPaymentAddress(),
                orderModel.getDeliveryAddress(), pos));
        if (orderModel.getLayByFee() != null) {
            transaction.setLaybyFee(new LayByFee(new BigDecimal(orderModel.getLayByFee().doubleValue())));
        }

        transaction.setPayment(new Payment(paymentAmount));
    }


    /**
     * @param purchaseOptionConfig
     * @param transaction
     */
    @Override
    public void addTransactionType(final PurchaseOptionConfigModel purchaseOptionConfig,
            final Transaction transaction, final Boolean isPreOrder) {
        if (BooleanUtils.isTrue(isPreOrder)) {
            transaction
                    .setType(TransactionTypeEnum.LAYBY_CREATE);
            transaction.setLaybyVersion(TgtsaleConstants.TLOG.PRE_ORDER_LAYBY_VERSION);
            transaction.setLaybyType(TgtsaleConstants.TLOG.PRE_ORDER_LAYBY_TYPE);
        }
        else {
            transaction
                    .setType(
                            purchaseOptionConfig.getAllowPaymentDues().booleanValue() ? TransactionTypeEnum.LAYBY_CREATE
                                    : TransactionTypeEnum.SALE);
            transaction.setLaybyType(purchaseOptionConfig.getPosCode());
        }

    }



    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogConverter#getTransactionForPreOrderCancel(de.hybris.platform.core.model.order.OrderModel, java.util.List)
     */
    @Override
    public Transaction getTransactionForPreOrderCancel(final OrderModel orderModel,
            final List<PaymentTransactionEntryModel> refundPaymentTransactions) {
        final Transaction transaction = new Transaction();
        transaction.setOrder(getOrderInfoFromOrderModel(orderModel, null));
        transaction.setTimestamp(orderModel.getDate());
        transaction.setType(TransactionTypeEnum.LAYBY_CANCEL);
        final List<TenderInfo> tenderList = new ArrayList<>();
        //populates the tender
        tlogCancelConverter.getTenderInfoForRefund(tenderList, refundPaymentTransactions);
        transaction.setTender(tenderList);
        return transaction;
    }

    @Override
    public Transaction getTransactionForPreOrderFulfilment(final OrderProcessModel process,
            final OrderModel orderModel) {
        final Transaction transaction = new Transaction();
        transaction.setOrder(getOrderInfoFromOrderModel(orderModel, null));
        transaction.setTimestamp(process.getCreationtime());
        transaction.setType(TransactionTypeEnum.LAYBY_PAYMENT);

        final List<TenderInfo> tenderList = new ArrayList<>();
        final PaymentTransactionEntryModel paymentEntry = PaymentTransactionHelper
                .findPreOrderBalanceAmountCaptureTransactionEntry(orderModel);
        final TenderInfo tenderInfo = tlogPaymentEntryConverter
                .getTenderInfoFromPaymentEntry(paymentEntry, false);
        tenderList.add(tenderInfo);
        transaction.setTender(tenderList);

        final BigDecimal balanceAmountCaptured = paymentEntry != null ? paymentEntry.getAmount() : BigDecimal.ZERO;
        transaction.setPayment(new Payment(balanceAmountCaptured));

        return transaction;
    }


    /**
     * Gets the existing tmd from map.
     * 
     * @param promotionResults
     *            the promotion results
     * @param promotionResult
     *            the promotion result
     * @return the existing tmd from map
     */
    private PromotionResult getExistingTMDFromMap(final Set<PromotionResult> promotionResults,
            final PromotionResult promotionResult) {
        for (final PromotionResult result : promotionResults) {
            if (StringUtils.equals(result.getPromotion().getCode(), promotionResult.getPromotion().getCode())) {
                return result;
            }
        }
        return null;
    }



    /**
     * @param tlogSaleConverter
     *            the tlogSaleConverter to set
     */
    @Required
    public void setTlogSaleConverter(final TlogSaleConverter tlogSaleConverter) {
        this.tlogSaleConverter = tlogSaleConverter;
    }



    /**
     * @param tlogTMDConverter
     *            the tlogTMDConverter to set
     */
    @Required
    public void setTlogTMDConverter(final TlogTMDConverter tlogTMDConverter) {
        this.tlogTMDConverter = tlogTMDConverter;
    }



    /**
     * @param tlogPaymentEntryConverter
     *            the tlogPaymentEntryConverter to set
     */
    @Required
    public void setTlogPaymentEntryConverter(final TlogPaymentEntryConverter tlogPaymentEntryConverter) {
        this.tlogPaymentEntryConverter = tlogPaymentEntryConverter;
    }



    /**
     * @param tlogCustomerConverter
     *            the tlogCustomerConverter to set
     */
    @Required
    public void setTlogCustomerConverter(final TlogCustomerConverter tlogCustomerConverter) {
        this.tlogCustomerConverter = tlogCustomerConverter;
    }



    /**
     * @param tlogCancelConverter
     *            the tlogCancelConverter to set
     */
    @Required
    public void setTlogCancelConverter(final TlogCancelConverter tlogCancelConverter) {
        this.tlogCancelConverter = tlogCancelConverter;
    }



    /**
     * @param targetLaybyPaymentDueService
     *            the targetLaybyPaymentDueService to set
     */
    public void setTargetLaybyPaymentDueService(final TargetLaybyPaymentDueService targetLaybyPaymentDueService) {
        this.targetLaybyPaymentDueService = targetLaybyPaymentDueService;
    }



    /**
     * @param targetPointOfServiceService
     *            the targetPointOfServiceService to set
     */
    public void setTargetPointOfServiceService(final TargetPointOfServiceService targetPointOfServiceService) {
        this.targetPointOfServiceService = targetPointOfServiceService;
    }
}

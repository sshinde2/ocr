/**
 * 
 */
package au.com.target.tgtsale.tlog.converter.impl;

import de.hybris.platform.core.model.user.AddressModel;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtsale.tlog.converter.TlogCustomerConverter;
import au.com.target.tgtsale.tlog.data.AddressInfo;
import au.com.target.tgtsale.tlog.data.AddressTypeEnum;
import au.com.target.tgtsale.tlog.data.CustomerInfo;



/**
 * @author mjanarth
 *
 */
public class TlogCustomerConverterImpl implements TlogCustomerConverter {


    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogCustomerConverter#getAddressInfoFromAddressModel(de.hybris.platform.core.model.user.AddressModel, au.com.target.tgtsale.tlog.data.AddressTypeEnum, au.com.target.tgtcore.model.TargetPointOfServiceModel)
     */
    @Override
    public AddressInfo getAddressInfoFromAddressModel(final AddressModel address,
            final AddressTypeEnum type, final TargetPointOfServiceModel targetPOS)
    {
        if (address == null)
        {
            return null;
        }
        final AddressInfo addressInfo = new AddressInfo();
        if (targetPOS != null)
        {
            addressInfo.setStore(String.valueOf(targetPOS.getStoreNumber().intValue()));
            addressInfo.setAddressLine1(targetPOS.getType().getCode() + "-" + targetPOS.getName());
            addressInfo.setAddressLine2(address.getLine1());
        }
        else
        {
            addressInfo.setAddressLine1(address.getLine1());
            addressInfo.setAddressLine2(address.getLine2());
        }
        addressInfo.setCity(address.getTown());
        addressInfo.setCountry(address.getCountry().getName());
        addressInfo.setPostCode(address.getPostalcode());
        //POS expects a mobile number for Layby delivery address (for sending SMS) and the front end does NOT distinguish
        //between a mobile and land line number
        if (AddressTypeEnum.DELIVERY.equals(type))
        {
            addressInfo.setMobile(StringUtils.isNotBlank(address.getCellphone()) ? address.getCellphone() : address
                    .getPhone1());
        }
        else
        {
            addressInfo.setPhone(address.getPhone1());
            addressInfo.setMobile(address.getCellphone());
        }
        addressInfo.setType(type);

        return addressInfo;
    }



    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogCustomerConverter#getCustomerInfoFromTargetCustomerModel(au.com.target.tgtcore.model.TargetCustomerModel, de.hybris.platform.core.model.user.AddressModel, de.hybris.platform.core.model.user.AddressModel, au.com.target.tgtcore.model.TargetPointOfServiceModel)
     */
    @Override
    public CustomerInfo getCustomerInfoFromTargetCustomerModel(final TargetCustomerModel customerModel,
            final AddressModel billingAddress, final AddressModel deliveryAddress, final TargetPointOfServiceModel pos)
    {
        if (customerModel == null)
        {
            return null;
        }

        final CustomerInfo customerInfo = new CustomerInfo();
        customerInfo.setId(customerModel.getCustomerID());
        customerInfo.setTitle(customerModel.getTitle() == null ? null : customerModel.getTitle().getName());
        customerInfo.setFirstName(customerModel.getFirstname());
        customerInfo.setLastName(customerModel.getLastname());
        customerInfo.setEmail(customerModel.getContactEmail());

        final List<AddressInfo> addressList = new LinkedList<>();
        if (billingAddress != null)
        {
            addressList
                    .add(getAddressInfoFromAddressModel(billingAddress,
                            AddressTypeEnum.BILLING,
                            null));
        }

        if (deliveryAddress != null)
        {
            addressList.add(getAddressInfoFromAddressModel(deliveryAddress,
                    AddressTypeEnum.DELIVERY, pos));
        }

        if (!addressList.isEmpty()) {
            customerInfo.setAddress(addressList);
        }

        return customerInfo;
    }


}

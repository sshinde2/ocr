/**
 * 
 */
package au.com.target.tgtsale.tlog.converter.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.model.TargetPromotionOrderEntryConsumedModel;

import java.io.InvalidObjectException;
import java.text.MessageFormat;
import java.util.Map;

import au.com.target.tgtcore.enums.DealItemTypeEnum;
import au.com.target.tgtsale.tlog.converter.TlogDealConverter;
import au.com.target.tgtsale.tlog.data.DealResults;
import au.com.target.tgtsale.tlog.data.ItemDeal;


/**
 * @author mjanarth
 *
 */
public class TlogDealConverterImpl implements TlogDealConverter {


    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogDealConverter#populateItemDeal(de.hybris.platform.core.model.order.AbstractOrderEntryModel, de.hybris.platform.promotions.model.TargetPromotionOrderEntryConsumedModel, au.com.target.tgtsale.tlog.data.DealResults, java.util.Map)
     */
    @Override
    public ItemDeal populateItemDeal(final AbstractOrderEntryModel orderEntry,
            final TargetPromotionOrderEntryConsumedModel entryConsumedModel,
            final DealResults dealResults, final Map<String, String> deals) {
        final PromotionResultModel entryPromoResultModel = entryConsumedModel.getPromotionResult();
        final AbstractPromotionModel promotion = entryPromoResultModel.getPromotion();
        if (promotion instanceof AbstractDealModel) {
            final String promoCode = promotion.getCode();
            final String instance = entryConsumedModel.getInstance().toString();
            final DealItemTypeEnum dealItemType = entryConsumedModel.getDealItemType();

            if (dealItemType == null) {
                return null;
            }

            final ItemDeal itemDeal = new ItemDeal();
            itemDeal.setId(promoCode);
            itemDeal.setInstance(instance);
            itemDeal.setType(dealItemType.getCode().substring(0, 1));
            final int markdown = TlogCommonHelper.getMarkdownInCents(orderEntry, entryConsumedModel);
            itemDeal.setMarkdown(Integer.toString(markdown));
            String type;
            try {
                type = getTransDealType(promotion, deals);
            }
            catch (final InvalidObjectException e) {
                // LOG.error("An error has occured when getting the Transaction Deal Type for TLOG", e);
                type = null;
            }
            final int markdownForInstance = markdown * ((int)entryConsumedModel.getQuantity().doubleValue());
            dealResults.addDealDetailsForInstance(instance, promoCode, type, markdownForInstance);
            return itemDeal;
        }
        return null;
    }

    /**
     * Deal type, valid types are: 1 = Buy/Get 2 = Buy/Get (buy and get list are the same) 3 = Spend/Save 4 = Spend/Get
     * 5 = Qty Break 6 = Value Bundle
     * 
     * @param promotion
     * @param deals
     * @return transDealType
     * @throws InvalidObjectException
     */
    private String getTransDealType(final AbstractPromotionModel promotion, final Map<String, String> deals)
            throws InvalidObjectException {
        final String className = promotion.getClass().getSimpleName();
        final String transDealType = deals.get(className);
        if (transDealType == null) {
            throw new InvalidObjectException(MessageFormat.format(
                    "Cannot find the dealType for the class {} as it may not be configured in the deals for TLOG.",
                    className));
        }
        return transDealType;
    }


}

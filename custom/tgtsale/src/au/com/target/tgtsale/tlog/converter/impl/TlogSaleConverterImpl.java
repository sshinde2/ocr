/**
 * 
 */
package au.com.target.tgtsale.tlog.converter.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.model.TargetPromotionOrderEntryConsumedModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.currency.conversion.service.CurrencyConversionFactorService;
import au.com.target.tgtcore.jalo.TMDiscountProductPromotion;
import au.com.target.tgtcore.model.TMDiscountPromotionModel;
import au.com.target.tgtsale.tlog.converter.TlogDealConverter;
import au.com.target.tgtsale.tlog.converter.TlogSaleConverter;
import au.com.target.tgtsale.tlog.converter.TlogTMDConverter;
import au.com.target.tgtsale.tlog.data.DealResults;
import au.com.target.tgtsale.tlog.data.DiscountInfo;
import au.com.target.tgtsale.tlog.data.ItemDeal;
import au.com.target.tgtsale.tlog.data.ItemInfo;


/**
 * @author mjanarth
 *
 */

public class TlogSaleConverterImpl implements TlogSaleConverter {

    private TlogTMDConverter tlogTMDConverter;

    private TlogDealConverter tlogDealConverter;

    private CurrencyConversionFactorService currencyConversionFactorService;



    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogSaleConverter#getItemInfoFromOrderEntry(de.hybris.platform.core.model.order.AbstractOrderEntryModel, de.hybris.platform.promotions.jalo.PromotionResult, java.util.Set, au.com.target.tgtsale.tlog.data.DealResults, java.util.Map)
     */
    @Override
    public List<ItemInfo> getItemInfoFromOrderEntry(final AbstractOrderEntryModel orderEntryModel,
            final PromotionResult promotionResult, final Set<PromotionResultModel> promoResultModels,
            final DealResults dealResults, final Map<String, String> deals)
    {
        final List<ItemInfo> items = new ArrayList<>();
        if (orderEntryModel == null)
        {
            return null;
        }

        final List<TargetPromotionOrderEntryConsumedModel> entryPromoResultsModel = findPromotionForEntry(
                orderEntryModel.getEntryNumber(),
                promoResultModels);

        //if there are no promotionResults then follow normal procedure
        if (CollectionUtils.isEmpty(promoResultModels) && CollectionUtils.isEmpty(entryPromoResultsModel)) {
            final ItemInfo item = new ItemInfo();
            item.setId(orderEntryModel.getProduct().getCode());
            item.setQuantity(orderEntryModel.getQuantity());
            final BigDecimal price = BigDecimal.valueOf(currencyConversionFactorService.convertPriceToBaseCurrency(
                    orderEntryModel.getBasePrice().doubleValue(), getOrderModelFromOrderEntry(orderEntryModel)));
            item.setPrice(price);
            item.setFilePrice(price);
            items.add(item);
            return items;
        }
        else {
            items.addAll(getItemsForConsumedEntriesAndTmd(orderEntryModel,
                    entryPromoResultsModel, promotionResult,
                    dealResults, deals));
        }

        return items;
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogSaleConverter#getItemsForConsumedEntriesAndTmd(de.hybris.platform.core.model.order.AbstractOrderEntryModel, java.util.List, de.hybris.platform.promotions.jalo.PromotionResult, au.com.target.tgtsale.tlog.data.DealResults, java.util.Map)
     */
    @Override
    public Collection<? extends ItemInfo> getItemsForConsumedEntriesAndTmd(
            final AbstractOrderEntryModel orderEntryModel,
            final List<TargetPromotionOrderEntryConsumedModel> entryPromoResultsModel,
            final PromotionResult promotionResult, final DealResults dealResults,
            final Map<String, String> deals) {
        final List<ItemInfo> items = new ArrayList<>();
        long actualQuantity = orderEntryModel.getQuantity().longValue();
        double percentage = 0.0;

        final TMDiscountPromotionModel tmd = tlogTMDConverter
                .getTMDFromPromotionResults((OrderModel)orderEntryModel.getOrder());
        final TMDiscountProductPromotion newTmd = tlogTMDConverter
                .getNewTMDFromPromotionResults(promotionResult);
        if (tmd != null) {
            final Double percentageDiscount = tlogTMDConverter.getTmdPercentage(tmd);
            if (percentageDiscount != null) {
                percentage = percentageDiscount.doubleValue();
            }
        }

        final double totalOrderEntryValue = orderEntryModel.getTotalPrice().doubleValue();

        if (CollectionUtils.isNotEmpty(entryPromoResultsModel)) {
            for (final TargetPromotionOrderEntryConsumedModel tgtConsumedEntry : entryPromoResultsModel) {
                final ItemInfo item = new ItemInfo();
                item.setId(orderEntryModel.getProduct().getCode());
                final Long quantity = tgtConsumedEntry.getQuantity();
                item.setQuantity(quantity);
                actualQuantity -= quantity.longValue();
                final BigDecimal price = BigDecimal.valueOf(orderEntryModel.getBasePrice().doubleValue());
                item.setPrice(price);
                item.setFilePrice(price);
                final ItemDeal itemDeal = tlogDealConverter.populateItemDeal(orderEntryModel,
                        tgtConsumedEntry, dealResults, deals);
                if (itemDeal == null && tmd != null) {
                    //price is already negated - so refund scenario is taken care of, automatically
                    item.setItemDiscount(tlogTMDConverter.getDiscountInfoFromPromotion(tmd, null,
                            item.getPrice().multiply(BigDecimal.valueOf(percentage))
                                    .divide(BigDecimal.valueOf(100))
                                    .multiply(BigDecimal.valueOf(item.getQuantity().longValue()))));
                }
                else if (itemDeal != null) {
                    item.setItemDeal(itemDeal);
                    if (null != newTmd) {

                        final int markdown = TlogCommonHelper.getMarkdownInCents(orderEntryModel,
                                tgtConsumedEntry);
                        final double currEntryValue = quantity.longValue() * (price.doubleValue() - (markdown / 100f));

                        // If the item has become free due to the deal, then we omit the tmd entry
                        if (currEntryValue > 0) {
                            final DiscountInfo tmdInfo = tlogTMDConverter
                                    .getDiscountInfoFromTMDProportionByValue(promotionResult,
                                            totalOrderEntryValue,
                                            currEntryValue);

                            if (tmdInfo != null) {
                                item.setItemDiscount(tmdInfo);
                            }
                        }


                    }
                }
                items.add(item);
            }
        }

        if (actualQuantity > 0) {
            final ItemInfo item = new ItemInfo();
            item.setId(orderEntryModel.getProduct().getCode());
            final Long quantity = Long.valueOf(actualQuantity);
            item.setQuantity(quantity);
            final BigDecimal price = BigDecimal.valueOf(orderEntryModel.getBasePrice().doubleValue());
            item.setPrice(price);
            item.setFilePrice(price);

            if (null != newTmd) {
                final double currEntryValue = actualQuantity * price.doubleValue();
                item.setItemDiscount(tlogTMDConverter.getDiscountInfoFromTMDProportionByValue(
                        promotionResult, totalOrderEntryValue,
                        currEntryValue));
            }
            items.add(item);
        }
        return items;
    }

    /**
     * Find the promotionEntryConsumed for the orderEntry
     * 
     * @param orderEntryNum
     * @param promoResultSet
     * @return resultModel that is applied to that entry
     */
    private static List<TargetPromotionOrderEntryConsumedModel> findPromotionForEntry(final Integer orderEntryNum,
            final Set<PromotionResultModel> promoResultSet) {
        final int entryNum = orderEntryNum.intValue();
        List<TargetPromotionOrderEntryConsumedModel> validEntries = null;
        if (CollectionUtils.isNotEmpty(promoResultSet)) {
            for (final PromotionResultModel productPromotionResult : promoResultSet)
            {
                if (productPromotionResult.getCertainty().floatValue() >= 1.0f) {
                    final Collection<PromotionOrderEntryConsumedModel> consumedEntries = productPromotionResult
                            .getConsumedEntries();
                    if (CollectionUtils.isNotEmpty(consumedEntries))
                    {
                        for (final PromotionOrderEntryConsumedModel consumedEntry : consumedEntries)
                        {
                            if (entryNum == consumedEntry.getOrderEntry().getEntryNumber().intValue())
                            {
                                if (validEntries == null) {
                                    validEntries = new ArrayList<>();
                                }
                                validEntries.add((TargetPromotionOrderEntryConsumedModel)consumedEntry);
                            }
                        }
                    }
                }
            }
        }
        return validEntries;
    }

    private OrderModel getOrderModelFromOrderEntry(final AbstractOrderEntryModel orderEntry) {
        if (orderEntry != null && orderEntry.getOrder() instanceof OrderModel) {
            return (OrderModel)orderEntry.getOrder();
        }
        return null;
    }


    /* (non-Javadoc)
     * @see au.com.target.tgtsale.tlog.converter.TlogSaleConverter#getDiscountInfoFromPromotion(de.hybris.platform.promotions.model.ProductPromotionModel, java.lang.String, java.math.BigDecimal)
     */
    @Override
    public DiscountInfo getDiscountInfoFromPromotion(final ProductPromotionModel tMDiscountModel, final String code,
            final BigDecimal amount) {
        // YTODO Auto-generated method stub
        return null;
    }

    /**
     * @param tlogTMDConverter
     *            the tlogTMDConverter to set
     */
    @Required
    public void setTlogTMDConverter(final TlogTMDConverter tlogTMDConverter) {
        this.tlogTMDConverter = tlogTMDConverter;
    }


    /**
     * @param tlogDealConverter
     *            the tlogDealConverter to set
     */
    @Required
    public void setTlogDealConverter(final TlogDealConverter tlogDealConverter) {
        this.tlogDealConverter = tlogDealConverter;
    }


    /**
     * @param currencyConversionFactorService
     *            the currencyConversionFactorService to set
     */
    @Required
    public void setCurrencyConversionFactorService(final CurrencyConversionFactorService currencyConversionFactorService) {
        this.currencyConversionFactorService = currencyConversionFactorService;
    }

}

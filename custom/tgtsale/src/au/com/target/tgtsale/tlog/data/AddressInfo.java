/**
 * 
 */
package au.com.target.tgtsale.tlog.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class AddressInfo
{
    @XmlAttribute(name = "type")
    private AddressTypeEnum type;

    @XmlAttribute(name = "line1")
    private String addressLine1;

    @XmlAttribute(name = "line2")
    private String addressLine2;

    @XmlAttribute(name = "city")
    private String city;

    @XmlAttribute(name = "postcode")
    private String postCode;

    @XmlAttribute(name = "store")
    private String store;

    @XmlAttribute(name = "country")
    private String country;

    @XmlAttribute(name = "phone")
    private String phone;

    @XmlAttribute(name = "mobile")
    private String mobile;

    /**
     * @return the type
     */
    public AddressTypeEnum getType()
    {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final AddressTypeEnum type)
    {
        this.type = type;
    }

    /**
     * @return the addressLine1
     */
    public String getAddressLine1()
    {
        return addressLine1;
    }

    /**
     * @param addressLine1
     *            the addressLine1 to set
     */
    public void setAddressLine1(final String addressLine1)
    {
        this.addressLine1 = addressLine1;
    }

    /**
     * @return the addressLine2
     */
    public String getAddressLine2()
    {
        return addressLine2;
    }

    /**
     * @param addressLine2
     *            the addressLine2 to set
     */
    public void setAddressLine2(final String addressLine2)
    {
        this.addressLine2 = addressLine2;
    }

    /**
     * @return the city
     */
    public String getCity()
    {
        return city;
    }

    /**
     * @param city
     *            the city to set
     */
    public void setCity(final String city)
    {
        this.city = city;
    }

    /**
     * @return the postCode
     */
    public String getPostCode()
    {
        return postCode;
    }

    /**
     * @param postCode
     *            the postCode to set
     */
    public void setPostCode(final String postCode)
    {
        this.postCode = postCode;
    }

    /**
     * @return the store
     */
    public String getStore()
    {
        return store;
    }

    /**
     * @param store
     *            the store to set
     */
    public void setStore(final String store)
    {
        this.store = store;
    }

    /**
     * @return the country
     */
    public String getCountry()
    {
        return country;
    }

    /**
     * @param country
     *            the country to set
     */
    public void setCountry(final String country)
    {
        this.country = country;
    }

    /**
     * @return the phone
     */
    public String getPhone()
    {
        return phone;
    }

    /**
     * @param phone
     *            the phone to set
     */
    public void setPhone(final String phone)
    {
        this.phone = phone;
    }

    /**
     * @return the mobile
     */
    public String getMobile()
    {
        return mobile;
    }

    /**
     * @param mobile
     *            the mobile to set
     */
    public void setMobile(final String mobile)
    {
        this.mobile = mobile;
    }

}

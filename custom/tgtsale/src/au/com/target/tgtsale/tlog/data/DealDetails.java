/**
 * 
 */
package au.com.target.tgtsale.tlog.data;


/**
 * @author rsamuel3
 * 
 */
public class DealDetails {
    private Integer markdown;

    private String code;

    private String type;

    private final String instance;

    public DealDetails(final String code, final String instance) {
        this.code = code;
        this.instance = instance;
    }

    /**
     */
    public Integer getMarkdown() {
        return markdown;
    }

    /**
     * @param markdownForInstance
     *            the markdown value set to set
     */
    public void addMarkdown(final int markdownForInstance) {
        if (this.markdown == null) {
            this.markdown = Integer.valueOf(0);
        }
        this.markdown = Integer.valueOf(this.markdown.intValue() + Math.abs(markdownForInstance));
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the posDealType
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     *            the posDealType to set
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * @return the instance
     */
    public String getInstance() {
        return instance;
    }

}

/**
 * 
 */
package au.com.target.tgtsale.tlog.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author rsamuel3
 * 
 */
public class DealResults {
    private Map<String, DealDetails> dealDetailsForInstance;

    private List<ItemInfo> items;

    /**
     * @return the items
     */
    public List<ItemInfo> getItems() {
        return items;
    }

    /**
     * @param items
     *            the items to set
     */
    public void setItems(final List<ItemInfo> items) {
        this.items = items;
    }

    /**
     * @return the dealDetailsForInstance
     */
    public Map<String, DealDetails> getDealDetailsForInstance() {
        return dealDetailsForInstance;
    }

    /**
     * @param dealDetailsForInstance
     *            the dealDetailsForInstance to set
     */
    public void setDealDetailsForInstance(final Map<String, DealDetails> dealDetailsForInstance) {
        this.dealDetailsForInstance = dealDetailsForInstance;
    }

    public void addDealDetailsForInstance(final String instanceKey, final String code, final String type,
            final int markdownForInstance) {
        if (dealDetailsForInstance == null) {
            this.dealDetailsForInstance = new HashMap<String, DealDetails>();
        }
        final String key = instanceKey + ":" + code;
        DealDetails dealDetails = this.dealDetailsForInstance.get(key);
        if (dealDetails == null) {
            dealDetails = new DealDetails(code, instanceKey);
        }
        dealDetails.setType(type);
        dealDetails.addMarkdown(markdownForInstance);
        this.dealDetailsForInstance.put(key, dealDetails);
    }
}

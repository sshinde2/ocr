/**
 * 
 */
package au.com.target.tgtsale.tlog.data;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * TLOG Transaction discount style attribute values
 * 
 */
@XmlEnum(String.class)
public enum DiscountStyleEnum {

    @XmlEnumValue("dollar off")
    DOLLAR_OFF("dollar off"),

    @XmlEnumValue("percent off")
    PERCENT_OFF("percent off");

    private String style;

    /**
     * @param style
     *            - Discount style type
     */
    private DiscountStyleEnum(final String style)
    {
        this.style = style;
    }

    /**
     * @return - discount style
     */
    public String getStyle()
    {
        return style;
    }

}

/**
 * 
 */
package au.com.target.tgtsale.tlog.data;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * @author vmuthura
 * 
 */
@XmlEnum(String.class)
public enum DiscountTypeEnum
{
    @XmlEnumValue("staff")
    TEAM_MEMBER("staff"),

    @XmlEnumValue("promo")
    VOUCHER("promo"),

    @XmlEnumValue("flybuys")
    FLYBUYS("flybuys");

    private String type;

    /**
     * @param type
     *            - Discount type
     */
    private DiscountTypeEnum(final String type)
    {
        this.type = type;
    }

    /**
     * @return - discount type
     */
    public String getType()
    {
        return type;
    }
}

/**
 * 
 */
package au.com.target.tgtsale.tlog.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author rsamuel3
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemDeal {
    @XmlAttribute
    private String id;

    @XmlAttribute
    private String instance;

    @XmlAttribute
    private String type;

    @XmlAttribute
    private String markdown;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * @return the instance
     */
    public String getInstance() {
        return instance;
    }

    /**
     * @param instance
     *            the instance to set
     */
    public void setInstance(final String instance) {
        this.instance = instance;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * @return the markdown
     */
    public String getMarkdown() {
        return markdown;
    }

    /**
     * @param markdown
     *            the markdown to set
     */
    public void setMarkdown(final String markdown) {
        this.markdown = markdown;
    }

}

/**
 * 
 */
package au.com.target.tgtsale.tlog.data;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemInfo
{
    @XmlAttribute(name = "code")
    private String id;

    @XmlAttribute(name = "qty")
    private Long quantity;

    @XmlAttribute(name = "price")
    private BigDecimal price;

    @XmlAttribute(name = "reason")
    private ReasonTypeEnum reason;

    @XmlAttribute(name = "filePrice")
    private BigDecimal filePrice;

    @XmlElement(name = "priceOverride")
    private PriceOverride priceOverride;

    @XmlElement(name = "itemDiscount")
    private DiscountInfo itemDiscount;

    @XmlElement(name = "itemDeal")
    private ItemDeal itemDeal;

    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final String id)
    {
        this.id = id;
    }

    /**
     * @return the quantity
     */
    public Long getQuantity()
    {
        return quantity;
    }

    /**
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(final Long quantity)
    {
        this.quantity = quantity;
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice()
    {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(final BigDecimal price)
    {
        this.price = price;
    }

    /**
     * @return the reason
     */
    public ReasonTypeEnum getReason()
    {
        return reason;
    }

    /**
     * @param reason
     *            the reason to set
     */
    public void setReason(final ReasonTypeEnum reason)
    {
        this.reason = reason;
    }

    /**
     * @return the filePrice
     */
    public BigDecimal getFilePrice()
    {
        return filePrice;
    }

    /**
     * @param filePrice
     *            the filePrice to set
     */
    public void setFilePrice(final BigDecimal filePrice)
    {
        this.filePrice = filePrice;
    }

    /**
     * @return the priceOverride
     */
    public PriceOverride getPriceOverride()
    {
        return priceOverride;
    }

    /**
     * @param priceOverride
     *            the priceOverride to set
     */
    public void setPriceOverride(final PriceOverride priceOverride)
    {
        this.priceOverride = priceOverride;
    }

    /**
     * @return the itemDiscount
     */
    public DiscountInfo getItemDiscount()
    {
        return itemDiscount;
    }

    /**
     * @param itemDiscount
     *            the itemDiscount to set
     */
    public void setItemDiscount(final DiscountInfo itemDiscount)
    {
        this.itemDiscount = itemDiscount;
    }

    /**
     * @return the itemDeal
     */
    public ItemDeal getItemDeal() {
        return itemDeal;
    }

    /**
     * @param itemDeal
     *            the itemDeal to set
     */
    public void setItemDeal(final ItemDeal itemDeal) {
        this.itemDeal = itemDeal;
    }

}

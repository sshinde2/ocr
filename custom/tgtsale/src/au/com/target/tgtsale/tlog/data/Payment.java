/**
 * 
 */
package au.com.target.tgtsale.tlog.data;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Payment
{
    @XmlAttribute(name = "amount")
    private BigDecimal amount;

    public Payment()
    {
        //
    }

    public Payment(final BigDecimal amount)
    {
        this.amount = amount;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount()
    {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final BigDecimal amount)
    {
        this.amount = amount;
    }
}

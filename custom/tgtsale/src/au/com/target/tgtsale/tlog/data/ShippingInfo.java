/**
 * 
 */
package au.com.target.tgtsale.tlog.data;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ShippingInfo
{
    @XmlAttribute(name = "amt")
    private BigDecimal amount;

    @XmlAttribute(name = "origAmt")
    private BigDecimal originalAmount;

    /**
     * @return the amount
     */
    public BigDecimal getAmount()
    {
        return amount;
    }

    /**
     * @param amount
     *            the amount to set
     */
    public void setAmount(final BigDecimal amount)
    {
        this.amount = amount;
    }

    /**
     * @return the originalAmount
     */
    public BigDecimal getOriginalAmount()
    {
        return originalAmount;
    }

    /**
     * @param originalAmount
     *            the originalAmount to set
     */
    public void setOriginalAmount(final BigDecimal originalAmount)
    {
        this.originalAmount = originalAmount;
    }

}

/**
 * 
 */
package au.com.target.tgtsale.tlog.data;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author vmuthura
 * 
 */
@XmlRootElement(name = "tlog")
@XmlAccessorType(XmlAccessType.FIELD)
public class TLog
{
    @XmlElement(name = "file")
    private Version fileVersion;

    @XmlElement(name = "trans")
    private List<Transaction> transactions;

    @SuppressWarnings("unused")
    private TLog()
    {
        //private no-args constructor
    }

    public TLog(final Version version, final List<Transaction> transactions)
    {
        this.fileVersion = version;
        this.transactions = transactions;
    }

    /**
     * @return the fileVersion
     */
    public Version getFileVersion()
    {
        return fileVersion;
    }

    /**
     * @param fileVersion
     *            the fileVersion to set
     */
    public void setFileVersion(final Version fileVersion)
    {
        this.fileVersion = fileVersion;
    }

    /**
     * @return the transactions
     */
    public List<Transaction> getTransactions()
    {
        return transactions;
    }

    /**
     * @param transactions
     *            the transactions to set
     */
    public void setTransactions(final List<Transaction> transactions)
    {
        this.transactions = transactions;
    }

}

/**
 * 
 */
package au.com.target.tgtsale.tlog.data.adapter;

import java.text.DecimalFormat;

import javax.xml.bind.annotation.adapters.XmlAdapter;


/**
 * JAXB Adapter {@link XmlAdapter} class to format a {@link Double} to have 2 decimal points
 * 
 * @author vmuthura
 * 
 */
public class DecimalFormatAdapter extends XmlAdapter<String, Double>
{
    private final DecimalFormat df = new DecimalFormat(".00");

    @Override
    public String marshal(final Double value) throws Exception
    {
        if (value == null)
        {
            return null;
        }


        return df.format(value.doubleValue());
    }

    @Override
    public Double unmarshal(final String value) throws Exception
    {
        return Double.valueOf(value);
    }

}

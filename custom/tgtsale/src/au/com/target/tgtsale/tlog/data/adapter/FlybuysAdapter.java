/**
 * 
 */
package au.com.target.tgtsale.tlog.data.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtsale.tlog.data.Flybuys;


/**
 * @author rsamuel3
 * 
 */
public class FlybuysAdapter extends XmlAdapter<Flybuys, Flybuys> {

    /* (non-Javadoc)
     * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
     */
    @Override
    public Flybuys marshal(final Flybuys flybuys) throws Exception {
        if (flybuys != null && StringUtils.isNotBlank(flybuys.getNumber())) {
            return flybuys;
        }
        return null;
    }

    /* (non-Javadoc)
     * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
     */
    @Override
    public Flybuys unmarshal(final Flybuys flybuys) throws Exception {
        return flybuys;
    }

}

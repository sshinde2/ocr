/**
 * 
 */
package au.com.target.tgtsale.tlog.exception;

/**
 * @author vmuthura
 * 
 */
public class PublishTlogFailedException extends RuntimeException {

    public PublishTlogFailedException(final String message) {
        super(message);
    }

    public PublishTlogFailedException(final Throwable throwable) {
        super(throwable);
    }

    public PublishTlogFailedException(final String message, final Throwable throwable) {
        super(message, throwable);
    }
}

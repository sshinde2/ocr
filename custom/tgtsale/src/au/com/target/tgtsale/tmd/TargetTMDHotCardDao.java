package au.com.target.tgtsale.tmd;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtsale.model.TargetTMDHotCardModel;


/**
 *
 */
public interface TargetTMDHotCardDao {

    /**
     * Find a card that is considered as hot card by number
     * 
     * @return TargetHotCardModel
     */
    TargetTMDHotCardModel getTargetHotCardModelByCardNumber(long cardNumber)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;

}

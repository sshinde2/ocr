/**
 * 
 */
package au.com.target.tgtsale.tmd;

import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtsale.model.TargetTMDHotCardModel;


/**
 * @author phi.tran
 * 
 */

public interface TargetTMDHotCardService {


    /**
     * Get TargetHotCard by card number
     * 
     * @param cardNumber
     * @return TargetHotCardModel
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */

    TargetTMDHotCardModel getTargetHotCardModelByCardCode(long cardNumber) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException;

    /**
     * Create a new a Target hot card if it does not already exist
     * 
     * @param cardNumber
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     * @throws ModelSavingException
     */
    void save(long cardNumber) throws ModelSavingException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException;

    /**
     * Check a Target hot card is existed or not by card number
     * 
     * @param cardNumber
     * @return true: if card is existed
     * 
     */
    boolean exists(final long cardNumber);

    /**
     * This method doesn't check if the card exits in order to verify this, please use method {@link #exists(long)}
     * first
     * 
     * @param cardNumber
     *            The card number to remove
     * @throws ModelRemovalException
     *             if removal fails
     */
    void remove(long cardNumber) throws ModelRemovalException;
}

/**
 * 
 */
package au.com.target.tgtsale.tmd.impl;

import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtsale.model.TargetTMDHotCardModel;
import au.com.target.tgtsale.tmd.TargetTMDHotCardDao;
import au.com.target.tgtsale.tmd.TargetTMDHotCardService;


/**
 * @author phi.tran
 * 
 */
public class TargetTMDHotCardServiceImpl implements TargetTMDHotCardService {
    private static final Logger LOG = Logger.getLogger(TargetTMDHotCardServiceImpl.class);

    private TargetTMDHotCardDao targetTMDHotCardDao;
    private ModelService modelService;

    @Override
    public TargetTMDHotCardModel getTargetHotCardModelByCardCode(final long cardNumber)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException
    {
        return targetTMDHotCardDao.getTargetHotCardModelByCardNumber(cardNumber);
    }

    @Override
    public void save(final long cardNumber) throws ModelSavingException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        if (!exists(cardNumber)) {
            final TargetTMDHotCardModel model = modelService.create(TargetTMDHotCardModel.class);
            model.setCardNum(Long.valueOf(cardNumber));
            modelService.save(model);
        }
        else {
            LOG.warn("The TargetHotCardModel with card number " + cardNumber + " is already existed");
        }
    }


    @Override
    public boolean exists(final long cardNumber) {
        TargetTMDHotCardModel hotCard = null;

        try {
            hotCard = this.getTargetHotCardModelByCardCode(cardNumber);
        }
        catch (final TargetUnknownIdentifierException e) {
            return false;
        }
        catch (final TargetAmbiguousIdentifierException e) {
            return true;
        }
        return hotCard != null;
    }

    @Override
    public void remove(final long cardNumber) throws ModelRemovalException {
        TargetTMDHotCardModel tmdHotCard = null;

        try {
            tmdHotCard = getTargetHotCardModelByCardCode(cardNumber);
        }
        catch (final TargetUnknownIdentifierException e) {
            LOG.warn("The TargetHotCardModel with card number " + cardNumber + " does not exist", e);
            return;
        }
        catch (final TargetAmbiguousIdentifierException e) {
            LOG.warn("Duplicate TargetHotCardModel with card number " + cardNumber, e);
            return;
        }

        modelService.remove(tmdHotCard);
    }

    /**
     * @return the targetTMDHotCardDao
     */
    public TargetTMDHotCardDao getTargetTMDHotCardDao() {
        return targetTMDHotCardDao;
    }

    /**
     * @param targetTMDHotCardDao
     *            the targetTMDHotCardDao to set
     */
    @Required
    public void setTargetTMDHotCardDao(final TargetTMDHotCardDao targetTMDHotCardDao) {
        this.targetTMDHotCardDao = targetTMDHotCardDao;
    }

    /**
     * @return the modelService
     */
    public ModelService getModelService() {
        return modelService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }
}

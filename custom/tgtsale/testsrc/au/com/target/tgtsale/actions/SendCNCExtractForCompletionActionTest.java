/**
 * 
 */
package au.com.target.tgtsale.actions;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtsale.pos.service.TargetPOSExtractsService;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendCNCExtractForCompletionActionTest {

    @InjectMocks
    private final SendCNCExtractForCompletionAction action = new SendCNCExtractForCompletionAction();

    @Mock
    private TargetPOSExtractsService posExtractsService;

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Before
    public void before() {
        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled("cncOrderExtractOnConsignment"))).willReturn(
                Boolean.TRUE);
    }

    @Test
    public void testExecuteAction() throws RetryLaterException, Exception {
        given(process.getOrder()).willReturn(order);
        action.executeAction(process);
        verify(posExtractsService).sendCncOrderExtractForCompletion(order);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteActionWithNullOder() throws RetryLaterException, Exception {
        given(process.getOrder()).willReturn(null);
        action.executeAction(process);
    }
}

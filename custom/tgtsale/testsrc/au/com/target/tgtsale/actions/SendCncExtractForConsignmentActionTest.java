/**
 * 
 */
package au.com.target.tgtsale.actions;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtsale.pos.service.TargetPOSExtractsService;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendCncExtractForConsignmentActionTest {

    @InjectMocks
    private final SendCncExtractForConsignmentAction action = new SendCncExtractForConsignmentAction();

    @Mock
    private TargetPOSExtractsService posExtractsService;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private OrderProcessModel process;

    @Mock
    private OrderModel order;

    @Mock
    private TargetConsignmentModel targetConsignment;

    @Test
    public void testExecuteAction() throws RetryLaterException, Exception {
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(targetConsignment);
        action.executeAction(process);
        verify(posExtractsService).sendCncOrderExtractForConsignment(targetConsignment);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteActionWithNullOder() throws RetryLaterException, Exception {
        given(orderProcessParameterHelper.getConsignment(process)).willReturn(null);
        action.executeAction(process);
    }
}

package au.com.target.tgtsale.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtsale.pos.service.TargetCustomerLockService;
import au.com.target.tgtsale.tlog.TargetTransactionLogService;


/**
 * Unit test for {@link SendLayByPaymentTlogAction}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendLayByPaymentTlogActionTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private final SendLayByPaymentTlogAction action = new SendLayByPaymentTlogAction();

    @Mock
    private TargetCustomerLockService targetCustomerLockService;

    @Mock
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Mock
    private TargetTransactionLogService targetTransactionLogService;

    @Mock
    private OrderModel orderModel;

    @Mock
    private OrderProcessModel orderProcessModel;

    @Mock
    private PaymentTransactionEntryModel ptem;

    @Test
    public void testExecuteActionWithNullProcess() throws RetryLaterException, Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Order process cannot be null");

        action.publishTlog(null);
    }

    @Test
    public void testExecuteActionWithNullOrder() throws RetryLaterException, Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Ordermodel cannot be null");

        BDDMockito.given(orderProcessParameterHelper.getPaymentTransactionEntry(Mockito.any(OrderProcessModel.class)))
                .willReturn(ptem);
        action.publishTlog(orderProcessModel);
    }

    @Test
    public void testExecuteActionWithNullPaymentTransactionEntry() throws RetryLaterException, Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("PaymentTransactionEntryModel may not be null");

        BDDMockito.given(orderProcessModel.getOrder()).willReturn(orderModel);
        action.publishTlog(orderProcessModel);
    }

    @Test
    public void testExecuteActionWithUnsuccessfulLock() throws RetryLaterException, Exception {
        BDDMockito.given(orderProcessModel.getOrder()).willReturn(orderModel);
        BDDMockito.given(orderProcessParameterHelper.getPaymentTransactionEntry(Mockito.any(OrderProcessModel.class)))
                .willReturn(ptem);

        action.publishTlog(orderProcessModel);
        Mockito.verify(targetCustomerLockService).convertLockIndirect(orderModel);
        Mockito.verify(targetTransactionLogService).publishTlogForPayment(ptem);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testExecuteActionWithSuccessfulLock() throws RetryLaterException, Exception {
        BDDMockito.given(orderProcessModel.getOrder()).willReturn(orderModel);
        BDDMockito.given(orderProcessParameterHelper.getPaymentTransactionEntry(Mockito.any(OrderProcessModel.class)))
                .willReturn(ptem);
        BDDMockito.given(targetCustomerLockService.convertLockIndirect(Mockito.any(OrderModel.class)))
                .willReturn(Boolean.TRUE);

        action.publishTlog(orderProcessModel);
        Mockito.verify(targetCustomerLockService).convertLockIndirect(orderModel);
        // even if lock fails, still send tlog
        Mockito.verify(targetTransactionLogService).publishTlogForPayment(ptem);
    }

}

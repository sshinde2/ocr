package au.com.target.tgtsale.actions;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.returns.model.OrderReturnRecordEntryModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtsale.tlog.TargetTransactionLogService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendOrderTlogReturnActionTest {
    @Mock
    private OrderProcessParameterHelper mockOrderProcessParameterHelper;

    @Mock
    private TargetTransactionLogService mockTargetTransactionLogService;

    @InjectMocks
    private final SendOrderTlogReturnAction sendOrderTlogReturnAction = new SendOrderTlogReturnAction();

    @Mock
    private OrderProcessModel mockOrderProcessModel;

    @Test(expected = IllegalArgumentException.class)
    public void testPublishTlogWithNullOrderCancelRequest() throws Exception {
        given(mockOrderProcessParameterHelper.getReturnRequest(mockOrderProcessModel)).willReturn(null);

        sendOrderTlogReturnAction.publishTlog(mockOrderProcessModel);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testPublishTlog() {
        final OrderReturnRecordEntryModel mockOrderReturnRecordEntry = mock(OrderReturnRecordEntryModel.class);
        given(mockOrderProcessParameterHelper.getReturnRequest(mockOrderProcessModel))
                .willReturn(mockOrderReturnRecordEntry);

        final List<PaymentTransactionEntryModel> refundPaymentTransactionEntries = new ArrayList<>();
        given(mockOrderProcessParameterHelper.getRefundPaymentEntryList(mockOrderProcessModel))
                .willReturn(refundPaymentTransactionEntries);

        sendOrderTlogReturnAction.publishTlog(mockOrderProcessModel);

        verify(mockTargetTransactionLogService).publishTlogForOrderReturn(eq(mockOrderReturnRecordEntry),
                eq(refundPaymentTransactionEntries), any(BigDecimal.class));
    }

}

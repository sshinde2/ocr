package au.com.target.tgtsale.actions;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtsale.tlog.TargetTransactionLogService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SendPreOrderTlogSaleActionTest {

    @InjectMocks
    private final SendPreOrderTlogSaleAction sendPreOrderTlogSaleAction = new SendPreOrderTlogSaleAction();

    @Mock
    private TargetTransactionLogService mockTargetTransactionLogService;

    @Test(expected = IllegalArgumentException.class)
    public void testPublishTlogWithNullOrderProcess() throws Exception {
        sendPreOrderTlogSaleAction.publishTlog(null);
    }

    @Test
    public void testPublishTlog() {
        final OrderProcessModel process = mock(OrderProcessModel.class);
        final OrderModel order = mock(OrderModel.class);
        given(process.getOrder()).willReturn(order);
        willDoNothing().given(mockTargetTransactionLogService).publishTlogForPreOrderFulfilment(process);
        sendPreOrderTlogSaleAction.publishTlog(process);
        verify(mockTargetTransactionLogService).publishTlogForPreOrderFulfilment(process);
    }

}

/**
 * 
 */
package au.com.target.tgtsale.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.voucher.model.PromotionVoucherModel;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtsale.interceptor.TgtSalePromotionVoucherModelValidateInterceptor.ErrorMessages;



/**
 * @author paul
 *
 */
@UnitTest
public class TgtSalePromotionVoucherModelValidateInterceptorTest {

    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";

    private final TgtSalePromotionVoucherModelValidateInterceptor promotionVoucherModelValidateInterceptor = new TgtSalePromotionVoucherModelValidateInterceptor();

    @Test
    public void testFailureWhenVoucherNotEnabledAtAll() {
        final String validVoucher = "HOMESALE-14";

        final PromotionVoucherModel mockPromotionVoucher = Mockito.mock(PromotionVoucherModel.class);
        BDDMockito.given(mockPromotionVoucher.getVoucherCode()).willReturn(validVoucher);
        try {
            promotionVoucherModelValidateInterceptor.onValidate(mockPromotionVoucher, null);
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.VOUCHER_NOT_ENABLED);
        }
    }

    @Test
    public void testFailBarcodeNullWhenInstoreEnabled() {
        final String validVoucher = "HOMESALE-14";

        final PromotionVoucherModel mockPromotionVoucher = Mockito.mock(PromotionVoucherModel.class);
        BDDMockito.given(mockPromotionVoucher.getVoucherCode()).willReturn(validVoucher);
        BDDMockito.given(mockPromotionVoucher.getEnabledInstore()).willReturn(Boolean.TRUE);
        try {
            promotionVoucherModelValidateInterceptor.onValidate(mockPromotionVoucher, null);
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.BARCODE_INVALID);
        }
    }

    @Test
    public void testFailBarcodeInvalidWhenInstoreEnabled() {
        final String validVoucher = "HOMESALE-14";

        final PromotionVoucherModel mockPromotionVoucher = Mockito.mock(PromotionVoucherModel.class);
        BDDMockito.given(mockPromotionVoucher.getVoucherCode()).willReturn(validVoucher);
        BDDMockito.given(mockPromotionVoucher.getEnabledInstore()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockPromotionVoucher.getBarcode()).willReturn(Long.valueOf("222300675001427"));
        try {
            promotionVoucherModelValidateInterceptor.onValidate(mockPromotionVoucher, null);
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.BARCODE_INVALID);
        }
    }



    @Test
    public void testSuccessfulInstoreEnabledVoucher() throws InterceptorException {
        final String validVoucher = "HOMESALE-14";

        final PromotionVoucherModel mockPromotionVoucher = Mockito.mock(PromotionVoucherModel.class);
        BDDMockito.given(mockPromotionVoucher.getVoucherCode()).willReturn(validVoucher);
        BDDMockito.given(mockPromotionVoucher.getEnabledInstore()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockPromotionVoucher.getBarcode()).willReturn(Long.valueOf("9300675001427"));
        promotionVoucherModelValidateInterceptor.onValidate(mockPromotionVoucher, null);
    }
}

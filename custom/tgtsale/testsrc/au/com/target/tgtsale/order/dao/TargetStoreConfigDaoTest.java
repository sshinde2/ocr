package au.com.target.tgtsale.order.dao;

import static org.mockito.Mockito.verify;

import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTest;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtsale.model.TargetStoreConfigModel;
import au.com.target.tgtsale.storeconfig.impl.TargetStoreConfigDaoImpl;

import com.google.common.base.Charsets;


public class TargetStoreConfigDaoTest extends ServicelayerTest
{

    @Resource
    private TargetStoreConfigDaoImpl targetStoreConfigDao;

    @Before
    public void setUp() throws ImpExException {
        // Import Target Store Config
        importCsv("/test/test-store-config.impex", Charsets.UTF_8.name());
    }


    @Test
    public void testGetListStoreConfigsMultipleResults()
    {
        final List<TargetStoreConfigModel> allStoreConfigs = targetStoreConfigDao
                .getListStoreConfigs("DISC EMPLOY", "PREFIX");

        Assert.assertEquals(15, allStoreConfigs.size());

        final Iterator<TargetStoreConfigModel> iterator = allStoreConfigs.iterator();
        final TargetStoreConfigModel storeConfig01 = iterator.next();
        Assert.assertEquals("PREFIX", storeConfig01.getTag());
        Assert.assertEquals(Integer.valueOf(1), storeConfig01.getOccurance());
        Assert.assertEquals("DISC EMPLOY", storeConfig01.getSection());
        Assert.assertEquals("27701", storeConfig01.getRecord());

        final TargetStoreConfigModel storeConfig02 = iterator.next();
        Assert.assertEquals("PREFIX", storeConfig02.getTag());
        Assert.assertEquals(Integer.valueOf(2), storeConfig02.getOccurance());
        Assert.assertEquals("DISC EMPLOY", storeConfig02.getSection());
        Assert.assertEquals("27713", storeConfig02.getRecord());


        final TargetStoreConfigModel storeConfig03 = iterator.next();
        Assert.assertEquals("PREFIX", storeConfig03.getTag());
        Assert.assertEquals(Integer.valueOf(3), storeConfig03.getOccurance());
        Assert.assertEquals("DISC EMPLOY", storeConfig03.getSection());
        Assert.assertEquals("27714", storeConfig03.getRecord());


        final TargetStoreConfigModel storeConfig15 = allStoreConfigs.get(14);
        Assert.assertEquals("PREFIX", storeConfig15.getTag());
        Assert.assertEquals(Integer.valueOf(15), storeConfig15.getOccurance());
        Assert.assertEquals("DISC EMPLOY", storeConfig15.getSection());
        Assert.assertEquals("27772", storeConfig15.getRecord());
    }

    @Test
    public void testGetListStoreConfigsNoResults()
    {
        final List<TargetStoreConfigModel> allStoreConfigs = targetStoreConfigDao
                .getListStoreConfigs("FRED", "WAS HERE");

        Assert.assertEquals(0, allStoreConfigs.size());
    }

    @Test
    public void testGetStoreConfigRecordExists() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException
    {
        final TargetStoreConfigModel storeConfig17 = targetStoreConfigDao
                .getStoreConfig("DISC EMPLOY", "VALIDATION CHECK DIGIT", 1);

        Assert.assertEquals("VALIDATION CHECK DIGIT", storeConfig17.getTag());
        Assert.assertEquals(Integer.valueOf(1), storeConfig17.getOccurance());
        Assert.assertEquals("DISC EMPLOY", storeConfig17.getSection());
        Assert.assertEquals("6", storeConfig17.getRecord());
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testGetStoreConfigRecordNotExist() throws Exception {
        verify(targetStoreConfigDao
                .getStoreConfig("WrongName", "fred", 1));
    }

}

/**
 * 
 */
package au.com.target.tgtsale.product.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.AddressModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtsale.product.client.TargetPOSProductClient;
import au.com.target.tgtsale.product.data.POSProduct;
import au.com.target.tgtsale.product.dto.request.ProductRequestDto;
import au.com.target.tgtsale.product.dto.response.ProductResponseDto;
import au.com.target.tgtwebcore.exception.TargetIntegrationException;


/**
 * Test for TargetPOSProductService
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("boxing")
public class TargetPOSProductServiceTest {

    private static final Integer STORE_NUMBER = Integer.valueOf(7001);
    private static final String STATE = "VIC";
    private static final String EAN = "1111";


    @Mock
    private TargetPointOfServiceModel pos;

    @Mock
    private AddressModel address;

    @Mock
    private TargetPointOfServiceService targetPointOfServiceService;

    @Mock
    private TargetPOSProductClient targetPOSProductClient;

    @InjectMocks
    private final TargetPOSProductServiceImpl targetPOSProductService = new TargetPOSProductServiceImpl();

    @Test
    public void testCreateRequest() {

        final ProductRequestDto request = targetPOSProductService.createRequest(STORE_NUMBER, EAN, STATE);

        assertThat(request, notNullValue());
        assertThat(request.getEan(), is(EAN));
        assertThat(request.getStoreNumber(), is(STORE_NUMBER.toString()));
        assertThat(request.getStoreState(), is(STATE));

    }

    @Test
    public void testGetStateSuccessful() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        setupStoreServiceMock();

        final String state = targetPOSProductService.getState(STORE_NUMBER);

        assertThat(state, notNullValue());
        assertThat(state, is(STATE));
    }

    @Test
    public void testGetStateStoreNotFound() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        Mockito.when(targetPointOfServiceService.getPOSByStoreNumber(STORE_NUMBER)).thenThrow(
                new TargetUnknownIdentifierException(""));

        final String state = targetPOSProductService.getState(STORE_NUMBER);

        assertThat(state, nullValue());
    }

    @Test
    public void testGetStateStoreNoAddress() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        Mockito.when(targetPointOfServiceService.getPOSByStoreNumber(STORE_NUMBER)).thenReturn(pos);
        Mockito.when(pos.getAddress()).thenReturn(null);

        final String state = targetPOSProductService.getState(STORE_NUMBER);

        assertThat(state, nullValue());
    }

    @Test
    public void testConvertSuccessResponse() {

        final ProductResponseDto response = createSuccessResponse();

        final POSProduct posProduct = targetPOSProductService.convertResponse(response);

        assertThat(posProduct.getDescription(), is("description"));
        assertThat(posProduct.getItemCode(), is("1234"));
        assertThat(posProduct.getError(), is(nullValue())); // Since successful
        assertThat(posProduct.getPriceInCents(), is(Integer.valueOf(567)));
        assertThat(posProduct.getWasPriceInCents(), is(Integer.valueOf(789)));
        assertThat(posProduct.getSystemError(), is(false));
    }

    @Test
    public void testConvertFailResponse() {

        final ProductResponseDto response = createFailResponse();

        final POSProduct posProduct = targetPOSProductService.convertResponse(response);

        assertThat(posProduct.getError(), is("errorMessage"));
        assertThat(posProduct.getDescription(), is(nullValue()));
        assertThat(posProduct.getItemCode(), is(nullValue()));
        assertThat(posProduct.getPriceInCents(), is(nullValue()));
        assertThat(posProduct.getWasPriceInCents(), is(nullValue()));
        assertThat(posProduct.getSystemError(), is(false));
    }

    @Test
    public void testGetPOSProductDetailsSuccess() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException, TargetIntegrationException {

        setupStoreServiceMock();

        Mockito.when(targetPOSProductClient.getPOSProductDetails(Mockito.any(ProductRequestDto.class)))
                .thenReturn(createSuccessResponse());

        final POSProduct posProduct = targetPOSProductService.getPOSProductDetails(STORE_NUMBER, EAN);

        assertThat(posProduct.getDescription(), is("description"));
        assertThat(posProduct.getItemCode(), is("1234"));
        assertThat(posProduct.getError(), is(nullValue())); // Since successful
        assertThat(posProduct.getPriceInCents(), is(Integer.valueOf(567)));
        assertThat(posProduct.getWasPriceInCents(), is(Integer.valueOf(789)));
        assertThat(posProduct.getSystemError(), is(false));
    }

    @Test
    public void testGetPOSProductDetailsSystemError() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        Mockito.when(targetPointOfServiceService.getPOSByStoreNumber(STORE_NUMBER)).thenReturn(null);

        final POSProduct posProduct = targetPOSProductService.getPOSProductDetails(STORE_NUMBER, EAN);

        assertThat(posProduct.getError(), is("Store number is not in the system"));
        assertThat(posProduct.getSystemError(), is(true));
    }

    @Test
    public void testGetPOSProductDetailsPosError() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException, TargetIntegrationException {

        setupStoreServiceMock();

        Mockito.when(targetPOSProductClient.getPOSProductDetails(Mockito.any(ProductRequestDto.class)))
                .thenReturn(createFailResponse());

        final POSProduct posProduct = targetPOSProductService.getPOSProductDetails(STORE_NUMBER, EAN);

        assertThat(posProduct.getError(), is("errorMessage"));
        assertThat(posProduct.getSystemError(), is(false));
    }

    @Test
    public void testGetPOSProductDetailsTimeout() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException, TargetIntegrationException {

        setupStoreServiceMock();

        final ProductResponseDto failResponse = createFailResponse();
        failResponse.setTimeout(true);

        Mockito.when(targetPOSProductClient.getPOSProductDetails(Mockito.any(ProductRequestDto.class)))
                .thenReturn(failResponse);

        final POSProduct posProduct = targetPOSProductService.getPOSProductDetails(STORE_NUMBER, EAN);

        assertThat(posProduct.getError(), is("Timeout"));
        assertThat(posProduct.getSystemError(), is(false));
    }

    @Test
    public void testGetPOSProductDetailsIntegrationException() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException, TargetIntegrationException {
        final String exceptionMessage = "System error from client";

        setupStoreServiceMock();

        when(targetPOSProductClient.getPOSProductDetails(Mockito.any(ProductRequestDto.class))).thenThrow(
                new TargetIntegrationException(exceptionMessage));

        final POSProduct posProduct = targetPOSProductService.getPOSProductDetails(STORE_NUMBER, EAN);

        assertThat(posProduct.getError(), is(exceptionMessage));
        assertThat(posProduct.getSystemError(), is(true));
    }


    private void setupStoreServiceMock() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        Mockito.when(targetPointOfServiceService.getPOSByStoreNumber(STORE_NUMBER)).thenReturn(pos);
        Mockito.when(pos.getAddress()).thenReturn(address);
        Mockito.when(address.getDistrict()).thenReturn(STATE);
    }

    private ProductResponseDto createSuccessResponse() {

        final ProductResponseDto response = new ProductResponseDto();
        response.setDescription("description");
        response.setItemcode("1234");
        response.setErrorMessage("errorMessage");
        response.setPrice(Integer.valueOf(567));
        response.setWasPrice(Integer.valueOf(789));
        response.setSuccess(true);
        return response;
    }

    private ProductResponseDto createFailResponse() {

        final ProductResponseDto response = new ProductResponseDto();
        response.setErrorMessage("errorMessage");
        response.setSuccess(false);
        return response;
    }

}

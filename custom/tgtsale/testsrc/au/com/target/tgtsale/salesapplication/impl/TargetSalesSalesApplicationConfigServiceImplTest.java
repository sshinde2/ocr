package au.com.target.tgtsale.salesapplication.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;

import java.util.Collections;
import java.util.Map;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.SalesApplicationConfigModel;


/**
 * Test TargetSalesSalesApplicationConfigServiceImpl
 * 
 * @author jjayawa1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetSalesSalesApplicationConfigServiceImplTest {

    private static final SalesApplication EBAY_SALES_APP = SalesApplication.EBAY;

    @InjectMocks
    private final TargetSalesSalesApplicationConfigServiceImpl service = new TargetSalesSalesApplicationConfigServiceImpl();

    @Mock
    private GenericDao<SalesApplicationConfigModel> salesApplicationConfigDao;

    @Mock
    private SalesApplicationConfigModel salesAppConfig;

    private final Map<String, SalesApplication> params = Collections
            .singletonMap(SalesApplicationConfigModel.SALESAPPLICATION, EBAY_SALES_APP);

    @Test
    public void testIsSkipTlogsWhenAppConfigNull() {
        Mockito.doReturn(null).when(salesApplicationConfigDao).find(params);

        Assertions.assertThat(service.isSkipTlogs(EBAY_SALES_APP)).isFalse();
    }

    @Test
    public void testIsSkipTlogsFalse() {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.FALSE).when(salesAppConfig).isSkipTlogs();

        Assertions.assertThat(service.isSkipTlogs(EBAY_SALES_APP)).isFalse();
    }

    @Test
    public void testIsSkipTlogsTrue() {
        Mockito.doReturn(Collections.singletonList(salesAppConfig)).when(salesApplicationConfigDao).find(params);
        Mockito.doReturn(Boolean.TRUE).when(salesAppConfig).isSkipTlogs();

        Assertions.assertThat(service.isSkipTlogs(EBAY_SALES_APP)).isTrue();
    }
}

/**
 * 
 */
package au.com.target.tgtsale.tlog.converter.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.model.BuyGetDealModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.model.TargetPromotionOrderEntryConsumedModel;

import java.util.Map;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import au.com.target.tgtcore.enums.DealItemTypeEnum;
import au.com.target.tgtsale.tlog.data.DealResults;
import au.com.target.tgtsale.tlog.data.ItemDeal;


/**
 * @author htan3
 *
 */
@UnitTest
public class TlogDealConverterImplTest {

    private final TlogDealConverterImpl converter = new TlogDealConverterImpl();

    private DealResults dealResults;
    private TargetPromotionOrderEntryConsumedModel entryConsumedModel;
    private Map<String, String> deals;
    private AbstractOrderEntryModel orderEntry;
    private PromotionResultModel entryPromoResultModel;

    @Before
    public void setUp() {
        dealResults = mock(DealResults.class);
        entryConsumedModel = mock(TargetPromotionOrderEntryConsumedModel.class);
        deals = mock(Map.class);
        orderEntry = mock(AbstractOrderEntryModel.class);
        entryPromoResultModel = mock(PromotionResultModel.class);

        when(entryConsumedModel.getPromotionResult()).thenReturn(entryPromoResultModel);

    }

    @Test
    public void testReturnNullIfPromotionIsNotDeal() {
        final AbstractPromotionModel promotion = mock(AbstractPromotionModel.class);
        when(entryPromoResultModel.getPromotion()).thenReturn(promotion);

        assertNull(converter.populateItemDeal(orderEntry, entryConsumedModel, dealResults, deals));
    }

    @Test
    public void testReturnNullIfNoDealItemType() {
        final AbstractDealModel promotion = mock(AbstractDealModel.class);
        when(entryPromoResultModel.getPromotion()).thenReturn(promotion);
        when(entryConsumedModel.getDealItemType()).thenReturn(null);

        assertNull(converter.populateItemDeal(orderEntry, entryConsumedModel, dealResults, deals));
    }

    /**
     * Currently this test will fail and a IllegalArgumentException is thrown due to existing problem in the
     * TlogDealConverterImpl
     * 
     * 1) line 83 of TlogDealConverterImpl the message
     * "Cannot find the dealType for the class {} as it may not be configured in the deals for TLOG." should be
     * "Cannot find the dealType for the class {0} as it may not be configured in the deals for TLOG."
     * 
     * 2) In the private method TlogDealConverterImpl.getTransDealType it checks null and try to throw
     * InvalidObjectException, in the upper method it catches the exception and assign null, which makes throwing
     * exception completely no sense. If this is the correct behavior, the null can be return directly from the private
     * method without throwing the exception.
     */
    @Test
    @Ignore
    public void testThrowExceptionIfDealIsNotConfiguredInTlog() {
        //TODO fix the code and make this test passed.
        final AbstractDealModel promotion = mock(BuyGetDealModel.class);
        when(entryPromoResultModel.getPromotion()).thenReturn(promotion);
        when(entryConsumedModel.getDealItemType()).thenReturn(DealItemTypeEnum.REWARD);
        when(deals.get("BuyGetDealModel")).thenReturn(null);

        assertNotNull(converter.populateItemDeal(orderEntry, entryConsumedModel, dealResults, deals));
    }

    @Test
    public void testGetBuyGetDeal() {
        final AbstractDealModel promotion = new BuyGetDealModel();
        promotion.setCode("BuyGet");
        when(entryPromoResultModel.getPromotion()).thenReturn(promotion);
        when(entryConsumedModel.getDealItemType()).thenReturn(DealItemTypeEnum.REWARD);
        when(entryConsumedModel.getInstance()).thenReturn(Integer.valueOf(2));
        when(deals.get("BuyGetDealModel")).thenReturn("BuyGetDealModel");

        final ItemDeal itemDeal = converter.populateItemDeal(orderEntry, entryConsumedModel, dealResults, deals);

        verify(dealResults)
                .addDealDetailsForInstance("2", "BuyGet", "BuyGetDealModel", 0);
        assertNotNull(itemDeal);
        assertEquals("R", itemDeal.getType());
        assertEquals("BuyGet", itemDeal.getId());
        assertEquals("0", itemDeal.getMarkdown());
        assertEquals("2", itemDeal.getInstance());

    }
}

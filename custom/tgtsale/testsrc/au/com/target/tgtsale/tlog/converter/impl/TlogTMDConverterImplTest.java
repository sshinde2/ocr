/**
 * 
 */
package au.com.target.tgtsale.tlog.converter.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.promotions.jalo.PromotionResult;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.enums.DealItemTypeEnum;
import au.com.target.tgtcore.jalo.TMDiscountProductPromotion;
import au.com.target.tgtsale.tlog.data.DiscountInfo;
import au.com.target.tgtsale.tlog.data.DiscountTypeEnum;
import au.com.target.tgtsale.tlog.data.ItemDeal;
import au.com.target.tgtsale.tlog.data.ItemInfo;


/**
 * @author htan3
 *
 */
@UnitTest
public class TlogTMDConverterImplTest {
    private final TlogTMDConverterImpl tlogTMDConverter = new TlogTMDConverterImpl();

    private final PromotionResult promotionResult = mock(PromotionResult.class);

    @Before
    public void setUp() {
        final JaloSession session = mock(JaloSession.class);
        when(promotionResult.getSession()).thenReturn(session);
    }

    @Test
    public void testAddVariationAmtWithPostiveAmt() {
        final double variationInTmd = 0.02d;
        final List<ItemInfo> itemList = createItemList();
        final BigDecimal sum = BigDecimal.valueOf(itemList.get(4).getItemDiscount().getAmount().doubleValue()
                + variationInTmd);
        tlogTMDConverter.addVariationAmtToTMDDiscount(itemList, variationInTmd);
        Assert.assertEquals(itemList.get(4).getItemDiscount().getAmount(), sum);
    }

    @Test
    public void testAddVariationAmtWithNegativeVariationAmt() {
        final double variationInTmd = -0.02d;
        final List<ItemInfo> itemList = createItemList();
        final BigDecimal sum = BigDecimal.valueOf(itemList.get(4).getItemDiscount().getAmount().doubleValue()
                + variationInTmd);
        tlogTMDConverter.addVariationAmtToTMDDiscount(itemList, variationInTmd);
        Assert.assertEquals(itemList.get(4).getItemDiscount().getAmount(), sum);
    }

    @Test
    public void testAddVariationAmtDiscountLessThanVariation() {
        final double variationInTmd = -0.43d;
        final List<ItemInfo> itemList = createItemList();
        tlogTMDConverter.addVariationAmtToTMDDiscount(itemList, variationInTmd);
        Assert.assertEquals(itemList.get(4).getItemDiscount().getAmount(), BigDecimal.valueOf(0.42));
        Assert.assertEquals(itemList.get(5).getItemDiscount().getAmount(), BigDecimal.valueOf(0.42));
        Assert.assertEquals(itemList.get(6).getItemDiscount().getAmount(), BigDecimal.valueOf(0.42));
        Assert.assertEquals(itemList.get(7).getItemDiscount().getAmount(), BigDecimal.valueOf(0.42));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetDiscountInfoFromTMDProportionByValue() {
        when(promotionResult.getTotalDiscount()).thenReturn(20d);
        final TMDiscountProductPromotion mockTmdPromotion = mock(TMDiscountProductPromotion.class);
        when(promotionResult.getPromotion()).thenReturn(mockTmdPromotion);
        when(mockTmdPromotion.getPercentageDiscount()).thenReturn(Double.valueOf(15));
        when(mockTmdPromotion.getCode()).thenReturn("tmdCode");

        final DiscountInfo discount = tlogTMDConverter.getDiscountInfoFromTMDProportionByValue(promotionResult, 80, 30);

        Assertions.assertThat(discount.getPercentage()).isEqualTo(Double.valueOf(15));
        Assertions.assertThat(discount.getCode()).isEqualTo("tmdCode");
        Assertions.assertThat(Double.valueOf(discount.getAmount().doubleValue())).isEqualTo(
                Double.valueOf(20 * 30 / 100d));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetDiscountInfoFromTMDProportionByValueZeroEntry() {
        when(promotionResult.getTotalDiscount()).thenReturn(0d);
        final TMDiscountProductPromotion mockTmdPromotion = mock(TMDiscountProductPromotion.class);
        when(promotionResult.getPromotion()).thenReturn(mockTmdPromotion);
        when(mockTmdPromotion.getPercentageDiscount()).thenReturn(Double.valueOf(15));
        when(mockTmdPromotion.getCode()).thenReturn("tmdCode");

        final DiscountInfo discount = tlogTMDConverter.getDiscountInfoFromTMDProportionByValue(promotionResult, 0, 0);

        Assertions.assertThat(discount).isNull();
    }

    private ItemInfo createItemInfo(final String prdCode, final long quantity, final BigDecimal price,
            final BigDecimal filePrice) {
        final ItemInfo item = new ItemInfo();
        item.setFilePrice(filePrice);
        item.setPrice(price);
        item.setQuantity(Long.valueOf(quantity));
        item.setId(prdCode);
        return item;
    }

    private DiscountInfo createItemDiscountInfo(final DiscountTypeEnum discountType, final String code,
            final Double percentage,
            final BigDecimal amount) {
        final DiscountInfo discountInfo = new DiscountInfo();
        discountInfo.setType(discountType);
        discountInfo.setCode(code);
        discountInfo.setPercentage(percentage);
        discountInfo.setAmount(amount);
        return discountInfo;
    }

    private ItemDeal createItemDeals(final String dealId, final String instance, final DealItemTypeEnum dealItemType,
            final double markdown) {
        final ItemDeal deal = new ItemDeal();
        deal.setId(dealId);
        deal.setInstance(instance);
        deal.setType(dealItemType.getCode());
        deal.setMarkdown(String.valueOf(markdown));
        return deal;

    }

    private List<ItemInfo> createItemList() {
        final Double percent = Double.valueOf(5);
        final List<ItemInfo> itemList = new ArrayList<>();
        final ItemDeal deal1 = createItemDeals("102", "1", DealItemTypeEnum.QUALIFIER, 0);
        final ItemDeal deal2 = createItemDeals("102", "2", DealItemTypeEnum.QUALIFIER, 0);
        final ItemDeal deal3 = createItemDeals("102", "3", DealItemTypeEnum.QUALIFIER, 0);
        final ItemDeal deal4 = createItemDeals("102", "4", DealItemTypeEnum.QUALIFIER, 0);
        final ItemDeal deal5 = createItemDeals("102", "1", DealItemTypeEnum.REWARD, 8.5);
        final ItemDeal deal6 = createItemDeals("102", "2", DealItemTypeEnum.REWARD, 8.5);
        final ItemDeal deal7 = createItemDeals("102", "3", DealItemTypeEnum.REWARD, 8.5);
        final ItemDeal deal8 = createItemDeals("102", "4", DealItemTypeEnum.REWARD, 8.5);
        final DiscountInfo discount1 = createItemDiscountInfo(DiscountTypeEnum.TEAM_MEMBER, "TMD-TGT-NonApparel",
                percent,
                BigDecimal.valueOf(0.42));
        final DiscountInfo discount2 = createItemDiscountInfo(DiscountTypeEnum.TEAM_MEMBER, "TMD-TGT-NonApparel",
                percent,
                BigDecimal.valueOf(0.42));
        final DiscountInfo discount3 = createItemDiscountInfo(DiscountTypeEnum.TEAM_MEMBER, "TMD-TGT-NonApparel",
                percent,
                BigDecimal.valueOf(0.42));
        final DiscountInfo discount4 = createItemDiscountInfo(DiscountTypeEnum.TEAM_MEMBER, "TMD-TGT-NonApparel",
                percent,
                BigDecimal.valueOf(0.42));
        final ItemInfo itemInfo1 = createItemInfo("CP2010", 1L, BigDecimal.valueOf(8.5), BigDecimal.valueOf(8.5));
        final ItemInfo itemInfo2 = createItemInfo("CP2010", 1L, BigDecimal.valueOf(8.5), BigDecimal.valueOf(8.5));
        final ItemInfo itemInfo3 = createItemInfo("CP2010", 1L, BigDecimal.valueOf(8.5), BigDecimal.valueOf(8.5));
        final ItemInfo itemInfo4 = createItemInfo("CP2010", 1L, BigDecimal.valueOf(8.5), BigDecimal.valueOf(8.5));
        final ItemInfo itemInfo5 = createItemInfo("CP2011", 1L, BigDecimal.valueOf(8.5), BigDecimal.valueOf(8.5));
        final ItemInfo itemInfo6 = createItemInfo("CP2011", 1L, BigDecimal.valueOf(8.5), BigDecimal.valueOf(8.5));
        final ItemInfo itemInfo7 = createItemInfo("CP2011", 1L, BigDecimal.valueOf(8.5), BigDecimal.valueOf(8.5));
        final ItemInfo itemInfo8 = createItemInfo("CP2011", 1L, BigDecimal.valueOf(8.5), BigDecimal.valueOf(8.5));
        itemInfo1.setItemDeal(deal1);
        itemInfo2.setItemDeal(deal2);
        itemInfo3.setItemDeal(deal3);
        itemInfo4.setItemDeal(deal4);
        itemInfo5.setItemDeal(deal5);
        itemInfo5.setItemDiscount(discount1);
        itemInfo6.setItemDeal(deal6);
        itemInfo6.setItemDiscount(discount2);
        itemInfo7.setItemDeal(deal7);
        itemInfo7.setItemDiscount(discount3);
        itemInfo8.setItemDeal(deal8);
        itemInfo8.setItemDiscount(discount4);
        itemList.add(itemInfo1);
        itemList.add(itemInfo2);
        itemList.add(itemInfo3);
        itemList.add(itemInfo4);
        itemList.add(itemInfo5);
        itemList.add(itemInfo6);
        itemList.add(itemInfo7);
        itemList.add(itemInfo8);
        return itemList;
    }
}

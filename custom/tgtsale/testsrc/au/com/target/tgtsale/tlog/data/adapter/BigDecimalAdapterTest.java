/**
 * 
 */
package au.com.target.tgtsale.tlog.data.adapter;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.Test;


/**
 * @author vmuthura
 * 
 */
@UnitTest
public class BigDecimalAdapterTest {

    private final BigDecimalAdapter adapter = new BigDecimalAdapter();

    @Test
    public void testAmount() throws Exception {
        final BigDecimal amount = new BigDecimal("125.30");
        final String inCents = "12530";

        assertEquals("Marshalled Value", inCents, adapter.marshal(amount));
        assertEquals("UnMarshalled Value", amount.setScale(2, RoundingMode.HALF_UP), adapter.unmarshal(inCents));
    }

    @Test
    public void testDollarValueOnly() throws Exception {
        final BigDecimal amount = new BigDecimal("19.00");
        final String inCents = "1900";

        assertEquals("Marshalled Value", inCents, adapter.marshal(amount));
        assertEquals("UnMarshalled Value", amount.setScale(2, RoundingMode.HALF_UP), adapter.unmarshal(inCents));
    }

    @Test
    public void testCentValueOnly() throws Exception {
        final BigDecimal amount = new BigDecimal("00.93");
        final String inCents = "093";

        assertEquals("Marshalled Value", inCents, adapter.marshal(amount));
        assertEquals("UnMarshalled Value", amount.setScale(2, RoundingMode.HALF_UP), adapter.unmarshal(inCents));
    }

    @Test
    public void testDoubleValue() throws Exception {
        final BigDecimal amount = new BigDecimal("123.40345");
        final String inCents = "12340";

        assertEquals("Marshalled Value", inCents, adapter.marshal(amount));
        assertEquals("UnMarshalled Value", amount.setScale(2, RoundingMode.HALF_UP), adapter.unmarshal(inCents));
    }

    @Test
    public void testNegativeDollarValueOnly() throws Exception {
        final BigDecimal amount = new BigDecimal("-19.00");
        final String inCents = "-1900";

        assertEquals("Marshalled Value", inCents, adapter.marshal(amount));
        assertEquals("UnMarshalled Value", amount.setScale(2, RoundingMode.HALF_UP), adapter.unmarshal(inCents));
    }

    @Test
    public void testNegativeCentValueOnly() throws Exception {
        final BigDecimal amount = new BigDecimal("-00.93");
        final String inCents = "-093";

        assertEquals("Marshalled Value", inCents, adapter.marshal(amount));
        assertEquals("UnMarshalled Value", amount.setScale(2, RoundingMode.HALF_UP), adapter.unmarshal(inCents));
    }

    @Test
    public void testNegativeDoubleValue() throws Exception {
        final BigDecimal amount = new BigDecimal("-123.40345");
        final String inCents = "-12340";

        assertEquals("Marshalled Value", inCents, adapter.marshal(amount));
        assertEquals("UnMarshalled Value", amount.setScale(2, RoundingMode.HALF_UP), adapter.unmarshal(inCents));
    }

    @Test
    public void testNegativeAmount() throws Exception {
        final BigDecimal amount = new BigDecimal("-125.30");
        final String inCents = "-12530";

        assertEquals("Marshalled Value", inCents, adapter.marshal(amount));
        assertEquals("UnMarshalled Value", amount.setScale(2, RoundingMode.HALF_UP), adapter.unmarshal(inCents));
    }


}

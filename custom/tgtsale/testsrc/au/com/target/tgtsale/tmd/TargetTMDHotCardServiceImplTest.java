/**
 * 
 */
package au.com.target.tgtsale.tmd;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtsale.model.TargetTMDHotCardModel;
import au.com.target.tgtsale.tmd.impl.TargetTMDHotCardServiceImpl;
import junit.framework.Assert;


/**
 * @author phi.tran
 * 
 */
@UnitTest
public class TargetTMDHotCardServiceImplTest {

    @Mock
    private TargetTMDHotCardDao targetTMDHotCardDao;
    @Mock
    private ModelService modelService;

    private TargetTMDHotCardServiceImpl targetTMDHotCardService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        targetTMDHotCardService = new TargetTMDHotCardServiceImpl();
        targetTMDHotCardService.setModelService(modelService);
        targetTMDHotCardService.setTargetTMDHotCardDao(targetTMDHotCardDao);
    }

    @Test
    public void testSaveCardNumberExists() throws Exception {

        final long cardNum = Long.parseLong(RandomStringUtils.randomNumeric(13), 10);
        final TargetTMDHotCardModel card = new TargetTMDHotCardModel();
        card.setCardNum(Long.valueOf(cardNum));
        BDDMockito.given(targetTMDHotCardDao.getTargetHotCardModelByCardNumber(cardNum)).willReturn(card);

        targetTMDHotCardService.save(cardNum);
        BDDMockito.verify(modelService, BDDMockito.times(0)).save(BDDMockito.any());
    }

    @Test
    public void testSaveCardNumberNotExists() throws Exception {
        final long cardNum = Long.parseLong(RandomStringUtils.randomNumeric(13), 10);
        BDDMockito.given(targetTMDHotCardDao.getTargetHotCardModelByCardNumber(cardNum))
                .willThrow(new TargetUnknownIdentifierException("junit"));
        final TargetTMDHotCardModel newCard = new TargetTMDHotCardModel();
        BDDMockito.given(modelService.create(TargetTMDHotCardModel.class)).willReturn(newCard);

        targetTMDHotCardService.save(cardNum);
        BDDMockito.verify(modelService).save(BDDMockito.any());
    }

    @Test
    public void testGetTargetHotCardModelByCardCodeCardNumberNotExists() throws Exception {

        final Long cardNum = Long.valueOf(RandomStringUtils.randomNumeric(13), 10);
        final TargetTMDHotCardModel card = new TargetTMDHotCardModel();
        card.setCardNum(cardNum);
        BDDMockito.given(targetTMDHotCardDao.getTargetHotCardModelByCardNumber(cardNum.longValue())).willReturn(card);

        final TargetTMDHotCardModel result = targetTMDHotCardService.getTargetHotCardModelByCardCode(cardNum
                .longValue());
        Assert.assertEquals(result.getCardNum(), cardNum);
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testGetTargetHotCardModelByCardCodeCardNumberExists() throws Exception {
        final long cardNum = Long.parseLong(RandomStringUtils.randomNumeric(13), 10);
        BDDMockito.given(targetTMDHotCardDao.getTargetHotCardModelByCardNumber(cardNum))
                .willThrow(new TargetUnknownIdentifierException("junit"));

        targetTMDHotCardService.getTargetHotCardModelByCardCode(cardNum);
    }

    @Test
    public void testExistsTrue() throws Exception {
        final long cardNum = Long.parseLong(RandomStringUtils.randomNumeric(13), 10);
        final TargetTMDHotCardModel card = new TargetTMDHotCardModel();
        card.setCardNum(Long.valueOf(cardNum));
        BDDMockito.given(targetTMDHotCardDao.getTargetHotCardModelByCardNumber(cardNum)).willReturn(card);

        Assert.assertTrue(targetTMDHotCardService.exists(cardNum));

    }

    @Test
    public void testExistFalse() throws Exception {
        final long cardNum = Long.parseLong(RandomStringUtils.randomNumeric(13), 10);
        BDDMockito.given(targetTMDHotCardDao.getTargetHotCardModelByCardNumber(cardNum))
                .willThrow(new TargetUnknownIdentifierException("junit"));

        Assert.assertFalse(targetTMDHotCardService.exists(cardNum));
    }

    @Test
    public void testRemoveHotCardDuplicateCards() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        BDDMockito.given(targetTMDHotCardDao.getTargetHotCardModelByCardNumber(1L)).willThrow(
                new TargetAmbiguousIdentifierException("duplicate cards"));
        targetTMDHotCardService.remove(1L);

        Mockito.verify(modelService, Mockito.times(0)).remove(Mockito.any(TargetTMDHotCardModel.class));
    }

    @Test
    public void testRemoveHotCardDoesntExist() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        BDDMockito.given(targetTMDHotCardDao.getTargetHotCardModelByCardNumber(1L)).willThrow(
                new TargetUnknownIdentifierException("no card"));
        targetTMDHotCardService.remove(1L);

        Mockito.verify(modelService, Mockito.times(0)).remove(Mockito.any(TargetTMDHotCardModel.class));
    }

    @Test
    public void testRemoveHotCardExists() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final TargetTMDHotCardModel tmdHotCard = Mockito.mock(TargetTMDHotCardModel.class);
        BDDMockito.given(targetTMDHotCardDao.getTargetHotCardModelByCardNumber(1L)).willReturn(tmdHotCard);
        targetTMDHotCardService.remove(1L);

        Mockito.verify(modelService).remove(tmdHotCard);
    }
}

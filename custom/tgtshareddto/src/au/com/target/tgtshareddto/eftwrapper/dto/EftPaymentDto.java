/**
 * 
 */
package au.com.target.tgtshareddto.eftwrapper.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;



/**
 * dummy dto's create for POC
 * 
 * @author rsamuel3
 * 
 */
@XmlRootElement(name = "eftPayment")
@XmlAccessorType(XmlAccessType.FIELD)
public class EftPaymentDto implements Serializable
{
    @XmlElement
    private String orderId;
    @XmlElement
    private PaymentDetailsDto paymentDetails;

    /**
     * @return the orderId
     */
    public String getOrderId()
    {
        return orderId;
    }

    /**
     * @param orderId
     *            the orderId to set
     */
    public void setOrderId(final String orderId)
    {
        this.orderId = orderId;
    }

    /**
     * @return the paymentDetails
     */
    public PaymentDetailsDto getPaymentDetails()
    {
        return paymentDetails;
    }

    /**
     * @param paymentDetails
     *            the paymentDetails to set
     */
    public void setPaymentDetails(final PaymentDetailsDto paymentDetails)
    {
        this.paymentDetails = paymentDetails;
    }
}

/**
 * 
 */
package au.com.target.tgtshareddto.eftwrapper.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * @author rsamuel3
 * 
 */
public class HybrisResponseDto implements Serializable {
    private String code;

    private boolean successStatus;

    private List<String> messages;

    private boolean recoverableError;

    public HybrisResponseDto() {
        super();
        messages = new ArrayList<>();
    }

    public HybrisResponseDto(final String code) {
        super();
        this.code = code;
        messages = new ArrayList<>();
    }


    /**
     * @return the productCode
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the successStatus
     */
    public boolean isSuccessStatus() {
        return successStatus;
    }

    /**
     * @param successStatus
     *            the successStatus to set
     */
    public void setSuccessStatus(final boolean successStatus) {
        this.successStatus = successStatus;
    }

    /**
     * @return the messages
     */
    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(final List<String> msgs) {
        this.messages = msgs;
    }

    public void addMessage(final String msg) {
        messages.add(msg);
    }

    public void addMessages(final List<String> msgs) {
        messages.addAll(msgs);
    }

    /**
     * @return the recoverableError
     */
    public boolean isRecoverableError() {
        return recoverableError;
    }

    /**
     * @param recoverableError
     *            the recoverableError to set
     */
    public void setRecoverableError(final boolean recoverableError) {
        this.recoverableError = recoverableError;
    }
}

/**
 * 
 */
package au.com.target.endeca.infront.assemble;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.endeca.infront.assembler.Assembler;
import com.endeca.infront.assembler.AssemblerException;
import com.endeca.infront.assembler.AssemblerFactory;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.ContentInclude;
import com.endeca.infront.cartridge.ContentSlotConfig;
import com.endeca.infront.cartridge.RedirectAwareContentInclude;
import com.endeca.infront.cartridge.model.DimensionSearchGroup;
import com.endeca.infront.cartridge.model.DimensionSearchValue;

import au.com.target.endeca.http.request.TgtHttpServletRequest;
import au.com.target.endeca.infront.cache.EndecaDimensionCacheService;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaWrapperException;
import au.com.target.endeca.infront.parser.EndecaDataParser;
import au.com.target.endeca.infront.querybuilder.EndecaQueryParamBuilder;
import au.com.target.endeca.infront.service.EndecaDimensionService;
import au.com.target.endeca.infront.web.spring.TgtSpringUtility;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * This is the base class which does the assembling work. All classes which needs to call the Assembler need to extend
 * this class.
 * 
 * @author pthoma20
 * 
 */
public abstract class EndecaAssemble {


    protected static final Logger LOG = Logger.getLogger(EndecaAssemble.class);

    protected EndecaDataParser endecaDataParser;

    protected EndecaDimensionCacheService endecaDimensionCacheService;

    protected EndecaDimensionService endecaDimensionService;

    private AssemblerFactory assemblerFactory;

    private TgtSpringUtility springUtility;

    private Map<String, String> iosAppRecordFilters;

    /**
     * This method is used to call the Assembler Factory. In order to create the query parameters the Endeca Search
     * State Data is passed to the Endeca Query Param Builder.
     * 
     * @param request
     * @param pageURI
     * @param endecaSearchStateData
     * @return contentItem
     */
    protected ContentItem callAssembler(final HttpServletRequest request, final String pageURI,
            final EndecaSearchStateData endecaSearchStateData) throws TargetEndecaWrapperException {
        ContentItem responseContentItem = null;
        if (null != endecaSearchStateData) {

            excludeUnavailableProduct(request, endecaSearchStateData);
            final String queryString = EndecaQueryParamBuilder.constructQueryParams(endecaSearchStateData);

            try {

                final Assembler assembler = assemblerFactory.createAssembler();
                ContentItem contentItem = null;
                if (endecaSearchStateData.isSkipRedirect()) {
                    contentItem = new ContentInclude(pageURI);
                }
                else {
                    contentItem = new RedirectAwareContentInclude(pageURI);
                }
                LOG.info("Query string value is " + queryString);
                final TgtHttpServletRequest tgtHttpServletRequest = new TgtHttpServletRequest(request);
                tgtHttpServletRequest.setQueryString(queryString);
                springUtility.setTgtHttpServletRequest(tgtHttpServletRequest);
                responseContentItem = assembler.assemble(contentItem);
                checkEndecaResponse(responseContentItem);
            }
            catch (final AssemblerException ae) {
                LOG.error(SplunkLogFormatter.formatMessage(
                        "Exception while calling Assembler ",
                        TgtutilityConstants.EndecaErrorCodes.ENDECA_GENERIC_ASSEMBLER_EXCEPTION, ""), ae);
                throw new TargetEndecaWrapperException(ae.getMessage(), ae.getCause());
            }
        }

        return responseContentItem;

    }



    protected void checkEndecaResponse(final ContentItem responseContentItem) throws TargetEndecaWrapperException {
        if (null != responseContentItem
                && responseContentItem.containsKey(EndecaConstants.EndecaParserNodes.ENDECA_ERROR)) {
            final Object errorFromEndecaAssembler = responseContentItem
                    .get(EndecaConstants.EndecaParserNodes.ENDECA_ERROR);
            if (null != errorFromEndecaAssembler) {
                throw new TargetEndecaWrapperException(String.valueOf(errorFromEndecaAssembler));
            }
        }
    }

    /**
     * use the content slot config to do the dynamic content search
     * 
     * @param request
     * @param endecaSearchStateData
     * @throws TargetEndecaWrapperException
     */
    public ContentItem dynamicContentAssemble(final HttpServletRequest request,
            final EndecaSearchStateData endecaSearchStateData, final ContentSlotConfig ContentSlotConfig)
            throws TargetEndecaWrapperException {

        ContentItem responseContentItem = null;
        if (null != endecaSearchStateData && null != ContentSlotConfig) {
            if ((ControllerConstants.MOBILE_APP_AUTO_SEARCH).equalsIgnoreCase(request.getRequestURI())) {
                excludeUnavailableProduct(request, endecaSearchStateData);
            }

            final String queryString = EndecaQueryParamBuilder.constructQueryParams(endecaSearchStateData);
            LOG.info("Query string value is " + queryString);
            try {
                final Assembler assembler = assemblerFactory.createAssembler();
                final TgtHttpServletRequest tgtHttpServletRequest = new TgtHttpServletRequest(request);
                tgtHttpServletRequest.setQueryString(queryString);
                springUtility.setTgtHttpServletRequest(tgtHttpServletRequest);
                responseContentItem = assembler.assemble(ContentSlotConfig);
                checkEndecaResponse(responseContentItem);
            }
            catch (final AssemblerException ae) {
                LOG.error(SplunkLogFormatter.formatMessage(
                        "Exception while calling Assembler ",
                        TgtutilityConstants.EndecaErrorCodes.ENDECA_GENERIC_ASSEMBLER_EXCEPTION, ""), ae);
                throw new TargetEndecaWrapperException(ae.getMessage(), ae.getCause());
            }
        }
        return responseContentItem;
    }

    /**
     * This method will extract the navigation State from the dimension search result.
     * 
     * @param contentItem
     * @return string
     */
    protected String getNavStateFromDimensionSearch(final ContentItem contentItem) {

        try {

            if (contentItem == null) {
                return "";
            }
            final ContentItem dimensionSearchResults = (ContentItem)contentItem
                    .get(EndecaConstants.DIMENSION_SEARCH_RESULTS);

            if (dimensionSearchResults == null) {
                return "";
            }
            final List<ContentItem> dimensionSearchGroupList = (List<ContentItem>)(dimensionSearchResults)
                    .get(EndecaConstants.DIMENSION_SEARCH_GROUPS);
            if (CollectionUtils.isEmpty(dimensionSearchGroupList)) {
                return "";
            }
            final DimensionSearchGroup dimensionSearchGroup = (DimensionSearchGroup)(dimensionSearchGroupList).get(0);
            if (dimensionSearchGroup == null || dimensionSearchGroup.getDimensionSearchValues() == null) {
                return "";
            }
            for (final DimensionSearchValue dimensionSearchValue : dimensionSearchGroup.getDimensionSearchValues()) {
                if (dimensionSearchValue != null && StringUtils.isNotEmpty(dimensionSearchValue.getNavigationState())) {
                    final String navigationState = dimensionSearchValue.getNavigationState();
                    final String navId = returnNavIdIfNavStateNotEncoded(navigationState);
                    if (StringUtils.isNotEmpty(navId)) {
                        return navId;
                    }
                    return navigationState;
                }
            }
        }
        catch (final Exception e) {
            LOG.error(SplunkLogFormatter.formatMessage(
                    "Failed to retrieve Nav State from Dimension Search",
                    TgtutilityConstants.EndecaErrorCodes.ENDECA_GENERIC_ASSEMBLER_EXCEPTION, ""), e);
        }
        return "";
    }

    private String returnNavIdIfNavStateNotEncoded(final String navState) {
        final String nonEncodedNavStateIdentifier = EndecaConstants.QUERY_PARAM_SEPERATOR_QNMRK
                + EndecaConstants.EndecaFilterQuery.ENDECA_NAV_STATE_TERM;
        if (StringUtils.contains(navState, nonEncodedNavStateIdentifier)) {
            return StringUtils
                    .replace(navState,
                            nonEncodedNavStateIdentifier, "");
        }
        return "";
    }


    /**
     * This method will give the existing navigation State by splitting it from the current Uri.
     * 
     * @param uri
     * @return string
     */
    protected String getExistingNavigtaionStateUri(final String uri) {
        if (StringUtils.contains(uri, EndecaConstants.ENDECA_NAVIGATION_STATE_SEPERATOR)) {
            return StringUtils.substring(uri, uri.indexOf(EndecaConstants.ENDECA_NAVIGATION_STATE_SEPERATOR));
        }
        return "";
    }

    /**
     * check if it is the ios app, exclude the unavailable product
     * 
     * @param request
     * @param searchStateData
     */
    private void excludeUnavailableProduct(final HttpServletRequest request,
            final EndecaSearchStateData searchStateData) {
        if (BooleanUtils.isTrue((Boolean)(request.getSession().getAttribute(TgtFacadesConstants.IOS_APP_WV)))) {
            Map<String, String> recordFilters = searchStateData.getRecordFilters();

            if (recordFilters == null) {
                recordFilters = new HashMap<>();
                searchStateData.setRecordFilters(recordFilters);
            }

            recordFilters.putAll(iosAppRecordFilters);
        }
    }



    /**
     * @param endecaDataParser
     *            the endecaDataParser to set
     */
    @Required
    public void setEndecaDataParser(final EndecaDataParser endecaDataParser) {
        this.endecaDataParser = endecaDataParser;
    }

    /**
     * @param assemblerFactory
     *            the assemblerFactory to set
     */
    @Required
    public void setAssemblerFactory(final AssemblerFactory assemblerFactory) {
        this.assemblerFactory = assemblerFactory;
    }


    /**
     * @param springUtility
     *            the springUtility to set
     */
    @Required
    public void setSpringUtility(final TgtSpringUtility springUtility) {
        this.springUtility = springUtility;
    }

    /**
     * @param endecaDimensionCacheService
     *            the endecaDimensionCacheService to set
     */
    @Required
    public void setEndecaDimensionCacheService(final EndecaDimensionCacheService endecaDimensionCacheService) {
        this.endecaDimensionCacheService = endecaDimensionCacheService;
    }


    /**
     * @param iosAppRecordFilters
     *            the iosAppRecordFilters to set
     */
    @Required
    public void setIosAppRecordFilters(final Map<String, String> iosAppRecordFilters) {
        this.iosAppRecordFilters = iosAppRecordFilters;
    }

    /**
     * @param endecaDimensionService
     *            the endecaDimensionService to set
     */
    @Required
    public void setEndecaDimensionService(final EndecaDimensionService endecaDimensionService) {
        this.endecaDimensionService = endecaDimensionService;
    }


}

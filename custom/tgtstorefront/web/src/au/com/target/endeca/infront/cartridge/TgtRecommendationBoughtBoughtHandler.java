package au.com.target.endeca.infront.cartridge;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.util.TargetEndecaUtil;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.RecordSpotlight;
import com.endeca.infront.cartridge.RecordSpotlightConfig;
import com.endeca.infront.cartridge.RecordSpotlightHandler;
import com.endeca.infront.cartridge.RecordSpotlightSelection;
import com.endeca.infront.cartridge.model.Record;
import com.endeca.infront.cartridge.support.StratifyHelper;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.model.FilterState;
import com.endeca.infront.navigation.request.MdexQuery;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.infront.navigation.request.RecordsMdexQuery;
import com.endeca.navigation.AggrERec;
import com.endeca.navigation.AggrERecList;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERec;
import com.endeca.navigation.Navigation;
import com.endeca.navigation.PropertyMap;


/**
 * The Class TgtRecommendationBoughtBoughtHandler.
 * 
 * @author ragarwa3
 */
public class TgtRecommendationBoughtBoughtHandler extends RecordSpotlightHandler {

    protected static final Logger LOG = Logger.getLogger(TgtRecommendationBoughtBoughtHandler.class);

    protected String mNavFilter = "0";
    protected MdexRequest mMdexRequest;
    protected FilterState mFilterState;
    protected MdexQuery mMdexQuery;

    private String rollupKey;
    private TargetEndecaUtil targetEndecaUtil;

    private String recommendationProductSplitConfig;


    @Override
    protected RecordSpotlightConfig wrapConfig(final ContentItem pContentItem) {
        return new TgtRecommendationBoughtBoughtConfig(pContentItem);
    }

    /**
     * Creates the mdex record for bought bought recommendations.
     *
     * @param pFilterState
     *            the filter state
     * @param pMdexQuery
     *            the mdex query
     * @param recommProductCodes
     * @return the mdex request
     * @throws CartridgeHandlerException
     *             the cartridge handler exception
     */
    protected MdexRequest createMdexRecordForBoughtBoughtRecommendation(final FilterState pFilterState,
            final MdexQuery pMdexQuery, final List recommProductCodes) throws CartridgeHandlerException {

        mFilterState = pFilterState;
        mMdexQuery = pMdexQuery;

        final List<String> recordFiltersMod = new ArrayList<>();
        // product should be available online
        recordFiltersMod.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_DISPLAY_FLAG
                + EndecaConstants.EndecaFilterQuery.ENDECA_FILTER_KEY_VALUE_SEPERATOR + EndecaConstants.ENDECA_NP_1);
        // product should have stock
        recordFiltersMod.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_INSTOCK_FLAG
                + EndecaConstants.EndecaFilterQuery.ENDECA_FILTER_KEY_VALUE_SEPERATOR + EndecaConstants.ENDECA_NP_1);
        recordFiltersMod.add(targetEndecaUtil.createRecordFilterForColourVariants(recommProductCodes));
        mFilterState.setRecordFilters(recordFiltersMod);

        if (pMdexQuery instanceof RecordsMdexQuery) {
            final RecordsMdexQuery recordsQuery = (RecordsMdexQuery)pMdexQuery;
            recordsQuery.setStratify(StratifyHelper.getStratifyEqlString(
                    targetEndecaUtil.createStratifyForOrderedRecommendations(recommProductCodes), null));
            mMdexQuery = recordsQuery;
        }

        initNavFilterState();

        mMdexRequest = super.createMdexRequest(mFilterState, mMdexQuery);
        return mMdexRequest;
    }

    @Override
    public void preprocess(final RecordSpotlightConfig cartridgeConfig) throws CartridgeHandlerException {

        final TgtRecommendationBoughtBoughtConfig tgtCartridgeConfig = (TgtRecommendationBoughtBoughtConfig)cartridgeConfig;

        tgtCartridgeConfig.setRecordSelection(new RecordSpotlightSelection());

        final NavigationState navigationState = getNavigationState();
        final RecordsMdexQuery recordsQuery = new RecordsMdexQuery();

        recordsQuery.setRecordsPerPage(tgtCartridgeConfig.getMaxNumRecords());
        recordsQuery.setFieldNames(combineFieldNames(navigationState, tgtCartridgeConfig));

        final FilterState filterState = navigationState.getFilterState();
        final String prodCodesForRecomm = ObjectUtils.toString(cartridgeConfig
                .get(EndecaConstants.EndecaParamMarshaller.ENDECA_PRODUCT_CODES));

        if (StringUtils.isNotBlank(prodCodesForRecomm)) {
            final List recordFilters = new ArrayList<>();
            recordFilters.add(targetEndecaUtil.createRecordFilterForColourVariants(Arrays.asList(prodCodesForRecomm
                    .split(EndecaConstants.EndecaFilterQuery.ENDECA_MULTIPLE_FILTER_VALUES_SEPERATOR))));
            filterState.setRollupKey(rollupKey);
            filterState.setRecordFilters(recordFilters);
        }

        mFilterState = filterState;
        mMdexQuery = recordsQuery;
        initNavFilterState();

        mMdexRequest = createMdexRequest(mFilterState, mMdexQuery);
    }

    @Override
    public RecordSpotlight process(final RecordSpotlightConfig cartridgeConfig) throws CartridgeHandlerException {

        final TgtRecommendationBoughtBoughtConfig tgtCartridgeConfig = (TgtRecommendationBoughtBoughtConfig)cartridgeConfig;

        // getting the records for which recommendations needs to be fetched
        FilterState initFilterState = initNavFilterState();
        ENEQueryResults eneResults = executeMdexRequest(mMdexRequest);

        // preparing the query for recommneded products
        final Map<String, List<String>> prodCodeAndRecomCode = new HashMap<String, List<String>>();
        List<String> recommRecords = getRecommendedProductsList(eneResults, cartridgeConfig, prodCodeAndRecomCode);
        if (CollectionUtils.isNotEmpty(recommRecords)) {

            recommRecords = validateAndFilterRecommendedProductData(recommRecords);
            sortRecommendationBasedOnSales(recommRecords);

            final List recommProductCodes = new LinkedList();
            for (final String recommProduct : recommRecords) {
                recommProductCodes
                        .add(recommProduct.split(EndecaConstants.ENDECA_CONCATENATE_SYMBOL_RECOMMENDATIONS)[1]);
            }

            // now querying the details of recommended products
            initFilterState = initNavFilterState();
            initFilterState.setRollupKey(rollupKey);

            alterMaxProductPerPageConfig(mMdexQuery, prodCodeAndRecomCode);
            mMdexRequest = createMdexRecordForBoughtBoughtRecommendation(initFilterState, mMdexQuery,
                    recommProductCodes);
            eneResults = executeMdexRequest(mMdexRequest);

            final NavigationState navigationState = getNavigationState();
            navigationState.inform(eneResults);

            final RecordSpotlight resultsList = new RecordSpotlight(tgtCartridgeConfig);
            final List<Record> records = getMaxRecomRecordsPerProduct(createRecords(eneResults, navigationState,
                    tgtCartridgeConfig), prodCodeAndRecomCode);
            resultsList.setRecords(targetEndecaUtil.enrichEndecaRecords(records, true));

            return resultsList;
        }
        else {
            LOG.warn("No recommendations found in Endeca for the requested products.");
        }

        return null;
    }

    /**
     * Change result per page configuration for cart page based on number of products
     * 
     * @param recordsQuery
     * @param prodCodeAndRecomCode
     */
    private void alterMaxProductPerPageConfig(final MdexQuery recordsQuery,
            final Map<String, List<String>> prodCodeAndRecomCode) {
        if (recordsQuery instanceof RecordsMdexQuery && !prodCodeAndRecomCode.isEmpty()
                && prodCodeAndRecomCode.size() > 1) {
            ((RecordsMdexQuery)recordsQuery).setRecordsPerPage(((RecordsMdexQuery)recordsQuery).getRecordsPerPage()
                    * prodCodeAndRecomCode.size());
        }
    }

    /**
     * Once after getting all in-stock products filter out the based on cart products
     * 
     * @param records
     * @param prodCodeAndRecomCode
     * @return ArrayList<Record>
     */
    protected List<Record> getMaxRecomRecordsPerProduct(final List<Record> records,
            final Map<String, List<String>> prodCodeAndRecomCode) {
        if (!prodCodeAndRecomCode.isEmpty() && prodCodeAndRecomCode.size() > 1) {
            final List<Record> uniqueRecords = new LinkedList<Record>(records);
            //Configuration to recomProducts/product for eg. config is 10,5,3,2,2,1
            //for eg.if cart has 2 products then 5 recommendations to be shown
            final List<String> recomProductsPerProduct = Arrays.asList(recommendationProductSplitConfig.split(","));
            //maximum recommendations to be shown per product
            final int tempMaxConfigureRecomSize = prodCodeAndRecomCode.size() > recomProductsPerProduct.size() ? Integer
                    .parseInt(recomProductsPerProduct.get(recomProductsPerProduct.size() - 1))
                    : Integer.parseInt(recomProductsPerProduct.get(prodCodeAndRecomCode.size() - 1));
            for (final Map.Entry<String, List<String>> entry : prodCodeAndRecomCode.entrySet()) {
                final Iterator<Record> recordIterator = records.iterator();
                removeFilteredRecords(uniqueRecords, tempMaxConfigureRecomSize, entry, recordIterator);
            }
            uniqueRecords.removeAll(records);
            return uniqueRecords;
        }
        return records;
    }

    /**
     * Select max per product and remove the selected products
     * 
     * @param uniqueRecords
     * @param tempMaxConfigureRecomSize
     * @param entry
     * @param recordIterator
     */
    private void removeFilteredRecords(final List<Record> uniqueRecords, final int tempMaxConfigureRecomSize,
            final Map.Entry<String, List<String>> entry, final Iterator<Record> recordIterator) {
        int counter = 0;
        while (recordIterator.hasNext()) {
            final Record currentRecord = recordIterator.next();
            if (entry.getValue().contains(
                    currentRecord.getAttributes().get(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING)
                            .toString())
                    && counter <= tempMaxConfigureRecomSize) {
                counter++;
                //remove the added element to avoid iteration for same item again
                recordIterator.remove();
                if (counter == tempMaxConfigureRecomSize) {
                    break;
                }
            }
        }
    }

    /**
     * Gets the recommended products list.
     *
     * @param eneResults
     *            the ene results
     * @param cartridgeConfig
     * @return the recommended products list
     */
    protected List<String> getRecommendedProductsList(final ENEQueryResults eneResults,
            final RecordSpotlightConfig cartridgeConfig, final Map<String, List<String>> prodCodeAndRecomCodes) {
        final List<String> recommendedProducts = new ArrayList<>();
        final List<String> productsForRecom = Arrays.asList(ObjectUtils.toString(cartridgeConfig
                .get(EndecaConstants.EndecaParamMarshaller.ENDECA_PRODUCT_CODES)).split(","));
        final Navigation navigation = eneResults.getNavigation();
        if (null != navigation) {
            final AggrERecList aggRecList = navigation.getAggrERecs();
            if (CollectionUtils.isNotEmpty(aggRecList)) {
                if (CollectionUtils.isNotEmpty(productsForRecom) && productsForRecom.size() == 1) {
                    fillSingleProductRecomms(recommendedProducts, aggRecList, productsForRecom);
                }
                else if (productsForRecom.size() > 1) {
                    fillMultipleProductsRecomms(recommendedProducts, productsForRecom, aggRecList,
                            prodCodeAndRecomCodes);
                }
            }
        }

        return recommendedProducts;
    }

    /**
     * Get the single product recommendations from first record itself
     * 
     * @param recommendedProducts
     * @param aggRecList
     * @param productsForRecom
     */
    private void fillSingleProductRecomms(final List<String> recommendedProducts, final AggrERecList aggRecList,
            final List<String> productsForRecom) {
        final AggrERec aggRec = (AggrERec)aggRecList.iterator().next();
        final ERec eRec = aggRec.getRepresentative();
        if (null != eRec) {
            final PropertyMap propMap = eRec.getProperties();
            final List<String> productCodes = (List<String>)propMap
                    .getValues(EndecaConstants.SEARCH_FIELD_RECOMMENDED_PRODUCTS);
            if (CollectionUtils.isNotEmpty(productCodes)) {
                recommendedProducts.addAll(productCodes);
            }
        }
    }

    /**
     * This method is to populate recommendations for multiple products say on cart page. There are some rules around
     * cart page **1.if cart has 1 product then the recommendations will picked as per config for eg.10 if 2 then 5,if 4
     * then 2.**2.Also there should not be duplicate recommendations repeated.**3.There should not be products which is
     * already there in cart
     * 
     * 
     * @param recommendedProducts
     * @param productsForRecom
     * @param aggRecList
     * @param prodCodeAndRecomCodes
     */
    private void fillMultipleProductsRecomms(final List<String> recommendedProducts,
            final List<String> productsForRecom, final AggrERecList aggRecList,
            final Map<String, List<String>> prodCodeAndRecomCodes) {
        for (final Object aggRecObject : aggRecList) {
            if (aggRecObject instanceof AggrERec) {
                final AggrERec aggRec = (AggrERec)aggRecObject;
                final ERec eRec = aggRec.getRepresentative();
                if (null != eRec) {
                    final PropertyMap propMap = eRec.getProperties();
                    filterProductRecomms(recommendedProducts, propMap, productsForRecom, prodCodeAndRecomCodes);
                }
            }
        }
    }

    /**
     * Filter the recommended products based on the cart products. Say if recommended product is there in cart then
     * ignore
     * 
     * @param recomProducts
     * @param propMap
     * @param productsForRecom
     * @param prodCodeAndRecomCodes
     */
    private void filterProductRecomms(final List<String> recomProducts, final PropertyMap propMap,
            final List<String> productsForRecom, final Map<String, List<String>> prodCodeAndRecomCodes) {
        final List<String> productCodes = (List<String>)propMap
                .getValues(EndecaConstants.SEARCH_FIELD_RECOMMENDED_PRODUCTS);
        final List<String> cartProdCode = (List<String>)propMap
                .getValues(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING);
        List<String> currentProductRecomms = null;
        if (CollectionUtils.isNotEmpty(productCodes)) {
            sortRecommendationBasedOnSales(productCodes);
            currentProductRecomms = new ArrayList<>();
            for (final String code : productCodes) {
                final String currentProduct = code.split(EndecaConstants.ENDECA_CONCATENATE_SYMBOL_RECOMMENDATIONS)[1];
                if (!productsForRecom.contains(currentProduct) && !recomProducts.contains(code)) {
                    recomProducts.add(code);
                    currentProductRecomms.add(currentProduct);
                }
            }
            prodCodeAndRecomCodes.put(cartProdCode.get(0), currentProductRecomms);
        }
    }

    /**
     * Validate and filter recommended product data.
     *
     * @param recommRecords
     *            the recomm records
     * @return the list
     */
    protected List<String> validateAndFilterRecommendedProductData(final List<String> recommRecords) {
        final List validRecommProducts = new ArrayList<>();
        for (final String recommProduct : recommRecords) {
            if (recommProduct.contains(EndecaConstants.ENDECA_CONCATENATE_SYMBOL_RECOMMENDATIONS)) {
                validRecommProducts.add(recommProduct);
            }
            else {
                LOG.warn("Invalid data found for recommendedProduct as:" + recommProduct);
            }
        }
        return validRecommProducts;
    }

    private void sortRecommendationBasedOnSales(final List<String> recommRecords) {

        Collections.sort(recommRecords, new Comparator<String>() {

            @Override
            public int compare(final String recomm1, final String recomm2) {
                final Integer salesRecomm1 = Integer.valueOf(recomm1
                        .split(EndecaConstants.ENDECA_CONCATENATE_SYMBOL_RECOMMENDATIONS)[0]);
                final Integer salesRecomm2 = Integer.valueOf(recomm2
                        .split(EndecaConstants.ENDECA_CONCATENATE_SYMBOL_RECOMMENDATIONS)[0]);
                return salesRecomm2.compareTo(salesRecomm1);
            }
        });
    }

    private FilterState initNavFilterState() {
        final List navFilters = new ArrayList<>();
        if (getNavFilter() != null) {
            navFilters.add(getNavFilter());
        }
        else {
            navFilters.add(EndecaConstants.ENDECA_N_0);
        }
        mFilterState.setNavigationFilters(navFilters);
        return mFilterState;
    }

    /**
     * @return the nav filter
     */
    public String getNavFilter() {
        return mNavFilter;
    }

    /**
     * @param pNavFilter
     *            the new nav filter
     */
    public void setNavFilter(final String pNavFilter) {
        mNavFilter = pNavFilter;
    }

    /**
     * @return the mFilterState
     */
    public FilterState getFilterState() {
        return mFilterState;
    }

    /**
     * @param filterState
     *            the mFilterState to set
     */
    public void setFilterState(final FilterState filterState) {
        this.mFilterState = filterState;
    }

    /**
     * @param rollupKey
     *            the rollupKey to set
     */
    @Required
    public void setRollupKey(final String rollupKey) {
        this.rollupKey = rollupKey;
    }

    /**
     * @param targetEndecaUtil
     *            the targetEndecaUtil to set
     */
    @Required
    public void setTargetEndecaUtil(final TargetEndecaUtil targetEndecaUtil) {
        this.targetEndecaUtil = targetEndecaUtil;
    }

    /**
     * @return the recommendationProductSplitConfig
     */
    public String getRecommendationProductSplitConfig() {
        return recommendationProductSplitConfig;
    }

    /**
     * @param recommendationProductSplitConfig
     *            the recommendationProductSplitConfig to set
     */
    @Required
    public void setRecommendationProductSplitConfig(final String recommendationProductSplitConfig) {
        this.recommendationProductSplitConfig = recommendationProductSplitConfig;
    }

}

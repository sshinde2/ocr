/**
 * 
 */
package au.com.target.endeca.infront.cartridge;

/**
 * @author smudumba
 *
 */
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.event.request.RequestEventFactory;
import com.endeca.infront.cartridge.ResultsList;
import com.endeca.infront.cartridge.ResultsListConfig;
import com.endeca.infront.cartridge.ResultsListHandler;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.model.FilterState;
import com.endeca.infront.navigation.model.SearchFilter;
import com.endeca.infront.navigation.request.MdexQuery;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.navigation.ENEQueryResults;

import au.com.target.endeca.infront.util.TargetEndecaUtil;


public class TgtRecordSearchAutoSuggestHandler extends ResultsListHandler {

    protected static final Logger LOG = Logger.getLogger(TgtRecordSearchAutoSuggestHandler.class);
    protected String searchKey;
    protected String mNavFilter = "0";
    protected MdexRequest mMdexRequest;
    protected FilterState mFilterState;
    protected MdexQuery mMdexQuery;
    private TargetEndecaUtil targetEndecaUtil;

    @Override
    protected MdexRequest createMdexRequest(final FilterState pFilterState,
            final MdexQuery pMdexQuery) throws CartridgeHandlerException {

        mFilterState = pFilterState;
        mMdexQuery = pMdexQuery;
        if (searchKey != null && pFilterState != null && pFilterState.getSearchFilters() != null) {
            for (final SearchFilter search : pFilterState.getSearchFilters()) {
                search.setKey(searchKey);
            }
        }

        mMdexRequest = super.createMdexRequest(pFilterState, pMdexQuery);
        return mMdexRequest;

    }

    @Override
    public ResultsList process(final ResultsListConfig cartridgeConfig) throws CartridgeHandlerException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("RecordsSearch: process!");
            LOG.debug("RecordSearch: process: PRE navigationFilters " + mFilterState.getNavigationFilters());
        }

        final ENEQueryResults results = executeMdexRequest(mMdexRequest);

        LOG.debug("Record Search: mMdexRequest: " + mMdexRequest);
        final NavigationState navigationState = getNavigationState();
        navigationState.inform(results);

        final ResultsList resultsList = new ResultsList(cartridgeConfig);
        resultsList.setRecords(createRecords(results, navigationState,
                cartridgeConfig));

        if (!resultsList.getRecords().isEmpty()) {
            resultsList.setFirstRecNum(cartridgeConfig.getOffset() + 1);
            resultsList.setLastRecNum(cartridgeConfig.getOffset()
                    + resultsList.getRecords().size());
        }
        resultsList.setTotalNumRecs(getTotalNumRecs(results));
        if (LOG.isDebugEnabled()) {
            LOG.debug("RecordSearch: totalNumRecs: " + getTotalNumRecs(results));
            LOG.debug("RecordSearch: getRecords " + resultsList.getRecords().size());
        }
        resultsList.setRecsPerPage(cartridgeConfig.getRecordsPerPage());

        // Navigation Event Logging
        if (RequestEventFactory.isLoggingEnabled()) {
            dispatchNavigationEventInformation(cartridgeConfig, resultsList);
        }

        resultsList.setRecords(targetEndecaUtil.enrichEndecaRecords(resultsList.getRecords(), false));
        return resultsList;
    }


    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(final String searchKey) {
        this.searchKey = searchKey;
    }

    public String getNavFilter() {
        return mNavFilter;
    }

    public void setNavFilter(final String pNavFilter) {
        mNavFilter = pNavFilter;
    }

    protected FilterState initNavFilterState() {
        final List navFilters = new ArrayList<>();
        if (getNavFilter() != null) {
            navFilters.add(getNavFilter());
            LOG.debug("getNavFilter is " + getNavFilter());
        }
        else {
            navFilters.add("0");
        }
        mFilterState.setNavigationFilters(navFilters);
        return mFilterState;
    }

    /**
     * @param targetEndecaUtil
     *            the new target endeca util
     */
    @Required
    public void setTargetEndecaUtil(final TargetEndecaUtil targetEndecaUtil) {
        this.targetEndecaUtil = targetEndecaUtil;
    }

}

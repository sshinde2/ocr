package au.com.target.endeca.infront.cartridge;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.target.endeca.infront.constants.EndecaConstants;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.ResultsList;
import com.endeca.infront.cartridge.ResultsListConfig;
import com.endeca.infront.cartridge.ResultsListHandler;
import com.endeca.infront.cartridge.model.SortOptionConfig;
import com.endeca.infront.cartridge.model.SortOptionLabel;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.model.SearchFilter;
import com.endeca.infront.navigation.model.SortSpec;


public class TgtResultsListHandler extends
        ResultsListHandler
{
    protected static final Logger LOG = Logger.getLogger(TgtResultsListHandler.class);

    private static final String RELEVANCE_RANKING_TOKENIZER = "@@QUERY@@";

    @Override
    public void preprocess(final ResultsListConfig cartridgeConfig)
            throws CartridgeHandlerException
    {
        final List<SortSpec> sortSpecList = new LinkedList<>();

        if (cartridgeConfig.getSortOption() != null) {
            final List<SortSpec> existingSortSpecs = cartridgeConfig.getSortOption().getSorts();
            sortSpecList.addAll(validateAndAddSortOptions(existingSortSpecs));
            cartridgeConfig.getSortOption().setSorts(sortSpecList);
        }


        final NavigationState navState = getNavigationState();
        if (null != navState && null != navState.getFilterState()) {
            final List<SearchFilter> searchFilterList = navState.getFilterState().getSearchFilters();

            if (CollectionUtils.isNotEmpty(searchFilterList)) {
                if (null != searchFilterList.get(0)) {
                    final String terms = searchFilterList.get(0).getTerms();
                    cartridgeConfig.setRelRankStrategy(StringUtils.replace(cartridgeConfig.getRelRankStrategy(),
                            RELEVANCE_RANKING_TOKENIZER, terms));
                }
            }
        }

        super.preprocess(cartridgeConfig);

    }

    /**
     * Validate whether the sort option passed from end exists. If it is invalid revert to default sort as per
     * Experience Manager.
     * 
     * @param existingSortSpecs
     * @return validatedSortSpecs
     */
    protected List<SortSpec> validateAndAddSortOptions(final List<SortSpec> existingSortSpecs) {
        final List<SortOptionConfig> sortOptionConfigs = getSortOptions();
        final Set<String> validSortOptionValues = new HashSet<>();
        if (CollectionUtils.isNotEmpty(sortOptionConfigs)) {
            for (final SortOptionConfig sortOptionConfig : sortOptionConfigs) {
                if (sortOptionConfig != null && sortOptionConfig.getValue() != null) {
                    final String sortOptionConfigValue = sortOptionConfig.getValue();
                    String[] options = { sortOptionConfigValue };
                    if (sortOptionConfigValue.contains(EndecaConstants.QUERY_PARAM_DOUBLE_PIPE)) {
                        options = StringUtils.splitByWholeSeparator(sortOptionConfigValue,
                                EndecaConstants.QUERY_PARAM_DOUBLE_PIPE);
                    }
                    if (options != null && options.length > 0) {
                        for (final String option : options) {
                            if (option.contains(EndecaConstants.QUERY_PARAM_SINGLE_PIPE)) {
                                validSortOptionValues
                                        .add(StringUtils.split(option, EndecaConstants.QUERY_PARAM_SINGLE_PIPE)[0]);
                            }
                            else {
                                validSortOptionValues.add(sortOptionConfigValue);
                            }
                        }
                    }
                }
            }
        }
        final List<SortSpec> validatedSortSpecs = new LinkedList<>();
        for (final SortSpec sortSpec : existingSortSpecs) {
            if (validSortOptionValues.contains(sortSpec.getKey())) {
                validatedSortSpecs.add(sortSpec);
            }
            else {
                LOG.error("ENDECA-INVALID-SORTOPTION : " + sortSpec.getKey());
            }
        }
        return validatedSortSpecs;

    }

    @Override
    public ResultsList process(final ResultsListConfig cartridgeConfig)
            throws CartridgeHandlerException
    {
        final ResultsList resultsList = super.process(cartridgeConfig);
        resultsList.setSortOptions(populateSortOptions(cartridgeConfig));
        return resultsList;
    }

    /**
     * Duplicating the method from the parent and setting the default Sort Option Label with Selected value
     * 
     * @param resultsListConfig
     * @return sortOptionLabelList
     */
    private List<SortOptionLabel> populateSortOptions(final ResultsListConfig resultsListConfig)
    {
        final List<SortOptionConfig> sortOptionConfigs = getSortOptions();
        final NavigationState navigationState = getNavigationState();

        final List sortOptionLabelList = new ArrayList<>();
        String sortParam;
        if (null != sortOptionConfigs)
        {
            sortParam = null;
            if ((resultsListConfig.getSortOption() != null))
            {
                sortParam = resultsListConfig.getSortOption().toString();
            }

            for (final SortOptionConfig sortOptionConfig : sortOptionConfigs) {
                final SortOptionLabel sortOptionLabel = new SortOptionLabel();

                sortOptionLabel.setLabel(sortOptionConfig.getLabel());
                populateNavigationPathDefaults(sortOptionLabel);

                if (((null == sortParam) ? "" : sortParam).equals(sortOptionConfig.getValue())) {
                    sortOptionLabel.setSelected(true);
                }

                if (StringUtils.isBlank(sortOptionConfig.getValue()))
                {
                    sortOptionLabel.setNavigationState(navigationState.removeParameter("No").removeParameter("Ns")
                            .toString());
                }
                else
                {
                    sortOptionLabel.setNavigationState(navigationState.removeParameter("No")
                            .putParameter("Ns", sortOptionConfig.getValue()).toString());

                }

                sortOptionLabelList.add(sortOptionLabel);
            }
        }

        return sortOptionLabelList;
    }
}

/**
 * 
 */
package au.com.target.endeca.infront.dto;

import org.codehaus.jackson.annotate.JsonProperty;


/**
 * @author bhuang3
 *
 */
public class DeliveryModePostcodeDto {

    @JsonProperty
    private String postcode;

    /**
     * @return the postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * @param postcode
     *            the postcode to set
     */
    public void setPostcode(final String postcode) {
        this.postcode = postcode;
    }



}

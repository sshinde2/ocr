/*

 */

package au.com.target.endeca.infront.logger;


import org.apache.log4j.Logger;

import au.com.target.tgtutility.constants.TgtutilityConstants;

import com.endeca.infront.assembler.event.AssemblerEvent;
import com.endeca.infront.assembler.event.AssemblerEventAdapter;


/**
 * Listen to events dispatched by the assembler and log them through slf4j.
 */
public class Log4JAssemblerEventLogger
        extends AssemblerEventAdapter
{
    private final Logger logger = Logger.getLogger(this.getClass());

    @Override
    public void assemblyStarting(final AssemblerEvent event)
    {
        logger.info("assembly starting");
    }

    @Override
    public void assemblyComplete(final AssemblerEvent event)
    {
        logger.info("assembly complete");
    }

    @Override
    public void assemblyError(final AssemblerEvent event)
    {
        logger.error("assembly error:", event.getError());
    }

    @Override
    public void cartridgePreprocessStarting(final AssemblerEvent event)
    {
        logger.debug("preprocessing " + event.getContentItem().getType() + " using "
                + event.getCartridgeHandler().getClass());
    }

    @Override
    public void cartridgePreprocessComplete(final AssemblerEvent event)
    {
        logger.debug("preprocessed " + event.getContentItem().getType() + " using "
                + event.getCartridgeHandler().getClass());
    }

    @Override
    public void cartridgeProcessStarting(final AssemblerEvent event)
    {
        logger.debug("process " + event.getContentItem().getType() + " using " + event.getCartridgeHandler().getClass());
    }

    @Override
    public void cartridgeProcessComplete(final AssemblerEvent event)
    {
        if (event.getContentItem() != null) {
            logger.debug("processed " + event.getContentItem().getType() + " using "
                    + event.getCartridgeHandler().getClass());
        }
        else {
            logger.debug("content item removed during process step by " + event.getCartridgeHandler().getClass());
        }
    }

    @Override
    public void cartridgeError(final AssemblerEvent event)
    {
        logger.error(TgtutilityConstants.EndecaErrorCodes.ENDECA_ASSEMBLER_EXCEPTION + "cartridge error:",
                event.getError());
    }
}

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.breadcrumb;

import java.util.List;

import au.com.target.tgtstorefront.controllers.pages.data.BreadcrumbData;


/**
 * ResourceBreadcrumbBuilder builds a list of breadcrumbs based on a resource key
 */
public interface ResourceBreadcrumbBuilder
{

    /**
     * 
     * @param resourceKey
     *            represent message key
     * @return breadcrumbs
     * @throws IllegalArgumentException
     */
    public List<Breadcrumb> getBreadcrumbs(String resourceKey) throws IllegalArgumentException;

    /**
     * 
     * @param data
     * @return breadcrumbs
     * @throws IllegalArgumentException
     */
    public List<Breadcrumb> getBreadcrumbs(BreadcrumbData... data) throws IllegalArgumentException;

}

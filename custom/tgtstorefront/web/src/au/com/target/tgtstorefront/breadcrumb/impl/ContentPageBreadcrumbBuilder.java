/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.breadcrumb.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.util.NavigationNodeUtils;


public class ContentPageBreadcrumbBuilder
{
    private static final String LAST_LINK_CLASS = "active";

    @Autowired
    private Converter<CategoryModel, CategoryData> categoryConverter;

    @Autowired
    private UrlResolver<CategoryData> categoryDataUrlResolver;

    /**
     * Creates the breadcrumbs for a given page. Will be in the correct order, with the last item in the list being the
     * current page.
     * 
     * @param page
     *            The model for the current page
     * @return A list of Breadcrumbs
     */
    public List<Breadcrumb> getBreadcrumbs(final ContentPageModel page)
    {
        final List<Breadcrumb> pageBreadcrumbs = new ArrayList<>();

        final List<CMSNavigationNodeModel> pageNavNodes = page.getNavigationNodeList();

        List<CMSNavigationNodeModel> navNodeTreeForPage = null;
        if (CollectionUtils.isNotEmpty(pageNavNodes)) {
            navNodeTreeForPage = getNavNodeTreeForPage(pageNavNodes.get(0));
        }

        if (CollectionUtils.isNotEmpty(navNodeTreeForPage)) {
            for (final CMSNavigationNodeModel navNode : navNodeTreeForPage) {
                String linkClass = "";
                if (pageBreadcrumbs.isEmpty()) {
                    linkClass = LAST_LINK_CLASS;
                }

                final Breadcrumb pageBreadcrumb = createBreadcrumb(navNode, linkClass);

                if (pageBreadcrumb != null) {
                    pageBreadcrumbs.add(pageBreadcrumb);
                }
            }
        }
        else {
            pageBreadcrumbs.add(createBreadcrumbForContentPage(page, LAST_LINK_CLASS, null));
        }

        Collections.reverse(pageBreadcrumbs);

        return pageBreadcrumbs;
    }

    private Breadcrumb createBreadcrumb(final CMSNavigationNodeModel navigationNode, final String linkClass) {
        final List<CMSNavigationEntryModel> entries = navigationNode.getEntries();

        if (CollectionUtils.isEmpty(entries)) {
            return null;
        }

        final String navigationNodeTitle = NavigationNodeUtils.getNavigationNodeTitle(navigationNode, true);

        for (final CMSNavigationEntryModel entry : entries) {
            final ItemModel entryItem = entry.getItem();

            if (entryItem instanceof ContentPageModel) {
                final ContentPageModel contentPage = (ContentPageModel)entryItem;

                return createBreadcrumbForContentPage(contentPage, linkClass, navigationNodeTitle);
            }
            else if (entryItem instanceof CMSLinkComponentModel) {
                final CMSLinkComponentModel cmsLinkComponent = (CMSLinkComponentModel)entryItem;

                return createBreadcrumbForCMSLinkComponent(cmsLinkComponent, linkClass, navigationNodeTitle);
            }
            else if (entryItem instanceof CategoryModel) {
                final CategoryModel category = (CategoryModel)entryItem;

                return createBreadcrumbForCategory(category, linkClass, navigationNodeTitle);
            }
        }

        return null;
    }

    private Breadcrumb createBreadcrumbForContentPage(final ContentPageModel contentPage, final String linkClass,
            final String navNodeName) {
        String breadcrumbName = navNodeName;

        if (StringUtils.isBlank(breadcrumbName)) {
            breadcrumbName = contentPage.getTitle();
        }

        if (StringUtils.isBlank(breadcrumbName))
        {
            breadcrumbName = contentPage.getName();
        }

        String label = contentPage.getLabel();
        if (StringUtils.isBlank(label)) {
            label = "";
        }

        return new Breadcrumb(label, breadcrumbName, linkClass);
    }

    private Breadcrumb createBreadcrumbForCMSLinkComponent(final CMSLinkComponentModel cmsLinkComponent,
            final String linkClass, final String navNodeName) {
        String breadcrumbName = navNodeName;

        if (StringUtils.isBlank(breadcrumbName)) {
            breadcrumbName = cmsLinkComponent.getName();
        }

        String url = cmsLinkComponent.getUrl();
        if (StringUtils.isBlank(url)) {
            url = "";
        }

        return new Breadcrumb(url, breadcrumbName, linkClass);
    }

    private Breadcrumb createBreadcrumbForCategory(final CategoryModel category, final String linkClass,
            final String navNodeName) {
        String breadcrumbName = navNodeName;

        if (StringUtils.isBlank(breadcrumbName)) {
            breadcrumbName = category.getName();
        }

        final CategoryData categoryData = categoryConverter.convert(category);
        final String url = categoryDataUrlResolver.resolve(categoryData);

        return new Breadcrumb(url, breadcrumbName, linkClass);
    }

    private List<CMSNavigationNodeModel> getNavNodeTreeForPage(final CMSNavigationNodeModel navNodeForPage) {
        CMSNavigationNodeModel currentNavNode = navNodeForPage;
        final List<CMSNavigationNodeModel> navNodeTree = new ArrayList<>();
        while (currentNavNode != null) {
            navNodeTree.add(currentNavNode);
            currentNavNode = currentNavNode.getParent();
        }

        // Assume that for any given content page there will be 2 levels above it
        if (navNodeTree.size() < 3) {
            return null;
        }

        return navNodeTree.subList(0, navNodeTree.size() - 2);
    }
}

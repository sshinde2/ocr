/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.breadcrumb.impl;

import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.search.facetdata.BreadcrumbData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.promotions.model.AbstractSimpleDealModel;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.endeca.navigation.DimLocationList;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.MutableDimLocation;
import com.endeca.navigation.MutableDimLocationList;
import com.endeca.navigation.MutableDimVal;
import com.endeca.soleng.urlformatter.UrlFormatException;
import com.endeca.soleng.urlformatter.UrlState;

import au.com.target.endeca.infront.cache.EndecaDimensionCacheService;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaAncestor;
import au.com.target.endeca.infront.data.EndecaBreadCrumb;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.transform.TargetEndecaURLHelper;
import au.com.target.endeca.soleng.urlformatter.TargetURLFormatter;
import au.com.target.tgtcore.deals.TargetDealService;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.kiosk.util.BulkyBoardHelper;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * SearchBreadcrumbBuilder implementation for
 * {@link de.hybris.platform.commerceservices.search.facetdata.FacetSearchPageData}
 */
public class SearchBreadcrumbBuilder implements EndecaConstants {

    protected static final Logger LOG = Logger.getLogger(SearchBreadcrumbBuilder.class);

    private static final String LAST_LINK_CLASS = "active";




    private CommerceCategoryService commerceCategoryService;
    private UrlResolver<CategoryModel> categoryModelUrlResolver;
    private UrlResolver<BrandModel> brandModelUrlResolver;
    private UrlResolver<TargetDealCategoryModel> dealCategoryModelUrlResolver;
    private TargetDealService targetDealService;
    private TargetEndecaURLHelper targetEndecaURLHelper;

    @Resource
    private BulkyBoardHelper bulkyBoardHelper;

    private EndecaDimensionCacheService endecaDimensionCacheService;

    private TargetURLFormatter targetUrlFormatter;

    /**
     * @param bulkyBoardHelper
     *            the bulkyBoardHeler to set
     */
    @Required
    public void setBulkyBoardHelper(final BulkyBoardHelper bulkyBoardHelper) {
        this.bulkyBoardHelper = bulkyBoardHelper;
    }

    /**
     * @param endecaDimensionCacheService
     *            the endecaDimensionCacheService to set
     */
    @Required
    public void setEndecaDimensionCacheService(final EndecaDimensionCacheService endecaDimensionCacheService) {
        this.endecaDimensionCacheService = endecaDimensionCacheService;
    }

    /**
     * @param targetUrlFormatter
     *            the targetUrlFormatter to set
     */
    @Required
    public void setTargetUrlFormatter(final TargetURLFormatter targetUrlFormatter) {
        this.targetUrlFormatter = targetUrlFormatter;
    }

    /**
     * @param targetEndecaURLHelper
     *            the targetEndecaURLHelper to set
     */
    public void setTargetEndecaURLHelper(final TargetEndecaURLHelper targetEndecaURLHelper) {
        this.targetEndecaURLHelper = targetEndecaURLHelper;
    }

    public List<Breadcrumb> getBreadcrumbs(final ProductSearchPageData<SearchStateData, ProductData> searchPageData)
            throws IllegalArgumentException {
        return getBreadcrumbs(null, null, searchPageData);
    }

    public List<Breadcrumb> getBreadcrumbs(final CategoryModel category, final BrandModel brand,
            final ProductSearchPageData<SearchStateData, ProductData> searchPageData)
            throws IllegalArgumentException {
        final List<Breadcrumb> breadcrumbs = new ArrayList<>();

        //final boolean emptyBreadcrumbs = CollectionUtils.isEmpty(searchPageData.getBreadcrumbs());
        final Breadcrumb breadcrumb;
        if (category == null) {
            breadcrumb = new Breadcrumb(ControllerConstants.SEARCH.concat("?text=")
                    + getEncodedUrl(searchPageData.getFreeTextSearch()),
                    searchPageData.getFreeTextSearch(), LAST_LINK_CLASS);
            breadcrumbs.add(breadcrumb);
        }
        else if (brand == null) {
            // Create category hierarchy path for breadcrumb
            final List<Breadcrumb> categoryBreadcrumbs = new ArrayList<>();
            final Collection<CategoryModel> categoryModels = new ArrayList<>();
            categoryModels.addAll(category.getSupercategories());
            categoryBreadcrumbs.add(getCategoryBreadcrumb(category, LAST_LINK_CLASS));

            while (!categoryModels.isEmpty()) {
                final CategoryModel categoryModel = categoryModels.iterator().next();
                if (!(categoryModel instanceof ClassificationClassModel)) {
                    if (categoryModel != null) {
                        final List<CategoryModel> supercategories = categoryModel.getSupercategories();
                        if (supercategories.isEmpty()) {
                            break; // If the current category has no supercategories then it's the top level and shouldn't be displayed.
                        }

                        categoryBreadcrumbs.add(getCategoryBreadcrumb(categoryModel));
                        categoryModels.clear();
                        categoryModels.addAll(supercategories);
                    }
                }
            }
            Collections.reverse(categoryBreadcrumbs);
            breadcrumbs.addAll(categoryBreadcrumbs);

        }
        else {
            // Create brand hierarchy path for breadcrumb
            breadcrumbs.add(getBrandBreadcrumb(brand, LAST_LINK_CLASS));
        }
        return breadcrumbs;
    }

    protected String getEncodedUrl(final String url) {
        try {
            if (null != url) {
                return URLEncoder.encode(url, "utf-8");
            }
            else {
                return StringUtils.EMPTY;
            }
        }
        catch (final UnsupportedEncodingException e) {
            return url;
        }
    }

    protected Breadcrumb getCategoryBreadcrumb(final CategoryModel category) {
        return getCategoryBreadcrumb(category, null);
    }

    protected Breadcrumb getCategoryBreadcrumb(final CategoryModel category, final String linkClass) {
        final String categoryUrl = getCategoryModelUrlResolver().resolve(category);
        return new Breadcrumb(categoryUrl, category.getName(), linkClass, category.getCode());
    }

    protected Breadcrumb getCategoryBreadcrumb(final String categoryCode, final String categoryName,
            final String lastLinkClass) {
        CategoryModel category = null;
        if (StringUtils.isNotBlank(categoryCode)) {
            category = commerceCategoryService.getCategoryForCode(categoryCode);
            return getCategoryBreadcrumb(category, lastLinkClass);
        }
        return new Breadcrumb(null, categoryName, lastLinkClass);
    }

    protected Breadcrumb getCategoryBreadcrumbForSearch(final String categoryCode, final String categoryName,
            final String searchText,
            final String lastLinkClass) {

        String url = null;
        final String encodedNavState = getNavState(categoryCode, null);
        if (StringUtils.isNotEmpty(encodedNavState)) {
            url = ControllerConstants.SEARCH
                    + encodedNavState
                    + EndecaConstants.QUERY_PARAM_SEPERATOR_AMP + "text="
                    + getEncodedUrl(searchText);
        }

        if (StringUtils.isNotBlank(url)) {
            return new Breadcrumb(url, categoryName, lastLinkClass);
        }
        return new Breadcrumb(null, categoryName, lastLinkClass);
    }

    /**
     * @param categoryCode
     * @param brandName
     * @return encodedNavState
     */
    protected String getNavState(final String categoryCode, final String brandName) {
        String encodedNavState = StringUtils.EMPTY;
        if (StringUtils.isNotEmpty(categoryCode) || StringUtils.isNotEmpty(brandName)) {
            try {
                final DimLocationList dimLocationList = getDimLocationList(categoryCode, brandName);

                final UrlState urlState = new UrlState(targetUrlFormatter, EndecaConstants.CODE_LITERAL);
                urlState.setNavState(dimLocationList);
                encodedNavState = targetUrlFormatter.formatUrl(urlState);
            }
            catch (final UrlFormatException e) {
                LOG.error(SplunkLogFormatter.formatMessage(
                        "URL FORMATTER EXCEPTION WHILE BUILDING BREADCRUMB URLS FOR REFINEMENTS IN SEARCH PAGE",
                        TgtutilityConstants.ErrorCode.ERR_SEARCH, ""), e);
            }
        }
        return encodedNavState;
    }

    /**
     * @param categoryCode
     * @param brandName
     * @return DimLocationList
     */
    protected DimLocationList getDimLocationList(final String categoryCode, final String brandName) {
        final MutableDimLocationList dimLocationList = new MutableDimLocationList();
        final Map<String, String> dimensions = new HashMap<>();
        dimensions.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY, categoryCode);
        dimensions.put(EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND, brandName);

        try {
            for (final Entry<String, String> dimensionEntry : dimensions.entrySet()) {
                if (StringUtils.isNotEmpty(dimensionEntry.getValue())) {
                    final String dimensionValue = endecaDimensionCacheService.getDimension(
                            dimensionEntry.getKey(), dimensionEntry.getValue());

                    final MutableDimLocation dimLocation = new MutableDimLocation();
                    final MutableDimVal dimVal = new MutableDimVal();
                    dimVal.setDimValId(Long.parseLong(dimensionValue));
                    dimLocation.setDimValue(dimVal);
                    dimLocationList.appendDimLocation(dimLocation);
                }
            }
        }
        catch (final ENEQueryException e) {
            LOG.error(SplunkLogFormatter.formatMessage(
                    " EXCEPTION WHEN QUERYING CACHE FOR CATEGORY ",
                    TgtutilityConstants.EndecaErrorCodes.ENDECA_ENE_QUERY_EXCEPTION, ""), e);
        }
        catch (final TargetEndecaException e) {
            LOG.error(SplunkLogFormatter.formatMessage(
                    "CATEGORY CODE NOT FOUND IN CACHE FOR CATEGORY",
                    TgtutilityConstants.ErrorCode.ERR_SEARCH, ""), e);
        }
        return dimLocationList;
    }

    protected Breadcrumb getBrandBreadcrumb(final BrandModel brand) {
        return getBrandBreadcrumb(brand, null);
    }

    protected Breadcrumb getBrandBreadcrumb(final BrandModel brand, final String linkClass) {
        final String brandUrl = getBrandModelUrlResolver().resolve(brand);
        return new Breadcrumb(brandUrl, brand.getName(), linkClass);
    }

    protected CommerceCategoryService getCommerceCategoryService() {
        return commerceCategoryService;
    }

    protected Breadcrumb getDealCategoryBreadcrumb(final TargetDealCategoryModel dealCategoryModel,
            final String linkClass) {
        final String dealCategoryUrl = dealCategoryModelUrlResolver.resolve(dealCategoryModel);
        final AbstractSimpleDealModel dealModel = targetDealService.getDealByCategory(dealCategoryModel);
        String breadcrumbName = StringUtils.EMPTY;
        if (dealModel != null) {
            breadcrumbName = dealModel.getFacetTitle();
        }
        return new Breadcrumb(dealCategoryUrl, breadcrumbName, linkClass);
    }

    @Required
    public void setCommerceCategoryService(final CommerceCategoryService commerceCategoryService) {
        this.commerceCategoryService = commerceCategoryService;
    }

    protected UrlResolver<CategoryModel> getCategoryModelUrlResolver() {
        return categoryModelUrlResolver;
    }

    @Required
    public void setTargetDealService(final TargetDealService targetDealService) {
        this.targetDealService = targetDealService;
    }

    @Required
    public void setCategoryModelUrlResolver(final UrlResolver<CategoryModel> categoryModelUrlResolver) {
        this.categoryModelUrlResolver = categoryModelUrlResolver;
    }

    protected UrlResolver<BrandModel> getBrandModelUrlResolver() {
        return brandModelUrlResolver;
    }

    @Required
    public void setBrandModelUrlResolver(final UrlResolver<BrandModel> brandModelUrlResolver) {
        this.brandModelUrlResolver = brandModelUrlResolver;
    }

    @Required
    public void setDealCategoryModelUrlResolver(
            final UrlResolver<TargetDealCategoryModel> dealCategoryModelUrlResolver) {
        this.dealCategoryModelUrlResolver = dealCategoryModelUrlResolver;
    }

    /**
     * populates the refinements and the breadcrumbs
     * 
     * @param category
     * @param brand
     * @param refinements
     * @param freeTextSearch
     * @param searchResults
     * @return List of Bread Crumbs to display on front end
     */
    public List<Breadcrumb> getBreadcrumbs(final CategoryModel category, final BrandModel brand,
            final List<BreadcrumbData<EndecaSearchStateData>> refinements, final String freeTextSearch,
            final EndecaSearch searchResults) {
        final List<Breadcrumb> breadcrumbs = new ArrayList<>();

        final Breadcrumb breadCrumb;

        if (null != searchResults) {
            final List<EndecaBreadCrumb> endecaCrumbs = searchResults.getBreadCrumb();
            if (CollectionUtils.isNotEmpty(endecaCrumbs)) {
                convertDealBreadCrumbData(endecaCrumbs);
            }
            if (category == null) {
                final List<Breadcrumb> categoryBreadcrumbs = new ArrayList<>();
                if (CollectionUtils.isNotEmpty(endecaCrumbs)) {
                    breadCrumb = new Breadcrumb(
                            ControllerConstants.SEARCH.concat("?text=") + getEncodedUrl(freeTextSearch),
                            StringEscapeUtils.escapeHtml(freeTextSearch), null);
                    breadcrumbs.add(breadCrumb);
                    for (final EndecaBreadCrumb endecaCrumb : endecaCrumbs) {
                        if ((StringUtils.equalsIgnoreCase(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY,
                                endecaCrumb.getDimensionType()))
                                || (StringUtils.equalsIgnoreCase(EndecaConstants.EndecaRecordSpecificFields.BULKYBOARD,
                                        endecaCrumb.getDimensionType()))) {
                            createCategoryBreadCrumbForSearch(categoryBreadcrumbs, endecaCrumb, freeTextSearch);
                        }
                        else if (!StringUtils.equalsIgnoreCase(EndecaConstants.EndecaRecordSpecificFields.BULKYBOARD,
                                endecaCrumb.getDimensionType())) {
                            populateRefinements(category, brand, endecaCrumb, refinements);
                        }
                    }
                }
                else {
                    breadCrumb = new Breadcrumb(
                            ControllerConstants.SEARCH.concat("?text=") + getEncodedUrl(freeTextSearch),
                            StringEscapeUtils.escapeHtml(freeTextSearch), LAST_LINK_CLASS);
                    breadcrumbs.add(breadCrumb);
                }
                breadcrumbs.addAll(categoryBreadcrumbs);
            }
            else if (brand == null) {
                if (category instanceof TargetDealCategoryModel) {
                    breadcrumbs.add(getDealCategoryBreadcrumb((TargetDealCategoryModel)category, LAST_LINK_CLASS));
                    if (CollectionUtils.isNotEmpty(endecaCrumbs)) {
                        for (final EndecaBreadCrumb endecaCrumb : endecaCrumbs) {
                            if (endecaCrumb == null) {
                                continue;
                            }
                            if (!StringUtils.equalsIgnoreCase(
                                    EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_QUALIFIER_CATEGORY,
                                    endecaCrumb.getDimensionType()) && !StringUtils.equalsIgnoreCase(
                                            EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_REWARD_CATEGORY,
                                            endecaCrumb.getDimensionType())) {
                                populateRefinements(category, brand, endecaCrumb, refinements);
                            }
                        }
                    }

                }
                else {
                    // Create category hierarchy path for breadcrumb & populate refinements
                    final List<Breadcrumb> categoryBreadcrumbs = new ArrayList<>();
                    if (CollectionUtils.isNotEmpty(endecaCrumbs)) {
                        for (final EndecaBreadCrumb endecaCrumb : endecaCrumbs) {
                            if ((StringUtils.equalsIgnoreCase(
                                    EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY,
                                    endecaCrumb.getDimensionType()))
                                    || (StringUtils.equalsIgnoreCase(
                                            EndecaConstants.EndecaRecordSpecificFields.BULKYBOARD,
                                            endecaCrumb.getDimensionType()))) {
                                createCategoryBreadCrumb(categoryBreadcrumbs, endecaCrumb, category.getCode());
                            }
                            else if (!StringUtils.equalsIgnoreCase(
                                    EndecaConstants.EndecaRecordSpecificFields.BULKYBOARD,
                                    endecaCrumb.getDimensionType())) {
                                populateRefinements(category, brand, endecaCrumb, refinements);
                            }
                        }
                    }
                    breadcrumbs.addAll(categoryBreadcrumbs);
                }
            }
            else {
                // Create brand hierarchy path for breadcrumb
                final boolean emptyBreadCrumbs = CollectionUtils.isEmpty(endecaCrumbs);
                final Breadcrumb brandBreadcrumb = getBrandBreadcrumb(brand, emptyBreadCrumbs ? LAST_LINK_CLASS : null);
                breadcrumbs.add(brandBreadcrumb);
                if (!emptyBreadCrumbs) {
                    for (final EndecaBreadCrumb endecaCrumb : endecaCrumbs) {
                        if (!StringUtils.equalsIgnoreCase("brand", endecaCrumb.getDimensionType())) {
                            createCategoryBreadCrumbForBrand(brand, brandBreadcrumb.getUrl(), breadcrumbs, endecaCrumb);
                        }
                    }
                }
            }
        }

        return breadcrumbs;

    }

    /**
     * convert the deal breadcrumb element name, split and get facet name.
     * 
     * @param endecaCrumbs
     */
    protected void convertDealBreadCrumbData(final List<EndecaBreadCrumb> endecaCrumbs) {
        if (CollectionUtils.isNotEmpty(endecaCrumbs)) {
            for (final EndecaBreadCrumb endecaBreadCrumb : endecaCrumbs) {
                if (endecaBreadCrumb != null) {
                    final String[] tokens = endecaBreadCrumb.getElement().split("@@");
                    if (tokens.length != 3) {
                        continue;
                    }
                    endecaBreadCrumb.setElement(tokens[1]);
                }
            }
        }
    }

    /**
     * @param category
     * @param brand
     * @param breadCrumb
     * @param refinements
     */
    protected void populateRefinements(final CategoryModel category,
            final BrandModel brand,
            final EndecaBreadCrumb breadCrumb,
            final List<BreadcrumbData<EndecaSearchStateData>> refinements) {
        final BreadcrumbData<EndecaSearchStateData> breadCrumbData = new BreadcrumbData<>();
        final String dimensionType = breadCrumb.getDimensionType();
        breadCrumbData.setFacetCode(dimensionType);
        breadCrumbData.setFacetName(dimensionType);
        final String element = breadCrumb.getElement();
        breadCrumbData.setFacetValueCode(element);

        if (StringUtils.equalsIgnoreCase(dimensionType,
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_DISPLAY_SIZE)) {
            final String[] tokens = element.split(Pattern.quote("**"));
            if (tokens.length == 4) {
                breadCrumbData.setFacetValueName(tokens[3]);
            }
        }

        if (StringUtils.isEmpty(breadCrumbData.getFacetValueName())) {
            breadCrumbData.setFacetValueName(element);
        }

        final EndecaSearchStateData searchStatedata = new EndecaSearchStateData();
        final String removeNavUrl = breadCrumb.getRemoveNavUrl();
        searchStatedata.setNavigationState(Collections.singletonList(removeNavUrl));
        String url = StringUtils.EMPTY;
        if (brand == null && category != null) {
            if (category instanceof TargetDealCategoryModel) {
                url += dealCategoryModelUrlResolver.resolve((TargetDealCategoryModel)category);
            }
            else {
                url += getCategoryModelUrlResolver().resolve(category);
            }
        }
        else if (brand != null) {
            url += getBrandModelUrlResolver().resolve(brand);
        }
        //on invalid navigation string use the base URL.
        if (StringUtils.isNotEmpty(removeNavUrl)) {
            String finalNavigationString = StringUtils.EMPTY;
            finalNavigationString = targetEndecaURLHelper.getValidEndecaToTargetNavigationState(removeNavUrl);
            if (StringUtils.isNotEmpty(finalNavigationString)) {
                searchStatedata.setUrl(url + finalNavigationString);
            }
            else {
                searchStatedata.setUrl(url);
            }
        }
        else {
            searchStatedata.setUrl(url);
        }
        breadCrumbData.setRemoveQuery(searchStatedata);
        refinements.add(breadCrumbData);
    }

    /**
     * @param categoryBreadcrumbs
     * @param crumbs
     * @param categoryCode
     */
    public void createCategoryBreadCrumb(final List<Breadcrumb> categoryBreadcrumbs,
            final EndecaBreadCrumb crumbs, final String categoryCode) {
        if (CollectionUtils.isNotEmpty(crumbs.getAncestor())) {
            for (final EndecaAncestor ancestor : crumbs.getAncestor()) {
                categoryBreadcrumbs.add(getCategoryBreadcrumb(ancestor.getCode(), ancestor.getElement(), null));
            }
        }

        if (!EndecaConstants.EndecaRecordSpecificFields.BULKYBOARD.equalsIgnoreCase(crumbs.getDimensionType())) {
            categoryBreadcrumbs.add(getCategoryBreadcrumb(categoryCode, crumbs.getElement(), LAST_LINK_CLASS));
        }
    }

    /**
     * @param brand
     * @param brandUrl
     * @param categoryBreadcrumbs
     * @param crumbs
     */
    public void createCategoryBreadCrumbForBrand(final BrandModel brand, final String brandUrl,
            final List<Breadcrumb> categoryBreadcrumbs,
            final EndecaBreadCrumb crumbs) {
        if (CollectionUtils.isNotEmpty(crumbs.getAncestor())) {
            for (final EndecaAncestor ancestor : crumbs.getAncestor()) {
                categoryBreadcrumbs
                        .add(getCategoryBreadcrumbForBrand(brand, brandUrl, ancestor.getCode(), ancestor.getElement(),
                                null));
            }
        }

        if (!EndecaConstants.EndecaRecordSpecificFields.BULKYBOARD.equalsIgnoreCase(crumbs.getDimensionType())) {
            categoryBreadcrumbs
                    .add(getCategoryBreadcrumbForBrand(brand, brandUrl, crumbs.getCode(), crumbs.getElement(),
                            LAST_LINK_CLASS));
        }
    }

    /**
     * @param brand
     * @param brandUrl
     * @param categoryCode
     * @param element
     * @param lastLinkClass
     * @return Breadcrumb
     */
    protected Breadcrumb getCategoryBreadcrumbForBrand(final BrandModel brand, final String brandUrl,
            final String categoryCode,
            final String element,
            final String lastLinkClass) {
        if (StringUtils.isNotBlank(categoryCode)) {
            final String categoryNavState = getNavState(categoryCode, brand.getName());
            return new Breadcrumb(brandUrl + categoryNavState, element, lastLinkClass);
        }
        return new Breadcrumb(brandUrl, element, lastLinkClass);
    }

    /**
     * @param categoryBreadcrumbs
     * @param crumbs
     * @param searchText
     */
    protected void createCategoryBreadCrumbForSearch(final List<Breadcrumb> categoryBreadcrumbs,
            final EndecaBreadCrumb crumbs, final String searchText) {
        if (CollectionUtils.isNotEmpty(crumbs.getAncestor())) {
            for (final EndecaAncestor ancestor : crumbs.getAncestor()) {
                categoryBreadcrumbs.add(getCategoryBreadcrumbForSearch(ancestor.getCode(), ancestor.getElement(),
                        searchText, null));
            }
        }

        if (!EndecaConstants.EndecaRecordSpecificFields.BULKYBOARD.equalsIgnoreCase(crumbs.getDimensionType())) {
            categoryBreadcrumbs.add(getCategoryBreadcrumbForSearch(crumbs.getCode(), crumbs.getElement(), searchText,
                    LAST_LINK_CLASS));
        }
    }
}
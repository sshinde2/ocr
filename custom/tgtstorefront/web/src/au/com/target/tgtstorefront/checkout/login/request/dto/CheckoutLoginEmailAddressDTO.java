/**
 * 
 */
package au.com.target.tgtstorefront.checkout.login.request.dto;

import au.com.target.tgtstorefront.forms.validation.Email;


/**
 * @author rmcalave
 *
 */
public class CheckoutLoginEmailAddressDTO {

    @Email
    private String emailAddress;
    private boolean blankRedirectPage;

    /**
     * @return the emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    public boolean getBlankRedirectPage() {
        return blankRedirectPage;
    }

    /**
     * @param emailAddress
     *            the emailAddress to set
     */
    public void setEmailAddress(final String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * @param blankRedirectPage
     *            sets blankRedirectPage
     */
    public void setBlankRedirectPage(final boolean blankRedirectPage) {
        this.blankRedirectPage = blankRedirectPage;
    }
}

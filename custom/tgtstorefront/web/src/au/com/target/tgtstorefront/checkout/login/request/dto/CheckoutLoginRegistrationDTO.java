package au.com.target.tgtstorefront.checkout.login.request.dto;

import au.com.target.tgtstorefront.forms.validation.FirstName;
import au.com.target.tgtstorefront.forms.validation.LastName;
import au.com.target.tgtstorefront.forms.validation.Password;


/**
 * Created by rmcalave on 10/05/2016.
 */
public class CheckoutLoginRegistrationDTO extends CheckoutLoginEmailAddressDTO {
    @FirstName
    private String firstName;

    @LastName
    private String lastName;

    private boolean optIntoMarketing;

    @Password(checkBlacklist = false)
    private String password;

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the optIntoMarketing
     */
    public boolean isOptIntoMarketing() {
        return optIntoMarketing;
    }

    /**
     * @param optIntoMarketing
     *            the optIntoMarketing to set
     */
    public void setOptIntoMarketing(final boolean optIntoMarketing) {
        this.optIntoMarketing = optIntoMarketing;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(final String password) {
        this.password = password;
    }
}

/**
 * 
 */
package au.com.target.tgtstorefront.checkout.login.request.dto;

import au.com.target.tgtstorefront.forms.validation.Password;


/**
 * @author bhuang3
 *
 */
public class CheckoutLoginResetPasswordDTO {

    @Password
    private String password;

    private String token;

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token
     *            the token to set
     */
    public void setToken(final String token) {
        this.token = token;
    }



}

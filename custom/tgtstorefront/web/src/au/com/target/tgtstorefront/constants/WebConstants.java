/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.constants;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Pattern;

import au.com.target.tgtstorefront.controllers.pages.checkout.AbstractCheckoutController.SelectOption;


/**
 * Constants used in the Web tier
 */
public final class WebConstants {
    public static final char FORWARD_SLASH = '/';
    public static final String MODEL_KEY_ADDITIONAL_BREADCRUMB = "additionalBreadcrumb";
    public static final String BREADCRUMBS_KEY = "breadcrumbs";
    public static final String DEPARTMENT_NAME = "departmentName";
    public static final String SUPERCATEGORIES_KEY = "supercategories";
    public static final String LEFT_NAV_ITEMS_KEY = "leftNavMenuItems";
    public static final String NAVIGATION_COMPONENT_MENU_ITEM = "navigationComponentMenuItem";
    public static final String RECOMMENDED_PRODUCTS_KEY = "recommendedProducts";

    public static final String CONTINUE_URL = "session_continue_url";
    public static final String ANONYMOUS_CHECKOUT = "anonymous_checkout";
    public static final String SECURE_GUID_SESSION_KEY = "acceleratorSecureGUID";

    public static final String STOREFRONT_ASSETMODE_DEVELOP_CONFIG_KEY = "tgtstorefront.assetmode.develop";
    public static final String STOREFRONT_ASSETMODE_DEVELOP_REQUEST_KEY = "assetModeDevelop";

    public static final String STOREFRONT_CACHEBUSTER_REWRITE_ENABLED_CONFIG_KEY = "tgtstorefront.cachebuster.rewrite";
    public static final String STOREFRONT_CACHEBUSTER_REWRITE_ENABLED_REQUEST_KEY = "cacheBusterRewriteEnabled";

    public static final String STOREFRONT_BUILDDATE_CONFIG_KEY = "build.builddate";
    public static final String STOREFRONT_CACHEBUSTER_TIMESTAMP_REQUEST_KEY = "cacheBusterTimestamp";


    public static final String ARTICLE_LIST_TITLE = "title";
    public static final String ARTICLE_LIST_COMPONENTS = "components";
    public static final String BANNERS = "banners";
    public static final String FEEDBACKS = "feedbacks";
    public static final String COORDINATE_LINK_COMPONENTS = "coordinateLinkComponents";
    public static final String LINK_COMPONENTS = "linkComponents";
    public static final String MOBILE_LINK = "mobileLink";
    public static final String RICHBANNER = "richbanner";

    public static final String ARTICLE_ITEM_YOUTUBE_LINK = "youTubeLink";

    public static final String NAVIGATION_COMPONENT_SHOW_TITLE = "showTitle";

    public static final String DELIVERY_MODE_HOME_DELIVERY_CODE = "home-delivery";

    public static final String STATE_CODE_NOT_APPLICABLE = "N/A";
    public static final String STATE_NAME_NOT_APPLICABLE = "Not Applicable";

    // Validation
    public static final Pattern NAME_VALIDATION_PATTERN = Pattern.compile("[A-Za-z\\-' ]*");

    public static final String BASE_SECURE_URL_KEY = "tgtstorefront.host.fqdn";
    public static final String TNS_HOSTED_PAYMENT_URL_KEY = "tns.hostedPaymentFormBaseUrl";
    public static final String PAYPAL_HOSTED_PAYMENT_URL_KEY = "paypal.hostedPaymentFormBaseUrl";

    public static final String CARD_TYPE_DINERS = "DINERS_CLUB";
    public static final String CARD_TYPE_OTHER = "OTHER";
    public static final String CARD_TYPE_JCB = "JCB";
    public static final String CANONICAL_QUERY_STRING = "?page=";
    public static final String QUERY_STRING_START_CHARACTER = "?";
    public static final String IS_SALES_ASSISTED = "salesAssisted";

    /** The Constant TMD_PROMOTION_TYPE. */
    public static final String TMD_PROMOTION_TYPE = "Team Member Discount";

    public static final String CMS_PAGE_MODEL = "cmsPage";
    public static final String SESSION_CART_KEY = "cart";

    public static final String WEB_FOLDER_UI = "/_ui";
    public static final String WEB_FOLDER_ASSETS = WEB_FOLDER_UI + "/_assets";

    public static final String SPRING_SECURITY_LAST_EXCEPTION = "SPRING_SECURITY_LAST_EXCEPTION";
    public static final String BRAND_SEARCH_NAVIGATION_STATE = "brandSearchNavigation";

    private WebConstants() {
        //empty
    }

    public interface Payment {
        List<SelectOption> MONTHS = Collections.unmodifiableList(new ArrayList<SelectOption>() {
            {
                add(new SelectOption("1", "01 - Jan"));
                add(new SelectOption("2", "02 - Feb"));
                add(new SelectOption("3", "03 - Mar"));
                add(new SelectOption("4", "04 - Apr"));
                add(new SelectOption("5", "05 - May"));
                add(new SelectOption("6", "06 - Jun"));
                add(new SelectOption("7", "07 - Jul"));
                add(new SelectOption("8", "08 - Aug"));
                add(new SelectOption("9", "09 - Sep"));
                add(new SelectOption("10", "10 - Oct"));
                add(new SelectOption("11", "11 - Nov"));
                add(new SelectOption("12", "12 - Dec"));
            }
        });

        /**
         * TODO must be cached instead of being generated only once, number of years (6) must come from configuration
         */
        List<SelectOption> START_YEARS = Collections.unmodifiableList(new ArrayList<SelectOption>() {
            {
                final Calendar calender = new GregorianCalendar();

                for (int i = calender.get(Calendar.YEAR); i > (calender.get(Calendar.YEAR) - 6); i--) {
                    add(new SelectOption(String.valueOf(i), String.valueOf(i)));
                }

            }
        });

        /**
         * TODO must be cached instead of being generated only once, number of years (11) must come from configuration
         */
        List<SelectOption> EXPIRY_YEARS = Collections.unmodifiableList(new ArrayList<SelectOption>() {
            {
                final Calendar calender = new GregorianCalendar();

                for (int i = calender.get(Calendar.YEAR); i < (calender.get(Calendar.YEAR) + 11); i++) {
                    add(new SelectOption(String.valueOf(i), String.valueOf(i)));
                }
            }
        });

        List<SelectOption> DAYS_OF_MONTH = Collections.unmodifiableList(new ArrayList<SelectOption>() {
            {
                for (int i = 1; i <= 31; i++) {

                    final String sDay = Integer.toString(i);
                    final String paddedDay;
                    if (sDay.length() == 1) {
                        paddedDay = "0" + sDay;
                    }
                    else {
                        paddedDay = sDay;
                    }

                    add(new SelectOption(sDay, paddedDay));
                }
            }
        });

        List<SelectOption> BIRTH_YEARS = Collections.unmodifiableList(new ArrayList<SelectOption>() {
            {
                final Calendar calender = new GregorianCalendar();

                // go back 100 years!
                for (int i = calender.get(Calendar.YEAR); i > (calender.get(Calendar.YEAR) - 100); i--) {
                    add(new SelectOption(String.valueOf(i), String.valueOf(i)));
                }

            }
        });

        /**
         * flybuys terms and conditions state that card holders are 16 years or older. This list starts from 15 years
         * ago.
         */
        List<SelectOption> FLYBUYS_BIRTH_YEARS = Collections.unmodifiableList(new ArrayList<SelectOption>() {
            {
                final Calendar calender = new GregorianCalendar();
                final int start = calender.get(Calendar.YEAR) - 15;

                // go back 100 years!
                for (int i = start; i > (start - 100); i--) {
                    add(new SelectOption(String.valueOf(i), String.valueOf(i)));
                }

            }
        });


        String VALID = "0~";
        String SESSION_INVALID = "2~";
        String FIELDERRORS = "3~";

        public static enum HostedFormStatus {
            /*
             * 0~OK Indicates the form is valid  returned when no form errors are found. 2~Form Session Id invalid or
             * closed for updates Indicates invalid form session returned when Form Session is missing, expired,
             * closed, or invalid. 3~Field Errors Indicates field errors returned if any form field fails 
             * validation.4~System Error Indicates system error returned if a Payment Gateway error or other 
             * failure occurs.
             */
            Valid, SessionInvalid, FieldErrors, SystemError
        }

        public static enum HostedFormFieldStatus {
            Valid, Empty, FailedValidation
        }
    }

    public static final boolean isValidCardType(final String cardType) {
        return cardType != null && !cardType.equals(CARD_TYPE_OTHER) && !cardType.equals(CARD_TYPE_JCB);
    }

    /**
     * Constants for donationRequestPage
     * 
     */
    public interface DonationRequest {
        public static enum DonationRequestScope {
            Local("Local"), Regional("Regional");

            private final String value;

            private DonationRequestScope(final String value) {
                this.value = value;
            }

            public String getValue() {
                return value;
            }
        }

        public static enum PreferredMethodofContact {
            Email("Email"), Phone("Phone"), Mail("Mail");

            private final String value;

            private PreferredMethodofContact(final String value) {
                this.value = value;
            }

            public String getValue() {
                return value;
            }
        }
    }

    public static enum ResponseMessageType {
        Success("success"), Info("info"), Error("error");

        private final String value;

        private ResponseMessageType(final String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public interface UserInformation {

        List<SelectOption> DUE_DAY_YEARS = Collections.unmodifiableList(new ArrayList<SelectOption>() {
            {
                final Calendar calender = new GregorianCalendar();
                final int start = calender.get(Calendar.YEAR);

                for (int i = start; i < start + 3; i++) {
                    add(new SelectOption(String.valueOf(i), String.valueOf(i)));
                }

            }
        });

        List<SelectOption> GENDERS = Collections.unmodifiableList(new ArrayList<SelectOption>() {
            {
                add(new SelectOption("M", "Male"));
                add(new SelectOption("F", "Female"));
            }
        });

        List<SelectOption> BABY_GENDERS = Collections.unmodifiableList(new ArrayList<SelectOption>() {
            {
                add(new SelectOption("M", "Boy"));
                add(new SelectOption("F", "Girl"));
            }
        });

        List<SelectOption> EXPECTED_BABY_GENDERS = Collections.unmodifiableList(new ArrayList<SelectOption>() {
            {
                add(new SelectOption("M", "Boy"));
                add(new SelectOption("F", "Girl"));
                add(new SelectOption("S", "It is a surprise!"));
                add(new SelectOption("T", "Twins"));
            }
        });
    }

}

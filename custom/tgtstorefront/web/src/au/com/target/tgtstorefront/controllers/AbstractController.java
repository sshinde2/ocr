/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers;

import de.hybris.platform.commerceservices.enums.SalesApplication;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseStatus;

import au.com.target.tgtfacades.order.TargetCartFacade;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtstorefront.security.cookie.EnhancedCookieGenerator;


/**
 * Base controller for all controllers. Provides common functionality for all controllers.
 */
public abstract class AbstractController
{
    @Resource(name = "targetUserFacade")
    private TargetUserFacade targetUserFacade;

    @Resource(name = "anonymousCartTokenGenerator")
    private EnhancedCookieGenerator anonymousCartCookieGenerator;

    @Resource(name = "targetCartFacade")
    private TargetCartFacade targetCartFacade;

    @Resource(name = "salesApplicationFacade")
    private SalesApplicationFacade salesApplicationFacade;

    @Resource(name = "excludedSalesAppsToRestoreCart")
    private List<SalesApplication> excludedSalesAppsToRestoreCart;

    @ModelAttribute("request")
    public HttpServletRequest addRequestToModel(final HttpServletRequest request)
    {
        return request;
    }


    /**
     * @param response
     */
    public void setAnonymousCartCookie(final HttpServletResponse response) {
        if (getTargetUserFacade().isCurrentUserAnonymous()
                && !(getExcludedSalesAppsToRestoreCart()
                        .contains(salesApplicationFacade.getCurrentSalesApplication()))) {
            final String anonymousCartTokenValue = getTargetCartFacade().getGuidForCurrentSessionCart();
            if (StringUtils.isNotEmpty(anonymousCartTokenValue)) {
                getAnonymousCartCookieGenerator().addCookie(response, anonymousCartTokenValue);
            }
        }
    }


    /**
     * @return the excludedSalesAppsToRestoreCart
     */
    protected List<SalesApplication> getExcludedSalesAppsToRestoreCart() {
        return excludedSalesAppsToRestoreCart;
    }


    protected TargetUserFacade getTargetUserFacade() {
        return targetUserFacade;
    }

    protected EnhancedCookieGenerator getAnonymousCartCookieGenerator()
    {
        return anonymousCartCookieGenerator;
    }

    public TargetCartFacade getTargetCartFacade() {
        return targetCartFacade;
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public static class HttpNotFoundException extends RuntimeException
    {
        /**
         * Default constructor
         */
        public HttpNotFoundException()
        {
            super();
        }

        /**
         * @param message
         *            the detail message (which is saved for later retrieval by the {@link #getMessage()} method).
         * @param cause
         *            the cause (which is saved for later retrieval by the {@link #getCause()} method). (A <tt>null</tt>
         *            value is permitted, and indicates that the cause is nonexistent or unknown.)
         */
        public HttpNotFoundException(final String message, final Throwable cause)
        {
            super(message, cause);
        }

        /**
         * @param message
         *            the detail message. The detail message is saved for later retrieval by the {@link #getMessage()}
         *            method.
         */
        public HttpNotFoundException(final String message)
        {
            super(message);
        }

        /**
         * @param cause
         *            the cause (which is saved for later retrieval by the {@link #getCause()} method). (A <tt>null</tt>
         *            value is permitted, and indicates that the cause is nonexistent or unknown.)
         */
        public HttpNotFoundException(final Throwable cause)
        {
            super(cause);
        }
    }

    /**
     * @return the salesApplicationFacade
     */
    public SalesApplicationFacade getSalesApplicationFacade() {
        return salesApplicationFacade;
    }
}

/**
 * 
 */
package au.com.target.tgtstorefront.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @author rmcalave
 * 
 */
@Controller
@RequestMapping("/status")
public class StatusController extends AbstractController {

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public String status(final HttpServletRequest request) {
        request.getSession().invalidate();
        return "OK";
    }
}

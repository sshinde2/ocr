/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.preview.CMSPreviewTicketModel;
import de.hybris.platform.cms2.servicelayer.data.CMSDataFactory;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.cms2.servicelayer.services.CMSPreviewService;
import de.hybris.platform.cms2.servicelayer.services.CMSRestrictionService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.product.BrandService;
import au.com.target.tgtstorefront.controllers.AbstractController;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.AbstractPageController;
import au.com.target.tgtwebcore.model.cms2.components.ArticleItemComponentModel;
import au.com.target.tgtwebcore.servicelayer.data.TargetCMSRestrictionData;
import au.com.target.tgtwebcore.servicelayer.data.impl.TargetCMSRestrictionDataImpl;


/**
 * Abstract Controller for CMS Components
 */
public abstract class AbstractCMSComponentController<T extends AbstractCMSComponentModel> extends AbstractController
{

    protected static final Logger LOG = Logger.getLogger(AbstractCMSComponentController.class);

    protected static final String COMPONENT_UID = "componentUid";
    private static final String CATALOG = "catalogId";
    private static final String CURRENT_CATEGORY_CODE = "currentCategoryCode";
    private static final String CURRENT_PRODUCT_CODE = "currentProductCode";
    private static final String CMS_TICKET_ID = "cmsTicketId";

    @Resource(name = "cmsComponentService")
    private CMSComponentService cmsComponentService;

    @Resource(name = "cmsPreviewService")
    private CMSPreviewService cmsPreviewService;

    @Resource(name = "cmsDataFactory")
    private CMSDataFactory cmsDataFactory;

    @Resource(name = "cmsRestrictionService")
    private CMSRestrictionService cmsRestrictionService;

    @Resource(name = "brandService")
    private BrandService brandService;

    /**
     * 
     * @param cmsComponentService
     */
    public void setCmsComponentService(final CMSComponentService cmsComponentService)
    {
        this.cmsComponentService = cmsComponentService;
    }

    @RequestMapping
    public String handleGet(final HttpServletRequest request, final Model model)
            throws Exception
    {
        String componentUid = (String)request.getAttribute(COMPONENT_UID);
        if (StringUtils.isEmpty(componentUid))
        {
            componentUid = request.getParameter(COMPONENT_UID);
        }

        if (StringUtils.isEmpty(componentUid))
        {
            LOG.error("No component specified in [" + COMPONENT_UID + "]");
            throw new AbstractPageController.HttpNotFoundException();
        }

        try
        {
            final T component = (T)cmsComponentService.getSimpleCMSComponent(componentUid);
            if (component == null)
            {
                LOG.error("Component with UID [" + componentUid + "] is null");
                throw new AbstractPageController.HttpNotFoundException();
            }
            else
            {
                // Add the component to the model
                model.addAttribute("component", component);

                // Allow subclasses to handle the component
                return handleComponent(request, model, component);
            }
        }
        catch (final CMSItemNotFoundException e)
        {
            LOG.error("Could not find component with UID [" + componentUid + "]");
            throw new AbstractPageController.HttpNotFoundException(e);
        }
    }

    protected String handleComponent(final HttpServletRequest request,
            final Model model, final T component) throws Exception
    {
        fillModel(request, model, component);
        return getView(component);
    }

    protected abstract void fillModel(final HttpServletRequest request, final Model model, final T component);

    protected String getView(final T component)
    {
        // build a jsp response based on the component type
        return ControllerConstants.Views.Cms.COMPONENT_PREFIX + StringUtils.lowerCase(getTypeCode(component));
    }

    protected String getTypeCode(final T component)
    {
        return component.getItemtype();
    }

    protected CMSComponentService getCmsComponentService()
    {
        return cmsComponentService;
    }

    /**
     * Check preview mode is enable or not
     * 
     * @param request
     * @return true if open in priview mode
     */
    protected boolean isPreviewEnabled(final HttpServletRequest request) {
        boolean previewEnabled = false;

        final String ticketId = request.getParameter(CMS_TICKET_ID);
        if (StringUtils.isNotBlank(ticketId)) {
            final CMSPreviewTicketModel previewTicket = cmsPreviewService.getPreviewTicket(ticketId);
            if (previewTicket == null) {
                LOG.warn("No preview ticket found with id '" + ticketId + "'.");
            }
            else {
                previewEnabled = Boolean.FALSE.equals(previewTicket.getPreviewData().getEditMode());
            }
        }
        else if (LOG.isDebugEnabled()) {
            LOG.debug("Could not determine preview edit status. Reason: No preview ticket ID supplied.");
        }
        return previewEnabled;
    }

    /**
     * populate RestrictionData
     * 
     * @param request
     *            HttpServletRequest
     * @return RestrictionData
     */
    protected RestrictionData populate(final HttpServletRequest request) {
        final Object catalog = request.getAttribute(CATALOG);
        final Object category = request.getAttribute(CURRENT_CATEGORY_CODE);
        final Object product = request.getAttribute(CURRENT_PRODUCT_CODE);
        final String catalogId = (catalog instanceof String) ? catalog.toString() : "";
        final String categoryCode = (category instanceof String) ? category.toString() : "";
        final String productCode = (product instanceof String) ? product.toString() : "";
        final RestrictionData restrictionData = cmsDataFactory.createRestrictionData(categoryCode, productCode,
                catalogId);
        final Object brand = request.getAttribute("currentBrandCode");
        if (brand instanceof String) {
            final String brandCode = brand.toString();

            if (StringUtils.isNotEmpty(brandCode)) {
                final TargetCMSRestrictionData targetRestrictionData = new TargetCMSRestrictionDataImpl(restrictionData);

                final BrandModel brandModel = brandService.getBrandForCode(brandCode);
                if (brandModel != null) {
                    targetRestrictionData.setBrand(brandModel);
                }
                else {
                    LOG.info("Could not find brand with code <" + brandCode + ">");
                }

                return targetRestrictionData;
            }
        }
        return restrictionData;
    }

    protected boolean isComponentAllowed(final AbstractCMSComponentModel component, final HttpServletRequest request) {
        boolean allowed = true;
        if (Boolean.FALSE.equals(component.getVisible())) {
            allowed = false;
        }
        else if (CollectionUtils.isNotEmpty(component.getRestrictions()) && !isPreviewEnabled(request))
        {
            final RestrictionData restrictionData = populate(request);
            allowed = cmsRestrictionService.evaluateCMSComponent(component, restrictionData);
        }
        return allowed;
    }


    /**
     * 
     * @param component
     *            AbstractCMSComponentModel
     * @return true is it is valid component otherwise false
     */
    protected boolean isValidComponent(final AbstractCMSComponentModel component) {
        if (component instanceof ArticleItemComponentModel) {
            final ArticleItemComponentModel articleItemComponentModel = (ArticleItemComponentModel)component;

            final boolean isArticleItemHavingUrlLink = (StringUtils.isNotBlank(articleItemComponentModel.getUrlLink())
                    || null != articleItemComponentModel.getPage());
            if (StringUtils.isNotBlank(articleItemComponentModel.getHeadline())
                    && StringUtils.isNotBlank(articleItemComponentModel.getContent())
                    && (articleItemComponentModel.getMedia() != null)
                    && StringUtils.isNotBlank(articleItemComponentModel.getMedia().getURL())
                    && isArticleItemHavingUrlLink) {
                return true;
            }

        }
        return false;

    }
}

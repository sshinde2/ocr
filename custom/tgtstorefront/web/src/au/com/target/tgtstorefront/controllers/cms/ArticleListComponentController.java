/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.CMSRestrictionService;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtwebcore.model.cms2.components.ArticleItemComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.ArticleListComponentModel;


@Controller("ArticleListComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.ARTICLE_LIST_COMPONENT)
public class ArticleListComponentController extends AbstractCMSComponentController<ArticleListComponentModel> {


    @Autowired
    private CMSRestrictionService cmsRestrictionService;

    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
            final ArticleListComponentModel component) {

        if (component.getTitle() != null) {
            model.addAttribute(WebConstants.ARTICLE_LIST_TITLE, component.getTitle());
        }
        if (component.getArticle() != null) {
            model.addAttribute(WebConstants.ARTICLE_LIST_COMPONENTS, getSimpleCMSComponents(component, request));
        }
    }

    /**
     * 
     * @param cmsComponent
     *            ArticleListing component
     * @param httpRequest
     *            HttpServletRequest
     * @return list of cms component
     */
    protected List getSimpleCMSComponents(final ArticleListComponentModel cmsComponent,
            final HttpServletRequest httpRequest) {
        final boolean previewEnabled = isPreviewEnabled(httpRequest);

        final List ret = new ArrayList();
        if (CollectionUtils.isEmpty(cmsComponent.getArticle())) {
            return ret;
        }
        for (final ArticleItemComponentModel component : cmsComponent.getArticle())
        {
            boolean allowed = true;
            if (CollectionUtils.isNotEmpty(component.getRestrictions()) && !previewEnabled)
            {
                final RestrictionData data = populate(httpRequest);
                allowed = cmsRestrictionService.evaluateCMSComponent(component, data);
            }
            else if (Boolean.FALSE.equals(component.getVisible())) {
                allowed = false;
            }

            if (allowed) {
                if (isValidComponent(component)) {
                    ret.add(component);
                }
            }
        }

        return ret;
    }




    /**
     * @param cmsRestrictionService
     *            the cmsRestrictionService to set
     */
    public void setCmsRestrictionService(final CMSRestrictionService cmsRestrictionService) {
        this.cmsRestrictionService = cmsRestrictionService;
    }




}

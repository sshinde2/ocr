/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.tags.Functions;


/**
 *
 */
@Controller("CMSLinkComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.CMS_LINK_COMPONENT)
public class CMSLinkComponentController<T extends CMSLinkComponentModel> extends
        AbstractCMSComponentController<CMSLinkComponentModel>
{
    @Resource(name = "productUrlConverter")
    private Converter<ProductModel, ProductData> productUrlConverter;

    @Resource(name = "categoryUrlConverter")
    private Converter<CategoryModel, CategoryData> categoryUrlConverter;

    @Override
    protected void fillModel(final HttpServletRequest request, final Model model, final CMSLinkComponentModel component)
    {
        model.addAttribute("url", getUrl(component));
    }

    protected String getUrl(final CMSLinkComponentModel component)
    {
        // Call the function getUrlForCMSLinkComponent so that this code is only in one place
        return Functions.getUrlForCMSLinkComponent(component, null, productUrlConverter, categoryUrlConverter);
    }
}

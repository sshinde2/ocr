/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtwebcore.model.cms2.components.CoordinateLinkComponentModel;


/**
 * @author asingh78
 * 
 */
@Controller("CoordinateLinkComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.COORDINATE_LINK_COMPONENT)
public class CoordinateLinkComponentController extends CMSLinkComponentController<CoordinateLinkComponentModel> {

    @Override
    protected void fillModel(final HttpServletRequest request, final Model model, final CMSLinkComponentModel component)
    {
        super.fillModel(request, model, component);
    }
}

package au.com.target.tgtstorefront.controllers.cms;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtwebcore.model.cms2.components.FeaturedProductsComponentModel;


/**
 * Controller for CMS FeaturedProductsComponent.
 */
@Controller("FeaturedProductsComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.FEATURED_PRODUCTS_COMPONENT)
public class FeaturedProductsComponentController extends
        ProductCarouselComponentController<FeaturedProductsComponentModel>
{
    private static final int MAX_NUMBER_PRODUCTS = 4;

    @Override
    protected void fillModelInternal(final HttpServletRequest request, final Model model,
            final FeaturedProductsComponentModel component)
    {
        super.fillModelInternal(request, model, component);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtstorefront.controllers.cms.ProductCarouselComponentController#getMaxProducts()
     */
    @Override
    protected int getMaxProducts() {
        return MAX_NUMBER_PRODUCTS;
    }
}

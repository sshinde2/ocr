/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.endeca.infront.assemble.impl.EndecaPageAssemble;
import au.com.target.endeca.infront.cache.EndecaDimensionCacheService;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.converter.AggregatedEndecaRecordToTargetProductListerConverter;
import au.com.target.endeca.infront.data.EndecaProduct;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.querybuilder.EndecaProductQueryBuilder;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.search.TargetProductHideStrategy;
import au.com.target.tgtfacades.url.impl.TargetProductDataUrlResolver;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.EndecaQueryHelper;
import au.com.target.tgtwebcore.model.cms2.components.FeaturedProductsComponentModel;

import com.endeca.navigation.AggrERec;
import com.endeca.navigation.AggrERecList;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.Navigation;


/**
 * Controller for CMS ProductReferencesComponent.
 */
@Controller("ProductCarouselComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.PRODUCT_CAROUSEL_COMPONENT)
public class ProductCarouselComponentController<T extends ProductCarouselComponentModel> extends
        AbstractCMSComponentController<ProductCarouselComponentModel> {
    protected static final List<ProductOption> PRODUCT_OPTIONS = Arrays
            .asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.STOCK);


    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Resource(name = "endecaPageAssemble")
    private EndecaPageAssemble endecaPageAssemble;

    @Resource(name = "conditionalOutOfStockHideStrategy")
    private TargetProductHideStrategy conditionalOutOfStockHideStrategy;

    @Resource(name = "aggrERecTotargetProductListerConverter")
    private AggregatedEndecaRecordToTargetProductListerConverter<AggrERec, TargetProductListerData> aggrERecToProductConverter;

    @Resource(name = "targetProductListerConverter")
    private Converter<EndecaProduct, TargetProductListerData> productConverter;

    @Resource(name = "endecaDimensionCacheService")
    private EndecaDimensionCacheService endecaDimensionCacheService;

    @Resource(name = "endecaProductQueryBuilder")
    private EndecaProductQueryBuilder endecaProductQueryBuilder;

    @Resource(name = "targetProductDataUrlResolver")
    private TargetProductDataUrlResolver targetProductDataUrlResolver;

    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
            final ProductCarouselComponentModel component) {
        fillModelInternal(request, model, (T)component);
    }


    /**
     * Collect the products that are directly linked to the component.<br/>
     * NOTE: since not based on SOLR this will involve DB hits for stock check, <br/>
     * so inefficient for a large number of linked products.
     * 
     * @param component
     *            carousel component
     * @param isFeaturedComponent
     *            whether component is a Featured Product Component
     * @return list
     */
    protected EndecaSearchStateData collectLinkedProducts(final ProductCarouselComponentModel component,
            final boolean isFeaturedComponent) {
        final EndecaSearchStateData products = getLinkedProducts(component.getProducts(), isFeaturedComponent);

        return products;
    }

    /**
     * Collect the products based on supplied categories linked to the component.
     * 
     * @param component
     *            carousel component
     * @param maxProducts
     *            maximun product for component
     * @param searchStateData
     *            DTO to hold search parameters
     * @return list
     */
    protected EndecaSearchStateData collectCategoryAndBrandProducts(final ProductCarouselComponentModel component,
            final int maxProducts, final EndecaSearchStateData searchStateData) {

        EndecaSearchStateData endecaSearchStateData = null;

        if (searchStateData != null) {
            endecaSearchStateData = searchStateData;
        }
        else {
            endecaSearchStateData = new EndecaSearchStateData();
        }

        if (null != component.getSortBy()) {
            final String sortBy = component.getSortBy().getCode();
            if (sortBy != null) {
                endecaSearchStateData.setSortStateMap(EndecaQueryHelper.getEndecaSortParams(sortBy));
            }
        }

        final int remainingProducts = maxProducts;
        final List<String> categoryCodes = new ArrayList<>();

        try {
            for (final CategoryModel categoryModel : component.getCategories()) {
                final String categoryDimId = endecaDimensionCacheService.getDimension(
                        EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY, categoryModel.getCode());
                if (StringUtils.isNotEmpty(categoryDimId)) {
                    categoryCodes.add(categoryDimId);
                }

                if (remainingProducts <= 0) {
                    break;
                }
            }

            final BrandModel brand = component.getBrand();
            if (brand != null) {
                final String brandDimId = endecaDimensionCacheService.getDimension(
                        EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND, brand.getName());
                if (StringUtils.isNotEmpty(brandDimId)) {
                    endecaSearchStateData.setBrandCode(brandDimId);
                }
            }
        }
        catch (final ENEQueryException e) {
            LOG.error("Exception when querying endeca for the specified categories and/or brand", e);
        }
        catch (final TargetEndecaException e) {
            LOG.error("Exception when calling dimension service", e);
        }

        if (CollectionUtils.isNotEmpty(categoryCodes)) {
            endecaSearchStateData.setCategoryCodes(categoryCodes);
        }

        return endecaSearchStateData;
    }

    /**
     * Add the list of linked ProductModels to the list with stock check
     * 
     * @param productModels
     *            list of selected colour variants
     * @param isFeaturedComponent
     *            whether the selected variants are for a Featured Product Component
     * @return list of linked products
     */
    protected EndecaSearchStateData getLinkedProducts(final List<ProductModel> productModels,
            final boolean isFeaturedComponent) {
        final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
        final List<String> produtCodes = new ArrayList<>();

        if (CollectionUtils.isEmpty(productModels)) {
            return searchStateData;
        }

        for (final ProductModel productModel : productModels) {

            if (!isFeaturedComponent) {
                if (productModel instanceof TargetColourVariantProductModel) {
                    final TargetColourVariantProductModel colourVariantModel = (TargetColourVariantProductModel)productModel;
                    produtCodes.add(colourVariantModel.getCode());
                }
            }
            else {
                if (productModel instanceof TargetColourVariantProductModel) {
                    final TargetColourVariantProductModel colourVariantModel = (TargetColourVariantProductModel)productModel;
                    produtCodes.add(colourVariantModel.getBaseProduct().getCode());
                }
                else if (productModel instanceof TargetSizeVariantProductModel) {
                    final TargetSizeVariantProductModel sizeVariantModel = (TargetSizeVariantProductModel)productModel;
                    final TargetColourVariantProductModel colourVariantModel = (TargetColourVariantProductModel)sizeVariantModel
                            .getBaseProduct();
                    produtCodes.add(colourVariantModel.getBaseProduct().getCode());
                }
                else {
                    produtCodes.add(productModel.getCode());
                }
            }
        }

        searchStateData.setMatchMode(EndecaConstants.MATCH_MODE_ANY);
        //if its a featured component the search filed is productCode
        if (isFeaturedComponent) {
            searchStateData.setSearchField(EndecaConstants.SEARCH_FIELD_PRODUCT_CODE);
        }
        else {
            searchStateData.setSearchField(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING);
        }
        searchStateData.setSearchTerm(produtCodes);

        return searchStateData;
    }


    /**
     * @param request
     *            request
     * @param model
     *            data model
     * @param component
     *            marketing component
     */
    protected void fillModelInternal(final HttpServletRequest request, final Model model, final T component) {
        final List<ProductData> products = new ArrayList<>();
        boolean isFeaturedComponent = false;

        if (FeaturedProductsComponentModel.class.isInstance(component)) {
            isFeaturedComponent = true;
        }

        EndecaSearchStateData searchStateData = collectLinkedProducts(component, isFeaturedComponent);

        if (getMaxProducts() > -1) {
            searchStateData = collectCategoryAndBrandProducts(component, getMaxProducts() - products.size(),
                    searchStateData);
        }
        else {
            searchStateData = collectCategoryAndBrandProducts(component, getCarouselSearchPageSize(), searchStateData);
        }

        final List<TargetProductListerData> filteredProductsList = new ArrayList<>();
        addProductDataIfItCanBeShown(
                getTargetProductsList(searchStateData, component.getProducts(), isFeaturedComponent,
                        false),
                filteredProductsList);
        model.addAttribute("title", component.getTitle());
        model.addAttribute("productData", filteredProductsList);

    }

    /**
     * Get product data from Endeca based on the supplied queryString.
     * 
     * category code to backfill
     * 
     * @param searchStateData
     *            DTO to hold search parameters
     * @param productModels
     *            selected colour variants
     * @param isFeaturedComponent
     *            whether the selected variants are for a Featured Product Component
     * @return list<TargetProductListerData>
     */
    protected List<TargetProductListerData> getTargetProductsList(
            final EndecaSearchStateData searchStateData, final List<ProductModel> productModels,
            final boolean isFeaturedComponent, final boolean alwaysHideOutOfStock) {

        final List<TargetProductListerData> productsData = new ArrayList<>();
        final List<TargetProductListerData> selectedProducts = new ArrayList<>();
        final List<TargetProductListerData> backFilledProducts = new ArrayList<>();

        if (isFeaturedComponent) {
            searchStateData.setNpSearchState(EndecaConstants.ENDECA_NP_2);
        }
        searchStateData.setFieldListConfig(EndecaConstants.FieldList.COMPONENT_FIELDLIST);
        final ENEQueryResults queryResults = endecaProductQueryBuilder.getQueryResults(searchStateData, productModels,
                getMaxProducts());

        if (queryResults != null && queryResults.getNavigation() != null) {
            final Navigation navigation = queryResults.getNavigation();
            final AggrERecList aggregateERecs = navigation.getAggrERecs();
            if (aggregateERecs != null) {
                for (final Object eRecObject : aggregateERecs) {
                    final AggrERec aggrERec = (AggrERec)eRecObject;
                    final TargetProductListerData targetProduct = aggrERecToProductConverter.convert(aggrERec);

                    if (isFeaturedComponent) {
                        if (CollectionUtils.isNotEmpty(productModels)) {
                            if (checkVariantsOutOfStock(targetProduct, productModels)) {
                                continue;
                            }
                        }

                        if (productsData.size() < getMaxProducts()
                                + (CollectionUtils.isNotEmpty(productModels) ? productModels.size() : 0)) {
                            sortProductVariants(targetProduct, productModels, isFeaturedComponent);
                            productsData.add(targetProduct);
                        }
                    }
                    else {
                        boolean skipAdd = false;
                        if (CollectionUtils.isNotEmpty(productModels)) {
                            for (final ProductModel variant : productModels) {
                                if (variantExists(targetProduct, variant)
                                        && !checkVariantOutOfStock(targetProduct, variant)) {
                                    final TargetProductListerData clonedProduct = aggrERecToProductConverter
                                            .convert(aggrERec, variant.getCode());
                                    selectedProducts.add(clonedProduct);
                                    skipAdd = true;
                                }
                            }
                        }

                        // Add to back filled list if not a selected variant.
                        if (!skipAdd) {
                            backFilledProducts.add(targetProduct);
                        }
                    }
                }
            }
        }

        if (!isFeaturedComponent) {
            if (CollectionUtils.isNotEmpty(selectedProducts) && CollectionUtils.isNotEmpty(productModels)) {
                final List<TargetProductListerData> orderedProducts;
                if (!alwaysHideOutOfStock) {
                    final List<TargetProductListerData> filteredProducts = new ArrayList<>();
                    addProductDataIfItCanBeShown(selectedProducts, filteredProducts);
                    orderedProducts = orderSelectedProducts(filteredProducts,
                            productModels);
                }
                else {
                    orderedProducts = orderSelectedProducts(selectedProducts, productModels);
                }

                if (CollectionUtils.isNotEmpty(orderedProducts)) {
                    for (final TargetProductListerData orderedProduct : orderedProducts) {
                        if (productsData.size() < getMaxProducts()) {
                            productsData.add(orderedProduct);
                        }
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(backFilledProducts)) {
                for (final TargetProductListerData backFilledProduct : backFilledProducts) {
                    if (getMaxProducts() - productsData.size() > 0) {
                        productsData.add(backFilledProduct);
                    }
                }
            }
        }

        return productsData;
    }

    /**
     * 
     * Add ProductData to list but only if not OUTOFSTOCK
     * 
     * @param unfilteredProductDataList
     *            all products
     * @param productDataList
     *            instock products
     */
    protected void addProductDataIfItCanBeShown(final List<TargetProductListerData> unfilteredProductDataList,
            final List<TargetProductListerData> productDataList) {
        for (final TargetProductListerData targetProductListerData : unfilteredProductDataList) {
            if (conditionalOutOfStockHideStrategy.hideThisProduct(targetProductListerData)) {
                continue;
            }
            productDataList.add(targetProductListerData);
        }
    }

    /**
     * @return the configurationService
     */
    protected ConfigurationService getConfigurationService() {
        return configurationService;
    }

    /**
     * Sort variants to bring the selected colour variant to top.
     * 
     * @param targetProduct
     *            product information for presentation layer
     * @param productModels
     *            selected colour variants
     * @param isFeaturedComponent
     *            whether the selected variants are for a Featured Product Component
     */
    protected void sortProductVariants(final TargetProductListerData targetProduct,
            final List<ProductModel> productModels, final boolean isFeaturedComponent) {
        final List<TargetVariantProductListerData> variants = targetProduct.getTargetVariantProductListerData();
        final TargetVariantProductListerData variant = (TargetVariantProductListerData)CollectionUtils.find(variants,
                new Predicate() {

                    @Override
                    public boolean evaluate(final Object arg0) {
                        if (arg0 instanceof TargetVariantProductListerData) {
                            final TargetVariantProductListerData listerVariant = (TargetVariantProductListerData)arg0;
                            for (final ProductModel model : productModels) {
                                if (listerVariant.getColourVariantCode().equals(model.getCode())) {
                                    return true;
                                }
                            }
                        }
                        return false;
                    }
                });

        if (variant != null && !conditionalOutOfStockHideStrategy.hideThisProduct(variant)) {
            variants.remove(variant);
            variants.add(0, variant);
            targetProduct.setCode(variant.getColourVariantCode());
            targetProduct.setUrl(targetProductDataUrlResolver.resolve(targetProduct.getName(),
                    variant.getColourVariantCode()));
        }
    }



    /**
     * Check whether the product has at least one variant out of stock.
     * 
     * @param targetProductData
     *            product information for presentation layer
     * @param productModels
     *            selected colour variants
     * @return boolean
     */
    private boolean checkVariantsOutOfStock(final TargetProductListerData targetProductData,
            final List<ProductModel> productModels) {
        final List<TargetVariantProductListerData> variants = targetProductData.getTargetVariantProductListerData();
        for (final TargetVariantProductListerData variant : variants) {
            for (final ProductModel model : productModels) {
                if (variant.getColourVariantCode().equals(model.getCode())) {
                    if (conditionalOutOfStockHideStrategy.hideThisProduct(variant)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Check whether the given variant is out of stock in a rolled up product.
     * 
     * @param targetProductData
     *            product information for presentation layer
     * @param productModel
     *            selected colour variants
     * @return boolean
     */
    private boolean checkVariantOutOfStock(final TargetProductListerData targetProductData,
            final ProductModel productModel) {
        final List<TargetVariantProductListerData> variants = targetProductData.getTargetVariantProductListerData();
        for (final TargetVariantProductListerData variant : variants) {
            if (variant.getColourVariantCode().equals(productModel.getCode())) {
                if (conditionalOutOfStockHideStrategy.hideThisProduct(variant)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Check whether variant exists in TargetProductListerData
     * 
     * @param targetProductData
     *            product information for presentation layer
     * @param productModel
     *            selected colour variants
     * @return boolean
     */
    private boolean variantExists(final TargetProductListerData targetProductData,
            final ProductModel productModel) {
        final List<TargetVariantProductListerData> variants = targetProductData.getTargetVariantProductListerData();
        for (final TargetVariantProductListerData variant : variants) {
            if (variant.getColourVariantCode().equals(productModel.getCode())) {
                return true;
            }
        }
        return false;
    }


    protected int getCarouselSearchPageSize() {
        return getConfigurationService().getConfiguration().getInt("storefront.carousel.search.pagesize", 36);
    }

    protected int getMaxProducts() {
        return getConfigurationService().getConfiguration().getInt("storefront.carousel.search.pagesize", 36);
    }

    /**
     * Order given list of products to match the variants selected.
     * 
     * @param productsList
     *            unordered list based on selection
     * @param productModels
     *            selected colour variants
     * @return java.util.List
     */
    private List<TargetProductListerData> orderSelectedProducts(final List<TargetProductListerData> productsList,
            final List<ProductModel> productModels) {
        final List<TargetProductListerData> orderedProducts = new ArrayList<>();
        for (final ProductModel product : productModels) {
            for (final TargetProductListerData targetProduct : productsList) {
                if (product.getCode().equals(targetProduct.getCode())) {
                    orderedProducts.add(targetProduct);
                }
            }
        }
        return orderedProducts;
    }


}

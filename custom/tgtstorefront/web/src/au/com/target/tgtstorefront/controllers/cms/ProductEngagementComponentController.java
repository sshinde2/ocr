package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.cms2lib.model.components.BannerComponentModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtwebcore.enums.RegionLayout;
import au.com.target.tgtwebcore.model.cms2.components.ProductEngagementComponentModel;


@Controller("ProductEngagementComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.PRODUCT_ENGAGEMENT_COMPONENT)
public class ProductEngagementComponentController extends
        ProductCarouselComponentController<ProductEngagementComponentModel> {

    private static final int MAX_NUMBER_PRODUCTS = 8;
    private static final Logger LOG = Logger.getLogger(ProductEngagementComponentController.class);

    @Override
    protected void fillModelInternal(final HttpServletRequest request, final Model model,
            final ProductEngagementComponentModel component) {

        final RegionLayout leftRegionLayout = component.getLeftRegionLayout();
        final List<EngagementContainerSlotData> leftRegionSlots = new ArrayList<>();
        final List<BannerComponentModel> leftBanners = Arrays.asList(component.getLeftBanner1(),
                component.getLeftBanner2(), component.getLeftBanner3(), component.getLeftBanner4());

        final RegionLayout rightRegionLayout = component.getRightRegionLayout();


        Queue<TargetProductListerData> backFillProducts = new LinkedList<>();

        if (!(RegionLayout.ONE.equals(leftRegionLayout) && RegionLayout.ONE.equals(rightRegionLayout))) {
            backFillProducts = getBackFillProducts(component, backFillProducts);
        }

        if (RegionLayout.ONE.equals(leftRegionLayout)) {
            populateSingleBanner(request, leftRegionSlots, leftBanners);
        }
        else {
            populateRegionSlots(request, leftBanners, component.getProducts(),
                    backFillProducts, leftRegionSlots);
        }

        final List<EngagementContainerSlotData> rightRegionSlots = new ArrayList<>();
        final List<BannerComponentModel> rightBanners = Arrays.asList(component.getRightBanner1(),
                component.getRightBanner2(), component.getRightBanner3(), component.getRightBanner4());

        if (RegionLayout.ONE.equals(rightRegionLayout)) {
            populateSingleBanner(request, rightRegionSlots, rightBanners);
        }
        else {
            populateRegionSlots(request, rightBanners, component.getProductsForRightRegion(),
                    backFillProducts, rightRegionSlots);
        }

        model.addAttribute("title", component.getTitle());
        model.addAttribute("link", component.getLink());
        model.addAttribute("leftRegionLayout", leftRegionLayout.getCode());
        model.addAttribute("rightRegionLayout", rightRegionLayout.getCode());
        model.addAttribute("leftRegionSlots", leftRegionSlots);
        model.addAttribute("rightRegionSlots", rightRegionSlots);
    }

    private void populateSingleBanner(final HttpServletRequest request,
            final List<EngagementContainerSlotData> leftRegionSlots, final List<BannerComponentModel> leftBanners) {
        final BannerComponentModel banner = getFirstAvailableBanner(request, leftBanners);
        if (banner != null) {
            final EngagementContainerSlotData containerSlotData = new EngagementContainerSlotData();
            containerSlotData.setBanner(banner);
            leftRegionSlots.add(containerSlotData);
        }
    }

    private BannerComponentModel getFirstAvailableBanner(final HttpServletRequest request,
            final List<BannerComponentModel> banners) {
        for (final BannerComponentModel banner : banners) {
            if (banner != null && isComponentAllowed(banner, request)) {
                return banner;
            }
        }

        LOG.error("No valid banner available to populate this region");
        return null;
    }

    protected void populateRegionSlots(final HttpServletRequest request, final List<BannerComponentModel> banners,
            final List<ProductModel> products, final Queue<TargetProductListerData> backFillProducts,
            final List<EngagementContainerSlotData> regionSlots) {

        int slotsMissingData = 0;

        // Fill the slots with valid banners
        for (final BannerComponentModel banner : banners) {
            final EngagementContainerSlotData containerSlotData = new EngagementContainerSlotData();
            if (banner != null && isComponentAllowed(banner, request)) {
                containerSlotData.setBanner(banner);
            }
            else {
                slotsMissingData++;
            }
            regionSlots.add(containerSlotData);
        }

        if (slotsMissingData > 0) { // Not enough banners to fill all the slots
            populateSlotsWithProducts(products, backFillProducts, regionSlots, slotsMissingData);
        }
    }

    private void populateSlotsWithProducts(
            final List<ProductModel> products, final Queue<TargetProductListerData> backFillProducts,
            final List<EngagementContainerSlotData> regionSlots, final int slotsMissingData) {

        final EndecaSearchStateData searchStateDataForProducts = getLinkedProducts(products, false); // Check the attached product list

        List<TargetProductListerData> productDatas = null;
        if (searchStateDataForProducts != null) {
            productDatas = getTargetProductsList(
                    searchStateDataForProducts,
                    products, false, true);
        }

        if (productDatas == null) {
            productDatas = new ArrayList<>();
        }
        if (productDatas.size() < slotsMissingData) {

            for (int index = productDatas.size(); index < slotsMissingData; index++) {
                final TargetProductListerData backFillProduct = backFillProducts.poll();
                if (backFillProduct != null) {
                    productDatas.add(backFillProduct);
                }
            }
        }

        //Fill the slots with product data
        if (CollectionUtils.isNotEmpty(productDatas)) {
            int productIndex = 0;
            for (final EngagementContainerSlotData containerSlotData : regionSlots) {
                if (containerSlotData.getBanner() == null && productIndex < productDatas.size()) { // no banner found, need to fill with a product
                    containerSlotData.setProductData(productDatas.get(productIndex++));
                }
            }
        }
    }


    private Queue<TargetProductListerData> getBackFillProducts(final ProductEngagementComponentModel component,
            final Queue<TargetProductListerData> backFillProducts) {
        final EndecaSearchStateData searchStateDataForCategory = collectCategoryAndBrandProducts(component,
                getCarouselSearchPageSize(),
                null);
        if (searchStateDataForCategory == null) {
            return backFillProducts;
        }
        final List<TargetProductListerData> productDatas = getTargetProductsList(
                searchStateDataForCategory,
                null, false, true);
        if (CollectionUtils.isNotEmpty(productDatas)) {
            backFillProducts.addAll(productDatas);
        }

        return backFillProducts;
    }

    @Override
    protected int getMaxProducts() {
        return MAX_NUMBER_PRODUCTS;
    }

}

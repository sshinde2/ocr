/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.acceleratorcms.model.components.ProductReferencesComponentModel;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.endeca.navigation.AggrERec;
import com.endeca.navigation.AggrERecList;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.Navigation;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.converter.AggregatedEndecaRecordToTargetProductListerConverter;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.querybuilder.EndecaProductQueryBuilder;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfacades.config.TargetSharedConfigFacade;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.util.TargetProductDataHelper;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.ProductDataHelper;


/**
 * Controller for CMS ProductReferencesComponent
 */
@Controller("ProductReferencesComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.PRODUCT_REFERENCES_COMPONENT)
public class ProductReferencesComponentController extends
        AbstractCMSComponentController<ProductReferencesComponentModel> {

    protected static final List<ProductOption> PRODUCT_OPTIONS = Arrays
            .asList(ProductOption.BASIC, ProductOption.PRICE);

    @Resource(name = "productFacade")
    private ProductFacade productFacade;

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Resource(name = "productService")
    private ProductService productService;

    @Resource(name = "targetSharedConfigFacade")
    private TargetSharedConfigFacade targetSharedConfigFacade;

    @Resource(name = "aggrERecTotargetProductListerConverter")
    private AggregatedEndecaRecordToTargetProductListerConverter<AggrERec, TargetProductListerData> aggrERecToProductConverter;

    @Resource(name = "endecaProductQueryBuilder")
    private EndecaProductQueryBuilder endecaProductQueryBuilder;

    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
            final ProductReferencesComponentModel component) {

        final Boolean isNotAvailableOnlineAndInStore = (Boolean)request
                .getAttribute(ControllerConstants.REQUEST_PARAM_NOTAVAILABLE_ONLINE_AND_INSTORE);

        if (targetFeatureSwitchFacade.isProductEndOfLifeCrossSellEnabled() && null != isNotAvailableOnlineAndInStore
                && isNotAvailableOnlineAndInStore.booleanValue()) {
            populateYouMayAlsoLike(ProductDataHelper.getCurrentProduct(request), model);

        }
        else {
            final List<ProductReferenceData> productReferences = productFacade.getProductReferencesForCode(
                    ProductDataHelper.getCurrentProduct(request), component.getProductReferenceTypes(), PRODUCT_OPTIONS,
                    component.getMaximumNumberProducts());
            model.addAttribute("productReferences", productReferences);
        }

        model.addAttribute("title", component.getTitle());
    }

    /**
     * @param currentProductCode
     * @param model
     */
    private void populateYouMayAlsoLike(final String currentProductCode, final Model model) {

        final ProductModel productModel = productService.getProductForCode(currentProductCode);
        //get base product to get original category
        final TargetProductModel baseProduct = (TargetProductModel)TargetProductDataHelper
                .getBaseProduct(productModel);
        //get color variant to get size group
        final TargetColourVariantProductModel colourVariantProduct = getColourVariantProduct(productModel);
        if (null != colourVariantProduct && null != baseProduct.getOriginalCategory()) {

            final EndecaSearchStateData searchStateData = populateEndecaSearchStateData(colourVariantProduct,
                    baseProduct);
            final ENEQueryResults results = endecaProductQueryBuilder.getYouMayAlsoLikeQueryResults(searchStateData,
                    baseProduct);

            if (null != results) {
                final List<TargetProductListerData> productListerData = getProductListerData(results, baseProduct);
                model.addAttribute("productListerData", productListerData);
            }
        }

    }

    /**
     * @param results
     * @param baseProduct
     * @return productDataList
     */
    private List<TargetProductListerData> getProductListerData(final ENEQueryResults results,
            final TargetProductModel baseProduct) {
        final List<TargetProductListerData> productDataList = new ArrayList<>();
        final Navigation navigation = results.getNavigation();
        final AggrERecList aggregateERecs = navigation.getAggrERecs();
        if (CollectionUtils.isEmpty(aggregateERecs)) {
            return productDataList;
        }
        for (final Object endecaRecordObject : aggregateERecs) {
            final AggrERec aggrERec = (AggrERec)endecaRecordObject;
            final TargetProductListerData targetProductListerData = aggrERecToProductConverter.convert(aggrERec);
            if (!baseProduct.getCode().equals(targetProductListerData.getCode())) {
                productDataList.add(targetProductListerData);
            }
        }
        return productDataList;
    }

    /**
     * @param colourVariantProduct
     * @param baseProduct
     * @return endecaSearchStateData
     */
    private EndecaSearchStateData populateEndecaSearchStateData(
            final TargetColourVariantProductModel colourVariantProduct,
            final TargetProductModel baseProduct) {
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setSkipDefaultSort(false);
        endecaSearchStateData.setSizeGroup(
                colourVariantProduct.getSizeGroup() == null ? "" : colourVariantProduct.getSizeGroup().getCode());
        endecaSearchStateData.setCategoryCodes(Arrays.asList(baseProduct.getOriginalCategory().getCode()));
        endecaSearchStateData.setFieldListConfig(EndecaConstants.FieldList.COMPONENT_FIELDLIST);
        endecaSearchStateData
                .setItemsPerPage(targetSharedConfigFacade.getInt("productEndOfLife.crossSellItemsPerPage", 12));
        return endecaSearchStateData;
    }

    /**
     * @param productModel
     * @return TargetColourVariantProductModel
     */
    private TargetColourVariantProductModel getColourVariantProduct(final ProductModel productModel) {

        if (productModel instanceof TargetColourVariantProductModel) {
            return (TargetColourVariantProductModel)productModel;
        }
        else if (productModel instanceof TargetSizeVariantProductModel) {
            return (TargetColourVariantProductModel)((TargetSizeVariantProductModel)productModel).getBaseProduct();
        }
        return null;
    }
}

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtfacades.suggestion.SimpleSuggestionFacade;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtwebcore.model.cms2.components.PurchasedProductReferencesComponentModel;


/**
 * Controller for CMS PurchasedProductReferencesComponent
 */
@Controller("PurchasedProductReferencesComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.PURCHASED_PRODUCT_REFERENCES_COMPONENT)
public class PurchasedProductReferenceComponentController extends
        AbstractCMSComponentController<PurchasedProductReferencesComponentModel>
{
    @Resource(name = "simpleSuggestionFacade")
    private SimpleSuggestionFacade simpleSuggestionFacade;

    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
            final PurchasedProductReferencesComponentModel component)
    {
        if (CollectionUtils.isNotEmpty(component.getProductReferenceTypes())) {
            final List<ProductData> products = simpleSuggestionFacade.getReferencesForPurchasedInCategory(component
                    .getCategory().getCode(), component.getProductReferenceTypes().get(0),
                    component.isFilterPurchased(),
                    component
                            .getMaximumNumberProducts());
            model.addAttribute("productReferences", products);
        }
        model.addAttribute("title", component.getTitle());
    }

    @Override
    protected String getView(final PurchasedProductReferencesComponentModel component)
    {
        return ControllerConstants.Views.Cms.COMPONENT_PREFIX
                + StringUtils.lowerCase(PurchasedProductReferencesComponentModel._TYPECODE);
    }
}

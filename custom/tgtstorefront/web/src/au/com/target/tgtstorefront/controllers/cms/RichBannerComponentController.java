/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtwebcore.model.cms2.components.CoordinateLinkComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.RichBannerComponentModel;


/**
 * @author asingh78
 * 
 */
@Controller("RichBannerComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.RICH_BANNER_COMPONENT)
public class RichBannerComponentController extends AbstractCMSComponentController<RichBannerComponentModel> {

    /* (non-Javadoc)
     * @see au.com.target.tgtstorefront.controllers.cms.AbstractCMSComponentController#fillModel(javax.servlet.http.HttpServletRequest, org.springframework.ui.Model, de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel)
     */
    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
            final RichBannerComponentModel component) {
        final List<CoordinateLinkComponentModel> coordinateLinkComponents = new ArrayList<>();
        final List<CMSLinkComponentModel> linkComponents = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(component.getCmsLinkComponents())) {
            populateLinkComponents(component, coordinateLinkComponents, linkComponents, request);
        }
        model.addAttribute(WebConstants.RICHBANNER, component);
        model.addAttribute(WebConstants.COORDINATE_LINK_COMPONENTS, coordinateLinkComponents);
        model.addAttribute(WebConstants.LINK_COMPONENTS, linkComponents);


        final CMSLinkComponentModel mobileLink = component.getCmsLinkComponent();

        if (mobileLink != null && isComponentAllowed(mobileLink, request)) {
            model.addAttribute(WebConstants.MOBILE_LINK, mobileLink);
        }

    }

    protected void populateLinkComponents(final RichBannerComponentModel component,
            final List<CoordinateLinkComponentModel> coordinateLinkComponents,
            final List<CMSLinkComponentModel> linkComponents, final HttpServletRequest request) {

        for (final CMSLinkComponentModel cmsLinkComponent : component.getCmsLinkComponents()) {

            if (isComponentAllowed(cmsLinkComponent, request)) {
                if (cmsLinkComponent instanceof CoordinateLinkComponentModel) {
                    coordinateLinkComponents.add((CoordinateLinkComponentModel)cmsLinkComponent);
                }
                else {
                    linkComponents.add(cmsLinkComponent);
                }
            }


        }



    }
}

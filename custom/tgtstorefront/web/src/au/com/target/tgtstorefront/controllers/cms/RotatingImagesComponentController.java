/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.CMSRestrictionService;
import de.hybris.platform.cms2lib.model.components.BannerComponentModel;
import de.hybris.platform.cms2lib.model.components.RotatingImagesComponentModel;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * @author asingh78
 * 
 */

@Controller("RotatingImagesComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.ROTATING_IMAGE_COMPONENT)
public class RotatingImagesComponentController<T extends RotatingImagesComponentModel> extends
        AbstractCMSComponentController<RotatingImagesComponentModel> {

    @Autowired
    private CMSRestrictionService cmsRestrictionService;

    /* (non-Javadoc)
     * @see au.com.target.tgtstorefront.controllers.cms.AbstractCMSComponentController#fillModel(javax.servlet.http.HttpServletRequest, org.springframework.ui.Model, de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel)
     */
    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
            final RotatingImagesComponentModel component) {

        if (CollectionUtils.isNotEmpty(component.getBanners())) {
            model.addAttribute(WebConstants.BANNERS, getBannerComponents(component, request));
        }
    }

    private List getBannerComponents(final RotatingImagesComponentModel cmsComponent,
            final HttpServletRequest httpRequest) {
        final boolean previewEnabled = isPreviewEnabled(httpRequest);

        final List bannerComponents = new ArrayList();
        if (CollectionUtils.isEmpty(cmsComponent.getBanners())) {
            return bannerComponents;
        }
        for (final BannerComponentModel component : cmsComponent.getBanners())
        {
            boolean allowed = true;
            if (Boolean.FALSE.equals(component.getVisible())) {
                allowed = false;
            }
            else if (CollectionUtils.isNotEmpty(component.getRestrictions()) && !previewEnabled)
            {
                final RestrictionData data = populate(httpRequest);
                allowed = cmsRestrictionService.evaluateCMSComponent(component, data);
            }

            if (allowed) {
                bannerComponents.add(component);
            }
        }

        return bannerComponents;
    }

}

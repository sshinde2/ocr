/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtfacades.navigation.NavigationMenuItem;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.CMSNavigationComponentHelper;
import au.com.target.tgtstorefront.navigation.NavNodeMenuBuilder;
import au.com.target.tgtstorefront.navigation.subcategorygrid.SubcategoryGridItem;
import au.com.target.tgtwebcore.enums.NavigationNodeTypeEnum;
import au.com.target.tgtwebcore.model.cms2.components.SubcategoryGridComponentModel;


@Controller("SubcategoryGridComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.SUBCATEGORY_GRID_COMPONENT)
public class SubcategoryGridComponentController extends AbstractCMSComponentController<SubcategoryGridComponentModel> {

    private static final Logger LOG = Logger.getLogger(SubcategoryGridComponentController.class);


    @Autowired
    private NavNodeMenuBuilder navNodeMenuBuilder;

    @Resource(name = "cmsNavigationComponentHelper")
    private CMSNavigationComponentHelper cmsNavigationComponentHelper;


    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
            final SubcategoryGridComponentModel component) {
        List<SubcategoryGridItem> subcategoryGridItems = null;
        CMSNavigationNodeModel departmentNavigationNode = component.getNavigationNode();

        if (departmentNavigationNode == null) {
            final String categoryCode = (String)request.getAttribute("categoryCode");
            departmentNavigationNode = cmsNavigationComponentHelper.getCategoryNavigationNode(categoryCode);
        }
        if (departmentNavigationNode != null) {
            subcategoryGridItems = createSubcategoryGridItems(departmentNavigationNode);
        }
        if (CollectionUtils.isNotEmpty(subcategoryGridItems)) {
            model.addAttribute("subcategoryGridItems", subcategoryGridItems);
        }
        else {
            LOG.info("Category navigation node not found");
        }

    }

    protected List<SubcategoryGridItem> createSubcategoryGridItems(final CMSNavigationNodeModel navigationNode) {
        final List<CMSNavigationNodeModel> childNavigationNodes = navigationNode.getChildren();
        if (CollectionUtils.isEmpty(childNavigationNodes)) {
            return null;
        }
        final List<SubcategoryGridItem> subcategoryGridItems = new ArrayList<>();
        for (final CMSNavigationNodeModel childNavigationNode : childNavigationNodes) {
            if (NavigationNodeTypeEnum.COLUMN.equals(childNavigationNode.getType())) {
                addSubcategoryGridItemsForSections(childNavigationNode.getChildren(), subcategoryGridItems);
            }
        }

        return subcategoryGridItems;
    }

    protected void addSubcategoryGridItemsForSections(final List<CMSNavigationNodeModel> navigationNodes,
            final List<SubcategoryGridItem> subcategoryGridItems) {
        if (CollectionUtils.isEmpty(navigationNodes)) {
            return;
        }

        for (final CMSNavigationNodeModel navigationNode : navigationNodes) {
            if ((NavigationNodeTypeEnum.SECTION.equals(navigationNode.getType())) && (navigationNode.isVisible())) {

                final NavigationMenuItem menuItem = navNodeMenuBuilder.createMenuItem(navigationNode);
                final SubcategoryGridItem subCategoryGridItem = new SubcategoryGridItem();
                if (menuItem != null)
                {
                    if ((StringUtils.isNotBlank(menuItem.getLink()))
                            && (StringUtils.isNotBlank(navigationNode.getTitle()))) {
                        subCategoryGridItem.setLinkUrl(menuItem.getLink());
                        subCategoryGridItem.setTitle(navigationNode.getTitle());
                        subcategoryGridItems.add(subCategoryGridItem);
                    }
                }


            }
        }
    }


}

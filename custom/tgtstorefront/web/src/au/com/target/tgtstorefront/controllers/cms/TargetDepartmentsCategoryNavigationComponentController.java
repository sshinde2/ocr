/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.CMSNavigationComponentHelper;
import au.com.target.tgtstorefront.controllers.util.NavigationNodeHelper;
import au.com.target.tgtstorefront.navigation.megamenu.MegaMenuColumn;
import au.com.target.tgtwebcore.model.cms2.components.TargetDepartmentsCategoryNavigationComponentModel;


/**
 * @author mjanarth
 *
 */

@Controller("TargetDepartmentsCategoryNavigationComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.TARGET_DEPT_CAT_NAV_COMPONENT)
public class TargetDepartmentsCategoryNavigationComponentController extends
        AbstractCMSComponentController<TargetDepartmentsCategoryNavigationComponentModel> {

    @Resource(name = "navigationNodeHelper")
    private NavigationNodeHelper navigationNodeHelper;

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Resource(name = "cmsNavigationComponentHelper")
    private CMSNavigationComponentHelper cmsNavigationComponentHelper;

    /* (non-Javadoc)
     * @see au.com.target.tgtstorefront.controllers.cms.AbstractCMSComponentController#fillModel(javax.servlet.http.HttpServletRequest, org.springframework.ui.Model, de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel)
     */
    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
            final TargetDepartmentsCategoryNavigationComponentModel component) {
        CMSNavigationNodeModel cmsNavigationNode = component.getNavigationNode();
        if (null == cmsNavigationNode && null != request.getAttribute("categoryCode")) {
            cmsNavigationNode = cmsNavigationComponentHelper.getCategoryNavigationNode((String)request
                    .getAttribute("categoryCode"));
        }

        if (null != cmsNavigationNode) {
            final List<MegaMenuColumn> megaMenuList = new ArrayList<>();
            if (null != cmsNavigationNode.getChildren()) {
                for (final CMSNavigationNodeModel childNode : cmsNavigationNode.getChildren()) {
                    final MegaMenuColumn column = navigationNodeHelper.buildColumn(childNode);
                    megaMenuList.add(column);
                    model.addAttribute("columns", megaMenuList);
                }
            }
            final boolean featureSvgBasedMenuEnabled = targetFeatureSwitchFacade
                    .isFeatureEnabled("svg.based.menu");
            model.addAttribute("featureSvgBasedMenuEnabled", new Boolean(featureSvgBasedMenuEnabled));
        }

    }
}

/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;

import java.text.MessageFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.AbstractPageController;
import au.com.target.tgtwebcore.model.cms2.components.TargetExternalRecommendationsCarouselComponentModel;


/**
 * @author pvarghe2
 * 
 *         Controller for External Recommendations Carousel Component.
 */
@Controller("TargetExternalRecommendationsCarouselComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.TARGET_EXTERNAL_RECOMMENDATIONS_CAROUSEL)
public class TargetExternalRecommendationsCarouselComponentController
        extends AbstractCMSComponentController<TargetExternalRecommendationsCarouselComponentModel> {


    private static final String HOME_PAGE = "home";
    private static final String CART_PAGE = "cart";
    private static final String CATEGORY_PAGE = "category";
    private static final String PRODUCT_PAGE = "product";

    @Value("#{configurationService.getConfiguration().getString('tgtstore.salesforce.recommendation.url','https://{0}.recs.igodigital.com/a/v2/{0}/{1}/recommend.json{2}')}")
    private String salesforceRecommendationUrl;

    @Value("#{configurationService.getConfiguration().getString('tgtstore.salesforce.clientid','7203076')}")
    private String clientId;


    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
            final TargetExternalRecommendationsCarouselComponentModel component) {
        model.addAttribute("requestURL", generateUrl(request));
    }

    /**
     * Generate recommendation JSON URL based on the page type.
     * 
     * @param request
     * @return URL
     */
    private String generateUrl(final HttpServletRequest request) {
        final Object pageTypeObj = request.getAttribute("pageType");
        if (null == pageTypeObj) {
            return buildRecommendationUrl(HOME_PAGE, null, null);
        }
        return generatePageSpecificUrl(request, (AbstractPageController.PageType)pageTypeObj);
    }

    private String generatePageSpecificUrl(final HttpServletRequest request,
            final AbstractPageController.PageType pageType) {
        if (pageType.equals(AbstractPageController.PageType.Product)) {
            return buildRecommendationUrl(PRODUCT_PAGE, "item", getProductCode(request));
        }
        if (pageType.equals(AbstractPageController.PageType.Cart)) {
            return buildRecommendationUrl(CART_PAGE, CART_PAGE, getCartItems(request));
        }
        if (pageType.equals(AbstractPageController.PageType.Favourite)) {
            return buildRecommendationUrl(CATEGORY_PAGE, "wishlist", "{wishlist}");
        }
        if (pageType.equals(AbstractPageController.PageType.Category)) {
            return buildRecommendationUrl(CATEGORY_PAGE, CATEGORY_PAGE, getCategoryName(request));
        }
        return buildRecommendationUrl(HOME_PAGE, null, null);
    }

    private String getCategoryName(final HttpServletRequest request) {
        final List<Breadcrumb> breadcrumb = (List<Breadcrumb>)request.getAttribute("breadcrumbs");
        String categoryName = "";
        if (CollectionUtils.isNotEmpty(breadcrumb)) {
            categoryName = breadcrumb.get(0).getName();
        }
        return categoryName;
    }

    private String getProductCode(final HttpServletRequest request) {
        final TargetProductData productData = (TargetProductData)request.getAttribute(PRODUCT_PAGE);
        return productData == null ? "" : productData.getCode();
    }

    private String getCartItems(final HttpServletRequest request) {
        final TargetCartData cartData = (TargetCartData)request.getAttribute("cartData");
        String basketItem = "";
        if (cartData != null) {
            int index = 0;
            final List<OrderEntryData> orderEntryDataList = cartData.getEntries();
            for (final OrderEntryData orderEntryData : orderEntryDataList) {
                final String variant = orderEntryData.getProduct().getCode();
                if (index > 0) {
                    basketItem += "|";
                }
                basketItem += variant;
                index++;
            }
        }
        return basketItem;
    }

    private String buildRecommendationUrl(final String page, final String queryParam, final String queryValue) {
        String queryString = "";
        if (StringUtils.isNotBlank(queryParam)) {
            queryString = String.format("%s%s%s%s", "?", queryParam, "=", queryValue);
        }
        return MessageFormat.format(salesforceRecommendationUrl, clientId, page, queryString);
    }
}

package au.com.target.tgtstorefront.controllers.cms;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtwebcore.model.cms2.components.TargetFlexiBannerContainerComponentModel;


/**
 * Controller for CMS TargetFlexiBannerContainerComponent.
 */
@Controller("TargetFlexiBannerContainerComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.TARGET_FLEXI_BANNERS_CONTAINER_COMPONENT)
public class TargetFlexiBannersContainerComponentController extends
        RotatingImagesComponentController<TargetFlexiBannerContainerComponentModel>
{
    // Component uses rotating image component controller so that restrictions and visibility of banners can be evaluated
}

/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtfacades.stl.TargetShopTheLookFacade;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtwebcore.model.cms2.components.TargetProductGroupCarouselComponentModel;
import au.com.target.tgtwebcore.model.cms2.pages.TargetProductCollectionPageModel;
import au.com.target.tgtwebcore.model.cms2.pages.TargetProductGroupPageModel;


@Controller("TargetProductGroupCarouselComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.TARGET_PRODUCT_GROUP_CAROUSEL_COMPONENT)
public class TargetProductGroupCarouselComponentController extends
        AbstractCMSComponentController<TargetProductGroupCarouselComponentModel> {

    @Resource
    private ConfigurationService configurationService;

    @Resource
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Resource
    private TargetShopTheLookFacade targetShopTheLookFacade;

    private int previousLookCount;

    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
            final TargetProductGroupCarouselComponentModel component) {

        final String lookUrl = (String)request.getAttribute("lookUrl");

        if (targetFeatureSwitchService
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.STEP_SHOP_THE_LOOK)) {
            if (component.getLookCollection() == null) {
                return;
            }
            final List<TargetLookModel> looks = getLooks(component.getLookCollection(), lookUrl);
            if (CollectionUtils.isEmpty(looks)) {
                return;
            }
            model.addAttribute("lookCollection", component.getLookCollection());
            model.addAttribute("looks", targetShopTheLookFacade.populateLookDetailsDataList(looks));
        }
        else {
            if (component.getNavigationNode() == null
                    || CollectionUtils.isEmpty(component.getNavigationNode().getChildren())) {
                return;
            }
            final List<TargetProductGroupPageModel> groupPageModels = getGroupPageModels(component, lookUrl);
            final TargetProductCollectionPageModel collectionPageModel = getCollectionPageModel(
                    component.getNavigationNode());
            model.addAttribute("groupPageModels", groupPageModels);
            model.addAttribute("collectionPageModel", collectionPageModel);
        }
        model.addAttribute("request", request);
    }

    /**
     * @param component
     * @param lookUrl
     * @return List of {@link TargetProductGroupPageModel}
     */
    private List<TargetProductGroupPageModel> getGroupPageModels(
            final TargetProductGroupCarouselComponentModel component, final String lookUrl) {
        final List<TargetProductGroupPageModel> groupPageModels = new ArrayList<>();
        final List<CMSNavigationNodeModel> children = component.getNavigationNode().getChildren();

        int foundAtIndex = -1;
        int index = 0;
        for (final CMSNavigationNodeModel node : children) {
            final TargetProductGroupPageModel currentModel = getGroupPageModel(node);
            if (currentModel != null) {
                if (!currentModel.getLabel().equals(lookUrl)) {
                    groupPageModels.add(currentModel);
                }
                else {
                    foundAtIndex = index;
                }
            }
            index++;
        }
        /* If the component is on a look page, and it is part of the collection displayed
        Shift the positioning of the carousel to begin at 2 looks before the index */

        if (foundAtIndex >= getPreviousLookCount()) {
            Collections.rotate(groupPageModels, -(foundAtIndex - getPreviousLookCount()));
        }
        return groupPageModels;
    }

    /**
     * @param navigationNodeModel
     * @return {@link TargetProductGroupPageModel}
     */
    private TargetProductGroupPageModel getGroupPageModel(final CMSNavigationNodeModel navigationNodeModel) {
        if (navigationNodeModel.isVisible()) {
            final List<CMSNavigationEntryModel> entries = navigationNodeModel.getEntries();
            for (final CMSNavigationEntryModel entry : entries) {
                final ItemModel entryItem = entry.getItem();
                if (entryItem instanceof TargetProductGroupPageModel) {
                    return (TargetProductGroupPageModel)entryItem;
                }
            }
        }
        return null;
    }

    /**
     * @param navigationNodeModel
     * @return {@link TargetProductCollectionPageModel}
     */
    private TargetProductCollectionPageModel getCollectionPageModel(final CMSNavigationNodeModel navigationNodeModel) {
        if (navigationNodeModel.isVisible()) {
            final List<CMSNavigationEntryModel> entries = navigationNodeModel.getEntries();
            for (final CMSNavigationEntryModel entry : entries) {
                final ItemModel entryItem = entry.getItem();
                if (entryItem instanceof TargetProductCollectionPageModel) {
                    return (TargetProductCollectionPageModel)entryItem;
                }
            }
        }
        return null;
    }

    /**
     * @param collection
     * @param lookUrl
     * @return List of {@link TargetLookModel}
     */
    private List<TargetLookModel> getLooks(final TargetLookCollectionModel collection, final String lookUrl) {
        final List<TargetLookModel> looks = new ArrayList<>(
                targetShopTheLookFacade.getVisibleLooksForCollection(collection.getId()));

        int currentLookIndex = -1;
        int i = 0;
        for (final Iterator<TargetLookModel> iterator = looks.iterator(); iterator.hasNext();) {
            final TargetLookModel look = iterator.next();
            if (StringUtils.isNotBlank(lookUrl) && StringUtils.endsWithIgnoreCase(lookUrl, look.getUrl())) {
                currentLookIndex = i;
                iterator.remove();
            }
            i++;
        }
        if (currentLookIndex >= getPreviousLookCount()) {
            Collections.rotate(looks, -(currentLookIndex - getPreviousLookCount()));
        }

        return looks;
    }

    /**
     * @return the previousLookCount
     */
    public int getPreviousLookCount() {
        return previousLookCount;
    }

    /**
     * @param previousLookCount
     *            the previousLookCount to set
     */
    @Value("#{configurationService.configuration.getInt('tgtstorefront.grouppage.relatedlooks.previous.count', 2)}")
    public void setPreviousLookCount(final int previousLookCount) {
        this.previousLookCount = previousLookCount;
    }
}

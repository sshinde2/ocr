/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.endeca.infront.converter.impl.EndecaContentItemContentsProcessor;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtwebcore.model.cms2.components.TargetRecommendationsComponentModel;

import com.endeca.infront.assembler.ContentItem;


/**
 * @author ayushman
 *
 */
@Controller("TargetRecommendationsComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.TARGET_RECOMMENDATIONS_COMPONENT)
public class TargetRecommendationsComponentController<T extends TargetRecommendationsComponentModel> extends
        AbstractCMSComponentController<TargetRecommendationsComponentModel> {

    @Resource
    private EndecaContentItemContentsProcessor endecaContentItemContentsParser;

    /* (non-Javadoc)
     * @see au.com.target.tgtstorefront.controllers.cms.AbstractCMSComponentController#fillModel(javax.servlet.http.HttpServletRequest, org.springframework.ui.Model, de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel)
     */
    @Override
    protected void fillModel(final HttpServletRequest request, final Model model,
            final TargetRecommendationsComponentModel component) {

        final Object recommProductsData = request.getAttribute(WebConstants.RECOMMENDED_PRODUCTS_KEY);

        if (null != recommProductsData && recommProductsData instanceof ContentItem) {
            final EndecaSearch endecaSearch = new EndecaSearch();
            endecaContentItemContentsParser.convertContentItem((ContentItem)recommProductsData, endecaSearch);

            model.addAttribute(WebConstants.RECOMMENDED_PRODUCTS_KEY, endecaSearch.getRecommendations());
        }
    }

}

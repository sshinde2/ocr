/**
 * 
 */
package au.com.target.tgtstorefront.controllers.custom.dimensions.impl;

import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.customdimensions.TargetCustomDimensionsMapping;


/**
 * @author bmcmuffin
 * 
 */
public class TargetCustomDimensionsMappingImpl implements ControllerConstants, TargetCustomDimensionsMapping {

    /**
     * @return assorted
     */
    @Override
    public String getAssorted() {
        return CustomDimensions.ASSORTED;
    }

    /**
     * @return displayOnly
     */
    @Override
    public String getDisplayOnly() {
        return CustomDimensions.DISPLAY_ONLY;
    }

    /**
     * @return endOfLife
     */
    @Override
    public String getEndOfLife() {
        return CustomDimensions.END_OF_LIFE;
    }
}

/**
 * 
 */
package au.com.target.tgtstorefront.controllers.dimensionscache;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.endeca.infront.cache.EndecaDimensionCacheService;
import au.com.target.tgtstorefront.controllers.AbstractController;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * @author rmcalave
 * 
 */
@Controller
public class EndecaCacheController extends AbstractController {

    @Resource(name = "endecaDimensionCacheService")
    private EndecaDimensionCacheService endecaDimensionCacheService;

    @RequestMapping(value = ControllerConstants.ENDECA_CLEAR_CACHE, method = RequestMethod.GET)
    @ResponseBody
    public String clearEndecaCache()
    {
        final Boolean result = endecaDimensionCacheService.invalidateEndecaDimensionsCache();
        return result.toString();
    }

    /**
     * @return the endecaDimensionCacheService
     */
    protected EndecaDimensionCacheService getEndecaDimensionCacheService() {
        return endecaDimensionCacheService;
    }

}
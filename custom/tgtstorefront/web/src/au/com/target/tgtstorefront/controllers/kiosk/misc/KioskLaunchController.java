/**
 * 
 */
package au.com.target.tgtstorefront.controllers.kiosk.misc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * @author gbaker2
 * 
 */
@Controller
public class KioskLaunchController {

    @RequestMapping(value = "/~/k/launch", method = RequestMethod.GET)
    public String doLaunch()
    {
        return ControllerConstants.Views.Pages.Kiosk.LAUNCH;
    }

    @RequestMapping(value = "/~/k/launch-ssl", method = RequestMethod.GET)
    public String doLaunchSSL()
    {
        return ControllerConstants.Views.Pages.Kiosk.LAUNCH_SSL;
    }


}

/**
 * 
 */
package au.com.target.tgtstorefront.controllers.kiosk.misc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * Controller for kiosk reset success. Simply forwards to a handling jsp page
 * 
 */
@Controller
public class KioskResetController {

    @RequestMapping(value = "/~/k/reset", method = RequestMethod.GET)
    public String doReset()
    {
        return ControllerConstants.Views.Pages.Kiosk.RESET;
    }

}

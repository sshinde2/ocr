/**
 * 
 */
package au.com.target.tgtstorefront.controllers.kiosk.util;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaBreadCrumb;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * @author gbaker2
 * 
 */
/**
 * @author smudumba
 * 
 */
public class BulkyBoardHelper {

    public static final String BULKY_BOARD_STORE_NUMBER_LABEL = "bulkyBoardStoreNumber";
    public static final String BULKY_BOARD_KEY = "kiosk.bulkyboard.name";

    private static final Logger LOG = Logger.getLogger(BulkyBoardHelper.class);

    private static final String BULKY_BOARD_LABEL = "bulkyBoardContentPage";
    private static final String BULKY_BOARD_TOP_LEVEL_LABEL = "bulkyBoardTopLevel";
    private static final String DIVIDER = ":";

    @Resource(name = "messageSource")
    protected MessageSource messageSource;

    @Resource(name = "i18nService")
    protected I18NService i18nService;

    @Resource(name = "cmsPageService")
    private CMSPageService cmsPageService;

    @Resource(name = "targetStoreLocatorFacade")
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    public String createDefaultSearchQuery(final String storeNumber) {
        final StringBuilder searchQuery = new StringBuilder();
        searchQuery.append(DIVIDER);
        searchQuery.append(DIVIDER);
        searchQuery.append(BULKY_BOARD_STORE_NUMBER_LABEL);
        searchQuery.append(DIVIDER);
        searchQuery.append(storeNumber);
        return searchQuery.toString();
    }

    public AbstractPageModel getContentPage() {
        try {
            return cmsPageService.getPageForLabelOrId(BULKY_BOARD_LABEL);
        }
        catch (final CMSItemNotFoundException e) {
            LOG.error("Bulky Board missing content page", e);
        }
        return null;
    }

    /**
     * Push "Bulky Board" onto the start of the breadcrumb List
     */
    public void updateBreadcrumb(final List<Breadcrumb> breadcrumbs,
            final Integer bulkyBoardStoreNumber, final String categoryCode) {
        String classLink = StringUtils.EMPTY;

        if (TgtCoreConstants.Category.ALL_PRODUCTS.equals(categoryCode)) {
            breadcrumbs.removeAll(breadcrumbs);
            classLink = "active";
        }

        // Push onto the start of the list
        breadcrumbs.add(
                0,
                new Breadcrumb(ControllerConstants.BULKYBOARD + bulkyBoardStoreNumber, messageSource.getMessage(
                        BULKY_BOARD_KEY, null, i18nService
                                .getCurrentLocale()),
                        classLink));
    }

    /**
     * Populate extra Bulky Board specific data into the model
     */
    public void populateBulkyBoard(final Model model, final String categoryCode, final Integer bulkyBoardStoreNumber) {
        model.addAttribute(BULKY_BOARD_STORE_NUMBER_LABEL, bulkyBoardStoreNumber);
        if (TgtCoreConstants.Category.ALL_PRODUCTS.equals(categoryCode)) {
            model.addAttribute(BULKY_BOARD_TOP_LEVEL_LABEL, Boolean.TRUE);
        }
    }

    /**
     * Parse and extract the bulkyBoardStoreNumber value from a searchQuery string
     * 
     * @param searchQuery
     * @return storeNumber
     */
    public Integer parseStoreNumber(final String searchQuery) {
        final List<String> params = CollectionUtils.arrayToList(StringUtils.split(searchQuery, ":"));
        for (final Iterator<String> iterator = params.iterator(); iterator.hasNext();) {
            if (BULKY_BOARD_STORE_NUMBER_LABEL.equals(iterator.next()) && iterator.hasNext()) {
                try {
                    return Integer.valueOf(iterator.next());
                }
                catch (final Exception ex) {
                    break;
                }
            }
        }
        return null;
    }

    /**
     * get the store number of a POS based on the bulkyBoardStoreNumber value from a searchQuery string
     * 
     * @param searchQuery
     * @return storeNumber
     */
    public Integer getBulkyBoardStoreNumber(final String searchQuery) {
        final Integer storeNumber = parseStoreNumber(searchQuery);

        if (storeNumber == null) {
            return null;
        }

        final TargetPointOfServiceData selectedStore = targetStoreLocatorFacade.getPointOfService(storeNumber);

        if (selectedStore != null) {
            return selectedStore.getStoreNumber();
        }

        return null;

    }


    /**
     * Get the BulkyBoard display name
     * 
     * @return String
     */
    public String getBulkyBoardDimDisplayName() {

        return messageSource.getMessage(
                BULKY_BOARD_KEY, null, i18nService
                        .getCurrentLocale());
    }


    /**
     * Return BulkyBoardNumber from searchResults
     * 
     * @param searchResults
     * @return Integer
     */
    public Integer getBulkyBoardNumber(final EndecaSearch searchResults) {
        String bulkyBoardNumber = null;
        Integer result = null;

        final List<EndecaBreadCrumb> endecaCrumbs = searchResults.getBreadCrumb();

        if (null != endecaCrumbs) {
            for (final EndecaBreadCrumb breadCrumb : endecaCrumbs) {

                if (StringUtils.equalsIgnoreCase(EndecaConstants.EndecaRecordSpecificFields.BULKYBOARD,
                        breadCrumb.getDimensionType())) {
                    bulkyBoardNumber = breadCrumb.getElement();
                }
            }
        }
        if (null != bulkyBoardNumber) {
            try {
                result = Integer.valueOf(bulkyBoardNumber);
            }
            catch (final NumberFormatException e) {
                result = null;
            }
        }

        return result;
    }

}

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package au.com.target.tgtstorefront.controllers.misc;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.text.MessageFormat;
import java.util.Arrays;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;
import au.com.target.tgtcore.order.TargetCommerceCartModificationStatus;
import au.com.target.tgtfacades.giftcards.exceptions.GiftCardValidationException;
import au.com.target.tgtfacades.order.TargetCartFacade;
import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtstorefront.controllers.AbstractController;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.CartDataHelper;
import au.com.target.tgtstorefront.forms.AddToCartForm;
import au.com.target.tgtstorefront.giftcards.converters.GiftRecipientFormToDTOConverter;


/**
 * Controller for Add to Cart functionality which is not specific to a certain page.
 */
@Controller
public class AddToCartController extends AbstractController {
    protected static final Logger LOG = Logger.getLogger(AddToCartController.class);

    @Resource(name = "targetCartFacade")
    private TargetCartFacade targetCartFacade;

    @Resource(name = "productFacade")
    private ProductFacade productFacade;

    @Resource(name = "messageSource")
    private MessageSource messageSource;

    @Resource(name = "i18nService")
    private I18NService i18nService;

    @Resource(name = "giftRecipientFormToDTOConverter")
    private GiftRecipientFormToDTOConverter giftRecipientFormToDTOConverter;

    @Resource(name = "targetProductFacade")
    private TargetProductFacade targetProductFacade;

    @RequestMapping(value = "/cart/add", method = RequestMethod.POST, produces = "application/json")
    public String addToCart(@Valid final AddToCartForm addtoCartForm, final BindingResult bindingResult,
            final Model model,
            final HttpServletResponse response) {
        final String code = addtoCartForm.getProductCode();
        final String qtyString = addtoCartForm.getQty();
        final Boolean isReactRequest = addtoCartForm.getIsReactRequest();

        final Long qty = processQuantity(model, qtyString);
        if (qty == null) {
            return getAddToCartPopup();
        }

        final boolean isGiftCard = targetCartFacade.isProductAGiftCard(code);
        final boolean isDigitalGiftCard = targetCartFacade.isProductADigitalGiftCard(code);

        model.addAttribute("giftCard", Boolean.valueOf(isGiftCard));
        model.addAttribute("digitalGiftCard", Boolean.valueOf(isDigitalGiftCard));
        model.addAttribute("isReactRequest", isReactRequest);


        //The product have size variants, user must select a size
        if (targetCartFacade.hasVariantsForProduct(code)) {
            String errorMessage = null;

            if (isGiftCard) {
                errorMessage = "basket.error.product.cardvalue";
            }
            else {
                if (targetCartFacade.hasSizeVariantsForProduct(code)) {
                    errorMessage = "basket.error.product.size";
                }
                else {
                    errorMessage = "basket.error.product.colour";
                }
            }

            model.addAttribute("errorMsg", errorMessage);
            return getAddToCartPopup();
        }

        // Check for validation failure (would be in the GiftRecipientForm)
        if (bindingResult.hasErrors()) {

            model.addAttribute("errorMsg", isGiftCard ? "basket.error.product.giftRecipientValidation"
                    : "basket.error.occurred");
            return getAddToCartPopup();
        }

        final boolean isPhysicalGiftCard = targetCartFacade.isProductAPhysicalGiftcard(code);

        try {
            GiftRecipientDTO giftRecipientDTO = null;
            if (addtoCartForm.getGiftRecipientForm() != null) {
                targetCartFacade.validateGiftCards(code, qty.longValue());
                giftRecipientDTO = giftRecipientFormToDTOConverter.convert(addtoCartForm
                        .getGiftRecipientForm());
            }
            if (isPhysicalGiftCard) {
                targetCartFacade.validatePhysicalGiftCards(code, qty.longValue());
            }

            final CartModificationData cartModification = targetCartFacade.addToCart(code, qty.longValue(),
                    giftRecipientDTO);
            model.addAttribute("quantity", Long.valueOf(cartModification.getQuantityAdded()));
            model.addAttribute("entry", cartModification.getEntry());
            final String statusCode = cartModification.getStatusCode();

            final Boolean success = isSuccess(statusCode);
            model.addAttribute("success", success);
            if (success.booleanValue()) {
                model.addAttribute(new AddToCartForm());
            }

            if (displayErrorMessage(statusCode)) {
                model.addAttribute("errorMsg",
                        CartDataHelper.CART_MESSAGES + cartModification.getStatusCode());
                model.addAttribute("realAddedQuantity", new Long(cartModification.getQuantity()));

            }
            setAnonymousCartCookie(response);
        }
        catch (final GiftCardValidationException e) {
            final Object[] params = new Object[] { e.getPermittedValue() };
            final String errorMessage;

            if (isPhysicalGiftCard) {
                errorMessage = messageSource.getMessage(
                        "basket.error.physical.giftcard." + e.getErrorType().getValue(),
                        params,
                        i18nService.getCurrentLocale());
            }
            else {
                errorMessage = messageSource.getMessage(
                        "basket.error.giftcard." + e.getErrorType().getValue(),
                        params,
                        i18nService.getCurrentLocale());
            }
            model.addAttribute("errorMsg", MessageFormat.format(errorMessage, params));
            model.addAttribute("success", Boolean.FALSE);
        }
        catch (final Exception ex) {
            if (TgtCoreConstants.CART.ERR_PREORDER_CART.equals(ex.getMessage())) {
                model.addAttribute("errorMsg", "basket.preOrder.error.occurred");
            }
            else {
                model.addAttribute("errorMsg", "basket.error.occurred");

            }
            model.addAttribute("quantity", Long.valueOf(0L));
            model.addAttribute("success", Boolean.FALSE);
            LOG.error("Couldn't add product of code " + code + " to cart", ex);
        }
        finally {
            model.addAttribute("cartData", targetCartFacade.getSessionCart());
        }

        // Retrieve the product data again to ensure that the data is current after
        // add to cart.
        final TargetProductData updatedProductData = (TargetProductData)productFacade
                .getProductForCodeAndOptions(code,
                        Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.PROMOTIONS,
                                ProductOption.CATEGORIES));
        model.addAttribute("product", updatedProductData);

        return getAddToCartPopup();
    }

    /**
     * @param statusCode
     * @return true is status different than SUCCESS
     */
    private boolean displayErrorMessage(final String statusCode) {
        if (!TargetCommerceCartModificationStatus.SUCCESS.equalsIgnoreCase(statusCode)) {
            return true;
        }
        return false;
    }

    /**
     * @param statusCode
     * @return True is status is success or low stock
     */
    private Boolean isSuccess(final String statusCode) {
        if (!TargetCommerceCartModificationStatus.LOW_STOCK.equalsIgnoreCase(statusCode)
                && !TargetCommerceCartModificationStatus.PREORDER_LOW_STOCK.equalsIgnoreCase(statusCode)
                && !TargetCommerceCartModificationStatus.SUCCESS.equalsIgnoreCase(statusCode)) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    /**
     * @param model
     * @param qtyString
     * @return the casted quantity into long or null if any error
     */
    private Long processQuantity(final Model model, final String qtyString) {
        Long qty = null;
        try {
            qty = Long.valueOf(qtyString);
            if (qty.longValue() <= 0) {
                throw new NumberFormatException();
            }
        }
        catch (final NumberFormatException e) {
            quantityIsInvalid(model);
            return null;
        }
        return qty;
    }

    /**
     * 
     * @param model
     */
    private void quantityIsInvalid(final Model model) {
        model.addAttribute("errorMsg", "basket.error.quantity.invalid");
    }

    /**
     * 
     * @return add to cart popup
     */
    private String getAddToCartPopup() {
        return ControllerConstants.Views.Fragments.Cart.ADD_TO_CART_POPUP;
    }
}

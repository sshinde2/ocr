/**
 * 
 */
package au.com.target.tgtstorefront.controllers.misc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.target.tgtcore.barcode.TargetBarCodeService;
import au.com.target.tgtcore.exception.GenerateBarCodeFailedException;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


@Controller
public class BarcodeGeneratorController {
    private static final String MIME_SVG = "image/svg+xml";
    private static final String MIME_PNG = "image/x-png";

    private static final List<String> SUPPORTED_FORMAT = Arrays.asList(TgtFacadesConstants.BARCODE_PNG_FORMAT,
            TgtFacadesConstants.BARCODE_SVG_FORMAT);

    @Resource(name = "targetBarCodeService")
    private TargetBarCodeService targetBarCodeService;

    @RequestMapping(value = ControllerConstants.GENERATE_BARCODE, method = RequestMethod.GET)
    public void generateBarcode(final HttpServletResponse response,
            @PathVariable("format") final String format, @RequestParam("orderId") final String encodedOrderId)
            throws ServletException, IOException
    {
        Assert.notNull(encodedOrderId, "Encoded OrderId can't be empty!");
        Assert.notNull(format, "Format can't be empty!");

        final String normalizedFormat = format.trim().toLowerCase();

        if (!SUPPORTED_FORMAT.contains(normalizedFormat)) {
            throw new ServletException(format + " is not supported at the moment!");
        }

        ByteArrayOutputStream bout = null;
        try {
            if (TgtFacadesConstants.BARCODE_PNG_FORMAT.equals(normalizedFormat)) {
                bout = targetBarCodeService.generatePngBarcode(encodedOrderId);
                response.setContentType(MIME_PNG);
            }
            else {
                bout = targetBarCodeService.generateSvgBarcode(encodedOrderId);
                response.setContentType(MIME_SVG);
            }
            if (bout != null) {
                response.setContentLength(bout.size());
                response.getOutputStream().write(bout.toByteArray());
            }

        }
        catch (final GenerateBarCodeFailedException e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
        catch (final IOException e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }

    }
}

/**
 * 
 */
package au.com.target.tgtstorefront.controllers.misc;

import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Arrays;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.payment.environment.data.EnvironmentData;
import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtstorefront.controllers.AbstractController;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.CookieRetrievalHelper;
import au.com.target.tgtstorefront.controllers.util.MiniCartDataHelper;
import au.com.target.tgtstorefront.security.cookie.EnhancedCookieGenerator;
import au.com.target.tgtstorefront.util.SearchQuerySanitiser;
import au.com.target.tgtutility.util.JsonConversionUtil;


/**
 * @author rmcalave
 * 
 */
@Controller
public class CustomerController extends AbstractController {

    @Value("#{configurationService.configuration.getProperty('tgtstorefront.host.fqdn')}")
    private String fqdn;

    @Value("#{configurationService.configuration.getProperty('tgtstorefront.google.maps.key')}")
    private String gMapsApiKey;

    @Resource(name = "miniCartDataHelper")
    private MiniCartDataHelper miniCartDataHelper;

    @Resource(name = "targetCustomerFacade")
    private TargetCustomerFacade customerFacade;


    @Resource(name = "productFacade")
    private TargetProductFacade productFacade;

    @Resource(name = "targetStoreLocatorFacade")
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Resource(name = "cookieRetrievalHelper")
    private CookieRetrievalHelper cookieRetrievalHelper;

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Resource(name = "salesApplicationFacade")
    private SalesApplicationFacade salesApplicationFacade;

    @Resource(name = "preferredStoreCookieGenerator")
    private EnhancedCookieGenerator preferredStoreCookieGenerator;

    @Resource(name = "locationPageTypeQuerySanitiser")
    private SearchQuerySanitiser locationPageTypeQuerySanitiser;

    @ModelAttribute("user")
    public CustomerData getUser() {
        return customerFacade.getCurrentCustomer();
    }

    @RequestMapping(value = ControllerConstants.CUSTOMER_LOCATION, method = RequestMethod.GET)
    public String getCustomerLocation(final Model model,
            @RequestParam(value = "psn", required = false) final String psn,
            @RequestParam(value = "type", required = false) final String type,
            final HttpServletResponse response) {
        final String sanitisedType = locationPageTypeQuerySanitiser.sanitiseSearchText(type);

        if (StringUtils.isNotEmpty(sanitisedType)) {
            model.addAttribute("type", sanitisedType);
        }

        populateEnvironmentData(model);

        if (StringUtils.isEmpty(psn) || !StringUtils.isNumeric(psn)) {
            return ControllerConstants.Views.Fragments.Customer.LOCATION;
        }

        model.addAttribute("psn", psn);

        try {
            final TargetPointOfServiceData selectedStore = targetStoreLocatorFacade.getPointOfService(Integer
                    .valueOf(psn));
            //if the store is closed its operations then don't set the data in model
            if (null == selectedStore || BooleanUtils.isTrue(selectedStore.getClosed())) {
                preferredStoreCookieGenerator.removeCookie(response);
                return ControllerConstants.Views.Fragments.Customer.LOCATION;
            }
            model.addAttribute("storeJson", JsonConversionUtil.convertToJsonString(selectedStore));
        }
        catch (final UnknownIdentifierException ex) {
            // Do nothing
        }
        return ControllerConstants.Views.Fragments.Customer.LOCATION;
    }

    @RequestMapping(value = ControllerConstants.CUSTOMER_GET_INFO, method = RequestMethod.GET)
    public String getCustomerInfo(final Model model,
            @RequestParam(value = "productCode", required = false) final String productCode,
            final HttpServletRequest request, final HttpServletResponse response) {
        miniCartDataHelper.populateMiniCartData(model);

        if (StringUtils.isNotBlank(productCode)) {
            try {
                final ProductData productData = getTargetProductFacade().getProductForCodeAndOptionsWithDealMessage(
                        productCode,
                        Arrays.asList(ProductOption.PROMOTIONS));
                model.addAttribute("product", productData);
            }
            //CHECKSTYLE:OFF allow empty block
            catch (final UnknownIdentifierException ex) {
                // Do nothing
            }
            //CHECKSTYLE:ON
        }

        if (targetFeatureSwitchFacade.isConsolidatedStoreStockAvailable()) {
            final String selectedStoreNumber = cookieRetrievalHelper.getCookieValue(request, "t_psn");
            if (StringUtils.isEmpty(selectedStoreNumber)) {
                return ControllerConstants.Views.Fragments.Customer.GETINFO;
            }

            try {
                final TargetPointOfServiceData selectedStore = targetStoreLocatorFacade.getPointOfService(Integer
                        .valueOf(selectedStoreNumber));
                //if the store is closed its operations then don't set the data in model
                if (null == selectedStore || BooleanUtils.isTrue(selectedStore.getClosed())) {
                    preferredStoreCookieGenerator.removeCookie(response);
                    return ControllerConstants.Views.Fragments.Customer.GETINFO;
                }
                model.addAttribute("store", selectedStore);

            }
            //CHECKSTYLE:OFF allow empty block
            catch (final NumberFormatException ex) {
                //Do nothing
            }
        }

        return ControllerConstants.Views.Fragments.Customer.GETINFO;
    }

    protected TargetProductFacade getTargetProductFacade() {
        return productFacade;
    }

    private void populateEnvironmentData(final Model model) {
        final EnvironmentData environmentData = new EnvironmentData();
        environmentData.setFqdn(fqdn);
        environmentData.setgMapsApiKey(gMapsApiKey);
        if (SalesApplication.KIOSK.equals(salesApplicationFacade.getCurrentSalesApplication())) {
            environmentData.setIsKioskMode(true);
        }
        model.addAttribute("environmentData", JsonConversionUtil.convertToJsonString(environmentData));
    }

}
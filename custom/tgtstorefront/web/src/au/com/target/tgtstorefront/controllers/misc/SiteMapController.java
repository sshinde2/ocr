package au.com.target.tgtstorefront.controllers.misc;

import de.hybris.platform.cms2.enums.CmsApprovalStatus;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.configuration.ConversionException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.endeca.navigation.ENEQueryException;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtfacades.category.TargetCategoryFacade;
import au.com.target.tgtfacades.category.data.TargetCategoryData;
import au.com.target.tgtfacades.category.exceptions.NotAPrimarySuperCategoryException;
import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.product.data.TargetProductCountDepartmentData;
import au.com.target.tgtfacades.sitemap.TargetSiteMapFacade;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtstorefront.controllers.AbstractController;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.data.sitemap.SiteMapData;
import au.com.target.tgtstorefront.data.sitemap.SiteMapIndexData;
import au.com.target.tgtstorefront.data.sitemap.SiteMapIndexElementData;
import au.com.target.tgtstorefront.data.sitemap.SiteMapUrlData;
import au.com.target.tgtstorefront.data.sitemap.SiteMapUrlData.ChangeFrequency;


/**
 * Builds document that for Site-map protocol. For more details, please visit http://www.sitemaps.org/protocol.html.
 */
@Controller
public class SiteMapController extends AbstractController {

    protected static final Logger LOG = Logger.getLogger(SiteMapController.class);
    private static final String CONTENT_SITEMAP_RELATIVE_PATH = "content-sitemap.xml";
    private static final String CATEGORY_SITEMAP_RELATIVE_PATH = "category-sitemap.xml";
    private static final String STORE_SITEMAP_RELATIVE_PATH = "stores-sitemap.xml";
    private static final String SITE_MAP_SUFFIX = "/sitemap.xml";
    private static final String MODAL_PAGE_TEMPLATE_UID = "ModalPageTemplate";
    private static final String FORWARD_SLASH = "/";
    private static final String FORWARD_SLASH_TILDA = "/~";
    private static final String HOST_FDQN_KEY = "tgtstorefront.host.fqdn";

    private static final String CONTENT_SITEMAP_CHANGE_FREQUENCY = "tgtstorefront.sitemap.contentPage.changeFreq";

    private static final String CONTENT_SITEMAP_PRIORITY = "tgtstorefront.sitemap.contentPage.priority";

    private static final String CATEGORY_SITEMAP_CHANGE_FREQUENCY = "tgtstorefront.sitemap.categoryPage.changeFreq";
    private static final String CATEGORY_SITEMAP_PRIORITY = "tgtstorefront.sitemap.categoryPage.priority";

    private static final String CATEGORY_TOPLEVEL_SITEMAP_CHANGE_FREQUENCY = "tgtstorefront.sitemap.categoryPage.toplevel.changeFreq";
    private static final String CATEGORY_TOPLEVEL_SITEMAP_PRIORITY = "tgtstorefront.sitemap.categoryPage.toplevel.priority";

    private static final String HOMEPAGE_SITEMAP_CHANGE_FREQUENCY = "tgtstorefront.sitemap.homePage.changeFreq";
    private static final String HOMEPAGE_SITEMAP_PRIORITY = "tgtstorefront.sitemap.homePage.priority";

    private static final String PRODUCT_SITEMAP_CHANGE_FREQUENCY = "tgtstorefront.sitemap.productPage.changeFreq";
    private static final String PRODUCT_SITEMAP_PRIORITY = "tgtstorefront.sitemap.productPage.priority";

    private static final String STORE_SITEMAP_CHANGE_FREQUENCY = "tgtstorefront.sitemap.storePage.changeFreq";
    private static final String STORE_SITEMAP_PRIORITY = "tgtstorefront.sitemap.storePage.priority";
    private static final String PRODUCT_SITEMAP_URL_PRIFIX = "psm";
    private static final String PRODUCT_SITEMAP_PAGESIZE = "tgtstorefront.sitemap.productPage.pageSize";

    @Resource(name = "targetCategoryFacade")
    private TargetCategoryFacade categoryFacade;

    @Resource(name = "targetProductFacade")
    private TargetProductFacade productFacade;

    @Resource(name = "targetStoreLocatorFacade")
    private TargetStoreLocatorFacade storeFacade;

    @Resource(name = "targetSiteMapFacade")
    private TargetSiteMapFacade targetSiteMapFacade;

    @Resource(name = "cmsPageService")
    private CMSPageService cmsPageService;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Value("#{sitemapAllowedContentPages}")
    private List<String> allowedContentPages;

    @Resource(name = "targetFeatureSwitchService")
    private TargetFeatureSwitchService targetFeatureSwitchService;

    /**
     * Returns the site map index document converted to the XML format.
     * 
     * @return the site map index document
     */
    @ResponseBody
    @RequestMapping("/sitemap-index.xml")
    public SiteMapIndexData getSiteMapIndex() {
        final SiteMapIndexData siteMapIndex = new SiteMapIndexData();
        final ImmutableList.Builder<SiteMapIndexElementData> elements = new ImmutableList.Builder<>();
        elements.add(new SiteMapIndexElementData(
                buildAbsoluteUrl(CONTENT_SITEMAP_RELATIVE_PATH), null));
        // TODO list the latest modified date among content pages
        elements.add(new SiteMapIndexElementData(
                buildAbsoluteUrl(CATEGORY_SITEMAP_RELATIVE_PATH), null));

        elements.add(new SiteMapIndexElementData(
                buildAbsoluteUrl(STORE_SITEMAP_RELATIVE_PATH), null));
        // TODO list the latest modified date among categories
        int pageSize = 0;
        try {
            pageSize = configurationService.getConfiguration().getInt(PRODUCT_SITEMAP_PAGESIZE);
        }
        catch (final ConversionException e) {
            LOG.error("product sitemap page size should be a int value", e);
        }

        if (pageSize > 0) {
            for (final TargetProductCountDepartmentData data : productFacade
                    .getProductPageCountForDepartment(pageSize)) {
                for (int i = 0; i < data.getPageCount(); i++) {
                    final StringBuilder urlBuilder = new StringBuilder();
                    urlBuilder.append(PRODUCT_SITEMAP_URL_PRIFIX).append(FORWARD_SLASH)
                            .append(data.getTargetMerchDepartmentCode()).append(FORWARD_SLASH)
                            .append(String.valueOf(i)).append(SITE_MAP_SUFFIX);
                    elements.add(new SiteMapIndexElementData(buildAbsoluteUrl(urlBuilder.toString()), null));
                }
            }
        }

        siteMapIndex.setElements(elements.build());
        return siteMapIndex;
    }

    /**
     * Returns a site map for "online" categories.
     * 
     * @return category site map
     */
    @ResponseBody
    @RequestMapping("/category-sitemap.xml")
    public SiteMapData getCategorySiteMap() {
        final SiteMapData siteMap = new SiteMapData();
        final ImmutableList.Builder<SiteMapUrlData> elements = new ImmutableList.Builder<>();

        for (final TargetCategoryData categoryData : getCategoryData()) {
            if (categoryFacade.isRootCategory(categoryData.getCode())) {
                // All normal categories are a subcategory of AP01, so they should have a supercategory
                continue;
            }

            String changeFrequency = CATEGORY_SITEMAP_CHANGE_FREQUENCY;
            String priority = CATEGORY_SITEMAP_PRIORITY;
            if (categoryFacade.isTopCategory(categoryData.getCode())) {
                changeFrequency = CATEGORY_TOPLEVEL_SITEMAP_CHANGE_FREQUENCY;
                priority = CATEGORY_TOPLEVEL_SITEMAP_PRIORITY;
            }

            elements.add(new SiteMapUrlData(buildAbsoluteUrl(categoryData.getUrl()),
                    null, getChangeFrequencyByCode(configurationService.getConfiguration().getString(
                            changeFrequency)),
                    configurationService.getConfiguration().getBigDecimal(
                            priority)));
            // TODO list correct "last modified" timestamp for category
        }
        siteMap.setUrls(elements.build());
        return siteMap;
    }

    /**
     * @return
     */
    private List<TargetCategoryData> getCategoryData() {

        if (targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.CATEGORY_SITEMAP_OPTIMIZATION)) {
            return categoryFacade.getCategoriesWithProducts();
        }
        else {
            return categoryFacade.getAllCategoryData();
        }
    }

    /**
     * Returns a site map for "online" content pages.
     * 
     * @return site map for content pages
     */
    @ResponseBody
    @RequestMapping("/content-sitemap.xml")
    public SiteMapData getContentSiteMap() {
        final SiteMapData siteMap = new SiteMapData();
        final ImmutableList.Builder<SiteMapUrlData> elements = new ImmutableList.Builder<>();
        for (final ContentPageModel contentPage : getContentPagesForSiteMap()) {
            if (contentPage.isHomepage()) {
                final String homepageUrl = buildAbsoluteUrl(contentPage.getLabel());
                elements.add(new SiteMapUrlData(homepageUrl.substring(0, homepageUrl.lastIndexOf(FORWARD_SLASH) + 1),
                        null, getChangeFrequencyByCode(configurationService.getConfiguration().getString(
                                HOMEPAGE_SITEMAP_CHANGE_FREQUENCY)),
                        configurationService.getConfiguration()
                                .getBigDecimal(HOMEPAGE_SITEMAP_PRIORITY)));
            }
            else {
                elements.add(new SiteMapUrlData(buildAbsoluteUrl(contentPage.getLabel()),
                        null, getChangeFrequencyByCode(configurationService.getConfiguration().getString(
                                CONTENT_SITEMAP_CHANGE_FREQUENCY)),
                        configurationService.getConfiguration()
                                .getBigDecimal(CONTENT_SITEMAP_PRIORITY)));
            }
            // TODO list correct "last modified" timestamp for content page
        }
        siteMap.setUrls(elements.build());
        return siteMap;
    }

    @ResponseBody
    @RequestMapping("/psm/{merchCode}/{pageNumber}/sitemap.xml")
    public SiteMapData getMerchDeptSiteMap(@PathVariable("merchCode") final String merchCodeStr,
            @PathVariable("pageNumber") final String pageNumberStr) throws TargetEndecaException, ENEQueryException {
        final SiteMapData siteMap = new SiteMapData();
        int pageInt = 0;
        int merchCode = 0;
        final ImmutableList.Builder<SiteMapUrlData> elements = new ImmutableList.Builder<>();
        final int count = configurationService.getConfiguration().getInt(PRODUCT_SITEMAP_PAGESIZE);
        try {
            pageInt = Integer.parseInt(pageNumberStr.trim());
            merchCode = Integer.parseInt(merchCodeStr.trim());
            if ((pageInt < 0) || (merchCode < 0)) {
                throw new HttpNotFoundException();
            }

            final List<String> productsUrl = targetSiteMapFacade.getSiteMapColourVariantUrl(merchCode, pageInt, count);
            if (CollectionUtils.isNotEmpty(productsUrl)) {
                for (final String url : productsUrl) {
                    elements.add(new SiteMapUrlData(buildAbsoluteUrl(url),
                            null, getChangeFrequencyByCode(configurationService.getConfiguration().getString(
                                    PRODUCT_SITEMAP_CHANGE_FREQUENCY)),
                            configurationService.getConfiguration()
                                    .getBigDecimal(
                                            PRODUCT_SITEMAP_PRIORITY)));
                }
            }
        }
        catch (final NumberFormatException ex) {
            LOG.warn("Error while Parsing  PageNumber/ MerchCode");
            throw new HttpNotFoundException();
        }
        siteMap.setUrls(elements.build());
        return siteMap;
    }

    /**
     * Returns a site map for stores
     * 
     * @return store site map
     */
    @ResponseBody
    @RequestMapping("/stores-sitemap.xml")
    public SiteMapData getStoreSiteMap() {
        final Map<String, List<TargetPointOfServiceData>> storeLocMap = storeFacade.getAllStateAndStores();
        final SiteMapData siteMap = new SiteMapData();
        final ImmutableList.Builder<SiteMapUrlData> elements = new ImmutableList.Builder<>();
        for (final Entry<String, List<TargetPointOfServiceData>> entry : storeLocMap.entrySet()) {
            final List<TargetPointOfServiceData> targetPointOfServiceDataList = entry.getValue();
            if (CollectionUtils.isNotEmpty(targetPointOfServiceDataList)) {
                for (final TargetPointOfServiceData targetPointOfServiceData : targetPointOfServiceDataList) {
                    elements.add(new SiteMapUrlData(buildAbsoluteUrl(targetPointOfServiceData.getUrl()),
                            null, getChangeFrequencyByCode(configurationService.getConfiguration().getString(
                                    STORE_SITEMAP_CHANGE_FREQUENCY)),
                            configurationService.getConfiguration()
                                    .getBigDecimal(
                                            STORE_SITEMAP_PRIORITY)));
                }
            }
        }
        siteMap.setUrls(elements.build());
        return siteMap;
    }

    /**
     * Returns 404 for a {@link NotAPrimarySuperCategoryException}.
     */
    @ExceptionHandler(NotAPrimarySuperCategoryException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleNotALeafCategoryException() {
        // do nothing, return 404
    }



    /**
     * Returns 404 for a HttpNotFoundException
     */
    @ExceptionHandler(HttpNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleMerchDeptException() {
        return ControllerConstants.Forward.ERROR_404;
    }

    /**
     * Builds an absolute URL from given {@code uri}, which might represent relative path.
     * 
     * @param uri
     *            the URI used to build and absolute resource locator
     * @return an absolute URL
     */
    private String buildAbsoluteUrl(final String uri) {
        String relativeUrl = StringUtils.defaultString(uri);
        if (UrlUtils.isAbsoluteUrl(relativeUrl)) {
            return relativeUrl;
        }
        relativeUrl = StringUtils.stripStart(relativeUrl, FORWARD_SLASH);
        String baseUrl = configurationService.getConfiguration().getString(HOST_FDQN_KEY);
        baseUrl = StringUtils.stripEnd(baseUrl, FORWARD_SLASH);
        return baseUrl + FORWARD_SLASH + relativeUrl;
    }

    /**
     * Returns content pages that are eligible for being reflected on the site map.
     * 
     * @return the list of content pages
     */
    private List<ContentPageModel> getContentPagesForSiteMap() {
        return ImmutableList.copyOf(Iterables.filter(
                cmsPageService.getAllContentPages(), new Predicate<ContentPageModel>() {
                    @Override
                    public boolean apply(final ContentPageModel input) {
                        return allowedContentPages.contains(input.getLabel())
                                || (CmsApprovalStatus.APPROVED.equals(input.getApprovalStatus())
                                        && !MODAL_PAGE_TEMPLATE_UID.equals(input.getMasterTemplate().getUid())
                                        && StringUtils.startsWith(input.getLabel(), FORWARD_SLASH)
                                        && !StringUtils.startsWith(input.getLabel(), FORWARD_SLASH_TILDA));
                    }
                }));
    }



    private ChangeFrequency getChangeFrequencyByCode(final String code) {
        ChangeFrequency frequency = null;

        if (StringUtils.isBlank(code)) {
            return null;
        }

        if (code.equalsIgnoreCase(ChangeFrequency.WEEKLY.toString())) {
            frequency = ChangeFrequency.WEEKLY;
        }
        else if (code.equalsIgnoreCase(ChangeFrequency.ALWAYS.toString())) {
            frequency = ChangeFrequency.ALWAYS;
        }
        else if (code.equalsIgnoreCase(ChangeFrequency.DAILY.toString())) {
            frequency = ChangeFrequency.DAILY;
        }
        else if (code.equalsIgnoreCase(ChangeFrequency.HOURLY.toString())) {
            frequency = ChangeFrequency.HOURLY;
        }
        else if (code.equalsIgnoreCase(ChangeFrequency.YEARLY.toString())) {
            frequency = ChangeFrequency.YEARLY;
        }
        else if (code.equalsIgnoreCase(ChangeFrequency.MONTHLY.toString())) {
            frequency = ChangeFrequency.MONTHLY;
        }
        else {
            frequency = ChangeFrequency.NEVER;
        }
        return frequency;
    }

}

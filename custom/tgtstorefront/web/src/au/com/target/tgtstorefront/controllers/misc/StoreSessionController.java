/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.misc;

import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.commerceservices.enums.UiExperienceLevel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import au.com.target.tgtstorefront.controllers.AbstractController;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;


/**
 * Controller for store session. Used to change the session language, currency and experience level.
 */
@Controller
public class StoreSessionController extends AbstractController
{
    private static final Logger LOG = Logger.getLogger(StoreSessionController.class);

    @Resource(name = "uiExperienceService")
    private UiExperienceService uiExperienceService;

    @Resource(name = "enumerationService")
    private EnumerationService enumerationService;

    public String selectUiExperienceLevel(@RequestParam("level") final String uiExperienceLevelString,
            final HttpServletRequest request)
    {
        if (uiExperienceLevelString == null || uiExperienceLevelString.isEmpty())
        {
            // Empty value - clear the override
            uiExperienceService.setOverrideUiExperienceLevel(null);
        }
        else
        {
            final UiExperienceLevel uiExperienceLevel = toUiExperienceLevel(uiExperienceLevelString);
            if (uiExperienceLevel == null)
            {
                LOG.warn("Unknown UiExperience level [" + uiExperienceLevelString + "] available values are: "
                        + Arrays.toString(getAvailableUiExperienceLevelsCodes()));
            }
            else
            {
                uiExperienceService.setOverrideUiExperienceLevel(uiExperienceLevel);
            }
        }

        // Always clear the prompt hide flag
        setHideUiExperienceLevelOverridePrompt(request, false);
        return getReturnRedirectUrl(request);
    }

    protected UiExperienceLevel toUiExperienceLevel(final String code)
    {
        if (code != null && !code.isEmpty())
        {
            try
            {
                return enumerationService.getEnumerationValue(UiExperienceLevel.class, code);
            }
            catch (final UnknownIdentifierException ignore)
            {
                // Ignore, return null
                return null;
            }
        }
        return null;
    }

    protected List<UiExperienceLevel> getAvailableUiExperienceLevels()
    {
        return enumerationService.getEnumerationValues(UiExperienceLevel.class);
    }

    protected String[] getAvailableUiExperienceLevelsCodes()
    {
        final List<UiExperienceLevel> availableUiExperienceLevels = getAvailableUiExperienceLevels();
        if (availableUiExperienceLevels == null || availableUiExperienceLevels.isEmpty())
        {
            return new String[0];
        }

        final String[] codes = new String[availableUiExperienceLevels.size()];
        for (int i = 0; i < codes.length; i++)
        {
            codes[i] = availableUiExperienceLevels.get(i).getCode();
        }

        return codes;
    }

    public String selectUiExperienceLevelPrompt(@RequestParam("hide") final boolean hideFlag,
            final HttpServletRequest request)
    {
        setHideUiExperienceLevelOverridePrompt(request, hideFlag);
        return getReturnRedirectUrl(request);
    }

    protected void setHideUiExperienceLevelOverridePrompt(final HttpServletRequest request, final boolean flag)
    {
        request.getSession().setAttribute("hideUiExperienceLevelOverridePrompt", Boolean.valueOf(flag));
    }

    protected String getReturnRedirectUrl(final HttpServletRequest request)
    {
        final String referer = request.getHeader("Referer");
        if (referer != null && !referer.isEmpty())
        {
            return UrlBasedViewResolver.REDIRECT_URL_PREFIX + referer;
        }
        return ControllerConstants.Redirection.HOME;
    }

    @ExceptionHandler(UnknownIdentifierException.class)
    public String handleUnknownIdentifierException(final UnknownIdentifierException exception,
            final HttpServletRequest request)
    {
        final Map<String, Object> currentFlashScope = RequestContextUtils.getOutputFlashMap(request);
        GlobalMessages.addErrorMessage(currentFlashScope, exception.getMessage());
        return ControllerConstants.Redirection.ERROR_404;
    }
}

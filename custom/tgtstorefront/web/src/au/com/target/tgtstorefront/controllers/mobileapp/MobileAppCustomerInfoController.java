package au.com.target.tgtstorefront.controllers.mobileapp;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.user.data.TargetCustomerInfoData;
import au.com.target.tgtstorefront.controllers.AbstractController;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


@Controller
@RequestMapping(ControllerConstants.MOBILE_CUSTOMER_INFO)
public class MobileAppCustomerInfoController extends AbstractController {

    @Resource(name = "targetCustomerFacade")
    private TargetCustomerFacade targetCustomerFacade;


    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public TargetCustomerInfoData getCustomerInfo() {
        final TargetCustomerInfoData targetCustomerInfoData = targetCustomerFacade.getCustomerInfo();
        return targetCustomerInfoData;
    }

}
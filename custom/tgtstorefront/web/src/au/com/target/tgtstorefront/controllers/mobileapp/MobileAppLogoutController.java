/**
 * 
 */
package au.com.target.tgtstorefront.controllers.mobileapp;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtstorefront.controllers.AbstractController;
import au.com.target.tgtstorefront.security.TargetRememberMeServices;


/**
 * @author rmcalave
 *
 */
@Controller
@RequestMapping("/~/mobile-app/logout")
public class MobileAppLogoutController extends AbstractController {

    @Resource(name = "securityContextLogoutHandler")
    private SecurityContextLogoutHandler securityContextLogoutHandler;

    @Resource(name = "rememberMeServices")
    private TargetRememberMeServices targetRememberMeServices;

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET)
    public Response logout(final HttpServletRequest request, final HttpServletResponse response) {
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        securityContextLogoutHandler.logout(request, response, auth);
        targetRememberMeServices.logout(request, response, auth);

        return new Response(true);
    }

}

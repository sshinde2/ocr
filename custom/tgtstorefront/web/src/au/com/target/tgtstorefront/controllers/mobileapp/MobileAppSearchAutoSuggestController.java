package au.com.target.tgtstorefront.controllers.mobileapp;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.endeca.infront.assembler.ContentItem;

import au.com.target.endeca.infront.assemble.impl.EndecaAutoSuggestAssemble;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.transform.TargetEndecaURLHelper;
import au.com.target.tgtstorefront.controllers.AbstractController;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.util.SearchQuerySanitiser;


@Controller
@RequestMapping(ControllerConstants.MOBILE_APP_SEARCH)
public class MobileAppSearchAutoSuggestController extends AbstractController {
    private static final Logger LOG = Logger.getLogger(MobileAppSearchAutoSuggestController.class);

    private static final int DEFAULT_MAX_SEARCH_TEXT_LENGTH = 50;

    private Integer maxSearchTextLength;

    @Resource(name = "searchQuerySanitiser")
    private SearchQuerySanitiser searchQuerySanitiser;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Resource(name = "targetEndecaURLHelper")
    private TargetEndecaURLHelper targetEndecaURLHelper;

    @Resource(name = "endecaAutoSuggestAssemble")
    private EndecaAutoSuggestAssemble endecaAutoSuggestAssemble;


    @RequestMapping(value = ControllerConstants.MOBILE_APP_AUTO_SUGGESTION, method = RequestMethod.GET)
    @ResponseBody
    public ContentItem getAutocompleteSuggestions(@RequestParam("term") final String term,
            final HttpServletRequest request, final HttpServletResponse response) {
        String safeSearchText = term;
        ContentItem autoSuggestSearchResult = null;
        if (safeSearchText.length() > getMaxSearchTextLength().intValue()) {
            safeSearchText = safeSearchText.substring(0, getMaxSearchTextLength().intValue());
        }
        final String cleanSearchText = searchQuerySanitiser.sanitiseSearchText(safeSearchText);
        if (StringUtils.isNotBlank(cleanSearchText)) {
            final List<String> clearSearchTexts = new ArrayList<>();
            clearSearchTexts.add(cleanSearchText);
            final EndecaSearchStateData searchStateData = targetEndecaURLHelper.buildEndecaSearchStateData(
                    null, clearSearchTexts, null, 0, 0, null, null, null, null);

            try {
                autoSuggestSearchResult = endecaAutoSuggestAssemble.assemble(request, searchStateData);
            }
            catch (final Exception e) {
                LOG.error("TGT-MOBILE-APP-AUTOSUGGEST-ERROR: Exception While doing an autosuggest Search." + e);
                response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
            }

        }
        else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
        return autoSuggestSearchResult;
    }

    private Integer getMaxSearchTextLength() {
        if (maxSearchTextLength == null) {
            maxSearchTextLength = configurationService.getConfiguration().getInteger(
                    "storefront.search.maxSearchTextLength", new Integer(DEFAULT_MAX_SEARCH_TEXT_LENGTH));
        }
        return maxSearchTextLength;
    }

    /**
     * @param searchQuerySanitiser
     *            the searchQuerySanitiser to set
     */
    public void setSearchQuerySanitiser(final SearchQuerySanitiser searchQuerySanitiser) {
        this.searchQuerySanitiser = searchQuerySanitiser;
    }
}
/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.TitleData;

import java.util.Collection;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

import au.com.target.tgtfacades.customer.TargetCustomerSubscriptionFacade;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionResponseDto;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionResponseType;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionSource;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionType;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.forms.ENewsSubscriptionForm;
import au.com.target.tgtstorefront.security.cookie.EnhancedCookieGenerator;


/**
 * Base controller for all page controllers. Provides common functionality for all page controllers.
 */
public abstract class AbstractCustomerSubscriptionController extends AbstractPageController
{

    protected static final Logger LOG = Logger.getLogger(AbstractCustomerSubscriptionController.class);

    @Resource(name = "targetCustomerSubscriptionFacade")
    private TargetCustomerSubscriptionFacade targetCustomerSubscriptionFacade;

    @Resource(name = "userFacade")
    private UserFacade userFacade;

    @Resource(name = "enewsCookieGenerator")
    private EnhancedCookieGenerator enewsCookieGenerator;

    @ModelAttribute("titles")
    public Collection<TitleData> getTitles() {
        return userFacade.getTitles();
    }

    /**
     * 
     * @param eNewsSubscriptionForm
     * @return TargetCustomerSubscriptionResponseType
     */
    protected TargetCustomerSubscriptionResponseType processCustomerSubscription(
            final ENewsSubscriptionForm eNewsSubscriptionForm, final TargetCustomerSubscriptionType subscriptionType) {
        TargetCustomerSubscriptionResponseType responseType = null;
        try {
            final TargetCustomerSubscriptionResponseDto responseDto = targetCustomerSubscriptionFacade
                    .performCustomerSubscription(getSubscriptionData(eNewsSubscriptionForm,
                            subscriptionType));
            if (null != responseDto) {
                responseType = responseDto.getResponseType();
            }

        }
        catch (final Exception e)
        {
            LOG.warn(subscriptionType.getValue() + " subscription failed", e);
        }
        return responseType;

    }

    /**
     * 
     * @param eNewsSubscriptionForm
     * @param httpServletResponse
     * @return boolean
     */
    protected boolean processSubscribe(final ENewsSubscriptionForm eNewsSubscriptionForm,
            final HttpServletResponse httpServletResponse)
    {
        boolean success = false;
        try {
            targetCustomerSubscriptionFacade
                    .performCustomerSubscription(getSubscriptionData(eNewsSubscriptionForm,
                            TargetCustomerSubscriptionType.NEWSLETTER));
            success = true;

            enewsCookieGenerator.addCookie(httpServletResponse, Boolean.TRUE.toString());
        }
        catch (final Exception e)
        {
            LOG.warn("Newsletter subscription failed", e);
        }
        return success;
    }

    /**
     * Method for converting the subscription form to subscription details for it to be processed further.
     * 
     * @param eNewsSubscriptionForm
     * @return subscriptionDetails
     */
    protected TargetCustomerSubscriptionRequestDto getSubscriptionData(
            final ENewsSubscriptionForm eNewsSubscriptionForm, final TargetCustomerSubscriptionType subscriptionType) {

        String subScriptionSourceStr = eNewsSubscriptionForm.getSubscriptionSource();
        final TargetCustomerSubscriptionRequestDto subscriptionDetails = new TargetCustomerSubscriptionRequestDto();
        if (null == subScriptionSourceStr) {
            subScriptionSourceStr = StringUtils.EMPTY;
        }
        switch (subScriptionSourceStr) {

            case ControllerConstants.ACCREDIT_CHECKOUT:
            {
                subscriptionDetails.setCustomerSubscriptionSource(TargetCustomerSubscriptionSource.WEBCHECKOUT);
                break;
            }
            case ControllerConstants.ACCREDIT_WIFI:
            {
                subscriptionDetails.setCustomerSubscriptionSource(TargetCustomerSubscriptionSource.WIFI);
                break;
            }
            case ControllerConstants.ACCREDIT_NEWSLETTER:
            {
                subscriptionDetails.setCustomerSubscriptionSource(TargetCustomerSubscriptionSource.NEWSLETTER);
                break;
            }
            case ControllerConstants.ACCREDIT_MUMSHUB:
            {
                subscriptionDetails.setCustomerSubscriptionSource(TargetCustomerSubscriptionSource.MUMSHUB);
                break;
            }
            default:
                break;
        }
        subscriptionDetails.setCustomerEmail(eNewsSubscriptionForm.getEmail());
        subscriptionDetails.setTitle(eNewsSubscriptionForm.getTitle());
        subscriptionDetails.setFirstName(eNewsSubscriptionForm.getFirstName());
        subscriptionDetails.setLastName(eNewsSubscriptionForm.getLastName());
        subscriptionDetails.setSubscriptionType(subscriptionType);

        return subscriptionDetails;
    }

    protected AbstractPageModel getCmsPage(final String siteCmsPageLabel) throws CMSItemNotFoundException
    {
        return getContentPageForLabelOrId(siteCmsPageLabel);
    }

    protected void setJsonResponseMsg(final Model model, final WebConstants.ResponseMessageType messageType,
            final TargetCustomerSubscriptionType subscriptionType,
            final String msg) {
        model.addAttribute("subscriptionType", subscriptionType.getValue());
        model.addAttribute("messageType", messageType.getValue());
        model.addAttribute("message", StringEscapeUtils.escapeJavaScript(msg));
    }

    protected String getSuccessfulSubscriptionMsg() {
        return getMessageFromProperties("enews.subscription.confirmation.message.title");
    }

}

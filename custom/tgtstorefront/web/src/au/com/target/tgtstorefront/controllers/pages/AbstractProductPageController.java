/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.product.ProductService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.ui.Model;

import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtstorefront.controllers.util.ProductDataHelper;


/**
 * Base controller for sharing some methods for populating product details in the model.
 * 
 */
public class AbstractProductPageController extends AbstractPageController {


    @Resource(name = "productFacade")
    private TargetProductFacade productFacade;

    @Resource(name = "productService")
    private ProductService productService;

    protected TargetProductFacade getTargetProductFacade() {
        return productFacade;
    }

    protected ProductService getProductService() {
        return productService;
    }


    /**
     * Populate product data and gallery images in the model
     * 
     * @param productData
     * @param model
     * @param request
     */
    protected void populateProductData(final ProductData productData, final Model model,
            final HttpServletRequest request)
    {
        ProductDataHelper.setCurrentProduct(request, productData.getCode());
        model.addAttribute("galleryImages", getGalleryImages(productData));
        model.addAttribute("product", productData);
    }


    /**
     * Construct gallery images from product data
     * 
     * @param productData
     * @return list of image data
     */
    protected List<Map<String, ImageData>> getGalleryImages(final ProductData productData)
    {
        final List<Map<String, ImageData>> galleryImages = new ArrayList<Map<String, ImageData>>();
        if (CollectionUtils.isNotEmpty(productData.getImages()))
        {
            final List<ImageData> images = new ArrayList<>();
            for (final ImageData image : productData.getImages())
            {
                if (ImageDataType.GALLERY.equals(image.getImageType()))
                {
                    images.add(image);
                }
            }
            Collections.sort(images, new Comparator<ImageData>()
            {
                @Override
                public int compare(final ImageData image1, final ImageData image2)
                {
                    return image1.getGalleryIndex().compareTo(image2.getGalleryIndex());
                }
            });

            if (CollectionUtils.isNotEmpty(images))
            {
                int currentIndex = images.get(0).getGalleryIndex().intValue();
                Map<String, ImageData> formats = new HashMap<String, ImageData>();
                for (final ImageData image : images)
                {
                    if (currentIndex != image.getGalleryIndex().intValue())
                    {
                        galleryImages.add(formats);
                        formats = new HashMap<String, ImageData>();
                        currentIndex = image.getGalleryIndex().intValue();
                    }
                    formats.put(image.getFormat(), image);
                }
                if (!formats.isEmpty())
                {
                    galleryImages.add(formats);
                }
            }
        }
        return galleryImages;
    }



}

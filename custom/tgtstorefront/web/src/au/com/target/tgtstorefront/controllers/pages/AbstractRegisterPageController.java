/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;

import java.util.Collection;
import java.util.Collections;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.support.RequestContextUtils;

import au.com.target.tgtfacades.customer.TargetCustomerSubscriptionFacade;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.user.data.TargetRegisterData;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionSource;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.forms.GuestForm;
import au.com.target.tgtstorefront.forms.LoginForm;
import au.com.target.tgtstorefront.forms.RegisterForm;
import au.com.target.tgtstorefront.security.AutoLoginStrategy;


public abstract class AbstractRegisterPageController extends AbstractPageController {
    private static final Logger LOG = Logger.getLogger(AbstractRegisterPageController.class);

    @Resource(name = "autoLoginStrategy")
    private AutoLoginStrategy autoLoginStrategy;

    @Resource(name = "userFacade")
    private UserFacade userFacade;

    @Resource(name = "targetCustomerSubscriptionFacade")
    private TargetCustomerSubscriptionFacade targetCustomerSubscriptionFacade;

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    protected abstract AbstractPageModel getCmsPage() throws CMSItemNotFoundException;

    protected abstract String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response);

    protected abstract String getView();


    /**
     * @return the autoLoginStrategy
     */
    protected AutoLoginStrategy getAutoLoginStrategy() {
        return autoLoginStrategy;
    }

    /**
     * @return the userFacade
     */
    protected UserFacade getUserFacade() {
        return userFacade;
    }

    @ModelAttribute("titles")
    public Collection<TitleData> getTitles() {
        return userFacade.getTitles();
    }

    protected String getDefaultRegistrationPage(final Model model) throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getCmsPage());
        setUpMetaDataForContentPage(model, (ContentPageModel)getCmsPage());
        final Breadcrumb loginBreadcrumbEntry = new Breadcrumb("#", getMessageSource().getMessage("header.link.login",
                null,
                getI18nService().getCurrentLocale()), null);
        model.addAttribute("breadcrumbs", Collections.singletonList(loginBreadcrumbEntry));
        model.addAttribute(new RegisterForm());
        return getView();
    }

    /**
     * This method takes data from the registration form and create a new customer account and attempts to log in using
     * the credentials of this new user.
     * 
     * @param referer
     * @param form
     * @param bindingResult
     * @param model
     * @param request
     * @param response
     * @return true if there are no binding errors or the account does not already exists.
     * @throws CMSItemNotFoundException
     */
    protected String processRegisterUserRequest(final String referer, final RegisterForm form,
            final BindingResult bindingResult,
            final Model model, final HttpServletRequest request, final HttpServletResponse response)
            throws CMSItemNotFoundException {
        if (bindingResult.hasErrors()) {
            model.addAttribute(new LoginForm());
            model.addAttribute(new GuestForm());
            GlobalMessages.addErrorMessage(model, "form.global.error");
            return handleRegistrationError(model);
        }

        final TargetRegisterData data = new TargetRegisterData();
        data.setFirstName(form.getFirstName());
        data.setLastName(form.getLastName());
        data.setLogin(form.getEmail());
        data.setPassword(form.getPwd());
        data.setTitleCode(form.getTitleCode());
        data.setMobileNumber(form.getMobileNumber());
        try {
            getCustomerFacade().register(data);
            getAutoLoginStrategy().login(form.getEmail(), form.getPwd(), request, response);
            GlobalMessages.addConfMessage(RequestContextUtils.getOutputFlashMap(request),
                    "registration.confirmation.message.title");
        }
        catch (final DuplicateUidException e) {
            LOG.warn("registration failed: " + e);
            model.addAttribute(new LoginForm());
            model.addAttribute(new GuestForm());
            bindingResult.rejectValue("email", "registration.error.account.exists.title");
            GlobalMessages.addErrorMessage(model, "form.global.error");
            return handleRegistrationError(model);
        }

        try {
            performCustomerSubscription(form);
        }
        catch (final Exception e) {
            LOG.warn("subscription failed", e);
        }

        return getSuccessRedirect(request, response);
    }

    /**
     * Method to subscribe customer to marketing emails.
     * 
     * @param form
     */
    private void performCustomerSubscription(final RegisterForm form) {
        final TargetCustomerSubscriptionRequestDto subscriptionDetails = new TargetCustomerSubscriptionRequestDto();
        subscriptionDetails.setCustomerEmail(form.getEmail());
        subscriptionDetails.setTitle(form.getTitleCode());
        subscriptionDetails.setFirstName(form.getFirstName());
        subscriptionDetails.setLastName(form.getLastName());
        if ((Boolean.TRUE).equals(form.getRegisterForMail())) {
            subscriptionDetails.setRegisterForEmail(true);
        }
        else {
            subscriptionDetails.setRegisterForEmail(false);
        }
        if (ControllerConstants.ACCREDIT_CHECKOUT.equals(form.getSubscriptionSource())) {
            subscriptionDetails.setCustomerSubscriptionSource(TargetCustomerSubscriptionSource.WEBCHECKOUT);
        }
        else {
            subscriptionDetails.setCustomerSubscriptionSource(TargetCustomerSubscriptionSource.WEBACCOUNT);
        }
        targetCustomerSubscriptionFacade.performCustomerSubscription(subscriptionDetails);
    }

    private String handleRegistrationError(final Model model) throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getCmsPage());
        setUpMetaDataForContentPage(model, (ContentPageModel)getCmsPage());
        return getView();
    }

}

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.pages;


import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.dto.DeliveryModePostcodeDto;
import au.com.target.endeca.infront.exception.TargetEndecaWrapperException;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;
import au.com.target.tgtcore.util.ProductUtil;
import au.com.target.tgtfacades.config.TargetSharedConfigFacade;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.flybuys.FlybuysDiscountFacade;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedeemConfigData;
import au.com.target.tgtfacades.giftcards.exceptions.GiftCardValidationException;
import au.com.target.tgtfacades.order.TargetCartFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.payment.AfterpayConfigFacade;
import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtstorefront.breadcrumb.ResourceBreadcrumbBuilder;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.CartDataHelper;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.controllers.util.ProductDataHelper;
import au.com.target.tgtstorefront.controllers.util.RequestHeadersHelper;
import au.com.target.tgtstorefront.forms.UpdateGiftCardForm;
import au.com.target.tgtstorefront.forms.UpdateQuantityForm;
import au.com.target.tgtstorefront.forms.checkout.DeliveryModePostcodeForm;
import au.com.target.tgtstorefront.giftcards.converters.GiftRecipientFormToDTOConverter;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;
import au.com.target.tgtutility.util.TargetValidationCommon;
import au.com.target.tgtutility.util.TargetValidationUtils;
import au.com.target.tgtwebcore.constants.TgtwebcoreConstants;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;
import au.com.target.tgtwebcore.servicelayer.services.TargetCMSPageService;


/**
 * Controller for cart page
 */
@Controller
@RequestMapping(value = ControllerConstants.CART)
public class CartPageController extends AbstractPageController {
    public static final String SHOW_CHECKOUT_STRATEGY_OPTIONS = "storefront.show.checkout.flows";

    protected static final Logger LOG = Logger.getLogger(CartPageController.class);

    protected static final String CART_DATA = "cartData";

    private static final String CART_CMS_PAGE_LABEL = "cart";
    private static final String CONTINUE_URL = "continueUrl";
    private static final String ENTERING_POSTCODE_ON_CART = "cart.postcode.check";
    private static final String INVALID_POSTCODE_ERROR = "deliveryMode.postcode.invalid";
    private static final String ERROR_MSG = "errorMsg";

    private static final String REMOVE_ACTION = "remove";

    private static final String UPDATE_ACTION = "update";

    @Resource(name = "targetCartFacade")
    private TargetCartFacade targetCartFacade;

    @Resource(name = "targetPurchaseOptionHelper")
    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;

    @Resource(name = "requestHeadersHelper")
    private RequestHeadersHelper requestHeadersHelper;

    @Resource(name = "siteConfigService")
    private SiteConfigService siteConfigService;

    @Resource(name = "sessionService")
    private SessionService sessionService;

    @Resource(name = "simpleBreadcrumbBuilder")
    private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

    @Resource(name = "cmsComponentService")
    private CMSComponentService cmsComponentService;

    @Resource(name = "commonI18NService")
    private CommonI18NService commonI18NService;

    @Resource(name = "targetCheckoutFacade")
    private TargetCheckoutFacade targetCheckoutFacade;

    @Resource(name = "priceDataFactory")
    private PriceDataFactory priceDataFactory;

    @Resource(name = "targetCommerceCartService")
    private TargetCommerceCartService targetCommerceCartService;

    @Resource(name = "targetCartService")
    private TargetLaybyCartService targetCartService;

    @Resource(name = "flybuysDiscountFacade")
    private FlybuysDiscountFacade flybuysDiscountFacade;

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Resource(name = "giftRecipientFormToDTOConverter")
    private GiftRecipientFormToDTOConverter giftRecipientFormToDTOConverter;

    @Resource
    private AfterpayConfigFacade afterpayConfigFacade;

    @Resource(name = "cmsPageService")
    private TargetCMSPageService cmsPageService;

    @Resource(name = "targetProductFacade")
    private TargetProductFacade targetProductFacade;

    @Resource(name = "messageSource")
    private MessageSource messageSource;

    @Resource(name = "i18nService")
    private I18NService i18nService;

    // Public getter used in a test
    @Override
    public SiteConfigService getSiteConfigService() {
        return siteConfigService;
    }

    @ModelAttribute("showCheckoutStrategies")
    public boolean isCheckoutStrategyVisible() {
        final String property = getSiteConfigService().getProperty(SHOW_CHECKOUT_STRATEGY_OPTIONS);
        return !StringUtils.isBlank(property) && Boolean.parseBoolean(property);
    }

    /*
     * Display the cart page
     */
    @RequestMapping(method = RequestMethod.GET)
    public String showCart(@RequestParam(value = "performSoh", defaultValue = "true") final boolean performSoh,
            final Model model, final HttpServletRequest request, final HttpServletResponse response)
                    throws CMSItemNotFoundException {
        if (targetCartFacade.hasSessionCart()) {
            if (performSoh) {
                if (sohValidation(model)) {
                    reassessFlybuysDiscount(model);
                }
            }
            final CartData cartData = targetCartFacade.getSessionCart();
            populateCartList(model, cartData);
            final String postCode = targetCartFacade.getPostalCodeFromCartOrSession(cartData);
            prepareDataForPage(model, cartData,
                    commonI18NService.getLocaleForLanguage(commonI18NService.getCurrentLanguage()));
            model.addAttribute("deliveryPostcode", postCode);
            model.addAttribute("isDeliveryModeSelectionByPostcodeEnabled",
                    Boolean.valueOf(targetFeatureSwitchFacade.isFeatureEnabled(ENTERING_POSTCODE_ON_CART)));
            final DeliveryModePostcodeForm postCodeDto = new DeliveryModePostcodeForm();
            postCodeDto.setPostcode(postCode);
            model.addAttribute("deliveryModePostcodeForm", postCodeDto);

            populateCartProductsRecommendations(request);
            populateAfterPayConfig(model);
            populateAfterPayModal(model);
            model.addAttribute("zipPaymentThresholdAmount", getZipPaymentThresholdAmount() );
            populateZipPaymentModal(model);
            // Remove missing products (eg ones that have become unapproved)
            final boolean removedMissingProducts = targetCommerceCartService.removeMissingProducts(targetCartService
                    .getSessionCart());
            if (removedMissingProducts) {
                GlobalMessages.addInfoMessage(model, "checkout.information.missing.products.removed");
                // update cart again within model
                populateCartList(model, targetCartFacade.getSessionCart());
            }
        }
        else {
            prepareDataForPage(model, null,
                    commonI18NService.getLocaleForLanguage(commonI18NService.getCurrentLanguage()));
        }
        setPageNotCached(response);
        return ControllerConstants.Views.Pages.Cart.CART_PAGE;
    }

    /**
     * @param request
     */
    protected void populateCartProductsRecommendations(final HttpServletRequest request) {
        final List<String> productCodes = targetCartFacade.getCartsColorVariantProductCodes();
        if (CollectionUtils.isNotEmpty(productCodes)) {
            final EndecaSearchStateData searchStateData = new EndecaSearchStateData();
            searchStateData.setProductCodes(productCodes);
            try {
                request.setAttribute(
                        WebConstants.RECOMMENDED_PRODUCTS_KEY,
                        getEndecaPageAssemble().getContentItemForPage(request,
                                EndecaConstants.EndecaPageUris.ENDECA_CART_PAGE_URI, null, searchStateData));
            }
            catch (final TargetEndecaWrapperException e) {
                LOG.error(SplunkLogFormatter.formatMessage("Error thrown inside Endeca Assembler.",
                        TgtutilityConstants.ErrorCode.ERR_TGTSTORE_PRODUCT_SEARCH), e);
            }
        }
    }

    /**
     * @param model
     */
    protected void populateAfterPayConfig(final Model model) {
        if (targetFeatureSwitchFacade.isAfterpayEnabled()) {
            model.addAttribute("afterpayConfig", afterpayConfigFacade.getAfterpayConfig());
        }
    }

    /**
     * @param model
     */
    protected void populateAfterPayModal(final Model model) {
        if (targetFeatureSwitchFacade.isAfterpayEnabled()) {
            try {
                final ContentPageModel page = cmsPageService.getPageForLabelOrId(TgtwebcoreConstants.AFTERPAY_MODAL_ID);
                model.addAttribute("afterpayModal", page.getLabel());
            }
            catch (final CMSItemNotFoundException e) {
                model.addAttribute("afterpayModal", "");
            }
        }
    }


    /**
     * Handle the '/basket/checkout' request url. This method checks to see if the cart is valid before allowing the
     * checkout to begin. Note that this method does not require the user to be authenticated and therefore allows us to
     * validate that the cart is valid without first forcing the user to login. The cart will be checked again once the
     * user has logged in.
     * 
     * @return The page to redirect to
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.CART_CHECKOUT, method = RequestMethod.GET)
    public String cartCheck(final Model model,
            @RequestParam(value = EXPEDITE_KEY, required = false) final String expedite,
            final HttpServletRequest request, final HttpServletResponse response)
                    throws CMSItemNotFoundException {
        if (sohValidation(model)) {
            reassessFlybuysDiscount(model);
            return showCart(false, model, request, response);
        }

        final Map<String, TargetCartData> carts = targetCartFacade.getCarts();

        if (!targetCartFacade.hasSessionCart() //No cart -> basket page (empty cart!)
                || areCartsAllEmpty(carts) //Empty cart -> basket page (empty cart!)
                || getOnlyCart(carts).getValue().isInsufficientAmount()//Only one cart but the total amount is insufficient for the purchase option
                || !getOnlyCart(carts).getValue().gethasValidDeliveryModes()) //if there are no valid delivery modes
        {
            return ControllerConstants.Redirection.CART;
        }

        sessionService.setAttribute(TgtFacadesConstants.ACTIVE_PURCHASE_OPTION, getOnlyCartPurchaseOption(carts));
        if (targetFeatureSwitchFacade.isExpeditedCheckoutEnabled() && StringUtils.equals(expedite, TURNED_ON)) {
            sessionService.setAttribute(EXPEDITE_KEY, TURNED_ON);
        }

        return ControllerConstants.Redirection.CHECKOUT;
    }

    /**
     * If their is soh update we redirect the user on the cart page
     * 
     * @param model
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.CART_CONTINUE_SHOPPING, method = RequestMethod.GET)
    public String continueShopping(final Model model, final HttpServletRequest request,
            final HttpServletResponse response)
                    throws CMSItemNotFoundException {
        if (sohValidation(model)) {
            reassessFlybuysDiscount(model);
            return showCart(false, model, request, response);
        }
        return ControllerConstants.Redirection.HOME;
    }

    /**
     * 
     * @param carts
     * @return true if all carts are empty
     */
    protected boolean areCartsAllEmpty(final Map<String, TargetCartData> carts) {
        for (final Entry<String, TargetCartData> entry : carts.entrySet()) {
            if (CollectionUtils.isNotEmpty(entry.getValue().getEntries())) {
                return false;
            }
        }
        return true;
    }

    /**
     * 
     * @param carts
     * @return true if only one cart is not empty
     */
    protected boolean isOnlyOneCartNotEmpty(final Map<String, TargetCartData> carts) {
        int notEmptyCartsNum = 0;
        for (final Entry<String, TargetCartData> entry : carts.entrySet()) {
            if (CollectionUtils.isNotEmpty(entry.getValue().getEntries())) {
                notEmptyCartsNum++;
            }
        }
        return notEmptyCartsNum == 1;
    }

    /**
     * 
     * @param carts
     * @return String representing purchase option of a cart (layby,buynow....)
     */
    protected String getOnlyCartPurchaseOption(final Map<String, TargetCartData> carts) {
        return getOnlyCart(carts).getKey();
    }

    /**
     * 
     * @param carts
     * @return entry of the only non empty cart
     */
    protected Entry<String, TargetCartData> getOnlyCart(final Map<String, TargetCartData> carts) {
        for (final Entry<String, TargetCartData> entry : carts.entrySet()) {
            if (CollectionUtils.isNotEmpty(entry.getValue().getEntries())) {
                return entry;
            }
        }
        return null;
    }

    @RequestMapping(value = ControllerConstants.CART_UPDATE, method = RequestMethod.GET)
    public String updateCartQuantities() {
        return ControllerConstants.Redirection.CART;
    }

    /**
     * 
     * @param entryNumber
     * @param model
     * @param form
     * @param bindingResult
     * @param redirectModel
     * @param productCode
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.CART_UPDATE, method = RequestMethod.POST)
    public String updateCartQuantities(@RequestParam("entryNumber") final long entryNumber, final Model model,
            @Valid final UpdateQuantityForm form, final BindingResult bindingResult,
            final RedirectAttributes redirectModel,
            @RequestParam(value = "productCode", required = true) final String productCode,
            final HttpServletRequest request, final HttpServletResponse response)
                    throws CMSItemNotFoundException {
        if (bindingResult.hasErrors()) {
            CartDataHelper.addUpdateEntryError(bindingResult, model);
        }
        else {
            final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
            synchronized (mutex) {

                if (targetCartFacade.getCarts() != null) {
                    if (REMOVE_ACTION.equals(form.getAction())) {
                        form.setQuantity(Long.valueOf(0));
                    }
                    try {
                        final ProductModel productModel = targetProductFacade.getProductForCode(productCode);
                        if (ProductUtil.isProductTypePhysicalGiftcard(productModel)) {
                            targetCartFacade.validateGiftCardsUpdateQty(productCode, form.getQuantity().longValue());
                        }
                        final CartModificationData cartModification = targetCartFacade.updateCartEntry(productCode,
                                form.getQuantity().longValue());
                        addModificationNotification(redirectModel, form.getAction(), response, cartModification,
                                    form.getQuantity());

                        return ControllerConstants.Redirection.CART;

                    }
                    catch (final CommerceCartModificationException ex) {
                        LOG.warn("Couldn't update product with the entry number: " + entryNumber + ".", ex);
                    }
                    catch (final GiftCardValidationException exp) {
                        final Object[] params = new Object[] { exp.getPermittedValue() };
                        final String errorMessage = messageSource.getMessage(
                                "basket.error.physical.giftcard." + exp.getErrorType().getValue(),
                                params,
                                i18nService.getCurrentLocale());
                        GlobalMessages.addErrorMessage(model, errorMessage, params);

                    }
                    catch (final UnknownIdentifierException uie) {
                        // product in cart is now unapproved and hence need to be removed
                        //return to cart page
                        targetCommerceCartService.removeMissingProducts(targetCartService.getSessionCart());
                        GlobalMessages.addInfoMessage(model, "checkout.information.missing.products.removed");
                    }
                }

            }
        }

        return showCart(false, model, request, response);
    }


    /**
     * @param entryNumber
     * @param model
     * @param redirectModel
     * @param productCode
     * @param response
     * @return String representing the redirection
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.CART_UPDATE_GIFTCARDS, method = RequestMethod.POST)
    public String updateGiftCards(final Model model,
            final RedirectAttributes redirectModel,
            @RequestParam(value = "entryNumber", required = true) final long entryNumber,
            @RequestParam(value = "giftRecipientNumber", required = true) final int giftRecipientNumber,
            @Valid final UpdateGiftCardForm form, final BindingResult bindingResult,
            @RequestParam(value = "id", required = true) final String id,
            @RequestParam(value = "productCode", required = true) final String productCode,
            final HttpServletRequest request, final HttpServletResponse response)
                    throws CMSItemNotFoundException {
        final boolean respondWithFragment = requestHeadersHelper.isXhrRequest(request);

        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {

            if (!bindingResult.hasErrors()) {
                if (targetCartFacade.getCarts() != null) {
                    try {
                        CartModificationData cartModification = null;
                        if (StringUtils.equals(REMOVE_ACTION, form.getAction())) {
                                cartModification = targetCartFacade.removeGiftCardRecipientEntry(productCode,
                                        giftRecipientNumber, id);
                                addModificationNotification(redirectModel, form.getAction(), response,
                                        cartModification,
                                    Long.valueOf(1L));
                        }
                        if (StringUtils.equals(UPDATE_ACTION, form.getAction())) {
                            GiftRecipientDTO giftRecipientDTO = null;
                            if (form.getGiftRecipientForm() != null) {
                                giftRecipientDTO = giftRecipientFormToDTOConverter.convert(form
                                        .getGiftRecipientForm());
                                cartModification = targetCartFacade.updateGiftCardDetails(productCode,
                                        giftRecipientDTO,
                                        giftRecipientNumber, id);
                            }
                            model.addAttribute("entry", cartModification != null ? cartModification.getEntry() : null);
                            model.addAttribute("recipientId", id);
                        }
                        if (respondWithFragment) {
                            model.addAttribute("success", Boolean.TRUE);
                            return ControllerConstants.Views.Fragments.Cart.UPDATE_GIFTCARD;
                        }
                        return ControllerConstants.Redirection.CART;

                    }
                    catch (final CommerceCartModificationException ex) {
                        LOG.warn("Couldn't update product with the product Code: " + productCode + ".", ex);
                    }
                }
            }

        }

        if (respondWithFragment) {
            model.addAttribute("success", Boolean.FALSE);
            model.addAttribute(form);
            return ControllerConstants.Views.Fragments.Cart.UPDATE_GIFTCARD;
        }

        return showCart(false, model, request, response);
    }

    /**
     * @param redirectModel
     * @param action
     * @param response
     * @param cartModification
     * @param quantity
     */
    protected void addModificationNotification(final RedirectAttributes redirectModel, final String action,
            final HttpServletResponse response, final CartModificationData cartModification, final Long quantity) {
        if (cartModification != null) {
            reassessFlybuysDiscount(redirectModel);

            CartDataHelper
                    .addCartModificationNotification(cartModification.getStatusCode(),
                            action,
                            redirectModel, Long.valueOf(cartModification.getQuantity()),
                            quantity);
            redirectModel.addFlashAttribute("cartModification", cartModification);
            setAnonymousCartCookie(response);
        }
    }


    @RequestMapping(value = ControllerConstants.CART_UPDATE_GIFTCARDS, method = RequestMethod.GET)
    public String updateGiftCards() {
        return ControllerConstants.Redirection.CART;
    }

    /**
     * 
     * @param model
     * @param cartData
     * @param locale
     * @throws CMSItemNotFoundException
     */
    protected void createProductList(final Model model, final CartData cartData, final Locale locale)
            throws CMSItemNotFoundException {
        if (cartData != null && cartData.getEntries() != null && !cartData.getEntries().isEmpty()) {
            for (final OrderEntryData entry : cartData.getEntries()) {
                final UpdateQuantityForm uqf = new UpdateQuantityForm();
                uqf.setQuantity(entry.getQuantity());
                model.addAttribute("updateQuantityForm" + entry.getProduct().getCode(), uqf);

                if (!model.containsAttribute(ProductDataHelper.CNC_ERROR_MESSAGE_ID)
                        && entry.getProduct() instanceof TargetProductData
                        && !((TargetProductData)entry.getProduct()).isAllowDeliverToStore()) {
                    //Adding content for CNC Error message
                    ProductDataHelper.addContentNameFromCMSComponent(ProductDataHelper.CNC_ERROR_MESSAGE_ID,
                            locale,
                            model, cmsComponentService);
                }

                if (!model.containsAttribute("hideIncentiveMessage") &&
                        entry.getProduct() instanceof TargetProductData &&
                        TgtCoreConstants.PHYSICAL_GIFTCARD
                                .equals(((TargetProductData)entry.getProduct()).getProductTypeCode())) {
                    model.addAttribute("hideIncentiveMessage", Boolean.TRUE);
                }
            }
        }

        model.addAttribute("cartData", cartData);

        final ContentPageModel contentPageForLabelOrId = getContentPageForLabelOrId(CART_CMS_PAGE_LABEL);
        storeCmsPageInModel(model, contentPageForLabelOrId);
        setUpMetaDataForContentPage(model, contentPageForLabelOrId);
    }

    protected void prepareDataForPage(final Model model, final CartData cartData, final Locale locale)
            throws CMSItemNotFoundException {
        final String continueUrl = (String)sessionService.getAttribute(WebConstants.CONTINUE_URL);
        model.addAttribute(CONTINUE_URL, (continueUrl != null && !continueUrl.isEmpty()) ? continueUrl
                : ControllerConstants.ROOT);

        createProductList(model, cartData, locale);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.cart"));
        model.addAttribute("pageType", PageType.Cart);
    }

    private void reassessFlybuysDiscount(final Model model) {
        if (flybuysDiscountFacade.reassessFlybuysDiscountOnCheckoutCart() && targetCartFacade.hasEntries()) {
            final FlybuysRedeemConfigData flybuysRedeemConfigData = flybuysDiscountFacade
                    .getFlybuysRedeemConfigData();

            final BigDecimal minCartValueAmount = BigDecimal.valueOf(flybuysRedeemConfigData
                    .getMinCartValue().doubleValue());

            final TargetCartData cart = getOnlyCart(targetCartFacade.getCarts()).getValue();
            final PriceData cartSubtotal = cart.getSubTotal();

            if (cartSubtotal.getValue().compareTo(minCartValueAmount) < 0) {
                final PriceData minCartValuePriceData = priceDataFactory.create(PriceDataType.BUY, minCartValueAmount,
                        cartSubtotal.getCurrencyIso());

                if (model instanceof RedirectAttributes) {
                    GlobalMessages.addFlashInfoMessage((RedirectAttributes)model,
                            "basket.information.flybuys.discount.removed",
                            new Object[] { minCartValuePriceData.getFormattedValue() });
                }
                else {
                    GlobalMessages.addInfoMessage(model,
                            "basket.information.flybuys.discount.removed",
                            new Object[] { minCartValuePriceData.getFormattedValue() });
                }

            }
        }
    }

    /**
     * @param targetCartFacade
     *            the targetCartFacade to set
     */
    public void setTargetCartFacade(final TargetCartFacade targetCartFacade) {
        this.targetCartFacade = targetCartFacade;
    }

    /**
     * @param sessionService
     *            the sessionService to set
     */
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

    /**
     * @param targetPurchaseOptionHelper
     *            the targetPurchaseOptionHelper to set
     */
    public void setTargetPurchaseOptionHelper(final TargetPurchaseOptionHelper targetPurchaseOptionHelper) {
        this.targetPurchaseOptionHelper = targetPurchaseOptionHelper;
    }

    protected void populateCartList(final Model model, final CartData cartData) {
        model.addAttribute(CART_DATA, cartData);
    }

    /**
     * 
     * @param model
     * @return true if their is any SOH modification
     */
    protected boolean sohValidation(final Model model) {
        final AdjustedCartEntriesData adjustedCartEntriesData = targetCheckoutFacade.adjustCart();

        if (adjustedCartEntriesData != null && adjustedCartEntriesData.isAdjusted()) {
            Set existingAdjustedCartEntriesSet = null;
            if (model.asMap().get(ControllerConstants.SOH_UPDATES) instanceof Set) {
                existingAdjustedCartEntriesSet = (Set)model.asMap().get(ControllerConstants.SOH_UPDATES);
            }
            final Map<String, AdjustedCartEntriesData> existingAdjustedCartEntriesDataMap = getExistingAdjustedCartEntriesDataMap(
                    existingAdjustedCartEntriesSet);
            final PurchaseOptionModel purchaseOptionModel = targetPurchaseOptionHelper.getCurrentPurchaseOptionModel();
            final AdjustedCartEntriesData existingAdjustedCartEntriesData = existingAdjustedCartEntriesDataMap
                    .get(purchaseOptionModel
                            .getCode());
            if (existingAdjustedCartEntriesData != null) {
                adjustedCartEntriesData.getAdjustedItems().addAll(existingAdjustedCartEntriesData.getAdjustedItems());
                adjustedCartEntriesData.getOutOfStockItems().addAll(
                        existingAdjustedCartEntriesData.getOutOfStockItems());
            }
            model.addAttribute(ControllerConstants.SOH_UPDATES, Collections.singleton(adjustedCartEntriesData));
            return true;
        }
        return false;
    }

    private Map<String, AdjustedCartEntriesData> getExistingAdjustedCartEntriesDataMap(
            final Set existingAdjustedCartEntriesSet) {
        final Map<String, AdjustedCartEntriesData> existingAdjustedCartEntriesDataMap = new HashMap<String, AdjustedCartEntriesData>();

        if (existingAdjustedCartEntriesSet != null) {
            for (final Object existingAdjustedCartEntry : existingAdjustedCartEntriesSet) {
                if (existingAdjustedCartEntry instanceof AdjustedCartEntriesData) {
                    existingAdjustedCartEntriesDataMap.put(
                            ((AdjustedCartEntriesData)existingAdjustedCartEntry).getPurchaseType(),
                            (AdjustedCartEntriesData)existingAdjustedCartEntry);
                }
            }
        }

        return existingAdjustedCartEntriesDataMap;
    }

    /**
     * Method to update delivery mode of the cart.
     * 
     * @param selectedDeliveryMode
     * @param model
     * @return String
     */
    @RequestMapping(value = ControllerConstants.CART_DELIVERY_MODE_UPDATE, method = RequestMethod.POST)
    public String updateDeliveryMode(@RequestParam("selectedDeliveryMode") final String selectedDeliveryMode,
            final Model model) {
        targetCheckoutFacade.updateDeliveryMode(selectedDeliveryMode);
        final CartData cartData = targetCheckoutFacade.getCheckoutCart();
        model.addAttribute("cartData", cartData);
        model.addAttribute("notShowEntries", Boolean.TRUE);
        return ControllerConstants.Views.Pages.MultiStepCheckout.UPDATE_DELIVERY_MODE_PAGE;
    }

    /**
     * Method to get the delivery mode by postcode on the cart.
     * 
     * @param postcodeDto
     * @param model
     * @return String
     */
    @RequestMapping(value = ControllerConstants.CART_DELIVERY_MODE_DISPLAY, method = RequestMethod.POST)
    public String showDeliveryModebyPostcode(@RequestBody final DeliveryModePostcodeDto postcodeDto,
            final Model model) {
        final String postcode = postcodeDto.getPostcode();
        if (!TargetValidationUtils.matchRegularExpression(postcode, TargetValidationCommon.PostCode.PATTERN,
                TargetValidationCommon.PostCode.SIZE, TargetValidationCommon.PostCode.SIZE, false)) {
            model.addAttribute(ERROR_MSG, INVALID_POSTCODE_ERROR);
            return getCartShowDeliveryMode();
        }
        sessionService.setAttribute(TgtCoreConstants.SESSION_POSTCODE, postcode);
        final CartData cartData = targetCheckoutFacade.getCheckoutCart();
        model.addAttribute("catchmentArea", targetCartFacade.getCatchmentAreaByPostcode(postcode));
        model.addAttribute("cartData", cartData);
        model.addAttribute("deliveryPostcode", postcode);
        return getCartShowDeliveryMode();
    }

    /**
     * Method to remove delivery info from cart when user chooses to change his post code.
     * 
     * @param model
     * @return String
     */
    @RequestMapping(value = ControllerConstants.CART_DELIVERY_MODE_REMOVAL, method = RequestMethod.GET)
    public String removeDeliveryMode(final Model model) {
        targetCheckoutFacade.removeDeliveryInfo();
        final CartData cartData = targetCheckoutFacade.getCheckoutCart();
        model.addAttribute("cartData", cartData);
        model.addAttribute("notShowEntries", Boolean.TRUE);
        return ControllerConstants.Views.Pages.MultiStepCheckout.UPDATE_DELIVERY_MODE_PAGE;
    }

    private String getCartShowDeliveryMode() {
        return ControllerConstants.Views.Fragments.Cart.CART_SHOW_DELIVERY_MODE;
    }

}

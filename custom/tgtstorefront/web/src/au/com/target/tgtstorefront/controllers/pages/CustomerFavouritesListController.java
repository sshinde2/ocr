/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UrlPathHelper;

import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.querybuilder.TargetFavouritesListQueryBuilder;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.product.data.CustomerProductInfo;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetSelectedVariantData;
import au.com.target.tgtfacades.wishlist.TargetWishListFacade;
import au.com.target.tgtstorefront.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.EndecaQueryResultsHelper;
import au.com.target.tgtstorefront.forms.FavouritesListShareForm;
import au.com.target.tgtwishlist.enums.WishListTypeEnum;
import au.com.target.tgtwishlist.exception.InvalidWishListUserException;
import au.com.target.tgtwishlist.logger.TgtWishListLogger;


/**
 * @author mjanarth
 *
 */
@Controller
@RequestMapping(value = ControllerConstants.CUSTOMER_WISHLIST_FAVOURITES)
public class CustomerFavouritesListController extends AbstractPageController {

    protected static final Logger LOG = Logger.getLogger(CustomerFavouritesListController.class);

    private static final String CUSTOMER_WISHLIST_FAVOURITES_CMS_PAGE_LABEL = "/favourites";

    @Resource(name = "urlPathHelper")
    private UrlPathHelper urlPathHelper;

    @Resource(name = "targetFavouritesListQueryBuilder")
    private TargetFavouritesListQueryBuilder targetFavouritesListQueryBuilder;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Resource(name = "endecaQueryResultsHelper")
    private EndecaQueryResultsHelper endecaQueryResultsHelper;

    @Resource(name = "targetWishListFacade")
    private TargetWishListFacade targetWishListFacade;

    @Resource(name = "contentPageBreadcrumbBuilder")
    private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;

    @Resource(name = "cmsPageService")
    private CMSPageService cmsPageService;

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    /**
     * For returning the favourites page.
     * 
     * @param model
     * @return view
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(method = RequestMethod.GET)
    public String getFavouritesProductPage(final Model model)
            throws CMSItemNotFoundException {
        final ContentPageModel pageModel = cmsPageService
                .getPageForLabelOrId(CUSTOMER_WISHLIST_FAVOURITES_CMS_PAGE_LABEL);

        storeCmsPageInModel(model, pageModel);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                contentPageBreadcrumbBuilder.getBreadcrumbs(pageModel));
        model.addAttribute("pageType", PageType.Favourite);
        return ControllerConstants.Views.Pages.Favourites.FAVOURITES_PAGE;
    }

    /**
     * This method gets the JSON Object and populate the product info from Endeca
     * 
     * @param products
     * @param request
     * @return String
     * @throws TargetEndecaException
     * @throws ENEQueryException
     */
    @RequestMapping(method = RequestMethod.POST)
    public String getFavouritesProductDetails(
            @RequestBody final CustomerProductInfo[] products,
            final HttpServletRequest request, final Model model) throws ENEQueryException, TargetEndecaException {
        final List<CustomerProductInfo> productsToRetrieve = getProductsToRetrieve(products);
        final List<TargetProductListerData> productDataList = getProductDataList(productsToRetrieve, getMaxProducts());
        final List<CustomerProductInfo> updatedCustomerProductData = updateCustomerProductData(productsToRetrieve,
                productDataList);
        targetWishListFacade.replaceFavouritesWithLatest(productsToRetrieve, updatedCustomerProductData);
        model.addAttribute("productData", productDataList);
        model.addAttribute("customerProductData", updatedCustomerProductData);
        return ControllerConstants.Views.Fragments.Wishlist.FAVOURITES_FRAGMENT;
    }

    /**
     * This method gets the JSON Object and populate the product info from Endeca
     * 
     * @param productInfo
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addProductsToFavouriteList(
            @RequestBody final CustomerProductInfo productInfo, final Model model) {
        try {
            targetWishListFacade.addProductToList(productInfo, null, WishListTypeEnum.FAVOURITE);
            model.addAttribute("success", Boolean.TRUE);
        }
        catch (final Exception ex) {
            model.addAttribute("success", Boolean.FALSE);
        }
        return ControllerConstants.Views.Fragments.Wishlist.FAVOURITES_FRAGMENT_ADD_REMOVE;
    }

    /**
     * This method gets the JSON Object and populate the product info from Endeca
     * 
     * @param productInfos
     */
    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public String removeProductsFromFavouriteList(
            @RequestBody final CustomerProductInfo[] productInfos, final Model model) {
        try {
            targetWishListFacade.removeProductsFromList(Arrays.asList(productInfos), null, WishListTypeEnum.FAVOURITE);
            model.addAttribute("success", Boolean.TRUE);
        }
        catch (final Exception ex) {
            model.addAttribute("success", Boolean.FALSE);
        }
        return ControllerConstants.Views.Fragments.Wishlist.FAVOURITES_FRAGMENT_ADD_REMOVE;
    }

    /**
     * This method gets the JSON Object and populate the product info from Endeca
     * 
     * @param productInfo
     * @throws TargetEndecaException
     * @throws ENEQueryException
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateVariantToFavourite(
            @RequestBody final CustomerProductInfo productInfo,
            final HttpServletRequest request, final Model model) throws ENEQueryException, TargetEndecaException {
        try {
            targetWishListFacade.addProductToList(productInfo, null, WishListTypeEnum.FAVOURITE);
            model.addAttribute("success", Boolean.TRUE);
        }
        catch (final InvalidWishListUserException ex) {
            model.addAttribute("success", Boolean.TRUE);
        }
        catch (final Exception ex) {
            TgtWishListLogger.logAddToListFailureSynch(LOG, TgtWishListLogger.UPDATE_FAV_LIST_ACTION,
                    WishListTypeEnum.FAVOURITE.getCode(), productInfo.getBaseProductCode(),
                    productInfo.getSelectedVariantCode(),
                    ex);
            model.addAttribute("success", Boolean.FALSE);
        }
        getFavouritesProductDetails(new CustomerProductInfo[] { productInfo }, request, model);
        return ControllerConstants.Views.Fragments.Wishlist.FAVOURITES_FRAGMENT;
    }

    @RequestMapping(value = "/sync", method = RequestMethod.POST)
    @ResponseBody
    public CustomerProductInfo[] syncProductsToFavouriteList(
            @RequestBody final CustomerProductInfo[] customerProductInfos) {
        try {
            final List<CustomerProductInfo> syncFavouritesList = targetWishListFacade
                    .syncFavouritesList(Arrays.asList(customerProductInfos));
            final CustomerProductInfo[] returnedList = syncFavouritesList
                    .toArray(new CustomerProductInfo[syncFavouritesList.size()]);
            return returnedList;
        }
        catch (final Exception e) {
            LOG.error("WISHLIST: An unexpected error has occured when synching the products", e);
            return customerProductInfos;
        }
    }

    /**
     * This method gets view for the sharing favourites form
     * 
     */
    //    @RequestMapping(value = "/share", method = RequestMethod.GET) // TODO uncomment this once the security vulnerability in favourites share feature has been fixed
    public String getFavouritesShareView(final Model model) {
        final FavouritesListShareForm form = new FavouritesListShareForm();
        form.setMessageText(getDefaultMessageForSharingFavourites());
        model.addAttribute("shareLimit", Integer.valueOf(getMaxProductsForSharing()));
        model.addAttribute("favouritesListShareForm", form);
        return ControllerConstants.Views.Fragments.Wishlist.FAVOURITES_SHARE;
    }


    /**
     * This method does the validation of the form and trigger sending sharing favorites email to customer
     * 
     */
    //    @RequestMapping(value = "/share", method = RequestMethod.POST) // TODO uncomment this once the security vulnerability in favourites share feature has been fixed
    public String sendFavouritesEmail(final Model model,
            @Valid final FavouritesListShareForm favouritesListShareForm, final BindingResult bindingResult) {
        boolean success = false;
        try {
            if (!bindingResult.hasErrors()) {
                final List<CustomerProductInfo> customerFavourites = targetWishListFacade.retrieveFavouriteProducts();
                final List<TargetProductListerData> productListToBeShared = populateListToBeShared(
                        getProductDataList(customerFavourites, getBufferSize()
                                + getMaxProductsForSharing()));
                success = targetWishListFacade.shareWishListForUser(productListToBeShared,
                        favouritesListShareForm.getRecipientEmailAddress(), favouritesListShareForm.getFullName(),
                        favouritesListShareForm.getMessageText());
                if (!success) {
                    model.addAttribute("integrationError", Boolean.valueOf(true));
                }
                model.addAttribute("recipientEmailAddress", favouritesListShareForm.getRecipientEmailAddress());
            }
            else {
                model.addAttribute(favouritesListShareForm);
            }
        }
        catch (final Exception e) {
            model.addAttribute("integrationError", Boolean.valueOf(true));
            LOG.error("WISHLIST: An unexpected error has occured when sharing favourites", e);
        }
        model.addAttribute("success", Boolean.valueOf(success));
        model.addAttribute("shareLimit", Integer.valueOf(getMaxProductsForSharing()));
        return ControllerConstants.Views.Fragments.Wishlist.FAVOURITES_SHARE_RESPONSE;
    }

    protected List<TargetProductListerData> getProductDataList(final List<CustomerProductInfo> productsToRetrieve,
            final int maxNumberOfProducts) throws ENEQueryException, TargetEndecaException {
        List<TargetProductListerData> productDataList = null;
        final EndecaSearchStateData endecaSearchStateData;
        final ENEQueryResults queryResults;
        endecaSearchStateData = populateEndecaSearchStateData(productsToRetrieve);
        queryResults = targetFavouritesListQueryBuilder.getQueryResults(endecaSearchStateData,
                maxNumberOfProducts);
        if (targetFeatureSwitchFacade.isFeatureEnabled("wishlist.favouritesVariants")) {
            productDataList = endecaQueryResultsHelper.getTargetProductList(
                    queryResults, createBaseProductCodeAndSelectedVariantMap(productsToRetrieve));
        }
        else {
            productDataList = endecaQueryResultsHelper.getTargetProductList(
                    queryResults);
        }
        return productDataList;
    }

    protected List<CustomerProductInfo> getProductsToRetrieve(final CustomerProductInfo[] products) {
        List<CustomerProductInfo> productsToRetrieve = new ArrayList<>();
        if (null != products) {
            productsToRetrieve = Arrays.asList(products);
        }
        try {
            productsToRetrieve = targetWishListFacade
                    .retrieveFavouriteProducts();
        }
        catch (final InvalidWishListUserException e) {
            /*No action needed as it is not a signed in or valid user.
            So will return the products from local storage. */
            //e.getMessage();
        }
        catch (final Exception e) {
            productsToRetrieve = ListUtils.EMPTY_LIST;
        }
        return productsToRetrieve;
    }

    /**
     * Populates endeca search state data
     * 
     * @param productInfos
     * @return EndecaSearchStateData
     */
    protected EndecaSearchStateData populateEndecaSearchStateData(final List<CustomerProductInfo> productInfos) {
        return populateEndecaSearchStateData(productInfos, true);
    }

    /**
     * Populates endeca search state data
     * 
     * @param productInfos
     * @param useSelectedVariantCode
     * @return EndecaSearchStateData
     */
    protected EndecaSearchStateData populateEndecaSearchStateData(final List<CustomerProductInfo> productInfos,
            final boolean useSelectedVariantCode) {
        final List recFilterOptions = new ArrayList<String>();
        recFilterOptions.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_PRODUCT_CODE);
        recFilterOptions.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        recFilterOptions.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE);

        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setSkipInStockFilter(true);
        endecaSearchStateData.setSkipDefaultSort(true);
        endecaSearchStateData.setRecordFilterOptions(recFilterOptions);
        final List<String> productCodes = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(productInfos)) {
            for (final CustomerProductInfo productInfo : productInfos) {
                if (useSelectedVariantCode && StringUtils.isNotEmpty(productInfo.getSelectedVariantCode())) {
                    productCodes.add(productInfo.getSelectedVariantCode());
                }
                else {
                    productCodes.add(productInfo.getBaseProductCode());
                }
            }
            endecaSearchStateData.setNpSearchState(EndecaConstants.ENDECA_NP_2);
            endecaSearchStateData.setProductCodes(productCodes);
            endecaSearchStateData.setFieldListConfig(EndecaConstants.FieldList.COMPONENT_FIELDLIST);
        }
        return endecaSearchStateData;
    }


    /**
     * 
     * @param customerFavourites
     * @return List
     */
    protected List<TargetProductListerData> populateListToBeShared(
            final List<TargetProductListerData> customerFavourites) {
        List<TargetProductListerData> productListToBeShared = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(customerFavourites)) {
            if (customerFavourites.size() > getMaxProductsForSharing()) {
                productListToBeShared = customerFavourites.subList(0, getMaxProductsForSharing());
            }
            else {
                productListToBeShared = customerFavourites;
            }
        }
        return productListToBeShared;
    }

    /**
     * Creates a map with baseproduct code and selected variant from the CustomerProductInfos
     * 
     * @param customerProductInfos
     * @return productCodeAndSelectedVaraintMap
     */
    protected Map<String, String> createBaseProductCodeAndSelectedVariantMap(
            final List<CustomerProductInfo> customerProductInfos) {
        final Map<String, String> productCodeAndSelectedVaraintMap = new HashMap<>();
        if (CollectionUtils.isEmpty(customerProductInfos)) {
            return null;
        }
        for (final CustomerProductInfo customerProductInfo : customerProductInfos) {
            productCodeAndSelectedVaraintMap.put(customerProductInfo.getBaseProductCode(),
                    customerProductInfo.getSelectedVariantCode());
        }
        return productCodeAndSelectedVaraintMap;

    }

    /**
     * Method to verify the customer product data with the record fetch from endeca.
     * 
     * @param productsToRetrieve
     * @param productDataList
     * @return List<CustomerProductInfo>
     */
    protected List<CustomerProductInfo> updateCustomerProductData(final List<CustomerProductInfo> productsToRetrieve,
            final List<TargetProductListerData> productDataList) {
        final List<CustomerProductInfo> updateProductList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(productsToRetrieve) && CollectionUtils.isNotEmpty(productDataList)) {
            for (final TargetProductListerData productData : productDataList) {
                final String baseProductCode = productData.getBaseProductCode();
                final TargetSelectedVariantData selectedVariantData = productData.getSelectedVariantData();
                final String selectedVariantCode = selectedVariantData == null ? StringUtils.EMPTY
                        : selectedVariantData.getProductCode();
                final CustomerProductInfo productInfo = (CustomerProductInfo)CollectionUtils.find(productsToRetrieve,
                        new Predicate() {

                            @Override
                            public boolean evaluate(final Object infoObj) {
                                final CustomerProductInfo customerProductInfo = (CustomerProductInfo)infoObj;
                                return customerProductInfo.getBaseProductCode()
                                        .equals(baseProductCode)
                                        || (customerProductInfo.getSelectedVariantCode() == null ? false
                                                : customerProductInfo.getSelectedVariantCode()
                                                        .equals(selectedVariantCode));
                            }
                        });
                if (productInfo != null) {
                    updateProductList.add(createCustomerProductData(baseProductCode, productInfo));
                }
            }
        }
        return updateProductList;
    }

    /**
     * @param baseProductCode
     * @param productInfo
     * @return CustomerProductInfo
     */
    private CustomerProductInfo createCustomerProductData(final String baseProductCode,
            final CustomerProductInfo productInfo) {
        final CustomerProductInfo newProductInfo = new CustomerProductInfo();
        newProductInfo.setBaseProductCode(baseProductCode);
        newProductInfo.setSelectedVariantCode(productInfo.getSelectedVariantCode());
        newProductInfo.setTimeStamp(productInfo.getTimeStamp());
        return newProductInfo;
    }

    /**
     * 
     * @return int
     */
    protected int getMaxProducts() {
        return configurationService.getConfiguration().getInt("storefront.favourites.component.size", 99);
    }

    /**
     * 
     * @return int
     */
    protected int getBufferSize() {
        return configurationService.getConfiguration().getInt("storefront.favourites.share.buffer", 10);
    }

    /**
     * 
     * @return int
     */
    protected int getMaxProductsForSharing() {
        return configurationService.getConfiguration().getInt("storefront.favourites.share.maxSize", 6);
    }

    /**
     * 
     * @return String
     */
    protected String getDefaultMessageForSharingFavourites() {
        return configurationService.getConfiguration().getString("tgtstorefront.share.favourites.message");
    }
}

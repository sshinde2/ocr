/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;

import java.io.UnsupportedEncodingException;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * @author rsparks1
 *
 */
@Controller
@RequestMapping(value = "/ws-api/v1/{baseSiteId}")
public class JsonCmsContentController extends AbstractPageController {
    protected static final Logger LOG = Logger.getLogger(JsonCmsContentController.class);
    private static final String JSON_CMS_LABEL_PATTERN = "/json-cms/{jsonCmsLabel:.*}";

    @Resource(name = "cmsPageService")
    private CMSPageService cmsPageService;

    /**
     * Returns a cms page as a JSON object. The JSP handles any errors.
     * 
     * @param jsonCmsLabel
     * @param model
     * @return String
     * @throws UnsupportedEncodingException
     */
    @RequestMapping(value = JSON_CMS_LABEL_PATTERN, method = RequestMethod.GET)
    public String getJsonCmsPage(@PathVariable("jsonCmsLabel") final String jsonCmsLabel,
            final Model model) throws UnsupportedEncodingException {
        try {
            final ContentPageModel pageModel = cmsPageService
                    .getPageForLabelOrId("/json-cms/" + jsonCmsLabel);
            storeCmsPageInModel(model, pageModel);
        }
        catch (final CMSItemNotFoundException e) {
            LOG.info("SPC CMS content not found", e);
        }
        return ControllerConstants.Views.Pages.JsonCmsContent.JSON_CMS_CONTENT_PAGE;
    }

}

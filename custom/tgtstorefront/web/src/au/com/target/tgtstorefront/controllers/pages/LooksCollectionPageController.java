/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtfacades.looks.data.LooksCollectionData;
import au.com.target.tgtfacades.looks.data.ShopTheLookPageData;
import au.com.target.tgtfacades.stl.TargetShopTheLookFacade;
import au.com.target.tgtfacades.url.impl.TargetShopTheLookUrlResolver;
import au.com.target.tgtfacades.util.TargetPriceDataHelper;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;
import au.com.target.tgtstorefront.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import au.com.target.tgtstorefront.breadcrumb.impl.SearchBreadcrumbBuilder;
import au.com.target.tgtstorefront.breadcrumb.impl.ShopTheLookBreadcrumbBuilder;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtwebcore.enums.CachedPageType;
import au.com.target.tgtwebcore.model.cms2.pages.TargetProductCollectionPageModel;
import au.com.target.tgtwebcore.model.cms2.pages.TargetProductGroupPageModel;


/**
 * @author mjanarth
 *
 */

@Controller
public class LooksCollectionPageController extends AbstractPageController {

    private static final String DEFAULT_SHOP_THE_LOOK_PAGE = "/inspiration/shopTheLookDefaultPage";

    private static final String COLLECTION_LABEL_PATTERN = "/collection/{collectionLabel:.*}";

    private static final String INSPIRATION_LABEL_PATTERN = "/inspiration/**/{stlCode:.*}";

    @Resource(name = "cmsPageService")
    private CMSPageService cmsPageService;

    @Resource(name = "targetPriceDataHelper")
    private TargetPriceDataHelper targetPriceDataHelper;

    @Resource(name = "contentPageBreadcrumbBuilder")
    private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;

    @Resource(name = "targetShopTheLookFacade")
    private TargetShopTheLookFacade targetShopTheLookFacade;

    @Resource(name = "searchBreadcrumbBuilder")
    private SearchBreadcrumbBuilder searchBreadcrumbBuilder;

    @Resource(name = "targetShopTheLookUrlResolver")
    private TargetShopTheLookUrlResolver targetShopTheLookUrlResolver;

    @Resource(name = "shopTheLookBreadcrumbBuilder")
    private ShopTheLookBreadcrumbBuilder shopTheLookBreadcrumbBuilder;

    /**
     * For getting Collection page,redirects to 404 if CMS page not found
     * 
     * @param collectionLabel
     * @param model
     * @param response
     * @return String
     * @throws UnsupportedEncodingException
     */
    @RequestMapping(value = COLLECTION_LABEL_PATTERN, method = RequestMethod.GET)
    public String geCollectionPage(@PathVariable("collectionLabel") final String collectionLabel,
            final Model model, final HttpServletResponse response) throws UnsupportedEncodingException {
        try {
            final ContentPageModel pageModel = cmsPageService
                    .getPageForLabelOrId("/collection/" + collectionLabel);
            if (pageModel instanceof TargetProductCollectionPageModel) {
                final TargetProductCollectionPageModel collectionPageModel = (TargetProductCollectionPageModel)pageModel;
                final LooksCollectionData collectionData = populateCollectionData(collectionPageModel);
                model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                        contentPageBreadcrumbBuilder.getBreadcrumbs(collectionPageModel));
                storeCmsPageInModel(model, collectionPageModel);
                model.addAttribute("collectionData", collectionData);
                return ControllerConstants.Views.Pages.LooksCollection.COLLECTION_PAGE;
            }
            else {
                return ControllerConstants.Forward.ERROR_404;
            }
        }
        catch (final CMSItemNotFoundException e) {
            return ControllerConstants.Forward.ERROR_404;
        }
    }

    @RequestMapping(value = INSPIRATION_LABEL_PATTERN, method = RequestMethod.GET)
    public String getShopTheLookPage(@PathVariable("stlCode") final String code,
            final HttpServletRequest request, final HttpServletResponse response, final Model model)
            throws UnsupportedEncodingException, CMSItemNotFoundException {
        final TargetShopTheLookModel targetShopTheLookModel = targetShopTheLookFacade.getShopTheLookByCode(code);
        if (targetShopTheLookModel == null) {
            return ControllerConstants.Forward.ERROR_404;
        }
        final String redirection = checkRequestUrl(request, response,
                targetShopTheLookUrlResolver.resolve(targetShopTheLookModel));
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }
        final ContentPageModel pageModel = cmsPageService.getPageForLabelOrId(DEFAULT_SHOP_THE_LOOK_PAGE);
        if (pageModel instanceof TargetProductCollectionPageModel) {
            final TargetProductCollectionPageModel collectionPageModel = (TargetProductCollectionPageModel)pageModel;
            storeCmsPageInModel(model, collectionPageModel);
        }
        populateShopTheLookData(targetShopTheLookModel, model);
        populateBreadcrumbData(targetShopTheLookModel, model);

        addAkamaiCacheAttributes(CachedPageType.SHOPTHELOOKPAGE, response, model);
        return ControllerConstants.Views.Pages.LooksCollection.COLLECTION_PAGE;
    }

    /**
     * Populates the collection and look data
     * 
     * @param collectionPageModel
     * @return LooksCollectionData
     */
    protected LooksCollectionData populateCollectionData(final TargetProductCollectionPageModel collectionPageModel) {
        final LooksCollectionData collectionData = new LooksCollectionData();
        final List<LookDetailsData> lookDetailsList = new ArrayList<>();
        if (null != collectionPageModel) {
            collectionData.setDescription(collectionPageModel.getCollectionDescription());
            collectionData.setName(collectionPageModel.getName());
            if (null != collectionPageModel.getCollectionImage()) {
                collectionData.setImageUrl(collectionPageModel.getCollectionImage().getURL());
                collectionData.setImgAltTxt(collectionPageModel.getCollectionImage().getAltText());
            }
            if (CollectionUtils.isNotEmpty(collectionPageModel.getNavigationNodeList())
                    && CollectionUtils.isNotEmpty(collectionPageModel.getNavigationNodeList().get(0).getChildren())) {
                final List<CMSNavigationNodeModel> cmsNavNodeList = collectionPageModel.getNavigationNodeList().get(0)
                        .getChildren();
                for (final CMSNavigationNodeModel navNodeModel : cmsNavNodeList) {
                    final LookDetailsData lookData = new LookDetailsData();
                    lookData.setVisible(navNodeModel.isVisible());
                    if (CollectionUtils.isNotEmpty(navNodeModel.getEntries())
                            && (null != navNodeModel.getEntries().get(0))) {
                        if (navNodeModel.getEntries().get(0).getItem() instanceof TargetProductGroupPageModel) {
                            final TargetProductGroupPageModel pageModel = (TargetProductGroupPageModel)navNodeModel
                                    .getEntries().get(0).getItem();
                            lookData.setLabel(pageModel.getLabel());
                            if (null != pageModel
                                    .getFromPrice()) {
                                final PriceData priceData = targetPriceDataHelper.populatePriceData(pageModel
                                        .getFromPrice(), PriceDataType.FROM);
                                lookData.setPriceData(priceData);
                            }
                            lookData.setName(pageModel.getName());
                            if (null != pageModel.getGroupImage()) {
                                lookData.setGroupImgUrl(pageModel.getGroupImage().getURL());
                            }

                        }
                    }
                    lookDetailsList.add(lookData);
                }
            }
            collectionData.setLookDetails(lookDetailsList);
        }
        return collectionData;
    }

    private void populateShopTheLookData(final TargetShopTheLookModel targetShopTheLookModel, final Model model) {
        final ShopTheLookPageData data = targetShopTheLookFacade.populateShopTheLookPageData(targetShopTheLookModel
                .getId());
        model.addAttribute("shopTheLookPageData", data);
    }

    private void populateBreadcrumbData(final TargetShopTheLookModel targetShopTheLookModel, final Model model) {
        model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                shopTheLookBreadcrumbBuilder.getBreadcrumbs(targetShopTheLookModel));
    }

    @ExceptionHandler(Exception.class)
    public String handleException(final Exception exception, final HttpServletRequest request) {
        request.setAttribute("message", exception.getMessage());
        return ControllerConstants.Forward.ERROR_404;
    }
}

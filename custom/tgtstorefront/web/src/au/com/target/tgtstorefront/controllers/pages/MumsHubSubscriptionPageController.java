/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.endeca.infront.dto.CustomerSubscriptionInfo;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionResponseType;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionType;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.forms.ENewsSubscriptionForm;
import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * @author bhuang3
 *
 */
@Controller
public class MumsHubSubscriptionPageController extends AbstractCustomerSubscriptionController
{

    /**
     * This method will accept the post request with an email and use the information for subscription. Will return a
     * Json Response indicating whether it was successful or not along with the success/failure msg.
     * 
     * @param model
     * @param subscriptionInfo
     * @param response
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.MUMS_HUB_SUBSCRIPTION, method = RequestMethod.POST)
    public String doMumsHubSubscribe(final Model model,
            @RequestBody final CustomerSubscriptionInfo subscriptionInfo, final HttpServletRequest request,
            final HttpServletResponse response) throws CMSItemNotFoundException
    {
        boolean isEmailValid = true;
        String failMessage = null;
        final String subscriptionEmail = subscriptionInfo.getEmail();
        if (StringUtils.isEmpty(subscriptionEmail)) {
            isEmailValid = false;
            failMessage = getMessageFromProperties("enews.email.invalid.empty");
        }
        else if (!TargetValidationCommon.Email.PATTERN.matcher(subscriptionEmail).matches()) {
            isEmailValid = false;
            failMessage = getMessageFromProperties("enews.email.invalid.pattern");
        }
        final ENewsSubscriptionForm eNewsSubscriptionForm = new ENewsSubscriptionForm();
        eNewsSubscriptionForm.setEmail(subscriptionEmail);
        eNewsSubscriptionForm.setSubscriptionSource(subscriptionInfo.getSubscriptionSource());
        if (!isEmailValid) {
            setJsonResponseMsg(model, WebConstants.ResponseMessageType.Error, TargetCustomerSubscriptionType.MUMS_HUB,
                    failMessage);
            LOG.warn("Unsuccessful Subscription because of invalid email address");
            return ControllerConstants.Views.Fragments.Enews.ENEWS_JSON_FRAGMENT;
        }

        final String emailInSession = (String)(request.getSession().getAttribute(
                ControllerConstants.SESSION_PARAM_MUMS_HUB_SUBSCRIPTION_EMAIL));
        if (StringUtils.isNotEmpty(emailInSession) && subscriptionEmail.equalsIgnoreCase(emailInSession)) {
            setJsonResponseMsg(model, WebConstants.ResponseMessageType.Success,
                    TargetCustomerSubscriptionType.MUMS_HUB, StringUtils.EMPTY);
            return ControllerConstants.Views.Fragments.Enews.ENEWS_JSON_FRAGMENT;
        }

        final TargetCustomerSubscriptionResponseType responseType = processCustomerSubscription(eNewsSubscriptionForm,
                TargetCustomerSubscriptionType.MUMS_HUB);
        if (TargetCustomerSubscriptionResponseType.SUCCESS.equals(responseType)) {
            setJsonResponseMsg(model, WebConstants.ResponseMessageType.Success,
                    TargetCustomerSubscriptionType.MUMS_HUB, StringUtils.EMPTY);
            request.getSession().setAttribute(ControllerConstants.SESSION_PARAM_MUMS_HUB_SUBSCRIPTION_EMAIL,
                    subscriptionEmail);
        }
        else if (TargetCustomerSubscriptionResponseType.ALREADY_EXISTS.equals(responseType)) {
            setJsonResponseMsg(model, WebConstants.ResponseMessageType.Info, TargetCustomerSubscriptionType.MUMS_HUB,
                    getMessageFromProperties("mumshub.subscription.already.subscribed"));
        }
        else {
            setJsonResponseMsg(model, WebConstants.ResponseMessageType.Success,
                    TargetCustomerSubscriptionType.MUMS_HUB, StringUtils.EMPTY);
            request.getSession().setAttribute(ControllerConstants.SESSION_PARAM_MUMS_HUB_SUBSCRIPTION_EMAIL,
                    subscriptionEmail);
            LOG.warn("Unsuccessful Subscription");
        }
        return ControllerConstants.Views.Fragments.Enews.ENEWS_JSON_FRAGMENT;

    }
}
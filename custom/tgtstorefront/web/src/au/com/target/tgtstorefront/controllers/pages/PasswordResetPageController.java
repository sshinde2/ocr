/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtcore.customer.ReusePasswordException;
import au.com.target.tgtfacades.customer.impl.DefaultTargetCustomerFacade;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtstorefront.breadcrumb.ResourceBreadcrumbBuilder;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.forms.ForgottenPwdForm;
import au.com.target.tgtstorefront.forms.UpdatePwdForm;
import au.com.target.tgtstorefront.security.AutoLoginStrategy;


/**
 * Controller for the forgotten password pages. Supports requesting a password reset email as well as changing the
 * password once you have got the token that was sent via email.
 */
@Controller
@RequestMapping(value = ControllerConstants.FORGOTTEN_PASSWORD)
public class PasswordResetPageController extends AbstractPageController {

    private static final Logger LOG = Logger.getLogger(PasswordResetPageController.class);

    private static final String UPDATE_PWD_CMS_PAGE = "updatePassword";

    private static final String FORGOT_PASSWORD_CMS_PAGE = "forgottenPassword";

    @Resource(name = "defaultTargetCustomerFacade")
    private DefaultTargetCustomerFacade customerFacade;

    @Resource(name = "autoLoginStrategy")
    private AutoLoginStrategy autoLoginStrategy;

    @Resource(name = "simpleBreadcrumbBuilder")
    private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @RequestMapping(method = RequestMethod.GET)
    public String getPasswordRequest(@RequestParam(required = false) final String username, final Model model)
            throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getContentPageForLabelOrId(FORGOT_PASSWORD_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(FORGOT_PASSWORD_CMS_PAGE));
        model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                resourceBreadcrumbBuilder.getBreadcrumbs("forgottenPwd.title"));

        final ForgottenPwdForm forgottenPwdForm = new ForgottenPwdForm();
        forgottenPwdForm.setEmail(username);
        model.addAttribute(forgottenPwdForm);
        return ControllerConstants.Views.Pages.Password.PASSWORD_RESET_REQUEST_PAGE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String passwordRequest(@Valid final ForgottenPwdForm form, final BindingResult bindingResult,
            @RequestParam(required = false) final String forward, final Model model,
            final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
        if (bindingResult.hasErrors()) {
            prepareErrorMessage(model, FORGOT_PASSWORD_CMS_PAGE, "form.global.error");
        }
        else {
            try {
                customerFacade.forgottenPassword(form.getEmail(), forward);
                GlobalMessages
                        .addFlashConfMessage(redirectModel, "account.confirmation.forgotten.password.link.sent");
            }
            catch (final UnknownIdentifierException unknownIdentifierException) {
                GlobalMessages
                        .addFlashConfMessage(redirectModel, "account.confirmation.forgotten.password.link.sent.onfail");
            }
            return ControllerConstants.Redirection.LOGIN;
        }
        model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                resourceBreadcrumbBuilder.getBreadcrumbs("forgottenPwd.title"));
        return ControllerConstants.Views.Pages.Password.PASSWORD_RESET_REQUEST_PAGE;
    }

    @RequestMapping(value = ControllerConstants.PASSWORD_RESET, method = RequestMethod.GET)
    public String getChangePassword(@RequestParam(required = false) final String token, final Model model)
            throws CMSItemNotFoundException, UnsupportedEncodingException {
        if (StringUtils.isBlank(token)) {
            return ControllerConstants.Redirection.HOME;
        }
        if (targetFeatureSwitchFacade.isFeatureEnabled("spa.reset.password.single.page")) {
            return ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_PASSWORD_RESET
                    + "?token=" + URLEncoder.encode(token, "UTF-8");
        }
        final UpdatePwdForm form = new UpdatePwdForm();
        form.setToken(token);
        model.addAttribute(form);
        storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_PWD_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PWD_CMS_PAGE));
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("updatePwd.title"));
        return ControllerConstants.Views.Pages.Password.PASSWORD_RESET_CHANGE_PAGE;
    }

    @RequestMapping(value = ControllerConstants.PASSWORD_RESET, method = RequestMethod.POST)
    public String changePassword(@Valid final UpdatePwdForm form, final BindingResult bindingResult, final Model model,
            final RedirectAttributes redirectModel, final HttpServletRequest request,
            final HttpServletResponse response) throws CMSItemNotFoundException {
        if (bindingResult.hasErrors()) {
            prepareErrorMessage(model, UPDATE_PWD_CMS_PAGE, "form.global.error");
            return ControllerConstants.Views.Pages.Password.PASSWORD_RESET_CHANGE_PAGE;
        }
        if (!StringUtils.isBlank(form.getToken())) {
            try {
                final String uid = customerFacade.getUidForPasswordResetToken(form.getToken());
                final String redirectPage = customerFacade.getRedirectPageForPasswordResetToken(form.getToken());
                customerFacade.updatePassword(form.getToken(), form.getPwd());
                autoLoginStrategy.login(uid, form.getPwd(), request, response);
                GlobalMessages.addFlashConfMessage(redirectModel,
                        "account.confirmation.forgotten.password.change.success");

                if (StringUtils.isNotEmpty(uid)) {
                    if (ControllerConstants.Views.PageNames.BASKET.equalsIgnoreCase(redirectPage)) {
                        customerFacade.resetRedirectPageForPasswordResetToken(uid);
                        return ControllerConstants.Redirection.CHECKOUT;
                    }
                }

                return ControllerConstants.Redirection.MY_ACCOUNT;
            }
            catch (final TokenInvalidatedException e) {
                GlobalMessages.addFlashErrorMessage(redirectModel,
                        "account.confirmation.forgotten.password.token.error");
            }
            catch (final ReusePasswordException ex) {
                prepareErrorMessage(model, UPDATE_PWD_CMS_PAGE, "account.confirmation.forgotten.password.same.error");
                return ControllerConstants.Views.Pages.Password.PASSWORD_RESET_CHANGE_PAGE;
            }
            catch (final IllegalArgumentException ex) {
                // This occurs when the token is too short, has expired, the associated user is not found
                GlobalMessages.addFlashErrorMessage(redirectModel,
                        "account.confirmation.forgotten.password.token.error");
                LOG.info("PASSWORD-RESET: exception=" + ex);
            }
        }

        return ControllerConstants.Redirection.FORGOTTEN_PASSWORD;
    }

    /**
     * Prepares the view to display an error message
     * 
     * @param model
     * @param page
     * @throws CMSItemNotFoundException
     */
    protected void prepareErrorMessage(final Model model, final String page, final String globalErrorMessageKey)
            throws CMSItemNotFoundException {
        GlobalMessages.addErrorMessage(model, globalErrorMessageKey);
        storeCmsPageInModel(model, getContentPageForLabelOrId(page));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(page));
    }
}

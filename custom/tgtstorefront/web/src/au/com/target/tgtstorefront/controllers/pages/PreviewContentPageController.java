/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.target.tgtstorefront.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.navigation.LeftNavigationMenuBuilder;


/**
 * Simple CMS Content Page controller. Used only to preview CMS Pages. The DefaultPageController is used to serve
 * generic content pages.
 */
@Controller
@RequestMapping(value = "/preview-content")
public class PreviewContentPageController extends AbstractPageController
{

    @Resource(name = "contentPageBreadcrumbBuilder")
    private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;

    @Resource(name = "leftNavigationMenuBuilder")
    private LeftNavigationMenuBuilder leftNavigationMenuBuilder;

    @RequestMapping(method = RequestMethod.GET, params = { "uid" })
    public String get(@RequestParam(value = "uid") final String cmsPageUid, final Model model)
            throws CMSItemNotFoundException
    {
        final ContentPageModel pageForRequest = getContentPageForLabelOrId(cmsPageUid);
        storeCmsPageInModel(model, getCmsPageService().getPageForId(cmsPageUid));
        setUpMetaDataForContentPage(model, pageForRequest);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                contentPageBreadcrumbBuilder.getBreadcrumbs(pageForRequest));
        model.addAttribute(WebConstants.LEFT_NAV_ITEMS_KEY,
                leftNavigationMenuBuilder.getLeftNavigationMenu(pageForRequest));
        return getViewForPage(pageForRequest);
    }
}

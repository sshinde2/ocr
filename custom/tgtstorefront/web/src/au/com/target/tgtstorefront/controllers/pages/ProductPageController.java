/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.pages;


import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.catalog.model.KeywordModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.BaseOptionData;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.util.CookieGenerator;

import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.querybuilder.TargetProductDetailPageEndecaQueryBuilder;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtcore.jalo.TargetColourVariantProduct;
import au.com.target.tgtcore.jalo.TargetSizeVariantProduct;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.config.TargetSharedConfigFacade;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.look.TargetLookPageFacade;
import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.payment.AfterpayConfigFacade;
import au.com.target.tgtfacades.payment.data.AfterpayConfigData;
import au.com.target.tgtfacades.payment.environment.data.EnvironmentData;
import au.com.target.tgtfacades.product.TargetProductListerFacade;
import au.com.target.tgtfacades.product.data.AbstractTargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtfacades.response.data.ProductStockSearchResultResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.stock.TargetStockLookUpFacade;
import au.com.target.tgtfacades.storefinder.TargetStoreFinderStockFacade;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtfacades.util.TargetProductDataHelper;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.breadcrumb.impl.ProductBreadcrumbBuilder;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.data.SchemaData;
import au.com.target.tgtstorefront.controllers.util.EndecaQueryResultsHelper;
import au.com.target.tgtstorefront.controllers.util.ProductDataHelper;
import au.com.target.tgtstorefront.data.TargetProductDetailColourData;
import au.com.target.tgtstorefront.data.TargetProductDetailSizeData;
import au.com.target.tgtstorefront.forms.AddToCartForm;
import au.com.target.tgtstorefront.util.MetaSanitizerUtil;
import au.com.target.tgtstorefront.util.SearchQuerySanitiser;
import au.com.target.tgtstorefront.variants.VariantSortStrategy;
import au.com.target.tgtstorefront.variants.impl.TargetCustomVariantSortStrategy;
import au.com.target.tgtutility.util.JsonConversionUtil;
import au.com.target.tgtutility.util.TargetDateUtil;
import au.com.target.tgtutility.util.TargetProductUtils;
import au.com.target.tgtwebcore.constants.TgtwebcoreConstants;
import au.com.target.tgtwebcore.enums.CachedPageType;
import au.com.target.tgtwebcore.servicelayer.services.TargetCMSPageService;
import au.com.target.tgtwebcore.userreviews.TargetUserReviewService;
import au.com.target.tgtwebcore.userreviews.data.TargetUserReviewRequest;
import au.com.target.tgtwebcore.userreviews.data.TargetUserReviewResponse;


/**
 * Controller for product details page
 */
@Controller
@RequestMapping(value = "/p/**/**")
public class ProductPageController extends AbstractProductPageController {
    /**
     * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
     * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
     * the issue and future resolution.
     */
    private static final String PRODUCT_CODE_PATH_VARIABLE_PATTERN = "/{productCode:.*}";

    private static final String PRODUCT_LOOKS = "tgtstorefront.product.looks.maximium.size";

    private static final String MIN_LEVEL_INTEREST = "urgencyToPurchase.minLevelOfInterest";

    private static final String MAX_LEVEL_INTEREST = "urgencyToPurchase.maxLevelOfInterest";

    private static final String MIN_PRICE_THRESHOLD = "urgencyToPurchase.minPriceThreshold";

    private static final String EXCLUDE_CATEGORY = "urgencyToPurchase.excludedCategory";

    private static final String ERR_URGENCYTOPURCHASE = "Unable to populate total interest or max threshold interest for product productcode={0}";

    private static final String ERR_STOCK_VIS_COOKIE = "Unable to store cookie for the location={0}";


    @Resource(name = "productModelUrlResolver")
    private UrlResolver<ProductModel> productModelUrlResolver;

    @Resource(name = "commerceCategoryService")
    private CommerceCategoryService commerceCategoryService;

    @Resource(name = "productBreadcrumbBuilder")
    private ProductBreadcrumbBuilder productBreadcrumbBuilder;

    @Resource(name = "categoryConverter")
    private Converter<CategoryModel, CategoryData> categoryConverter;

    @Resource(name = "categoryDataUrlResolver")
    private UrlResolver<CategoryData> categoryDataUrlResolver;

    @Resource(name = "cmsPageService")
    private TargetCMSPageService cmsPageService;

    @Resource(name = "variantSortStrategy")
    private VariantSortStrategy variantSortStrategy;

    @Resource(name = "targetCustomVariantSortStrategy")
    private TargetCustomVariantSortStrategy targetCustomVariantSortStrategy;

    @Resource(name = "cmsComponentService")
    private CMSComponentService cmsComponentService;

    @Resource(name = "commonI18NService")
    private CommonI18NService commonI18NService;

    @Resource(name = "targetCheckoutFacade")
    private TargetCheckoutFacade targetCheckoutFacade;

    @Resource(name = "targetDeliveryModeHelper")
    private TargetDeliveryModeHelper targetDeliveryModeHelper;

    @Resource(name = "targetBazaarVoiceService")
    private TargetUserReviewService targetBazaarVoiceService;

    @Resource(name = "targetLookPageFacade")
    private TargetLookPageFacade targetLookPageFacade;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Resource(name = "salesApplicationFacade")
    private SalesApplicationFacade salesApplicationFacade;

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Value("#{configurationService.configuration.getString('tgtstorefront.preview.redirect.page')}")
    private String previewRedirectPageId;

    @Value("#{configurationService.getConfiguration().getInt('tgtstorefront.urgencytopurchase.productlist.maxSize', 1)}")
    private int maxProductSizeEndeca;

    @Value("#{configurationService.configuration.getProperty('tgtstorefront.host.fqdn')}")
    private String fqdn;

    @Value("#{configurationService.configuration.getProperty('tgtstorefront.bazaarvoice.api.uri')}")
    private String bazaarVoiceApiUri;

    @Value("#{configurationService.configuration.getProperty('tgtstorefront.google.maps.key')}")
    private String gMapsApiKey;

    @Value("#{configurationService.configuration.getProperty('tgtstorefront.addthis.pubid')}")
    private String addThisClientId;

    @Value("#{configurationService.configuration.getProperty('tgtstorefront.addthis.container')}")
    private String addThisContainer;

    @Resource(name = "targetSharedConfigFacade")
    private TargetSharedConfigFacade targetSharedConfigFacade;

    @Resource(name = "targetProductDetailPageEndecaQueryBuilder")
    private TargetProductDetailPageEndecaQueryBuilder targetProductDetailPageEndecaQueryBuilder;

    @Resource(name = "endecaQueryResultsHelper")
    private EndecaQueryResultsHelper endecaQueryResultsHelper;

    @Resource(name = "storeStockPostCodeCookieGenerator")
    private CookieGenerator storeStockPostCodeCookieGenerator;

    @Resource
    private TargetStoreFinderStockFacade targetStoreFinderStockFacade;

    @Resource
    private SearchQuerySanitiser inStoreStockSearchQuerySanitiser;

    @Resource
    private AfterpayConfigFacade afterpayConfigFacade;

    @Resource(name = "targetProductListerFacade")
    private TargetProductListerFacade targetProductListerFacade;

    @Resource(name = "targetStockLookupFacade")
    private TargetStockLookUpFacade targetStockLookupFacade;

    @Resource(name = "targetStoreLocatorFacade")
    private TargetStoreLocatorFacade storeLocatorFacade;


    @RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    public String productDetail(@PathVariable("productCode") final String productCode, final Model model,
            final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "bbsn", required = false) final String bulkyBoardStoreNumber)
            throws CMSItemNotFoundException,
            UnsupportedEncodingException {
        ProductModel productModel = getProductService().getProductForCode(productCode);

        final String redirection = checkForRedirect(request, response, productModel);
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }

        productModel = getProductModelToRedirect(productCode, productModel);

        updatePageTitle(productModel, model);
        storeCmsPageInModel(model, getPageForProduct(productModel));
        populateDisplayOnlyFlagForProduct(productModel, model);
        populateProductDetailForDisplay(productModel, model, request, bulkyBoardStoreNumber);
        // Get the recommended product details if there is no related products for the current product
        if (CollectionUtils.isEmpty(productModel.getProductReferences())) {
            populateRecommendedProductsDetails(request, EndecaConstants.EndecaPageUris.ENDECA_PRODUCT_PAGE_URI);
        }

        final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(getProductKeywords(productModel));
        final String metaDescription = MetaSanitizerUtil.sanitizeDescription(getProductDescription(productModel));
        setUpMetaData(model, metaKeywords, metaDescription);

        populateAvailableLooks(model, productModel);

        if (!populateUserReviewSeoContent(productModel, model, request)) {
            addAkamaiCacheAttributes(CachedPageType.PRODUCT, response, model);
        }

        populateDetailsForReact(model, productCode, productModel, bulkyBoardStoreNumber);
        populateAfterPayConfig(model);
        populateAfterPayModal(model);
        model.addAttribute("zipPaymentThresholdAmount", getZipPaymentThresholdAmount());
        populateZipPaymentModal(model);


        return getViewForPage(model);
    }

    /**
     * This method will populate set the listerdatta as json in model
     * 
     * @param model
     * @param targetProductListerData
     */
    protected void populateProductListerData(final Model model, final TargetProductListerData targetProductListerData) {
        if (targetProductListerData != null) {
            model.addAttribute("targetProductListerData",
                    JsonConversionUtil.convertToJsonString(targetProductListerData));
        }

    }

    /**
     * Fetch the product details for the corresponding productCode and populate it in the lister data
     * 
     * @param productCode
     * @return TargetProductListerData
     */
    private TargetProductListerData getProductListerData(final String productCode) {
        final List<ProductOption> productOptions = new ArrayList<>();
        productOptions.add(ProductOption.BASIC);
        productOptions.add(ProductOption.VARIANT_FULL);
        productOptions.add(ProductOption.DELIVERY_MODE_AVAILABILITY);
        productOptions.add(ProductOption.DELIVERY_PROMOTIONAL_MESSAGE);
        productOptions.add(ProductOption.BULKY_PRODUCT_DETAIL);

        final TargetProductListerData targetProductListerData = targetProductListerFacade
                .getBaseProductDataByCode(productCode, productOptions);
        return targetProductListerData;
    }

    /**
     * @param model
     */
    protected void populateAfterPayConfig(final Model model) {
        if (targetFeatureSwitchFacade.isAfterpayEnabled()) {
            model.addAttribute("afterpayConfig", afterpayConfigFacade.getAfterpayConfig());
        }
    }

    protected void populateEnvironmentData(final Model model, final TargetPointOfServiceData bulkyBoardStore) {
        final EnvironmentData environmentData = new EnvironmentData();

        environmentData.setBazaarVoiceApiUri(bazaarVoiceApiUri);
        environmentData.setFqdn(fqdn);
        environmentData.setAddThisClientId(addThisClientId);
        environmentData.setAddThisContainer(addThisContainer);
        environmentData.setgMapsApiKey(gMapsApiKey);

        if (SalesApplication.KIOSK.equals(salesApplicationFacade.getCurrentSalesApplication())) {
            environmentData.setIsKioskMode(true);
            if (null != bulkyBoardStore) {
                environmentData.setBbStoreNumber(bulkyBoardStore.getStoreNumber());
                environmentData.setBbStoreName(bulkyBoardStore.getName());
            }
        }
        environmentData.setPreOrderDeposit(populatePreOrderDeposit());
        if (targetFeatureSwitchFacade.isAfterpayEnabled()) {
            populateAfterPayEnvironmentData(environmentData);
        }

        model.addAttribute("environmentData", JsonConversionUtil.convertToJsonString(environmentData));
    }

    /**
     * Method to populate shop the look data for react framework.
     * 
     * Here we are nullifying collectionId and instagramUrl, because these two attributes are not required as part PDP
     * re-design by front end.
     * 
     * @param model
     * @param productModel
     */
    protected void populateShopTheLookData(final Model model, final ProductModel productModel) {

        final List<LookDetailsData> shopTheLookDataList = shopTheLookDataList(productModel);
        if (CollectionUtils.isNotEmpty(shopTheLookDataList)) {
            for (final LookDetailsData shopTheLookData : shopTheLookDataList) {
                shopTheLookData.setCollectionId(null);
                shopTheLookData.setInstagramUrl(null);
            }
            model.addAttribute("shopTheLookData", JsonConversionUtil.convertToJsonString(shopTheLookDataList));
        }

    }

    /**
     * @param productModel
     * @return shopTheLookDataList
     */
    private List<LookDetailsData> shopTheLookDataList(final ProductModel productModel) {

        final ProductModel baseProduct = TargetProductDataHelper.getBaseProduct(productModel);
        final int maxSize = configurationService.getConfiguration().getInt(PRODUCT_LOOKS);

        final List<LookDetailsData> shopTheLookDataList = targetLookPageFacade.getActiveLooksForProduct(baseProduct,
                maxSize);

        return shopTheLookDataList;
    }

    /**
     * Method is used to set Afterpay environment data.
     * 
     * @return environmentData
     */
    private EnvironmentData populateAfterPayEnvironmentData(final EnvironmentData environmentData) {

        final AfterpayConfigData afterpayConfigData = afterpayConfigFacade.getAfterpayConfig();

        if (afterpayConfigData != null) {

            afterpayConfigData.setPaymentType(null);
            afterpayConfigData.setDescription(null);

            environmentData.setAfterpayConfigData(afterpayConfigData);
        }

        return environmentData;
    }

    /**
     * @param model
     */
    protected void populateAfterPayModal(final Model model) {
        if (targetFeatureSwitchFacade.isAfterpayEnabled()) {
            try {
                final ContentPageModel page = cmsPageService.getPageForLabelOrId(TgtwebcoreConstants.AFTERPAY_MODAL_ID);
                model.addAttribute("afterpayModal", page.getLabel());
            }
            catch (final CMSItemNotFoundException e) {
                model.addAttribute("afterpayModal", "");
            }
        }
    }

    /**
     * Populate the available looks for it's co-variant products which belongs to the same base product
     * 
     * @param model
     * @param productModel
     */
    protected void populateAvailableLooks(final Model model, final ProductModel productModel) {

        final List<LookDetailsData> lookDetailsList = shopTheLookDataList(productModel);

        model.addAttribute("lookDetailsData", lookDetailsList);
    }


    /**
     * @param productModel
     * @return actual product description is not blank or description from base product
     */
    private String getProductDescription(final ProductModel productModel) {
        if (!StringUtils.isBlank(productModel.getDescription())) {
            return productModel.getDescription();
        }
        else if (productModel instanceof VariantProductModel) {
            final ProductModel baseProduct = ((VariantProductModel)productModel).getBaseProduct();
            return baseProduct.getDescription();
        }
        return StringUtils.EMPTY;
    }

    /**
     * @param productModel
     * @return actual product keywords if not empty or keywords from base product
     */
    private List<KeywordModel> getProductKeywords(final ProductModel productModel) {
        if (!CollectionUtils.isEmpty(productModel.getKeywords())) {
            return productModel.getKeywords();
        }
        else if (productModel instanceof VariantProductModel) {
            final ProductModel baseProduct = ((VariantProductModel)productModel).getBaseProduct();
            return baseProduct.getKeywords();
        }
        return ListUtils.EMPTY_LIST;
    }

    /*
     * Used by JavaScript to load the PDP in a modal
     */
    @RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/quickView", method = RequestMethod.GET)
    public String showQuickView(@PathVariable("productCode") final String productCode, final Model model,
            final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "bbsn", required = false) final String bulkyBoardStoreNumber)

    {
        ProductModel productModel = getProductService().getProductForCode(productCode);

        getPreviewRedirect(productModel, false);

        productModel = getProductModelToRedirect(productCode, productModel);
        populateDisplayOnlyFlagForProduct(productModel, model);
        populateProductDetailForDisplay(productModel, model, request, bulkyBoardStoreNumber);
        // Avoid cache in PDP modal IE
        response.setHeader("Cache-Control", "no-cache");
        populateAfterPayConfig(model);
        populateAfterPayModal(model);
        model.addAttribute("zipPaymentThresholdAmount", getZipPaymentThresholdAmount());
        populateZipPaymentModal(model);
        populateDetailsForReact(model, productCode, productModel, bulkyBoardStoreNumber);

        return ControllerConstants.Views.Fragments.Product.QUICK_VIEW_POPUP;
    }

    /*
     * Used by JavaScript to fully detailed PDP markup
     */
    @RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/seriesView", method = RequestMethod.GET)
    public String showDetailedFragment(@PathVariable("productCode") final String productCode, final Model model,
            final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "bbsn", required = false) final String bulkyBoardStoreNumber)

    {
        ProductModel productModel = getProductService().getProductForCode(productCode);
        getPreviewRedirect(productModel, false);
        productModel = getProductModelToRedirect(productCode, productModel);
        populateDisplayOnlyFlagForProduct(productModel, model);
        populateProductDetailForDisplay(productModel, model, request, bulkyBoardStoreNumber);
        // Avoid cache in IE
        response.setHeader("Cache-Control", "no-cache");
        populateAfterPayConfig(model);
        populateAfterPayModal(model);
        populateZipPaymentModal(model);
        model.addAttribute("zipPaymentThresholdAmount", getZipPaymentThresholdAmount());
        populateDetailsForReact(model, productCode, productModel, bulkyBoardStoreNumber);

        return ControllerConstants.Views.Fragments.Product.SERIES_VIEW;
    }

    /*
     * Used by JavaScript to Ajax the variant detail content
     */
    @RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/partial", method = RequestMethod.GET)
    public String showProductVariantContent(@PathVariable("productCode") final String productCode, final Model model,
            final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "bbsn", required = false) final String bulkyBoardStoreNumber)

    {
        ProductModel productModel = getProductService().getProductForCode(productCode);
        productModel = getProductModelToRedirect(productCode, productModel);
        populateDisplayOnlyFlagForProduct(productModel, model);
        populateProductDetailForPartial(productModel, model, request, bulkyBoardStoreNumber);
        response.setHeader("Cache-Control", "no-cache");
        return ControllerConstants.Views.Fragments.Product.VARIANT_PARTIAL_DETAIL;
    }


    /*
     * Used by JavaScript to Ajax the variant content
     */
    @RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/variants", method = RequestMethod.GET)
    public String showProductVariants(@PathVariable("productCode") final String productCode, final Model model,
            final HttpServletResponse response) {
        ProductModel productModel = getProductService().getProductForCode(productCode);
        productModel = getProductModelToRedirect(productCode, productModel);
        populateProductVariantDetailForDisplay(productModel, model);
        response.setHeader("Cache-Control", "no-cache");
        populateDetailsForReact(model, productCode, productModel, null);
        return ControllerConstants.Views.Fragments.Product.VARIANT_PARTIAL;
    }

    @RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/stock", method = RequestMethod.GET)
    public String getStockForVariants(@PathVariable("productCode") final String productCode, final Model model,
            @RequestParam(value = "sn", required = false) final Integer storeNumber, final HttpServletRequest request) {
        final List<ProductData> variants;
        if (targetFeatureSwitchFacade.isFluentStockLookupEnabled()) {
            variants = getTargetProductFacade().getFluentStock(productCode,
                    storeNumber == null ? null : storeNumber.toString());
        }
        else {
            if (storeNumber == null) {
                //fetch both online stock and consolidated stock
                variants = getTargetProductFacade().getAllSellableVariantsWithOptions(productCode,
                        Arrays.asList(ProductOption.STOCK, ProductOption.CONSOLIDATED_STOCK));
            }
            else {
                //fetch store stock and consolidated stock
                variants = getTargetProductFacade().getAllSellableVariantsWithStoreStockAndOptions(productCode,
                        storeNumber, Arrays.asList(ProductOption.CONSOLIDATED_STOCK));
            }
        }
        if (storeNumber == null) {
            final boolean isAvailableOnlineOrStore = populateOverallStockFlag(model, variants);
            if (!isAvailableOnlineOrStore && targetFeatureSwitchFacade.isProductEndOfLifeEnabled()) {
                request.setAttribute(ControllerConstants.REQUEST_PARAM_NOTAVAILABLE_ONLINE_AND_INSTORE, Boolean.TRUE);
                final ProductModel productModel = getProductService().getProductForCode(productCode);
                final TargetProductData productData = (TargetProductData)getTargetProductFacade()
                        .getProductForOptions(productModel, Arrays.asList(ProductOption.PRODUCT_END_OF_LIFE_LINKS));
                model.addAttribute("productEndOfLifeLinks", productData.getProductEndLinks());
            }
        }
        model.addAttribute("availabilityTime", TargetDateUtil.getCurrentFormattedDate());
        model.addAttribute("variantsStock", variants);
        return ControllerConstants.Views.Fragments.Product.VARIANT_STOCK;
    }

    /**
     * Populates the stocklevel and overall stock level for the product
     * 
     * @param productCode
     * @param model
     */
    public void getStockForVariantsDisplay(final String productCode, final Model model) {
        final List<ProductData> variants;
        if (targetFeatureSwitchFacade.isFluentStockLookupEnabled()) {
            variants = getTargetProductFacade().getFluentStock(productCode, null);
        }
        else {
            variants = getTargetProductFacade().getAllSellableVariantsWithOptions(productCode,
                    Arrays.asList(ProductOption.STOCK, ProductOption.CONSOLIDATED_STOCK));
        }
        model.addAttribute("variantsStock", variants);
        populateOverallStockFlag(model, variants);
    }

    /**
     * @param productModel
     * @param model
     */
    private void populateDisplayOnlyFlagForProduct(final ProductModel productModel, final Model model) {
        if (productModel instanceof TargetProductModel) {
            final TargetProductModel baseProductModel = (TargetProductModel)productModel;
            model.addAttribute("isProductDisplayOnly", baseProductModel.getDisplayOnly());
            return;
        }
        final VariantProductModel variant = (VariantProductModel)productModel;
        populateDisplayOnlyFlagForProduct(variant.getBaseProduct(), model);
    }

    private boolean populateOverallStockFlag(final Model model, final List<ProductData> variants) {
        boolean availableOnline = false;
        boolean availableInStore = false;
        boolean availableForStore = false;
        for (final ProductData variant : variants) {
            if (availableOnline && availableForStore && availableInStore) {
                break;
            }
            final TargetProductData productData = (TargetProductData)variant;
            final StockData onlineStock = productData.getStock();
            final StockData storeStock = productData.getConsolidatedStoreStock();

            if (!availableOnline && TargetProductUtils.checkIfStockDataHasStock(onlineStock)) {
                availableOnline = true;
            }

            if (!availableForStore && TargetProductUtils.checkIfStockDataExists(storeStock)) {
                availableForStore = true;
            }

            if (!availableInStore && TargetProductUtils.checkIfStockDataHasStock(storeStock)) {
                availableInStore = true;
            }
        }
        model.addAttribute("availableOnline", Boolean.valueOf(availableOnline));
        model.addAttribute("availableForStore", Boolean.valueOf(availableForStore));
        model.addAttribute("availableInStore", Boolean.valueOf(availableInStore));
        setMetaRobots(model, availableOnline, availableInStore);

        return (availableOnline || availableInStore);
    }

    /*
     * Api to get Json Data for content pages
     */
    @RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN
            + "/json", method = RequestMethod.GET, produces = "application/json")
    public String getProductJson(@PathVariable("productCode") final String productCode, final Model model,
            final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "bbsn", required = false) final String bulkyBoardStoreNumber) {
        ProductModel productModel = getProductService().getProductForCode(productCode);

        getPreviewRedirect(productModel, false);

        productModel = getProductModelToRedirect(productCode, productModel);
        populateProductDetailForDisplay(productModel, model, request, bulkyBoardStoreNumber);
        addAkamaiCacheAttributes(CachedPageType.PRODUCT, response, model);
        return ControllerConstants.Views.Fragments.Product.PRODUCT_JSON;
    }

    @RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN
            + ControllerConstants.PRODUCT_REVIEW, method = RequestMethod.GET)
    public String writeReview(@PathVariable final String productCode, final Model model)
            throws CMSItemNotFoundException {
        final ProductModel productModel = getProductService().getProductForCode(productCode);

        getPreviewRedirect(productModel, false);

        updatePageTitle(productModel, model);

        final ProductData productData = getTargetProductFacade().getProductForOptions(productModel, Arrays.asList(
                ProductOption.BASIC, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION,
                ProductOption.CATEGORIES, ProductOption.PROMOTIONS, ProductOption.REVIEW));

        if (targetFeatureSwitchFacade
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_DETAIL_PANEL)) {
            model.addAttribute("bazaarVoiceApiUri", bazaarVoiceApiUri);
        }
        model.addAttribute("productData", productData);
        storeCmsPageInModel(model, getPageForProduct(productModel));
        return ControllerConstants.Views.Pages.Product.REVIEW;
    }

    /**
     * Method returns product's stock levels sorted by distance from specific location passed by free-text parameter or
     * longitude and latitude parameters.<br>
     * There are two set of parameters available :
     * <ul>
     * <li>location (Required), currentPage (Optional), pageSize(Optional) or</li>>
     * <li>longitude (Required), latitude (Required), currentPage (Optional), pageSize(Optional).</li>
     * </ul>
     *
     * @param location
     *            Free-text location
     * @param latitude
     *            Longitude location parameter.
     * @param longitude
     *            Latitude location parameter.
     * @param currentPage
     *            The current result page requested.
     * @return response with product's stock levels
     * 
     */
    @RequestMapping(value = "/{productCode}/store/stock", method = RequestMethod.GET)
    @ResponseBody
    public Response searchProductStockByLocation(@PathVariable final String productCode,
            @RequestParam(required = false) final String location,
            @RequestParam(required = false) final Double latitude,
            @RequestParam(required = false) final Double longitude,
            @RequestParam(required = false, defaultValue = "0") final int currentPage,
            final HttpServletResponse httpResponse) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getProductStockByLocation: code=" + productCode + " | location=" + location + " | latitude="
                    + latitude
                    + " | longitude=" + longitude);
        }
        String sanitisedLocation = null;
        if (StringUtils.isNotEmpty(location)) {
            sanitisedLocation = inStoreStockSearchQuerySanitiser.sanitiseSearchText(location);
        }
        final PageableData pageableData = createPageableData();
        pageableData.setPageSize(configurationService.getConfiguration().getInt("storefront.storelocator.pageSize", 5));
        pageableData.setCurrentPage(currentPage);
        final Response response = targetStoreFinderStockFacade.doSearchProductStock(productCode, sanitisedLocation,
                latitude,
                longitude, null, pageableData, true);
        try {
            if (response.isSuccess() && StringUtils.isNotEmpty(sanitisedLocation)) {
                final ProductStockSearchResultResponseData responseData = (ProductStockSearchResultResponseData)response
                        .getData();
                final String sanitisedSelectedLocation = inStoreStockSearchQuerySanitiser
                        .sanitiseSearchText(responseData
                                .getSelectedLocation());
                storeStockPostCodeCookieGenerator
                        .addCookie(httpResponse,
                                URIUtil.encodePath(sanitisedSelectedLocation, "utf-8"));
            }
        }
        catch (final URIException ex) {
            LOG.error(MessageFormat.format(ERR_STOCK_VIS_COOKIE, sanitisedLocation));
        }
        return response;
    }

    @RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/store", method = RequestMethod.GET)
    public String findStore(@PathVariable("productCode") final String productCode,
            @RequestParam(required = false) final boolean fias,
            final Model model) {
        final boolean locationServicesEnabled = targetFeatureSwitchFacade
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.LOCATION_SERVICES);
        if (fias) {
            final ProductData productData = getTargetProductFacade().getProductForCodeAndOptions(productCode,
                    Arrays.asList(ProductOption.BASIC));
            model.addAttribute("product", productData);

            model.addAttribute("isFias", Boolean.TRUE);

            if (locationServicesEnabled) {
                populateEnvironmentData(model, null);
                model.addAttribute("productJson", JsonConversionUtil.convertToJsonString(productData));
            }
        }

        return ControllerConstants.Views.Fragments.Product.FIND_NEAREST_STORE;
    }

    @ExceptionHandler(UnknownIdentifierException.class)
    public String handleUnknownIdentifierException(final UnknownIdentifierException exception,
            final HttpServletRequest request) {
        request.setAttribute("message", exception.getMessage());
        request.setAttribute("notFoundPageType", PageType.Product);
        return ControllerConstants.Forward.ERROR_404;
    }

    protected void updatePageTitle(final ProductModel productModel, final Model model) {
        storeContentPageTitleInModel(model, getPageTitleResolver()
                .resolveProductPageTitle(productModel));
    }

    protected void populateProductVariantDetailForDisplay(final ProductModel productModel, final Model model) {
        final TargetProductData productData = (TargetProductData)getTargetProductFacade()
                .getProductDataForOptions(productModel, Arrays.asList(ProductOption.BASIC, ProductOption.VARIANT_FULL));
        populateProductVariant(model, productData);
    }

    protected void populateProductDetailForDisplay(final ProductModel productModel, final Model model,
            final HttpServletRequest request, final String storeNumber) {

        final TargetProductData productData = fetchProductData(productModel, model, request,
                storeNumber, true);

        final List<Breadcrumb> breadcrumbs = productBreadcrumbBuilder.getBreadcrumbs(productModel);

        model.addAttribute(new AddToCartForm());
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);

        // Note: This is the index of the category above the product's
        //       supercategory
        int productSuperSuperCategoryIndex = breadcrumbs.size() - 3;
        final List<CategoryData> superCategories = new ArrayList<>();

        if (productSuperSuperCategoryIndex == 0) {
            // The category at index 0 is never displayed as a supercategory; for
            // display purposes, the category at index 1 is the root category 
            productSuperSuperCategoryIndex = 1;
        }

        if (productSuperSuperCategoryIndex > 0) {
            // When product has any supercategory
            final Breadcrumb productSuperSuperCategory = breadcrumbs.get(productSuperSuperCategoryIndex);
            final CategoryModel superSuperCategory = commerceCategoryService
                    .getCategoryForCode(productSuperSuperCategory
                            .getCategoryCode());


            for (final CategoryModel superCategory : superSuperCategory.getCategories()) {
                final CategoryData categoryData = new CategoryData();

                categoryData.setName(superCategory.getName());
                categoryData.setUrl(categoryDataUrlResolver.resolve(categoryConverter.convert(superCategory)));

                superCategories.add(categoryData);
            }
        }
        if (targetFeatureSwitchFacade.isUrgenyToPurchasePDPEnabled()) {
            final List<TargetProductListerData> endecaProductDataList = getProductDetailsFromEndeca(productData
                    .getBaseProductCode());
            if (CollectionUtils.isNotEmpty(endecaProductDataList)) {
                populateCustomerTotalInterestNumberForPrd(productData, endecaProductDataList.get(0));
            }
        }
        addCMSComponents(productData, model);

        model.addAttribute(WebConstants.SUPERCATEGORIES_KEY, superCategories);
    }

    /**
     * @param productModel
     * @param model
     * @param request
     * @param storeNumber
     * @param populateAll
     * @return {@link TargetProductData}
     */
    protected TargetProductData fetchProductData(final ProductModel productModel, final Model model,
            final HttpServletRequest request, final String storeNumber, final boolean populateAll) {

        final List<ProductOption> options = new ArrayList<>();
        options.addAll(Arrays.asList(ProductOption.BASIC,
                ProductOption.SUMMARY, ProductOption.DESCRIPTION, ProductOption.GALLERY, ProductOption.CATEGORIES,
                ProductOption.REVIEW, ProductOption.PROMOTIONS, ProductOption.CLASSIFICATION,
                ProductOption.VARIANT_FULL, ProductOption.DELIVERY_PROMOTIONAL_MESSAGE, ProductOption.DELIVERY_ZONE));
        if (!targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.PDP_OPTIMISATION)) {
            options.add(ProductOption.PRICE);
        }
        if (!populateAll) {
            if (targetFeatureSwitchFacade.isFluentStockLookupEnabled()) {
                final List<ProductData> variants = getTargetProductFacade().getFluentStock(productModel, storeNumber);
                model.addAttribute("variantsStock", variants);
            }
            else {
                options.add(ProductOption.STOCK);
                options.add(ProductOption.CONSOLIDATED_STOCK);
            }
        }
        final TargetProductData productData = (TargetProductData)getTargetProductFacade()
                .getBulkyBoardProductForOptions(productModel, options, storeNumber);

        populateProductData(productModel, model, request, productData, populateAll);
        populateProductStockFromVariants(productData, model);

        return productData;
    }


    /**
     * @param productData
     * @param model
     */
    protected void populateProductStockFromVariants(final TargetProductData productData, final Model model) {
        final List<TargetProductData> variants = (List<TargetProductData>)model.asMap().get("variantsStock");
        if (CollectionUtils.isEmpty(variants)) {
            return;
        }

        boolean stockSet = false;
        for (final TargetProductData variant : variants) {
            if (variant.getCode() != null && variant.getCode().equalsIgnoreCase(productData.getCode())) {
                stockSet = true;
                setProductStockFromVariant(productData, variant);
                break;
            }
            if (!variant.isInStock()) {
                continue;
            }
            stockSet = true;
            setProductStockFromVariant(productData, variant);
        }
        if (!stockSet) {
            setEmptyStock(productData);
        }
    }

    /**
     * @param productData
     */
    protected void setEmptyStock(final TargetProductData productData) {
        final StockData stockData = new StockData();
        stockData.setStockLevel(Long.valueOf(0));
        stockData.setStockLevelStatus(StockLevelStatus.OUTOFSTOCK);
        productData.setStock(stockData);
    }

    /**
     * @param productData
     * @param variant
     */
    protected void setProductStockFromVariant(final AbstractTargetProductData productData,
            final AbstractTargetProductData variant) {
        productData.setInStock(variant.isInStock());
        productData.setStock(variant.getStock());
        productData.setAvailable(variant.getAvailable());
    }

    protected void populateProductDetailForPartial(final ProductModel productModel, final Model model,
            final HttpServletRequest request, final String storeNumber) {

        final TargetProductData productData = fetchProductData(productModel, model, request,
                storeNumber, false);

        model.addAttribute(new AddToCartForm());

        addCMSComponents(productData, model);
    }

    /**
     * This method populates the totalInterest and maxThresholdinterest for a product in the productData
     * 
     * @param productData
     */
    protected void populateCustomerTotalInterestNumberForPrd(final AbstractTargetProductData productData,
            final TargetProductListerData endecaProductData) {

        //check if the product category is excluded in the category list,if so then don't populate totalinterest
        //Also don't proceed if the product is a gift card
        if (isCategoryExcluded(productData) || productData.isGiftCard()) {
            return;
        }
        //Exit if the list is empty
        if (null == endecaProductData) {
            return;
        }
        //There will be always one productList
        final PriceData priceData = populatePrice(endecaProductData);
        //read the threshold price from hybris shared config
        final String price = targetSharedConfigFacade.getConfigByCode(MIN_PRICE_THRESHOLD);
        final BigDecimal configuredPriceThreshold = StringUtils.isNotEmpty(price) ? new BigDecimal(price)
                : new BigDecimal(0);
        //Exit  if all variants are unavailable and the price is less than threshold or price is null
        if (!(endecaProductData.isInStock()) || (null == priceData)
                || (priceData.getValue().compareTo(configuredPriceThreshold) < 0)) {
            return;
        }
        try {

            final String minThresholdInterest = targetSharedConfigFacade
                    .getConfigByCode(MIN_LEVEL_INTEREST);
            final String totalInterestStr = endecaProductData.getTotalInterest();
            final String maxThresholdInterest = targetSharedConfigFacade
                    .getConfigByCode(MAX_LEVEL_INTEREST);
            final Integer configThresholdInterest = minThresholdInterest != null ? Integer
                    .valueOf(minThresholdInterest) : Integer.valueOf(0);
            final int totalInterest = totalInterestStr != null ? Integer
                    .parseInt(totalInterestStr) : 0;
            if (Integer.valueOf(totalInterest).compareTo(configThresholdInterest) >= 0) {
                if (productData instanceof TargetProductData) {
                    ((TargetProductData)productData).setTotalInterest(totalInterest);
                    ((TargetProductData)productData).setMaxThresholdInterest(
                            maxThresholdInterest != null ? Integer.parseInt(maxThresholdInterest) : 0);
                }
                else if (productData instanceof TargetProductListerData) {
                    ((TargetProductListerData)productData).setTotalInterest(String.valueOf(totalInterest));
                    ((TargetProductListerData)productData).setMaxThresholdInterest(
                            maxThresholdInterest != null ? Integer.parseInt(maxThresholdInterest) : 0);
                }
            }
        }
        catch (final NumberFormatException ex) {
            LOG.error(MessageFormat.format(ERR_URGENCYTOPURCHASE, productData.getBaseProductCode()));
        }
    }

    /**
     * Check whether the product category is in excluded list
     * 
     * @param data
     * @return boolean
     */
    protected boolean isCategoryExcluded(final AbstractTargetProductData data) {
        final String configuredCategory = targetSharedConfigFacade.getConfigByCode(EXCLUDE_CATEGORY);
        //return false if the configured category is empty
        if (StringUtils.isEmpty(configuredCategory)) {
            return false;
        }
        final List<String> categoryList = Arrays.asList(configuredCategory.split(","));
        for (final String category : categoryList) {
            if (category.equalsIgnoreCase(data.getTopLevelCategory())) {
                return true;
            }
        }
        return false;
    }



    /**
     * Populates endeca search state data
     * 
     * @param productCode
     * @return EndecaSearchStateData
     */
    protected EndecaSearchStateData populateEndecaSearchStateData(final String productCode) {
        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setSkipInStockFilter(true);
        endecaSearchStateData.setSkipDefaultSort(true);
        final List recFilterOptions = new ArrayList<String>();
        recFilterOptions.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        recFilterOptions.add(EndecaConstants.EndecaRecordSpecificFields.ENDECA_SIZE_VARIANT_CODE);
        endecaSearchStateData.setRecordFilterOptions(recFilterOptions);
        final List<String> productCodes = new ArrayList<>();
        productCodes.add(productCode);
        endecaSearchStateData.setMatchMode(EndecaConstants.MATCH_MODE_ANY);
        endecaSearchStateData.setNpSearchState(EndecaConstants.ENDECA_NP_1);
        endecaSearchStateData.setNuRollupField(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
        endecaSearchStateData.setFieldListConfig(EndecaConstants.FieldList.URGENCYTOPURCHASE_FIELDLIST);
        endecaSearchStateData.setSearchTerm(productCodes);
        return endecaSearchStateData;
    }

    /**
     * Add size object to the model, loaded from variant option
     * 
     * @param model
     * @param variantOptionDatas
     */
    protected void addSizesFromVariantOptionsToModel(final Model model,
            final List<VariantOptionData> variantOptionDatas) {
        final List<TargetProductDetailSizeData> sizes = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(variantOptionDatas)) {
            for (final VariantOptionData variantOptionData : variantOptionDatas) {
                sizes.add(new TargetProductDetailSizeData(variantOptionData));
            }
        }
        model.addAttribute(ProductDataHelper.SIZES_LIST_NAME, sizes);
    }

    /**
     * Add colours object to the model, colours are loaded from base option
     * 
     * @param model
     * @param baseOptionData
     */
    protected void addColoursFromBaseOptionToModel(final Model model, final BaseOptionData baseOptionData) {
        final List<TargetProductDetailColourData> colors = new ArrayList<>();
        for (final VariantOptionData variantOptionData : baseOptionData.getOptions()) {
            final TargetProductDetailColourData targetProductDetailColourDTO = new TargetProductDetailColourData(
                    variantOptionData);
            if (baseOptionData.getSelected().getUrl().equalsIgnoreCase(variantOptionData.getUrl())) {
                targetProductDetailColourDTO.setActive(true);
                model.addAttribute(ProductDataHelper.ACTIVE_COLOR, targetProductDetailColourDTO);
            }
            colors.add(targetProductDetailColourDTO);
        }
        model.addAttribute(ProductDataHelper.COLORS_LIST_NAME, colors);
    }

    /**
     * Add size object to the model, loaded from base option
     * 
     * @param model
     * @param baseOptionData
     */
    protected void addSizesFromBaseOptionToModel(final Model model, final BaseOptionData baseOptionData) {
        final List<TargetProductDetailSizeData> sizes = new ArrayList<>();
        for (final VariantOptionData variantOptionData : baseOptionData.getOptions()) {
            final TargetProductDetailSizeData targetProductDetailSizeDTO = new TargetProductDetailSizeData(
                    variantOptionData);
            if (baseOptionData.getSelected().getUrl().equalsIgnoreCase(variantOptionData.getUrl())) {
                targetProductDetailSizeDTO.setActive(true);
            }
            sizes.add(targetProductDetailSizeDTO);
        }
        model.addAttribute(ProductDataHelper.SIZES_LIST_NAME, sizes);
    }

    protected void sortVariantOptionData(final ProductData productData) {
        if (CollectionUtils.isNotEmpty(productData.getBaseOptions())) {
            for (final BaseOptionData baseOptionData : productData.getBaseOptions()) {
                if (CollectionUtils.isNotEmpty(baseOptionData.getOptions())) {
                    if (targetFeatureSwitchFacade.isSizeOrderingForUIEnabled()) {
                        Collections.sort(baseOptionData.getOptions(), targetCustomVariantSortStrategy);
                    }
                    else {
                        Collections.sort(baseOptionData.getOptions(), variantSortStrategy);
                    }
                }
            }
        }

        if (CollectionUtils.isNotEmpty(productData.getVariantOptions())) {
            if (targetFeatureSwitchFacade.isSizeOrderingForUIEnabled()) {
                Collections.sort(productData.getVariantOptions(), targetCustomVariantSortStrategy);
            }
            else {
                Collections.sort(productData.getVariantOptions(), variantSortStrategy);
            }
        }
    }

    protected AbstractPageModel getPageForProduct(final ProductModel product) throws CMSItemNotFoundException {
        return cmsPageService.getPageForProduct(product);
    }

    /**
     * Fetches the product details from Endeca
     * 
     * @return List
     */
    private List<TargetProductListerData> getProductDetailsFromEndeca(
            final String productCode) {
        if (StringUtils.isNotEmpty(productCode)) {
            final EndecaSearchStateData endecaSearchStateData = populateEndecaSearchStateData(
                    productCode);
            ENEQueryResults queryResults;
            try {
                queryResults = targetProductDetailPageEndecaQueryBuilder.getQueryResults(
                        endecaSearchStateData, maxProductSizeEndeca);
                final List<TargetProductListerData> productDataList = endecaQueryResultsHelper
                        .getTargetProductList(queryResults);
                return productDataList;
            }
            catch (final TargetEndecaException | ENEQueryException e) {
                LOG.error("Exception when querying endeca", e);

            }
        }
        return null;
    }

    /**
     * Populate the priceData,check if the pricerange available for the product If then consider the max price of the
     * range,if not take the price of the product
     * 
     * @param targetProductListerData
     * @return PriceData
     */
    private PriceData populatePrice(final TargetProductListerData targetProductListerData) {
        PriceData priceData = null;
        if (null != targetProductListerData.getPriceRange()
                && null != targetProductListerData.getPriceRange().getMaxPrice()) {
            priceData = targetProductListerData.getPriceRange().getMaxPrice();
        }
        else {
            priceData = targetProductListerData.getPrice();
        }
        return priceData;
    }

    /**
     * populate the User Review SEO content for search engineer
     * 
     * @param productModel
     * @param model
     * @param request
     */
    private boolean populateUserReviewSeoContent(final ProductModel productModel, final Model model,
            final HttpServletRequest request) {
        ProductModel targetProductModel = productModel;
        if (targetProductModel instanceof AbstractTargetVariantProductModel) {
            targetProductModel = getTargetProductFacade().getBaseTargetProduct(
                    (AbstractTargetVariantProductModel)targetProductModel);
        }
        final String userAgent = request.getHeader("User-Agent");
        final String baseUrl = productModelUrlResolver.resolve(targetProductModel);
        String pageUrl = baseUrl;
        final String queryString = request.getQueryString();
        if (StringUtils.isNotEmpty(queryString)) {
            pageUrl = baseUrl + "?" + queryString;
        }
        final TargetUserReviewRequest targetUserReviewRequest = new TargetUserReviewRequest();
        targetUserReviewRequest.setUserAgent(userAgent);
        targetUserReviewRequest.setPageURI(pageUrl);
        targetUserReviewRequest.setBaseURI(baseUrl);
        targetUserReviewRequest.setProduct(targetProductModel.getCode());
        final TargetUserReviewResponse targetUserReviewResponse = targetBazaarVoiceService
                .getUserReviewsContent(targetUserReviewRequest);
        boolean responseResult = false;
        if (targetUserReviewResponse != null) {
            model.addAttribute("bvSeoStatus", Boolean.valueOf(targetUserReviewResponse.getStatus()));
            model.addAttribute("bvSeoContent", targetUserReviewResponse.getContent());
            responseResult = targetUserReviewResponse.getStatus();
        }
        return responseResult;
    }

    private void addCMSComponents(final TargetProductData productData, final Model model) {

        // In Store Purchase for Bulky Board
        if (productData.getBulkyBoard() != null) {
            //Adding content for In Store Purchase icons + tooltip
            ProductDataHelper.addContentNameFromCMSComponent(ProductDataHelper.TAB_DELIVERY_IN_STORE_PURCHASE_ID,
                    commonI18NService.getLocaleForLanguage(commonI18NService.getCurrentLanguage()), model,
                    cmsComponentService);
            //Adding content for In Store Purchase content inside delivery Tab
            ProductDataHelper.addContentNameFromCMSComponent(ProductDataHelper.DELIVERY_IN_STORE_PURCHASE_ID,
                    commonI18NService.getLocaleForLanguage(commonI18NService.getCurrentLanguage()),
                    model, cmsComponentService);
        }

    }

    /**
     * @param productCode
     * @param productModel
     */
    private ProductModel getProductModelToRedirect(final String productCode, ProductModel productModel) {
        //Base Product Redirection
        String baseProductCode = productCode;
        if (targetFeatureSwitchFacade.isFeatureEnabled("productvaraint.defaultselect")) {
            productModel = getTargetProductFacade().getProductToRedirect(productModel);
        }
        else {
            if (TargetProductDataHelper.isBaseProduct(productModel)) {
                baseProductCode = ProductDataHelper.getProductCodeToRedirect(getTargetProductFacade(), productModel);
                productModel = getProductService().getProductForCode(baseProductCode);
            }
        }
        return productModel;
    }

    /**
     * Checks if a redirect should be performed for a given product based on:
     * <ul>
     * <li>The product.preview.mode feature is enabled, and</li>
     * <li>The preview flag on the product is true, and</li>
     * <li>The sales application is not the mobile app, and</li>
     * <li>The CMS page with ID 'mobileOnlyPreviewRedirect' exists.</li>
     * </ul>
     * 
     * If all of these conditions are true, the user will be taken somewhere else. Where depends on the redirect flag:
     * <ul>
     * <li>If redirect is true, redirect to the CMS page with ID 'mobileOnlyPreviewRedirect', or</li>
     * <li>If redirect is false, return a 404 response by throwing {@code UnknownIdentifierException} (handled by
     * <code>handleUnknownIdentifierException</code>).</li>
     * </ul>
     * 
     * @param product
     *            The product to check
     * @param redirect
     *            Whether a redirect is performed, or a 404 returned.
     * @return The redirect URL if the redirect conditions are met and redirect is true, blank String otherwise.
     * @throws UnknownIdentifierException
     *             If the redirect conditions are met and redirect is false.
     */
    protected String getPreviewRedirect(final ProductModel product, final boolean redirect) {
        if (!targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.PRODUCT_PREVIEW_MODE)) {
            return "";
        }

        if (product instanceof TargetProductModel) {
            if (!Boolean.TRUE.equals(((TargetProductModel)product).getPreview())) {
                return "";
            }
        }
        else if (product instanceof AbstractTargetVariantProductModel) {
            if (!Boolean.TRUE.equals(((AbstractTargetVariantProductModel)product).getPreview())) {
                return "";
            }
        }

        if (salesApplicationFacade.isSalesChannelMobileApp()) {
            return "";
        }

        try {
            // Get page by ID because the label could change
            final ContentPageModel previewPage = cmsPageService.getPageForIdWithRestrictions(previewRedirectPageId);
            if (previewPage != null) {
                LOG.info(
                        "ProductPreview: This product is in preview mode and is not viewable by the current sales channel. productCode="
                                + product.getCode());
                if (redirect) {
                    return UrlBasedViewResolver.REDIRECT_URL_PREFIX + previewPage.getLabel();
                }
                else {
                    throw new UnknownIdentifierException(
                            "This product is not viewable by the current sales channel");
                }
            }
        }
        catch (final CMSItemNotFoundException ex) {
            // Do nothing
        }

        return "";
    }

    protected String checkForRedirect(final HttpServletRequest request, final HttpServletResponse response,
            final ProductModel product) throws UnsupportedEncodingException {
        final String previewRedirect = getPreviewRedirect(product, true);
        if (StringUtils.isNotEmpty(previewRedirect)) {
            return previewRedirect;
        }

        final String redirection = checkRequestUrl(request, response, productModelUrlResolver.resolve(product));
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }

        return "";
    }

    /**
     * Populate product data for both populateProductDetailForDisplay and populateProductVariantDetailForDisplay
     * 
     * @param model
     * @param productData
     */
    private void populateProductVariant(final Model model, final TargetProductData productData) {
        sortVariantOptionData(productData);

        model.addAttribute("product", productData);

        if (targetFeatureSwitchFacade.isConsolidatedStoreStockAvailable()) {
            getStockForVariantsDisplay(productData.getCode(), model);
        }

        //If we have children variance (can only be size variance)
        if (productData.getVariantType() != null) {
            if (productData.getVariantType().equalsIgnoreCase(TargetSizeVariantProduct.class.getSimpleName())) {
                //Load sizes from product data (variant option data)
                addSizesFromVariantOptionsToModel(model, productData.getVariantOptions());
            }
        }
        addBaseOptions(model, productData);
    }

    /**
     * @param model
     * @param productData
     */
    private void addBaseOptions(final Model model, final TargetProductData productData) {
        //If we have parent variance
        if (productData.getBaseOptions() != null) {
            for (final BaseOptionData baseOptionData : productData.getBaseOptions()) {
                //If it is a colour variance
                if (baseOptionData.getVariantType().equalsIgnoreCase(
                        TargetColourVariantProduct.class.getSimpleName())) {
                    addColoursFromBaseOptionToModel(model, baseOptionData);
                }
                //If it is a size variance
                else if (baseOptionData.getVariantType().equalsIgnoreCase(
                        TargetSizeVariantProduct.class.getSimpleName())) {
                    addSizesFromBaseOptionToModel(model, baseOptionData);
                }
            }
        }
    }

    /**
     * Populate product data for both populateProductDetailForDisplay and populateProductVariantDetailForDisplay
     * 
     * @param productModel
     * @param model
     * @param request
     * @param productData
     */
    private void populateProductData(final ProductModel productModel, final Model model,
            final HttpServletRequest request, final TargetProductData productData, final boolean populateAll) {
        sortVariantOptionData(productData);
        populateProductData(productData, model, request);

        if (populateAll && targetFeatureSwitchFacade.isConsolidatedStoreStockAvailable()
                && !targetFeatureSwitchFacade
                        .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.PDP_OPTIMISATION)) {
            getStockForVariants(productData.getCode(), model, null, request);
        }

        //If we have children variance (can only be size variance)
        if (productData.getVariantType() != null) {
            if (productData.getVariantType().equalsIgnoreCase(TargetSizeVariantProduct.class.getSimpleName())) {
                //Load sizes from product data (variant option data)
                addSizesFromVariantOptionsToModel(model, productData.getVariantOptions());
                final String dealdescription = getTargetProductFacade().getSummaryDealDescription(productModel);
                productData.setDealDescription(dealdescription);
            }
        }
        addBaseOptions(model, productData);
        model.addAttribute("pageType", PageType.Product);
    }

    protected PageableData createPageableData() {
        return new PageableData();
    }

    /**
     * set the robots meta tag to noindex, nofollow if both online and consolidated stock is zero.
     * 
     * @param model
     * @param availableOnline
     * @param availableInStore
     */
    private void setMetaRobots(final Model model, final boolean availableOnline, final boolean availableInStore) {
        if (!availableOnline && !availableInStore) {
            storeMetaRobotsNoIndexNoFollow(model);
        }
    }

    /**
     * This method will populate lister data ,stock and environment data which will be used by react framework for pdp
     * 
     * @param model
     * @param productCode
     * @param productModel
     * @param bulkyBoardStoreNumber
     */
    private void populateDetailsForReact(final Model model, final String productCode, final ProductModel productModel,
            final String bulkyBoardStoreNumber) {
        if (targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_CAROUSEL)) {
            final TargetProductListerData targetProductListerData = getProductListerData(productCode);
            final TargetPointOfServiceData bulkyBoardStore = retrieveBulkyBoardStore(bulkyBoardStoreNumber);

            sortListerDataSizeVariants(targetProductListerData);
            final String prdToSearchEndeca = getProductCodeForEndecaSearch(productCode, targetProductListerData);
            final List<TargetProductListerData> endecaProductDataList = getProductDetailsFromEndeca(prdToSearchEndeca);
            if (CollectionUtils.isNotEmpty(endecaProductDataList)) {
                //There will be always one variantData
                final TargetProductListerData endecaData = endecaProductDataList.get(0);
                populateCustomerTotalInterestNumberForPrd(targetProductListerData, endecaData);
                final SchemaData schemaData = ProductDataHelper.getSchemaData(endecaData, prdToSearchEndeca);
                model.addAttribute("schemaData",
                        schemaData);
            }
            populateProductListerData(model, targetProductListerData);
            populateEnvironmentData(model, bulkyBoardStore);

            if (targetFeatureSwitchFacade
                    .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.UI_REACT_PDP_DETAIL_PANEL)) {
                populateShopTheLookData(model, productModel);
            }

        }
    }

    private String getProductCodeForEndecaSearch(final String productCode, final TargetProductListerData listerData) {
        if (productCode.equals(listerData.getBaseProductCode())
                && CollectionUtils.isNotEmpty(listerData.getTargetVariantProductListerData())) {
            final TargetVariantProductListerData variantData = listerData.getTargetVariantProductListerData().get(0);
            return variantData.getCode();
        }
        return productCode;
    }

    /**
     * This method will return sorted list of variants based on their size position.
     * 
     * @param targetProductListerData
     */
    protected void sortListerDataSizeVariants(final TargetProductListerData targetProductListerData) {

        final List<TargetVariantProductListerData> colourVariantList = new ArrayList();
        if (null != targetProductListerData && CollectionUtils.isNotEmpty(targetProductListerData
                .getTargetVariantProductListerData())) {
            for (final TargetVariantProductListerData colourVariant : targetProductListerData
                    .getTargetVariantProductListerData()) {
                colourVariantList.add(colourVariant);
                if (CollectionUtils.isEmpty(colourVariant.getVariants())) {
                    continue;
                }
                final List<TargetVariantProductListerData> sizeVariants = colourVariant.getVariants();
                Collections.sort(sizeVariants, new Comparator<TargetVariantProductListerData>() {
                    @Override
                    public int compare(final TargetVariantProductListerData variant1,
                            final TargetVariantProductListerData variant2) {
                        if (null == variant1 || null == variant2) {
                            return 0;
                        }
                        if (variant1.getSizePosition().intValue() < variant2.getSizePosition().intValue()) {
                            return -1;
                        }
                        return 0;
                    }

                });
                colourVariant.setVariants(sizeVariants);
            }
            targetProductListerData.setTargetVariantProductListerData(colourVariantList);
        }

    }

    private BigDecimal populatePreOrderDeposit() {
        final String deposit = targetSharedConfigFacade.getConfigByCode(TgtCoreConstants.Config.PREODER_DEPOSIT_AMOUNT);
        return StringUtils.isNotEmpty(deposit) ? new BigDecimal(deposit)
                : new BigDecimal(0);
    }


    /**
     * Returns a TargetPointOfServiceData object for the supplied store number
     * 
     * @param bulkyBoardStoreNumber
     * @return TargetPointOfServiceData
     */
    private TargetPointOfServiceData retrieveBulkyBoardStore(final String bulkyBoardStoreNumber) {
        if (StringUtils.isNotBlank(bulkyBoardStoreNumber) && StringUtils.isNumeric(bulkyBoardStoreNumber)) {
            try {
                return storeLocatorFacade.getPointOfService(Integer
                        .valueOf(bulkyBoardStoreNumber));
            }
            catch (final Exception e) {
                LOG.error("BULKY_BOARD_STORE_LOOKUP: store " + bulkyBoardStoreNumber + " not found", e);
            }
        }
        return null;
    }
}

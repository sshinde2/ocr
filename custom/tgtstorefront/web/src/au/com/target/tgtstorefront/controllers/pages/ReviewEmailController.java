/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Arrays;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import au.com.target.tgtfacades.data.CanonicalData;
import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * @author rmcalave
 * 
 */
@Controller
@RequestMapping(value = ControllerConstants.PRODUCT_REVIEW_EMAIL)
public class ReviewEmailController extends AbstractPageController {
    @Resource(name = "productService")
    private ProductService productService;

    @Resource(name = "targetProductFacade")
    private TargetProductFacade productFacade;

    @RequestMapping(method = RequestMethod.GET)
    public String reviewEmail(@RequestParam(value = "bvproductid") final String productCode,
            final HttpServletRequest request) {
        final ProductModel productModel = productService.getProductForCode(productCode);

        final TargetProductData productData = (TargetProductData)productFacade.getProductForOptions(productModel,
                Arrays.asList(ProductOption.BASIC));
        final CanonicalData baseProductCanonicalData = productData.getBaseProductCanonical();

        return UrlBasedViewResolver.REDIRECT_URL_PREFIX + baseProductCanonicalData.getUrl()
                + ControllerConstants.PRODUCT_REVIEW + '?' + request.getQueryString();
    }

    @ExceptionHandler(UnknownIdentifierException.class)
    public String handleUnknownIdentifierException(final HttpServletRequest request)
    {
        request.setAttribute("isProductNotFound", Boolean.TRUE);
        return ControllerConstants.Forward.ERROR_404;
    }
}

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.pages;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtutility.util.JsonConversionUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.facetdata.BreadcrumbData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import au.com.target.endeca.infront.assemble.impl.EndecaAutoSuggestAssemble;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaWrapperException;
import au.com.target.endeca.infront.transform.TargetEndecaURLHelper;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtfacades.search.data.TargetProductCategorySearchPageData;
import au.com.target.tgtfacades.url.impl.TargetProductModelUrlResolver;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.breadcrumb.impl.SearchBreadcrumbBuilder;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.data.SeachEvaluatorData;
import au.com.target.tgtstorefront.controllers.pages.data.ViewPreferencesInitialData;
import au.com.target.tgtstorefront.controllers.pages.preferences.StoreViewPreferencesHandler;
import au.com.target.tgtstorefront.controllers.pages.url.URLElements;
import au.com.target.tgtstorefront.controllers.util.TargetEndecaSearchHelper;
import au.com.target.tgtstorefront.util.CommonUtils;
import au.com.target.tgtstorefront.util.MetaSanitizerUtil;
import au.com.target.tgtstorefront.util.SearchQuerySanitiser;
import au.com.target.tgtutility.util.StringTools;

import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.shaded.org.apache.commons.lang.BooleanUtils;


@Controller
@RequestMapping(ControllerConstants.SEARCH)
public class SearchPageController extends AbstractSearchPageController {
    private static final String SEARCH_CMS_PAGE_ID = "search";
    private static final String NO_RESULTS_CMS_PAGE_ID = "searchEmpty";

    private static final String SEARCH_GRID_PAGE = "search/searchGridPage";
    private static final String SEARCH_LOOK_PAGE = "search/searchLookPage";

    private static final int DEFAULT_MAX_SEARCH_TEXT_LENGTH = 50;

    private static final Pattern PRODUCT_CODE_PATTERN = Pattern.compile("[A-Za-z]?[0-9]{4,10}");

    static {
        PAGE_TO_VIEW_AS_TYPE_MAP.put(PAGE_ROOT + SEARCH_GRID_PAGE, ViewAsType.Grid);
        PAGE_TO_VIEW_AS_TYPE_MAP.put(PAGE_ROOT + SEARCH_LOOK_PAGE, ViewAsType.Look);
    }

    private Integer maxSearchTextLength;

    @Resource(name = "searchBreadcrumbBuilder")
    private SearchBreadcrumbBuilder searchBreadcrumbBuilder;

    @Resource(name = "storeViewPreferencesHandler")
    private StoreViewPreferencesHandler storeViewPreferencesHandler;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Resource(name = "searchQuerySanitiser")
    private SearchQuerySanitiser searchQuerySanitiser;

    @Resource(name = "targetProductService")
    private TargetProductService targetProductService;

    @Resource(name = "targetProductModelUrlResolver")
    private TargetProductModelUrlResolver targetProductModelUrlResolver;

    @Resource(name = "targetEndecaURLHelper")
    private TargetEndecaURLHelper targetEndecaURLHelper;

    @Resource(name = "endecaAutoSuggestAssemble")
    private EndecaAutoSuggestAssemble endecaAutoSuggestAssemble;

    @Resource(name = "targetEndecaSearchHelper")
    private TargetEndecaSearchHelper targetEndecaSearchHelper;

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @RequestMapping(method = RequestMethod.GET)
    public String textSearch(@RequestParam(value = "text", defaultValue = "") final String searchText,
            @RequestParam(value = "page", defaultValue = "0") final String page,
            @RequestParam(value = "Ns", required = false) final String sortCode,
            @RequestParam(value = "viewAs", required = false) final String viewAs,
            @RequestParam(value = "Nrpp", defaultValue = "0") final String pageSize,
            @RequestParam(value = "N", required = false) final String navigationState,
            @RequestParam(value = "skipRedirect", required = false, defaultValue = "false") final String skipRedirect,
            final HttpServletRequest request,
            final HttpServletResponse response, final Model model)
            throws CMSItemNotFoundException {
        boolean resultsFound = false;

        //pageNo
        final int pageNo = CommonUtils.convertToPositiveInt(page);

        final SeachEvaluatorData seachEvaluatorData = targetEndecaSearchHelper.handlePreferencesInitialData(request,
                response, StringUtils.EMPTY, navigationState, sortCode, viewAs, pageSize, searchText);

        ProductSearchPageData<EndecaSearchStateData, ProductData> searchPageData = null;
		final String cleanSearchText = targetEndecaSearchHelper.sanitizeSearchText(searchText, model);
		if (StringUtils.isNotBlank(cleanSearchText)) {
			// Setup the EndecaSearchStateData
			final List<String> clearSearchTexts = new ArrayList<>();
			clearSearchTexts.add(cleanSearchText);
			final EndecaSearchStateData searchStateData = targetEndecaURLHelper.buildEndecaSearchStateData(
					navigationState, clearSearchTexts, seachEvaluatorData.getSortCodeFn(),
					seachEvaluatorData.getItemPerPageFn(), pageNo, seachEvaluatorData.getViewAsFn(),
					request.getRequestURI(), null, null);

			if (PRODUCT_CODE_PATTERN.matcher(cleanSearchText).matches()) {
				searchPageData = doSearchTargetProduct(cleanSearchText, searchPageData);
			}

			if (null == searchPageData && null != searchStateData) {
				searchStateData.setPersistAvailableOnlineFacet(false);
				searchStateData.setSkipRedirect(BooleanUtils.toBoolean(skipRedirect));
				searchPageData = doSearchInEndeca(request, model, searchStateData, cleanSearchText);
			}
		}

        if (searchPageData != null) {
            if (searchPageData.getKeywordRedirectUrl() != null) {
                // if the search engine returns a redirect, just
                return UrlBasedViewResolver.REDIRECT_URL_PREFIX + searchPageData.getKeywordRedirectUrl();
            }
            else if (null != searchPageData.getPagination()) {
                if ((0 != searchPageData.getPagination().getTotalNumberOfResults())) {
                    resultsFound = true;
                    storeContinueUrl(request);
                }
                updatePageTitle(searchPageData.getFreeTextSearch(), model);
            }
            populateModel(model, searchPageData);
            populateReactData(searchPageData, model);
            model.addAttribute("freeTextSearch", searchPageData.getFreeTextSearch());
        }
        model.addAttribute("pageType", PageType.ProductSearch);
        storeMetaRobotsNoIndex(model);

        model.addAttribute(
                "urlElements",
                new URLElements(seachEvaluatorData.getViewAsFn(), seachEvaluatorData.getSortCodeFn(), String
                        .valueOf(seachEvaluatorData.getItemPerPageFn())));


        final String metaDescription = MetaSanitizerUtil.sanitizeDescription(getMessageSource().getMessage(
                "search.meta.description.results", null, getI18nService().getCurrentLocale())
                + " "
                + cleanSearchText
                + " "
                + getMessageSource()
                        .getMessage("search.meta.description.on", null, getI18nService().getCurrentLocale())
                + " "
                + getSiteName());
        setUpMetaData(model, StringUtils.EMPTY, metaDescription);

        String viewPage = null;

        if (resultsFound) {
            storeCmsPageInModel(model, getContentPageForLabelOrId(SEARCH_CMS_PAGE_ID));

            viewPage = getViewPage(seachEvaluatorData.getViewAsFn(), model);
            populateActualViewAsType(viewPage, model);
        }
        else {
            storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
            viewPage = getViewPage(null, model);
        }


        return viewPage;
    }


    @RequestMapping(value = "/auto-suggestions", method = RequestMethod.GET)
    public String getAutocompleteSuggestions(@RequestParam("term") final String term, final HttpServletRequest request,
            final HttpServletResponse response, final Model model) {

        String safeSearchText = term;
        if (safeSearchText.length() > getMaxSearchTextLength().intValue()) {
            safeSearchText = safeSearchText.substring(0, getMaxSearchTextLength().intValue());
        }
        final String cleanSearchText = searchQuerySanitiser.sanitiseSearchText(safeSearchText);
        if (StringUtils.isNotBlank(cleanSearchText)) {
            final ViewPreferencesInitialData viewData = new ViewPreferencesInitialData(request, StringUtils.EMPTY,
                    null);
            final String viewAsFn = storeViewPreferencesHandler.getViewAsPreference(response, viewData, null);
            final List<String> clearSearchTexts = new ArrayList<>();
            clearSearchTexts.add(cleanSearchText);
            final EndecaSearchStateData searchStateData = targetEndecaURLHelper.buildEndecaSearchStateData(
                    null, clearSearchTexts, null, 0, 0, viewAsFn, null, null, null);
            ContentItem autoSuggestSearchResult = null;
            try {
                autoSuggestSearchResult = endecaAutoSuggestAssemble.assemble(request, searchStateData);
            }
            //CHECKSTYLE:OFF allow empty block
            catch (final TargetEndecaWrapperException e) {
                //Error messages have already log in splunk, continue
            }
            model.addAttribute("autoSuggestSearchResult", autoSuggestSearchResult);
        }
        model.addAttribute("term", term);
        return ControllerConstants.Views.Fragments.Search.SEARCH_AUTO_SUGGESTIONS;
    }

    /**
     * get the product from DB rather than from Endeca
     *
     * @param cleanSearchText
     * @param srchPageData
     * @return ProductSearchPageData containing the product or null if product not found
     */
    public ProductSearchPageData<EndecaSearchStateData, ProductData> doSearchTargetProduct(
            final String cleanSearchText,
            final ProductSearchPageData<EndecaSearchStateData, ProductData> srchPageData) {
        final String productLookupCode = StringTools.trimLeadingZeros(cleanSearchText);
        ProductSearchPageData<EndecaSearchStateData, ProductData> searchPageData = srchPageData;
        try {
            final ProductModel product = targetProductService.getProductForCode(productLookupCode);
            if (product != null) {
                searchPageData = new ProductSearchPageData<EndecaSearchStateData, ProductData>();
                searchPageData.setKeywordRedirectUrl(targetProductModelUrlResolver.resolve(product));
            }

        }
        //CHECKSTYLE:OFF allow empty block
        catch (final UnknownIdentifierException ex) {
            // No product found, continue with the search
        }
        //CHECKSTYLE:ON
        return searchPageData;
    }

    /**
     * @param request
     * @param model
     * @param searchStateData
     * @return boolean indicating whether the result was found
     */
    public TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> doSearchInEndeca(
            final HttpServletRequest request, final Model model, final EndecaSearchStateData searchStateData,
            final String cleanSearchText) {

        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData = new TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData>();

        final List<BreadcrumbData<EndecaSearchStateData>> refinements = new ArrayList<BreadcrumbData<EndecaSearchStateData>>();
        searchPageData.setBreadcrumbs(refinements);

        //set up the free text
        if (StringUtils.isNotEmpty(cleanSearchText)) {
            searchPageData.setFreeTextSearch(StringEscapeUtils.escapeHtml(cleanSearchText));
        }

        final EndecaSearch searchResults = targetEndecaSearchHelper.getEndecaSearchResult(request, searchStateData);

        targetEndecaSearchHelper.populateSearchPageData(searchPageData, searchResults, searchStateData);

        if (searchResults != null && searchResults.getRecords() != null) {
            this.populateSearchTermForBreadcrumb(searchStateData, searchPageData, refinements, searchResults,
                    cleanSearchText, model);
        }

        return searchPageData;
    }

    protected void populateSearchTermForBreadcrumb(final EndecaSearchStateData searchStateData,
            final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData,
            final List<BreadcrumbData<EndecaSearchStateData>> refinements,
            final EndecaSearch searchResults, final String cleanSearchText, final Model model) {
        String searchTermForBreadcrumb = null;
        if (StringUtils.isNotEmpty(searchPageData.getAutoCorrectedTerm())) {
            searchTermForBreadcrumb = searchPageData.getAutoCorrectedTerm();
        }
        else if (searchStateData != null) {
            searchTermForBreadcrumb = cleanSearchText;
        }
        if (null != searchTermForBreadcrumb) {
            final List<Breadcrumb> breadcrumbs = searchBreadcrumbBuilder.getBreadcrumbs(null, null, refinements,
                    searchTermForBreadcrumb,
                    searchResults);
            targetEndecaSearchHelper.populateClearAllUrl(searchPageData, searchResults, breadcrumbs);
            model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);
        }
    }

    protected String getViewPage(final String viewAs, final Model model) {
        if (StringUtils.isNotBlank(viewAs)) {
            final ViewAsType viewAsType = getViewAsType(viewAs);

            if (ViewAsType.Grid.equals(viewAsType)) {
                return PAGE_ROOT + SEARCH_GRID_PAGE;
            }
            else if (ViewAsType.Look.equals(viewAsType)) {
                return PAGE_ROOT + SEARCH_LOOK_PAGE;
            }
        }

        return getViewForPage(model);
    }

	/**
	 * This method will populate a product list which will be used by react
	 * framework for listing page
	 * 
	 * @param searchPageData
	 * @param model
	 */
	protected void populateReactData(final ProductSearchPageData<EndecaSearchStateData, ProductData> searchPageData,
			final Model model) {
		if (targetFeatureSwitchFacade.isReactListingEnabled()) {
			if (null != searchPageData) {
				final List<ProductData> productList = searchPageData.getResults();
				final Map<String, ProductData> productsDataMap = new HashMap<>();
				for (ProductData productData : productList) {
					productsDataMap.put(productData.getCode(), productData);
				}
				model.addAttribute("productList", JsonConversionUtil.convertToJsonString(productsDataMap));
			}
		}
	}

    protected void updatePageTitle(final String searchText, final Model model) {
        storeContentPageTitleInModel(
                model,
                getPageTitleResolver().resolveContentPageTitle(
                        getMessageSource().getMessage("search.meta.title", null, getI18nService().getCurrentLocale())
                                + " "
                                + searchText));
    }

    private Integer getMaxSearchTextLength() {
        if (maxSearchTextLength == null) {
            maxSearchTextLength = configurationService.getConfiguration().getInteger(
                    "storefront.search.maxSearchTextLength", new Integer(DEFAULT_MAX_SEARCH_TEXT_LENGTH));
        }
        return maxSearchTextLength;
    }


    /**
     * @param searchQuerySanitiser2
     */
    public void setSearchQuerySanitiser(final SearchQuerySanitiser searchQuerySanitiser2) {
        this.searchQuerySanitiser = searchQuerySanitiser2;
    }

}
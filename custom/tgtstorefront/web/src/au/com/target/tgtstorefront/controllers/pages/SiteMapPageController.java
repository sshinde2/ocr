/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSNavigationService;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgtfacades.navigation.NavigationMenuItem;
import au.com.target.tgtstorefront.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.navigation.SiteMapMenuBuilder;
import au.com.target.tgtwebcore.enums.CachedPageType;


/**
 * @author rmcalave
 * 
 */
@Controller
public class SiteMapPageController extends AbstractPageController {
    private static final Logger LOG = Logger.getLogger(SiteMapPageController.class);
    private static final String CONTENT_ROOT_NODE_ID = "SiteHomeNavNode";
    private static final String CATEGORY_ROOT_NODE_ID = "TargetSiteCategoriesNavNode";
    private static final String CATEGORIES = "categories";
    private static final String CONTENT = "content";
    private static final String SITE_MAP_STRUCTURE_MAP = "siteMapStructureMap";
    private static final String SITE_MAP_CMS_PAGE_LABEL = "SiteMap";

    @Autowired
    private CMSNavigationService cmsNavigationService;

    @Autowired
    private SiteMapMenuBuilder siteMapMenuBuilder;

    @Autowired
    private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;

    @RequestMapping(value = "/sitemap")
    public String getSiteMapPage(final Model model, final HttpServletResponse response)
            throws CMSItemNotFoundException {
        final ContentPageModel siteMapPage = getContentPageForLabelOrId(SITE_MAP_CMS_PAGE_LABEL);

        final Set<NavigationMenuItem> categoryStructure = buildStructure(CATEGORY_ROOT_NODE_ID);
        final Set<NavigationMenuItem> contentStructure = buildStructure(CONTENT_ROOT_NODE_ID);

        final Map<String, Set<NavigationMenuItem>> structureMap = new HashMap<String, Set<NavigationMenuItem>>();
        structureMap.put(CATEGORIES, categoryStructure);
        structureMap.put(CONTENT, contentStructure);

        model.addAttribute(SITE_MAP_STRUCTURE_MAP, structureMap);
        addAkamaiCacheAttributes(CachedPageType.SITEMAP, response, model);

        storeCmsPageInModel(model, siteMapPage);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY,
                contentPageBreadcrumbBuilder.getBreadcrumbs(siteMapPage));

        return ControllerConstants.Views.Pages.SiteMap.SITE_MAP;
    }

    private Set<NavigationMenuItem> buildStructure(final String navigationNodeId) {
        CMSNavigationNodeModel navigationNode = null;
        try {
            navigationNode = cmsNavigationService.getNavigationNodeForId(navigationNodeId);
        }
        catch (final CMSItemNotFoundException ex) {
            LOG.error(navigationNodeId + " not found");
            return null;
        }

        final Set<NavigationMenuItem> structure = siteMapMenuBuilder
                .createMenuItemsForChildren(navigationNode
                        .getChildren());

        return structure;
    }
}

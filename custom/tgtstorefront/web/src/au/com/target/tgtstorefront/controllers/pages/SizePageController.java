/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtcore.product.TargetSizeTypeService;
import au.com.target.tgtfacades.navigation.NavigationMenuItem;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.navigation.SiteMapMenuBuilder;
import au.com.target.tgtwebcore.model.cms2.pages.SizePageModel;
import au.com.target.tgtwebcore.servicelayer.services.TargetCMSPageService;


/**
 * Controller to handle Size Chart Request.
 */
@Controller
@RequestMapping(ControllerConstants.SIZE_PAGE)
public class SizePageController extends AbstractPageController {
    private static final String SIZE_CODE = "/{sizeTypeCode}";
    private static final String VIEWALLSIZE = "viewallsize";
    @Resource(name = "cmsPageService")
    private CMSPageService cmsPageService;

    @Autowired
    private SiteMapMenuBuilder siteMapMenuBuilder;

    @Autowired
    private TargetSizeTypeService targetSizeTypeService;

    @Resource(name = "targetCMSPageService")
    private TargetCMSPageService targetCMSPageService;

    @RequestMapping(value = SIZE_CODE, method = RequestMethod.GET)
    public String getViewForPage(@PathVariable("sizeTypeCode") final String sizeTypeCode, final Model model)
            throws CMSItemNotFoundException {
        SizeTypeModel sizeTypeModel = null;
        if (!StringUtils.equals(sizeTypeCode, VIEWALLSIZE)) {
            sizeTypeModel = targetSizeTypeService.findSizeTypeForCode(sizeTypeCode);
            if (sizeTypeModel == null) {
                //Returning Default Size Page
                sizeTypeModel = targetSizeTypeService.getDefaultSizeType();
            }
        }
        buildStructure(sizeTypeModel, model);
        final AbstractPageModel pageModel = getSizePage(sizeTypeModel);
        storeCmsPageInModel(model, pageModel);
        return ControllerConstants.Views.Pages.SizePage.SIZE_PAGE;
    }

    private void buildStructure(final SizeTypeModel sizeTypeModel, final Model model) {
        if (sizeTypeModel != null) {
            model.addAttribute("sizeTypeName", sizeTypeModel.getName());
            model.addAttribute("sizeTypeCode", sizeTypeModel.getCode());
            final CMSNavigationNodeModel navModel = sizeTypeModel.getCmsNavigationNode();
            if (navModel != null) {
                final NavigationMenuItem structure = siteMapMenuBuilder
                        .createMenuItem(navModel);
                if (structure != null) {
                    model.addAttribute("navigation", structure);
                }
            }
        }
    }

    protected SizePageModel getSizePage(final SizeTypeModel sizeType) {
        try {
            return targetCMSPageService.getPageForSize(sizeType);
        }
        //CHECKSTYLE:OFF
        catch (final CMSItemNotFoundException ignore) {
            // Ignore
        }
        //CHECKSTYLE:ON
        return null;
    }

    /**
     * @param cmsPageService
     *            the cmsPageService to set
     */
    public void setCmsPageService(final CMSPageService cmsPageService) {
        this.cmsPageService = cmsPageService;
    }

    /**
     * @param siteMapMenuBuilder
     *            the siteMapMenuBuilder to set
     */
    public void setSiteMapMenuBuilder(final SiteMapMenuBuilder siteMapMenuBuilder) {
        this.siteMapMenuBuilder = siteMapMenuBuilder;
    }

    /**
     * @param targetSizeTypeService
     *            the targetSizeTypeService to set
     */
    public void setTargetSizeTypeService(final TargetSizeTypeService targetSizeTypeService) {
        this.targetSizeTypeService = targetSizeTypeService;
    }

    /**
     * @param targetCMSPageService
     *            the targetCMSPageService to set
     */
    public void setTargetCMSPageService(final TargetCMSPageService targetCMSPageService) {
        this.targetCMSPageService = targetCMSPageService;
    }

}

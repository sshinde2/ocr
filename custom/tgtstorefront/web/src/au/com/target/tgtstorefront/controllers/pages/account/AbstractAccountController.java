/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.account;

import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ModelAttribute;

import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.order.TargetOrderFacade;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtfacades.user.data.TargetCustomerData;
import au.com.target.tgtstorefront.breadcrumb.ResourceBreadcrumbBuilder;
import au.com.target.tgtstorefront.controllers.pages.AbstractSearchPageController;
import au.com.target.tgtstorefront.controllers.pages.checkout.AbstractCheckoutController.SelectOption;
import au.com.target.tgtstorefront.controllers.url.TargetUrlsMapping;
import au.com.target.tgtstorefront.controllers.util.AddressDataHelper;
import au.com.target.tgtstorefront.forms.validation.validator.AddressFormValidator;
import au.com.target.tgtverifyaddr.TargetAddressVerificationService;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public abstract class AbstractAccountController extends AbstractSearchPageController {

    @Resource(name = "targetUserFacade")
    protected TargetUserFacade targetUserFacade;

    @Resource(name = "targetCustomerFacade")
    protected TargetCustomerFacade<TargetCustomerData> targetCustomerFacade;

    @Resource(name = "targetOrderFacade")
    protected TargetOrderFacade targetOrderFacade;

    @Resource(name = "acceleratorCheckoutFacade")
    protected CheckoutFacade checkoutFacade;

    @Resource(name = "accountBreadcrumbBuilder")
    protected ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

    @Resource(name = "targetStoreLocatorFacade")
    protected TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Resource(name = "addressDataHelper")
    protected AddressDataHelper addressDataHelper;

    @Resource(name = "addressFormValidator")
    protected AddressFormValidator addressFormValidator;

    @Resource(name = "targetAddressVerificationService")
    protected TargetAddressVerificationService targetAddressVerificationService;

    @Resource(name = "priceDataFactory")
    protected PriceDataFactory priceDataFactory;

    @Resource(name = "commonI18NService")
    protected CommonI18NService commonI18NService;

    @Resource(name = "targetUrlsMapping")
    protected TargetUrlsMapping targetUrlsMapping;

    @ModelAttribute("states")
    public List<SelectOption> getStates() {
        return addressDataHelper.getStates();
    }

    @ModelAttribute("countries")
    public Collection<CountryData> getCountries() {
        return checkoutFacade.getDeliveryCountries();
    }

    @ModelAttribute("titles")
    public Collection<TitleData> getTitles() {
        return targetUserFacade.getTitles();
    }

    public String getDomainUrl(final HttpServletRequest request) {
        return request.getRequestURL().toString().replace(request.getRequestURI(), request.getContextPath());
    }

}

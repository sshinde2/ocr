/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.account;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtstorefront.annotations.RequireHardLogIn;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.data.BreadcrumbData;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.forms.AddressForm;
import au.com.target.tgtstorefront.forms.ConfirmAddressForm;
import au.com.target.tgtverifyaddr.data.FormattedAddressData;
import au.com.target.tgtverifyaddr.exception.ServiceNotAvailableException;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
@Controller
@RequestMapping(value = ControllerConstants.MY_ACCOUNT)
@RequireHardLogIn
public class AccountAddressDetailsController extends AbstractAccountController {

    /**
     * List all user's address
     * 
     * @param model
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_ADDRESS_DETAILS, method = RequestMethod.GET)
    public String getAddressBook(final Model model) throws CMSItemNotFoundException
    {
        model.addAttribute("addressData", targetUserFacade.getAddressBook());

        storeCmsPageAndSetUpMetadata(model, ControllerConstants.CmsPageLabel.MY_ACCOUNT_ADDRESS_DETAILS);

        model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.myAddressDetails"));
        storeMetaRobotsNoIndexNoFollow(model);
        return ControllerConstants.Views.Pages.Account.ACCOUNT_ADDRESS_DETAILS_PAGE;
    }


    /**
     * Form to edit an existing address
     * 
     * @param addressCode
     * @param model
     * @param redirectAttributes
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_ADDRESS_EDIT + ControllerConstants.MY_ACCOUNT_ADDRESS_CODE, method = RequestMethod.GET)
    public String editAddress(@PathVariable(value = "addressCode") final String addressCode,
            final Model model, final RedirectAttributes redirectAttributes)
            throws CMSItemNotFoundException
    {
        addressDataHelper.checkIfSingleLineAddressFeatureIsEnabled(model);
        final boolean isEdit = StringUtils.isNotBlank(addressCode);

        AddressData addressData = null;
        if (isEdit) {
            addressData = addressDataHelper.getAddressDataFromAddressBookById(addressCode);
            model.addAttribute("addressData", addressData);
            model.addAttribute("isDefault", Boolean.valueOf(addressData.isDefaultAddress()));
        }

        AddressForm addressForm = (AddressForm)model.asMap().get("addressForm");
        if (addressForm == null) {
            addressForm = new AddressForm();

            if (addressData != null) {
                addressDataHelper.populateAddressForm(addressForm, (TargetAddressData)addressData);
            }

            model.addAttribute("addressForm", addressForm);
            model.addAttribute("edit", Boolean.valueOf(isEdit));
        }
        else {
            model.addAttribute("edit", Boolean.valueOf(StringUtils.isNotBlank(addressForm.getAddressId())));
        }

        storeCmsPageAndSetUpMetadata(model, ControllerConstants.CmsPageLabel.MY_ACCOUNT_ADD_EDIT_ADDRESS);
        model.addAttribute("breadcrumbs",
                accountBreadcrumbBuilder.getBreadcrumbs(new BreadcrumbData("text.account.manageAddresses.heading",
                        null, targetUrlsMapping.getMyAccountAddressDetails()),
                        new BreadcrumbData("text.account.editAddress.heading")));
        storeMetaRobotsNoIndexNoFollow(model);
        return ControllerConstants.Views.Pages.Account.ACCOUNT_EDIT_ADDRESS_PAGE;
    }

    /**
     * Submit an address modification with an address code, or submit a new address
     * 
     * @param addressForm
     * @param bindingResult
     * @param model
     * @param redirectModel
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = { ControllerConstants.MY_ACCOUNT_ADDRESS_EDIT,
            ControllerConstants.MY_ACCOUNT_ADDRESS_EDIT + ControllerConstants.MY_ACCOUNT_ADDRESS_CODE }, method = RequestMethod.POST)
    public String editAddress(@Valid final AddressForm addressForm, final BindingResult bindingResult,
            final Model model,
            final RedirectAttributes redirectModel, final HttpServletRequest request) throws CMSItemNotFoundException
    {
        addressDataHelper.checkIfSingleLineAddressFeatureIsEnabled(model);
        storeMetaRobotsNoIndexNoFollow(model);
        addressFormValidator.validate(addressForm, bindingResult);
        if (bindingResult.hasErrors())
        {
            GlobalMessages.addErrorMessage(model, "form.global.error");
            model.addAttribute("isDefault",
                    Boolean.valueOf(targetUserFacade.isAddressDefault(addressForm.getAddressId())));
            model.addAttribute("edit", Boolean.valueOf(StringUtils.isNotBlank(addressForm.getAddressId())));
            storeCmsPageAndSetUpMetadata(model, ControllerConstants.CmsPageLabel.MY_ACCOUNT_ADD_EDIT_ADDRESS);
            return ControllerConstants.Views.Pages.Account.ACCOUNT_EDIT_ADDRESS_PAGE;
        }
        return processAddress(addressForm, model, true, redirectModel, request);
    }

    /**
     * Form to create a new address
     * 
     * @param model
     * @param redirectAttributes
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_ADDRESS_ADD, method = RequestMethod.GET)
    public String addAddress(final Model model, final RedirectAttributes redirectAttributes)
            throws CMSItemNotFoundException
    {
        addressDataHelper.checkIfSingleLineAddressFeatureIsEnabled(model);
        if (!model.containsAttribute("addressForm")) {
            final AddressForm addressForm = new AddressForm();
            addressDataHelper.populateAddressFormFromUser(addressForm);
            model.addAttribute("addressForm", addressForm);
        }

        storeCmsPageAndSetUpMetadata(model, ControllerConstants.CmsPageLabel.MY_ACCOUNT_ADD_EDIT_ADDRESS);
        model.addAttribute("breadcrumbs",
                accountBreadcrumbBuilder.getBreadcrumbs(new BreadcrumbData("text.account.manageAddresses.heading",
                        null, targetUrlsMapping.getMyAccountAddressDetails()),
                        new BreadcrumbData("text.account.addAddress.heading")));
        storeMetaRobotsNoIndexNoFollow(model);
        return ControllerConstants.Views.Pages.Account.ACCOUNT_EDIT_ADDRESS_PAGE;
    }


    /**
     * Submit an address modification with an address code, or submit a new address
     * 
     * @param addressForm
     * @param bindingResult
     * @param model
     * @param redirectModel
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_ADDRESS_ADD, method = RequestMethod.POST)
    public String addAddress(@Valid final AddressForm addressForm, final BindingResult bindingResult,
            final Model model,
            final RedirectAttributes redirectModel, final HttpServletRequest request) throws CMSItemNotFoundException
    {
        storeMetaRobotsNoIndexNoFollow(model);
        addressDataHelper.checkIfSingleLineAddressFeatureIsEnabled(model);
        addressFormValidator.validate(addressForm, bindingResult);
        if (bindingResult.hasErrors())
        {
            GlobalMessages.addErrorMessage(model, "form.global.error");
            storeCmsPageAndSetUpMetadata(model, ControllerConstants.CmsPageLabel.MY_ACCOUNT_ADD_EDIT_ADDRESS);
            return ControllerConstants.Views.Pages.Account.ACCOUNT_EDIT_ADDRESS_PAGE;
        }
        return processAddress(addressForm, model, false, redirectModel, request);
    }

    /**
     * Confirm new/edit addresses from the modal
     * 
     * @param confirmAddressForm
     * @param model
     * @param redirectAttributes
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = { ControllerConstants.MY_ACCOUNT_ADDRESS_ADD_CONFIRM,
            ControllerConstants.MY_ACCOUNT_ADDRESS_EDIT_CONFIRM }, method = RequestMethod.POST)
    public String confirmRedirect(@Valid final ConfirmAddressForm confirmAddressForm, final Model model,
            final RedirectAttributes redirectAttributes, final HttpServletRequest request)
            throws CMSItemNotFoundException
    {
        final AddressForm suppliedAddress = confirmAddressForm.getSuppliedAddress();
        final boolean isEdit = StringUtils.isNotBlank(suppliedAddress.getAddressId());
        boolean useSuppliedAddress = confirmAddressForm.isUseSuppliedAddress();

        if (!useSuppliedAddress && StringUtils.isBlank(confirmAddressForm.getConfirmedAddressCode())) {
            GlobalMessages.addErrorMessage(model, "account.confirmation.address.error");
            model.addAttribute("addressForm", suppliedAddress);
            if (isEdit) {
                return editAddress(suppliedAddress.getAddressId(), model,
                        redirectAttributes);
            }
            else {
                return addAddress(model, redirectAttributes);
            }
        }

        final TargetAddressData newAddress = new TargetAddressData();
        if (isEdit) {
            newAddress.setId(suppliedAddress.getAddressId());
        }
        newAddress.setTitleCode(suppliedAddress.getTitleCode());
        newAddress.setFirstName(suppliedAddress.getFirstName());
        newAddress.setLastName(suppliedAddress.getLastName());
        newAddress.setPhone(suppliedAddress.getPhoneNumber());
        newAddress.setBillingAddress(false);
        newAddress.setShippingAddress(true);
        newAddress.setVisibleInAddressBook(true);
        if (targetUserFacade.isAddressBookEmpty())
        {
            newAddress.setDefaultAddress(true);
        }
        else
        {
            newAddress.setDefaultAddress(BooleanUtils.toBoolean(suppliedAddress
                    .getDefaultAddress()));
        }
        final CountryData countryData = new CountryData();
        countryData.setIsocode(suppliedAddress.getCountryIso());
        newAddress.setCountry(countryData);

        if (!useSuppliedAddress) {
            final au.com.target.tgtverifyaddr.data.AddressData qasAddressData = new au.com.target.tgtverifyaddr.data.AddressData(
                    confirmAddressForm.getConfirmedAddressCode(), null, null);
            try {
                final FormattedAddressData selectedAddress = targetAddressVerificationService.formatAddress(
                        qasAddressData);

                newAddress.setLine1(selectedAddress.getAddressLine1());
                newAddress.setLine2(selectedAddress.getAddressLine2());
                newAddress.setTown(selectedAddress.getCity());
                newAddress.setPostalCode(selectedAddress.getPostcode());
                newAddress.setState(selectedAddress.getState());

                newAddress.setAddressValidated(true);
            }
            catch (final ServiceNotAvailableException ex) {
                GlobalMessages.addFlashInfoMessage(redirectAttributes, "address.info.verification.unavailable");
                useSuppliedAddress = true;
            }
        }

        if (useSuppliedAddress) {
            newAddress.setLine1(suppliedAddress.getLine1());
            newAddress.setLine2(suppliedAddress.getLine2());
            newAddress.setTown(suppliedAddress.getTownCity());
            newAddress.setPostalCode(suppliedAddress.getPostcode());
            newAddress.setState(suppliedAddress.getState());
            newAddress.setAddressValidated(addressDataHelper.isAddressFormVerifiedInSession(suppliedAddress,
                    request.getSession()));
        }

        if (isEdit) {
            targetUserFacade.editAddress(newAddress);
            GlobalMessages.addFlashConfMessage(redirectAttributes, "address.success.update");
        }
        else {
            targetUserFacade.addAddress(newAddress);
            GlobalMessages.addFlashConfMessage(redirectAttributes, "address.success.created");
        }
        return ControllerConstants.Redirection.MY_ACCOUNT_ADDRESS_DETAILS_PAGE;
    }

    /**
     * Remove a user address from address code
     * 
     * @param addressCode
     * @param redirectModel
     * @return String
     */
    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_ADDRESS_REMOVE
            + ControllerConstants.MY_ACCOUNT_ADDRESS_CODE, method = RequestMethod.GET)
    public String removeAddress(@PathVariable("addressCode") final String addressCode,
            final RedirectAttributes redirectModel)
    {
        final AddressData addressData = new AddressData();
        addressData.setId(addressCode);
        targetUserFacade.removeAddress(addressData);
        GlobalMessages.addFlashConfMessage(redirectModel, "account.confirmation.address.removed");
        return ControllerConstants.Redirection.MY_ACCOUNT_ADDRESS_DETAILS_PAGE;
    }

    /**
     * Set a user address as default address from address code
     * 
     * @param addressCode
     * @param redirectModel
     * @return String
     */
    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_ADDRESS_SET_DEFAULT
            + ControllerConstants.MY_ACCOUNT_ADDRESS_CODE, method = RequestMethod.GET)
    public String setDefaultAddress(@PathVariable("addressCode") final String addressCode,
            final RedirectAttributes redirectModel)
    {
        final AddressData addressData = new AddressData();
        addressData.setDefaultAddress(true);
        addressData.setId(addressCode);
        targetUserFacade.setDefaultAddress(addressData);
        GlobalMessages.addFlashConfMessage(redirectModel, "account.confirmation.default.address.changed");
        return ControllerConstants.Redirection.MY_ACCOUNT_ADDRESS_DETAILS_PAGE;
    }

    /**
     * 
     * @param addressForm
     * @param model
     * @param isEdit
     * @param redirectAttributes
     * @return page name
     * @throws CMSItemNotFoundException
     */
    private String processAddress(final AddressForm addressForm, final Model model, final boolean isEdit,
            final RedirectAttributes redirectAttributes, final HttpServletRequest request)
            throws CMSItemNotFoundException {
        if (addressDataHelper.isSingleLineLookupFeatureEnabled()) {
            final ConfirmAddressForm confirmForm = new ConfirmAddressForm();
            confirmForm.setSuppliedAddress(addressForm);
            confirmForm.setUseSuppliedAddress(true);
            return confirmRedirect(confirmForm, model, redirectAttributes, request);
        }
        else {
            final TargetAddressData newAddress = new TargetAddressData();
            addressDataHelper.populateTargetAddressData(newAddress, addressForm);
            addressDataHelper.processAddressAndVerifyWithQas(model, addressForm, newAddress, isEdit);
            if (Boolean.TRUE.equals(addressForm.getDefaultAddress()))
            {
                newAddress.setDefaultAddress(true);
            }
            model.addAttribute("isCheckout", Boolean.FALSE);
            return ControllerConstants.Views.Pages.Account.ACCOUNT_CONFIRM_ADDRESS_PAGE;
        }
    }

}

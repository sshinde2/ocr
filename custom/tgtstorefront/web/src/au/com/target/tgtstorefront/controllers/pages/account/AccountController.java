/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.account;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.tgtstorefront.annotations.RequireHardLogIn;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
@Controller
@RequestMapping(value = ControllerConstants.MY_ACCOUNT)
@RequireHardLogIn
public class AccountController extends AbstractAccountController {

    @RequestMapping(method = RequestMethod.GET)
    public String account(final Model model) throws CMSItemNotFoundException
    {
        storeCmsPageInModel(model, getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.MY_ACCOUNT));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.MY_ACCOUNT));
        model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs());
        model.addAttribute("metaRobots", "no-index,no-follow");
        return ControllerConstants.Views.Pages.Account.ACCOUNT_HOME_PAGE;
    }
}

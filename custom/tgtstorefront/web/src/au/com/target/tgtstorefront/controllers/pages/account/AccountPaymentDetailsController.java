/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.account;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtstorefront.annotations.RequireHardLogIn;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
@Controller
@RequestMapping(value = ControllerConstants.MY_ACCOUNT)
@RequireHardLogIn
public class AccountPaymentDetailsController extends AbstractAccountController {

    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_PAYMENT, method = RequestMethod.GET)
    public String paymentDetails(final Model model) throws CMSItemNotFoundException
    {
        model.addAttribute("customerData", targetCustomerFacade.getCurrentCustomer());
        model.addAttribute("paymentInfoData", targetUserFacade.getSavedCreditCardPaymentInfosInValidTime());
        storeCmsPageInModel(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.MY_ACCOUNT_PAYMENT_DETAILS));
        setUpMetaDataForContentPage(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.MY_ACCOUNT_PAYMENT_DETAILS));
        model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.myPaymentDetails"));
        model.addAttribute("metaRobots", "no-index,no-follow");
        return ControllerConstants.Views.Pages.Account.ACCOUNT_PAYMENT_INFO_PAGE;
    }

    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_PAYMENT_SET_DEFAULT, method = RequestMethod.POST)
    public String setDefaultPaymentDetails(@RequestParam final String paymentInfoId)
    {
        CCPaymentInfoData paymentInfoData = null;
        if (StringUtils.isNotBlank(paymentInfoId))
        {
            paymentInfoData = targetUserFacade.getCCPaymentInfoForCode(paymentInfoId);
        }
        targetUserFacade.setDefaultPaymentInfo(paymentInfoData);
        return ControllerConstants.Redirection.MY_ACCOUNT_PAYMENT_INFO_PAGE;
    }

    @RequestMapping(value = ControllerConstants.MY_ACCOUNT_PAYMENT_REMOVE, method = RequestMethod.POST)
    public String removePaymentMethod(@RequestParam(value = "paymentInfoId") final String paymentMethodId,
            final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
    {
        targetUserFacade.unlinkCCPaymentInfo(paymentMethodId);
        GlobalMessages.addFlashConfMessage(redirectAttributes, "text.account.profile.paymentCart.removed");
        return ControllerConstants.Redirection.MY_ACCOUNT_PAYMENT_INFO_PAGE;
    }
}

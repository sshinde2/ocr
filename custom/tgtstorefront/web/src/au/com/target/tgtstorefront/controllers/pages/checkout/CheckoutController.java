/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import de.hybris.platform.servicelayer.session.SessionService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.CookieGenerator;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.prepopulatecart.TargetPrepopulateCheckoutCartFacade;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtstorefront.annotations.RequireHardLogIn;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.CookieRetrievalHelper;


/**
 * CheckoutController
 */
@Controller
@RequireHardLogIn
public class CheckoutController extends AbstractCheckoutController {

    protected static final Logger LOG = Logger.getLogger(CheckoutController.class);


    @Resource(name = "sessionService")
    private SessionService sessionService;

    @Resource(name = "targetCheckoutFacade")
    private TargetCheckoutFacade targetCheckoutFacade;

    @Resource(name = "targetPrepopulateCheckoutCartFacade")
    private TargetPrepopulateCheckoutCartFacade targetPrepopulateCheckoutCartFacade;

    @Resource(name = "guidCookieGenerator")
    private CookieGenerator guidCookieGenerator;

    @Resource(name = "cookieRetrievalHelper")
    private CookieRetrievalHelper cookieRetrievalHelper;

    @Value("#{configurationService.configuration.getProperty('tgtpaymentprovider.payment.feature.tns')}")
    private String tnsFeature;

    @Resource
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Resource(name = "salesApplicationFacade")
    private SalesApplicationFacade salesApplicationFacade;

    /**
     * Method used to determine the checkout redirect URL that will handle the checkout process.
     * 
     * @return A <code>String</code> object of the URL to redirect to.
     */
    @RequestMapping(value = ControllerConstants.CHECKOUT, method = RequestMethod.GET)
    public String getCheckoutRedirectUrl(final HttpServletRequest request, final Model model,
            final RedirectAttributes redirectAttributes) {
        TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        final String redirection = getRedirectionAndCheckSOH(model, redirectAttributes, DEFAULT_SOH_REDIRECT_URL,
                cartData);
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }
        if (cartData.isDataStale()) {
            cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        }
        if (isAnonymousUser()
                && !Boolean.TRUE.equals(getSessionService().getAttribute(WebConstants.ANONYMOUS_CHECKOUT))) {
            return ControllerConstants.Redirection.CHECKOUT_LOGIN;
        }

        if (StringUtils.equals(ObjectUtils.toString(sessionService.getAttribute(EXPEDITE_KEY)), TURNED_ON)
                && isValidGuid(request) && targetFeatureSwitchFacade.isFeatureEnabled(tnsFeature)
                && targetPrepopulateCheckoutCartFacade.isCartPrePopulatedForCheckout()) {
            return ControllerConstants.Redirection.EXPEDITE_CHECKOUT_ORDER_SUMMARY;
        }
        if (getCheckoutFacade().hasIncompleteDeliveryDetails()) {
            getCheckoutFacade().removeDeliveryInfo();
        }
        if (shouldGoToSPC()) {
            targetPrepopulateCheckoutCartFacade.isCartPrePopulatedForCheckout();
            return ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN;
        }

        return ControllerConstants.Redirection.CHECKOUT_YOUR_ADDRESS;
    }


    /**
     * @param request
     * @return valid GuidExist
     */
    private boolean isValidGuid(final HttpServletRequest request) {
        return StringUtils.equals(
                ObjectUtils.toString(request.getSession().getAttribute(
                        WebConstants.SECURE_GUID_SESSION_KEY)),
                cookieRetrievalHelper.getCookieValue(request, guidCookieGenerator.getCookieName()));
    }

    /**
     * 
     * @param binder
     */
    @InitBinder
    public void initBinder(final WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    /**
     * Display store detail
     * 
     * @param selectedStoreNumber
     * @param model
     * @return String
     */
    @RequestMapping(value = ControllerConstants.CHECKOUT + ControllerConstants.STORE, method = RequestMethod.GET)
    public String viewStoreDetails(@RequestParam("ssn") final String selectedStoreNumber,
            final Model model) {
        //TODO use a common method to add store data
        final TargetPointOfServiceData selectedStore = getTargetStoreLocatorFacade().getPointOfService(Integer
                .valueOf(selectedStoreNumber));
        model.addAttribute("store", selectedStore);

        return ControllerConstants.Views.Pages.MultiStepCheckout.VIEW_STORE_DETAIL_PAGE;
    }

}

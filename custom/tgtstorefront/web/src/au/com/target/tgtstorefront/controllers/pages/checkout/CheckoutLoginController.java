/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.servicelayer.session.SessionService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.AbstractLoginPageController;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.forms.GuestForm;
import au.com.target.tgtstorefront.forms.LoginForm;
import au.com.target.tgtstorefront.forms.RegisterForm;
import au.com.target.tgtstorefront.forms.validation.validator.GuestValidator;


/**
 * Checkout Login Controller. Handles login and register for the checkout flow. Please remind that
 * CheckoutLoginController is different than every other Checkout controller since it extends
 * AbstractLoginPageController
 */
@Controller
@RequestMapping(value = ControllerConstants.CHECKOUT_LOGIN)
public class CheckoutLoginController extends AbstractLoginPageController
{

    @Resource(name = "targetCheckoutFacade")
    private TargetCheckoutFacade checkoutFacade;

    @Resource(name = "guestValidator")
    private GuestValidator guestValidator;

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Resource(name = "sessionService")
    private SessionService sessionService;

    @Resource(name = "salesApplicationFacade")
    private SalesApplicationFacade salesApplicationFacade;

    /**
     * 
     * @return String
     */
    protected String getCheckoutUrl()
    {
        return ControllerConstants.Redirection.CHECKOUT;
    }

    /**
     * @return the checkoutFacade
     */
    protected TargetCheckoutFacade getCheckoutFacade() {
        return checkoutFacade;
    }

    @Override
    protected String getView()
    {
        return ControllerConstants.Views.Pages.Checkout.CHECKOUT_LOGIN_PAGE;
    }

    @Override
    protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
    {
        //We don't do any verification here since it's done in CheckoutController
        return getCheckoutUrl();
    }

    @Override
    protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
    {
        return getContentPageForLabelOrId("checkout-landing");
    }

    /**
     * 
     * @param session
     * @param model
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(method = RequestMethod.GET)
    public String doCheckoutLogin(
            final HttpSession session,
            final Model model,
            @RequestParam(value = ControllerConstants.ERROR, required = false) final String error)
            throws CMSItemNotFoundException
    {
        model.addAttribute("pageType", PageType.CheckoutLogin);
        model.addAttribute("forwardPage", ControllerConstants.Views.PageNames.BASKET);
        if (StringUtils.equals(error, ControllerConstants.Views.ErrorActions.CHECKOUT)) {
            GlobalMessages.addErrorMessage(model,
                    "checkout.error.paymentmethod.ipg.transactiondetails.queryresult.unavailable");
        }
        addCartData(model);
        return getDefaultLoginPage(session, model);
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String doCheckoutRegister()
    {
        return ControllerConstants.Redirection.CHECKOUT_LOGIN;
    }

    /**
     * 
     * @param form
     * @param bindingResult
     * @param model
     * @param request
     * @param response
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String doCheckoutRegister(@Valid final RegisterForm form, final BindingResult bindingResult,
            final Model model,
            final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
    {
        model.addAttribute("pageType", PageType.CheckoutLogin);
        addCartData(model);
        form.setSubscriptionSource(ControllerConstants.ACCREDIT_CHECKOUT);
        return processRegisterUserRequest(null, form, bindingResult, model, request, response);
    }

    @RequestMapping(value = "/guest", method = RequestMethod.GET)
    public String doCheckoutGuest()
    {
        return ControllerConstants.Redirection.CHECKOUT_LOGIN;
    }

    /**
     * 
     * @param form
     * @param bindingResult
     * @param model
     * @param request
     * @param response
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = "/guest", method = RequestMethod.POST)
    public String doCheckoutGuest(@Valid final GuestForm form, final BindingResult bindingResult,
            final Model model,
            final HttpServletRequest request, final HttpServletResponse response,
            final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
    {
        if (!targetFeatureSwitchFacade.isFeatureEnabled("checkout.allow.guest")) {
            GlobalMessages.addFlashInfoMessage(redirectAttributes, "checkout.guest.login.disabled");
            return ControllerConstants.Redirection.CHECKOUT_LOGIN;
        }

        model.addAttribute("pageType", PageType.CheckoutLogin);
        getGuestValidator().validate(form, bindingResult);
        addCartData(model);
        return processAnonymousCheckoutUserRequest(form, bindingResult, model, request, response);

    }

    protected String processAnonymousCheckoutUserRequest(final GuestForm form, final BindingResult bindingResult,
            final Model model,
            final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
    {
        try
        {
            if (bindingResult.hasErrors())
            {
                model.addAttribute("guestForm", form);
                model.addAttribute(new LoginForm());
                model.addAttribute(new RegisterForm());
                GlobalMessages.addErrorMessage(model, "form.global.error");
                return handleRegistrationError(model);
            }

            getCustomerFacade().createGuestUserForAnonymousCheckout(form.getEmail(),
                    "Guest");
            getGuidCookieStrategy().setCookie(request, response);
            getSessionService().setAttribute(WebConstants.ANONYMOUS_CHECKOUT, Boolean.TRUE);
        }
        catch (final DuplicateUidException e)
        {
            if (LOG.isInfoEnabled()) {
                LOG.info("guest registration failed: ", e);
            }
            GlobalMessages.addErrorMessage(model, "form.global.error");
            return handleRegistrationError(model);
        }
        return ControllerConstants.Redirection.CHECKOUT;
    }

    /**
     * Add cart data
     * 
     * @param model
     */
    protected void addCartData(final Model model) {
        final TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();

        if (targetFeatureSwitchFacade.isFeatureEnabled("checkout.allow.guest")) {
            // based on purchase option configuration, guest checkout has already been enabled/disabled. 
            // if there are any gift card in cart, prevent customer from using guest checkout
            if (checkoutFacade.isGiftCardInCart()) {
                cartData.setAllowGuestCheckout(false);
            }
        }
        model.addAttribute("cartData", cartData);
    }

    /**
     * @return the guestValidator
     */
    public GuestValidator getGuestValidator() {
        return guestValidator;
    }

}

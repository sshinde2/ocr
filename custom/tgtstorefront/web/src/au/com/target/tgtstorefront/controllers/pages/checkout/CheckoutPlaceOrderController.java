/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.InvalidCartException;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;
import au.com.target.tgtfacades.order.data.AdjustedEntryQuantityData;
import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.order.data.TargetPlaceOrderResult;
import au.com.target.tgtfacades.order.enums.TargetPlaceOrderResultEnum;
import au.com.target.tgtfacades.user.data.PinPadPaymentInfoData;
import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtstorefront.annotations.RequireHardLogIn;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.util.TargetSessionTokenUtil;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * @author rmcalave
 * 
 */
@Controller
@RequestMapping(value = ControllerConstants.PLACE_ORDER)
public class CheckoutPlaceOrderController extends AbstractCheckoutController {

    private static final Logger LOG = Logger.getLogger(CheckoutPlaceOrderController.class);

    private static final String PREPARE_PLACE_ORDER_RESULT_MSG_PREFIX = "placeorder.prepare.result.";
    private static final String PREPARE_PLACE_ORDER_RESULT_GENERAL_ERROR = "GENERAL_ERROR";

    private static final String PINPAD_CANCELLED_RESPONSE_CODE = "152";
    private static final String IPG_ABORT_ACTION = "ipg_abort_session";

    /**
     * @param model
     * @return String
     * @throws CMSItemNotFoundException
     * @throws InvalidCartException
     */
    @RequestMapping(method = RequestMethod.POST)
    @RequireHardLogIn
    public String placeOrder(final Model model, final RedirectAttributes redirectAttributes,
            final HttpServletRequest request)
                    throws CMSItemNotFoundException, InvalidCartException {
        if (!TargetSessionTokenUtil.isTokenValid(request)) {
            return ControllerConstants.Redirection.CHECKOUT_ORDER_PLACE_HOLDING;
        }

        return placeOrderInternal(model, redirectAttributes, request, false);
    }

    @RequestMapping(value = ControllerConstants.PLACE_ORDER_IPG, method = RequestMethod.POST)
    @RequireHardLogIn
    public String placeIpgOrder(final Model model, final RedirectAttributes redirectAttributes,
            final HttpServletRequest request)
                    throws CMSItemNotFoundException, InvalidCartException {
        if (!TargetSessionTokenUtil.isIpgTokenValid(request)) {
            return ControllerConstants.Redirection.CHECKOUT_ORDER_PLACE_HOLDING;
        }
        final CartModel curCart = getCheckoutFacade().getCart();
        synchronized (curCart.getCode().intern()) {
            final String requestToken = request.getParameter(ControllerConstants.IPG_PARAM_TOKEN);
            getCheckoutFacade().updateIpgPaymentInfoWithToken(requestToken);
            return placeOrderInternal(model, redirectAttributes, request, true);
        }
    }

    @RequestMapping(value = ControllerConstants.PLACE_ORDER_IPG_IFRAME_RETURN, method = { RequestMethod.GET,
            RequestMethod.POST })
    public String fromIpg(
            @RequestParam(value = ControllerConstants.IPG_PARAM_TOKEN) final String ipgToken,
            @RequestParam(value = ControllerConstants.IPG_PARAM_SESSIONID) final String ipgSessionId,
            @RequestParam(value = ControllerConstants.IPG_PARAM_QS, required = false) final String qsParam,
            final Model model,
            final HttpServletRequest request) {
        request.getSession().setAttribute(ipgToken, ipgToken);
        final String redirectUrl = getRedirectUrl(ipgToken, qsParam, ipgSessionId);
        if (StringUtils.isNotEmpty(redirectUrl)) {
            model.addAttribute("ipgRedirectUrl", redirectUrl);
        }
        return ControllerConstants.Views.Pages.Checkout.CHECKOUT_IPG_RETURN_PAGE;
    }

    public String getRedirectUrl(final String sst, final String qs, final String ipgSessionId) {
        // Handle the back button. If user click back button, IPG send us the return url with qs param populated with 'ipg_abort_session'
        // we then populate return URL with payment page to send the user back
        String ipgRedirectUrl = null;

        if (IPG_ABORT_ACTION.equals(qs)) {
            ipgRedirectUrl = ControllerConstants.PAYMENT;
        }
        else if (StringUtils.isNotEmpty(sst)) {
            // query transaction results to see whether customer cancels the payment. If so, go back to payment page
            final TargetQueryTransactionDetailsResult result = getTargetPlaceOrderFacade()
                    .getQueryTransactionDetailResults();

            if (result != null && result.isCancel()) {
                ipgRedirectUrl = ControllerConstants.REVIEW;
            }
            else {
                ipgRedirectUrl = ControllerConstants.PLACE_ORDER
                        + ControllerConstants.PLACE_IPG_ORDER_HOLDING + "?"
                        + ControllerConstants.IPG_PARAM_TOKEN
                        + "="
                        + sst + "&"
                        + ControllerConstants.IPG_PARAM_SESSIONID
                        + "="
                        + ipgSessionId;
            }

        }
        return ipgRedirectUrl;
    }

    @RequestMapping(value = ControllerConstants.PLACE_IPG_ORDER_HOLDING, method = RequestMethod.GET)
    @RequireHardLogIn
    public String ipgCheckoutWait(
            @RequestParam(value = ControllerConstants.IPG_PARAM_TOKEN, required = true) final String ipgToken,
            @RequestParam(value = ControllerConstants.IPG_PARAM_SESSIONID, required = true) final String ipgSessionId,
            final Model model)
                    throws CMSItemNotFoundException {
        model.addAttribute("placeIpgOrderUrl", ControllerConstants.PLACE_ORDER
                + ControllerConstants.PLACE_ORDER_IPG);
        model.addAttribute("ipgTokenName", ControllerConstants.IPG_PARAM_TOKEN);
        model.addAttribute("ipgTokenValue", ipgToken);
        model.addAttribute("ipgSessionName", ControllerConstants.IPG_PARAM_SESSIONID);
        model.addAttribute("ipgSessionValue", ipgSessionId);
        return ControllerConstants.Views.Pages.MultiStepCheckout.PLACE_ORDER_HOLDING_PAGE;
    }

    @ExceptionHandler(Exception.class)
    public String handleExceptionDuringCheckout(final Exception e, final HttpServletRequest request) {
        LOG.error(SplunkLogFormatter.formatMessage("Exception during place order",
                TgtutilityConstants.ErrorCode.ERR_CHECKOUT), e);
        final String ipgSessionId = request.getParameter(ControllerConstants.IPG_PARAM_SESSIONID);
        if (ipgSessionId != null) {
            final String ipgToken = request.getParameter(ControllerConstants.IPG_PARAM_TOKEN);
            if (ipgToken != null) {
                getTargetPlaceOrderFacade().reverseGiftCardPayment(ipgToken, ipgSessionId);
            }
            String ipgRedirectUrl = ControllerConstants.THANK_YOU + ControllerConstants.THANK_YOU_CHECK;
            if (isAnonymousCheckout()) {
                ipgRedirectUrl = ipgRedirectUrl + "?" + ControllerConstants.ERROR + "="
                        + ControllerConstants.Views.ErrorActions.CHECKOUT;
            }
            request.setAttribute("ipgRedirectUrl", ipgRedirectUrl);
            return ControllerConstants.Views.Pages.Checkout.CHECKOUT_IPG_RETURN_PAGE;
        }
        return ControllerConstants.Redirection.CHECKOUT_THANK_YOU_CHECK;
    }

    protected String placeOrderInternal(final Model model, final RedirectAttributes redirectAttributes,
            final HttpServletRequest request, final boolean isIpgPayment) throws CMSItemNotFoundException {
        try {
            request.getSession().removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);

            if (!isGiftCardPaymentAllowed()) {
                GlobalMessages.addFlashErrorMessage(redirectAttributes,
                        "checkout.error.paymentmethod.ipg.giftcard.not.allowed");
                getCheckoutFacade().removePaymentInfo();
                return ControllerConstants.Redirection.CHECKOUT_PAYMENT;
            }

            if (getFlybuysDiscountFacade().checkFlybuysDiscountUsed()) {
                getCheckoutFacade().recalculateCart();
            }
            getCheckoutFacade().validateDeliveryMode();

            final TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();

            if (!SalesApplication.KIOSK.equals(getSalesApplicationFacade().getCurrentSalesApplication())) {
                final String redirection = getRedirectionAndCheckSOH(model, redirectAttributes,
                        DEFAULT_SOH_REDIRECT_URL,
                        cartData);

                // if a redirection required (e.g. all items out of stock) reverse gift card payment before being redirected
                if (StringUtils.isNotEmpty(redirection)) {
                    redirectAttributes.addFlashAttribute("orderPlaceFailed", Boolean.TRUE);

                    final List<TargetCardResult> giftCards = getTargetPlaceOrderFacade().getGiftCardPayments(
                            getCheckoutFacade().getCart());

                    if (CollectionUtils.isNotEmpty(giftCards)) {
                        getTargetPlaceOrderFacade().reverseGiftCardPayment();
                    }

                    return redirection;
                }

                // check for SOH updates. If so, reverse gift card payment and redirect
                if (hasSOHUpdated(model)) {
                    redirectAttributes.addFlashAttribute("orderPlaceFailed", Boolean.TRUE);

                    final List<TargetCardResult> giftCards = getTargetPlaceOrderFacade().getGiftCardPayments(
                            getCheckoutFacade().getCart());

                    if (CollectionUtils.isNotEmpty(giftCards)) {
                        getTargetPlaceOrderFacade().reverseGiftCardPayment();
                        return ControllerConstants.Redirection.CHECKOUT_ORDER_SUMMARY;
                    }
                }
            }

            // Remove old order values from the session so that the holding page
            // will function correctly
            request.getSession().removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_RESULT);
            request.getSession().removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE);

            // Set the IP address and browser cookies in checkout cart for use by fraud check when the order is created
            getTargetPlaceOrderFacade().setClientDetailsForFraudDetection(
                    getRequestHeadersHelper().getRemoteIPAddress(request),
                    getCookieStringBuilder().buildCookieString(request.getCookies()));


            final TargetPlaceOrderResult placeOrderResult = handlePlaceOrder();

            final TargetPlaceOrderResultEnum placeOrderResultEnum = placeOrderResult.getPlaceOrderResult();
            request.getSession().setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_RESULT,
                    placeOrderResultEnum);

            String orderCode = null;
            if (TargetPlaceOrderResultEnum.SUCCESS.equals(placeOrderResultEnum)) {
                orderCode = placeOrderResult.getAbstractOrderModel().getCode();
                request.getSession().setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE, orderCode);
            }
            return handlePlaceOrderResult(placeOrderResultEnum, orderCode, redirectAttributes, isIpgPayment);
        }
        finally {
            request.getSession().removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);
        }

    }

    /**
     * Checks whether a SOH has happened
     * 
     * @param model
     * @return boolean
     */
    protected boolean hasSOHUpdated(final Model model) {
        boolean sohUpdated = false;

        if (model.containsAttribute(ControllerConstants.SOH_UPDATES) && model.asMap() != null) {
            final Set sohUpdates = (Set)model.asMap().get(ControllerConstants.SOH_UPDATES);

            if (CollectionUtils.isNotEmpty(sohUpdates)) {
                final Iterator<AdjustedCartEntriesData> iter = sohUpdates.iterator();

                if (iter != null && iter.hasNext()) {
                    final AdjustedCartEntriesData adjustedCartEntriesData = iter.next();

                    if (adjustedCartEntriesData != null) {
                        final List<AdjustedEntryQuantityData> adjustedItems = adjustedCartEntriesData
                                .getAdjustedItems();
                        final List<AdjustedEntryQuantityData> outOfStockItems = adjustedCartEntriesData
                                .getOutOfStockItems();

                        // something has changed in shopping cart
                        if (CollectionUtils.isNotEmpty(adjustedItems) || CollectionUtils.isNotEmpty(outOfStockItems)) {
                            sohUpdated = true;
                        }
                    }
                }
            }
        }
        return sohUpdated;
    }

    /**
     * Method to check if payment mode is gift card in cart but the cart doesn't permit gift card payment, then get gift
     * card payments and reverse them and stop order to be placed. Redirect to payment page.
     * 
     * @return true, if gift card payment is allowed.
     */
    protected boolean isGiftCardPaymentAllowed() {
        if (getCheckoutFacade().isPaymentModeGiftCard() && !getCheckoutFacade().isGiftCardPaymentAllowedForCart()) {
            final List<TargetCardResult> giftCards = getTargetPlaceOrderFacade().getGiftCardPayments(
                    getCheckoutFacade().getCart());
            if (CollectionUtils.isNotEmpty(giftCards)) {
                getTargetPlaceOrderFacade().reverseGiftCardPayment();
            }
            return false;
        }
        return true;
    }

    /**
     * @return placeOrderResult
     */
    protected TargetPlaceOrderResult handlePlaceOrder() {
        TargetPlaceOrderResult placeOrderResult = null;
        if (SalesApplication.KIOSK.equals(getSalesApplicationFacade().getCurrentSalesApplication())) {
            placeOrderResult = getTargetPlaceOrderFacade().finishPlaceOrder();
        }
        else {
            placeOrderResult = getTargetPlaceOrderFacade().placeOrder();
        }
        return placeOrderResult;
    }

    @RequestMapping(value = ControllerConstants.PLACE_ORDER_HOLDING, method = RequestMethod.GET)
    @RequireHardLogIn
    public String checkoutWait(final HttpServletRequest request, final RedirectAttributes redirectAttributes)
            throws CMSItemNotFoundException {
        final TargetPlaceOrderResultEnum placeOrderResultEnum = (TargetPlaceOrderResultEnum)request.getSession()
                .getAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_RESULT);

        if (placeOrderResultEnum == null) {
            return ControllerConstants.Views.Pages.MultiStepCheckout.PLACE_ORDER_HOLDING_PAGE;
        }

        final String orderCode = (String)request.getSession().getAttribute(
                ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE);

        return handlePlaceOrderResult(placeOrderResultEnum, orderCode, redirectAttributes, false);
    }

    @RequestMapping(value = ControllerConstants.PLACE_ORDER_PREPARE, method = RequestMethod.POST)
    @RequireHardLogIn
    public String preparePlaceOrder(
            @RequestParam(value = "kioskStoreNumber", required = true) final String kioskStoreNumber,
            final Model model, final HttpServletResponse response) throws IOException {

        final SalesApplication currentSalesApplication = getSalesApplicationFacade().getCurrentSalesApplication();
        if (!SalesApplication.KIOSK.equals(currentSalesApplication)) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }

        final Object mutex = RequestContextHolder.getRequestAttributes().getSessionMutex();
        synchronized (mutex) {
            if (StringUtils.isNotBlank(kioskStoreNumber)) {
                getCheckoutFacade().setKioskStoreNumber(kioskStoreNumber);
            }

            boolean isPrepareSuccess = true;
            String errorMessage = null;
            final TargetPlaceOrderResult preparePlaceOrderResult = getTargetPlaceOrderFacade().prepareForPlaceOrder();
            if (preparePlaceOrderResult != null
                    && !TargetPlaceOrderResultEnum.PREPARE_SUCCESS
                            .equals(preparePlaceOrderResult.getPlaceOrderResult())) {
                isPrepareSuccess = false;
                errorMessage = getMessageFromProperties(PREPARE_PLACE_ORDER_RESULT_MSG_PREFIX
                        + preparePlaceOrderResult.getPlaceOrderResult().name(),
                        PREPARE_PLACE_ORDER_RESULT_MSG_PREFIX + PREPARE_PLACE_ORDER_RESULT_GENERAL_ERROR);
            }

            final TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
            populateModelDataForPreparePlaceOrder(model, isPrepareSuccess, errorMessage, cartData);
        }
        return ControllerConstants.Views.Pages.MultiStepCheckout.PLACE_ORDER_PREPARE_PAGE;
    }

    /**
     * @param model
     * @param isPrepareSuccess
     * @param errorMessage
     * @param cartData
     */
    protected void populateModelDataForPreparePlaceOrder(final Model model, final boolean isPrepareSuccess,
            final String errorMessage, final TargetCartData cartData) {
        model.addAttribute("cartData", cartData);
        model.addAttribute("isPrepareSuccess", Boolean.valueOf(isPrepareSuccess));
        model.addAttribute("errorMsg", errorMessage);
    }

    @Override
    protected String getRedirectionAndCheckSOH(final Model model, final RedirectAttributes redirectAttributes,
            final String url, final TargetCartData cartData) {
        final String redirect = super.getRedirectionAndCheckSOH(model, redirectAttributes, url, cartData);

        if (StringUtils.isNotBlank(redirect)) {
            return redirect;
        }
        if (getCheckoutFacade().hasIncompleteDeliveryDetails()) {
            return ControllerConstants.Redirection.CHECKOUT_YOUR_ADDRESS;
        }
        if (hasNoPaymentInfo(cartData)) {
            return ControllerConstants.Redirection.CHECKOUT_PAYMENT;
        }
        return getRedirectionIfFlybuysReassessed(redirectAttributes);
    }

    /**
     * Process place order results and redirect appropriately
     * 
     * @param placeOrderResult
     * @param orderCode
     * @param redirectAttributes
     * @param isIpgPayment
     * @return redirect URL
     * @throws CMSItemNotFoundException
     */
    protected String handlePlaceOrderResult(final TargetPlaceOrderResultEnum placeOrderResult, final String orderCode,
            final RedirectAttributes redirectAttributes, final boolean isIpgPayment)
                    throws CMSItemNotFoundException {

        final TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();

        if (TargetPlaceOrderResultEnum.SUCCESS.equals(placeOrderResult)) {
            return ControllerConstants.Redirection.CHECKOUT_THANK_YOU.concat(orderCode);
        }

        if (TargetPlaceOrderResultEnum.PAYMENT_FAILURE.equals(placeOrderResult)) {

            final TargetCCPaymentInfoData paymentInfo = (TargetCCPaymentInfoData)cartData.getPaymentInfo();

            final PinPadPaymentInfoData pinPadPaymentInfo = paymentInfo.getPinPadPaymentInfoData();
            if (pinPadPaymentInfo != null) {
                if (PINPAD_CANCELLED_RESPONSE_CODE.equals(pinPadPaymentInfo.getRespAscii())) {
                    GlobalMessages.addFlashErrorMessage(redirectAttributes, "payment.error.pinpad.cancelled");
                }
                else {
                    GlobalMessages.addFlashErrorMessage(redirectAttributes, "payment.error.pinpad.declined");
                }
                return ControllerConstants.Redirection.CHECKOUT_ORDER_SUMMARY;
            }
            else if (isIpgPayment) {
                GlobalMessages.addFlashErrorMessage(redirectAttributes, "payment.error.ipg");
                return ControllerConstants.Redirection.CHECKOUT_ORDER_SUMMARY;
            }
            else { // redirect to Payment page with error message for others
                if (paymentInfo.isPaymentSucceeded()) {
                    GlobalMessages.addFlashErrorMessage(redirectAttributes, "payment.error.existingcreditcard");
                }
                else {
                    GlobalMessages.addFlashErrorMessage(redirectAttributes, "payment.error.newcreditcard");
                }
            }
            return ControllerConstants.Redirection.CHECKOUT_PAYMENT;
        }


        if (TargetPlaceOrderResultEnum.PAYMENT_FAILURE_DUETO_MISSMATCH_TOTALS.equals(placeOrderResult)
                && isIpgPayment) {
            GlobalMessages.addFlashErrorMessage(redirectAttributes, "payment.error.ipg.mismatchtotals");
            return ControllerConstants.Redirection.CHECKOUT_ORDER_SUMMARY;
        }

        // For validation errors or SOH check go to CHECKOUT_ORDER_SUMMARY
        if (TargetPlaceOrderResultEnum.INSUFFICIENT_STOCK.equals(placeOrderResult)
                || TargetPlaceOrderResultEnum.USER_MISMATCH.equals(placeOrderResult)
                || TargetPlaceOrderResultEnum.INVALID_CART.equals(placeOrderResult)) {
            return ControllerConstants.Redirection.CHECKOUT_ORDER_SUMMARY;
        }


        // treat all other cases including UNKNOWN as order creation failed
        final String logCode = null != orderCode ? orderCode : (null != cartData ? cartData.getCode() : null);
        return handleOrderCreationFailed(placeOrderResult, logCode);
    }

    /**
     * 
     * @param request
     * @return List<CheckoutSteps>
     */
    @Override
    @ModelAttribute("checkoutSteps")
    protected List<CheckoutSteps> addCheckoutStepsToModel(final HttpServletRequest request) {
        return null;
    }
}

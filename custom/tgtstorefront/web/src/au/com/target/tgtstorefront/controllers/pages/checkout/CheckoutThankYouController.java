/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.support.RequestContextUtils;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.checkout.flow.TargetCheckoutCustomerStrategy;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.flow.impl.SessionOverrideCheckoutFlowFacade;
import au.com.target.tgtfacades.order.TargetCartFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.TargetPlaceOrderFacade;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.user.data.TargetRegisterData;
import au.com.target.tgtstorefront.annotations.RequireHardLogIn;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.forms.RegisterForm;
import au.com.target.tgtstorefront.security.AutoLoginStrategy;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
@Controller
@RequestMapping(value = ControllerConstants.THANK_YOU)
@RequireHardLogIn
public class CheckoutThankYouController extends AbstractCheckoutController {
    @Autowired
    private TargetCheckoutCustomerStrategy targetCheckoutCustomerStrategy;

    @Autowired
    private AutoLoginStrategy autoLoginStrategy;

    @Autowired
    private TargetCartFacade targetCartFacade;

    @Autowired
    private TargetPlaceOrderFacade targetPlaceOrderFacade;

    @Autowired
    private TargetCheckoutFacade targetCheckoutFacade;

    @Autowired
    private SessionService sessionService;

    /**
     * 
     * @param orderId
     * @param model
     * @param request
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.THANK_YOU_ORDER_CODE, method = RequestMethod.GET)
    public String orderConfirmation(@PathVariable("orderCode") final String orderId, final Model model,
            final HttpServletRequest request)
            throws CMSItemNotFoundException {
        final String orderCode = (String)request.getSession().getAttribute(
                ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE);
        SessionOverrideCheckoutFlowFacade.resetSessionOverrides();
        if (null == orderCode || !orderId.equals(orderCode)) {
            return ControllerConstants.Redirection.CART;
        }
        //For Guest Checkout 
        final boolean isAnonymousCheckout = targetCheckoutCustomerStrategy.isAnonymousCheckout()
                && !Boolean.TRUE.equals(sessionService.getAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST));
        model.addAttribute("isAnonymousCheckout", Boolean.valueOf(isAnonymousCheckout));
        final TargetOrderData orderDetails = getOrderFacade().getOrderDetailsForCode(orderCode);
        if (isAnonymousCheckout) {
            final RegisterForm registerForm = new RegisterForm();
            populateRegisterFormFromOrder(orderDetails, registerForm);
            model.addAttribute(registerForm);
        }
        populateOrderDetail(model, orderDetails);
        final AbstractPageModel cmsPage = getContentPageForLabelOrId(
                ControllerConstants.CmsPageLabel.CHECKOUT_THANK_YOU_CMS_PAGE_LABEL);
        storeCmsPageInModel(model, cmsPage);
        setUpMetaDataForContentPage(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.CHECKOUT_THANK_YOU_CMS_PAGE_LABEL));
        model.addAttribute("metaRobots", "no-index,no-follow");
        if (shouldGoToSPC()) {
            model.addAttribute(SPC_KEY, Boolean.TRUE);
        }
        storeAnalyticsPageUrl(model, ControllerConstants.THANK_YOU);
        getSessionService().removeAttribute(TgtCoreConstants.SESSION_POSTCODE);
        return ControllerConstants.Views.Pages.MultiStepCheckout.THANK_YOU_PAGE;
    }

    @RequestMapping(value = ControllerConstants.GUEST_REGISTER
            + ControllerConstants.THANK_YOU_ORDER_CODE, method = RequestMethod.GET)
    public String convertGuestUserToRegisterUserGet(@PathVariable("orderCode") final String orderId)
            throws CMSItemNotFoundException {
        return ControllerConstants.Redirection.CHECKOUT_THANK_YOU + orderId;
    }

    /**
     * Convert guest user as register user
     * 
     * @param orderId
     * @param form
     * @param bindingResult
     * @param model
     * @param request
     * @param response
     * @return String
     * @throws CMSItemNotFoundException
     */

    @RequestMapping(value = ControllerConstants.GUEST_REGISTER
            + ControllerConstants.THANK_YOU_ORDER_CODE, method = RequestMethod.POST)
    public String convertGuestUserToRegisterUser(@PathVariable("orderCode") final String orderId,
            @Valid final RegisterForm form,
            final BindingResult bindingResult,
            final Model model, final HttpServletRequest request, final HttpServletResponse response)
            throws CMSItemNotFoundException {
        if (bindingResult.hasErrors()) {
            GlobalMessages.addErrorMessage(model, "form.global.error");
            final String orderCode = (String)request.getSession().getAttribute(
                    ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE);
            final TargetOrderData orderDetails = getOrderFacade().getOrderDetailsForCode(orderCode);
            populateOrderDetail(model, orderDetails);
            return handleRegistrationError(model);
        }

        final TargetRegisterData data = new TargetRegisterData();
        data.setUid(form.getUid());
        data.setFirstName(form.getFirstName());
        data.setLastName(form.getLastName());
        data.setLogin(form.getEmail());
        data.setPassword(form.getPwd());
        data.setTitleCode(form.getTitleCode());
        data.setMobileNumber(form.getMobileNumber());
        try {
            getCustomerFacade().convertGuestToRegisteredUser(data);
            autoLoginStrategy.login(form.getEmail(), form.getPwd(), request, response);
            GlobalMessages.addConfMessage(RequestContextUtils.getOutputFlashMap(request),
                    "registration.confirmation.message.title");
        }
        catch (final DuplicateUidException e) {
            bindingResult.rejectValue("email", "user.exists");

            LOG.warn("registration failed: " + e);
            GlobalMessages.addErrorMessage(model, "form.global.error");
            final String orderCode = (String)request.getSession().getAttribute(
                    ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE);
            final TargetOrderData orderDetails = getOrderFacade().getOrderDetailsForCode(orderCode);
            populateOrderDetail(model, orderDetails);
            storeAnalyticsPageUrl(model, ControllerConstants.THANK_YOU.concat(ControllerConstants.GUEST_REGISTER));
            return handleRegistrationError(model);
        }

        return getSuccessRedirect();
    }

    @RequestMapping(value = ControllerConstants.THANK_YOU_CHECK, method = RequestMethod.GET)
    public String checkoutProblem(final Model model,
            @RequestParam(value = "cart", required = false) final String cartNumber)
            throws CMSItemNotFoundException {

        // Quick check that the number is ok for HTML injection
        if (cartNumber != null && Pattern.matches("^[\\d]{8,12}$", cartNumber)) {
            model.addAttribute("cartNumber", cartNumber);
        }
        model.addAttribute("checkoutSteps", getCheckoutSteps());
        storeCmsPageInModel(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.CHECKOUT_THANK_YOU_CHECK));
        getSessionService().removeAttribute(TgtCoreConstants.SESSION_POSTCODE);

        // Check whether there is existing payment for the cart
        final CartModel cart = targetCheckoutFacade.getCart();
        if (cart != null) {
            final Double amountPaidSoFar = getAmountPaidSoFar(cart);

            if (cart.getTotalPrice().compareTo(Double.valueOf(0d)) == 0 // fresh cart
                    || isPaidValueMatchesCartTotal(cart, amountPaidSoFar)) {

                model.addAttribute("paidValueMatchesCartTotal", Boolean.TRUE);
            }
        }
        else {// Cart has been converted to order. So, should have fully paid
            model.addAttribute("paidValueMatchesCartTotal", Boolean.TRUE);
        }
        return ControllerConstants.Views.Pages.MultiStepCheckout.THANK_YOU_CHECK_PROBLEM_PAGE;
    }

    protected Double getAmountPaidSoFar(final CartModel cart) {
        Double paidAmount = Double.valueOf(0);
        if (cart != null) {
            paidAmount = targetPlaceOrderFacade.getAmountPaidSoFar(cart);
        }
        return paidAmount;
    }

    protected boolean isPaidValueMatchesCartTotal(final CartModel cart, final Double amountPaidSoFar) {
        boolean isValuesMatch = false;
        if (amountPaidSoFar != null && amountPaidSoFar.doubleValue() > 0) {
            if (cart.getTotalPrice().compareTo(amountPaidSoFar) == 0) {
                isValuesMatch = true;
            }
        }
        return isValuesMatch;
    }

    private String handleRegistrationError(final Model model) throws CMSItemNotFoundException {
        final AbstractPageModel cmsPage = getContentPageForLabelOrId(
                ControllerConstants.CmsPageLabel.CHECKOUT_THANK_YOU_CMS_PAGE_LABEL);
        storeCmsPageInModel(model, cmsPage);
        setUpMetaDataForContentPage(model,
                getContentPageForLabelOrId(ControllerConstants.CmsPageLabel.CHECKOUT_THANK_YOU_CMS_PAGE_LABEL));
        model.addAttribute("isAnonymousCheckout", Boolean.TRUE);
        return ControllerConstants.Views.Pages.MultiStepCheckout.THANK_YOU_PAGE;
    }

    /**
     * @param model
     * @param orderDetails
     */
    private void insertDataForCNCStore(final Model model, final TargetOrderData orderDetails) {
        final Integer storeNumber = orderDetails.getCncStoreNumber();
        if (storeNumber != null) {
            model.addAttribute("selectedStore",
                    getTargetStoreLocatorFacade().getPointOfService(storeNumber));
        }
    }

    private String getSuccessRedirect() {
        if (targetCartFacade.hasItemsInAnyCart()) {
            return ControllerConstants.Redirection.CART;
        }
        return ControllerConstants.Redirection.MY_ACCOUNT;
    }

    /**
     * populate order detail
     * 
     * @param model
     * @param orderDetails
     */
    private void populateOrderDetail(final Model model, final TargetOrderData orderDetails) {



        if (orderDetails.getEntries() != null && !orderDetails.getEntries().isEmpty()) {
            for (final OrderEntryData entry : orderDetails.getEntries()) {
                final String productCode = entry.getProduct().getCode();
                final ProductData product = getProductFacade().getProductForCodeAndOptions(productCode,
                        Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.CATEGORIES));
                entry.setProduct(product);
            }
        }

        insertDataForCNCStore(model, orderDetails);
        model.addAttribute("orderCode", orderDetails.getCode());
        model.addAttribute("orderData", orderDetails);
        model.addAttribute("allItems", orderDetails.getEntries());
        model.addAttribute("deliveryAddress", orderDetails.getDeliveryAddress());
        model.addAttribute("deliveryMode", orderDetails.getDeliveryMode());
        model.addAttribute("paymentInfo", orderDetails.getPaymentInfo());
        model.addAttribute("email", getCustomerFacade().getCurrentCustomer().getUid());
        model.addAttribute("pageType", PageType.OrderConfirmation);
        model.addAttribute("otherCarts", Boolean.valueOf(targetCartFacade.hasItemsInAnyCart()));
        model.addAttribute("checkoutSteps", getCheckoutSteps());

    }

    /**
     * Populate register Form
     * 
     * @param orderDetails
     * @param registerForm
     */
    public void populateRegisterFormFromOrder(final TargetOrderData orderDetails, final RegisterForm registerForm) {

        if (null != orderDetails) {
            registerForm.setUid(getCheckoutFacade().getOrderUserUid(orderDetails.getCode()));
            if (null != orderDetails.getDeliveryAddress().getTitle()) {
                registerForm.setTitleCode(orderDetails.getDeliveryAddress().getTitleCode());
            }
            registerForm.setEmail(orderDetails.getEmail());
            registerForm.setFirstName(orderDetails.getDeliveryAddress().getFirstName());
            registerForm.setLastName(orderDetails.getDeliveryAddress().getLastName());
        }

    }

    /**
     * 
     * @param request
     * @return List<CheckoutSteps>
     */
    @Override
    @ModelAttribute("checkoutSteps")
    protected List<CheckoutSteps> addCheckoutStepsToModel(final HttpServletRequest request) {
        return null;
    }
}

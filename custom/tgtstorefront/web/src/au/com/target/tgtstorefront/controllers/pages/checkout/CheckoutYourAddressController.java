/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.order.data.ClickAndCollectDeliveryDetailData;
import au.com.target.tgtfacades.order.data.DeliveryModeData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtstorefront.annotations.RequireHardLogIn;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.data.CheckoutDeliveryTypeData;
import au.com.target.tgtstorefront.controllers.util.AddressDataHelper;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.forms.AddressForm;
import au.com.target.tgtstorefront.forms.ConfirmAddressForm;
import au.com.target.tgtstorefront.forms.checkout.ClickAndCollectDetailsForm;
import au.com.target.tgtstorefront.forms.validation.validator.AddressFormValidator;
import au.com.target.tgtutility.util.PhoneValidationUtils;
import au.com.target.tgtverifyaddr.data.FormattedAddressData;
import au.com.target.tgtverifyaddr.exception.ServiceNotAvailableException;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
@Controller
@RequestMapping(value = ControllerConstants.YOUR_ADDRESS)
@RequireHardLogIn
public class CheckoutYourAddressController extends AbstractCheckoutController {

    protected static final Logger LOG = Logger.getLogger(CheckoutYourAddressController.class);

    private static final String FEECHANGE_NOTIFICATION = "delivery.postcode.feechange.notification";

    @Resource(name = "addressFormValidator")
    private AddressFormValidator addressFormValidator;

    @Resource(name = "addressDataHelper")
    private AddressDataHelper addressDataHelper;

    @Resource(name = "targetStoreLocatorFacade")
    private TargetStoreLocatorFacade targetStoreLocatorFacadeImpl;

    @Resource
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    /* (non-Javadoc)
     * @see au.com.target.tgtstorefront.controllers.pages.checkout.AbstractCheckoutController#getRedirectionAndCheckSOH(org.springframework.ui.Model, org.springframework.web.servlet.mvc.support.RedirectAttributes, java.lang.String, au.com.target.tgtfacades.order.data.TargetCartData)
     */
    @Override
    protected String getRedirectionAndCheckSOH(final Model model, final RedirectAttributes redirectAttributes,
            final String redirectUrl,
            final TargetCartData cartData) {
        final String redirect = super.getRedirectionAndCheckSOH(model, redirectAttributes, redirectUrl, cartData);

        if (StringUtils.isNotBlank(redirect)) {
            return redirect;
        }

        if (cartData.isDataStale()) {
            //check if the updated cart has just digital products and the delivery type is digital delivery.
            final CartData updatedCartData = getCheckoutFacade().getCheckoutCart();
            final de.hybris.platform.commercefacades.order.data.DeliveryModeData deliveryMode = updatedCartData
                    .getDeliveryMode();
            if (deliveryMode instanceof TargetZoneDeliveryModeData) {
                final TargetZoneDeliveryModeData targetDeliveryMode = (TargetZoneDeliveryModeData)deliveryMode;
                if (targetDeliveryMode.isDigitalDelivery()) {
                    return ControllerConstants.Redirection.CHECKOUT_PAYMENT;
                }
            }
        }

        getFlybuysDiscountFacade().reassessFlybuysDiscountOnCheckoutCart();

        return StringUtils.EMPTY;
    }

    /**
     * This is the entry point (first page) for the the checkout process. The page returned by this call will show a
     * list of customer addresses. If there is a default address, this will be selected in the view. If there are no
     * address then we redirect to the create new delivery address page.
     * 
     * @param model
     *            - the model for the view.
     * @return - the deliver address step page.
     * @throws CMSItemNotFoundException
     *             - when a CMS page is not found
     */
    @RequestMapping(method = RequestMethod.GET)
    public String doChooseDeliveryAddress(final Model model, final RedirectAttributes redirectAttributes)
            throws CMSItemNotFoundException {
        final CartModel cartModel = getCheckoutFacade().getCart();
        getCheckoutFacade().setThreatMatrixSessionID(cartModel);

        TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();

        final String redirection = getRedirectionAndCheckSOH(model, redirectAttributes, DEFAULT_SOH_REDIRECT_URL,
                cartData);
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }

        if (cartData.isDataStale()) {
            cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        }

        String deliveryModeCode = null;
        if (cartData.getDeliveryMode() != null) {
            deliveryModeCode = cartData.getDeliveryMode().getCode();
        }

        addModelDataForSelectDeliveryAddress(cartData, model, deliveryModeCode, null);

        final ClickAndCollectDetailsForm cncForm = new ClickAndCollectDetailsForm();
        final Integer storeNumber = cartData.getCncStoreNumber();
        final de.hybris.platform.commercefacades.order.data.DeliveryModeData deliveryMode = cartData.getDeliveryMode();
        boolean isdeliveryToStore = false;
        if (deliveryMode instanceof TargetZoneDeliveryModeData &&
                ((TargetZoneDeliveryModeData)deliveryMode).isDeliveryToStore()) {
            isdeliveryToStore = true;
        }

        if (isdeliveryToStore && null != storeNumber) {
            cncForm.setDeliveryMode(deliveryModeCode);
            cncForm.setSelectedStoreNumber(storeNumber.toString());
            final AddressData cncDeliveryAddress = cartData.getDeliveryAddress();
            cncForm.setTitle(cncDeliveryAddress.getTitleCode());
            cncForm.setFirstName(cncDeliveryAddress.getFirstName());
            cncForm.setLastName(cncDeliveryAddress.getLastName());
            cncForm.setPhoneNumber(cncDeliveryAddress.getPhone());
        }
        else {
            addressDataHelper.populateClickAndCollectDetailsFormFromUser(cncForm);
        }

        if (isdeliveryToStore) {
            final CartData sessionCart = getTargetCartFacade().getSessionCart();
            final String postCode = getTargetCartFacade().getPostalCodeFromCartOrSession(sessionCart);
            model.addAttribute("enteredPostCode", postCode);
            final StoreFinderSearchPageData<PointOfServiceData> storeSearchResultData = getStoreSearchResultData(
                    postCode);
            model.addAttribute("storeList", storeSearchResultData);
        }
        model.addAttribute(cncForm);
        model.addAttribute("metaRobots", "no-index,no-follow");

        return ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_DELIVERY_ADDRESS_PAGE;
    }


    /**
     * 
     * @param clickAndCollectDetailsForm
     * @param bindingResult
     * @param model
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(method = RequestMethod.POST)
    public String doChooseDeliveryAddress(@Valid final ClickAndCollectDetailsForm clickAndCollectDetailsForm,
            final BindingResult bindingResult,
            final Model model,
            final RedirectAttributes redirectAttributes)
            throws CMSItemNotFoundException {
        final CartModel cartModel = getCheckoutFacade().getCart();
        TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart(cartModel);
        final String redirection = getRedirectionAndCheckSOH(model, redirectAttributes, DEFAULT_SOH_REDIRECT_URL,
                cartData);
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }
        if (cartData.isDataStale()) {
            cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        }

        // we may not have set it in the cart data yet, so need to get the delivery mode this way
        final DeliveryModeModel deliveryModeModel = getTargetDeliveryModeHelper()
                .getDeliveryModeModel(clickAndCollectDetailsForm.getDeliveryMode());
        if (deliveryModeModel == null) {
            GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
            addModelDataForSelectDeliveryAddress(cartData, model, "", clickAndCollectDetailsForm);
            return ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_DELIVERY_ADDRESS_PAGE;
        }

        if (bindingResult.hasErrors()) {
            GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
            addModelDataForSelectDeliveryAddress(cartData, model, clickAndCollectDetailsForm.getDeliveryMode(),
                    clickAndCollectDetailsForm);
            return ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_DELIVERY_ADDRESS_PAGE;
        }

        if (getCheckoutFacade().hasDeliveryOptionConflicts(cartModel, deliveryModeModel)) {
            GlobalMessages.addErrorMessage(model, "checkout.delivery.option.conflict.error");
            addModelDataForSelectDeliveryAddress(cartData, model, clickAndCollectDetailsForm.getDeliveryMode(),
                    clickAndCollectDetailsForm);
            return ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_DELIVERY_ADDRESS_PAGE;
        }

        final ClickAndCollectDeliveryDetailData cncData = new ClickAndCollectDeliveryDetailData();

        final String selectedStoreNumber = clickAndCollectDetailsForm.getSelectedStoreNumber();

        LOG.info(MessageFormat.format("CNCORDER-STORENUMBER : The store number {0} selected for the CNC cart {1} ",
                selectedStoreNumber,
                cartData.getCode()));

        cncData.setStoreNumber(Integer.valueOf(selectedStoreNumber));
        cncData.setTitle(clickAndCollectDetailsForm.getTitle());
        cncData.setFirstName(clickAndCollectDetailsForm.getFirstName());
        cncData.setLastName(clickAndCollectDetailsForm.getLastName());
        cncData.setTelephoneNumber(clickAndCollectDetailsForm.getPhoneNumber());
        cncData.setDeliveryModeModel(deliveryModeModel);


        if (getCheckoutFacade().setClickAndCollectDeliveryDetails(cncData)) {
            return getNextCheckoutStepRedirectUrl();
        }

        GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
        addModelDataForSelectDeliveryAddress(cartData, model, cartData.getDeliveryMode().getCode(), null);

        return ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_DELIVERY_ADDRESS_PAGE;
    }

    /**
     * This method gets called when the "Use this Address" button is clicked. It sets the selected delivery address on
     * the checkout facade - if it has changed, and reloads the page highlighting the selected delivery address.
     * 
     * @param selectedAddressCode
     *            - the id of the delivery address.
     * @return - a URL to the page to load.
     */
    @RequestMapping(value = ControllerConstants.YOUR_ADDRESS_SELECT, method = RequestMethod.GET)
    public String doSelectDeliveryAddress(final Model model, final RedirectAttributes redirectAttributes,
            @RequestParam("selectedAddressCode") final String selectedAddressCode,
            @RequestParam("deliveryModeCode") final String deliveryModeCode) throws CMSItemNotFoundException {
        CartModel cartModel = getCheckoutFacade().getCart();
        TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart(cartModel);
        final BigDecimal deliveryFee = cartData.getDeliveryCost() != null ? cartData.getDeliveryCost().getValue()
                : BigDecimal.valueOf(0);
        final String redirection = getRedirectionAndCheckSOH(model, redirectAttributes, DEFAULT_SOH_REDIRECT_URL,
                cartData);
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }

        if (cartData.isDataStale()) {
            cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        }
        boolean isAddressValid = false;

        boolean isDeliveryModePostCodeCombinationValid = true;
        AddressData selectedAddressData = null;

        if (StringUtils.isNotBlank(selectedAddressCode)) {
            selectedAddressData = getCheckoutFacade().getDeliveryAddressForCode(selectedAddressCode);
            if (selectedAddressData != null) {
                if (!PhoneValidationUtils.validatePhoneNumber(selectedAddressData.getPhone())) {
                    GlobalMessages.addErrorMessage(model, "address.error.phone.invalid");
                    addModelDataForAddEditDeliveryAddress(model, cartData, deliveryModeCode);
                    return editDeliveryAddress(selectedAddressCode, deliveryModeCode, model, redirectAttributes);
                }
                isAddressValid = true;
                final AddressData cartCheckoutDeliveryAddress = cartData.getDeliveryAddress();
                if (null == cartCheckoutDeliveryAddress
                        || isAddressIdChanged(cartCheckoutDeliveryAddress, selectedAddressData)) {
                    getCheckoutFacade().setDeliveryAddress(selectedAddressData);
                    cartModel = getCheckoutFacade().getCart();
                    cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
                }

                isDeliveryModePostCodeCombinationValid = getTargetDeliveryFacade()
                        .isDeliveryModePostCodeCombinationValid(deliveryModeCode,
                                selectedAddressData.getPostalCode());
            }
        }

        if (isAddressValid && StringUtils.isNotBlank(deliveryModeCode) && isDeliveryModePostCodeCombinationValid) {
            final TargetZoneDeliveryModeModel deliveryMode = getTargetDeliveryModeHelper()
                    .getDeliveryModeModel(deliveryModeCode);
            if (deliveryMode != null && BooleanUtils.isNotTrue(deliveryMode.getIsDeliveryToStore())) {
                getSessionService()
                        .setAttribute(TgtCoreConstants.SESSION_POSTCODE, selectedAddressData.getPostalCode());
                getCheckoutFacade().setDeliveryMode(deliveryModeCode);
                return getNextCheckoutRedirectUrl(redirectAttributes, deliveryFee);
            }
        }

        GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
        addModelDataForSelectDeliveryAddress(cartData, model, deliveryModeCode, null);
        return ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_DELIVERY_ADDRESS_PAGE;
    }

    @RequestMapping(value = ControllerConstants.YOUR_ADDRESS_CONTINUE, method = RequestMethod.GET)
    public String doContinueDeliveryAddress(final Model model, final RedirectAttributes redirectAttributes)
            throws CMSItemNotFoundException {
        final CartModel cartModel = getCheckoutFacade().getCart();
        TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart(cartModel);

        final String redirection = getRedirectionAndCheckSOH(model, redirectAttributes, DEFAULT_SOH_REDIRECT_URL,
                cartData);
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }

        if (cartData.isDataStale()) {
            cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        }
        if (hasNoDeliveryAddress(cartData)) {
            GlobalMessages.addFlashErrorMessage(redirectAttributes, "checkout.deliveryAddress.notSelected");
            return ControllerConstants.Redirection.CHECKOUT_YOUR_ADDRESS;
        }

        if (getCheckoutFacade().hasDeliveryOptionConflicts(cartModel, cartModel.getDeliveryMode())) {
            GlobalMessages.addFlashErrorMessage(redirectAttributes, "checkout.delivery.option.conflict.error");
            return ControllerConstants.Redirection.CHECKOUT_YOUR_ADDRESS;
        }

        return getNextCheckoutStepRedirectUrl();
    }

    /**
     * 
     * @param model
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.YOUR_ADDRESS_ADD, method = RequestMethod.GET)
    public String addDeliveryAddress(@RequestParam("deliveryModeCode") final String deliveryModeCode,
            final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {
        addressDataHelper.checkIfSingleLineAddressFeatureIsEnabled(model);
        TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        final String redirection = getRedirectionAndCheckSOH(model, redirectAttributes, DEFAULT_SOH_REDIRECT_URL,
                cartData);
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }

        if (cartData.isDataStale()) {
            cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        }
        addModelDataForAddEditDeliveryAddress(model, cartData, deliveryModeCode);
        //when ever we got error on qas model at that time we have addressForm in model
        if (!model.containsAttribute("addressForm")) {
            final AddressForm addressForm = new AddressForm();
            addressDataHelper.populateAddressFormFromUser(addressForm);
            model.addAttribute("addressForm", addressForm);
        }
        model.addAttribute("deliveryModeCode", deliveryModeCode);

        storeMetaRobotsNoIndexNoFollow(model);
        return ControllerConstants.Views.Pages.MultiStepCheckout.ADD_EDIT_DELIVERY_ADDRESS_PAGE;
    }

    /**
     * 
     * @param editAddressCode
     * @param model
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = ControllerConstants.YOUR_ADDRESS_EDIT, method = RequestMethod.GET)
    public String editDeliveryAddress(@RequestParam("editAddressCode") final String editAddressCode,
            @RequestParam("deliveryModeCode") final String deliveryModeCode, final Model model,
            final RedirectAttributes redirectAttributes)
            throws CMSItemNotFoundException {
        addressDataHelper.checkIfSingleLineAddressFeatureIsEnabled(model);
        TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        final String redirection = getRedirectionAndCheckSOH(model, redirectAttributes, DEFAULT_SOH_REDIRECT_URL,
                cartData);
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }

        if (cartData.isDataStale()) {
            cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
        }
        AddressForm addressForm = (AddressForm)model.asMap().get("addressForm");
        if (addressForm != null) {
            model.addAttribute("edit", Boolean.valueOf(StringUtils.isNotBlank(addressForm.getAddressId())));
        }
        else {
            TargetAddressData addressData = null;
            if (StringUtils.isNotEmpty(editAddressCode)) {
                addressData = (TargetAddressData)getCheckoutFacade().getDeliveryAddressForCode(editAddressCode);
            }

            addressForm = new AddressForm();

            final boolean hasAddressData = addressData != null;
            if (hasAddressData) {
                addressDataHelper.populateAddressForm(addressForm, addressData);
            }
            addressForm.setDeliveryModeCode(deliveryModeCode);
            model.addAttribute("edit", Boolean.valueOf(hasAddressData));
            model.addAttribute("addressForm", addressForm);
        }

        addModelDataForAddEditDeliveryAddress(model, cartData, deliveryModeCode);
        storeMetaRobotsNoIndexNoFollow(model);
        model.addAttribute("deliveryModeCode", deliveryModeCode);
        return ControllerConstants.Views.Pages.MultiStepCheckout.ADD_EDIT_DELIVERY_ADDRESS_PAGE;
    }

    /**
     * 
     * @param addressForm
     * @param bindingResult
     * @param model
     * @param redirectModel
     * @return String
     * @throws CMSItemNotFoundException
     */
    @RequestMapping(value = { ControllerConstants.YOUR_ADDRESS_ADD,
            ControllerConstants.YOUR_ADDRESS_EDIT }, method = RequestMethod.POST)
    public String confirmAddDeliveryAddress(@Valid final AddressForm addressForm, final BindingResult bindingResult,
            final Model model, final RedirectAttributes redirectModel, final HttpServletRequest request)
            throws CMSItemNotFoundException {

        model.addAttribute("hasAddress", Boolean.valueOf(CollectionUtils.isNotEmpty(getDeliveryAddresses())));

        addressFormValidator.validate(addressForm, bindingResult);
        addressDataHelper.checkIfSingleLineAddressFeatureIsEnabled(model);
        getSessionService().setAttribute(TgtCoreConstants.SESSION_POSTCODE, addressForm.getPostcode());
        if (bindingResult.hasErrors()) {
            GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
            final TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();
            addModelDataForAddEditDeliveryAddress(model, cartData, addressForm.getDeliveryModeCode());
            return ControllerConstants.Views.Pages.MultiStepCheckout.ADD_EDIT_DELIVERY_ADDRESS_PAGE;
        }

        final boolean isEdit = StringUtils.isNotBlank(addressForm.getAddressId());

        final TargetAddressData suppliedAddress = new TargetAddressData();
        if (isEdit) {
            suppliedAddress.setId(addressForm.getAddressId());
        }
        addressDataHelper.populateTargetAddressData(suppliedAddress, addressForm);
        if (getUserFacade().isAddressBookEmpty()) {
            suppliedAddress.setDefaultAddress(true);
        }
        else {
            suppliedAddress.setDefaultAddress(BooleanUtils.toBoolean(addressForm.getDefaultAddress()));
        }

        if (addressDataHelper.isSingleLineLookupFeatureEnabled()) {
            final ConfirmAddressForm confirmForm = new ConfirmAddressForm();
            confirmForm.setSuppliedAddress(addressForm);
            confirmForm.setUseSuppliedAddress(true);
            return confirmRedirect(confirmForm, model, redirectModel, request);
        }
        else {
            addressDataHelper.processAddressAndVerifyWithQas(model, addressForm, suppliedAddress, isEdit);
            model.addAttribute("isCheckout", Boolean.TRUE);
            return ControllerConstants.Views.Pages.MultiStepCheckout.CONFIRM_DELIVERY_ADDRESS_PAGE;
        }
    }

    private String failedAddressValidation(final Model model, final AddressForm suppliedAddress, final boolean isEdit,
            final String deliveryModeCode, final RedirectAttributes redirectAttributes)
            throws CMSItemNotFoundException {
        GlobalMessages.addErrorMessage(model, "address.error.verification.invalid");
        model.addAttribute("addressForm", suppliedAddress);
        if (isEdit) {
            return editDeliveryAddress(suppliedAddress.getAddressId(),
                    deliveryModeCode, model, redirectAttributes);
        }
        else {
            return addDeliveryAddress(deliveryModeCode, model, redirectAttributes);
        }
    }

    @RequestMapping(value = { ControllerConstants.YOUR_ADDRESS_ADD_CONFIRM,
            ControllerConstants.YOUR_ADDRESS_EDIT_CONFIRM }, method = RequestMethod.POST)
    public String confirmRedirect(@Valid final ConfirmAddressForm confirmAddressForm,
            final Model model, final RedirectAttributes redirectAttributes, final HttpServletRequest request)
            throws CMSItemNotFoundException {
        final AddressForm suppliedAddress = confirmAddressForm.getSuppliedAddress();
        final boolean isEdit = StringUtils.isNotBlank(suppliedAddress.getAddressId());
        boolean useSuppliedAddress = confirmAddressForm.isUseSuppliedAddress();

        final String deliveryModeCode = suppliedAddress.getDeliveryModeCode();

        final TargetZoneDeliveryModeModel deliveryMode = getTargetDeliveryModeHelper().getDeliveryModeModel(
                deliveryModeCode);

        if (deliveryMode == null || BooleanUtils.isTrue(deliveryMode.getIsDeliveryToStore())) {
            return failedAddressValidation(model, suppliedAddress, isEdit, deliveryModeCode, redirectAttributes);
        }

        if (!useSuppliedAddress && StringUtils.isBlank(confirmAddressForm.getConfirmedAddressCode())) {
            return failedAddressValidation(model, suppliedAddress, isEdit, deliveryModeCode, redirectAttributes);
        }

        final TargetAddressData newAddress = new TargetAddressData();
        if (isEdit) {
            newAddress.setId(suppliedAddress.getAddressId());
        }
        newAddress.setTitleCode(suppliedAddress.getTitleCode());
        newAddress.setFirstName(suppliedAddress.getFirstName());
        newAddress.setLastName(suppliedAddress.getLastName());
        newAddress.setPhone(suppliedAddress.getPhoneNumber());
        newAddress.setBillingAddress(false);
        newAddress.setShippingAddress(true);
        newAddress.setVisibleInAddressBook(true);
        if (getUserFacade().isAddressBookEmpty()) {
            newAddress.setDefaultAddress(true);
        }
        else {
            newAddress.setDefaultAddress(BooleanUtils.toBoolean(suppliedAddress
                    .getDefaultAddress()));
        }
        final CountryData countryData = new CountryData();
        countryData.setIsocode(suppliedAddress.getCountryIso());
        newAddress.setCountry(countryData);

        if (!useSuppliedAddress) {
            final au.com.target.tgtverifyaddr.data.AddressData qasAddressData = new au.com.target.tgtverifyaddr.data.AddressData(
                    confirmAddressForm.getConfirmedAddressCode(), null, null);
            try {
                final FormattedAddressData selectedAddress = getTargetAddressVerificationService().formatAddress(
                        qasAddressData);
                newAddress.setLine1(selectedAddress.getAddressLine1());
                newAddress.setLine2(selectedAddress.getAddressLine2());
                newAddress.setTown(selectedAddress.getCity());
                newAddress.setPostalCode(selectedAddress.getPostcode());
                newAddress.setState(selectedAddress.getState());
                newAddress.setAddressValidated(true);
            }
            catch (final ServiceNotAvailableException ex) {
                GlobalMessages.addFlashInfoMessage(redirectAttributes, "address.info.verification.unavailable");
                useSuppliedAddress = true;
            }
        }

        if (useSuppliedAddress) {
            newAddress.setLine1(suppliedAddress.getLine1());
            newAddress.setLine2(suppliedAddress.getLine2());
            newAddress.setTown(suppliedAddress.getTownCity());
            newAddress.setPostalCode(suppliedAddress.getPostcode());
            newAddress.setState(suppliedAddress.getState());
            newAddress.setAddressValidated(addressDataHelper.isAddressFormVerifiedInSession(suppliedAddress,
                    request.getSession()));
        }

        final TargetCartData cartData = (TargetCartData)getCheckoutFacade().getCheckoutCart();

        if (getTargetDeliveryFacade()
                .isDeliveryModePostCodeCombinationValid(deliveryModeCode,
                        newAddress.getPostalCode())) {
            if (isEdit) {
                getUserFacade().editAddress(newAddress);
            }
            else {
                getUserFacade().addAddress(newAddress);
            }

            getCheckoutFacade().setDeliveryAddress(newAddress);
            getCheckoutFacade().setDeliveryMode(deliveryModeCode);
            getSessionService().setAttribute(TgtCoreConstants.SESSION_POSTCODE, newAddress.getPostalCode());
        }
        else {
            GlobalMessages.addErrorMessage(model, "address.info.verification.deliverymodePostCodeCombination.invalid");
            model.addAttribute("addressForm", suppliedAddress);
            model.addAttribute("cancelTarget", ControllerConstants.YOUR_ADDRESS);
            model.addAttribute(
                    "activeDeliveryMode",
                    getTargetCartFacade().getDeliveryModeOptionInformation(deliveryModeCode));
            model.addAttribute("cartData", cartData);
            final ContentPageModel contentPageModel = getContentPageForLabelOrId(
                    ControllerConstants.CmsPageLabel.CHECKOUT_ADD_EDIT_ADDRESS_CMS_PAGE_LABEL);
            storeCmsPageInModel(model, contentPageModel);
            setUpMetaDataForContentPage(model, contentPageModel);
            return ControllerConstants.Views.Pages.MultiStepCheckout.ADD_EDIT_DELIVERY_ADDRESS_PAGE;
        }

        final BigDecimal oldDeliveryCost = cartData.getDeliveryCost() != null ? cartData.getDeliveryCost().getValue()
                : BigDecimal.valueOf(0d);
        return getNextCheckoutRedirectUrl(redirectAttributes, oldDeliveryCost);
    }

    /**
     * Method to update delivery mode selected by the user.
     * 
     * @param selectedDeliveryMode
     * @param model
     * @return String
     */
    @RequestMapping(value = ControllerConstants.UPDATE_DELIVERY_MODE, method = RequestMethod.POST)
    public String updateDeliveryMode(@RequestParam("selectedDeliveryMode") final String selectedDeliveryMode,
            final Model model) {
        getCheckoutFacade().updateDeliveryMode(selectedDeliveryMode);
        final CartData cartData = getCheckoutFacade().getCheckoutCart();
        model.addAttribute("cartData", cartData);
        return ControllerConstants.Views.Pages.MultiStepCheckout.UPDATE_DELIVERY_MODE_PAGE;
    }

    @RequestMapping(value = ControllerConstants.STORE_CNC_SEARCH_POSITION, method = RequestMethod.GET)
    public String storeSearchPosition(@RequestParam("latitude") final String lat,
            @RequestParam("longitude") final String lng, final Model model) {
        getCheckoutFacade().removeSelectedCncStore();
        if (lat != null && lng != null) {
            final GeoPoint geoPoint = new GeoPoint();
            try {
                geoPoint.setLatitude(Double.parseDouble(lat));
                geoPoint.setLongitude(Double.parseDouble(lng));
            }
            catch (final NumberFormatException e) {
                return ControllerConstants.Views.Pages.MultiStepCheckout.STORE_CNC_SEARCH_POSITION;
            }
            final StoreFinderSearchPageData<PointOfServiceData> storeSearchResultData = targetStoreLocatorFacadeImpl
                    .getCncStoresByPositionSearch(getCheckoutFacade().getCart(), geoPoint,
                            setPageableDataForCncStore());
            model.addAttribute("storeSearchPageData", storeSearchResultData);
            return ControllerConstants.Views.Pages.MultiStepCheckout.STORE_CNC_SEARCH_POSITION;
        }

        return ControllerConstants.Views.Pages.MultiStepCheckout.STORE_CNC_SEARCH_POSITION;
    }

    @RequestMapping(value = ControllerConstants.STORE_CNC_SEARCH_LOCATION, method = RequestMethod.GET)
    public String storeSearchLocation(@RequestParam("location") final String location, final Model model) {
        getCheckoutFacade().removeSelectedCncStore();
        if (location != null) {
            final StoreFinderSearchPageData<PointOfServiceData> storeSearchResultData = getStoreSearchResultData(
                    location);
            model.addAttribute("storeSearchPageData", storeSearchResultData);
            return ControllerConstants.Views.Pages.MultiStepCheckout.STORE_CNC_SEARCH_LOCATION;
        }
        return ControllerConstants.Views.Pages.MultiStepCheckout.STORE_CNC_SEARCH_LOCATION;
    }

    /**
     * 
     * @param model
     * @throws CMSItemNotFoundException
     */
    private void addModelDataForAddEditDeliveryAddress(final Model model, final TargetCartData cartData,
            final String selectedDeliveryType)
            throws CMSItemNotFoundException {
        model.addAttribute("cartData", cartData);
        model.addAttribute("hasAddress", Boolean.valueOf(CollectionUtils.isNotEmpty(getDeliveryAddresses())));

        String cancelTarget = ControllerConstants.YOUR_ADDRESS;
        final Map<String, DeliveryModeData> availableDeliveryModes = getTargetCartFacade()
                .getDeliveryModeOptionsInformation();
        if (availableDeliveryModes.size() == 1 && CollectionUtils.isEmpty(getDeliveryAddresses())) {
            cancelTarget = ControllerConstants.CART;
        }

        model.addAttribute("cancelTarget", cancelTarget);
        model.addAttribute("activeDeliveryMode",
                getTargetCartFacade().getDeliveryModeOptionInformation(selectedDeliveryType));

        final ContentPageModel addEditAddressPage = getContentPageForLabelOrId(
                ControllerConstants.CmsPageLabel.CHECKOUT_ADD_EDIT_ADDRESS_CMS_PAGE_LABEL);
        storeCmsPageInModel(
                model,
                addEditAddressPage);
        setUpMetaDataForContentPage(
                model,
                addEditAddressPage);
    }

    /**
     * 
     * @param cartData
     * @param model
     * @param selectedDeliveryType
     * @throws CMSItemNotFoundException
     */
    protected void addModelDataForSelectDeliveryAddress(final TargetCartData cartData, final Model model,
            final String selectedDeliveryType, final ClickAndCollectDetailsForm clickAndCollectDetailsForm)
            throws CMSItemNotFoundException {
        model.addAttribute("cartData", cartData);

        //populate digital delivery Mode
        final List<CheckoutDeliveryTypeData> checkoutDigitalDeliveryModes = new ArrayList<>();
        final List<TargetZoneDeliveryModeData> availableDigitalDeliveryModes = cartData.getDigitalDeliveryModes();
        final Map<String, DeliveryModeData> deliveryModeDataValidForCart = getTargetCartFacade()
                .getDeliveryModeOptionsInformation();

        if (CollectionUtils.isNotEmpty(availableDigitalDeliveryModes)) {
            for (final TargetZoneDeliveryModeData zoneDeliveryModeData : availableDigitalDeliveryModes) {
                if (deliveryModeDataValidForCart.containsKey(zoneDeliveryModeData.getCode())) {
                    final CheckoutDeliveryTypeData checkoutDeliveryTypeData = createDeliveryData(
                            deliveryModeDataValidForCart.get(zoneDeliveryModeData.getCode()), selectedDeliveryType,
                            cartData, clickAndCollectDetailsForm);
                    checkoutDeliveryTypeData.setFee(zoneDeliveryModeData.getDeliveryCost());
                    setupCheckoutDeliveryData(checkoutDigitalDeliveryModes, zoneDeliveryModeData,
                            checkoutDeliveryTypeData);
                }
            }
        }

        // populate the non-digital delivery modes
        final List<CheckoutDeliveryTypeData> checkoutDeliveryModes = new ArrayList<>();
        final List<TargetZoneDeliveryModeData> availableDeliveryModes = cartData.getDeliveryModes();

        if (CollectionUtils.isNotEmpty(availableDeliveryModes)) {
            if (targetFeatureSwitchFacade.isFluentEnabled()) {
                getCheckoutFacade().overlayFluentStockDataOnDeliveryModes(availableDeliveryModes);
            }
            if (targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FASLINE_FALCON)) {
                getCheckoutFacade().checkFastlineStockOnExpressDeliveryModes(availableDeliveryModes);
            }
            for (final TargetZoneDeliveryModeData zoneDeliveryModeData : availableDeliveryModes) {
                final CheckoutDeliveryTypeData checkoutDeliveryTypeData;
                if (deliveryModeDataValidForCart.containsKey(zoneDeliveryModeData.getCode())) {
                    checkoutDeliveryTypeData = createDeliveryData(
                            deliveryModeDataValidForCart.get(zoneDeliveryModeData.getCode()), selectedDeliveryType,
                            cartData, clickAndCollectDetailsForm);
                }
                else {
                    checkoutDeliveryTypeData = new CheckoutDeliveryTypeData();
                    checkoutDeliveryTypeData.setDeliveryModeCode(zoneDeliveryModeData.getCode());
                    checkoutDeliveryTypeData.setMessage(zoneDeliveryModeData.getLongDescription());
                }

                //setting delivery fee
                if (zoneDeliveryModeData.isAvailable() && checkoutDeliveryTypeData.getFee() == null) {
                    checkoutDeliveryTypeData.setFee(zoneDeliveryModeData.getDeliveryCost());
                }

                setupCheckoutDeliveryData(checkoutDeliveryModes, zoneDeliveryModeData, checkoutDeliveryTypeData);
            }
        }

        Collections.sort(checkoutDeliveryModes, new Comparator<CheckoutDeliveryTypeData>() {
            @Override
            public int compare(final CheckoutDeliveryTypeData first, final CheckoutDeliveryTypeData second) {
                return first.getDisplayOrder() - second.getDisplayOrder();
            }

        });


        model.addAttribute("availableDeliveryModes", checkoutDeliveryModes);

        model.addAttribute("availableDigitalDeliveryModes", checkoutDigitalDeliveryModes);

        storeCmsPageInModel(
                model,
                getContentPageForLabelOrId(
                        ControllerConstants.CmsPageLabel.CHECKOUT_CHOOSE_DELIVERY_ADDRESS_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(
                model,
                getContentPageForLabelOrId(
                        ControllerConstants.CmsPageLabel.CHECKOUT_CHOOSE_DELIVERY_ADDRESS_CMS_PAGE_LABEL));
    }

    /**
     * @param checkoutDeliveryModes
     * @param zoneDeliveryModeData
     * @param checkoutDeliveryTypeData
     */
    protected void setupCheckoutDeliveryData(final List<CheckoutDeliveryTypeData> checkoutDeliveryModes,
            final TargetZoneDeliveryModeData zoneDeliveryModeData,
            final CheckoutDeliveryTypeData checkoutDeliveryTypeData) {
        final String postCode = ObjectUtils.toString(getSessionService()
                .getAttribute(TgtCoreConstants.SESSION_POSTCODE));
        if (StringUtils.isNotEmpty(postCode)
                && !getTargetDeliveryFacade().isDeliveryModePostCodeCombinationValid(zoneDeliveryModeData.getCode(),
                        postCode)
                && !getTargetUserFacade().isCurrentUserAnonymous()
                && zoneDeliveryModeData.isPostCodeNotSupported()) {
            checkoutDeliveryTypeData.setAvailable(true);
            checkoutDeliveryTypeData.setPostCodeNotSupported(false);
        }
        else {
            checkoutDeliveryTypeData.setAvailable(zoneDeliveryModeData.isAvailable());
            checkoutDeliveryTypeData.setPostCodeNotSupported(zoneDeliveryModeData.isPostCodeNotSupported());
        }
        checkoutDeliveryTypeData.setDisplayOrder(zoneDeliveryModeData.getDisplayOrder());
        checkoutDeliveryModes.add(checkoutDeliveryTypeData);
    }

    /**
     * 
     * @param deliveryMode
     * @param selectedDeliveryType
     * @param cartData
     * @return CheckoutDeliveryTypeData
     */
    private CheckoutDeliveryTypeData createDeliveryData(final DeliveryModeData deliveryMode,
            final String selectedDeliveryType, final TargetCartData cartData,
            final ClickAndCollectDetailsForm clickAndCollectDetailsForm) {
        if (deliveryMode == null) {
            return null;
        }

        final CheckoutDeliveryTypeData checkoutDeliveryTypeData = createCheckoutDeliveryTypeData(deliveryMode,
                selectedDeliveryType);
        if (deliveryMode.isDeliverToStores()) {
            checkoutDeliveryTypeData.setDeliverToStores(true);
            Integer selectedStoreNumber = null;
            if (clickAndCollectDetailsForm != null
                    && StringUtils.isNotEmpty(clickAndCollectDetailsForm.getSelectedStoreNumber())) {
                selectedStoreNumber = Integer.valueOf(clickAndCollectDetailsForm.getSelectedStoreNumber());
            }
            else {
                selectedStoreNumber = cartData.getCncStoreNumber();
            }
            if (selectedStoreNumber != null) {
                final TargetPointOfServiceData selectedStore = getTargetStoreLocatorFacade().getPointOfService(
                        selectedStoreNumber);
                if (selectedStore != null) {
                    checkoutDeliveryTypeData.setSelectedStore(selectedStore);
                }
            }
        }
        else {
            final List<TargetAddressData> deliveryAddresses = (List<TargetAddressData>)getSupportedDeliveryAddresses(
                    deliveryMode
                            .getDeliveryModeCode());
            checkoutDeliveryTypeData.setDeliveryAddresses(deliveryAddresses);

            if (cartData.getDeliveryAddress() != null && cartData.getDeliveryAddress().getId() != null) {
                checkoutDeliveryTypeData.setSelectedDeliveryAddressId(cartData.getDeliveryAddress().getId());
            }
        }

        return checkoutDeliveryTypeData;
    }

    private List<? extends AddressData> getSupportedDeliveryAddresses(final String deliveryModeCode) {
        final List<AddressData> deliveryAddresses = getCheckoutFacade().getSupportedDeliveryAddresses(
                deliveryModeCode, true);
        return deliveryAddresses == null ? Collections.<TargetAddressData> emptyList()
                : orderAddresses(deliveryAddresses);
    }

    /**
     * 
     * @param cartCheckoutDeliveryAddress
     * @param selectedAddressData
     * @return boolean
     */
    protected boolean isAddressIdChanged(final AddressData cartCheckoutDeliveryAddress,
            final AddressData selectedAddressData) {
        return (cartCheckoutDeliveryAddress != null && !selectedAddressData.getId().equals(
                cartCheckoutDeliveryAddress.getId()));
    }

    /**
     * 
     * @param deliveryMode
     * @param selectedDeliveryType
     * @return CheckoutDeliveryTypeData
     */
    private CheckoutDeliveryTypeData createCheckoutDeliveryTypeData(final DeliveryModeData deliveryMode,
            final String selectedDeliveryType) {
        final CheckoutDeliveryTypeData checkoutDeliveryTypeData = new CheckoutDeliveryTypeData();

        checkoutDeliveryTypeData.setDeliveryModeCode(deliveryMode.getDeliveryModeCode());
        checkoutDeliveryTypeData.setFee(deliveryMode.getDeliveryFee());
        checkoutDeliveryTypeData.setMessage(deliveryMode.getMessage());
        checkoutDeliveryTypeData.setDisclaimer(deliveryMode.getDisclaimer());
        checkoutDeliveryTypeData.setAvailable(deliveryMode.isAvailable());
        checkoutDeliveryTypeData.setDisplayOrder(deliveryMode.getDisplayOrder());
        if (checkoutDeliveryTypeData.getDeliveryModeCode().equals(selectedDeliveryType)) {
            checkoutDeliveryTypeData.setSelected(true);
        }


        return checkoutDeliveryTypeData;
    }

    private String getNextCheckoutStepRedirectUrl() {
        return ControllerConstants.Redirection.CHECKOUT_PAYMENT;
    }

    /**
     * @param redirectAttributes
     * @param deliveryFee
     * @return String
     */
    private String getNextCheckoutRedirectUrl(final RedirectAttributes redirectAttributes,
            final BigDecimal deliveryFee) {
        if (getCheckoutFacade().isDeliveryFeeVariesAfterAddressChange(deliveryFee)) {
            final CartData sessionCart = getTargetCartFacade().getSessionCart();
            GlobalMessages.addFlashInfoMessage(redirectAttributes, FEECHANGE_NOTIFICATION,
                    new Object[] { getTargetCartFacade()
                            .getCatchmentAreaByPostcode(
                                    getTargetCartFacade().getPostalCodeFromCartOrSession(sessionCart)) });
            return ControllerConstants.Redirection.CHECKOUT_YOUR_ADDRESS;
        }
        else {
            return getNextCheckoutStepRedirectUrl();
        }
    }

    /**
     * 
     * @return List<? extends AddressData>
     */
    public List<? extends AddressData> getDeliveryAddresses() {
        final List<? extends AddressData> deliveryAddresses = getCheckoutFacade().getSupportedDeliveryAddresses(true);
        return deliveryAddresses == null ? Collections.<TargetAddressData> emptyList()
                : orderAddresses(deliveryAddresses);
    }

    /**
     * Order a list of addresses, default address will be first
     * 
     * @param deliveryAddresses
     */
    private List<? extends AddressData> orderAddresses(final List<? extends AddressData> deliveryAddresses) {
        Collections.sort(deliveryAddresses, new Comparator<AddressData>() {
            @Override
            public int compare(final AddressData arg0, final AddressData arg1) {
                if (!arg0.isDefaultAddress() && arg1.isDefaultAddress()) {
                    return 1;
                }
                else if (arg0.isDefaultAddress() && !arg1.isDefaultAddress()) {
                    return -1;
                }
                return 0;
            }
        });
        return deliveryAddresses;
    }

    /**
     * @param location
     * @return StoreFinderSearchPageData
     */
    protected StoreFinderSearchPageData<PointOfServiceData> getStoreSearchResultData(final String location) {
        final StoreFinderSearchPageData<PointOfServiceData> storeSearchResultData = targetStoreLocatorFacadeImpl
                .getCncStoresByPostCode(location, getCheckoutFacade().getCart(), setPageableDataForCncStore());
        return storeSearchResultData;
    }
}

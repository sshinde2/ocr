/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.support.RequestContextUtils;

import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtstorefront.annotations.RequireHardLogIn;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * @author rsparks1
 *
 */
@Controller
@RequireHardLogIn
@RequestMapping(value = ControllerConstants.SPC_IPG_IFRAME_RETURN)
public class SpcIpgReturnController extends AbstractCheckoutController {


    @RequestMapping(method = { RequestMethod.GET, RequestMethod.POST })
    public String fromIpg(
            @RequestParam(value = ControllerConstants.IPG_PARAM_TOKEN) final String ipgToken,
            @RequestParam(value = ControllerConstants.IPG_PARAM_QS, required = false) final String qsParam,
            final Model model,
            final HttpServletRequest request) {
        request.getSession().setAttribute(ipgToken, ipgToken);

        if (ControllerConstants.Ipg.IPG_ABORT_ACTION.equals(qsParam)) {
            model.addAttribute("postMessageActionType", ControllerConstants.Ipg.IPG_PAYMENT_TYPE_ABORT);
        }
        else {
            final TargetQueryTransactionDetailsResult result = getTargetPlaceOrderFacade()
                    .getQueryTransactionDetailResults();
            if (null != result && result.isCancel()) {
                model.addAttribute("postMessageActionType", ControllerConstants.Ipg.IPG_PAYMENT_TYPE_CANCEL);
            }
            else {
                model.addAttribute("postMessageActionType", ControllerConstants.Ipg.IPG_PAYMENT_TYPE_COMPLETE);
            }
        }

        return ControllerConstants.Views.Pages.Checkout.CHECKOUT_SPC_IPG_RETURN_PAGE;
    }

    @ExceptionHandler(RuntimeException.class)
    public String handleException(final Exception exception, final HttpServletRequest request) {
        LOG.error("Runtime exception when trying to communicate with IPG", exception);
        final Map<String, Object> currentFlashMap = RequestContextUtils.getOutputFlashMap(request);
        currentFlashMap.put("postMessageActionType", ControllerConstants.Ipg.IPG_PAYMENT_TYPE_COMPLETE);
        return ControllerConstants.Views.Pages.Checkout.CHECKOUT_SPC_IPG_RETURN_PAGE;
    }
}

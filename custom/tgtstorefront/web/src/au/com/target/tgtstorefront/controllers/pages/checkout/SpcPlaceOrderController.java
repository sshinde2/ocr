/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.order.InvalidCartException;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.order.TargetCartFacade;
import au.com.target.tgtfacades.order.TargetPlaceOrderFacade;
import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;
import au.com.target.tgtfacades.order.data.TargetPlaceOrderRequest;
import au.com.target.tgtfacades.order.data.TargetPlaceOrderResult;
import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderAfterpayPaymentInfoData;
import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderIPGPaymentInfoData;
import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderPayPalPaymentInfoData;
import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderZipPaymentInfoData;
import au.com.target.tgtfacades.order.enums.TargetPlaceOrderResultEnum;
import au.com.target.tgtfacades.response.data.Error;
import au.com.target.tgtfacades.response.data.PlaceOrderResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtstorefront.annotations.RequireHardLogIn;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.GlobalMessages;
import au.com.target.tgtstorefront.controllers.util.TargetPaymentHelper;
import au.com.target.tgtstorefront.servlet.support.TargetFlashMapManager;
import au.com.target.tgtstorefront.util.TargetSessionTokenUtil;
import au.com.target.tgtwebcore.enums.AfterpayReturnStatusEnum;


@Controller
@RequireHardLogIn
public class SpcPlaceOrderController extends AbstractCheckoutController {

    private static final String VENDOR_ERROR_CODE_PREFIX = "ERR_PLACEORDER_";

    private static final String VENDOR_ERROR_CODE_UNKNOWN = "UNKNOWN";

    private static final String AFTERPAY_PREFIX = "AFTERPAY_";

    private static final String ZIPPAYMENT_PREFIX = "ZIPPAYMENT_";

    public static final String ENCODING_FORMAT_IS_NOT_SUPPORTED = "Encoding format is not supported";

    @Autowired
    private TargetFlashMapManager flashMapManager;

    @Autowired
    private TargetPlaceOrderFacade placeOrderFacade;

    @Autowired
    private TargetPaymentHelper targetPaymentHelper;

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Resource(name = "targetCartFacade")
    private TargetCartFacade targetCartFacade;

    @RequestMapping(value = "/ws-api/v1/{baseSiteId}/checkout/place-order"
            + ControllerConstants.PLACE_ORDER_IPG, method = RequestMethod.POST)
    @ResponseBody
    public Response placeIpgOrder(
            @RequestParam(value = ControllerConstants.IPG_PARAM_TOKEN, required = false) final String ipgToken,
            @RequestParam(value = "sessionId", required = false) final String sessionId,
            final HttpServletRequest request,
            final HttpServletResponse httpResponse)
            throws CMSItemNotFoundException, InvalidCartException {
        try {
            Response response = new Response(false);
            Error error = null;
            final PlaceOrderResponseData data = new PlaceOrderResponseData();
            boolean orderPlaced = false;
            if (!TargetSessionTokenUtil.isIpgTokenValid(request)) {
                error = new Error("ERR_PLACEORDER_IN_PROGRESS");
            }
            else {
                request.getSession().setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS,
                        Boolean.TRUE);

                final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();
                ipgPaymentInfo.setIpgToken(ipgToken);
                ipgPaymentInfo.setSessionId(sessionId);

                final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
                placeOrderRequest.setPaymentInfo(ipgPaymentInfo);
                placeOrderRequest.setIpAddress(getRequestHeadersHelper().getRemoteIPAddress(request));
                placeOrderRequest.setCookieString(getCookieStringBuilder().buildCookieString(request.getCookies()));
                final TargetPlaceOrderResult placeOrderResult = placeOrderFacade.placeOrder(placeOrderRequest);
                if (placeOrderResult != null) {
                    final FlashMap flashMap = RequestContextUtils.getOutputFlashMap(request);
                    final AdjustedCartEntriesData adjustedCartEntriesData = placeOrderResult
                            .getAdjustedCartEntriesData();
                    if (adjustedCartEntriesData != null && adjustedCartEntriesData.isAdjusted()) {
                        flashMap.put(ControllerConstants.SOH_UPDATES, Collections.singleton(adjustedCartEntriesData));
                        data.setSohUpdates(adjustedCartEntriesData);
                    }
                    final TargetPlaceOrderResultEnum placeOrderResultEnum = placeOrderResult.getPlaceOrderResult();
                    final String orderCode = placeOrderResult.getCartCode();
                    error = new Error("ERR_PLACEORDER_" + placeOrderResultEnum.name());
                    switch (placeOrderResultEnum) {
                        case SUCCESS:
                            response = new Response(true);
                            error = null;
                            handlePlaceOrderSuccessSessionAttributes(request, orderCode, adjustedCartEntriesData);
                            if (targetFeatureSwitchFacade
                                    .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU)) {
                                data.setRedirectUrl(ControllerConstants.SINGLE_PAGE_CHECKOUT_AND_LOGIN_THANKYOU
                                        .concat(orderCode));
                            }
                            else {
                                data.setRedirectUrl(ControllerConstants.THANK_YOU.concat(orderCode));
                            }

                            orderPlaced = true;
                            break;
                        case INVALID_CART:
                            data.setRedirectUrl(ControllerConstants.CART);
                            break;
                        case INVALID_PRODUCT_IN_CART:
                            GlobalMessages.addInfoMessage(flashMap, "checkout.information.missing.products.removed");
                            data.setRedirectUrl(ControllerConstants.CART);
                            break;
                        case UNKNOWN:
                            if (targetFeatureSwitchFacade
                                    .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU)) {
                                data.setRedirectUrl(ControllerConstants.SINGLE_PAGE_CHECKOUT_AND_LOGIN_THANKYOUCHECK
                                        .concat(StringUtils.isEmpty(orderCode) ? "" : orderCode));
                            }
                            else {
                                data.setRedirectUrl(ControllerConstants.THANK_YOU + ControllerConstants.THANK_YOU_CHECK
                                        + "?cart=" + (StringUtils.isEmpty(orderCode) ? "" : orderCode));
                            }

                            break;
                        default:
                            break;
                    }
                    flashMapManager.saveFlashMapToOutput(flashMap, request, httpResponse);
                }
            }

            if (!orderPlaced && error == null) {
                error = new Error("ERR_PLACEORDER");
            }
            data.setError(error);
            response.setData(data);
            return response;
        }
        finally {
            request.getSession().removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);
        }
    }

    @RequestMapping(value = ControllerConstants.PAYMENT + ControllerConstants.PAYMENT_PAYPAL_RETURN
            + ControllerConstants.SINGLE_PAGE_CHECKOUT, method = {
            RequestMethod.GET, RequestMethod.POST })
    public String fromPaypal(
            @RequestParam(ControllerConstants.REQUEST_PARAM_PAYPAL_TOKEN) final String paypalSessionToken,
            @RequestParam(ControllerConstants.REQUEST_PARAM_PAYPAL_PAYER_ID) final String paypalPayerId,
            final HttpServletRequest request, final RedirectAttributes redirectAttributes) {
        try {
            request.getSession().setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS,
                    Boolean.TRUE);
            String redirectURL = ControllerConstants.Redirection.CART;

            final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfo = new TargetPlaceOrderPayPalPaymentInfoData();
            payPalPaymentInfo.setPaypalPayerId(paypalPayerId);
            payPalPaymentInfo.setPaypalSessionToken(paypalSessionToken);

            final TargetPlaceOrderRequest placeOrderRequest = createBasicPlaceOrderRequest(request);
            placeOrderRequest.setPaymentInfo(payPalPaymentInfo);
            placeOrderRequest.setIpAddress(getRequestHeadersHelper().getRemoteIPAddress(request));
            placeOrderRequest.setCookieString(getCookieStringBuilder().buildCookieString(request.getCookies()));

            final TargetPlaceOrderResult placeOrderResult = placeOrderFacade.placeOrder(placeOrderRequest);
            if (placeOrderResult != null) {
                LOG.debug("Place order result = " + placeOrderResult.getPlaceOrderResult().toString());
                final AdjustedCartEntriesData adjustedCartEntriesData = placeOrderResult.getAdjustedCartEntriesData();
                addAdjustedCartToRedirectAttributes(adjustedCartEntriesData, redirectAttributes);

                final String orderCode = placeOrderResult.getCartCode();
                final TargetPlaceOrderResultEnum placeOrderResultEnum = placeOrderResult.getPlaceOrderResult();
                switch (placeOrderResultEnum) {
                    case SUCCESS:
                        handlePlaceOrderSuccessSessionAttributes(request, orderCode, adjustedCartEntriesData);
                        redirectURL = getPlaceOrderSuccessRedirectUrl(orderCode);
                        break;
                    case INVALID_CART:
                        redirectURL = ControllerConstants.Redirection.CART;
                        break;
                    case INVALID_PRODUCT_IN_CART:
                        GlobalMessages.addFlashInfoMessage(redirectAttributes,
                                "checkout.information.missing.products.removed");
                        redirectURL = ControllerConstants.Redirection.CART;
                        break;
                    case UNKNOWN:
                        if (targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU)) {
                            redirectURL = ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN_THANKYOUCHECK
                                    + (StringUtils.isEmpty(orderCode) ? "" : orderCode);
                        }
                        else {
                            redirectURL = (ControllerConstants.Redirection.CHECKOUT_THANK_YOU
                                    + ControllerConstants.THANK_YOU_CHECK + "?cart="
                                    + (StringUtils.isEmpty(orderCode) ? "" : orderCode));
                        }

                        break;
                    case FUNDING_FAILURE:
                        redirectURL = UrlBasedViewResolver.REDIRECT_URL_PREFIX.concat(
                                targetPaymentHelper.getPayPalHostedPaymentFormBaseUrl())
                                .concat(paypalSessionToken);
                        break;
                    default:
                    {
                        try {
                            redirectURL = this.getVendorRedirectUrl(placeOrderResult);
                        }
                        catch (final UnsupportedEncodingException e) {
                            LOG.error(
                                    "SpcPlaceOrderController.getPlaceOrderError : The encoding UTF-8 is not supported.");
                        }
                    }
                }
            }

            return redirectURL;
        }
        finally {
            request.getSession().removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);
        }
    }

    /**
     *
     * @param paymentResult
     * @param checkoutToken
     * @param additionalParams
     * @param request
     * @param redirectAttributes
     * @return redirectUrl
     */

    @RequestMapping(value = ControllerConstants.PAYMENT + ControllerConstants.PAYMENT_ZIPPAY_REDIRECT
            + ControllerConstants.SINGLE_PAGE_CHECKOUT,
            method = RequestMethod.GET)
    public String fromZipPay(@RequestParam(ControllerConstants.REQUEST_PARAM_ZIPPAY_RESULT) final String paymentResult,
            @RequestParam(ControllerConstants.REQUEST_PARAM_ZIPPAY_CHECKOUT_TOKEN) final String checkoutToken,
            @RequestParam final Map<String, String> additionalParams,
            final HttpServletRequest request, final RedirectAttributes redirectAttributes) {

        String redirectUrl = ControllerConstants.Redirection.CART;

        try {
            TargetSessionTokenUtil.checkZipPayRequest(request);
        }
        catch (final IllegalArgumentException e) {
            LOG.error("Trying to attempt invalid request", e);
            return redirectUrl;
        }
        catch (final InvalidCartException e) {
            LOG.error("Request and Session Cart does not match", e);
            return getRedirectionUrl4MisMatch(redirectUrl, e);
        }

        if (!StringUtils.equalsIgnoreCase(ControllerConstants.REQUEST_PARAM_ZIPPAY_STATUS_APPROVED, paymentResult)) {
            if (StringUtils.equalsIgnoreCase(ControllerConstants.REQUEST_PARAM_ZIPPAY_STATUS_CANCELLED, paymentResult)) {
                clearPaymentRequest(request);
                return ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN;
            }
            try {
                redirectUrl = getVendorRedirectUrl("ZIP_" + StringUtils.upperCase(paymentResult));
                if (StringUtils.equalsIgnoreCase(ControllerConstants.REQUEST_PARAM_ZIPPAY_STATUS_DECLINED,
                        paymentResult)) {
                    clearPaymentRequest(request);
                }
                return redirectUrl;
            }
            catch (final UnsupportedEncodingException e) {
                LOG.error(ENCODING_FORMAT_IS_NOT_SUPPORTED, e);
                return redirectUrl;
            }
        }

        //# process the cart for approved payment request
        request.getSession().setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS,
                Boolean.TRUE);
        final TargetPlaceOrderZipPaymentInfoData paymentInfoData = new TargetPlaceOrderZipPaymentInfoData();
        paymentInfoData.setCheckoutId(checkoutToken);
        paymentInfoData.setResult(paymentResult);

        final TargetPlaceOrderRequest placeOrderRequest = createBasicPlaceOrderRequest(request);
        placeOrderRequest.setPaymentInfo(paymentInfoData);

        try {
            final TargetPlaceOrderResult placeOrderResult = placeOrderFacade.placeOrder(placeOrderRequest);

            final AdjustedCartEntriesData adjustedCartEntriesData = placeOrderResult.getAdjustedCartEntriesData();
            addAdjustedCartToRedirectAttributes(adjustedCartEntriesData, redirectAttributes);
            redirectUrl = handlePlaceOrderResult(ZIPPAYMENT_PREFIX, placeOrderResult, request, redirectAttributes);

        }
        catch (final Exception e) {
            LOG.error("Error while placing order", e);
            return redirectUrl;
        }
        finally {
            request.getSession().removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);
        }

        return redirectUrl;
    }

    private String getRedirectionUrl4MisMatch(final String defaultUrl, final InvalidCartException e) {
        if (targetCartFacade.hasEntries()) {
            try {
                return getVendorRedirectUrl(TargetPlaceOrderResultEnum.ITEM_MISMATCH_IN_CART_AFTER_PAYMENT.toString());
            }
            catch (final UnsupportedEncodingException e1) {
                LOG.error(ENCODING_FORMAT_IS_NOT_SUPPORTED, e);
                return defaultUrl;
            }
        }
        return defaultUrl;
    }

    private String handlePlaceOrderResult(final String paymentPartner, final TargetPlaceOrderResult placeOrderResult,
            final HttpServletRequest request, final RedirectAttributes redirectAttributes) {

        String redirectUrl = ControllerConstants.Redirection.CART;
        final String orderCode = placeOrderResult.getCartCode();
        final AdjustedCartEntriesData adjustedCartEntriesData = placeOrderResult.getAdjustedCartEntriesData();
        final TargetPlaceOrderResultEnum placeOrderResultEnum = placeOrderResult.getPlaceOrderResult();

        switch (placeOrderResultEnum) {
            case SUCCESS:
                handlePlaceOrderSuccessSessionAttributes(request, orderCode, adjustedCartEntriesData);
                redirectUrl = getPlaceOrderSuccessRedirectUrl(orderCode);
                break;
            case INVALID_CART:
                redirectUrl = ControllerConstants.Redirection.CART;
                break;
            case INVALID_PRODUCT_IN_CART:
                GlobalMessages.addFlashInfoMessage(redirectAttributes,
                        "checkout.information.missing.products.removed");
                redirectUrl = ControllerConstants.Redirection.CART;
                break;
            case UNKNOWN:
                if (targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU)) {
                    redirectUrl = ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN_THANKYOUCHECK
                            + (StringUtils.isEmpty(orderCode) ? "" : orderCode);
                }
                else {
                    redirectUrl = (ControllerConstants.Redirection.CHECKOUT_THANK_YOU
                            + ControllerConstants.THANK_YOU_CHECK + "?cart="
                            + (StringUtils.isEmpty(orderCode) ? "" : orderCode));
                }
                break;
            default:
                try {
                    redirectUrl = mapPaymentPartnerErrorRedirectUrl(paymentPartner, placeOrderResult);
                    if (StringUtils.isEmpty(redirectUrl)) {
                        redirectUrl = this.getVendorRedirectUrl(placeOrderResult);
                    }
                }
                catch (final UnsupportedEncodingException e) {
                    LOG.error(
                            "SpcPlaceOrderController.getPlaceOrderError : The encoding UTF-8 is not supported.");
                }
        }
        return redirectUrl;
    }


    /**
     * clears payment request token
     * 
     * @param request
     */
    private void clearPaymentRequest(final HttpServletRequest request) {
        request.getSession().removeAttribute(ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN);
    }

    @RequestMapping(value = ControllerConstants.PAYMENT + ControllerConstants.PAYMENT_AFTERPAY_RETURN
            + ControllerConstants.SINGLE_PAGE_CHECKOUT, method = {
            RequestMethod.GET, RequestMethod.POST })
    public String fromAfterpay(
            @RequestParam(ControllerConstants.REQUEST_PARAM_AFTERPAY_ORDER_TOKEN) final String afterpayOrderToken,
            @RequestParam(ControllerConstants.REQUEST_PARAM_AFTERPAY_STATUS) final String afterpayStatus,
            final HttpServletRequest request, final RedirectAttributes redirectAttributes) {

        String redirectUrl = ControllerConstants.Redirection.CART;

        if (!AfterpayReturnStatusEnum.SUCCESS.getCode().equals(afterpayStatus)) {
            try {
                return getAfterpayReturnRedirectUrl(afterpayStatus);
            }
            catch (final UnsupportedEncodingException e) {
                LOG.error(
                        "SpcPlaceOrderController.getPlaceOrderError : The encoding UTF-8 is not supported.");
                return redirectUrl;
            }
        }

        try {
            request.getSession().setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS,
                    Boolean.TRUE);

            final TargetPlaceOrderAfterpayPaymentInfoData paymentInfoData = new TargetPlaceOrderAfterpayPaymentInfoData();
            paymentInfoData.setAfterpayOrderToken(afterpayOrderToken);

            final TargetPlaceOrderRequest placeOrderRequest = createBasicPlaceOrderRequest(request);
            placeOrderRequest.setPaymentInfo(paymentInfoData);

            final TargetPlaceOrderResult placeOrderResult = placeOrderFacade.placeOrder(placeOrderRequest);

            if (placeOrderResult != null) {
                LOG.debug("Place order result = " + placeOrderResult.getPlaceOrderResult().toString());

                final AdjustedCartEntriesData adjustedCartEntriesData = placeOrderResult.getAdjustedCartEntriesData();
                addAdjustedCartToRedirectAttributes(adjustedCartEntriesData, redirectAttributes);

                final TargetPlaceOrderResultEnum placeOrderResultEnum = placeOrderResult.getPlaceOrderResult();
                LOG.info("placeOrderResult=" + placeOrderResultEnum);
                redirectUrl = handlePlaceOrderResult(AFTERPAY_PREFIX, placeOrderResult, request, redirectAttributes);
            }

            return redirectUrl;
        }
        finally {
            request.getSession().removeAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS);
        }
    }

    @RequestMapping(value = ControllerConstants.PAYMENT + ControllerConstants.PAYMENT_AFTERPAY_CANCEL
            + ControllerConstants.SINGLE_PAGE_CHECKOUT, method = { RequestMethod.GET, RequestMethod.POST })
    public String cancelAfterpay(
            @RequestParam("status") final String status,
            final HttpServletRequest request) {
        LOG.info("cancelAfterpay status:" + status);
        request.getSession().removeAttribute(ControllerConstants.SESSION_PARAM_AFTERPAY_TOKEN);
        return ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN;
    }

    private String mapPaymentPartnerErrorRedirectUrl(final String paymentPartnerPrefix,
            final TargetPlaceOrderResult placeOrderResult)
            throws UnsupportedEncodingException {
        String redirectUrl = StringUtils.EMPTY;
        final TargetPlaceOrderResultEnum placeOrderResultEnum = placeOrderResult.getPlaceOrderResult();
        if (TargetPlaceOrderResultEnum.SERVICE_UNAVAILABLE.equals(placeOrderResultEnum)
                || TargetPlaceOrderResultEnum.PAYMENT_FAILURE_DUETO_MISSMATCH_TOTALS.equals(placeOrderResultEnum)
                || TargetPlaceOrderResultEnum.ITEM_MISMATCH_IN_CART_AFTER_PAYMENT.equals(placeOrderResultEnum)
                || TargetPlaceOrderResultEnum.PAYMENT_FAILURE.equals(placeOrderResultEnum)) {
            redirectUrl = getVendorRedirectUrl(paymentPartnerPrefix + placeOrderResultEnum.name());
        }
        return redirectUrl;
    }

    private String getVendorRedirectUrl(final TargetPlaceOrderResult placeOrderResult)
            throws UnsupportedEncodingException {
        return getVendorRedirectUrl(placeOrderResult.getPlaceOrderResult().name());
    }

    private String getAfterpayReturnRedirectUrl(final String afterpayStatus) throws UnsupportedEncodingException {
        final AfterpayReturnStatusEnum errorCodePostfixEnum = EnumUtils.getEnum(AfterpayReturnStatusEnum.class,
                afterpayStatus);

        return getVendorRedirectUrl(errorCodePostfixEnum != null ? errorCodePostfixEnum.getCode()
                : VENDOR_ERROR_CODE_UNKNOWN);
    }

    private String getVendorRedirectUrl(final String errorCodePostfix) throws UnsupportedEncodingException {
        return ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN_PLACE_ORDER_ERROR + DatatypeConverter
                .printBase64Binary((VENDOR_ERROR_CODE_PREFIX + errorCodePostfix).getBytes(StandardCharsets.UTF_8));
    }

    private TargetPlaceOrderRequest createBasicPlaceOrderRequest(final HttpServletRequest request) {
        final TargetPlaceOrderRequest placeOrderRequest = new TargetPlaceOrderRequest();
        placeOrderRequest.setIpAddress(getRequestHeadersHelper().getRemoteIPAddress(request));
        placeOrderRequest.setCookieString(getCookieStringBuilder().buildCookieString(request.getCookies()));

        return placeOrderRequest;
    }

    private void handlePlaceOrderSuccessSessionAttributes(final HttpServletRequest request, final String orderCode,
            final AdjustedCartEntriesData adjustedCartEntriesData) {
        request.getSession().setAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE,
                orderCode);
        clearPaymentRequest(request);
        if (targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU)
                && adjustedCartEntriesData != null && adjustedCartEntriesData.isAdjusted()) {
            request.getSession().setAttribute(ControllerConstants.SOH_UPDATES,
                    adjustedCartEntriesData);
        }
    }

    private String getPlaceOrderSuccessRedirectUrl(final String orderCode) {
        if (!targetFeatureSwitchFacade.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.SPC_THANKYOU)) {
            return ControllerConstants.Redirection.CHECKOUT_THANK_YOU
                    .concat(StringUtils.isEmpty(orderCode) ? "" : orderCode);
        }

        return ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN_THANKYOU
                .concat(StringUtils.isEmpty(orderCode) ? "" : orderCode);
    }

    private void addAdjustedCartToRedirectAttributes(final AdjustedCartEntriesData adjustedCartEntriesData,
            final RedirectAttributes redirectAttributes) {
        if (adjustedCartEntriesData != null && adjustedCartEntriesData.isAdjusted()) {
            redirectAttributes.addFlashAttribute(ControllerConstants.SOH_UPDATES,
                    Collections.singleton(adjustedCartEntriesData));
        }
    }

}
/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.data;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.AddressData;

import java.util.List;

import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;


/**
 * @author rmcalave
 * 
 */
public class CheckoutDeliveryTypeData {
    private String deliveryModeCode;

    private PriceData fee;

    private String selectedDeliveryAddressId;

    private TargetPointOfServiceData selectedStore;

    private List<? extends AddressData> deliveryAddresses;

    private boolean selected;

    private String message;

    private String disclaimer;

    private boolean available;

    private int displayOrder;

    private boolean deliverToStores;

    private boolean postCodeNotSupported;

    /**
     * @return the deliveryModeCode
     */
    public String getDeliveryModeCode() {
        return deliveryModeCode;
    }

    /**
     * @param deliveryModeCode
     *            the deliveryModeCode to set
     */
    public void setDeliveryModeCode(final String deliveryModeCode) {
        this.deliveryModeCode = deliveryModeCode;
    }

    /**
     * @return the fee
     */
    public PriceData getFee() {
        return fee;
    }

    /**
     * @param fee
     *            the fee to set
     */
    public void setFee(final PriceData fee) {
        this.fee = fee;
    }

    /**
     * @return the selectedDeliveryAddressId
     */
    public String getSelectedDeliveryAddressId() {
        return selectedDeliveryAddressId;
    }

    /**
     * @param selectedDeliveryAddressId
     *            the selectedDeliveryAddressId to set
     */
    public void setSelectedDeliveryAddressId(final String selectedDeliveryAddressId) {
        this.selectedDeliveryAddressId = selectedDeliveryAddressId;
    }


    /**
     * @return the selectedStore
     */
    public TargetPointOfServiceData getSelectedStore() {
        return selectedStore;
    }

    /**
     * @param selectedStore
     *            the selectedStore to set
     */
    public void setSelectedStore(final TargetPointOfServiceData selectedStore) {
        this.selectedStore = selectedStore;
    }

    /**
     * @return the deliveryAddresses
     */
    public List<? extends AddressData> getDeliveryAddresses() {
        return deliveryAddresses;
    }

    /**
     * @param deliveryAddresses
     *            the deliveryAddresses to set
     */
    public void setDeliveryAddresses(final List<? extends AddressData> deliveryAddresses) {
        this.deliveryAddresses = deliveryAddresses;
    }

    /**
     * @return the selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * @param selected
     *            the selected to set
     */
    public void setSelected(final boolean selected) {
        this.selected = selected;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(final String message) {
        this.message = message;
    }

    /**
     * @return the disclaimer
     */
    public String getDisclaimer() {
        return disclaimer;
    }

    /**
     * @param disclaimer
     *            the disclaimer to set
     */
    public void setDisclaimer(final String disclaimer) {
        this.disclaimer = disclaimer;
    }

    /**
     * @return the available
     */
    public boolean isAvailable() {
        return available;
    }

    /**
     * @param available
     *            the available to set
     */
    public void setAvailable(final boolean available) {
        this.available = available;
    }

    /**
     * @return the displayOrder
     */
    public int getDisplayOrder() {
        return displayOrder;
    }

    /**
     * @param displayOrder
     *            the displayOrder to set
     */
    public void setDisplayOrder(final int displayOrder) {
        this.displayOrder = displayOrder;
    }

    /**
     * @return the deliverToStores
     */
    public boolean isDeliverToStores() {
        return deliverToStores;
    }

    /**
     * @param deliverToStores
     *            the deliverToStores to set
     */
    public void setDeliverToStores(final boolean deliverToStores) {
        this.deliverToStores = deliverToStores;
    }

    /**
     * @return the postCodeNotSupported
     */
    public boolean isPostCodeNotSupported() {
        return postCodeNotSupported;
    }

    /**
     * @param postCodeNotSupported
     *            the postCodeNotSupported to set
     */
    public void setPostCodeNotSupported(final boolean postCodeNotSupported) {
        this.postCodeNotSupported = postCodeNotSupported;
    }


}

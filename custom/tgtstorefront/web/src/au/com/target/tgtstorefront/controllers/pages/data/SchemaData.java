/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.data;

import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.commercefacades.product.data.PriceData;

import java.util.Date;

import au.com.target.tgtcore.product.data.ProductDisplayType;


/**
 * @author mjanarth
 *
 */
public class SchemaData {

    private String name;
    private String productCode;
    private String primaryImage;
    private ProductDisplayType productDisplayType;
    private String categoryName;
    private PriceRangeData priceRange;
    private PriceData price;
    private Date normalSaleDate;
    private Integer maxAvailQtyOnline;
    private Integer maxAvailQtyInStore;

    /**
     * @return the productDisplayType
     */
    public ProductDisplayType getProductDisplayType() {
        return productDisplayType;
    }

    /**
     * @param productDisplayType
     *            the productDisplayType to set
     */
    public void setProductDisplayType(final ProductDisplayType productDisplayType) {
        this.productDisplayType = productDisplayType;
    }

    /**
     * @return the priceRange
     */
    public PriceRangeData getPriceRange() {
        return priceRange;
    }

    /**
     * @param priceRange
     *            the priceRange to set
     */
    public void setPriceRange(final PriceRangeData priceRange) {
        this.priceRange = priceRange;
    }

    /**
     * @return the price
     */
    public PriceData getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(final PriceData price) {
        this.price = price;
    }

    /**
     * @return the normalSaleDate
     */
    public Date getNormalSaleDate() {
        return normalSaleDate;
    }

    /**
     * @param normalSaleDate
     *            the normalSaleDate to set
     */
    public void setNormalSaleDate(final Date normalSaleDate) {
        this.normalSaleDate = normalSaleDate;
    }

    /**
     * @return the maxAvailQtyOnline
     */
    public Integer getMaxAvailQtyOnline() {
        return maxAvailQtyOnline;
    }

    /**
     * @param maxAvailQtyOnline
     *            the maxAvailQtyOnline to set
     */
    public void setMaxAvailQtyOnline(final Integer maxAvailQtyOnline) {
        this.maxAvailQtyOnline = maxAvailQtyOnline;
    }

    /**
     * @return the maxAvailQtyInStore
     */
    public Integer getMaxAvailQtyInStore() {
        return maxAvailQtyInStore;
    }

    /**
     * @param maxAvailQtyInStore
     *            the maxAvailQtyInStore to set
     */
    public void setMaxAvailQtyInStore(final Integer maxAvailQtyInStore) {
        this.maxAvailQtyInStore = maxAvailQtyInStore;
    }

    /**
     * @return the categoryName
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * @param categoryName
     *            the categoryName to set
     */
    public void setCategoryName(final String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the primaryImage
     */
    public String getPrimaryImage() {
        return primaryImage;
    }

    /**
     * @param primaryImage
     *            the primaryImage to set
     */
    public void setPrimaryImage(final String primaryImage) {
        this.primaryImage = primaryImage;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }


}

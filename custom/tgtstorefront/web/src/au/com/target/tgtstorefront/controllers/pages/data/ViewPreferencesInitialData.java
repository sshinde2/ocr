/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.data;

import javax.servlet.http.HttpServletRequest;


/**
 * @author quan.le
 * 
 */
public class ViewPreferencesInitialData {
    private String rootCategoryCode;
    private String queryString;
    private HttpServletRequest request;

    public ViewPreferencesInitialData(final HttpServletRequest request, final String rootCategoryCode,
            final String queryString) {
        setRootCategoryCode(rootCategoryCode);
        setQueryString(queryString);
        setRequest(request);
    }

    /**
     * @return the rootCategoryCode
     */
    public String getRootCategoryCode() {
        return rootCategoryCode;
    }

    /**
     * @param rootCategoryCode
     *            the rootCategoryCode to set
     */
    public void setRootCategoryCode(final String rootCategoryCode) {
        this.rootCategoryCode = rootCategoryCode;
    }

    /**
     * @return the queryString
     */
    public String getQueryString() {
        return queryString;
    }

    /**
     * @param queryString
     *            the queryString to set
     */
    public void setQueryString(final String queryString) {
        this.queryString = queryString;
    }

    /**
     * @return the request
     */
    public HttpServletRequest getRequest() {
        return request;
    }

    /**
     * @param request
     *            the request to set
     */
    public void setRequest(final HttpServletRequest request) {
        this.request = request;
    }
}

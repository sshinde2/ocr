/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.preferences;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Required;

//import au.com.target.tgtsearch.service.TargetFacetSearchConfigService;
//import au.com.target.tgtsearch.service.impl.TargetCommerceFacetSearchConfigServiceImpl;
import au.com.target.tgtstorefront.controllers.pages.AbstractSearchPageController.PageSize;
import au.com.target.tgtstorefront.controllers.pages.AbstractSearchPageController.ViewAsType;
import au.com.target.tgtstorefront.controllers.pages.data.ViewPreferencesInitialData;


/**
 * @author quan.le
 * 
 */
public class StoreViewPreferencesHandler {
    private static final String VIEW_AS = "pref_v";
    private static final String SORT_CODE = "pref_s";
    private static final String ITEM_PER_PAGE = "pref_pp";

    private String viewAsDefaultValue = StringUtils.EMPTY;
    private final String sortCodeDefaultValue = StringUtils.EMPTY;
    private String itemPerPageDefaultValue = StringUtils.EMPTY;
    private String viewPreferencesMaxAgeDefaultValue = StringUtils.EMPTY;

    /**
     * Generate cookie based on the name and value,These cookies are always secure
     * 
     * @param name
     * @param value
     * @return Cookie
     */
    protected Cookie generateCookie(final String name, final String value) {
        final Cookie cookie = new Cookie(name, value);
        cookie.setPath("/");
        cookie.setSecure(true);
        cookie.setMaxAge(getDefaultValue(viewPreferencesMaxAgeDefaultValue));
        return cookie;
    }

    private String getCookieValueByName(final ViewPreferencesInitialData viewData, final String name) {
        final Cookie[] cookies = viewData.getRequest().getCookies();

        if (cookies != null) {
            for (final Cookie cookie : cookies) {
                if (cookie.getName().equalsIgnoreCase(name)) {
                    return cookie.getValue();
                }
            }
        }

        return StringUtils.EMPTY;
    }

    /**
     * Get viewAs value from controller and return necessary viewAs value for category page or search page
     * 
     * @param response
     * @param viewAsValueFromController
     * @return viewAs from cookie
     */
    public String getViewAsPreference(final HttpServletResponse response, final ViewPreferencesInitialData viewData,
            final String viewAsValueFromController) {
        final String viewAsCookieName = VIEW_AS;
        final String viewAsStr = getCookieValueByName(viewData, viewAsCookieName);

        if (StringUtils.isBlank(viewAsStr)) { // cookie without value
            if (!StringUtils.equals(viewAsDefaultValue, viewAsValueFromController)
                    && ViewAsType.isValidValue(viewAsValueFromController)) { // change viewAs with input different than default value, update cookie value
                response.addCookie(generateCookie(viewAsCookieName, viewAsValueFromController));
                return viewAsValueFromController;
            }

            return viewAsDefaultValue; // if not, return default
        }
        else { // cookie with value
            if (!StringUtils.equals(viewAsStr, viewAsValueFromController)
                    && ViewAsType.isValidValue(viewAsValueFromController)) { // if param different than cookie value, update cookie value

                response.addCookie(generateCookie(viewAsCookieName, viewAsValueFromController));
                return viewAsValueFromController;
            }

            return viewAsStr; // if not, return cookie value
        }
    }

    /**
     * Get sortCode value from controller and return necessary viewAs value for category page or search page
     * 
     * @param response
     * @param sortCodeValueFromController
     * @return sortCode from cookie
     */
    public String getSortCodePreference(final HttpServletResponse response, final ViewPreferencesInitialData viewData,
            final String sortCodeValueFromController) {

        //Query String based user preference
        if (StringUtils.isNotBlank(sortCodeValueFromController)) {
            response.addCookie(generateCookie(SORT_CODE, sortCodeValueFromController));//session based user preference
            return sortCodeValueFromController;
        }
        //Session based user preference
        else if (StringUtils.isNotBlank(getCookieValueByName(viewData, SORT_CODE))) {
            return getCookieValueByName(viewData, SORT_CODE);
        }

        return sortCodeDefaultValue; // if not, return default
    }

    /**
     * Get itemPerPage value from controller and return necessary viewAs value for category page or search page
     * 
     * @param response
     * @param itemsPerPageValueFromController
     * @return itemsPerPage value from cookie
     */
    public int getItemPerPagePreference(final HttpServletResponse response, final ViewPreferencesInitialData viewData,
            final int itemsPerPageValueFromController) {
        final String itemsPerPageCookieName = ITEM_PER_PAGE;
        final String itemsPerPageStr = getCookieValueByName(viewData, itemsPerPageCookieName);

        if (StringUtils.isBlank(itemsPerPageStr)) { // cookie without value
            final int defaultValue = getDefaultValue(itemPerPageDefaultValue);
            if (defaultValue != itemsPerPageValueFromController
                    && PageSize.isValidValue(itemsPerPageValueFromController)) { // input different than default value, update cookie value
                response.addCookie(generateCookie(itemsPerPageCookieName,
                        String.valueOf(itemsPerPageValueFromController)));
                return itemsPerPageValueFromController;
            }

            return defaultValue; // if not, return default
        }
        else { // cookie with value
            final int itemsPerPage = NumberUtils.toInt(itemsPerPageStr);

            if (itemsPerPage != itemsPerPageValueFromController
                    && PageSize.isValidValue(itemsPerPageValueFromController)) { // if param different than cookie value, update cookie value

                response.addCookie(generateCookie(itemsPerPageCookieName,
                        String.valueOf(itemsPerPageValueFromController)));
                return itemsPerPageValueFromController;
            }

            return itemsPerPage; // if not, return cookie value
        }
    }

    /**
     * @return the viewAsDefaultValue
     */
    public String getViewAsDefaultValue() {
        return viewAsDefaultValue;
    }

    private int getDefaultValue(final String value) {
        return StringUtils.isNotEmpty(value) && StringUtils.isNumeric(value) ? Integer
                .parseInt(value)
                : 0;
    }

    /**
     * @param viewAsDefaultValue
     *            the viewAsDefaultValue to set
     */
    @Required
    public void setViewAsDefaultValue(final String viewAsDefaultValue) {
        this.viewAsDefaultValue = viewAsDefaultValue;
    }


    /**
     * @param itemPerPageDefaultValue
     *            the itemPerPageDefaultValue to set
     */
    @Required
    public void setItemPerPageDefaultValue(final String itemPerPageDefaultValue) {
        this.itemPerPageDefaultValue = itemPerPageDefaultValue;
    }

    /**
     * @param viewPreferencesMaxAgeDefaultValue
     *            the viewPreferencesMaxAgeDefaultValue to set
     */
    @Required
    public void setViewPreferencesMaxAgeDefaultValue(final String viewPreferencesMaxAgeDefaultValue) {
        this.viewPreferencesMaxAgeDefaultValue = viewPreferencesMaxAgeDefaultValue;
    }
}

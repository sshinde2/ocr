package au.com.target.tgtstorefront.controllers.personalisation;

import java.io.UnsupportedEncodingException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.tgtfacades.personalisation.TargetCustomerSegmentedContentFacade;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.AbstractPageController;
import au.com.target.tgtwebcore.enums.CachedPageType;
import au.com.target.tgtwebcore.model.cms2.pages.TargetFragmentPageModel;


@Controller
@RequestMapping(ControllerConstants.CUSTOMER_SEGMENTED)
public class CustomerSegmentedContentController extends AbstractPageController {


    private static final String CATEGORY_CODE_PATH_VARIABLE_PATTERN = "/{componentId}/{segId}";
    private static final String PERSONALISED = "personalised";

    @Resource(name = "targetCustomerSegmentedContentFacade")
    private TargetCustomerSegmentedContentFacade targetCustomerSegmentedContentFacade;

    @RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    public String getCustomerSegmentDynamicPageForComponent(@PathVariable("componentId") final String componentId,
            @PathVariable("segId") final String segmentId,
            final Model model, final HttpServletResponse response) throws UnsupportedEncodingException {

        final TargetFragmentPageModel targetFragmentPageModel = targetCustomerSegmentedContentFacade
                .getSegmentedContentFromPersonalisedComponent(componentId, segmentId);
        storeCmsPageInModel(model, targetFragmentPageModel);
        model.addAttribute(PERSONALISED, "-Personalised");
        addAkamaiCacheAttributes(CachedPageType.TARGETFRAGMENTPAGE, response, model);
        return ControllerConstants.Views.Pages.Fragment.FRAGMENT_PAGE;

    }

}
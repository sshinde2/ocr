/**
 * 
 */
package au.com.target.tgtstorefront.controllers.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.order.TargetCheckoutResponseFacade;
import au.com.target.tgtfacades.response.data.Response;


/**
 * @author bhuang3 This is a controller for ws-api calls before hard login
 *
 */
@Controller
@RequestMapping(value = "/ws-api/v1/{baseSiteId}/cart")
public class CartServiceController {

    @Autowired
    private TargetCheckoutResponseFacade targetCheckoutResponseFacade;

    @RequestMapping(value = "/delivery/applicable-modes", method = RequestMethod.GET)
    @ResponseBody
    public Response getApplicableModes() {
        return targetCheckoutResponseFacade.getApplicableDeliveryModes();
    }

    @RequestMapping(value = "/summary", method = RequestMethod.GET)
    @ResponseBody
    public Response getCartSummary() {
        return targetCheckoutResponseFacade.getCartSummary();
    }

    @RequestMapping(value = "/delivery/shipster-status", method = RequestMethod.GET)
    @ResponseBody
    public Response shipsterStatus() {
        return targetCheckoutResponseFacade.verifyShipsterStatus();
    }
}

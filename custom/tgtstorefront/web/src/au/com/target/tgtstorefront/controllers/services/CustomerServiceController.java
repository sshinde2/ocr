package au.com.target.tgtstorefront.controllers.services;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.response.data.Error;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.response.data.StoreResponseData;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtstorefront.controllers.util.WebServiceExceptionHelper;
import au.com.target.tgtstorefront.security.cookie.EnhancedCookieGenerator;


@Controller
@RequestMapping(value = "/ws-api/v1/{baseSiteId}/customer")
public class CustomerServiceController {

    @Resource
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Resource
    private WebServiceExceptionHelper webServiceExceptionHelper;

    @Resource(name = "preferredStoreCookieGenerator")
    private EnhancedCookieGenerator preferredStoreCookieGenerator;

    @ResponseBody
    @RequestMapping(value = "/preferredStore/{storeNumber:.*}", method = RequestMethod.POST)
    public Response setPreferredStore(@PathVariable("storeNumber") final Integer storeNumber,
            final HttpServletResponse httpServletResponse) {
        final TargetPointOfServiceData storeData = targetStoreLocatorFacade.getPointOfService(storeNumber);

        if (storeData == null) {
            final StoreResponseData responseData = new StoreResponseData();
            responseData.setStoreNumber(storeNumber);
            responseData.setError(new Error("INVALID_STORE_NUMBER"));

            final Response response = new Response(false);
            response.setData(responseData);

            return response;
        }

        final StoreResponseData responseData = new StoreResponseData();
        responseData.setStoreNumber(storeData.getStoreNumber());
        responseData.setStoreDetails(storeData);

        final Response response = new Response(true);
        response.setData(responseData);

        preferredStoreCookieGenerator.addCookie(httpServletResponse, storeNumber.toString());

        return response;
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Response handleException(final HttpServletRequest request,
            final Exception exception) {
        return webServiceExceptionHelper.handleException(request, exception);
    }

}

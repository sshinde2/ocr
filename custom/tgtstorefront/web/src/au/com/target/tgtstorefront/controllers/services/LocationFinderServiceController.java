/**
 * 
 */
package au.com.target.tgtstorefront.controllers.services;

import java.text.MessageFormat;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.CookieGenerator;

import au.com.target.tgtfacades.google.location.finder.TargetGoogleLocationFinderFacade;
import au.com.target.tgtfacades.google.location.finder.data.GoogleResponseData;
import au.com.target.tgtfacades.google.location.finder.data.Result;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.Error;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtstorefront.util.SearchQuerySanitiser;


/**
 * @author bbaral1
 *
 */
@Controller
@RequestMapping(value = "/ws-api/v1/{baseSiteId}/search")
public class LocationFinderServiceController {

    private static final Logger LOG = Logger.getLogger(LocationFinderServiceController.class);

    private static final String ERROR_LOCATION_SERVICES_COOKIE = "Unable to store cookie for the location={0}";

    private static final String INVALID_INPUT_PARAMETER = "INVALID_INPUT_PARAMETER";

    private static final String ERROR_MESSAGE = "Please enter valid input parameter(s).";

    @Resource(name = "searchQuerySanitiser")
    private SearchQuerySanitiser searchQuerySanitiser;

    @Resource(name = "targetGoogleLocationFinderFacade")
    private TargetGoogleLocationFinderFacade targetGoogleLocationFinderFacade;

    @Resource(name = "storeStockPostCodeCookieGenerator")
    private CookieGenerator storeStockPostCodeCookieGenerator;

    @ResponseBody
    @RequestMapping(value = "suburb", method = RequestMethod.GET)
    public Response getDeliveryLocationDetails(@RequestParam(required = false) final String location,
            @RequestParam(required = false) final Double latitude,
            @RequestParam(required = false) final Double longitude,
            final HttpServletResponse httpServletResponse) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getDeliveryLocationDetails: Location Or PostCode=" + location);
        }
        final String sanitisedLocation = StringUtils.isNotEmpty(location)
                ? searchQuerySanitiser.sanitiseSearchText(location) : null;
        if (isInvalidInputParameters(sanitisedLocation, latitude, longitude)) {
            return createErrorResponse(INVALID_INPUT_PARAMETER);
        }
        final Response response = targetGoogleLocationFinderFacade
                .searchUserDeliveryLocation(sanitisedLocation, latitude, longitude);
        addCookie(httpServletResponse, response, location);
        return response;
    }

    /**
     * This method will add cookie for the selected location.
     * 
     * @param httpServletResponse
     * @param response
     * @param location
     */
    private void addCookie(final HttpServletResponse httpServletResponse,
            final Response response, final String location) {
        if (null != response && response.isSuccess()) {
            final GoogleResponseData googleResponseData = (GoogleResponseData)response.getData();
            try {
                storeStockPostCodeCookieGenerator.addCookie(httpServletResponse,
                        URIUtil.encodePath(selectedLocation(googleResponseData.getResults().get(0)), "utf-8"));
            }
            catch (final URIException uriException) {
                LOG.error(MessageFormat.format(ERROR_LOCATION_SERVICES_COOKIE, location));
            }
        }
    }

    /**
     * @param result
     * @return String
     */
    private String selectedLocation(final Result result) {
        final String locationName = StringUtils.isEmpty(result.getSuburb()) ? StringUtils.EMPTY
                : result.getSuburb();
        final String postCode = StringUtils.isEmpty(result.getPostalCode()) ? StringUtils.EMPTY
                : result.getPostalCode();
        if (StringUtils.isEmpty(locationName) && StringUtils.isEmpty(postCode)) {
            return StringUtils.EMPTY;
        }
        return new StringBuilder().append(locationName).append(" ").append(postCode).toString().trim();
    }

    /**
     * Check for empty inputs.
     *
     * @param location
     * @param latitude
     * @param longitude
     * @return boolean
     */
    private boolean isInvalidInputParameters(final String location, final Double latitude, final Double longitude) {
        return !(StringUtils.isNotEmpty(location) || (longitude != null && latitude != null));
    }


    /**
     * This method will throw error message when invalid input paramters given before making geocoding api call.
     * 
     * @param errCode
     * @return Response
     */
    private Response createErrorResponse(final String errCode) {
        final Response response = new Response(false);
        final Error error = new Error(errCode);
        error.setMessage(ERROR_MESSAGE);
        final BaseResponseData responseData = new BaseResponseData();
        responseData.setError(error);
        response.setData(responseData);
        return response;
    }
}
/**
 * 
 */
package au.com.target.tgtstorefront.controllers.services;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * This controller is used to post a message to the parent window from an iframe. We're using this as the survey end
 * page for the thank you page survey so that we can close it when the user is done with the survey.
 * 
 * @author mgazal
 *
 */
@Controller
@RequestMapping("/checkout/survey-end")
public class SpcSurveyEndController {

    private static final String SURVEY_END = "SURVEY_END";

    @RequestMapping(method = RequestMethod.GET)
    public String surveyEnd(final Model model) {
        model.addAttribute("postMessageActionType", SURVEY_END);
        return ControllerConstants.Views.Pages.Checkout.CHECKOUT_SPC_IPG_RETURN_PAGE;
    }

}

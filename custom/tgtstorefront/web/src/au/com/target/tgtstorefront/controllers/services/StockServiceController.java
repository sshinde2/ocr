/**
 * 
 */
package au.com.target.tgtstorefront.controllers.services;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.stock.TargetStockLookUpFacade;
import au.com.target.tgtwebcore.fluent.StockRequestDto;


/**
 * @author mgazal
 *
 */
@Controller
@RequestMapping(value = "/ws-api/v1/{baseSiteId}/stock")
public class StockServiceController {

    @Autowired
    private TargetStockLookUpFacade targetStockLookUpFacade;

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST)
    public Response lookupStock(final StockRequestDto request) {

        if (StringUtils.isNotBlank(request.getBaseProductCode())) {
            return targetStockLookUpFacade.lookupStock(request.getBaseProductCode(), request.getDeliveryTypes(),
                    request.getLocations());
        }

        return targetStockLookUpFacade.lookupStock(request.getVariants(),
                request.getDeliveryTypes(), request.getLocations());
    }
}

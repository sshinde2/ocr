/**
 * 
 */
package au.com.target.tgtstorefront.controllers.services;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtfacades.config.CheckoutConfigurationFacade;
import au.com.target.tgtfacades.order.data.SessionData;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.Error;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.response.data.SessionResponseData;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.util.TargetSessionTokenUtil;


/**
 * This is a sample controller for ws-api calls
 *
 */
@Controller
@RequestMapping(value = "/ws-api/v1/{baseSiteId}")
public class WSController {

    @Resource
    private CheckoutConfigurationFacade checkoutConfigurationFacade;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Response getResponse() {
        final Response response = new Response(true);
        final BaseResponseData data = new BaseResponseData();
        final Error error = new Error("WSAPI-ERR-01");
        error.setMessage("service is not found");
        data.setError(error);
        response.setData(data);
        return response;
    }

    @RequestMapping(value = "/session", method = RequestMethod.GET)
    @ResponseBody
    public Response getSessionInformation(final HttpServletRequest request) {
        final Response response = new Response(true);
        final SessionResponseData sessionResponseData = new SessionResponseData();
        final String orderCode = (String)request.getSession()
                .getAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE);
        final SessionData sessionInfo = checkoutConfigurationFacade.getSessionInformation(orderCode);

        if (null != sessionInfo) {
            sessionInfo.setCsrfToken(TargetSessionTokenUtil.getCsrfToken(request));
            sessionResponseData.setSession(sessionInfo);
        }
        response.setData(sessionResponseData);
        return response;
    }

    @RequestMapping(value = "/env", method = RequestMethod.GET)
    @ResponseBody
    public Response getEnv() {
        final Response response = checkoutConfigurationFacade.getCheckoutEnvConfigs();
        return response;
    }

}

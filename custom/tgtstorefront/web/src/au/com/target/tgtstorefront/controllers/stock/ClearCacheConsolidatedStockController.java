/**
 * 
 */
package au.com.target.tgtstorefront.controllers.stock;





import de.hybris.platform.servicelayer.event.EventService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtcore.cache.TargetUpdateStockCacheEvent;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * @author fkhan4
 *
 */
@Controller
public class ClearCacheConsolidatedStockController {

    @Autowired
    private EventService eventService;

    @RequestMapping(value = ControllerConstants.CLEAR_CACHE, method = RequestMethod.GET)
    @ResponseBody
    public Response clearCache() {
        eventService.publishEvent(new TargetUpdateStockCacheEvent());
        return new Response(true);
    }
}

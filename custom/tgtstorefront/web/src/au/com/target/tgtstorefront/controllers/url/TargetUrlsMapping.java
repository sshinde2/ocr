/**
 * 
 */
package au.com.target.tgtstorefront.controllers.url;

/**
 * @author Benoit Vanalderweireldt
 * 
 */
public interface TargetUrlsMapping {
    /**
     * 
     * @return url to home page
     */
    String getHome();

    /**
     * 
     * @return url to login page
     */
    String getLogin();

    /**
     * 
     * @return url to logout
     */
    String getLogout();

    /**
     * 
     * @return url to checkout page
     */
    String getCheckout();

    /**
     * 
     * @return url to checkout login page
     */
    String getCheckoutLogin();

    /**
     * 
     * @return url to search
     */
    String getSearch();

    /**
     * 
     * @return url to my account orderS page
     */
    String getMyAccountOrders();

    /**
     * 
     * @return url to my account order make payment page
     */
    String getMyAccountOrderMakePayment();

    /**
     * 
     * @return url to my account order make payment page modal
     */
    String getMyAccountOrderMakePaymentModal();

    /**
     * 
     * @return url to my account order details page
     */
    String getMyAccountOrderDetails();

    /**
     * 
     * @return url to my account personal details page
     */
    String getMyAccountPersonalDetails();

    /**
     * 
     * @return url to update personal details page
     */
    String getMyAccountUpdatePersonalDetails();

    /**
     * 
     * @return url to update my password page
     */
    String getMyAccountUpdatePassword();

    /**
     * 
     * @return url to update my email address page
     */
    String getMyAccountUpdateEmail();

    /**
     * 
     * @return url to my payments details
     */
    String getMyAccountPayment();

    /**
     * 
     * @return url to my addresses details
     */
    String getMyAccountAddressDetails();

    /**
     * 
     * @return My account address add
     */
    String getMyAccountAddressAdd();

    /**
     * 
     * @return url to my account address edit
     */
    String getMyAccountAddressEdit();

    /**
     * 
     * @return My account address add confirm
     */
    String getMyAccountAddressAddConfirm();

    /**
     * 
     * @return url to my account address edit confirm
     */
    String getMyAccountAddressEditConfirm();

    /**
     * @return the register
     */
    String getRegister();

    /**
     * @return the newCustomer
     */
    String getNewCustomer();

    /**
     * @return the yourAddress
     */
    String getYourAddress();

    /**
     * @return the yourAddressAdd
     */
    String getYourAddressAdd();

    /**
     * @return the yourAddressAddConfirm
     */
    String getYourAddressAddConfirm();

    /**
     * @return the yourAddressSelect
     */
    String getYourAddressSelect();

    /**
     * @return the getYourAddressContinue
     */
    String getYourAddressContinue();

    /**
     * @return the yourAddressEdit
     */
    String getYourAddressEdit();

    /**
     * @return the yourAddressEditConfirm
     */
    String getYourAddressEditConfirm();

    /**
     * @return the removeEntries
     */
    String getRemoveEntries();

    /**
     * @return the updateDeliveryMode
     */
    String getUpdateDeliveryMode();

    /**
     * @return the cncSearchPosition
     */
    String getCncSearchPosition();

    /**
     * @return the cncSearchLocation
     */
    String getCncSearchLocation();

    /**
     * @return the payment
     */
    String getPayment();

    /**
     * @return the paymentExisting
     */
    String getPaymentExisting();

    /**
     * @return the paymentPaypal
     */
    String getPaymentPaypal();



    /**
     * @return the review
     */
    String getReview();

    /**
     * @return the placeOrder
     */
    String getPlaceOrder();

    /**
     * @return the PlaceOrderHolding
     */
    String getPlaceOrderHolding();

    /**
     * @return the thankYou
     */
    String getThankYou();

    /**
     * @return the store
     */
    String getStore();

    /**
     * @return store finder state
     */
    String getStoreFinderState();

    /**
     * @return the cart
     */
    String getCart();

    /**
     * @return the paymentValidateTmd
     */
    String getPaymentValidateTmd();

    /**
     * @return the myAccount
     */
    String getMyAccount();

    /**
     * 
     * @return store finder url
     */
    String getStoreFinder();

    /**
     * Returns the request mapping for store finder results page.
     * 
     * @return the request mapping for store finder results page
     */
    String getStoreFinderPosition();

    /**
     * 
     * @return Guest registration url
     */
    String getGuestRegistration();

    /**
     * @return cart url
     */
    String getCartCheckout();

    /**
     * @return cart update url
     */
    String getCartUpdate();

    /**
     * @return continue shopping url
     */
    String getCartContinueShopping();

    /**
     * @return the payment validate voucher
     */
    String getPaymentValidateVoucher();

    /**
     * @return the payment remove voucher
     */
    String getPaymentRemoveVoucher();

    /**
     * @return validate flybuys number
     */
    String getPaymentFlybuysValidate();

    /**
     * @return the flybuys login
     */
    String getPaymentFlybuysLogin();

    /**
     * @return the flybuys select
     */
    String getPaymentFlybuysSelectOption();

    /**
     * @return the bulky board
     */
    String getBulkyBoard();

    /**
     * 
     * @return url to logout
     */
    String getLogoutKiosk();
}

/**
 * 
 */
package au.com.target.tgtstorefront.controllers.url.impl;

import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.url.TargetUrlsMapping;






/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class TargetUrlsMappingImpl implements ControllerConstants, TargetUrlsMapping {

    /**
     * @return the root
     */
    @Override
    public String getHome() {
        return ROOT;
    }

    /**
     * @return the login
     */
    @Override
    public String getLogin() {
        return LOGIN;
    }

    /**
     * @return the logout
     */
    @Override
    public String getLogout() {
        return LOGOUT;
    }

    /**
     * @return the checkout
     */
    @Override
    public String getCheckout() {
        return CHECKOUT;
    }

    /**
     * @return the checkoutLogin
     */
    @Override
    public String getCheckoutLogin() {
        return CHECKOUT_LOGIN;
    }

    /**
     * @return the search
     */
    @Override
    public String getSearch() {
        return SEARCH;
    }

    /**
     * @return the register
     */
    @Override
    public String getRegister() {
        return REGISTER;
    }

    /**
     * @return the newCustomer
     */
    @Override
    public String getNewCustomer() {
        return NEW_CUSTOMER;
    }

    /**
     * @return the yourAddress
     */
    @Override
    public String getYourAddress() {
        return YOUR_ADDRESS;
    }

    /**
     * @return the yourAddressAdd
     */
    @Override
    public String getYourAddressAdd() {
        return YOUR_ADDRESS + YOUR_ADDRESS_ADD;
    }

    /**
     * @return the yourAddressAddConfirm
     */
    @Override
    public String getYourAddressAddConfirm() {
        return YOUR_ADDRESS + YOUR_ADDRESS_ADD_CONFIRM;
    }

    /**
     * @return the yourAddressSelect
     */
    @Override
    public String getYourAddressSelect() {
        return YOUR_ADDRESS + YOUR_ADDRESS_SELECT;
    }

    /**
     * @return the getYourAddressContinue
     */
    @Override
    public String getYourAddressContinue() {
        return YOUR_ADDRESS + YOUR_ADDRESS_CONTINUE;
    }

    /**
     * @return the yourAddressEdit
     */
    @Override
    public String getYourAddressEdit() {
        return YOUR_ADDRESS + YOUR_ADDRESS_EDIT;
    }

    /**
     * @return the yourAddressEditConfirm
     */
    @Override
    public String getYourAddressEditConfirm() {
        return YOUR_ADDRESS + YOUR_ADDRESS_EDIT_CONFIRM;
    }

    /**
     * @return the removeEntries
     */
    @Override
    public String getRemoveEntries() {
        return YOUR_ADDRESS + REMOVE_ENTRIES;
    }

    /**
     * @return the updateDeliveryMode
     */
    @Override
    public String getUpdateDeliveryMode() {
        return YOUR_ADDRESS + UPDATE_DELIVERY_MODE;
    }

    /**
     * @return the cncSearchPosition
     */
    @Override
    public String getCncSearchPosition() {
        return YOUR_ADDRESS + STORE_CNC_SEARCH_POSITION;

    }

    /**
     * @return the cncSearchLocation
     */
    @Override
    public String getCncSearchLocation() {
        return YOUR_ADDRESS + STORE_CNC_SEARCH_LOCATION;
    }

    /**
     * @return the payment
     */
    @Override
    public String getPayment() {
        return PAYMENT;
    }

    /**
     * @return the paymentExisting
     */
    @Override
    public String getPaymentExisting() {
        return PAYMENT + PAYMENT_EXISTING;
    }

    /**
     * @return the paymentPaypal
     */
    @Override
    public String getPaymentPaypal() {
        return PAYMENT + PAYMENT_PAYPAL;
    }



    /**
     * @return the review
     */
    @Override
    public String getReview() {
        return REVIEW;
    }

    /**
     * @return the reviewPlace
     */
    @Override
    public String getPlaceOrder() {
        return PLACE_ORDER;
    }

    /**
     * @return the reviewPlace
     */
    @Override
    public String getPlaceOrderHolding() {
        return PLACE_ORDER + PLACE_ORDER_HOLDING;
    }

    /**
     * @return the thankYou
     */
    @Override
    public String getThankYou() {
        return THANK_YOU;
    }

    /**
     * @return the store
     */
    @Override
    public String getStore() {
        return STORE;
    }

    /**
     * @return store finder state
     */
    @Override
    public String getStoreFinderState() {
        return STORE_FINDER + STORE_FINDER_STATE;
    }

    /**
     * @return the cart
     */
    @Override
    public String getCart() {
        return CART;
    }

    /**
     * @return the cart
     */
    @Override
    public String getCartUpdate() {
        return CART + CART_UPDATE;
    }

    /**
     * @return the cart
     */
    @Override
    public String getCartCheckout() {
        return CART + CART_CHECKOUT;
    }

    /**
     * @return the cart
     */
    @Override
    public String getCartContinueShopping() {
        return CART + CART_CONTINUE_SHOPPING;
    }

    /**
     * @return the paymentValidateTmd
     */
    @Override
    public String getPaymentValidateTmd() {
        return PAYMENT + PAYMENT_VALIDATE_TMD;
    }

    @Override
    public String getPaymentValidateVoucher() {
        return PAYMENT + PAYMENT_VALIDATE_VOUCHER;
    }

    @Override
    public String getPaymentRemoveVoucher() {
        return PAYMENT + PAYMENT_REMOVE_VOUCHER;
    }

    @Override
    public String getPaymentFlybuysValidate() {
        return PAYMENT + PAYMENT_FLYBUYS_VALIDATE;
    }

    @Override
    public String getPaymentFlybuysLogin() {
        return PAYMENT + PAYMENT_FLYBUYS_LOGIN;
    }

    @Override
    public String getPaymentFlybuysSelectOption() {
        return PAYMENT + PAYMENT_FLYBUYS_SELECT_OPTION;
    }

    /**
     * @return the myAccount
     */
    @Override
    public String getMyAccount() {
        return MY_ACCOUNT;
    }

    @Override
    public String getMyAccountOrders() {
        return MY_ACCOUNT + MY_ACCOUNT_ORDERS;
    }

    @Override
    public String getMyAccountOrderMakePayment() {
        return MY_ACCOUNT + MY_ACCOUNT_ORDER_MAKE_PAYMENT;
    }

    @Override
    public String getMyAccountOrderMakePaymentModal() {
        return MY_ACCOUNT + MY_ACCOUNT_ORDERS + MY_ACCOUNT_ORDER_MAKE_PAYMENT_MODAL;
    }

    @Override
    public String getMyAccountPersonalDetails() {
        return MY_ACCOUNT + MY_ACCOUNT_PERSONAL_DETAILS;
    }

    @Override
    public String getMyAccountUpdatePersonalDetails() {
        return MY_ACCOUNT + MY_ACCOUNT_UPDATE_PERSONAL_DETAILS;
    }

    @Override
    public String getMyAccountUpdatePassword() {
        return MY_ACCOUNT + MY_ACCOUNT_UPDATE_PASSWORD;
    }

    @Override
    public String getMyAccountUpdateEmail() {
        return MY_ACCOUNT + MY_ACCOUNT_UPDATE_EMAIL;
    }


    @Override
    public String getMyAccountPayment() {
        return MY_ACCOUNT + MY_ACCOUNT_PAYMENT;
    }

    @Override
    public String getMyAccountAddressDetails() {
        return MY_ACCOUNT + MY_ACCOUNT_ADDRESS_DETAILS;
    }

    @Override
    public String getMyAccountOrderDetails() {
        return MY_ACCOUNT + MY_ACCOUNT_ORDER;
    }

    @Override
    public String getMyAccountAddressAdd() {
        return MY_ACCOUNT + MY_ACCOUNT_ADDRESS_ADD;
    }

    @Override
    public String getMyAccountAddressEdit() {
        return MY_ACCOUNT + MY_ACCOUNT_ADDRESS_EDIT;
    }

    @Override
    public String getMyAccountAddressAddConfirm() {
        return MY_ACCOUNT + MY_ACCOUNT_ADDRESS_ADD_CONFIRM;
    }

    @Override
    public String getMyAccountAddressEditConfirm() {
        return MY_ACCOUNT + MY_ACCOUNT_ADDRESS_EDIT_CONFIRM;
    }

    @Override
    public String getStoreFinder() {
        return STORE_FINDER;
    }

    @Override
    public String getStoreFinderPosition() {
        return STORE_FINDER + STORE_FINDER_POSITION;
    }

    @Override
    public String getGuestRegistration() {
        return THANK_YOU + GUEST_REGISTER;
    }

    @Override
    public String getBulkyBoard() {
        return BULKYBOARD;
    }

    @Override
    public String getLogoutKiosk() {
        return LOGOUT_KIOSK;
    }
}

/**
 *
 */
package au.com.target.tgtstorefront.controllers.util;

import javax.servlet.http.HttpServletRequest;


/**
 * @author rmcalave
 *
 */
public final class BrandDataHelper {
    public static final String CURRENT_BRAND = "currentBrandCode";

    private BrandDataHelper() {
        // prevent construction
    }

    public static String getCurrentBrand(final HttpServletRequest request)
    {
        return (String)request.getAttribute(CURRENT_BRAND);
    }

    public static void setCurrentBrand(final HttpServletRequest request, final String currentBrandCode)
    {
        request.setAttribute(CURRENT_BRAND, currentBrandCode);
    }
}

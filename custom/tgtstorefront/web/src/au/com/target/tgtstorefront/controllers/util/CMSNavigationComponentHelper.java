/**
 *
 */
package au.com.target.tgtstorefront.controllers.util;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.servicelayer.services.CMSNavigationService;
import de.hybris.platform.core.model.ItemModel;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;



/**
 * @author pthoma20
 *
 */
public class CMSNavigationComponentHelper {

    private static final String CATEGORY_ROOT_NODE_ID = "TargetSiteCategoriesNavNode";

    private static final Logger LOG = Logger.getLogger(CMSNavigationComponentHelper.class);

    private CMSNavigationService cmsNavigationService;

    public CMSNavigationNodeModel getCategoryNavigationNode(final String categoryCode) {
        CMSNavigationNodeModel categoryRootNode = null;
        try {
            categoryRootNode = cmsNavigationService.getNavigationNodeForId(CATEGORY_ROOT_NODE_ID);

        }
        catch (final CMSItemNotFoundException ex) {
            LOG.error(CATEGORY_ROOT_NODE_ID + " not found");
            return null;
        }
        if (categoryCode != null && null != categoryRootNode.getChildren()) {
            for (final CMSNavigationNodeModel navigationNode : categoryRootNode.getChildren()) {
                if (isCategoryInNavigationNode(categoryCode, navigationNode)) {
                    return navigationNode;
                }
            }
        }
        return null;
    }


    private boolean isCategoryInNavigationNode(final String categoryCode, final CMSNavigationNodeModel navigationNode) {
        final CategoryModel category = getCategoryForNavigationNode(navigationNode);

        if (category != null && categoryCode.equals(category.getCode())) {
            return true;
        }

        return false;
    }

    private CategoryModel getCategoryForNavigationNode(final CMSNavigationNodeModel navigationNode) {
        final List<CMSNavigationEntryModel> entries = navigationNode.getEntries();
        for (final CMSNavigationEntryModel entry : entries) {
            final ItemModel entryItem = entry.getItem();
            if (entryItem instanceof CategoryModel) {
                final CategoryModel category = (CategoryModel)entryItem;
                return category;
            }
        }
        return null;
    }


    /**
     * @param cmsNavigationService
     *            the cmsNavigationService to set
     */
    @Required
    public void setCmsNavigationService(final CMSNavigationService cmsNavigationService) {
        this.cmsNavigationService = cmsNavigationService;
    }
}

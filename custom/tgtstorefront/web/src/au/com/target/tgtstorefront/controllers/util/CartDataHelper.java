/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import de.hybris.platform.commercefacades.product.ProductFacade;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtcore.order.TargetCommerceCartModificationStatus;
import au.com.target.tgtfacades.order.data.TargetCartData;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class CartDataHelper {

    public static final String CART_PAGE = "/basket";
    public static final String CHECKOUT_PAGE = "/checkout";

    public static final String BUY_NOW = "buynow";

    public static final String CART_MESSAGES = "basket.information.quantity.";

    public static final String ERROR_MISMATCH = "typeMismatch";

    public static final String TYPE = "?type=";
    private static final Logger LOG = Logger.getLogger(CartDataHelper.class);

    @Resource(name = "productFacade")
    private ProductFacade productFacade;

    /**
     * 
     * Order carts, Buy now / Layby / Long term layby
     * 
     * @param carts
     */
    public static Map<String, TargetCartData> orderCarts(final Map<String, TargetCartData> carts) {
        final LinkedHashMap<String, TargetCartData> cartsOrdered = new LinkedHashMap();
        if (carts.containsKey(CartDataHelper.BUY_NOW)) {
            cartsOrdered.put(CartDataHelper.BUY_NOW, carts.remove(CartDataHelper.BUY_NOW));
        }
        if (!carts.isEmpty()) {
            LOG.warn("Cannot order Carts correctly ! ");
        }
        return cartsOrdered;
    }

    /**
     * @param bindingResult
     * @param model
     */
    public static void addUpdateEntryError(final BindingResult bindingResult, final Model model) {
        for (final ObjectError error : bindingResult.getAllErrors())
        {
            if (ERROR_MISMATCH.equalsIgnoreCase(error.getCode()))
            {
                GlobalMessages.addErrorMessage(model, "basket.error.quantity.invalid");
            }
            else
            {
                GlobalMessages.addErrorMessage(model, error.getDefaultMessage());
            }
        }
    }

    /**
     * @param statusCode
     * @param action
     * @param redirectModel
     * @param formQty
     */
    public static void addCartModificationNotification(final String statusCode, final String action,
            final RedirectAttributes redirectModel, final Long cartQty, final Long formQty) {
        if (TargetCommerceCartModificationStatus.SUCCESS.equalsIgnoreCase(statusCode))
        {
            GlobalMessages.addFlashConfMessage(redirectModel, CART_MESSAGES.concat(action), new Object[] { cartQty });
        }
        else if (hasChanged(statusCode, cartQty, formQty))
        {
            GlobalMessages.addFlashErrorMessage(redirectModel, CART_MESSAGES.concat(statusCode),
                    new Object[] { cartQty });
        }
    }

    /**
     * @param statusCode
     * @param cartQty
     * @param formQty
     * @return true if any change happened
     */
    private static boolean hasChanged(final String statusCode, final Long cartQty, final Long formQty) {
        return (formQty.compareTo(cartQty) != 0);
    }
}

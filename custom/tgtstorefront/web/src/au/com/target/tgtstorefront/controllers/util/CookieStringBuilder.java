package au.com.target.tgtstorefront.controllers.util;

import javax.servlet.http.Cookie;

import org.apache.commons.lang.ArrayUtils;


/**
 * Builds cookie string in the <b>COOKIE1-NAME=COOKIE1-VAL;COOKIE2-NAME=COOKIE2-VAL;</b> format
 */
public class CookieStringBuilder {

    private static final String EQUAL = "=";
    private static final String SEMI_COLON = ";";

    /**
     * @param cookies
     * @return the cookie string
     */
    public String buildCookieString(final Cookie[] cookies) {
        final StringBuilder cookieString = new StringBuilder();
        if (ArrayUtils.isNotEmpty(cookies)) {
            for (final Cookie cookie : cookies) {
                cookieString.append(cookie.getName()).append(EQUAL).append(cookie.getValue()).append(SEMI_COLON);
            }
        }
        return cookieString.toString();
    }

}

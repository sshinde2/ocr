/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.endeca.navigation.AggrERec;
import com.endeca.navigation.AggrERecList;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERec;
import com.endeca.navigation.ERecList;
import com.endeca.navigation.Navigation;
import com.endeca.navigation.PropertyMap;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.converter.AggregatedEndecaRecordToTargetProductListerConverter;
import au.com.target.tgtfacades.product.data.TargetProductListerData;


/**
 * @author mjanarth
 *
 */
public class EndecaQueryResultsHelper {

    @Resource(name = "aggrERecTotargetProductListerConverter")
    private AggregatedEndecaRecordToTargetProductListerConverter<AggrERec, TargetProductListerData> aggrERecToProductConverter;


    /**
     * Converts query result to ProductsData
     * 
     * @param queryResults
     * @return productsData
     */
    public List<TargetProductListerData> getTargetProductsListForColorVariant(final ENEQueryResults queryResults) {
        final List<TargetProductListerData> productsData = new ArrayList<>();
        if (null != queryResults && null != queryResults.getNavigation()) {
            final Navigation navigation = queryResults.getNavigation();
            final AggrERecList aggregateERecs = navigation.getAggrERecs();
            if (null != aggregateERecs) {
                for (final Object eRecObject : aggregateERecs) {
                    final AggrERec aggrERec = (AggrERec)eRecObject;
                    final String colorVariantCode = getColourVariantCodeFromAggrRec(aggrERec);
                    if (null != colorVariantCode) {
                        final TargetProductListerData targetProduct = aggrERecToProductConverter.convert(aggrERec,
                                colorVariantCode);
                        productsData.add(targetProduct);
                    }
                }
            }
        }

        return productsData;
    }

    /**
     * 
     * @param queryResults
     * @return List
     */
    public List<TargetProductListerData> getTargetProductList(final ENEQueryResults queryResults) {
        return getTargetProductList(queryResults, null);
    }


    /**
     * Converts QueryResults to ListerData based on the selected variant and baseproductcode from the
     * customerProductInfo
     * 
     * @param queryResults
     * @param baseProductSelectedVariantMap
     * @return List
     */
    public List<TargetProductListerData> getTargetProductList(final ENEQueryResults queryResults,
            final Map<String, String> baseProductSelectedVariantMap) {

        final List<TargetProductListerData> productDataList = new ArrayList<>();
        if (queryResults != null && queryResults.getNavigation() != null) {
            final Navigation navigation = queryResults.getNavigation();
            final AggrERecList aggregateERecs = navigation.getAggrERecs();
            if (CollectionUtils.isEmpty(aggregateERecs)) {
                return productDataList;
            }
            for (final Object eRecObject : aggregateERecs) {
                final AggrERec aggrERec = (AggrERec)eRecObject;
                final TargetProductListerData targetProductListerData;
                if (null == baseProductSelectedVariantMap) {
                    targetProductListerData = aggrERecToProductConverter.convert(aggrERec);
                }
                else {
                    targetProductListerData = convertAggrERecToListerData(aggrERec, baseProductSelectedVariantMap);
                }

                productDataList.add(targetProductListerData);
            }
        }
        return productDataList;
    }

    /**
     * Check if the map has the selected variantcode,if not then pass call the converter without the selected variant
     * 
     * @param aggrERec
     * @return TargetProductListerData
     */
    protected TargetProductListerData convertAggrERecToListerData(final AggrERec aggrERec,
            final Map<String, String> productCodeAndSelectedVaraint) {
        final TargetProductListerData targetProductListerData;
        if (null == productCodeAndSelectedVaraint) {
            return aggrERecToProductConverter.convert(aggrERec);
        }

        final String baseProductCode = getProductCodeFromAggrERec(aggrERec);
        final String selectedVariant = productCodeAndSelectedVaraint.get(baseProductCode);
        if (null != selectedVariant) {
            targetProductListerData = aggrERecToProductConverter.convert(aggrERec, selectedVariant);
        }
        else {
            targetProductListerData = aggrERecToProductConverter.convert(aggrERec);
        }
        return targetProductListerData;

    }

    /**
     * Get the base productCode from the aggrecRec
     * 
     * @param aggrERec
     * @return baseproductCode
     */
    protected String getProductCodeFromAggrERec(final AggrERec aggrERec) {
        String baseProductCode = null;
        if (aggrERec.getRepresentative().getProperties()
                .containsKey(EndecaConstants.SEARCH_FIELD_PRODUCT_CODE)) {
            baseProductCode = (String)aggrERec.getRepresentative().getProperties()
                    .get(EndecaConstants.SEARCH_FIELD_PRODUCT_CODE);
        }
        return baseProductCode;
    }


    private String getColourVariantCodeFromAggrRec(final AggrERec aggrERec) {
        String colorVariantCode = null;
        if (null != aggrERec) {
            final ERecList childRecords = aggrERec.getERecs();
            for (final Iterator<ERec> it = childRecords.iterator(); it.hasNext();) {
                final ERec record = it.next();
                final PropertyMap representativeRecordProperties = record.getProperties();
                if (representativeRecordProperties
                        .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE)) {
                    colorVariantCode = (String)representativeRecordProperties
                            .get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE);
                    break;
                }

            }
        }
        return colorVariantCode;
    }


}

/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import java.util.Date;

import javax.annotation.Resource;

import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.voucher.data.TargetFlybuysLoginData;
import au.com.target.tgtstorefront.forms.checkout.FlybuysLoginForm;


/**
 * @author bhuang3
 *
 */
public class FlybuysDataHelper {

    @Resource(name = "targetCheckoutFacade")
    private TargetCheckoutFacade checkoutFacade;

    /**
     * convert the FlybuysLoginForm to FlybuysLoginData
     * 
     * @param flybuysLoginForm
     * @return targetFlybuysLoginData
     */
    public TargetFlybuysLoginData convertToFlybuysLoginData(final FlybuysLoginForm flybuysLoginForm) {
        final TargetFlybuysLoginData flybuysLoginData = new TargetFlybuysLoginData();
        final Date dateOfBirth = ThreePartDateHelper.convertToDate(flybuysLoginForm.getBirthDay(),
                flybuysLoginForm.getBirthMonth(),
                flybuysLoginForm.getBirthYear());
        flybuysLoginData.setDateOfBirth(dateOfBirth);
        final TargetCartData cartData = (TargetCartData)checkoutFacade.getCheckoutCart();
        flybuysLoginData.setCardNumber(cartData.getFlybuysNumber());
        flybuysLoginData.setPostCode(flybuysLoginForm.getPostCode());
        return flybuysLoginData;
    }
}

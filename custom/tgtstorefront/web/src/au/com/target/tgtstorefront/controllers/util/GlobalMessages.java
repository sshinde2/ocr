/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.util;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtstorefront.data.MessageKeyArguments;


/**
 * Displays "confirmation, information, error" messages
 */
public final class GlobalMessages
{
    private static final String CONF_MESSAGES_HOLDER = "accConfMsgs";
    private static final String INFO_MESSAGES_HOLDER = "accInfoMsgs";
    private static final String ERROR_MESSAGES_HOLDER = "accErrorMsgs";

    private static final String ARGS_SEPARATOR = ";";

    private GlobalMessages() {
        // prevent construction
    }

    public static void addConfMessage(final Model model, final String messageKey)
    {
        addMessage(model, CONF_MESSAGES_HOLDER, messageKey, null);
    }

    public static void addInfoMessage(final Model model, final String messageKey)
    {
        addMessage(model, INFO_MESSAGES_HOLDER, messageKey, null);
    }

    public static void addErrorMessage(final Model model, final String messageKey)
    {
        addMessage(model, ERROR_MESSAGES_HOLDER, messageKey, null);
    }

    public static void addConfMessage(final Model model, final String messageKey, final Object[] args)
    {
        addMessage(model, CONF_MESSAGES_HOLDER, messageKey, args);
    }

    public static void addInfoMessage(final Model model, final String messageKey, final Object[] args)
    {
        addMessage(model, INFO_MESSAGES_HOLDER, messageKey, args);
    }

    public static void addErrorMessage(final Model model, final String messageKey, final Object[] args)
    {
        addMessage(model, ERROR_MESSAGES_HOLDER, messageKey, args);
    }

    public static void addFlashConfMessage(final RedirectAttributes model, final String messageKey)
    {
        addFlashMessage(model, CONF_MESSAGES_HOLDER, messageKey, null);
    }

    public static void addFlashInfoMessage(final RedirectAttributes model, final String messageKey)
    {
        addFlashMessage(model, INFO_MESSAGES_HOLDER, messageKey, null);
    }

    public static void addFlashErrorMessage(final RedirectAttributes model, final String messageKey)
    {
        addFlashMessage(model, ERROR_MESSAGES_HOLDER, messageKey, null);
    }

    public static void addFlashConfMessage(final RedirectAttributes model, final String messageKey, final Object[] args)
    {
        addFlashMessage(model, CONF_MESSAGES_HOLDER, messageKey, args);
    }

    public static void addFlashInfoMessage(final RedirectAttributes model, final String messageKey, final Object[] args)
    {
        addFlashMessage(model, INFO_MESSAGES_HOLDER, messageKey, args);
    }

    public static void addFlashErrorMessage(final RedirectAttributes model, final String messageKey, final Object[] args)
    {
        addFlashMessage(model, ERROR_MESSAGES_HOLDER, messageKey, args);
    }

    protected static void addMessage(final Model model, final String messageHolder, final String messageKey,
            final Object[] args)
    {
        final MessageKeyArguments messageKeyArguments = new MessageKeyArguments();
        messageKeyArguments.setKey(messageKey);
        messageKeyArguments.setArgumentsString(processArgs(args));

        addMessage(messageHolder, messageKeyArguments, model.asMap());
    }

    protected static void addFlashMessage(final RedirectAttributes model, final String messageHolder,
            final String messageKey,
            final Object[] args)
    {
        final MessageKeyArguments messageKeyArguments = new MessageKeyArguments();
        messageKeyArguments.setKey(messageKey);
        messageKeyArguments.setArgumentsString(processArgs(args));
        addMessage(messageHolder, messageKeyArguments, (Map<String, Object>)model.getFlashAttributes());
    }

    /**
     * @param messageHolder
     * @param messageKeyArguments
     * @param map
     */
    private static void addMessage(final String messageHolder, final MessageKeyArguments messageKeyArguments,
            final Map<String, Object> map) {
        if (map.containsValue(messageHolder))
        {
            final List<MessageKeyArguments> messageKeys = (List<MessageKeyArguments>)map.get(messageHolder);
            messageKeys.add(messageKeyArguments);
            map.put(messageHolder, messageKeys);
        }
        else
        {
            map.put(messageHolder, Arrays.asList(new MessageKeyArguments[] { messageKeyArguments }));
        }

    }

    /**
     * @param args
     * @return args separate by ARGS_SEPARATOR
     */
    private static String processArgs(final Object[] args) {
        if (args == null) {
            return null;
        }
        final StringBuilder argsString = new StringBuilder();
        for (int i = 0; i < args.length; i++) {
            if (i != 0) {
                argsString.append(ARGS_SEPARATOR);
            }
            argsString.append(args[i]);
        }
        return argsString.toString();
    }

    /**
     * @param currentFlashScope
     * @param message
     */
    public static void addErrorMessage(final Map<String, Object> currentFlashScope, final String message) {
        addMessage(ERROR_MESSAGES_HOLDER, new MessageKeyArguments(message), currentFlashScope);
    }

    /**
     * @param currentFlashScope
     * @param message
     */
    public static void addInfoMessage(final Map<String, Object> currentFlashScope, final String message) {
        addMessage(INFO_MESSAGES_HOLDER, new MessageKeyArguments(message), currentFlashScope);
    }

    /**
     * @param currentFlashScope
     * @param message
     */
    public static void addConfMessage(final Map<String, Object> currentFlashScope, final String message) {
        addMessage(CONF_MESSAGES_HOLDER, new MessageKeyArguments(message), currentFlashScope);
    }

    /**
     * Copy all message holders ( {@value #CONF_MESSAGES_HOLDER}, {@value #INFO_MESSAGES_HOLDER},
     * {@value #ERROR_MESSAGES_HOLDER} ) from the supplied {@code model} to the supplied {@code redirectAttributes}.
     * 
     * Useful if performing a redirect after a redirect to ensure that the original messages are still displayed.
     * 
     * @param model
     *            The source
     * @param redirectAttributes
     *            The target
     */
    public static void copyMessagesToRedirectModel(final Model model, final RedirectAttributes redirectAttributes) {
        final Map<String, Object> modelMap = model.asMap();

        if (modelMap.containsKey(CONF_MESSAGES_HOLDER)) {
            redirectAttributes.addFlashAttribute(CONF_MESSAGES_HOLDER, modelMap.get(CONF_MESSAGES_HOLDER));
        }
        if (modelMap.containsKey(INFO_MESSAGES_HOLDER)) {
            redirectAttributes.addFlashAttribute(INFO_MESSAGES_HOLDER, modelMap.get(INFO_MESSAGES_HOLDER));
        }
        if (modelMap.containsKey(ERROR_MESSAGES_HOLDER)) {
            redirectAttributes.addFlashAttribute(ERROR_MESSAGES_HOLDER, modelMap.get(ERROR_MESSAGES_HOLDER));
        }
    }

    /**
     * Checks the provided model for the presence of any message. For this to be true, the model must contain at least
     * one message holder that has at least one message in it.
     * 
     * @param model
     *            The model
     * @return true if the model contains at least one message, false otherwise
     * @see #hasConfMessages(Model)
     * @see #hasInfoMessages(Model)
     * @see #hasErrorMessages(Model)
     */
    public static boolean hasMessages(final Model model) {
        return hasConfMessages(model) || hasInfoMessages(model) || hasErrorMessages(model);
    }

    /**
     * Checks the provided model for the presence of conf messages. For this to be true, the model must contain the conf
     * message holder and that message holder must contain as least one message.
     * 
     * @param model
     *            The model
     * @return true if the model contains conf messages, false otherwise
     * @see #CONF_MESSAGES_HOLDER
     */
    public static boolean hasConfMessages(final Model model) {
        return hasMessagesHolder(model, CONF_MESSAGES_HOLDER);
    }

    /**
     * Checks the provided model for the presence of info messages. For this to be true, the model must contain the info
     * message holder and that message holder must contain as least one message.
     * 
     * @param model
     *            The model
     * @return true if the model contains info messages, false otherwise
     * @see #INFO_MESSAGES_HOLDER
     */
    public static boolean hasInfoMessages(final Model model) {
        return hasMessagesHolder(model, INFO_MESSAGES_HOLDER);
    }

    /**
     * Checks the provided model for the presence of error messages. For this to be true, the model must contain the
     * error message holder and that message holder must contain as least one message.
     * 
     * @param model
     *            The model
     * @return true if the model contains error messages, false otherwise
     * @see #ERROR_MESSAGES_HOLDER
     */
    public static boolean hasErrorMessages(final Model model) {
        return hasMessagesHolder(model, ERROR_MESSAGES_HOLDER);
    }

    private static boolean hasMessagesHolder(final Model model, final String messageHolder) {
        final Map<String, Object> modelMap = model.asMap();

        if (!modelMap.containsKey(messageHolder)) {
            return false;
        }

        final List<MessageKeyArguments> messageKeys = (List<MessageKeyArguments>)modelMap.get(messageHolder);
        return CollectionUtils.isNotEmpty(messageKeys);
    }
}

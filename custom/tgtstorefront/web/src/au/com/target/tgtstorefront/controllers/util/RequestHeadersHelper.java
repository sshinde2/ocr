/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Required;


/**
 * Utility for reading request headers
 * 
 */
public class RequestHeadersHelper {

    public static final String X_REQUESTED_WITH = "X-Requested-With";
    public static final String XHR = "XMLHttpRequest";

    private String clientIPHeaderName;

    public String getRemoteIPAddress(final HttpServletRequest request) {

        final String trueClientIP = request.getHeader(clientIPHeaderName);
        final String remoteAddress = trueClientIP != null ? trueClientIP : request.getRemoteAddr();
        return remoteAddress;
    }


    public boolean isXhrRequest(final HttpServletRequest request)
    {
        return XHR.equals(request.getHeader(X_REQUESTED_WITH));
    }

    /**
     * @param clientIPHeaderName
     *            the clientIPHeaderName to set
     */
    @Required
    public void setClientIPHeaderName(final String clientIPHeaderName) {
        this.clientIPHeaderName = clientIPHeaderName;
    }


}

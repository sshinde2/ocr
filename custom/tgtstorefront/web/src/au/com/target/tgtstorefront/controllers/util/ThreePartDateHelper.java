/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


/**
 * Utils for three part dates
 * 
 */
public final class ThreePartDateHelper {

    private ThreePartDateHelper() {
        // util
    }

    /**
     * Try to create a date out of the given parts.
     * 
     * @param day
     * @param month
     * @param year
     * @return date or null if not valid
     */
    public static Date convertToDate(final String day, final String month, final String year) {

        if (day == null || month == null || year == null) {
            return null;
        }

        int iDay = 0;
        int iMonth = 0;
        int iYear = 0;

        try {
            iDay = Integer.parseInt(day);
            iMonth = Integer.parseInt(month);
            iYear = Integer.parseInt(year);

        }
        catch (final NumberFormatException e) {
            return null;
        }

        try {
            final Calendar cal = new GregorianCalendar();
            cal.setLenient(false);
            cal.set(iYear, iMonth - 1, iDay);
            return cal.getTime();
        }
        catch (final Exception e) {
            return null;
        }

    }
}

/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.TypeMismatchException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.Error;
import au.com.target.tgtfacades.response.data.Response;


/**
 * Helper to create consistent responses for web service exceptions. Could be replaced with a controller advisor if we
 * ever move to Spring 4. in Spring 3.2 handles exceptions globally, but in Spring 4 you can specify which classes it
 * should handle exceptions from.
 *
 */
public class WebServiceExceptionHelper {

    /**
     * Create a failed Response with the according exception
     * 
     * @param ex
     * @return an appropriate response with error
     */
    public Response handleException(final HttpServletRequest request, final Exception ex) {
        final Response response = new Response(false);
        final BaseResponseData responseData = new BaseResponseData();
        Error error = null;
        if (ex instanceof BindException) {
            error = createBindError((BindException)ex);
        }
        else if (ex instanceof MethodArgumentNotValidException) {
            error = createMethodArgumentNotValidError((MethodArgumentNotValidException)ex);
        }
        else if (ex instanceof TypeMismatchException) {
            error = createTypeMismatchError();
        }
        //default error setup
        if (error == null) {
            error = new Error("ERROR_UNEXPECTED_EXCEPTION");
            error.setMessage(ex.getMessage());
            error.setStackTrace(ExceptionUtils.getFullStackTrace(ex));
        }
        error.setRequestUri(request.getRequestURI());
        responseData.setError(error);
        response.setData(responseData);
        return response;
    }

    /**
     * Create a Response for a BindException.
     * 
     * @param ex
     *            The exception to handle
     * @return An appropriate response
     */
    private Error createBindError(final BindException ex) {
        return createBindingResultError(ex.getBindingResult());
    }

    /**
     * Create a Response for a MethodArgumentNotValidException.
     * 
     * @param ex
     *            The exception to handle
     * @return An appropriate response
     */
    private Error createMethodArgumentNotValidError(final MethodArgumentNotValidException ex) {
        return createBindingResultError(ex.getBindingResult());
    }

    /**
     * Create a Response to a TypeMismatchException (exception not required).
     * 
     * @return An appropriate response
     */
    private Error createTypeMismatchError() {
        final Error error = new Error("ERR_INVALID_TYPE");
        error.setMessage("Invalid post data");
        return error;
    }

    private Error createBindingResultError(final BindingResult result) {
        final List<FieldError> fieldErrors = result.getFieldErrors();
        if (CollectionUtils.isNotEmpty(fieldErrors)) {
            final Error error = new Error("ERR_VALIDATION");
            error.setMessage("Invalid post data");
            return error;
        }
        return null;
    }
}

/**
 * 
 */
package au.com.target.tgtstorefront.controllers.verifyaddr;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.target.tgtstorefront.controllers.AbstractController;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.AddressDataHelper;
import au.com.target.tgtstorefront.data.AddressSuggestionResult;
import au.com.target.tgtverifyaddr.data.FormattedAddressData;


@Controller
public class AddressVerificationController extends AbstractController {

    protected static final Logger LOG = Logger.getLogger(AddressVerificationController.class);

    @Resource(name = "addressDataHelper")
    private AddressDataHelper addressDataHelper;

    @RequestMapping(value = "/address-lookup/search", method = RequestMethod.GET)
    public String getSuggestions(
            @RequestParam("keyword") final String keyword, final Model model) {
        final AddressSuggestionResult result = addressDataHelper.getAddressSuggestions(keyword);
        model.addAttribute("addressSuggestionResult", result);
        return ControllerConstants.Views.Fragments.Address.ADDRESS_AUTO_SUGGESTIONS;
    }

    @RequestMapping(value = "/address-lookup/format", method = RequestMethod.GET)
    public String formatAddress(@RequestParam(value = "code") final String code,
            @RequestParam(required = false) final String particialAddress, final Model model,
            final HttpServletRequest request) {
        final FormattedAddressData formattedAddress = addressDataHelper.formatAddress(code, particialAddress);
        model.addAttribute("formattedAddress", formattedAddress);
        request.getSession().setAttribute(AddressDataHelper.SINGLE_LINE_ADDRESS_FORMAT, formattedAddress);
        return ControllerConstants.Views.Fragments.Address.ADDRESS_FORMAT;
    }
}

/**
 * 
 */
package au.com.target.tgtstorefront.data;

import java.util.List;

import au.com.target.tgtstorefront.controllers.pages.checkout.AbstractCheckoutController.SelectOption;


/**
 * @author htan3
 *
 */
public class AddressSuggestionResult {

    private String status;

    private String keyword;

    private List<SelectOption> suggestions;

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * @return the keyword
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * @param keyword
     *            the keyword to set
     */
    public void setKeyword(final String keyword) {
        this.keyword = keyword;
    }

    /**
     * @return the suggestions
     */
    public List<SelectOption> getSuggestions() {
        return suggestions;
    }

    /**
     * @param suggestions
     *            the suggestions to set
     */
    public void setSuggestions(final List<SelectOption> suggestions) {
        this.suggestions = suggestions;
    }


}

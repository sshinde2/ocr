/**
 * 
 */
package au.com.target.tgtstorefront.data;



/**
 * @author Benoit VanalderWeireldt
 * 
 */
public class MessageKeyArguments {
    private String key;
    private String argumentsString;

    /**
     * Default Constructor
     */
    public MessageKeyArguments() {
        this.key = null;
        this.argumentsString = null;
    }

    /**
     * 
     * @param key
     */
    public MessageKeyArguments(final String key) {
        super();
        this.key = key;
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key
     *            the key to set
     */
    public void setKey(final String key) {
        this.key = key;
    }

    /**
     * @return the argumentsString
     */
    public String getArgumentsString() {
        return argumentsString;
    }

    /**
     * @param argumentsString
     *            the argumentsString to set
     */
    public void setArgumentsString(final String argumentsString) {
        this.argumentsString = argumentsString;
    }
}

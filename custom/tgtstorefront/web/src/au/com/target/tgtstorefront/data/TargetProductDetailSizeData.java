/**
 * 
 */
package au.com.target.tgtstorefront.data;

import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.product.data.VariantOptionQualifierData;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class TargetProductDetailSizeData extends TargetProductGenericData {
    /**
     * 
     */
    public TargetProductDetailSizeData(final VariantOptionData variantOptionData) {
        super(variantOptionData);
        setUrl(variantOptionData.getUrl());
        for (final VariantOptionQualifierData variantOptionQualifierData : variantOptionData
                .getVariantOptionQualifiers()) {
            if ("size".equalsIgnoreCase(variantOptionQualifierData.getQualifier())) {
                setName(variantOptionQualifierData.getValue());
            }
        }
    }
}

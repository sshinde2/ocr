package au.com.target.tgtstorefront.data.sitemap;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * Represents a site map index document. Because sitemap cannot be larger than {@code 10MB} or list more than
 * {@code 50,000} URLs, index document can contain multiple site map documents.
 */
@XmlRootElement(name = "sitemapindex")
@XmlAccessorType(XmlAccessType.FIELD)
public class SiteMapIndexData {

    @XmlElement(name = "sitemap", required = true)
    private List<SiteMapIndexElementData> elements;

    /**
     * Returns the {@code &lt;sitemap&gt;} elements for this index.
     * 
     * @return the list of elements for this index
     */
    public List<SiteMapIndexElementData> getElements() {
        return elements;
    }

    /**
     * Sets the {@code &lt;sitemap&gt;} elements for this index.
     * 
     * @param elements
     *            the list of elements to set
     */
    public void setElements(final List<SiteMapIndexElementData> elements) {
        this.elements = elements;
    }
}

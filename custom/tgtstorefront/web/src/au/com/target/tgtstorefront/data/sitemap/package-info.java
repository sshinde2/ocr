@XmlSchema(namespace = "http://www.sitemaps.org/schemas/sitemap/0.9",
        location = "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd",
        elementFormDefault = XmlNsForm.QUALIFIED)
@XmlJavaTypeAdapters(@XmlJavaTypeAdapter(type = DateTime.class, value = DateTimeAdapter.class))
package au.com.target.tgtstorefront.data.sitemap;

import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;

import org.joda.time.DateTime;

import au.com.target.tgtstorefront.util.DateTimeAdapter;



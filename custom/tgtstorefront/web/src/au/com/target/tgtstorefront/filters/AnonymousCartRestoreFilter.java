package au.com.target.tgtstorefront.filters;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.servicelayer.session.SessionService;

import java.io.IOException;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.filter.OncePerRequestFilter;

import au.com.target.tgtfacades.order.TargetCartFacade;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.util.CookieRetrievalHelper;


/**
 *
 */
public class AnonymousCartRestoreFilter extends OncePerRequestFilter {

    private static final Logger LOG = Logger.getLogger(AnonymousCartRestoreFilter.class);
    private SessionService sessionService;
    private String cookieName;
    private TargetUserFacade targetUserFacade;
    private TargetCartFacade targetCartFacade;
    private CookieRetrievalHelper cookieRetrievalHelper;
    private SalesApplicationFacade salesApplicationFacade;

    private List<SalesApplication> excludedSalesAppsToRestoreCart;

    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
            final FilterChain filterChain) throws ServletException, IOException {

        if (targetUserFacade.isCurrentUserAnonymous()
                && sessionService.getAttribute(WebConstants.SESSION_CART_KEY) == null
                && !(excludedSalesAppsToRestoreCart.contains(salesApplicationFacade.getCurrentSalesApplication()))) {
            final String guidToken = cookieRetrievalHelper.getCookieValue(request, getCookieName());
            if (StringUtils.isNotEmpty(guidToken)) {
                try {
                    targetCartFacade.restoreCart(guidToken);
                }
                catch (final CommerceCartRestorationException exception) {
                    LOG.warn("Could not restore  Anonymous Cart for guid::" + guidToken);
                }
            }
        }

        filterChain.doFilter(request, response);
    }

    @Required
    public void setSessionService(final SessionService sessionService)
    {
        this.sessionService = sessionService;
    }

    /**
     * @param targetUserFacade
     *            the targetUserFacade to set
     */
    @Required
    public void setTargetUserFacade(final TargetUserFacade targetUserFacade) {
        this.targetUserFacade = targetUserFacade;
    }

    /**
     * @param targetCartFacade
     *            the targetCartFacade to set
     */
    @Required
    public void setTargetCartFacade(final TargetCartFacade targetCartFacade) {
        this.targetCartFacade = targetCartFacade;
    }


    /**
     * @return the cookieName
     */
    public String getCookieName() {
        return cookieName;
    }


    /**
     * @param cookieName
     *            the cookieName to set
     */
    @Required
    public void setCookieName(final String cookieName) {
        this.cookieName = cookieName;
    }

    /**
     * @param cookieRetrievalHelper
     *            the cookieRetrievalHelper to set
     */
    @Required
    public void setCookieRetrievalHelper(final CookieRetrievalHelper cookieRetrievalHelper) {
        this.cookieRetrievalHelper = cookieRetrievalHelper;
    }


    /**
     * @param salesApplicationFacade
     *            the salesApplicationFacade to set
     */
    @Required
    public void setSalesApplicationFacade(final SalesApplicationFacade salesApplicationFacade) {
        this.salesApplicationFacade = salesApplicationFacade;
    }

    /**
     * @param excludedSalesAppsToRestoreCart
     *            the excludedSalesAppsToRestoreCart to set
     */
    @Required
    public void setExcludedSalesAppsToRestoreCart(final List<SalesApplication> excludedSalesAppsToRestoreCart) {
        this.excludedSalesAppsToRestoreCart = excludedSalesAppsToRestoreCart;
    }

}

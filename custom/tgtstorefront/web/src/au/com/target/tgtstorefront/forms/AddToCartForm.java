/**
 * 
 */
package au.com.target.tgtstorefront.forms;

import javax.validation.Valid;


/**
 * @author gbaker2
 *
 */
public class AddToCartForm {

    private String productCode;

    // Defaults to 1
    private String qty = "1";

    @Valid
    private GiftRecipientForm giftRecipientForm;

    private Boolean isReactRequest = Boolean.FALSE;

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the qty
     */
    public String getQty() {
        return qty;
    }

    /**
     * @param qty
     *            the qty to set
     */
    public void setQty(final String qty) {
        this.qty = qty;
    }

    /**
     * @return the giftRecipientForm
     */
    public GiftRecipientForm getGiftRecipientForm() {
        return giftRecipientForm;
    }

    /**
     * @param giftRecipientForm
     *            the giftRecipientForm to set
     */
    public void setGiftRecipientForm(final GiftRecipientForm giftRecipientForm) {
        this.giftRecipientForm = giftRecipientForm;
    }

    /**
     * @return the isReactRequest
     */
    public Boolean getIsReactRequest() {
        return isReactRequest;
    }

    /**
     * @param isReactRequest
     *            the isReactRequest to set
     */
    public void setIsReactRequest(final Boolean isReactRequest) {
        this.isReactRequest = isReactRequest;
    }

}

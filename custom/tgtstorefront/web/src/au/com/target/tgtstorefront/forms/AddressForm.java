/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.forms;

import javax.validation.constraints.NotNull;

import au.com.target.tgtstorefront.forms.validation.AddressLine1;
import au.com.target.tgtstorefront.forms.validation.AddressLine2;
import au.com.target.tgtstorefront.forms.validation.CitySuburb;
import au.com.target.tgtstorefront.forms.validation.FirstName;
import au.com.target.tgtstorefront.forms.validation.LastName;
import au.com.target.tgtstorefront.forms.validation.Phone;
import au.com.target.tgtstorefront.forms.validation.PostCode;
import au.com.target.tgtstorefront.forms.validation.Title;


/**
 */
public class AddressForm
{
    private String addressId;

    @Title
    private String titleCode;

    @FirstName
    private String firstName;

    @LastName
    private String lastName;

    @AddressLine1
    private String line1;

    @AddressLine2
    private String line2;

    @CitySuburb
    private String townCity;

    private String state;

    @PostCode
    private String postcode;

    private String countryIso;

    @Phone
    private String phoneNumber;

    private Boolean saveInAddressBook;
    private Boolean defaultAddress;
    private Boolean shippingAddress;
    private Boolean billingAddress;

    private String deliveryModeCode;

    public String getAddressId()
    {
        return addressId;
    }

    public void setAddressId(final String addressId)
    {
        this.addressId = addressId;
    }

    /**
     * @return the titleCode
     */
    public String getTitleCode()
    {
        return titleCode;
    }

    /**
     * @param titleCode
     *            the titleCode to set
     */
    public void setTitleCode(final String titleCode)
    {
        this.titleCode = titleCode;
    }

    /**
     * @return the firstName
     */
    public String getFirstName()
    {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName)
    {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName()
    {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName)
    {
        this.lastName = lastName;
    }


    /**
     * @return the line1
     */
    public String getLine1()
    {
        return line1;
    }

    /**
     * @param line1
     *            the line1 to set
     */
    public void setLine1(final String line1)
    {
        this.line1 = line1;
    }

    /**
     * @return the line2
     */
    public String getLine2()
    {
        return line2;
    }

    /**
     * @param line2
     *            the line2 to set
     */
    public void setLine2(final String line2)
    {
        this.line2 = line2;
    }

    /**
     * @return the townCity
     */
    public String getTownCity()
    {
        return townCity;
    }

    /**
     * @param townCity
     *            the townCity to set
     */
    public void setTownCity(final String townCity)
    {
        this.townCity = townCity;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */

    public void setState(final String state) {
        this.state = state;
    }

    /**
     * @return the postcode
     */
    public String getPostcode()
    {
        return postcode;
    }

    /**
     * @param postcode
     *            the postcode to set
     */
    public void setPostcode(final String postcode)
    {
        this.postcode = postcode;
    }

    /**
     * @return the countryIso
     */
    @NotNull(message = "{required.country}")
    public String getCountryIso()
    {
        return countryIso;
    }

    /**
     * @param countryIso
     *            the countryIso to set
     */
    public void setCountryIso(final String countryIso)
    {
        this.countryIso = countryIso;
    }

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber
     *            the phoneNumber to set
     */
    public void setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Boolean getSaveInAddressBook()
    {
        return saveInAddressBook;
    }

    public void setSaveInAddressBook(final Boolean saveInAddressBook)
    {
        this.saveInAddressBook = saveInAddressBook;
    }

    public Boolean getDefaultAddress()
    {
        return defaultAddress;
    }

    public void setDefaultAddress(final Boolean defaultAddress)
    {
        this.defaultAddress = defaultAddress;
    }

    public Boolean getShippingAddress()
    {
        return shippingAddress;
    }

    public void setShippingAddress(final Boolean shippingAddress)
    {
        this.shippingAddress = shippingAddress;
    }

    public Boolean getBillingAddress()
    {
        return billingAddress;
    }

    public void setBillingAddress(final Boolean billingAddress)
    {
        this.billingAddress = billingAddress;
    }

    /**
     * @return the deliveryModeCode
     */
    public String getDeliveryModeCode() {
        return deliveryModeCode;
    }

    /**
     * @param deliveryModeCode
     *            the deliveryModeCode to set
     */
    public void setDeliveryModeCode(final String deliveryModeCode) {
        this.deliveryModeCode = deliveryModeCode;
    }
}

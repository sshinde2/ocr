/**
 * 
 */
package au.com.target.tgtstorefront.forms;

import au.com.target.tgtstorefront.forms.validation.AddressLine1;
import au.com.target.tgtstorefront.forms.validation.AddressLine2;
import au.com.target.tgtstorefront.forms.validation.CitySuburb;
import au.com.target.tgtstorefront.forms.validation.Country;
import au.com.target.tgtstorefront.forms.validation.FirstName;
import au.com.target.tgtstorefront.forms.validation.InternationalPostCode;
import au.com.target.tgtstorefront.forms.validation.LastName;
import au.com.target.tgtstorefront.forms.validation.Title;


/**
 * @author asingh78
 * 
 */
public class BillingAddressForm {

    private String addressId;
    @Title
    private String titleCode;

    @FirstName
    private String firstName;

    @LastName
    private String lastName;

    @AddressLine1
    private String line1;

    @AddressLine2
    private String line2;

    @CitySuburb
    private String townCity;

    private String state;

    @InternationalPostCode
    private String postcode;
    @Country
    private String countryIso;



    private Boolean saveInAddressBook;
    private Boolean defaultAddress;

    public String getAddressId()
    {
        return addressId;
    }

    public void setAddressId(final String addressId)
    {
        this.addressId = addressId;
    }

    /**
     * @return the titleCode
     */
    public String getTitleCode()
    {
        return titleCode;
    }

    /**
     * @param titleCode
     *            the titleCode to set
     */
    public void setTitleCode(final String titleCode)
    {
        this.titleCode = titleCode;
    }

    /**
     * @return the firstName
     */
    public String getFirstName()
    {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName)
    {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName()
    {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName)
    {
        this.lastName = lastName;
    }


    /**
     * @return the line1
     */
    public String getLine1()
    {
        return line1;
    }

    /**
     * @param line1
     *            the line1 to set
     */
    public void setLine1(final String line1)
    {
        this.line1 = line1;
    }

    /**
     * @return the line2
     */
    public String getLine2()
    {
        return line2;
    }

    /**
     * @param line2
     *            the line2 to set
     */
    public void setLine2(final String line2)
    {
        this.line2 = line2;
    }

    /**
     * @return the townCity
     */
    public String getTownCity()
    {
        return townCity;
    }

    /**
     * @param townCity
     *            the townCity to set
     */
    public void setTownCity(final String townCity)
    {
        this.townCity = townCity;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */

    public void setState(final String state) {
        this.state = state;
    }

    /**
     * @return the postcode
     */
    public String getPostcode()
    {
        return postcode;
    }

    /**
     * @param postcode
     *            the postcode to set
     */
    public void setPostcode(final String postcode)
    {
        this.postcode = postcode;
    }

    /**
     * @return the countryIso
     */
    public String getCountryIso()
    {
        return countryIso;
    }

    /**
     * @param countryIso
     *            the countryIso to set
     */
    public void setCountryIso(final String countryIso)
    {
        this.countryIso = countryIso;
    }



    public Boolean getSaveInAddressBook()
    {
        return saveInAddressBook;
    }

    public void setSaveInAddressBook(final Boolean saveInAddressBook)
    {
        this.saveInAddressBook = saveInAddressBook;
    }

    public Boolean getDefaultAddress()
    {
        return defaultAddress;
    }

    public void setDefaultAddress(final Boolean defaultAddress)
    {
        this.defaultAddress = defaultAddress;
    }


}

/**
 * 
 */
package au.com.target.tgtstorefront.forms;

/**
 * @author gbaker2
 * 
 */
public class ConfirmAddressForm {

    private String confirmedAddressCode;
    private boolean useSuppliedAddress;

    private AddressForm suppliedAddress;

    /**
     * @return the confirmedAddressCode
     */
    public String getConfirmedAddressCode() {
        return confirmedAddressCode;
    }

    /**
     * @param confirmedAddressCode
     *            the confirmedAddressCode to set
     */
    public void setConfirmedAddressCode(final String confirmedAddressCode) {
        this.confirmedAddressCode = confirmedAddressCode;
    }

    /**
     * @return the useSuppliedAddress
     */
    public boolean isUseSuppliedAddress() {
        return useSuppliedAddress;
    }

    /**
     * @param useSuppliedAddress
     *            the useSuppliedAddress to set
     */
    public void setUseSuppliedAddress(final boolean useSuppliedAddress) {
        this.useSuppliedAddress = useSuppliedAddress;
    }

    /**
     * @return the suppliedAddress
     */
    public AddressForm getSuppliedAddress() {
        return suppliedAddress;
    }

    /**
     * @param suppliedAddress
     *            the suppliedAddress to set
     */
    public void setSuppliedAddress(final AddressForm suppliedAddress) {
        this.suppliedAddress = suppliedAddress;
    }

}

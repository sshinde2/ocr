package au.com.target.tgtstorefront.forms;

import au.com.target.tgtstorefront.forms.validation.Email;
import au.com.target.tgtstorefront.forms.validation.FirstName;
import au.com.target.tgtstorefront.forms.validation.LastName;
import au.com.target.tgtstorefront.forms.validation.Title;


/**
 * Form for subscription
 */

public class ENewsSubscriptionForm
{

    @Title(mandatory = true)
    private String title;

    @FirstName
    private String firstName;

    @LastName
    private String lastName;

    @Email
    private String email;

    private String subscriptionSource;



    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * @return the subscriptionSource
     */
    public String getSubscriptionSource() {
        return subscriptionSource;
    }

    /**
     * @param subscriptionSource
     *            the subscriptionSource to set
     */
    public void setSubscriptionSource(final String subscriptionSource) {
        this.subscriptionSource = subscriptionSource;
    }

}

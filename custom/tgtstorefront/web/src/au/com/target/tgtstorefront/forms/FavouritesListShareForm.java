/**
 * 
 */
package au.com.target.tgtstorefront.forms;

import au.com.target.tgtstorefront.forms.validation.Email;
import au.com.target.tgtstorefront.forms.validation.FullName;
import au.com.target.tgtstorefront.forms.validation.MessageText;
import au.com.target.tgtstorefront.forms.validation.validator.FieldTypeEnum;



/**
 * @author bmcalis2
 *
 */
public class FavouritesListShareForm {

    @FullName(field = FieldTypeEnum.fullName)
    private String fullName;

    @Email(field = FieldTypeEnum.recipientEmail)
    private String recipientEmailAddress;

    @MessageText(mandatory = false)
    private String messageText;



    /**
     * @return the recipientEmailAddress
     */
    public String getRecipientEmailAddress() {
        return recipientEmailAddress;
    }

    /**
     * @param recipientEmailAddress
     *            the recipientEmailAddress to set
     */
    public void setRecipientEmailAddress(final String recipientEmailAddress) {
        this.recipientEmailAddress = recipientEmailAddress;
    }

    /**
     * @return the messageText
     */
    public String getMessageText() {
        return messageText;
    }

    /**
     * @param messageText
     *            the messageText to set
     */
    public void setMessageText(final String messageText) {
        this.messageText = messageText;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName
     *            the fullName to set
     */
    public void setFullName(final String fullName) {
        this.fullName = fullName;
    }

}

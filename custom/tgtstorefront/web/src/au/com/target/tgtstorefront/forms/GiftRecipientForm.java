/**
 * 
 */
package au.com.target.tgtstorefront.forms;

import au.com.target.tgtstorefront.forms.validation.Email;
import au.com.target.tgtstorefront.forms.validation.FirstName;
import au.com.target.tgtstorefront.forms.validation.LastName;
import au.com.target.tgtstorefront.forms.validation.MessageText;
import au.com.target.tgtstorefront.forms.validation.validator.FieldTypeEnum;



/**
 * @author smishra1
 *
 */
public class GiftRecipientForm {

    @FirstName(field = FieldTypeEnum.recipientFirstName)
    private String firstName;

    @LastName(field = FieldTypeEnum.recipientLastName)
    private String lastName;

    @Email(field = FieldTypeEnum.recipientEmail)
    private String recipientEmailAddress;

    @MessageText(mandatory = false)
    private String messageText;

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the recipientEmailAddress
     */
    public String getRecipientEmailAddress() {
        return recipientEmailAddress;
    }

    /**
     * @param recipientEmailAddress
     *            the recipientEmailAddress to set
     */
    public void setRecipientEmailAddress(final String recipientEmailAddress) {
        this.recipientEmailAddress = recipientEmailAddress;
    }

    /**
     * @return the messageText
     */
    public String getMessageText() {
        return messageText;
    }

    /**
     * @param messageText
     *            the messageText to set
     */
    public void setMessageText(final String messageText) {
        this.messageText = messageText;
    }

}

/**
 * 
 */
package au.com.target.tgtstorefront.forms;

import au.com.target.tgtstorefront.forms.validation.Email;


/**
 * @author asingh78
 * 
 */
public class GuestForm
{
    @Email
    private String email;

    public String getEmail()
    {
        return email;
    }

    public void setEmail(final String email)
    {
        this.email = email;
    }
}

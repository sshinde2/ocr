/**
 * 
 */
package au.com.target.tgtstorefront.forms;

/**
 * @author gbaker2
 * 
 */
public class MakePaymentForm {

    private String paymentAmount;

    private boolean payOutstandingAmount = false;

    /**
     * @return the paymentAmount
     */
    public String getPaymentAmount() {
        return paymentAmount;
    }

    /**
     * @param paymentAmount
     *            the paymentAmount to set
     */
    public void setPaymentAmount(final String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    /**
     * @return the payOutstandingAmount
     */
    public boolean isPayOutstandingAmount() {
        return payOutstandingAmount;
    }

    /**
     * @param payOutstandingAmount
     *            the payOutstandingAmount to set
     */
    public void setPayOutstandingAmount(final boolean payOutstandingAmount) {
        this.payOutstandingAmount = payOutstandingAmount;
    }

}

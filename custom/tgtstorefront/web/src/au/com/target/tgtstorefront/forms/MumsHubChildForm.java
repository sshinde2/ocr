/**
 * 
 */
package au.com.target.tgtstorefront.forms;

import au.com.target.tgtstorefront.forms.validation.BabyGender;
import au.com.target.tgtstorefront.forms.validation.FirstName;
import au.com.target.tgtstorefront.forms.validation.ThreePartDate;



/**
 * @author gbaker2
 * 
 */
public class MumsHubChildForm {

    @FirstName(mandatory = false)
    private String firstName;

    @ThreePartDate(message = "{validation.mumsHub.dob}", mandatory = false)
    private ThreePartDateForm dateOfBirth;

    @BabyGender
    private String gender;


    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }



    /**
     * @return the dateOfBirth
     */
    public ThreePartDateForm getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth
     *            the dateOfBirth to set
     */
    public void setDateOfBirth(final ThreePartDateForm dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender
     *            the gender to set
     */
    public void setGender(final String gender) {
        this.gender = gender;
    }


}

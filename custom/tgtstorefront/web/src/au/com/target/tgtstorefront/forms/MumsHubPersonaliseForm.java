/**
 * 
 */
package au.com.target.tgtstorefront.forms;

import java.util.List;

import javax.validation.Valid;

import au.com.target.tgtstorefront.forms.validation.ExpectedGender;
import au.com.target.tgtstorefront.forms.validation.FirstName;
import au.com.target.tgtstorefront.forms.validation.Gender;
import au.com.target.tgtstorefront.forms.validation.LastName;
import au.com.target.tgtstorefront.forms.validation.ThreePartDate;
import au.com.target.tgtstorefront.forms.validation.Title;


/**
 * @author gbaker2
 * 
 */
public class MumsHubPersonaliseForm
{

    @Title(mandatory = true)
    private String titleCode;

    @FirstName
    private String firstName;

    @LastName
    private String lastName;

    @Gender
    private String gender;

    @ThreePartDate(message = "{validation.mumsHub.dob}", mandatory = false)
    private ThreePartDateForm dateOfBirth;

    @ThreePartDate(message = "{validation.mumsHub.dod}", mandatory = false)
    private ThreePartDateForm dueDate;

    @ExpectedGender
    private String expectedGender;

    @Valid
    private List<MumsHubChildForm> children;

    private String email;

    /**
     * @return the titleCode
     */
    public String getTitleCode() {
        return titleCode;
    }

    /**
     * @param titleCode
     *            the titleCode to set
     */
    public void setTitleCode(final String titleCode) {
        this.titleCode = titleCode;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }


    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender
     *            the gender to set
     */
    public void setGender(final String gender) {
        this.gender = gender;
    }

    /**
     * @return the dateOfBirth
     */
    public ThreePartDateForm getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth
     *            the dateOfBirth to set
     */
    public void setDateOfBirth(final ThreePartDateForm dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * @return the dueDate
     */
    public ThreePartDateForm getDueDate() {
        return dueDate;
    }

    /**
     * @param dueDate
     *            the dueDate to set
     */
    public void setDueDate(final ThreePartDateForm dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * @return the expectedGender
     */
    public String getExpectedGender() {
        return expectedGender;
    }

    /**
     * @param expectedGender
     *            the expectedGender to set
     */
    public void setExpectedGender(final String expectedGender) {
        this.expectedGender = expectedGender;
    }

    /**
     * @return the children
     */
    public List<MumsHubChildForm> getChildren() {
        return children;
    }

    /**
     * @param children
     *            the children to set
     */
    public void setChildren(final List<MumsHubChildForm> children) {
        this.children = children;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

}

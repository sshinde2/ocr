/**
 * 
 */
package au.com.target.tgtstorefront.forms;

import java.util.List;

import javax.validation.Valid;

import au.com.target.tgtstorefront.forms.validation.Email;
import au.com.target.tgtstorefront.forms.validation.FirstName;
import au.com.target.tgtstorefront.forms.validation.FlyBuys;
import au.com.target.tgtstorefront.forms.validation.Gender;
import au.com.target.tgtstorefront.forms.validation.LastName;
import au.com.target.tgtstorefront.forms.validation.MobilePhone;
import au.com.target.tgtstorefront.forms.validation.PostCode;
import au.com.target.tgtstorefront.forms.validation.State;
import au.com.target.tgtstorefront.forms.validation.Title;


/**
 * @author gbaker2
 * 
 */
public class MumsHubSubscriptionForm
{

    @Title(mandatory = true)
    private String titleCode;

    @FirstName
    private String firstName;

    @LastName
    private String lastName;

    @Email
    private String email;

    @MobilePhone
    private String mobileNumber;

    private boolean receiveSMS;

    @FlyBuys
    private String flyBuys;

    @Gender
    private String gender;


    private ThreePartDateForm dateOfBirth;

    @PostCode
    private String postCode;

    @State
    private String state;


    private ThreePartDateForm dueDate;

    @Gender
    private String expectedGender;

    private boolean signUpEnewsLetter;

    @Valid
    private List<MumsHubChildForm> children;

    /**
     * @return the titleCode
     */
    public String getTitleCode() {
        return titleCode;
    }

    /**
     * @param titleCode
     *            the titleCode to set
     */
    public void setTitleCode(final String titleCode) {
        this.titleCode = titleCode;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * @return the mobileNumber
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * @param mobileNumber
     *            the mobileNumber to set
     */
    public void setMobileNumber(final String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * @return the receiveSMS
     */
    public boolean isReceiveSMS() {
        return receiveSMS;
    }

    /**
     * @param receiveSMS
     *            the receiveSMS to set
     */
    public void setReceiveSMS(final boolean receiveSMS) {
        this.receiveSMS = receiveSMS;
    }

    /**
     * @return the flyBuys
     */
    public String getFlyBuys() {
        return flyBuys;
    }

    /**
     * @param flyBuys
     *            the flyBuys to set
     */
    public void setFlyBuys(final String flyBuys) {
        this.flyBuys = flyBuys;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender
     *            the gender to set
     */
    public void setGender(final String gender) {
        this.gender = gender;
    }

    /**
     * @return the dateOfBirth
     */
    public ThreePartDateForm getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth
     *            the dateOfBirth to set
     */
    public void setDateOfBirth(final ThreePartDateForm dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * @return the postCode
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * @param postCode
     *            the postCode to set
     */
    public void setPostCode(final String postCode) {
        this.postCode = postCode;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(final String state) {
        this.state = state;
    }

    /**
     * @return the dueDate
     */
    public ThreePartDateForm getDueDate() {
        return dueDate;
    }

    /**
     * @param dueDate
     *            the dueDate to set
     */
    public void setDueDate(final ThreePartDateForm dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * @return the expectedGender
     */
    public String getExpectedGender() {
        return expectedGender;
    }

    /**
     * @param expectedGender
     *            the expectedGender to set
     */
    public void setExpectedGender(final String expectedGender) {
        this.expectedGender = expectedGender;
    }

    /**
     * @return the signUpEnewsLetter
     */
    public boolean isSignUpEnewsLetter() {
        return signUpEnewsLetter;
    }

    /**
     * @param signUpEnewsLetter
     *            the signUpEnewsLetter to set
     */
    public void setSignUpEnewsLetter(final boolean signUpEnewsLetter) {
        this.signUpEnewsLetter = signUpEnewsLetter;
    }

    /**
     * @return the children
     */
    public List<MumsHubChildForm> getChildren() {
        return children;
    }

    /**
     * @param children
     *            the children to set
     */
    public void setChildren(final List<MumsHubChildForm> children) {
        this.children = children;
    }





}

package au.com.target.tgtstorefront.forms;

import au.com.target.tgtstorefront.forms.validation.AddressLine1;
import au.com.target.tgtstorefront.forms.validation.AddressLine2;
import au.com.target.tgtstorefront.forms.validation.CitySuburb;
import au.com.target.tgtstorefront.forms.validation.FirstName;
import au.com.target.tgtstorefront.forms.validation.InternationalPostCode;
import au.com.target.tgtstorefront.forms.validation.LastName;
import au.com.target.tgtstorefront.forms.validation.Phone;
import au.com.target.tgtstorefront.forms.validation.PostCode;
import au.com.target.tgtstorefront.forms.validation.Title;


/**
 * This is a form currently used by /ws-api/v1/{baseSiteId}/checkout/delivery/addresses/create for single page checkout
 * 
 * Legacy checkout please refer to AddressForm
 * 
 * @author htan3
 *
 */
public class TargetAddressForm
{
    @Title(groups = { ContactValidationGroup.class })
    private String title;

    @FirstName(groups = { ContactValidationGroup.class })
    private String firstName;

    @LastName(groups = { ContactValidationGroup.class })
    private String lastName;

    @AddressLine1(groups = { DomesticAddressValidationGroup.class, InternationalAddressValidationGroup.class })
    private String line1;

    @AddressLine2(groups = { DomesticAddressValidationGroup.class, InternationalAddressValidationGroup.class })
    private String line2;

    @CitySuburb(groups = { DomesticAddressValidationGroup.class, InternationalAddressValidationGroup.class })
    private String town;

    private String state;

    @PostCode(groups = { DomesticAddressValidationGroup.class })
    private String postalCode;

    @InternationalPostCode(groups = { InternationalAddressValidationGroup.class })
    private String internationalPostalcode;

    @Phone(groups = { ContactValidationGroup.class })
    private String phone;

    private String singleLineId;

    private String singleLineLabel;

    private String countryCode;

    public interface ContactValidationGroup {
        //Marker interface for contact validation group.
    }

    public interface DomesticAddressValidationGroup {
        //Marker interface for address validation group.
    }

    public interface InternationalAddressValidationGroup {
        //Marker interface for address validation group.
    }

    /**
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * @return the firstName
     */
    public String getFirstName()
    {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName)
    {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName()
    {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName)
    {
        this.lastName = lastName;
    }


    /**
     * @return the line1
     */
    public String getLine1()
    {
        return line1;
    }

    /**
     * @param line1
     *            the line1 to set
     */
    public void setLine1(final String line1)
    {
        this.line1 = line1;
    }

    /**
     * @return the line2
     */
    public String getLine2()
    {
        return line2;
    }

    /**
     * @param line2
     *            the line2 to set
     */
    public void setLine2(final String line2)
    {
        this.line2 = line2;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */

    public void setState(final String state) {
        this.state = state;
    }

    /**
     * @return the postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * @param postalCode
     *            the postalCode to set
     */
    public void setPostalCode(final String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     *            the phone to set
     */
    public void setPhone(final String phone) {
        this.phone = phone;
    }

    /**
     * @return the town
     */
    public String getTown() {
        return town;
    }

    /**
     * @param town
     *            the town to set
     */
    public void setTown(final String town) {
        this.town = town;
    }

    /**
     * @return the singleLineId
     */
    public String getSingleLineId() {
        return singleLineId;
    }

    /**
     * @param singleLineId
     *            the singleLineId to set
     */
    public void setSingleLineId(final String singleLineId) {
        this.singleLineId = singleLineId;
    }

    /**
     * @return the singleLineLabel
     */
    public String getSingleLineLabel() {
        return singleLineLabel;
    }

    /**
     * @param singleLineLabel
     *            the singleLineLabel to set
     */
    public void setSingleLineLabel(final String singleLineLabel) {
        this.singleLineLabel = singleLineLabel;
    }

    /**
     * @return the countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @param countryCode
     *            the countryCode to set
     */
    public void setCountryCode(final String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * @return the internationalPostalcode
     */
    public String getInternationalPostalcode() {
        return internationalPostalcode;
    }

    /**
     * @param internationalPostalcode
     *            the internationalPostalcode to set
     */
    public void setInternationalPostalcode(final String internationalPostalcode) {
        this.internationalPostalcode = internationalPostalcode;
    }
}
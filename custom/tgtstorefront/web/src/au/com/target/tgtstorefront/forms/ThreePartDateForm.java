/**
 * 
 */
package au.com.target.tgtstorefront.forms;

/**
 * @author bhuang3
 * 
 */
public class ThreePartDateForm {

    private String year;

    private String month;

    private String day;

    /**
     * @return the year
     */
    public String getYear() {
        return year;
    }

    /**
     * @param year
     *            the year to set
     */
    public void setYear(final String year) {
        this.year = year;
    }

    /**
     * @return the month
     */
    public String getMonth() {
        return month;
    }

    /**
     * @param month
     *            the month to set
     */
    public void setMonth(final String month) {
        this.month = month;
    }

    /**
     * @return the day
     */
    public String getDay() {
        return day;
    }

    /**
     * @param day
     *            the day to set
     */
    public void setDay(final String day) {
        this.day = day;
    }


}

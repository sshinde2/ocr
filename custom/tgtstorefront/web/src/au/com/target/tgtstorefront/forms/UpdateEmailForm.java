/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.forms;

import au.com.target.tgtstorefront.forms.validation.Email;
import au.com.target.tgtstorefront.forms.validation.EqualAttributes;
import au.com.target.tgtstorefront.forms.validation.Password;



/**
 * Form object for updating email
 */
@EqualAttributes(message = "{validation.checkEmail.equals}", value =
{ "email", "chkEmail" })
public class UpdateEmailForm
{
    @Email
    private String email;

    private String chkEmail;
    @Password
    private String password;


    /**
     * @return the password
     */
    public String getPassword()
    {
        return password;
    }


    /**
     * @param password
     *            the password to set
     */
    public void setPassword(final String password)
    {
        this.password = password;
    }

    /**
     * @return the email
     */
    public String getEmail()
    {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(final String email)
    {
        this.email = email;
    }


    /**
     * @return the chkEmail
     */
    public String getChkEmail()
    {
        return chkEmail;
    }


    /**
     * @param chkEmail
     *            the chkEmail to set
     */
    public void setChkEmail(final String chkEmail)
    {
        this.chkEmail = chkEmail;
    }

}

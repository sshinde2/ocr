/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.forms;

import au.com.target.tgtstorefront.forms.validation.FirstName;
import au.com.target.tgtstorefront.forms.validation.LastName;
import au.com.target.tgtstorefront.forms.validation.Title;



/**
 * Form object for updating profile.
 */
public class UpdateProfileForm
{
    @Title
    private String titleCode;
    @FirstName
    private String firstName;
    @LastName
    private String lastName;
    private Boolean allowTracking;


    /**
     * @return the titleCode
     */
    public String getTitleCode()
    {
        return titleCode;
    }

    /**
     * @param titleCode
     *            the titleCode to set
     */
    public void setTitleCode(final String titleCode)
    {
        this.titleCode = titleCode;
    }

    /**
     * @return the firstName
     */
    public String getFirstName()
    {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName)
    {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName()
    {
        return lastName;
    }


    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName)
    {
        this.lastName = lastName;
    }

    /**
     * @return the allowTracking
     */
    public Boolean getAllowTracking() {
        return allowTracking;
    }

    /**
     * @param allowTracking
     *            the allowTracking to set
     */
    public void setAllowTracking(final Boolean allowTracking) {
        this.allowTracking = allowTracking;
    }


}

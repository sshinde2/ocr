/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.forms;

import au.com.target.tgtstorefront.forms.validation.EqualAttributes;
import au.com.target.tgtstorefront.forms.validation.Password;



/**
 * Form object for updating the password.
 */
@EqualAttributes(message = "{validation.checkPwd.equals}", value =
{ "pwd", "checkPwd" })
public class UpdatePwdForm
{
    @Password
    private String pwd;
    private String checkPwd;
    private String token;


    /**
     * @return the pwd
     */
    public String getPwd()
    {
        return pwd;
    }

    /**
     * @param pwd
     *            the pwd to set
     */
    public void setPwd(final String pwd)
    {
        this.pwd = pwd;
    }

    /**
     * @return the checkPwd
     */
    public String getCheckPwd()
    {
        return checkPwd;
    }

    /**
     * @param checkPwd
     *            the checkPwd to set
     */
    public void setCheckPwd(final String checkPwd)
    {
        this.checkPwd = checkPwd;
    }

    /**
     * @return the token
     */
    public String getToken()
    {
        return token;
    }

    /**
     * @param token
     *            the token to set
     */
    public void setToken(final String token)
    {
        this.token = token;
    }
}

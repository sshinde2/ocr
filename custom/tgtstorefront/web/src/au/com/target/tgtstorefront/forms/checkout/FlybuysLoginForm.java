/**
 * 
 */
package au.com.target.tgtstorefront.forms.checkout;

import au.com.target.tgtstorefront.forms.validation.PostCode;
import au.com.target.tgtstorefront.forms.validation.ThreePartDate;


/**
 * @author gbaker2
 * 
 */
@ThreePartDate(message = "{validation.flybuys.dob}", value =
{ "birthDay", "birthMonth", "birthYear" })
public class FlybuysLoginForm {

    private String cardNumber;

    private String birthDay;
    private String birthMonth;
    private String birthYear;

    @PostCode
    private String postCode;

    /**
     * @return the cardNumber
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * @param cardNumber
     *            the cardNumber to set
     */
    public void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * @return the birthDay
     */
    public String getBirthDay() {
        return birthDay;
    }

    /**
     * @param birthDay
     *            the birthDay to set
     */
    public void setBirthDay(final String birthDay) {
        this.birthDay = birthDay;
    }

    /**
     * @return the birthMonth
     */
    public String getBirthMonth() {
        return birthMonth;
    }

    /**
     * @param birthMonth
     *            the birthMonth to set
     */
    public void setBirthMonth(final String birthMonth) {
        this.birthMonth = birthMonth;
    }

    /**
     * @return the birthYear
     */
    public String getBirthYear() {
        return birthYear;
    }

    /**
     * @param birthYear
     *            the birthYear to set
     */
    public void setBirthYear(final String birthYear) {
        this.birthYear = birthYear;
    }

    /**
     * @return the postCode
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * @param postCode
     *            the postCode to set
     */
    public void setPostCode(final String postCode) {
        this.postCode = postCode;
    }

}

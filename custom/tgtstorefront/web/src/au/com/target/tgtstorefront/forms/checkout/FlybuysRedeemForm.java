/**
 * 
 */
package au.com.target.tgtstorefront.forms.checkout;

import org.hibernate.validator.constraints.NotBlank;



/**
 * @author bmcalis2
 * 
 */
public class FlybuysRedeemForm {

    @NotBlank(message = "{validation.flybuys.redeem}")
    private String redeemCode;

    /**
     * @return the redeemCode
     */
    public String getRedeemCode() {
        return redeemCode;
    }

    /**
     * @param redeemCode
     *            the redeemCode to set
     */
    public void setRedeemCode(final String redeemCode) {
        this.redeemCode = redeemCode;
    }

}

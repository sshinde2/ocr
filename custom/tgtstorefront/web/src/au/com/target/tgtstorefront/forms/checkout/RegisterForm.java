/**
 * 
 */
package au.com.target.tgtstorefront.forms.checkout;

import au.com.target.tgtstorefront.forms.validation.Password;


/**
 * @author mgazal
 *
 */
public class RegisterForm {

    @Password(checkBlacklist = false)
    private String password;

    private boolean optIntoMarketing;

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * @return the optIntoMarketing
     */
    public boolean isOptIntoMarketing() {
        return optIntoMarketing;
    }

    /**
     * @param optIntoMarketing
     *            the optIntoMarketing to set
     */
    public void setOptIntoMarketing(final boolean optIntoMarketing) {
        this.optIntoMarketing = optIntoMarketing;
    }

}

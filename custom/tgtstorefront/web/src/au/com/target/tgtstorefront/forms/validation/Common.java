/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtstorefront.forms.validation.validator.CommonValidator;
import au.com.target.tgtstorefront.forms.validation.validator.FieldTypeEnum;
import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * @author Benoit Vanalderweireldt
 * 
 *         If you set boolean to false test will not be executed (faster validation)
 * 
 */
@Retention(RUNTIME)
@Constraint(validatedBy = CommonValidator.class)
@Documented
@Target({ ANNOTATION_TYPE, FIELD })
public @interface Common {

    String message() default StringUtils.EMPTY;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * 
     * Use for error messages
     * 
     */
    FieldTypeEnum field();

    /**
     * 
     * Can it be blank, default not
     * 
     */
    boolean isMandatory() default true;

    /**
     * 
     * is the size is range
     * 
     */
    boolean isSizeRanged() default true;

    /**
     * 
     * must match the pattern
     * 
     */
    boolean mustMatch() default true;

    /**
     * 
     * Size range
     * 
     */
    int[] sizeRange() default { 0, Integer.MAX_VALUE };

    /**
     * 
     * Pattern it must match, it will always be contain in field PATTERN of the given class
     * 
     */
    Class pattern() default TargetValidationCommon.Default.class;
}

/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtstorefront.forms.validation.validator.EmailValidator;
import au.com.target.tgtstorefront.forms.validation.validator.FieldTypeEnum;


/**
 * 
 * @author Benoit Vanalderweireldt
 * @Email never use Default Message (please see
 *        {@link au.com.target.tgtstorefront.forms.validation.validator.EmailValidator}) to edit messages
 * 
 */
@Retention(RUNTIME)
@Constraint(validatedBy = EmailValidator.class)
@Documented
@Target({ FIELD })
public @interface Email {
    String message() default StringUtils.EMPTY;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    FieldTypeEnum field() default FieldTypeEnum.email;
}

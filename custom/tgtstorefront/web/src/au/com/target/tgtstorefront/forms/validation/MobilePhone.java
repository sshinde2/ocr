/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation;

import static java.lang.annotation.ElementType.FIELD;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtstorefront.forms.validation.validator.MobilePhoneValidator;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
@Documented
@Constraint(validatedBy = { MobilePhoneValidator.class })
@Target({ FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface MobilePhone {
    String message() default StringUtils.EMPTY;

    boolean mandatory() default false;

    Class<?>[] groups() default {};

    Class<?>[] payload() default {};
}

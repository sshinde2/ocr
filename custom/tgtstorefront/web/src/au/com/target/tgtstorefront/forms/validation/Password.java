/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtstorefront.forms.validation.validator.PasswordValidator;


/**
 * 
 * @author Benoit Vanalderweireldt
 * 
 */
@Retention(RUNTIME)
@Constraint(validatedBy = PasswordValidator.class)
@Documented
@Target({ FIELD })
public @interface Password {
    public abstract String message() default StringUtils.EMPTY;

    public abstract Class<?>[] groups() default {};

    public abstract Class<?>[] payload() default {};

    public abstract boolean checkBlacklist() default true;
}

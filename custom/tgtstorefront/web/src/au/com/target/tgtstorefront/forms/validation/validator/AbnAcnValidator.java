/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtstorefront.forms.validation.AbnAcn;
import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * @author rmcalave
 * 
 */
public class AbnAcnValidator extends AbstractTargetValidator implements ConstraintValidator<AbnAcn, String> {

    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
     */
    @Override
    public void initialize(final AbnAcn arg0) {
        field = FieldTypeEnum.abnOrAcn;
        sizeRange = new int[] { TargetValidationCommon.AbnOrAcn.MIN_SIZE,
                TargetValidationCommon.AbnOrAcn.MAX_SIZE };
        isSizeRange = true;
        mustMatch = true;
        loadPattern(TargetValidationCommon.AbnOrAcn.class);
    }

    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
     */
    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtstorefront.forms.validation.validator.AbstractTargetValidator#isAvailable(java.lang.String)
     */
    @Override
    protected boolean isAvailable(final String value) {
        if (StringUtils.isBlank(value)) {
            return true;
        }

        if (value.length() == 9) {
            final String reverseAcn = StringUtils.reverse(value);
            final int checkDigit = Character.getNumericValue(reverseAcn.charAt(0));

            int sum = 0;

            // Start at 1 since 0 is the check digit
            for (int i = 1; i < reverseAcn.length(); i++) {
                sum += i * Character.getNumericValue(reverseAcn.charAt(i));
            }

            final int remainder = sum % 10;
            int complement = 10 - remainder;

            if (complement == 10) {
                complement = 0;
            }

            if (complement == checkDigit) {
                return true;
            }

            return false;
        }
        else if (value.length() == 11) {
            // Start with the first digit
            int sum = (Character.getNumericValue(value.charAt(0)) - 1) * 10;

            for (int i = 1; i < value.length(); i++) {
                final int weightingFactor = (i * 2) - 1;
                sum += weightingFactor * Character.getNumericValue(value.charAt(i));
            }

            final int remainder = sum % 89;

            return remainder == 0;
        }

        return false;
    }
}

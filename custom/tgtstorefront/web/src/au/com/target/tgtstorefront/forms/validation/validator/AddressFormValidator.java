/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtstorefront.forms.AddressForm;



/**
 * @author Benoit VanalderWeireldt
 * 
 */
public class AddressFormValidator implements Validator {

    private static final String ISO_SEPARATOR = "-";

    @Resource(name = "commonI18NService")
    private CommonI18NService commonI18NService;

    @Override
    public boolean supports(final Class<?> arg0) {
        return AddressForm.class.equals(arg0);
    }

    @Override
    public void validate(final Object arg0, final Errors errors) {
        final AddressForm addressForm = (AddressForm)arg0;

        if (StringUtils.isEmpty(addressForm.getCountryIso())) {
            errors.rejectValue("countryIso",
                    AbstractTargetValidator.INVALID_PREFIX.concat(ErrorTypeEnum.empty.toString()),
                    new Object[] { FieldTypeEnum.country.isConsonant(), FieldTypeEnum.country.name() },
                    StringUtils.EMPTY);
        }
        else if (TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA.equalsIgnoreCase(addressForm.getCountryIso())) {
            verifyFormForAustralia(addressForm, errors);
        }
        else {
            verifyFormForOtherCountry(addressForm, errors);
        }

    }

    /**
     * @param addressForm
     * @param errors
     */
    private void verifyFormForOtherCountry(final AddressForm addressForm, final Errors errors) {
        try {
            commonI18NService.getCountry(addressForm.getCountryIso());
        }
        catch (final UnknownIdentifierException unknownIdentifierException) {
            errors.rejectValue("countryIso",
                    AbstractTargetValidator.INVALID_PREFIX.concat(ErrorTypeEnum.notAvailable.toString()),
                    new Object[] { FieldTypeEnum.country.name() },
                    StringUtils.EMPTY);
        }
    }

    /**
     * @param addressForm
     * @param errors
     */
    private void verifyFormForAustralia(final AddressForm addressForm, final Errors errors) {
        final CountryModel country = commonI18NService.getCountry(TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);

        try {
            if (StringUtils.isBlank(addressForm.getState())) {
                throw new IllegalArgumentException();
            }
            commonI18NService.getRegion(country,
                    country.getIsocode().concat(AddressFormValidator.ISO_SEPARATOR).concat(addressForm.getState()));
        }
        catch (final UnknownIdentifierException unknownIdentifierException) {
            errors.rejectValue("state",
                    AbstractTargetValidator.INVALID_PREFIX.concat(ErrorTypeEnum.notAvailable.toString()),
                    new Object[] { FieldTypeEnum.state.name() },
                    StringUtils.EMPTY);
        }
        catch (final IllegalArgumentException illegalArgumentException) {
            errors.rejectValue(
                    "state",
                    AbstractTargetValidator.INVALID_PREFIX.concat(ErrorTypeEnum.empty.toString()),
                    new Object[] { FieldTypeEnum.state.isConsonant(),
                            FieldTypeEnum.state.name() },
                    StringUtils.EMPTY);
        }
    }

}

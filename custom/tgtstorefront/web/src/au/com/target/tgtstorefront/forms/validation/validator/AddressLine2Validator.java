/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtstorefront.forms.validation.AddressLine2;
import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class AddressLine2Validator extends AbstractTargetValidator implements ConstraintValidator<AddressLine2, String> {

    @Override
    public void initialize(final AddressLine2 arg0) {
        field = FieldTypeEnum.addressLine2;
        sizeRange = new int[] { TargetValidationCommon.AddressLine.MIN_SIZE,
                TargetValidationCommon.AddressLine.MAX_SIZE };
        isMandatory = false;
        isSizeRange = true;
        mustMatch = true;
        loadPattern(TargetValidationCommon.AddressLine.class);
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }

    @Override
    protected String getInvalidPatternMessage() {
        return getInvalidSizeRangeSpecCaraSlash(field, sizeRange[0], sizeRange[1]);
    }

}

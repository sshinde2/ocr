/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;


import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.pages.checkout.AbstractCheckoutController.SelectOption;
import au.com.target.tgtstorefront.forms.validation.BabyGender;



/**
 * @author bhuang3
 * 
 */
public class BabyGenderValidator extends AbstractTargetValidator implements
        ConstraintValidator<BabyGender, String> {

    @Override
    public void initialize(final BabyGender arg0) {
        field = FieldTypeEnum.babyGender;
        isMandatory = arg0.mandatory();
        isSizeRange = false;
        mustMatch = false;
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }

    @Override
    protected boolean isAvailable(final String value) {
        final List<SelectOption> babyGenders = WebConstants.UserInformation.BABY_GENDERS;
        for (final SelectOption babyGender : babyGenders) {
            if (babyGender.getCode().equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }
}

/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtstorefront.forms.validation.CitySuburb;
import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class CitySuburbValidator extends AbstractTargetValidator implements ConstraintValidator<CitySuburb, String> {

    @Override
    public void initialize(final CitySuburb arg0) {
        field = FieldTypeEnum.citySuburb;
        sizeRange = new int[] { TargetValidationCommon.CitySuburb.MIN_SIZE,
                TargetValidationCommon.CitySuburb.MAX_SIZE };
        isMandatory = true;
        isSizeRange = true;
        mustMatch = true;
        loadPattern(TargetValidationCommon.CitySuburb.class);
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }

    @Override
    protected String getInvalidPatternMessage() {
        return getInvalidsizeRangeSpaceLetters(field, sizeRange[0], sizeRange[1]);
    }

}

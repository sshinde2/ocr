/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

import au.com.target.tgtstorefront.forms.validation.Common;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class CommonValidator extends AbstractTargetValidator implements ConstraintValidator<Common, String> {

    protected static final Logger LOG = Logger.getLogger(CommonValidator.class);

    @Override
    public void initialize(final Common common) {
        Validate.notNull(common.field());
        Validate.notNull(common.sizeRange());
        Validate.isTrue(common.sizeRange().length == 2);
        field = common.field();
        sizeRange = common.sizeRange();
        isSizeRange = common.isSizeRanged();
        mustMatch = common.mustMatch();
        isMandatory = common.isMandatory();
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }
}

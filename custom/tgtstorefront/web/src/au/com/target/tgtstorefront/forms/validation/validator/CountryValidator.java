/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtstorefront.forms.validation.Country;



public class CountryValidator extends AbstractTargetValidator implements ConstraintValidator<Country, String> {



    @Override
    public void initialize(final Country arg0) {
        field = FieldTypeEnum.country;
        isMandatory = true;
        isSizeRange = false;
        mustMatch = false;
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }


}

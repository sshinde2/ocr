/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtstorefront.constants.WebConstants.DonationRequest.PreferredMethodofContact;
import au.com.target.tgtstorefront.forms.validation.DonationPreferredContactMethod;


/**
 * @author bhuang3
 * 
 */
public class DonationPreferredContactMethodValidator extends AbstractTargetValidator implements
        ConstraintValidator<DonationPreferredContactMethod, String> {

    @Override
    public void initialize(final DonationPreferredContactMethod arg0) {
        field = FieldTypeEnum.donationPreferredContactMethod;
        isMandatory = true;
        isSizeRange = false;
        mustMatch = false;
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }

    @Override
    protected boolean isAvailable(final String value) {
        for (final PreferredMethodofContact result : PreferredMethodofContact.values()) {
            if (result.getValue().equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }

}

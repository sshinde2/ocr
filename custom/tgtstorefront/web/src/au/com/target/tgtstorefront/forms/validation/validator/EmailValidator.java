/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtstorefront.forms.validation.Email;
import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class EmailValidator extends AbstractTargetValidator implements ConstraintValidator<Email, String> {

    @Override
    public void initialize(final Email arg0) {
        field = arg0.field();
    }

    @Override
    public boolean isValid(final String email, final ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation(); //Disable the empty default message
        if (StringUtils.isBlank(email)) {
            updatedContext(context, getInvalidEmpty(field));
            return false;
        }
        if (!TargetValidationCommon.Email.PATTERN.matcher(email).matches()) {
            updatedContext(context, getInvalidFormat(field));
            return false;
        }
        else {
            return true;
        }
    }
}

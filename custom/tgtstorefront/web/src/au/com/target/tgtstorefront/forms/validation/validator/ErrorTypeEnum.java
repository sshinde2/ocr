/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

/**
 * @author Benoit Vanalderweireldt
 * 
 */
public enum ErrorTypeEnum {
    empty, size, sizeRangeSpecCara, sizeRangeSpaceLetters, select, sizeRangeSpecCaraSlash, sizeRangeSpecCaraPhone, sizeRangeFormat, sizeFormat, sizeRange, sizeMore, insecure, match, format, notAvailable, sizeRangeDigits, characters
}

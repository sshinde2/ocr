/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;


import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.pages.checkout.AbstractCheckoutController.SelectOption;
import au.com.target.tgtstorefront.forms.validation.ExpectedGender;



/**
 * @author bhuang3
 * 
 */
public class ExpectedGenderValidator extends AbstractTargetValidator implements
        ConstraintValidator<ExpectedGender, String> {

    @Override
    public void initialize(final ExpectedGender arg0) {
        field = FieldTypeEnum.expectedGender;
        isMandatory = arg0.mandatory();
        isSizeRange = false;
        mustMatch = false;
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }

    @Override
    protected boolean isAvailable(final String value) {
        final List<SelectOption> expectedGenders = WebConstants.UserInformation.EXPECTED_BABY_GENDERS;
        for (final SelectOption expectedGender : expectedGenders) {
            if (expectedGender.getCode().equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }
}

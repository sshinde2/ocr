/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import org.apache.commons.lang.BooleanUtils;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public enum FieldTypeEnum {
    firstName(true), lastName(true), fullName(true), password(true), email(false), addressLine1(false), addressLine2(
            false),
    citySuburb(true), state(true), postCode(true), phone(true), mobilePhone(true), title(true), store(true),
    flyBuys(true), teamMemberCode(true), cardId(true), securityNumber(true), nameOnCard(true), cardExpiryMonth(false),
    cardExpiryYear(false), cardNumber(true), country(true), username(true), donationPreferredContactMethod(true),
    organisationName(true), abnOrAcn(true), contactName(true),
    organisationStreet(true), organisationCity(true), organisationState(true), organisationPostCode(true),
    organisationCountry(true), reason(true), eventName(true), role(true), notes(true), gender(true), threePartDate(true), babyGender(
            true), expectedGender(true), messageText(true),
    recipientFirstName(true), recipientLastName(true), recipientEmail(true);

    /**
     * Does the word (field name) begin with a consonant sound ? Used to select correct article.
     */
    private Boolean isConsonant;

    private FieldTypeEnum(final boolean isConsonant) {
        this.isConsonant = Boolean.valueOf(isConsonant);
    }

    /**
     * @return the consonant
     */
    public Boolean getConsonant() {
        return isConsonant;
    }

    /**
     * 
     * @return 1 if the enum is a consonant or 0 if not
     */
    public Integer isConsonant() {
        return BooleanUtils.toIntegerObject(isConsonant);
    }
}

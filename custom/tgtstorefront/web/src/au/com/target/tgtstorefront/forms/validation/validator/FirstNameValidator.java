/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtstorefront.forms.validation.FirstName;
import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class FirstNameValidator extends AbstractTargetValidator implements ConstraintValidator<FirstName, String> {

    @Override
    public void initialize(final FirstName arg0) {
        field = arg0.field();
        sizeRange = new int[] { TargetValidationCommon.Name.MIN_SIZE, TargetValidationCommon.Name.MAX_SIZE };
        isMandatory = arg0.mandatory();
        isSizeRange = true;
        mustMatch = true;
        loadPattern(TargetValidationCommon.FirstName.class);
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }

    @Override
    protected String getInvalidPatternMessage() {
        return getInvalidSizeRangeSpecCara(field, sizeRange[0], sizeRange[1]);
    }
}

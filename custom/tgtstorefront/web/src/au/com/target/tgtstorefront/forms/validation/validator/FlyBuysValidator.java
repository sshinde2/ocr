/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtcore.order.TargetCommerceCheckoutService;
import au.com.target.tgtstorefront.forms.validation.FlyBuys;


/**
 * @author asingh78
 * 
 */
public class FlyBuysValidator extends AbstractTargetValidator implements ConstraintValidator<FlyBuys, String> {

    @Resource(name = "targetCommerceCheckoutService")
    private TargetCommerceCheckoutService targetCommerceCheckoutService;


    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
     */
    @Override
    public void initialize(final FlyBuys arg0) {
        field = FieldTypeEnum.flyBuys;

    }

    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
     */
    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }

    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
     */
    @Override
    public boolean isAvailable(final String flybuys) {
        return targetCommerceCheckoutService.isFlybuysNumberValid(flybuys);
    }

}

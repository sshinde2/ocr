/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtstorefront.forms.validation.FullName;
import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * @author mjanarth
 *
 */
public class FullNameValidator extends AbstractTargetValidator implements ConstraintValidator<FullName, String> {
    @Override
    public void initialize(final FullName arg0) {
        field = arg0.field();
        sizeRange = new int[] { TargetValidationCommon.Name.MIN_SIZE, TargetValidationCommon.Name.FULLNAME_MAX_SIZE };
        isMandatory = arg0.mandatory();
        isSizeRange = true;
        mustMatch = true;
        loadPattern(TargetValidationCommon.FullName.class);
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }

    @Override
    protected String getInvalidPatternMessage() {
        return getInvalidSizeRangeSpecCara(field, sizeRange[0], sizeRange[1]);
    }
}

package au.com.target.tgtstorefront.forms.validation.validator;


import de.hybris.platform.servicelayer.user.UserService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import au.com.target.tgtstorefront.forms.GuestForm;


/**
 * Validates entries on Guest user forms.
 */
@Component("guestValidator")
public class GuestValidator implements Validator
{


    @Autowired
    private UserService userService;

    @Override
    public boolean supports(final Class<?> aClass)
    {
        return au.com.target.tgtstorefront.forms.GuestForm.class.equals(aClass);
    }

    @Override
    public void validate(final Object object, final Errors errors)
    {
        final GuestForm guestForm = (GuestForm)object;
        final String email = guestForm.getEmail();
        if (StringUtils.isEmpty(email))
        {
            errors.rejectValue("email", "profile.email.invalid");
        }

        else if (userService.isUserExisting(email)) {
            errors.rejectValue("email", "user.exists");
        }
    }



}

/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtstorefront.forms.validation.InternationalPostCode;
import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * @author asingh78
 * 
 */
public class InternationalPostCodeValidator extends AbstractTargetValidator implements
        ConstraintValidator<InternationalPostCode, String> {
    @Override
    public void initialize(final InternationalPostCode arg0) {
        field = FieldTypeEnum.postCode;
        sizeRange = new int[] { TargetValidationCommon.InternationalPostCode.MIN_SIZE,
                TargetValidationCommon.InternationalPostCode.MAX_SIZE };
        isMandatory = true;
        isSizeRange = true;
        mustMatch = true;
        loadPattern(TargetValidationCommon.InternationalPostCode.class);
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        final String trimValue = value.trim();
        return isValidCommon(trimValue, context);
    }
}

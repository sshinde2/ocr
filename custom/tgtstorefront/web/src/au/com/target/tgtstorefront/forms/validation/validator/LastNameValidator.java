/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtstorefront.forms.validation.LastName;
import au.com.target.tgtutility.util.TargetValidationCommon;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class LastNameValidator extends AbstractTargetValidator implements ConstraintValidator<LastName, String> {

    @Override
    public void initialize(final LastName arg0) {
        field = arg0.field();
        sizeRange = new int[] { TargetValidationCommon.Name.MIN_SIZE, TargetValidationCommon.Name.MAX_SIZE };
        isSizeRange = true;
        isMandatory = arg0.mandatory();
        mustMatch = true;
        loadPattern(TargetValidationCommon.LastName.class);
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }

    @Override
    protected String getInvalidPatternMessage() {
        return getInvalidSizeRangeSpecCara(field, sizeRange[0], sizeRange[1]);
    }

}

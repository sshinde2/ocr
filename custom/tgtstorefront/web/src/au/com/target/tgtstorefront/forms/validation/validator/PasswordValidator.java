/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtstorefront.forms.validation.Password;
import au.com.target.tgtutility.util.TargetValidationCommon;



/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class PasswordValidator extends AbstractTargetValidator implements ConstraintValidator<Password, String> {

    @Resource(name = "targetCustomerFacade")
    private TargetCustomerFacade targetCustomerFacade;

    private boolean checkBlacklist;

    @Override
    public void initialize(final Password arg0) {
        field = FieldTypeEnum.password;
        sizeRange = new int[] { TargetValidationCommon.Password.MIN_SIZE, TargetValidationCommon.Password.MAX_SIZE };
        isSizeRange = true;
        isMandatory = true;
        mustMatch = true;
        checkBlacklist = arg0.checkBlacklist();
        loadPattern(TargetValidationCommon.Password.class);
    }

    @Override
    public boolean isValid(final String password, final ConstraintValidatorContext context) {
        boolean valid = isValidCommon(password, context);
        if (valid && checkBlacklist) {
            valid = isNotBlackListedPassword(password, context);
        }
        return valid;
    }

    /**
     * @param password
     * @param context
     * @return true if the given password
     */
    private boolean isNotBlackListedPassword(final String password, final ConstraintValidatorContext context) {
        if (targetCustomerFacade.isPasswordBlacklisted(password)) {
            updatedContext(context, getInvalidInsecure(FieldTypeEnum.password));
            return false;
        }
        return true;
    }

    @Override
    protected String getInvalidPatternMessage() {
        return getInvalidCharacters(field, "~ or spaces");
    }
}

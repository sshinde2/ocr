/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtstorefront.forms.validation.PaymentCardExpiryMonth;


/**
 * @author asingh78
 * 
 */
public class PaymentCardExpiryMonthValidator extends AbstractTargetValidator implements
        ConstraintValidator<PaymentCardExpiryMonth, String> {

    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
     */
    @Override
    public void initialize(final PaymentCardExpiryMonth paymentCardExpiryMonth) {
        field = FieldTypeEnum.cardExpiryMonth;
        isMandatory = true;

    }

    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
     */
    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }

}

/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtstorefront.forms.validation.PaymentCardNumber;


/**
 * @author asingh78
 * 
 */
public class PaymentCardNumberValidator extends AbstractTargetValidator implements
        ConstraintValidator<PaymentCardNumber, String> {

    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
     */
    @Override
    public void initialize(final PaymentCardNumber paymentCardNumber) {
        field = FieldTypeEnum.cardNumber;
        isMandatory = true;
    }

    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
     */
    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        if (isValidCommon(value, context)) {
            return isValidCardNumber(value, context);
        }
        return true;
    }


    protected boolean isValidCardNumber(final String value, final ConstraintValidatorContext context) {
        if (null != value && value.length() > 2) {
            return true;
        }
        updatedContext(context, getInvalidEmpty(field));
        return false;
    }

}

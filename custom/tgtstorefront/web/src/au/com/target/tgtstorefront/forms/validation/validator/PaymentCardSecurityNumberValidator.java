/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtstorefront.forms.validation.PaymentCardSecurityNumber;


/**
 * @author asingh78
 * 
 */
public class PaymentCardSecurityNumberValidator extends AbstractTargetValidator implements
        ConstraintValidator<PaymentCardSecurityNumber, String> {



    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
     */
    @Override
    public void initialize(final PaymentCardSecurityNumber paymentCardSecurityNumber) {
        field = FieldTypeEnum.securityNumber;
        isMandatory = true;

    }

    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
     */
    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }

}

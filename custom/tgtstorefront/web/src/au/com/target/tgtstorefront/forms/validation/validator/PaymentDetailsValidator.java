/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import de.hybris.platform.servicelayer.i18n.I18NService;

import javax.annotation.Resource;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.forms.checkout.NewCardPaymentDetailsForm;


@Component("paymentDetailsValidator")
public class PaymentDetailsValidator implements Validator
{

    @Resource(name = "messageSource")
    protected MessageSource messageSource;

    @Resource(name = "i18nService")
    protected I18NService i18nService;

    /* (non-Javadoc)
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(final Class<?> aClass) {
        return NewCardPaymentDetailsForm.class.equals(aClass);

    }

    /* (non-Javadoc)
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(final Object object, final Errors errors)
    {
        final NewCardPaymentDetailsForm form = (NewCardPaymentDetailsForm)object;

        final String cardBrandValue = form.getGatewayCardScheme();
        final boolean requiredSecurityCode = !WebConstants.CARD_TYPE_DINERS.equals(cardBrandValue);
        final String tnsFormResponse = form.getGatewayFormResponse();
        final WebConstants.Payment.HostedFormStatus status = getStatus(tnsFormResponse);
        // This CV2 has been validated here because of the test card details the TNS was returning valid response for a empty CV2 filed.
        if (WebConstants.Payment.HostedFormStatus.Valid.equals(status)) {

            // Checking it for the CV2 number provided in the form
            if (StringUtils.isNotBlank(cardBrandValue)
                    && requiredSecurityCode) {
                if (WebConstants.isValidCardType(cardBrandValue)) {
                    if (StringUtils.isBlank(form.getGatewayCardSecurityCode()))
                    {
                        addError(form, errors, true);
                    }
                }
                else {
                    addError(form, errors, true);
                }
            }
        }
        else if (WebConstants.Payment.HostedFormStatus.SessionInvalid.equals(status)
                || WebConstants.Payment.HostedFormStatus.SystemError.equals(status)) {

            addError(form, errors, requiredSecurityCode);

        }
        else if (WebConstants.Payment.HostedFormStatus.FieldErrors.equals(status)) {

            addError(form, errors, requiredSecurityCode);

        }

    }

    private void addError(final NewCardPaymentDetailsForm form, final Errors errors, final boolean cvvRequired) {
        if (cvvRequired && StringUtils.isBlank(form.getGatewayCardSecurityCode())) {
            errors.rejectValue(
                    "gatewayCardSecurityCode",
                    "field.invalid.empty",
                    new Object[] { BooleanUtils.toIntegerObject(FieldTypeEnum.securityNumber.getConsonant()),
                            messageSource.getMessage("field.securityNumber", null,
                                    i18nService.getCurrentLocale()) }, "field.invalid.empty");
        }

        else if (cvvRequired) {
            errors.rejectValue("gatewayCardSecurityCode", "field.re-enter",
                    new Object[] { messageSource.getMessage("field.securityNumber", null,
                            i18nService.getCurrentLocale()) }, "field.invalid.empty");
            // errors.rejectValue("gatewayCardSecurityCode", "payment.card.number.reenter.message");
        }
        if (StringUtils.isNotBlank(form.getGatewayCardNumber()) && form.getGatewayCardNumber().length() > 2) {
            errors.rejectValue(
                    "gatewayCardNumber",
                    "field.re-enter",
                    new Object[] { messageSource.getMessage("field.cardNumber", null,
                            i18nService.getCurrentLocale()) }, "field.cardNumber");
        }
    }

    private WebConstants.Payment.HostedFormStatus getStatus(final String tnsFormResponse) {
        /*
         * 0~OK Indicates the form is valid - returned when no form errors are found. 2~Form Session Id invalid or closed
         * for updates Indicates invalid form session - returned when Form Session is missing, expired, closed, or
         * invalid. 3~Field Errors Indicates field errors - returned if any form field fails validation. 4~System Error
         * Indicates system error - returned if a Payment Gateway error or other failure occurs.
         */

        if (tnsFormResponse != null) {

            if (tnsFormResponse.contains(WebConstants.Payment.VALID)) {
                return WebConstants.Payment.HostedFormStatus.Valid;
            }
            else if (tnsFormResponse.contains(WebConstants.Payment.SESSION_INVALID)) {
                return WebConstants.Payment.HostedFormStatus.SessionInvalid;
            }
            else if (tnsFormResponse.contains(WebConstants.Payment.FIELDERRORS)) {
                return WebConstants.Payment.HostedFormStatus.FieldErrors;
            }
        }

        return WebConstants.Payment.HostedFormStatus.SystemError;
    }
}

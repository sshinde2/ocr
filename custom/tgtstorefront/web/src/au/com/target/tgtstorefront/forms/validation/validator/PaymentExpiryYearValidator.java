/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtstorefront.forms.validation.PaymentCardExpiryYear;


/**
 * @author asingh78
 * 
 */
public class PaymentExpiryYearValidator extends AbstractTargetValidator implements
        ConstraintValidator<PaymentCardExpiryYear, String> {

    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
     */
    @Override
    public void initialize(final PaymentCardExpiryYear paymentCardExpiryYear) {
        field = FieldTypeEnum.cardExpiryYear;
        isMandatory = true;

    }

    /* (non-Javadoc)
     * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
     */
    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }
}

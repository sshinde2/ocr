/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import java.util.List;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.delivery.TargetDeliveryFacade;
import au.com.target.tgtfacades.delivery.data.TargetRegionData;
import au.com.target.tgtstorefront.forms.validation.State;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class StateValidator extends AbstractTargetValidator implements ConstraintValidator<State, String> {

    @Resource(name = "targetDeliveryFacade")
    private TargetDeliveryFacade targetDeliveryFacade;

    @Override
    public void initialize(final State arg0) {
        field = FieldTypeEnum.state;
        isMandatory = true;
        isSizeRange = false;
        mustMatch = false;
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }

    @Override
    protected boolean isAvailable(final String value) {
        final List<TargetRegionData> states = targetDeliveryFacade
                .getRegionsForCountry(TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);
        for (final TargetRegionData targetRegionData : states) {
            if (targetRegionData.getAbbreviation().equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }
}

/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtstorefront.forms.validation.Store;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class StoreValidator extends AbstractTargetValidator implements ConstraintValidator<Store, String> {

    private boolean checkClickAndCollectAvailable;

    private Integer storeNumber;

    @Resource(name = "targetStoreLocatorFacade")
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Override
    public void initialize(final Store arg0) {
        field = FieldTypeEnum.store;
        isMandatory = true;
        isSizeRange = false;
        mustMatch = false;
        checkClickAndCollectAvailable = arg0.checkClickAndCollectAvailable();
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return isValidCommon(value, context);
    }

    @Override
    protected boolean isType(final String value) {
        if (StringUtils.isBlank(value)) {
            return false;
        }
        try {
            storeNumber = Integer.valueOf(value);
        }
        catch (final Exception e) {
            return false;
        }
        return true;
    }

    @Override
    protected boolean isAvailable(final String value) {
        if (storeNumber == null) {
            return false;
        }
        final TargetPointOfServiceData pointOfService = targetStoreLocatorFacade.getPointOfService(storeNumber);
        if (pointOfService == null) {
            return false;
        }
        if (checkClickAndCollectAvailable) {
            return pointOfService.getAcceptCNC().booleanValue();
        }
        return true;
    }
}

/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import au.com.target.tgtstorefront.forms.validation.ThreePartDate;


/**
 * Validator for a three part date (day, month, year)
 * 
 */
public class ThreePartDateValidator extends AbstractTargetValidator implements
        ConstraintValidator<ThreePartDate, Object> {

    private static final Logger LOG = Logger.getLogger(ThreePartDateValidator.class);

    private String day;
    private String month;
    private String year;
    private boolean isMandatory = true;

    @Override
    public void initialize(final ThreePartDate constraintAnnotation) {

        Assert.notEmpty(constraintAnnotation.value());
        Assert.isTrue(constraintAnnotation.value().length == 3);
        day = constraintAnnotation.value()[0];
        month = constraintAnnotation.value()[1];
        year = constraintAnnotation.value()[2];
        isMandatory = constraintAnnotation.mandatory();
        field = FieldTypeEnum.threePartDate;
    }

    @Override
    public boolean isValid(final Object object, final ConstraintValidatorContext constraintContext) {

        if (object == null) {
            return !isMandatory;
        }

        try {
            final Object objDay = PropertyUtils.getProperty(object, day);
            final Object objMonth = PropertyUtils.getProperty(object, month);
            final Object objYear = PropertyUtils.getProperty(object, year);

            return isDateValid(objDay, objMonth, objYear);
        }
        catch (final Exception e) {
            LOG.error("Could not validate", e);
            throw new IllegalArgumentException(e);
        }
    }


    /**
     * Check if the given object date parts constitute a valid date.
     * 
     * @param objDay
     * @param objMonth
     * @param objYear
     * @return true or false
     */
    protected boolean isDateValid(final Object objDay, final Object objMonth, final Object objYear) {

        if (!isMandatory && objDay == null && objMonth == null && objYear == null) {
            return true;
        }

        if (objDay == null || objMonth == null || objYear == null) {
            return false;
        }

        if (!(objDay instanceof String) || !(objMonth instanceof String) || !(objYear instanceof String)) {
            return false;
        }

        int iDay = 0;
        int iMonth = 0;
        int iYear = 0;

        try {
            iDay = Integer.parseInt((String)objDay);
            iMonth = Integer.parseInt((String)objMonth);
            iYear = Integer.parseInt((String)objYear);

        }
        catch (final NumberFormatException e) {

            return false;
        }

        return isDateValid(iDay, iMonth, iYear);
    }


    /**
     * Check if the given integer date parts constitute a valid date.
     * 
     * @param iDay
     * @param iMonth
     * @param iYear
     * @return true or false
     */
    private boolean isDateValid(final int iDay, final int iMonth, final int iYear) {

        try {
            final Calendar cal = new GregorianCalendar();
            cal.setLenient(false);
            cal.set(iYear, iMonth - 1, iDay);
            //this will throw an exception if the date is not valid:
            cal.getTime();
        }
        catch (final Exception e) {
            return false;
        }

        return true;
    }
}

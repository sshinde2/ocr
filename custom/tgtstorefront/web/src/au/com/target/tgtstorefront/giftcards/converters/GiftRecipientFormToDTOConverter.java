/**
 * 
 */
package au.com.target.tgtstorefront.giftcards.converters;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.converters.TargetConverter;
import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;
import au.com.target.tgtstorefront.forms.GiftRecipientForm;


/**
 * Convert GiftRecipientForm to GiftRecipientDTO
 * 
 * @author jjayawa1
 *
 */
public class GiftRecipientFormToDTOConverter implements TargetConverter<GiftRecipientForm, GiftRecipientDTO> {

    @Override
    public GiftRecipientDTO convert(final GiftRecipientForm form) {
        final GiftRecipientDTO giftRecipientDTO = new GiftRecipientDTO();

        if (StringUtils.isNotEmpty(form.getFirstName())) {
            giftRecipientDTO.setFirstName(form.getFirstName());
        }

        if (StringUtils.isNotEmpty(form.getLastName())) {
            giftRecipientDTO.setLastName(form.getLastName());
        }

        if (StringUtils.isNotEmpty(form.getRecipientEmailAddress())) {
            giftRecipientDTO.setRecipientEmailAddress(form.getRecipientEmailAddress());
        }

        if (StringUtils.isNotEmpty(form.getMessageText())) {
            giftRecipientDTO.setMessageText(form.getMessageText());
        }

        return giftRecipientDTO;
    }

}

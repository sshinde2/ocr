/**
 * 
 */
package au.com.target.tgtstorefront.interceptors.beforecontroller;

import de.hybris.platform.util.Base64;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtstorefront.controllers.ControllerConstants;



/**
 * Abstract implementation of BeforeControllerHandler to hold common methods.
 * 
 * @author jjayawa1
 *
 */
public abstract class AbstractBeforeControllerHandler implements BeforeControllerHandler {

    private static final String WS_API_CHECKOUT_PREFIX = "/ws-api/v1/target/checkout";

    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;
    private SalesApplicationFacade salesApplicationFacade;

    /**
     * Returns a redirect URL for the given request.
     * 
     * @param request
     * @return redirect URL
     */
    protected String getRedirectUrl(final HttpServletRequest request) {
        if (request != null) {
            if (request.getServletPath().startsWith(WS_API_CHECKOUT_PREFIX)) {
                return WS_API_CHECKOUT_PREFIX + "/login";
            }
            if (request.getServletPath().contains("checkout")) {
                final String errorMessageKey = request.getParameter(ControllerConstants.ERROR);

                final String redirectUrl;
                if (isLegacyCheckoutLogin()) {
                    redirectUrl = ControllerConstants.CHECKOUT_LOGIN;
                }
                else {
                    redirectUrl = checkAfterpayReturn(request);
                }
                return redirectUrl + (StringUtils.isNotEmpty(errorMessageKey)
                        ? "?" + ControllerConstants.ERROR + "=" + errorMessageKey : "");
            }
        }
        return ControllerConstants.LOGIN;
    }


    private boolean isLegacyCheckoutLogin() {
        if (getSalesApplicationFacade().isKioskApplication()) {
            return true;
        }
        return !getTargetFeatureSwitchFacade().isSpcLoginAndCheckout();
    }

    /**
     * @param request
     * @return redirectUrl
     */
    private String checkAfterpayReturn(final HttpServletRequest request) {
        final String redirectUrl;
        if (request.getServletPath().contains(ControllerConstants.PAYMENT_AFTERPAY_RETURN)) {
            redirectUrl = ControllerConstants.SINGLE_PAGE_CHECKOUT_AND_LOGIN_PLACE_ORDER_ERROR
                    + Base64.encodeBytes("AFTERPAY_SESSION_TIMEOUT".getBytes());
        }
        else {
            redirectUrl = ControllerConstants.SINGLE_PAGE_CHECKOUT_AND_LOGIN;
        }
        return redirectUrl;
    }

    /**
     * @return the targetFeatureSwitchFacade
     */
    protected TargetFeatureSwitchFacade getTargetFeatureSwitchFacade() {
        return targetFeatureSwitchFacade;
    }

    /**
     * @param targetFeatureSwitchFacade
     *            the targetFeatureSwitchFacade to set
     */
    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }

    /**
     * @return the salesApplicationFacade
     */
    protected SalesApplicationFacade getSalesApplicationFacade() {
        return salesApplicationFacade;
    }

    /**
     * @param salesApplicationFacade
     *            the salesApplicationFacade to set
     */
    @Required
    public void setSalesApplicationFacade(final SalesApplicationFacade salesApplicationFacade) {
        this.salesApplicationFacade = salesApplicationFacade;
    }
}

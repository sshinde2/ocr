/**
 * 
 */
package au.com.target.tgtstorefront.interceptors.beforecontroller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import au.com.target.tgtstorefront.util.CSRFTokenManager;
import au.com.target.tgtutility.util.ExcludeHttpRequestPathHelper;


/**
 * @author rmcalave
 * 
 */
@Deprecated
public class CSRFHandlerInterceptor extends HandlerInterceptorAdapter {

    private static final Logger LOG = Logger.getLogger(CSRFHandlerInterceptor.class);

    private List<String> excludedPaths;

    /* (non-Javadoc)
     * @see org.springframework.web.servlet.handler.HandlerInterceptorAdapter#preHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object)
     */
    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler)
            throws Exception {

        if (request.isSecure() && HttpMethod.POST.toString().equalsIgnoreCase(request.getMethod())
                && !ExcludeHttpRequestPathHelper.isExcluded(request, excludedPaths)) {
            // This is a POST request - need to check the CSRF token
            final String sessionToken = CSRFTokenManager.getTokenForSession(request.getSession());
            final String requestToken = CSRFTokenManager.getTokenFromRequest(request);
            if (!sessionToken.equals(requestToken)) {
                LOG.warn("CSRF Attempt!");
                LOG.warn("Tokens provided for post to " + request.getServletPath() + " do not match.");
                LOG.warn("\tExpected token: " + sessionToken);
                LOG.warn("\tProvided token: " + requestToken);
                LOG.warn("This may indicate a CSRF attempt or a misconfigured form.");
                LOG.warn("If this is a new form make sure that the CSRF token has been added.");

                response.sendError(HttpServletResponse.SC_FORBIDDEN);
                return false;
            }
        }

        return true;
    }


    /**
     * @param excludedPaths
     *            the excludedPaths to set
     */
    public void setExcludedPaths(final List<String> excludedPaths) {
        this.excludedPaths = excludedPaths;
    }
}

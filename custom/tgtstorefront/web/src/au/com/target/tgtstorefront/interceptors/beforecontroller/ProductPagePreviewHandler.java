package au.com.target.tgtstorefront.interceptors.beforecontroller;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import au.com.target.tgtcore.constants.TgtCoreConstants.Catalog;


/***
 * 
 * Enables to view the staged version of a product. This is achieved by setting a user whose has access to the staged
 * product CV and the CV itself into hybris session
 * 
 */
public class ProductPagePreviewHandler extends HandlerInterceptorAdapter {

    private UserService userService;
    private SessionService sessionService;
    private CatalogVersionService catalogVersionService;
    private SearchRestrictionService searchRestrictionService;
    private CatalogVersionModel stagedProductCatalogVersion;
    private String productPagePreviewUser;
    private String stagedProductPagePreviewQueryString;
    private boolean enabled;

    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler)
            throws Exception {

        if (!enabled || !isStagedProductPreview(request)) {
            return true;
        }

        userService.setCurrentUser(userService.getUserForUID(productPagePreviewUser));
        catalogVersionService.addSessionCatalogVersion(getStagedProductCatalogVersion());
        return true;
    }

    @Override
    public void postHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler,
            final ModelAndView modelAndView) throws Exception {

        if (!enabled || !isStagedProductPreview(request)) {
            return;
        }

        restoreSessionUser();
        removeStagedProductCVFromSession();
    }

    private void removeStagedProductCVFromSession() {
        final Set<CatalogVersionModel> sessionCatalogVersions = new HashSet<CatalogVersionModel>(
                catalogVersionService.getSessionCatalogVersions());
        sessionCatalogVersions.remove(getStagedProductCatalogVersion());
        catalogVersionService.setSessionCatalogVersions(sessionCatalogVersions);
    }

    /**
     * Restore the correct user from Spring security context
     */
    private void restoreSessionUser() {
        final String userId = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            final UserModel userModel = userService.getUserForUID(userId);
            userService.setCurrentUser(userModel);
        }
        catch (final UnknownIdentifierException uie) {
            userService.setCurrentUser(userService.getAnonymousUser());
        }
    }

    private CatalogVersionModel getStagedProductCatalogVersion() {
        if (stagedProductCatalogVersion == null) {
            stagedProductCatalogVersion = sessionService.executeInLocalView(new SessionExecutionBody()
            {
                @Override
                public Object execute()
                {
                    try
                    {
                        searchRestrictionService.disableSearchRestrictions();
                        return catalogVersionService.getCatalogVersion(Catalog.PRODUCTS, Catalog.OFFLINE_VERSION);
                    }
                    finally
                    {
                        searchRestrictionService.enableSearchRestrictions();
                    }
                }

            });
        }
        return stagedProductCatalogVersion;
    }

    private boolean isStagedProductPreview(final HttpServletRequest request) {
        final String queryString = request.getQueryString();
        return StringUtils.contains(queryString, stagedProductPagePreviewQueryString);
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @param searchRestrictionService
     *            the searchRestrictionService to set
     */
    @Required
    public void setSearchRestrictionService(final SearchRestrictionService searchRestrictionService) {
        this.searchRestrictionService = searchRestrictionService;
    }

    /**
     * @param sessionService
     *            the sessionService to set
     */
    @Required
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

    /**
     * @param productPagePreviewUser
     *            the productPagePreviewUser to set
     */
    @Required
    public void setProductPagePreviewUser(final String productPagePreviewUser) {
        this.productPagePreviewUser = productPagePreviewUser;
    }

    /**
     * @param stagedProductPagePreviewQueryString
     *            the stagedProductPagePreviewQueryString to set
     */
    @Required
    public void setStagedProductPagePreviewQueryString(final String stagedProductPagePreviewQueryString) {
        this.stagedProductPagePreviewQueryString = stagedProductPagePreviewQueryString;
    }

    /**
     * @param enabled
     *            the enabled to set
     */
    @Required
    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }

}

/**
 * 
 */
package au.com.target.tgtstorefront.interceptors.beforecontroller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.method.HandlerMethod;

import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;


/**
 * Sets attributes related to sales application before passing to controllers.
 * 
 * @author jjayawa1
 * 
 */
public class SalesApplicationBeforeControllerHandler implements BeforeControllerHandler {

    @Resource(name = "salesApplicationFacade")
    private SalesApplicationFacade salesApplicationFacade;

    /* (non-Javadoc)
     * @see au.com.target.tgtstorefront.interceptors.beforecontroller.BeforeControllerHandler#beforeController(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public boolean beforeController(final HttpServletRequest request, final HttpServletResponse response,
            final HandlerMethod handler) {
        salesApplicationFacade.initializeRequest(request);
        return true;
    }

}

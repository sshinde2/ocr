package au.com.target.tgtstorefront.interceptors.beforeview;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.configuration.ConversionException;
import org.springframework.web.servlet.ModelAndView;

import au.com.target.tgtstorefront.constants.WebConstants;


public class DevelopmentModeBeforeViewHandler implements BeforeViewHandler {
    private Boolean isAssetModeDevelop;

    public DevelopmentModeBeforeViewHandler(final ConfigurationService configurationService) {
        try {
            isAssetModeDevelop = configurationService.getConfiguration().getBoolean(
                    WebConstants.STOREFRONT_ASSETMODE_DEVELOP_CONFIG_KEY, Boolean.FALSE);
        }
        catch (final ConversionException ex) {
            isAssetModeDevelop = Boolean.FALSE;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * au.com.target.tgtstorefront.interceptors.beforeview.BeforeViewHandler#beforeView(javax.servlet.http.HttpServletRequest
     * , javax.servlet.http.HttpServletResponse, org.springframework.web.servlet.ModelAndView)
     */
    @Override
    public void beforeView(final HttpServletRequest request, final HttpServletResponse response,
            final ModelAndView modelAndView) {
        request.setAttribute(WebConstants.STOREFRONT_ASSETMODE_DEVELOP_REQUEST_KEY, isAssetModeDevelop);
    }
}

/**
 * 
 */
package au.com.target.tgtstorefront.interceptors.beforeview;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtutility.util.JsonConversionUtil;


public class FeatureSwitchBeforeViewHandler implements BeforeViewHandler {

    public static final String FEATURES_ENABLED = "featuresEnabled";
    public static final String UI_FEATURES_ENABLED = "uiFeaturesEnabled";

    @Resource(name = "targetFeatureSwitchFacade")
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Override
    public void beforeView(final HttpServletRequest request, final HttpServletResponse response,
            final ModelAndView modelAndView) {
        modelAndView.addObject(FEATURES_ENABLED,
                targetFeatureSwitchFacade.getEnabledFeatureMap(false));
        modelAndView.addObject(UI_FEATURES_ENABLED, JsonConversionUtil.convertToJsonString(
                targetFeatureSwitchFacade.getEnabledFeatureMap(true)));
    }

}

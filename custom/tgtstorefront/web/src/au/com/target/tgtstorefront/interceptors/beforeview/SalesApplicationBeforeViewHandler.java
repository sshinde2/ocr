/**
 * 
 */
package au.com.target.tgtstorefront.interceptors.beforeview;

import de.hybris.platform.commerceservices.enums.SalesApplication;

import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.tuckey.web.filters.urlrewrite.utils.StringUtils;

import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtstorefront.constants.WebConstants;


/**
 * @author gbaker2
 * 
 */
public class SalesApplicationBeforeViewHandler implements BeforeViewHandler {

    private static final Logger LOG = Logger.getLogger(SalesApplicationBeforeViewHandler.class);

    private static final String SUFFIX = "Mode";

    private static final String MOBILE_APP_MODE = "mobileApp".concat(SUFFIX);
    private static final String IOS_APP_MODE = "iosApp".concat(SUFFIX);
    private static final String ANDROID_APP_MODE = "androidApp".concat(SUFFIX);

    private static final Pattern ANDROID_WV_UA_PATTERN = Pattern.compile(".*Android.*");
    private static final Pattern IOS_WV_UA_PATTERN = Pattern.compile(".*(iPhone|iPad|iPod).*");

    @Resource(name = "salesApplicationFacade")
    private SalesApplicationFacade salesApplicationFacade;

    @Override
    public void beforeView(final HttpServletRequest request, final HttpServletResponse response,
            final ModelAndView modelAndView) {
        final SalesApplication currentSalesApplication = salesApplicationFacade.getCurrentSalesApplication();

        modelAndView.addObject(WebConstants.IS_SALES_ASSISTED, Boolean.FALSE);
        if (SalesApplication.KIOSK.equals(currentSalesApplication)) {
            modelAndView.addObject(
                    salesApplicationFacade.getCurrentSalesApplication().getCode().toLowerCase().concat(SUFFIX),
                    Boolean.TRUE);
        }
        else if (SalesApplication.MOBILEAPP.equals(currentSalesApplication)) {
            modelAndView.addObject(MOBILE_APP_MODE, Boolean.TRUE);

            final String userAgent = request.getHeader("User-Agent");
            if (StringUtils.isBlank(userAgent)) {
                LOG.warn("No User-Agent provided by mobile app client");
            }
            else if (IOS_WV_UA_PATTERN.matcher(userAgent).matches()) {
                modelAndView.addObject(IOS_APP_MODE, Boolean.TRUE);
            }
            else if (ANDROID_WV_UA_PATTERN.matcher(userAgent).matches()) {
                modelAndView.addObject(ANDROID_APP_MODE, Boolean.TRUE);
            }
            else {
                LOG.warn("Unknown User-Agent provided by mobile app client. User-Agent=" + userAgent);
            }
        }

    }
}

/**
 * 
 */
package au.com.target.tgtstorefront.listener;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationListener;
import org.springframework.security.web.session.HttpSessionDestroyedEvent;
import org.springframework.stereotype.Service;

import au.com.target.tgtfacades.checkout.flow.TargetCheckoutCustomerStrategy;


/**
 * @author bhuang3
 *
 */
@Service
public class TargetHttpSessionDestroyEventListener implements ApplicationListener<HttpSessionDestroyedEvent> {

    protected static final Logger LOG = Logger.getLogger(TargetHttpSessionDestroyEventListener.class);

    @Resource(name = "checkoutCustomerStrategy")
    private TargetCheckoutCustomerStrategy checkoutCustomerStrategy;

    @Override
    public void onApplicationEvent(final HttpSessionDestroyedEvent event) {
        final HttpSession httpSession = event.getSession();
        if (httpSession != null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Session is expired for session=" + httpSession.getId());
            }
            checkoutCustomerStrategy.resetGuestCartWithCurrentUser(httpSession);
        }

    }

}

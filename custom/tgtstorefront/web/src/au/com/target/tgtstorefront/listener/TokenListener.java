/**
 * 
 */
package au.com.target.tgtstorefront.listener;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import au.com.target.tgtstorefront.util.CSRFTokenManager;
import au.com.target.tgtstorefront.util.TargetSessionTokenUtil;


/**
 * @author rmcalave
 * 
 */
public class TokenListener implements HttpSessionListener {

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpSessionListener#sessionCreated(javax.servlet.http.HttpSessionEvent)
     */
    @Override
    public void sessionCreated(final HttpSessionEvent sessionEvent) {
        final HttpSession session = sessionEvent.getSession();
        session.setAttribute(TargetSessionTokenUtil.getTokenKey(), TargetSessionTokenUtil.nextToken());

        CSRFTokenManager.getTokenForSession(session);
    }

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpSessionListener#sessionDestroyed(javax.servlet.http.HttpSessionEvent)
     */
    @Override
    public void sessionDestroyed(final HttpSessionEvent sessionEvent) {
        // Do nothing
    }

}

/**
 * 
 */
package au.com.target.tgtstorefront.navigation;

import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtfacades.navigation.AbstractMenuBuilder;
import au.com.target.tgtfacades.navigation.NavigationMenuItem;


/**
 * @author rmcalave
 * 
 */
public class LeftNavigationMenuBuilder extends AbstractMenuBuilder {
    private static final Logger LOG = Logger.getLogger(LeftNavigationMenuBuilder.class);

    /**
     * Build the left navigation menu structure. Begins at the root node for the current page, includes the tree down to
     * the current page and includes all of the children of the current page.
     * 
     * @param currentPage
     *            The page the user is currently viewing.
     * @return The left navigation menu for the current page.
     */
    public NavigationMenuItem getLeftNavigationMenu(final ContentPageModel currentPage) {
        final List<CMSNavigationNodeModel> navNodesForPage = currentPage.getNavigationNodeList();
        if (CollectionUtils.isEmpty(navNodesForPage)) {
            LOG.error("No navigation nodes found for page, menu cannot be drawn");
            return null;
        }

        final CMSNavigationNodeModel rootNodeForPage = findRootNodeForSection(navNodesForPage.get(0));

        if (rootNodeForPage == null) {
            return null;
        }

        return createMenuItem(rootNodeForPage, currentPage);
    }

    private Set<NavigationMenuItem> createMenuItemsForChildren(final List<CMSNavigationNodeModel> navigationNodes,
            final ContentPageModel currentPage) {
        if (CollectionUtils.isEmpty(navigationNodes)) {
            return null;
        }

        final Set<NavigationMenuItem> menuItems = new LinkedHashSet<>();

        for (final CMSNavigationNodeModel navNode : navigationNodes) {
            if (!navNode.isVisible()) {
                continue;
            }

            final NavigationMenuItem menuItem = createMenuItem(navNode, currentPage);

            if (menuItem != null) {
                menuItems.add(menuItem);
            }
        }

        return menuItems;
    }

    private NavigationMenuItem createMenuItem(final CMSNavigationNodeModel navNode,
            final ContentPageModel currentPage) {
        final Set<NavigationMenuItem> childMenuItems = createMenuItemsForChildren(navNode.getChildren(),
                currentPage);

        final List<CMSNavigationEntryModel> entries = navNode.getEntries();

        NavigationMenuItem menuItem = null;

        if (CollectionUtils.isNotEmpty(entries)) {
            final CMSNavigationEntryModel entry = entries.get(0);

            menuItem = createNavigationMenuItem(entry, currentPage, childMenuItems, navNode.getTitle());
        }

        if (menuItem == null) {
            return null;
        }

        if (!menuItem.isCurrentPage() && !menuItem.isCurrentPageInChildren()) {
            menuItem.setChildren(null);
        }

        return menuItem;
    }

    private CMSNavigationNodeModel findRootNodeForSection(final CMSNavigationNodeModel navNodeForPage) {
        CMSNavigationNodeModel currentNavNode = navNodeForPage;
        final List<CMSNavigationNodeModel> navNodeTree = new ArrayList<>();
        while (currentNavNode != null) {
            navNodeTree.add(0, currentNavNode);
            currentNavNode = currentNavNode.getParent();
        }

        if (navNodeTree.size() < 3) {
            return null;
        }

        return navNodeTree.get(2);
    }
}

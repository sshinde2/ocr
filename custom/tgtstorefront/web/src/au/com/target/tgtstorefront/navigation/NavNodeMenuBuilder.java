/**
 * 
 */
package au.com.target.tgtstorefront.navigation;

import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtfacades.navigation.AbstractMenuBuilder;
import au.com.target.tgtfacades.navigation.NavigationMenuItem;


/**
 * @author mjanarth Created for adding links to subcategorygrid(used in NavNodeGridComponentController)
 * 
 */
public class NavNodeMenuBuilder extends AbstractMenuBuilder {


    public NavigationMenuItem createMenuItem(final CMSNavigationNodeModel navNode)
    {

        final List<CMSNavigationEntryModel> entries = navNode.getEntries();
        NavigationMenuItem menuItem = null;
        if (CollectionUtils.isNotEmpty(entries)) {
            final CMSNavigationEntryModel entry = entries.get(0);
            menuItem = createNavigationMenuItem(entry, null, null, navNode.getTitle());
        }
        return menuItem;
    }

}

/**
 * 
 */
package au.com.target.tgtstorefront.navigation;

import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtfacades.navigation.AbstractMenuBuilder;
import au.com.target.tgtfacades.navigation.NavigationMenuItem;


/**
 * @author rmcalave
 * 
 */
public class SiteMapMenuBuilder extends AbstractMenuBuilder {
    public Set<NavigationMenuItem> createMenuItemsForChildren(final List<CMSNavigationNodeModel> navigationNodes) {
        if (CollectionUtils.isEmpty(navigationNodes)) {
            return null;
        }

        final Set<NavigationMenuItem> menuItems = new LinkedHashSet<>();

        for (final CMSNavigationNodeModel navNode : navigationNodes) {
            if (!navNode.isVisible()) {
                continue;
            }


            // If the current navigation node has no entries skip over it and process the children instead.
            if (!isValidEntryTypeInNavigationNode(navNode)) {
                final Set<NavigationMenuItem> menuItemsForChildren = createMenuItemsForChildren(navNode.getChildren());

                if (CollectionUtils.isNotEmpty(menuItemsForChildren)) {
                    menuItems.addAll(menuItemsForChildren);
                }
            }
            else {
                final NavigationMenuItem menuItem = createMenuItem(navNode);

                if (menuItem != null) {
                    menuItems.add(menuItem);
                }
            }
        }

        return menuItems;
    }

    /**
     * Creates the menu item.
     * 
     * @param navNode
     *            the nav node
     * @return the navigation menu item
     */
    public NavigationMenuItem createMenuItem(final CMSNavigationNodeModel navNode) {
        if (navNode == null) {
            return null;
        }
        final Set<NavigationMenuItem> childMenuItems = createMenuItemsForChildren(navNode.getChildren());

        final List<CMSNavigationEntryModel> entries = navNode.getEntries();

        if (CollectionUtils.isNotEmpty(entries)) {
            for (final CMSNavigationEntryModel entry : entries) {
                final NavigationMenuItem menuItem = createNavigationMenuItem(entry, null, childMenuItems,
                        navNode.getName());

                if (menuItem != null) {
                    return menuItem;
                }
            }
        }


        return null;
    }

}

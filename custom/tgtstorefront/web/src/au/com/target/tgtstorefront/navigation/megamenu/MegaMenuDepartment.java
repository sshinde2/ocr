/**
 * 
 */
package au.com.target.tgtstorefront.navigation.megamenu;

import java.util.List;


/**
 * @author rmcalave
 * 
 */
public class MegaMenuDepartment {
    private String title;

    private String link;

    private List<MegaMenuColumn> columns;

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link
     *            the link to set
     */
    public void setLink(final String link) {
        this.link = link;
    }

    /**
     * @return the columns
     */
    public List<MegaMenuColumn> getColumns() {
        return columns;
    }

    /**
     * @param columns
     *            the columns to set
     */
    public void setColumns(final List<MegaMenuColumn> columns) {
        this.columns = columns;
    }
}

/**
 * 
 */
package au.com.target.tgtstorefront.navigation.subcategorygrid;

/**
 * @author rmcalave
 * 
 */
public class SubcategoryGridItem {
    private String title;

    private String linkUrl;

    private String thumbnailUrl;

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * @return the linkUrl
     */
    public String getLinkUrl() {
        return linkUrl;
    }

    /**
     * @param linkUrl
     *            the linkUrl to set
     */
    public void setLinkUrl(final String linkUrl) {
        this.linkUrl = linkUrl;
    }

    /**
     * @return the thumbnailUrl
     */
    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    /**
     * @param thumbnailUrl
     *            the thumbnailUrl to set
     */
    public void setThumbnailUrl(final String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }
}

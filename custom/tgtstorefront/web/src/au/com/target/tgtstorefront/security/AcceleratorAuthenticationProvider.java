/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.security;

import de.hybris.platform.spring.security.CoreAuthenticationProvider;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;


/**
 * Derived authentication provider supporting two additional authentication checks:
 * <ul>
 * <li>prevent login without password for users created via CSCockpit</li>
 * <li>prevent admin login since all SearchRestrictions are disabled and therefore no page can be viewed correctly</li>
 * </ul>
 */
public class AcceleratorAuthenticationProvider extends CoreAuthenticationProvider
{
    /**
     * @see de.hybris.platform.spring.security.CoreAuthenticationProvider#additionalAuthenticationChecks(org.springframework.security.core.userdetails.UserDetails,
     *      org.springframework.security.authentication.AbstractAuthenticationToken)
     */
    @Override
    protected void additionalAuthenticationChecks(final UserDetails details,
            final AbstractAuthenticationToken authentication)
            throws AuthenticationException
    {
        super.additionalAuthenticationChecks(details, authentication);

        // Check if user has supplied no password
        if (StringUtils.isEmpty((String)authentication.getCredentials()))
        {
            throw new BadCredentialsException("Login without password");
        }
    }
}

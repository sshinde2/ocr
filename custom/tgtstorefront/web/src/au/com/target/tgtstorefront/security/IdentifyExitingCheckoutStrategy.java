/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.security;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtutility.util.ExcludeHttpRequestPathHelper;


/**
 * Strategy for identifying whether user is leaving checkout
 */
public class IdentifyExitingCheckoutStrategy {

    private List<String> excludedPaths;

    /**
     * This method will identify whether a user has moved out of the checkout process.
     * 
     * @param request
     * @return whetherUserHasExitedCheckout
     */
    public boolean isUserMovingOutOfCheckout(final HttpServletRequest request) {
        //this checks whether the user is still in checkout based on a set of pre-defined paths.
        //this needs to be revisited during checkout re-design.
        return !ExcludeHttpRequestPathHelper.isExcluded(request, excludedPaths);
    }

    /**
     * @param excludedPaths
     *            the excludedPaths to set
     */
    @Required
    public void setExcludedPaths(final List<String> excludedPaths) {
        this.excludedPaths = excludedPaths;
    }

}

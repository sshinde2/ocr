/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.security;

import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

import au.com.target.tgtstorefront.cookie.TargetCookieStrategy;
import au.com.target.tgtstorefront.customersegment.impl.CustomerSegmentCookieStrategy;
import au.com.target.tgtstorefront.security.cookie.EnhancedCookieGenerator;


public class StorefrontLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler
{
    private static final Logger LOG = Logger.getLogger(StorefrontLogoutSuccessHandler.class);

    private static final String FORWARD_PARAMETER_NAME = "forward";

    private TargetCookieStrategy guidCookieStrategy;

    private CustomerSegmentCookieStrategy customerSegmentCookieStrategy;

    private CMSSiteService cmsSiteService;

    private EnhancedCookieGenerator preferredStoreCookieGenerator;

    // Map of alternative target urls
    private Map<String, String> alternateTargetUrls;


    protected TargetCookieStrategy getGuidCookieStrategy()
    {
        return guidCookieStrategy;
    }

    @Required
    public void setGuidCookieStrategy(final TargetCookieStrategy guidCookieStrategy)
    {
        this.guidCookieStrategy = guidCookieStrategy;
    }

    protected CMSSiteService getCmsSiteService()
    {
        return cmsSiteService;
    }

    @Required
    public void setCmsSiteService(final CMSSiteService cmsSiteService)
    {
        this.cmsSiteService = cmsSiteService;
    }

    @Override
    public void onLogoutSuccess(final HttpServletRequest request, final HttpServletResponse response,
            final Authentication authentication) throws IOException, ServletException
    {

        getGuidCookieStrategy().deleteCookie(request, response);
        //Remove the preferred store cookie
        preferredStoreCookieGenerator.removeCookie(response);
        customerSegmentCookieStrategy.deleteCookie(request, response);

        // Delegate to default redirect behaviour
        super.onLogoutSuccess(request, response, authentication);
    }

    /**
     * Override the default target url if a valid alternative is indicated by a key, <br/>
     * given as value of the FORWARD_PARAMETER_NAME parameter. <br/>
     * Lookup the alternate target url in the alternateTargetUrls map.
     * 
     * @param request
     * @param response
     */
    @Override
    protected String determineTargetUrl(final HttpServletRequest request, final HttpServletResponse response)
    {
        String targetUrl = getDefaultTargetUrl();

        final String altForward = request.getParameter(FORWARD_PARAMETER_NAME);

        if (!StringUtils.isEmpty(altForward)) {

            if (alternateTargetUrls.containsKey(altForward)) {

                targetUrl = alternateTargetUrls.get(altForward);
            }
            else {
                LOG.info("Unrecognised key supplied for alternateTargetUrls lookup: " + altForward);
            }
        }

        return targetUrl;
    }


    @Required
    public void setAlternateTargetUrls(final Map<String, String> alternateTargetUrls) {
        this.alternateTargetUrls = alternateTargetUrls;
    }

    /**
     * @param customerSegmentCookieStrategy
     *            the customerSegmentCookieStrategy to set
     */
    @Required
    public void setCustomerSegmentCookieStrategy(final CustomerSegmentCookieStrategy customerSegmentCookieStrategy) {
        this.customerSegmentCookieStrategy = customerSegmentCookieStrategy;
    }

    /**
     * @param preferredStoreCookieGenerator
     *            the preferredStoreCookieGenerator to set
     */
    @Required
    public void setPreferredStoreCookieGenerator(final EnhancedCookieGenerator preferredStoreCookieGenerator) {
        this.preferredStoreCookieGenerator = preferredStoreCookieGenerator;
    }


}

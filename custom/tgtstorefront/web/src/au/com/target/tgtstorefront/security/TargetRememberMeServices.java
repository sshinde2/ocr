/**
 * 
 */
package au.com.target.tgtstorefront.security;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.text.MessageFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationException;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;

import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;


/**
 * @author vmuthura
 * 
 */
public class TargetRememberMeServices extends TokenBasedRememberMeServices {
    private static final Logger LOG = Logger.getLogger(TargetRememberMeServices.class);

    private UserService userService;
    private CustomerFacade customerFacade;
    private SalesApplicationFacade salesApplicationFacade;

    public TargetRememberMeServices(final String key, final UserDetailsService userDetailsService) {
        super(key, userDetailsService);
    }

    /**
     * @return the userService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @return the customerFacade
     */
    public CustomerFacade getCustomerFacade() {
        return customerFacade;
    }

    /**
     * @param salesApplicationFacade
     *            the salesApplicationFacade to set
     */
    @Required
    public void setSalesApplicationFacade(final SalesApplicationFacade salesApplicationFacade) {
        this.salesApplicationFacade = salesApplicationFacade;
    }

    /**
     * @param customerFacade
     *            the customerFacade to set
     */
    @Required
    public void setCustomerFacade(final CustomerFacade customerFacade) {
        this.customerFacade = customerFacade;
    }

    @Override
    protected UserDetails processAutoLoginCookie(final String[] cookieTokens,
            final HttpServletRequest httpservletrequest,
            final HttpServletResponse httpservletresponse) throws RememberMeAuthenticationException,
                    UsernameNotFoundException {

        final UserDetails userDetails = super.processAutoLoginCookie(cookieTokens, httpservletrequest,
                httpservletresponse);

        salesApplicationFacade.initializeRequest(httpservletrequest);

        if (!getUserService().isUserExisting(userDetails.getUsername())) {
            LOG.warn(MessageFormat.format("Attempt to soft login user {0} failed: Customer not found",
                    userDetails.getUsername()));
            throw new UsernameNotFoundException("Unknown User");
        }
        final CustomerModel customerModel = getUserService().getUserForUID(userDetails.getUsername(),
                CustomerModel.class);

        getUserService().setCurrentUser(customerModel);

        //Restore cart
        getCustomerFacade().loginSuccess();

        return userDetails;
    }

    @Override
    public void logout(final HttpServletRequest request, final HttpServletResponse response,
            final Authentication authentication) {
        if (LOG.isDebugEnabled()) {
            LOG.debug((new StringBuilder()).append("Logout of user ")
                    .append(authentication != null ? authentication.getName() : "Unknown").toString());
        }
        super.cancelCookie(request, response);
    }

    @Override
    protected void cancelCookie(final HttpServletRequest request, final HttpServletResponse response) {
        //
    }

}

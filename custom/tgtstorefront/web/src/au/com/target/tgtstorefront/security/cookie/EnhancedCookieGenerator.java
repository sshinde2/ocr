/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.security.cookie;



import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.util.CookieGenerator;


/**
 * Enhanced {@link CookieGenerator} sets additionally header attribute {@value #HEADER_COOKIE}
 * 
 * Enhanced to handle httpOnly flag
 * 
 */
public class EnhancedCookieGenerator extends CookieGenerator
{
    public static final String HEADER_COOKIE = "Set-Cookie";
    public static final boolean DEFAULT_HTTP_ONLY = false;
    private boolean httpOnly = DEFAULT_HTTP_ONLY;


    public void setHttpOnly(final boolean httpOnly)
    {
        this.httpOnly = httpOnly;
    }

    protected boolean isHttpOnly()
    {
        return httpOnly;
    }

    @Override
    public void addCookie(final HttpServletResponse response, final String cookieValue)
    {
        if (!isHttpOnly()) {
            super.addCookie(response, cookieValue);
            return;
        }

        // Create cookie code from superclass addCookie method
        final Cookie cookie = createCookie(cookieValue);
        final Integer maxAge = getCookieMaxAge();
        if (maxAge != null) {
            cookie.setMaxAge(maxAge.intValue());
        }
        if (isCookieSecure()) {
            cookie.setSecure(true);
        }

        // Add cookie to response headers
        // (the default CookieGenerator.addCookie method does not support setting httpOnly).
        addHeaderCookie(cookie, response);
    }


    /**
     * Add header cookie
     * 
     * @param cookieIn
     * @param response
     */
    protected void addHeaderCookie(final Cookie cookieIn, final HttpServletResponse response)
    {
        final StringBuilder headerBuffer = new StringBuilder(100);
        ServerCookie.appendCookieValue(headerBuffer, cookieIn.getVersion(), cookieIn.getName(),
                cookieIn.getValue(),
                cookieIn.getPath(), cookieIn.getDomain(), cookieIn.getComment(), cookieIn.getMaxAge(),
                cookieIn.getSecure(),
                isHttpOnly());
        response.addHeader(HEADER_COOKIE, headerBuffer.toString());
    }

}

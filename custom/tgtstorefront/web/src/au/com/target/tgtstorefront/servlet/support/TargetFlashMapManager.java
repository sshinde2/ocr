/**
 * 
 */
package au.com.target.tgtstorefront.servlet.support;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.support.SessionFlashMapManager;
import org.springframework.web.util.UrlPathHelper;


/**
 * An extension to the {@link SessionFlashMapManager} that contains modified behaviour to support the use of
 * {@link FlashMap}s when caching is in effect.
 * 
 */
public class TargetFlashMapManager extends SessionFlashMapManager {

    private UrlPathHelper urlPathHelper;

    /**
     * A map containing a list of request paths (the keys) for which the query string parameter (the values) should be
     * used to determine if the flashMap is for the current request.
     */
    private Map<String, String> pathParameterMappings;

    /**
     * A modified version of the default isFlashMapForRequest method that also considers query string parameters to
     * determine if the given flashMap is for the current request.
     * 
     * If the originating requestUri matches one of the keys in {@link #pathParameterMappings}, then the query string
     * parameter matching the value is compared to the {@link FlashMap#getTargetRequestPath()} value instead of the
     * originating requestUri to determine whether the given flashMap is for the current request.
     * 
     * Apart from this change it behaves the same way as {
     * AbstractFlashMapManager#isFlashMapForRequest(org.springframework.web.servlet.FlashMap,
     * javax.servlet.http.HttpServletRequest)}
     * 
     * @param flashMap
     *            the flashMap
     * @param request
     *            the request
     * @return true if the given flashMap is for the current request, false if not
     * @see org.springframework.web.servlet.support.AbstractFlashMapManager#isFlashMapForRequest(org.springframework.web.servlet.FlashMap,
     *      javax.servlet.http.HttpServletRequest)
     */
    @Override
    protected boolean isFlashMapForRequest(final FlashMap flashMap, final HttpServletRequest request) {
        if (flashMap.getTargetRequestPath() != null) {
            boolean isFlashMapForRequest = false;

            final String requestUri = this.urlPathHelper.getOriginatingRequestUri(request);

            if (pathParameterMappings != null && pathParameterMappings.containsKey(requestUri)) {
                final String originalTarget = getQueryStringParameters(request).get(
                        pathParameterMappings.get(requestUri));

                if (flashMap.getTargetRequestPath().equals(originalTarget)) {
                    isFlashMapForRequest = true;
                }
            }
            else if (requestUri.equals(flashMap.getTargetRequestPath())
                    || requestUri.equals(flashMap.getTargetRequestPath() + "/")) {
                isFlashMapForRequest = true;
            }

            if (!isFlashMapForRequest) {
                return false;
            }
        }
        final MultiValueMap<String, String> targetParams = flashMap.getTargetRequestParams();
        for (final String paramName : targetParams.keySet()) {
            for (final String targetValue : targetParams.get(paramName)) {
                if (!ObjectUtils.containsElement(request.getParameterValues(paramName), targetValue)) {
                    return false;
                }
            }
        }
        return true;
    }

    private Map<String, String> getQueryStringParameters(final HttpServletRequest request) {
        final Map<String, String> queryStringParameters = new HashMap<String, String>();

        final String queryString = urlPathHelper.getOriginatingQueryString(request);
        if (StringUtils.isNotBlank(queryString)) {
            final String[] queryStringPairs = StringUtils.split(
                    urlPathHelper.decodeRequestString(request, queryString), '&');

            for (final String queryStringPair : queryStringPairs) {
                queryStringParameters.put(StringUtils.substringBefore(queryStringPair, "="),
                        StringUtils.substringAfter(queryStringPair, "="));
            }
        }

        return queryStringParameters;
    }

    /**
     * This method is will save the flash map to the existing set of flashmaps in the request
     * 
     * @param flashMap
     * @param request
     * @param response
     */
    public void saveFlashMapToOutput(final FlashMap flashMap, final HttpServletRequest request,
            final HttpServletResponse response) {
        saveOutputFlashMap(flashMap, request, response);
    }

    /**
     * @param urlPathHelper
     *            the urlPathHelper to set
     */
    @Override
    public void setUrlPathHelper(final UrlPathHelper urlPathHelper) {
        this.urlPathHelper = urlPathHelper;
    }

    /**
     * @param pathParameterMappings
     *            the pathParameterMappings to set
     */
    public void setPathParameterMappings(final Map<String, String> pathParameterMappings) {
        this.pathParameterMappings = pathParameterMappings;
    }
}

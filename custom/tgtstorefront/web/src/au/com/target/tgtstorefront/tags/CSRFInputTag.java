/**
 * 
 */
package au.com.target.tgtstorefront.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.springframework.security.web.csrf.CsrfToken;


/**
 * @author mjanarth
 *
 */
public class CSRFInputTag extends SimpleTagSupport {

    @Override
    public void doTag() throws JspException {
        //TODO to look for alternatives to this code to take care of the soft login wrong password scenario OCR-17697
        final CsrfToken tokenInRequest = (CsrfToken)((PageContext)getJspContext()).getRequest().getAttribute(
                CsrfToken.class.getName());
        final CsrfToken tokenInSession = (CsrfToken)((PageContext)getJspContext()).getSession()
                .getAttribute("org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository.CSRF_TOKEN");
        final CsrfToken token = (tokenInSession != null) ? tokenInSession : tokenInRequest;
        if (null != token) {
            try {
                getJspContext().getOut().write(handleToken(token));
            }
            catch (final IOException ioe) {
                throw new JspException(ioe);
            }
        }
    }


    /**
     * Outputs hidden form field for the CSRF token,name _csrf and value as csrftoken generated
     * 
     * @param token
     * @return String
     */
    protected String handleToken(final CsrfToken token) {
        return "<input type=\"hidden\" name=\"" + token.getParameterName()
                + "\" value=\"" + token.getToken() + "\" />";
    }

}
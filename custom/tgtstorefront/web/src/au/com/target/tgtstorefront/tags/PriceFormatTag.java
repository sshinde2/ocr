/**
 * 
 */
package au.com.target.tgtstorefront.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import au.com.target.tgtfacades.util.PriceFormatUtils;


/**
 * @author rmcalave
 * 
 */
public class PriceFormatTag extends SimpleTagSupport {

    private String value;

    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.SimpleTagSupport#doTag()
     */
    @Override
    public void doTag() throws JspException, IOException {
        getJspContext().getOut().write(PriceFormatUtils.formatPrice(value));
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(final String value) {
        this.value = value;
    }
}

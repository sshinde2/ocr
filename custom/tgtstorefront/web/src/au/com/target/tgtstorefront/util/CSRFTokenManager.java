/**
 * 
 */
package au.com.target.tgtstorefront.util;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
 * @author rmcalave
 * 
 */
public final class CSRFTokenManager {
    public static final String CSRF_PARAM_NAME = "ctoken";
    public static final String CSRF_TOKEN_FOR_SESSION_ATTR_NAME = "_targetCSRFToken";

    private CSRFTokenManager() {
        // Prevent instantiation
    }

    /**
     * Fetches the CSRF protection token from the current session. If there isn't one, creates one and sets it in the
     * session, before returning it.
     * 
     * @param session
     *            - {@link HttpSession}
     * @return csrf token - String
     */
    public static String getTokenForSession(final HttpSession session) {
        String token = null;
        synchronized (session.getId().intern()) {
            token = (String)session.getAttribute(CSRF_TOKEN_FOR_SESSION_ATTR_NAME);
            if (token == null) {
                token = UUID.randomUUID().toString();
                session.setAttribute(CSRF_TOKEN_FOR_SESSION_ATTR_NAME, token);
            }
        }
        return token;
    }

    /**
     * Extracts the CSFR token value from the provided {@link HttpServletRequest}
     * 
     * @param request
     *            - {@link HttpServletRequest}
     * @return csrf token - String
     */
    public static String getTokenFromRequest(final HttpServletRequest request) {
        return request.getParameter(CSRF_PARAM_NAME);
    }
}

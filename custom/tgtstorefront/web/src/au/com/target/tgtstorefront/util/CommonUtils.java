package au.com.target.tgtstorefront.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import au.com.target.tgtstorefront.forms.ThreePartDateForm;


/**
 * Common methods are implemented in this class
 * 
 * @author smudumba
 * 
 */
public class CommonUtils {

    //return default page size
    public static final int DEFAULT_PAGE_SIZE = 30;
    public static final int DEFAULT_INT_VAL = 0;
    public static final String QMARK = "?";

    private static final Logger LOG = Logger.getLogger(CommonUtils.class);

    /**
     * Constructor
     */
    private CommonUtils() {
        super();
        // YTODO Auto-generated constructor stub
    }

    /**
     * Convert string to integer and on error and if negative returns 0
     * 
     * @param value
     * @return int
     */

    public static int convertToPositiveInt(final String value) {
        try {
            final int intValue = Integer.parseInt(value);
            return (intValue >= DEFAULT_INT_VAL ? intValue : DEFAULT_INT_VAL);
        }
        catch (final NumberFormatException nfe) {
            // if this is reached set it to a default to 0
            return DEFAULT_INT_VAL;
        }

    }

    /**
     * Convert string to page size integer, on error return default page size
     * 
     * @param value
     * @return int
     */
    public static int getPageSizeInt(final String value) {
        try {
            final int intValue = Integer.parseInt(value);
            return (intValue > DEFAULT_INT_VAL ? intValue : DEFAULT_PAGE_SIZE);
        }
        catch (final NumberFormatException nfe) {
            // if this is reached set it to a default values default page size
            return DEFAULT_PAGE_SIZE;
        }

    }

    /**
     * 
     * @param form
     * @return java.util.Date
     */
    public static Date transformThreePartDateFormToDate(final ThreePartDateForm form) {

        if (form != null) {
            int day = 0;
            int month = 0;
            int year = 0;
            try {
                day = Integer.parseInt(form.getDay());
                month = Integer.parseInt(form.getMonth());
                year = Integer.parseInt(form.getYear());
            }
            catch (final NumberFormatException e) {
                LOG.error(
                        "invalid day month or year value while parsing string to int in transform three part date to date object",
                        e);
                return null;
            }
            try {
                final Calendar cal = new GregorianCalendar();
                cal.setLenient(false);
                cal.set(year, month - 1, day);
                return cal.getTime();
            }
            catch (final Exception e) {
                LOG.error("invalid day month or year value while transform three part date to date object", e);
                return null;
            }
        }
        return null;
    }

}
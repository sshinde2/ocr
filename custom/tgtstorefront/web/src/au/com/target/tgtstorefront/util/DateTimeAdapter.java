package au.com.target.tgtstorefront.util;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.DateTime;


/**
 * JAXB adapter for {@link DateTime} objects.
 */
public class DateTimeAdapter extends XmlAdapter<String, DateTime> {

    /**
     * Converts {@link String} to {@link DateTime}.
     * 
     * @param v
     *            the string to convert
     * @return the date & time object
     */
    @Override
    public DateTime unmarshal(final String v) {
        return new DateTime(v);
    }

    /**
     * Converts {@link DateTime} to {@link String}.
     * 
     * @param v
     *            the date & time to convert
     * @return the string representation
     */
    @Override
    public String marshal(final DateTime v) {
        return v.toString();
    }

}
package au.com.target.tgtstorefront.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import au.com.target.tgtutility.util.MarkupUtils;


public final class ElFunctions {
    protected static final Logger LOG = Logger.getLogger(ElFunctions.class);

    private static final String ELLIPSIS = "...";
    private static final String ENDECA_DELIMITER = "**";
    private static final String ENDECA_DELIMITER_PATTERN = "\\*\\*";

    private static final String CACHEBUSTER_REGEX = "^(.+)\\.(js|css)$";
    private static final Pattern CACHEBUSTER_REGEX_COMPILED = Pattern.compile(CACHEBUSTER_REGEX);

    private static final String VALID_URL_STRING_REGEX = "^(http(s)?://|/(/)?)";
    private static final Pattern VALID_URL_STRING_REGEX_COMPILED = Pattern.compile(VALID_URL_STRING_REGEX);

    private static final String NUMBER_LIST_REGEX = "^((\\d+)(\\,(\\d+))*)$";
    private static final Pattern NUMBER_LIST_REGEX_COMPILED = Pattern.compile(NUMBER_LIST_REGEX);

    private static final String NEW_LINE = "\n";
    private static final String HTML_BREAK = "<br />";

    private ElFunctions() {
        // utility class, should not be instantiated
    }

    public static String abbreviateString(final String stringToAbbreviate, final int maxLength) {
        if (maxLength < ELLIPSIS.length()) {
            throw new IllegalArgumentException("maxLength is too short. Must be " + ELLIPSIS.length() + " or more.");
        }

        if (StringUtils.length(stringToAbbreviate) <= maxLength) {
            return stringToAbbreviate;
        }

        final int maxLengthBeforeEllipsis = maxLength - ELLIPSIS.length();

        String newString = stringToAbbreviate.substring(0, maxLengthBeforeEllipsis);

        final int indexOfLastSpace = newString.lastIndexOf(' ');

        if (indexOfLastSpace > -1) {
            newString = newString.substring(0, indexOfLastSpace);
        }

        return newString + ELLIPSIS;
    }

    public static String abbreviateHTMLString(final String stringToAbbreviate, final int maxLength) {
        return abbreviateString(MarkupUtils.stripHtmlTagsFromText(stringToAbbreviate), maxLength);
    }

    // Function to convert integer to corresponding string value
    public static String intToString(final int subject) {
        final String[] values = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
                "ten", "eleven", "twelve" };

        return values[subject];
    }

    // Function to convert integer to corresponding string value
    // Note that the NavigationComponentController will restrict the number of
    // columns to 4 so this will always resolve to a sensible number
    public static String gridColumnsPerMegamenuColumn(final int noMegamenuColumns) {
        return intToString(12 / noMegamenuColumns);
    }

    /*
     * Return a cache busting url based on the property.
     * INPUT
     *  /_ui/build/css/app.css
     * OUTPUT
     * - /_ui/build/css/app.XXXXXXXXXXXX.css
     * - /_ui/build/css/app.css?build=XXXXXXXXXXXX 
     * - /_ui/build/css/app.css
     * 
     */
    public static String cacheBusterUrl(final boolean enabled, final String timestamp, final String url) {
        final StringBuilder ret = new StringBuilder();
        if (url != null) {
            if (StringUtils.isBlank(timestamp) || !CACHEBUSTER_REGEX_COMPILED.matcher(url).find()) {
                ret.append(url);
            }
            else if (enabled) {
                final StringBuilder timestampReplacement = new StringBuilder();
                timestampReplacement.append("$1.");
                timestampReplacement.append(timestamp);
                timestampReplacement.append(".$2");
                ret.append(CACHEBUSTER_REGEX_COMPILED.matcher(url).replaceAll(timestampReplacement.toString()));
            }
            else {
                ret.append(url);
                ret.append(url.indexOf('?') >= 0 ? "&" : "?");
                ret.append("b=");
                ret.append(timestamp);
            }
        }
        return ret.toString();
    }

    public static String validUrlString(final String url) {
        final String trimmedUrl = url.trim();
        if (VALID_URL_STRING_REGEX_COMPILED.matcher(trimmedUrl).find()) {
            return trimmedUrl;
        }
        else {
            return "/".concat(trimmedUrl);
        }
    }

    public static UriComponents getUriComponents(final String uri) {
        if (StringUtils.isNotBlank(uri)) {
            try {
                final String decodedUri = URLDecoder.decode(uri, "UTF-8");
                return UriComponentsBuilder.fromUriString(decodedUri).build();
            }
            catch (final UnsupportedEncodingException e) {
                // Suppress
                LOG.info("Got UnsupportedEncodingException while decoding the uri, suppressing it.");
            }
        }
        return null;
    }

    public static String splitEndecaUrlField(final String original) {
        if (StringUtils.isNotEmpty(original) && original.indexOf(ENDECA_DELIMITER) > -1) {
            final String[] parts = original.split(ENDECA_DELIMITER_PATTERN);
            if (parts != null && parts.length == 2 && StringUtils.isNotEmpty(parts[1])) {
                return parts[1];
            }
            else {
                return StringUtils.EMPTY;
            }
        }
        return original;
    }

    public static List<String> validateNumberListToArray(final String numberList) {

        List<String> items = new ArrayList<>();

        if (StringUtils.isNotBlank(numberList)) {
            if (NUMBER_LIST_REGEX_COMPILED.matcher(numberList).find()) {
                items = Arrays.asList(numberList.split(","));
            }
        }

        return items;
    }

    public static List reverseArrayList(final ArrayList list) {
        if (list != null) {
            final List reverseList = (List)list.clone();
            Collections.reverse(reverseList);
            return reverseList;
        }
        return null;
    }

    public static String replaceAllNewLineWithBreak(final String original) {
        return StringUtils.replace(original, NEW_LINE, HTML_BREAK);
    }



}

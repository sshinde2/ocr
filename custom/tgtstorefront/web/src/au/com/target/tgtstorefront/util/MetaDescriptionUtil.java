/**
 * 
 */
package au.com.target.tgtstorefront.util;

import de.hybris.platform.servicelayer.i18n.I18NService;

import java.text.MessageFormat;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.MessageSource;


/**
 * get the default meta description from base.properties
 * 
 */
public final class MetaDescriptionUtil {

    @Resource(name = "messageSource")
    private MessageSource messageSource;

    @Resource(name = "i18nService")
    private I18NService i18nService;

    private MetaDescriptionUtil() {
    }

    /**
     * combine the category name and category meta description.
     * 
     * @param metaDescription
     *            ,category name
     * @return string for meta description
     */
    public String replaceTokenInCategoryMetaDescription(final String metaDescription, final String categoryName) {
        String result = null;
        if (StringUtils.isNotEmpty(metaDescription) && StringUtils.isNotEmpty(categoryName)) {
            result = MessageFormat.format(metaDescription, categoryName);
        }
        return result;
    }

    /**
     * combine the category name and default category meta description.
     * 
     * @param categoryName
     * @return String for meta description
     */
    public String getDefaultCategoryMetaDescription(final String categoryName) {
        String result = null;
        if (StringUtils.isNotEmpty(categoryName)) {
            final Object[] parm = new Object[] { categoryName };
            result = messageSource.getMessage("meta.category.description", parm, i18nService.getCurrentLocale());
        }
        return result;
    }
}

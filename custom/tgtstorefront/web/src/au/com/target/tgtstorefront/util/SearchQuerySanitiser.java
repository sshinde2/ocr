/**
 * 
 */
package au.com.target.tgtstorefront.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;


/**
 * @author rmcalave
 * 
 */
public class SearchQuerySanitiser {

    private final Pattern allowedCharacterPattern;

    public SearchQuerySanitiser(final String allowedCharacterRegex) {
        Assert.notNull(allowedCharacterRegex, "allowedCharacterRegex must not be null");

        allowedCharacterPattern = Pattern.compile(allowedCharacterRegex);
    }

    /**
     * Sanitise search text
     * 
     * @param searchText
     * @return The sanitised text
     */
    public String sanitiseSearchText(final String searchText) {
        String cleanSearchText = searchText;
        if (StringUtils.isNotBlank(cleanSearchText))
        {
            cleanSearchText = cleanSearchText.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
            final Matcher matchString = allowedCharacterPattern.matcher(cleanSearchText);
            cleanSearchText = matchString.replaceAll(" ");
        }

        return StringUtils.trimToEmpty(cleanSearchText);
    }
}

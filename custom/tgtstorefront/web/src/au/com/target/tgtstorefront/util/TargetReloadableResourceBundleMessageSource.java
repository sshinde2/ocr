/**
 * 
 */
package au.com.target.tgtstorefront.util;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;


/**
 * @author bhuang3
 *
 */
public class TargetReloadableResourceBundleMessageSource extends ReloadableResourceBundleMessageSource {

    private String uiManifest;

    public Properties getUiManifestProperties() {
        final PropertiesHolder propertiesHolder = getProperties(uiManifest);
        return propertiesHolder.getProperties();
    }

    /**
     * @param uiManifest
     *            the uiManifest to set
     */
    @Required
    public void setUiManifest(final String uiManifest) {
        this.uiManifest = uiManifest;
    }

}
/**
 * 
 */
package au.com.target.tgtstorefront.util;

import de.hybris.platform.order.InvalidCartException;

import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.web.csrf.CsrfToken;

import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * @author rmcalave
 * 
 */
public final class TargetSessionTokenUtil {

    private static final String TOKEN_KEY = "_targetSessionToken";

    private TargetSessionTokenUtil() {
        // prevent construction
    }

    /**
     * Confirm the token is valid, then increments token to ensure it cannot be used again.
     * 
     * @param request
     * @return true if the token is valid
     */
    public static synchronized boolean isTokenValid(final HttpServletRequest request) {
        final HttpSession session = request.getSession();
        final String sessionToken = (String)session.getAttribute(getTokenKey());
        final String requestToken = request.getParameter(getTokenKey());
        if (requestToken == null) {
            // The hidden field wasn't provided
            return false;
        }
        if (sessionToken == null) {
            // The session has lost the token.
            return false;
        }
        if (sessionToken.equals(requestToken)) {
            // Accept the submission and increment the token so this form can't
            // be submitted again ...
            session.setAttribute(getTokenKey(), nextToken());
            return true;
        }
        return false;
    }

    public static synchronized boolean isIpgTokenValid(final HttpServletRequest request) {
        final HttpSession session = request.getSession();
        final String requestToken = request.getParameter(ControllerConstants.IPG_PARAM_TOKEN);
        if (requestToken == null) {
            // The hidden field wasn't provided
            return false;
        }
        final String sessionToken = (String)session.getAttribute(requestToken);
        if (sessionToken == null) {
            // The session has lost the token.
            return false;
        }
        if (sessionToken.equals(requestToken)) {
            //remove the token in the session, so the form cannot submit again
            session.removeAttribute(requestToken);
            return true;
        }
        return false;
    }

    public static synchronized boolean isPlaceOrderInProgress(final HttpServletRequest request) {
        final HttpSession session = request.getSession();
        if (session.getAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS) != null) {
            return true;
        }
        return false;
    }

    public static String nextToken() {
        final long seed = System.currentTimeMillis();
        final Random r = new Random();
        r.setSeed(seed);
        return Long.toString(seed) + Long.toString(Math.abs(r.nextInt(Integer.MAX_VALUE)));
    }

    public static String getTokenKey() {
        return TOKEN_KEY;
    }

    /**
     * Extracts CSRF Token from session
     * 
     * @param request
     * @return csrfToken
     */
    public static String getCsrfToken(final HttpServletRequest request) {
        final CsrfToken csrfToken = (CsrfToken)request.getAttribute(CsrfToken.class.getName());
        return csrfToken != null ? csrfToken.getToken() : null;
    }

    /**
     * Validate the request parameters
     * 
     * @param request
     * @throws IllegalArgumentException
     */
    public static void checkZipPayRequest(final HttpServletRequest request) throws IllegalArgumentException,
            InvalidCartException {

        final String sessionToken = (String)request.getSession().getAttribute(
                ControllerConstants.SESSION_PARAM_ZIPPAY_TOKEN);
        final String requestToken = request.getParameter(ControllerConstants.REQUEST_PARAM_ZIPPAY_CHECKOUT_TOKEN);
        final String paymentResult = request.getParameter(ControllerConstants.REQUEST_PARAM_ZIPPAY_RESULT);

        if (StringUtils.isEmpty(requestToken)) {
            throw new IllegalArgumentException("Invalid request or zipPay token does not match!");
        }

        if (!paymentResult.matches("(?i)" + ControllerConstants.REQUEST_PARAM_ZIPPAY_STATUS_APPROVED + "|"
                + ControllerConstants.REQUEST_PARAM_ZIPPAY_STATUS_DECLINED + "|"
                + ControllerConstants.REQUEST_PARAM_ZIPPAY_STATUS_REFERRED + "|"
                + ControllerConstants.REQUEST_PARAM_ZIPPAY_STATUS_CANCELLED))
        {
            throw new IllegalArgumentException("Invalid status code returned!");
        }

        if (!StringUtils.equals(sessionToken, requestToken)) {
            if (paymentResult.equalsIgnoreCase(ControllerConstants.REQUEST_PARAM_ZIPPAY_STATUS_APPROVED)) {
                throw new InvalidCartException("Invalid request or zipPay token does not match!");
            }
            throw new IllegalArgumentException("Invalid request or zipPay token does not match!");
        }



    }
}

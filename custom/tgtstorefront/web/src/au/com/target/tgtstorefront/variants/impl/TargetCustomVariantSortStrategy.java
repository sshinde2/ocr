/**
 * 
 */
package au.com.target.tgtstorefront.variants.impl;

import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.product.data.VariantOptionQualifierData;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.product.data.TargetVariantOptionQualifierData;


/**
 * @author rsamuel3
 *
 */
public class TargetCustomVariantSortStrategy extends DefaultVariantSortStrategy {

    private List<String> sortingFieldsOrder;
    private Map<String, Comparator<String>> customComparators;
    private Comparator<String> defaultComparator;

    /* (non-Javadoc)
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(final VariantOptionData product1, final VariantOptionData product2) {
        if (sortingFieldsOrder != null) {
            for (final String field : sortingFieldsOrder) {
                final int result = getCustomComparator(field).compare(getObjectValue(field, product1),
                        getObjectValue(field, product2));
                if (result != 0) {
                    return result;
                }
            }
        }
        return -1;
    }

    /**
     * get the ObjectValue from the variant data
     * 
     * @param field
     * @param variant
     * @return Object
     */
    protected Object getObjectValue(final String field, final VariantOptionData variant)
    {
        for (final VariantOptionQualifierData variantOptionQualifier : variant.getVariantOptionQualifiers()) {
            if (variantOptionQualifier instanceof TargetVariantOptionQualifierData) {
                final TargetVariantOptionQualifierData targetVariantOptionQualifier = (TargetVariantOptionQualifierData)variantOptionQualifier;
                if (field.equals(variantOptionQualifier.getQualifier())) {
                    return targetVariantOptionQualifier.getObjectValue();
                }
            }
            else {
                return getVariantValue(field, variant);
            }
        }
        return null;
    }

    /**
     * @param field
     * @return Comparator
     */
    private Comparator getCustomComparator(final String field) {
        final Comparator<String> comparator = getCustomComparators().get(field);
        if (comparator == null) {
            return getDefaultComparator();
        }
        return comparator;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtstorefront.variants.VariantSortStrategy#setSortingFieldsOrder(java.util.List)
     */
    @Override
    public void setSortingFieldsOrder(final List<String> fields) {
        this.sortingFieldsOrder = fields;
    }

    @Override
    @Required
    public void setDefaultComparator(final Comparator<String> defaultComparator) {
        super.setDefaultComparator(defaultComparator);
    }

    /**
     * @return the sortingFieldsOrder
     */
    @Override
    public List<String> getSortingFieldsOrder() {
        return sortingFieldsOrder;
    }

    /**
     * @return the defaultComparator
     */
    @Override
    public Comparator<String> getDefaultComparator() {
        return defaultComparator;
    }

    /**
     * @return the customComparators
     */
    public Map<String, Comparator<String>> getCustomComparators() {
        return customComparators;
    }

    /**
     * @param customComparators
     *            the customComparators to set
     */
    public void setCustomComparators(final Map<String, Comparator<String>> customComparators) {
        this.customComparators = customComparators;
    }

}

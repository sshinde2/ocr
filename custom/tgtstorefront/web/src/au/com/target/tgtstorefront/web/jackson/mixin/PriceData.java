/**
 * 
 */
package au.com.target.tgtstorefront.web.jackson.mixin;

import org.codehaus.jackson.annotate.JsonIgnore;


/**
 * @author htan3
 *
 */
public abstract class PriceData {
    @JsonIgnore
    abstract int getCurrencyIso();

    @JsonIgnore
    abstract int getPriceType();

    @JsonIgnore
    abstract int getMaxQuantity();

    @JsonIgnore
    abstract int getMinQuantity();
}

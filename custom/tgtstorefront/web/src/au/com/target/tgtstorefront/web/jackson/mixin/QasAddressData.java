package au.com.target.tgtstorefront.web.jackson.mixin;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * This is the jackson mixin configuration for au.com.target.tgtverifyaddr.data.AddressData
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public abstract class QasAddressData {

    @JsonProperty("label")
    abstract String getPartialAddress();

    @JsonProperty("id")
    abstract String getMoniker();

    @JsonIgnore
    abstract String getPostCode();

    @JsonIgnore
    abstract int getScore();
}

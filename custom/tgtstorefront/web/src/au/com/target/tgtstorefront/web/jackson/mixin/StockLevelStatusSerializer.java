/**
 * 
 */
package au.com.target.tgtstorefront.web.jackson.mixin;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;


/**
 * @author mgazal
 *
 */
public class StockLevelStatusSerializer extends JsonSerializer<StockLevelStatus> {

    @Override
    public void serialize(final StockLevelStatus value, final JsonGenerator jgen, final SerializerProvider provider)
            throws IOException, JsonProcessingException {
        jgen.writeString(value.getCode());
    }

}

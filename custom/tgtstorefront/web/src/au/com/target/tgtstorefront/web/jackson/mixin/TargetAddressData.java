/**
 * 
 */
package au.com.target.tgtstorefront.web.jackson.mixin;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * @author htan3
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public abstract class TargetAddressData {

    @JsonIgnore
    abstract String getFormattedAddress();

    @JsonIgnore
    abstract boolean isVisibleInAddressBook();

    @JsonIgnore
    abstract boolean isBillingAddress();

    @JsonIgnore
    abstract boolean isShippingAddress();
}

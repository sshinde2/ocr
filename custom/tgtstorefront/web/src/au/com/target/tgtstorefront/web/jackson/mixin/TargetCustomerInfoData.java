/**
 * 
 */
package au.com.target.tgtstorefront.web.jackson.mixin;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Mixing type to change DTO fields returned to UI.
 * 
 * @author jjayawa1
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public abstract class TargetCustomerInfoData {
    @JsonIgnore
    abstract String getUid();

}

/**
 * 
 */
package au.com.target.tgtstorefront.web.jackson.mixin;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import au.com.target.tgtfacades.order.data.GiftRecipientData;


/**
 * @author htan3
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public abstract class TargetOrderEntryData {
    @JsonIgnore
    abstract String getAffiliateOfferCategory();

    @JsonIgnore
    abstract String getDepartmentName();

    @JsonIgnore
    abstract boolean isUpdateable();

    @JsonIgnore
    abstract List<GiftRecipientData> getRecipients();
}

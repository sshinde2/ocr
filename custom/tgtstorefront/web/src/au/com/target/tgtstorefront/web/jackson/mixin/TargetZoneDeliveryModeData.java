/**
 * 
 */
package au.com.target.tgtstorefront.web.jackson.mixin;

import de.hybris.platform.commercefacades.product.data.PriceData;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * @author htan3
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public abstract class TargetZoneDeliveryModeData {
    @JsonProperty("id")
    abstract String getCode();

    @JsonProperty("fee")
    abstract PriceData getDeliveryCost();

    @JsonIgnore
    abstract int getDisplayOrder();

    @JsonIgnore
    abstract boolean isPostCodeNotSupported();

    @JsonIgnore
    abstract boolean isDigitalDelivery();

    @JsonIgnore
    abstract String getIncentiveMetMessage();

    @JsonIgnore
    abstract String getPotentialIncentiveMessage();
}

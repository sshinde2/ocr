/**
 * 
 */
package au.com.target.tgtstorefront.web.servlet.support;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.support.RequestDataValueProcessor;


/**
 * Basic implementation of RequestDataValueProcessor, mainly to eliminate bean lookup exceptions.
 * 
 * @author rmcalave
 * 
 */
public class TargetRequestDataValueProcessor implements RequestDataValueProcessor {

    /**
     * Performs no operations.
     * 
     * @return {@code action} unmodified
     * @see org.springframework.web.servlet.support.RequestDataValueProcessor#processAction(javax.servlet.http.HttpServletRequest,
     *      java.lang.String)
     */
    @Override
    public String processAction(final HttpServletRequest request, final String action) {
        return action;
    }

    /**
     * Performs no operations.
     * 
     * @return {@code value} unmodified
     * @see org.springframework.web.servlet.support.RequestDataValueProcessor#processFormFieldValue(javax.servlet.http.HttpServletRequest,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String processFormFieldValue(final HttpServletRequest request, final String name, final String value,
            final String type) {
        return value;
    }

    /**
     * Performs no operations.
     * 
     * @return {@code null}
     * @see org.springframework.web.servlet.support.RequestDataValueProcessor#getExtraHiddenFields(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public Map<String, String> getExtraHiddenFields(final HttpServletRequest request) {
        return null;
    }

    /**
     * Performs no operations.
     * 
     * @return {@code url} unmodified
     * @see org.springframework.web.servlet.support.RequestDataValueProcessor#processUrl(javax.servlet.http.HttpServletRequest,
     *      java.lang.String)
     */
    @Override
    public String processUrl(final HttpServletRequest request, final String url) {
        return url;
    }

}

/**
 * 
 */
package au.com.target.tgtstorefront.web.view;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.SerializationConfig;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;


/**
 * @author htan3
 *
 */
public class TargetMappingJacksonHttpMessageConverter extends MappingJacksonHttpMessageConverter {
    private static final Logger LOG = Logger.getLogger(TargetMappingJacksonHttpMessageConverter.class);

    @Required
    public void setMixins(final Map<String, String> mixins) {
        final SerializationConfig serializationConfig = getObjectMapper().getSerializationConfig();
        serializationConfig.setMixInAnnotations(null);
        if (mixins != null) {
            for (final Entry<String, String> mixin : mixins.entrySet()) {
                try {
                    serializationConfig.addMixInAnnotations(Class.forName(mixin.getKey()),
                            Class.forName(mixin.getValue()));
                }
                catch (final ClassNotFoundException e) {
                    LOG.warn("Failed to config jackson mixin for target[" + mixin.getKey() + "]", e);
                }
            }
        }
    }
}

/**
 * 
 */
package au.com.target.tgtstorefront.web.view;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;


/**
 * @author bhuang3
 * 
 */
public class TargetRedirectView extends RedirectView {

    public TargetRedirectView(final String url, final boolean contextRelative, final boolean http10Compatible) {
        super(url, contextRelative, http10Compatible);
    }

    /**
     * modify the logic, first check the RESPONSE_STATUS_ATTRIBUTE in request, if we have set the permanent redirect in
     * it, do it firstly, then check the http10Compatible if true send 302, if false, send 303
     */
    @Override
    protected void sendRedirect(
            final HttpServletRequest request, final HttpServletResponse response, final String targetUrl,
            final boolean http10Compatible)
            throws IOException {
        if (request.getAttribute(View.RESPONSE_STATUS_ATTRIBUTE) != null) {
            if (request.getAttribute(View.RESPONSE_STATUS_ATTRIBUTE) instanceof HttpStatus) {
                setStatusCode((HttpStatus)request.getAttribute(View.RESPONSE_STATUS_ATTRIBUTE));
            }
        }
        super.sendRedirect(request, response, targetUrl, http10Compatible);
    }

}

/**
 * 
 */
package au.com.target.endeca.infront.assemble;


import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.MapAssert.entry;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.endeca.infront.assembler.Assembler;
import com.endeca.infront.assembler.AssemblerException;
import com.endeca.infront.assembler.AssemblerFactory;
import com.endeca.infront.assembler.BasicContentItem;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.ContentInclude;
import com.endeca.infront.cartridge.ContentSlotConfig;
import com.endeca.infront.cartridge.RedirectAwareContentInclude;
import com.endeca.infront.cartridge.model.DimensionSearchGroup;
import com.endeca.infront.cartridge.model.DimensionSearchValue;
import com.endeca.navigation.ENEQueryException;
import com.endeca.soleng.urlformatter.seo.SeoNavStateEncoder;

import au.com.target.endeca.infront.assemble.impl.EndecaPageAssemble;
import au.com.target.endeca.infront.cache.EndecaDimensionCacheService;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.endeca.infront.exception.TargetEndecaWrapperException;
import au.com.target.endeca.infront.web.spring.TgtSpringUtility;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * @author Paul
 * 
 */
@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class EndecaAssembleTest {


    @Mock
    private AssemblerFactory assemblerFactory;

    @Mock
    private TgtSpringUtility springUtility;

    @Mock
    private HttpSession session;

    @Mock
    private HttpServletRequest request;

    @Mock
    private EndecaDimensionCacheService endecaDimensionCacheService;

    @InjectMocks
    private final EndecaPageAssemble endecaPageAssemble = new EndecaPageAssemble();

    @InjectMocks
    private final EndecaAssemble endecaAssemble = new EndecaPageAssemble();

    private Map<String, String> iosAppRecordFilters;

    @Before
    public void setupTestData()
            throws AssemblerException, JAXBException, javax.xml.transform.TransformerException, IOException {
        final SeoNavStateEncoder navStateEncoder = new SeoNavStateEncoder();
        navStateEncoder.setParamKey("N");

        given(request.getSession()).willReturn(session);

        iosAppRecordFilters = new HashMap<String, String>();
        iosAppRecordFilters.put("productDisplayFlag", "1");
        iosAppRecordFilters.put("availableOnline", "Available Online");
        iosAppRecordFilters.put("preOrder", "Pre-order");

        endecaAssemble.setIosAppRecordFilters(iosAppRecordFilters);
        endecaPageAssemble.setIosAppRecordFilters(iosAppRecordFilters);
    }

    /**
     * Test method for
     * {@link au.com.target.endeca.infront.assemble.EndecaAssemble#callAssembler(javax.servlet.http.HttpServletRequest, java.lang.String, au.com.target.endeca.infront.data.EndecaSearchStateData)}
     * .
     * 
     * @throws AssemblerException
     * @throws TargetEndecaException
     * @throws ENEQueryException
     */
    @Test
    public void testCallAssembler()
            throws AssemblerException, TargetEndecaWrapperException, ENEQueryException, TargetEndecaException {

        final EndecaSearchStateData endecaSearchStateData = mock(EndecaSearchStateData.class);
        given(request.getSession()).willReturn(session);
        given(request.getSession().getAttribute(TgtFacadesConstants.IOS_APP_WV)).willReturn(Boolean.TRUE);
        given(endecaDimensionCacheService
                .getDimension(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY, "W93742"))
                        .willReturn("2271506221");

        final Assembler assembler = mock(Assembler.class);

        given(endecaSearchStateData.getSearchTerm()).willReturn(Arrays.asList("red dress"));
        given(assemblerFactory.createAssembler()).willReturn(assembler);
        final String pageUri = "/pages/category";

        final ContentItem contentItem = new RedirectAwareContentInclude(pageUri);

        final ContentItem responseContentItem = mock(ContentItem.class);

        given(assembler.assemble(contentItem)).willReturn(responseContentItem);

        endecaAssemble.setSpringUtility(springUtility);

        final ContentItem result = endecaAssemble.callAssembler(request, pageUri, endecaSearchStateData);

        assertThat(responseContentItem.hashCode()).isEqualTo(result.hashCode());

    }

    /**
     * Test method for
     * {@link au.com.target.endeca.infront.assemble.EndecaAssemble#callAssembler(javax.servlet.http.HttpServletRequest, java.lang.String, au.com.target.endeca.infront.data.EndecaSearchStateData)}
     * .
     * 
     * @throws AssemblerException
     */
    @Test
    public void testCallAssemblerWhenSkipRedirect() throws AssemblerException, TargetEndecaWrapperException {

        final EndecaSearchStateData endecaSearchStateData = mock(EndecaSearchStateData.class);

        doReturn(Boolean.TRUE).when(endecaSearchStateData).isSkipRedirect();

        given(request.getSession()).willReturn(session);
        doReturn(Boolean.TRUE).when(session).getAttribute(TgtFacadesConstants.IOS_APP_WV);

        final Assembler assembler = mock(Assembler.class);

        given(endecaSearchStateData.getSearchTerm()).willReturn(Arrays.asList("red dress"));

        given(assemblerFactory.createAssembler()).willReturn(assembler);

        final String pageUri = "/pages/category";

        final ContentItem contentItem = new ContentInclude(pageUri);

        final ContentItem responseContentItem = mock(ContentItem.class);

        given(assembler.assemble(contentItem)).willReturn(responseContentItem);

        endecaAssemble.setSpringUtility(springUtility);

        final ContentItem result = endecaAssemble.callAssembler(request, pageUri, endecaSearchStateData);

        assertThat(result.hashCode()).isEqualTo(responseContentItem.hashCode());

    }

    /**
     * Test method for
     * {@link au.com.target.endeca.infront.assemble.EndecaAssemble#getNavStateFromDimensionSearch(com.endeca.infront.assembler.ContentItem)}
     * .
     */
    @Test
    public void testGetNavStateFromDimensionSearchEncoded() {
        final String result = getNavStateFromDimensionSearch("baby/kids/W12354/_/NZxzzy");
        assertThat(result).isEqualTo("baby/kids/W12354/_/NZxzzy");
    }

    /**
     * Test method for
     * {@link au.com.target.endeca.infront.assemble.EndecaAssemble#getNavStateFromDimensionSearch(com.endeca.infront.assembler.ContentItem)}
     * .
     */
    @Test
    public void testGetNavIdFromDimensionSearch() {
        final String result = getNavStateFromDimensionSearch("?N=10223");
        assertThat(result).isEqualTo("10223");
    }

    /**
     * Test method for
     * {@link au.com.target.endeca.infront.assemble.EndecaAssemble#getNavStateFromDimensionSearch(com.endeca.infront.assembler.ContentItem)}
     * .
     */
    @Test
    public void testGetNavStateFromDimensionSearchNormal() {

        final String result = getNavStateFromDimensionSearch("?N=12334+23322");
        assertThat(result).isEqualTo("12334+23322");

    }

    private String getNavStateFromDimensionSearch(final String navStateToMock) {
        final ContentItem contentItem = mock(ContentItem.class);

        final DimensionSearchValue dimSearchValue = new DimensionSearchValue();
        dimSearchValue.setNavigationState(navStateToMock);

        final List<DimensionSearchValue> dimSearchValueList = new ArrayList<>();
        dimSearchValueList.add(dimSearchValue);

        final DimensionSearchGroup dimensionSearchGroup = new DimensionSearchGroup();
        dimensionSearchGroup.setDimensionSearchValues(dimSearchValueList);

        final List<DimensionSearchGroup> dimensionSearchGroupList = new ArrayList<>();
        dimensionSearchGroupList.add(dimensionSearchGroup);


        final ContentItem contentItemDimSearchResults = mock(ContentItem.class);
        given(contentItem.get("dimensionSearchResults")).willReturn(contentItemDimSearchResults);

        given(contentItemDimSearchResults.get("dimensionSearchGroups")).willReturn(dimensionSearchGroupList);

        return endecaAssemble.getNavStateFromDimensionSearch(contentItem);
    }

    /**
     * Test method for {@link au.com.target.endeca.infront.assemble.EndecaAssemble#checkEndecaResponse}.
     * 
     * @throws TargetEndecaWrapperException
     */
    @Test(expected = TargetEndecaWrapperException.class)
    public void testCheckEndecaResponse() throws TargetEndecaWrapperException {

        final ContentItem responseContentItem = new BasicContentItem();
        final String errorMsg = "com.endeca.infront.content.ContentException: com.endeca.navigation.ENEConnectionException: "
                + "Error establishing connection to retrieve "
                + "Navigation Engine request 'http://localhost:15002/graph?node=0&offset=0&nbins=0&attrs=All|towels&irversion=640'."
                + " Connection refused: connect Check MDEX Logs and specified query parameters";
        responseContentItem.put("@error", errorMsg);
        endecaAssemble.checkEndecaResponse(responseContentItem);
    }

    /**
     * Test method for
     * {@link au.com.target.endeca.infront.assemble.EndecaAssemble#getExistingNavigtaionStateUri(java.lang.String)}.
     */
    @Test
    public void testGetExistingNavigtaionStateUri() {
        final String result = endecaAssemble.getExistingNavigtaionStateUri("/pages/category/baby/W1234/_/NzzxYe22");
        assertThat(result).isEqualTo("/_/NzzxYe22");

    }


    /**
     * Test method for
     * {@link au.com.target.endeca.infront.assemble.EndecaAssemble#getExistingNavigtaionStateUri(java.lang.String)}.
     */
    @Test
    public void testGetExistingNavigtaionStateUriEmpty() {
        final String result = endecaAssemble.getExistingNavigtaionStateUri("/pages/category/baby/W1234");
        assertThat(result).isEqualTo("");

    }

    @Test
    public void testDynamicContentAssemble() throws TargetEndecaWrapperException, AssemblerException {

        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setSearchTerm(Arrays.asList("red dress"));

        given(request.getRequestURI()).willReturn(ControllerConstants.MOBILE_APP_AUTO_SEARCH);

        final Assembler assembler = mock(Assembler.class);
        given(assemblerFactory.createAssembler()).willReturn(assembler);

        given(request.getSession()).willReturn(session);
        given(request.getSession().getAttribute(TgtFacadesConstants.IOS_APP_WV)).willReturn(Boolean.TRUE);
        final ContentSlotConfig contentSlotConfig = new ContentSlotConfig();
        final List<String> contentPaths = new ArrayList<>();
        contentPaths.add(EndecaConstants.EndecaPageUris.ENDECA_AUTOSUGGEST_SEARCH_CONTENT_PATH);
        contentSlotConfig.setContentPaths(contentPaths);
        final List<String> templateTypes = new ArrayList<>();
        templateTypes.add(EndecaConstants.EndecaPageUris.ENDECA_AUTOSUGGEST_SEARCH_TEMPLATE_TYPE);
        contentSlotConfig.setTemplateTypes(templateTypes);
        contentSlotConfig.setRuleLimit(3);
        final ContentItem responseContentItem = mock(ContentItem.class);
        given(assembler.assemble(contentSlotConfig)).willReturn(responseContentItem);
        endecaAssemble.setSpringUtility(springUtility);
        final ContentItem result = endecaAssemble.dynamicContentAssemble(request, endecaSearchStateData,
                contentSlotConfig);
        assertThat(result.hashCode()).isEqualTo(responseContentItem.hashCode());

        assertThat(endecaSearchStateData.getRecordFilters()).hasSize(3).includes(entry("productDisplayFlag", "1"),
                entry("availableOnline", "Available Online"), entry("preOrder", "Pre-order"));
    }



    @Test
    public void testDynamicContentAssembleFailure() throws TargetEndecaWrapperException, AssemblerException {

        final EndecaSearchStateData endecaSearchStateData = mock(EndecaSearchStateData.class);

        final Assembler assembler = mock(Assembler.class);
        given(endecaSearchStateData.getSearchTerm()).willReturn(Arrays.asList("red dress"));
        given(assemblerFactory.createAssembler()).willReturn(assembler);
        given(request.getRequestURI()).willReturn(ControllerConstants.MOBILE_APP_AUTO_SEARCH);

        given(request.getSession()).willReturn(session);

        final ContentSlotConfig contentSlotConfig = new ContentSlotConfig();
        final List<String> contentPaths = new ArrayList<>();
        contentPaths.add(EndecaConstants.EndecaPageUris.ENDECA_AUTOSUGGEST_SEARCH_CONTENT_PATH);
        contentSlotConfig.setContentPaths(contentPaths);
        final List<String> templateTypes = new ArrayList<>();
        templateTypes.add(EndecaConstants.EndecaPageUris.ENDECA_AUTOSUGGEST_SEARCH_TEMPLATE_TYPE);
        contentSlotConfig.setTemplateTypes(templateTypes);
        contentSlotConfig.setRuleLimit(3);
        final ContentItem responseContentItem = mock(ContentItem.class);
        given(assembler.assemble(contentSlotConfig)).willReturn(responseContentItem);
        endecaAssemble.setSpringUtility(springUtility);
        final ContentItem result = endecaAssemble.dynamicContentAssemble(request, endecaSearchStateData,
                contentSlotConfig);
        assertThat(result.hashCode()).isEqualTo(responseContentItem.hashCode());
        verify(endecaSearchStateData, never()).setNavigationState(anyList());


    }




    @Test
    public void testDynamicContentAssembleWithHeaders() throws TargetEndecaWrapperException, AssemblerException {

        final EndecaSearchStateData endecaSearchStateData = new EndecaSearchStateData();
        endecaSearchStateData.setSearchTerm(Arrays.asList("red dress"));

        final Assembler assembler = mock(Assembler.class);
        given(assemblerFactory.createAssembler()).willReturn(assembler);

        given(request.getSession()).willReturn(session);
        given(request.getRequestURI()).willReturn(ControllerConstants.MOBILE_APP_AUTO_SEARCH);
        given(session.getAttribute(TgtFacadesConstants.IOS_APP_WV)).willReturn(Boolean.TRUE);

        final ContentSlotConfig contentSlotConfig = new ContentSlotConfig();
        final List<String> contentPaths = new ArrayList<>();
        contentPaths.add(EndecaConstants.EndecaPageUris.ENDECA_AUTOSUGGEST_SEARCH_CONTENT_PATH);
        contentSlotConfig.setContentPaths(contentPaths);
        final List<String> templateTypes = new ArrayList<>();
        templateTypes.add(EndecaConstants.EndecaPageUris.ENDECA_AUTOSUGGEST_SEARCH_TEMPLATE_TYPE);
        contentSlotConfig.setTemplateTypes(templateTypes);
        contentSlotConfig.setRuleLimit(3);
        final ContentItem responseContentItem = mock(ContentItem.class);
        given(assembler.assemble(contentSlotConfig)).willReturn(responseContentItem);
        endecaAssemble.setSpringUtility(springUtility);
        final ContentItem result = endecaAssemble.dynamicContentAssemble(request, endecaSearchStateData,
                contentSlotConfig);
        assertThat(result.hashCode()).isEqualTo(responseContentItem.hashCode());

        assertThat(endecaSearchStateData.getRecordFilters()).hasSize(3).includes(entry("productDisplayFlag", "1"),
                entry("availableOnline", "Available Online"), entry("preOrder", "Pre-order"));
    }


}

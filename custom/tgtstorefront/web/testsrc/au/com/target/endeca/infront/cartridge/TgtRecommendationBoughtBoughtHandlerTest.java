/**
 * 
 */
package au.com.target.endeca.infront.cartridge;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.util.TargetEndecaUtil;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.RecordSpotlightConfig;
import com.endeca.infront.cartridge.model.Attribute;
import com.endeca.infront.cartridge.model.Record;
import com.endeca.infront.navigation.MdexRequestBroker;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.model.FilterState;
import com.endeca.infront.navigation.request.MdexQuery;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.infront.navigation.request.RecordsMdexQuery;
import com.endeca.navigation.AggrERec;
import com.endeca.navigation.AggrERecList;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERec;
import com.endeca.navigation.ERecList;
import com.endeca.navigation.Navigation;
import com.endeca.navigation.PropertyMap;


/**
 * @author Nandini
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TgtRecommendationBoughtBoughtHandlerTest {

    @Mock
    private ENEQueryResults eneResults;
    @Mock
    private Navigation navigation;
    @Mock
    private AggrERecList recList;
    @Mock
    private FilterState filterState;
    @Mock
    private MdexRequest mdexRequest;
    @Mock
    private NavigationState mNavigationState;
    @Mock
    private MdexQuery mMdexQuery;
    @Mock
    private MdexRequestBroker mMdexRequestBroker;
    @Mock
    private AggrERec aggregateERec;
    @Mock
    private RecordSpotlightConfig config;
    @Mock
    private TargetEndecaUtil targetEndecaUtil;

    @Mock
    private Map<String, List<String>> prodCodeAndRecomCode;

    @InjectMocks
    private final TgtRecommendationBoughtBoughtHandler handler = new MyTgtRecommendationBoughtBoughtHandler();

    private class MyTgtRecommendationBoughtBoughtHandler extends TgtRecommendationBoughtBoughtHandler {


        @Override
        protected ENEQueryResults executeMdexRequest(final MdexRequest mMdexRequest) {
            return eneResults;
        }

        @Override
        protected MdexRequestBroker getMdexRequestBroker() {
            return mMdexRequestBroker;
        }

        @Override
        public NavigationState getNavigationState() {
            return mNavigationState;
        }

        @Override
        protected MdexRequest createMdexRequest(final FilterState pFilterState, final MdexQuery pMdexQuery) {
            return mdexRequest;
        }

    }


    @Test
    public void testProcess() throws CartridgeHandlerException {
        final TgtRecommendationBoughtBoughtConfig cartridgeConfig = new TgtRecommendationBoughtBoughtConfig(
                EndecaConstants.SEARCH_FIELD_RECOMMENDED_PRODUCTS);
        final List<String> recommendedProducts = new ArrayList<>();
        recommendedProducts.add("2@Test1");
        recommendedProducts.add("4@Test2");

        final List navFilters = new ArrayList<>();
        navFilters.add("0");
        when(filterState.getNavigationFilters()).thenReturn(navFilters);
        when(eneResults.getNavigation()).thenReturn(navigation);
        when(navigation.getAggrERecs()).thenReturn(recList);
        final AggrERec aggrERec = mock(AggrERec.class);
        final ERec rec = mock(ERec.class);
        final PropertyMap properties = mock(PropertyMap.class);
        when(recList.iterator()).thenReturn(createIterator(aggrERec));
        final ERecList eRecList = mock(ERecList.class);
        when(eRecList.iterator()).thenReturn(createIterator(rec));
        when(aggrERec.getERecs()).thenReturn(eRecList);
        when(rec.getProperties()).thenReturn(properties);
        when(aggrERec.getRepresentative()).thenReturn(rec);
        when(properties.getValues(EndecaConstants.SEARCH_FIELD_RECOMMENDED_PRODUCTS)).thenReturn(recommendedProducts);
        handler.process(cartridgeConfig);
    }

    @Test
    public void testPreprocessEmptyProdCodes() throws CartridgeHandlerException {
        final TgtRecommendationBoughtBoughtConfig cartridgeConfig = new TgtRecommendationBoughtBoughtConfig(
                EndecaConstants.SEARCH_FIELD_RECOMMENDED_PRODUCTS);
        final RecordsMdexQuery recordsQuery = new RecordsMdexQuery();
        final List fields = new ArrayList();
        fields.add("field1");
        fields.add("field2");
        recordsQuery.setFieldNames(fields);
        when(mNavigationState.getFilterState()).thenReturn(filterState);
        cartridgeConfig.put(EndecaConstants.EndecaParamMarshaller.ENDECA_PRODUCT_CODES, "P1100");
        handler.preprocess(cartridgeConfig);
    }

    @Test
    public void testPreprocessWithProdCodes() throws CartridgeHandlerException {
        final TgtRecommendationBoughtBoughtConfig cartridgeConfig = new TgtRecommendationBoughtBoughtConfig(
                EndecaConstants.SEARCH_FIELD_RECOMMENDED_PRODUCTS);
        final RecordsMdexQuery recordsQuery = new RecordsMdexQuery();
        final List fields = new ArrayList();
        fields.add("field1");
        fields.add("field2");
        recordsQuery.setFieldNames(fields);
        when(mNavigationState.getFilterState()).thenReturn(filterState);
        handler.preprocess(cartridgeConfig);
    }

    @Test
    public void testGetRecommendedProductsListEmpty() {
        final TgtRecommendationBoughtBoughtConfig cartridgeConfig = Mockito
                .mock(TgtRecommendationBoughtBoughtConfig.class);
        when(eneResults.getNavigation()).thenReturn(navigation);
        handler.getRecommendedProductsList(eneResults, cartridgeConfig, prodCodeAndRecomCode);
    }

    @Test
    public void testGetRecommendedProductsList() {
        final TgtRecommendationBoughtBoughtConfig cartridgeConfig = Mockito
                .mock(TgtRecommendationBoughtBoughtConfig.class);
        final List<String> recommendedProducts = new ArrayList<>();
        recommendedProducts.add("2@Test1");
        recommendedProducts.add("4@Test2");
        when(eneResults.getNavigation()).thenReturn(navigation);
        when(navigation.getAggrERecs()).thenReturn(recList);
        final AggrERec aggrERec = mock(AggrERec.class);
        final ERec rec = mock(ERec.class);
        final PropertyMap properties = mock(PropertyMap.class);
        when(recList.iterator()).thenReturn(createIterator(aggrERec));
        final ERecList eRecList = mock(ERecList.class);
        when(eRecList.iterator()).thenReturn(createIterator(rec));
        when(aggrERec.getERecs()).thenReturn(eRecList);
        when(rec.getProperties()).thenReturn(properties);
        when(aggrERec.getRepresentative()).thenReturn(rec);
        when(properties.getValues(EndecaConstants.SEARCH_FIELD_RECOMMENDED_PRODUCTS)).thenReturn(recommendedProducts);
        final List<String> results = handler.getRecommendedProductsList(eneResults, cartridgeConfig,
                prodCodeAndRecomCode);
        Mockito.verify(eneResults, Mockito.times(1)).getNavigation();
        assertEquals(2, results.size());
    }

    @Test
    public void testValidateAndFilterRecommendedProductData() {
        final List<String> recommendedProducts = new ArrayList<>();
        recommendedProducts.add("2@Test1");
        recommendedProducts.add("4@Test2");
        when(eneResults.getNavigation()).thenReturn(navigation);
        when(navigation.getAggrERecs()).thenReturn(recList);
        final AggrERec aggrERec = mock(AggrERec.class);
        final ERec rec = mock(ERec.class);
        final PropertyMap properties = mock(PropertyMap.class);
        when(recList.iterator()).thenReturn(createIterator(aggrERec));
        final ERecList eRecList = mock(ERecList.class);
        when(eRecList.iterator()).thenReturn(createIterator(rec));
        when(aggrERec.getERecs()).thenReturn(eRecList);
        when(rec.getProperties()).thenReturn(properties);
        when(aggrERec.getRepresentative()).thenReturn(rec);
        when(properties.getValues(EndecaConstants.SEARCH_FIELD_RECOMMENDED_PRODUCTS)).thenReturn(recommendedProducts);
        final List<String> results = handler.validateAndFilterRecommendedProductData(recommendedProducts);
        assertEquals(2, results.size());
    }

    @Test
    public void testValidateAndFilterRecommendedProductDataInvalid() {
        final List<String> recommendedProducts = new ArrayList<>();
        recommendedProducts.add("2#Test1");
        recommendedProducts.add("4#Test2");
        when(eneResults.getNavigation()).thenReturn(navigation);
        when(navigation.getAggrERecs()).thenReturn(recList);
        final AggrERec aggrERec = mock(AggrERec.class);
        final ERec rec = mock(ERec.class);
        final PropertyMap properties = mock(PropertyMap.class);
        when(recList.iterator()).thenReturn(createIterator(aggrERec));
        final ERecList eRecList = mock(ERecList.class);
        when(eRecList.iterator()).thenReturn(createIterator(rec));
        when(aggrERec.getERecs()).thenReturn(eRecList);
        when(rec.getProperties()).thenReturn(properties);
        when(aggrERec.getRepresentative()).thenReturn(rec);
        when(properties.getValues(EndecaConstants.SEARCH_FIELD_RECOMMENDED_PRODUCTS)).thenReturn(recommendedProducts);
        final List<String> results = handler.validateAndFilterRecommendedProductData(recommendedProducts);
        assertEquals(0, results.size());
    }


    @Test
    public void testGetRecommendedProductsListForMultipleProducts() {

        final TgtRecommendationBoughtBoughtConfig cartridgeConfig = Mockito
                .mock(TgtRecommendationBoughtBoughtConfig.class);
        when(cartridgeConfig
                .get(EndecaConstants.EndecaParamMarshaller.ENDECA_PRODUCT_CODES)).thenReturn("Prod1,Prod2");
        final List<String> recommendedProducts = new ArrayList<>();
        recommendedProducts.add("2@Test1");
        recommendedProducts.add("4@Test2");
        final List<String> recommendedProducts2 = new ArrayList<>();
        recommendedProducts2.add("2@Test3");
        recommendedProducts2.add("4@Test4");
        when(eneResults.getNavigation()).thenReturn(navigation);
        when(navigation.getAggrERecs()).thenReturn(recList);
        final AggrERec aggrERec1 = mock(AggrERec.class);
        final AggrERec aggrERec2 = mock(AggrERec.class);
        final ERec rec1 = mock(ERec.class);
        final PropertyMap properties1 = mock(PropertyMap.class);
        when(recList.iterator()).thenReturn(createIteratorForMultipleRecords(aggrERec1, aggrERec2, null));
        final ERecList eRecList1 = mock(ERecList.class);
        when(eRecList1.iterator()).thenReturn(createIterator(rec1));
        when(aggrERec1.getERecs()).thenReturn(eRecList1);
        when(rec1.getProperties()).thenReturn(properties1);
        when(aggrERec1.getRepresentative()).thenReturn(rec1);
        when(properties1.getValues(EndecaConstants.SEARCH_FIELD_RECOMMENDED_PRODUCTS)).thenReturn(recommendedProducts);
        when(properties1.getValues(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING)).thenReturn(
                Arrays.asList("Prod1"));
        final ERec rec2 = mock(ERec.class);
        final PropertyMap properties2 = mock(PropertyMap.class);
        final ERecList eRecList2 = mock(ERecList.class);
        when(eRecList2.iterator()).thenReturn(createIterator(rec2));
        when(aggrERec2.getERecs()).thenReturn(eRecList2);
        when(rec2.getProperties()).thenReturn(properties2);
        when(aggrERec2.getRepresentative()).thenReturn(rec2);
        when(properties2.getValues(EndecaConstants.SEARCH_FIELD_RECOMMENDED_PRODUCTS)).thenReturn(recommendedProducts2);
        when(properties2.getValues(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING)).thenReturn(
                Arrays.asList("Prod2"));
        final List<String> results = handler.getRecommendedProductsList(eneResults, cartridgeConfig,
                prodCodeAndRecomCode);
        Mockito.verify(eneResults, Mockito.times(1)).getNavigation();
        assertEquals(4, results.size());
    }

    @Test
    public void testGetRecommendedProductsListForMultipleProductsWhenCartAlreadyHasRecommended() {

        final TgtRecommendationBoughtBoughtConfig cartridgeConfig = Mockito
                .mock(TgtRecommendationBoughtBoughtConfig.class);
        when(cartridgeConfig
                .get(EndecaConstants.EndecaParamMarshaller.ENDECA_PRODUCT_CODES)).thenReturn("Prod1,Test1");
        final List<String> recommendedProducts = new ArrayList<>();
        recommendedProducts.add("2@Test1");
        recommendedProducts.add("4@Test2");
        recommendedProducts.add("2@Test3");
        recommendedProducts.add("4@Test4");
        recommendedProducts.add("2@Test5");
        recommendedProducts.add("4@Test6");
        recommendedProducts.add("2@Test7");
        recommendedProducts.add("4@Test8");
        final List<String> recommendedProducts2 = new ArrayList<>();
        recommendedProducts2.add("2@Test9");
        recommendedProducts2.add("4@Test10");
        recommendedProducts2.add("2@Test11");
        recommendedProducts2.add("4@Test12");
        recommendedProducts2.add("2@Test13");
        recommendedProducts2.add("4@Test14");
        recommendedProducts2.add("2@Test15");
        recommendedProducts2.add("4@Test16");
        when(eneResults.getNavigation()).thenReturn(navigation);
        when(navigation.getAggrERecs()).thenReturn(recList);
        final AggrERec aggrERec1 = mock(AggrERec.class);
        final AggrERec aggrERec2 = mock(AggrERec.class);
        final ERec rec1 = mock(ERec.class);
        final PropertyMap properties1 = mock(PropertyMap.class);
        when(recList.iterator()).thenReturn(createIteratorForMultipleRecords(aggrERec1, aggrERec2, null));
        final ERecList eRecList1 = mock(ERecList.class);
        when(eRecList1.iterator()).thenReturn(createIterator(rec1));
        when(aggrERec1.getERecs()).thenReturn(eRecList1);
        when(rec1.getProperties()).thenReturn(properties1);
        when(aggrERec1.getRepresentative()).thenReturn(rec1);
        when(properties1.getValues(EndecaConstants.SEARCH_FIELD_RECOMMENDED_PRODUCTS)).thenReturn(recommendedProducts);
        when(properties1.getValues(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING)).thenReturn(
                Arrays.asList("Prod1"));
        final ERec rec2 = mock(ERec.class);
        final PropertyMap properties2 = mock(PropertyMap.class);
        final ERecList eRecList2 = mock(ERecList.class);
        when(eRecList2.iterator()).thenReturn(createIterator(rec2));
        when(aggrERec2.getERecs()).thenReturn(eRecList2);
        when(rec2.getProperties()).thenReturn(properties2);
        when(aggrERec2.getRepresentative()).thenReturn(rec2);
        when(properties2.getValues(EndecaConstants.SEARCH_FIELD_RECOMMENDED_PRODUCTS)).thenReturn(recommendedProducts2);
        when(properties2.getValues(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING)).thenReturn(
                Arrays.asList("Test1"));
        final List<String> results = handler.getRecommendedProductsList(eneResults, cartridgeConfig,
                prodCodeAndRecomCode);
        Mockito.verify(eneResults, Mockito.times(1)).getNavigation();
        assertEquals(15, results.size());
    }

    @Test
    public void testGetRecommendedProductsListForMultipleProductsForSplit() {

        final TgtRecommendationBoughtBoughtConfig cartridgeConfig = Mockito
                .mock(TgtRecommendationBoughtBoughtConfig.class);
        when(cartridgeConfig
                .get(EndecaConstants.EndecaParamMarshaller.ENDECA_PRODUCT_CODES)).thenReturn("Prod1,Prod2,Prod3");
        final List<String> recommendedProducts = new ArrayList<>();
        recommendedProducts.add("2@Test1");
        recommendedProducts.add("4@Test2");
        recommendedProducts.add("2@Test3");
        recommendedProducts.add("4@Test4");
        recommendedProducts.add("2@Test5");
        recommendedProducts.add("4@Test6");
        recommendedProducts.add("2@Test7");
        recommendedProducts.add("4@Test8");
        final List<String> recommendedProducts2 = new ArrayList<>();
        recommendedProducts2.add("2@Test9");
        recommendedProducts2.add("4@Test10");
        recommendedProducts2.add("2@Test11");
        recommendedProducts2.add("4@Test12");
        recommendedProducts2.add("2@Test13");
        recommendedProducts2.add("4@Test14");
        recommendedProducts2.add("2@Test15");
        recommendedProducts2.add("4@Test16");
        final List<String> recommendedProducts3 = new ArrayList<>();
        recommendedProducts3.add("2@Test31");
        recommendedProducts3.add("4@Test32");
        recommendedProducts3.add("2@Test33");
        recommendedProducts3.add("4@Test34");
        recommendedProducts3.add("2@Test35");
        recommendedProducts3.add("4@Test36");
        recommendedProducts3.add("2@Test37");
        recommendedProducts3.add("4@Test38");
        when(eneResults.getNavigation()).thenReturn(navigation);
        when(navigation.getAggrERecs()).thenReturn(recList);
        final AggrERec aggrERec1 = mock(AggrERec.class);
        final AggrERec aggrERec2 = mock(AggrERec.class);
        final AggrERec aggrERec3 = mock(AggrERec.class);
        final ERec rec1 = mock(ERec.class);
        final PropertyMap properties1 = mock(PropertyMap.class);
        when(recList.iterator()).thenReturn(createIteratorForMultipleRecords(aggrERec1, aggrERec2, aggrERec3));
        final ERecList eRecList1 = mock(ERecList.class);
        when(eRecList1.iterator()).thenReturn(createIterator(rec1));
        when(aggrERec1.getERecs()).thenReturn(eRecList1);
        when(rec1.getProperties()).thenReturn(properties1);
        when(aggrERec1.getRepresentative()).thenReturn(rec1);
        when(properties1.getValues(EndecaConstants.SEARCH_FIELD_RECOMMENDED_PRODUCTS)).thenReturn(recommendedProducts);
        final ERec rec2 = mock(ERec.class);
        final PropertyMap properties2 = mock(PropertyMap.class);
        final ERecList eRecList2 = mock(ERecList.class);
        when(eRecList2.iterator()).thenReturn(createIterator(rec2));
        when(aggrERec2.getERecs()).thenReturn(eRecList2);
        when(rec2.getProperties()).thenReturn(properties2);
        when(aggrERec2.getRepresentative()).thenReturn(rec2);
        when(properties2.getValues(EndecaConstants.SEARCH_FIELD_RECOMMENDED_PRODUCTS)).thenReturn(recommendedProducts2);
        final ERec rec3 = mock(ERec.class);
        final PropertyMap properties3 = mock(PropertyMap.class);
        final ERecList eRecList3 = mock(ERecList.class);
        when(eRecList3.iterator()).thenReturn(createIterator(rec3));
        when(aggrERec3.getERecs()).thenReturn(eRecList3);
        when(rec3.getProperties()).thenReturn(properties3);
        when(aggrERec3.getRepresentative()).thenReturn(rec3);
        when(properties3.getValues(EndecaConstants.SEARCH_FIELD_RECOMMENDED_PRODUCTS)).thenReturn(recommendedProducts3);
        when(properties1.getValues(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING)).thenReturn(
                Arrays.asList("Prod1"));
        when(properties2.getValues(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING)).thenReturn(
                Arrays.asList("Prod2"));
        when(properties3.getValues(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING)).thenReturn(
                Arrays.asList("Prod3"));
        final List<String> results = handler.getRecommendedProductsList(eneResults, cartridgeConfig,
                prodCodeAndRecomCode);
        Mockito.verify(eneResults, Mockito.times(1)).getNavigation();
        assertEquals(24, results.size());
    }

    @Test
    public void testGetMaxRecommendedPerProduct() {
        handler.setRecommendationProductSplitConfig("10,5,3,2,2,1");
        final TgtRecommendationBoughtBoughtConfig cartridgeConfig = Mockito
                .mock(TgtRecommendationBoughtBoughtConfig.class);
        when(cartridgeConfig
                .get(EndecaConstants.EndecaParamMarshaller.ENDECA_PRODUCT_CODES)).thenReturn("Prod1,Prod2,Prod3");
        final List<String> recommendedPerProducts = new ArrayList<>();
        recommendedPerProducts.add("Test1");
        recommendedPerProducts.add("Test2");
        recommendedPerProducts.add("Test3");
        recommendedPerProducts.add("Test4");
        recommendedPerProducts.add("Test5");
        recommendedPerProducts.add("Test6");
        recommendedPerProducts.add("Test7");
        recommendedPerProducts.add("Test8");
        final List<String> recommendedPerProducts2 = new ArrayList<>();
        recommendedPerProducts2.add("Test9");
        recommendedPerProducts2.add("Test10");
        recommendedPerProducts2.add("Test11");
        recommendedPerProducts2.add("Test12");
        recommendedPerProducts2.add("Test13");
        recommendedPerProducts2.add("Test14");
        recommendedPerProducts2.add("Test15");
        recommendedPerProducts2.add("Test16");
        final List<String> recommendedPerProducts3 = new ArrayList<>();
        recommendedPerProducts3.add("Test17");
        recommendedPerProducts3.add("Test18");
        recommendedPerProducts3.add("Test19");
        recommendedPerProducts3.add("Test20");
        recommendedPerProducts3.add("Test21");
        recommendedPerProducts3.add("Test22");
        recommendedPerProducts3.add("Test23");
        recommendedPerProducts3.add("Test24");
        final List<Record> records = new ArrayList<>();
        for (int i = 1; i <= 24; i++) {
            final Record record = new Record();
            final Map<String, Attribute> attributes = new HashMap<String, Attribute>();
            final Attribute attribute = new Attribute();
            attribute.add("Test" + i);
            attributes.put(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING, attribute);
            record.setAttributes(attributes);
            records.add(record);
        }
        final Map<String, List<String>> prodCodeAndRecomCodes = new HashMap<String, List<String>>();
        prodCodeAndRecomCodes.put("Prod1", recommendedPerProducts);
        prodCodeAndRecomCodes.put("Prod2", recommendedPerProducts2);
        prodCodeAndRecomCodes.put("Prod3", recommendedPerProducts3);
        final List<Record> results = handler.getMaxRecomRecordsPerProduct(records, prodCodeAndRecomCodes);
        Assert.assertEquals(9, results.size());
        Assert.assertEquals("Test1",
                results.get(0).getAttributes().get(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING).toString());
        Assert.assertEquals("Test2",
                results.get(1).getAttributes().get(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING).toString());
        Assert.assertEquals("Test3",
                results.get(2).getAttributes().get(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING).toString());
        Assert.assertEquals("Test9",
                results.get(3).getAttributes().get(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING).toString());
        Assert.assertEquals("Test10",
                results.get(4).getAttributes().get(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING).toString());
        Assert.assertEquals("Test11",
                results.get(5).getAttributes().get(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING).toString());
        Assert.assertEquals("Test17",
                results.get(6).getAttributes().get(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING).toString());
        Assert.assertEquals("Test18",
                results.get(7).getAttributes().get(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING).toString());
        Assert.assertEquals("Test19",
                results.get(8).getAttributes().get(EndecaConstants.SEARCH_FIELD_CV_PRODUCT_CODE_STRING).toString());
    }

    /**
     * Creates the iterator for multiple records.
     *
     * @param object1
     *            the object1
     * @param object2
     *            the object2
     * @param object3
     *            the object3
     * @return the iterator
     */
    private Iterator createIteratorForMultipleRecords(final Object object1, final Object object2,
            final Object object3) {
        final List list = new ArrayList();
        list.add(object1);
        list.add(object2);
        list.add(object3);
        return list.iterator();
    }

    private Iterator createIterator(final Object object) {
        final List list = new ArrayList();
        list.add(object);
        return list.iterator();
    }

}

/**
 * 
 */
package au.com.target.endeca.infront.cartridge;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;

import com.endeca.infront.cartridge.model.SortOptionConfig;
import com.endeca.infront.navigation.model.SortSpec;


/**
 * @author rsamuel3
 * 
 */
public class TgtResultsListHandlerTest {

    private TgtResultsListHandler handler = null;

    @Before
    public void setUp() {
        handler = new TgtResultsListHandler();
        final List<SortOptionConfig> sortOptions = buildSortOptionConfig("price", "reviewAvgRating", "brand",
                "category");
        handler.setSortOptions(sortOptions);
    }

    @Test
    public void testValidateAndAddSortOptions() {
        final List<SortSpec> sortSpec = buildSortSpec("reviewAvgRating", true);
        final List<SortSpec> validSortSpec = handler.validateAndAddSortOptions(sortSpec);
        Assertions.assertThat(validSortSpec).isNotNull().isNotEmpty().hasSize(2);
    }

    @Test
    public void testValidateAndAddSortOptionsWithOnlyInStock() {
        final List<SortSpec> sortSpec = buildSortSpec("", true);
        final List<SortSpec> validSortSpec = handler.validateAndAddSortOptions(sortSpec);
        Assertions.assertThat(validSortSpec).isNotNull().isNotEmpty().hasSize(1);
        final SortSpec sortSpec2 = validSortSpec.get(0);
        Assertions.assertThat(sortSpec2.getKey()).isEqualTo("inStockFlag");
        Assertions.assertThat(sortSpec2.isDescending()).isFalse();
    }

    /**
     * @return List of SortSpec
     */
    public List<SortSpec> buildSortSpec(final String specKey, final boolean isDescending) {
        final List<SortSpec> sortSpec = new ArrayList<>();
        final SortSpec spec1 = new SortSpec();
        spec1.setKey("inStockFlag");
        spec1.setDescending(false);
        sortSpec.add(spec1);
        if (StringUtils.isNotBlank(specKey)) {
            final SortSpec spec2 = new SortSpec();
            spec2.setKey(specKey);
            spec2.setDescending(isDescending);
            sortSpec.add(spec2);
        }
        return sortSpec;
    }

    /**
     * @return List of SortOptionConfig
     */
    private List<SortOptionConfig> buildSortOptionConfig(final String... options) {
        final List<SortOptionConfig> config = new ArrayList<>();
        for (final String option : options) {
            final SortOptionConfig soconfig = new SortOptionConfig();
            soconfig.setLabel(option);
            soconfig.setValue("inStockFlag|0||" + option + "|1");
            config.add(soconfig);
        }
        return config;
    }
}
